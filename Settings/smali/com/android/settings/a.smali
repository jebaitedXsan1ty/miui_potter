.class public abstract Lcom/android/settings/a;
.super Lcom/android/settings/bL;
.source "ConfirmDeviceCredentialBaseActivity.java"


# instance fields
.field private bqG:Lcom/android/settings/ConfirmDeviceCredentialBaseActivity$ConfirmCredentialTheme;

.field private bqH:Z

.field private bqI:Z

.field private bqJ:Z

.field private bqK:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/android/settings/bL;-><init>()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/settings/a;->bqI:Z

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/settings/a;->bqJ:Z

    return-void
.end method

.method private bee()Lcom/android/settings/ConfirmDeviceCredentialBaseFragment;
    .locals 3

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/android/settings/a;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    const v1, 0x7f0a0284

    invoke-virtual {v0, v1}, Landroid/app/FragmentManager;->findFragmentById(I)Landroid/app/Fragment;

    move-result-object v0

    if-eqz v0, :cond_0

    instance-of v1, v0, Lcom/android/settings/ConfirmDeviceCredentialBaseFragment;

    if-eqz v1, :cond_0

    check-cast v0, Lcom/android/settings/ConfirmDeviceCredentialBaseFragment;

    return-object v0

    :cond_0
    return-object v2
.end method


# virtual methods
.method public bef()V
    .locals 1

    invoke-direct {p0}, Lcom/android/settings/a;->bee()Lcom/android/settings/ConfirmDeviceCredentialBaseFragment;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/settings/ConfirmDeviceCredentialBaseFragment;->bpb()V

    return-void
.end method

.method public beg()V
    .locals 1

    invoke-direct {p0}, Lcom/android/settings/a;->bee()Lcom/android/settings/ConfirmDeviceCredentialBaseFragment;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/settings/ConfirmDeviceCredentialBaseFragment;->bpc()V

    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/android/settings/a;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/android/settings/aq;->bqz(Landroid/content/Context;Landroid/os/Bundle;)I

    move-result v0

    invoke-static {p0, v0}, Lcom/android/settings/aq;->bqA(Landroid/content/Context;I)I

    move-result v0

    invoke-static {p0}, Landroid/os/UserManager;->get(Landroid/content/Context;)Landroid/os/UserManager;

    move-result-object v3

    invoke-virtual {v3, v0}, Landroid/os/UserManager;->isManagedProfile(I)Z

    move-result v0

    if-eqz v0, :cond_3

    const v0, 0x7f1301e8

    invoke-virtual {p0, v0}, Lcom/android/settings/a;->setTheme(I)V

    sget-object v0, Lcom/android/settings/ConfirmDeviceCredentialBaseActivity$ConfirmCredentialTheme;->bqN:Lcom/android/settings/ConfirmDeviceCredentialBaseActivity$ConfirmCredentialTheme;

    iput-object v0, p0, Lcom/android/settings/a;->bqG:Lcom/android/settings/ConfirmDeviceCredentialBaseActivity$ConfirmCredentialTheme;

    :goto_0
    invoke-super {p0, p1}, Lcom/android/settings/bL;->onCreate(Landroid/os/Bundle;)V

    iget-object v0, p0, Lcom/android/settings/a;->bqG:Lcom/android/settings/ConfirmDeviceCredentialBaseActivity$ConfirmCredentialTheme;

    sget-object v3, Lcom/android/settings/ConfirmDeviceCredentialBaseActivity$ConfirmCredentialTheme;->bqM:Lcom/android/settings/ConfirmDeviceCredentialBaseActivity$ConfirmCredentialTheme;

    if-ne v0, v3, :cond_0

    const v0, 0x7f0a00ff

    invoke-virtual {p0, v0}, Lcom/android/settings/a;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setFitsSystemWindows(Z)V

    :cond_0
    invoke-virtual {p0}, Lcom/android/settings/a;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v3, 0x2000

    invoke-virtual {v0, v3}, Landroid/view/Window;->addFlags(I)V

    if-nez p1, :cond_5

    const-class v0, Landroid/app/KeyguardManager;

    invoke-virtual {p0, v0}, Lcom/android/settings/a;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/KeyguardManager;

    invoke-virtual {v0}, Landroid/app/KeyguardManager;->isKeyguardLocked()Z

    move-result v0

    :goto_1
    iput-boolean v0, p0, Lcom/android/settings/a;->bqJ:Z

    iget-boolean v0, p0, Lcom/android/settings/a;->bqJ:Z

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/android/settings/a;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v3, "com.android.settings.ConfirmCredentials.showWhenLocked"

    invoke-virtual {v0, v3, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/android/settings/a;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/high16 v3, 0x80000

    invoke-virtual {v0, v3}, Landroid/view/Window;->addFlags(I)V

    :cond_1
    invoke-virtual {p0}, Lcom/android/settings/a;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v3, "com.android.settings.ConfirmCredentials.title"

    invoke-virtual {v0, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/settings/a;->setTitle(Ljava/lang/CharSequence;)V

    invoke-virtual {p0}, Lcom/android/settings/a;->getActionBar()Lmiui/app/ActionBar;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lcom/android/settings/a;->getActionBar()Lmiui/app/ActionBar;

    move-result-object v0

    invoke-virtual {v0, v1}, Lmiui/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    invoke-virtual {p0}, Lcom/android/settings/a;->getActionBar()Lmiui/app/ActionBar;

    move-result-object v0

    invoke-virtual {v0, v1}, Lmiui/app/ActionBar;->setHomeButtonEnabled(Z)V

    :cond_2
    if-eqz p1, :cond_6

    move v0, v1

    :goto_2
    iput-boolean v0, p0, Lcom/android/settings/a;->bqK:Z

    return-void

    :cond_3
    invoke-virtual {p0}, Lcom/android/settings/a;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v3, "com.android.settings.ConfirmCredentials.darkTheme"

    invoke-virtual {v0, v3, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_4

    const v0, 0x7f1301e7

    invoke-virtual {p0, v0}, Lcom/android/settings/a;->setTheme(I)V

    sget-object v0, Lcom/android/settings/ConfirmDeviceCredentialBaseActivity$ConfirmCredentialTheme;->bqL:Lcom/android/settings/ConfirmDeviceCredentialBaseActivity$ConfirmCredentialTheme;

    iput-object v0, p0, Lcom/android/settings/a;->bqG:Lcom/android/settings/ConfirmDeviceCredentialBaseActivity$ConfirmCredentialTheme;

    goto/16 :goto_0

    :cond_4
    const v0, 0x7f13012c

    invoke-virtual {p0, v0}, Lcom/android/settings/a;->setTheme(I)V

    sget-object v0, Lcom/android/settings/ConfirmDeviceCredentialBaseActivity$ConfirmCredentialTheme;->bqM:Lcom/android/settings/ConfirmDeviceCredentialBaseActivity$ConfirmCredentialTheme;

    iput-object v0, p0, Lcom/android/settings/a;->bqG:Lcom/android/settings/ConfirmDeviceCredentialBaseActivity$ConfirmCredentialTheme;

    goto/16 :goto_0

    :cond_5
    const-string/jumbo v0, "STATE_IS_KEYGUARD_LOCKED"

    invoke-virtual {p1, v0, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    goto :goto_1

    :cond_6
    move v0, v2

    goto :goto_2
.end method

.method public onEnterAnimationComplete()V
    .locals 1

    invoke-super {p0}, Lcom/android/settings/bL;->onEnterAnimationComplete()V

    iget-boolean v0, p0, Lcom/android/settings/a;->bqH:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/android/settings/a;->beg()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/settings/a;->bqH:Z

    :cond_0
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x102002c

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/android/settings/a;->finish()V

    const/4 v0, 0x1

    return v0

    :cond_0
    invoke-super {p0, p1}, Lcom/android/settings/bL;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    return v0
.end method

.method public onResume()V
    .locals 2

    invoke-super {p0}, Lcom/android/settings/bL;->onResume()V

    invoke-virtual {p0}, Lcom/android/settings/a;->isChangingConfigurations()Z

    move-result v0

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/android/settings/a;->bqK:Z

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/a;->bqG:Lcom/android/settings/ConfirmDeviceCredentialBaseActivity$ConfirmCredentialTheme;

    sget-object v1, Lcom/android/settings/ConfirmDeviceCredentialBaseActivity$ConfirmCredentialTheme;->bqL:Lcom/android/settings/ConfirmDeviceCredentialBaseActivity$ConfirmCredentialTheme;

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/android/settings/a;->bqI:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/settings/a;->bqI:Z

    invoke-virtual {p0}, Lcom/android/settings/a;->bef()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/settings/a;->bqH:Z

    :cond_0
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Lcom/android/settings/bL;->onSaveInstanceState(Landroid/os/Bundle;)V

    const-string/jumbo v0, "STATE_IS_KEYGUARD_LOCKED"

    iget-boolean v1, p0, Lcom/android/settings/a;->bqJ:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    return-void
.end method
