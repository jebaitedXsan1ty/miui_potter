.class final Lcom/android/settings/if;
.super Ljava/lang/Object;
.source "RadioInfo.java"

# interfaces
.implements Landroid/view/MenuItem$OnMenuItemClickListener;


# instance fields
.field final synthetic coa:Lcom/android/settings/RadioInfo;


# direct methods
.method constructor <init>(Lcom/android/settings/RadioInfo;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/if;->coa:Lcom/android/settings/RadioInfo;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onMenuItemClick(Landroid/view/MenuItem;)Z
    .locals 11

    const/4 v10, 0x1

    iget-object v0, p0, Lcom/android/settings/if;->coa:Lcom/android/settings/RadioInfo;

    invoke-static {v0}, Lcom/android/settings/RadioInfo;->bGZ(Lcom/android/settings/RadioInfo;)Lcom/android/internal/telephony/Phone;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/internal/telephony/Phone;->isImsRegistered()Z

    move-result v0

    iget-object v1, p0, Lcom/android/settings/if;->coa:Lcom/android/settings/RadioInfo;

    invoke-static {v1}, Lcom/android/settings/RadioInfo;->bGZ(Lcom/android/settings/RadioInfo;)Lcom/android/internal/telephony/Phone;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/internal/telephony/Phone;->isVolteEnabled()Z

    move-result v3

    iget-object v1, p0, Lcom/android/settings/if;->coa:Lcom/android/settings/RadioInfo;

    invoke-static {v1}, Lcom/android/settings/RadioInfo;->bGZ(Lcom/android/settings/RadioInfo;)Lcom/android/internal/telephony/Phone;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/internal/telephony/Phone;->isWifiCallingEnabled()Z

    move-result v4

    iget-object v1, p0, Lcom/android/settings/if;->coa:Lcom/android/settings/RadioInfo;

    invoke-static {v1}, Lcom/android/settings/RadioInfo;->bGZ(Lcom/android/settings/RadioInfo;)Lcom/android/internal/telephony/Phone;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/internal/telephony/Phone;->isVideoEnabled()Z

    move-result v5

    iget-object v1, p0, Lcom/android/settings/if;->coa:Lcom/android/settings/RadioInfo;

    invoke-static {v1}, Lcom/android/settings/RadioInfo;->bGZ(Lcom/android/settings/RadioInfo;)Lcom/android/internal/telephony/Phone;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/internal/telephony/Phone;->isUtEnabled()Z

    move-result v6

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/if;->coa:Lcom/android/settings/RadioInfo;

    const v1, 0x7f120dde

    invoke-virtual {v0, v1}, Lcom/android/settings/RadioInfo;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    iget-object v1, p0, Lcom/android/settings/if;->coa:Lcom/android/settings/RadioInfo;

    const v2, 0x7f120dda

    invoke-virtual {v1, v2}, Lcom/android/settings/RadioInfo;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/android/settings/if;->coa:Lcom/android/settings/RadioInfo;

    const v7, 0x7f120ddb

    invoke-virtual {v2, v7}, Lcom/android/settings/RadioInfo;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v7, p0, Lcom/android/settings/if;->coa:Lcom/android/settings/RadioInfo;

    const/4 v8, 0x5

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    aput-object v0, v8, v9

    if-eqz v3, :cond_1

    move-object v0, v1

    :goto_1
    aput-object v0, v8, v10

    if-eqz v4, :cond_2

    move-object v0, v1

    :goto_2
    const/4 v3, 0x2

    aput-object v0, v8, v3

    if-eqz v5, :cond_3

    move-object v0, v1

    :goto_3
    const/4 v3, 0x3

    aput-object v0, v8, v3

    if-eqz v6, :cond_4

    :goto_4
    const/4 v0, 0x4

    aput-object v1, v8, v0

    const v0, 0x7f120ddc

    invoke-virtual {v7, v0, v8}, Lcom/android/settings/RadioInfo;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Landroid/app/AlertDialog$Builder;

    iget-object v2, p0, Lcom/android/settings/if;->coa:Lcom/android/settings/RadioInfo;

    invoke-direct {v1, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, v0}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/if;->coa:Lcom/android/settings/RadioInfo;

    const v2, 0x7f120ddf

    invoke-virtual {v1, v2}, Lcom/android/settings/RadioInfo;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    return v10

    :cond_0
    iget-object v0, p0, Lcom/android/settings/if;->coa:Lcom/android/settings/RadioInfo;

    const v1, 0x7f120ddd

    invoke-virtual {v0, v1}, Lcom/android/settings/RadioInfo;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_1
    move-object v0, v2

    goto :goto_1

    :cond_2
    move-object v0, v2

    goto :goto_2

    :cond_3
    move-object v0, v2

    goto :goto_3

    :cond_4
    move-object v1, v2

    goto :goto_4
.end method
