.class Lcom/android/settings/cq;
.super Landroid/content/BroadcastReceiver;
.source "PrivacyModeDialog.java"

# interfaces
.implements Landroid/content/DialogInterface$OnDismissListener;


# instance fields
.field private bXJ:Landroid/app/Dialog;

.field final synthetic bXK:Lcom/android/settings/PrivacyModeDialog;

.field private mContext:Landroid/content/Context;


# direct methods
.method constructor <init>(Lcom/android/settings/PrivacyModeDialog;Landroid/content/Context;Landroid/app/Dialog;)V
    .locals 2

    iput-object p1, p0, Lcom/android/settings/cq;->bXK:Lcom/android/settings/PrivacyModeDialog;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    iput-object p2, p0, Lcom/android/settings/cq;->mContext:Landroid/content/Context;

    iput-object p3, p0, Lcom/android/settings/cq;->bXJ:Landroid/app/Dialog;

    new-instance v0, Landroid/content/IntentFilter;

    const-string/jumbo v1, "android.intent.action.SCREEN_OFF"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    const-string/jumbo v1, "android.intent.action.CLOSE_SYSTEM_DIALOGS"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    invoke-virtual {p2, p0, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    return-void
.end method


# virtual methods
.method public onDismiss(Landroid/content/DialogInterface;)V
    .locals 1

    iget-object v0, p0, Lcom/android/settings/cq;->mContext:Landroid/content/Context;

    invoke-virtual {v0, p0}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    iget-object v0, p0, Lcom/android/settings/cq;->bXK:Lcom/android/settings/PrivacyModeDialog;

    invoke-virtual {v0}, Lcom/android/settings/PrivacyModeDialog;->finish()V

    return-void
.end method

.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 1

    iget-object v0, p0, Lcom/android/settings/cq;->bXJ:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->cancel()V

    return-void
.end method
