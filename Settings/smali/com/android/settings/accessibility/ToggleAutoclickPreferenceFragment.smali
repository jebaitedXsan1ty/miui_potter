.class public Lcom/android/settings/accessibility/ToggleAutoclickPreferenceFragment;
.super Lcom/android/settings/accessibility/ToggleFeaturePreferenceFragment;
.source "ToggleAutoclickPreferenceFragment.java"

# interfaces
.implements Lcom/android/settings/widget/M;
.implements Landroid/preference/Preference$OnPreferenceChangeListener;


# static fields
.field private static final afa:[I


# instance fields
.field private afb:Lcom/android/settings/MiuiSeekBarPreference;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/high16 v0, 0x7f100000

    const v1, 0x7f100004

    const v2, 0x7f100002

    const v3, 0x7f100001

    const v4, 0x7f100003

    filled-new-array {v0, v1, v2, v3, v4}, [I

    move-result-object v0

    sput-object v0, Lcom/android/settings/accessibility/ToggleAutoclickPreferenceFragment;->afa:[I

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/accessibility/ToggleFeaturePreferenceFragment;-><init>()V

    return-void
.end method

.method static VV(Landroid/content/res/Resources;I)Ljava/lang/CharSequence;
    .locals 4

    invoke-static {p1}, Lcom/android/settings/accessibility/ToggleAutoclickPreferenceFragment;->VX(I)I

    move-result v0

    sget-object v1, Lcom/android/settings/accessibility/ToggleAutoclickPreferenceFragment;->afa:[I

    aget v0, v1, v0

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-virtual {p0, v0, p1, v1}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private VW(I)I
    .locals 1

    add-int/lit16 v0, p1, -0xc8

    div-int/lit8 v0, v0, 0x64

    return v0
.end method

.method private static VX(I)I
    .locals 2

    const/16 v0, 0xc8

    if-gt p0, v0, :cond_0

    const/4 v0, 0x0

    return v0

    :cond_0
    const/16 v0, 0x3e8

    if-lt p0, v0, :cond_1

    sget-object v0, Lcom/android/settings/accessibility/ToggleAutoclickPreferenceFragment;->afa:[I

    array-length v0, v0

    add-int/lit8 v0, v0, -0x1

    return v0

    :cond_1
    sget-object v0, Lcom/android/settings/accessibility/ToggleAutoclickPreferenceFragment;->afa:[I

    array-length v0, v0

    add-int/lit8 v0, v0, -0x1

    const/16 v1, 0x320

    div-int v0, v1, v0

    add-int/lit16 v1, p0, -0xc8

    div-int v0, v1, v0

    return v0
.end method

.method private VY(I)I
    .locals 1

    mul-int/lit8 v0, p1, 0x64

    add-int/lit16 v0, v0, 0xc8

    return v0
.end method


# virtual methods
.method protected Uh()V
    .locals 5

    const/4 v2, 0x0

    const/4 v1, 0x1

    invoke-super {p0}, Lcom/android/settings/accessibility/ToggleFeaturePreferenceFragment;->Uh()V

    invoke-virtual {p0}, Lcom/android/settings/accessibility/ToggleAutoclickPreferenceFragment;->bWB()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v3, "accessibility_autoclick_enabled"

    invoke-static {v0, v3, v2}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v3

    iget-object v4, p0, Lcom/android/settings/accessibility/ToggleAutoclickPreferenceFragment;->adw:Lcom/android/settings/widget/TogglePreference;

    if-ne v3, v1, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {v4, v0}, Lcom/android/settings/widget/TogglePreference;->setCheckedInternal(Z)V

    iget-object v0, p0, Lcom/android/settings/accessibility/ToggleAutoclickPreferenceFragment;->adw:Lcom/android/settings/widget/TogglePreference;

    invoke-virtual {v0, p0}, Lcom/android/settings/widget/TogglePreference;->aCY(Lcom/android/settings/widget/M;)V

    iget-object v0, p0, Lcom/android/settings/accessibility/ToggleAutoclickPreferenceFragment;->afb:Lcom/android/settings/MiuiSeekBarPreference;

    if-ne v3, v1, :cond_1

    :goto_1
    invoke-virtual {v0, v1}, Lcom/android/settings/MiuiSeekBarPreference;->setEnabled(Z)V

    return-void

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    move v1, v2

    goto :goto_1
.end method

.method protected Ui()V
    .locals 2

    invoke-super {p0}, Lcom/android/settings/accessibility/ToggleFeaturePreferenceFragment;->Ui()V

    iget-object v0, p0, Lcom/android/settings/accessibility/ToggleAutoclickPreferenceFragment;->adw:Lcom/android/settings/widget/TogglePreference;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/android/settings/widget/TogglePreference;->aCY(Lcom/android/settings/widget/M;)V

    return-void
.end method

.method protected Uu(Ljava/lang/String;Z)V
    .locals 2

    invoke-virtual {p0}, Lcom/android/settings/accessibility/ToggleAutoclickPreferenceFragment;->bWB()Landroid/content/ContentResolver;

    move-result-object v1

    if-eqz p2, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v1, p1, v0}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    iget-object v0, p0, Lcom/android/settings/accessibility/ToggleAutoclickPreferenceFragment;->afb:Lcom/android/settings/MiuiSeekBarPreference;

    invoke-virtual {v0, p2}, Lcom/android/settings/MiuiSeekBarPreference;->setEnabled(Z)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public VR(Lcom/android/settings/widget/TogglePreference;Z)Z
    .locals 1

    const-string/jumbo v0, "accessibility_autoclick_enabled"

    invoke-virtual {p0, v0, p2}, Lcom/android/settings/accessibility/ToggleAutoclickPreferenceFragment;->Uu(Ljava/lang/String;Z)V

    const/4 v0, 0x0

    return v0
.end method

.method public getMetricsCategory()I
    .locals 1

    const/16 v0, 0x14f

    return v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3

    invoke-super {p0, p1}, Lcom/android/settings/accessibility/ToggleFeaturePreferenceFragment;->onCreate(Landroid/os/Bundle;)V

    const v0, 0x7f150005

    invoke-virtual {p0, v0}, Lcom/android/settings/accessibility/ToggleAutoclickPreferenceFragment;->addPreferencesFromResource(I)V

    invoke-virtual {p0}, Lcom/android/settings/accessibility/ToggleAutoclickPreferenceFragment;->bWB()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v1, "accessibility_autoclick_delay"

    const/16 v2, 0x258

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    const-string/jumbo v0, "autoclick_delay"

    invoke-virtual {p0, v0}, Lcom/android/settings/accessibility/ToggleAutoclickPreferenceFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Lcom/android/settings/MiuiSeekBarPreference;

    iput-object v0, p0, Lcom/android/settings/accessibility/ToggleAutoclickPreferenceFragment;->afb:Lcom/android/settings/MiuiSeekBarPreference;

    iget-object v0, p0, Lcom/android/settings/accessibility/ToggleAutoclickPreferenceFragment;->afb:Lcom/android/settings/MiuiSeekBarPreference;

    const/16 v2, 0x3e8

    invoke-direct {p0, v2}, Lcom/android/settings/accessibility/ToggleAutoclickPreferenceFragment;->VW(I)I

    move-result v2

    invoke-virtual {v0, v2}, Lcom/android/settings/MiuiSeekBarPreference;->bzk(I)V

    iget-object v0, p0, Lcom/android/settings/accessibility/ToggleAutoclickPreferenceFragment;->afb:Lcom/android/settings/MiuiSeekBarPreference;

    invoke-direct {p0, v1}, Lcom/android/settings/accessibility/ToggleAutoclickPreferenceFragment;->VW(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/android/settings/MiuiSeekBarPreference;->setProgress(I)V

    iget-object v0, p0, Lcom/android/settings/accessibility/ToggleAutoclickPreferenceFragment;->afb:Lcom/android/settings/MiuiSeekBarPreference;

    invoke-virtual {v0, p0}, Lcom/android/settings/MiuiSeekBarPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    return-void
.end method

.method public onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 4

    const/4 v3, 0x1

    iget-object v0, p0, Lcom/android/settings/accessibility/ToggleAutoclickPreferenceFragment;->afb:Lcom/android/settings/MiuiSeekBarPreference;

    if-ne p1, v0, :cond_0

    instance-of v0, p2, Ljava/lang/Integer;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/android/settings/accessibility/ToggleAutoclickPreferenceFragment;->bWB()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v1, "accessibility_autoclick_delay"

    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-direct {p0, v2}, Lcom/android/settings/accessibility/ToggleAutoclickPreferenceFragment;->VY(I)I

    move-result v2

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    return v3

    :cond_0
    iget-object v0, p0, Lcom/android/settings/accessibility/ToggleAutoclickPreferenceFragment;->adw:Lcom/android/settings/widget/TogglePreference;

    if-ne p1, v0, :cond_1

    const-string/jumbo v0, "accessibility_autoclick_enabled"

    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/android/settings/accessibility/ToggleAutoclickPreferenceFragment;->Uu(Ljava/lang/String;Z)V

    return v3

    :cond_1
    const/4 v0, 0x0

    return v0
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 1

    invoke-super {p0, p1, p2}, Lcom/android/settings/accessibility/ToggleFeaturePreferenceFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    const v0, 0x7f120040

    invoke-virtual {p0, v0}, Lcom/android/settings/accessibility/ToggleAutoclickPreferenceFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/settings/accessibility/ToggleAutoclickPreferenceFragment;->Uk(Ljava/lang/String;)V

    return-void
.end method
