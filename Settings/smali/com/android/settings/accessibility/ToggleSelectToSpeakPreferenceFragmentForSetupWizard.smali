.class public Lcom/android/settings/accessibility/ToggleSelectToSpeakPreferenceFragmentForSetupWizard;
.super Lcom/android/settings/accessibility/ToggleAccessibilityServicePreferenceFragment;
.source "ToggleSelectToSpeakPreferenceFragmentForSetupWizard.java"


# instance fields
.field private adu:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/accessibility/ToggleAccessibilityServicePreferenceFragment;-><init>()V

    return-void
.end method


# virtual methods
.method protected Ue(Landroid/os/Bundle;)V
    .locals 1

    invoke-super {p0, p1}, Lcom/android/settings/accessibility/ToggleAccessibilityServicePreferenceFragment;->Ue(Landroid/os/Bundle;)V

    iget-object v0, p0, Lcom/android/settings/accessibility/ToggleSelectToSpeakPreferenceFragmentForSetupWizard;->adw:Lcom/android/settings/widget/TogglePreference;

    invoke-virtual {v0}, Lcom/android/settings/widget/TogglePreference;->isChecked()Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/settings/accessibility/ToggleSelectToSpeakPreferenceFragmentForSetupWizard;->adu:Z

    return-void
.end method

.method public getMetricsCategory()I
    .locals 1

    const/16 v0, 0x173

    return v0
.end method

.method public onStop()V
    .locals 4

    iget-object v0, p0, Lcom/android/settings/accessibility/ToggleSelectToSpeakPreferenceFragmentForSetupWizard;->adw:Lcom/android/settings/widget/TogglePreference;

    invoke-virtual {v0}, Lcom/android/settings/widget/TogglePreference;->isChecked()Z

    move-result v0

    iget-boolean v1, p0, Lcom/android/settings/accessibility/ToggleSelectToSpeakPreferenceFragmentForSetupWizard;->adu:Z

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/android/settings/accessibility/ToggleSelectToSpeakPreferenceFragmentForSetupWizard;->mMetricsFeatureProvider:Lcom/android/settings/core/instrumentation/e;

    invoke-virtual {p0}, Lcom/android/settings/accessibility/ToggleSelectToSpeakPreferenceFragmentForSetupWizard;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/android/settings/accessibility/ToggleSelectToSpeakPreferenceFragmentForSetupWizard;->adw:Lcom/android/settings/widget/TogglePreference;

    invoke-virtual {v2}, Lcom/android/settings/widget/TogglePreference;->isChecked()Z

    move-result v2

    const/16 v3, 0x331

    invoke-virtual {v0, v1, v3, v2}, Lcom/android/settings/core/instrumentation/e;->ajV(Landroid/content/Context;IZ)V

    :cond_0
    invoke-super {p0}, Lcom/android/settings/accessibility/ToggleAccessibilityServicePreferenceFragment;->onStop()V

    return-void
.end method
