.class public Lcom/android/settings/accessibility/CaptionPropertiesFragment;
.super Lcom/android/settings/MiuiSettingsPreferenceFragment;
.source "CaptionPropertiesFragment.java"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceChangeListener;
.implements Lcom/android/settings/accessibility/MiuiListDialogPreference$OnValueChangedListener;


# instance fields
.field private aeA:Landroid/view/View;

.field private aeB:Landroid/view/View;

.field private aeC:Z

.field private aeD:Landroid/preference/ListPreference;

.field private aeE:Lcom/android/settings/accessibility/ColorPreference;

.field private aeF:Lcom/android/settings/accessibility/ColorPreference;

.field private aeo:Lcom/android/settings/accessibility/ColorPreference;

.field private aep:Lcom/android/settings/accessibility/ColorPreference;

.field private aeq:Landroid/view/accessibility/CaptioningManager;

.field private aer:Landroid/preference/PreferenceCategory;

.field private aes:Lcom/android/settings/accessibility/ColorPreference;

.field private aet:Lcom/android/settings/accessibility/EdgeTypePreference;

.field private aeu:Landroid/preference/ListPreference;

.field private aev:Lcom/android/settings/accessibility/ColorPreference;

.field private aew:Lcom/android/settings/accessibility/ColorPreference;

.field private aex:Lcom/android/settings/accessibility/LocalePreference;

.field private aey:Lcom/android/settings/accessibility/PresetPreference;

.field private aez:Lcom/android/internal/widget/SubtitleView;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/MiuiSettingsPreferenceFragment;-><init>()V

    return-void
.end method

.method public static Vf(Landroid/view/accessibility/CaptioningManager;Lcom/android/internal/widget/SubtitleView;Landroid/view/View;I)V
    .locals 5

    const v4, 0x7f1203da

    invoke-virtual {p1, p3}, Lcom/android/internal/widget/SubtitleView;->setStyle(I)V

    invoke-virtual {p1}, Lcom/android/internal/widget/SubtitleView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    invoke-virtual {p0}, Landroid/view/accessibility/CaptioningManager;->getFontScale()F

    move-result v1

    if-eqz p2, :cond_0

    invoke-virtual {p2}, Landroid/view/View;->getWidth()I

    move-result v2

    mul-int/lit8 v2, v2, 0x9

    invoke-virtual {p2}, Landroid/view/View;->getHeight()I

    move-result v3

    mul-int/lit8 v3, v3, 0x10

    invoke-static {v2, v3}, Ljava/lang/Math;->max(II)I

    move-result v2

    int-to-float v2, v2

    const/high16 v3, 0x41800000    # 16.0f

    div-float/2addr v2, v3

    const v3, 0x3d5a511a    # 0.0533f

    mul-float/2addr v2, v3

    mul-float/2addr v1, v2

    invoke-virtual {p1, v1}, Lcom/android/internal/widget/SubtitleView;->setTextSize(F)V

    :goto_0
    invoke-virtual {p0}, Landroid/view/accessibility/CaptioningManager;->getLocale()Ljava/util/Locale;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-static {v0, v1, v4}, Lcom/android/settingslib/i/a;->cpj(Landroid/content/Context;Ljava/util/Locale;I)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/android/internal/widget/SubtitleView;->setText(Ljava/lang/CharSequence;)V

    :goto_1
    return-void

    :cond_0
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f070084

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    mul-float/2addr v1, v2

    invoke-virtual {p1, v1}, Lcom/android/internal/widget/SubtitleView;->setTextSize(F)V

    goto :goto_0

    :cond_1
    invoke-virtual {p1, v4}, Lcom/android/internal/widget/SubtitleView;->setText(I)V

    goto :goto_1
.end method

.method private Vh()V
    .locals 9

    const/4 v8, 0x1

    const/4 v7, 0x0

    const-string/jumbo v0, "captioning_locale"

    invoke-virtual {p0, v0}, Lcom/android/settings/accessibility/CaptionPropertiesFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Lcom/android/settings/accessibility/LocalePreference;

    iput-object v0, p0, Lcom/android/settings/accessibility/CaptionPropertiesFragment;->aex:Lcom/android/settings/accessibility/LocalePreference;

    const-string/jumbo v0, "captioning_font_size"

    invoke-virtual {p0, v0}, Lcom/android/settings/accessibility/CaptionPropertiesFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/ListPreference;

    iput-object v0, p0, Lcom/android/settings/accessibility/CaptionPropertiesFragment;->aeu:Landroid/preference/ListPreference;

    invoke-virtual {p0}, Lcom/android/settings/accessibility/CaptionPropertiesFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v0, 0x7f030047

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getIntArray(I)[I

    move-result-object v2

    const v0, 0x7f030046

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v3

    const-string/jumbo v0, "captioning_preset"

    invoke-virtual {p0, v0}, Lcom/android/settings/accessibility/CaptionPropertiesFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Lcom/android/settings/accessibility/PresetPreference;

    iput-object v0, p0, Lcom/android/settings/accessibility/CaptionPropertiesFragment;->aey:Lcom/android/settings/accessibility/PresetPreference;

    iget-object v0, p0, Lcom/android/settings/accessibility/CaptionPropertiesFragment;->aey:Lcom/android/settings/accessibility/PresetPreference;

    invoke-virtual {v0, v2}, Lcom/android/settings/accessibility/PresetPreference;->Uy([I)V

    iget-object v0, p0, Lcom/android/settings/accessibility/CaptionPropertiesFragment;->aey:Lcom/android/settings/accessibility/PresetPreference;

    invoke-virtual {v0, v3}, Lcom/android/settings/accessibility/PresetPreference;->og([Ljava/lang/CharSequence;)V

    const-string/jumbo v0, "custom"

    invoke-virtual {p0, v0}, Lcom/android/settings/accessibility/CaptionPropertiesFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceCategory;

    iput-object v0, p0, Lcom/android/settings/accessibility/CaptionPropertiesFragment;->aer:Landroid/preference/PreferenceCategory;

    iput-boolean v8, p0, Lcom/android/settings/accessibility/CaptionPropertiesFragment;->aeC:Z

    const v0, 0x7f03003f

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getIntArray(I)[I

    move-result-object v2

    const v0, 0x7f03003e

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v3

    iget-object v0, p0, Lcom/android/settings/accessibility/CaptionPropertiesFragment;->aer:Landroid/preference/PreferenceCategory;

    const-string/jumbo v4, "captioning_foreground_color"

    invoke-virtual {v0, v4}, Landroid/preference/PreferenceCategory;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Lcom/android/settings/accessibility/ColorPreference;

    iput-object v0, p0, Lcom/android/settings/accessibility/CaptionPropertiesFragment;->aev:Lcom/android/settings/accessibility/ColorPreference;

    iget-object v0, p0, Lcom/android/settings/accessibility/CaptionPropertiesFragment;->aev:Lcom/android/settings/accessibility/ColorPreference;

    invoke-virtual {v0, v3}, Lcom/android/settings/accessibility/ColorPreference;->og([Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/android/settings/accessibility/CaptionPropertiesFragment;->aev:Lcom/android/settings/accessibility/ColorPreference;

    invoke-virtual {v0, v2}, Lcom/android/settings/accessibility/ColorPreference;->Uy([I)V

    const v0, 0x7f030045

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getIntArray(I)[I

    move-result-object v4

    const v0, 0x7f030044

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v1

    iget-object v0, p0, Lcom/android/settings/accessibility/CaptionPropertiesFragment;->aer:Landroid/preference/PreferenceCategory;

    const-string/jumbo v5, "captioning_foreground_opacity"

    invoke-virtual {v0, v5}, Landroid/preference/PreferenceCategory;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Lcom/android/settings/accessibility/ColorPreference;

    iput-object v0, p0, Lcom/android/settings/accessibility/CaptionPropertiesFragment;->aew:Lcom/android/settings/accessibility/ColorPreference;

    iget-object v0, p0, Lcom/android/settings/accessibility/CaptionPropertiesFragment;->aew:Lcom/android/settings/accessibility/ColorPreference;

    invoke-virtual {v0, v1}, Lcom/android/settings/accessibility/ColorPreference;->og([Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/android/settings/accessibility/CaptionPropertiesFragment;->aew:Lcom/android/settings/accessibility/ColorPreference;

    invoke-virtual {v0, v4}, Lcom/android/settings/accessibility/ColorPreference;->Uy([I)V

    iget-object v0, p0, Lcom/android/settings/accessibility/CaptionPropertiesFragment;->aer:Landroid/preference/PreferenceCategory;

    const-string/jumbo v5, "captioning_edge_color"

    invoke-virtual {v0, v5}, Landroid/preference/PreferenceCategory;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Lcom/android/settings/accessibility/ColorPreference;

    iput-object v0, p0, Lcom/android/settings/accessibility/CaptionPropertiesFragment;->aes:Lcom/android/settings/accessibility/ColorPreference;

    iget-object v0, p0, Lcom/android/settings/accessibility/CaptionPropertiesFragment;->aes:Lcom/android/settings/accessibility/ColorPreference;

    invoke-virtual {v0, v3}, Lcom/android/settings/accessibility/ColorPreference;->og([Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/android/settings/accessibility/CaptionPropertiesFragment;->aes:Lcom/android/settings/accessibility/ColorPreference;

    invoke-virtual {v0, v2}, Lcom/android/settings/accessibility/ColorPreference;->Uy([I)V

    array-length v0, v2

    add-int/lit8 v0, v0, 0x1

    new-array v5, v0, [I

    array-length v0, v3

    add-int/lit8 v0, v0, 0x1

    new-array v6, v0, [Ljava/lang/String;

    array-length v0, v2

    invoke-static {v2, v7, v5, v8, v0}, Ljava/lang/System;->arraycopy([II[III)V

    array-length v0, v3

    invoke-static {v3, v7, v6, v8, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    aput v7, v5, v7

    const v0, 0x7f12042d

    invoke-virtual {p0, v0}, Lcom/android/settings/accessibility/CaptionPropertiesFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v6, v7

    iget-object v0, p0, Lcom/android/settings/accessibility/CaptionPropertiesFragment;->aer:Landroid/preference/PreferenceCategory;

    const-string/jumbo v2, "captioning_background_color"

    invoke-virtual {v0, v2}, Landroid/preference/PreferenceCategory;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Lcom/android/settings/accessibility/ColorPreference;

    iput-object v0, p0, Lcom/android/settings/accessibility/CaptionPropertiesFragment;->aeo:Lcom/android/settings/accessibility/ColorPreference;

    iget-object v0, p0, Lcom/android/settings/accessibility/CaptionPropertiesFragment;->aeo:Lcom/android/settings/accessibility/ColorPreference;

    invoke-virtual {v0, v6}, Lcom/android/settings/accessibility/ColorPreference;->og([Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/android/settings/accessibility/CaptionPropertiesFragment;->aeo:Lcom/android/settings/accessibility/ColorPreference;

    invoke-virtual {v0, v5}, Lcom/android/settings/accessibility/ColorPreference;->Uy([I)V

    iget-object v0, p0, Lcom/android/settings/accessibility/CaptionPropertiesFragment;->aer:Landroid/preference/PreferenceCategory;

    const-string/jumbo v2, "captioning_background_opacity"

    invoke-virtual {v0, v2}, Landroid/preference/PreferenceCategory;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Lcom/android/settings/accessibility/ColorPreference;

    iput-object v0, p0, Lcom/android/settings/accessibility/CaptionPropertiesFragment;->aep:Lcom/android/settings/accessibility/ColorPreference;

    iget-object v0, p0, Lcom/android/settings/accessibility/CaptionPropertiesFragment;->aep:Lcom/android/settings/accessibility/ColorPreference;

    invoke-virtual {v0, v1}, Lcom/android/settings/accessibility/ColorPreference;->og([Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/android/settings/accessibility/CaptionPropertiesFragment;->aep:Lcom/android/settings/accessibility/ColorPreference;

    invoke-virtual {v0, v4}, Lcom/android/settings/accessibility/ColorPreference;->Uy([I)V

    iget-object v0, p0, Lcom/android/settings/accessibility/CaptionPropertiesFragment;->aer:Landroid/preference/PreferenceCategory;

    const-string/jumbo v2, "captioning_window_color"

    invoke-virtual {v0, v2}, Landroid/preference/PreferenceCategory;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Lcom/android/settings/accessibility/ColorPreference;

    iput-object v0, p0, Lcom/android/settings/accessibility/CaptionPropertiesFragment;->aeE:Lcom/android/settings/accessibility/ColorPreference;

    iget-object v0, p0, Lcom/android/settings/accessibility/CaptionPropertiesFragment;->aeE:Lcom/android/settings/accessibility/ColorPreference;

    invoke-virtual {v0, v6}, Lcom/android/settings/accessibility/ColorPreference;->og([Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/android/settings/accessibility/CaptionPropertiesFragment;->aeE:Lcom/android/settings/accessibility/ColorPreference;

    invoke-virtual {v0, v5}, Lcom/android/settings/accessibility/ColorPreference;->Uy([I)V

    iget-object v0, p0, Lcom/android/settings/accessibility/CaptionPropertiesFragment;->aer:Landroid/preference/PreferenceCategory;

    const-string/jumbo v2, "captioning_window_opacity"

    invoke-virtual {v0, v2}, Landroid/preference/PreferenceCategory;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Lcom/android/settings/accessibility/ColorPreference;

    iput-object v0, p0, Lcom/android/settings/accessibility/CaptionPropertiesFragment;->aeF:Lcom/android/settings/accessibility/ColorPreference;

    iget-object v0, p0, Lcom/android/settings/accessibility/CaptionPropertiesFragment;->aeF:Lcom/android/settings/accessibility/ColorPreference;

    invoke-virtual {v0, v1}, Lcom/android/settings/accessibility/ColorPreference;->og([Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/android/settings/accessibility/CaptionPropertiesFragment;->aeF:Lcom/android/settings/accessibility/ColorPreference;

    invoke-virtual {v0, v4}, Lcom/android/settings/accessibility/ColorPreference;->Uy([I)V

    iget-object v0, p0, Lcom/android/settings/accessibility/CaptionPropertiesFragment;->aer:Landroid/preference/PreferenceCategory;

    const-string/jumbo v1, "captioning_edge_type"

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceCategory;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Lcom/android/settings/accessibility/EdgeTypePreference;

    iput-object v0, p0, Lcom/android/settings/accessibility/CaptionPropertiesFragment;->aet:Lcom/android/settings/accessibility/EdgeTypePreference;

    iget-object v0, p0, Lcom/android/settings/accessibility/CaptionPropertiesFragment;->aer:Landroid/preference/PreferenceCategory;

    const-string/jumbo v1, "captioning_typeface"

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceCategory;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/ListPreference;

    iput-object v0, p0, Lcom/android/settings/accessibility/CaptionPropertiesFragment;->aeD:Landroid/preference/ListPreference;

    return-void
.end method

.method private Vi()V
    .locals 0

    invoke-virtual {p0}, Lcom/android/settings/accessibility/CaptionPropertiesFragment;->Vl()V

    return-void
.end method

.method private Vj()V
    .locals 1

    iget-object v0, p0, Lcom/android/settings/accessibility/CaptionPropertiesFragment;->aey:Lcom/android/settings/accessibility/PresetPreference;

    invoke-virtual {v0, p0}, Lcom/android/settings/accessibility/PresetPreference;->UB(Lcom/android/settings/accessibility/MiuiListDialogPreference$OnValueChangedListener;)V

    iget-object v0, p0, Lcom/android/settings/accessibility/CaptionPropertiesFragment;->aev:Lcom/android/settings/accessibility/ColorPreference;

    invoke-virtual {v0, p0}, Lcom/android/settings/accessibility/ColorPreference;->UB(Lcom/android/settings/accessibility/MiuiListDialogPreference$OnValueChangedListener;)V

    iget-object v0, p0, Lcom/android/settings/accessibility/CaptionPropertiesFragment;->aew:Lcom/android/settings/accessibility/ColorPreference;

    invoke-virtual {v0, p0}, Lcom/android/settings/accessibility/ColorPreference;->UB(Lcom/android/settings/accessibility/MiuiListDialogPreference$OnValueChangedListener;)V

    iget-object v0, p0, Lcom/android/settings/accessibility/CaptionPropertiesFragment;->aes:Lcom/android/settings/accessibility/ColorPreference;

    invoke-virtual {v0, p0}, Lcom/android/settings/accessibility/ColorPreference;->UB(Lcom/android/settings/accessibility/MiuiListDialogPreference$OnValueChangedListener;)V

    iget-object v0, p0, Lcom/android/settings/accessibility/CaptionPropertiesFragment;->aeo:Lcom/android/settings/accessibility/ColorPreference;

    invoke-virtual {v0, p0}, Lcom/android/settings/accessibility/ColorPreference;->UB(Lcom/android/settings/accessibility/MiuiListDialogPreference$OnValueChangedListener;)V

    iget-object v0, p0, Lcom/android/settings/accessibility/CaptionPropertiesFragment;->aep:Lcom/android/settings/accessibility/ColorPreference;

    invoke-virtual {v0, p0}, Lcom/android/settings/accessibility/ColorPreference;->UB(Lcom/android/settings/accessibility/MiuiListDialogPreference$OnValueChangedListener;)V

    iget-object v0, p0, Lcom/android/settings/accessibility/CaptionPropertiesFragment;->aeE:Lcom/android/settings/accessibility/ColorPreference;

    invoke-virtual {v0, p0}, Lcom/android/settings/accessibility/ColorPreference;->UB(Lcom/android/settings/accessibility/MiuiListDialogPreference$OnValueChangedListener;)V

    iget-object v0, p0, Lcom/android/settings/accessibility/CaptionPropertiesFragment;->aeF:Lcom/android/settings/accessibility/ColorPreference;

    invoke-virtual {v0, p0}, Lcom/android/settings/accessibility/ColorPreference;->UB(Lcom/android/settings/accessibility/MiuiListDialogPreference$OnValueChangedListener;)V

    iget-object v0, p0, Lcom/android/settings/accessibility/CaptionPropertiesFragment;->aet:Lcom/android/settings/accessibility/EdgeTypePreference;

    invoke-virtual {v0, p0}, Lcom/android/settings/accessibility/EdgeTypePreference;->UB(Lcom/android/settings/accessibility/MiuiListDialogPreference$OnValueChangedListener;)V

    iget-object v0, p0, Lcom/android/settings/accessibility/CaptionPropertiesFragment;->aeD:Landroid/preference/ListPreference;

    invoke-virtual {v0, p0}, Landroid/preference/ListPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    iget-object v0, p0, Lcom/android/settings/accessibility/CaptionPropertiesFragment;->aeu:Landroid/preference/ListPreference;

    invoke-virtual {v0, p0}, Landroid/preference/ListPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    iget-object v0, p0, Lcom/android/settings/accessibility/CaptionPropertiesFragment;->aex:Lcom/android/settings/accessibility/LocalePreference;

    invoke-virtual {v0, p0}, Lcom/android/settings/accessibility/LocalePreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    return-void
.end method

.method private Vk(Lcom/android/settings/accessibility/ColorPreference;Lcom/android/settings/accessibility/ColorPreference;)I
    .locals 3

    invoke-virtual {p1}, Lcom/android/settings/accessibility/ColorPreference;->getValue()I

    move-result v0

    invoke-virtual {p2}, Lcom/android/settings/accessibility/ColorPreference;->getValue()I

    move-result v1

    invoke-static {v0}, Landroid/view/accessibility/CaptioningManager$CaptionStyle;->hasColor(I)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-static {v1}, Landroid/graphics/Color;->alpha(I)I

    move-result v0

    const v1, 0xffff00

    or-int/2addr v0, v1

    :goto_0
    return v0

    :cond_0
    if-nez v0, :cond_1

    invoke-static {v1}, Landroid/graphics/Color;->alpha(I)I

    move-result v0

    goto :goto_0

    :cond_1
    const v2, 0xffffff

    and-int/2addr v0, v2

    const/high16 v2, -0x1000000

    and-int/2addr v1, v2

    or-int/2addr v0, v1

    goto :goto_0
.end method

.method private Vm(Lcom/android/settings/accessibility/ColorPreference;Lcom/android/settings/accessibility/ColorPreference;I)V
    .locals 4

    const v2, 0xffffff

    const/4 v1, 0x0

    const/high16 v3, -0x1000000

    invoke-static {p3}, Landroid/view/accessibility/CaptioningManager$CaptionStyle;->hasColor(I)Z

    move-result v0

    if-nez v0, :cond_0

    and-int/lit16 v0, p3, 0xff

    shl-int/lit8 v0, v0, 0x18

    move v1, v2

    :goto_0
    or-int/2addr v0, v2

    invoke-virtual {p2, v0}, Lcom/android/settings/accessibility/ColorPreference;->UC(I)V

    invoke-virtual {p1, v1}, Lcom/android/settings/accessibility/ColorPreference;->UC(I)V

    return-void

    :cond_0
    ushr-int/lit8 v0, p3, 0x18

    if-nez v0, :cond_1

    and-int/lit16 v0, p3, 0xff

    shl-int/lit8 v0, v0, 0x18

    goto :goto_0

    :cond_1
    or-int v1, p3, v3

    and-int v0, p3, v3

    goto :goto_0
.end method

.method private Vn()V
    .locals 6

    const v5, 0x7f1203db

    invoke-virtual {p0}, Lcom/android/settings/accessibility/CaptionPropertiesFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    iget-object v1, p0, Lcom/android/settings/accessibility/CaptionPropertiesFragment;->aez:Lcom/android/internal/widget/SubtitleView;

    if-eqz v1, :cond_1

    iget-object v2, p0, Lcom/android/settings/accessibility/CaptionPropertiesFragment;->aeq:Landroid/view/accessibility/CaptioningManager;

    invoke-virtual {v2}, Landroid/view/accessibility/CaptioningManager;->getRawUserStyle()I

    move-result v2

    iget-object v3, p0, Lcom/android/settings/accessibility/CaptionPropertiesFragment;->aeq:Landroid/view/accessibility/CaptioningManager;

    iget-object v4, p0, Lcom/android/settings/accessibility/CaptionPropertiesFragment;->aeA:Landroid/view/View;

    invoke-static {v3, v1, v4, v2}, Lcom/android/settings/accessibility/CaptionPropertiesFragment;->Vf(Landroid/view/accessibility/CaptioningManager;Lcom/android/internal/widget/SubtitleView;Landroid/view/View;I)V

    iget-object v2, p0, Lcom/android/settings/accessibility/CaptionPropertiesFragment;->aeq:Landroid/view/accessibility/CaptioningManager;

    invoke-virtual {v2}, Landroid/view/accessibility/CaptioningManager;->getLocale()Ljava/util/Locale;

    move-result-object v2

    if-eqz v2, :cond_2

    invoke-static {v0, v2, v5}, Lcom/android/settingslib/i/a;->cpj(Landroid/content/Context;Ljava/util/Locale;I)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/android/internal/widget/SubtitleView;->setText(Ljava/lang/CharSequence;)V

    :goto_0
    iget-object v0, p0, Lcom/android/settings/accessibility/CaptionPropertiesFragment;->aeq:Landroid/view/accessibility/CaptioningManager;

    invoke-virtual {v0}, Landroid/view/accessibility/CaptioningManager;->getUserStyle()Landroid/view/accessibility/CaptioningManager$CaptionStyle;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/accessibility/CaptioningManager$CaptionStyle;->hasWindowColor()Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/android/settings/accessibility/CaptionPropertiesFragment;->aeB:Landroid/view/View;

    iget v0, v0, Landroid/view/accessibility/CaptioningManager$CaptionStyle;->windowColor:I

    invoke-virtual {v1, v0}, Landroid/view/View;->setBackgroundColor(I)V

    :cond_1
    :goto_1
    return-void

    :cond_2
    invoke-virtual {v1, v5}, Lcom/android/internal/widget/SubtitleView;->setText(I)V

    goto :goto_0

    :cond_3
    sget-object v0, Landroid/view/accessibility/CaptioningManager$CaptionStyle;->DEFAULT:Landroid/view/accessibility/CaptioningManager$CaptionStyle;

    iget-object v1, p0, Lcom/android/settings/accessibility/CaptionPropertiesFragment;->aeB:Landroid/view/View;

    iget v0, v0, Landroid/view/accessibility/CaptioningManager$CaptionStyle;->windowColor:I

    invoke-virtual {v1, v0}, Landroid/view/View;->setBackgroundColor(I)V

    goto :goto_1
.end method

.method private Vo()V
    .locals 4

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/android/settings/accessibility/CaptionPropertiesFragment;->aey:Lcom/android/settings/accessibility/PresetPreference;

    invoke-virtual {v0}, Lcom/android/settings/accessibility/PresetPreference;->getValue()I

    move-result v0

    const/4 v3, -0x1

    if-ne v0, v3, :cond_1

    move v0, v1

    :goto_0
    if-nez v0, :cond_2

    iget-boolean v3, p0, Lcom/android/settings/accessibility/CaptionPropertiesFragment;->aeC:Z

    if-eqz v3, :cond_2

    invoke-virtual {p0}, Lcom/android/settings/accessibility/CaptionPropertiesFragment;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/accessibility/CaptionPropertiesFragment;->aer:Landroid/preference/PreferenceCategory;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    iput-boolean v2, p0, Lcom/android/settings/accessibility/CaptionPropertiesFragment;->aeC:Z

    :cond_0
    :goto_1
    return-void

    :cond_1
    move v0, v2

    goto :goto_0

    :cond_2
    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/android/settings/accessibility/CaptionPropertiesFragment;->aeC:Z

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/android/settings/accessibility/CaptionPropertiesFragment;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    iget-object v2, p0, Lcom/android/settings/accessibility/CaptionPropertiesFragment;->aer:Landroid/preference/PreferenceCategory;

    invoke-virtual {v0, v2}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    iput-boolean v1, p0, Lcom/android/settings/accessibility/CaptionPropertiesFragment;->aeC:Z

    goto :goto_1
.end method

.method private Vp()V
    .locals 0

    return-void
.end method

.method private Vq()V
    .locals 5

    const v1, 0xffffff

    iget-object v0, p0, Lcom/android/settings/accessibility/CaptionPropertiesFragment;->aeq:Landroid/view/accessibility/CaptioningManager;

    invoke-virtual {v0}, Landroid/view/accessibility/CaptioningManager;->getRawUserStyle()I

    move-result v0

    iget-object v2, p0, Lcom/android/settings/accessibility/CaptionPropertiesFragment;->aey:Lcom/android/settings/accessibility/PresetPreference;

    invoke-virtual {v2, v0}, Lcom/android/settings/accessibility/PresetPreference;->UC(I)V

    iget-object v0, p0, Lcom/android/settings/accessibility/CaptionPropertiesFragment;->aeq:Landroid/view/accessibility/CaptioningManager;

    invoke-virtual {v0}, Landroid/view/accessibility/CaptioningManager;->getFontScale()F

    move-result v0

    iget-object v2, p0, Lcom/android/settings/accessibility/CaptionPropertiesFragment;->aeu:Landroid/preference/ListPreference;

    invoke-static {v0}, Ljava/lang/Float;->toString(F)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/preference/ListPreference;->setValue(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/android/settings/accessibility/CaptionPropertiesFragment;->bWB()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-static {v0}, Landroid/view/accessibility/CaptioningManager$CaptionStyle;->getCustomStyle(Landroid/content/ContentResolver;)Landroid/view/accessibility/CaptioningManager$CaptionStyle;

    move-result-object v2

    iget-object v0, p0, Lcom/android/settings/accessibility/CaptionPropertiesFragment;->aet:Lcom/android/settings/accessibility/EdgeTypePreference;

    iget v3, v2, Landroid/view/accessibility/CaptioningManager$CaptionStyle;->edgeType:I

    invoke-virtual {v0, v3}, Lcom/android/settings/accessibility/EdgeTypePreference;->UC(I)V

    iget-object v0, p0, Lcom/android/settings/accessibility/CaptionPropertiesFragment;->aes:Lcom/android/settings/accessibility/ColorPreference;

    iget v3, v2, Landroid/view/accessibility/CaptioningManager$CaptionStyle;->edgeColor:I

    invoke-virtual {v0, v3}, Lcom/android/settings/accessibility/ColorPreference;->UC(I)V

    invoke-virtual {v2}, Landroid/view/accessibility/CaptioningManager$CaptionStyle;->hasForegroundColor()Z

    move-result v0

    if-eqz v0, :cond_3

    iget v0, v2, Landroid/view/accessibility/CaptioningManager$CaptionStyle;->foregroundColor:I

    :goto_0
    iget-object v3, p0, Lcom/android/settings/accessibility/CaptionPropertiesFragment;->aev:Lcom/android/settings/accessibility/ColorPreference;

    iget-object v4, p0, Lcom/android/settings/accessibility/CaptionPropertiesFragment;->aew:Lcom/android/settings/accessibility/ColorPreference;

    invoke-direct {p0, v3, v4, v0}, Lcom/android/settings/accessibility/CaptionPropertiesFragment;->Vm(Lcom/android/settings/accessibility/ColorPreference;Lcom/android/settings/accessibility/ColorPreference;I)V

    invoke-virtual {v2}, Landroid/view/accessibility/CaptioningManager$CaptionStyle;->hasBackgroundColor()Z

    move-result v0

    if-eqz v0, :cond_4

    iget v0, v2, Landroid/view/accessibility/CaptioningManager$CaptionStyle;->backgroundColor:I

    :goto_1
    iget-object v3, p0, Lcom/android/settings/accessibility/CaptionPropertiesFragment;->aeo:Lcom/android/settings/accessibility/ColorPreference;

    iget-object v4, p0, Lcom/android/settings/accessibility/CaptionPropertiesFragment;->aep:Lcom/android/settings/accessibility/ColorPreference;

    invoke-direct {p0, v3, v4, v0}, Lcom/android/settings/accessibility/CaptionPropertiesFragment;->Vm(Lcom/android/settings/accessibility/ColorPreference;Lcom/android/settings/accessibility/ColorPreference;I)V

    invoke-virtual {v2}, Landroid/view/accessibility/CaptioningManager$CaptionStyle;->hasWindowColor()Z

    move-result v0

    if-eqz v0, :cond_0

    iget v1, v2, Landroid/view/accessibility/CaptioningManager$CaptionStyle;->windowColor:I

    :cond_0
    iget-object v0, p0, Lcom/android/settings/accessibility/CaptionPropertiesFragment;->aeE:Lcom/android/settings/accessibility/ColorPreference;

    iget-object v3, p0, Lcom/android/settings/accessibility/CaptionPropertiesFragment;->aeF:Lcom/android/settings/accessibility/ColorPreference;

    invoke-direct {p0, v0, v3, v1}, Lcom/android/settings/accessibility/CaptionPropertiesFragment;->Vm(Lcom/android/settings/accessibility/ColorPreference;Lcom/android/settings/accessibility/ColorPreference;I)V

    iget-object v0, v2, Landroid/view/accessibility/CaptioningManager$CaptionStyle;->mRawTypeface:Ljava/lang/String;

    iget-object v1, p0, Lcom/android/settings/accessibility/CaptionPropertiesFragment;->aeD:Landroid/preference/ListPreference;

    if-nez v0, :cond_1

    const-string/jumbo v0, ""

    :cond_1
    invoke-virtual {v1, v0}, Landroid/preference/ListPreference;->setValue(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/settings/accessibility/CaptionPropertiesFragment;->aeq:Landroid/view/accessibility/CaptioningManager;

    invoke-virtual {v0}, Landroid/view/accessibility/CaptioningManager;->getRawLocale()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/accessibility/CaptionPropertiesFragment;->aex:Lcom/android/settings/accessibility/LocalePreference;

    if-nez v0, :cond_2

    const-string/jumbo v0, ""

    :cond_2
    invoke-virtual {v1, v0}, Lcom/android/settings/accessibility/LocalePreference;->setValue(Ljava/lang/String;)V

    return-void

    :cond_3
    move v0, v1

    goto :goto_0

    :cond_4
    move v0, v1

    goto :goto_1
.end method

.method private Vr(Z)V
    .locals 7

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/android/settings/accessibility/CaptionPropertiesFragment;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v2

    invoke-virtual {v2}, Landroid/preference/PreferenceScreen;->getPreferenceCount()I

    move-result v3

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    invoke-virtual {v2, v1}, Landroid/preference/PreferenceScreen;->getPreference(I)Landroid/preference/Preference;

    move-result-object v4

    invoke-virtual {v4}, Landroid/preference/Preference;->getOrder()I

    move-result v5

    const/4 v6, -0x1

    if-eq v5, v6, :cond_0

    invoke-virtual {v4, p1}, Landroid/preference/Preference;->setEnabled(Z)V

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    iget-object v1, p0, Lcom/android/settings/accessibility/CaptionPropertiesFragment;->aez:Lcom/android/internal/widget/SubtitleView;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/android/settings/accessibility/CaptionPropertiesFragment;->aez:Lcom/android/internal/widget/SubtitleView;

    if-eqz p1, :cond_3

    :goto_1
    invoke-virtual {v1, v0}, Lcom/android/internal/widget/SubtitleView;->setVisibility(I)V

    :cond_2
    return-void

    :cond_3
    const/4 v0, 0x4

    goto :goto_1
.end method

.method static synthetic Vs(Lcom/android/settings/accessibility/CaptionPropertiesFragment;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/accessibility/CaptionPropertiesFragment;->Vn()V

    return-void
.end method

.method static synthetic Vt(Lcom/android/settings/accessibility/CaptionPropertiesFragment;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/settings/accessibility/CaptionPropertiesFragment;->Vr(Z)V

    return-void
.end method


# virtual methods
.method public UG(Lcom/android/settings/accessibility/MiuiListDialogPreference;I)V
    .locals 3

    invoke-virtual {p0}, Lcom/android/settings/accessibility/CaptionPropertiesFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/accessibility/CaptionPropertiesFragment;->aev:Lcom/android/settings/accessibility/ColorPreference;

    if-eq v1, p1, :cond_0

    iget-object v1, p0, Lcom/android/settings/accessibility/CaptionPropertiesFragment;->aew:Lcom/android/settings/accessibility/ColorPreference;

    if-ne v1, p1, :cond_2

    :cond_0
    iget-object v1, p0, Lcom/android/settings/accessibility/CaptionPropertiesFragment;->aev:Lcom/android/settings/accessibility/ColorPreference;

    iget-object v2, p0, Lcom/android/settings/accessibility/CaptionPropertiesFragment;->aew:Lcom/android/settings/accessibility/ColorPreference;

    invoke-direct {p0, v1, v2}, Lcom/android/settings/accessibility/CaptionPropertiesFragment;->Vk(Lcom/android/settings/accessibility/ColorPreference;Lcom/android/settings/accessibility/ColorPreference;)I

    move-result v1

    const-string/jumbo v2, "accessibility_captioning_foreground_color"

    invoke-static {v0, v2, v1}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    :cond_1
    :goto_0
    invoke-direct {p0}, Lcom/android/settings/accessibility/CaptionPropertiesFragment;->Vn()V

    return-void

    :cond_2
    iget-object v1, p0, Lcom/android/settings/accessibility/CaptionPropertiesFragment;->aeo:Lcom/android/settings/accessibility/ColorPreference;

    if-eq v1, p1, :cond_3

    iget-object v1, p0, Lcom/android/settings/accessibility/CaptionPropertiesFragment;->aep:Lcom/android/settings/accessibility/ColorPreference;

    if-ne v1, p1, :cond_4

    :cond_3
    iget-object v1, p0, Lcom/android/settings/accessibility/CaptionPropertiesFragment;->aeo:Lcom/android/settings/accessibility/ColorPreference;

    iget-object v2, p0, Lcom/android/settings/accessibility/CaptionPropertiesFragment;->aep:Lcom/android/settings/accessibility/ColorPreference;

    invoke-direct {p0, v1, v2}, Lcom/android/settings/accessibility/CaptionPropertiesFragment;->Vk(Lcom/android/settings/accessibility/ColorPreference;Lcom/android/settings/accessibility/ColorPreference;)I

    move-result v1

    const-string/jumbo v2, "accessibility_captioning_background_color"

    invoke-static {v0, v2, v1}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    goto :goto_0

    :cond_4
    iget-object v1, p0, Lcom/android/settings/accessibility/CaptionPropertiesFragment;->aeE:Lcom/android/settings/accessibility/ColorPreference;

    if-eq v1, p1, :cond_5

    iget-object v1, p0, Lcom/android/settings/accessibility/CaptionPropertiesFragment;->aeF:Lcom/android/settings/accessibility/ColorPreference;

    if-ne v1, p1, :cond_6

    :cond_5
    iget-object v1, p0, Lcom/android/settings/accessibility/CaptionPropertiesFragment;->aeE:Lcom/android/settings/accessibility/ColorPreference;

    iget-object v2, p0, Lcom/android/settings/accessibility/CaptionPropertiesFragment;->aeF:Lcom/android/settings/accessibility/ColorPreference;

    invoke-direct {p0, v1, v2}, Lcom/android/settings/accessibility/CaptionPropertiesFragment;->Vk(Lcom/android/settings/accessibility/ColorPreference;Lcom/android/settings/accessibility/ColorPreference;)I

    move-result v1

    const-string/jumbo v2, "accessibility_captioning_window_color"

    invoke-static {v0, v2, v1}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    goto :goto_0

    :cond_6
    iget-object v1, p0, Lcom/android/settings/accessibility/CaptionPropertiesFragment;->aes:Lcom/android/settings/accessibility/ColorPreference;

    if-ne v1, p1, :cond_7

    const-string/jumbo v1, "accessibility_captioning_edge_color"

    invoke-static {v0, v1, p2}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    goto :goto_0

    :cond_7
    iget-object v1, p0, Lcom/android/settings/accessibility/CaptionPropertiesFragment;->aey:Lcom/android/settings/accessibility/PresetPreference;

    if-ne v1, p1, :cond_8

    const-string/jumbo v1, "accessibility_captioning_preset"

    invoke-static {v0, v1, p2}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    invoke-direct {p0}, Lcom/android/settings/accessibility/CaptionPropertiesFragment;->Vo()V

    goto :goto_0

    :cond_8
    iget-object v1, p0, Lcom/android/settings/accessibility/CaptionPropertiesFragment;->aet:Lcom/android/settings/accessibility/EdgeTypePreference;

    if-ne v1, p1, :cond_1

    const-string/jumbo v1, "accessibility_captioning_edge_type"

    invoke-static {v0, v1, p2}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    goto :goto_0
.end method

.method public Vg(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 4

    const/4 v3, -0x1

    const v0, 0x7f0d0056

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    instance-of v0, p2, Landroid/preference/PreferenceFrameLayout;

    if-eqz v0, :cond_0

    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceFrameLayout$LayoutParams;

    const/4 v2, 0x1

    iput-boolean v2, v0, Landroid/preference/PreferenceFrameLayout$LayoutParams;->removeBorders:Z

    :cond_0
    invoke-super {p0, p1, p2, p3}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->Vg(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v2

    const v0, 0x7f0a035a

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    invoke-virtual {v0, v2, v3, v3}, Landroid/view/ViewGroup;->addView(Landroid/view/View;II)V

    return-object v1
.end method

.method protected Vl()V
    .locals 5

    const/4 v0, 0x1

    const/4 v1, 0x0

    new-instance v2, Landroid/preference/CheckBoxPreference;

    invoke-virtual {p0}, Lcom/android/settings/accessibility/CaptionPropertiesFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/preference/CheckBoxPreference;-><init>(Landroid/content/Context;)V

    const v3, 0x7f120049

    invoke-virtual {v2, v3}, Landroid/preference/CheckBoxPreference;->setTitle(I)V

    const/4 v3, -0x1

    invoke-virtual {v2, v3}, Landroid/preference/CheckBoxPreference;->setOrder(I)V

    invoke-virtual {p0}, Lcom/android/settings/accessibility/CaptionPropertiesFragment;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v3

    invoke-virtual {v3, v2}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    invoke-virtual {p0}, Lcom/android/settings/accessibility/CaptionPropertiesFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string/jumbo v4, "accessibility_captioning_enabled"

    invoke-static {v3, v4, v1}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v3

    if-ne v3, v0, :cond_0

    :goto_0
    invoke-virtual {v2, v0}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    new-instance v0, Lcom/android/settings/accessibility/CaptionPropertiesFragment$2;

    invoke-direct {v0, p0, v2}, Lcom/android/settings/accessibility/CaptionPropertiesFragment$2;-><init>(Lcom/android/settings/accessibility/CaptionPropertiesFragment;Landroid/preference/CheckBoxPreference;)V

    invoke-virtual {v2, v0}, Landroid/preference/CheckBoxPreference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    invoke-virtual {v2}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/android/settings/accessibility/CaptionPropertiesFragment;->Vr(Z)V

    return-void

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method public getMetricsCategory()I
    .locals 1

    const/4 v0, 0x3

    return v0
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 1

    invoke-super {p0, p1}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->onActivityCreated(Landroid/os/Bundle;)V

    iget-object v0, p0, Lcom/android/settings/accessibility/CaptionPropertiesFragment;->aeq:Landroid/view/accessibility/CaptioningManager;

    invoke-virtual {v0}, Landroid/view/accessibility/CaptioningManager;->isEnabled()Z

    invoke-direct {p0}, Lcom/android/settings/accessibility/CaptionPropertiesFragment;->Vn()V

    invoke-direct {p0}, Lcom/android/settings/accessibility/CaptionPropertiesFragment;->Vi()V

    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1

    invoke-super {p0, p1}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->onCreate(Landroid/os/Bundle;)V

    const-string/jumbo v0, "captioning"

    invoke-virtual {p0, v0}, Lcom/android/settings/accessibility/CaptionPropertiesFragment;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/accessibility/CaptioningManager;

    iput-object v0, p0, Lcom/android/settings/accessibility/CaptionPropertiesFragment;->aeq:Landroid/view/accessibility/CaptioningManager;

    const v0, 0x7f15002e

    invoke-virtual {p0, v0}, Lcom/android/settings/accessibility/CaptionPropertiesFragment;->addPreferencesFromResource(I)V

    invoke-direct {p0}, Lcom/android/settings/accessibility/CaptionPropertiesFragment;->Vh()V

    invoke-direct {p0}, Lcom/android/settings/accessibility/CaptionPropertiesFragment;->Vq()V

    invoke-direct {p0}, Lcom/android/settings/accessibility/CaptionPropertiesFragment;->Vo()V

    invoke-direct {p0}, Lcom/android/settings/accessibility/CaptionPropertiesFragment;->Vj()V

    return-void
.end method

.method public onDestroyView()V
    .locals 0

    invoke-super {p0}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->onDestroyView()V

    invoke-direct {p0}, Lcom/android/settings/accessibility/CaptionPropertiesFragment;->Vp()V

    return-void
.end method

.method public onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 3

    invoke-virtual {p0}, Lcom/android/settings/accessibility/CaptionPropertiesFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/accessibility/CaptionPropertiesFragment;->aeD:Landroid/preference/ListPreference;

    if-ne v1, p1, :cond_1

    const-string/jumbo v1, "accessibility_captioning_typeface"

    check-cast p2, Ljava/lang/String;

    invoke-static {v0, v1, p2}, Landroid/provider/Settings$Secure;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    :cond_0
    :goto_0
    invoke-direct {p0}, Lcom/android/settings/accessibility/CaptionPropertiesFragment;->Vn()V

    const/4 v0, 0x1

    return v0

    :cond_1
    iget-object v1, p0, Lcom/android/settings/accessibility/CaptionPropertiesFragment;->aeu:Landroid/preference/ListPreference;

    if-ne v1, p1, :cond_2

    const-string/jumbo v1, "accessibility_captioning_font_scale"

    check-cast p2, Ljava/lang/String;

    invoke-static {p2}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v2

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$Secure;->putFloat(Landroid/content/ContentResolver;Ljava/lang/String;F)Z

    goto :goto_0

    :cond_2
    iget-object v1, p0, Lcom/android/settings/accessibility/CaptionPropertiesFragment;->aex:Lcom/android/settings/accessibility/LocalePreference;

    if-ne v1, p1, :cond_0

    const-string/jumbo v1, "accessibility_captioning_locale"

    check-cast p2, Ljava/lang/String;

    invoke-static {v0, v1, p2}, Landroid/provider/Settings$Secure;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    goto :goto_0
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 3

    invoke-super {p0, p1, p2}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    iget-object v0, p0, Lcom/android/settings/accessibility/CaptionPropertiesFragment;->aeq:Landroid/view/accessibility/CaptioningManager;

    invoke-virtual {v0}, Landroid/view/accessibility/CaptioningManager;->isEnabled()Z

    move-result v1

    const v0, 0x7f0a0335

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/internal/widget/SubtitleView;

    iput-object v0, p0, Lcom/android/settings/accessibility/CaptionPropertiesFragment;->aez:Lcom/android/internal/widget/SubtitleView;

    iget-object v2, p0, Lcom/android/settings/accessibility/CaptionPropertiesFragment;->aez:Lcom/android/internal/widget/SubtitleView;

    if-eqz v1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v2, v0}, Lcom/android/internal/widget/SubtitleView;->setVisibility(I)V

    const v0, 0x7f0a0337

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/accessibility/CaptionPropertiesFragment;->aeB:Landroid/view/View;

    const v0, 0x7f0a0336

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/accessibility/CaptionPropertiesFragment;->aeA:Landroid/view/View;

    iget-object v0, p0, Lcom/android/settings/accessibility/CaptionPropertiesFragment;->aeA:Landroid/view/View;

    new-instance v1, Lcom/android/settings/accessibility/CaptionPropertiesFragment$1;

    invoke-direct {v1, p0}, Lcom/android/settings/accessibility/CaptionPropertiesFragment$1;-><init>(Lcom/android/settings/accessibility/CaptionPropertiesFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->addOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    return-void

    :cond_0
    const/4 v0, 0x4

    goto :goto_0
.end method
