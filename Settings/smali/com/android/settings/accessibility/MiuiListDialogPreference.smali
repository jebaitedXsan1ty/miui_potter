.class public abstract Lcom/android/settings/accessibility/MiuiListDialogPreference;
.super Landroid/preference/DialogPreference;
.source "MiuiListDialogPreference.java"


# instance fields
.field private adG:[Ljava/lang/CharSequence;

.field private adH:[I

.field private adI:I

.field private adJ:Lcom/android/settings/accessibility/MiuiListDialogPreference$OnValueChangedListener;

.field private adK:I

.field private adL:Z

.field private mValue:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Landroid/preference/DialogPreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method static synthetic UD(Lcom/android/settings/accessibility/MiuiListDialogPreference;)[I
    .locals 1

    iget-object v0, p0, Lcom/android/settings/accessibility/MiuiListDialogPreference;->adH:[I

    return-object v0
.end method

.method static synthetic UE(Lcom/android/settings/accessibility/MiuiListDialogPreference;)I
    .locals 1

    iget v0, p0, Lcom/android/settings/accessibility/MiuiListDialogPreference;->adI:I

    return v0
.end method

.method static synthetic UF(Lcom/android/settings/accessibility/MiuiListDialogPreference;Ljava/lang/Object;)Z
    .locals 1

    invoke-virtual {p0, p1}, Lcom/android/settings/accessibility/MiuiListDialogPreference;->callChangeListener(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method


# virtual methods
.method protected UA(I)I
    .locals 1

    iget-object v0, p0, Lcom/android/settings/accessibility/MiuiListDialogPreference;->adH:[I

    aget v0, v0, p1

    return v0
.end method

.method public UB(Lcom/android/settings/accessibility/MiuiListDialogPreference$OnValueChangedListener;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/accessibility/MiuiListDialogPreference;->adJ:Lcom/android/settings/accessibility/MiuiListDialogPreference$OnValueChangedListener;

    return-void
.end method

.method public UC(I)V
    .locals 3

    const/4 v1, 0x1

    iget v0, p0, Lcom/android/settings/accessibility/MiuiListDialogPreference;->mValue:I

    if-eq v0, p1, :cond_3

    move v0, v1

    :goto_0
    if-nez v0, :cond_0

    iget-boolean v2, p0, Lcom/android/settings/accessibility/MiuiListDialogPreference;->adL:Z

    xor-int/lit8 v2, v2, 0x1

    if-eqz v2, :cond_2

    :cond_0
    iput p1, p0, Lcom/android/settings/accessibility/MiuiListDialogPreference;->mValue:I

    invoke-virtual {p0, p1}, Lcom/android/settings/accessibility/MiuiListDialogPreference;->Uz(I)I

    move-result v2

    iput v2, p0, Lcom/android/settings/accessibility/MiuiListDialogPreference;->adK:I

    iput-boolean v1, p0, Lcom/android/settings/accessibility/MiuiListDialogPreference;->adL:Z

    invoke-virtual {p0, p1}, Lcom/android/settings/accessibility/MiuiListDialogPreference;->persistInt(I)Z

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/android/settings/accessibility/MiuiListDialogPreference;->shouldDisableDependents()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/android/settings/accessibility/MiuiListDialogPreference;->notifyDependencyChange(Z)V

    invoke-virtual {p0}, Lcom/android/settings/accessibility/MiuiListDialogPreference;->notifyChanged()V

    :cond_1
    iget-object v0, p0, Lcom/android/settings/accessibility/MiuiListDialogPreference;->adJ:Lcom/android/settings/accessibility/MiuiListDialogPreference$OnValueChangedListener;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/settings/accessibility/MiuiListDialogPreference;->adJ:Lcom/android/settings/accessibility/MiuiListDialogPreference$OnValueChangedListener;

    invoke-interface {v0, p0, p1}, Lcom/android/settings/accessibility/MiuiListDialogPreference$OnValueChangedListener;->UG(Lcom/android/settings/accessibility/MiuiListDialogPreference;I)V

    :cond_2
    return-void

    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public Uw(I)V
    .locals 0

    iput p1, p0, Lcom/android/settings/accessibility/MiuiListDialogPreference;->adI:I

    return-void
.end method

.method protected Ux(I)Ljava/lang/CharSequence;
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/android/settings/accessibility/MiuiListDialogPreference;->adG:[Ljava/lang/CharSequence;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/accessibility/MiuiListDialogPreference;->adG:[Ljava/lang/CharSequence;

    array-length v0, v0

    if-gt v0, p1, :cond_1

    :cond_0
    return-object v1

    :cond_1
    iget-object v0, p0, Lcom/android/settings/accessibility/MiuiListDialogPreference;->adG:[Ljava/lang/CharSequence;

    aget-object v0, v0, p1

    return-object v0
.end method

.method public Uy([I)V
    .locals 2

    iput-object p1, p0, Lcom/android/settings/accessibility/MiuiListDialogPreference;->adH:[I

    iget-boolean v0, p0, Lcom/android/settings/accessibility/MiuiListDialogPreference;->adL:Z

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/android/settings/accessibility/MiuiListDialogPreference;->adK:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    iget v0, p0, Lcom/android/settings/accessibility/MiuiListDialogPreference;->mValue:I

    invoke-virtual {p0, v0}, Lcom/android/settings/accessibility/MiuiListDialogPreference;->Uz(I)I

    move-result v0

    iput v0, p0, Lcom/android/settings/accessibility/MiuiListDialogPreference;->adK:I

    :cond_0
    return-void
.end method

.method protected Uz(I)I
    .locals 4

    iget-object v1, p0, Lcom/android/settings/accessibility/MiuiListDialogPreference;->adH:[I

    if-eqz v1, :cond_1

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget v3, v1, v0

    if-ne v3, p1, :cond_0

    return v0

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, -0x1

    return v0
.end method

.method public getSummary()Ljava/lang/CharSequence;
    .locals 1

    iget v0, p0, Lcom/android/settings/accessibility/MiuiListDialogPreference;->adK:I

    if-ltz v0, :cond_0

    iget v0, p0, Lcom/android/settings/accessibility/MiuiListDialogPreference;->adK:I

    invoke-virtual {p0, v0}, Lcom/android/settings/accessibility/MiuiListDialogPreference;->Ux(I)Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public getValue()I
    .locals 1

    iget v0, p0, Lcom/android/settings/accessibility/MiuiListDialogPreference;->mValue:I

    return v0
.end method

.method protected abstract of(Landroid/view/View;I)V
.end method

.method public og([Ljava/lang/CharSequence;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/accessibility/MiuiListDialogPreference;->adG:[Ljava/lang/CharSequence;

    return-void
.end method

.method protected onGetDefaultValue(Landroid/content/res/TypedArray;I)Ljava/lang/Object;
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p1, p2, v0}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method protected onPrepareDialogBuilder(Landroid/app/AlertDialog$Builder;)V
    .locals 5

    const/4 v4, 0x0

    invoke-super {p0, p1}, Landroid/preference/DialogPreference;->onPrepareDialogBuilder(Landroid/app/AlertDialog$Builder;)V

    invoke-virtual {p0}, Lcom/android/settings/accessibility/MiuiListDialogPreference;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {p0}, Lcom/android/settings/accessibility/MiuiListDialogPreference;->getDialogLayoutResource()I

    move-result v1

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    invoke-virtual {v0, v1, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    new-instance v2, Lcom/android/settings/accessibility/MiuiListDialogPreference$ListPreferenceAdapter;

    invoke-direct {v2, p0, v4}, Lcom/android/settings/accessibility/MiuiListDialogPreference$ListPreferenceAdapter;-><init>(Lcom/android/settings/accessibility/MiuiListDialogPreference;Lcom/android/settings/accessibility/MiuiListDialogPreference$ListPreferenceAdapter;)V

    const v0, 0x102000a

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/AbsListView;

    invoke-virtual {v0, v2}, Landroid/widget/AbsListView;->setAdapter(Landroid/widget/ListAdapter;)V

    new-instance v2, Lcom/android/settings/accessibility/MiuiListDialogPreference$1;

    invoke-direct {v2, p0}, Lcom/android/settings/accessibility/MiuiListDialogPreference$1;-><init>(Lcom/android/settings/accessibility/MiuiListDialogPreference;)V

    invoke-virtual {v0, v2}, Landroid/widget/AbsListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    iget v2, p0, Lcom/android/settings/accessibility/MiuiListDialogPreference;->mValue:I

    invoke-virtual {p0, v2}, Lcom/android/settings/accessibility/MiuiListDialogPreference;->Uz(I)I

    move-result v2

    const/4 v3, -0x1

    if-eq v2, v3, :cond_0

    invoke-virtual {v0, v2}, Landroid/widget/AbsListView;->setSelection(I)V

    :cond_0
    invoke-virtual {p1, v1}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {p1, v4, v4}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    return-void
.end method

.method protected onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 2

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-class v1, Lcom/android/settings/accessibility/MiuiListDialogPreference$SavedState;

    invoke-virtual {v0, v1}, Ljava/lang/Class;->equals(Ljava/lang/Object;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_1

    :cond_0
    invoke-super {p0, p1}, Landroid/preference/DialogPreference;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    return-void

    :cond_1
    check-cast p1, Lcom/android/settings/accessibility/MiuiListDialogPreference$SavedState;

    invoke-virtual {p1}, Lcom/android/settings/accessibility/MiuiListDialogPreference$SavedState;->getSuperState()Landroid/os/Parcelable;

    move-result-object v0

    invoke-super {p0, v0}, Landroid/preference/DialogPreference;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    iget v0, p1, Lcom/android/settings/accessibility/MiuiListDialogPreference$SavedState;->value:I

    invoke-virtual {p0, v0}, Lcom/android/settings/accessibility/MiuiListDialogPreference;->UC(I)V

    return-void
.end method

.method protected onSaveInstanceState()Landroid/os/Parcelable;
    .locals 2

    invoke-super {p0}, Landroid/preference/DialogPreference;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v0

    invoke-virtual {p0}, Lcom/android/settings/accessibility/MiuiListDialogPreference;->isPersistent()Z

    move-result v1

    if-eqz v1, :cond_0

    return-object v0

    :cond_0
    new-instance v1, Lcom/android/settings/accessibility/MiuiListDialogPreference$SavedState;

    invoke-direct {v1, v0}, Lcom/android/settings/accessibility/MiuiListDialogPreference$SavedState;-><init>(Landroid/os/Parcelable;)V

    invoke-virtual {p0}, Lcom/android/settings/accessibility/MiuiListDialogPreference;->getValue()I

    move-result v0

    iput v0, v1, Lcom/android/settings/accessibility/MiuiListDialogPreference$SavedState;->value:I

    return-object v1
.end method

.method protected onSetInitialValue(ZLjava/lang/Object;)V
    .locals 1

    if-eqz p1, :cond_0

    iget v0, p0, Lcom/android/settings/accessibility/MiuiListDialogPreference;->mValue:I

    invoke-virtual {p0, v0}, Lcom/android/settings/accessibility/MiuiListDialogPreference;->getPersistedInt(I)I

    move-result v0

    :goto_0
    invoke-virtual {p0, v0}, Lcom/android/settings/accessibility/MiuiListDialogPreference;->UC(I)V

    return-void

    :cond_0
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v0

    goto :goto_0
.end method
