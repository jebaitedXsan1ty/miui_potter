.class public Lcom/android/settings/accessibility/ToggleFontSizePreferenceFragment;
.super Lcom/android/settings/PreviewSeekBarPreferenceFragment;
.source "ToggleFontSizePreferenceFragment.java"


# instance fields
.field private adv:[F


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/PreviewSeekBarPreferenceFragment;-><init>()V

    return-void
.end method

.method public static Uf(F[Ljava/lang/String;)I
    .locals 5

    const/4 v0, 0x0

    aget-object v0, p1, v0

    invoke-static {v0}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v1

    const/4 v0, 0x1

    :goto_0
    array-length v2, p1

    if-ge v0, v2, :cond_1

    aget-object v2, p1, v0

    invoke-static {v2}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v2

    sub-float v3, v2, v1

    const/high16 v4, 0x3f000000    # 0.5f

    mul-float/2addr v3, v4

    add-float/2addr v1, v3

    cmpg-float v1, p0, v1

    if-gez v1, :cond_0

    add-int/lit8 v0, v0, -0x1

    return v0

    :cond_0
    add-int/lit8 v0, v0, 0x1

    move v1, v2

    goto :goto_0

    :cond_1
    array-length v0, p1

    add-int/lit8 v0, v0, -0x1

    return v0
.end method


# virtual methods
.method protected Mt(Landroid/content/res/Configuration;I)Landroid/content/res/Configuration;
    .locals 2

    new-instance v0, Landroid/content/res/Configuration;

    invoke-direct {v0, p1}, Landroid/content/res/Configuration;-><init>(Landroid/content/res/Configuration;)V

    iget-object v1, p0, Lcom/android/settings/accessibility/ToggleFontSizePreferenceFragment;->adv:[F

    aget v1, v1, p2

    iput v1, v0, Landroid/content/res/Configuration;->fontScale:F

    return-object v0
.end method

.method protected commit()V
    .locals 4

    invoke-virtual {p0}, Lcom/android/settings/accessibility/ToggleFontSizePreferenceFragment;->getContext()Landroid/content/Context;

    move-result-object v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/android/settings/accessibility/ToggleFontSizePreferenceFragment;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v1, "font_scale"

    iget-object v2, p0, Lcom/android/settings/accessibility/ToggleFontSizePreferenceFragment;->adv:[F

    iget v3, p0, Lcom/android/settings/accessibility/ToggleFontSizePreferenceFragment;->bQs:I

    aget v2, v2, v3

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$System;->putFloat(Landroid/content/ContentResolver;Ljava/lang/String;F)Z

    return-void
.end method

.method public getMetricsCategory()I
    .locals 1

    const/16 v0, 0x154

    return v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 5

    const/4 v0, 0x0

    invoke-super {p0, p1}, Lcom/android/settings/PreviewSeekBarPreferenceFragment;->onCreate(Landroid/os/Bundle;)V

    const v1, 0x7f0d00c4

    iput v1, p0, Lcom/android/settings/accessibility/ToggleFontSizePreferenceFragment;->bQo:I

    const/4 v1, 0x1

    new-array v1, v1, [I

    const v2, 0x7f0d00c5

    aput v2, v1, v0

    iput-object v1, p0, Lcom/android/settings/accessibility/ToggleFontSizePreferenceFragment;->bQp:[I

    invoke-virtual {p0}, Lcom/android/settings/accessibility/ToggleFontSizePreferenceFragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {p0}, Lcom/android/settings/accessibility/ToggleFontSizePreferenceFragment;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const v3, 0x7f030076

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/android/settings/accessibility/ToggleFontSizePreferenceFragment;->bQq:[Ljava/lang/String;

    const v3, 0x7f030077

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v3, "font_scale"

    const/high16 v4, 0x3f800000    # 1.0f

    invoke-static {v2, v3, v4}, Landroid/provider/Settings$System;->getFloat(Landroid/content/ContentResolver;Ljava/lang/String;F)F

    move-result v2

    invoke-static {v2, v1}, Lcom/android/settings/accessibility/ToggleFontSizePreferenceFragment;->Uf(F[Ljava/lang/String;)I

    move-result v2

    iput v2, p0, Lcom/android/settings/accessibility/ToggleFontSizePreferenceFragment;->bQr:I

    array-length v2, v1

    new-array v2, v2, [F

    iput-object v2, p0, Lcom/android/settings/accessibility/ToggleFontSizePreferenceFragment;->adv:[F

    :goto_0
    array-length v2, v1

    if-ge v0, v2, :cond_0

    iget-object v2, p0, Lcom/android/settings/accessibility/ToggleFontSizePreferenceFragment;->adv:[F

    aget-object v3, v1, v0

    invoke-static {v3}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v3

    aput v3, v2, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method
