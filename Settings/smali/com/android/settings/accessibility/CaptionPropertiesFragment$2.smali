.class final Lcom/android/settings/accessibility/CaptionPropertiesFragment$2;
.super Ljava/lang/Object;
.source "CaptionPropertiesFragment.java"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceClickListener;


# instance fields
.field final synthetic afl:Lcom/android/settings/accessibility/CaptionPropertiesFragment;

.field final synthetic afm:Landroid/preference/CheckBoxPreference;


# direct methods
.method constructor <init>(Lcom/android/settings/accessibility/CaptionPropertiesFragment;Landroid/preference/CheckBoxPreference;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/accessibility/CaptionPropertiesFragment$2;->afl:Lcom/android/settings/accessibility/CaptionPropertiesFragment;

    iput-object p2, p0, Lcom/android/settings/accessibility/CaptionPropertiesFragment$2;->afm:Landroid/preference/CheckBoxPreference;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPreferenceClick(Landroid/preference/Preference;)Z
    .locals 5

    const/4 v1, 0x1

    iget-object v0, p0, Lcom/android/settings/accessibility/CaptionPropertiesFragment$2;->afm:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v2

    iget-object v0, p0, Lcom/android/settings/accessibility/CaptionPropertiesFragment$2;->afl:Lcom/android/settings/accessibility/CaptionPropertiesFragment;

    invoke-virtual {v0}, Lcom/android/settings/accessibility/CaptionPropertiesFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string/jumbo v4, "accessibility_captioning_enabled"

    if-eqz v2, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v3, v4, v0}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    iget-object v0, p0, Lcom/android/settings/accessibility/CaptionPropertiesFragment$2;->afl:Lcom/android/settings/accessibility/CaptionPropertiesFragment;

    invoke-static {v0, v2}, Lcom/android/settings/accessibility/CaptionPropertiesFragment;->Vt(Lcom/android/settings/accessibility/CaptionPropertiesFragment;Z)V

    return v1

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
