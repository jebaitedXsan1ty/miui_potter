.class Lcom/android/settings/deviceinfo/StorageSettings$SummaryProvider;
.super Ljava/lang/Object;
.source "StorageSettings.java"

# interfaces
.implements Lcom/android/settings/dashboard/D;


# instance fields
.field private final aJu:Lcom/android/settings/dashboard/C;

.field private final aJv:Lcom/android/settingslib/f/g;

.field private final mContext:Landroid/content/Context;


# direct methods
.method private constructor <init>(Landroid/content/Context;Lcom/android/settings/dashboard/C;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/android/settings/deviceinfo/StorageSettings$SummaryProvider;->mContext:Landroid/content/Context;

    iput-object p2, p0, Lcom/android/settings/deviceinfo/StorageSettings$SummaryProvider;->aJu:Lcom/android/settings/dashboard/C;

    iget-object v0, p0, Lcom/android/settings/deviceinfo/StorageSettings$SummaryProvider;->mContext:Landroid/content/Context;

    const-class v1, Landroid/os/storage/StorageManager;

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/storage/StorageManager;

    new-instance v1, Lcom/android/settingslib/f/g;

    invoke-direct {v1, v0}, Lcom/android/settingslib/f/g;-><init>(Landroid/os/storage/StorageManager;)V

    iput-object v1, p0, Lcom/android/settings/deviceinfo/StorageSettings$SummaryProvider;->aJv:Lcom/android/settingslib/f/g;

    return-void
.end method

.method synthetic constructor <init>(Landroid/content/Context;Lcom/android/settings/dashboard/C;Lcom/android/settings/deviceinfo/StorageSettings$SummaryProvider;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/android/settings/deviceinfo/StorageSettings$SummaryProvider;-><init>(Landroid/content/Context;Lcom/android/settings/dashboard/C;)V

    return-void
.end method

.method private updateSummary()V
    .locals 10

    invoke-static {}, Ljava/text/NumberFormat;->getPercentInstance()Ljava/text/NumberFormat;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/deviceinfo/StorageSettings$SummaryProvider;->aJv:Lcom/android/settingslib/f/g;

    invoke-static {v1}, Lcom/android/settingslib/f/a;->cog(Lcom/android/settingslib/f/b;)Lcom/android/settingslib/f/a;

    move-result-object v1

    iget-wide v2, v1, Lcom/android/settingslib/f/a;->cIH:J

    iget-wide v4, v1, Lcom/android/settingslib/f/a;->cII:J

    sub-long/2addr v2, v4

    long-to-double v2, v2

    iget-object v4, p0, Lcom/android/settings/deviceinfo/StorageSettings$SummaryProvider;->aJu:Lcom/android/settings/dashboard/C;

    iget-object v5, p0, Lcom/android/settings/deviceinfo/StorageSettings$SummaryProvider;->mContext:Landroid/content/Context;

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    iget-wide v8, v1, Lcom/android/settingslib/f/a;->cIH:J

    long-to-double v8, v8

    div-double/2addr v2, v8

    invoke-virtual {v0, v2, v3}, Ljava/text/NumberFormat;->format(D)Ljava/lang/String;

    move-result-object v0

    const/4 v2, 0x0

    aput-object v0, v6, v2

    iget-object v0, p0, Lcom/android/settings/deviceinfo/StorageSettings$SummaryProvider;->mContext:Landroid/content/Context;

    iget-wide v2, v1, Lcom/android/settingslib/f/a;->cII:J

    invoke-static {v0, v2, v3}, Landroid/text/format/Formatter;->formatFileSize(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    aput-object v0, v6, v1

    const v0, 0x7f1211a7

    invoke-virtual {v5, v0, v6}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, p0, v0}, Lcom/android/settings/dashboard/C;->Fd(Lcom/android/settings/dashboard/D;Ljava/lang/CharSequence;)V

    return-void
.end method


# virtual methods
.method public jt(Z)V
    .locals 0

    if-eqz p1, :cond_0

    invoke-direct {p0}, Lcom/android/settings/deviceinfo/StorageSettings$SummaryProvider;->updateSummary()V

    :cond_0
    return-void
.end method
