.class public Lcom/android/settings/deviceinfo/MiuiStorageVolumePreferenceCategory;
.super Landroid/preference/PreferenceCategory;
.source "MiuiStorageVolumePreferenceCategory.java"


# instance fields
.field private aIU:Landroid/os/storage/DiskInfo;

.field private aIV:Landroid/preference/Preference;

.field aIW:Z

.field private aIX:Landroid/preference/Preference;

.field private final aIY:Landroid/content/res/Resources;

.field private final aIZ:Landroid/os/storage/StorageManager;

.field private aJa:J

.field private aJb:Lcom/android/settings/deviceinfo/UsageBarPreference;

.field private aJc:Z

.field private aJd:Ljava/lang/String;

.field private final aJe:Landroid/os/storage/VolumeInfo;


# direct methods
.method private constructor <init>(Landroid/content/Context;Landroid/os/storage/VolumeInfo;Landroid/os/storage/DiskInfo;)V
    .locals 1

    invoke-direct {p0, p1}, Landroid/preference/PreferenceCategory;-><init>(Landroid/content/Context;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/settings/deviceinfo/MiuiStorageVolumePreferenceCategory;->aIW:Z

    iput-object p2, p0, Lcom/android/settings/deviceinfo/MiuiStorageVolumePreferenceCategory;->aJe:Landroid/os/storage/VolumeInfo;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/deviceinfo/MiuiStorageVolumePreferenceCategory;->aIY:Landroid/content/res/Resources;

    invoke-static {p1}, Landroid/os/storage/StorageManager;->from(Landroid/content/Context;)Landroid/os/storage/StorageManager;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/deviceinfo/MiuiStorageVolumePreferenceCategory;->aIZ:Landroid/os/storage/StorageManager;

    iput-object p3, p0, Lcom/android/settings/deviceinfo/MiuiStorageVolumePreferenceCategory;->aIU:Landroid/os/storage/DiskInfo;

    iget-object v0, p0, Lcom/android/settings/deviceinfo/MiuiStorageVolumePreferenceCategory;->aIU:Landroid/os/storage/DiskInfo;

    invoke-virtual {v0}, Landroid/os/storage/DiskInfo;->isSd()Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/settings/deviceinfo/MiuiStorageVolumePreferenceCategory;->aIW:Z

    iget-object v0, p0, Lcom/android/settings/deviceinfo/MiuiStorageVolumePreferenceCategory;->aIU:Landroid/os/storage/DiskInfo;

    invoke-virtual {v0}, Landroid/os/storage/DiskInfo;->getDescription()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/settings/deviceinfo/MiuiStorageVolumePreferenceCategory;->setTitle(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public static awl(Landroid/content/Context;Landroid/os/storage/VolumeInfo;Landroid/os/storage/DiskInfo;)Lcom/android/settings/deviceinfo/MiuiStorageVolumePreferenceCategory;
    .locals 1

    invoke-virtual {p1}, Landroid/os/storage/VolumeInfo;->getType()I

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Lcom/android/settings/deviceinfo/MiuiStorageVolumePreferenceCategory;

    invoke-direct {v0, p0, p1, p2}, Lcom/android/settings/deviceinfo/MiuiStorageVolumePreferenceCategory;-><init>(Landroid/content/Context;Landroid/os/storage/VolumeInfo;Landroid/os/storage/DiskInfo;)V

    return-object v0

    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method private awm(J)Ljava/lang/String;
    .locals 1

    invoke-virtual {p0}, Lcom/android/settings/deviceinfo/MiuiStorageVolumePreferenceCategory;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, p1, p2}, Lmiui/text/ExtraTextUtils;->formatFileSize(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private awo(JJ)V
    .locals 7

    const/4 v6, 0x0

    iput-wide p1, p0, Lcom/android/settings/deviceinfo/MiuiStorageVolumePreferenceCategory;->aJa:J

    sub-long v0, p1, p3

    iget-object v2, p0, Lcom/android/settings/deviceinfo/MiuiStorageVolumePreferenceCategory;->aJb:Lcom/android/settings/deviceinfo/UsageBarPreference;

    invoke-virtual {v2}, Lcom/android/settings/deviceinfo/UsageBarPreference;->clear()V

    iget-wide v2, p0, Lcom/android/settings/deviceinfo/MiuiStorageVolumePreferenceCategory;->aJa:J

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-lez v2, :cond_0

    iget-object v2, p0, Lcom/android/settings/deviceinfo/MiuiStorageVolumePreferenceCategory;->aJb:Lcom/android/settings/deviceinfo/UsageBarPreference;

    long-to-float v0, v0

    long-to-float v1, p1

    div-float/2addr v0, v1

    const/high16 v1, -0x10000

    invoke-virtual {v2, v6, v0, v1}, Lcom/android/settings/deviceinfo/UsageBarPreference;->aue(IFI)V

    iget-object v0, p0, Lcom/android/settings/deviceinfo/MiuiStorageVolumePreferenceCategory;->aJb:Lcom/android/settings/deviceinfo/UsageBarPreference;

    iget-object v1, p0, Lcom/android/settings/deviceinfo/MiuiStorageVolumePreferenceCategory;->aIY:Landroid/content/res/Resources;

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    invoke-direct {p0, p3, p4}, Lcom/android/settings/deviceinfo/MiuiStorageVolumePreferenceCategory;->awm(J)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v6

    invoke-direct {p0, p1, p2}, Lcom/android/settings/deviceinfo/MiuiStorageVolumePreferenceCategory;->awm(J)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x1

    aput-object v3, v2, v4

    const v3, 0x7f120a6d

    invoke-virtual {v1, v3, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/settings/deviceinfo/UsageBarPreference;->setTitle(Ljava/lang/CharSequence;)V

    :cond_0
    iget-object v0, p0, Lcom/android/settings/deviceinfo/MiuiStorageVolumePreferenceCategory;->aJb:Lcom/android/settings/deviceinfo/UsageBarPreference;

    invoke-virtual {v0}, Lcom/android/settings/deviceinfo/UsageBarPreference;->commit()V

    return-void
.end method

.method private awp()V
    .locals 8

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v1, 0x1

    const/4 v5, 0x0

    iget-object v0, p0, Lcom/android/settings/deviceinfo/MiuiStorageVolumePreferenceCategory;->aJe:Landroid/os/storage/VolumeInfo;

    invoke-virtual {v0}, Landroid/os/storage/VolumeInfo;->getState()I

    move-result v2

    invoke-virtual {p0}, Lcom/android/settings/deviceinfo/MiuiStorageVolumePreferenceCategory;->getContext()Landroid/content/Context;

    iget-object v0, p0, Lcom/android/settings/deviceinfo/MiuiStorageVolumePreferenceCategory;->aIX:Landroid/preference/Preference;

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setEnabled(Z)V

    if-eq v2, v6, :cond_0

    if-ne v2, v7, :cond_7

    :cond_0
    iget-object v0, p0, Lcom/android/settings/deviceinfo/MiuiStorageVolumePreferenceCategory;->aIX:Landroid/preference/Preference;

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setEnabled(Z)V

    iget-boolean v0, p0, Lcom/android/settings/deviceinfo/MiuiStorageVolumePreferenceCategory;->aIW:Z

    if-eqz v0, :cond_6

    const v1, 0x7f120ee5

    const v0, 0x7f120ee6

    :goto_0
    iget-object v3, p0, Lcom/android/settings/deviceinfo/MiuiStorageVolumePreferenceCategory;->aIX:Landroid/preference/Preference;

    invoke-virtual {v3, v1}, Landroid/preference/Preference;->setTitle(I)V

    iget-object v1, p0, Lcom/android/settings/deviceinfo/MiuiStorageVolumePreferenceCategory;->aIX:Landroid/preference/Preference;

    invoke-virtual {v1, v0}, Landroid/preference/Preference;->setSummary(I)V

    :cond_1
    :goto_1
    iget-boolean v0, p0, Lcom/android/settings/deviceinfo/MiuiStorageVolumePreferenceCategory;->aJc:Z

    if-eqz v0, :cond_c

    const-string/jumbo v0, "mtp"

    iget-object v1, p0, Lcom/android/settings/deviceinfo/MiuiStorageVolumePreferenceCategory;->aJd:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    const-string/jumbo v0, "ptp"

    iget-object v1, p0, Lcom/android/settings/deviceinfo/MiuiStorageVolumePreferenceCategory;->aJd:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_c

    :cond_2
    iget-object v0, p0, Lcom/android/settings/deviceinfo/MiuiStorageVolumePreferenceCategory;->aIX:Landroid/preference/Preference;

    invoke-virtual {v0, v5}, Landroid/preference/Preference;->setEnabled(Z)V

    if-eq v2, v6, :cond_3

    if-ne v2, v7, :cond_4

    :cond_3
    iget-object v0, p0, Lcom/android/settings/deviceinfo/MiuiStorageVolumePreferenceCategory;->aIX:Landroid/preference/Preference;

    iget-object v1, p0, Lcom/android/settings/deviceinfo/MiuiStorageVolumePreferenceCategory;->aIY:Landroid/content/res/Resources;

    const v2, 0x7f120add

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    :cond_4
    iget-object v0, p0, Lcom/android/settings/deviceinfo/MiuiStorageVolumePreferenceCategory;->aIV:Landroid/preference/Preference;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/android/settings/deviceinfo/MiuiStorageVolumePreferenceCategory;->aIV:Landroid/preference/Preference;

    invoke-virtual {v0, v5}, Landroid/preference/Preference;->setEnabled(Z)V

    iget-object v0, p0, Lcom/android/settings/deviceinfo/MiuiStorageVolumePreferenceCategory;->aIV:Landroid/preference/Preference;

    iget-object v1, p0, Lcom/android/settings/deviceinfo/MiuiStorageVolumePreferenceCategory;->aIY:Landroid/content/res/Resources;

    const v2, 0x7f120add

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    :cond_5
    :goto_2
    return-void

    :cond_6
    const v1, 0x7f12134b

    const v0, 0x7f12134c

    goto :goto_0

    :cond_7
    if-eqz v2, :cond_8

    const/4 v0, 0x6

    if-ne v2, v0, :cond_a

    :cond_8
    iget-object v0, p0, Lcom/android/settings/deviceinfo/MiuiStorageVolumePreferenceCategory;->aIX:Landroid/preference/Preference;

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setEnabled(Z)V

    iget-boolean v0, p0, Lcom/android/settings/deviceinfo/MiuiStorageVolumePreferenceCategory;->aIW:Z

    if-eqz v0, :cond_9

    const v1, 0x7f120eed

    const v0, 0x7f120eee

    :goto_3
    iget-object v3, p0, Lcom/android/settings/deviceinfo/MiuiStorageVolumePreferenceCategory;->aIX:Landroid/preference/Preference;

    iget-object v4, p0, Lcom/android/settings/deviceinfo/MiuiStorageVolumePreferenceCategory;->aIY:Landroid/content/res/Resources;

    invoke-virtual {v4, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Landroid/preference/Preference;->setTitle(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/android/settings/deviceinfo/MiuiStorageVolumePreferenceCategory;->aIX:Landroid/preference/Preference;

    iget-object v3, p0, Lcom/android/settings/deviceinfo/MiuiStorageVolumePreferenceCategory;->aIY:Landroid/content/res/Resources;

    invoke-virtual {v3, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    :goto_4
    iget-object v0, p0, Lcom/android/settings/deviceinfo/MiuiStorageVolumePreferenceCategory;->aJb:Lcom/android/settings/deviceinfo/UsageBarPreference;

    invoke-virtual {p0, v0}, Lcom/android/settings/deviceinfo/MiuiStorageVolumePreferenceCategory;->removePreference(Landroid/preference/Preference;)Z

    iget-object v0, p0, Lcom/android/settings/deviceinfo/MiuiStorageVolumePreferenceCategory;->aIV:Landroid/preference/Preference;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/settings/deviceinfo/MiuiStorageVolumePreferenceCategory;->aIV:Landroid/preference/Preference;

    invoke-virtual {p0, v0}, Lcom/android/settings/deviceinfo/MiuiStorageVolumePreferenceCategory;->removePreference(Landroid/preference/Preference;)Z

    goto/16 :goto_1

    :cond_9
    const v1, 0x7f121356

    const v0, 0x7f121357

    goto :goto_3

    :cond_a
    iget-object v0, p0, Lcom/android/settings/deviceinfo/MiuiStorageVolumePreferenceCategory;->aIX:Landroid/preference/Preference;

    invoke-virtual {v0, v5}, Landroid/preference/Preference;->setEnabled(Z)V

    iget-boolean v0, p0, Lcom/android/settings/deviceinfo/MiuiStorageVolumePreferenceCategory;->aIW:Z

    if-eqz v0, :cond_b

    const v1, 0x7f120eed

    const v0, 0x7f120eeb

    :goto_5
    iget-object v3, p0, Lcom/android/settings/deviceinfo/MiuiStorageVolumePreferenceCategory;->aIX:Landroid/preference/Preference;

    iget-object v4, p0, Lcom/android/settings/deviceinfo/MiuiStorageVolumePreferenceCategory;->aIY:Landroid/content/res/Resources;

    invoke-virtual {v4, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Landroid/preference/Preference;->setTitle(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/android/settings/deviceinfo/MiuiStorageVolumePreferenceCategory;->aIX:Landroid/preference/Preference;

    iget-object v3, p0, Lcom/android/settings/deviceinfo/MiuiStorageVolumePreferenceCategory;->aIY:Landroid/content/res/Resources;

    invoke-virtual {v3, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    goto :goto_4

    :cond_b
    const v1, 0x7f121356

    const v0, 0x7f121351

    goto :goto_5

    :cond_c
    iget-object v0, p0, Lcom/android/settings/deviceinfo/MiuiStorageVolumePreferenceCategory;->aIV:Landroid/preference/Preference;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/android/settings/deviceinfo/MiuiStorageVolumePreferenceCategory;->aIV:Landroid/preference/Preference;

    iget-object v1, p0, Lcom/android/settings/deviceinfo/MiuiStorageVolumePreferenceCategory;->aIX:Landroid/preference/Preference;

    invoke-virtual {v1}, Landroid/preference/Preference;->isEnabled()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setEnabled(Z)V

    iget-boolean v0, p0, Lcom/android/settings/deviceinfo/MiuiStorageVolumePreferenceCategory;->aIW:Z

    if-eqz v0, :cond_d

    iget-object v0, p0, Lcom/android/settings/deviceinfo/MiuiStorageVolumePreferenceCategory;->aIV:Landroid/preference/Preference;

    const v1, 0x7f120eea

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setSummary(I)V

    goto/16 :goto_2

    :cond_d
    iget-object v0, p0, Lcom/android/settings/deviceinfo/MiuiStorageVolumePreferenceCategory;->aIV:Landroid/preference/Preference;

    const v1, 0x7f12134e

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setSummary(I)V

    goto/16 :goto_2
.end method

.method static synthetic awq(Lcom/android/settings/deviceinfo/MiuiStorageVolumePreferenceCategory;JJ)V
    .locals 1

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/android/settings/deviceinfo/MiuiStorageVolumePreferenceCategory;->awo(JJ)V

    return-void
.end method


# virtual methods
.method public awi()Landroid/os/storage/VolumeInfo;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/deviceinfo/MiuiStorageVolumePreferenceCategory;->aJe:Landroid/os/storage/VolumeInfo;

    return-object v0
.end method

.method public awj(Landroid/preference/Preference;)Z
    .locals 1

    iget-object v0, p0, Lcom/android/settings/deviceinfo/MiuiStorageVolumePreferenceCategory;->aIX:Landroid/preference/Preference;

    if-ne p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public awk(Landroid/preference/Preference;)Z
    .locals 1

    iget-object v0, p0, Lcom/android/settings/deviceinfo/MiuiStorageVolumePreferenceCategory;->aIV:Landroid/preference/Preference;

    if-ne p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public awn()V
    .locals 5

    const/4 v4, 0x0

    invoke-virtual {p0}, Lcom/android/settings/deviceinfo/MiuiStorageVolumePreferenceCategory;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {p0}, Lcom/android/settings/deviceinfo/MiuiStorageVolumePreferenceCategory;->removeAll()V

    new-instance v0, Lcom/android/settings/deviceinfo/UsageBarPreference;

    invoke-direct {v0, v2}, Lcom/android/settings/deviceinfo/UsageBarPreference;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/settings/deviceinfo/MiuiStorageVolumePreferenceCategory;->aJb:Lcom/android/settings/deviceinfo/UsageBarPreference;

    iget-object v0, p0, Lcom/android/settings/deviceinfo/MiuiStorageVolumePreferenceCategory;->aJb:Lcom/android/settings/deviceinfo/UsageBarPreference;

    const/4 v1, -0x2

    invoke-virtual {v0, v1}, Lcom/android/settings/deviceinfo/UsageBarPreference;->setOrder(I)V

    iget-object v0, p0, Lcom/android/settings/deviceinfo/MiuiStorageVolumePreferenceCategory;->aJb:Lcom/android/settings/deviceinfo/UsageBarPreference;

    invoke-virtual {p0, v0}, Lcom/android/settings/deviceinfo/MiuiStorageVolumePreferenceCategory;->addPreference(Landroid/preference/Preference;)Z

    new-instance v0, Landroid/preference/Preference;

    invoke-direct {v0, v2}, Landroid/preference/Preference;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/settings/deviceinfo/MiuiStorageVolumePreferenceCategory;->aIX:Landroid/preference/Preference;

    iget-boolean v0, p0, Lcom/android/settings/deviceinfo/MiuiStorageVolumePreferenceCategory;->aIW:Z

    if-eqz v0, :cond_1

    const v1, 0x7f120ee5

    const v0, 0x7f120ee6

    :goto_0
    iget-object v3, p0, Lcom/android/settings/deviceinfo/MiuiStorageVolumePreferenceCategory;->aIX:Landroid/preference/Preference;

    invoke-virtual {v3, v1}, Landroid/preference/Preference;->setTitle(I)V

    iget-object v1, p0, Lcom/android/settings/deviceinfo/MiuiStorageVolumePreferenceCategory;->aIX:Landroid/preference/Preference;

    invoke-virtual {v1, v0}, Landroid/preference/Preference;->setSummary(I)V

    iget-object v0, p0, Lcom/android/settings/deviceinfo/MiuiStorageVolumePreferenceCategory;->aIX:Landroid/preference/Preference;

    invoke-virtual {p0, v0}, Lcom/android/settings/deviceinfo/MiuiStorageVolumePreferenceCategory;->addPreference(Landroid/preference/Preference;)Z

    new-instance v0, Landroid/preference/Preference;

    invoke-direct {v0, v2}, Landroid/preference/Preference;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/settings/deviceinfo/MiuiStorageVolumePreferenceCategory;->aIV:Landroid/preference/Preference;

    iget-boolean v0, p0, Lcom/android/settings/deviceinfo/MiuiStorageVolumePreferenceCategory;->aIW:Z

    if-eqz v0, :cond_2

    const v1, 0x7f120ee9

    const v0, 0x7f120eea

    :goto_1
    iget-object v2, p0, Lcom/android/settings/deviceinfo/MiuiStorageVolumePreferenceCategory;->aIV:Landroid/preference/Preference;

    invoke-virtual {v2, v1}, Landroid/preference/Preference;->setTitle(I)V

    iget-object v1, p0, Lcom/android/settings/deviceinfo/MiuiStorageVolumePreferenceCategory;->aIV:Landroid/preference/Preference;

    invoke-virtual {v1, v0}, Landroid/preference/Preference;->setSummary(I)V

    iget-object v0, p0, Lcom/android/settings/deviceinfo/MiuiStorageVolumePreferenceCategory;->aIV:Landroid/preference/Preference;

    invoke-virtual {p0, v0}, Lcom/android/settings/deviceinfo/MiuiStorageVolumePreferenceCategory;->addPreference(Landroid/preference/Preference;)Z

    iget-object v0, p0, Lcom/android/settings/deviceinfo/MiuiStorageVolumePreferenceCategory;->aJe:Landroid/os/storage/VolumeInfo;

    invoke-virtual {v0}, Landroid/os/storage/VolumeInfo;->isMountedReadable()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Lcom/android/settings/deviceinfo/MiuiStorageVolumePreferenceCategory$ReadVolumeTask;

    invoke-direct {v0, p0}, Lcom/android/settings/deviceinfo/MiuiStorageVolumePreferenceCategory$ReadVolumeTask;-><init>(Lcom/android/settings/deviceinfo/MiuiStorageVolumePreferenceCategory;)V

    const/4 v1, 0x1

    new-array v1, v1, [Landroid/os/storage/VolumeInfo;

    iget-object v2, p0, Lcom/android/settings/deviceinfo/MiuiStorageVolumePreferenceCategory;->aJe:Landroid/os/storage/VolumeInfo;

    aput-object v2, v1, v4

    invoke-virtual {v0, v1}, Lcom/android/settings/deviceinfo/MiuiStorageVolumePreferenceCategory$ReadVolumeTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    :cond_0
    invoke-direct {p0}, Lcom/android/settings/deviceinfo/MiuiStorageVolumePreferenceCategory;->awp()V

    return-void

    :cond_1
    const v1, 0x7f12134b

    const v0, 0x7f12134c

    goto :goto_0

    :cond_2
    const v1, 0x7f12134d

    const v0, 0x7f12134e

    goto :goto_1
.end method
