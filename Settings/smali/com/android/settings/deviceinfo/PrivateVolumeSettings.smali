.class public Lcom/android/settings/deviceinfo/PrivateVolumeSettings;
.super Lcom/android/settings/MiuiSettingsPreferenceFragment;
.source "PrivateVolumeSettings.java"


# static fields
.field private static final aKe:[I

.field private static final aKf:[I


# instance fields
.field private aKA:Lcom/android/settings/deviceinfo/UsageBarPreference;

.field private aKB:Lcom/android/settings/dc;

.field private aKC:Landroid/os/storage/VolumeInfo;

.field private aKD:Ljava/lang/String;

.field public aKd:Ljava/util/List;

.field private aKg:J

.field private aKh:Landroid/content/pm/UserInfo;

.field private aKi:Landroid/preference/Preference;

.field private aKj:Landroid/content/BroadcastReceiver;

.field private aKk:I

.field private aKl:Ljava/util/List;

.field private aKm:Landroid/preference/PreferenceCategory;

.field private aKn:I

.field private aKo:Ljava/util/List;

.field private aKp:J

.field private aKq:Lcom/android/settingslib/f/c;

.field private aKr:Z

.field private final aKs:Lcom/android/settingslib/f/e;

.field private aKt:Landroid/os/storage/VolumeInfo;

.field private final aKu:Landroid/os/storage/StorageEventListener;

.field private aKv:Landroid/os/storage/StorageManager;

.field private aKw:Landroid/preference/PreferenceCategory;

.field private aKx:J

.field private aKy:Landroid/content/BroadcastReceiver;

.field private aKz:J

.field private mUserManager:Landroid/os/UserManager;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const v4, 0x7f120a53

    const v0, 0x7f121176

    filled-new-array {v4, v0}, [I

    move-result-object v0

    sput-object v0, Lcom/android/settings/deviceinfo/PrivateVolumeSettings;->aKe:[I

    const v0, 0x7f120a5b

    const v1, 0x7f120a65

    const v2, 0x7f120a62

    const v3, 0x7f120a63

    filled-new-array {v4, v0, v1, v2, v3}, [I

    move-result-object v0

    sput-object v0, Lcom/android/settings/deviceinfo/PrivateVolumeSettings;->aKf:[I

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/android/settings/MiuiSettingsPreferenceFragment;-><init>()V

    invoke-static {}, Lcom/google/android/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/deviceinfo/PrivateVolumeSettings;->aKd:Ljava/util/List;

    invoke-static {}, Lcom/google/android/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/deviceinfo/PrivateVolumeSettings;->aKo:Ljava/util/List;

    invoke-static {}, Lcom/google/android/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/deviceinfo/PrivateVolumeSettings;->aKl:Ljava/util/List;

    new-instance v0, Lcom/android/settings/deviceinfo/PrivateVolumeSettings$1;

    invoke-direct {v0, p0}, Lcom/android/settings/deviceinfo/PrivateVolumeSettings$1;-><init>(Lcom/android/settings/deviceinfo/PrivateVolumeSettings;)V

    iput-object v0, p0, Lcom/android/settings/deviceinfo/PrivateVolumeSettings;->aKs:Lcom/android/settingslib/f/e;

    new-instance v0, Lcom/android/settings/deviceinfo/PrivateVolumeSettings$2;

    invoke-direct {v0, p0}, Lcom/android/settings/deviceinfo/PrivateVolumeSettings$2;-><init>(Lcom/android/settings/deviceinfo/PrivateVolumeSettings;)V

    iput-object v0, p0, Lcom/android/settings/deviceinfo/PrivateVolumeSettings;->aKu:Landroid/os/storage/StorageEventListener;

    new-instance v0, Lcom/android/settings/deviceinfo/PrivateVolumeSettings$3;

    invoke-direct {v0, p0}, Lcom/android/settings/deviceinfo/PrivateVolumeSettings$3;-><init>(Lcom/android/settings/deviceinfo/PrivateVolumeSettings;)V

    iput-object v0, p0, Lcom/android/settings/deviceinfo/PrivateVolumeSettings;->aKj:Landroid/content/BroadcastReceiver;

    new-instance v0, Lcom/android/settings/deviceinfo/PrivateVolumeSettings$4;

    invoke-direct {v0, p0}, Lcom/android/settings/deviceinfo/PrivateVolumeSettings$4;-><init>(Lcom/android/settings/deviceinfo/PrivateVolumeSettings;)V

    iput-object v0, p0, Lcom/android/settings/deviceinfo/PrivateVolumeSettings;->aKy:Landroid/content/BroadcastReceiver;

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/android/settings/deviceinfo/PrivateVolumeSettings;->setRetainInstance(Z)V

    return-void
.end method

.method static synthetic axA(Lcom/android/settings/deviceinfo/PrivateVolumeSettings;)Lcom/android/settings/dc;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/deviceinfo/PrivateVolumeSettings;->aKB:Lcom/android/settings/dc;

    return-object v0
.end method

.method static synthetic axB(Lcom/android/settings/deviceinfo/PrivateVolumeSettings;)Landroid/os/storage/VolumeInfo;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/deviceinfo/PrivateVolumeSettings;->aKC:Landroid/os/storage/VolumeInfo;

    return-object v0
.end method

.method static synthetic axC(Lcom/android/settings/deviceinfo/PrivateVolumeSettings;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/deviceinfo/PrivateVolumeSettings;->aKD:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic axD(Lcom/android/settings/deviceinfo/PrivateVolumeSettings;Landroid/os/storage/VolumeInfo;)Landroid/os/storage/VolumeInfo;
    .locals 0

    iput-object p1, p0, Lcom/android/settings/deviceinfo/PrivateVolumeSettings;->aKC:Landroid/os/storage/VolumeInfo;

    return-object p1
.end method

.method static synthetic axE(Landroid/os/storage/VolumeInfo;)Z
    .locals 1

    invoke-static {p0}, Lcom/android/settings/deviceinfo/PrivateVolumeSettings;->axp(Landroid/os/storage/VolumeInfo;)Z

    move-result v0

    return v0
.end method

.method static synthetic axF(Lcom/android/settings/deviceinfo/PrivateVolumeSettings;Lcom/android/settingslib/f/d;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/settings/deviceinfo/PrivateVolumeSettings;->axv(Lcom/android/settingslib/f/d;)V

    return-void
.end method

.method static synthetic axG(Lcom/android/settings/deviceinfo/PrivateVolumeSettings;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/deviceinfo/PrivateVolumeSettings;->axu()V

    return-void
.end method

.method static axe(Landroid/os/Bundle;J)V
    .locals 1

    const-string/jumbo v0, "volume_size"

    invoke-virtual {p0, v0, p1, p2}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    return-void
.end method

.method private axf(Landroid/preference/PreferenceGroup;Ljava/lang/CharSequence;)Landroid/preference/PreferenceCategory;
    .locals 2

    iget v0, p0, Lcom/android/settings/deviceinfo/PrivateVolumeSettings;->aKk:I

    iget-object v1, p0, Lcom/android/settings/deviceinfo/PrivateVolumeSettings;->aKl:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    iget-object v0, p0, Lcom/android/settings/deviceinfo/PrivateVolumeSettings;->aKl:Ljava/util/List;

    iget v1, p0, Lcom/android/settings/deviceinfo/PrivateVolumeSettings;->aKk:I

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceCategory;

    :goto_0
    invoke-virtual {v0, p2}, Landroid/preference/PreferenceCategory;->setTitle(Ljava/lang/CharSequence;)V

    invoke-virtual {v0}, Landroid/preference/PreferenceCategory;->removeAll()V

    invoke-direct {p0, p1, v0}, Lcom/android/settings/deviceinfo/PrivateVolumeSettings;->axj(Landroid/preference/PreferenceGroup;Landroid/preference/Preference;)V

    iget v1, p0, Lcom/android/settings/deviceinfo/PrivateVolumeSettings;->aKk:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/android/settings/deviceinfo/PrivateVolumeSettings;->aKk:I

    return-object v0

    :cond_0
    new-instance v0, Landroid/preference/PreferenceCategory;

    invoke-virtual {p0}, Lcom/android/settings/deviceinfo/PrivateVolumeSettings;->bWz()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/preference/PreferenceCategory;-><init>(Landroid/content/Context;)V

    iget-object v1, p0, Lcom/android/settings/deviceinfo/PrivateVolumeSettings;->aKl:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method private axg(Landroid/preference/PreferenceGroup;ZI)V
    .locals 4

    if-eqz p2, :cond_0

    sget-object v0, Lcom/android/settings/deviceinfo/PrivateVolumeSettings;->aKf:[I

    :goto_0
    const/4 v1, 0x0

    :goto_1
    array-length v2, v0

    if-ge v1, v2, :cond_1

    aget v2, v0, v1

    const/4 v3, 0x0

    invoke-direct {p0, p1, v2, v3, p3}, Lcom/android/settings/deviceinfo/PrivateVolumeSettings;->axi(Landroid/preference/PreferenceGroup;ILjava/lang/CharSequence;I)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_0
    sget-object v0, Lcom/android/settings/deviceinfo/PrivateVolumeSettings;->aKe:[I

    goto :goto_0

    :cond_1
    return-void
.end method

.method private axh(Landroid/preference/PreferenceGroup;)V
    .locals 4

    const v2, 0x7f12040b

    new-instance v0, Landroid/preference/PreferenceCategory;

    invoke-virtual {p0}, Lcom/android/settings/deviceinfo/PrivateVolumeSettings;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/preference/PreferenceCategory;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v2}, Landroid/preference/PreferenceCategory;->setTitle(I)V

    invoke-virtual {p1, v0}, Landroid/preference/PreferenceGroup;->addPreference(Landroid/preference/Preference;)Z

    new-instance v0, Landroid/preference/Preference;

    invoke-virtual {p0}, Lcom/android/settings/deviceinfo/PrivateVolumeSettings;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/preference/Preference;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v2}, Landroid/preference/Preference;->setTitle(I)V

    new-instance v1, Landroid/content/Intent;

    const-string/jumbo v2, "com.miui.cleanmaster.InstallAndLunchCleanMaster"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string/jumbo v2, "cleanMasterAction"

    const-string/jumbo v3, "miui.intent.action.GARBAGE_CLEANUP"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setIntent(Landroid/content/Intent;)V

    invoke-virtual {p1, v0}, Landroid/preference/PreferenceGroup;->addPreference(Landroid/preference/Preference;)Z

    return-void
.end method

.method private axi(Landroid/preference/PreferenceGroup;ILjava/lang/CharSequence;I)V
    .locals 4

    const v0, 0x7f121176

    if-ne p2, v0, :cond_1

    iget-wide v0, p0, Lcom/android/settings/deviceinfo/PrivateVolumeSettings;->aKx:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-gtz v0, :cond_0

    const-string/jumbo v0, "PrivateVolumeSettings"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Skipping System storage because its size is "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lcom/android/settings/deviceinfo/PrivateVolumeSettings;->aKx:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :cond_0
    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v0

    if-eq p4, v0, :cond_1

    return-void

    :cond_1
    iget v0, p0, Lcom/android/settings/deviceinfo/PrivateVolumeSettings;->aKn:I

    iget-object v1, p0, Lcom/android/settings/deviceinfo/PrivateVolumeSettings;->aKo:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_3

    iget-object v0, p0, Lcom/android/settings/deviceinfo/PrivateVolumeSettings;->aKo:Ljava/util/List;

    iget v1, p0, Lcom/android/settings/deviceinfo/PrivateVolumeSettings;->aKn:I

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settings/deviceinfo/StorageItemPreference;

    :goto_0
    if-eqz p3, :cond_4

    invoke-virtual {v0, p3}, Lcom/android/settings/deviceinfo/StorageItemPreference;->setTitle(Ljava/lang/CharSequence;)V

    :goto_1
    invoke-static {}, Landroid/app/ActivityManager;->getCurrentUser()I

    move-result v1

    if-eq p4, v1, :cond_2

    const v1, 0x7f080317

    iput v1, v0, Lcom/android/settings/deviceinfo/StorageItemPreference;->aJY:I

    const v1, 0x7f08031c

    invoke-virtual {v0, v1}, Lcom/android/settings/deviceinfo/StorageItemPreference;->setIcon(I)V

    :cond_2
    const v1, 0x7f120a58

    invoke-virtual {v0, v1}, Lcom/android/settings/deviceinfo/StorageItemPreference;->setSummary(I)V

    iput p4, v0, Lcom/android/settings/deviceinfo/StorageItemPreference;->aKb:I

    invoke-direct {p0, p1, v0}, Lcom/android/settings/deviceinfo/PrivateVolumeSettings;->axj(Landroid/preference/PreferenceGroup;Landroid/preference/Preference;)V

    iget v0, p0, Lcom/android/settings/deviceinfo/PrivateVolumeSettings;->aKn:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/android/settings/deviceinfo/PrivateVolumeSettings;->aKn:I

    return-void

    :cond_3
    invoke-direct {p0}, Lcom/android/settings/deviceinfo/PrivateVolumeSettings;->axm()Lcom/android/settings/deviceinfo/StorageItemPreference;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/deviceinfo/PrivateVolumeSettings;->aKo:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_4
    sparse-switch p2, :sswitch_data_0

    goto :goto_1

    :sswitch_0
    const v1, 0x7f080310

    iput v1, v0, Lcom/android/settings/deviceinfo/StorageItemPreference;->aJY:I

    const v1, 0x7f08030e

    invoke-virtual {v0, v1}, Lcom/android/settings/deviceinfo/StorageItemPreference;->setIcon(I)V

    const v1, 0x7f120a53

    invoke-virtual {v0, v1}, Lcom/android/settings/deviceinfo/StorageItemPreference;->setTitle(I)V

    goto :goto_1

    :sswitch_1
    const v1, 0x7f080313

    iput v1, v0, Lcom/android/settings/deviceinfo/StorageItemPreference;->aJY:I

    const v1, 0x7f08031a

    invoke-virtual {v0, v1}, Lcom/android/settings/deviceinfo/StorageItemPreference;->setIcon(I)V

    const v1, 0x7f120a5b

    invoke-virtual {v0, v1}, Lcom/android/settings/deviceinfo/StorageItemPreference;->setTitle(I)V

    goto :goto_1

    :sswitch_2
    const v1, 0x7f080311

    iput v1, v0, Lcom/android/settings/deviceinfo/StorageItemPreference;->aJY:I

    const v1, 0x7f08030f

    invoke-virtual {v0, v1}, Lcom/android/settings/deviceinfo/StorageItemPreference;->setIcon(I)V

    const v1, 0x7f120a65

    invoke-virtual {v0, v1}, Lcom/android/settings/deviceinfo/StorageItemPreference;->setTitle(I)V

    goto :goto_1

    :sswitch_3
    const v1, 0x7f080316

    iput v1, v0, Lcom/android/settings/deviceinfo/StorageItemPreference;->aJY:I

    const v1, 0x7f08031b

    invoke-virtual {v0, v1}, Lcom/android/settings/deviceinfo/StorageItemPreference;->setIcon(I)V

    const v1, 0x7f120a63

    invoke-virtual {v0, v1}, Lcom/android/settings/deviceinfo/StorageItemPreference;->setTitle(I)V

    goto :goto_1

    :sswitch_4
    const v1, 0x7f080312

    iput v1, v0, Lcom/android/settings/deviceinfo/StorageItemPreference;->aJY:I

    const v1, 0x7f080319

    invoke-virtual {v0, v1}, Lcom/android/settings/deviceinfo/StorageItemPreference;->setIcon(I)V

    const v1, 0x7f120a62

    invoke-virtual {v0, v1}, Lcom/android/settings/deviceinfo/StorageItemPreference;->setTitle(I)V

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/android/settings/deviceinfo/StorageItemPreference;->setEnabled(Z)V

    goto/16 :goto_1

    :sswitch_5
    const v1, 0x7f080318

    iput v1, v0, Lcom/android/settings/deviceinfo/StorageItemPreference;->aJY:I

    const v1, 0x7f08031d

    invoke-virtual {v0, v1}, Lcom/android/settings/deviceinfo/StorageItemPreference;->setIcon(I)V

    const v1, 0x7f120a6b

    invoke-virtual {v0, v1}, Lcom/android/settings/deviceinfo/StorageItemPreference;->setTitle(I)V

    goto/16 :goto_1

    nop

    :sswitch_data_0
    .sparse-switch
        0x7f120a53 -> :sswitch_0
        0x7f120a5b -> :sswitch_1
        0x7f120a62 -> :sswitch_4
        0x7f120a63 -> :sswitch_3
        0x7f120a65 -> :sswitch_2
        0x7f120a6b -> :sswitch_5
    .end sparse-switch
.end method

.method private axj(Landroid/preference/PreferenceGroup;Landroid/preference/Preference;)V
    .locals 1

    const v0, 0x7fffffff

    invoke-virtual {p2, v0}, Landroid/preference/Preference;->setOrder(I)V

    invoke-virtual {p1, p2}, Landroid/preference/PreferenceGroup;->addPreference(Landroid/preference/Preference;)Z

    return-void
.end method

.method private axk()V
    .locals 4

    iget-object v0, p0, Lcom/android/settings/deviceinfo/PrivateVolumeSettings;->aKw:Landroid/preference/PreferenceCategory;

    const v1, 0x7f120a6b

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-direct {p0, v0, v1, v2, v3}, Lcom/android/settings/deviceinfo/PrivateVolumeSettings;->axi(Landroid/preference/PreferenceGroup;ILjava/lang/CharSequence;I)V

    return-void
.end method

.method private axl(I)Landroid/preference/Preference;
    .locals 2

    new-instance v0, Landroid/preference/Preference;

    invoke-virtual {p0}, Lcom/android/settings/deviceinfo/PrivateVolumeSettings;->bWz()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/preference/Preference;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, p1}, Landroid/preference/Preference;->setTitle(I)V

    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setKey(Ljava/lang/String;)V

    return-object v0
.end method

.method private axm()Lcom/android/settings/deviceinfo/StorageItemPreference;
    .locals 2

    new-instance v0, Lcom/android/settings/deviceinfo/StorageItemPreference;

    invoke-virtual {p0}, Lcom/android/settings/deviceinfo/PrivateVolumeSettings;->bWz()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/android/settings/deviceinfo/StorageItemPreference;-><init>(Landroid/content/Context;)V

    const v1, 0x7f080114

    invoke-virtual {v0, v1}, Lcom/android/settings/deviceinfo/StorageItemPreference;->setIcon(I)V

    return-object v0
.end method

.method private axn(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
    .locals 3

    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v1, "android.intent.action.VIEW"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-static {p1, p2}, Landroid/provider/DocumentsContract;->buildRootUri(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    const-string/jumbo v2, "vnd.android.document/root"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setDataAndType(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/Intent;

    const-string/jumbo v1, "android.intent.category.DEFAULT"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    return-object v0
.end method

.method private axo(Landroid/content/pm/UserInfo;)Ljava/lang/String;
    .locals 5

    const/4 v4, 0x0

    iget-object v0, p1, Landroid/content/pm/UserInfo;->name:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/android/settings/deviceinfo/PrivateVolumeSettings;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string/jumbo v2, "second_user_id"

    const/16 v3, -0x2710

    invoke-static {v1, v2, v3, v4}, Landroid/provider/Settings$Secure;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v1

    iget v2, p1, Landroid/content/pm/UserInfo;->id:I

    if-ne v2, v1, :cond_1

    const v0, 0x7f120fbf

    invoke-virtual {p0, v0}, Lcom/android/settings/deviceinfo/PrivateVolumeSettings;->getString(I)Ljava/lang/String;

    move-result-object v0

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    iget v1, p1, Landroid/content/pm/UserInfo;->id:I

    if-nez v1, :cond_0

    const v0, 0x7f120c37

    invoke-virtual {p0, v0}, Lcom/android/settings/deviceinfo/PrivateVolumeSettings;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private static axp(Landroid/os/storage/VolumeInfo;)Z
    .locals 1

    invoke-virtual {p0}, Landroid/os/storage/VolumeInfo;->getType()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    const/4 v0, 0x0

    return v0

    :pswitch_0
    const/4 v0, 0x1

    return v0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method private axq()Z
    .locals 2

    iget-object v0, p0, Lcom/android/settings/deviceinfo/PrivateVolumeSettings;->aKC:Landroid/os/storage/VolumeInfo;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/deviceinfo/PrivateVolumeSettings;->aKC:Landroid/os/storage/VolumeInfo;

    invoke-virtual {v0}, Landroid/os/storage/VolumeInfo;->getType()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/android/settings/deviceinfo/PrivateVolumeSettings;->aKC:Landroid/os/storage/VolumeInfo;

    invoke-virtual {v0}, Landroid/os/storage/VolumeInfo;->isMountedReadable()Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private axr(Ljava/util/List;)V
    .locals 5

    const/16 v4, 0x63

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/UserInfo;

    iget v2, v0, Landroid/content/pm/UserInfo;->id:I

    const/16 v3, 0x3e7

    if-eq v2, v3, :cond_1

    iget v0, v0, Landroid/content/pm/UserInfo;->id:I

    if-ne v0, v4, :cond_0

    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    :cond_2
    return-void
.end method

.method private axs()V
    .locals 3

    invoke-virtual {p0}, Lcom/android/settings/deviceinfo/PrivateVolumeSettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/deviceinfo/PrivateVolumeSettings;->aKv:Landroid/os/storage/StorageManager;

    iget-object v2, p0, Lcom/android/settings/deviceinfo/PrivateVolumeSettings;->aKC:Landroid/os/storage/VolumeInfo;

    invoke-virtual {v1, v2}, Landroid/os/storage/StorageManager;->getBestVolumeDescription(Landroid/os/storage/VolumeInfo;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/Activity;->setTitle(Ljava/lang/CharSequence;)V

    return-void
.end method

.method private static varargs axt(Lcom/android/settingslib/f/d;I[Ljava/lang/String;)J
    .locals 8

    const-wide/16 v2, 0x0

    iget-object v0, p0, Lcom/android/settingslib/f/d;->cIT:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/HashMap;

    if-eqz v0, :cond_1

    const/4 v1, 0x0

    array-length v5, p2

    move v4, v1

    :goto_0
    if-ge v4, v5, :cond_2

    aget-object v1, p2, v4

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    add-long/2addr v2, v6

    :cond_0
    add-int/lit8 v1, v4, 0x1

    move v4, v1

    goto :goto_0

    :cond_1
    const-string/jumbo v0, "PrivateVolumeSettings"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "MeasurementDetails mediaSize array does not have key for user "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    return-wide v2
.end method

.method private axu()V
    .locals 12

    const/4 v3, 0x1

    const/4 v6, 0x0

    const/4 v2, 0x0

    invoke-direct {p0}, Lcom/android/settings/deviceinfo/PrivateVolumeSettings;->axq()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/android/settings/deviceinfo/PrivateVolumeSettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/android/settings/deviceinfo/PrivateVolumeSettings;->getActionBar()Lmiui/app/ActionBar;

    move-result-object v0

    const v1, 0x7f1211a2

    invoke-virtual {v0, v1}, Lmiui/app/ActionBar;->setTitle(I)V

    invoke-virtual {p0}, Lcom/android/settings/deviceinfo/PrivateVolumeSettings;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentManager;->invalidateOptionsMenu()V

    invoke-virtual {p0}, Lcom/android/settings/deviceinfo/PrivateVolumeSettings;->getActivity()Landroid/app/Activity;

    move-result-object v8

    invoke-virtual {p0}, Lcom/android/settings/deviceinfo/PrivateVolumeSettings;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v9

    invoke-virtual {v9}, Landroid/preference/PreferenceScreen;->removeAll()V

    iget-object v0, p0, Lcom/android/settings/deviceinfo/PrivateVolumeSettings;->aKd:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    iget-object v0, p0, Lcom/android/settings/deviceinfo/PrivateVolumeSettings;->aKA:Lcom/android/settings/deviceinfo/UsageBarPreference;

    invoke-direct {p0, v9, v0}, Lcom/android/settings/deviceinfo/PrivateVolumeSettings;->axj(Landroid/preference/PreferenceGroup;Landroid/preference/Preference;)V

    iget-object v0, p0, Lcom/android/settings/deviceinfo/PrivateVolumeSettings;->aKm:Landroid/preference/PreferenceCategory;

    invoke-virtual {v0}, Landroid/preference/PreferenceCategory;->removeAll()V

    iget-object v0, p0, Lcom/android/settings/deviceinfo/PrivateVolumeSettings;->aKm:Landroid/preference/PreferenceCategory;

    invoke-direct {p0, v9, v0}, Lcom/android/settings/deviceinfo/PrivateVolumeSettings;->axj(Landroid/preference/PreferenceGroup;Landroid/preference/Preference;)V

    iget-object v0, p0, Lcom/android/settings/deviceinfo/PrivateVolumeSettings;->mUserManager:Landroid/os/UserManager;

    invoke-virtual {v0}, Landroid/os/UserManager;->getUsers()Ljava/util/List;

    move-result-object v10

    iget-object v0, p0, Lcom/android/settings/deviceinfo/PrivateVolumeSettings;->aKt:Landroid/os/storage/VolumeInfo;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/settings/deviceinfo/PrivateVolumeSettings;->aKt:Landroid/os/storage/VolumeInfo;

    invoke-virtual {v0}, Landroid/os/storage/VolumeInfo;->isMountedReadable()Z

    move-result v0

    move v1, v0

    :goto_0
    iput v2, p0, Lcom/android/settings/deviceinfo/PrivateVolumeSettings;->aKn:I

    iput v2, p0, Lcom/android/settings/deviceinfo/PrivateVolumeSettings;->aKk:I

    invoke-direct {p0, v10}, Lcom/android/settings/deviceinfo/PrivateVolumeSettings;->axr(Ljava/util/List;)V

    invoke-interface {v10}, Ljava/util/List;->size()I

    move-result v11

    if-le v11, v3, :cond_2

    :goto_1
    move v7, v2

    move v5, v2

    :goto_2
    if-ge v7, v11, :cond_4

    invoke-interface {v10, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/UserInfo;

    iget-object v4, p0, Lcom/android/settings/deviceinfo/PrivateVolumeSettings;->aKh:Landroid/content/pm/UserInfo;

    invoke-static {v4, v0}, Lcom/android/settings/aq;->bqT(Landroid/content/pm/UserInfo;Landroid/content/pm/UserInfo;)Z

    move-result v4

    if-eqz v4, :cond_c

    if-eqz v3, :cond_3

    invoke-direct {p0, v0}, Lcom/android/settings/deviceinfo/PrivateVolumeSettings;->axo(Landroid/content/pm/UserInfo;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0, v9, v4}, Lcom/android/settings/deviceinfo/PrivateVolumeSettings;->axf(Landroid/preference/PreferenceGroup;Ljava/lang/CharSequence;)Landroid/preference/PreferenceCategory;

    move-result-object v4

    :goto_3
    iget v0, v0, Landroid/content/pm/UserInfo;->id:I

    invoke-direct {p0, v4, v1, v0}, Lcom/android/settings/deviceinfo/PrivateVolumeSettings;->axg(Landroid/preference/PreferenceGroup;ZI)V

    add-int/lit8 v0, v5, 0x1

    :goto_4
    add-int/lit8 v4, v7, 0x1

    move v7, v4

    move v5, v0

    goto :goto_2

    :cond_1
    move v1, v2

    goto :goto_0

    :cond_2
    move v3, v2

    goto :goto_1

    :cond_3
    iget-object v4, p0, Lcom/android/settings/deviceinfo/PrivateVolumeSettings;->aKm:Landroid/preference/PreferenceCategory;

    goto :goto_3

    :cond_4
    sub-int v0, v11, v5

    if-lez v0, :cond_6

    const v0, 0x7f12119e

    invoke-virtual {p0, v0}, Lcom/android/settings/deviceinfo/PrivateVolumeSettings;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-direct {p0, v9, v0}, Lcom/android/settings/deviceinfo/PrivateVolumeSettings;->axf(Landroid/preference/PreferenceGroup;Ljava/lang/CharSequence;)Landroid/preference/PreferenceCategory;

    move-result-object v3

    move v1, v2

    :goto_5
    if-ge v1, v11, :cond_6

    invoke-interface {v10, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/UserInfo;

    iget-object v4, p0, Lcom/android/settings/deviceinfo/PrivateVolumeSettings;->aKh:Landroid/content/pm/UserInfo;

    invoke-static {v4, v0}, Lcom/android/settings/aq;->bqT(Landroid/content/pm/UserInfo;Landroid/content/pm/UserInfo;)Z

    move-result v4

    if-nez v4, :cond_5

    invoke-direct {p0, v0}, Lcom/android/settings/deviceinfo/PrivateVolumeSettings;->axo(Landroid/content/pm/UserInfo;)Ljava/lang/String;

    move-result-object v4

    iget v0, v0, Landroid/content/pm/UserInfo;->id:I

    invoke-direct {p0, v3, v2, v4, v0}, Lcom/android/settings/deviceinfo/PrivateVolumeSettings;->axi(Landroid/preference/PreferenceGroup;ILjava/lang/CharSequence;I)V

    :cond_5
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_5

    :cond_6
    iget-object v0, p0, Lcom/android/settings/deviceinfo/PrivateVolumeSettings;->aKw:Landroid/preference/PreferenceCategory;

    invoke-virtual {v0}, Landroid/preference/PreferenceCategory;->removeAll()V

    iget-object v0, p0, Lcom/android/settings/deviceinfo/PrivateVolumeSettings;->aKw:Landroid/preference/PreferenceCategory;

    invoke-direct {p0, v9, v0}, Lcom/android/settings/deviceinfo/PrivateVolumeSettings;->axj(Landroid/preference/PreferenceGroup;Landroid/preference/Preference;)V

    invoke-direct {p0}, Lcom/android/settings/deviceinfo/PrivateVolumeSettings;->axk()V

    iget-object v0, p0, Lcom/android/settings/deviceinfo/PrivateVolumeSettings;->aKv:Landroid/os/storage/StorageManager;

    invoke-virtual {v0}, Landroid/os/storage/StorageManager;->getVolumes()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_7
    :goto_6
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_9

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/storage/VolumeInfo;

    invoke-virtual {v0}, Landroid/os/storage/VolumeInfo;->getType()I

    move-result v1

    if-nez v1, :cond_8

    iget-object v1, p0, Lcom/android/settings/deviceinfo/PrivateVolumeSettings;->aKv:Landroid/os/storage/StorageManager;

    invoke-virtual {v0}, Landroid/os/storage/VolumeInfo;->getDiskId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Landroid/os/storage/StorageManager;->findDiskById(Ljava/lang/String;)Landroid/os/storage/DiskInfo;

    move-result-object v1

    :goto_7
    if-eqz v1, :cond_7

    invoke-static {v8, v0, v1}, Lcom/android/settings/deviceinfo/MiuiStorageVolumePreferenceCategory;->awl(Landroid/content/Context;Landroid/os/storage/VolumeInfo;Landroid/os/storage/DiskInfo;)Lcom/android/settings/deviceinfo/MiuiStorageVolumePreferenceCategory;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/deviceinfo/PrivateVolumeSettings;->aKd:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-direct {p0, v9, v0}, Lcom/android/settings/deviceinfo/PrivateVolumeSettings;->axj(Landroid/preference/PreferenceGroup;Landroid/preference/Preference;)V

    invoke-virtual {v0}, Lcom/android/settings/deviceinfo/MiuiStorageVolumePreferenceCategory;->awn()V

    goto :goto_6

    :cond_8
    move-object v1, v6

    goto :goto_7

    :cond_9
    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v1, "miui.intent.action.GARBAGE_CLEANUP"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/android/settings/deviceinfo/PrivateVolumeSettings;->bWA()Landroid/content/pm/PackageManager;

    move-result-object v1

    const/high16 v3, 0x10000

    invoke-virtual {v1, v0, v3}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_a

    invoke-direct {p0, v9}, Lcom/android/settings/deviceinfo/PrivateVolumeSettings;->axh(Landroid/preference/PreferenceGroup;)V

    :cond_a
    iget-object v0, p0, Lcom/android/settings/deviceinfo/PrivateVolumeSettings;->aKC:Landroid/os/storage/VolumeInfo;

    invoke-virtual {v0}, Landroid/os/storage/VolumeInfo;->getPath()Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->getUsableSpace()J

    invoke-virtual {v0}, Ljava/io/File;->getTotalSpace()J

    move-result-wide v0

    iget-wide v4, p0, Lcom/android/settings/deviceinfo/PrivateVolumeSettings;->aKz:J

    invoke-virtual {p0}, Lcom/android/settings/deviceinfo/PrivateVolumeSettings;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-static {v3}, Lcom/android/settings/device/h;->getInstance(Landroid/content/Context;)Lcom/android/settings/device/h;

    move-result-object v3

    invoke-virtual {v3}, Lcom/android/settings/device/h;->aEj()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/android/settings/deviceinfo/PrivateVolumeSettings;->aKp:J

    iget-object v3, p0, Lcom/android/settings/deviceinfo/PrivateVolumeSettings;->aKA:Lcom/android/settings/deviceinfo/UsageBarPreference;

    invoke-virtual {p0}, Lcom/android/settings/deviceinfo/PrivateVolumeSettings;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f120a58

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/android/settings/deviceinfo/UsageBarPreference;->setTitle(Ljava/lang/CharSequence;)V

    iput-wide v0, p0, Lcom/android/settings/deviceinfo/PrivateVolumeSettings;->aKg:J

    iget-object v0, p0, Lcom/android/settings/deviceinfo/PrivateVolumeSettings;->aKq:Lcom/android/settingslib/f/c;

    invoke-virtual {v0}, Lcom/android/settingslib/f/c;->cor()V

    iput-boolean v2, p0, Lcom/android/settings/deviceinfo/PrivateVolumeSettings;->aKr:Z

    iget-object v0, p0, Lcom/android/settings/deviceinfo/PrivateVolumeSettings;->aKm:Landroid/preference/PreferenceCategory;

    invoke-virtual {v0}, Landroid/preference/PreferenceCategory;->getPreferenceCount()I

    move-result v0

    if-nez v0, :cond_b

    iget-object v0, p0, Lcom/android/settings/deviceinfo/PrivateVolumeSettings;->aKm:Landroid/preference/PreferenceCategory;

    invoke-virtual {v9, v0}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    :cond_b
    return-void

    :cond_c
    move v0, v5

    goto/16 :goto_4
.end method

.method private axv(Lcom/android/settingslib/f/d;)V
    .locals 22

    const/4 v5, 0x0

    const-wide/16 v10, 0x0

    const-wide/16 v8, 0x0

    const-wide/16 v6, 0x0

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/settings/deviceinfo/PrivateVolumeSettings;->aKA:Lcom/android/settings/deviceinfo/UsageBarPreference;

    invoke-virtual {v4}, Lcom/android/settings/deviceinfo/UsageBarPreference;->clear()V

    const/4 v4, 0x0

    move-object v12, v5

    move v5, v4

    :goto_0
    move-object/from16 v0, p0

    iget v4, v0, Lcom/android/settings/deviceinfo/PrivateVolumeSettings;->aKn:I

    if-ge v5, v4, :cond_0

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/settings/deviceinfo/PrivateVolumeSettings;->aKo:Ljava/util/List;

    invoke-interface {v4, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/android/settings/deviceinfo/StorageItemPreference;

    iget v13, v4, Lcom/android/settings/deviceinfo/StorageItemPreference;->aKb:I

    invoke-virtual {v4}, Lcom/android/settings/deviceinfo/StorageItemPreference;->getTitleRes()I

    move-result v14

    sparse-switch v14, :sswitch_data_0

    :goto_1
    add-int/lit8 v4, v5, 0x1

    move v5, v4

    goto :goto_0

    :sswitch_0
    move-object/from16 v0, p0

    iget-wide v14, v0, Lcom/android/settings/deviceinfo/PrivateVolumeSettings;->aKp:J

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/android/settings/deviceinfo/PrivateVolumeSettings;->aKg:J

    move-wide/from16 v16, v0

    sub-long v14, v14, v16

    move-object/from16 v0, p0

    invoke-direct {v0, v4, v14, v15}, Lcom/android/settings/deviceinfo/PrivateVolumeSettings;->axw(Lcom/android/settings/deviceinfo/StorageItemPreference;J)V

    goto :goto_1

    :sswitch_1
    move-object/from16 v0, p0

    iget-wide v14, v0, Lcom/android/settings/deviceinfo/PrivateVolumeSettings;->aKx:J

    move-object/from16 v0, p0

    invoke-direct {v0, v4, v14, v15}, Lcom/android/settings/deviceinfo/PrivateVolumeSettings;->axw(Lcom/android/settings/deviceinfo/StorageItemPreference;J)V

    move-object/from16 v0, p0

    iget-wide v14, v0, Lcom/android/settings/deviceinfo/PrivateVolumeSettings;->aKx:J

    add-long/2addr v10, v14

    goto :goto_1

    :sswitch_2
    move-object/from16 v0, p1

    iget-object v14, v0, Lcom/android/settingslib/f/d;->cIR:Landroid/util/SparseLongArray;

    invoke-virtual {v14, v13}, Landroid/util/SparseLongArray;->get(I)J

    move-result-wide v14

    move-object/from16 v0, p0

    invoke-direct {v0, v4, v14, v15}, Lcom/android/settings/deviceinfo/PrivateVolumeSettings;->axw(Lcom/android/settings/deviceinfo/StorageItemPreference;J)V

    move-object/from16 v0, p1

    iget-object v4, v0, Lcom/android/settingslib/f/d;->cIR:Landroid/util/SparseLongArray;

    invoke-virtual {v4, v13}, Landroid/util/SparseLongArray;->get(I)J

    move-result-wide v14

    add-long/2addr v10, v14

    goto :goto_1

    :sswitch_3
    const/4 v14, 0x3

    new-array v14, v14, [Ljava/lang/String;

    sget-object v15, Landroid/os/Environment;->DIRECTORY_DCIM:Ljava/lang/String;

    const/16 v16, 0x0

    aput-object v15, v14, v16

    sget-object v15, Landroid/os/Environment;->DIRECTORY_MOVIES:Ljava/lang/String;

    const/16 v16, 0x1

    aput-object v15, v14, v16

    sget-object v15, Landroid/os/Environment;->DIRECTORY_PICTURES:Ljava/lang/String;

    const/16 v16, 0x2

    aput-object v15, v14, v16

    move-object/from16 v0, p1

    invoke-static {v0, v13, v14}, Lcom/android/settings/deviceinfo/PrivateVolumeSettings;->axt(Lcom/android/settingslib/f/d;I[Ljava/lang/String;)J

    move-result-wide v14

    move-object/from16 v0, p0

    invoke-direct {v0, v4, v14, v15}, Lcom/android/settings/deviceinfo/PrivateVolumeSettings;->axw(Lcom/android/settings/deviceinfo/StorageItemPreference;J)V

    add-long/2addr v10, v14

    goto :goto_1

    :sswitch_4
    const/4 v14, 0x1

    new-array v14, v14, [Ljava/lang/String;

    sget-object v15, Landroid/os/Environment;->DIRECTORY_MOVIES:Ljava/lang/String;

    const/16 v16, 0x0

    aput-object v15, v14, v16

    move-object/from16 v0, p1

    invoke-static {v0, v13, v14}, Lcom/android/settings/deviceinfo/PrivateVolumeSettings;->axt(Lcom/android/settingslib/f/d;I[Ljava/lang/String;)J

    move-result-wide v14

    move-object/from16 v0, p0

    invoke-direct {v0, v4, v14, v15}, Lcom/android/settings/deviceinfo/PrivateVolumeSettings;->axw(Lcom/android/settings/deviceinfo/StorageItemPreference;J)V

    add-long/2addr v10, v14

    goto :goto_1

    :sswitch_5
    const/4 v14, 0x5

    new-array v14, v14, [Ljava/lang/String;

    sget-object v15, Landroid/os/Environment;->DIRECTORY_MUSIC:Ljava/lang/String;

    const/16 v16, 0x0

    aput-object v15, v14, v16

    sget-object v15, Landroid/os/Environment;->DIRECTORY_ALARMS:Ljava/lang/String;

    const/16 v16, 0x1

    aput-object v15, v14, v16

    sget-object v15, Landroid/os/Environment;->DIRECTORY_NOTIFICATIONS:Ljava/lang/String;

    const/16 v16, 0x2

    aput-object v15, v14, v16

    sget-object v15, Landroid/os/Environment;->DIRECTORY_RINGTONES:Ljava/lang/String;

    const/16 v16, 0x3

    aput-object v15, v14, v16

    sget-object v15, Landroid/os/Environment;->DIRECTORY_PODCASTS:Ljava/lang/String;

    const/16 v16, 0x4

    aput-object v15, v14, v16

    move-object/from16 v0, p1

    invoke-static {v0, v13, v14}, Lcom/android/settings/deviceinfo/PrivateVolumeSettings;->axt(Lcom/android/settingslib/f/d;I[Ljava/lang/String;)J

    move-result-wide v14

    move-object/from16 v0, p0

    invoke-direct {v0, v4, v14, v15}, Lcom/android/settings/deviceinfo/PrivateVolumeSettings;->axw(Lcom/android/settings/deviceinfo/StorageItemPreference;J)V

    add-long/2addr v10, v14

    goto/16 :goto_1

    :sswitch_6
    move-object/from16 v0, p1

    iget-object v13, v0, Lcom/android/settingslib/f/d;->cIS:Landroid/util/SparseLongArray;

    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v14

    invoke-virtual {v13, v14}, Landroid/util/SparseLongArray;->get(I)J

    move-result-wide v14

    move-object/from16 v0, p0

    invoke-direct {v0, v4, v14, v15}, Lcom/android/settings/deviceinfo/PrivateVolumeSettings;->axw(Lcom/android/settings/deviceinfo/StorageItemPreference;J)V

    goto/16 :goto_1

    :sswitch_7
    move-object/from16 v0, p1

    iget-wide v14, v0, Lcom/android/settingslib/f/d;->cIU:J

    move-object/from16 v0, p0

    invoke-direct {v0, v4, v14, v15}, Lcom/android/settings/deviceinfo/PrivateVolumeSettings;->axw(Lcom/android/settings/deviceinfo/StorageItemPreference;J)V

    goto/16 :goto_1

    :sswitch_8
    const/4 v12, 0x1

    new-array v12, v12, [Ljava/lang/String;

    sget-object v14, Landroid/os/Environment;->DIRECTORY_DOWNLOADS:Ljava/lang/String;

    const/4 v15, 0x0

    aput-object v14, v12, v15

    move-object/from16 v0, p1

    invoke-static {v0, v13, v12}, Lcom/android/settings/deviceinfo/PrivateVolumeSettings;->axt(Lcom/android/settingslib/f/d;I[Ljava/lang/String;)J

    move-result-wide v14

    move-object/from16 v0, p1

    iget-object v12, v0, Lcom/android/settingslib/f/d;->cIS:Landroid/util/SparseLongArray;

    invoke-virtual {v12, v13}, Landroid/util/SparseLongArray;->get(I)J

    move-result-wide v12

    add-long/2addr v6, v14

    add-long/2addr v8, v12

    add-long/2addr v12, v14

    add-long/2addr v10, v12

    move-object v12, v4

    goto/16 :goto_1

    :sswitch_9
    move-object/from16 v0, p1

    iget-wide v14, v0, Lcom/android/settingslib/f/d;->cIU:J

    move-object/from16 v0, p0

    invoke-direct {v0, v4, v14, v15}, Lcom/android/settings/deviceinfo/PrivateVolumeSettings;->axw(Lcom/android/settings/deviceinfo/StorageItemPreference;J)V

    move-object/from16 v0, p1

    iget-wide v14, v0, Lcom/android/settingslib/f/d;->cIU:J

    add-long/2addr v10, v14

    goto/16 :goto_1

    :sswitch_a
    move-object/from16 v0, p1

    iget-object v14, v0, Lcom/android/settingslib/f/d;->cIP:Landroid/util/SparseLongArray;

    invoke-virtual {v14, v13}, Landroid/util/SparseLongArray;->get(I)J

    move-result-wide v14

    move-object/from16 v0, p0

    invoke-direct {v0, v4, v14, v15}, Lcom/android/settings/deviceinfo/PrivateVolumeSettings;->axw(Lcom/android/settings/deviceinfo/StorageItemPreference;J)V

    add-long/2addr v10, v14

    goto/16 :goto_1

    :cond_0
    if-eqz v12, :cond_1

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/android/settings/deviceinfo/PrivateVolumeSettings;->aKz:J

    move-object/from16 v0, p1

    iget-wide v14, v0, Lcom/android/settingslib/f/d;->cIO:J

    sub-long/2addr v4, v14

    sub-long v14, v4, v10

    add-long v16, v8, v6

    add-long v16, v16, v14

    const-string/jumbo v13, "PrivateVolumeSettings"

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v19, "Other items: \n\tmTotalSize: "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/android/settings/deviceinfo/PrivateVolumeSettings;->aKz:J

    move-wide/from16 v20, v0

    move-object/from16 v0, v18

    move-wide/from16 v1, v20

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string/jumbo v19, " availSize: "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, p1

    iget-wide v0, v0, Lcom/android/settingslib/f/d;->cIO:J

    move-wide/from16 v20, v0

    move-object/from16 v0, v18

    move-wide/from16 v1, v20

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string/jumbo v19, " usedSize: "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, "\n\taccountedSize: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, " unaccountedSize size: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v14, v15}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, "\n\ttotalMiscSize: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, " totalDownloadsSize: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, "\n\tdetails: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v13, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v0, p0

    move-wide/from16 v1, v16

    invoke-direct {v0, v12, v1, v2}, Lcom/android/settings/deviceinfo/PrivateVolumeSettings;->axw(Lcom/android/settings/deviceinfo/StorageItemPreference;J)V

    :cond_1
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/settings/deviceinfo/PrivateVolumeSettings;->aKA:Lcom/android/settings/deviceinfo/UsageBarPreference;

    invoke-virtual {v4}, Lcom/android/settings/deviceinfo/UsageBarPreference;->commit()V

    return-void

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_a
        0x7f120a53 -> :sswitch_2
        0x7f120a5b -> :sswitch_3
        0x7f120a62 -> :sswitch_7
        0x7f120a63 -> :sswitch_6
        0x7f120a65 -> :sswitch_5
        0x7f120a6b -> :sswitch_0
        0x7f12116f -> :sswitch_9
        0x7f121175 -> :sswitch_8
        0x7f121176 -> :sswitch_1
        0x7f121177 -> :sswitch_4
    .end sparse-switch
.end method

.method private axw(Lcom/android/settings/deviceinfo/StorageItemPreference;J)V
    .locals 6

    iget-object v0, p0, Lcom/android/settings/deviceinfo/PrivateVolumeSettings;->aKB:Lcom/android/settings/dc;

    invoke-virtual {p0}, Lcom/android/settings/deviceinfo/PrivateVolumeSettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0, p2, p3}, Lcom/android/settings/dc;->bYJ(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/android/settings/deviceinfo/StorageItemPreference;->setSummary(Ljava/lang/CharSequence;)V

    invoke-virtual {p1}, Lcom/android/settings/deviceinfo/StorageItemPreference;->getOrder()I

    move-result v0

    iget-object v1, p0, Lcom/android/settings/deviceinfo/PrivateVolumeSettings;->aKA:Lcom/android/settings/deviceinfo/UsageBarPreference;

    long-to-float v2, p2

    iget-wide v4, p0, Lcom/android/settings/deviceinfo/PrivateVolumeSettings;->aKp:J

    long-to-float v3, v4

    div-float/2addr v2, v3

    iget v3, p1, Lcom/android/settings/deviceinfo/StorageItemPreference;->aJY:I

    invoke-virtual {v1, v0, v2, v3}, Lcom/android/settings/deviceinfo/UsageBarPreference;->aue(IFI)V

    return-void
.end method

.method static synthetic axx(Lcom/android/settings/deviceinfo/PrivateVolumeSettings;)J
    .locals 2

    iget-wide v0, p0, Lcom/android/settings/deviceinfo/PrivateVolumeSettings;->aKp:J

    return-wide v0
.end method

.method static synthetic axy(Lcom/android/settings/deviceinfo/PrivateVolumeSettings;)Landroid/os/storage/StorageManager;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/deviceinfo/PrivateVolumeSettings;->aKv:Landroid/os/storage/StorageManager;

    return-object v0
.end method

.method static synthetic axz(Lcom/android/settings/deviceinfo/PrivateVolumeSettings;)Lcom/android/settings/deviceinfo/UsageBarPreference;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/deviceinfo/PrivateVolumeSettings;->aKA:Lcom/android/settings/deviceinfo/UsageBarPreference;

    return-object v0
.end method


# virtual methods
.method public at(Landroid/preference/Preference;)Z
    .locals 8

    const/4 v3, 0x0

    const/4 v4, 0x1

    instance-of v0, p1, Lcom/android/settings/deviceinfo/StorageItemPreference;

    if-eqz v0, :cond_0

    move-object v0, p1

    check-cast v0, Lcom/android/settings/deviceinfo/StorageItemPreference;

    iget v0, v0, Lcom/android/settings/deviceinfo/StorageItemPreference;->aKb:I

    :cond_0
    invoke-virtual {p1}, Landroid/preference/Preference;->getTitleRes()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    :goto_0
    :sswitch_0
    invoke-super {p0, p1}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->at(Landroid/preference/Preference;)Z

    move-result v0

    return v0

    :sswitch_1
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    const-string/jumbo v0, "classname"

    const-class v1, Lcom/android/settings/Settings$StorageUseActivity;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string/jumbo v0, "volumeUuid"

    iget-object v1, p0, Lcom/android/settings/deviceinfo/PrivateVolumeSettings;->aKC:Landroid/os/storage/VolumeInfo;

    invoke-virtual {v1}, Landroid/os/storage/VolumeInfo;->getFsUuid()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string/jumbo v0, "volumeName"

    iget-object v1, p0, Lcom/android/settings/deviceinfo/PrivateVolumeSettings;->aKC:Landroid/os/storage/VolumeInfo;

    invoke-virtual {v1}, Landroid/os/storage/VolumeInfo;->getDescription()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/android/settings/deviceinfo/PrivateVolumeSettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const-class v1, Lcom/android/settings/applications/ManageApplications;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/android/settings/deviceinfo/PrivateVolumeSettings;->getMetricsCategory()I

    move-result v7

    const v4, 0x7f120174

    const/4 v6, 0x0

    move-object v5, v3

    invoke-static/range {v0 .. v7}, Lcom/android/settings/aq;->bqs(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;Ljava/lang/String;ILjava/lang/CharSequence;ZI)Landroid/content/Intent;

    goto :goto_0

    :sswitch_2
    const-string/jumbo v0, "com.android.providers.media.documents"

    const-string/jumbo v1, "images_root"

    invoke-direct {p0, v0, v1}, Lcom/android/settings/deviceinfo/PrivateVolumeSettings;->axn(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_0

    :sswitch_3
    const-string/jumbo v0, "com.android.providers.media.documents"

    const-string/jumbo v1, "videos_root"

    invoke-direct {p0, v0, v1}, Lcom/android/settings/deviceinfo/PrivateVolumeSettings;->axn(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_0

    :sswitch_4
    const-string/jumbo v0, "com.android.providers.media.documents"

    const-string/jumbo v1, "audio_root"

    invoke-direct {p0, v0, v1}, Lcom/android/settings/deviceinfo/PrivateVolumeSettings;->axn(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_0

    :sswitch_5
    invoke-static {p0}, Lcom/android/settings/deviceinfo/PrivateVolumeSettings$SystemInfoFragment;->axI(Landroid/app/Fragment;)V

    return v4

    :sswitch_6
    return v4

    :sswitch_7
    invoke-static {p0}, Lcom/android/settings/deviceinfo/PrivateVolumeSettings$ConfirmClearCacheFragment;->axJ(Landroid/app/Fragment;)V

    return v4

    :sswitch_8
    iget-object v0, p0, Lcom/android/settings/deviceinfo/PrivateVolumeSettings;->aKt:Landroid/os/storage/VolumeInfo;

    invoke-virtual {v0}, Landroid/os/storage/VolumeInfo;->buildBrowseIntent()Landroid/content/Intent;

    goto :goto_0

    :sswitch_9
    invoke-virtual {p1}, Landroid/preference/Preference;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0}, Lcom/android/settings/deviceinfo/PrivateVolumeSettings;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->isInMultiWindowMode()Z

    move-result v1

    if-eqz v1, :cond_1

    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    :cond_1
    iget-object v1, p0, Lcom/android/settings/deviceinfo/PrivateVolumeSettings;->aKB:Lcom/android/settings/dc;

    invoke-virtual {p0}, Lcom/android/settings/deviceinfo/PrivateVolumeSettings;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Lcom/android/settings/dc;->bYt(Landroid/content/Context;Landroid/content/Intent;)Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {p0, v0}, Lcom/android/settings/deviceinfo/PrivateVolumeSettings;->startActivity(Landroid/content/Intent;)V

    :cond_2
    return v4

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x7f12040b -> :sswitch_9
        0x7f120a62 -> :sswitch_7
        0x7f12116d -> :sswitch_1
        0x7f12116e -> :sswitch_4
        0x7f121174 -> :sswitch_2
        0x7f121175 -> :sswitch_6
        0x7f121176 -> :sswitch_5
        0x7f121177 -> :sswitch_3
        0x7f12118d -> :sswitch_8
    .end sparse-switch
.end method

.method public getMetricsCategory()I
    .locals 1

    const/16 v0, 0x2a

    return v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 9

    const/4 v8, 0x1

    const-wide/16 v6, 0x0

    invoke-super {p0, p1}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/android/settings/deviceinfo/PrivateVolumeSettings;->getActivity()Landroid/app/Activity;

    move-result-object v1

    const-class v0, Landroid/os/UserManager;

    invoke-virtual {v1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/UserManager;

    iput-object v0, p0, Lcom/android/settings/deviceinfo/PrivateVolumeSettings;->mUserManager:Landroid/os/UserManager;

    const-class v0, Landroid/os/storage/StorageManager;

    invoke-virtual {v1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/storage/StorageManager;

    iput-object v0, p0, Lcom/android/settings/deviceinfo/PrivateVolumeSettings;->aKv:Landroid/os/storage/StorageManager;

    invoke-virtual {p0}, Lcom/android/settings/deviceinfo/PrivateVolumeSettings;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string/jumbo v2, "android.os.storage.extra.VOLUME_ID"

    const-string/jumbo v3, "private"

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/deviceinfo/PrivateVolumeSettings;->aKD:Ljava/lang/String;

    iget-object v0, p0, Lcom/android/settings/deviceinfo/PrivateVolumeSettings;->aKv:Landroid/os/storage/StorageManager;

    iget-object v2, p0, Lcom/android/settings/deviceinfo/PrivateVolumeSettings;->aKD:Ljava/lang/String;

    invoke-virtual {v0, v2}, Landroid/os/storage/StorageManager;->findVolumeById(Ljava/lang/String;)Landroid/os/storage/VolumeInfo;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/deviceinfo/PrivateVolumeSettings;->aKC:Landroid/os/storage/VolumeInfo;

    iget-object v0, p0, Lcom/android/settings/deviceinfo/PrivateVolumeSettings;->aKC:Landroid/os/storage/VolumeInfo;

    invoke-virtual {v0}, Landroid/os/storage/VolumeInfo;->getPath()Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->getTotalSpace()J

    move-result-wide v2

    invoke-virtual {p0}, Lcom/android/settings/deviceinfo/PrivateVolumeSettings;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string/jumbo v4, "volume_size"

    invoke-virtual {v0, v4, v6, v7}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v4

    iput-wide v4, p0, Lcom/android/settings/deviceinfo/PrivateVolumeSettings;->aKz:J

    iget-wide v4, p0, Lcom/android/settings/deviceinfo/PrivateVolumeSettings;->aKz:J

    sub-long/2addr v4, v2

    iput-wide v4, p0, Lcom/android/settings/deviceinfo/PrivateVolumeSettings;->aKx:J

    iget-wide v4, p0, Lcom/android/settings/deviceinfo/PrivateVolumeSettings;->aKz:J

    cmp-long v0, v4, v6

    if-gtz v0, :cond_0

    iput-wide v2, p0, Lcom/android/settings/deviceinfo/PrivateVolumeSettings;->aKz:J

    iput-wide v6, p0, Lcom/android/settings/deviceinfo/PrivateVolumeSettings;->aKx:J

    :cond_0
    iget-object v0, p0, Lcom/android/settings/deviceinfo/PrivateVolumeSettings;->aKv:Landroid/os/storage/StorageManager;

    iget-object v2, p0, Lcom/android/settings/deviceinfo/PrivateVolumeSettings;->aKC:Landroid/os/storage/VolumeInfo;

    invoke-virtual {v0, v2}, Landroid/os/storage/StorageManager;->findEmulatedForPrivate(Landroid/os/storage/VolumeInfo;)Landroid/os/storage/VolumeInfo;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/deviceinfo/PrivateVolumeSettings;->aKt:Landroid/os/storage/VolumeInfo;

    new-instance v0, Lcom/android/settingslib/f/c;

    iget-object v2, p0, Lcom/android/settings/deviceinfo/PrivateVolumeSettings;->aKC:Landroid/os/storage/VolumeInfo;

    iget-object v3, p0, Lcom/android/settings/deviceinfo/PrivateVolumeSettings;->aKt:Landroid/os/storage/VolumeInfo;

    invoke-direct {v0, v1, v2, v3}, Lcom/android/settingslib/f/c;-><init>(Landroid/content/Context;Landroid/os/storage/VolumeInfo;Landroid/os/storage/VolumeInfo;)V

    iput-object v0, p0, Lcom/android/settings/deviceinfo/PrivateVolumeSettings;->aKq:Lcom/android/settingslib/f/c;

    iget-object v0, p0, Lcom/android/settings/deviceinfo/PrivateVolumeSettings;->aKq:Lcom/android/settingslib/f/c;

    iget-object v2, p0, Lcom/android/settings/deviceinfo/PrivateVolumeSettings;->aKs:Lcom/android/settingslib/f/e;

    invoke-virtual {v0, v2}, Lcom/android/settingslib/f/c;->cou(Lcom/android/settingslib/f/e;)V

    invoke-direct {p0}, Lcom/android/settings/deviceinfo/PrivateVolumeSettings;->axq()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lcom/android/settings/deviceinfo/PrivateVolumeSettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    return-void

    :cond_1
    const v0, 0x7f150044

    invoke-virtual {p0, v0}, Lcom/android/settings/deviceinfo/PrivateVolumeSettings;->addPreferencesFromResource(I)V

    invoke-virtual {p0}, Lcom/android/settings/deviceinfo/PrivateVolumeSettings;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    invoke-virtual {v0, v8}, Landroid/preference/PreferenceScreen;->setOrderingAsAdded(Z)V

    const-string/jumbo v0, "storage_internal"

    invoke-virtual {p0, v0}, Lcom/android/settings/deviceinfo/PrivateVolumeSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceCategory;

    iput-object v0, p0, Lcom/android/settings/deviceinfo/PrivateVolumeSettings;->aKm:Landroid/preference/PreferenceCategory;

    const-string/jumbo v0, "system_rom"

    invoke-virtual {p0, v0}, Lcom/android/settings/deviceinfo/PrivateVolumeSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceCategory;

    iput-object v0, p0, Lcom/android/settings/deviceinfo/PrivateVolumeSettings;->aKw:Landroid/preference/PreferenceCategory;

    invoke-static {}, Lcom/android/settings/dc;->getInstance()Lcom/android/settings/dc;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/deviceinfo/PrivateVolumeSettings;->aKB:Lcom/android/settings/dc;

    new-instance v0, Lcom/android/settings/deviceinfo/UsageBarPreference;

    invoke-direct {v0, v1}, Lcom/android/settings/deviceinfo/UsageBarPreference;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/settings/deviceinfo/PrivateVolumeSettings;->aKA:Lcom/android/settings/deviceinfo/UsageBarPreference;

    iget-object v0, p0, Lcom/android/settings/deviceinfo/PrivateVolumeSettings;->mUserManager:Landroid/os/UserManager;

    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/os/UserManager;->getUserInfo(I)Landroid/content/pm/UserInfo;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/deviceinfo/PrivateVolumeSettings;->aKh:Landroid/content/pm/UserInfo;

    const v0, 0x7f12118d

    invoke-direct {p0, v0}, Lcom/android/settings/deviceinfo/PrivateVolumeSettings;->axl(I)Landroid/preference/Preference;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/deviceinfo/PrivateVolumeSettings;->aKi:Landroid/preference/Preference;

    iput-boolean v8, p0, Lcom/android/settings/deviceinfo/PrivateVolumeSettings;->aKr:Z

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/android/settings/deviceinfo/PrivateVolumeSettings;->setHasOptionsMenu(Z)V

    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V
    .locals 1

    invoke-super {p0, p1, p2}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V

    const v0, 0x7f0e0007

    invoke-virtual {p2, v0, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    return-void
.end method

.method public onDestroy()V
    .locals 1

    invoke-super {p0}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->onDestroy()V

    iget-object v0, p0, Lcom/android/settings/deviceinfo/PrivateVolumeSettings;->aKq:Lcom/android/settingslib/f/c;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/deviceinfo/PrivateVolumeSettings;->aKq:Lcom/android/settingslib/f/c;

    invoke-virtual {v0}, Lcom/android/settingslib/f/c;->onDestroy()V

    :cond_0
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 7

    const/4 v4, 0x0

    const/4 v6, 0x1

    invoke-virtual {p0}, Lcom/android/settings/deviceinfo/PrivateVolumeSettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    new-instance v5, Landroid/os/Bundle;

    invoke-direct {v5}, Landroid/os/Bundle;-><init>()V

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    :pswitch_0
    invoke-super {p0, p1}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    return v0

    :pswitch_1
    iget-object v0, p0, Lcom/android/settings/deviceinfo/PrivateVolumeSettings;->aKC:Landroid/os/storage/VolumeInfo;

    invoke-static {p0, v0}, Lcom/android/settings/deviceinfo/PrivateVolumeSettings$RenameFragment;->axH(Lcom/android/settings/deviceinfo/PrivateVolumeSettings;Landroid/os/storage/VolumeInfo;)V

    return v6

    :pswitch_2
    new-instance v1, Lcom/android/settings/deviceinfo/StorageSettings$MountTask;

    iget-object v2, p0, Lcom/android/settings/deviceinfo/PrivateVolumeSettings;->aKC:Landroid/os/storage/VolumeInfo;

    invoke-direct {v1, v0, v2}, Lcom/android/settings/deviceinfo/StorageSettings$MountTask;-><init>(Landroid/content/Context;Landroid/os/storage/VolumeInfo;)V

    new-array v0, v4, [Ljava/lang/Void;

    invoke-virtual {v1, v0}, Lcom/android/settings/deviceinfo/StorageSettings$MountTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    return v6

    :pswitch_3
    const-string/jumbo v0, "android.os.storage.extra.VOLUME_ID"

    iget-object v1, p0, Lcom/android/settings/deviceinfo/PrivateVolumeSettings;->aKC:Landroid/os/storage/VolumeInfo;

    invoke-virtual {v1}, Landroid/os/storage/VolumeInfo;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-class v0, Lcom/android/settings/deviceinfo/PrivateVolumeUnmount;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v2

    const v3, 0x7f121198

    move-object v0, p0

    move-object v1, p0

    invoke-virtual/range {v0 .. v5}, Lcom/android/settings/deviceinfo/PrivateVolumeSettings;->bWM(Landroid/app/Fragment;Ljava/lang/String;IILandroid/os/Bundle;)Z

    return v6

    :pswitch_4
    const-string/jumbo v0, "android.os.storage.extra.VOLUME_ID"

    iget-object v1, p0, Lcom/android/settings/deviceinfo/PrivateVolumeSettings;->aKC:Landroid/os/storage/VolumeInfo;

    invoke-virtual {v1}, Landroid/os/storage/VolumeInfo;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-class v0, Lcom/android/settings/deviceinfo/PrivateVolumeFormat;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v2

    const v3, 0x7f12118f

    move-object v0, p0

    move-object v1, p0

    invoke-virtual/range {v0 .. v5}, Lcom/android/settings/deviceinfo/PrivateVolumeSettings;->bWM(Landroid/app/Fragment;Ljava/lang/String;IILandroid/os/Bundle;)Z

    return v6

    :pswitch_5
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/android/settings/deviceinfo/StorageWizardMigrateConfirm;

    invoke-direct {v1, v0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v0, "android.os.storage.extra.VOLUME_ID"

    iget-object v2, p0, Lcom/android/settings/deviceinfo/PrivateVolumeSettings;->aKC:Landroid/os/storage/VolumeInfo;

    invoke-virtual {v2}, Landroid/os/storage/VolumeInfo;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p0, v1}, Lcom/android/settings/deviceinfo/PrivateVolumeSettings;->startActivity(Landroid/content/Intent;)V

    return v6

    :pswitch_6
    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v1, "android.os.storage.action.MANAGE_STORAGE"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/android/settings/deviceinfo/PrivateVolumeSettings;->startActivity(Landroid/content/Intent;)V

    return v6

    nop

    :pswitch_data_0
    .packed-switch 0x7f0a0430
        :pswitch_4
        :pswitch_6
        :pswitch_5
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method public onPause()V
    .locals 2

    invoke-super {p0}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->onPause()V

    iget-object v0, p0, Lcom/android/settings/deviceinfo/PrivateVolumeSettings;->aKv:Landroid/os/storage/StorageManager;

    iget-object v1, p0, Lcom/android/settings/deviceinfo/PrivateVolumeSettings;->aKu:Landroid/os/storage/StorageEventListener;

    invoke-virtual {v0, v1}, Landroid/os/storage/StorageManager;->unregisterListener(Landroid/os/storage/StorageEventListener;)V

    invoke-virtual {p0}, Lcom/android/settings/deviceinfo/PrivateVolumeSettings;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/support/v4/content/b;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/b;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/deviceinfo/PrivateVolumeSettings;->aKj:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/support/v4/content/b;->eai(Landroid/content/BroadcastReceiver;)V

    invoke-virtual {p0}, Lcom/android/settings/deviceinfo/PrivateVolumeSettings;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/support/v4/content/b;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/b;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/deviceinfo/PrivateVolumeSettings;->aKy:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/support/v4/content/b;->eai(Landroid/content/BroadcastReceiver;)V

    return-void
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)V
    .locals 10

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-direct {p0}, Lcom/android/settings/deviceinfo/PrivateVolumeSettings;->axq()Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    const v0, 0x7f0a0434

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v3

    const v0, 0x7f0a0433

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v4

    const v0, 0x7f0a0436

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v5

    const v0, 0x7f0a0430

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v6

    const v0, 0x7f0a0432

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v7

    const v0, 0x7f0a0431

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v8

    const-string/jumbo v0, "private"

    iget-object v9, p0, Lcom/android/settings/deviceinfo/PrivateVolumeSettings;->aKC:Landroid/os/storage/VolumeInfo;

    invoke-virtual {v9}, Landroid/os/storage/VolumeInfo;->getId()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v0, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    invoke-interface {v4, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    invoke-interface {v5, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    invoke-interface {v6, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    invoke-virtual {p0}, Lcom/android/settings/deviceinfo/PrivateVolumeSettings;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v3, 0x7f050015

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    invoke-interface {v8, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    :goto_0
    const v0, 0x7f121191

    invoke-interface {v6, v0}, Landroid/view/MenuItem;->setTitle(I)Landroid/view/MenuItem;

    invoke-virtual {p0}, Lcom/android/settings/deviceinfo/PrivateVolumeSettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/pm/PackageManager;->getPrimaryStorageCurrentVolume()Landroid/os/storage/VolumeInfo;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/os/storage/VolumeInfo;->getType()I

    move-result v3

    if-ne v3, v1, :cond_1

    iget-object v1, p0, Lcom/android/settings/deviceinfo/PrivateVolumeSettings;->aKC:Landroid/os/storage/VolumeInfo;

    invoke-static {v1, v0}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    xor-int/lit8 v2, v0, 0x1

    :cond_1
    invoke-interface {v7, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    return-void

    :cond_2
    iget-object v0, p0, Lcom/android/settings/deviceinfo/PrivateVolumeSettings;->aKC:Landroid/os/storage/VolumeInfo;

    invoke-virtual {v0}, Landroid/os/storage/VolumeInfo;->getType()I

    move-result v0

    if-ne v0, v1, :cond_3

    move v0, v1

    :goto_1
    invoke-interface {v3, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    iget-object v0, p0, Lcom/android/settings/deviceinfo/PrivateVolumeSettings;->aKC:Landroid/os/storage/VolumeInfo;

    invoke-virtual {v0}, Landroid/os/storage/VolumeInfo;->getState()I

    move-result v0

    if-nez v0, :cond_4

    move v0, v1

    :goto_2
    invoke-interface {v4, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    iget-object v0, p0, Lcom/android/settings/deviceinfo/PrivateVolumeSettings;->aKC:Landroid/os/storage/VolumeInfo;

    invoke-virtual {v0}, Landroid/os/storage/VolumeInfo;->isMountedReadable()Z

    move-result v0

    invoke-interface {v5, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    invoke-interface {v6, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    invoke-interface {v8, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto :goto_0

    :cond_3
    move v0, v2

    goto :goto_1

    :cond_4
    move v0, v2

    goto :goto_2
.end method

.method public onResume()V
    .locals 4

    invoke-super {p0}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->onResume()V

    iget-object v0, p0, Lcom/android/settings/deviceinfo/PrivateVolumeSettings;->aKv:Landroid/os/storage/StorageManager;

    iget-object v1, p0, Lcom/android/settings/deviceinfo/PrivateVolumeSettings;->aKD:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/os/storage/StorageManager;->findVolumeById(Ljava/lang/String;)Landroid/os/storage/VolumeInfo;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/deviceinfo/PrivateVolumeSettings;->aKC:Landroid/os/storage/VolumeInfo;

    invoke-direct {p0}, Lcom/android/settings/deviceinfo/PrivateVolumeSettings;->axq()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/android/settings/deviceinfo/PrivateVolumeSettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/settings/deviceinfo/PrivateVolumeSettings;->aKv:Landroid/os/storage/StorageManager;

    iget-object v1, p0, Lcom/android/settings/deviceinfo/PrivateVolumeSettings;->aKu:Landroid/os/storage/StorageEventListener;

    invoke-virtual {v0, v1}, Landroid/os/storage/StorageManager;->registerListener(Landroid/os/storage/StorageEventListener;)V

    invoke-virtual {p0}, Lcom/android/settings/deviceinfo/PrivateVolumeSettings;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/support/v4/content/b;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/b;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/deviceinfo/PrivateVolumeSettings;->aKj:Landroid/content/BroadcastReceiver;

    new-instance v2, Landroid/content/IntentFilter;

    const-string/jumbo v3, "action_format_done"

    invoke-direct {v2, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/content/b;->eak(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)V

    invoke-virtual {p0}, Lcom/android/settings/deviceinfo/PrivateVolumeSettings;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/support/v4/content/b;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/b;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/deviceinfo/PrivateVolumeSettings;->aKy:Landroid/content/BroadcastReceiver;

    new-instance v2, Landroid/content/IntentFilter;

    const-string/jumbo v3, "action_top_ui"

    invoke-direct {v2, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/content/b;->eak(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)V

    iget-boolean v0, p0, Lcom/android/settings/deviceinfo/PrivateVolumeSettings;->aKr:Z

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lcom/android/settings/deviceinfo/PrivateVolumeSettings;->axu()V

    :goto_0
    return-void

    :cond_1
    invoke-direct {p0}, Lcom/android/settings/deviceinfo/PrivateVolumeSettings;->axs()V

    goto :goto_0
.end method
