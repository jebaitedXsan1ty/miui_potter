.class public Lcom/android/settings/deviceinfo/UsbModeChooserActivity$CustomListViewAdapter;
.super Landroid/widget/ArrayAdapter;
.source "UsbModeChooserActivity.java"


# instance fields
.field private aHu:I

.field final synthetic aHv:Lcom/android/settings/deviceinfo/UsbModeChooserActivity;


# direct methods
.method public constructor <init>(Lcom/android/settings/deviceinfo/UsbModeChooserActivity;Landroid/content/Context;ILjava/util/List;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/deviceinfo/UsbModeChooserActivity$CustomListViewAdapter;->aHv:Lcom/android/settings/deviceinfo/UsbModeChooserActivity;

    invoke-direct {p0, p2, p3, p4}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    iput p3, p0, Lcom/android/settings/deviceinfo/UsbModeChooserActivity$CustomListViewAdapter;->aHu:I

    return-void
.end method


# virtual methods
.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 7

    const/4 v2, 0x1

    const/4 v4, 0x0

    const/4 v3, 0x0

    if-nez p2, :cond_0

    iget-object v0, p0, Lcom/android/settings/deviceinfo/UsbModeChooserActivity$CustomListViewAdapter;->aHv:Lcom/android/settings/deviceinfo/UsbModeChooserActivity;

    invoke-static {v0}, Lcom/android/settings/deviceinfo/UsbModeChooserActivity;->auI(Lcom/android/settings/deviceinfo/UsbModeChooserActivity;)Landroid/view/LayoutInflater;

    move-result-object v0

    iget v1, p0, Lcom/android/settings/deviceinfo/UsbModeChooserActivity$CustomListViewAdapter;->aHu:I

    invoke-virtual {v0, v1, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    :cond_0
    invoke-virtual {p0, p1}, Lcom/android/settings/deviceinfo/UsbModeChooserActivity$CustomListViewAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v4

    iget-object v0, p0, Lcom/android/settings/deviceinfo/UsbModeChooserActivity$CustomListViewAdapter;->aHv:Lcom/android/settings/deviceinfo/UsbModeChooserActivity;

    invoke-static {v0}, Lcom/android/settings/deviceinfo/UsbModeChooserActivity;->auJ(Lcom/android/settings/deviceinfo/UsbModeChooserActivity;)Ljava/util/HashMap;

    move-result-object v0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v0, v1, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/android/settings/deviceinfo/UsbModeChooserActivity$CustomListViewAdapter;->aHv:Lcom/android/settings/deviceinfo/UsbModeChooserActivity;

    invoke-static {v0}, Lcom/android/settings/deviceinfo/UsbModeChooserActivity;->auE(Lcom/android/settings/deviceinfo/UsbModeChooserActivity;)Lcom/android/settings/deviceinfo/UsbBackend;

    move-result-object v0

    invoke-virtual {v0, v4}, Lcom/android/settings/deviceinfo/UsbBackend;->auY(I)Z

    move-result v5

    const v0, 0x7f0a04d8

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckedTextView;

    const v1, 0x7f0a038d

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    invoke-static {v4}, Lcom/android/settings/deviceinfo/UsbModeChooserActivity;->getTitle(I)I

    move-result v6

    invoke-virtual {v0, v6}, Landroid/widget/CheckedTextView;->setText(I)V

    if-eqz v5, :cond_1

    iget-object v5, p0, Lcom/android/settings/deviceinfo/UsbModeChooserActivity$CustomListViewAdapter;->aHv:Lcom/android/settings/deviceinfo/UsbModeChooserActivity;

    invoke-static {v5}, Lcom/android/settings/deviceinfo/UsbModeChooserActivity;->auH(Lcom/android/settings/deviceinfo/UsbModeChooserActivity;)Lcom/android/settingslib/n;

    move-result-object v5

    if-eqz v5, :cond_1

    invoke-virtual {v0, v3}, Landroid/widget/CheckedTextView;->setEnabled(Z)V

    invoke-virtual {v0, v3}, Landroid/widget/CheckedTextView;->setChecked(Z)V

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    :goto_0
    return-object p2

    :cond_1
    invoke-virtual {v0}, Landroid/widget/CheckedTextView;->isEnabled()Z

    move-result v5

    if-nez v5, :cond_2

    invoke-virtual {v0, v2}, Landroid/widget/CheckedTextView;->setEnabled(Z)V

    :cond_2
    iget-object v5, p0, Lcom/android/settings/deviceinfo/UsbModeChooserActivity$CustomListViewAdapter;->aHv:Lcom/android/settings/deviceinfo/UsbModeChooserActivity;

    invoke-static {v5}, Lcom/android/settings/deviceinfo/UsbModeChooserActivity;->auF(Lcom/android/settings/deviceinfo/UsbModeChooserActivity;)I

    move-result v5

    if-ne v4, v5, :cond_3

    :goto_1
    invoke-virtual {v0, v2}, Landroid/widget/CheckedTextView;->setChecked(Z)V

    const/16 v0, 0x8

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0

    :cond_3
    move v2, v3

    goto :goto_1
.end method
