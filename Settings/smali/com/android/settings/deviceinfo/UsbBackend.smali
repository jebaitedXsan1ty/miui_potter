.class public Lcom/android/settings/deviceinfo/UsbBackend;
.super Ljava/lang/Object;
.source "UsbBackend.java"


# instance fields
.field private aHI:Z

.field private final aHJ:Z

.field private aHK:Landroid/hardware/usb/UsbPort;

.field private aHL:Landroid/hardware/usb/UsbPortStatus;

.field private final aHM:Z

.field private final aHN:Z

.field private aHO:Landroid/hardware/usb/UsbManager;

.field private mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    new-instance v0, Lcom/android/settings/deviceinfo/UsbBackend$UserRestrictionUtil;

    invoke-direct {v0, p1}, Lcom/android/settings/deviceinfo/UsbBackend$UserRestrictionUtil;-><init>(Landroid/content/Context;)V

    invoke-direct {p0, p1, v0}, Lcom/android/settings/deviceinfo/UsbBackend;-><init>(Landroid/content/Context;Lcom/android/settings/deviceinfo/UsbBackend$UserRestrictionUtil;)V

    return-void
.end method

.method constructor <init>(Landroid/content/Context;Lcom/android/settings/deviceinfo/UsbBackend$UserRestrictionUtil;)V
    .locals 5

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string/jumbo v0, "persist.vendor.strict_op_enable"

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/settings/deviceinfo/UsbBackend;->aHI:Z

    iput-object p1, p0, Lcom/android/settings/deviceinfo/UsbBackend;->mContext:Landroid/content/Context;

    const-class v0, Landroid/hardware/usb/UsbManager;

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/usb/UsbManager;

    iput-object v0, p0, Lcom/android/settings/deviceinfo/UsbBackend;->aHO:Landroid/hardware/usb/UsbManager;

    invoke-virtual {p2}, Lcom/android/settings/deviceinfo/UsbBackend$UserRestrictionUtil;->avj()Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/settings/deviceinfo/UsbBackend;->aHM:Z

    invoke-virtual {p2}, Lcom/android/settings/deviceinfo/UsbBackend$UserRestrictionUtil;->avk()Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/settings/deviceinfo/UsbBackend;->aHN:Z

    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    const-string/jumbo v2, "android.software.midi"

    invoke-virtual {v0, v2}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/settings/deviceinfo/UsbBackend;->aHJ:Z

    iget-object v0, p0, Lcom/android/settings/deviceinfo/UsbBackend;->aHO:Landroid/hardware/usb/UsbManager;

    invoke-virtual {v0}, Landroid/hardware/usb/UsbManager;->getPorts()[Landroid/hardware/usb/UsbPort;

    move-result-object v2

    if-nez v2, :cond_0

    return-void

    :cond_0
    array-length v3, v2

    move v0, v1

    :goto_0
    if-ge v0, v3, :cond_1

    iget-object v1, p0, Lcom/android/settings/deviceinfo/UsbBackend;->aHO:Landroid/hardware/usb/UsbManager;

    aget-object v4, v2, v0

    invoke-virtual {v1, v4}, Landroid/hardware/usb/UsbManager;->getPortStatus(Landroid/hardware/usb/UsbPort;)Landroid/hardware/usb/UsbPortStatus;

    move-result-object v1

    invoke-virtual {v1}, Landroid/hardware/usb/UsbPortStatus;->isConnected()Z

    move-result v4

    if-eqz v4, :cond_2

    aget-object v0, v2, v0

    iput-object v0, p0, Lcom/android/settings/deviceinfo/UsbBackend;->aHK:Landroid/hardware/usb/UsbPort;

    iput-object v1, p0, Lcom/android/settings/deviceinfo/UsbBackend;->aHL:Landroid/hardware/usb/UsbPortStatus;

    :cond_1
    return-void

    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private avd()I
    .locals 3

    const/4 v0, 0x1

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/android/settings/deviceinfo/UsbBackend;->aHK:Landroid/hardware/usb/UsbPort;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/android/settings/deviceinfo/UsbBackend;->aHL:Landroid/hardware/usb/UsbPortStatus;

    invoke-virtual {v2}, Landroid/hardware/usb/UsbPortStatus;->getCurrentPowerRole()I

    move-result v2

    if-ne v2, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method private avf()Z
    .locals 2

    const/4 v0, 0x1

    invoke-direct {p0}, Lcom/android/settings/deviceinfo/UsbBackend;->avd()I

    move-result v1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private avg()Z
    .locals 5

    const/4 v4, 0x0

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/android/settings/deviceinfo/UsbBackend;->mContext:Landroid/content/Context;

    new-instance v2, Landroid/content/IntentFilter;

    const-string/jumbo v3, "android.hardware.usb.action.USB_STATE"

    invoke-direct {v2, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v4, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    move-result-object v1

    if-nez v1, :cond_0

    :goto_0
    return v0

    :cond_0
    const-string/jumbo v2, "unlocked"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    goto :goto_0
.end method

.method private avh(I)I
    .locals 2

    const/4 v0, 0x1

    and-int/lit8 v1, p1, 0x1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x2

    goto :goto_0
.end method

.method private avi(I)V
    .locals 4

    const/4 v3, 0x0

    const/4 v2, 0x1

    packed-switch p1, :pswitch_data_0

    :cond_0
    :pswitch_0
    iget-object v0, p0, Lcom/android/settings/deviceinfo/UsbBackend;->mContext:Landroid/content/Context;

    const/4 v1, 0x0

    invoke-static {v0, v1, v3}, Lcom/android/settings/dc;->bYF(Landroid/content/Context;Ljava/lang/String;Z)V

    :goto_0
    return-void

    :pswitch_1
    iget-object v0, p0, Lcom/android/settings/deviceinfo/UsbBackend;->mContext:Landroid/content/Context;

    const-string/jumbo v1, "mtp"

    invoke-static {v0, v1, v2}, Lcom/android/settings/dc;->bYF(Landroid/content/Context;Ljava/lang/String;Z)V

    goto :goto_0

    :pswitch_2
    iget-object v0, p0, Lcom/android/settings/deviceinfo/UsbBackend;->mContext:Landroid/content/Context;

    const-string/jumbo v1, "ptp"

    invoke-static {v0, v1, v2}, Lcom/android/settings/dc;->bYF(Landroid/content/Context;Ljava/lang/String;Z)V

    goto :goto_0

    :pswitch_3
    iget-object v0, p0, Lcom/android/settings/deviceinfo/UsbBackend;->mContext:Landroid/content/Context;

    const-string/jumbo v1, "midi"

    invoke-static {v0, v1, v2}, Lcom/android/settings/dc;->bYF(Landroid/content/Context;Ljava/lang/String;Z)V

    goto :goto_0

    :pswitch_4
    iget-boolean v0, p0, Lcom/android/settings/deviceinfo/UsbBackend;->aHI:Z

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/android/settings/deviceinfo/UsbBackend;->avf()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/deviceinfo/UsbBackend;->aHO:Landroid/hardware/usb/UsbManager;

    const-string/jumbo v1, "mtp"

    invoke-virtual {v0, v1, v3}, Landroid/hardware/usb/UsbManager;->setCurrentFunction(Ljava/lang/String;Z)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_4
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method


# virtual methods
.method public auY(I)Z
    .locals 3

    const/4 v2, 0x0

    iget-boolean v0, p0, Lcom/android/settings/deviceinfo/UsbBackend;->aHM:Z

    if-eqz v0, :cond_0

    and-int/lit8 v0, p1, 0x6

    if-eqz v0, :cond_0

    and-int/lit8 v0, p1, 0x6

    const/4 v1, 0x6

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    return v0

    :cond_0
    return v2
.end method

.method public auZ()I
    .locals 3

    const/4 v0, 0x1

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/android/settings/deviceinfo/UsbBackend;->aHK:Landroid/hardware/usb/UsbPort;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/android/settings/deviceinfo/UsbBackend;->aHL:Landroid/hardware/usb/UsbPortStatus;

    invoke-virtual {v2}, Landroid/hardware/usb/UsbPortStatus;->getCurrentPowerRole()I

    move-result v2

    if-ne v2, v0, :cond_0

    :goto_0
    invoke-virtual {p0}, Lcom/android/settings/deviceinfo/UsbBackend;->ave()I

    move-result v1

    or-int/2addr v0, v1

    return v0

    :cond_0
    move v0, v1

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lcom/android/settings/deviceinfo/UsbBackend;->ave()I

    move-result v0

    or-int/lit8 v0, v0, 0x0

    return v0
.end method

.method public ava(I)Z
    .locals 5

    const/4 v4, 0x2

    const/4 v1, 0x0

    const/4 v0, 0x1

    iget-boolean v2, p0, Lcom/android/settings/deviceinfo/UsbBackend;->aHJ:Z

    if-nez v2, :cond_0

    and-int/lit8 v2, p1, 0x6

    const/4 v3, 0x6

    if-ne v2, v3, :cond_0

    return v1

    :cond_0
    iget-object v2, p0, Lcom/android/settings/deviceinfo/UsbBackend;->aHK:Landroid/hardware/usb/UsbPort;

    if-eqz v2, :cond_3

    invoke-direct {p0, p1}, Lcom/android/settings/deviceinfo/UsbBackend;->avh(I)I

    move-result v1

    and-int/lit8 v2, p1, 0x6

    if-eqz v2, :cond_1

    iget-object v0, p0, Lcom/android/settings/deviceinfo/UsbBackend;->aHL:Landroid/hardware/usb/UsbPortStatus;

    invoke-virtual {v0, v1, v4}, Landroid/hardware/usb/UsbPortStatus;->isRoleCombinationSupported(II)Z

    move-result v0

    return v0

    :cond_1
    iget-object v2, p0, Lcom/android/settings/deviceinfo/UsbBackend;->aHL:Landroid/hardware/usb/UsbPortStatus;

    invoke-virtual {v2, v1, v4}, Landroid/hardware/usb/UsbPortStatus;->isRoleCombinationSupported(II)Z

    move-result v2

    if-nez v2, :cond_2

    iget-object v2, p0, Lcom/android/settings/deviceinfo/UsbBackend;->aHL:Landroid/hardware/usb/UsbPortStatus;

    invoke-virtual {v2, v1, v0}, Landroid/hardware/usb/UsbPortStatus;->isRoleCombinationSupported(II)Z

    move-result v0

    :cond_2
    return v0

    :cond_3
    and-int/lit8 v2, p1, 0x1

    if-eq v2, v0, :cond_4

    :goto_0
    return v0

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method public avb(I)Z
    .locals 3

    const/4 v2, 0x0

    iget-boolean v0, p0, Lcom/android/settings/deviceinfo/UsbBackend;->aHN:Z

    if-eqz v0, :cond_0

    and-int/lit8 v0, p1, 0x6

    if-eqz v0, :cond_0

    and-int/lit8 v0, p1, 0x6

    const/4 v1, 0x6

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    return v0

    :cond_0
    return v2
.end method

.method public avc(I)V
    .locals 4

    const/4 v0, 0x1

    iget-object v1, p0, Lcom/android/settings/deviceinfo/UsbBackend;->aHK:Landroid/hardware/usb/UsbPort;

    if-eqz v1, :cond_0

    invoke-direct {p0, p1}, Lcom/android/settings/deviceinfo/UsbBackend;->avh(I)I

    move-result v1

    and-int/lit8 v2, p1, 0x6

    if-nez v2, :cond_1

    iget-object v2, p0, Lcom/android/settings/deviceinfo/UsbBackend;->aHL:Landroid/hardware/usb/UsbPortStatus;

    invoke-virtual {v2, v1, v0}, Landroid/hardware/usb/UsbPortStatus;->isRoleCombinationSupported(II)Z

    move-result v2

    if-eqz v2, :cond_1

    :goto_0
    iget-object v2, p0, Lcom/android/settings/deviceinfo/UsbBackend;->aHO:Landroid/hardware/usb/UsbManager;

    iget-object v3, p0, Lcom/android/settings/deviceinfo/UsbBackend;->aHK:Landroid/hardware/usb/UsbPort;

    invoke-virtual {v2, v3, v1, v0}, Landroid/hardware/usb/UsbManager;->setPortRoles(Landroid/hardware/usb/UsbPort;II)V

    :cond_0
    and-int/lit8 v0, p1, 0x6

    invoke-direct {p0, v0}, Lcom/android/settings/deviceinfo/UsbBackend;->avi(I)V

    return-void

    :cond_1
    const/4 v0, 0x2

    goto :goto_0
.end method

.method public ave()I
    .locals 3

    const/4 v0, 0x0

    invoke-direct {p0}, Lcom/android/settings/deviceinfo/UsbBackend;->avg()Z

    move-result v1

    if-nez v1, :cond_0

    return v0

    :cond_0
    iget-object v1, p0, Lcom/android/settings/deviceinfo/UsbBackend;->aHO:Landroid/hardware/usb/UsbManager;

    const-string/jumbo v2, "mtp"

    invoke-virtual {v1, v2}, Landroid/hardware/usb/UsbManager;->isFunctionEnabled(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v0, 0x2

    return v0

    :cond_1
    iget-object v1, p0, Lcom/android/settings/deviceinfo/UsbBackend;->aHO:Landroid/hardware/usb/UsbManager;

    const-string/jumbo v2, "ptp"

    invoke-virtual {v1, v2}, Landroid/hardware/usb/UsbManager;->isFunctionEnabled(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v0, 0x4

    return v0

    :cond_2
    iget-object v1, p0, Lcom/android/settings/deviceinfo/UsbBackend;->aHO:Landroid/hardware/usb/UsbManager;

    const-string/jumbo v2, "midi"

    invoke-virtual {v1, v2}, Landroid/hardware/usb/UsbManager;->isFunctionEnabled(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    const/4 v0, 0x6

    return v0

    :cond_3
    iget-boolean v1, p0, Lcom/android/settings/deviceinfo/UsbBackend;->aHI:Z

    if-eqz v1, :cond_4

    const/4 v0, -0x1

    :cond_4
    return v0
.end method
