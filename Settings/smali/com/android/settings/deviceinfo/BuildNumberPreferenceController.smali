.class public Lcom/android/settings/deviceinfo/BuildNumberPreferenceController;
.super Lcom/android/settings/core/e;
.source "BuildNumberPreferenceController.java"

# interfaces
.implements Lcom/android/settings/core/lifecycle/b;
.implements Lcom/android/settings/core/lifecycle/a/b;


# instance fields
.field private final aIM:Landroid/app/Activity;

.field private aIN:Lcom/android/settingslib/n;

.field private aIO:Z

.field private aIP:I

.field private aIQ:Landroid/widget/Toast;

.field private final aIR:Landroid/app/Fragment;

.field private aIS:Z

.field private final aIT:Landroid/os/UserManager;

.field private final mMetricsFeatureProvider:Lcom/android/settings/core/instrumentation/e;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/app/Activity;Landroid/app/Fragment;Lcom/android/settings/core/lifecycle/c;)V
    .locals 1

    invoke-direct {p0, p1}, Lcom/android/settings/core/e;-><init>(Landroid/content/Context;)V

    iput-object p2, p0, Lcom/android/settings/deviceinfo/BuildNumberPreferenceController;->aIM:Landroid/app/Activity;

    iput-object p3, p0, Lcom/android/settings/deviceinfo/BuildNumberPreferenceController;->aIR:Landroid/app/Fragment;

    const-string/jumbo v0, "user"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/UserManager;

    iput-object v0, p0, Lcom/android/settings/deviceinfo/BuildNumberPreferenceController;->aIT:Landroid/os/UserManager;

    invoke-static {p1}, Lcom/android/settings/overlay/a;->aIk(Landroid/content/Context;)Lcom/android/settings/overlay/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/settings/overlay/a;->aIm()Lcom/android/settings/core/instrumentation/e;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/deviceinfo/BuildNumberPreferenceController;->mMetricsFeatureProvider:Lcom/android/settings/core/instrumentation/e;

    if-eqz p4, :cond_0

    invoke-virtual {p4, p0}, Lcom/android/settings/core/lifecycle/c;->ajv(Lcom/android/settings/core/lifecycle/b;)Lcom/android/settings/core/lifecycle/b;

    :cond_0
    return-void
.end method

.method private awh()V
    .locals 5

    const/4 v4, 0x1

    const/4 v3, 0x0

    iput v3, p0, Lcom/android/settings/deviceinfo/BuildNumberPreferenceController;->aIP:I

    iput-boolean v3, p0, Lcom/android/settings/deviceinfo/BuildNumberPreferenceController;->aIS:Z

    iget-object v0, p0, Lcom/android/settings/deviceinfo/BuildNumberPreferenceController;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/android/settings/deviceinfo/BuildNumberPreferenceController;->mContext:Landroid/content/Context;

    const-string/jumbo v2, "development"

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/settings/development/b;->aUr(Landroid/content/Context;Landroid/content/SharedPreferences;)Z

    iget-object v0, p0, Lcom/android/settings/deviceinfo/BuildNumberPreferenceController;->aIQ:Landroid/widget/Toast;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/deviceinfo/BuildNumberPreferenceController;->aIQ:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->cancel()V

    :cond_0
    iget-object v0, p0, Lcom/android/settings/deviceinfo/BuildNumberPreferenceController;->mContext:Landroid/content/Context;

    const v1, 0x7f121065

    invoke-static {v0, v1, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/deviceinfo/BuildNumberPreferenceController;->aIQ:Landroid/widget/Toast;

    iget-object v0, p0, Lcom/android/settings/deviceinfo/BuildNumberPreferenceController;->aIQ:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    iget-object v0, p0, Lcom/android/settings/deviceinfo/BuildNumberPreferenceController;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/settings/overlay/a;->aIk(Landroid/content/Context;)Lcom/android/settings/overlay/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/settings/overlay/a;->aIt()Lcom/android/settings/search2/SearchFeatureProvider;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/deviceinfo/BuildNumberPreferenceController;->mContext:Landroid/content/Context;

    invoke-interface {v0, v1}, Lcom/android/settings/search2/SearchFeatureProvider;->getIndexingManager(Landroid/content/Context;)Lcom/android/settings/search2/DatabaseIndexingManager;

    move-result-object v0

    const-class v1, Lcom/android/settings/development/DevelopmentSettings;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, v4}, Lcom/android/settings/search2/DatabaseIndexingManager;->updateFromClassNameResource(Ljava/lang/String;Z)V

    return-void
.end method


# virtual methods
.method public fm(Landroid/preference/Preference;)Z
    .locals 9

    const/16 v8, 0x350

    const/16 v7, 0x34f

    const/4 v2, 0x1

    const/4 v1, 0x0

    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v3, "build_number"

    invoke-static {v0, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    return v1

    :cond_0
    iget-object v0, p0, Lcom/android/settings/deviceinfo/BuildNumberPreferenceController;->aIT:Landroid/os/UserManager;

    invoke-virtual {v0}, Landroid/os/UserManager;->isAdminUser()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/android/settings/deviceinfo/BuildNumberPreferenceController;->mMetricsFeatureProvider:Lcom/android/settings/core/instrumentation/e;

    iget-object v2, p0, Lcom/android/settings/deviceinfo/BuildNumberPreferenceController;->mContext:Landroid/content/Context;

    new-array v3, v1, [Landroid/util/Pair;

    invoke-virtual {v0, v2, v7, v3}, Lcom/android/settings/core/instrumentation/e;->ajS(Landroid/content/Context;I[Landroid/util/Pair;)V

    return v1

    :cond_1
    iget-object v0, p0, Lcom/android/settings/deviceinfo/BuildNumberPreferenceController;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/settings/aq;->bqV(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/android/settings/deviceinfo/BuildNumberPreferenceController;->mMetricsFeatureProvider:Lcom/android/settings/core/instrumentation/e;

    iget-object v2, p0, Lcom/android/settings/deviceinfo/BuildNumberPreferenceController;->mContext:Landroid/content/Context;

    new-array v3, v1, [Landroid/util/Pair;

    invoke-virtual {v0, v2, v7, v3}, Lcom/android/settings/core/instrumentation/e;->ajS(Landroid/content/Context;I[Landroid/util/Pair;)V

    return v1

    :cond_2
    iget-object v0, p0, Lcom/android/settings/deviceinfo/BuildNumberPreferenceController;->aIT:Landroid/os/UserManager;

    const-string/jumbo v3, "no_debugging_features"

    invoke-virtual {v0, v3}, Landroid/os/UserManager;->hasUserRestriction(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/android/settings/deviceinfo/BuildNumberPreferenceController;->aIN:Lcom/android/settingslib/n;

    if-eqz v0, :cond_3

    iget-boolean v0, p0, Lcom/android/settings/deviceinfo/BuildNumberPreferenceController;->aIO:Z

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/android/settings/deviceinfo/BuildNumberPreferenceController;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/android/settings/deviceinfo/BuildNumberPreferenceController;->aIN:Lcom/android/settingslib/n;

    invoke-static {v0, v2}, Lcom/android/settingslib/w;->cqW(Landroid/content/Context;Lcom/android/settingslib/n;)V

    :cond_3
    iget-object v0, p0, Lcom/android/settings/deviceinfo/BuildNumberPreferenceController;->mMetricsFeatureProvider:Lcom/android/settings/core/instrumentation/e;

    iget-object v2, p0, Lcom/android/settings/deviceinfo/BuildNumberPreferenceController;->mContext:Landroid/content/Context;

    new-array v3, v1, [Landroid/util/Pair;

    invoke-virtual {v0, v2, v7, v3}, Lcom/android/settings/core/instrumentation/e;->ajS(Landroid/content/Context;I[Landroid/util/Pair;)V

    return v1

    :cond_4
    iget v0, p0, Lcom/android/settings/deviceinfo/BuildNumberPreferenceController;->aIP:I

    if-lez v0, :cond_b

    iget v0, p0, Lcom/android/settings/deviceinfo/BuildNumberPreferenceController;->aIP:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/android/settings/deviceinfo/BuildNumberPreferenceController;->aIP:I

    iget v0, p0, Lcom/android/settings/deviceinfo/BuildNumberPreferenceController;->aIP:I

    if-nez v0, :cond_9

    iget-boolean v0, p0, Lcom/android/settings/deviceinfo/BuildNumberPreferenceController;->aIS:Z

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_9

    iget v0, p0, Lcom/android/settings/deviceinfo/BuildNumberPreferenceController;->aIP:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/android/settings/deviceinfo/BuildNumberPreferenceController;->aIP:I

    new-instance v0, Lcom/android/settings/cx;

    iget-object v3, p0, Lcom/android/settings/deviceinfo/BuildNumberPreferenceController;->aIM:Landroid/app/Activity;

    iget-object v4, p0, Lcom/android/settings/deviceinfo/BuildNumberPreferenceController;->aIR:Landroid/app/Fragment;

    invoke-direct {v0, v3, v4}, Lcom/android/settings/cx;-><init>(Landroid/app/Activity;Landroid/app/Fragment;)V

    iget-object v3, p0, Lcom/android/settings/deviceinfo/BuildNumberPreferenceController;->mContext:Landroid/content/Context;

    const v4, 0x7f12130b

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/16 v4, 0x64

    invoke-virtual {v0, v4, v3}, Lcom/android/settings/cx;->bSO(ILjava/lang/CharSequence;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/settings/deviceinfo/BuildNumberPreferenceController;->aIS:Z

    iget-boolean v0, p0, Lcom/android/settings/deviceinfo/BuildNumberPreferenceController;->aIS:Z

    if-nez v0, :cond_5

    invoke-direct {p0}, Lcom/android/settings/deviceinfo/BuildNumberPreferenceController;->awh()V

    :cond_5
    iget-object v3, p0, Lcom/android/settings/deviceinfo/BuildNumberPreferenceController;->mMetricsFeatureProvider:Lcom/android/settings/core/instrumentation/e;

    iget-object v4, p0, Lcom/android/settings/deviceinfo/BuildNumberPreferenceController;->mContext:Landroid/content/Context;

    new-array v5, v2, [Landroid/util/Pair;

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    iget-boolean v0, p0, Lcom/android/settings/deviceinfo/BuildNumberPreferenceController;->aIS:Z

    if-eqz v0, :cond_8

    move v0, v1

    :goto_0
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {v6, v0}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    aput-object v0, v5, v1

    invoke-virtual {v3, v4, v7, v5}, Lcom/android/settings/core/instrumentation/e;->ajS(Landroid/content/Context;I[Landroid/util/Pair;)V

    :cond_6
    :goto_1
    iget-object v0, p0, Lcom/android/settings/deviceinfo/BuildNumberPreferenceController;->mMetricsFeatureProvider:Lcom/android/settings/core/instrumentation/e;

    iget-object v3, p0, Lcom/android/settings/deviceinfo/BuildNumberPreferenceController;->mContext:Landroid/content/Context;

    new-array v4, v2, [Landroid/util/Pair;

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v5

    aput-object v5, v4, v1

    invoke-virtual {v0, v3, v7, v4}, Lcom/android/settings/core/instrumentation/e;->ajS(Landroid/content/Context;I[Landroid/util/Pair;)V

    :cond_7
    :goto_2
    return v2

    :cond_8
    move v0, v2

    goto :goto_0

    :cond_9
    iget v0, p0, Lcom/android/settings/deviceinfo/BuildNumberPreferenceController;->aIP:I

    if-lez v0, :cond_6

    iget v0, p0, Lcom/android/settings/deviceinfo/BuildNumberPreferenceController;->aIP:I

    const/4 v3, 0x5

    if-ge v0, v3, :cond_6

    iget-object v0, p0, Lcom/android/settings/deviceinfo/BuildNumberPreferenceController;->aIQ:Landroid/widget/Toast;

    if-eqz v0, :cond_a

    iget-object v0, p0, Lcom/android/settings/deviceinfo/BuildNumberPreferenceController;->aIQ:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->cancel()V

    :cond_a
    iget-object v0, p0, Lcom/android/settings/deviceinfo/BuildNumberPreferenceController;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/android/settings/deviceinfo/BuildNumberPreferenceController;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    iget v4, p0, Lcom/android/settings/deviceinfo/BuildNumberPreferenceController;->aIP:I

    new-array v5, v2, [Ljava/lang/Object;

    iget v6, p0, Lcom/android/settings/deviceinfo/BuildNumberPreferenceController;->aIP:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v1

    const v6, 0x7f100038

    invoke-virtual {v3, v6, v4, v5}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/deviceinfo/BuildNumberPreferenceController;->aIQ:Landroid/widget/Toast;

    iget-object v0, p0, Lcom/android/settings/deviceinfo/BuildNumberPreferenceController;->aIQ:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_1

    :cond_b
    iget v0, p0, Lcom/android/settings/deviceinfo/BuildNumberPreferenceController;->aIP:I

    if-gez v0, :cond_7

    iget-object v0, p0, Lcom/android/settings/deviceinfo/BuildNumberPreferenceController;->aIQ:Landroid/widget/Toast;

    if-eqz v0, :cond_c

    iget-object v0, p0, Lcom/android/settings/deviceinfo/BuildNumberPreferenceController;->aIQ:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->cancel()V

    :cond_c
    iget-object v0, p0, Lcom/android/settings/deviceinfo/BuildNumberPreferenceController;->mContext:Landroid/content/Context;

    const v3, 0x7f121064

    invoke-static {v0, v3, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/deviceinfo/BuildNumberPreferenceController;->aIQ:Landroid/widget/Toast;

    iget-object v0, p0, Lcom/android/settings/deviceinfo/BuildNumberPreferenceController;->aIQ:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    iget-object v0, p0, Lcom/android/settings/deviceinfo/BuildNumberPreferenceController;->mMetricsFeatureProvider:Lcom/android/settings/core/instrumentation/e;

    iget-object v3, p0, Lcom/android/settings/deviceinfo/BuildNumberPreferenceController;->mContext:Landroid/content/Context;

    new-array v4, v2, [Landroid/util/Pair;

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v5

    aput-object v5, v4, v1

    invoke-virtual {v0, v3, v7, v4}, Lcom/android/settings/core/instrumentation/e;->ajS(Landroid/content/Context;I[Landroid/util/Pair;)V

    goto :goto_2
.end method

.method public i(Landroid/preference/PreferenceScreen;)V
    .locals 2

    invoke-super {p0, p1}, Lcom/android/settings/core/e;->i(Landroid/preference/PreferenceScreen;)V

    const-string/jumbo v0, "build_number"

    invoke-virtual {p1, v0}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    if-eqz v0, :cond_0

    :try_start_0
    sget-object v1, Landroid/os/Build;->DISPLAY:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setEnabled(Z)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v1

    const v1, 0x7f120594

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setSummary(I)V

    goto :goto_0
.end method

.method public l()Ljava/lang/String;
    .locals 1

    const-string/jumbo v0, "build_number"

    return-object v0
.end method

.method public onActivityResult(IILandroid/content/Intent;)Z
    .locals 2

    const/4 v1, 0x0

    const/16 v0, 0x64

    if-eq p1, v0, :cond_0

    return v1

    :cond_0
    const/4 v0, -0x1

    if-ne p2, v0, :cond_1

    invoke-direct {p0}, Lcom/android/settings/deviceinfo/BuildNumberPreferenceController;->awh()V

    :cond_1
    iput-boolean v1, p0, Lcom/android/settings/deviceinfo/BuildNumberPreferenceController;->aIS:Z

    const/4 v0, 0x1

    return v0
.end method

.method public onResume()V
    .locals 4

    iget-object v0, p0, Lcom/android/settings/deviceinfo/BuildNumberPreferenceController;->mContext:Landroid/content/Context;

    const-string/jumbo v1, "no_debugging_features"

    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v2

    invoke-static {v0, v1, v2}, Lcom/android/settingslib/w;->crb(Landroid/content/Context;Ljava/lang/String;I)Lcom/android/settingslib/n;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/deviceinfo/BuildNumberPreferenceController;->aIN:Lcom/android/settingslib/n;

    iget-object v0, p0, Lcom/android/settings/deviceinfo/BuildNumberPreferenceController;->mContext:Landroid/content/Context;

    const-string/jumbo v1, "no_debugging_features"

    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v2

    invoke-static {v0, v1, v2}, Lcom/android/settingslib/w;->crn(Landroid/content/Context;Ljava/lang/String;I)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/settings/deviceinfo/BuildNumberPreferenceController;->aIO:Z

    iget-object v0, p0, Lcom/android/settings/deviceinfo/BuildNumberPreferenceController;->mContext:Landroid/content/Context;

    const-string/jumbo v1, "development"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string/jumbo v1, "show"

    sget-object v2, Landroid/os/Build;->TYPE:Ljava/lang/String;

    const-string/jumbo v3, "eng"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, -0x1

    :goto_0
    iput v0, p0, Lcom/android/settings/deviceinfo/BuildNumberPreferenceController;->aIP:I

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/settings/deviceinfo/BuildNumberPreferenceController;->aIQ:Landroid/widget/Toast;

    return-void

    :cond_0
    const/4 v0, 0x7

    goto :goto_0
.end method

.method public p()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method
