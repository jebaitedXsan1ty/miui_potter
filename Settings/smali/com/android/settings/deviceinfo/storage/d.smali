.class public Lcom/android/settings/deviceinfo/storage/d;
.super Lcom/android/settings/core/e;
.source "UserProfileController.java"

# interfaces
.implements Lcom/android/settings/deviceinfo/storage/h;
.implements Lcom/android/settings/deviceinfo/storage/l;


# instance fields
.field private final aGa:I

.field private aGb:Lcom/android/settings/deviceinfo/StorageItemPreference;

.field private aGc:J

.field private aGd:Landroid/content/pm/UserInfo;

.field private aGe:Lcom/android/settings/applications/UserManagerWrapper;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/content/pm/UserInfo;Lcom/android/settings/applications/UserManagerWrapper;I)V
    .locals 1

    invoke-direct {p0, p1}, Lcom/android/settings/core/e;-><init>(Landroid/content/Context;)V

    invoke-static {p2}, Lcom/android/internal/util/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/UserInfo;

    iput-object v0, p0, Lcom/android/settings/deviceinfo/storage/d;->aGd:Landroid/content/pm/UserInfo;

    iput-object p3, p0, Lcom/android/settings/deviceinfo/storage/d;->aGe:Lcom/android/settings/applications/UserManagerWrapper;

    iput p4, p0, Lcom/android/settings/deviceinfo/storage/d;->aGa:I

    return-void
.end method


# virtual methods
.method public atA(Landroid/util/SparseArray;)V
    .locals 6

    invoke-static {p1}, Lcom/android/internal/util/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/android/settings/deviceinfo/storage/d;->aGd:Landroid/content/pm/UserInfo;

    iget v0, v0, Landroid/content/pm/UserInfo;->id:I

    invoke-virtual {p1, v0}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settings/deviceinfo/storage/g;

    if-eqz v0, :cond_0

    iget-object v1, v0, Lcom/android/settings/deviceinfo/storage/g;->aGw:Lcom/android/settingslib/b/I;

    iget-wide v2, v1, Lcom/android/settingslib/b/I;->cBF:J

    iget-wide v4, v0, Lcom/android/settings/deviceinfo/storage/g;->aGx:J

    add-long/2addr v2, v4

    iget-wide v4, v0, Lcom/android/settings/deviceinfo/storage/g;->aGy:J

    add-long/2addr v2, v4

    iget-wide v4, v0, Lcom/android/settings/deviceinfo/storage/g;->aGz:J

    add-long/2addr v2, v4

    iget-wide v0, v0, Lcom/android/settings/deviceinfo/storage/g;->aGA:J

    add-long/2addr v0, v2

    iget-wide v2, p0, Lcom/android/settings/deviceinfo/storage/d;->aGc:J

    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/android/settings/deviceinfo/storage/d;->atG(JJ)V

    :cond_0
    return-void
.end method

.method public atB(Landroid/util/SparseArray;)V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/deviceinfo/storage/d;->aGd:Landroid/content/pm/UserInfo;

    iget v0, v0, Landroid/content/pm/UserInfo;->id:I

    invoke-virtual {p1, v0}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/android/settings/deviceinfo/storage/d;->aGb:Lcom/android/settings/deviceinfo/StorageItemPreference;

    invoke-virtual {v1, v0}, Lcom/android/settings/deviceinfo/StorageItemPreference;->setIcon(Landroid/graphics/drawable/Drawable;)V

    :cond_0
    return-void
.end method

.method public atG(JJ)V
    .locals 1

    iget-object v0, p0, Lcom/android/settings/deviceinfo/storage/d;->aGb:Lcom/android/settings/deviceinfo/StorageItemPreference;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/deviceinfo/storage/d;->aGb:Lcom/android/settings/deviceinfo/StorageItemPreference;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/android/settings/deviceinfo/StorageItemPreference;->axb(JJ)V

    :cond_0
    return-void
.end method

.method public i(Landroid/preference/PreferenceScreen;)V
    .locals 3

    new-instance v0, Lcom/android/settings/deviceinfo/StorageItemPreference;

    invoke-virtual {p1}, Landroid/preference/PreferenceScreen;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/android/settings/deviceinfo/StorageItemPreference;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/settings/deviceinfo/storage/d;->aGb:Lcom/android/settings/deviceinfo/StorageItemPreference;

    iget-object v0, p0, Lcom/android/settings/deviceinfo/storage/d;->aGb:Lcom/android/settings/deviceinfo/StorageItemPreference;

    iget v1, p0, Lcom/android/settings/deviceinfo/storage/d;->aGa:I

    invoke-virtual {v0, v1}, Lcom/android/settings/deviceinfo/StorageItemPreference;->setOrder(I)V

    iget-object v0, p0, Lcom/android/settings/deviceinfo/storage/d;->aGb:Lcom/android/settings/deviceinfo/StorageItemPreference;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "pref_profile_"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/settings/deviceinfo/storage/d;->aGd:Landroid/content/pm/UserInfo;

    iget v2, v2, Landroid/content/pm/UserInfo;->id:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/settings/deviceinfo/StorageItemPreference;->setKey(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/settings/deviceinfo/storage/d;->aGb:Lcom/android/settings/deviceinfo/StorageItemPreference;

    iget-object v1, p0, Lcom/android/settings/deviceinfo/storage/d;->aGd:Landroid/content/pm/UserInfo;

    iget-object v1, v1, Landroid/content/pm/UserInfo;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/android/settings/deviceinfo/StorageItemPreference;->setTitle(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public l()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "pref_profile_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/deviceinfo/storage/d;->aGd:Landroid/content/pm/UserInfo;

    iget v1, v1, Landroid/content/pm/UserInfo;->id:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public p()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method
