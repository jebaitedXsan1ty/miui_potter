.class public Lcom/android/settings/deviceinfo/storage/a;
.super Lcom/android/settings/core/e;
.source "SecondaryUserController.java"

# interfaces
.implements Lcom/android/settings/deviceinfo/storage/h;
.implements Lcom/android/settings/deviceinfo/storage/l;


# instance fields
.field private aFS:J

.field private aFT:Lcom/android/settings/deviceinfo/StorageItemPreference;

.field private aFU:J

.field private aFV:Landroid/content/pm/UserInfo;

.field private aFW:Landroid/graphics/drawable/Drawable;


# direct methods
.method constructor <init>(Landroid/content/Context;Landroid/content/pm/UserInfo;)V
    .locals 2

    invoke-direct {p0, p1}, Lcom/android/settings/core/e;-><init>(Landroid/content/Context;)V

    iput-object p2, p0, Lcom/android/settings/deviceinfo/storage/a;->aFV:Landroid/content/pm/UserInfo;

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/android/settings/deviceinfo/storage/a;->aFS:J

    return-void
.end method

.method private atC()V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/deviceinfo/storage/a;->aFW:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/deviceinfo/storage/a;->aFT:Lcom/android/settings/deviceinfo/StorageItemPreference;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/deviceinfo/storage/a;->aFT:Lcom/android/settings/deviceinfo/StorageItemPreference;

    iget-object v1, p0, Lcom/android/settings/deviceinfo/storage/a;->aFW:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1}, Lcom/android/settings/deviceinfo/StorageItemPreference;->setIcon(Landroid/graphics/drawable/Drawable;)V

    :cond_0
    return-void
.end method

.method public static aty(Landroid/content/Context;Lcom/android/settings/applications/UserManagerWrapper;)Ljava/util/List;
    .locals 9

    const/4 v0, 0x0

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {p1}, Lcom/android/settings/applications/UserManagerWrapper;->wb()Landroid/content/pm/UserInfo;

    move-result-object v4

    invoke-interface {p1}, Lcom/android/settings/applications/UserManagerWrapper;->wc()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v6

    move v2, v0

    move v1, v0

    :goto_0
    if-ge v2, v6, :cond_3

    invoke-interface {v5, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/UserInfo;

    invoke-virtual {v0}, Landroid/content/pm/UserInfo;->isPrimary()Z

    move-result v7

    if-eqz v7, :cond_0

    move v0, v1

    :goto_1
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    move v1, v0

    goto :goto_0

    :cond_0
    if-eqz v0, :cond_1

    invoke-static {v4, v0}, Lcom/android/settings/aq;->bqT(Landroid/content/pm/UserInfo;Landroid/content/pm/UserInfo;)Z

    move-result v7

    if-eqz v7, :cond_2

    :cond_1
    new-instance v7, Lcom/android/settings/deviceinfo/storage/d;

    const/4 v8, 0x6

    invoke-direct {v7, p0, v0, p1, v8}, Lcom/android/settings/deviceinfo/storage/d;-><init>(Landroid/content/Context;Landroid/content/pm/UserInfo;Lcom/android/settings/applications/UserManagerWrapper;I)V

    invoke-interface {v3, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move v0, v1

    goto :goto_1

    :cond_2
    new-instance v1, Lcom/android/settings/deviceinfo/storage/a;

    invoke-direct {v1, p0, v0}, Lcom/android/settings/deviceinfo/storage/a;-><init>(Landroid/content/Context;Landroid/content/pm/UserInfo;)V

    invoke-interface {v3, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const/4 v0, 0x1

    goto :goto_1

    :cond_3
    if-nez v1, :cond_4

    new-instance v0, Lcom/android/settings/deviceinfo/storage/b;

    invoke-direct {v0, p0}, Lcom/android/settings/deviceinfo/storage/b;-><init>(Landroid/content/Context;)V

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_4
    return-object v3
.end method


# virtual methods
.method public atA(Landroid/util/SparseArray;)V
    .locals 2

    invoke-virtual {p0}, Lcom/android/settings/deviceinfo/storage/a;->atz()Landroid/content/pm/UserInfo;

    move-result-object v0

    iget v0, v0, Landroid/content/pm/UserInfo;->id:I

    invoke-virtual {p1, v0}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settings/deviceinfo/storage/g;

    if-eqz v0, :cond_0

    iget-object v0, v0, Lcom/android/settings/deviceinfo/storage/g;->aGw:Lcom/android/settingslib/b/I;

    iget-wide v0, v0, Lcom/android/settingslib/b/I;->cBF:J

    invoke-virtual {p0, v0, v1}, Lcom/android/settings/deviceinfo/storage/a;->atD(J)V

    :cond_0
    return-void
.end method

.method public atB(Landroid/util/SparseArray;)V
    .locals 1

    iget-object v0, p0, Lcom/android/settings/deviceinfo/storage/a;->aFV:Landroid/content/pm/UserInfo;

    iget v0, v0, Landroid/content/pm/UserInfo;->id:I

    invoke-virtual {p1, v0}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/Drawable;

    iput-object v0, p0, Lcom/android/settings/deviceinfo/storage/a;->aFW:Landroid/graphics/drawable/Drawable;

    invoke-direct {p0}, Lcom/android/settings/deviceinfo/storage/a;->atC()V

    return-void
.end method

.method public atD(J)V
    .locals 7

    iput-wide p1, p0, Lcom/android/settings/deviceinfo/storage/a;->aFS:J

    iget-object v0, p0, Lcom/android/settings/deviceinfo/storage/a;->aFT:Lcom/android/settings/deviceinfo/StorageItemPreference;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/deviceinfo/storage/a;->aFT:Lcom/android/settings/deviceinfo/StorageItemPreference;

    iget-wide v2, p0, Lcom/android/settings/deviceinfo/storage/a;->aFS:J

    iget-wide v4, p0, Lcom/android/settings/deviceinfo/storage/a;->aFU:J

    invoke-virtual {v0, v2, v3, v4, v5}, Lcom/android/settings/deviceinfo/StorageItemPreference;->axb(JJ)V

    :cond_0
    return-void
.end method

.method public atE(J)V
    .locals 1

    iput-wide p1, p0, Lcom/android/settings/deviceinfo/storage/a;->aFU:J

    return-void
.end method

.method public atz()Landroid/content/pm/UserInfo;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/deviceinfo/storage/a;->aFV:Landroid/content/pm/UserInfo;

    return-object v0
.end method

.method public i(Landroid/preference/PreferenceScreen;)V
    .locals 6

    iget-object v0, p0, Lcom/android/settings/deviceinfo/storage/a;->aFT:Lcom/android/settings/deviceinfo/StorageItemPreference;

    if-nez v0, :cond_1

    new-instance v0, Lcom/android/settings/deviceinfo/StorageItemPreference;

    invoke-virtual {p1}, Landroid/preference/PreferenceScreen;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/android/settings/deviceinfo/StorageItemPreference;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/settings/deviceinfo/storage/a;->aFT:Lcom/android/settings/deviceinfo/StorageItemPreference;

    const-string/jumbo v0, "pref_secondary_users"

    invoke-virtual {p1, v0}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceGroup;

    iget-object v1, p0, Lcom/android/settings/deviceinfo/storage/a;->aFT:Lcom/android/settings/deviceinfo/StorageItemPreference;

    iget-object v2, p0, Lcom/android/settings/deviceinfo/storage/a;->aFV:Landroid/content/pm/UserInfo;

    iget-object v2, v2, Landroid/content/pm/UserInfo;->name:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/android/settings/deviceinfo/StorageItemPreference;->setTitle(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/android/settings/deviceinfo/storage/a;->aFT:Lcom/android/settings/deviceinfo/StorageItemPreference;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "pref_user_"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/settings/deviceinfo/storage/a;->aFV:Landroid/content/pm/UserInfo;

    iget v3, v3, Landroid/content/pm/UserInfo;->id:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/android/settings/deviceinfo/StorageItemPreference;->setKey(Ljava/lang/String;)V

    iget-wide v2, p0, Lcom/android/settings/deviceinfo/storage/a;->aFS:J

    const-wide/16 v4, -0x1

    cmp-long v1, v2, v4

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/settings/deviceinfo/storage/a;->aFT:Lcom/android/settings/deviceinfo/StorageItemPreference;

    iget-wide v2, p0, Lcom/android/settings/deviceinfo/storage/a;->aFS:J

    iget-wide v4, p0, Lcom/android/settings/deviceinfo/storage/a;->aFU:J

    invoke-virtual {v1, v2, v3, v4, v5}, Lcom/android/settings/deviceinfo/StorageItemPreference;->axb(JJ)V

    :cond_0
    iget-object v1, p0, Lcom/android/settings/deviceinfo/storage/a;->aFT:Lcom/android/settings/deviceinfo/StorageItemPreference;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceGroup;->addPreference(Landroid/preference/Preference;)Z

    invoke-direct {p0}, Lcom/android/settings/deviceinfo/storage/a;->atC()V

    :cond_1
    return-void
.end method

.method public l()Ljava/lang/String;
    .locals 2

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/android/settings/deviceinfo/storage/a;->aFT:Lcom/android/settings/deviceinfo/StorageItemPreference;

    if-eqz v1, :cond_0

    iget-object v0, p0, Lcom/android/settings/deviceinfo/storage/a;->aFT:Lcom/android/settings/deviceinfo/StorageItemPreference;

    invoke-virtual {v0}, Lcom/android/settings/deviceinfo/StorageItemPreference;->getKey()Ljava/lang/String;

    move-result-object v0

    :cond_0
    return-object v0
.end method

.method public p()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method
