.class public Lcom/android/settings/deviceinfo/storage/e;
.super Lcom/android/settings/core/e;
.source "StorageItemPreferenceController.java"


# static fields
.field static final AUDIO_KEY:Ljava/lang/String; = "pref_music_audio"

.field static final FILES_KEY:Ljava/lang/String; = "pref_files"

.field static final GAME_KEY:Ljava/lang/String; = "pref_games"

.field static final MOVIES_KEY:Ljava/lang/String; = "pref_movies"

.field static final OTHER_APPS_KEY:Ljava/lang/String; = "pref_other_apps"

.field static final PHOTO_KEY:Ljava/lang/String; = "pref_photos_videos"

.field static final SYSTEM_KEY:Ljava/lang/String; = "pref_system"


# instance fields
.field private aGf:Lcom/android/settings/deviceinfo/StorageItemPreference;

.field private aGg:Lcom/android/settings/deviceinfo/StorageItemPreference;

.field private aGh:Lcom/android/settings/deviceinfo/StorageItemPreference;

.field private final aGi:Landroid/app/Fragment;

.field private aGj:Lcom/android/settings/deviceinfo/StorageItemPreference;

.field private aGk:Lcom/android/settings/deviceinfo/StorageItemPreference;

.field private aGl:Lcom/android/settings/deviceinfo/StorageItemPreference;

.field private aGm:Landroid/preference/PreferenceScreen;

.field private final aGn:Lcom/android/settingslib/f/b;

.field private aGo:Lcom/android/settings/deviceinfo/StorageItemPreference;

.field private aGp:J

.field private aGq:J

.field private aGr:Landroid/os/storage/VolumeInfo;

.field private final mMetricsFeatureProvider:Lcom/android/settings/core/instrumentation/e;

.field private mUserId:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/app/Fragment;Landroid/os/storage/VolumeInfo;Lcom/android/settingslib/f/b;)V
    .locals 1

    invoke-direct {p0, p1}, Lcom/android/settings/core/e;-><init>(Landroid/content/Context;)V

    iput-object p2, p0, Lcom/android/settings/deviceinfo/storage/e;->aGi:Landroid/app/Fragment;

    iput-object p3, p0, Lcom/android/settings/deviceinfo/storage/e;->aGr:Landroid/os/storage/VolumeInfo;

    iput-object p4, p0, Lcom/android/settings/deviceinfo/storage/e;->aGn:Lcom/android/settingslib/f/b;

    invoke-static {p1}, Lcom/android/settings/overlay/a;->aIk(Landroid/content/Context;)Lcom/android/settings/overlay/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/settings/overlay/a;->aIm()Lcom/android/settings/core/instrumentation/e;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/deviceinfo/storage/e;->mMetricsFeatureProvider:Lcom/android/settings/core/instrumentation/e;

    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v0

    iput v0, p0, Lcom/android/settings/deviceinfo/storage/e;->mUserId:I

    return-void
.end method

.method private static atL(Landroid/content/Context;Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable;
    .locals 3

    const/4 v2, 0x0

    const/4 v0, 0x1

    new-array v0, v0, [I

    const v1, 0x1010429

    aput v1, v0, v2

    invoke-virtual {p0, v0}, Landroid/content/Context;->obtainStyledAttributes([I)Landroid/content/res/TypedArray;

    move-result-object v0

    invoke-virtual {p1}, Landroid/graphics/drawable/Drawable;->mutate()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v2, v2}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/graphics/drawable/Drawable;->setTint(I)V

    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    return-object v1
.end method

.method private atM(Landroid/content/pm/PackageManager;Landroid/os/UserHandle;Landroid/preference/Preference;)V
    .locals 2

    if-eqz p3, :cond_0

    invoke-virtual {p3}, Landroid/preference/Preference;->getIcon()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/deviceinfo/storage/e;->mContext:Landroid/content/Context;

    invoke-static {v1, v0}, Lcom/android/settings/deviceinfo/storage/e;->atL(Landroid/content/Context;Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p1, v0, p2}, Landroid/content/pm/PackageManager;->getUserBadgedIcon(Landroid/graphics/drawable/Drawable;Landroid/os/UserHandle;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p3, v0}, Landroid/preference/Preference;->setIcon(Landroid/graphics/drawable/Drawable;)V

    :cond_0
    return-void
.end method

.method private atN()Landroid/content/Intent;
    .locals 8

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/android/settings/deviceinfo/storage/e;->aGr:Landroid/os/storage/VolumeInfo;

    if-nez v0, :cond_0

    return-object v3

    :cond_0
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    const-string/jumbo v0, "classname"

    const-class v1, Lcom/android/settings/Settings$StorageUseActivity;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string/jumbo v0, "volumeUuid"

    iget-object v1, p0, Lcom/android/settings/deviceinfo/storage/e;->aGr:Landroid/os/storage/VolumeInfo;

    invoke-virtual {v1}, Landroid/os/storage/VolumeInfo;->getFsUuid()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string/jumbo v0, "volumeName"

    iget-object v1, p0, Lcom/android/settings/deviceinfo/storage/e;->aGr:Landroid/os/storage/VolumeInfo;

    invoke-virtual {v1}, Landroid/os/storage/VolumeInfo;->getDescription()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/settings/deviceinfo/storage/e;->mContext:Landroid/content/Context;

    const-class v1, Lcom/android/settings/applications/ManageApplications;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    iget-object v4, p0, Lcom/android/settings/deviceinfo/storage/e;->mMetricsFeatureProvider:Lcom/android/settings/core/instrumentation/e;

    iget-object v5, p0, Lcom/android/settings/deviceinfo/storage/e;->aGi:Landroid/app/Fragment;

    invoke-virtual {v4, v5}, Lcom/android/settings/core/instrumentation/e;->ajT(Ljava/lang/Object;)I

    move-result v7

    const v4, 0x7f120174

    const/4 v6, 0x0

    move-object v5, v3

    invoke-static/range {v0 .. v7}, Lcom/android/settings/aq;->bqs(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;Ljava/lang/String;ILjava/lang/CharSequence;ZI)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method private atO()Landroid/content/Intent;
    .locals 8

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/android/settings/deviceinfo/storage/e;->aGr:Landroid/os/storage/VolumeInfo;

    if-nez v0, :cond_0

    return-object v3

    :cond_0
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    const-string/jumbo v0, "classname"

    const-class v1, Lcom/android/settings/Settings$StorageUseActivity;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string/jumbo v0, "volumeUuid"

    iget-object v1, p0, Lcom/android/settings/deviceinfo/storage/e;->aGr:Landroid/os/storage/VolumeInfo;

    invoke-virtual {v1}, Landroid/os/storage/VolumeInfo;->getFsUuid()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string/jumbo v0, "volumeName"

    iget-object v1, p0, Lcom/android/settings/deviceinfo/storage/e;->aGr:Landroid/os/storage/VolumeInfo;

    invoke-virtual {v1}, Landroid/os/storage/VolumeInfo;->getDescription()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string/jumbo v0, "storageType"

    const/4 v1, 0x1

    invoke-virtual {v2, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    iget-object v0, p0, Lcom/android/settings/deviceinfo/storage/e;->mContext:Landroid/content/Context;

    const-class v1, Lcom/android/settings/applications/ManageApplications;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    iget-object v4, p0, Lcom/android/settings/deviceinfo/storage/e;->mMetricsFeatureProvider:Lcom/android/settings/core/instrumentation/e;

    iget-object v5, p0, Lcom/android/settings/deviceinfo/storage/e;->aGi:Landroid/app/Fragment;

    invoke-virtual {v4, v5}, Lcom/android/settings/core/instrumentation/e;->ajT(Ljava/lang/Object;)I

    move-result v7

    const v4, 0x7f12119c

    const/4 v6, 0x0

    move-object v5, v3

    invoke-static/range {v0 .. v7}, Lcom/android/settings/aq;->bqs(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;Ljava/lang/String;ILjava/lang/CharSequence;ZI)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method private atP()Landroid/content/Intent;
    .locals 2

    iget-object v0, p0, Lcom/android/settings/deviceinfo/storage/e;->aGn:Lcom/android/settingslib/f/b;

    iget-object v1, p0, Lcom/android/settings/deviceinfo/storage/e;->aGr:Landroid/os/storage/VolumeInfo;

    invoke-interface {v0, v1}, Lcom/android/settingslib/f/b;->coi(Landroid/os/storage/VolumeInfo;)Landroid/os/storage/VolumeInfo;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/storage/VolumeInfo;->buildBrowseIntent()Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method private atQ()Landroid/content/Intent;
    .locals 8

    const/4 v3, 0x0

    new-instance v2, Landroid/os/Bundle;

    const/4 v0, 0x1

    invoke-direct {v2, v0}, Landroid/os/Bundle;-><init>(I)V

    const-string/jumbo v0, "classname"

    const-class v1, Lcom/android/settings/aE;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/settings/deviceinfo/storage/e;->mContext:Landroid/content/Context;

    const-class v1, Lcom/android/settings/applications/ManageApplications;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    iget-object v4, p0, Lcom/android/settings/deviceinfo/storage/e;->mMetricsFeatureProvider:Lcom/android/settings/core/instrumentation/e;

    iget-object v5, p0, Lcom/android/settings/deviceinfo/storage/e;->aGi:Landroid/app/Fragment;

    invoke-virtual {v4, v5}, Lcom/android/settings/core/instrumentation/e;->ajT(Ljava/lang/Object;)I

    move-result v7

    const v4, 0x7f1207c0

    const/4 v6, 0x0

    move-object v5, v3

    invoke-static/range {v0 .. v7}, Lcom/android/settings/aq;->bqs(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;Ljava/lang/String;ILjava/lang/CharSequence;ZI)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method private atR()Landroid/content/Intent;
    .locals 8

    const/4 v3, 0x0

    new-instance v2, Landroid/os/Bundle;

    const/4 v0, 0x1

    invoke-direct {v2, v0}, Landroid/os/Bundle;-><init>(I)V

    const-string/jumbo v0, "classname"

    const-class v1, Lcom/android/settings/aF;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/settings/deviceinfo/storage/e;->mContext:Landroid/content/Context;

    const-class v1, Lcom/android/settings/applications/ManageApplications;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    iget-object v4, p0, Lcom/android/settings/deviceinfo/storage/e;->mMetricsFeatureProvider:Lcom/android/settings/core/instrumentation/e;

    iget-object v5, p0, Lcom/android/settings/deviceinfo/storage/e;->aGi:Landroid/app/Fragment;

    invoke-virtual {v4, v5}, Lcom/android/settings/core/instrumentation/e;->ajT(Ljava/lang/Object;)I

    move-result v7

    const v4, 0x7f12119b

    const/4 v6, 0x0

    move-object v5, v3

    invoke-static/range {v0 .. v7}, Lcom/android/settings/aq;->bqs(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;Ljava/lang/String;ILjava/lang/CharSequence;ZI)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method private atS()Landroid/content/Intent;
    .locals 3

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string/jumbo v1, "android.intent.action.VIEW"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const/high16 v1, 0x80000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    const-string/jumbo v1, "image/*"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    const-string/jumbo v1, "android.intent.extra.FROM_STORAGE"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    return-object v0
.end method

.method private atT(Landroid/content/Intent;)V
    .locals 3

    const/4 v2, -0x1

    :try_start_0
    const-string/jumbo v0, "android.intent.extra.USER_ID"

    const/4 v1, -0x1

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    if-ne v0, v2, :cond_0

    iget-object v0, p0, Lcom/android/settings/deviceinfo/storage/e;->aGi:Landroid/app/Fragment;

    invoke-virtual {v0, p1}, Landroid/app/Fragment;->startActivity(Landroid/content/Intent;)V

    :goto_0
    return-void

    :cond_0
    iget-object v1, p0, Lcom/android/settings/deviceinfo/storage/e;->aGi:Landroid/app/Fragment;

    invoke-virtual {v1}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    new-instance v2, Landroid/os/UserHandle;

    invoke-direct {v2, v0}, Landroid/os/UserHandle;-><init>(I)V

    invoke-virtual {v1, p1, v2}, Landroid/app/Activity;->startActivityAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string/jumbo v0, "StorageItemPreference"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "No activity found for "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private atU()V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/deviceinfo/storage/e;->aGm:Landroid/preference/PreferenceScreen;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/deviceinfo/storage/e;->aGn:Lcom/android/settingslib/f/b;

    iget-object v1, p0, Lcom/android/settings/deviceinfo/storage/e;->aGr:Landroid/os/storage/VolumeInfo;

    invoke-interface {v0, v1}, Lcom/android/settingslib/f/b;->coi(Landroid/os/storage/VolumeInfo;)Landroid/os/storage/VolumeInfo;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/os/storage/VolumeInfo;->isMountedReadable()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    :goto_0
    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/settings/deviceinfo/storage/e;->aGm:Landroid/preference/PreferenceScreen;

    iget-object v1, p0, Lcom/android/settings/deviceinfo/storage/e;->aGh:Lcom/android/settings/deviceinfo/StorageItemPreference;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    :cond_0
    :goto_1
    return-void

    :cond_1
    const/4 v0, 0x1

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/android/settings/deviceinfo/storage/e;->aGm:Landroid/preference/PreferenceScreen;

    iget-object v1, p0, Lcom/android/settings/deviceinfo/storage/e;->aGh:Lcom/android/settings/deviceinfo/StorageItemPreference;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    goto :goto_1
.end method


# virtual methods
.method public atH(Landroid/os/UserHandle;)V
    .locals 2

    invoke-virtual {p1}, Landroid/os/UserHandle;->getIdentifier()I

    move-result v0

    iput v0, p0, Lcom/android/settings/deviceinfo/storage/e;->mUserId:I

    iget-object v0, p0, Lcom/android/settings/deviceinfo/storage/e;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/deviceinfo/storage/e;->aGl:Lcom/android/settings/deviceinfo/StorageItemPreference;

    invoke-direct {p0, v0, p1, v1}, Lcom/android/settings/deviceinfo/storage/e;->atM(Landroid/content/pm/PackageManager;Landroid/os/UserHandle;Landroid/preference/Preference;)V

    iget-object v1, p0, Lcom/android/settings/deviceinfo/storage/e;->aGk:Lcom/android/settings/deviceinfo/StorageItemPreference;

    invoke-direct {p0, v0, p1, v1}, Lcom/android/settings/deviceinfo/storage/e;->atM(Landroid/content/pm/PackageManager;Landroid/os/UserHandle;Landroid/preference/Preference;)V

    iget-object v1, p0, Lcom/android/settings/deviceinfo/storage/e;->aGg:Lcom/android/settings/deviceinfo/StorageItemPreference;

    invoke-direct {p0, v0, p1, v1}, Lcom/android/settings/deviceinfo/storage/e;->atM(Landroid/content/pm/PackageManager;Landroid/os/UserHandle;Landroid/preference/Preference;)V

    iget-object v1, p0, Lcom/android/settings/deviceinfo/storage/e;->aGj:Lcom/android/settings/deviceinfo/StorageItemPreference;

    invoke-direct {p0, v0, p1, v1}, Lcom/android/settings/deviceinfo/storage/e;->atM(Landroid/content/pm/PackageManager;Landroid/os/UserHandle;Landroid/preference/Preference;)V

    iget-object v1, p0, Lcom/android/settings/deviceinfo/storage/e;->aGf:Lcom/android/settings/deviceinfo/StorageItemPreference;

    invoke-direct {p0, v0, p1, v1}, Lcom/android/settings/deviceinfo/storage/e;->atM(Landroid/content/pm/PackageManager;Landroid/os/UserHandle;Landroid/preference/Preference;)V

    iget-object v1, p0, Lcom/android/settings/deviceinfo/storage/e;->aGo:Lcom/android/settings/deviceinfo/StorageItemPreference;

    invoke-direct {p0, v0, p1, v1}, Lcom/android/settings/deviceinfo/storage/e;->atM(Landroid/content/pm/PackageManager;Landroid/os/UserHandle;Landroid/preference/Preference;)V

    iget-object v1, p0, Lcom/android/settings/deviceinfo/storage/e;->aGh:Lcom/android/settings/deviceinfo/StorageItemPreference;

    invoke-direct {p0, v0, p1, v1}, Lcom/android/settings/deviceinfo/storage/e;->atM(Landroid/content/pm/PackageManager;Landroid/os/UserHandle;Landroid/preference/Preference;)V

    return-void
.end method

.method public atI(Landroid/util/SparseArray;I)V
    .locals 8

    invoke-virtual {p1, p2}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settings/deviceinfo/storage/g;

    iget-object v1, p0, Lcom/android/settings/deviceinfo/storage/e;->aGl:Lcom/android/settings/deviceinfo/StorageItemPreference;

    iget-object v2, v0, Lcom/android/settings/deviceinfo/storage/g;->aGw:Lcom/android/settingslib/b/I;

    iget-wide v2, v2, Lcom/android/settingslib/b/I;->cBI:J

    iget-object v4, v0, Lcom/android/settings/deviceinfo/storage/g;->aGw:Lcom/android/settingslib/b/I;

    iget-wide v4, v4, Lcom/android/settingslib/b/I;->cBH:J

    add-long/2addr v2, v4

    iget-wide v4, p0, Lcom/android/settings/deviceinfo/storage/e;->aGp:J

    invoke-virtual {v1, v2, v3, v4, v5}, Lcom/android/settings/deviceinfo/StorageItemPreference;->axb(JJ)V

    iget-object v1, p0, Lcom/android/settings/deviceinfo/storage/e;->aGg:Lcom/android/settings/deviceinfo/StorageItemPreference;

    iget-wide v2, v0, Lcom/android/settings/deviceinfo/storage/g;->aGz:J

    iget-object v4, v0, Lcom/android/settings/deviceinfo/storage/g;->aGw:Lcom/android/settingslib/b/I;

    iget-wide v4, v4, Lcom/android/settingslib/b/I;->cBG:J

    add-long/2addr v2, v4

    iget-wide v4, p0, Lcom/android/settings/deviceinfo/storage/e;->aGp:J

    invoke-virtual {v1, v2, v3, v4, v5}, Lcom/android/settings/deviceinfo/StorageItemPreference;->axb(JJ)V

    iget-object v1, p0, Lcom/android/settings/deviceinfo/storage/e;->aGj:Lcom/android/settings/deviceinfo/StorageItemPreference;

    iget-wide v2, v0, Lcom/android/settings/deviceinfo/storage/g;->aGA:J

    iget-wide v4, p0, Lcom/android/settings/deviceinfo/storage/e;->aGp:J

    invoke-virtual {v1, v2, v3, v4, v5}, Lcom/android/settings/deviceinfo/StorageItemPreference;->axb(JJ)V

    iget-object v1, p0, Lcom/android/settings/deviceinfo/storage/e;->aGk:Lcom/android/settings/deviceinfo/StorageItemPreference;

    iget-wide v2, v0, Lcom/android/settings/deviceinfo/storage/g;->aGy:J

    iget-wide v4, p0, Lcom/android/settings/deviceinfo/storage/e;->aGp:J

    invoke-virtual {v1, v2, v3, v4, v5}, Lcom/android/settings/deviceinfo/StorageItemPreference;->axb(JJ)V

    iget-object v1, p0, Lcom/android/settings/deviceinfo/storage/e;->aGf:Lcom/android/settings/deviceinfo/StorageItemPreference;

    iget-wide v2, v0, Lcom/android/settings/deviceinfo/storage/g;->aGx:J

    iget-wide v4, p0, Lcom/android/settings/deviceinfo/storage/e;->aGp:J

    invoke-virtual {v1, v2, v3, v4, v5}, Lcom/android/settings/deviceinfo/StorageItemPreference;->axb(JJ)V

    iget-object v1, v0, Lcom/android/settings/deviceinfo/storage/g;->aGw:Lcom/android/settingslib/b/I;

    iget-wide v2, v1, Lcom/android/settingslib/b/I;->cBF:J

    iget-object v1, v0, Lcom/android/settings/deviceinfo/storage/g;->aGw:Lcom/android/settingslib/b/I;

    iget-wide v4, v1, Lcom/android/settingslib/b/I;->cBG:J

    sub-long/2addr v2, v4

    iget-object v1, v0, Lcom/android/settings/deviceinfo/storage/g;->aGw:Lcom/android/settingslib/b/I;

    iget-wide v4, v1, Lcom/android/settingslib/b/I;->cBH:J

    sub-long/2addr v2, v4

    iget-object v1, v0, Lcom/android/settings/deviceinfo/storage/g;->aGw:Lcom/android/settingslib/b/I;

    iget-wide v4, v1, Lcom/android/settingslib/b/I;->cBI:J

    sub-long/2addr v2, v4

    iget-object v0, v0, Lcom/android/settings/deviceinfo/storage/g;->aGw:Lcom/android/settingslib/b/I;

    iget-wide v0, v0, Lcom/android/settingslib/b/I;->cBE:J

    sub-long v0, v2, v0

    iget-object v2, p0, Lcom/android/settings/deviceinfo/storage/e;->aGh:Lcom/android/settings/deviceinfo/StorageItemPreference;

    iget-wide v4, p0, Lcom/android/settings/deviceinfo/storage/e;->aGp:J

    invoke-virtual {v2, v0, v1, v4, v5}, Lcom/android/settings/deviceinfo/StorageItemPreference;->axb(JJ)V

    iget-object v0, p0, Lcom/android/settings/deviceinfo/storage/e;->aGo:Lcom/android/settings/deviceinfo/StorageItemPreference;

    if-eqz v0, :cond_1

    const-wide/16 v2, 0x0

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    invoke-virtual {p1}, Landroid/util/SparseArray;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    invoke-virtual {p1, v1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settings/deviceinfo/storage/g;

    iget-wide v4, v0, Lcom/android/settings/deviceinfo/storage/g;->aGA:J

    iget-wide v6, v0, Lcom/android/settings/deviceinfo/storage/g;->aGz:J

    add-long/2addr v4, v6

    iget-wide v6, v0, Lcom/android/settings/deviceinfo/storage/g;->aGy:J

    add-long/2addr v4, v6

    iget-wide v6, v0, Lcom/android/settings/deviceinfo/storage/g;->aGx:J

    add-long/2addr v4, v6

    add-long/2addr v2, v4

    iget-object v4, v0, Lcom/android/settings/deviceinfo/storage/g;->aGw:Lcom/android/settingslib/b/I;

    iget-wide v4, v4, Lcom/android/settingslib/b/I;->cBF:J

    iget-object v0, v0, Lcom/android/settings/deviceinfo/storage/g;->aGw:Lcom/android/settingslib/b/I;

    iget-wide v6, v0, Lcom/android/settingslib/b/I;->cBE:J

    sub-long/2addr v4, v6

    add-long/2addr v2, v4

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    iget-wide v0, p0, Lcom/android/settings/deviceinfo/storage/e;->aGq:J

    sub-long/2addr v0, v2

    const-wide/32 v2, 0x40000000

    invoke-static {v2, v3, v0, v1}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    iget-object v2, p0, Lcom/android/settings/deviceinfo/storage/e;->aGo:Lcom/android/settings/deviceinfo/StorageItemPreference;

    iget-wide v4, p0, Lcom/android/settings/deviceinfo/storage/e;->aGp:J

    invoke-virtual {v2, v0, v1, v4, v5}, Lcom/android/settings/deviceinfo/StorageItemPreference;->axb(JJ)V

    :cond_1
    return-void
.end method

.method public atJ(J)V
    .locals 1

    iput-wide p1, p0, Lcom/android/settings/deviceinfo/storage/e;->aGq:J

    return-void
.end method

.method public atK(J)V
    .locals 1

    iput-wide p1, p0, Lcom/android/settings/deviceinfo/storage/e;->aGp:J

    return-void
.end method

.method public fm(Landroid/preference/Preference;)Z
    .locals 6

    const/4 v5, 0x1

    const/4 v3, 0x0

    const/4 v0, 0x0

    if-nez p1, :cond_0

    return v3

    :cond_0
    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_1

    return v3

    :cond_1
    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "pref_photos_videos"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-direct {p0}, Lcom/android/settings/deviceinfo/storage/e;->atS()Landroid/content/Intent;

    move-result-object v0

    :cond_2
    :goto_0
    if-eqz v0, :cond_9

    const-string/jumbo v1, "android.intent.extra.USER_ID"

    iget v2, p0, Lcom/android/settings/deviceinfo/storage/e;->mUserId:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    invoke-direct {p0, v0}, Lcom/android/settings/deviceinfo/storage/e;->atT(Landroid/content/Intent;)V

    return v5

    :cond_3
    const-string/jumbo v2, "pref_music_audio"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-direct {p0}, Lcom/android/settings/deviceinfo/storage/e;->atO()Landroid/content/Intent;

    move-result-object v0

    goto :goto_0

    :cond_4
    const-string/jumbo v2, "pref_games"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-direct {p0}, Lcom/android/settings/deviceinfo/storage/e;->atQ()Landroid/content/Intent;

    move-result-object v0

    goto :goto_0

    :cond_5
    const-string/jumbo v2, "pref_movies"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    invoke-direct {p0}, Lcom/android/settings/deviceinfo/storage/e;->atR()Landroid/content/Intent;

    move-result-object v0

    goto :goto_0

    :cond_6
    const-string/jumbo v2, "pref_other_apps"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_7

    iget-object v1, p0, Lcom/android/settings/deviceinfo/storage/e;->aGr:Landroid/os/storage/VolumeInfo;

    if-eqz v1, :cond_2

    invoke-direct {p0}, Lcom/android/settings/deviceinfo/storage/e;->atN()Landroid/content/Intent;

    move-result-object v0

    goto :goto_0

    :cond_7
    const-string/jumbo v2, "pref_files"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_8

    invoke-direct {p0}, Lcom/android/settings/deviceinfo/storage/e;->atP()Landroid/content/Intent;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/deviceinfo/storage/e;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/android/settings/overlay/a;->aIk(Landroid/content/Context;)Lcom/android/settings/overlay/a;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/settings/overlay/a;->aIm()Lcom/android/settings/core/instrumentation/e;

    move-result-object v1

    iget-object v2, p0, Lcom/android/settings/deviceinfo/storage/e;->mContext:Landroid/content/Context;

    new-array v3, v3, [Landroid/util/Pair;

    const/16 v4, 0x349

    invoke-virtual {v1, v2, v4, v3}, Lcom/android/settings/core/instrumentation/e;->ajS(Landroid/content/Context;I[Landroid/util/Pair;)V

    goto :goto_0

    :cond_8
    const-string/jumbo v2, "pref_system"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    new-instance v0, Lcom/android/settings/deviceinfo/PrivateVolumeSettings$SystemInfoFragment;

    invoke-direct {v0}, Lcom/android/settings/deviceinfo/PrivateVolumeSettings$SystemInfoFragment;-><init>()V

    iget-object v1, p0, Lcom/android/settings/deviceinfo/storage/e;->aGi:Landroid/app/Fragment;

    invoke-virtual {v0, v1, v3}, Lcom/android/settings/deviceinfo/PrivateVolumeSettings$SystemInfoFragment;->setTargetFragment(Landroid/app/Fragment;I)V

    iget-object v1, p0, Lcom/android/settings/deviceinfo/storage/e;->aGi:Landroid/app/Fragment;

    invoke-virtual {v1}, Landroid/app/Fragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const-string/jumbo v2, "SystemInfo"

    invoke-virtual {v0, v1, v2}, Lcom/android/settings/deviceinfo/PrivateVolumeSettings$SystemInfoFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    return v5

    :cond_9
    invoke-super {p0, p1}, Lcom/android/settings/core/e;->fm(Landroid/preference/Preference;)Z

    move-result v0

    return v0
.end method

.method public i(Landroid/preference/PreferenceScreen;)V
    .locals 1

    iput-object p1, p0, Lcom/android/settings/deviceinfo/storage/e;->aGm:Landroid/preference/PreferenceScreen;

    const-string/jumbo v0, "pref_photos_videos"

    invoke-virtual {p1, v0}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Lcom/android/settings/deviceinfo/StorageItemPreference;

    iput-object v0, p0, Lcom/android/settings/deviceinfo/storage/e;->aGl:Lcom/android/settings/deviceinfo/StorageItemPreference;

    const-string/jumbo v0, "pref_music_audio"

    invoke-virtual {p1, v0}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Lcom/android/settings/deviceinfo/StorageItemPreference;

    iput-object v0, p0, Lcom/android/settings/deviceinfo/storage/e;->aGg:Lcom/android/settings/deviceinfo/StorageItemPreference;

    const-string/jumbo v0, "pref_games"

    invoke-virtual {p1, v0}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Lcom/android/settings/deviceinfo/StorageItemPreference;

    iput-object v0, p0, Lcom/android/settings/deviceinfo/storage/e;->aGj:Lcom/android/settings/deviceinfo/StorageItemPreference;

    const-string/jumbo v0, "pref_movies"

    invoke-virtual {p1, v0}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Lcom/android/settings/deviceinfo/StorageItemPreference;

    iput-object v0, p0, Lcom/android/settings/deviceinfo/storage/e;->aGk:Lcom/android/settings/deviceinfo/StorageItemPreference;

    const-string/jumbo v0, "pref_other_apps"

    invoke-virtual {p1, v0}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Lcom/android/settings/deviceinfo/StorageItemPreference;

    iput-object v0, p0, Lcom/android/settings/deviceinfo/storage/e;->aGf:Lcom/android/settings/deviceinfo/StorageItemPreference;

    const-string/jumbo v0, "pref_system"

    invoke-virtual {p1, v0}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Lcom/android/settings/deviceinfo/StorageItemPreference;

    iput-object v0, p0, Lcom/android/settings/deviceinfo/storage/e;->aGo:Lcom/android/settings/deviceinfo/StorageItemPreference;

    const-string/jumbo v0, "pref_files"

    invoke-virtual {p1, v0}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Lcom/android/settings/deviceinfo/StorageItemPreference;

    iput-object v0, p0, Lcom/android/settings/deviceinfo/storage/e;->aGh:Lcom/android/settings/deviceinfo/StorageItemPreference;

    invoke-direct {p0}, Lcom/android/settings/deviceinfo/storage/e;->atU()V

    return-void
.end method

.method public l()Ljava/lang/String;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public p()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public setVolume(Landroid/os/storage/VolumeInfo;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/deviceinfo/storage/e;->aGr:Landroid/os/storage/VolumeInfo;

    invoke-direct {p0}, Lcom/android/settings/deviceinfo/storage/e;->atU()V

    return-void
.end method
