.class public Lcom/android/settings/deviceinfo/storage/f;
.super Lcom/android/settings/utils/k;
.source "StorageAsyncLoader.java"


# instance fields
.field private aGs:Landroid/util/ArraySet;

.field private aGt:Lcom/android/settingslib/b/H;

.field private aGu:Lcom/android/settings/applications/UserManagerWrapper;

.field private aGv:Ljava/lang/String;

.field private mPackageManager:Lcom/android/settings/applications/PackageManagerWrapper;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/android/settings/applications/UserManagerWrapper;Ljava/lang/String;Lcom/android/settingslib/b/H;Lcom/android/settings/applications/PackageManagerWrapper;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/settings/utils/k;-><init>(Landroid/content/Context;)V

    iput-object p2, p0, Lcom/android/settings/deviceinfo/storage/f;->aGu:Lcom/android/settings/applications/UserManagerWrapper;

    iput-object p3, p0, Lcom/android/settings/deviceinfo/storage/f;->aGv:Ljava/lang/String;

    iput-object p4, p0, Lcom/android/settings/deviceinfo/storage/f;->aGt:Lcom/android/settingslib/b/H;

    iput-object p5, p0, Lcom/android/settings/deviceinfo/storage/f;->mPackageManager:Lcom/android/settings/applications/PackageManagerWrapper;

    return-void
.end method

.method private atV(I)Lcom/android/settings/deviceinfo/storage/g;
    .locals 14

    const/4 v0, 0x0

    const-string/jumbo v1, "StorageAsyncLoader"

    const-string/jumbo v2, "Loading apps"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/android/settings/deviceinfo/storage/f;->mPackageManager:Lcom/android/settings/applications/PackageManagerWrapper;

    invoke-interface {v1, v0, p1}, Lcom/android/settings/applications/PackageManagerWrapper;->uV(II)Ljava/util/List;

    move-result-object v4

    new-instance v5, Lcom/android/settings/deviceinfo/storage/g;

    invoke-direct {v5}, Lcom/android/settings/deviceinfo/storage/g;-><init>()V

    invoke-static {p1}, Landroid/os/UserHandle;->of(I)Landroid/os/UserHandle;

    move-result-object v6

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v7

    move v1, v0

    :goto_0
    if-ge v1, v7, :cond_3

    invoke-interface {v4, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/ApplicationInfo;

    :try_start_0
    iget-object v2, p0, Lcom/android/settings/deviceinfo/storage/f;->aGt:Lcom/android/settingslib/b/H;

    iget-object v3, p0, Lcom/android/settings/deviceinfo/storage/f;->aGv:Ljava/lang/String;

    iget-object v8, v0, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v2, v3, v8, v6}, Lcom/android/settingslib/b/H;->cfO(Ljava/lang/String;Ljava/lang/String;Landroid/os/UserHandle;)Lcom/android/settingslib/b/J;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v8

    invoke-interface {v8}, Lcom/android/settingslib/b/J;->cfQ()J

    move-result-wide v2

    iget-object v9, p0, Lcom/android/settings/deviceinfo/storage/f;->aGt:Lcom/android/settingslib/b/H;

    iget-object v10, p0, Lcom/android/settings/deviceinfo/storage/f;->aGv:Ljava/lang/String;

    iget v11, v0, Landroid/content/pm/ApplicationInfo;->uid:I

    invoke-virtual {v9, v10, v11}, Lcom/android/settingslib/b/H;->cfN(Ljava/lang/String;I)J

    move-result-wide v10

    invoke-interface {v8}, Lcom/android/settingslib/b/J;->cfR()J

    move-result-wide v12

    cmp-long v9, v10, v12

    if-gez v9, :cond_0

    sub-long/2addr v2, v12

    add-long/2addr v2, v10

    :cond_0
    iget-object v9, p0, Lcom/android/settings/deviceinfo/storage/f;->aGs:Landroid/util/ArraySet;

    iget-object v10, v0, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v9, v10}, Landroid/util/ArraySet;->contains(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_1

    invoke-interface {v8}, Lcom/android/settingslib/b/J;->cfS()J

    move-result-wide v8

    add-long/2addr v2, v8

    iget-object v8, p0, Lcom/android/settings/deviceinfo/storage/f;->aGs:Landroid/util/ArraySet;

    iget-object v9, v0, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v8, v9}, Landroid/util/ArraySet;->add(Ljava/lang/Object;)Z

    :cond_1
    iget v8, v0, Landroid/content/pm/ApplicationInfo;->category:I

    packed-switch v8, :pswitch_data_0

    iget v0, v0, Landroid/content/pm/ApplicationInfo;->flags:I

    const/high16 v8, 0x2000000

    and-int/2addr v0, v8

    if-eqz v0, :cond_2

    iget-wide v8, v5, Lcom/android/settings/deviceinfo/storage/g;->aGA:J

    add-long/2addr v2, v8

    iput-wide v2, v5, Lcom/android/settings/deviceinfo/storage/g;->aGA:J

    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string/jumbo v2, "StorageAsyncLoader"

    const-string/jumbo v3, "App unexpectedly not found"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1

    :pswitch_0
    iget-wide v8, v5, Lcom/android/settings/deviceinfo/storage/g;->aGA:J

    add-long/2addr v2, v8

    iput-wide v2, v5, Lcom/android/settings/deviceinfo/storage/g;->aGA:J

    goto :goto_1

    :pswitch_1
    iget-wide v8, v5, Lcom/android/settings/deviceinfo/storage/g;->aGz:J

    add-long/2addr v2, v8

    iput-wide v2, v5, Lcom/android/settings/deviceinfo/storage/g;->aGz:J

    goto :goto_1

    :pswitch_2
    iget-wide v8, v5, Lcom/android/settings/deviceinfo/storage/g;->aGy:J

    add-long/2addr v2, v8

    iput-wide v2, v5, Lcom/android/settings/deviceinfo/storage/g;->aGy:J

    goto :goto_1

    :cond_2
    iget-wide v8, v5, Lcom/android/settings/deviceinfo/storage/g;->aGx:J

    add-long/2addr v2, v8

    iput-wide v2, v5, Lcom/android/settings/deviceinfo/storage/g;->aGx:J

    goto :goto_1

    :cond_3
    const-string/jumbo v0, "StorageAsyncLoader"

    const-string/jumbo v1, "Loading external stats"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :try_start_1
    iget-object v0, p0, Lcom/android/settings/deviceinfo/storage/f;->aGt:Lcom/android/settingslib/b/H;

    iget-object v1, p0, Lcom/android/settings/deviceinfo/storage/f;->aGv:Ljava/lang/String;

    invoke-static {p1}, Landroid/os/UserHandle;->of(I)Landroid/os/UserHandle;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/android/settingslib/b/H;->cfM(Ljava/lang/String;Landroid/os/UserHandle;)Lcom/android/settingslib/b/I;

    move-result-object v0

    iput-object v0, v5, Lcom/android/settings/deviceinfo/storage/g;->aGw:Lcom/android/settingslib/b/I;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    :goto_2
    const-string/jumbo v0, "StorageAsyncLoader"

    const-string/jumbo v1, "Obtaining result completed"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-object v5

    :catch_1
    move-exception v0

    const-string/jumbo v1, "StorageAsyncLoader"

    invoke-static {v1, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private atW()Landroid/util/SparseArray;
    .locals 6

    new-instance v0, Landroid/util/ArraySet;

    invoke-direct {v0}, Landroid/util/ArraySet;-><init>()V

    iput-object v0, p0, Lcom/android/settings/deviceinfo/storage/f;->aGs:Landroid/util/ArraySet;

    new-instance v2, Landroid/util/SparseArray;

    invoke-direct {v2}, Landroid/util/SparseArray;-><init>()V

    iget-object v0, p0, Lcom/android/settings/deviceinfo/storage/f;->aGu:Lcom/android/settings/applications/UserManagerWrapper;

    invoke-interface {v0}, Lcom/android/settings/applications/UserManagerWrapper;->wc()Ljava/util/List;

    move-result-object v3

    new-instance v0, Lcom/android/settings/deviceinfo/storage/m;

    invoke-direct {v0, p0}, Lcom/android/settings/deviceinfo/storage/m;-><init>(Lcom/android/settings/deviceinfo/storage/f;)V

    invoke-static {v3, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    const/4 v0, 0x0

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v4

    move v1, v0

    :goto_0
    if-ge v1, v4, :cond_0

    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/UserInfo;

    iget v5, v0, Landroid/content/pm/UserInfo;->id:I

    iget v0, v0, Landroid/content/pm/UserInfo;->id:I

    invoke-direct {p0, v0}, Lcom/android/settings/deviceinfo/storage/f;->atV(I)Lcom/android/settings/deviceinfo/storage/g;

    move-result-object v0

    invoke-virtual {v2, v5, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    return-object v2
.end method


# virtual methods
.method protected atX(Landroid/util/SparseArray;)V
    .locals 0

    return-void
.end method

.method public loadInBackground()Landroid/util/SparseArray;
    .locals 1

    invoke-direct {p0}, Lcom/android/settings/deviceinfo/storage/f;->atW()Landroid/util/SparseArray;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic loadInBackground()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/android/settings/deviceinfo/storage/f;->loadInBackground()Landroid/util/SparseArray;

    move-result-object v0

    return-object v0
.end method

.method protected bridge synthetic onDiscardResult(Ljava/lang/Object;)V
    .locals 0

    check-cast p1, Landroid/util/SparseArray;

    invoke-virtual {p0, p1}, Lcom/android/settings/deviceinfo/storage/f;->atX(Landroid/util/SparseArray;)V

    return-void
.end method
