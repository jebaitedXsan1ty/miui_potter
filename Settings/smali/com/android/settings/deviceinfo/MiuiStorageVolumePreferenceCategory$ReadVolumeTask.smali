.class Lcom/android/settings/deviceinfo/MiuiStorageVolumePreferenceCategory$ReadVolumeTask;
.super Landroid/os/AsyncTask;
.source "MiuiStorageVolumePreferenceCategory.java"


# instance fields
.field private aJf:Ljava/lang/ref/WeakReference;


# direct methods
.method constructor <init>(Lcom/android/settings/deviceinfo/MiuiStorageVolumePreferenceCategory;)V
    .locals 1

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/android/settings/deviceinfo/MiuiStorageVolumePreferenceCategory$ReadVolumeTask;->aJf:Ljava/lang/ref/WeakReference;

    return-void
.end method


# virtual methods
.method protected varargs awr([Landroid/os/storage/VolumeInfo;)Landroid/util/Pair;
    .locals 5

    const/4 v0, 0x0

    aget-object v0, p1, v0

    invoke-virtual {v0}, Landroid/os/storage/VolumeInfo;->getPath()Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->getUsableSpace()J

    move-result-wide v2

    invoke-virtual {v0}, Ljava/io/File;->getTotalSpace()J

    move-result-wide v0

    new-instance v4, Landroid/util/Pair;

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-direct {v4, v0, v1}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    return-object v4
.end method

.method protected aws(Landroid/util/Pair;)V
    .locals 6

    iget-object v0, p0, Lcom/android/settings/deviceinfo/MiuiStorageVolumePreferenceCategory$ReadVolumeTask;->aJf:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/deviceinfo/MiuiStorageVolumePreferenceCategory$ReadVolumeTask;->aJf:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settings/deviceinfo/MiuiStorageVolumePreferenceCategory;

    iget-object v1, p1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    iget-object v1, p1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-static {v0, v2, v3, v4, v5}, Lcom/android/settings/deviceinfo/MiuiStorageVolumePreferenceCategory;->awq(Lcom/android/settings/deviceinfo/MiuiStorageVolumePreferenceCategory;JJ)V

    :cond_0
    return-void
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    check-cast p1, [Landroid/os/storage/VolumeInfo;

    invoke-virtual {p0, p1}, Lcom/android/settings/deviceinfo/MiuiStorageVolumePreferenceCategory$ReadVolumeTask;->awr([Landroid/os/storage/VolumeInfo;)Landroid/util/Pair;

    move-result-object v0

    return-object v0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    check-cast p1, Landroid/util/Pair;

    invoke-virtual {p0, p1}, Lcom/android/settings/deviceinfo/MiuiStorageVolumePreferenceCategory$ReadVolumeTask;->aws(Landroid/util/Pair;)V

    return-void
.end method
