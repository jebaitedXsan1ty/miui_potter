.class final Lcom/android/settings/deviceinfo/PrivateVolumeSettings$4;
.super Landroid/content/BroadcastReceiver;
.source "PrivateVolumeSettings.java"


# instance fields
.field final synthetic aLQ:Lcom/android/settings/deviceinfo/PrivateVolumeSettings;


# direct methods
.method constructor <init>(Lcom/android/settings/deviceinfo/PrivateVolumeSettings;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/deviceinfo/PrivateVolumeSettings$4;->aLQ:Lcom/android/settings/deviceinfo/PrivateVolumeSettings;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 8

    iget-object v0, p0, Lcom/android/settings/deviceinfo/PrivateVolumeSettings$4;->aLQ:Lcom/android/settings/deviceinfo/PrivateVolumeSettings;

    invoke-static {v0}, Lcom/android/settings/deviceinfo/PrivateVolumeSettings;->axz(Lcom/android/settings/deviceinfo/PrivateVolumeSettings;)Lcom/android/settings/deviceinfo/UsageBarPreference;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/deviceinfo/PrivateVolumeSettings$4;->aLQ:Lcom/android/settings/deviceinfo/PrivateVolumeSettings;

    invoke-virtual {v1}, Lcom/android/settings/deviceinfo/PrivateVolumeSettings;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/android/settings/deviceinfo/PrivateVolumeSettings$4;->aLQ:Lcom/android/settings/deviceinfo/PrivateVolumeSettings;

    invoke-static {v3}, Lcom/android/settings/deviceinfo/PrivateVolumeSettings;->axA(Lcom/android/settings/deviceinfo/PrivateVolumeSettings;)Lcom/android/settings/dc;

    iget-object v3, p0, Lcom/android/settings/deviceinfo/PrivateVolumeSettings$4;->aLQ:Lcom/android/settings/deviceinfo/PrivateVolumeSettings;

    invoke-virtual {v3}, Lcom/android/settings/deviceinfo/PrivateVolumeSettings;->getActivity()Landroid/app/Activity;

    move-result-object v3

    const-string/jumbo v4, "details.availSize"

    const-wide/16 v6, 0x0

    invoke-virtual {p2, v4, v6, v7}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v4

    invoke-static {v3, v4, v5}, Lcom/android/settings/dc;->bYJ(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    aput-object v3, v2, v4

    iget-object v3, p0, Lcom/android/settings/deviceinfo/PrivateVolumeSettings$4;->aLQ:Lcom/android/settings/deviceinfo/PrivateVolumeSettings;

    invoke-static {v3}, Lcom/android/settings/deviceinfo/PrivateVolumeSettings;->axA(Lcom/android/settings/deviceinfo/PrivateVolumeSettings;)Lcom/android/settings/dc;

    iget-object v3, p0, Lcom/android/settings/deviceinfo/PrivateVolumeSettings$4;->aLQ:Lcom/android/settings/deviceinfo/PrivateVolumeSettings;

    invoke-virtual {v3}, Lcom/android/settings/deviceinfo/PrivateVolumeSettings;->getActivity()Landroid/app/Activity;

    move-result-object v3

    iget-object v4, p0, Lcom/android/settings/deviceinfo/PrivateVolumeSettings$4;->aLQ:Lcom/android/settings/deviceinfo/PrivateVolumeSettings;

    invoke-static {v4}, Lcom/android/settings/deviceinfo/PrivateVolumeSettings;->axx(Lcom/android/settings/deviceinfo/PrivateVolumeSettings;)J

    move-result-wide v4

    invoke-static {v3, v4, v5}, Lcom/android/settings/dc;->bYJ(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x1

    aput-object v3, v2, v4

    const v3, 0x7f120a6d

    invoke-virtual {v1, v3, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/settings/deviceinfo/UsageBarPreference;->setTitle(Ljava/lang/CharSequence;)V

    return-void
.end method
