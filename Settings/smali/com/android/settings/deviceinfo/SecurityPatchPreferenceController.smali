.class public Lcom/android/settings/deviceinfo/SecurityPatchPreferenceController;
.super Lcom/android/settings/core/e;
.source "SecurityPatchPreferenceController.java"


# instance fields
.field private final aKX:Ljava/lang/String;

.field private final mPackageManager:Landroid/content/pm/PackageManager;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    invoke-direct {p0, p1}, Lcom/android/settings/core/e;-><init>(Landroid/content/Context;)V

    iget-object v0, p0, Lcom/android/settings/deviceinfo/SecurityPatchPreferenceController;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/deviceinfo/SecurityPatchPreferenceController;->mPackageManager:Landroid/content/pm/PackageManager;

    invoke-static {}, Lcom/android/settingslib/I;->csp()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/deviceinfo/SecurityPatchPreferenceController;->aKX:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public fm(Landroid/preference/Preference;)Z
    .locals 3

    const/4 v2, 0x0

    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "security_patch"

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    return v2

    :cond_0
    iget-object v0, p0, Lcom/android/settings/deviceinfo/SecurityPatchPreferenceController;->mPackageManager:Landroid/content/pm/PackageManager;

    invoke-virtual {p1}, Landroid/preference/Preference;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1, v2}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    const-string/jumbo v0, "SecurityPatchPref"

    const-string/jumbo v1, "Stop click action on security_patch: queryIntentActivities() returns empty"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x1

    return v0

    :cond_1
    return v2
.end method

.method public i(Landroid/preference/PreferenceScreen;)V
    .locals 2

    invoke-super {p0, p1}, Lcom/android/settings/core/e;->i(Landroid/preference/PreferenceScreen;)V

    const-string/jumbo v0, "security_patch"

    invoke-virtual {p1, v0}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/android/settings/deviceinfo/SecurityPatchPreferenceController;->aKX:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    :cond_0
    return-void
.end method

.method public l()Ljava/lang/String;
    .locals 1

    const-string/jumbo v0, "security_patch"

    return-object v0
.end method

.method public p()Z
    .locals 1

    iget-object v0, p0, Lcom/android/settings/deviceinfo/SecurityPatchPreferenceController;->aKX:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    return v0
.end method
