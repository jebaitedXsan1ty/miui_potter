.class public Lcom/android/settings/deviceinfo/MiuiUsbSettings;
.super Lcom/android/settings/MiuiSettingsPreferenceFragment;
.source "MiuiUsbSettings.java"


# instance fields
.field private aJM:Landroid/preference/CheckBoxPreference;

.field private aJN:Z

.field private aJO:Ljava/lang/String;

.field private aJP:Z

.field private aJQ:Ljava/lang/String;

.field private aJR:Landroid/preference/CheckBoxPreference;

.field private aJS:Landroid/preference/CheckBoxPreference;

.field private aJT:Ljava/lang/String;

.field private aJU:Landroid/preference/CheckBoxPreference;

.field private aJV:Ljava/lang/String;

.field private final aJW:Landroid/content/BroadcastReceiver;

.field private aJX:Landroid/hardware/usb/UsbManager;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/android/settings/MiuiSettingsPreferenceFragment;-><init>()V

    new-instance v0, Lcom/android/settings/deviceinfo/MiuiUsbSettings$1;

    invoke-direct {v0, p0}, Lcom/android/settings/deviceinfo/MiuiUsbSettings$1;-><init>(Lcom/android/settings/deviceinfo/MiuiUsbSettings;)V

    iput-object v0, p0, Lcom/android/settings/deviceinfo/MiuiUsbSettings;->aJW:Landroid/content/BroadcastReceiver;

    return-void
.end method

.method private awY()Landroid/preference/PreferenceScreen;
    .locals 4

    const/4 v3, 0x0

    invoke-virtual {p0}, Lcom/android/settings/deviceinfo/MiuiUsbSettings;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/preference/PreferenceScreen;->removeAll()V

    :cond_0
    const v0, 0x7f150100

    invoke-virtual {p0, v0}, Lcom/android/settings/deviceinfo/MiuiUsbSettings;->addPreferencesFromResource(I)V

    invoke-virtual {p0}, Lcom/android/settings/deviceinfo/MiuiUsbSettings;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v1

    const-string/jumbo v0, "usb_mtp"

    invoke-virtual {v1, v0}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    iput-object v0, p0, Lcom/android/settings/deviceinfo/MiuiUsbSettings;->aJS:Landroid/preference/CheckBoxPreference;

    const-string/jumbo v0, "usb_ptp"

    invoke-virtual {v1, v0}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    iput-object v0, p0, Lcom/android/settings/deviceinfo/MiuiUsbSettings;->aJU:Landroid/preference/CheckBoxPreference;

    const-string/jumbo v0, "usb_msd"

    invoke-virtual {v1, v0}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    iput-object v0, p0, Lcom/android/settings/deviceinfo/MiuiUsbSettings;->aJR:Landroid/preference/CheckBoxPreference;

    const-string/jumbo v0, "usb_charging"

    invoke-virtual {v1, v0}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    iput-object v0, p0, Lcom/android/settings/deviceinfo/MiuiUsbSettings;->aJM:Landroid/preference/CheckBoxPreference;

    invoke-virtual {p0}, Lcom/android/settings/deviceinfo/MiuiUsbSettings;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f120455

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/deviceinfo/MiuiUsbSettings;->aJT:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/android/settings/deviceinfo/MiuiUsbSettings;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f120456

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/deviceinfo/MiuiUsbSettings;->aJV:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/android/settings/deviceinfo/MiuiUsbSettings;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f120454

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/deviceinfo/MiuiUsbSettings;->aJQ:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/android/settings/deviceinfo/MiuiUsbSettings;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f120451

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/deviceinfo/MiuiUsbSettings;->aJO:Ljava/lang/String;

    sget-boolean v0, Lmiui/os/Build;->IS_MITWO:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/settings/deviceinfo/MiuiUsbSettings;->aJR:Landroid/preference/CheckBoxPreference;

    const v2, 0x7f121359

    invoke-virtual {v0, v2}, Landroid/preference/CheckBoxPreference;->setTitle(I)V

    iget-object v0, p0, Lcom/android/settings/deviceinfo/MiuiUsbSettings;->aJR:Landroid/preference/CheckBoxPreference;

    const v2, 0x7f121358

    invoke-virtual {v0, v2}, Landroid/preference/CheckBoxPreference;->setSummary(I)V

    :cond_1
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/settings/deviceinfo/MiuiUsbSettings;->aJP:Z

    iget-object v0, p0, Lcom/android/settings/deviceinfo/MiuiUsbSettings;->aJR:Landroid/preference/CheckBoxPreference;

    if-eqz v0, :cond_2

    iget-boolean v0, p0, Lcom/android/settings/deviceinfo/MiuiUsbSettings;->aJP:Z

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/settings/deviceinfo/MiuiUsbSettings;->aJR:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v1, v0}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    iput-object v3, p0, Lcom/android/settings/deviceinfo/MiuiUsbSettings;->aJR:Landroid/preference/CheckBoxPreference;

    :cond_2
    sget-boolean v0, Lmiui/os/Build;->IS_CTA_BUILD:Z

    if-nez v0, :cond_4

    sget-boolean v0, Lmiui/os/Build;->IS_CM_CUSTOMIZATION_TEST:Z

    :goto_0
    iput-boolean v0, p0, Lcom/android/settings/deviceinfo/MiuiUsbSettings;->aJN:Z

    iget-object v0, p0, Lcom/android/settings/deviceinfo/MiuiUsbSettings;->aJM:Landroid/preference/CheckBoxPreference;

    if-eqz v0, :cond_3

    iget-boolean v0, p0, Lcom/android/settings/deviceinfo/MiuiUsbSettings;->aJN:Z

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/android/settings/deviceinfo/MiuiUsbSettings;->aJM:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v1, v0}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    iput-object v3, p0, Lcom/android/settings/deviceinfo/MiuiUsbSettings;->aJM:Landroid/preference/CheckBoxPreference;

    :cond_3
    return-object v1

    :cond_4
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private awZ(Ljava/lang/String;)V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/deviceinfo/MiuiUsbSettings;->aJS:Landroid/preference/CheckBoxPreference;

    iget-object v1, p0, Lcom/android/settings/deviceinfo/MiuiUsbSettings;->aJT:Ljava/lang/String;

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    iget-object v0, p0, Lcom/android/settings/deviceinfo/MiuiUsbSettings;->aJU:Landroid/preference/CheckBoxPreference;

    iget-object v1, p0, Lcom/android/settings/deviceinfo/MiuiUsbSettings;->aJV:Ljava/lang/String;

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    iget-boolean v0, p0, Lcom/android/settings/deviceinfo/MiuiUsbSettings;->aJP:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/deviceinfo/MiuiUsbSettings;->aJR:Landroid/preference/CheckBoxPreference;

    iget-object v1, p0, Lcom/android/settings/deviceinfo/MiuiUsbSettings;->aJQ:Ljava/lang/String;

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    :cond_0
    iget-boolean v0, p0, Lcom/android/settings/deviceinfo/MiuiUsbSettings;->aJN:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/settings/deviceinfo/MiuiUsbSettings;->aJM:Landroid/preference/CheckBoxPreference;

    iget-object v1, p0, Lcom/android/settings/deviceinfo/MiuiUsbSettings;->aJO:Ljava/lang/String;

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    :cond_1
    return-void
.end method

.method static synthetic axa(Lcom/android/settings/deviceinfo/MiuiUsbSettings;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/settings/deviceinfo/MiuiUsbSettings;->awZ(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public cC()Ljava/lang/String;
    .locals 1

    const-class v0, Lcom/android/settings/deviceinfo/MiuiUsbSettings;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1

    invoke-super {p0, p1}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->onCreate(Landroid/os/Bundle;)V

    const-string/jumbo v0, "usb"

    invoke-virtual {p0, v0}, Lcom/android/settings/deviceinfo/MiuiUsbSettings;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/usb/UsbManager;

    iput-object v0, p0, Lcom/android/settings/deviceinfo/MiuiUsbSettings;->aJX:Landroid/hardware/usb/UsbManager;

    return-void
.end method

.method public onPause()V
    .locals 2

    invoke-super {p0}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->onPause()V

    invoke-virtual {p0}, Lcom/android/settings/deviceinfo/MiuiUsbSettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/deviceinfo/MiuiUsbSettings;->aJW:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/app/Activity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    return-void
.end method

.method public onPreferenceTreeClick(Landroid/preference/PreferenceScreen;Landroid/preference/Preference;)Z
    .locals 3

    const/4 v2, 0x1

    invoke-static {}, Lcom/android/settings/aq;->bqE()Z

    move-result v0

    if-eqz v0, :cond_0

    return v2

    :cond_0
    iget-object v0, p0, Lcom/android/settings/deviceinfo/MiuiUsbSettings;->aJS:Landroid/preference/CheckBoxPreference;

    if-ne p2, v0, :cond_1

    iget-object v0, p0, Lcom/android/settings/deviceinfo/MiuiUsbSettings;->aJS:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/android/settings/deviceinfo/MiuiUsbSettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/deviceinfo/MiuiUsbSettings;->aJT:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/android/settings/dc;->bYF(Landroid/content/Context;Ljava/lang/String;Z)V

    iget-object v0, p0, Lcom/android/settings/deviceinfo/MiuiUsbSettings;->aJT:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/android/settings/deviceinfo/MiuiUsbSettings;->awZ(Ljava/lang/String;)V

    :goto_0
    invoke-super {p0, p1, p2}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->onPreferenceTreeClick(Landroid/preference/PreferenceScreen;Landroid/preference/Preference;)Z

    move-result v0

    return v0

    :cond_1
    iget-object v0, p0, Lcom/android/settings/deviceinfo/MiuiUsbSettings;->aJU:Landroid/preference/CheckBoxPreference;

    if-ne p2, v0, :cond_2

    iget-object v0, p0, Lcom/android/settings/deviceinfo/MiuiUsbSettings;->aJU:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lcom/android/settings/deviceinfo/MiuiUsbSettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/deviceinfo/MiuiUsbSettings;->aJV:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/android/settings/dc;->bYF(Landroid/content/Context;Ljava/lang/String;Z)V

    iget-object v0, p0, Lcom/android/settings/deviceinfo/MiuiUsbSettings;->aJV:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/android/settings/deviceinfo/MiuiUsbSettings;->awZ(Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/android/settings/deviceinfo/MiuiUsbSettings;->aJR:Landroid/preference/CheckBoxPreference;

    if-ne p2, v0, :cond_3

    iget-boolean v0, p0, Lcom/android/settings/deviceinfo/MiuiUsbSettings;->aJP:Z

    if-eqz v0, :cond_3

    invoke-virtual {p0}, Lcom/android/settings/deviceinfo/MiuiUsbSettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/deviceinfo/MiuiUsbSettings;->aJQ:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/android/settings/dc;->bYF(Landroid/content/Context;Ljava/lang/String;Z)V

    iget-object v0, p0, Lcom/android/settings/deviceinfo/MiuiUsbSettings;->aJQ:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/android/settings/deviceinfo/MiuiUsbSettings;->awZ(Ljava/lang/String;)V

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/android/settings/deviceinfo/MiuiUsbSettings;->aJM:Landroid/preference/CheckBoxPreference;

    if-ne p2, v0, :cond_4

    iget-boolean v0, p0, Lcom/android/settings/deviceinfo/MiuiUsbSettings;->aJN:Z

    if-eqz v0, :cond_4

    invoke-virtual {p0}, Lcom/android/settings/deviceinfo/MiuiUsbSettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/deviceinfo/MiuiUsbSettings;->aJO:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/android/settings/dc;->bYF(Landroid/content/Context;Ljava/lang/String;Z)V

    iget-object v0, p0, Lcom/android/settings/deviceinfo/MiuiUsbSettings;->aJO:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/android/settings/deviceinfo/MiuiUsbSettings;->awZ(Ljava/lang/String;)V

    goto :goto_0

    :cond_4
    invoke-virtual {p0}, Lcom/android/settings/deviceinfo/MiuiUsbSettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const-string/jumbo v1, "none"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/android/settings/dc;->bYF(Landroid/content/Context;Ljava/lang/String;Z)V

    goto :goto_0
.end method

.method public onResume()V
    .locals 4

    invoke-super {p0}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->onResume()V

    invoke-direct {p0}, Lcom/android/settings/deviceinfo/MiuiUsbSettings;->awY()Landroid/preference/PreferenceScreen;

    invoke-virtual {p0}, Lcom/android/settings/deviceinfo/MiuiUsbSettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/deviceinfo/MiuiUsbSettings;->aJW:Landroid/content/BroadcastReceiver;

    new-instance v2, Landroid/content/IntentFilter;

    const-string/jumbo v3, "android.hardware.usb.action.USB_STATE"

    invoke-direct {v2, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/Activity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    return-void
.end method
