.class public Lcom/android/settings/ak;
.super Ljava/lang/Object;
.source "NotificationAppListSettings.java"


# instance fields
.field bBD:Ljava/lang/String;

.field private bBE:Landroid/content/pm/ApplicationInfo;

.field private bBF:Ljava/lang/String;

.field private bBG:Z

.field private bBH:I

.field final synthetic bBI:Lcom/android/settings/NotificationAppListSettings;


# direct methods
.method public constructor <init>(Lcom/android/settings/NotificationAppListSettings;Landroid/content/pm/ApplicationInfo;I)V
    .locals 4

    const/4 v3, 0x0

    iput-object p1, p0, Lcom/android/settings/ak;->bBI:Lcom/android/settings/NotificationAppListSettings;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, -0x1

    iput v0, p0, Lcom/android/settings/ak;->bBH:I

    iput-boolean v3, p0, Lcom/android/settings/ak;->bBG:Z

    iput-object p2, p0, Lcom/android/settings/ak;->bBE:Landroid/content/pm/ApplicationInfo;

    iget-object v0, p1, Lcom/android/settings/NotificationAppListSettings;->mPackageManager:Landroid/content/pm/PackageManager;

    invoke-virtual {p2, v0}, Landroid/content/pm/ApplicationInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "\\u00A0"

    const-string/jumbo v2, " "

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/ak;->bBD:Ljava/lang/String;

    iget-object v0, p1, Lcom/android/settings/NotificationAppListSettings;->mContext:Landroid/content/Context;

    invoke-virtual {p0}, Lcom/android/settings/ak;->bpz()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, p3}, Lcom/android/settings/NotificationAppListSettings;->bpw(Landroid/content/Context;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/ak;->bBF:Ljava/lang/String;

    iput v3, p0, Lcom/android/settings/ak;->bBH:I

    return-void
.end method

.method public constructor <init>(Lcom/android/settings/NotificationAppListSettings;Ljava/lang/String;)V
    .locals 1

    iput-object p1, p0, Lcom/android/settings/ak;->bBI:Lcom/android/settings/NotificationAppListSettings;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, -0x1

    iput v0, p0, Lcom/android/settings/ak;->bBH:I

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/settings/ak;->bBG:Z

    iput-object p2, p0, Lcom/android/settings/ak;->bBD:Ljava/lang/String;

    const/4 v0, 0x1

    iput v0, p0, Lcom/android/settings/ak;->bBH:I

    return-void
.end method

.method static synthetic bpA(Lcom/android/settings/ak;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/ak;->bBF:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic bpB(Lcom/android/settings/ak;)I
    .locals 1

    iget v0, p0, Lcom/android/settings/ak;->bBH:I

    return v0
.end method


# virtual methods
.method public bpz()Ljava/lang/String;
    .locals 2

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/android/settings/ak;->bBE:Landroid/content/pm/ApplicationInfo;

    if-nez v1, :cond_0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/android/settings/ak;->bBE:Landroid/content/pm/ApplicationInfo;

    iget-object v0, v0, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    goto :goto_0
.end method
