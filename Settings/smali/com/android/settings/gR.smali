.class final Lcom/android/settings/gR;
.super Ljava/lang/Object;
.source "MiuiSecurityCommonSettings.java"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceChangeListener;


# instance fields
.field final synthetic cmq:Lcom/android/settings/MiuiSecurityCommonSettings$MiuiSecurityCommonSettingsFragment;


# direct methods
.method constructor <init>(Lcom/android/settings/MiuiSecurityCommonSettings$MiuiSecurityCommonSettingsFragment;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/gR;->cmq:Lcom/android/settings/MiuiSecurityCommonSettings$MiuiSecurityCommonSettingsFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 5

    const/16 v4, 0x69

    const/4 v3, 0x0

    const-string/jumbo v0, "change_password"

    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    check-cast p2, Ljava/lang/String;

    iget-object v0, p0, Lcom/android/settings/gR;->cmq:Lcom/android/settings/MiuiSecurityCommonSettings$MiuiSecurityCommonSettingsFragment;

    invoke-virtual {v0}, Lcom/android/settings/MiuiSecurityCommonSettings$MiuiSecurityCommonSettingsFragment;->getTargetFragment()Landroid/app/Fragment;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/gR;->cmq:Lcom/android/settings/MiuiSecurityCommonSettings$MiuiSecurityCommonSettingsFragment;

    invoke-virtual {v1}, Lcom/android/settings/MiuiSecurityCommonSettings$MiuiSecurityCommonSettingsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string/jumbo v2, "add_keyguard_password_then_add_fingerprint"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string/jumbo v2, "add_keyguard_password_then_add_face_recoginition"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string/jumbo v1, "facial"

    invoke-virtual {v1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v0, p0, Lcom/android/settings/gR;->cmq:Lcom/android/settings/MiuiSecurityCommonSettings$MiuiSecurityCommonSettingsFragment;

    const v1, 0x8000

    invoke-static {v1, v0}, Lcom/android/settings/MiuiSecurityChooseUnlock;->bpG(ILcom/android/settings/KeyguardSettingsPreferenceFragment;)V

    :cond_0
    :goto_0
    return v3

    :cond_1
    const-string/jumbo v1, "pattern"

    invoke-virtual {v1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/android/settings/gR;->cmq:Lcom/android/settings/MiuiSecurityCommonSettings$MiuiSecurityCommonSettingsFragment;

    const/high16 v1, 0x10000

    invoke-static {v1, v0}, Lcom/android/settings/MiuiSecurityChooseUnlock;->bpG(ILcom/android/settings/KeyguardSettingsPreferenceFragment;)V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/android/settings/gR;->cmq:Lcom/android/settings/MiuiSecurityCommonSettings$MiuiSecurityCommonSettingsFragment;

    invoke-static {v0, v4}, Lcom/android/settings/MiuiSecurityChooseUnlock;->bpK(Lcom/android/settings/KeyguardSettingsPreferenceFragment;I)V

    goto :goto_0

    :cond_3
    const-string/jumbo v1, "numerical"

    invoke-virtual {v1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/android/settings/gR;->cmq:Lcom/android/settings/MiuiSecurityCommonSettings$MiuiSecurityCommonSettingsFragment;

    const/high16 v1, 0x20000

    invoke-static {v1, v0}, Lcom/android/settings/MiuiSecurityChooseUnlock;->bpG(ILcom/android/settings/KeyguardSettingsPreferenceFragment;)V

    goto :goto_0

    :cond_4
    iget-object v0, p0, Lcom/android/settings/gR;->cmq:Lcom/android/settings/MiuiSecurityCommonSettings$MiuiSecurityCommonSettingsFragment;

    invoke-static {v0, v4}, Lcom/android/settings/MiuiSecurityChooseUnlock;->bpI(Lcom/android/settings/KeyguardSettingsPreferenceFragment;I)V

    goto :goto_0

    :cond_5
    const-string/jumbo v1, "mixed"

    invoke-virtual {v1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    if-nez v0, :cond_6

    iget-object v0, p0, Lcom/android/settings/gR;->cmq:Lcom/android/settings/MiuiSecurityCommonSettings$MiuiSecurityCommonSettingsFragment;

    const/high16 v1, 0x40000

    invoke-static {v1, v0}, Lcom/android/settings/MiuiSecurityChooseUnlock;->bpG(ILcom/android/settings/KeyguardSettingsPreferenceFragment;)V

    goto :goto_0

    :cond_6
    iget-object v0, p0, Lcom/android/settings/gR;->cmq:Lcom/android/settings/MiuiSecurityCommonSettings$MiuiSecurityCommonSettingsFragment;

    invoke-static {v0, v4}, Lcom/android/settings/MiuiSecurityChooseUnlock;->bpH(Lcom/android/settings/KeyguardSettingsPreferenceFragment;I)V

    goto :goto_0
.end method
