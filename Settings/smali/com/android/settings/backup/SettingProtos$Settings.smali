.class public final Lcom/android/settings/backup/SettingProtos$Settings;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "SettingProtos.java"

# interfaces
.implements Lcom/google/protobuf/MessageLiteOrBuilder;


# static fields
.field private static final aUC:Lcom/android/settings/backup/SettingProtos$Settings;

.field private static final serialVersionUID:J


# instance fields
.field private lock_:Ljava/util/List;

.field private memoizedIsInitialized:B

.field private memoizedSerializedSize:I

.field private secure_:Ljava/util/List;

.field private system_:Ljava/util/List;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/android/settings/backup/SettingProtos$Settings;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/android/settings/backup/SettingProtos$Settings;-><init>(Z)V

    sput-object v0, Lcom/android/settings/backup/SettingProtos$Settings;->aUC:Lcom/android/settings/backup/SettingProtos$Settings;

    sget-object v0, Lcom/android/settings/backup/SettingProtos$Settings;->aUC:Lcom/android/settings/backup/SettingProtos$Settings;

    invoke-direct {v0}, Lcom/android/settings/backup/SettingProtos$Settings;->aHv()V

    return-void
.end method

.method private constructor <init>(Lcom/android/settings/backup/p;)V
    .locals 1

    const/4 v0, -0x1

    invoke-direct {p0, p1}, Lcom/google/protobuf/GeneratedMessageLite;-><init>(Lcom/google/protobuf/GeneratedMessageLite$Builder;)V

    iput-byte v0, p0, Lcom/android/settings/backup/SettingProtos$Settings;->memoizedIsInitialized:B

    iput v0, p0, Lcom/android/settings/backup/SettingProtos$Settings;->memoizedSerializedSize:I

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/settings/backup/p;Lcom/android/settings/backup/SettingProtos$Settings;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/settings/backup/SettingProtos$Settings;-><init>(Lcom/android/settings/backup/p;)V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1

    const/4 v0, -0x1

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    iput-byte v0, p0, Lcom/android/settings/backup/SettingProtos$Settings;->memoizedIsInitialized:B

    iput v0, p0, Lcom/android/settings/backup/SettingProtos$Settings;->memoizedSerializedSize:I

    return-void
.end method

.method static synthetic aHA(Lcom/android/settings/backup/SettingProtos$Settings;)Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/backup/SettingProtos$Settings;->system_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic aHB(Lcom/android/settings/backup/SettingProtos$Settings;Ljava/util/List;)Ljava/util/List;
    .locals 0

    iput-object p1, p0, Lcom/android/settings/backup/SettingProtos$Settings;->lock_:Ljava/util/List;

    return-object p1
.end method

.method static synthetic aHC(Lcom/android/settings/backup/SettingProtos$Settings;Ljava/util/List;)Ljava/util/List;
    .locals 0

    iput-object p1, p0, Lcom/android/settings/backup/SettingProtos$Settings;->secure_:Ljava/util/List;

    return-object p1
.end method

.method static synthetic aHD(Lcom/android/settings/backup/SettingProtos$Settings;Ljava/util/List;)Ljava/util/List;
    .locals 0

    iput-object p1, p0, Lcom/android/settings/backup/SettingProtos$Settings;->system_:Ljava/util/List;

    return-object p1
.end method

.method public static aHr()Lcom/android/settings/backup/SettingProtos$Settings;
    .locals 1

    sget-object v0, Lcom/android/settings/backup/SettingProtos$Settings;->aUC:Lcom/android/settings/backup/SettingProtos$Settings;

    return-object v0
.end method

.method private aHv()V
    .locals 1

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/backup/SettingProtos$Settings;->system_:Ljava/util/List;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/backup/SettingProtos$Settings;->secure_:Ljava/util/List;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/backup/SettingProtos$Settings;->lock_:Ljava/util/List;

    return-void
.end method

.method public static aHw()Lcom/android/settings/backup/p;
    .locals 1

    invoke-static {}, Lcom/android/settings/backup/p;->aHM()Lcom/android/settings/backup/p;

    move-result-object v0

    return-object v0
.end method

.method public static aHx(Lcom/android/settings/backup/SettingProtos$Settings;)Lcom/android/settings/backup/p;
    .locals 1

    invoke-static {}, Lcom/android/settings/backup/SettingProtos$Settings;->aHw()Lcom/android/settings/backup/p;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/android/settings/backup/p;->aHL(Lcom/android/settings/backup/SettingProtos$Settings;)Lcom/android/settings/backup/p;

    move-result-object v0

    return-object v0
.end method

.method static synthetic aHy(Lcom/android/settings/backup/SettingProtos$Settings;)Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/backup/SettingProtos$Settings;->lock_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic aHz(Lcom/android/settings/backup/SettingProtos$Settings;)Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/backup/SettingProtos$Settings;->secure_:Ljava/util/List;

    return-object v0
.end method


# virtual methods
.method public aHs()Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/backup/SettingProtos$Settings;->lock_:Ljava/util/List;

    return-object v0
.end method

.method public aHt()Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/backup/SettingProtos$Settings;->secure_:Ljava/util/List;

    return-object v0
.end method

.method public aHu()Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/backup/SettingProtos$Settings;->system_:Ljava/util/List;

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/android/settings/backup/SettingProtos$Settings;
    .locals 1

    sget-object v0, Lcom/android/settings/backup/SettingProtos$Settings;->aUC:Lcom/android/settings/backup/SettingProtos$Settings;

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    invoke-virtual {p0}, Lcom/android/settings/backup/SettingProtos$Settings;->getDefaultInstanceForType()Lcom/android/settings/backup/SettingProtos$Settings;

    move-result-object v0

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 5

    const/4 v2, 0x0

    iget v0, p0, Lcom/android/settings/backup/SettingProtos$Settings;->memoizedSerializedSize:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    return v0

    :cond_0
    move v1, v2

    move v3, v2

    :goto_0
    iget-object v0, p0, Lcom/android/settings/backup/SettingProtos$Settings;->system_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    iget-object v0, p0, Lcom/android/settings/backup/SettingProtos$Settings;->system_:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/MessageLite;

    const/4 v4, 0x1

    invoke-static {v4, v0}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v0

    add-int/2addr v3, v0

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    move v1, v2

    :goto_1
    iget-object v0, p0, Lcom/android/settings/backup/SettingProtos$Settings;->secure_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    iget-object v0, p0, Lcom/android/settings/backup/SettingProtos$Settings;->secure_:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/MessageLite;

    const/4 v4, 0x2

    invoke-static {v4, v0}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v0

    add-int/2addr v3, v0

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    :cond_2
    :goto_2
    iget-object v0, p0, Lcom/android/settings/backup/SettingProtos$Settings;->lock_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_3

    iget-object v0, p0, Lcom/android/settings/backup/SettingProtos$Settings;->lock_:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/MessageLite;

    const/4 v1, 0x3

    invoke-static {v1, v0}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v0

    add-int/2addr v3, v0

    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    :cond_3
    iput v3, p0, Lcom/android/settings/backup/SettingProtos$Settings;->memoizedSerializedSize:I

    return v3
.end method

.method public final isInitialized()Z
    .locals 3

    const/4 v0, 0x1

    iget-byte v1, p0, Lcom/android/settings/backup/SettingProtos$Settings;->memoizedIsInitialized:B

    const/4 v2, -0x1

    if-eq v1, v2, :cond_1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    iput-byte v0, p0, Lcom/android/settings/backup/SettingProtos$Settings;->memoizedIsInitialized:B

    return v0
.end method

.method public newBuilderForType()Lcom/android/settings/backup/p;
    .locals 1

    invoke-static {}, Lcom/android/settings/backup/SettingProtos$Settings;->aHw()Lcom/android/settings/backup/p;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic newBuilderForType()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/android/settings/backup/SettingProtos$Settings;->newBuilderForType()Lcom/android/settings/backup/p;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lcom/android/settings/backup/p;
    .locals 1

    invoke-static {p0}, Lcom/android/settings/backup/SettingProtos$Settings;->aHx(Lcom/android/settings/backup/SettingProtos$Settings;)Lcom/android/settings/backup/p;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/android/settings/backup/SettingProtos$Settings;->toBuilder()Lcom/android/settings/backup/p;

    move-result-object v0

    return-object v0
.end method

.method protected writeReplace()Ljava/lang/Object;
    .locals 1

    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite;->writeReplace()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 4

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/android/settings/backup/SettingProtos$Settings;->getSerializedSize()I

    move v1, v2

    :goto_0
    iget-object v0, p0, Lcom/android/settings/backup/SettingProtos$Settings;->system_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/backup/SettingProtos$Settings;->system_:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/MessageLite;

    const/4 v3, 0x1

    invoke-virtual {p1, v3, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    move v1, v2

    :goto_1
    iget-object v0, p0, Lcom/android/settings/backup/SettingProtos$Settings;->secure_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    iget-object v0, p0, Lcom/android/settings/backup/SettingProtos$Settings;->secure_:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/MessageLite;

    const/4 v3, 0x2

    invoke-virtual {p1, v3, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    :cond_1
    :goto_2
    iget-object v0, p0, Lcom/android/settings/backup/SettingProtos$Settings;->lock_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_2

    iget-object v0, p0, Lcom/android/settings/backup/SettingProtos$Settings;->lock_:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/MessageLite;

    const/4 v1, 0x3

    invoke-virtual {p1, v1, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    :cond_2
    return-void
.end method
