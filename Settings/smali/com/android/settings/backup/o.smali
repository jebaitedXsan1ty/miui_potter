.class public final Lcom/android/settings/backup/o;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "SettingProtos.java"

# interfaces
.implements Lcom/google/protobuf/MessageLiteOrBuilder;


# instance fields
.field private bitField0_:I

.field private guid_:Ljava/lang/Object;

.field private luid_:Ljava/lang/Object;

.field private name_:Ljava/lang/Object;

.field private value_:J


# direct methods
.method private constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/android/settings/backup/o;->guid_:Ljava/lang/Object;

    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/android/settings/backup/o;->luid_:Ljava/lang/Object;

    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/android/settings/backup/o;->name_:Ljava/lang/Object;

    invoke-direct {p0}, Lcom/android/settings/backup/o;->aHm()V

    return-void
.end method

.method private aHm()V
    .locals 0

    return-void
.end method

.method static synthetic aHq()Lcom/android/settings/backup/o;
    .locals 1

    invoke-static {}, Lcom/android/settings/backup/o;->create()Lcom/android/settings/backup/o;

    move-result-object v0

    return-object v0
.end method

.method private static create()Lcom/android/settings/backup/o;
    .locals 1

    new-instance v0, Lcom/android/settings/backup/o;

    invoke-direct {v0}, Lcom/android/settings/backup/o;-><init>()V

    return-object v0
.end method


# virtual methods
.method public aHl(Ljava/lang/String;)Lcom/android/settings/backup/o;
    .locals 1

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget v0, p0, Lcom/android/settings/backup/o;->bitField0_:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/android/settings/backup/o;->bitField0_:I

    iput-object p1, p0, Lcom/android/settings/backup/o;->name_:Ljava/lang/Object;

    return-object p0
.end method

.method public aHn(Lcom/android/settings/backup/SettingProtos$LockSetting;)Lcom/android/settings/backup/o;
    .locals 2

    invoke-static {}, Lcom/android/settings/backup/SettingProtos$LockSetting;->aGU()Lcom/android/settings/backup/SettingProtos$LockSetting;

    move-result-object v0

    if-ne p1, v0, :cond_0

    return-object p0

    :cond_0
    invoke-virtual {p1}, Lcom/android/settings/backup/SettingProtos$LockSetting;->aHa()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Lcom/android/settings/backup/SettingProtos$LockSetting;->aGV()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/settings/backup/o;->aHo(Ljava/lang/String;)Lcom/android/settings/backup/o;

    :cond_1
    invoke-virtual {p1}, Lcom/android/settings/backup/SettingProtos$LockSetting;->aHb()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Lcom/android/settings/backup/SettingProtos$LockSetting;->aGX()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/settings/backup/o;->aHp(Ljava/lang/String;)Lcom/android/settings/backup/o;

    :cond_2
    invoke-virtual {p1}, Lcom/android/settings/backup/SettingProtos$LockSetting;->aHc()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p1}, Lcom/android/settings/backup/SettingProtos$LockSetting;->aGT()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/settings/backup/o;->aHl(Ljava/lang/String;)Lcom/android/settings/backup/o;

    :cond_3
    invoke-virtual {p1}, Lcom/android/settings/backup/SettingProtos$LockSetting;->aHd()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-virtual {p1}, Lcom/android/settings/backup/SettingProtos$LockSetting;->getValue()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/android/settings/backup/o;->setValue(J)Lcom/android/settings/backup/o;

    :cond_4
    return-object p0
.end method

.method public aHo(Ljava/lang/String;)Lcom/android/settings/backup/o;
    .locals 1

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget v0, p0, Lcom/android/settings/backup/o;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/android/settings/backup/o;->bitField0_:I

    iput-object p1, p0, Lcom/android/settings/backup/o;->guid_:Ljava/lang/Object;

    return-object p0
.end method

.method public aHp(Ljava/lang/String;)Lcom/android/settings/backup/o;
    .locals 1

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget v0, p0, Lcom/android/settings/backup/o;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/android/settings/backup/o;->bitField0_:I

    iput-object p1, p0, Lcom/android/settings/backup/o;->luid_:Ljava/lang/Object;

    return-object p0
.end method

.method public build()Lcom/android/settings/backup/SettingProtos$LockSetting;
    .locals 2

    invoke-virtual {p0}, Lcom/android/settings/backup/o;->buildPartial()Lcom/android/settings/backup/SettingProtos$LockSetting;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/settings/backup/SettingProtos$LockSetting;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {v0}, Lcom/android/settings/backup/o;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    :cond_0
    return-object v0
.end method

.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    invoke-virtual {p0}, Lcom/android/settings/backup/o;->build()Lcom/android/settings/backup/SettingProtos$LockSetting;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lcom/android/settings/backup/SettingProtos$LockSetting;
    .locals 6

    const/4 v0, 0x1

    new-instance v2, Lcom/android/settings/backup/SettingProtos$LockSetting;

    const/4 v1, 0x0

    invoke-direct {v2, p0, v1}, Lcom/android/settings/backup/SettingProtos$LockSetting;-><init>(Lcom/android/settings/backup/o;Lcom/android/settings/backup/SettingProtos$LockSetting;)V

    iget v3, p0, Lcom/android/settings/backup/o;->bitField0_:I

    const/4 v1, 0x0

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_3

    :goto_0
    iget-object v1, p0, Lcom/android/settings/backup/o;->guid_:Ljava/lang/Object;

    invoke-static {v2, v1}, Lcom/android/settings/backup/SettingProtos$LockSetting;->aHh(Lcom/android/settings/backup/SettingProtos$LockSetting;Ljava/lang/Object;)Ljava/lang/Object;

    and-int/lit8 v1, v3, 0x2

    const/4 v4, 0x2

    if-ne v1, v4, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget-object v1, p0, Lcom/android/settings/backup/o;->luid_:Ljava/lang/Object;

    invoke-static {v2, v1}, Lcom/android/settings/backup/SettingProtos$LockSetting;->aHi(Lcom/android/settings/backup/SettingProtos$LockSetting;Ljava/lang/Object;)Ljava/lang/Object;

    and-int/lit8 v1, v3, 0x4

    const/4 v4, 0x4

    if-ne v1, v4, :cond_1

    or-int/lit8 v0, v0, 0x4

    :cond_1
    iget-object v1, p0, Lcom/android/settings/backup/o;->name_:Ljava/lang/Object;

    invoke-static {v2, v1}, Lcom/android/settings/backup/SettingProtos$LockSetting;->aHj(Lcom/android/settings/backup/SettingProtos$LockSetting;Ljava/lang/Object;)Ljava/lang/Object;

    and-int/lit8 v1, v3, 0x8

    const/16 v3, 0x8

    if-ne v1, v3, :cond_2

    or-int/lit8 v0, v0, 0x8

    :cond_2
    iget-wide v4, p0, Lcom/android/settings/backup/o;->value_:J

    invoke-static {v2, v4, v5}, Lcom/android/settings/backup/SettingProtos$LockSetting;->aHk(Lcom/android/settings/backup/SettingProtos$LockSetting;J)J

    invoke-static {v2, v0}, Lcom/android/settings/backup/SettingProtos$LockSetting;->aHg(Lcom/android/settings/backup/SettingProtos$LockSetting;I)I

    return-object v2

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public bridge synthetic buildPartial()Lcom/google/protobuf/MessageLite;
    .locals 1

    invoke-virtual {p0}, Lcom/android/settings/backup/o;->buildPartial()Lcom/android/settings/backup/SettingProtos$LockSetting;

    move-result-object v0

    return-object v0
.end method

.method public clear()Lcom/android/settings/backup/o;
    .locals 2

    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;

    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/android/settings/backup/o;->guid_:Ljava/lang/Object;

    iget v0, p0, Lcom/android/settings/backup/o;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/android/settings/backup/o;->bitField0_:I

    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/android/settings/backup/o;->luid_:Ljava/lang/Object;

    iget v0, p0, Lcom/android/settings/backup/o;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/android/settings/backup/o;->bitField0_:I

    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/android/settings/backup/o;->name_:Ljava/lang/Object;

    iget v0, p0, Lcom/android/settings/backup/o;->bitField0_:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/android/settings/backup/o;->bitField0_:I

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/android/settings/backup/o;->value_:J

    iget v0, p0, Lcom/android/settings/backup/o;->bitField0_:I

    and-int/lit8 v0, v0, -0x9

    iput v0, p0, Lcom/android/settings/backup/o;->bitField0_:I

    return-object p0
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/android/settings/backup/o;->clear()Lcom/android/settings/backup/o;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/android/settings/backup/o;->clear()Lcom/android/settings/backup/o;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lcom/android/settings/backup/o;
    .locals 2

    invoke-static {}, Lcom/android/settings/backup/o;->create()Lcom/android/settings/backup/o;

    move-result-object v0

    invoke-virtual {p0}, Lcom/android/settings/backup/o;->buildPartial()Lcom/android/settings/backup/SettingProtos$LockSetting;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/settings/backup/o;->aHn(Lcom/android/settings/backup/SettingProtos$LockSetting;)Lcom/android/settings/backup/o;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/android/settings/backup/o;->clone()Lcom/android/settings/backup/o;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/android/settings/backup/o;->clone()Lcom/android/settings/backup/o;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/android/settings/backup/o;->clone()Lcom/android/settings/backup/o;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/android/settings/backup/SettingProtos$LockSetting;
    .locals 1

    invoke-static {}, Lcom/android/settings/backup/SettingProtos$LockSetting;->aGU()Lcom/android/settings/backup/SettingProtos$LockSetting;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/GeneratedMessageLite;
    .locals 1

    invoke-virtual {p0}, Lcom/android/settings/backup/o;->getDefaultInstanceForType()Lcom/android/settings/backup/SettingProtos$LockSetting;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    invoke-virtual {p0}, Lcom/android/settings/backup/o;->getDefaultInstanceForType()Lcom/android/settings/backup/SettingProtos$LockSetting;

    move-result-object v0

    return-object v0
.end method

.method public final isInitialized()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/android/settings/backup/o;
    .locals 2

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, p2, v0}, Lcom/android/settings/backup/o;->parseUnknownField(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;I)Z

    move-result v0

    if-nez v0, :cond_0

    return-object p0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget v0, p0, Lcom/android/settings/backup/o;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/android/settings/backup/o;->bitField0_:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/backup/o;->guid_:Ljava/lang/Object;

    goto :goto_0

    :sswitch_2
    iget v0, p0, Lcom/android/settings/backup/o;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/android/settings/backup/o;->bitField0_:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/backup/o;->luid_:Ljava/lang/Object;

    goto :goto_0

    :sswitch_3
    iget v0, p0, Lcom/android/settings/backup/o;->bitField0_:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/android/settings/backup/o;->bitField0_:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/backup/o;->name_:Ljava/lang/Object;

    goto :goto_0

    :sswitch_4
    iget v0, p0, Lcom/android/settings/backup/o;->bitField0_:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/android/settings/backup/o;->bitField0_:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readSInt64()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/android/settings/backup/o;->value_:J

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    invoke-virtual {p0, p1, p2}, Lcom/android/settings/backup/o;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/android/settings/backup/o;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    check-cast p1, Lcom/android/settings/backup/SettingProtos$LockSetting;

    invoke-virtual {p0, p1}, Lcom/android/settings/backup/o;->aHn(Lcom/android/settings/backup/SettingProtos$LockSetting;)Lcom/android/settings/backup/o;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    invoke-virtual {p0, p1, p2}, Lcom/android/settings/backup/o;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/android/settings/backup/o;

    move-result-object v0

    return-object v0
.end method

.method public setValue(J)Lcom/android/settings/backup/o;
    .locals 1

    iget v0, p0, Lcom/android/settings/backup/o;->bitField0_:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/android/settings/backup/o;->bitField0_:I

    iput-wide p1, p0, Lcom/android/settings/backup/o;->value_:J

    return-object p0
.end method
