.class public Lcom/android/settings/backup/SettingsBackupAgent;
.super Lmiui/app/backup/FullBackupAgent;
.source "SettingsBackupAgent.java"


# instance fields
.field private aTT:Lcom/android/settings/backup/e;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lmiui/app/backup/FullBackupAgent;-><init>()V

    return-void
.end method

.method private aFu(I)V
    .locals 1

    packed-switch p1, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    new-instance v0, Lcom/android/settings/backup/q;

    invoke-direct {v0, p0}, Lcom/android/settings/backup/q;-><init>(Lmiui/app/backup/FullBackupAgent;)V

    iput-object v0, p0, Lcom/android/settings/backup/SettingsBackupAgent;->aTT:Lcom/android/settings/backup/e;

    goto :goto_0

    :pswitch_1
    new-instance v0, Lcom/android/settings/backup/a;

    invoke-direct {v0, p0}, Lcom/android/settings/backup/a;-><init>(Lmiui/app/backup/FullBackupAgent;)V

    iput-object v0, p0, Lcom/android/settings/backup/SettingsBackupAgent;->aTT:Lcom/android/settings/backup/e;

    goto :goto_0

    :pswitch_2
    new-instance v0, Lcom/android/settings/backup/u;

    invoke-direct {v0, p0}, Lcom/android/settings/backup/u;-><init>(Lmiui/app/backup/FullBackupAgent;)V

    iput-object v0, p0, Lcom/android/settings/backup/SettingsBackupAgent;->aTT:Lcom/android/settings/backup/e;

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method


# virtual methods
.method protected getVersion(I)I
    .locals 1

    iget-object v0, p0, Lcom/android/settings/backup/SettingsBackupAgent;->aTT:Lcom/android/settings/backup/e;

    if-nez v0, :cond_0

    invoke-direct {p0, p1}, Lcom/android/settings/backup/SettingsBackupAgent;->aFu(I)V

    :cond_0
    iget-object v0, p0, Lcom/android/settings/backup/SettingsBackupAgent;->aTT:Lcom/android/settings/backup/e;

    invoke-virtual {v0}, Lcom/android/settings/backup/e;->aEU()I

    move-result v0

    return v0
.end method

.method protected onAttachRestore(Lmiui/app/backup/BackupMeta;Landroid/os/ParcelFileDescriptor;Ljava/lang/String;)I
    .locals 1

    iget-object v0, p0, Lcom/android/settings/backup/SettingsBackupAgent;->aTT:Lcom/android/settings/backup/e;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/backup/SettingsBackupAgent;->aTT:Lcom/android/settings/backup/e;

    invoke-virtual {v0, p1, p2, p3}, Lcom/android/settings/backup/e;->aEV(Lmiui/app/backup/BackupMeta;Landroid/os/ParcelFileDescriptor;Ljava/lang/String;)I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method protected onDataRestore(Lmiui/app/backup/BackupMeta;Landroid/os/ParcelFileDescriptor;)I
    .locals 1

    iget-object v0, p0, Lcom/android/settings/backup/SettingsBackupAgent;->aTT:Lcom/android/settings/backup/e;

    if-nez v0, :cond_0

    iget v0, p1, Lmiui/app/backup/BackupMeta;->feature:I

    invoke-direct {p0, v0}, Lcom/android/settings/backup/SettingsBackupAgent;->aFu(I)V

    :cond_0
    iget-object v0, p0, Lcom/android/settings/backup/SettingsBackupAgent;->aTT:Lcom/android/settings/backup/e;

    invoke-virtual {v0, p1, p2}, Lcom/android/settings/backup/e;->aEW(Lmiui/app/backup/BackupMeta;Landroid/os/ParcelFileDescriptor;)I

    move-result v0

    return v0
.end method

.method protected onFullBackup(Landroid/os/ParcelFileDescriptor;I)I
    .locals 1

    iget-object v0, p0, Lcom/android/settings/backup/SettingsBackupAgent;->aTT:Lcom/android/settings/backup/e;

    if-nez v0, :cond_0

    invoke-direct {p0, p2}, Lcom/android/settings/backup/SettingsBackupAgent;->aFu(I)V

    :cond_0
    iget-object v0, p0, Lcom/android/settings/backup/SettingsBackupAgent;->aTT:Lcom/android/settings/backup/e;

    invoke-virtual {v0, p1}, Lcom/android/settings/backup/e;->aET(Landroid/os/ParcelFileDescriptor;)I

    move-result v0

    return v0
.end method

.method protected onRestoreEnd(Lmiui/app/backup/BackupMeta;)I
    .locals 1

    iget-object v0, p0, Lcom/android/settings/backup/SettingsBackupAgent;->aTT:Lcom/android/settings/backup/e;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/backup/SettingsBackupAgent;->aTT:Lcom/android/settings/backup/e;

    invoke-virtual {v0, p1}, Lcom/android/settings/backup/e;->aES(Lmiui/app/backup/BackupMeta;)I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method protected tarAttaches(Ljava/lang/String;Landroid/app/backup/FullBackupDataOutput;I)I
    .locals 1

    invoke-super {p0, p1, p2, p3}, Lmiui/app/backup/FullBackupAgent;->tarAttaches(Ljava/lang/String;Landroid/app/backup/FullBackupDataOutput;I)I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/backup/SettingsBackupAgent;->aTT:Lcom/android/settings/backup/e;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/settings/backup/SettingsBackupAgent;->aTT:Lcom/android/settings/backup/e;

    invoke-virtual {v0, p1, p2}, Lcom/android/settings/backup/e;->aEX(Ljava/lang/String;Landroid/app/backup/FullBackupDataOutput;)I

    move-result v0

    :cond_0
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method
