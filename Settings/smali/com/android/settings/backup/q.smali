.class public Lcom/android/settings/backup/q;
.super Lcom/android/settings/backup/e;
.source "SettingsAgent.java"


# instance fields
.field private final aUD:I

.field private aUE:Lcom/android/settings/backup/s;

.field private aUF:Lcom/android/settings/backup/r;


# direct methods
.method public constructor <init>(Lmiui/app/backup/FullBackupAgent;)V
    .locals 1

    invoke-direct {p0, p1}, Lcom/android/settings/backup/e;-><init>(Lmiui/app/backup/FullBackupAgent;)V

    const/4 v0, 0x2

    iput v0, p0, Lcom/android/settings/backup/q;->aUD:I

    return-void
.end method


# virtual methods
.method public aES(Lmiui/app/backup/BackupMeta;)I
    .locals 3

    const/4 v1, 0x1

    const/4 v0, 0x0

    iget v2, p1, Lmiui/app/backup/BackupMeta;->version:I

    packed-switch v2, :pswitch_data_0

    const/4 v0, 0x4

    :goto_0
    return v0

    :pswitch_0
    iget-object v2, p0, Lcom/android/settings/backup/q;->aUE:Lcom/android/settings/backup/s;

    if-eqz v2, :cond_0

    iget-object v1, p0, Lcom/android/settings/backup/q;->aUE:Lcom/android/settings/backup/s;

    invoke-virtual {v1}, Lcom/android/settings/backup/s;->aHP()I

    goto :goto_0

    :cond_0
    move v0, v1

    goto :goto_0

    :pswitch_1
    iget-object v2, p0, Lcom/android/settings/backup/q;->aUF:Lcom/android/settings/backup/r;

    if-eqz v2, :cond_1

    move v1, v0

    :cond_1
    move v0, v1

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public aET(Landroid/os/ParcelFileDescriptor;)I
    .locals 4

    const/4 v3, 0x0

    const/4 v0, 0x0

    const/4 v1, 0x2

    packed-switch v1, :pswitch_data_0

    const/4 v0, 0x4

    :goto_0
    return v0

    :pswitch_0
    new-instance v1, Lcom/android/settings/backup/s;

    invoke-direct {v1, p0, v3}, Lcom/android/settings/backup/s;-><init>(Lcom/android/settings/backup/q;Lcom/android/settings/backup/s;)V

    iput-object v1, p0, Lcom/android/settings/backup/q;->aUE:Lcom/android/settings/backup/s;

    iget-object v1, p0, Lcom/android/settings/backup/q;->aUE:Lcom/android/settings/backup/s;

    invoke-virtual {v1, p1}, Lcom/android/settings/backup/s;->aHQ(Landroid/os/ParcelFileDescriptor;)I

    goto :goto_0

    :pswitch_1
    const-string/jumbo v1, "Backup:SettingsAgent"

    const-string/jumbo v2, "full backup"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v1, Lcom/android/settings/backup/r;

    invoke-direct {v1, p0, v3}, Lcom/android/settings/backup/r;-><init>(Lcom/android/settings/backup/q;Lcom/android/settings/backup/r;)V

    iput-object v1, p0, Lcom/android/settings/backup/q;->aUF:Lcom/android/settings/backup/r;

    iget-object v1, p0, Lcom/android/settings/backup/q;->aUF:Lcom/android/settings/backup/r;

    invoke-virtual {v1, p1}, Lcom/android/settings/backup/r;->aHN(Landroid/os/ParcelFileDescriptor;)I

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public aEU()I
    .locals 1

    const/4 v0, 0x2

    return v0
.end method

.method public aEV(Lmiui/app/backup/BackupMeta;Landroid/os/ParcelFileDescriptor;Ljava/lang/String;)I
    .locals 3

    const/4 v1, 0x1

    const/4 v0, 0x0

    iget v2, p1, Lmiui/app/backup/BackupMeta;->version:I

    packed-switch v2, :pswitch_data_0

    const/4 v0, 0x4

    :goto_0
    return v0

    :pswitch_0
    iget-object v2, p0, Lcom/android/settings/backup/q;->aUE:Lcom/android/settings/backup/s;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/android/settings/backup/q;->aUE:Lcom/android/settings/backup/s;

    invoke-virtual {v2, p2, p3}, Lcom/android/settings/backup/s;->aHR(Landroid/os/ParcelFileDescriptor;Ljava/lang/String;)I

    :goto_1
    :pswitch_1
    iget-object v2, p0, Lcom/android/settings/backup/q;->aUF:Lcom/android/settings/backup/r;

    if-eqz v2, :cond_1

    invoke-static {p3, p2}, Lcom/xiaomi/settingsdk/backup/SettingsBackupHelper;->restoreOneFile(Ljava/lang/String;Landroid/os/ParcelFileDescriptor;)V

    goto :goto_0

    :cond_0
    move v0, v1

    goto :goto_1

    :cond_1
    move v0, v1

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public aEW(Lmiui/app/backup/BackupMeta;Landroid/os/ParcelFileDescriptor;)I
    .locals 4

    const/4 v3, 0x0

    const/4 v0, 0x0

    iget v1, p1, Lmiui/app/backup/BackupMeta;->version:I

    packed-switch v1, :pswitch_data_0

    const/4 v0, 0x4

    :goto_0
    return v0

    :pswitch_0
    new-instance v1, Lcom/android/settings/backup/s;

    invoke-direct {v1, p0, v3}, Lcom/android/settings/backup/s;-><init>(Lcom/android/settings/backup/q;Lcom/android/settings/backup/s;)V

    iput-object v1, p0, Lcom/android/settings/backup/q;->aUE:Lcom/android/settings/backup/s;

    iget-object v1, p0, Lcom/android/settings/backup/q;->aUE:Lcom/android/settings/backup/s;

    invoke-virtual {v1, p2}, Lcom/android/settings/backup/s;->aHS(Landroid/os/ParcelFileDescriptor;)I

    goto :goto_0

    :pswitch_1
    const-string/jumbo v1, "Backup:SettingsAgent"

    const-string/jumbo v2, "restore data"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v1, Lcom/android/settings/backup/r;

    invoke-direct {v1, p0, v3}, Lcom/android/settings/backup/r;-><init>(Lcom/android/settings/backup/q;Lcom/android/settings/backup/r;)V

    iput-object v1, p0, Lcom/android/settings/backup/q;->aUF:Lcom/android/settings/backup/r;

    iget-object v1, p0, Lcom/android/settings/backup/q;->aUF:Lcom/android/settings/backup/r;

    invoke-virtual {v1, p2}, Lcom/android/settings/backup/r;->aHO(Landroid/os/ParcelFileDescriptor;)I

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public aEX(Ljava/lang/String;Landroid/app/backup/FullBackupDataOutput;)I
    .locals 3

    const/4 v1, 0x1

    const/4 v0, 0x0

    const/4 v2, 0x2

    packed-switch v2, :pswitch_data_0

    const/4 v0, 0x4

    :goto_0
    return v0

    :pswitch_0
    iget-object v2, p0, Lcom/android/settings/backup/q;->aUE:Lcom/android/settings/backup/s;

    if-eqz v2, :cond_0

    iget-object v1, p0, Lcom/android/settings/backup/q;->aUE:Lcom/android/settings/backup/s;

    invoke-virtual {v1, p1, p2}, Lcom/android/settings/backup/s;->aHT(Ljava/lang/String;Landroid/app/backup/FullBackupDataOutput;)I

    goto :goto_0

    :cond_0
    move v0, v1

    goto :goto_0

    :pswitch_1
    iget-object v2, p0, Lcom/android/settings/backup/q;->aUF:Lcom/android/settings/backup/r;

    if-eqz v2, :cond_1

    move v1, v0

    :cond_1
    move v0, v1

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
