.class public final Lcom/android/settings/backup/n;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "SettingProtos.java"

# interfaces
.implements Lcom/google/protobuf/MessageLiteOrBuilder;


# instance fields
.field private bitField0_:I

.field private guid_:Ljava/lang/Object;

.field private luid_:Ljava/lang/Object;

.field private name_:Ljava/lang/Object;

.field private value_:Ljava/lang/Object;


# direct methods
.method private constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/android/settings/backup/n;->guid_:Ljava/lang/Object;

    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/android/settings/backup/n;->luid_:Ljava/lang/Object;

    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/android/settings/backup/n;->name_:Ljava/lang/Object;

    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/android/settings/backup/n;->value_:Ljava/lang/Object;

    invoke-direct {p0}, Lcom/android/settings/backup/n;->aGO()V

    return-void
.end method

.method private aGO()V
    .locals 0

    return-void
.end method

.method static synthetic aGR()Lcom/android/settings/backup/n;
    .locals 1

    invoke-static {}, Lcom/android/settings/backup/n;->create()Lcom/android/settings/backup/n;

    move-result-object v0

    return-object v0
.end method

.method private static create()Lcom/android/settings/backup/n;
    .locals 1

    new-instance v0, Lcom/android/settings/backup/n;

    invoke-direct {v0}, Lcom/android/settings/backup/n;-><init>()V

    return-object v0
.end method


# virtual methods
.method public aGM(Ljava/lang/String;)Lcom/android/settings/backup/n;
    .locals 1

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget v0, p0, Lcom/android/settings/backup/n;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/android/settings/backup/n;->bitField0_:I

    iput-object p1, p0, Lcom/android/settings/backup/n;->luid_:Ljava/lang/Object;

    return-object p0
.end method

.method public aGN(Ljava/lang/String;)Lcom/android/settings/backup/n;
    .locals 1

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget v0, p0, Lcom/android/settings/backup/n;->bitField0_:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/android/settings/backup/n;->bitField0_:I

    iput-object p1, p0, Lcom/android/settings/backup/n;->name_:Ljava/lang/Object;

    return-object p0
.end method

.method public aGP(Lcom/android/settings/backup/SettingProtos$SecureSetting;)Lcom/android/settings/backup/n;
    .locals 1

    invoke-static {}, Lcom/android/settings/backup/SettingProtos$SecureSetting;->aGw()Lcom/android/settings/backup/SettingProtos$SecureSetting;

    move-result-object v0

    if-ne p1, v0, :cond_0

    return-object p0

    :cond_0
    invoke-virtual {p1}, Lcom/android/settings/backup/SettingProtos$SecureSetting;->aGD()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Lcom/android/settings/backup/SettingProtos$SecureSetting;->aGx()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/settings/backup/n;->aGQ(Ljava/lang/String;)Lcom/android/settings/backup/n;

    :cond_1
    invoke-virtual {p1}, Lcom/android/settings/backup/SettingProtos$SecureSetting;->aGE()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Lcom/android/settings/backup/SettingProtos$SecureSetting;->aGz()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/settings/backup/n;->aGM(Ljava/lang/String;)Lcom/android/settings/backup/n;

    :cond_2
    invoke-virtual {p1}, Lcom/android/settings/backup/SettingProtos$SecureSetting;->aGt()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p1}, Lcom/android/settings/backup/SettingProtos$SecureSetting;->aGu()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/settings/backup/n;->aGN(Ljava/lang/String;)Lcom/android/settings/backup/n;

    :cond_3
    invoke-virtual {p1}, Lcom/android/settings/backup/SettingProtos$SecureSetting;->aGv()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-virtual {p1}, Lcom/android/settings/backup/SettingProtos$SecureSetting;->getValue()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/settings/backup/n;->setValue(Ljava/lang/String;)Lcom/android/settings/backup/n;

    :cond_4
    return-object p0
.end method

.method public aGQ(Ljava/lang/String;)Lcom/android/settings/backup/n;
    .locals 1

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget v0, p0, Lcom/android/settings/backup/n;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/android/settings/backup/n;->bitField0_:I

    iput-object p1, p0, Lcom/android/settings/backup/n;->guid_:Ljava/lang/Object;

    return-object p0
.end method

.method public build()Lcom/android/settings/backup/SettingProtos$SecureSetting;
    .locals 2

    invoke-virtual {p0}, Lcom/android/settings/backup/n;->buildPartial()Lcom/android/settings/backup/SettingProtos$SecureSetting;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/settings/backup/SettingProtos$SecureSetting;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {v0}, Lcom/android/settings/backup/n;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    :cond_0
    return-object v0
.end method

.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    invoke-virtual {p0}, Lcom/android/settings/backup/n;->build()Lcom/android/settings/backup/SettingProtos$SecureSetting;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lcom/android/settings/backup/SettingProtos$SecureSetting;
    .locals 5

    const/4 v0, 0x1

    new-instance v2, Lcom/android/settings/backup/SettingProtos$SecureSetting;

    const/4 v1, 0x0

    invoke-direct {v2, p0, v1}, Lcom/android/settings/backup/SettingProtos$SecureSetting;-><init>(Lcom/android/settings/backup/n;Lcom/android/settings/backup/SettingProtos$SecureSetting;)V

    iget v3, p0, Lcom/android/settings/backup/n;->bitField0_:I

    const/4 v1, 0x0

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_3

    :goto_0
    iget-object v1, p0, Lcom/android/settings/backup/n;->guid_:Ljava/lang/Object;

    invoke-static {v2, v1}, Lcom/android/settings/backup/SettingProtos$SecureSetting;->aGI(Lcom/android/settings/backup/SettingProtos$SecureSetting;Ljava/lang/Object;)Ljava/lang/Object;

    and-int/lit8 v1, v3, 0x2

    const/4 v4, 0x2

    if-ne v1, v4, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget-object v1, p0, Lcom/android/settings/backup/n;->luid_:Ljava/lang/Object;

    invoke-static {v2, v1}, Lcom/android/settings/backup/SettingProtos$SecureSetting;->aGJ(Lcom/android/settings/backup/SettingProtos$SecureSetting;Ljava/lang/Object;)Ljava/lang/Object;

    and-int/lit8 v1, v3, 0x4

    const/4 v4, 0x4

    if-ne v1, v4, :cond_1

    or-int/lit8 v0, v0, 0x4

    :cond_1
    iget-object v1, p0, Lcom/android/settings/backup/n;->name_:Ljava/lang/Object;

    invoke-static {v2, v1}, Lcom/android/settings/backup/SettingProtos$SecureSetting;->aGK(Lcom/android/settings/backup/SettingProtos$SecureSetting;Ljava/lang/Object;)Ljava/lang/Object;

    and-int/lit8 v1, v3, 0x8

    const/16 v3, 0x8

    if-ne v1, v3, :cond_2

    or-int/lit8 v0, v0, 0x8

    :cond_2
    iget-object v1, p0, Lcom/android/settings/backup/n;->value_:Ljava/lang/Object;

    invoke-static {v2, v1}, Lcom/android/settings/backup/SettingProtos$SecureSetting;->aGL(Lcom/android/settings/backup/SettingProtos$SecureSetting;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {v2, v0}, Lcom/android/settings/backup/SettingProtos$SecureSetting;->aGH(Lcom/android/settings/backup/SettingProtos$SecureSetting;I)I

    return-object v2

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public bridge synthetic buildPartial()Lcom/google/protobuf/MessageLite;
    .locals 1

    invoke-virtual {p0}, Lcom/android/settings/backup/n;->buildPartial()Lcom/android/settings/backup/SettingProtos$SecureSetting;

    move-result-object v0

    return-object v0
.end method

.method public clear()Lcom/android/settings/backup/n;
    .locals 1

    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;

    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/android/settings/backup/n;->guid_:Ljava/lang/Object;

    iget v0, p0, Lcom/android/settings/backup/n;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/android/settings/backup/n;->bitField0_:I

    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/android/settings/backup/n;->luid_:Ljava/lang/Object;

    iget v0, p0, Lcom/android/settings/backup/n;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/android/settings/backup/n;->bitField0_:I

    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/android/settings/backup/n;->name_:Ljava/lang/Object;

    iget v0, p0, Lcom/android/settings/backup/n;->bitField0_:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/android/settings/backup/n;->bitField0_:I

    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/android/settings/backup/n;->value_:Ljava/lang/Object;

    iget v0, p0, Lcom/android/settings/backup/n;->bitField0_:I

    and-int/lit8 v0, v0, -0x9

    iput v0, p0, Lcom/android/settings/backup/n;->bitField0_:I

    return-object p0
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/android/settings/backup/n;->clear()Lcom/android/settings/backup/n;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/android/settings/backup/n;->clear()Lcom/android/settings/backup/n;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lcom/android/settings/backup/n;
    .locals 2

    invoke-static {}, Lcom/android/settings/backup/n;->create()Lcom/android/settings/backup/n;

    move-result-object v0

    invoke-virtual {p0}, Lcom/android/settings/backup/n;->buildPartial()Lcom/android/settings/backup/SettingProtos$SecureSetting;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/settings/backup/n;->aGP(Lcom/android/settings/backup/SettingProtos$SecureSetting;)Lcom/android/settings/backup/n;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/android/settings/backup/n;->clone()Lcom/android/settings/backup/n;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/android/settings/backup/n;->clone()Lcom/android/settings/backup/n;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/android/settings/backup/n;->clone()Lcom/android/settings/backup/n;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/android/settings/backup/SettingProtos$SecureSetting;
    .locals 1

    invoke-static {}, Lcom/android/settings/backup/SettingProtos$SecureSetting;->aGw()Lcom/android/settings/backup/SettingProtos$SecureSetting;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/GeneratedMessageLite;
    .locals 1

    invoke-virtual {p0}, Lcom/android/settings/backup/n;->getDefaultInstanceForType()Lcom/android/settings/backup/SettingProtos$SecureSetting;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    invoke-virtual {p0}, Lcom/android/settings/backup/n;->getDefaultInstanceForType()Lcom/android/settings/backup/SettingProtos$SecureSetting;

    move-result-object v0

    return-object v0
.end method

.method public final isInitialized()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/android/settings/backup/n;
    .locals 1

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, p2, v0}, Lcom/android/settings/backup/n;->parseUnknownField(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;I)Z

    move-result v0

    if-nez v0, :cond_0

    return-object p0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget v0, p0, Lcom/android/settings/backup/n;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/android/settings/backup/n;->bitField0_:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/backup/n;->guid_:Ljava/lang/Object;

    goto :goto_0

    :sswitch_2
    iget v0, p0, Lcom/android/settings/backup/n;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/android/settings/backup/n;->bitField0_:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/backup/n;->luid_:Ljava/lang/Object;

    goto :goto_0

    :sswitch_3
    iget v0, p0, Lcom/android/settings/backup/n;->bitField0_:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/android/settings/backup/n;->bitField0_:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/backup/n;->name_:Ljava/lang/Object;

    goto :goto_0

    :sswitch_4
    iget v0, p0, Lcom/android/settings/backup/n;->bitField0_:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/android/settings/backup/n;->bitField0_:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/backup/n;->value_:Ljava/lang/Object;

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    invoke-virtual {p0, p1, p2}, Lcom/android/settings/backup/n;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/android/settings/backup/n;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    check-cast p1, Lcom/android/settings/backup/SettingProtos$SecureSetting;

    invoke-virtual {p0, p1}, Lcom/android/settings/backup/n;->aGP(Lcom/android/settings/backup/SettingProtos$SecureSetting;)Lcom/android/settings/backup/n;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    invoke-virtual {p0, p1, p2}, Lcom/android/settings/backup/n;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/android/settings/backup/n;

    move-result-object v0

    return-object v0
.end method

.method public setValue(Ljava/lang/String;)Lcom/android/settings/backup/n;
    .locals 1

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget v0, p0, Lcom/android/settings/backup/n;->bitField0_:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/android/settings/backup/n;->bitField0_:I

    iput-object p1, p0, Lcom/android/settings/backup/n;->value_:Ljava/lang/Object;

    return-object p0
.end method
