.class public Lcom/android/settings/backup/d;
.super Ljava/lang/Object;
.source "Customization.java"


# static fields
.field public static final aTS:[Lcom/android/settings/backup/j;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const/4 v5, 0x0

    const/16 v0, 0x8

    new-array v0, v0, [Lcom/android/settings/backup/j;

    new-instance v1, Lcom/android/settings/backup/j;

    const-string/jumbo v2, ""

    const-string/jumbo v3, "gesture.key"

    const-string/jumbo v4, "/data/system/gesture.key"

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/android/settings/backup/j;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    aput-object v1, v0, v5

    new-instance v1, Lcom/android/settings/backup/j;

    const-string/jumbo v2, ""

    const-string/jumbo v3, "password.key"

    const-string/jumbo v4, "/data/system/password.key"

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/android/settings/backup/j;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    const/4 v2, 0x1

    aput-object v1, v0, v2

    new-instance v1, Lcom/android/settings/backup/j;

    const-string/jumbo v2, ""

    const-string/jumbo v3, "password.key"

    const-string/jumbo v4, "/data/system/password.key"

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/android/settings/backup/j;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    const/4 v2, 0x2

    aput-object v1, v0, v2

    new-instance v1, Lcom/android/settings/backup/j;

    const-string/jumbo v2, ""

    const-string/jumbo v3, "netpolicy.xml"

    const-string/jumbo v4, "/data/system/netpolicy.xml"

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/android/settings/backup/j;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    const/4 v2, 0x3

    aput-object v1, v0, v2

    new-instance v1, Lcom/android/settings/backup/j;

    const-string/jumbo v2, ""

    const-string/jumbo v3, "netstats.bin"

    const-string/jumbo v4, "/data/system/netstats.bin"

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/android/settings/backup/j;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    const/4 v2, 0x4

    aput-object v1, v0, v2

    new-instance v1, Lcom/android/settings/backup/j;

    const-string/jumbo v2, ""

    const-string/jumbo v3, "netstats_uid.bin"

    const-string/jumbo v4, "/data/system/netstats_uid.bin"

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/android/settings/backup/j;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    const/4 v2, 0x5

    aput-object v1, v0, v2

    new-instance v1, Lcom/android/settings/backup/j;

    const-string/jumbo v2, ""

    const-string/jumbo v3, "netstats_xt.bin"

    const-string/jumbo v4, "/data/system/netstats_xt.bin"

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/android/settings/backup/j;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    const/4 v2, 0x6

    aput-object v1, v0, v2

    new-instance v1, Lcom/android/settings/backup/j;

    const-string/jumbo v2, ""

    const-string/jumbo v3, "access_control.key"

    const-string/jumbo v4, "/data/system/access_control.key"

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/android/settings/backup/j;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    const/4 v2, 0x7

    aput-object v1, v0, v2

    sput-object v0, Lcom/android/settings/backup/d;->aTS:[Lcom/android/settings/backup/j;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
