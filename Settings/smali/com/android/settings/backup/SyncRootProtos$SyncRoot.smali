.class public final Lcom/android/settings/backup/SyncRootProtos$SyncRoot;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "SyncRootProtos.java"

# interfaces
.implements Lcom/google/protobuf/MessageLiteOrBuilder;


# static fields
.field private static final aUL:Lcom/android/settings/backup/SyncRootProtos$SyncRoot;

.field private static final serialVersionUID:J


# instance fields
.field private bitField0_:I

.field private memoizedIsInitialized:B

.field private memoizedSerializedSize:I

.field private setting_:Lcom/android/settings/backup/SettingProtos$Settings;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/android/settings/backup/SyncRootProtos$SyncRoot;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/android/settings/backup/SyncRootProtos$SyncRoot;-><init>(Z)V

    sput-object v0, Lcom/android/settings/backup/SyncRootProtos$SyncRoot;->aUL:Lcom/android/settings/backup/SyncRootProtos$SyncRoot;

    sget-object v0, Lcom/android/settings/backup/SyncRootProtos$SyncRoot;->aUL:Lcom/android/settings/backup/SyncRootProtos$SyncRoot;

    invoke-direct {v0}, Lcom/android/settings/backup/SyncRootProtos$SyncRoot;->aHZ()V

    return-void
.end method

.method private constructor <init>(Lcom/android/settings/backup/t;)V
    .locals 1

    const/4 v0, -0x1

    invoke-direct {p0, p1}, Lcom/google/protobuf/GeneratedMessageLite;-><init>(Lcom/google/protobuf/GeneratedMessageLite$Builder;)V

    iput-byte v0, p0, Lcom/android/settings/backup/SyncRootProtos$SyncRoot;->memoizedIsInitialized:B

    iput v0, p0, Lcom/android/settings/backup/SyncRootProtos$SyncRoot;->memoizedSerializedSize:I

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/settings/backup/t;Lcom/android/settings/backup/SyncRootProtos$SyncRoot;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/settings/backup/SyncRootProtos$SyncRoot;-><init>(Lcom/android/settings/backup/t;)V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1

    const/4 v0, -0x1

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    iput-byte v0, p0, Lcom/android/settings/backup/SyncRootProtos$SyncRoot;->memoizedIsInitialized:B

    iput v0, p0, Lcom/android/settings/backup/SyncRootProtos$SyncRoot;->memoizedSerializedSize:I

    return-void
.end method

.method public static aHU()Lcom/android/settings/backup/t;
    .locals 1

    invoke-static {}, Lcom/android/settings/backup/t;->aIi()Lcom/android/settings/backup/t;

    move-result-object v0

    return-object v0
.end method

.method public static aHV(Ljava/io/InputStream;)Lcom/android/settings/backup/SyncRootProtos$SyncRoot;
    .locals 1

    invoke-static {}, Lcom/android/settings/backup/SyncRootProtos$SyncRoot;->aHU()Lcom/android/settings/backup/t;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/android/settings/backup/t;->mergeFrom(Ljava/io/InputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/android/settings/backup/t;

    invoke-static {v0}, Lcom/android/settings/backup/t;->aIj(Lcom/android/settings/backup/t;)Lcom/android/settings/backup/SyncRootProtos$SyncRoot;

    move-result-object v0

    return-object v0
.end method

.method public static aHX()Lcom/android/settings/backup/SyncRootProtos$SyncRoot;
    .locals 1

    sget-object v0, Lcom/android/settings/backup/SyncRootProtos$SyncRoot;->aUL:Lcom/android/settings/backup/SyncRootProtos$SyncRoot;

    return-object v0
.end method

.method private aHZ()V
    .locals 1

    invoke-static {}, Lcom/android/settings/backup/SettingProtos$Settings;->aHr()Lcom/android/settings/backup/SettingProtos$Settings;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/backup/SyncRootProtos$SyncRoot;->setting_:Lcom/android/settings/backup/SettingProtos$Settings;

    return-void
.end method

.method public static aIa(Lcom/android/settings/backup/SyncRootProtos$SyncRoot;)Lcom/android/settings/backup/t;
    .locals 1

    invoke-static {}, Lcom/android/settings/backup/SyncRootProtos$SyncRoot;->aHU()Lcom/android/settings/backup/t;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/android/settings/backup/t;->aIg(Lcom/android/settings/backup/SyncRootProtos$SyncRoot;)Lcom/android/settings/backup/t;

    move-result-object v0

    return-object v0
.end method

.method static synthetic aIb(Lcom/android/settings/backup/SyncRootProtos$SyncRoot;I)I
    .locals 0

    iput p1, p0, Lcom/android/settings/backup/SyncRootProtos$SyncRoot;->bitField0_:I

    return p1
.end method

.method static synthetic aIc(Lcom/android/settings/backup/SyncRootProtos$SyncRoot;Lcom/android/settings/backup/SettingProtos$Settings;)Lcom/android/settings/backup/SettingProtos$Settings;
    .locals 0

    iput-object p1, p0, Lcom/android/settings/backup/SyncRootProtos$SyncRoot;->setting_:Lcom/android/settings/backup/SettingProtos$Settings;

    return-object p1
.end method


# virtual methods
.method public aHW()Lcom/android/settings/backup/SettingProtos$Settings;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/backup/SyncRootProtos$SyncRoot;->setting_:Lcom/android/settings/backup/SettingProtos$Settings;

    return-object v0
.end method

.method public aHY()Z
    .locals 2

    const/4 v0, 0x1

    iget v1, p0, Lcom/android/settings/backup/SyncRootProtos$SyncRoot;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getDefaultInstanceForType()Lcom/android/settings/backup/SyncRootProtos$SyncRoot;
    .locals 1

    sget-object v0, Lcom/android/settings/backup/SyncRootProtos$SyncRoot;->aUL:Lcom/android/settings/backup/SyncRootProtos$SyncRoot;

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    invoke-virtual {p0}, Lcom/android/settings/backup/SyncRootProtos$SyncRoot;->getDefaultInstanceForType()Lcom/android/settings/backup/SyncRootProtos$SyncRoot;

    move-result-object v0

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 3

    const/4 v0, 0x0

    iget v1, p0, Lcom/android/settings/backup/SyncRootProtos$SyncRoot;->memoizedSerializedSize:I

    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    return v1

    :cond_0
    iget v1, p0, Lcom/android/settings/backup/SyncRootProtos$SyncRoot;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_1

    iget-object v0, p0, Lcom/android/settings/backup/SyncRootProtos$SyncRoot;->setting_:Lcom/android/settings/backup/SettingProtos$Settings;

    const/4 v1, 0x7

    invoke-static {v1, v0}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    :cond_1
    iput v0, p0, Lcom/android/settings/backup/SyncRootProtos$SyncRoot;->memoizedSerializedSize:I

    return v0
.end method

.method public final isInitialized()Z
    .locals 3

    const/4 v0, 0x1

    iget-byte v1, p0, Lcom/android/settings/backup/SyncRootProtos$SyncRoot;->memoizedIsInitialized:B

    const/4 v2, -0x1

    if-eq v1, v2, :cond_1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    iput-byte v0, p0, Lcom/android/settings/backup/SyncRootProtos$SyncRoot;->memoizedIsInitialized:B

    return v0
.end method

.method public newBuilderForType()Lcom/android/settings/backup/t;
    .locals 1

    invoke-static {}, Lcom/android/settings/backup/SyncRootProtos$SyncRoot;->aHU()Lcom/android/settings/backup/t;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic newBuilderForType()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/android/settings/backup/SyncRootProtos$SyncRoot;->newBuilderForType()Lcom/android/settings/backup/t;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lcom/android/settings/backup/t;
    .locals 1

    invoke-static {p0}, Lcom/android/settings/backup/SyncRootProtos$SyncRoot;->aIa(Lcom/android/settings/backup/SyncRootProtos$SyncRoot;)Lcom/android/settings/backup/t;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/android/settings/backup/SyncRootProtos$SyncRoot;->toBuilder()Lcom/android/settings/backup/t;

    move-result-object v0

    return-object v0
.end method

.method protected writeReplace()Ljava/lang/Object;
    .locals 1

    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite;->writeReplace()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 2

    invoke-virtual {p0}, Lcom/android/settings/backup/SyncRootProtos$SyncRoot;->getSerializedSize()I

    iget v0, p0, Lcom/android/settings/backup/SyncRootProtos$SyncRoot;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/android/settings/backup/SyncRootProtos$SyncRoot;->setting_:Lcom/android/settings/backup/SettingProtos$Settings;

    const/4 v1, 0x7

    invoke-virtual {p1, v1, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    :cond_0
    return-void
.end method
