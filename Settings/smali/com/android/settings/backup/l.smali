.class public Lcom/android/settings/backup/l;
.super Lcom/android/settings/core/c;
.source "BackupSettingsPreferenceController.java"


# instance fields
.field private aUp:Landroid/content/Intent;

.field private aUq:Ljava/lang/String;

.field private aUr:Ljava/lang/String;

.field private aUs:Landroid/content/Intent;

.field private aUt:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    invoke-direct {p0, p1}, Lcom/android/settings/core/c;-><init>(Landroid/content/Context;)V

    new-instance v0, Lcom/android/settings/backup/k;

    invoke-direct {v0, p1}, Lcom/android/settings/backup/k;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0}, Lcom/android/settings/backup/k;->aFG()Landroid/content/Intent;

    move-result-object v1

    iput-object v1, p0, Lcom/android/settings/backup/l;->aUp:Landroid/content/Intent;

    invoke-virtual {v0}, Lcom/android/settings/backup/k;->aFK()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/android/settings/backup/l;->aUr:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/android/settings/backup/k;->aFM()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/android/settings/backup/l;->aUq:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/android/settings/backup/k;->aFJ()Landroid/content/Intent;

    move-result-object v1

    iput-object v1, p0, Lcom/android/settings/backup/l;->aUs:Landroid/content/Intent;

    invoke-virtual {v0}, Lcom/android/settings/backup/k;->aFL()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/backup/l;->aUt:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public a(Landroid/support/v7/preference/PreferenceScreen;)V
    .locals 3

    const-string/jumbo v0, "backup_settings"

    invoke-virtual {p1, v0}, Landroid/support/v7/preference/PreferenceScreen;->dlg(Ljava/lang/CharSequence;)Landroid/support/v7/preference/Preference;

    move-result-object v0

    const-string/jumbo v1, "manufacturer_backup"

    invoke-virtual {p1, v1}, Landroid/support/v7/preference/PreferenceScreen;->dlg(Ljava/lang/CharSequence;)Landroid/support/v7/preference/Preference;

    move-result-object v1

    iget-object v2, p0, Lcom/android/settings/backup/l;->aUp:Landroid/content/Intent;

    invoke-virtual {v0, v2}, Landroid/support/v7/preference/Preference;->setIntent(Landroid/content/Intent;)V

    iget-object v2, p0, Lcom/android/settings/backup/l;->aUr:Ljava/lang/String;

    invoke-virtual {v0, v2}, Landroid/support/v7/preference/Preference;->setTitle(Ljava/lang/CharSequence;)V

    iget-object v2, p0, Lcom/android/settings/backup/l;->aUq:Ljava/lang/String;

    invoke-virtual {v0, v2}, Landroid/support/v7/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/android/settings/backup/l;->aUs:Landroid/content/Intent;

    invoke-virtual {v1, v0}, Landroid/support/v7/preference/Preference;->setIntent(Landroid/content/Intent;)V

    iget-object v0, p0, Lcom/android/settings/backup/l;->aUt:Ljava/lang/String;

    invoke-virtual {v1, v0}, Landroid/support/v7/preference/Preference;->setTitle(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public b()Ljava/lang/String;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public c()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method
