.class public Lcom/android/settings/backup/v;
.super Lcom/android/settings/core/e;
.source "BackupSettingsActivityPreferenceController.java"


# instance fields
.field private final aUN:Landroid/app/backup/BackupManager;

.field private final aUO:Landroid/os/UserManager;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    invoke-direct {p0, p1}, Lcom/android/settings/core/e;-><init>(Landroid/content/Context;)V

    const-string/jumbo v0, "user"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/UserManager;

    iput-object v0, p0, Lcom/android/settings/backup/v;->aUO:Landroid/os/UserManager;

    new-instance v0, Landroid/app/backup/BackupManager;

    invoke-direct {v0, p1}, Landroid/app/backup/BackupManager;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/settings/backup/v;->aUN:Landroid/app/backup/BackupManager;

    return-void
.end method


# virtual methods
.method public cz(Landroid/preference/Preference;)V
    .locals 1

    iget-object v0, p0, Lcom/android/settings/backup/v;->aUN:Landroid/app/backup/BackupManager;

    invoke-virtual {v0}, Landroid/app/backup/BackupManager;->isBackupEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    const v0, 0x7f12004b

    :goto_0
    invoke-virtual {p1, v0}, Landroid/preference/Preference;->setSummary(I)V

    return-void

    :cond_0
    const v0, 0x7f12004a

    goto :goto_0
.end method

.method public l()Ljava/lang/String;
    .locals 1

    const-string/jumbo v0, "backup_settings"

    return-object v0
.end method

.method public p()Z
    .locals 1

    iget-object v0, p0, Lcom/android/settings/backup/v;->aUO:Landroid/os/UserManager;

    invoke-virtual {v0}, Landroid/os/UserManager;->isAdminUser()Z

    move-result v0

    return v0
.end method
