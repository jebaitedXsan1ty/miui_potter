.class public Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;
.super Lcom/android/settings/KeyguardSettingsPreferenceFragment;
.source "SetUpChooseLockPassword.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/text/TextWatcher;


# instance fields
.field private cgA:I

.field private cgB:Landroid/os/AsyncTask;

.field private cgC:I

.field private cgD:I

.field private cgE:Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment$Stage;

.field private cgF:I

.field private cgd:Landroid/widget/TextView;

.field private cge:Landroid/content/ClipData;

.field private cgf:Z

.field private cgg:Lcom/android/settings/bM;

.field private cgh:Ljava/lang/String;

.field private cgi:Landroid/os/Handler;

.field private cgj:Landroid/widget/TextView;

.field private cgk:Landroid/widget/TextView;

.field private cgl:Z

.field private cgm:Z

.field private cgn:Z

.field private cgo:Z

.field private final cgp:Lmiui/view/MiuiKeyBoardView$OnKeyboardActionListener;

.field private cgq:Lmiui/view/MiuiKeyBoardView;

.field private cgr:Landroid/widget/TextView;

.field private cgs:Landroid/widget/TextView;

.field private cgt:I

.field private cgu:I

.field private cgv:I

.field private cgw:I

.field private cgx:I

.field private cgy:I

.field private cgz:I

.field private mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;


# direct methods
.method public constructor <init>()V
    .locals 3

    const/4 v2, 0x0

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/android/settings/KeyguardSettingsPreferenceFragment;-><init>()V

    const/4 v0, 0x4

    iput v0, p0, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->cgu:I

    const/16 v0, 0x10

    iput v0, p0, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->cgt:I

    iput v1, p0, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->cgv:I

    iput v1, p0, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->cgA:I

    iput v1, p0, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->cgw:I

    iput v1, p0, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->cgz:I

    iput v1, p0, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->cgy:I

    iput v1, p0, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->cgx:I

    const/high16 v0, 0x20000

    iput v0, p0, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->cgC:I

    sget-object v0, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment$Stage;->cgH:Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment$Stage;

    iput-object v0, p0, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->cgE:Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment$Stage;

    iput-object v2, p0, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->cgg:Lcom/android/settings/bM;

    iput-boolean v1, p0, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->cgm:Z

    new-instance v0, Lcom/android/settings/ln;

    invoke-direct {v0, p0}, Lcom/android/settings/ln;-><init>(Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;)V

    iput-object v0, p0, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->cgi:Landroid/os/Handler;

    new-instance v0, Lcom/android/settings/lo;

    invoke-direct {v0, p0}, Lcom/android/settings/lo;-><init>(Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;)V

    iput-object v0, p0, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->cgp:Lmiui/view/MiuiKeyBoardView$OnKeyboardActionListener;

    iput-object v2, p0, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->cge:Landroid/content/ClipData;

    return-void
.end method

.method private bYZ(Ljava/lang/String;)V
    .locals 3

    const/4 v2, 0x0

    iget v0, p0, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->cgD:I

    const/16 v1, -0x2710

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lmiui/securityspace/CrossUserUtils;->hasAirSpace(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_0
    iget-object v0, p0, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->cgq:Lmiui/view/MiuiKeyBoardView;

    invoke-virtual {v0, v2}, Lmiui/view/MiuiKeyBoardView;->setEnabled(Z)V

    iget-object v0, p0, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->cgB:Landroid/os/AsyncTask;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->cgB:Landroid/os/AsyncTask;

    invoke-virtual {v0, v2}, Landroid/os/AsyncTask;->cancel(Z)Z

    :cond_1
    iget-object v0, p0, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

    invoke-virtual {p0}, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->bZb(Landroid/content/Context;)Ljava/util/List;

    move-result-object v1

    new-instance v2, Lcom/android/settings/lq;

    invoke-direct {v2, p0, p1}, Lcom/android/settings/lq;-><init>(Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;Ljava/lang/String;)V

    invoke-static {v0, p1, v1, v2}, Lcom/android/settings/L;->blT(Lcom/android/internal/widget/LockPatternUtils;Ljava/lang/String;Ljava/util/List;Lcom/android/settings/N;)Landroid/os/AsyncTask;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->cgB:Landroid/os/AsyncTask;

    :goto_0
    return-void

    :cond_2
    invoke-direct {p0, p1}, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->bZc(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private bZa(I)V
    .locals 3

    invoke-virtual {p0}, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/app/FragmentManager;->popBackStackImmediate()Z

    :cond_0
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string/jumbo v1, "miui_security_fragment_result"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    invoke-virtual {p0}, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->getTargetFragment()Landroid/app/Fragment;

    move-result-object v1

    invoke-virtual {p0}, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->getTargetRequestCode()I

    move-result v2

    invoke-static {v1, v2, v0}, Lcom/android/settings/aN;->bwR(Landroid/app/Fragment;ILandroid/os/Bundle;)V

    return-void
.end method

.method private bZc(Ljava/lang/String;)V
    .locals 2

    iput-object p1, p0, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->cgh:Ljava/lang/String;

    iget-object v0, p0, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->cgs:Landroid/widget/TextView;

    const-string/jumbo v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    sget-object v0, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment$Stage;->cgI:Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment$Stage;

    invoke-virtual {p0, v0}, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->bZi(Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment$Stage;)V

    return-void
.end method

.method private bZe([B)V
    .locals 3

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    if-eqz p1, :cond_0

    const-string/jumbo v1, "hw_auth_token"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[B)Landroid/content/Intent;

    :cond_0
    invoke-virtual {p0}, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    const/4 v2, -0x1

    invoke-virtual {v1, v2, v0}, Landroid/app/Activity;->setResult(ILandroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    return-void
.end method

.method private bZf([B)V
    .locals 4

    const/4 v3, 0x0

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    instance-of v0, v0, Lcom/android/settings/MiuiSettings;

    if-eqz v0, :cond_1

    invoke-direct {p0, v3}, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->bZa(I)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0}, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    if-eqz v0, :cond_2

    const-string/jumbo v1, "has_challenge"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-direct {p0, p1}, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->bZe([B)V

    return-void

    :cond_2
    invoke-direct {p0, v2}, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->bZe([B)V

    goto :goto_0
.end method

.method private bZg(Ljava/lang/String;)V
    .locals 6

    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string/jumbo v3, "lockscreen.biometric_weak_fallback"

    invoke-virtual {v2, v3, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v3

    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0x17

    if-ge v2, v4, :cond_2

    iget-object v2, p0, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

    iget v4, p0, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->cgF:I

    invoke-static {v2, v4, v3}, Lcom/android/settings/bn;->bFj(Lcom/android/internal/widget/LockPatternUtils;IZ)V

    :goto_0
    invoke-virtual {p0}, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->bWB()Landroid/content/ContentResolver;

    move-result-object v2

    const-string/jumbo v4, "is_security_encryption_enabled"

    invoke-static {v2, v4, v1}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    if-ne v2, v0, :cond_3

    move v2, v0

    :goto_1
    if-nez v2, :cond_0

    invoke-virtual {p0}, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string/jumbo v4, "use_lock_password_to_encrypt_device"

    invoke-virtual {v2, v4, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    iget-object v2, p0, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

    invoke-static {v2, v0}, Lcom/android/settings/bn;->bFk(Lcom/android/internal/widget/LockPatternUtils;Z)V

    :cond_1
    iget-object v2, p0, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->cgr:Landroid/widget/TextView;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setEnabled(Z)V

    iget-object v2, p0, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->cgd:Landroid/widget/TextView;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setEnabled(Z)V

    invoke-virtual {p0}, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string/jumbo v4, "new_numeric_password_type"

    iget v5, p0, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->cgF:I

    invoke-static {v2, v4, v0, v5}, Landroid/provider/MiuiSettings$System;->putBooleanForUser(Landroid/content/ContentResolver;Ljava/lang/String;ZI)Z

    new-instance v2, Lcom/android/settings/bM;

    invoke-virtual {p0}, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->getActivity()Landroid/app/Activity;

    move-result-object v4

    invoke-direct {v2, v4}, Lcom/android/settings/bM;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->cgg:Lcom/android/settings/bM;

    iget-object v2, p0, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->cgg:Lcom/android/settings/bM;

    invoke-virtual {v2}, Lcom/android/settings/bM;->bNd()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-static {}, Lcom/android/settings/SetUpChooseLockPassword;->-get0()Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->cgg:Lcom/android/settings/bM;

    invoke-virtual {v2}, Lcom/android/settings/bM;->bNe()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-nez v2, :cond_4

    :goto_2
    new-instance v2, Lcom/android/settings/lr;

    invoke-direct {v2, p0, p1, v3, v0}, Lcom/android/settings/lr;-><init>(Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;Ljava/lang/String;ZZ)V

    new-array v0, v1, [Ljava/lang/Void;

    invoke-virtual {v2, v0}, Lcom/android/settings/lr;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    return-void

    :cond_2
    iget-object v2, p0, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

    invoke-static {v2, v1}, Lcom/android/settings/bn;->bFk(Lcom/android/internal/widget/LockPatternUtils;Z)V

    goto :goto_0

    :cond_3
    move v2, v1

    goto :goto_1

    :cond_4
    move v0, v1

    goto :goto_2
.end method

.method private bZh(Ljava/lang/String;Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment$Stage;)V
    .locals 4

    const/4 v2, 0x1

    iget-object v0, p0, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->cgj:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->cgj:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->cgj:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->announceForAccessibility(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->cgi:Landroid/os/Handler;

    invoke-virtual {v0, v2, p2}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->cgi:Landroid/os/Handler;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeMessages(I)V

    iget-object v1, p0, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->cgi:Landroid/os/Handler;

    const-wide/16 v2, 0xbb8

    invoke-virtual {v1, v0, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    return-void
.end method

.method private bZj(Ljava/lang/String;)Ljava/lang/String;
    .locals 11

    const/4 v8, 0x1

    const/4 v1, 0x0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    iget v2, p0, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->cgu:I

    if-ge v0, v2, :cond_2

    iget-boolean v0, p0, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->cgl:Z

    if-eqz v0, :cond_0

    new-array v0, v8, [Ljava/lang/Object;

    iget v2, p0, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->cgu:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const v1, 0x7f1209b8

    invoke-virtual {p0, v1, v0}, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    iget v0, p0, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->cgu:I

    iget v2, p0, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->cgt:I

    if-ne v0, v2, :cond_1

    invoke-virtual {p0}, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iget v2, p0, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->cgu:I

    new-array v3, v8, [Ljava/lang/Object;

    iget v4, p0, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->cgu:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v1

    const v1, 0x7f100020

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iget v2, p0, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->cgu:I

    new-array v3, v8, [Ljava/lang/Object;

    iget v4, p0, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->cgu:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v1

    const v1, 0x7f100022

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_2
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    iget v2, p0, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->cgt:I

    if-le v0, v2, :cond_4

    iget-boolean v0, p0, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->cgl:Z

    if-eqz v0, :cond_3

    new-array v0, v8, [Ljava/lang/Object;

    iget v2, p0, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->cgt:I

    add-int/lit8 v2, v2, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const v1, 0x7f1209b7

    invoke-virtual {p0, v1, v0}, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    :goto_1
    return-object v0

    :cond_3
    invoke-virtual {p0}, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iget v2, p0, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->cgt:I

    add-int/lit8 v2, v2, 0x1

    new-array v3, v8, [Ljava/lang/Object;

    iget v4, p0, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->cgt:I

    add-int/lit8 v4, v4, 0x1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v1

    const v1, 0x7f100021

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_4
    move v0, v1

    move v2, v1

    move v3, v1

    move v4, v1

    move v5, v1

    move v6, v1

    move v7, v1

    :goto_2
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v9

    if-ge v0, v9, :cond_a

    invoke-virtual {p1, v0}, Ljava/lang/String;->charAt(I)C

    move-result v9

    const/16 v10, 0x20

    if-lt v9, v10, :cond_5

    const/16 v10, 0x7f

    if-le v9, v10, :cond_6

    :cond_5
    const v0, 0x7f1209ae

    invoke-virtual {p0, v0}, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_6
    const/16 v10, 0x30

    if-lt v9, v10, :cond_7

    const/16 v10, 0x39

    if-gt v9, v10, :cond_7

    add-int/lit8 v6, v6, 0x1

    add-int/lit8 v2, v2, 0x1

    :goto_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_7
    const/16 v10, 0x41

    if-lt v9, v10, :cond_8

    const/16 v10, 0x5a

    if-gt v9, v10, :cond_8

    add-int/lit8 v7, v7, 0x1

    add-int/lit8 v3, v3, 0x1

    goto :goto_3

    :cond_8
    const/16 v10, 0x61

    if-lt v9, v10, :cond_9

    const/16 v10, 0x7a

    if-gt v9, v10, :cond_9

    add-int/lit8 v7, v7, 0x1

    add-int/lit8 v5, v5, 0x1

    goto :goto_3

    :cond_9
    add-int/lit8 v4, v4, 0x1

    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    :cond_a
    iget v0, p0, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->cgC:I

    const/high16 v9, 0x20000

    if-eq v9, v0, :cond_b

    iget v0, p0, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->cgC:I

    const/high16 v9, 0x30000

    if-ne v0, v9, :cond_d

    :cond_b
    if-gtz v7, :cond_c

    if-lez v4, :cond_d

    :cond_c
    const v0, 0x7f1209ba

    invoke-virtual {p0, v0}, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_d
    iget v0, p0, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->cgC:I

    const/high16 v9, 0x60000

    if-ne v9, v0, :cond_13

    iget v0, p0, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->cgv:I

    if-ge v7, v0, :cond_e

    invoke-virtual {p0}, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iget v2, p0, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->cgv:I

    const v3, 0x7f10001a

    invoke-virtual {v0, v3, v2}, Landroid/content/res/Resources;->getQuantityString(II)Ljava/lang/String;

    move-result-object v0

    new-array v2, v8, [Ljava/lang/Object;

    iget v3, p0, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->cgv:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v1

    invoke-static {v0, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_e
    iget v0, p0, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->cgy:I

    if-ge v6, v0, :cond_f

    invoke-virtual {p0}, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iget v2, p0, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->cgy:I

    const v3, 0x7f10001d

    invoke-virtual {v0, v3, v2}, Landroid/content/res/Resources;->getQuantityString(II)Ljava/lang/String;

    move-result-object v0

    new-array v2, v8, [Ljava/lang/Object;

    iget v3, p0, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->cgy:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v1

    invoke-static {v0, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_f
    iget v0, p0, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->cgw:I

    if-ge v5, v0, :cond_10

    invoke-virtual {p0}, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iget v2, p0, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->cgw:I

    const v3, 0x7f10001b

    invoke-virtual {v0, v3, v2}, Landroid/content/res/Resources;->getQuantityString(II)Ljava/lang/String;

    move-result-object v0

    new-array v2, v8, [Ljava/lang/Object;

    iget v3, p0, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->cgw:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v1

    invoke-static {v0, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_10
    iget v0, p0, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->cgA:I

    if-ge v3, v0, :cond_11

    invoke-virtual {p0}, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iget v2, p0, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->cgA:I

    const v3, 0x7f10001f

    invoke-virtual {v0, v3, v2}, Landroid/content/res/Resources;->getQuantityString(II)Ljava/lang/String;

    move-result-object v0

    new-array v2, v8, [Ljava/lang/Object;

    iget v3, p0, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->cgA:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v1

    invoke-static {v0, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_11
    iget v0, p0, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->cgz:I

    if-ge v4, v0, :cond_12

    invoke-virtual {p0}, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iget v2, p0, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->cgz:I

    const v3, 0x7f10001e

    invoke-virtual {v0, v3, v2}, Landroid/content/res/Resources;->getQuantityString(II)Ljava/lang/String;

    move-result-object v0

    new-array v2, v8, [Ljava/lang/Object;

    iget v3, p0, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->cgz:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v1

    invoke-static {v0, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_12
    iget v0, p0, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->cgx:I

    if-ge v2, v0, :cond_15

    invoke-virtual {p0}, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iget v2, p0, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->cgx:I

    const v3, 0x7f10001c

    invoke-virtual {v0, v3, v2}, Landroid/content/res/Resources;->getQuantityString(II)Ljava/lang/String;

    move-result-object v0

    new-array v2, v8, [Ljava/lang/Object;

    iget v3, p0, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->cgx:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v1

    invoke-static {v0, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_13
    iget v0, p0, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->cgC:I

    const/high16 v2, 0x40000

    iget v0, p0, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->cgC:I

    const/high16 v2, 0x50000

    if-ne v2, v0, :cond_14

    move v1, v8

    :cond_14
    if-eqz v1, :cond_15

    if-nez v6, :cond_15

    const v0, 0x7f1209b4

    invoke-virtual {p0, v0}, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_15
    iget-object v0, p0, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

    invoke-static {v0, p1}, Lcom/android/settings/bn;->bFD(Lcom/android/internal/widget/LockPatternUtils;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_17

    iget-boolean v0, p0, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->cgl:Z

    if-eqz v0, :cond_16

    const v0, 0x7f1209b2

    :goto_4
    invoke-virtual {p0, v0}, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_16
    const v0, 0x7f1209bc

    goto :goto_4

    :cond_17
    const/4 v0, 0x0

    return-object v0
.end method

.method static synthetic bZk(Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;)Lcom/android/settings/bM;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->cgg:Lcom/android/settings/bM;

    return-object v0
.end method

.method static synthetic bZl(Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->cgm:Z

    return v0
.end method

.method static synthetic bZm(Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;)Lmiui/view/MiuiKeyBoardView;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->cgq:Lmiui/view/MiuiKeyBoardView;

    return-object v0
.end method

.method static synthetic bZn(Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;)Lcom/android/internal/widget/LockPatternUtils;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

    return-object v0
.end method

.method static synthetic bZo(Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;)Landroid/widget/TextView;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->cgs:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic bZp(Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;)I
    .locals 1

    iget v0, p0, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->cgC:I

    return v0
.end method

.method static synthetic bZq(Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;)I
    .locals 1

    iget v0, p0, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->cgD:I

    return v0
.end method

.method static synthetic bZr(Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;)Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment$Stage;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->cgE:Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment$Stage;

    return-object v0
.end method

.method static synthetic bZs(Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;)I
    .locals 1

    iget v0, p0, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->cgF:I

    return v0
.end method

.method static synthetic bZt(Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->cgm:Z

    return p1
.end method

.method static synthetic bZu(Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;Landroid/os/AsyncTask;)Landroid/os/AsyncTask;
    .locals 0

    iput-object p1, p0, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->cgB:Landroid/os/AsyncTask;

    return-object p1
.end method

.method static synthetic bZv(Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->bZc(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic bZw(Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;[B)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->bZf([B)V

    return-void
.end method

.method static synthetic bZx(Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;Ljava/lang/String;Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment$Stage;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->bZh(Ljava/lang/String;Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment$Stage;)V

    return-void
.end method

.method private updateUi()V
    .locals 7

    const/4 v6, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->cgs:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    iget-object v4, p0, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->cgE:Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment$Stage;

    sget-object v5, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment$Stage;->cgH:Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment$Stage;

    if-ne v4, v5, :cond_4

    if-lez v3, :cond_4

    iget v4, p0, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->cgu:I

    if-ge v3, v4, :cond_2

    iget-boolean v0, p0, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->cgl:Z

    if-eqz v0, :cond_0

    new-array v0, v1, [Ljava/lang/Object;

    iget v1, p0, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->cgu:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v2

    const v1, 0x7f1209b8

    invoke-virtual {p0, v1, v0}, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    iget-object v1, p0, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->cgj:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->cgr:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setEnabled(Z)V

    :goto_1
    iget-object v0, p0, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->cgr:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->cgE:Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment$Stage;

    iget v1, v1, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment$Stage;->buttonText:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    return-void

    :cond_0
    iget v0, p0, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->cgu:I

    iget v3, p0, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->cgt:I

    if-ne v0, v3, :cond_1

    invoke-virtual {p0}, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iget v3, p0, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->cgu:I

    new-array v1, v1, [Ljava/lang/Object;

    iget v4, p0, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->cgu:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v1, v2

    const v4, 0x7f100020

    invoke-virtual {v0, v4, v3, v1}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iget v3, p0, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->cgu:I

    new-array v1, v1, [Ljava/lang/Object;

    iget v4, p0, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->cgu:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v1, v2

    const v4, 0x7f100022

    invoke-virtual {v0, v4, v3, v1}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_2
    invoke-direct {p0, v0}, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->bZj(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_3

    iget-object v1, p0, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->cgj:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->cgr:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setEnabled(Z)V

    goto :goto_1

    :cond_3
    iget-object v0, p0, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->cgj:Landroid/widget/TextView;

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->cgr:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setEnabled(Z)V

    goto :goto_1

    :cond_4
    iget-boolean v0, p0, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->cgo:Z

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->cgE:Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment$Stage;

    sget-object v4, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment$Stage;->cgH:Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment$Stage;

    if-ne v0, v4, :cond_6

    iget-object v4, p0, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->cgj:Landroid/widget/TextView;

    iget-boolean v0, p0, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->cgl:Z

    if-eqz v0, :cond_5

    const v0, 0x7f120998

    :goto_2
    invoke-virtual {v4, v0}, Landroid/widget/TextView;->setText(I)V

    :goto_3
    iget-object v4, p0, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->cgr:Landroid/widget/TextView;

    if-lez v3, :cond_8

    move v0, v1

    :goto_4
    invoke-virtual {v4, v0}, Landroid/widget/TextView;->setEnabled(Z)V

    goto :goto_1

    :cond_5
    const v0, 0x7f12099b

    goto :goto_2

    :cond_6
    iget-object v4, p0, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->cgj:Landroid/widget/TextView;

    iget-boolean v0, p0, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->cgl:Z

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->cgE:Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment$Stage;

    iget v0, v0, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment$Stage;->alphaHint:I

    :goto_5
    invoke-virtual {v4, v0}, Landroid/widget/TextView;->setText(I)V

    goto :goto_3

    :cond_7
    iget-object v0, p0, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->cgE:Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment$Stage;

    iget v0, v0, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment$Stage;->numericHint:I

    goto :goto_5

    :cond_8
    move v0, v2

    goto :goto_4
.end method


# virtual methods
.method public QH(ILandroid/os/Bundle;)V
    .locals 3

    const/4 v0, 0x0

    if-eqz p2, :cond_0

    const-string/jumbo v1, "miui_security_fragment_result"

    const/4 v2, -0x1

    invoke-virtual {p2, v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    const/16 v1, 0x3a

    if-ne p1, v1, :cond_1

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->finish()V

    :cond_1
    return-void
.end method

.method public Vg(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    const/4 v2, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ActionBar;->isShowing()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v0}, Landroid/app/ActionBar;->hide()V

    :cond_0
    const v0, 0x7f0d01c5

    invoke-virtual {p1, v0, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v3

    const v0, 0x7f0a00a5

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->cgd:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->cgd:Landroid/widget/TextView;

    invoke-virtual {v0, p0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f0a00aa

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->cgr:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->cgr:Landroid/widget/TextView;

    const v4, 0x7f121054

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(I)V

    iget-object v0, p0, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->cgr:Landroid/widget/TextView;

    invoke-virtual {v0, p0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget v0, p0, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->cgC:I

    const/high16 v4, 0x40000

    if-eq v4, v0, :cond_1

    iget v0, p0, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->cgC:I

    const/high16 v4, 0x50000

    if-ne v4, v0, :cond_7

    :cond_1
    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->cgl:Z

    const v0, 0x7f0a023a

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lmiui/view/MiuiKeyBoardView;

    iput-object v0, p0, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->cgq:Lmiui/view/MiuiKeyBoardView;

    const v0, 0x7f0a03ef

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->cgs:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->cgs:Landroid/widget/TextView;

    invoke-virtual {v0, p0}, Landroid/widget/TextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    iget-boolean v0, p0, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->cgl:Z

    if-nez v0, :cond_2

    const/4 v0, 0x4

    iput v0, p0, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->cgt:I

    :cond_2
    iget-object v0, p0, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->cgs:Landroid/widget/TextView;

    new-instance v2, Lcom/android/settings/lp;

    invoke-direct {v2, p0}, Lcom/android/settings/lp;-><init>(Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;)V

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    iget-object v0, p0, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->cgq:Lmiui/view/MiuiKeyBoardView;

    iget-object v2, p0, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->cgp:Lmiui/view/MiuiKeyBoardView$OnKeyboardActionListener;

    invoke-virtual {v0, v2}, Lmiui/view/MiuiKeyBoardView;->addKeyboardListener(Lmiui/view/MiuiKeyBoardView$OnKeyboardActionListener;)V

    const v0, 0x7f0a03e5

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->cgj:Landroid/widget/TextView;

    const v0, 0x7f0a03e6

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->cgk:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->cgk:Landroid/widget/TextView;

    iget-boolean v0, p0, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->cgl:Z

    if-eqz v0, :cond_9

    const v0, 0x7f121045

    :goto_1
    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(I)V

    iget-object v0, p0, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->cgs:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getInputType()I

    move-result v0

    iget-object v2, p0, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->cgs:Landroid/widget/TextView;

    iget-boolean v4, p0, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->cgl:Z

    if-eqz v4, :cond_a

    :goto_2
    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setInputType(I)V

    if-nez p3, :cond_b

    invoke-virtual {p0}, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->getTargetFragment()Landroid/app/Fragment;

    move-result-object v0

    if-nez v0, :cond_3

    iget v0, p0, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->cgF:I

    const/16 v2, 0x3a

    invoke-static {p0, v2, v0}, Lcom/android/settings/SetUpChooseLockPassword;->bYW(Lcom/android/settings/KeyguardSettingsPreferenceFragment;II)V

    :cond_3
    sget-object v0, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment$Stage;->cgH:Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment$Stage;

    invoke-virtual {p0, v0}, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->bZi(Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment$Stage;)V

    :cond_4
    :goto_3
    invoke-virtual {p0}, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    instance-of v2, v0, Lmiui/preference/PreferenceActivity;

    if-eqz v2, :cond_5

    check-cast v0, Lmiui/preference/PreferenceActivity;

    iget-boolean v2, p0, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->cgl:Z

    if-eqz v2, :cond_c

    const v2, 0x7f120997

    :goto_4
    invoke-virtual {p0, v2}, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v0, v2, v2}, Lmiui/preference/PreferenceActivity;->showBreadCrumbs(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    :cond_5
    iget-boolean v0, p0, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->cgl:Z

    if-eqz v0, :cond_d

    invoke-virtual {p0}, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/high16 v2, 0x20000

    invoke-virtual {v0, v2}, Landroid/view/Window;->addFlags(I)V

    :goto_5
    invoke-virtual {p0}, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    instance-of v0, v0, Lcom/android/settings/SetUpChooseLockPassword;

    if-eqz v0, :cond_6

    invoke-virtual {v3}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    if-eqz v0, :cond_6

    invoke-virtual {v3}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    instance-of v0, v0, Landroid/preference/PreferenceFrameLayout$LayoutParams;

    if-eqz v0, :cond_6

    invoke-virtual {v3}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceFrameLayout$LayoutParams;

    iput-boolean v1, v0, Landroid/preference/PreferenceFrameLayout$LayoutParams;->removeBorders:Z

    :cond_6
    return-object v3

    :cond_7
    iget v0, p0, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->cgC:I

    const/high16 v4, 0x60000

    if-ne v4, v0, :cond_8

    move v0, v1

    goto/16 :goto_0

    :cond_8
    move v0, v2

    goto/16 :goto_0

    :cond_9
    const v0, 0x7f121048

    goto :goto_1

    :cond_a
    const/16 v0, 0x12

    goto :goto_2

    :cond_b
    const-string/jumbo v0, "first_pin"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->cgh:Ljava/lang/String;

    const-string/jumbo v0, "ui_stage"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_4

    invoke-static {v0}, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment$Stage;->valueOf(Ljava/lang/String;)Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment$Stage;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->cgE:Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment$Stage;

    iget-object v0, p0, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->cgE:Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment$Stage;

    invoke-virtual {p0, v0}, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->bZi(Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment$Stage;)V

    goto :goto_3

    :cond_c
    const v2, 0x7f12099a

    goto :goto_4

    :cond_d
    iget-object v0, p0, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->cgq:Lmiui/view/MiuiKeyBoardView;

    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Lmiui/view/MiuiKeyBoardView;->setVisibility(I)V

    goto :goto_5
.end method

.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->cgE:Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment$Stage;

    sget-object v1, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment$Stage;->cgG:Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment$Stage;

    if-ne v0, v1, :cond_0

    sget-object v0, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment$Stage;->cgI:Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment$Stage;

    iput-object v0, p0, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->cgE:Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment$Stage;

    :cond_0
    invoke-direct {p0}, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->updateUi()V

    return-void
.end method

.method protected bZb(Landroid/content/Context;)Ljava/util/List;
    .locals 1

    const-string/jumbo v0, "user"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/UserManager;

    invoke-virtual {v0}, Landroid/os/UserManager;->getUsers()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method protected bZd()V
    .locals 3

    iget-object v0, p0, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->cgs:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    return-void

    :cond_0
    iget-object v1, p0, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->cgE:Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment$Stage;

    sget-object v2, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment$Stage;->cgH:Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment$Stage;

    if-ne v1, v2, :cond_3

    invoke-direct {p0, v0}, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->bZj(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_2

    invoke-direct {p0, v0}, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->bYZ(Ljava/lang/String;)V

    :cond_1
    :goto_0
    return-void

    :cond_2
    iget-object v0, p0, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->cgE:Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment$Stage;

    invoke-direct {p0, v1, v0}, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->bZh(Ljava/lang/String;Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment$Stage;)V

    goto :goto_0

    :cond_3
    iget-object v1, p0, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->cgE:Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment$Stage;

    sget-object v2, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment$Stage;->cgI:Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment$Stage;

    if-ne v1, v2, :cond_1

    iget-object v1, p0, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->cgh:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-direct {p0, v0}, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->bZg(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/android/settings/aH;->bva(Landroid/content/Context;)V

    goto :goto_0

    :cond_4
    iget-object v0, p0, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->cgs:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    if-eqz v1, :cond_5

    move-object v0, v1

    check-cast v0, Landroid/text/Spannable;

    invoke-interface {v1}, Ljava/lang/CharSequence;->length()I

    move-result v1

    const/4 v2, 0x0

    invoke-static {v0, v2, v1}, Landroid/text/Selection;->setSelection(Landroid/text/Spannable;II)V

    iget-boolean v0, p0, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->cgl:Z

    if-eqz v0, :cond_5

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->cgm:Z

    :cond_5
    sget-object v0, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment$Stage;->cgG:Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment$Stage;

    invoke-virtual {p0, v0}, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->bZi(Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment$Stage;)V

    goto :goto_0
.end method

.method protected bZi(Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment$Stage;)V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->cgE:Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment$Stage;

    iput-object p1, p0, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->cgE:Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment$Stage;

    invoke-direct {p0}, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->updateUi()V

    if-eq v0, p1, :cond_0

    iget-object v0, p0, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->cgj:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->cgj:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->announceForAccessibility(Ljava/lang/CharSequence;)V

    :cond_0
    return-void
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    return-void
.end method

.method public cC()Ljava/lang/String;
    .locals 1

    const-class v0, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 2

    invoke-super {p0, p1, p2, p3}, Lcom/android/settings/KeyguardSettingsPreferenceFragment;->onActivityResult(IILandroid/content/Intent;)V

    packed-switch p1, :pswitch_data_0

    :cond_0
    :goto_0
    return-void

    :pswitch_0
    const/4 v0, -0x1

    if-eq p2, v0, :cond_0

    invoke-virtual {p0}, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/Activity;->setResult(I)V

    invoke-virtual {p0}, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x3a
        :pswitch_0
    .end packed-switch
.end method

.method public onClick(Landroid/view/View;)V
    .locals 1

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    :goto_0
    return-void

    :sswitch_0
    invoke-virtual {p0}, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->bZd()V

    goto :goto_0

    :sswitch_1
    invoke-virtual {p0}, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    instance-of v0, v0, Lcom/android/settings/MiuiSettings;

    if-eqz v0, :cond_0

    const/4 v0, -0x1

    invoke-direct {p0, v0}, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->bZa(I)V

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x7f0a00a5 -> :sswitch_1
        0x7f0a00aa -> :sswitch_0
    .end sparse-switch
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 5

    const/4 v4, 0x0

    const/16 v3, -0x2710

    invoke-super {p0, p1}, Lcom/android/settings/KeyguardSettingsPreferenceFragment;->onCreate(Landroid/os/Bundle;)V

    new-instance v0, Lcom/android/internal/widget/LockPatternUtils;

    invoke-virtual {p0}, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/android/internal/widget/LockPatternUtils;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

    invoke-virtual {p0}, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    invoke-virtual {p0}, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    instance-of v1, v1, Lcom/android/settings/SetUpChooseLockPassword;

    if-nez v1, :cond_0

    invoke-virtual {p0}, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    instance-of v1, v1, Lcom/android/settings/MiuiSettings;

    xor-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    new-instance v0, Ljava/lang/SecurityException;

    const-string/jumbo v1, "Fragment contained in wrong activity"

    invoke-direct {v0, v1}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    invoke-virtual {p0}, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    instance-of v1, v1, Lcom/android/settings/MiuiSettings;

    if-eqz v1, :cond_1

    invoke-virtual {p0}, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    :cond_1
    invoke-virtual {p0}, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    instance-of v1, v1, Lcom/android/settings/SetUpChooseLockPassword;

    if-eqz v1, :cond_2

    sget v1, Lmiui/R$style;->Theme_Light_Settings_NoTitle:I

    invoke-virtual {p0, v1}, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->setThemeRes(I)V

    :cond_2
    invoke-virtual {p0}, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->bWB()Landroid/content/ContentResolver;

    move-result-object v1

    const-string/jumbo v2, "second_user_id"

    invoke-static {v1, v2, v3, v4}, Landroid/provider/Settings$Secure;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v1

    iput v1, p0, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->cgD:I

    const-string/jumbo v1, "user_id_to_set_password"

    invoke-virtual {v0, v1, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->cgF:I

    iget v1, p0, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->cgF:I

    if-ne v1, v3, :cond_5

    invoke-virtual {p0}, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/android/settings/aN;->bwP(Landroid/content/Context;Landroid/os/Bundle;)I

    move-result v1

    :goto_0
    iput v1, p0, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->cgF:I

    iget v1, p0, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->cgF:I

    if-nez v1, :cond_3

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->cgn:Z

    :cond_3
    const-string/jumbo v1, "lockscreen.password_type"

    iget v2, p0, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->cgC:I

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v1

    iget-object v2, p0, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

    iget v3, p0, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->cgF:I

    invoke-static {v2, v3}, Lcom/android/settings/bn;->bFv(Lcom/android/internal/widget/LockPatternUtils;I)I

    move-result v2

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    iput v1, p0, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->cgC:I

    const-string/jumbo v1, "lockscreen.password_min"

    iget v2, p0, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->cgu:I

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v1

    iget-object v2, p0, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

    iget v3, p0, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->cgF:I

    invoke-static {v2, v3}, Lcom/android/settings/bn;->bFw(Lcom/android/internal/widget/LockPatternUtils;I)I

    move-result v2

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    iput v1, p0, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->cgu:I

    const-string/jumbo v1, "lockscreen.password_max"

    iget v2, p0, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->cgt:I

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->cgt:I

    const-string/jumbo v1, "lockscreen.password_min_letters"

    iget v2, p0, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->cgv:I

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v1

    iget-object v2, p0, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

    iget v3, p0, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->cgF:I

    invoke-static {v2, v3}, Lcom/android/settings/bn;->bFx(Lcom/android/internal/widget/LockPatternUtils;I)I

    move-result v2

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    iput v1, p0, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->cgv:I

    const-string/jumbo v1, "lockscreen.password_min_uppercase"

    iget v2, p0, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->cgA:I

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v1

    iget-object v2, p0, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

    iget v3, p0, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->cgF:I

    invoke-static {v2, v3}, Lcom/android/settings/bn;->bFy(Lcom/android/internal/widget/LockPatternUtils;I)I

    move-result v2

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    iput v1, p0, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->cgA:I

    const-string/jumbo v1, "lockscreen.password_min_lowercase"

    iget v2, p0, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->cgw:I

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v1

    iget-object v2, p0, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

    iget v3, p0, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->cgF:I

    invoke-static {v2, v3}, Lcom/android/settings/bn;->bFz(Lcom/android/internal/widget/LockPatternUtils;I)I

    move-result v2

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    iput v1, p0, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->cgw:I

    const-string/jumbo v1, "lockscreen.password_min_numeric"

    iget v2, p0, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->cgy:I

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v1

    iget-object v2, p0, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

    iget v3, p0, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->cgF:I

    invoke-static {v2, v3}, Lcom/android/settings/bn;->bFA(Lcom/android/internal/widget/LockPatternUtils;I)I

    move-result v2

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    iput v1, p0, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->cgy:I

    const-string/jumbo v1, "lockscreen.password_min_symbols"

    iget v2, p0, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->cgz:I

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v1

    iget-object v2, p0, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

    iget v3, p0, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->cgF:I

    invoke-static {v2, v3}, Lcom/android/settings/bn;->bFB(Lcom/android/internal/widget/LockPatternUtils;I)I

    move-result v2

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    iput v1, p0, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->cgz:I

    const-string/jumbo v1, "lockscreen.password_min_nonletter"

    iget v2, p0, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->cgx:I

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v1

    iget-object v2, p0, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

    iget v3, p0, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->cgF:I

    invoke-static {v2, v3}, Lcom/android/settings/bn;->bFC(Lcom/android/internal/widget/LockPatternUtils;I)I

    move-result v2

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    iput v1, p0, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->cgx:I

    const-string/jumbo v1, "set_keyguard_password"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->cgf:Z

    const-string/jumbo v1, "add_keyguard_password_then_add_fingerprint"

    invoke-virtual {v0, v1, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    invoke-static {v0}, Lcom/android/settings/SetUpChooseLockPassword;->bYY(Z)Z

    invoke-virtual {p0}, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->getTargetFragment()Landroid/app/Fragment;

    move-result-object v0

    invoke-static {v0}, Lcom/android/settings/aN;->bwQ(Landroid/app/Fragment;)Z

    move-result v0

    if-eqz v0, :cond_4

    iget v0, p0, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->cgF:I

    const/16 v1, 0x3a

    invoke-static {p0, v1, v0}, Lcom/android/settings/ChooseLockPassword;->bmX(Lcom/android/settings/KeyguardSettingsPreferenceFragment;II)V

    :cond_4
    return-void

    :cond_5
    iget v1, p0, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->cgF:I

    goto/16 :goto_0
.end method

.method public onDestroy()V
    .locals 2

    invoke-virtual {p0}, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    instance-of v0, v0, Lcom/android/settings/MiuiSettings;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->cgl:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/high16 v1, 0x20000

    invoke-virtual {v0, v1}, Landroid/view/Window;->clearFlags(I)V

    :cond_0
    invoke-super {p0}, Lcom/android/settings/KeyguardSettingsPreferenceFragment;->onDestroy()V

    return-void
.end method

.method public onPause()V
    .locals 4

    const/4 v3, 0x0

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->cgi:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    invoke-super {p0}, Lcom/android/settings/KeyguardSettingsPreferenceFragment;->onPause()V

    invoke-virtual {p0}, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const-string/jumbo v1, "input_method"

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    invoke-virtual {p0}, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->getView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    invoke-virtual {v0, v1, v3}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    iget-object v0, p0, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->cge:Landroid/content/ClipData;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const-string/jumbo v1, "clipboard"

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/ClipboardManager;

    iget-object v1, p0, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->cge:Landroid/content/ClipData;

    invoke-virtual {v0, v1}, Landroid/content/ClipboardManager;->setPrimaryClip(Landroid/content/ClipData;)V

    :cond_0
    iget-object v0, p0, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->cgB:Landroid/os/AsyncTask;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->cgB:Landroid/os/AsyncTask;

    invoke-virtual {v0, v3}, Landroid/os/AsyncTask;->cancel(Z)Z

    iput-object v2, p0, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->cgB:Landroid/os/AsyncTask;

    :cond_1
    return-void
.end method

.method public onResume()V
    .locals 3

    invoke-super {p0}, Lcom/android/settings/KeyguardSettingsPreferenceFragment;->onResume()V

    iget-object v0, p0, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->cgE:Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment$Stage;

    invoke-virtual {p0, v0}, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->bZi(Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment$Stage;)V

    iget-object v0, p0, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->cgs:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->requestFocus()Z

    iget-boolean v0, p0, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->cgl:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const-string/jumbo v1, "input_method"

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    iget-object v1, p0, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->cgs:Landroid/widget/TextView;

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->showSoftInput(Landroid/view/View;I)Z

    :cond_0
    invoke-virtual {p0}, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const-string/jumbo v1, "clipboard"

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/ClipboardManager;

    invoke-virtual {v0}, Landroid/content/ClipboardManager;->hasPrimaryClip()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {v0}, Landroid/content/ClipboardManager;->getPrimaryClip()Landroid/content/ClipData;

    move-result-object v1

    iput-object v1, p0, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->cge:Landroid/content/ClipData;

    const-string/jumbo v1, ""

    const/4 v2, 0x0

    invoke-static {v2, v1}, Landroid/content/ClipData;->newPlainText(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Landroid/content/ClipData;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/ClipboardManager;->setPrimaryClip(Landroid/content/ClipData;)V

    :cond_1
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Lcom/android/settings/KeyguardSettingsPreferenceFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    const-string/jumbo v0, "ui_stage"

    iget-object v1, p0, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->cgE:Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment$Stage;

    invoke-virtual {v1}, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment$Stage;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string/jumbo v0, "first_pin"

    iget-object v1, p0, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->cgh:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public onStart()V
    .locals 0

    invoke-super {p0}, Lcom/android/settings/KeyguardSettingsPreferenceFragment;->onStart()V

    invoke-static {p0}, Lcom/android/settings/bn;->bFg(Landroid/app/Fragment;)V

    return-void
.end method

.method public onStop()V
    .locals 0

    invoke-static {p0}, Lcom/android/settings/bn;->bFh(Landroid/app/Fragment;)V

    invoke-super {p0}, Lcom/android/settings/KeyguardSettingsPreferenceFragment;->onStop()V

    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    return-void
.end method
