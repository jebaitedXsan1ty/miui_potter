.class final Lcom/android/settings/iQ;
.super Landroid/content/BroadcastReceiver;
.source "MiuiTetherSettings.java"


# instance fields
.field final synthetic coU:Lcom/android/settings/MiuiTetherSettings;


# direct methods
.method constructor <init>(Lcom/android/settings/MiuiTetherSettings;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/iQ;->coU:Lcom/android/settings/MiuiTetherSettings;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 4

    const/4 v3, 0x0

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "android.net.conn.TETHER_STATE_CHANGED"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v0, p0, Lcom/android/settings/iQ;->coU:Lcom/android/settings/MiuiTetherSettings;

    invoke-static {v0}, Lcom/android/settings/MiuiTetherSettings;->bJu(Lcom/android/settings/MiuiTetherSettings;)V

    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/android/settings/iQ;->coU:Lcom/android/settings/MiuiTetherSettings;

    iget-object v1, p0, Lcom/android/settings/iQ;->coU:Lcom/android/settings/MiuiTetherSettings;

    invoke-static {v1}, Lcom/android/settings/MiuiTetherSettings;->bJo(Lcom/android/settings/MiuiTetherSettings;)Landroid/preference/CheckBoxPreference;

    move-result-object v1

    invoke-virtual {v1}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v1

    invoke-static {v0, v1}, Lcom/android/settings/MiuiTetherSettings;->bJt(Lcom/android/settings/MiuiTetherSettings;Z)V

    return-void

    :cond_1
    const-string/jumbo v1, "android.net.wifi.WIFI_AP_STATE_CHANGED"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/iQ;->coU:Lcom/android/settings/MiuiTetherSettings;

    invoke-static {v0}, Lcom/android/settings/MiuiTetherSettings;->bJr(Lcom/android/settings/MiuiTetherSettings;)Landroid/net/wifi/WifiManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/wifi/WifiManager;->getWifiApState()I

    move-result v0

    const/16 v1, 0xb

    if-ne v0, v1, :cond_2

    iget-object v0, p0, Lcom/android/settings/iQ;->coU:Lcom/android/settings/MiuiTetherSettings;

    invoke-static {v0}, Lcom/android/settings/MiuiTetherSettings;->bJp(Lcom/android/settings/MiuiTetherSettings;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/settings/iQ;->coU:Lcom/android/settings/MiuiTetherSettings;

    invoke-static {v0, v3}, Lcom/android/settings/MiuiTetherSettings;->bJs(Lcom/android/settings/MiuiTetherSettings;Z)Z

    const-string/jumbo v0, "TetherSettings"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Restarting WifiAp due to prior config change."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/settings/iQ;->coU:Lcom/android/settings/MiuiTetherSettings;

    invoke-static {v2}, Lcom/android/settings/MiuiTetherSettings;->bJq(Lcom/android/settings/MiuiTetherSettings;)Landroid/net/wifi/WifiConfiguration;

    move-result-object v2

    iget-object v2, v2, Landroid/net/wifi/WifiConfiguration;->SSID:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/settings/iQ;->coU:Lcom/android/settings/MiuiTetherSettings;

    invoke-static {v0, v3}, Lcom/android/settings/MiuiTetherSettings;->bJw(Lcom/android/settings/MiuiTetherSettings;I)V

    :cond_2
    iget-object v0, p0, Lcom/android/settings/iQ;->coU:Lcom/android/settings/MiuiTetherSettings;

    invoke-static {v0}, Lcom/android/settings/MiuiTetherSettings;->bJu(Lcom/android/settings/MiuiTetherSettings;)V

    goto :goto_0
.end method
