.class final Lcom/android/settings/dw;
.super Ljava/lang/Object;
.source "LockPatternView.java"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# direct methods
.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public createFromParcel(Landroid/os/Parcel;)Lcom/android/settings/LockPatternView$SavedState;
    .locals 2

    new-instance v0, Lcom/android/settings/LockPatternView$SavedState;

    const/4 v1, 0x0

    invoke-direct {v0, p1, v1}, Lcom/android/settings/LockPatternView$SavedState;-><init>(Landroid/os/Parcel;Lcom/android/settings/LockPatternView$SavedState;)V

    return-object v0
.end method

.method public bridge synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0, p1}, Lcom/android/settings/dw;->createFromParcel(Landroid/os/Parcel;)Lcom/android/settings/LockPatternView$SavedState;

    move-result-object v0

    return-object v0
.end method

.method public newArray(I)[Lcom/android/settings/LockPatternView$SavedState;
    .locals 1

    new-array v0, p1, [Lcom/android/settings/LockPatternView$SavedState;

    return-object v0
.end method

.method public bridge synthetic newArray(I)[Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0, p1}, Lcom/android/settings/dw;->newArray(I)[Lcom/android/settings/LockPatternView$SavedState;

    move-result-object v0

    return-object v0
.end method
