.class public Lcom/android/settings/device/e;
.super Ljava/lang/Object;
.source "UpdateBroadcastManager.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static aDq(I)I
    .locals 2

    const/4 v0, 0x0

    :goto_0
    if-lez p0, :cond_0

    add-int/lit8 v0, v0, 0x1

    add-int/lit8 v1, p0, -0x1

    and-int/2addr p0, v1

    goto :goto_0

    :cond_0
    return v0
.end method

.method private static aDr(Landroid/content/ContentResolver;)I
    .locals 2

    const/4 v1, 0x0

    :try_start_0
    const-string/jumbo v0, "com.android.settings.superscript_map"

    invoke-static {p0, v0}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/provider/Settings$SettingNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    :goto_0
    return v0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/provider/Settings$SettingNotFoundException;->printStackTrace()V

    move v0, v1

    goto :goto_0
.end method

.method public static aDs(Landroid/content/Context;Landroid/os/Bundle;)V
    .locals 2

    const-string/jumbo v0, "state"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    const/4 v1, 0x1

    invoke-static {p0, v1, v0}, Lcom/android/settings/device/e;->aDt(Landroid/content/Context;IZ)V

    return-void
.end method

.method public static aDt(Landroid/content/Context;IZ)V
    .locals 2

    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    new-instance v1, Lcom/android/settings/device/p;

    invoke-direct {v1, v0, p2, p1}, Lcom/android/settings/device/p;-><init>(Landroid/content/Context;ZI)V

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Void;

    invoke-virtual {v1, v0}, Lcom/android/settings/device/p;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    return-void
.end method

.method static synthetic aDu(I)I
    .locals 1

    invoke-static {p0}, Lcom/android/settings/device/e;->aDq(I)I

    move-result v0

    return v0
.end method

.method static synthetic aDv(Landroid/content/ContentResolver;)I
    .locals 1

    invoke-static {p0}, Lcom/android/settings/device/e;->aDr(Landroid/content/ContentResolver;)I

    move-result v0

    return v0
.end method
