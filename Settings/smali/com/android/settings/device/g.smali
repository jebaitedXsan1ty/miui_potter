.class Lcom/android/settings/device/g;
.super Ljava/lang/Object;
.source "MiuiMyDeviceSettings.java"

# interfaces
.implements Lcom/android/settings/device/k;


# instance fields
.field private aTj:Ljava/lang/ref/WeakReference;


# direct methods
.method public constructor <init>(Lcom/android/settings/device/MiuiMyDeviceSettings;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/android/settings/device/g;->aTj:Ljava/lang/ref/WeakReference;

    return-void
.end method


# virtual methods
.method public aDK(Ljava/lang/String;)V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/device/g;->aTj:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settings/device/MiuiMyDeviceSettings;

    if-eqz v0, :cond_0

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    xor-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/android/settings/device/MiuiMyDeviceSettings;->getActivity()Landroid/app/Activity;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {v0, p1}, Lcom/android/settings/device/MiuiMyDeviceSettings;->aDI(Lcom/android/settings/device/MiuiMyDeviceSettings;Ljava/lang/String;)V

    invoke-static {v0}, Lcom/android/settings/device/MiuiMyDeviceSettings;->aDF(Lcom/android/settings/device/MiuiMyDeviceSettings;)Lcom/android/settings/device/m;

    move-result-object v1

    invoke-static {v0}, Lcom/android/settings/device/MiuiMyDeviceSettings;->aDH(Lcom/android/settings/device/MiuiMyDeviceSettings;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v1, v0, p1}, Lcom/android/settings/device/m;->aEH(Landroid/view/View;Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public bridge synthetic aDL(Ljava/lang/Object;)V
    .locals 0

    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/android/settings/device/g;->aDK(Ljava/lang/String;)V

    return-void
.end method
