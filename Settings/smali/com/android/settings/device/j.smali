.class Lcom/android/settings/device/j;
.super Landroid/os/AsyncTask;
.source "HttpUtils.java"


# instance fields
.field private aTw:Lcom/android/settings/device/k;


# direct methods
.method public constructor <init>(Lcom/android/settings/device/k;)V
    .locals 0

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    iput-object p1, p0, Lcom/android/settings/device/j;->aTw:Lcom/android/settings/device/k;

    return-void
.end method


# virtual methods
.method protected varargs aEy([Ljava/lang/String;)Ljava/lang/Object;
    .locals 4

    const/4 v3, 0x1

    const/4 v2, 0x0

    const-string/jumbo v0, "do_get"

    aget-object v1, p1, v2

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    aget-object v0, p1, v3

    invoke-static {v0}, Lcom/android/settings/device/i;->aEs(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    const-string/jumbo v0, "do_post"

    aget-object v1, p1, v2

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    aget-object v0, p1, v3

    const/4 v1, 0x2

    aget-object v1, p1, v1

    invoke-static {v0, v1}, Lcom/android/settings/device/i;->aEu(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_1
    const-string/jumbo v0, "do_get_pic"

    aget-object v1, p1, v2

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    aget-object v0, p1, v3

    invoke-static {v0}, Lcom/android/settings/device/i;->aEx(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0

    :cond_2
    const/4 v0, 0x0

    return-object v0
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    check-cast p1, [Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/android/settings/device/j;->aEy([Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method protected onPostExecute(Ljava/lang/Object;)V
    .locals 1

    iget-object v0, p0, Lcom/android/settings/device/j;->aTw:Lcom/android/settings/device/k;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/device/j;->aTw:Lcom/android/settings/device/k;

    invoke-interface {v0, p1}, Lcom/android/settings/device/k;->aDL(Ljava/lang/Object;)V

    :cond_0
    return-void
.end method
