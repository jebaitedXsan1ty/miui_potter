.class Lcom/android/settings/device/n;
.super Landroid/widget/BaseAdapter;
.source "DeviceBasicInfoPresenter.java"


# instance fields
.field private aTA:Landroid/content/Context;

.field private aTB:[Ljava/lang/String;

.field private aTC:[Ljava/lang/String;

.field final synthetic aTD:Lcom/android/settings/device/m;

.field private aTz:[Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/android/settings/device/m;Landroid/content/Context;Ljava/lang/String;)V
    .locals 7

    const/4 v6, 0x5

    const/4 v0, 0x0

    const/4 v5, 0x6

    iput-object p1, p0, Lcom/android/settings/device/n;->aTD:Lcom/android/settings/device/m;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    new-array v1, v5, [Ljava/lang/String;

    const-string/jumbo v2, "device_description_cpu"

    aput-object v2, v1, v0

    const-string/jumbo v2, "device_description_battery"

    const/4 v3, 0x1

    aput-object v2, v1, v3

    const-string/jumbo v2, "device_description_camera"

    const/4 v3, 0x2

    aput-object v2, v1, v3

    const-string/jumbo v2, "device_description_screen"

    const/4 v3, 0x3

    aput-object v2, v1, v3

    const-string/jumbo v2, "device_description_resolution"

    const/4 v3, 0x4

    aput-object v2, v1, v3

    const-string/jumbo v2, "device_description_ram"

    aput-object v2, v1, v6

    iput-object v1, p0, Lcom/android/settings/device/n;->aTz:[Ljava/lang/String;

    new-array v1, v5, [Ljava/lang/String;

    iput-object v1, p0, Lcom/android/settings/device/n;->aTB:[Ljava/lang/String;

    new-array v1, v5, [Ljava/lang/String;

    iput-object v1, p0, Lcom/android/settings/device/n;->aTC:[Ljava/lang/String;

    iput-object p2, p0, Lcom/android/settings/device/n;->aTA:Landroid/content/Context;

    invoke-static {p3}, Lcom/android/settings/device/l;->aEC(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v1

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lorg/json/JSONArray;->length()I

    move-result v2

    if-lez v2, :cond_2

    :goto_0
    invoke-virtual {v1}, Lorg/json/JSONArray;->length()I

    move-result v2

    if-ge v0, v2, :cond_2

    invoke-static {v1, v0}, Lcom/android/settings/device/o;->aER(Lorg/json/JSONArray;I)Lorg/json/JSONObject;

    move-result-object v2

    invoke-static {v2}, Lcom/android/settings/device/l;->aED(Lorg/json/JSONObject;)I

    move-result v3

    if-ltz v3, :cond_1

    if-ge v3, v5, :cond_1

    invoke-static {v2}, Lcom/android/settings/device/l;->aEF(Lorg/json/JSONObject;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    iget-object v4, p0, Lcom/android/settings/device/n;->aTB:[Ljava/lang/String;

    aput-object v3, v4, v0

    :cond_0
    invoke-static {v2}, Lcom/android/settings/device/l;->aEE(Lorg/json/JSONObject;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    iget-object v3, p0, Lcom/android/settings/device/n;->aTC:[Ljava/lang/String;

    aput-object v2, v3, v0

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/android/settings/device/n;->aTC:[Ljava/lang/String;

    invoke-static {p1}, Lcom/android/settings/device/m;->aEK(Lcom/android/settings/device/m;)Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/android/settings/device/h;->getInstance(Landroid/content/Context;)Lcom/android/settings/device/h;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/settings/device/h;->aDN()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v6

    const-string/jumbo v0, "device_description_overlay"

    invoke-static {p2, v0}, Lcom/android/settings/device/h;->aEo(Landroid/content/Context;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_3

    array-length v1, v0

    if-ne v1, v5, :cond_3

    iput-object v0, p0, Lcom/android/settings/device/n;->aTz:[Ljava/lang/String;

    :cond_3
    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 1

    iget-object v0, p0, Lcom/android/settings/device/n;->aTB:[Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/device/n;->aTB:[Ljava/lang/String;

    array-length v0, v0

    return v0

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2

    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 5

    if-eqz p2, :cond_0

    :goto_0
    const v0, 0x7f0a0139

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    const v1, 0x1020014

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    const v2, 0x1020015

    invoke-virtual {p2, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iget-object v3, p0, Lcom/android/settings/device/n;->aTA:Landroid/content/Context;

    iget-object v4, p0, Lcom/android/settings/device/n;->aTz:[Ljava/lang/String;

    aget-object v4, v4, p1

    invoke-static {v3, v4}, Lcom/android/settings/device/h;->aEn(Landroid/content/Context;Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    iget-object v0, p0, Lcom/android/settings/device/n;->aTB:[Ljava/lang/String;

    aget-object v0, v0, p1

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/android/settings/device/n;->aTC:[Ljava/lang/String;

    aget-object v0, v0, p1

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-object p2

    :cond_0
    invoke-virtual {p3}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0d0098

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    goto :goto_0
.end method

.method public isEnabled(I)Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method
