.class public Lcom/android/settings/device/b;
.super Landroid/os/AsyncTask;
.source "MiuiDeviceInfoSettings.java"


# instance fields
.field private aSR:Ljava/lang/String;

.field private aSS:Ljava/lang/ref/WeakReference;

.field private aST:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/android/settings/device/MiuiDeviceInfoSettings;)V
    .locals 1

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/android/settings/device/b;->aSS:Ljava/lang/ref/WeakReference;

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/android/settings/device/b;->doInBackground([Ljava/lang/Void;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Void;
    .locals 2

    invoke-static {}, Landroid/app/AppGlobals;->getInitialApplication()Landroid/app/Application;

    move-result-object v0

    invoke-static {v0}, Lcom/android/settings/device/h;->getInstance(Landroid/content/Context;)Lcom/android/settings/device/h;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/settings/device/h;->aDM()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/android/settings/device/b;->aSR:Ljava/lang/String;

    invoke-static {v0}, Lcom/android/settings/device/h;->getInstance(Landroid/content/Context;)Lcom/android/settings/device/h;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/settings/device/h;->aDN()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/device/b;->aST:Ljava/lang/String;

    const/4 v0, 0x0

    return-object v0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    check-cast p1, Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/android/settings/device/b;->onPostExecute(Ljava/lang/Void;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/Void;)V
    .locals 3

    iget-object v0, p0, Lcom/android/settings/device/b;->aSS:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settings/device/MiuiDeviceInfoSettings;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/android/settings/device/b;->aSR:Ljava/lang/String;

    iget-object v2, p0, Lcom/android/settings/device/b;->aST:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/android/settings/device/MiuiDeviceInfoSettings;->aDk(Lcom/android/settings/device/MiuiDeviceInfoSettings;Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    return-void
.end method
