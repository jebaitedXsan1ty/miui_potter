.class public Lcom/android/settings/device/l;
.super Ljava/lang/Object;
.source "ParseMiShopDataUtils.java"


# static fields
.field private static aTx:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x1

    sput v0, Lcom/android/settings/device/l;->aTx:I

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static aEA(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    invoke-static {p0}, Lcom/android/settings/device/o;->aEL(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0

    const-string/jumbo v1, "Mishop"

    invoke-static {v0, v1}, Lcom/android/settings/device/o;->aEP(Lorg/json/JSONObject;Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0

    const-string/jumbo v1, "RightValue"

    invoke-static {v0, v1}, Lcom/android/settings/device/o;->aEO(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static aEB(Ljava/lang/String;)Z
    .locals 3

    invoke-static {p0}, Lcom/android/settings/device/o;->aEL(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0

    const-string/jumbo v1, "Mishop"

    invoke-static {v0, v1}, Lcom/android/settings/device/o;->aEP(Lorg/json/JSONObject;Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0

    const-string/jumbo v1, "ShowRedDot"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/android/settings/device/o;->aEQ(Lorg/json/JSONObject;Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public static aEC(Ljava/lang/String;)Lorg/json/JSONArray;
    .locals 2

    invoke-static {p0}, Lcom/android/settings/device/o;->aEL(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0

    const-string/jumbo v1, "BasicItems"

    invoke-static {v0, v1}, Lcom/android/settings/device/o;->aEN(Lorg/json/JSONObject;Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v0

    return-object v0
.end method

.method public static aED(Lorg/json/JSONObject;)I
    .locals 2

    const-string/jumbo v0, "Index"

    const/4 v1, -0x1

    invoke-static {p0, v0, v1}, Lcom/android/settings/device/o;->aEM(Lorg/json/JSONObject;Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public static aEE(Lorg/json/JSONObject;)Ljava/lang/String;
    .locals 1

    const-string/jumbo v0, "Summary"

    invoke-static {p0, v0}, Lcom/android/settings/device/o;->aEO(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static aEF(Lorg/json/JSONObject;)Ljava/lang/String;
    .locals 1

    const-string/jumbo v0, "Title"

    invoke-static {p0, v0}, Lcom/android/settings/device/o;->aEO(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static aEG(Ljava/lang/String;)Z
    .locals 3

    invoke-static {p0}, Lcom/android/settings/device/o;->aEL(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0

    const-string/jumbo v1, "BasicInfoToggle"

    const/4 v2, -0x1

    invoke-static {v0, v1, v2}, Lcom/android/settings/device/o;->aEM(Lorg/json/JSONObject;Ljava/lang/String;I)I

    move-result v0

    sget v1, Lcom/android/settings/device/l;->aTx:I

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static aEz(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    invoke-static {p0}, Lcom/android/settings/device/o;->aEL(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0

    const-string/jumbo v1, "Mishop"

    invoke-static {v0, v1}, Lcom/android/settings/device/o;->aEP(Lorg/json/JSONObject;Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0

    const-string/jumbo v1, "Url"

    invoke-static {v0, v1}, Lcom/android/settings/device/o;->aEO(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
