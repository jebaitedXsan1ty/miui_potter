.class public Lcom/android/settings/device/m;
.super Ljava/lang/Object;
.source "DeviceBasicInfoPresenter.java"


# static fields
.field private static aTy:Lcom/android/settings/device/m;


# instance fields
.field private mContext:Landroid/content/Context;


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/android/settings/device/m;->mContext:Landroid/content/Context;

    return-void
.end method

.method public static aEI(Landroid/content/Context;)Lcom/android/settings/device/m;
    .locals 1

    sget-object v0, Lcom/android/settings/device/m;->aTy:Lcom/android/settings/device/m;

    if-nez v0, :cond_0

    new-instance v0, Lcom/android/settings/device/m;

    invoke-direct {v0, p0}, Lcom/android/settings/device/m;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/android/settings/device/m;->aTy:Lcom/android/settings/device/m;

    :cond_0
    sget-object v0, Lcom/android/settings/device/m;->aTy:Lcom/android/settings/device/m;

    return-object v0
.end method

.method static synthetic aEK(Lcom/android/settings/device/m;)Landroid/content/Context;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/device/m;->mContext:Landroid/content/Context;

    return-object v0
.end method


# virtual methods
.method public aEH(Landroid/view/View;Ljava/lang/String;)V
    .locals 5

    const v4, 0x7f0a0141

    const/4 v3, 0x0

    const/16 v2, 0x8

    iget-object v0, p0, Lcom/android/settings/device/m;->mContext:Landroid/content/Context;

    if-nez v0, :cond_0

    return-void

    :cond_0
    const v0, 0x7f0a0131

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/GridView;

    invoke-static {p2}, Lcom/android/settings/device/l;->aEG(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {p1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {v0, v3}, Landroid/widget/GridView;->setVisibility(I)V

    new-instance v1, Lcom/android/settings/device/n;

    iget-object v2, p0, Lcom/android/settings/device/m;->mContext:Landroid/content/Context;

    invoke-direct {v1, p0, v2, p2}, Lcom/android/settings/device/n;-><init>(Lcom/android/settings/device/m;Landroid/content/Context;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/widget/GridView;->setAdapter(Landroid/widget/ListAdapter;)V

    :cond_1
    :goto_0
    return-void

    :cond_2
    invoke-virtual {v0}, Landroid/widget/GridView;->getVisibility()I

    move-result v1

    if-eq v1, v2, :cond_1

    invoke-virtual {v0, v2}, Landroid/widget/GridView;->setVisibility(I)V

    invoke-virtual {p1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method public aEJ()V
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/settings/device/m;->mContext:Landroid/content/Context;

    sput-object v0, Lcom/android/settings/device/m;->aTy:Lcom/android/settings/device/m;

    return-void
.end method
