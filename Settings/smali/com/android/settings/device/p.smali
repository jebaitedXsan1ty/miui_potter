.class final Lcom/android/settings/device/p;
.super Landroid/os/AsyncTask;
.source "UpdateBroadcastManager.java"


# instance fields
.field final synthetic aTE:Landroid/content/Context;

.field final synthetic aTF:Z

.field final synthetic aTG:I


# direct methods
.method constructor <init>(Landroid/content/Context;ZI)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/device/p;->aTE:Landroid/content/Context;

    iput-boolean p2, p0, Lcom/android/settings/device/p;->aTF:Z

    iput p3, p0, Lcom/android/settings/device/p;->aTG:I

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Integer;
    .locals 4

    iget-object v0, p0, Lcom/android/settings/device/p;->aTE:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-static {v1}, Lcom/android/settings/device/e;->aDv(Landroid/content/ContentResolver;)I

    move-result v0

    iget-boolean v2, p0, Lcom/android/settings/device/p;->aTF:Z

    if-eqz v2, :cond_0

    iget v2, p0, Lcom/android/settings/device/p;->aTG:I

    or-int/2addr v0, v2

    :goto_0
    const-string/jumbo v2, "com.android.settings.superscript_map"

    invoke-static {v1, v2, v0}, Landroid/provider/Settings$Global;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    const-string/jumbo v2, "com.android.settings.superscript_count"

    invoke-static {v0}, Lcom/android/settings/device/e;->aDu(I)I

    move-result v3

    invoke-static {v1, v2, v3}, Landroid/provider/Settings$Global;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0

    :cond_0
    iget v2, p0, Lcom/android/settings/device/p;->aTG:I

    not-int v2, v2

    and-int/2addr v0, v2

    goto :goto_0
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/android/settings/device/p;->doInBackground([Ljava/lang/Void;)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method
