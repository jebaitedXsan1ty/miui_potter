.class final Lcom/android/settings/gx;
.super Ljava/lang/Object;
.source "DeviceAdminAddFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic clU:Lcom/android/settings/DeviceAdminAddFragment;


# direct methods
.method constructor <init>(Lcom/android/settings/DeviceAdminAddFragment;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/gx;->clU:Lcom/android/settings/DeviceAdminAddFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5

    const/4 v4, -0x1

    iget-object v0, p0, Lcom/android/settings/gx;->clU:Lcom/android/settings/DeviceAdminAddFragment;

    iget-boolean v0, v0, Lcom/android/settings/DeviceAdminAddFragment;->bHu:Z

    if-eqz v0, :cond_1

    :try_start_0
    iget-object v0, p0, Lcom/android/settings/gx;->clU:Lcom/android/settings/DeviceAdminAddFragment;

    iget-object v0, v0, Lcom/android/settings/DeviceAdminAddFragment;->bHC:Landroid/app/admin/DevicePolicyManager;

    iget-object v1, p0, Lcom/android/settings/gx;->clU:Lcom/android/settings/DeviceAdminAddFragment;

    iget-object v1, v1, Lcom/android/settings/DeviceAdminAddFragment;->bHD:Landroid/app/admin/DeviceAdminInfo;

    invoke-virtual {v1}, Landroid/app/admin/DeviceAdminInfo;->getComponent()Landroid/content/ComponentName;

    move-result-object v1

    iget-object v2, p0, Lcom/android/settings/gx;->clU:Lcom/android/settings/DeviceAdminAddFragment;

    iget-boolean v2, v2, Lcom/android/settings/DeviceAdminAddFragment;->bHG:Z

    invoke-virtual {v0, v1, v2}, Landroid/app/admin/DevicePolicyManager;->setActiveAdmin(Landroid/content/ComponentName;Z)V

    iget-object v0, p0, Lcom/android/settings/gx;->clU:Lcom/android/settings/DeviceAdminAddFragment;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Lcom/android/settings/DeviceAdminAddFragment;->bLU(I)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/android/settings/gx;->clU:Lcom/android/settings/DeviceAdminAddFragment;

    invoke-virtual {v0}, Lcom/android/settings/DeviceAdminAddFragment;->finish()V

    :goto_1
    return-void

    :catch_0
    move-exception v0

    const-string/jumbo v1, "DeviceAdminAdd"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "Exception trying to activate admin "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/settings/gx;->clU:Lcom/android/settings/DeviceAdminAddFragment;

    iget-object v3, v3, Lcom/android/settings/DeviceAdminAddFragment;->bHD:Landroid/app/admin/DeviceAdminInfo;

    invoke-virtual {v3}, Landroid/app/admin/DeviceAdminInfo;->getComponent()Landroid/content/ComponentName;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    iget-object v0, p0, Lcom/android/settings/gx;->clU:Lcom/android/settings/DeviceAdminAddFragment;

    iget-object v0, v0, Lcom/android/settings/DeviceAdminAddFragment;->bHC:Landroid/app/admin/DevicePolicyManager;

    iget-object v1, p0, Lcom/android/settings/gx;->clU:Lcom/android/settings/DeviceAdminAddFragment;

    iget-object v1, v1, Lcom/android/settings/DeviceAdminAddFragment;->bHD:Landroid/app/admin/DeviceAdminInfo;

    invoke-virtual {v1}, Landroid/app/admin/DeviceAdminInfo;->getComponent()Landroid/content/ComponentName;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/admin/DevicePolicyManager;->isAdminActive(Landroid/content/ComponentName;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/gx;->clU:Lcom/android/settings/DeviceAdminAddFragment;

    invoke-virtual {v0, v4}, Lcom/android/settings/DeviceAdminAddFragment;->bLU(I)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/android/settings/gx;->clU:Lcom/android/settings/DeviceAdminAddFragment;

    iget-object v1, p0, Lcom/android/settings/gx;->clU:Lcom/android/settings/DeviceAdminAddFragment;

    iget-object v1, v1, Lcom/android/settings/DeviceAdminAddFragment;->bHD:Landroid/app/admin/DeviceAdminInfo;

    invoke-static {v0, v1}, Lcom/android/settings/DeviceAdminAddFragment;->byj(Lcom/android/settings/DeviceAdminAddFragment;Landroid/app/admin/DeviceAdminInfo;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/settings/gx;->clU:Lcom/android/settings/DeviceAdminAddFragment;

    iget-object v0, v0, Lcom/android/settings/DeviceAdminAddFragment;->bHD:Landroid/app/admin/DeviceAdminInfo;

    invoke-virtual {v0}, Landroid/app/admin/DeviceAdminInfo;->getComponent()Landroid/content/ComponentName;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/gx;->clU:Lcom/android/settings/DeviceAdminAddFragment;

    iget-object v1, v1, Lcom/android/settings/DeviceAdminAddFragment;->bHC:Landroid/app/admin/DevicePolicyManager;

    invoke-virtual {v1}, Landroid/app/admin/DevicePolicyManager;->getProfileOwner()Landroid/content/ComponentName;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/ComponentName;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v0

    iget-object v1, p0, Lcom/android/settings/gx;->clU:Lcom/android/settings/DeviceAdminAddFragment;

    invoke-virtual {v1}, Lcom/android/settings/DeviceAdminAddFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    new-instance v2, Lcom/android/settings/gy;

    invoke-direct {v2, p0, v0}, Lcom/android/settings/gy;-><init>(Lcom/android/settings/gx;I)V

    invoke-static {v1, v0, v2}, Lcom/android/settings/users/UserDialogs;->aSL(Landroid/content/Context;ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/Dialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    goto :goto_1

    :cond_2
    :try_start_1
    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    move-result-object v0

    invoke-interface {v0}, Landroid/app/IActivityManager;->stopAppSwitches()V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_1

    :goto_2
    iget-object v0, p0, Lcom/android/settings/gx;->clU:Lcom/android/settings/DeviceAdminAddFragment;

    iget-object v0, v0, Lcom/android/settings/DeviceAdminAddFragment;->bHC:Landroid/app/admin/DevicePolicyManager;

    iget-object v1, p0, Lcom/android/settings/gx;->clU:Lcom/android/settings/DeviceAdminAddFragment;

    iget-object v1, v1, Lcom/android/settings/DeviceAdminAddFragment;->bHD:Landroid/app/admin/DeviceAdminInfo;

    invoke-virtual {v1}, Landroid/app/admin/DeviceAdminInfo;->getComponent()Landroid/content/ComponentName;

    move-result-object v1

    new-instance v2, Landroid/os/RemoteCallback;

    new-instance v3, Lcom/android/settings/gz;

    invoke-direct {v3, p0}, Lcom/android/settings/gz;-><init>(Lcom/android/settings/gx;)V

    iget-object v4, p0, Lcom/android/settings/gx;->clU:Lcom/android/settings/DeviceAdminAddFragment;

    iget-object v4, v4, Lcom/android/settings/DeviceAdminAddFragment;->bHF:Landroid/os/Handler;

    invoke-direct {v2, v3, v4}, Landroid/os/RemoteCallback;-><init>(Landroid/os/RemoteCallback$OnResultListener;Landroid/os/Handler;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/admin/DevicePolicyManager;->getRemoveWarning(Landroid/content/ComponentName;Landroid/os/RemoteCallback;)V

    goto/16 :goto_1

    :catch_1
    move-exception v0

    goto :goto_2
.end method
