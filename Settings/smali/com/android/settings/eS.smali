.class final Lcom/android/settings/eS;
.super Ljava/lang/Object;
.source "ImportanceListPreference.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field final synthetic cjG:Lcom/android/settings/ImportanceListPreference;


# direct methods
.method constructor <init>(Lcom/android/settings/ImportanceListPreference;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/eS;->cjG:Lcom/android/settings/ImportanceListPreference;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 2

    if-ltz p2, :cond_0

    iget-object v0, p0, Lcom/android/settings/eS;->cjG:Lcom/android/settings/ImportanceListPreference;

    invoke-static {v0}, Lcom/android/settings/ImportanceListPreference;->boW(Lcom/android/settings/ImportanceListPreference;)[Ljava/lang/String;

    move-result-object v0

    array-length v0, v0

    if-lt p2, v0, :cond_1

    :cond_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/settings/eS;->cjG:Lcom/android/settings/ImportanceListPreference;

    invoke-virtual {v0, p2}, Lcom/android/settings/ImportanceListPreference;->setValueIndex(I)V

    iget-object v0, p0, Lcom/android/settings/eS;->cjG:Lcom/android/settings/ImportanceListPreference;

    invoke-static {v0, p2}, Lcom/android/settings/ImportanceListPreference;->boY(Lcom/android/settings/ImportanceListPreference;I)I

    iget-object v0, p0, Lcom/android/settings/eS;->cjG:Lcom/android/settings/ImportanceListPreference;

    invoke-virtual {v0}, Lcom/android/settings/ImportanceListPreference;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    iget-object v0, p0, Lcom/android/settings/eS;->cjG:Lcom/android/settings/ImportanceListPreference;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/settings/ImportanceListPreference;->boZ(Lcom/android/settings/ImportanceListPreference;Ljava/lang/Object;)Z

    return-void
.end method
