.class final Lcom/android/settings/lo;
.super Ljava/lang/Object;
.source "SetUpChooseLockPassword.java"

# interfaces
.implements Lmiui/view/MiuiKeyBoardView$OnKeyboardActionListener;


# instance fields
.field final synthetic csj:Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;


# direct methods
.method constructor <init>(Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/lo;->csj:Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onKeyBoardDelete()V
    .locals 3

    iget-object v0, p0, Lcom/android/settings/lo;->csj:Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;

    invoke-static {v0}, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->bZo(Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/TextView;->getEditableText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-interface {v0}, Landroid/text/Editable;->length()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-interface {v0}, Landroid/text/Editable;->length()I

    move-result v2

    invoke-interface {v0, v1, v2}, Landroid/text/Editable;->delete(II)Landroid/text/Editable;

    :cond_0
    return-void
.end method

.method public onKeyBoardOK()V
    .locals 1

    iget-object v0, p0, Lcom/android/settings/lo;->csj:Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;

    invoke-virtual {v0}, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->bZd()V

    return-void
.end method

.method public onText(Ljava/lang/CharSequence;)V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/lo;->csj:Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;

    invoke-static {v0}, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->bZl(Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/lo;->csj:Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;

    invoke-static {v0}, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->bZo(Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;)Landroid/widget/TextView;

    move-result-object v0

    const-string/jumbo v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/android/settings/lo;->csj:Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->bZt(Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;Z)Z

    :cond_0
    iget-object v0, p0, Lcom/android/settings/lo;->csj:Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;

    invoke-static {v0}, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->bZo(Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->append(Ljava/lang/CharSequence;)V

    return-void
.end method
