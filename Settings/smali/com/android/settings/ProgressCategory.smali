.class public Lcom/android/settings/ProgressCategory;
.super Lcom/android/settings/ProgressCategoryBase;
.source "ProgressCategory.java"


# instance fields
.field private ccr:I

.field private ccs:Z

.field private cct:Landroid/support/v7/preference/Preference;

.field private ccu:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    invoke-direct {p0, p1, p2}, Lcom/android/settings/ProgressCategoryBase;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/settings/ProgressCategory;->ccu:Z

    const v0, 0x7f0d0147

    invoke-virtual {p0, v0}, Lcom/android/settings/ProgressCategory;->setLayoutResource(I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    invoke-direct {p0, p1, p2, p3}, Lcom/android/settings/ProgressCategoryBase;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/settings/ProgressCategory;->ccu:Z

    const v0, 0x7f0d0147

    invoke-virtual {p0, v0}, Lcom/android/settings/ProgressCategory;->setLayoutResource(I)V

    return-void
.end method


# virtual methods
.method public al(Landroid/support/v7/preference/l;)V
    .locals 5

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-super {p0, p1}, Lcom/android/settings/ProgressCategoryBase;->al(Landroid/support/v7/preference/l;)V

    const v0, 0x7f0a03a8

    invoke-virtual {p1, v0}, Landroid/support/v7/preference/l;->dma(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {p0}, Lcom/android/settings/ProgressCategory;->dln()I

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lcom/android/settings/ProgressCategory;->dln()I

    move-result v0

    if-ne v0, v1, :cond_3

    invoke-virtual {p0, v2}, Lcom/android/settings/ProgressCategory;->dlf(I)Landroid/support/v7/preference/Preference;

    move-result-object v0

    iget-object v3, p0, Lcom/android/settings/ProgressCategory;->cct:Landroid/support/v7/preference/Preference;

    if-ne v0, v3, :cond_3

    move v0, v1

    :goto_0
    iget-boolean v3, p0, Lcom/android/settings/ProgressCategory;->ccu:Z

    if-eqz v3, :cond_4

    move v3, v2

    :goto_1
    invoke-virtual {v4, v3}, Landroid/view/View;->setVisibility(I)V

    iget-boolean v3, p0, Lcom/android/settings/ProgressCategory;->ccu:Z

    if-nez v3, :cond_0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_5

    :cond_0
    iget-boolean v0, p0, Lcom/android/settings/ProgressCategory;->ccs:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/settings/ProgressCategory;->cct:Landroid/support/v7/preference/Preference;

    invoke-virtual {p0, v0}, Lcom/android/settings/ProgressCategory;->dll(Landroid/support/v7/preference/Preference;)Z

    iput-boolean v2, p0, Lcom/android/settings/ProgressCategory;->ccs:Z

    :cond_1
    :goto_2
    return-void

    :cond_2
    move v0, v1

    goto :goto_0

    :cond_3
    move v0, v2

    goto :goto_0

    :cond_4
    const/16 v3, 0x8

    goto :goto_1

    :cond_5
    iget-boolean v0, p0, Lcom/android/settings/ProgressCategory;->ccs:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/android/settings/ProgressCategory;->cct:Landroid/support/v7/preference/Preference;

    if-nez v0, :cond_6

    new-instance v0, Landroid/support/v7/preference/Preference;

    invoke-virtual {p0}, Lcom/android/settings/ProgressCategory;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v0, v3}, Landroid/support/v7/preference/Preference;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/settings/ProgressCategory;->cct:Landroid/support/v7/preference/Preference;

    iget-object v0, p0, Lcom/android/settings/ProgressCategory;->cct:Landroid/support/v7/preference/Preference;

    const v3, 0x7f0d0136

    invoke-virtual {v0, v3}, Landroid/support/v7/preference/Preference;->setLayoutResource(I)V

    iget-object v0, p0, Lcom/android/settings/ProgressCategory;->cct:Landroid/support/v7/preference/Preference;

    iget v3, p0, Lcom/android/settings/ProgressCategory;->ccr:I

    invoke-virtual {v0, v3}, Landroid/support/v7/preference/Preference;->setTitle(I)V

    iget-object v0, p0, Lcom/android/settings/ProgressCategory;->cct:Landroid/support/v7/preference/Preference;

    invoke-virtual {v0, v2}, Landroid/support/v7/preference/Preference;->boV(Z)V

    :cond_6
    iget-object v0, p0, Lcom/android/settings/ProgressCategory;->cct:Landroid/support/v7/preference/Preference;

    invoke-virtual {p0, v0}, Lcom/android/settings/ProgressCategory;->im(Landroid/support/v7/preference/Preference;)Z

    iput-boolean v1, p0, Lcom/android/settings/ProgressCategory;->ccs:Z

    goto :goto_2
.end method
