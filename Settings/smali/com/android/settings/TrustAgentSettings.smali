.class public Lcom/android/settings/TrustAgentSettings;
.super Lcom/android/settings/MiuiSettingsPreferenceFragment;
.source "TrustAgentSettings.java"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceChangeListener;


# instance fields
.field private final bFM:Landroid/util/ArraySet;

.field private bFN:Landroid/util/ArrayMap;

.field private bFO:Landroid/app/admin/DevicePolicyManager;

.field private bFP:Lcom/android/settings/d/b;

.field private mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/android/settings/MiuiSettingsPreferenceFragment;-><init>()V

    new-instance v0, Landroid/util/ArraySet;

    invoke-direct {v0}, Landroid/util/ArraySet;-><init>()V

    iput-object v0, p0, Lcom/android/settings/TrustAgentSettings;->bFM:Landroid/util/ArraySet;

    return-void
.end method

.method private bvN()V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/TrustAgentSettings;->mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/android/internal/widget/LockPatternUtils;->getEnabledTrustAgents(I)Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/android/settings/TrustAgentSettings;->bFM:Landroid/util/ArraySet;

    invoke-virtual {v1, v0}, Landroid/util/ArraySet;->addAll(Ljava/util/Collection;)Z

    :cond_0
    return-void
.end method

.method private bvO()V
    .locals 3

    iget-object v0, p0, Lcom/android/settings/TrustAgentSettings;->mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

    iget-object v1, p0, Lcom/android/settings/TrustAgentSettings;->bFM:Landroid/util/ArraySet;

    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/android/internal/widget/LockPatternUtils;->setEnabledTrustAgents(Ljava/util/Collection;I)V

    return-void
.end method

.method private bvP()V
    .locals 10

    const/4 v3, 0x0

    const/4 v9, 0x0

    invoke-virtual {p0}, Lcom/android/settings/TrustAgentSettings;->getActivity()Landroid/app/Activity;

    move-result-object v1

    iget-object v0, p0, Lcom/android/settings/TrustAgentSettings;->bFN:Landroid/util/ArrayMap;

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/android/settings/TrustAgentSettings;->bvM()Landroid/util/ArrayMap;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/TrustAgentSettings;->bFN:Landroid/util/ArrayMap;

    :cond_0
    iget-object v0, p0, Lcom/android/settings/TrustAgentSettings;->mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

    if-nez v0, :cond_1

    new-instance v0, Lcom/android/internal/widget/LockPatternUtils;

    invoke-virtual {p0}, Lcom/android/settings/TrustAgentSettings;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-direct {v0, v2}, Lcom/android/internal/widget/LockPatternUtils;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/settings/TrustAgentSettings;->mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

    :cond_1
    invoke-direct {p0}, Lcom/android/settings/TrustAgentSettings;->bvN()V

    invoke-virtual {p0}, Lcom/android/settings/TrustAgentSettings;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    const-string/jumbo v2, "trust_agents"

    invoke-virtual {v0, v2}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceGroup;

    invoke-virtual {v0}, Landroid/preference/PreferenceGroup;->removeAll()V

    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v2

    const/16 v4, 0x10

    invoke-static {v1, v4, v2}, Lcom/android/settingslib/w;->cqR(Landroid/content/Context;II)Lcom/android/settingslib/n;

    move-result-object v4

    iget-object v1, p0, Lcom/android/settings/TrustAgentSettings;->bFN:Landroid/util/ArrayMap;

    invoke-virtual {v1}, Landroid/util/ArrayMap;->size()I

    move-result v5

    move v2, v3

    :goto_0
    if-ge v2, v5, :cond_3

    iget-object v1, p0, Lcom/android/settings/TrustAgentSettings;->bFN:Landroid/util/ArrayMap;

    invoke-virtual {v1, v2}, Landroid/util/ArrayMap;->valueAt(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/settings/aJ;

    new-instance v6, Lcom/android/settingslib/MiuiRestrictedSwitchPreference;

    invoke-virtual {p0}, Lcom/android/settings/TrustAgentSettings;->bWz()Landroid/content/Context;

    move-result-object v7

    invoke-direct {v6, v7}, Lcom/android/settingslib/MiuiRestrictedSwitchPreference;-><init>(Landroid/content/Context;)V

    const/4 v7, 0x1

    invoke-virtual {v6, v7}, Lcom/android/settingslib/MiuiRestrictedSwitchPreference;->cqv(Z)V

    iput-object v6, v1, Lcom/android/settings/aJ;->bFS:Landroid/preference/SwitchPreference;

    invoke-virtual {v6, v3}, Lcom/android/settingslib/MiuiRestrictedSwitchPreference;->setPersistent(Z)V

    iget-object v7, v1, Lcom/android/settings/aJ;->bFR:Ljava/lang/CharSequence;

    invoke-virtual {v6, v7}, Lcom/android/settingslib/MiuiRestrictedSwitchPreference;->setTitle(Ljava/lang/CharSequence;)V

    iget-object v7, v1, Lcom/android/settings/aJ;->icon:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v6, v7}, Lcom/android/settingslib/MiuiRestrictedSwitchPreference;->setIcon(Landroid/graphics/drawable/Drawable;)V

    invoke-virtual {v6, v3}, Lcom/android/settingslib/MiuiRestrictedSwitchPreference;->setPersistent(Z)V

    invoke-virtual {v6, p0}, Lcom/android/settingslib/MiuiRestrictedSwitchPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    iget-object v7, p0, Lcom/android/settings/TrustAgentSettings;->bFM:Landroid/util/ArraySet;

    iget-object v8, v1, Lcom/android/settings/aJ;->bFQ:Landroid/content/ComponentName;

    invoke-virtual {v7, v8}, Landroid/util/ArraySet;->contains(Ljava/lang/Object;)Z

    move-result v7

    invoke-virtual {v6, v7}, Lcom/android/settingslib/MiuiRestrictedSwitchPreference;->setChecked(Z)V

    if-eqz v4, :cond_2

    iget-object v7, p0, Lcom/android/settings/TrustAgentSettings;->bFO:Landroid/app/admin/DevicePolicyManager;

    iget-object v8, v1, Lcom/android/settings/aJ;->bFQ:Landroid/content/ComponentName;

    invoke-virtual {v7, v9, v8}, Landroid/app/admin/DevicePolicyManager;->getTrustAgentConfiguration(Landroid/content/ComponentName;Landroid/content/ComponentName;)Ljava/util/List;

    move-result-object v7

    if-nez v7, :cond_2

    invoke-virtual {v6, v3}, Lcom/android/settingslib/MiuiRestrictedSwitchPreference;->setChecked(Z)V

    invoke-virtual {v6, v4}, Lcom/android/settingslib/MiuiRestrictedSwitchPreference;->setDisabledByAdmin(Lcom/android/settingslib/n;)V

    :cond_2
    iget-object v1, v1, Lcom/android/settings/aJ;->bFS:Landroid/preference/SwitchPreference;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceGroup;->addPreference(Landroid/preference/Preference;)Z

    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_0

    :cond_3
    return-void
.end method


# virtual methods
.method bvM()Landroid/util/ArrayMap;
    .locals 9

    invoke-virtual {p0}, Lcom/android/settings/TrustAgentSettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v1, "android.service.trust.TrustAgentService"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const/16 v1, 0x80

    invoke-virtual {v2, v0, v1}, Landroid/content/pm/PackageManager;->queryIntentServices(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v3

    new-instance v4, Landroid/util/ArrayMap;

    invoke-direct {v4}, Landroid/util/ArrayMap;-><init>()V

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v5

    invoke-virtual {v4, v5}, Landroid/util/ArrayMap;->ensureCapacity(I)V

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v5, :cond_2

    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/ResolveInfo;

    iget-object v6, v0, Landroid/content/pm/ResolveInfo;->serviceInfo:Landroid/content/pm/ServiceInfo;

    if-nez v6, :cond_1

    :cond_0
    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    iget-object v6, p0, Lcom/android/settings/TrustAgentSettings;->bFP:Lcom/android/settings/d/b;

    invoke-interface {v6, v0, v2}, Lcom/android/settings/d/b;->aYO(Landroid/content/pm/ResolveInfo;Landroid/content/pm/PackageManager;)Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-static {v0}, Lcom/android/settings/ct;->bRW(Landroid/content/pm/ResolveInfo;)Landroid/content/ComponentName;

    move-result-object v6

    new-instance v7, Lcom/android/settings/aJ;

    invoke-direct {v7}, Lcom/android/settings/aJ;-><init>()V

    invoke-virtual {v0, v2}, Landroid/content/pm/ResolveInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v8

    iput-object v8, v7, Lcom/android/settings/aJ;->bFR:Ljava/lang/CharSequence;

    invoke-virtual {v0, v2}, Landroid/content/pm/ResolveInfo;->loadIcon(Landroid/content/pm/PackageManager;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, v7, Lcom/android/settings/aJ;->icon:Landroid/graphics/drawable/Drawable;

    iput-object v6, v7, Lcom/android/settings/aJ;->bFQ:Landroid/content/ComponentName;

    invoke-virtual {v4, v6, v7}, Landroid/util/ArrayMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    :cond_2
    return-object v4
.end method

.method public getMetricsCategory()I
    .locals 1

    const/16 v0, 0x5b

    return v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/android/settings/TrustAgentSettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const-class v1, Landroid/app/admin/DevicePolicyManager;

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/admin/DevicePolicyManager;

    iput-object v0, p0, Lcom/android/settings/TrustAgentSettings;->bFO:Landroid/app/admin/DevicePolicyManager;

    invoke-virtual {p0}, Lcom/android/settings/TrustAgentSettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/android/settings/overlay/a;->aIk(Landroid/content/Context;)Lcom/android/settings/overlay/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/settings/overlay/a;->aIu()Lcom/android/settings/security/d;

    move-result-object v0

    invoke-interface {v0}, Lcom/android/settings/security/d;->aTZ()Lcom/android/settings/d/b;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/TrustAgentSettings;->bFP:Lcom/android/settings/d/b;

    const v0, 0x7f1500f9

    invoke-virtual {p0, v0}, Lcom/android/settings/TrustAgentSettings;->addPreferencesFromResource(I)V

    return-void
.end method

.method public onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 5

    const/4 v2, 0x0

    instance-of v0, p1, Landroid/preference/SwitchPreference;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/android/settings/TrustAgentSettings;->bFN:Landroid/util/ArrayMap;

    invoke-virtual {v0}, Landroid/util/ArrayMap;->size()I

    move-result v3

    move v1, v2

    :goto_0
    if-ge v1, v3, :cond_3

    iget-object v0, p0, Lcom/android/settings/TrustAgentSettings;->bFN:Landroid/util/ArrayMap;

    invoke-virtual {v0, v1}, Landroid/util/ArrayMap;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settings/aJ;

    iget-object v4, v0, Lcom/android/settings/aJ;->bFS:Landroid/preference/SwitchPreference;

    if-ne v4, p1, :cond_2

    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/android/settings/TrustAgentSettings;->bFM:Landroid/util/ArraySet;

    iget-object v2, v0, Lcom/android/settings/aJ;->bFQ:Landroid/content/ComponentName;

    invoke-virtual {v1, v2}, Landroid/util/ArraySet;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/android/settings/TrustAgentSettings;->bFM:Landroid/util/ArraySet;

    iget-object v0, v0, Lcom/android/settings/aJ;->bFQ:Landroid/content/ComponentName;

    invoke-virtual {v1, v0}, Landroid/util/ArraySet;->add(Ljava/lang/Object;)Z

    :cond_0
    :goto_1
    invoke-direct {p0}, Lcom/android/settings/TrustAgentSettings;->bvO()V

    const/4 v0, 0x1

    return v0

    :cond_1
    iget-object v1, p0, Lcom/android/settings/TrustAgentSettings;->bFM:Landroid/util/ArraySet;

    iget-object v0, v0, Lcom/android/settings/aJ;->bFQ:Landroid/content/ComponentName;

    invoke-virtual {v1, v0}, Landroid/util/ArraySet;->remove(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_3
    return v2
.end method

.method public onResume()V
    .locals 1

    invoke-super {p0}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->onResume()V

    const-string/jumbo v0, "dummy_preference"

    invoke-virtual {p0, v0}, Lcom/android/settings/TrustAgentSettings;->bWK(Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/android/settings/TrustAgentSettings;->bvP()V

    return-void
.end method
