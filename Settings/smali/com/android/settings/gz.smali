.class final Lcom/android/settings/gz;
.super Ljava/lang/Object;
.source "DeviceAdminAddFragment.java"

# interfaces
.implements Landroid/os/RemoteCallback$OnResultListener;


# instance fields
.field final synthetic clX:Lcom/android/settings/gx;


# direct methods
.method constructor <init>(Lcom/android/settings/gx;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/gz;->clX:Lcom/android/settings/gx;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onResult(Landroid/os/Bundle;)V
    .locals 2

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/android/settings/gz;->clX:Lcom/android/settings/gx;

    iget-object v1, v1, Lcom/android/settings/gx;->clU:Lcom/android/settings/DeviceAdminAddFragment;

    if-eqz p1, :cond_0

    const-string/jumbo v0, "android.app.extra.DISABLE_WARNING"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getCharSequence(Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v0

    :cond_0
    invoke-static {v1, v0}, Lcom/android/settings/DeviceAdminAddFragment;->byi(Lcom/android/settings/DeviceAdminAddFragment;Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    iget-object v0, p0, Lcom/android/settings/gz;->clX:Lcom/android/settings/gx;

    iget-object v0, v0, Lcom/android/settings/gx;->clU:Lcom/android/settings/DeviceAdminAddFragment;

    invoke-static {v0}, Lcom/android/settings/DeviceAdminAddFragment;->byh(Lcom/android/settings/DeviceAdminAddFragment;)Ljava/lang/CharSequence;

    move-result-object v0

    if-nez v0, :cond_1

    :try_start_0
    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    move-result-object v0

    invoke-interface {v0}, Landroid/app/IActivityManager;->resumeAppSwitches()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    iget-object v0, p0, Lcom/android/settings/gz;->clX:Lcom/android/settings/gx;

    iget-object v0, v0, Lcom/android/settings/gx;->clU:Lcom/android/settings/DeviceAdminAddFragment;

    iget-object v0, v0, Lcom/android/settings/DeviceAdminAddFragment;->bHC:Landroid/app/admin/DevicePolicyManager;

    iget-object v1, p0, Lcom/android/settings/gz;->clX:Lcom/android/settings/gx;

    iget-object v1, v1, Lcom/android/settings/gx;->clU:Lcom/android/settings/DeviceAdminAddFragment;

    iget-object v1, v1, Lcom/android/settings/DeviceAdminAddFragment;->bHD:Landroid/app/admin/DeviceAdminInfo;

    invoke-virtual {v1}, Landroid/app/admin/DeviceAdminInfo;->getComponent()Landroid/content/ComponentName;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/admin/DevicePolicyManager;->removeActiveAdmin(Landroid/content/ComponentName;)V

    iget-object v0, p0, Lcom/android/settings/gz;->clX:Lcom/android/settings/gx;

    iget-object v0, v0, Lcom/android/settings/gx;->clU:Lcom/android/settings/DeviceAdminAddFragment;

    invoke-virtual {v0}, Lcom/android/settings/DeviceAdminAddFragment;->finish()V

    :goto_1
    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/settings/gz;->clX:Lcom/android/settings/gx;

    iget-object v0, v0, Lcom/android/settings/gx;->clU:Lcom/android/settings/DeviceAdminAddFragment;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/android/settings/DeviceAdminAddFragment;->bLV(I)V

    goto :goto_1

    :catch_0
    move-exception v0

    goto :goto_0
.end method
