.class final Lcom/android/settings/hj;
.super Ljava/lang/Object;
.source "MutedVideoView.java"

# interfaces
.implements Landroid/view/SurfaceHolder$Callback;


# instance fields
.field final synthetic cmL:Lcom/android/settings/MutedVideoView;


# direct methods
.method constructor <init>(Lcom/android/settings/MutedVideoView;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/hj;->cmL:Lcom/android/settings/MutedVideoView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public surfaceChanged(Landroid/view/SurfaceHolder;III)V
    .locals 4

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/android/settings/hj;->cmL:Lcom/android/settings/MutedVideoView;

    invoke-static {v0, p3}, Lcom/android/settings/MutedVideoView;->bAP(Lcom/android/settings/MutedVideoView;I)I

    iget-object v0, p0, Lcom/android/settings/hj;->cmL:Lcom/android/settings/MutedVideoView;

    invoke-static {v0, p4}, Lcom/android/settings/MutedVideoView;->bAN(Lcom/android/settings/MutedVideoView;I)I

    iget-object v0, p0, Lcom/android/settings/hj;->cmL:Lcom/android/settings/MutedVideoView;

    invoke-static {v0}, Lcom/android/settings/MutedVideoView;->bAF(Lcom/android/settings/MutedVideoView;)I

    move-result v0

    const/4 v3, 0x3

    if-ne v0, v3, :cond_3

    move v0, v1

    :goto_0
    iget-object v3, p0, Lcom/android/settings/hj;->cmL:Lcom/android/settings/MutedVideoView;

    invoke-static {v3}, Lcom/android/settings/MutedVideoView;->bAH(Lcom/android/settings/MutedVideoView;)I

    move-result v3

    if-ne v3, p3, :cond_0

    iget-object v3, p0, Lcom/android/settings/hj;->cmL:Lcom/android/settings/MutedVideoView;

    invoke-static {v3}, Lcom/android/settings/MutedVideoView;->bAG(Lcom/android/settings/MutedVideoView;)I

    move-result v3

    if-ne v3, p4, :cond_0

    move v2, v1

    :cond_0
    iget-object v1, p0, Lcom/android/settings/hj;->cmL:Lcom/android/settings/MutedVideoView;

    invoke-static {v1}, Lcom/android/settings/MutedVideoView;->bAx(Lcom/android/settings/MutedVideoView;)Landroid/media/MediaPlayer;

    move-result-object v1

    if-eqz v1, :cond_2

    if-eqz v0, :cond_2

    if-eqz v2, :cond_2

    iget-object v0, p0, Lcom/android/settings/hj;->cmL:Lcom/android/settings/MutedVideoView;

    invoke-static {v0}, Lcom/android/settings/MutedVideoView;->bAC(Lcom/android/settings/MutedVideoView;)I

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/settings/hj;->cmL:Lcom/android/settings/MutedVideoView;

    iget-object v1, p0, Lcom/android/settings/hj;->cmL:Lcom/android/settings/MutedVideoView;

    invoke-static {v1}, Lcom/android/settings/MutedVideoView;->bAC(Lcom/android/settings/MutedVideoView;)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/android/settings/MutedVideoView;->seekTo(I)V

    :cond_1
    iget-object v0, p0, Lcom/android/settings/hj;->cmL:Lcom/android/settings/MutedVideoView;

    invoke-virtual {v0}, Lcom/android/settings/MutedVideoView;->start()V

    :cond_2
    return-void

    :cond_3
    move v0, v2

    goto :goto_0
.end method

.method public surfaceCreated(Landroid/view/SurfaceHolder;)V
    .locals 1

    iget-object v0, p0, Lcom/android/settings/hj;->cmL:Lcom/android/settings/MutedVideoView;

    invoke-static {v0, p1}, Lcom/android/settings/MutedVideoView;->bAO(Lcom/android/settings/MutedVideoView;Landroid/view/SurfaceHolder;)Landroid/view/SurfaceHolder;

    iget-object v0, p0, Lcom/android/settings/hj;->cmL:Lcom/android/settings/MutedVideoView;

    invoke-static {v0}, Lcom/android/settings/MutedVideoView;->bAT(Lcom/android/settings/MutedVideoView;)V

    return-void
.end method

.method public surfaceDestroyed(Landroid/view/SurfaceHolder;)V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/android/settings/hj;->cmL:Lcom/android/settings/MutedVideoView;

    invoke-static {v0, v1}, Lcom/android/settings/MutedVideoView;->bAO(Lcom/android/settings/MutedVideoView;Landroid/view/SurfaceHolder;)Landroid/view/SurfaceHolder;

    iget-object v0, p0, Lcom/android/settings/hj;->cmL:Lcom/android/settings/MutedVideoView;

    invoke-static {v0}, Lcom/android/settings/MutedVideoView;->bAw(Lcom/android/settings/MutedVideoView;)Landroid/widget/MediaController;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/hj;->cmL:Lcom/android/settings/MutedVideoView;

    invoke-static {v0}, Lcom/android/settings/MutedVideoView;->bAw(Lcom/android/settings/MutedVideoView;)Landroid/widget/MediaController;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/MediaController;->hide()V

    :cond_0
    iget-object v0, p0, Lcom/android/settings/hj;->cmL:Lcom/android/settings/MutedVideoView;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/android/settings/MutedVideoView;->bAU(Lcom/android/settings/MutedVideoView;Z)V

    return-void
.end method
