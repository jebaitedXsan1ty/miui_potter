.class public Lcom/android/settings/i/a;
.super Ljava/lang/Object;
.source "MiHomeManager.java"


# static fields
.field private static bqA:Lcom/android/settings/i/a;

.field private static final bqB:Ljava/lang/Object;

.field private static final bqu:Landroid/net/Uri;

.field private static final bqv:Landroid/net/Uri;


# instance fields
.field public bqt:Z

.field private volatile bqw:Z

.field private bqx:Ljava/util/HashSet;

.field private bqy:Landroid/content/pm/PackageManager;

.field private bqz:Ljava/util/HashSet;

.field private mContext:Landroid/content/Context;

.field private mPackageInfo:Landroid/content/pm/PackageInfo;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string/jumbo v0, "content://com.xiaomi.mihomemanager.whitelistProvider/packageName"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/android/settings/i/a;->bqv:Landroid/net/Uri;

    const-string/jumbo v0, "content://com.xiaomi.mihomemanager.settingsProvider/settings"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/android/settings/i/a;->bqu:Landroid/net/Uri;

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/android/settings/i/a;->bqB:Ljava/lang/Object;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 3

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/settings/i/a;->bqt:Z

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/i/a;->mContext:Landroid/content/Context;

    iget-object v0, p0, Lcom/android/settings/i/a;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/i/a;->bqy:Landroid/content/pm/PackageManager;

    :try_start_0
    iget-object v0, p0, Lcom/android/settings/i/a;->bqy:Landroid/content/pm/PackageManager;

    const-string/jumbo v1, "com.xiaomi.mihomemanager"

    const/16 v2, 0x2240

    invoke-virtual {v0, v1, v2}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/i/a;->mPackageInfo:Landroid/content/pm/PackageInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/android/settings/i/a;->bqt:Z

    const-string/jumbo v1, "MiHomeManager"

    const-string/jumbo v2, "Exception when retrieving package:com.xiaomi.mihomemanager"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method private bdP()V
    .locals 6

    const/4 v2, 0x0

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/android/settings/i/a;->bqz:Ljava/util/HashSet;

    invoke-direct {p0}, Lcom/android/settings/i/a;->bdS()Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/settings/i/a;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/android/settings/i/a;->bqu:Landroid/net/Uri;

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    if-nez v0, :cond_1

    return-void

    :cond_1
    :goto_0
    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/android/settings/i/a;->bqz:Ljava/util/HashSet;

    const/4 v2, 0x1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_2
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    return-void
.end method

.method private bdQ()V
    .locals 6

    const/4 v2, 0x0

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/android/settings/i/a;->bqx:Ljava/util/HashSet;

    invoke-direct {p0}, Lcom/android/settings/i/a;->bdS()Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/settings/i/a;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/android/settings/i/a;->bqv:Landroid/net/Uri;

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    if-nez v0, :cond_1

    return-void

    :cond_1
    :goto_0
    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/android/settings/i/a;->bqx:Ljava/util/HashSet;

    const/4 v2, 0x1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_2
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    return-void
.end method

.method private bdS()Z
    .locals 4

    const/4 v0, 0x0

    :try_start_0
    iget-object v1, p0, Lcom/android/settings/i/a;->bqy:Landroid/content/pm/PackageManager;

    const-string/jumbo v2, "android"

    const/16 v3, 0x40

    invoke-virtual {v1, v2, v3}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v1

    iget-object v2, p0, Lcom/android/settings/i/a;->mPackageInfo:Landroid/content/pm/PackageInfo;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/android/settings/i/a;->mPackageInfo:Landroid/content/pm/PackageInfo;

    iget-object v2, v2, Landroid/content/pm/PackageInfo;->signatures:[Landroid/content/pm/Signature;

    if-eqz v2, :cond_0

    iget-object v1, v1, Landroid/content/pm/PackageInfo;->signatures:[Landroid/content/pm/Signature;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    iget-object v2, p0, Lcom/android/settings/i/a;->mPackageInfo:Landroid/content/pm/PackageInfo;

    iget-object v2, v2, Landroid/content/pm/PackageInfo;->signatures:[Landroid/content/pm/Signature;

    const/4 v3, 0x0

    aget-object v2, v2, v3

    invoke-virtual {v1, v2}, Landroid/content/pm/Signature;->equals(Ljava/lang/Object;)Z
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    :cond_0
    return v0

    :catch_0
    move-exception v1

    const-string/jumbo v2, "MiHomeManager"

    const-string/jumbo v3, "Exception when getting system signature"

    invoke-static {v2, v3, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    return v0
.end method

.method static synthetic bdT(Lcom/android/settings/i/a;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/android/settings/i/a;->bqw:Z

    return p1
.end method

.method static synthetic bdU(Lcom/android/settings/i/a;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/i/a;->bdP()V

    return-void
.end method

.method public static getInstance(Landroid/content/Context;)Lcom/android/settings/i/a;
    .locals 2

    sget-object v1, Lcom/android/settings/i/a;->bqB:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/android/settings/i/a;->bqA:Lcom/android/settings/i/a;

    if-nez v0, :cond_0

    new-instance v0, Lcom/android/settings/i/a;

    invoke-direct {v0, p0}, Lcom/android/settings/i/a;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/android/settings/i/a;->bqA:Lcom/android/settings/i/a;

    :cond_0
    sget-object v0, Lcom/android/settings/i/a;->bqA:Lcom/android/settings/i/a;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public bdN(Ljava/lang/String;)Z
    .locals 1

    iget-object v0, p0, Lcom/android/settings/i/a;->bqx:Ljava/util/HashSet;

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/android/settings/i/a;->bdQ()V

    :cond_0
    iget-object v0, p0, Lcom/android/settings/i/a;->bqx:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public bdO(Ljava/lang/String;)Z
    .locals 1

    iget-object v0, p0, Lcom/android/settings/i/a;->bqz:Ljava/util/HashSet;

    invoke-virtual {p0, v0}, Lcom/android/settings/i/a;->bdR(Ljava/util/HashSet;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/android/settings/i/a;->bqw:Z

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/settings/i/a;->bqw:Z

    new-instance v0, Lcom/android/settings/i/b;

    invoke-direct {v0, p0}, Lcom/android/settings/i/b;-><init>(Lcom/android/settings/i/a;)V

    invoke-virtual {v0}, Lcom/android/settings/i/b;->start()V

    :cond_0
    iget-object v0, p0, Lcom/android/settings/i/a;->bqz:Ljava/util/HashSet;

    invoke-virtual {p0, v0}, Lcom/android/settings/i/a;->bdR(Ljava/util/HashSet;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_1
    iget-object v0, p0, Lcom/android/settings/i/a;->bqz:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public bdR(Ljava/util/HashSet;)Z
    .locals 3

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/util/HashSet;->size()I

    move-result v2

    if-nez v2, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    move v0, v1

    goto :goto_0
.end method
