.class public Lcom/android/settings/NotificationAppListSettings;
.super Lcom/android/settings/BaseFragment;
.source "NotificationAppListSettings.java"


# instance fields
.field private bBA:Landroid/widget/ListView;

.field private bBB:Landroid/content/BroadcastReceiver;

.field private bBC:Lcom/android/settings/r;

.field bBv:Ljava/util/List;

.field bBw:Ljava/util/List;

.field bBx:Lcom/android/settings/al;

.field private bBy:Lcom/android/settings/cN;

.field private bBz:Landroid/view/LayoutInflater;

.field mContext:Landroid/content/Context;

.field mPackageManager:Landroid/content/pm/PackageManager;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/android/settings/BaseFragment;-><init>()V

    new-instance v0, Lcom/android/settings/eW;

    invoke-direct {v0, p0}, Lcom/android/settings/eW;-><init>(Lcom/android/settings/NotificationAppListSettings;)V

    iput-object v0, p0, Lcom/android/settings/NotificationAppListSettings;->bBB:Landroid/content/BroadcastReceiver;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/settings/NotificationAppListSettings;->bBv:Ljava/util/List;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/settings/NotificationAppListSettings;->bBw:Ljava/util/List;

    return-void
.end method

.method private static bpr(Landroid/content/Context;Ljava/lang/String;I)Ljava/lang/String;
    .locals 5

    const v4, 0x7f121110

    const/4 v1, 0x1

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    return-object v0

    :cond_0
    const/4 v0, 0x3

    if-ne p2, v0, :cond_1

    const v0, 0x7f121111

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_1
    invoke-static {p0}, Lcom/android/settings/g;->bfH(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-static {p0, p1}, Lmiui/util/NotificationFilterHelper;->getImportance(Landroid/content/Context;Ljava/lang/String;)I

    move-result v0

    const/4 v2, -0x1

    if-ne v2, v0, :cond_2

    const v0, 0x7f1212da

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_2
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v0, 0x0

    const-string/jumbo v3, "_message"

    invoke-static {p0, p1, v3}, Lmiui/util/NotificationFilterHelper;->isAllowed(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3

    const v0, 0x7f121114

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move v0, v1

    :cond_3
    const/4 v3, 0x2

    if-ne p2, v3, :cond_4

    if-eqz v0, :cond_7

    invoke-virtual {p0, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_0
    const v1, 0x7f121112

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_4
    const-string/jumbo v1, "_keyguard"

    invoke-static {p0, p1, v1}, Lmiui/util/NotificationFilterHelper;->isAllowed(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_6

    if-eqz v0, :cond_5

    invoke-virtual {p0, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_5
    const v0, 0x7f121113

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_6
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_7
    move v0, v1

    goto :goto_0
.end method

.method private bps(Landroid/os/Bundle;Ljava/lang/String;Landroid/app/Fragment;)V
    .locals 1

    invoke-virtual {p0}, Lcom/android/settings/NotificationAppListSettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0, p0, p1, p2, p3}, Lcom/android/settings/bk;->bEP(Landroid/app/Activity;Landroid/app/Fragment;Landroid/os/Bundle;Ljava/lang/String;Landroid/app/Fragment;)V

    return-void
.end method

.method private bpt()V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/android/settings/NotificationAppListSettings;->bBC:Lcom/android/settings/r;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/NotificationAppListSettings;->bBC:Lcom/android/settings/r;

    invoke-virtual {v0}, Lcom/android/settings/r;->isCancelled()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/NotificationAppListSettings;->bBC:Lcom/android/settings/r;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/android/settings/r;->cancel(Z)Z

    :cond_0
    new-instance v0, Lcom/android/settings/r;

    invoke-virtual {p0}, Lcom/android/settings/NotificationAppListSettings;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/android/settings/r;-><init>(Landroid/app/FragmentManager;)V

    iput-object v0, p0, Lcom/android/settings/NotificationAppListSettings;->bBC:Lcom/android/settings/r;

    iget-object v0, p0, Lcom/android/settings/NotificationAppListSettings;->bBC:Lcom/android/settings/r;

    invoke-virtual {v0, p0}, Lcom/android/settings/r;->bin(Lcom/android/settings/NotificationAppListSettings;)V

    iget-object v0, p0, Lcom/android/settings/NotificationAppListSettings;->bBC:Lcom/android/settings/r;

    const v1, 0x7f121121

    invoke-virtual {v0, v1}, Lcom/android/settings/r;->setMessage(I)Lmiui/os/AsyncTaskWithProgress;

    move-result-object v0

    invoke-virtual {v0, v2}, Lmiui/os/AsyncTaskWithProgress;->setCancelable(Z)Lmiui/os/AsyncTaskWithProgress;

    move-result-object v0

    new-array v1, v2, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lmiui/os/AsyncTaskWithProgress;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    return-void
.end method

.method static synthetic bpu(Lcom/android/settings/NotificationAppListSettings;)Lcom/android/settings/cN;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/NotificationAppListSettings;->bBy:Lcom/android/settings/cN;

    return-object v0
.end method

.method static synthetic bpv(Lcom/android/settings/NotificationAppListSettings;)Landroid/view/LayoutInflater;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/NotificationAppListSettings;->bBz:Landroid/view/LayoutInflater;

    return-object v0
.end method

.method static synthetic bpw(Landroid/content/Context;Ljava/lang/String;I)Ljava/lang/String;
    .locals 1

    invoke-static {p0, p1, p2}, Lcom/android/settings/NotificationAppListSettings;->bpr(Landroid/content/Context;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic bpx(Lcom/android/settings/NotificationAppListSettings;Landroid/os/Bundle;Ljava/lang/String;Landroid/app/Fragment;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/android/settings/NotificationAppListSettings;->bps(Landroid/os/Bundle;Ljava/lang/String;Landroid/app/Fragment;)V

    return-void
.end method

.method static synthetic bpy(Lcom/android/settings/NotificationAppListSettings;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/NotificationAppListSettings;->bpt()V

    return-void
.end method


# virtual methods
.method public bpp(Ljava/lang/String;)Lcom/android/settings/ak;
    .locals 1

    new-instance v0, Lcom/android/settings/ak;

    invoke-direct {v0, p0, p1}, Lcom/android/settings/ak;-><init>(Lcom/android/settings/NotificationAppListSettings;Ljava/lang/String;)V

    return-object v0
.end method

.method public bpq(Landroid/content/pm/ApplicationInfo;I)Lcom/android/settings/ak;
    .locals 1

    new-instance v0, Lcom/android/settings/ak;

    invoke-direct {v0, p0, p1, p2}, Lcom/android/settings/ak;-><init>(Lcom/android/settings/NotificationAppListSettings;Landroid/content/pm/ApplicationInfo;I)V

    return-object v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3

    invoke-super {p0, p1}, Lcom/android/settings/BaseFragment;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/android/settings/NotificationAppListSettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/NotificationAppListSettings;->mContext:Landroid/content/Context;

    new-instance v0, Lcom/android/settings/cN;

    iget-object v1, p0, Lcom/android/settings/NotificationAppListSettings;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/android/settings/cN;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/settings/NotificationAppListSettings;->bBy:Lcom/android/settings/cN;

    invoke-virtual {p0}, Lcom/android/settings/NotificationAppListSettings;->bLS()Landroid/content/pm/PackageManager;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/NotificationAppListSettings;->mPackageManager:Landroid/content/pm/PackageManager;

    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    const-string/jumbo v1, "android.intent.action.PACKAGE_ADDED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string/jumbo v1, "android.intent.action.PACKAGE_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string/jumbo v1, "android.intent.action.PACKAGE_FULLY_REMOVED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string/jumbo v1, "android.intent.action.PACKAGE_REMOVED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string/jumbo v1, "android.intent.action.PACKAGE_REPLACED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/android/settings/NotificationAppListSettings;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/android/settings/NotificationAppListSettings;->bBB:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    return-void
.end method

.method public onDestroy()V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/NotificationAppListSettings;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/android/settings/NotificationAppListSettings;->bBB:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    iget-object v0, p0, Lcom/android/settings/NotificationAppListSettings;->bBy:Lcom/android/settings/cN;

    invoke-virtual {v0}, Lcom/android/settings/cN;->stop()V

    invoke-super {p0}, Lcom/android/settings/BaseFragment;->onDestroy()V

    return-void
.end method

.method public onInflateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2

    iput-object p1, p0, Lcom/android/settings/NotificationAppListSettings;->bBz:Landroid/view/LayoutInflater;

    const v0, 0x7f0d01d8

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    if-eqz p2, :cond_0

    invoke-virtual {p0}, Lcom/android/settings/NotificationAppListSettings;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-static {v1, p2}, Lcom/android/settings/dc;->bYu(Landroid/app/Activity;Landroid/view/View;)V

    :cond_0
    return-object v0
.end method

.method public onPause()V
    .locals 2

    invoke-super {p0}, Lcom/android/settings/BaseFragment;->onPause()V

    iget-object v0, p0, Lcom/android/settings/NotificationAppListSettings;->bBC:Lcom/android/settings/r;

    invoke-virtual {v0}, Lcom/android/settings/r;->isCancelled()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/NotificationAppListSettings;->bBC:Lcom/android/settings/r;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/android/settings/r;->cancel(Z)Z

    :cond_0
    return-void
.end method

.method public onResume()V
    .locals 0

    invoke-super {p0}, Lcom/android/settings/BaseFragment;->onResume()V

    invoke-direct {p0}, Lcom/android/settings/NotificationAppListSettings;->bpt()V

    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1, p2}, Lcom/android/settings/BaseFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    const v0, 0x7f0a0270

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/android/settings/NotificationAppListSettings;->bBA:Landroid/widget/ListView;

    new-instance v0, Lcom/android/settings/al;

    invoke-direct {v0, p0}, Lcom/android/settings/al;-><init>(Lcom/android/settings/NotificationAppListSettings;)V

    iput-object v0, p0, Lcom/android/settings/NotificationAppListSettings;->bBx:Lcom/android/settings/al;

    iget-object v0, p0, Lcom/android/settings/NotificationAppListSettings;->bBA:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/android/settings/NotificationAppListSettings;->bBx:Lcom/android/settings/al;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    return-void
.end method
