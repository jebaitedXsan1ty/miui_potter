.class final Lcom/android/settings/wifi/as;
.super Landroid/content/BroadcastReceiver;
.source "WifiEnabler.java"


# instance fields
.field final synthetic atF:Lcom/android/settings/wifi/n;


# direct methods
.method constructor <init>(Lcom/android/settings/wifi/n;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/wifi/as;->atF:Lcom/android/settings/wifi/n;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 3

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "android.net.wifi.WIFI_STATE_CHANGED"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v0, p0, Lcom/android/settings/wifi/as;->atF:Lcom/android/settings/wifi/n;

    iget-object v1, p0, Lcom/android/settings/wifi/as;->atF:Lcom/android/settings/wifi/n;

    invoke-static {v1}, Lcom/android/settings/wifi/n;->acH(Lcom/android/settings/wifi/n;)Landroid/net/wifi/WifiManager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/wifi/WifiManager;->getWifiState()I

    move-result v1

    invoke-static {v0, v1}, Lcom/android/settings/wifi/n;->acJ(Lcom/android/settings/wifi/n;I)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string/jumbo v1, "android.net.wifi.supplicant.STATE_CHANGE"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v0, p0, Lcom/android/settings/wifi/as;->atF:Lcom/android/settings/wifi/n;

    invoke-static {v0}, Lcom/android/settings/wifi/n;->acG(Lcom/android/settings/wifi/n;)Ljava/util/concurrent/atomic/AtomicBoolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v1, p0, Lcom/android/settings/wifi/as;->atF:Lcom/android/settings/wifi/n;

    const-string/jumbo v0, "newState"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/SupplicantState;

    invoke-static {v0}, Landroid/net/wifi/WifiInfo;->getDetailedStateOf(Landroid/net/wifi/SupplicantState;)Landroid/net/NetworkInfo$DetailedState;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/android/settings/wifi/n;->acI(Lcom/android/settings/wifi/n;Landroid/net/NetworkInfo$DetailedState;)V

    goto :goto_0

    :cond_2
    const-string/jumbo v1, "android.net.wifi.STATE_CHANGE"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string/jumbo v0, "networkInfo"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/NetworkInfo;

    iget-object v1, p0, Lcom/android/settings/wifi/as;->atF:Lcom/android/settings/wifi/n;

    invoke-static {v1}, Lcom/android/settings/wifi/n;->acG(Lcom/android/settings/wifi/n;)Ljava/util/concurrent/atomic/AtomicBoolean;

    move-result-object v1

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    iget-object v1, p0, Lcom/android/settings/wifi/as;->atF:Lcom/android/settings/wifi/n;

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->getDetailedState()Landroid/net/NetworkInfo$DetailedState;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/android/settings/wifi/n;->acI(Lcom/android/settings/wifi/n;Landroid/net/NetworkInfo$DetailedState;)V

    goto :goto_0
.end method
