.class final Lcom/android/settings/wifi/E;
.super Landroid/content/BroadcastReceiver;
.source "RequestToggleWiFiActivity.java"


# instance fields
.field final synthetic apA:Lcom/android/settings/wifi/RequestToggleWiFiActivity;

.field private final apz:Landroid/content/IntentFilter;


# direct methods
.method private constructor <init>(Lcom/android/settings/wifi/RequestToggleWiFiActivity;)V
    .locals 2

    iput-object p1, p0, Lcom/android/settings/wifi/E;->apA:Lcom/android/settings/wifi/RequestToggleWiFiActivity;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    new-instance v0, Landroid/content/IntentFilter;

    const-string/jumbo v1, "android.net.wifi.WIFI_STATE_CHANGED"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/android/settings/wifi/E;->apz:Landroid/content/IntentFilter;

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/settings/wifi/RequestToggleWiFiActivity;Lcom/android/settings/wifi/E;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/settings/wifi/E;-><init>(Lcom/android/settings/wifi/RequestToggleWiFiActivity;)V

    return-void
.end method


# virtual methods
.method public afV()V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/wifi/E;->apA:Lcom/android/settings/wifi/RequestToggleWiFiActivity;

    iget-object v1, p0, Lcom/android/settings/wifi/E;->apz:Landroid/content/IntentFilter;

    invoke-virtual {v0, p0, v1}, Lcom/android/settings/wifi/RequestToggleWiFiActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    return-void
.end method

.method public afW()V
    .locals 1

    iget-object v0, p0, Lcom/android/settings/wifi/E;->apA:Lcom/android/settings/wifi/RequestToggleWiFiActivity;

    invoke-virtual {v0, p0}, Lcom/android/settings/wifi/RequestToggleWiFiActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    return-void
.end method

.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 3

    iget-object v0, p0, Lcom/android/settings/wifi/E;->apA:Lcom/android/settings/wifi/RequestToggleWiFiActivity;

    invoke-virtual {v0}, Landroid/app/Activity;->isFinishing()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {v0}, Landroid/app/Activity;->isDestroyed()Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    return-void

    :cond_1
    iget-object v1, p0, Lcom/android/settings/wifi/E;->apA:Lcom/android/settings/wifi/RequestToggleWiFiActivity;

    invoke-static {v1}, Lcom/android/settings/wifi/RequestToggleWiFiActivity;->afU(Lcom/android/settings/wifi/RequestToggleWiFiActivity;)Landroid/net/wifi/WifiManager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/wifi/WifiManager;->getWifiState()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    :cond_2
    :goto_0
    :pswitch_0
    return-void

    :pswitch_1
    iget-object v0, p0, Lcom/android/settings/wifi/E;->apA:Lcom/android/settings/wifi/RequestToggleWiFiActivity;

    invoke-static {v0}, Lcom/android/settings/wifi/RequestToggleWiFiActivity;->afT(Lcom/android/settings/wifi/RequestToggleWiFiActivity;)I

    move-result v0

    const/4 v1, 0x2

    if-eq v0, v1, :cond_3

    iget-object v0, p0, Lcom/android/settings/wifi/E;->apA:Lcom/android/settings/wifi/RequestToggleWiFiActivity;

    invoke-static {v0}, Lcom/android/settings/wifi/RequestToggleWiFiActivity;->afT(Lcom/android/settings/wifi/RequestToggleWiFiActivity;)I

    move-result v0

    const/4 v1, 0x4

    if-ne v0, v1, :cond_2

    :cond_3
    iget-object v0, p0, Lcom/android/settings/wifi/E;->apA:Lcom/android/settings/wifi/RequestToggleWiFiActivity;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Lcom/android/settings/wifi/RequestToggleWiFiActivity;->setResult(I)V

    iget-object v0, p0, Lcom/android/settings/wifi/E;->apA:Lcom/android/settings/wifi/RequestToggleWiFiActivity;

    invoke-virtual {v0}, Lcom/android/settings/wifi/RequestToggleWiFiActivity;->finish()V

    goto :goto_0

    :pswitch_2
    const v1, 0x7f121537

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    iget-object v0, p0, Lcom/android/settings/wifi/E;->apA:Lcom/android/settings/wifi/RequestToggleWiFiActivity;

    invoke-virtual {v0}, Lcom/android/settings/wifi/RequestToggleWiFiActivity;->finish()V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
