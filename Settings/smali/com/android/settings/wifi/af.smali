.class final Lcom/android/settings/wifi/af;
.super Landroid/content/BroadcastReceiver;
.source "MiuiWifiSettings.java"


# instance fields
.field final synthetic atm:Lcom/android/settings/wifi/MiuiWifiSettings;


# direct methods
.method constructor <init>(Lcom/android/settings/wifi/MiuiWifiSettings;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/wifi/af;->atm:Lcom/android/settings/wifi/MiuiWifiSettings;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 4

    const/4 v3, 0x0

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "android.net.wifi.SCAN_RESULTS"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v0, p0, Lcom/android/settings/wifi/af;->atm:Lcom/android/settings/wifi/MiuiWifiSettings;

    invoke-virtual {v0, v3}, Lcom/android/settings/wifi/MiuiWifiSettings;->abi(Z)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string/jumbo v1, "android.net.wifi.STATE_CHANGE"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/wifi/af;->atm:Lcom/android/settings/wifi/MiuiWifiSettings;

    iget-boolean v0, v0, Lcom/android/settings/wifi/MiuiWifiSettings;->aqH:Z

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    const-string/jumbo v0, "networkInfo"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/NetworkInfo;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/android/settings/wifi/af;->atm:Lcom/android/settings/wifi/MiuiWifiSettings;

    invoke-static {v1}, Lcom/android/settings/wifi/MiuiWifiSettings;->abk(Lcom/android/settings/wifi/MiuiWifiSettings;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/android/settings/wifi/af;->atm:Lcom/android/settings/wifi/MiuiWifiSettings;

    invoke-static {v1}, Lcom/android/settings/wifi/MiuiWifiSettings;->abj(Lcom/android/settings/wifi/MiuiWifiSettings;)Landroid/net/NetworkInfo$State;

    move-result-object v1

    sget-object v2, Landroid/net/NetworkInfo$State;->DISCONNECTED:Landroid/net/NetworkInfo$State;

    if-ne v1, v2, :cond_2

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->getState()Landroid/net/NetworkInfo$State;

    move-result-object v1

    sget-object v2, Landroid/net/NetworkInfo$State;->CONNECTING:Landroid/net/NetworkInfo$State;

    if-ne v1, v2, :cond_2

    iget-object v1, p0, Lcom/android/settings/wifi/af;->atm:Lcom/android/settings/wifi/MiuiWifiSettings;

    invoke-virtual {v1}, Lcom/android/settings/wifi/MiuiWifiSettings;->getListView()Landroid/widget/ListView;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/widget/ListView;->smoothScrollToPosition(I)V

    iget-object v1, p0, Lcom/android/settings/wifi/af;->atm:Lcom/android/settings/wifi/MiuiWifiSettings;

    invoke-static {v1, v3}, Lcom/android/settings/wifi/MiuiWifiSettings;->abp(Lcom/android/settings/wifi/MiuiWifiSettings;Z)Z

    :cond_2
    iget-object v1, p0, Lcom/android/settings/wifi/af;->atm:Lcom/android/settings/wifi/MiuiWifiSettings;

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->getState()Landroid/net/NetworkInfo$State;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/android/settings/wifi/MiuiWifiSettings;->abo(Lcom/android/settings/wifi/MiuiWifiSettings;Landroid/net/NetworkInfo$State;)Landroid/net/NetworkInfo$State;

    goto :goto_0
.end method
