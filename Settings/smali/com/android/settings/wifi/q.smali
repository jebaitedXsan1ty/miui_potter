.class public Lcom/android/settings/wifi/q;
.super Lcom/android/settings/core/e;
.source "WpsPreferenceController.java"

# interfaces
.implements Lcom/android/settings/core/lifecycle/b;
.implements Lcom/android/settings/core/lifecycle/a/d;
.implements Lcom/android/settings/core/lifecycle/a/b;


# instance fields
.field private final amj:Landroid/content/IntentFilter;

.field private final amk:Landroid/app/FragmentManager;

.field private final aml:Landroid/net/wifi/WifiManager;

.field private amm:Landroid/preference/Preference;

.field private amn:Landroid/preference/Preference;

.field final mReceiver:Landroid/content/BroadcastReceiver;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/android/settings/core/lifecycle/c;Landroid/net/wifi/WifiManager;Landroid/app/FragmentManager;)V
    .locals 2

    invoke-direct {p0, p1}, Lcom/android/settings/core/e;-><init>(Landroid/content/Context;)V

    new-instance v0, Lcom/android/settings/wifi/at;

    invoke-direct {v0, p0}, Lcom/android/settings/wifi/at;-><init>(Lcom/android/settings/wifi/q;)V

    iput-object v0, p0, Lcom/android/settings/wifi/q;->mReceiver:Landroid/content/BroadcastReceiver;

    new-instance v0, Landroid/content/IntentFilter;

    const-string/jumbo v1, "android.net.wifi.WIFI_STATE_CHANGED"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/android/settings/wifi/q;->amj:Landroid/content/IntentFilter;

    iput-object p3, p0, Lcom/android/settings/wifi/q;->aml:Landroid/net/wifi/WifiManager;

    iput-object p4, p0, Lcom/android/settings/wifi/q;->amk:Landroid/app/FragmentManager;

    invoke-virtual {p2, p0}, Lcom/android/settings/core/lifecycle/c;->ajv(Lcom/android/settings/core/lifecycle/b;)Lcom/android/settings/core/lifecycle/b;

    return-void
.end method

.method private acR()V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/wifi/q;->amn:Landroid/preference/Preference;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/wifi/q;->amm:Landroid/preference/Preference;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/wifi/q;->aml:Landroid/net/wifi/WifiManager;

    invoke-virtual {v0}, Landroid/net/wifi/WifiManager;->isWifiEnabled()Z

    move-result v0

    iget-object v1, p0, Lcom/android/settings/wifi/q;->amn:Landroid/preference/Preference;

    invoke-virtual {v1, v0}, Landroid/preference/Preference;->setEnabled(Z)V

    iget-object v1, p0, Lcom/android/settings/wifi/q;->amm:Landroid/preference/Preference;

    invoke-virtual {v1, v0}, Landroid/preference/Preference;->setEnabled(Z)V

    :cond_0
    return-void
.end method

.method static synthetic acU(Lcom/android/settings/wifi/q;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/wifi/q;->acR()V

    return-void
.end method


# virtual methods
.method synthetic acS(Landroid/preference/Preference;)Z
    .locals 3

    new-instance v0, Lcom/android/settings/wifi/WpsPreferenceController$WpsFragment;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/android/settings/wifi/WpsPreferenceController$WpsFragment;-><init>(I)V

    iget-object v1, p0, Lcom/android/settings/wifi/q;->amk:Landroid/app/FragmentManager;

    const-string/jumbo v2, "wps_push_button"

    invoke-virtual {v0, v1, v2}, Lcom/android/settings/wifi/WpsPreferenceController$WpsFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    const/4 v0, 0x1

    return v0
.end method

.method synthetic acT(Landroid/preference/Preference;)Z
    .locals 4

    const/4 v3, 0x1

    new-instance v0, Lcom/android/settings/wifi/WpsPreferenceController$WpsFragment;

    invoke-direct {v0, v3}, Lcom/android/settings/wifi/WpsPreferenceController$WpsFragment;-><init>(I)V

    iget-object v1, p0, Lcom/android/settings/wifi/q;->amk:Landroid/app/FragmentManager;

    const-string/jumbo v2, "wps_pin_entry"

    invoke-virtual {v0, v1, v2}, Lcom/android/settings/wifi/WpsPreferenceController$WpsFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    return v3
.end method

.method public fm(Landroid/preference/Preference;)Z
    .locals 7

    const/4 v6, 0x1

    const/4 v5, 0x0

    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "wps_pin"

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    const-string/jumbo v0, "wps_setup"

    invoke-virtual {v2, v0, v6}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    iget-object v0, p0, Lcom/android/settings/wifi/q;->mContext:Landroid/content/Context;

    invoke-virtual {p1}, Landroid/preference/Preference;->getFragment()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Landroid/preference/Preference;->getTitleRes()I

    move-result v3

    invoke-virtual {p1}, Landroid/preference/Preference;->getTitle()Ljava/lang/CharSequence;

    move-result-object v4

    invoke-static/range {v0 .. v5}, Lcom/android/settings/aq;->bqH(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;ILjava/lang/CharSequence;Z)Landroid/content/Intent;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/wifi/q;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    return v6

    :cond_0
    return v5
.end method

.method public i(Landroid/preference/PreferenceScreen;)V
    .locals 3

    invoke-super {p0, p1}, Lcom/android/settings/core/e;->i(Landroid/preference/PreferenceScreen;)V

    const-string/jumbo v0, "wps_push_button"

    invoke-virtual {p1, v0}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/wifi/q;->amn:Landroid/preference/Preference;

    const-string/jumbo v0, "wps_pin_entry"

    invoke-virtual {p1, v0}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/wifi/q;->amm:Landroid/preference/Preference;

    iget-object v0, p0, Lcom/android/settings/wifi/q;->amn:Landroid/preference/Preference;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/wifi/q;->amm:Landroid/preference/Preference;

    if-nez v0, :cond_1

    :cond_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/settings/wifi/q;->amn:Landroid/preference/Preference;

    new-instance v1, Lcom/android/settings/wifi/-$Lambda$BqansEhJKuZ7wfZITgn6bG0h-7U;

    const/4 v2, 0x0

    invoke-direct {v1, v2, p0}, Lcom/android/settings/wifi/-$Lambda$BqansEhJKuZ7wfZITgn6bG0h-7U;-><init>(BLjava/lang/Object;)V

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    iget-object v0, p0, Lcom/android/settings/wifi/q;->amm:Landroid/preference/Preference;

    new-instance v1, Lcom/android/settings/wifi/-$Lambda$BqansEhJKuZ7wfZITgn6bG0h-7U;

    const/4 v2, 0x1

    invoke-direct {v1, v2, p0}, Lcom/android/settings/wifi/-$Lambda$BqansEhJKuZ7wfZITgn6bG0h-7U;-><init>(BLjava/lang/Object;)V

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    invoke-direct {p0}, Lcom/android/settings/wifi/q;->acR()V

    return-void
.end method

.method public l()Ljava/lang/String;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public onPause()V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/wifi/q;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/android/settings/wifi/q;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    return-void
.end method

.method public onResume()V
    .locals 3

    iget-object v0, p0, Lcom/android/settings/wifi/q;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/android/settings/wifi/q;->mReceiver:Landroid/content/BroadcastReceiver;

    iget-object v2, p0, Lcom/android/settings/wifi/q;->amj:Landroid/content/IntentFilter;

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    return-void
.end method

.method public p()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method
