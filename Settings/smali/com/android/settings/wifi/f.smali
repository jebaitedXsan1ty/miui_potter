.class public Lcom/android/settings/wifi/f;
.super Lcom/android/settings/bA;
.source "TetherStatusController.java"


# instance fields
.field private akB:Z

.field private akC:Landroid/content/IntentFilter;

.field private final mReceiver:Landroid/content/BroadcastReceiver;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/widget/TextView;)V
    .locals 2

    invoke-direct {p0, p1, p2}, Lcom/android/settings/bA;-><init>(Landroid/content/Context;Landroid/widget/TextView;)V

    new-instance v0, Lcom/android/settings/wifi/ae;

    invoke-direct {v0, p0}, Lcom/android/settings/wifi/ae;-><init>(Lcom/android/settings/wifi/f;)V

    iput-object v0, p0, Lcom/android/settings/wifi/f;->mReceiver:Landroid/content/BroadcastReceiver;

    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    iput-object v0, p0, Lcom/android/settings/wifi/f;->akC:Landroid/content/IntentFilter;

    iget-object v0, p0, Lcom/android/settings/wifi/f;->akC:Landroid/content/IntentFilter;

    const-string/jumbo v1, "android.net.conn.TETHER_STATE_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public aaN(Landroid/content/Context;)Z
    .locals 2

    const-string/jumbo v0, "wifi"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/WifiManager;

    invoke-virtual {v0}, Landroid/net/wifi/WifiManager;->getWifiApState()I

    move-result v0

    const/16 v1, 0xd

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public pause()V
    .locals 2

    iget-boolean v0, p0, Lcom/android/settings/wifi/f;->akB:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/wifi/f;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/android/settings/wifi/f;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/settings/wifi/f;->akB:Z

    :cond_0
    return-void
.end method

.method public wt()V
    .locals 3

    iget-object v0, p0, Lcom/android/settings/wifi/f;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/android/settings/wifi/f;->mReceiver:Landroid/content/BroadcastReceiver;

    iget-object v2, p0, Lcom/android/settings/wifi/f;->akC:Landroid/content/IntentFilter;

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/settings/wifi/f;->akB:Z

    return-void
.end method

.method public wv()V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/wifi/f;->bRZ:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/android/settings/wifi/f;->bRZ:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/android/settings/wifi/f;->mContext:Landroid/content/Context;

    invoke-virtual {p0, v0}, Lcom/android/settings/wifi/f;->aaN(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    const v0, 0x7f121657

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(I)V

    :cond_0
    return-void

    :cond_1
    const v0, 0x7f121656

    goto :goto_0
.end method
