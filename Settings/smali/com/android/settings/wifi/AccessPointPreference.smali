.class public Lcom/android/settings/wifi/AccessPointPreference;
.super Lmiui/preference/RadioButtonPreference;
.source "AccessPointPreference.java"


# static fields
.field private static final anC:[I

.field private static final anD:[I

.field static final anE:[I

.field private static anQ:[I


# instance fields
.field protected anF:Lcom/android/settingslib/wifi/i;

.field private anG:Landroid/graphics/drawable/Drawable;

.field private final anH:Lcom/android/settings/wifi/w;

.field private final anI:I

.field private anJ:Ljava/lang/CharSequence;

.field private anK:Z

.field private anL:Z

.field private anM:I

.field private final anN:Ljava/lang/Runnable;

.field private anO:Landroid/widget/TextView;

.field private final anP:Landroid/graphics/drawable/StateListDrawable;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-array v0, v3, [I

    const v1, 0x7f04013f

    aput v1, v0, v2

    sput-object v0, Lcom/android/settings/wifi/AccessPointPreference;->anD:[I

    new-array v0, v2, [I

    sput-object v0, Lcom/android/settings/wifi/AccessPointPreference;->anC:[I

    new-array v0, v3, [I

    const v1, 0x7f0401b2

    aput v1, v0, v2

    sput-object v0, Lcom/android/settings/wifi/AccessPointPreference;->anQ:[I

    const v0, 0x7f120077

    const v1, 0x7f12007c

    const v2, 0x7f12007b

    const v3, 0x7f12007a

    filled-new-array {v0, v1, v2, v3}, [I

    move-result-object v0

    sput-object v0, Lcom/android/settings/wifi/AccessPointPreference;->anE:[I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3

    const/4 v2, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, p2}, Lmiui/preference/RadioButtonPreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    iput-boolean v1, p0, Lcom/android/settings/wifi/AccessPointPreference;->anK:Z

    new-instance v0, Lcom/android/settings/wifi/aF;

    invoke-direct {v0, p0}, Lcom/android/settings/wifi/aF;-><init>(Lcom/android/settings/wifi/AccessPointPreference;)V

    iput-object v0, p0, Lcom/android/settings/wifi/AccessPointPreference;->anN:Ljava/lang/Runnable;

    iput-object v2, p0, Lcom/android/settings/wifi/AccessPointPreference;->anP:Landroid/graphics/drawable/StateListDrawable;

    iput v1, p0, Lcom/android/settings/wifi/AccessPointPreference;->anI:I

    iput-object v2, p0, Lcom/android/settings/wifi/AccessPointPreference;->anH:Lcom/android/settings/wifi/w;

    return-void
.end method

.method public constructor <init>(Lcom/android/settingslib/wifi/i;Landroid/content/Context;Lcom/android/settings/wifi/w;Z)V
    .locals 3

    const/4 v2, 0x0

    invoke-direct {p0, p2}, Lmiui/preference/RadioButtonPreference;-><init>(Landroid/content/Context;)V

    iput-boolean v2, p0, Lcom/android/settings/wifi/AccessPointPreference;->anK:Z

    new-instance v0, Lcom/android/settings/wifi/aF;

    invoke-direct {v0, p0}, Lcom/android/settings/wifi/aF;-><init>(Lcom/android/settings/wifi/AccessPointPreference;)V

    iput-object v0, p0, Lcom/android/settings/wifi/AccessPointPreference;->anN:Ljava/lang/Runnable;

    iput-object p3, p0, Lcom/android/settings/wifi/AccessPointPreference;->anH:Lcom/android/settings/wifi/w;

    iput-object p1, p0, Lcom/android/settings/wifi/AccessPointPreference;->anF:Lcom/android/settingslib/wifi/i;

    iput-boolean p4, p0, Lcom/android/settings/wifi/AccessPointPreference;->anK:Z

    iget-object v0, p0, Lcom/android/settings/wifi/AccessPointPreference;->anF:Lcom/android/settingslib/wifi/i;

    invoke-virtual {v0, p0}, Lcom/android/settingslib/wifi/i;->chK(Ljava/lang/Object;)V

    const/4 v0, -0x1

    iput v0, p0, Lcom/android/settings/wifi/AccessPointPreference;->anM:I

    invoke-virtual {p2}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v0

    sget-object v1, Lcom/android/settings/wifi/AccessPointPreference;->anQ:[I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources$Theme;->obtainStyledAttributes([I)Landroid/content/res/TypedArray;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/StateListDrawable;

    iput-object v0, p0, Lcom/android/settings/wifi/AccessPointPreference;->anP:Landroid/graphics/drawable/StateListDrawable;

    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0702c9

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/android/settings/wifi/AccessPointPreference;->anI:I

    invoke-virtual {p0}, Lcom/android/settings/wifi/AccessPointPreference;->aew()V

    return-void
.end method

.method private aez()V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/wifi/AccessPointPreference;->anO:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/wifi/AccessPointPreference;->anO:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/android/settings/wifi/AccessPointPreference;->anN:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->post(Ljava/lang/Runnable;)Z

    :cond_0
    return-void
.end method


# virtual methods
.method protected aeA(Landroid/content/Context;)V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/wifi/AccessPointPreference;->anF:Lcom/android/settingslib/wifi/i;

    invoke-virtual {v0}, Lcom/android/settingslib/wifi/i;->chL()Landroid/net/wifi/WifiConfiguration;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/android/settings/wifi/AccessPointPreference;->anH:Lcom/android/settings/wifi/w;

    iget v0, v0, Landroid/net/wifi/WifiConfiguration;->creatorUid:I

    invoke-static {v1, v0}, Lcom/android/settings/wifi/w;->aeD(Lcom/android/settings/wifi/w;I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/wifi/AccessPointPreference;->anG:Landroid/graphics/drawable/Drawable;

    :cond_0
    return-void
.end method

.method protected aeB(ILandroid/content/Context;)V
    .locals 2

    const/4 v1, 0x0

    const/4 v0, -0x1

    if-ne p1, v0, :cond_1

    invoke-virtual {p0, v1}, Lcom/android/settings/wifi/AccessPointPreference;->setIcon(Landroid/graphics/drawable/Drawable;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0}, Lcom/android/settings/wifi/AccessPointPreference;->getIcon()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-nez v0, :cond_0

    const v0, 0x7f08045e

    invoke-virtual {p2, v0}, Landroid/content/Context;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/StateListDrawable;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/android/settings/wifi/AccessPointPreference;->anF:Lcom/android/settingslib/wifi/i;

    iget v1, v1, Lcom/android/settingslib/wifi/i;->cCS:I

    if-eqz v1, :cond_2

    sget-object v1, Lcom/android/settings/wifi/AccessPointPreference;->anD:[I

    :goto_1
    invoke-virtual {v0, v1}, Landroid/graphics/drawable/StateListDrawable;->setState([I)Z

    invoke-virtual {v0}, Landroid/graphics/drawable/StateListDrawable;->getCurrent()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/settings/wifi/AccessPointPreference;->setIcon(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    :cond_2
    sget-object v1, Lcom/android/settings/wifi/AccessPointPreference;->anC:[I

    goto :goto_1
.end method

.method public aeu()Lcom/android/settingslib/wifi/i;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/wifi/AccessPointPreference;->anF:Lcom/android/settingslib/wifi/i;

    return-object v0
.end method

.method public aev(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/android/settings/wifi/AccessPointPreference;->anL:Z

    return-void
.end method

.method public aew()V
    .locals 8

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/android/settings/wifi/AccessPointPreference;->anF:Lcom/android/settingslib/wifi/i;

    invoke-virtual {v0}, Lcom/android/settingslib/wifi/i;->chk()Landroid/net/wifi/WifiInfo;

    move-result-object v0

    if-eqz v0, :cond_8

    iget-object v3, p0, Lcom/android/settings/wifi/AccessPointPreference;->anF:Lcom/android/settingslib/wifi/i;

    iget v3, v3, Lcom/android/settingslib/wifi/i;->cCO:I

    const/4 v4, -0x1

    if-eq v3, v4, :cond_4

    invoke-virtual {v0}, Landroid/net/wifi/WifiInfo;->getNetworkId()I

    move-result v0

    iget-object v3, p0, Lcom/android/settings/wifi/AccessPointPreference;->anF:Lcom/android/settingslib/wifi/i;

    iget v3, v3, Lcom/android/settingslib/wifi/i;->cCO:I

    if-ne v0, v3, :cond_3

    move v0, v1

    :goto_0
    invoke-virtual {p0, v0}, Lcom/android/settings/wifi/AccessPointPreference;->aev(Z)V

    iget-boolean v0, p0, Lcom/android/settings/wifi/AccessPointPreference;->anK:Z

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/android/settings/wifi/AccessPointPreference;->anF:Lcom/android/settingslib/wifi/i;

    invoke-virtual {v0}, Lcom/android/settingslib/wifi/i;->chx()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/settings/wifi/AccessPointPreference;->setTitle(Ljava/lang/CharSequence;)V

    :goto_1
    invoke-virtual {p0}, Lcom/android/settings/wifi/AccessPointPreference;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v3, p0, Lcom/android/settings/wifi/AccessPointPreference;->anF:Lcom/android/settingslib/wifi/i;

    invoke-virtual {v3}, Lcom/android/settingslib/wifi/i;->chn()I

    move-result v3

    iget v4, p0, Lcom/android/settings/wifi/AccessPointPreference;->anM:I

    if-eq v3, v4, :cond_0

    iput v3, p0, Lcom/android/settings/wifi/AccessPointPreference;->anM:I

    iget v4, p0, Lcom/android/settings/wifi/AccessPointPreference;->anM:I

    invoke-virtual {p0, v4, v0}, Lcom/android/settings/wifi/AccessPointPreference;->aeB(ILandroid/content/Context;)V

    invoke-virtual {p0}, Lcom/android/settings/wifi/AccessPointPreference;->notifyChanged()V

    :cond_0
    invoke-virtual {p0, v0}, Lcom/android/settings/wifi/AccessPointPreference;->aeA(Landroid/content/Context;)V

    iget-boolean v0, p0, Lcom/android/settings/wifi/AccessPointPreference;->anK:Z

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/android/settings/wifi/AccessPointPreference;->anF:Lcom/android/settingslib/wifi/i;

    invoke-virtual {v0}, Lcom/android/settingslib/wifi/i;->chp()Ljava/lang/String;

    move-result-object v0

    :goto_2
    invoke-virtual {p0, v0}, Lcom/android/settings/wifi/AccessPointPreference;->setSummary(Ljava/lang/CharSequence;)V

    invoke-virtual {p0}, Lcom/android/settings/wifi/AccessPointPreference;->getTitle()Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/wifi/AccessPointPreference;->anJ:Ljava/lang/CharSequence;

    invoke-virtual {p0}, Lcom/android/settings/wifi/AccessPointPreference;->getSummary()Ljava/lang/CharSequence;

    move-result-object v0

    if-eqz v0, :cond_1

    new-array v0, v7, [Ljava/lang/CharSequence;

    iget-object v4, p0, Lcom/android/settings/wifi/AccessPointPreference;->anJ:Ljava/lang/CharSequence;

    aput-object v4, v0, v2

    const-string/jumbo v4, ","

    aput-object v4, v0, v1

    invoke-virtual {p0}, Lcom/android/settings/wifi/AccessPointPreference;->getSummary()Ljava/lang/CharSequence;

    move-result-object v4

    aput-object v4, v0, v6

    invoke-static {v0}, Landroid/text/TextUtils;->concat([Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/wifi/AccessPointPreference;->anJ:Ljava/lang/CharSequence;

    :cond_1
    if-lez v3, :cond_2

    sget-object v0, Lcom/android/settings/wifi/AccessPointPreference;->anE:[I

    array-length v0, v0

    if-gt v3, v0, :cond_2

    new-array v0, v7, [Ljava/lang/CharSequence;

    iget-object v4, p0, Lcom/android/settings/wifi/AccessPointPreference;->anJ:Ljava/lang/CharSequence;

    aput-object v4, v0, v2

    const-string/jumbo v4, ","

    aput-object v4, v0, v1

    invoke-virtual {p0}, Lcom/android/settings/wifi/AccessPointPreference;->getContext()Landroid/content/Context;

    move-result-object v4

    sget-object v5, Lcom/android/settings/wifi/AccessPointPreference;->anE:[I

    add-int/lit8 v3, v3, -0x1

    aget v3, v5, v3

    invoke-virtual {v4, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v0, v6

    invoke-static {v0}, Landroid/text/TextUtils;->concat([Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/wifi/AccessPointPreference;->anJ:Ljava/lang/CharSequence;

    :cond_2
    new-array v3, v7, [Ljava/lang/CharSequence;

    iget-object v0, p0, Lcom/android/settings/wifi/AccessPointPreference;->anJ:Ljava/lang/CharSequence;

    aput-object v0, v3, v2

    const-string/jumbo v0, ","

    aput-object v0, v3, v1

    iget-object v0, p0, Lcom/android/settings/wifi/AccessPointPreference;->anF:Lcom/android/settingslib/wifi/i;

    invoke-virtual {v0}, Lcom/android/settingslib/wifi/i;->chP()I

    move-result v0

    if-nez v0, :cond_7

    invoke-virtual {p0}, Lcom/android/settings/wifi/AccessPointPreference;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f120078

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_3
    aput-object v0, v3, v6

    invoke-static {v3}, Landroid/text/TextUtils;->concat([Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/wifi/AccessPointPreference;->anJ:Ljava/lang/CharSequence;

    return-void

    :cond_3
    move v0, v2

    goto/16 :goto_0

    :cond_4
    move v0, v2

    goto/16 :goto_0

    :cond_5
    iget-object v0, p0, Lcom/android/settings/wifi/AccessPointPreference;->anF:Lcom/android/settingslib/wifi/i;

    invoke-virtual {v0}, Lcom/android/settingslib/wifi/i;->chW()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/settings/wifi/AccessPointPreference;->setTitle(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    :cond_6
    iget-object v0, p0, Lcom/android/settings/wifi/AccessPointPreference;->anF:Lcom/android/settingslib/wifi/i;

    invoke-virtual {v0}, Lcom/android/settingslib/wifi/i;->chz()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_2

    :cond_7
    invoke-virtual {p0}, Lcom/android/settings/wifi/AccessPointPreference;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f120079

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_3

    :cond_8
    move v0, v2

    goto/16 :goto_0
.end method

.method public aex()Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/settings/wifi/AccessPointPreference;->anL:Z

    return v0
.end method

.method public aey()V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/wifi/AccessPointPreference;->aez()V

    return-void
.end method

.method protected notifyChanged()V
    .locals 2

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v1

    if-eq v0, v1, :cond_0

    invoke-direct {p0}, Lcom/android/settings/wifi/AccessPointPreference;->aez()V

    :goto_0
    return-void

    :cond_0
    invoke-super {p0}, Lmiui/preference/RadioButtonPreference;->notifyChanged()V

    goto :goto_0
.end method

.method protected onBindView(Landroid/view/View;)V
    .locals 2

    invoke-super {p0, p1}, Lmiui/preference/RadioButtonPreference;->onBindView(Landroid/view/View;)V

    iget-object v0, p0, Lcom/android/settings/wifi/AccessPointPreference;->anF:Lcom/android/settingslib/wifi/i;

    if-nez v0, :cond_0

    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/android/settings/wifi/AccessPointPreference;->getIcon()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-eqz v0, :cond_1

    iget v1, p0, Lcom/android/settings/wifi/AccessPointPreference;->anM:I

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setLevel(I)Z

    :cond_1
    const v0, 0x1020016

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/settings/wifi/AccessPointPreference;->anO:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/android/settings/wifi/AccessPointPreference;->anJ:Ljava/lang/CharSequence;

    invoke-virtual {p1, v0}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    return-void
.end method
