.class public Lcom/android/settings/wifi/WifiSettings;
.super Lcom/android/settings/MiuiRestrictedSettingsFragment;
.source "WifiSettings.java"

# interfaces
.implements Lcom/android/settings/search/Indexable;
.implements Lcom/android/settingslib/wifi/e;
.implements Lcom/android/settingslib/wifi/j;
.implements Lcom/android/settings/wifi/t;


# static fields
.field public static final SEARCH_INDEX_DATA_PROVIDER:Lcom/android/settings/search/Indexable$SearchIndexProvider;

.field public static final SUMMARY_PROVIDER_FACTORY:Lcom/android/settings/dashboard/F;

.field private static final aqQ:Z


# instance fields
.field protected aqH:Z

.field protected aqI:Z

.field protected aqJ:Z

.field protected aqK:Lcom/android/settings/wifi/i;

.field protected aqL:Lcom/android/settingslib/wifi/a;

.field protected aqM:Landroid/net/wifi/WifiManager$ActionListener;

.field protected aqN:Landroid/net/wifi/WifiManager$ActionListener;

.field protected aqO:Landroid/net/wifi/WifiManager$ActionListener;

.field protected aqP:Lcom/android/settings/wifi/w;

.field private aqR:Landroid/os/Bundle;

.field private aqS:Landroid/preference/PreferenceCategory;

.field private aqT:Landroid/preference/Preference;

.field private aqU:Landroid/preference/PreferenceCategory;

.field private aqV:Landroid/os/HandlerThread;

.field private aqW:Landroid/preference/Preference;

.field private aqX:Landroid/preference/PreferenceCategory;

.field private aqY:Lcom/android/settings/wifi/s;

.field private aqZ:I

.field private ara:Lcom/android/settingslib/wifi/i;

.field private arb:Z

.field private final arc:Ljava/lang/Runnable;

.field private ard:Z

.field private are:Ljava/lang/String;

.field private arf:Landroid/widget/ProgressBar;

.field private arg:Landroid/preference/Preference;

.field private arh:Lcom/android/settingslib/wifi/i;

.field private ari:Lcom/android/settings/wifi/LinkablePreference;

.field private final arj:Ljava/lang/Runnable;

.field protected ark:Landroid/net/wifi/WifiManager;

.field private arl:Landroid/os/Bundle;

.field private arm:Lcom/android/settings/wifi/ab;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const-string/jumbo v0, "WifiSettings"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    sput-boolean v0, Lcom/android/settings/wifi/WifiSettings;->aqQ:Z

    new-instance v0, Lcom/android/settings/wifi/aW;

    invoke-direct {v0}, Lcom/android/settings/wifi/aW;-><init>()V

    sput-object v0, Lcom/android/settings/wifi/WifiSettings;->SEARCH_INDEX_DATA_PROVIDER:Lcom/android/settings/search/Indexable$SearchIndexProvider;

    new-instance v0, Lcom/android/settings/wifi/aX;

    invoke-direct {v0}, Lcom/android/settings/wifi/aX;-><init>()V

    sput-object v0, Lcom/android/settings/wifi/WifiSettings;->SUMMARY_PROVIDER_FACTORY:Lcom/android/settings/dashboard/F;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    const-string/jumbo v0, "no_config_wifi"

    invoke-direct {p0, v0}, Lcom/android/settings/MiuiRestrictedSettingsFragment;-><init>(Ljava/lang/String;)V

    sget-object v0, Lcom/android/settings/wifi/-$Lambda$3CQBHR4u5uax4MZbtojdY5iwvP0;->$INST$0:Lcom/android/settings/wifi/-$Lambda$3CQBHR4u5uax4MZbtojdY5iwvP0;

    iput-object v0, p0, Lcom/android/settings/wifi/WifiSettings;->arj:Ljava/lang/Runnable;

    new-instance v0, Lcom/android/settings/wifi/-$Lambda$i6EKOFH0rP-I9Zxq9YpMtsrt1s0;

    const/4 v1, 0x1

    invoke-direct {v0, v1, p0}, Lcom/android/settings/wifi/-$Lambda$i6EKOFH0rP-I9Zxq9YpMtsrt1s0;-><init>(BLjava/lang/Object;)V

    iput-object v0, p0, Lcom/android/settings/wifi/WifiSettings;->arc:Ljava/lang/Runnable;

    return-void
.end method

.method static ahf(Landroid/content/Context;Landroid/net/wifi/WifiConfiguration;)Z
    .locals 1

    invoke-static {p0, p1}, Lcom/android/settings/wifi/WifiSettings;->ahg(Landroid/content/Context;Landroid/net/wifi/WifiConfiguration;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    return v0
.end method

.method static ahg(Landroid/content/Context;Landroid/net/wifi/WifiConfiguration;)Z
    .locals 5

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-nez p1, :cond_0

    return v1

    :cond_0
    const-string/jumbo v0, "device_policy"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/admin/DevicePolicyManager;

    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    const-string/jumbo v4, "android.software.device_admin"

    invoke-virtual {v3, v4}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    if-nez v0, :cond_1

    return v2

    :cond_1
    if-eqz v0, :cond_5

    invoke-virtual {v0}, Landroid/app/admin/DevicePolicyManager;->getDeviceOwnerComponentOnAnyUser()Landroid/content/ComponentName;

    move-result-object v4

    if-eqz v4, :cond_5

    invoke-virtual {v0}, Landroid/app/admin/DevicePolicyManager;->getDeviceOwnerUserId()I

    move-result v0

    :try_start_0
    invoke-virtual {v4}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4, v0}, Landroid/content/pm/PackageManager;->getPackageUidAsUser(Ljava/lang/String;I)I

    move-result v0

    iget v3, p1, Landroid/net/wifi/WifiConfiguration;->creatorUid:I
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    if-ne v0, v3, :cond_2

    move v0, v1

    :goto_0
    if-nez v0, :cond_3

    return v1

    :cond_2
    move v0, v2

    goto :goto_0

    :catch_0
    move-exception v0

    move v0, v2

    goto :goto_0

    :cond_3
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v3, "wifi_device_owner_configs_lockdown"

    invoke-static {v0, v3, v2}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    if-eqz v0, :cond_4

    :goto_1
    xor-int/lit8 v0, v1, 0x1

    return v0

    :cond_4
    move v1, v2

    goto :goto_1

    :cond_5
    move v0, v2

    goto :goto_0
.end method

.method private ahk()V
    .locals 0

    return-void
.end method

.method private ahl()V
    .locals 5

    const/4 v0, 0x1

    const/4 v1, 0x0

    const v2, 0x7f121531

    invoke-virtual {p0, v2}, Lcom/android/settings/wifi/WifiSettings;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {p0}, Lcom/android/settings/wifi/WifiSettings;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string/jumbo v4, "wifi_scan_always_enabled"

    invoke-static {v3, v4, v1}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v3

    if-ne v3, v0, :cond_0

    :goto_0
    if-eqz v0, :cond_1

    const v0, 0x7f12159f

    invoke-virtual {p0, v0}, Lcom/android/settings/wifi/WifiSettings;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    :goto_1
    new-instance v1, Lcom/android/settings/wifi/bb;

    invoke-direct {v1, p0}, Lcom/android/settings/wifi/bb;-><init>(Lcom/android/settings/wifi/WifiSettings;)V

    iget-object v3, p0, Lcom/android/settings/wifi/WifiSettings;->ari:Lcom/android/settings/wifi/LinkablePreference;

    invoke-virtual {v3, v2, v0, v1}, Lcom/android/settings/wifi/LinkablePreference;->acQ(Ljava/lang/CharSequence;Ljava/lang/CharSequence;Lcom/android/settings/X;)V

    return-void

    :cond_0
    move v0, v1

    goto :goto_0

    :cond_1
    const v0, 0x7f1215a2

    invoke-virtual {p0, v0}, Lcom/android/settings/wifi/WifiSettings;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    goto :goto_1
.end method

.method private ahn()V
    .locals 3

    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/android/settings/wifi/WifiSettings;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v1, 0x7f12149a

    invoke-virtual {p0, v1}, Lcom/android/settings/wifi/WifiSettings;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    new-instance v1, Lcom/android/settings/wifi/bd;

    invoke-direct {v1, p0}, Lcom/android/settings/wifi/bd;-><init>(Lcom/android/settings/wifi/WifiSettings;)V

    const v2, 0x7f1214a2

    invoke-virtual {v0, v2, v1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    new-instance v1, Lcom/android/settings/wifi/be;

    invoke-direct {v1, p0}, Lcom/android/settings/wifi/be;-><init>(Lcom/android/settings/wifi/WifiSettings;)V

    const v2, 0x7f121499

    invoke-virtual {v0, v2, v1}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    return-void
.end method

.method private aho()V
    .locals 4

    invoke-virtual {p0}, Lcom/android/settings/wifi/WifiSettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/android/settings/wifi/WifiSettings;->ard:Z

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/wifi/WifiSettings;->ark:Landroid/net/wifi/WifiManager;

    invoke-virtual {v0}, Landroid/net/wifi/WifiManager;->isWifiEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/android/settings/wifi/WifiSettings;->ahm(Z)V

    invoke-virtual {p0}, Lcom/android/settings/wifi/WifiSettings;->getView()Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/wifi/WifiSettings;->arj:Ljava/lang/Runnable;

    const-wide/16 v2, 0x12c

    invoke-virtual {v0, v1, v2, v3}, Landroid/view/View;->postDelayed(Ljava/lang/Runnable;J)Z

    :cond_0
    return-void
.end method

.method static synthetic ahp()V
    .locals 0

    return-void
.end method


# virtual methods
.method public Lx(I)I
    .locals 1

    packed-switch p1, :pswitch_data_0

    :pswitch_0
    const/4 v0, 0x0

    return v0

    :pswitch_1
    const/16 v0, 0x25b

    return v0

    :pswitch_2
    const/16 v0, 0x25c

    return v0

    :pswitch_3
    const/16 v0, 0x25d

    return v0

    :pswitch_4
    const/16 v0, 0x25e

    return v0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_4
    .end packed-switch
.end method

.method protected aaT(Z)V
    .locals 1

    iget-boolean v0, p0, Lcom/android/settings/wifi/WifiSettings;->arb:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/android/settings/wifi/WifiSettings;->bWT()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/android/settings/wifi/WifiSettings;->getNextButton()Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setEnabled(Z)V

    :cond_0
    return-void
.end method

.method public aaX(Lcom/android/settingslib/wifi/i;)V
    .locals 2

    const-string/jumbo v0, "WifiSettings"

    const-string/jumbo v1, "onAccessPointChanged (singular) callback initiated"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lcom/android/settings/wifi/WifiSettings;->getView()Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    new-instance v1, Lcom/android/settings/wifi/bc;

    invoke-direct {v1, p0, p1}, Lcom/android/settings/wifi/bc;-><init>(Lcom/android/settings/wifi/WifiSettings;Lcom/android/settingslib/wifi/i;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->post(Ljava/lang/Runnable;)Z

    :cond_0
    return-void
.end method

.method public aaY()V
    .locals 2

    const-string/jumbo v0, "WifiSettings"

    const-string/jumbo v1, "onAccessPointsChanged (WifiTracker) callback initiated"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/android/settings/wifi/WifiSettings;->aho()V

    return-void
.end method

.method public aaZ()V
    .locals 1

    invoke-direct {p0}, Lcom/android/settings/wifi/WifiSettings;->aho()V

    iget-object v0, p0, Lcom/android/settings/wifi/WifiSettings;->aqL:Lcom/android/settingslib/wifi/a;

    invoke-virtual {v0}, Lcom/android/settingslib/wifi/a;->cgD()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/android/settings/wifi/WifiSettings;->aaT(Z)V

    return-void
.end method

.method public aba(Lcom/android/settingslib/wifi/i;)V
    .locals 1

    invoke-virtual {p1}, Lcom/android/settingslib/wifi/i;->cio()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settings/wifi/ac;

    invoke-virtual {v0}, Lcom/android/settings/wifi/ac;->aey()V

    return-void
.end method

.method public abb(I)V
    .locals 1

    iget-boolean v0, p0, Lcom/android/settings/wifi/WifiSettings;->ard:Z

    if-eqz v0, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/settings/wifi/WifiSettings;->ark:Landroid/net/wifi/WifiManager;

    invoke-virtual {v0}, Landroid/net/wifi/WifiManager;->getWifiState()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :goto_0
    :pswitch_0
    return-void

    :pswitch_1
    invoke-direct {p0}, Lcom/android/settings/wifi/WifiSettings;->ahk()V

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/android/settings/wifi/WifiSettings;->ahm(Z)V

    goto :goto_0

    :pswitch_2
    invoke-direct {p0}, Lcom/android/settings/wifi/WifiSettings;->ahk()V

    goto :goto_0

    :pswitch_3
    invoke-direct {p0}, Lcom/android/settings/wifi/WifiSettings;->ahl()V

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/android/settings/wifi/WifiSettings;->ahm(Z)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_3
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method abg(Lcom/android/settings/wifi/A;)V
    .locals 4

    const/16 v3, 0xb

    invoke-virtual {p1}, Lcom/android/settings/wifi/A;->afa()Landroid/net/wifi/WifiConfiguration;

    move-result-object v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/android/settings/wifi/WifiSettings;->arh:Lcom/android/settingslib/wifi/i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/wifi/WifiSettings;->arh:Lcom/android/settingslib/wifi/i;

    invoke-virtual {v0}, Lcom/android/settingslib/wifi/i;->cii()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/wifi/WifiSettings;->arh:Lcom/android/settingslib/wifi/i;

    invoke-virtual {v0}, Lcom/android/settingslib/wifi/i;->chL()Landroid/net/wifi/WifiConfiguration;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/android/settings/wifi/WifiSettings;->ahh(Landroid/net/wifi/WifiConfiguration;Z)V

    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/android/settings/wifi/WifiSettings;->aqL:Lcom/android/settingslib/wifi/a;

    invoke-virtual {v0}, Lcom/android/settingslib/wifi/a;->cgI()V

    return-void

    :cond_1
    invoke-virtual {p1}, Lcom/android/settings/wifi/A;->afe()I

    move-result v1

    const/4 v2, 0x2

    if-ne v1, v2, :cond_4

    invoke-virtual {p1}, Lcom/android/settings/wifi/A;->aeX()Z

    move-result v1

    if-nez v1, :cond_3

    invoke-virtual {p1}, Lcom/android/settings/wifi/A;->afc()I

    move-result v0

    if-ne v0, v3, :cond_2

    invoke-direct {p0}, Lcom/android/settings/wifi/WifiSettings;->ahn()V

    :cond_2
    return-void

    :cond_3
    iget-object v1, p0, Lcom/android/settings/wifi/WifiSettings;->ark:Landroid/net/wifi/WifiManager;

    iget-object v2, p0, Lcom/android/settings/wifi/WifiSettings;->aqN:Landroid/net/wifi/WifiManager$ActionListener;

    invoke-virtual {v1, v0, v2}, Landroid/net/wifi/WifiManager;->save(Landroid/net/wifi/WifiConfiguration;Landroid/net/wifi/WifiManager$ActionListener;)V

    goto :goto_0

    :cond_4
    invoke-virtual {p1}, Lcom/android/settings/wifi/A;->aeX()Z

    move-result v1

    if-nez v1, :cond_6

    invoke-virtual {p1}, Lcom/android/settings/wifi/A;->afc()I

    move-result v0

    if-ne v0, v3, :cond_5

    invoke-direct {p0}, Lcom/android/settings/wifi/WifiSettings;->ahn()V

    :cond_5
    return-void

    :cond_6
    iget-object v1, p0, Lcom/android/settings/wifi/WifiSettings;->ark:Landroid/net/wifi/WifiManager;

    iget-object v2, p0, Lcom/android/settings/wifi/WifiSettings;->aqN:Landroid/net/wifi/WifiManager$ActionListener;

    invoke-virtual {v1, v0, v2}, Landroid/net/wifi/WifiManager;->save(Landroid/net/wifi/WifiConfiguration;Landroid/net/wifi/WifiManager$ActionListener;)V

    iget-object v1, p0, Lcom/android/settings/wifi/WifiSettings;->arh:Lcom/android/settingslib/wifi/i;

    if-eqz v1, :cond_0

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/android/settings/wifi/WifiSettings;->ahh(Landroid/net/wifi/WifiConfiguration;Z)V

    goto :goto_0
.end method

.method public adG(Lcom/android/settings/wifi/s;)V
    .locals 0

    invoke-virtual {p0}, Lcom/android/settings/wifi/WifiSettings;->ahi()V

    return-void
.end method

.method public adH(Lcom/android/settings/wifi/s;)V
    .locals 1

    iget-object v0, p0, Lcom/android/settings/wifi/WifiSettings;->aqY:Lcom/android/settings/wifi/s;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/wifi/WifiSettings;->aqY:Lcom/android/settings/wifi/s;

    invoke-virtual {v0}, Lcom/android/settings/wifi/s;->adw()Lcom/android/settings/wifi/A;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/settings/wifi/WifiSettings;->abg(Lcom/android/settings/wifi/A;)V

    :cond_0
    return-void
.end method

.method protected ahe(Lcom/android/settingslib/wifi/i;I)V
    .locals 4

    const/4 v3, 0x1

    const/4 v2, 0x0

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/android/settingslib/wifi/i;->chL()Landroid/net/wifi/WifiConfiguration;

    move-result-object v0

    invoke-virtual {p0}, Lcom/android/settings/wifi/WifiSettings;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/android/settings/wifi/WifiSettings;->ahf(Landroid/content/Context;Landroid/net/wifi/WifiConfiguration;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/android/settingslib/wifi/i;->chM()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/android/settings/wifi/WifiSettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {p0}, Lcom/android/settings/wifi/WifiSettings;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-static {v1}, Lcom/android/settingslib/w;->cqN(Landroid/content/Context;)Lcom/android/settingslib/n;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/settingslib/w;->cqW(Landroid/content/Context;Lcom/android/settingslib/n;)V

    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/settings/wifi/WifiSettings;->aqY:Lcom/android/settings/wifi/s;

    if-eqz v0, :cond_1

    invoke-virtual {p0, v3}, Lcom/android/settings/wifi/WifiSettings;->bWO(I)V

    iput-object v2, p0, Lcom/android/settings/wifi/WifiSettings;->aqY:Lcom/android/settings/wifi/s;

    :cond_1
    iput-object p1, p0, Lcom/android/settings/wifi/WifiSettings;->ara:Lcom/android/settingslib/wifi/i;

    iput p2, p0, Lcom/android/settings/wifi/WifiSettings;->aqZ:I

    invoke-virtual {p0, v3}, Lcom/android/settings/wifi/WifiSettings;->alW(I)V

    return-void
.end method

.method protected ahh(Landroid/net/wifi/WifiConfiguration;Z)V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/wifi/WifiSettings;->ark:Landroid/net/wifi/WifiManager;

    iget-object v1, p0, Lcom/android/settings/wifi/WifiSettings;->aqM:Landroid/net/wifi/WifiManager$ActionListener;

    invoke-virtual {v0, p1, v1}, Landroid/net/wifi/WifiManager;->connect(Landroid/net/wifi/WifiConfiguration;Landroid/net/wifi/WifiManager$ActionListener;)V

    return-void
.end method

.method ahi()V
    .locals 3

    iget-object v0, p0, Lcom/android/settings/wifi/WifiSettings;->arh:Lcom/android/settingslib/wifi/i;

    invoke-virtual {v0}, Lcom/android/settingslib/wifi/i;->cii()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/android/settings/wifi/WifiSettings;->arh:Lcom/android/settingslib/wifi/i;

    invoke-virtual {v0}, Lcom/android/settingslib/wifi/i;->cho()Landroid/net/NetworkInfo;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/wifi/WifiSettings;->arh:Lcom/android/settingslib/wifi/i;

    invoke-virtual {v0}, Lcom/android/settingslib/wifi/i;->cho()Landroid/net/NetworkInfo;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->getState()Landroid/net/NetworkInfo$State;

    move-result-object v0

    sget-object v1, Landroid/net/NetworkInfo$State;->DISCONNECTED:Landroid/net/NetworkInfo$State;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/android/settings/wifi/WifiSettings;->ark:Landroid/net/wifi/WifiManager;

    iget-object v1, p0, Lcom/android/settings/wifi/WifiSettings;->arh:Lcom/android/settingslib/wifi/i;

    invoke-virtual {v1}, Lcom/android/settingslib/wifi/i;->cih()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/android/settingslib/wifi/i;->chG(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/net/wifi/WifiManager;->disableEphemeralNetwork(Ljava/lang/String;)V

    :goto_0
    iget-object v0, p0, Lcom/android/settings/wifi/WifiSettings;->aqL:Lcom/android/settingslib/wifi/a;

    invoke-virtual {v0}, Lcom/android/settingslib/wifi/a;->cgI()V

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/android/settings/wifi/WifiSettings;->aaT(Z)V

    return-void

    :cond_0
    const-string/jumbo v0, "WifiSettings"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Failed to forget invalid network "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/settings/wifi/WifiSettings;->arh:Lcom/android/settingslib/wifi/i;

    invoke-virtual {v2}, Lcom/android/settingslib/wifi/i;->chL()Landroid/net/wifi/WifiConfiguration;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/settings/wifi/WifiSettings;->arh:Lcom/android/settingslib/wifi/i;

    invoke-virtual {v0}, Lcom/android/settingslib/wifi/i;->chL()Landroid/net/wifi/WifiConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/wifi/WifiConfiguration;->isPasspoint()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/settings/wifi/WifiSettings;->ark:Landroid/net/wifi/WifiManager;

    iget-object v1, p0, Lcom/android/settings/wifi/WifiSettings;->arh:Lcom/android/settingslib/wifi/i;

    invoke-virtual {v1}, Lcom/android/settingslib/wifi/i;->chL()Landroid/net/wifi/WifiConfiguration;

    move-result-object v1

    iget-object v1, v1, Landroid/net/wifi/WifiConfiguration;->FQDN:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/net/wifi/WifiManager;->removePasspointConfiguration(Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/android/settings/wifi/WifiSettings;->ark:Landroid/net/wifi/WifiManager;

    iget-object v1, p0, Lcom/android/settings/wifi/WifiSettings;->arh:Lcom/android/settingslib/wifi/i;

    invoke-virtual {v1}, Lcom/android/settingslib/wifi/i;->chL()Landroid/net/wifi/WifiConfiguration;

    move-result-object v1

    iget v1, v1, Landroid/net/wifi/WifiConfiguration;->networkId:I

    iget-object v2, p0, Lcom/android/settings/wifi/WifiSettings;->aqO:Landroid/net/wifi/WifiManager$ActionListener;

    invoke-virtual {v0, v1, v2}, Landroid/net/wifi/WifiManager;->forget(ILandroid/net/wifi/WifiManager$ActionListener;)V

    goto :goto_0
.end method

.method ahj()V
    .locals 2

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/android/settings/wifi/WifiSettings;->arh:Lcom/android/settingslib/wifi/i;

    const/4 v0, 0x1

    invoke-virtual {p0, v1, v0}, Lcom/android/settings/wifi/WifiSettings;->ahe(Lcom/android/settingslib/wifi/i;I)V

    return-void
.end method

.method protected ahm(Z)V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/wifi/WifiSettings;->arf:Landroid/widget/ProgressBar;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/android/settings/wifi/WifiSettings;->arf:Landroid/widget/ProgressBar;

    if-eqz p1, :cond_1

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/ProgressBar;->setVisibility(I)V

    :cond_0
    return-void

    :cond_1
    const/4 v0, 0x4

    goto :goto_0
.end method

.method synthetic ahq()V
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/android/settings/wifi/WifiSettings;->ahm(Z)V

    return-void
.end method

.method protected aq()I
    .locals 1

    const v0, 0x7f120829

    return v0
.end method

.method public at(Landroid/preference/Preference;)Z
    .locals 4

    const/4 v3, 0x0

    const/4 v2, 0x1

    const/4 v1, 0x0

    invoke-virtual {p1}, Landroid/preference/Preference;->getFragment()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1, v1}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    invoke-super {p0, p1}, Lcom/android/settings/MiuiRestrictedSettingsFragment;->at(Landroid/preference/Preference;)Z

    move-result v0

    return v0

    :cond_0
    instance-of v0, p1, Lcom/android/settings/wifi/ac;

    if-eqz v0, :cond_6

    move-object v0, p1

    check-cast v0, Lcom/android/settings/wifi/ac;

    invoke-virtual {v0}, Lcom/android/settings/wifi/ac;->aeu()Lcom/android/settingslib/wifi/i;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/wifi/WifiSettings;->arh:Lcom/android/settingslib/wifi/i;

    iget-object v0, p0, Lcom/android/settings/wifi/WifiSettings;->arh:Lcom/android/settingslib/wifi/i;

    if-nez v0, :cond_1

    return v3

    :cond_1
    iget-object v0, p0, Lcom/android/settings/wifi/WifiSettings;->arh:Lcom/android/settingslib/wifi/i;

    invoke-virtual {v0}, Lcom/android/settingslib/wifi/i;->chM()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-super {p0, p1}, Lcom/android/settings/MiuiRestrictedSettingsFragment;->at(Landroid/preference/Preference;)Z

    move-result v0

    return v0

    :cond_2
    iget-object v0, p0, Lcom/android/settings/wifi/WifiSettings;->arh:Lcom/android/settingslib/wifi/i;

    invoke-virtual {v0}, Lcom/android/settingslib/wifi/i;->chL()Landroid/net/wifi/WifiConfiguration;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/wifi/WifiSettings;->arh:Lcom/android/settingslib/wifi/i;

    invoke-virtual {v1}, Lcom/android/settingslib/wifi/i;->chP()I

    move-result v1

    if-nez v1, :cond_3

    iget-object v0, p0, Lcom/android/settings/wifi/WifiSettings;->arh:Lcom/android/settingslib/wifi/i;

    invoke-virtual {v0}, Lcom/android/settingslib/wifi/i;->chm()V

    iget-object v0, p0, Lcom/android/settings/wifi/WifiSettings;->arh:Lcom/android/settingslib/wifi/i;

    invoke-virtual {v0}, Lcom/android/settingslib/wifi/i;->chL()Landroid/net/wifi/WifiConfiguration;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/wifi/WifiSettings;->arh:Lcom/android/settingslib/wifi/i;

    invoke-virtual {v1}, Lcom/android/settingslib/wifi/i;->cii()Z

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/android/settings/wifi/WifiSettings;->ahh(Landroid/net/wifi/WifiConfiguration;Z)V

    :goto_0
    return v2

    :cond_3
    iget-object v1, p0, Lcom/android/settings/wifi/WifiSettings;->arh:Lcom/android/settingslib/wifi/i;

    invoke-virtual {v1}, Lcom/android/settingslib/wifi/i;->cii()Z

    move-result v1

    if-eqz v1, :cond_4

    if-eqz v0, :cond_4

    invoke-virtual {v0}, Landroid/net/wifi/WifiConfiguration;->getNetworkSelectionStatus()Landroid/net/wifi/WifiConfiguration$NetworkSelectionStatus;

    move-result-object v1

    if-eqz v1, :cond_4

    invoke-virtual {v0}, Landroid/net/wifi/WifiConfiguration;->getNetworkSelectionStatus()Landroid/net/wifi/WifiConfiguration$NetworkSelectionStatus;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/wifi/WifiConfiguration$NetworkSelectionStatus;->getHasEverConnected()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-virtual {p0, v0, v2}, Lcom/android/settings/wifi/WifiSettings;->ahh(Landroid/net/wifi/WifiConfiguration;Z)V

    goto :goto_0

    :cond_4
    iget-object v1, p0, Lcom/android/settings/wifi/WifiSettings;->arh:Lcom/android/settingslib/wifi/i;

    invoke-virtual {v1}, Lcom/android/settingslib/wifi/i;->chO()Z

    move-result v1

    if-eqz v1, :cond_5

    invoke-virtual {p0, v0, v2}, Lcom/android/settings/wifi/WifiSettings;->ahh(Landroid/net/wifi/WifiConfiguration;Z)V

    goto :goto_0

    :cond_5
    iget-object v0, p0, Lcom/android/settings/wifi/WifiSettings;->arh:Lcom/android/settingslib/wifi/i;

    invoke-virtual {p0, v0, v2}, Lcom/android/settings/wifi/WifiSettings;->ahe(Lcom/android/settingslib/wifi/i;I)V

    goto :goto_0

    :cond_6
    iget-object v0, p0, Lcom/android/settings/wifi/WifiSettings;->aqT:Landroid/preference/Preference;

    if-ne p1, v0, :cond_7

    invoke-virtual {p0}, Lcom/android/settings/wifi/WifiSettings;->ahj()V

    goto :goto_0

    :cond_7
    invoke-super {p0, p1}, Lcom/android/settings/MiuiRestrictedSettingsFragment;->at(Landroid/preference/Preference;)Z

    move-result v0

    return v0
.end method

.method public cC()Ljava/lang/String;
    .locals 1

    const-class v0, Lcom/android/settings/wifi/WifiSettings;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getMetricsCategory()I
    .locals 1

    const/16 v0, 0x67

    return v0
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 6

    const/4 v5, 0x0

    const/4 v3, 0x1

    invoke-super {p0, p1}, Lcom/android/settings/MiuiRestrictedSettingsFragment;->onActivityCreated(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/android/settings/wifi/WifiSettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/wifi/WifiSettings;->aqV:Landroid/os/HandlerThread;

    invoke-virtual {v1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v2

    move-object v1, p0

    move v4, v3

    invoke-static/range {v0 .. v5}, Lcom/android/settingslib/wifi/l;->cir(Landroid/content/Context;Lcom/android/settingslib/wifi/e;Landroid/os/Looper;ZZZ)Lcom/android/settingslib/wifi/a;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/wifi/WifiSettings;->aqL:Lcom/android/settingslib/wifi/a;

    iget-object v0, p0, Lcom/android/settings/wifi/WifiSettings;->aqL:Lcom/android/settingslib/wifi/a;

    invoke-virtual {v0}, Lcom/android/settingslib/wifi/a;->cgq()Landroid/net/wifi/WifiManager;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/wifi/WifiSettings;->ark:Landroid/net/wifi/WifiManager;

    new-instance v0, Lcom/android/settings/wifi/aY;

    invoke-direct {v0, p0}, Lcom/android/settings/wifi/aY;-><init>(Lcom/android/settings/wifi/WifiSettings;)V

    iput-object v0, p0, Lcom/android/settings/wifi/WifiSettings;->aqM:Landroid/net/wifi/WifiManager$ActionListener;

    new-instance v0, Lcom/android/settings/wifi/aZ;

    invoke-direct {v0, p0}, Lcom/android/settings/wifi/aZ;-><init>(Lcom/android/settings/wifi/WifiSettings;)V

    iput-object v0, p0, Lcom/android/settings/wifi/WifiSettings;->aqN:Landroid/net/wifi/WifiManager$ActionListener;

    new-instance v0, Lcom/android/settings/wifi/ba;

    invoke-direct {v0, p0}, Lcom/android/settings/wifi/ba;-><init>(Lcom/android/settings/wifi/WifiSettings;)V

    iput-object v0, p0, Lcom/android/settings/wifi/WifiSettings;->aqO:Landroid/net/wifi/WifiManager$ActionListener;

    if-eqz p1, :cond_1

    const-string/jumbo v0, "dialog_mode"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/android/settings/wifi/WifiSettings;->aqZ:I

    const-string/jumbo v0, "wifi_ap_state"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string/jumbo v0, "wifi_ap_state"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/wifi/WifiSettings;->aqR:Landroid/os/Bundle;

    :cond_0
    const-string/jumbo v0, "wifi_nfc_dlg_state"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string/jumbo v0, "wifi_nfc_dlg_state"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/wifi/WifiSettings;->arl:Landroid/os/Bundle;

    :cond_1
    invoke-virtual {p0}, Lcom/android/settings/wifi/WifiSettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string/jumbo v0, "wifi_enable_next_on_connect"

    invoke-virtual {v1, v0, v5}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/settings/wifi/WifiSettings;->arb:Z

    iget-boolean v0, p0, Lcom/android/settings/wifi/WifiSettings;->arb:Z

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lcom/android/settings/wifi/WifiSettings;->bWT()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lcom/android/settings/wifi/WifiSettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const-string/jumbo v2, "connectivity"

    invoke-virtual {v0, v2}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    if-eqz v0, :cond_2

    invoke-virtual {v0, v3}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/android/settings/wifi/WifiSettings;->aaT(Z)V

    :cond_2
    invoke-virtual {p0}, Lcom/android/settings/wifi/WifiSettings;->getListView()Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/settings/wifi/WifiSettings;->registerForContextMenu(Landroid/view/View;)V

    invoke-virtual {p0, v3}, Lcom/android/settings/wifi/WifiSettings;->setHasOptionsMenu(Z)V

    const-string/jumbo v0, "wifi_start_connect_ssid"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    const-string/jumbo v0, "wifi_start_connect_ssid"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/wifi/WifiSettings;->are:Ljava/lang/String;

    :cond_3
    return-void
.end method

.method public onContextItemSelected(Landroid/view/MenuItem;)Z
    .locals 3

    const/4 v2, 0x1

    iget-object v0, p0, Lcom/android/settings/wifi/WifiSettings;->arh:Lcom/android/settingslib/wifi/i;

    if-nez v0, :cond_0

    invoke-super {p0, p1}, Lcom/android/settings/MiuiRestrictedSettingsFragment;->onContextItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    return v0

    :cond_0
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    invoke-super {p0, p1}, Lcom/android/settings/MiuiRestrictedSettingsFragment;->onContextItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    return v0

    :pswitch_0
    iget-object v0, p0, Lcom/android/settings/wifi/WifiSettings;->arh:Lcom/android/settingslib/wifi/i;

    invoke-virtual {v0}, Lcom/android/settingslib/wifi/i;->cii()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/android/settings/wifi/WifiSettings;->arh:Lcom/android/settingslib/wifi/i;

    invoke-virtual {v1}, Lcom/android/settingslib/wifi/i;->chL()Landroid/net/wifi/WifiConfiguration;

    move-result-object v1

    invoke-virtual {p0, v1, v0}, Lcom/android/settings/wifi/WifiSettings;->ahh(Landroid/net/wifi/WifiConfiguration;Z)V

    :goto_0
    return v2

    :cond_1
    iget-object v1, p0, Lcom/android/settings/wifi/WifiSettings;->arh:Lcom/android/settingslib/wifi/i;

    invoke-virtual {v1}, Lcom/android/settingslib/wifi/i;->chP()I

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/android/settings/wifi/WifiSettings;->arh:Lcom/android/settingslib/wifi/i;

    invoke-virtual {v1}, Lcom/android/settingslib/wifi/i;->chm()V

    iget-object v1, p0, Lcom/android/settings/wifi/WifiSettings;->arh:Lcom/android/settingslib/wifi/i;

    invoke-virtual {v1}, Lcom/android/settingslib/wifi/i;->chL()Landroid/net/wifi/WifiConfiguration;

    move-result-object v1

    invoke-virtual {p0, v1, v0}, Lcom/android/settings/wifi/WifiSettings;->ahh(Landroid/net/wifi/WifiConfiguration;Z)V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/android/settings/wifi/WifiSettings;->arh:Lcom/android/settingslib/wifi/i;

    invoke-virtual {p0, v0, v2}, Lcom/android/settings/wifi/WifiSettings;->ahe(Lcom/android/settingslib/wifi/i;I)V

    goto :goto_0

    :pswitch_1
    invoke-virtual {p0}, Lcom/android/settings/wifi/WifiSettings;->ahi()V

    return v2

    :pswitch_2
    iget-object v0, p0, Lcom/android/settings/wifi/WifiSettings;->arh:Lcom/android/settingslib/wifi/i;

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/android/settings/wifi/WifiSettings;->ahe(Lcom/android/settingslib/wifi/i;I)V

    return v2

    :pswitch_3
    const/4 v0, 0x6

    invoke-virtual {p0, v0}, Lcom/android/settings/wifi/WifiSettings;->alW(I)V

    return v2

    nop

    :pswitch_data_0
    .packed-switch 0x7
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3

    const/4 v2, 0x0

    invoke-super {p0, p1}, Lcom/android/settings/MiuiRestrictedSettingsFragment;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/android/settings/wifi/WifiSettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "wifi_setup_wizard"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/settings/wifi/WifiSettings;->aqH:Z

    invoke-virtual {p0}, Lcom/android/settings/wifi/WifiSettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "wifi_settings_keyguard"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/settings/wifi/WifiSettings;->aqI:Z

    invoke-virtual {p0}, Lcom/android/settings/wifi/WifiSettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "from_phone_activation"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/settings/wifi/WifiSettings;->aqJ:Z

    const-string/jumbo v0, "connected_access_point"

    invoke-virtual {p0, v0}, Lcom/android/settings/wifi/WifiSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceCategory;

    iput-object v0, p0, Lcom/android/settings/wifi/WifiSettings;->aqX:Landroid/preference/PreferenceCategory;

    const-string/jumbo v0, "access_points"

    invoke-virtual {p0, v0}, Lcom/android/settings/wifi/WifiSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceCategory;

    iput-object v0, p0, Lcom/android/settings/wifi/WifiSettings;->aqS:Landroid/preference/PreferenceCategory;

    const-string/jumbo v0, "additional_settings"

    invoke-virtual {p0, v0}, Lcom/android/settings/wifi/WifiSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceCategory;

    iput-object v0, p0, Lcom/android/settings/wifi/WifiSettings;->aqU:Landroid/preference/PreferenceCategory;

    const-string/jumbo v0, "configure_settings"

    invoke-virtual {p0, v0}, Lcom/android/settings/wifi/WifiSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/wifi/WifiSettings;->aqW:Landroid/preference/Preference;

    const-string/jumbo v0, "saved_networks"

    invoke-virtual {p0, v0}, Lcom/android/settings/wifi/WifiSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/wifi/WifiSettings;->arg:Landroid/preference/Preference;

    invoke-virtual {p0}, Lcom/android/settings/wifi/WifiSettings;->bWz()Landroid/content/Context;

    move-result-object v0

    new-instance v1, Landroid/preference/Preference;

    invoke-direct {v1, v0}, Landroid/preference/Preference;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/android/settings/wifi/WifiSettings;->aqT:Landroid/preference/Preference;

    iget-object v1, p0, Lcom/android/settings/wifi/WifiSettings;->aqT:Landroid/preference/Preference;

    const v2, 0x7f080203

    invoke-virtual {v1, v2}, Landroid/preference/Preference;->setIcon(I)V

    iget-object v1, p0, Lcom/android/settings/wifi/WifiSettings;->aqT:Landroid/preference/Preference;

    const v2, 0x7f1214c2

    invoke-virtual {v1, v2}, Landroid/preference/Preference;->setTitle(I)V

    new-instance v1, Lcom/android/settings/wifi/LinkablePreference;

    invoke-direct {v1, v0}, Lcom/android/settings/wifi/LinkablePreference;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/android/settings/wifi/WifiSettings;->ari:Lcom/android/settings/wifi/LinkablePreference;

    new-instance v0, Lcom/android/settings/wifi/w;

    invoke-virtual {p0}, Lcom/android/settings/wifi/WifiSettings;->bWA()Landroid/content/pm/PackageManager;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/android/settings/wifi/w;-><init>(Landroid/content/pm/PackageManager;)V

    iput-object v0, p0, Lcom/android/settings/wifi/WifiSettings;->aqP:Lcom/android/settings/wifi/w;

    invoke-virtual {p0}, Lcom/android/settings/wifi/WifiSettings;->bXB()Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/settings/wifi/WifiSettings;->ard:Z

    new-instance v0, Landroid/os/HandlerThread;

    const-string/jumbo v1, "WifiSettings"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;I)V

    iput-object v0, p0, Lcom/android/settings/wifi/WifiSettings;->aqV:Landroid/os/HandlerThread;

    iget-object v0, p0, Lcom/android/settings/wifi/WifiSettings;->aqV:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    return-void
.end method

.method public onCreateContextMenu(Landroid/view/ContextMenu;Landroid/view/View;Landroid/view/ContextMenu$ContextMenuInfo;)V
    .locals 3

    const/4 v2, 0x0

    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/preference/Preference;

    instance-of v1, v0, Lcom/android/settings/wifi/ac;

    if-eqz v1, :cond_4

    check-cast v0, Lcom/android/settings/wifi/ac;

    invoke-virtual {v0}, Lcom/android/settings/wifi/ac;->aeu()Lcom/android/settingslib/wifi/i;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/wifi/WifiSettings;->arh:Lcom/android/settingslib/wifi/i;

    iget-object v0, p0, Lcom/android/settings/wifi/WifiSettings;->arh:Lcom/android/settingslib/wifi/i;

    invoke-virtual {v0}, Lcom/android/settingslib/wifi/i;->chW()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {p1, v0}, Landroid/view/ContextMenu;->setHeaderTitle(Ljava/lang/CharSequence;)Landroid/view/ContextMenu;

    iget-object v0, p0, Lcom/android/settings/wifi/WifiSettings;->arh:Lcom/android/settingslib/wifi/i;

    invoke-virtual {v0}, Lcom/android/settingslib/wifi/i;->chs()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x7

    const v1, 0x7f121558

    invoke-interface {p1, v2, v0, v2, v1}, Landroid/view/ContextMenu;->add(IIII)Landroid/view/MenuItem;

    :cond_0
    iget-object v0, p0, Lcom/android/settings/wifi/WifiSettings;->arh:Lcom/android/settingslib/wifi/i;

    invoke-virtual {v0}, Lcom/android/settingslib/wifi/i;->chL()Landroid/net/wifi/WifiConfiguration;

    move-result-object v0

    invoke-virtual {p0}, Lcom/android/settings/wifi/WifiSettings;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/android/settings/wifi/WifiSettings;->ahf(Landroid/content/Context;Landroid/net/wifi/WifiConfiguration;)Z

    move-result v0

    if-eqz v0, :cond_1

    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/settings/wifi/WifiSettings;->arh:Lcom/android/settingslib/wifi/i;

    invoke-virtual {v0}, Lcom/android/settingslib/wifi/i;->cii()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/android/settings/wifi/WifiSettings;->arh:Lcom/android/settingslib/wifi/i;

    invoke-virtual {v0}, Lcom/android/settingslib/wifi/i;->cib()Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_2
    const/16 v0, 0x8

    const v1, 0x7f121559

    invoke-interface {p1, v2, v0, v2, v1}, Landroid/view/ContextMenu;->add(IIII)Landroid/view/MenuItem;

    :cond_3
    iget-object v0, p0, Lcom/android/settings/wifi/WifiSettings;->arh:Lcom/android/settingslib/wifi/i;

    invoke-virtual {v0}, Lcom/android/settingslib/wifi/i;->cii()Z

    move-result v0

    if-eqz v0, :cond_4

    const/16 v0, 0x9

    const v1, 0x7f12155a

    invoke-interface {p1, v2, v0, v2, v1}, Landroid/view/ContextMenu;->add(IIII)Landroid/view/MenuItem;

    invoke-virtual {p0}, Lcom/android/settings/wifi/WifiSettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Landroid/nfc/NfcAdapter;->getDefaultAdapter(Landroid/content/Context;)Landroid/nfc/NfcAdapter;

    move-result-object v0

    if-eqz v0, :cond_4

    invoke-virtual {v0}, Landroid/nfc/NfcAdapter;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/android/settings/wifi/WifiSettings;->arh:Lcom/android/settingslib/wifi/i;

    invoke-virtual {v0}, Lcom/android/settingslib/wifi/i;->chP()I

    move-result v0

    if-eqz v0, :cond_4

    const/16 v0, 0xa

    const v1, 0x7f121562

    invoke-interface {p1, v2, v0, v2, v1}, Landroid/view/ContextMenu;->add(IIII)Landroid/view/MenuItem;

    :cond_4
    return-void
.end method

.method public onCreateDialog(I)Landroid/app/Dialog;
    .locals 6

    const/4 v5, 0x0

    const/4 v2, 0x0

    packed-switch p1, :pswitch_data_0

    :pswitch_0
    invoke-super {p0, p1}, Lcom/android/settings/MiuiRestrictedSettingsFragment;->onCreateDialog(I)Landroid/app/Dialog;

    move-result-object v0

    return-object v0

    :pswitch_1
    iget-object v3, p0, Lcom/android/settings/wifi/WifiSettings;->ara:Lcom/android/settingslib/wifi/i;

    if-nez v3, :cond_0

    iget-object v0, p0, Lcom/android/settings/wifi/WifiSettings;->aqR:Landroid/os/Bundle;

    if-eqz v0, :cond_0

    new-instance v3, Lcom/android/settingslib/wifi/i;

    invoke-virtual {p0}, Lcom/android/settings/wifi/WifiSettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/wifi/WifiSettings;->aqR:Landroid/os/Bundle;

    invoke-direct {v3, v0, v1}, Lcom/android/settingslib/wifi/i;-><init>(Landroid/content/Context;Landroid/os/Bundle;)V

    iput-object v3, p0, Lcom/android/settings/wifi/WifiSettings;->ara:Lcom/android/settingslib/wifi/i;

    iput-object v2, p0, Lcom/android/settings/wifi/WifiSettings;->aqR:Landroid/os/Bundle;

    :cond_0
    iput-object v3, p0, Lcom/android/settings/wifi/WifiSettings;->arh:Lcom/android/settingslib/wifi/i;

    new-instance v0, Lcom/android/settings/wifi/s;

    invoke-virtual {p0}, Lcom/android/settings/wifi/WifiSettings;->getActivity()Landroid/app/Activity;

    move-result-object v1

    iget v4, p0, Lcom/android/settings/wifi/WifiSettings;->aqZ:I

    move-object v2, p0

    invoke-direct/range {v0 .. v5}, Lcom/android/settings/wifi/s;-><init>(Landroid/content/Context;Lcom/android/settings/wifi/t;Lcom/android/settingslib/wifi/i;IZ)V

    iput-object v0, p0, Lcom/android/settings/wifi/WifiSettings;->aqY:Lcom/android/settings/wifi/s;

    iget-object v0, p0, Lcom/android/settings/wifi/WifiSettings;->aqY:Lcom/android/settings/wifi/s;

    return-object v0

    :pswitch_2
    new-instance v0, Lcom/android/settings/wifi/S;

    invoke-virtual {p0}, Lcom/android/settings/wifi/WifiSettings;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, v1, v5}, Lcom/android/settings/wifi/S;-><init>(Landroid/content/Context;I)V

    return-object v0

    :pswitch_3
    new-instance v0, Lcom/android/settings/wifi/S;

    invoke-virtual {p0}, Lcom/android/settings/wifi/WifiSettings;->getActivity()Landroid/app/Activity;

    move-result-object v1

    const/4 v2, 0x1

    invoke-direct {v0, v1, v2}, Lcom/android/settings/wifi/S;-><init>(Landroid/content/Context;I)V

    return-object v0

    :pswitch_4
    iget-object v0, p0, Lcom/android/settings/wifi/WifiSettings;->arh:Lcom/android/settingslib/wifi/i;

    if-eqz v0, :cond_2

    new-instance v0, Lcom/android/settings/wifi/ab;

    invoke-virtual {p0}, Lcom/android/settings/wifi/WifiSettings;->getActivity()Landroid/app/Activity;

    move-result-object v1

    iget-object v2, p0, Lcom/android/settings/wifi/WifiSettings;->arh:Lcom/android/settingslib/wifi/i;

    invoke-virtual {v2}, Lcom/android/settingslib/wifi/i;->chP()I

    move-result v2

    new-instance v3, Lcom/android/settings/wifi/N;

    iget-object v4, p0, Lcom/android/settings/wifi/WifiSettings;->ark:Landroid/net/wifi/WifiManager;

    invoke-direct {v3, v4}, Lcom/android/settings/wifi/N;-><init>(Landroid/net/wifi/WifiManager;)V

    invoke-direct {v0, v1, v2, v3}, Lcom/android/settings/wifi/ab;-><init>(Landroid/content/Context;ILcom/android/settings/wifi/N;)V

    iput-object v0, p0, Lcom/android/settings/wifi/WifiSettings;->arm:Lcom/android/settings/wifi/ab;

    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/android/settings/wifi/WifiSettings;->arm:Lcom/android/settings/wifi/ab;

    return-object v0

    :cond_2
    iget-object v0, p0, Lcom/android/settings/wifi/WifiSettings;->arl:Landroid/os/Bundle;

    if-eqz v0, :cond_1

    new-instance v0, Lcom/android/settings/wifi/ab;

    invoke-virtual {p0}, Lcom/android/settings/wifi/WifiSettings;->getActivity()Landroid/app/Activity;

    move-result-object v1

    iget-object v2, p0, Lcom/android/settings/wifi/WifiSettings;->arl:Landroid/os/Bundle;

    new-instance v3, Lcom/android/settings/wifi/N;

    iget-object v4, p0, Lcom/android/settings/wifi/WifiSettings;->ark:Landroid/net/wifi/WifiManager;

    invoke-direct {v3, v4}, Lcom/android/settings/wifi/N;-><init>(Landroid/net/wifi/WifiManager;)V

    invoke-direct {v0, v1, v2, v3}, Lcom/android/settings/wifi/ab;-><init>(Landroid/content/Context;Landroid/os/Bundle;Lcom/android/settings/wifi/N;)V

    iput-object v0, p0, Lcom/android/settings/wifi/WifiSettings;->arm:Lcom/android/settings/wifi/ab;

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_4
    .end packed-switch
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V
    .locals 3

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/android/settings/wifi/WifiSettings;->bXB()Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    :cond_0
    const/4 v0, 0x6

    const v1, 0x7f120a87

    invoke-interface {p1, v2, v0, v2, v1}, Landroid/view/Menu;->add(IIII)Landroid/view/MenuItem;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/wifi/WifiSettings;->aqL:Lcom/android/settingslib/wifi/a;

    invoke-virtual {v1}, Lcom/android/settingslib/wifi/a;->cgw()Z

    move-result v1

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    move-result-object v0

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setShowAsAction(I)V

    invoke-super {p0, p1, p2}, Lcom/android/settings/MiuiRestrictedSettingsFragment;->onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V

    return-void
.end method

.method public onDestroy()V
    .locals 1

    iget-object v0, p0, Lcom/android/settings/wifi/WifiSettings;->aqV:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->quit()Z

    invoke-super {p0}, Lcom/android/settings/MiuiRestrictedSettingsFragment;->onDestroy()V

    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2

    const/4 v1, 0x1

    iget-boolean v0, p0, Lcom/android/settings/wifi/WifiSettings;->ard:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    return v0

    :cond_0
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    invoke-super {p0, p1}, Lcom/android/settings/MiuiRestrictedSettingsFragment;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    return v0

    :pswitch_0
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/android/settings/wifi/WifiSettings;->alW(I)V

    return v1

    :pswitch_1
    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lcom/android/settings/wifi/WifiSettings;->alW(I)V

    return v1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onPause()V
    .locals 1

    invoke-super {p0}, Lcom/android/settings/MiuiRestrictedSettingsFragment;->onPause()V

    iget-object v0, p0, Lcom/android/settings/wifi/WifiSettings;->aqK:Lcom/android/settings/wifi/i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/wifi/WifiSettings;->aqK:Lcom/android/settings/wifi/i;

    invoke-virtual {v0}, Lcom/android/settings/wifi/i;->pause()V

    :cond_0
    return-void
.end method

.method public onResume()V
    .locals 1

    invoke-virtual {p0}, Lcom/android/settings/wifi/WifiSettings;->getActivity()Landroid/app/Activity;

    invoke-super {p0}, Lcom/android/settings/MiuiRestrictedSettingsFragment;->onResume()V

    iget-object v0, p0, Lcom/android/settings/wifi/WifiSettings;->aqK:Lcom/android/settings/wifi/i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/wifi/WifiSettings;->aqK:Lcom/android/settings/wifi/i;

    invoke-virtual {v0}, Lcom/android/settings/wifi/i;->abN()V

    :cond_0
    iget-object v0, p0, Lcom/android/settings/wifi/WifiSettings;->aqL:Lcom/android/settingslib/wifi/a;

    invoke-virtual {v0}, Lcom/android/settingslib/wifi/a;->cgL()V

    iget-object v0, p0, Lcom/android/settings/wifi/WifiSettings;->aqL:Lcom/android/settingslib/wifi/a;

    invoke-virtual {v0}, Lcom/android/settingslib/wifi/a;->cgY()V

    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Lcom/android/settings/MiuiRestrictedSettingsFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    iget-object v0, p0, Lcom/android/settings/wifi/WifiSettings;->aqY:Lcom/android/settings/wifi/s;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/wifi/WifiSettings;->aqY:Lcom/android/settings/wifi/s;

    invoke-virtual {v0}, Lcom/android/settings/wifi/s;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string/jumbo v0, "dialog_mode"

    iget v1, p0, Lcom/android/settings/wifi/WifiSettings;->aqZ:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    iget-object v0, p0, Lcom/android/settings/wifi/WifiSettings;->ara:Lcom/android/settingslib/wifi/i;

    if-eqz v0, :cond_0

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iput-object v0, p0, Lcom/android/settings/wifi/WifiSettings;->aqR:Landroid/os/Bundle;

    iget-object v0, p0, Lcom/android/settings/wifi/WifiSettings;->ara:Lcom/android/settingslib/wifi/i;

    iget-object v1, p0, Lcom/android/settings/wifi/WifiSettings;->aqR:Landroid/os/Bundle;

    invoke-virtual {v0, v1}, Lcom/android/settingslib/wifi/i;->cie(Landroid/os/Bundle;)V

    const-string/jumbo v0, "wifi_ap_state"

    iget-object v1, p0, Lcom/android/settings/wifi/WifiSettings;->aqR:Landroid/os/Bundle;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    :cond_0
    iget-object v0, p0, Lcom/android/settings/wifi/WifiSettings;->arm:Lcom/android/settings/wifi/ab;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/settings/wifi/WifiSettings;->arm:Lcom/android/settings/wifi/ab;

    invoke-virtual {v0}, Lcom/android/settings/wifi/ab;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iget-object v1, p0, Lcom/android/settings/wifi/WifiSettings;->arm:Lcom/android/settings/wifi/ab;

    invoke-virtual {v1, v0}, Lcom/android/settings/wifi/ab;->saveState(Landroid/os/Bundle;)V

    const-string/jumbo v1, "wifi_nfc_dlg_state"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    :cond_1
    return-void
.end method

.method public onStop()V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/wifi/WifiSettings;->aqL:Lcom/android/settingslib/wifi/a;

    invoke-virtual {v0}, Lcom/android/settingslib/wifi/a;->cgn()V

    invoke-virtual {p0}, Lcom/android/settings/wifi/WifiSettings;->getView()Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/wifi/WifiSettings;->arj:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/view/View;->removeCallbacks(Ljava/lang/Runnable;)Z

    invoke-virtual {p0}, Lcom/android/settings/wifi/WifiSettings;->getView()Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/wifi/WifiSettings;->arc:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/view/View;->removeCallbacks(Ljava/lang/Runnable;)Z

    invoke-super {p0}, Lcom/android/settings/MiuiRestrictedSettingsFragment;->onStop()V

    return-void
.end method
