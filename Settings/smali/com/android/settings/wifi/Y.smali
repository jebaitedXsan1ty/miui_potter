.class Lcom/android/settings/wifi/Y;
.super Ljava/lang/Object;
.source "MiuiTetherBlockList.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;
.implements Landroid/content/DialogInterface$OnDismissListener;


# instance fields
.field final synthetic asA:Lcom/android/settings/wifi/MiuiTetherBlockList;

.field private asx:Z

.field private asy:Lcom/android/settings/wifi/D;

.field private asz:Landroid/app/AlertDialog;


# direct methods
.method private constructor <init>(Lcom/android/settings/wifi/MiuiTetherBlockList;Lcom/android/settings/wifi/D;)V
    .locals 6

    const/4 v4, 0x1

    const/4 v5, 0x0

    iput-object p1, p0, Lcom/android/settings/wifi/Y;->asA:Lcom/android/settings/wifi/MiuiTetherBlockList;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/android/settings/wifi/Y;->asy:Lcom/android/settings/wifi/D;

    invoke-virtual {p1}, Lcom/android/settings/wifi/MiuiTetherBlockList;->getActivity()Landroid/app/Activity;

    move-result-object v1

    iget-object v0, p0, Lcom/android/settings/wifi/Y;->asy:Lcom/android/settings/wifi/D;

    invoke-virtual {v0}, Lcom/android/settings/wifi/D;->afO()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v2, ""

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/wifi/Y;->asy:Lcom/android/settings/wifi/D;

    invoke-virtual {v0}, Lcom/android/settings/wifi/D;->afP()Ljava/lang/String;

    move-result-object v0

    :goto_0
    const v2, 0x7f12027d

    invoke-virtual {v1, v2}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-array v3, v4, [Ljava/lang/Object;

    aput-object v0, v3, v5

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    const v3, 0x7f12027c

    invoke-virtual {v1, v3}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-array v4, v4, [Ljava/lang/Object;

    aput-object v0, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v3, Landroid/app/AlertDialog$Builder;

    invoke-direct {v3, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v3, v2}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x1010355

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setIconAttribute(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x104000a

    invoke-virtual {v0, v1, p0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const/high16 v1, 0x1040000

    invoke-virtual {v0, v1, p0}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/wifi/Y;->asz:Landroid/app/AlertDialog;

    iget-object v0, p0, Lcom/android/settings/wifi/Y;->asz:Landroid/app/AlertDialog;

    invoke-virtual {v0, p0}, Landroid/app/AlertDialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/settings/wifi/Y;->asy:Lcom/android/settings/wifi/D;

    invoke-virtual {v0}, Lcom/android/settings/wifi/D;->afO()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method synthetic constructor <init>(Lcom/android/settings/wifi/MiuiTetherBlockList;Lcom/android/settings/wifi/D;Lcom/android/settings/wifi/Y;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/android/settings/wifi/Y;-><init>(Lcom/android/settings/wifi/MiuiTetherBlockList;Lcom/android/settings/wifi/D;)V

    return-void
.end method

.method private ait()V
    .locals 6

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/android/settings/wifi/Y;->asA:Lcom/android/settings/wifi/MiuiTetherBlockList;

    invoke-virtual {v0}, Lcom/android/settings/wifi/MiuiTetherBlockList;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v2

    move v0, v1

    :goto_0
    invoke-virtual {v2}, Landroid/preference/PreferenceScreen;->getPreferenceCount()I

    move-result v3

    if-ge v0, v3, :cond_2

    invoke-virtual {v2, v0}, Landroid/preference/PreferenceScreen;->getPreference(I)Landroid/preference/Preference;

    move-result-object v3

    invoke-virtual {v3}, Landroid/preference/Preference;->getTitle()Ljava/lang/CharSequence;

    move-result-object v4

    iget-object v5, p0, Lcom/android/settings/wifi/Y;->asy:Lcom/android/settings/wifi/D;

    invoke-virtual {v5}, Lcom/android/settings/wifi/D;->afO()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-virtual {v2, v3}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    return-void

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    add-int/lit8 v1, v1, 0x1

    :cond_2
    invoke-virtual {v2}, Landroid/preference/PreferenceScreen;->getPreferenceCount()I

    move-result v0

    if-ge v1, v0, :cond_3

    invoke-virtual {v2, v1}, Landroid/preference/PreferenceScreen;->getPreference(I)Landroid/preference/Preference;

    move-result-object v0

    invoke-virtual {v0}, Landroid/preference/Preference;->getTitle()Ljava/lang/CharSequence;

    move-result-object v3

    iget-object v4, p0, Lcom/android/settings/wifi/Y;->asy:Lcom/android/settings/wifi/D;

    invoke-virtual {v4}, Lcom/android/settings/wifi/D;->afP()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-virtual {v2, v0}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    return-void

    :cond_3
    return-void
.end method


# virtual methods
.method public ais()Lcom/android/settings/wifi/D;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/wifi/Y;->asy:Lcom/android/settings/wifi/D;

    return-object v0
.end method

.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 1

    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/android/settings/wifi/Y;->asx:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onDismiss(Landroid/content/DialogInterface;)V
    .locals 4

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/android/settings/wifi/Y;->asA:Lcom/android/settings/wifi/MiuiTetherBlockList;

    invoke-static {v0, v3}, Lcom/android/settings/wifi/MiuiTetherBlockList;->air(Lcom/android/settings/wifi/MiuiTetherBlockList;Z)Z

    iget-boolean v0, p0, Lcom/android/settings/wifi/Y;->asx:Z

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/android/settings/wifi/Y;->ait()V

    iget-object v0, p0, Lcom/android/settings/wifi/Y;->asA:Lcom/android/settings/wifi/MiuiTetherBlockList;

    invoke-static {v0}, Lcom/android/settings/wifi/MiuiTetherBlockList;->aiq(Lcom/android/settings/wifi/MiuiTetherBlockList;)Ljava/util/Set;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/wifi/Y;->asy:Lcom/android/settings/wifi/D;

    invoke-virtual {v1}, Lcom/android/settings/wifi/D;->afP()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    invoke-static {}, Lcom/android/settings/dc;->getInstance()Lcom/android/settings/dc;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/wifi/Y;->asA:Lcom/android/settings/wifi/MiuiTetherBlockList;

    invoke-virtual {v1}, Lcom/android/settings/wifi/MiuiTetherBlockList;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/android/settings/wifi/Y;->asA:Lcom/android/settings/wifi/MiuiTetherBlockList;

    invoke-static {v2}, Lcom/android/settings/wifi/MiuiTetherBlockList;->aiq(Lcom/android/settings/wifi/MiuiTetherBlockList;)Ljava/util/Set;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/android/settings/dc;->bsi(Landroid/content/Context;Ljava/util/Set;)V

    iput-boolean v3, p0, Lcom/android/settings/wifi/Y;->asx:Z

    :cond_0
    return-void
.end method

.method public show()V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/wifi/Y;->asA:Lcom/android/settings/wifi/MiuiTetherBlockList;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/android/settings/wifi/MiuiTetherBlockList;->air(Lcom/android/settings/wifi/MiuiTetherBlockList;Z)Z

    iget-object v0, p0, Lcom/android/settings/wifi/Y;->asz:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    return-void
.end method
