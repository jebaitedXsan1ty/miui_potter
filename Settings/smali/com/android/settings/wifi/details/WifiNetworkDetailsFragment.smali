.class public Lcom/android/settings/wifi/details/WifiNetworkDetailsFragment;
.super Lcom/android/settings/dashboard/DashboardFragment;
.source "WifiNetworkDetailsFragment.java"


# instance fields
.field private ajF:Lcom/android/settingslib/wifi/i;

.field private ajG:Lcom/android/settings/wifi/details/a;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/dashboard/DashboardFragment;-><init>()V

    return-void
.end method


# virtual methods
.method protected EU()Ljava/lang/String;
    .locals 1

    const-string/jumbo v0, "WifiNetworkDetailsFrg"

    return-object v0
.end method

.method protected EV()I
    .locals 1

    const v0, 0x7f150113

    return v0
.end method

.method public getMetricsCategory()I
    .locals 1

    const/16 v0, 0x351

    return v0
.end method

.method protected getPreferenceControllers(Landroid/content/Context;)Ljava/util/List;
    .locals 9

    const-class v0, Landroid/net/ConnectivityManager;

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Landroid/net/ConnectivityManager;

    new-instance v0, Lcom/android/settings/wifi/details/a;

    iget-object v1, p0, Lcom/android/settings/wifi/details/WifiNetworkDetailsFragment;->ajF:Lcom/android/settingslib/wifi/i;

    new-instance v2, Lcom/android/settings/vpn2/ConnectivityManagerWrapperImpl;

    invoke-direct {v2, v3}, Lcom/android/settings/vpn2/ConnectivityManagerWrapperImpl;-><init>(Landroid/net/ConnectivityManager;)V

    new-instance v5, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v3

    invoke-direct {v5, v3}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    invoke-virtual {p0}, Lcom/android/settings/wifi/details/WifiNetworkDetailsFragment;->getLifecycle()Lcom/android/settings/core/lifecycle/a;

    move-result-object v6

    const-class v3, Landroid/net/wifi/WifiManager;

    invoke-virtual {p1, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/net/wifi/WifiManager;

    const/4 v8, 0x0

    move-object v3, p1

    move-object v4, p0

    invoke-direct/range {v0 .. v8}, Lcom/android/settings/wifi/details/a;-><init>(Lcom/android/settingslib/wifi/i;Lcom/android/settings/vpn2/ConnectivityManagerWrapper;Landroid/content/Context;Landroid/app/Fragment;Landroid/os/Handler;Lcom/android/settings/core/lifecycle/a;Landroid/net/wifi/WifiManager;Lcom/android/settings/core/instrumentation/e;)V

    iput-object v0, p0, Lcom/android/settings/wifi/details/WifiNetworkDetailsFragment;->ajG:Lcom/android/settings/wifi/details/a;

    new-instance v0, Ljava/util/ArrayList;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iget-object v1, p0, Lcom/android/settings/wifi/details/WifiNetworkDetailsFragment;->ajG:Lcom/android/settings/wifi/details/a;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-object v0
.end method

.method public onAttach(Landroid/content/Context;)V
    .locals 2

    new-instance v0, Lcom/android/settingslib/wifi/i;

    invoke-virtual {p0}, Lcom/android/settings/wifi/details/WifiNetworkDetailsFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    invoke-direct {v0, p1, v1}, Lcom/android/settingslib/wifi/i;-><init>(Landroid/content/Context;Landroid/os/Bundle;)V

    iput-object v0, p0, Lcom/android/settings/wifi/details/WifiNetworkDetailsFragment;->ajF:Lcom/android/settingslib/wifi/i;

    invoke-super {p0, p1}, Lcom/android/settings/dashboard/DashboardFragment;->onAttach(Landroid/content/Context;)V

    return-void
.end method
