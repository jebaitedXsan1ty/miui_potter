.class public Lcom/android/settings/wifi/H;
.super Lcom/android/settings/wifi/A;
.source "MiuiWifiConfigController.java"


# instance fields
.field private final aqd:Lcom/android/settingslib/wifi/i;

.field private final aqe:Landroid/app/Activity;

.field private aqf:Z

.field private final aqg:Lcom/android/settings/wifi/I;

.field private aqh:Z

.field private final aqi:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Landroid/view/View;Lcom/android/settingslib/wifi/i;ZLcom/android/settings/wifi/I;)V
    .locals 10

    const/4 v9, -0x1

    const/4 v8, 0x1

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-direct {p0, p2, p3}, Lcom/android/settings/wifi/A;-><init>(Landroid/view/View;Lcom/android/settingslib/wifi/i;)V

    iput-object p1, p0, Lcom/android/settings/wifi/H;->aqe:Landroid/app/Activity;

    iput-object p5, p0, Lcom/android/settings/wifi/H;->aqg:Lcom/android/settings/wifi/I;

    iput-object p2, p0, Lcom/android/settings/wifi/H;->aqi:Landroid/view/View;

    iput-object p3, p0, Lcom/android/settings/wifi/H;->aqd:Lcom/android/settingslib/wifi/i;

    if-nez p3, :cond_1

    move v0, v2

    :goto_0
    iput v0, p0, Lcom/android/settings/wifi/H;->aoh:I

    iput-boolean p4, p0, Lcom/android/settings/wifi/H;->aqh:Z

    invoke-virtual {p1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    iget-object v0, p0, Lcom/android/settings/wifi/H;->aqd:Lcom/android/settingslib/wifi/i;

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/android/settings/wifi/H;->aqi:Landroid/view/View;

    const v1, 0x7f0a0425

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/settings/wifi/H;->aoU:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/android/settings/wifi/H;->aoU:Landroid/widget/TextView;

    invoke-virtual {v0, p0}, Landroid/widget/TextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    iget-object v0, p0, Lcom/android/settings/wifi/H;->aqi:Landroid/view/View;

    const v1, 0x7f0a04c3

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    :cond_0
    :goto_1
    return-void

    :cond_1
    iget v0, p3, Lcom/android/settingslib/wifi/i;->cCS:I

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/android/settings/wifi/H;->aqi:Landroid/view/View;

    const v1, 0x7f0a021a

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    iput-object v0, p0, Lcom/android/settings/wifi/H;->aoy:Landroid/widget/Spinner;

    iget-object v0, p0, Lcom/android/settings/wifi/H;->aqi:Landroid/view/View;

    const v1, 0x7f0a036c

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    iput-object v0, p0, Lcom/android/settings/wifi/H;->aoO:Landroid/widget/Spinner;

    iget-object v0, p0, Lcom/android/settings/wifi/H;->aqi:Landroid/view/View;

    const v1, 0x7f0a020a

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/android/settings/wifi/H;->aqd:Lcom/android/settingslib/wifi/i;

    invoke-virtual {v1}, Lcom/android/settingslib/wifi/i;->chY()Landroid/net/NetworkInfo$DetailedState;

    move-result-object v5

    if-eqz v5, :cond_3

    iget-object v1, p0, Lcom/android/settings/wifi/H;->aqd:Lcom/android/settingslib/wifi/i;

    invoke-virtual {v1}, Lcom/android/settingslib/wifi/i;->cib()Z

    move-result v6

    iget-object v1, p0, Lcom/android/settings/wifi/H;->aqd:Lcom/android/settingslib/wifi/i;

    invoke-virtual {v1}, Lcom/android/settingslib/wifi/i;->chL()Landroid/net/wifi/WifiConfiguration;

    move-result-object v1

    if-eqz v1, :cond_8

    invoke-virtual {v1}, Landroid/net/wifi/WifiConfiguration;->isPasspoint()Z

    move-result v7

    if-eqz v7, :cond_f

    iget-object v1, v1, Landroid/net/wifi/WifiConfiguration;->providerFriendlyName:Ljava/lang/String;

    :goto_2
    iget-object v7, p0, Lcom/android/settings/wifi/H;->aqe:Landroid/app/Activity;

    invoke-static {v7, v5, v6, v1}, Lcom/android/settingslib/wifi/i;->chC(Landroid/content/Context;Landroid/net/NetworkInfo$DetailedState;ZLjava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const v5, 0x7f12160d

    invoke-direct {p0, v0, v5, v1}, Lcom/android/settings/wifi/H;->agT(Landroid/view/ViewGroup;ILjava/lang/String;)V

    :cond_3
    iget-object v1, p0, Lcom/android/settings/wifi/H;->aqd:Lcom/android/settingslib/wifi/i;

    invoke-virtual {v1}, Lcom/android/settingslib/wifi/i;->chn()I

    move-result v1

    if-eq v1, v9, :cond_5

    const v5, 0x7f030139

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v4

    array-length v5, v4

    if-lt v1, v5, :cond_4

    array-length v1, v4

    add-int/lit8 v1, v1, -0x1

    :cond_4
    aget-object v1, v4, v1

    const v4, 0x7f121601

    invoke-direct {p0, v0, v4, v1}, Lcom/android/settings/wifi/H;->agT(Landroid/view/ViewGroup;ILjava/lang/String;)V

    :cond_5
    iget-object v1, p0, Lcom/android/settings/wifi/H;->aqd:Lcom/android/settingslib/wifi/i;

    invoke-virtual {v1}, Lcom/android/settingslib/wifi/i;->chk()Landroid/net/wifi/WifiInfo;

    move-result-object v1

    if-eqz v1, :cond_6

    invoke-virtual {v1}, Landroid/net/wifi/WifiInfo;->getLinkSpeed()I

    move-result v4

    if-eq v4, v9, :cond_6

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1}, Landroid/net/wifi/WifiInfo;->getLinkSpeed()I

    move-result v1

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v4, " Mbps"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const v4, 0x7f121603

    invoke-direct {p0, v0, v4, v1}, Lcom/android/settings/wifi/H;->agT(Landroid/view/ViewGroup;ILjava/lang/String;)V

    :cond_6
    iget-object v1, p0, Lcom/android/settings/wifi/H;->aqd:Lcom/android/settingslib/wifi/i;

    invoke-virtual {v1, v2}, Lcom/android/settingslib/wifi/i;->chV(Z)Ljava/lang/String;

    move-result-object v1

    const v4, 0x7f1215a3

    invoke-direct {p0, v0, v4, v1}, Lcom/android/settings/wifi/H;->agT(Landroid/view/ViewGroup;ILjava/lang/String;)V

    iget-object v1, p0, Lcom/android/settings/wifi/H;->aqd:Lcom/android/settingslib/wifi/i;

    invoke-virtual {v1}, Lcom/android/settingslib/wifi/i;->chL()Landroid/net/wifi/WifiConfiguration;

    move-result-object v4

    if-eqz v4, :cond_7

    invoke-virtual {v4}, Landroid/net/wifi/WifiConfiguration;->getIpAssignment()Landroid/net/IpConfiguration$IpAssignment;

    move-result-object v1

    sget-object v5, Landroid/net/IpConfiguration$IpAssignment;->STATIC:Landroid/net/IpConfiguration$IpAssignment;

    if-ne v1, v5, :cond_9

    iget-object v1, p0, Lcom/android/settings/wifi/H;->aoy:Landroid/widget/Spinner;

    invoke-virtual {v1, v8}, Landroid/widget/Spinner;->setSelection(I)V

    iput-boolean v8, p0, Lcom/android/settings/wifi/H;->aqf:Z

    :goto_3
    invoke-virtual {v4}, Landroid/net/wifi/WifiConfiguration;->getProxySettings()Landroid/net/IpConfiguration$ProxySettings;

    move-result-object v1

    sget-object v5, Landroid/net/IpConfiguration$ProxySettings;->STATIC:Landroid/net/IpConfiguration$ProxySettings;

    if-ne v1, v5, :cond_a

    iget-object v1, p0, Lcom/android/settings/wifi/H;->aoO:Landroid/widget/Spinner;

    invoke-virtual {v1, v8}, Landroid/widget/Spinner;->setSelection(I)V

    iput-boolean v8, p0, Lcom/android/settings/wifi/H;->aqf:Z

    :goto_4
    iget-object v1, p0, Lcom/android/settings/wifi/H;->aqd:Lcom/android/settingslib/wifi/i;

    iget v1, v1, Lcom/android/settingslib/wifi/i;->cCO:I

    if-eq v1, v9, :cond_7

    iget-object v1, p0, Lcom/android/settings/wifi/H;->aqe:Landroid/app/Activity;

    const-string/jumbo v5, "connectivity"

    invoke-virtual {v1, v5}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/net/ConnectivityManager;

    invoke-virtual {v1, v8}, Landroid/net/ConnectivityManager;->getLinkProperties(I)Landroid/net/LinkProperties;

    move-result-object v1

    const-string/jumbo v5, ""

    iget-object v5, p0, Lcom/android/settings/wifi/H;->aqd:Lcom/android/settingslib/wifi/i;

    invoke-virtual {v5}, Lcom/android/settingslib/wifi/i;->chv()Z

    move-result v5

    if-eqz v5, :cond_7

    invoke-direct {p0, v1}, Lcom/android/settings/wifi/H;->agU(Landroid/net/LinkProperties;)Ljava/lang/String;

    move-result-object v5

    const v1, 0x7f12154c

    invoke-direct {p0, v0, v1, v5}, Lcom/android/settings/wifi/H;->agT(Landroid/view/ViewGroup;ILjava/lang/String;)V

    invoke-virtual {v4}, Landroid/net/wifi/WifiConfiguration;->getIpAssignment()Landroid/net/IpConfiguration$IpAssignment;

    move-result-object v1

    sget-object v6, Landroid/net/IpConfiguration$IpAssignment;->STATIC:Landroid/net/IpConfiguration$IpAssignment;

    if-ne v1, v6, :cond_d

    invoke-virtual {v4}, Landroid/net/wifi/WifiConfiguration;->getStaticIpConfiguration()Landroid/net/StaticIpConfiguration;

    move-result-object v4

    if-eqz v4, :cond_c

    iget-object v1, v4, Landroid/net/StaticIpConfiguration;->ipAddress:Landroid/net/LinkAddress;

    if-eqz v1, :cond_c

    iget-object v1, v4, Landroid/net/StaticIpConfiguration;->ipAddress:Landroid/net/LinkAddress;

    invoke-virtual {v1}, Landroid/net/LinkAddress;->getNetworkPrefixLength()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/android/settings/wifi/H;->agV(I)Ljava/lang/String;

    move-result-object v1

    iget-object v3, v4, Landroid/net/StaticIpConfiguration;->gateway:Ljava/net/InetAddress;

    invoke-virtual {v3}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;

    move-result-object v3

    :goto_5
    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_7

    const v4, 0x7f121610

    invoke-direct {p0, v0, v4, v1}, Lcom/android/settings/wifi/H;->agT(Landroid/view/ViewGroup;ILjava/lang/String;)V

    const v1, 0x7f12153e

    invoke-direct {p0, v0, v1, v3}, Lcom/android/settings/wifi/H;->agT(Landroid/view/ViewGroup;ILjava/lang/String;)V

    :cond_7
    iget-boolean v0, p0, Lcom/android/settings/wifi/H;->aqh:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/wifi/H;->aqi:Landroid/view/View;

    const v1, 0x7f0a03cc

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/settings/wifi/H;->aqi:Landroid/view/View;

    const v1, 0x7f0a051a

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/settings/wifi/H;->aqi:Landroid/view/View;

    const v1, 0x7f0a0368

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/settings/wifi/H;->aoK:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/android/settings/wifi/H;->aoK:Landroid/widget/TextView;

    invoke-virtual {v0, p0}, Landroid/widget/TextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    iget-object v0, p0, Lcom/android/settings/wifi/H;->aqi:Landroid/view/View;

    const v1, 0x7f0a036b

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/settings/wifi/H;->aoM:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/android/settings/wifi/H;->aoM:Landroid/widget/TextView;

    invoke-virtual {v0, p0}, Landroid/widget/TextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    iget-object v0, p0, Lcom/android/settings/wifi/H;->aqi:Landroid/view/View;

    const v1, 0x7f0a0366

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/settings/wifi/H;->aoJ:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/android/settings/wifi/H;->aoJ:Landroid/widget/TextView;

    invoke-virtual {v0, p0}, Landroid/widget/TextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    invoke-virtual {p0}, Lcom/android/settings/wifi/H;->afD()V

    invoke-virtual {p0}, Lcom/android/settings/wifi/H;->afB()V

    iget-object v0, p0, Lcom/android/settings/wifi/H;->aoy:Landroid/widget/Spinner;

    invoke-virtual {v0, p0}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    iget-object v0, p0, Lcom/android/settings/wifi/H;->aoO:Landroid/widget/Spinner;

    invoke-virtual {v0, p0}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    goto/16 :goto_1

    :cond_8
    move-object v1, v3

    goto/16 :goto_2

    :cond_9
    iget-object v1, p0, Lcom/android/settings/wifi/H;->aoy:Landroid/widget/Spinner;

    invoke-virtual {v1, v2}, Landroid/widget/Spinner;->setSelection(I)V

    goto/16 :goto_3

    :cond_a
    invoke-virtual {v4}, Landroid/net/wifi/WifiConfiguration;->getProxySettings()Landroid/net/IpConfiguration$ProxySettings;

    move-result-object v1

    sget-object v5, Landroid/net/IpConfiguration$ProxySettings;->PAC:Landroid/net/IpConfiguration$ProxySettings;

    if-ne v1, v5, :cond_b

    iget-object v1, p0, Lcom/android/settings/wifi/H;->aoO:Landroid/widget/Spinner;

    const/4 v5, 0x2

    invoke-virtual {v1, v5}, Landroid/widget/Spinner;->setSelection(I)V

    iput-boolean v8, p0, Lcom/android/settings/wifi/H;->aqf:Z

    goto/16 :goto_4

    :cond_b
    iget-object v1, p0, Lcom/android/settings/wifi/H;->aoO:Landroid/widget/Spinner;

    invoke-virtual {v1, v2}, Landroid/widget/Spinner;->setSelection(I)V

    goto/16 :goto_4

    :cond_c
    move-object v1, v3

    goto/16 :goto_5

    :cond_d
    iget-object v1, p0, Lcom/android/settings/wifi/H;->aqe:Landroid/app/Activity;

    const-string/jumbo v4, "wifi"

    invoke-virtual {v1, v4}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/net/wifi/WifiManager;

    invoke-virtual {v1}, Landroid/net/wifi/WifiManager;->getDhcpInfo()Landroid/net/DhcpInfo;

    move-result-object v4

    if-eqz v4, :cond_e

    iget v1, v4, Landroid/net/DhcpInfo;->netmask:I

    invoke-static {v1}, Landroid/net/NetworkUtils;->intToInetAddress(I)Ljava/net/InetAddress;

    move-result-object v1

    invoke-virtual {v1}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;

    move-result-object v1

    iget v3, v4, Landroid/net/DhcpInfo;->gateway:I

    invoke-static {v3}, Landroid/net/NetworkUtils;->intToInetAddress(I)Ljava/net/InetAddress;

    move-result-object v3

    invoke-virtual {v3}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_5

    :cond_e
    move-object v1, v3

    goto/16 :goto_5

    :cond_f
    move-object v1, v3

    goto/16 :goto_2
.end method

.method private agT(Landroid/view/ViewGroup;ILjava/lang/String;)V
    .locals 4

    const v3, 0x1020010

    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/settings/wifi/H;->aqe:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0d0272

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const v0, 0x1020016

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, p2}, Landroid/widget/TextView;->setText(I)V

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, p3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const/4 v2, 0x5

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setGravity(I)V

    invoke-virtual {p1, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    return-void
.end method

.method private agU(Landroid/net/LinkProperties;)Ljava/lang/String;
    .locals 4

    const/4 v0, 0x0

    if-nez p1, :cond_0

    return-object v0

    :cond_0
    const-string/jumbo v0, ""

    invoke-virtual {p1}, Landroid/net/LinkProperties;->getAllAddresses()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move-object v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/net/InetAddress;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v3, "\n"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    :cond_1
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    goto :goto_0

    :cond_2
    return-object v1
.end method

.method private agV(I)Ljava/lang/String;
    .locals 10

    const/16 v3, 0x8

    const/4 v4, 0x0

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v0, 0x4

    const/16 v1, 0x20

    if-le p1, v1, :cond_0

    const/16 v0, 0x10

    :cond_0
    move v6, v4

    move v2, p1

    :goto_0
    if-ge v6, v0, :cond_5

    if-ge v2, v3, :cond_1

    move v1, v2

    :goto_1
    rsub-int/lit8 v8, v1, 0x8

    add-int/lit8 v1, v1, -0x1

    move v5, v1

    move v1, v4

    :goto_2
    if-ltz v5, :cond_2

    const/4 v9, 0x1

    shl-int/2addr v9, v5

    add-int/2addr v1, v9

    add-int/lit8 v5, v5, -0x1

    goto :goto_2

    :cond_1
    move v1, v3

    goto :goto_1

    :cond_2
    if-lez v8, :cond_3

    shl-int/2addr v1, v8

    :cond_3
    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string/jumbo v1, "."

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    if-lt v2, v3, :cond_4

    add-int/lit8 v1, v2, -0x8

    :goto_3
    add-int/lit8 v2, v6, 0x1

    move v6, v2

    move v2, v1

    goto :goto_0

    :cond_4
    move v1, v4

    goto :goto_3

    :cond_5
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {v7, v4, v0}, Ljava/lang/StringBuilder;->substring(II)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method aeS()V
    .locals 5

    const/16 v4, 0x8

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/android/settings/wifi/H;->aoU:Landroid/widget/TextView;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/android/settings/wifi/H;->aoU:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->length()I

    move-result v0

    if-lez v0, :cond_3

    iget-object v0, p0, Lcom/android/settings/wifi/H;->aoU:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getVisibility()I

    move-result v0

    if-eq v0, v4, :cond_3

    move v0, v1

    :goto_0
    if-eqz v0, :cond_5

    iget-object v3, p0, Lcom/android/settings/wifi/H;->aoE:Landroid/widget/TextView;

    if-eqz v3, :cond_5

    iget v3, p0, Lcom/android/settings/wifi/H;->aoh:I

    if-ne v3, v1, :cond_4

    iget-object v1, p0, Lcom/android/settings/wifi/H;->aoE:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->length()I

    move-result v1

    if-nez v1, :cond_4

    :cond_0
    :goto_1
    iget-boolean v0, p0, Lcom/android/settings/wifi/H;->aqh:Z

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/android/settings/wifi/H;->afg()Z

    move-result v2

    :cond_1
    iget-object v0, p0, Lcom/android/settings/wifi/H;->aqg:Lcom/android/settings/wifi/I;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/settings/wifi/H;->aqg:Lcom/android/settings/wifi/I;

    invoke-interface {v0, v2}, Lcom/android/settings/wifi/I;->agW(Z)V

    :cond_2
    return-void

    :cond_3
    move v0, v2

    goto :goto_0

    :cond_4
    iget v1, p0, Lcom/android/settings/wifi/H;->aoh:I

    const/4 v3, 0x2

    if-ne v1, v3, :cond_5

    iget-object v1, p0, Lcom/android/settings/wifi/H;->aoE:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->length()I

    move-result v1

    if-lt v1, v4, :cond_0

    :cond_5
    move v2, v0

    goto :goto_1
.end method

.method afa()Landroid/net/wifi/WifiConfiguration;
    .locals 4

    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v0, 0x0

    iget-object v3, p0, Lcom/android/settings/wifi/H;->aqd:Lcom/android/settingslib/wifi/i;

    if-eqz v3, :cond_0

    iget-object v0, p0, Lcom/android/settings/wifi/H;->aqd:Lcom/android/settingslib/wifi/i;

    invoke-virtual {v0}, Lcom/android/settingslib/wifi/i;->chL()Landroid/net/wifi/WifiConfiguration;

    move-result-object v0

    :cond_0
    iget-boolean v3, p0, Lcom/android/settings/wifi/H;->aqh:Z

    if-eqz v3, :cond_1

    if-eqz v0, :cond_2

    iget v0, v0, Landroid/net/wifi/WifiConfiguration;->networkId:I

    const/4 v3, -0x1

    if-ne v0, v3, :cond_2

    :cond_1
    move v0, v1

    :goto_0
    invoke-virtual {p0, v0}, Lcom/android/settings/wifi/H;->afb(Z)Landroid/net/wifi/WifiConfiguration;

    move-result-object v0

    return-object v0

    :cond_2
    move v0, v2

    goto :goto_0
.end method

.method protected afg()Z
    .locals 7

    const/4 v5, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/android/settings/wifi/H;->aoy:Landroid/widget/Spinner;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/wifi/H;->aoy:Landroid/widget/Spinner;

    invoke-virtual {v0}, Landroid/widget/Spinner;->getSelectedItemPosition()I

    move-result v0

    if-ne v0, v1, :cond_0

    sget-object v0, Landroid/net/IpConfiguration$IpAssignment;->STATIC:Landroid/net/IpConfiguration$IpAssignment;

    :goto_0
    iput-object v0, p0, Lcom/android/settings/wifi/H;->aox:Landroid/net/IpConfiguration$IpAssignment;

    iget-object v0, p0, Lcom/android/settings/wifi/H;->aox:Landroid/net/IpConfiguration$IpAssignment;

    sget-object v3, Landroid/net/IpConfiguration$IpAssignment;->STATIC:Landroid/net/IpConfiguration$IpAssignment;

    if-ne v0, v3, :cond_8

    new-instance v0, Landroid/net/StaticIpConfiguration;

    invoke-direct {v0}, Landroid/net/StaticIpConfiguration;-><init>()V

    iput-object v0, p0, Lcom/android/settings/wifi/H;->aoV:Landroid/net/StaticIpConfiguration;

    iget-object v0, p0, Lcom/android/settings/wifi/H;->aoV:Landroid/net/StaticIpConfiguration;

    invoke-virtual {p0, v0}, Lcom/android/settings/wifi/H;->afH(Landroid/net/StaticIpConfiguration;)I

    move-result v0

    if-eqz v0, :cond_1

    return v2

    :cond_0
    sget-object v0, Landroid/net/IpConfiguration$IpAssignment;->DHCP:Landroid/net/IpConfiguration$IpAssignment;

    goto :goto_0

    :cond_1
    move v0, v1

    :goto_1
    iget-object v3, p0, Lcom/android/settings/wifi/H;->aoO:Landroid/widget/Spinner;

    invoke-virtual {v3}, Landroid/widget/Spinner;->getSelectedItemPosition()I

    move-result v3

    sget-object v4, Landroid/net/IpConfiguration$ProxySettings;->NONE:Landroid/net/IpConfiguration$ProxySettings;

    iput-object v4, p0, Lcom/android/settings/wifi/H;->aoN:Landroid/net/IpConfiguration$ProxySettings;

    iput-object v5, p0, Lcom/android/settings/wifi/H;->aov:Landroid/net/ProxyInfo;

    if-ne v3, v1, :cond_5

    iget-object v4, p0, Lcom/android/settings/wifi/H;->aoK:Landroid/widget/TextView;

    if-eqz v4, :cond_5

    sget-object v0, Landroid/net/IpConfiguration$ProxySettings;->STATIC:Landroid/net/IpConfiguration$ProxySettings;

    iput-object v0, p0, Lcom/android/settings/wifi/H;->aoN:Landroid/net/IpConfiguration$ProxySettings;

    iget-object v0, p0, Lcom/android/settings/wifi/H;->aoK:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v4

    iget-object v0, p0, Lcom/android/settings/wifi/H;->aoM:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v3

    iget-object v0, p0, Lcom/android/settings/wifi/H;->aoJ:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v5

    :try_start_0
    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    :try_start_1
    invoke-static {v4, v3, v5}, Lcom/android/settings/ProxySelector;->blm(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_1

    move-result v3

    move v6, v3

    move v3, v0

    move v0, v6

    :goto_2
    if-nez v0, :cond_2

    new-instance v0, Landroid/net/ProxyInfo;

    invoke-direct {v0, v4, v3, v5}, Landroid/net/ProxyInfo;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    iput-object v0, p0, Lcom/android/settings/wifi/H;->aov:Landroid/net/ProxyInfo;

    move v2, v1

    :cond_2
    move v0, v2

    :cond_3
    :goto_3
    iget-object v2, p0, Lcom/android/settings/wifi/H;->aox:Landroid/net/IpConfiguration$IpAssignment;

    sget-object v3, Landroid/net/IpConfiguration$IpAssignment;->DHCP:Landroid/net/IpConfiguration$IpAssignment;

    if-ne v2, v3, :cond_4

    iget-object v2, p0, Lcom/android/settings/wifi/H;->aoN:Landroid/net/IpConfiguration$ProxySettings;

    sget-object v3, Landroid/net/IpConfiguration$ProxySettings;->NONE:Landroid/net/IpConfiguration$ProxySettings;

    if-ne v2, v3, :cond_4

    move v0, v1

    :cond_4
    return v0

    :catch_0
    move-exception v0

    move v0, v2

    :goto_4
    const v3, 0x7f120d99

    move v6, v3

    move v3, v0

    move v0, v6

    goto :goto_2

    :cond_5
    const/4 v4, 0x2

    if-ne v3, v4, :cond_3

    iget-object v3, p0, Lcom/android/settings/wifi/H;->aoL:Landroid/widget/TextView;

    if-eqz v3, :cond_3

    sget-object v0, Landroid/net/IpConfiguration$ProxySettings;->PAC:Landroid/net/IpConfiguration$ProxySettings;

    iput-object v0, p0, Lcom/android/settings/wifi/H;->aoN:Landroid/net/IpConfiguration$ProxySettings;

    iget-object v0, p0, Lcom/android/settings/wifi/H;->aoL:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_6

    return v2

    :cond_6
    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    if-nez v0, :cond_7

    return v2

    :cond_7
    new-instance v2, Landroid/net/ProxyInfo;

    invoke-direct {v2, v0}, Landroid/net/ProxyInfo;-><init>(Landroid/net/Uri;)V

    iput-object v2, p0, Lcom/android/settings/wifi/H;->aov:Landroid/net/ProxyInfo;

    move v0, v1

    goto :goto_3

    :catch_1
    move-exception v3

    goto :goto_4

    :cond_8
    move v0, v2

    goto/16 :goto_1
.end method
