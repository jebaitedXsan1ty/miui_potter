.class Lcom/android/settings/wifi/T;
.super Ljava/lang/Object;
.source "MiuiAccessPointPreference.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private arY:Lcom/android/settingslib/wifi/i;

.field private arZ:Lcom/android/settings/MiuiSettingsPreferenceFragment;


# direct methods
.method public constructor <init>(Lcom/android/settingslib/wifi/i;Lcom/android/settings/MiuiSettingsPreferenceFragment;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/android/settings/wifi/T;->arY:Lcom/android/settingslib/wifi/i;

    iput-object p2, p0, Lcom/android/settings/wifi/T;->arZ:Lcom/android/settings/MiuiSettingsPreferenceFragment;

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6

    const/4 v5, 0x0

    const/4 v0, 0x0

    new-instance v4, Landroid/os/Bundle;

    invoke-direct {v4}, Landroid/os/Bundle;-><init>()V

    iget-object v1, p0, Lcom/android/settings/wifi/T;->arY:Lcom/android/settingslib/wifi/i;

    invoke-virtual {v1, v4}, Lcom/android/settingslib/wifi/i;->cie(Landroid/os/Bundle;)V

    const-string/jumbo v1, ":miui:starting_window_label"

    const-string/jumbo v2, ""

    invoke-virtual {v4, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string/jumbo v1, "ssid"

    iget-object v2, p0, Lcom/android/settings/wifi/T;->arY:Lcom/android/settingslib/wifi/i;

    iget-object v2, v2, Lcom/android/settingslib/wifi/i;->cCL:Ljava/lang/String;

    invoke-virtual {v4, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/android/settings/wifi/T;->arZ:Lcom/android/settings/MiuiSettingsPreferenceFragment;

    invoke-virtual {v1}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    :cond_0
    if-eqz v0, :cond_1

    const-string/jumbo v1, "extra_show_on_finddevice_keyguard"

    invoke-virtual {v0, v1, v5}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string/jumbo v0, "extra_show_on_finddevice_keyguard"

    const/4 v1, 0x1

    invoke-virtual {v4, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    :cond_1
    iget-object v0, p0, Lcom/android/settings/wifi/T;->arZ:Lcom/android/settings/MiuiSettingsPreferenceFragment;

    iget-object v1, p0, Lcom/android/settings/wifi/T;->arZ:Lcom/android/settings/MiuiSettingsPreferenceFragment;

    const-class v2, Lcom/android/settings/wifi/MiuiWifiDetailFragment;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    const/16 v3, 0xc8

    invoke-virtual/range {v0 .. v5}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->bWC(Landroid/app/Fragment;Ljava/lang/String;ILandroid/os/Bundle;I)Z

    return-void
.end method
