.class final Lcom/android/settings/wifi/bs;
.super Ljava/lang/Object;
.source "EditTetherFragment.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemSelectedListener;


# instance fields
.field final synthetic auP:Lcom/android/settings/wifi/EditTetherFragment;


# direct methods
.method constructor <init>(Lcom/android/settings/wifi/EditTetherFragment;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/wifi/bs;->auP:Lcom/android/settings/wifi/EditTetherFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemSelected(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 3

    iget-object v0, p0, Lcom/android/settings/wifi/bs;->auP:Lcom/android/settings/wifi/EditTetherFragment;

    invoke-static {v0, p3}, Lcom/android/settings/wifi/EditTetherFragment;->ahY(Lcom/android/settings/wifi/EditTetherFragment;I)I

    iget-object v0, p0, Lcom/android/settings/wifi/bs;->auP:Lcom/android/settings/wifi/EditTetherFragment;

    iget-object v0, v0, Lcom/android/settings/wifi/EditTetherFragment;->asi:Landroid/net/wifi/WifiConfiguration;

    iget-object v1, p0, Lcom/android/settings/wifi/bs;->auP:Lcom/android/settings/wifi/EditTetherFragment;

    invoke-static {v1}, Lcom/android/settings/wifi/EditTetherFragment;->ahW(Lcom/android/settings/wifi/EditTetherFragment;)I

    move-result v1

    iput v1, v0, Landroid/net/wifi/WifiConfiguration;->apBand:I

    iget-object v0, p0, Lcom/android/settings/wifi/bs;->auP:Lcom/android/settings/wifi/EditTetherFragment;

    invoke-static {v0}, Lcom/android/settings/wifi/EditTetherFragment;->ahV(Lcom/android/settings/wifi/EditTetherFragment;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "config on channelIndex : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/settings/wifi/bs;->auP:Lcom/android/settings/wifi/EditTetherFragment;

    invoke-static {v2}, Lcom/android/settings/wifi/EditTetherFragment;->ahW(Lcom/android/settings/wifi/EditTetherFragment;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " Band: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/settings/wifi/bs;->auP:Lcom/android/settings/wifi/EditTetherFragment;

    iget-object v2, v2, Lcom/android/settings/wifi/EditTetherFragment;->asi:Landroid/net/wifi/WifiConfiguration;

    iget v2, v2, Landroid/net/wifi/WifiConfiguration;->apBand:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public onNothingSelected(Landroid/widget/AdapterView;)V
    .locals 0

    return-void
.end method
