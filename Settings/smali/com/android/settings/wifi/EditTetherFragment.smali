.class public Lcom/android/settings/wifi/EditTetherFragment;
.super Lcom/android/settings/BaseEditFragment;
.source "EditTetherFragment.java"

# interfaces
.implements Lcom/android/settings/wifi/I;
.implements Landroid/view/View$OnClickListener;
.implements Landroid/text/TextWatcher;
.implements Landroid/widget/AdapterView$OnItemSelectedListener;


# instance fields
.field private TAG:Ljava/lang/String;

.field private asa:I

.field private asb:I

.field private asc:Z

.field private asd:Z

.field private ase:Landroid/widget/EditText;

.field private asf:I

.field private asg:Landroid/widget/ImageView;

.field private ash:Landroid/widget/TextView;

.field asi:Landroid/net/wifi/WifiConfiguration;

.field private asj:Landroid/net/wifi/WifiManager;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/android/settings/BaseEditFragment;-><init>()V

    const/4 v0, 0x1

    iput v0, p0, Lcom/android/settings/wifi/EditTetherFragment;->asf:I

    const/4 v0, 0x0

    iput v0, p0, Lcom/android/settings/wifi/EditTetherFragment;->asa:I

    const-class v0, Lcom/android/settings/wifi/EditTetherFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/wifi/EditTetherFragment;->TAG:Ljava/lang/String;

    return-void
.end method

.method public static ahQ(Landroid/net/wifi/WifiConfiguration;)I
    .locals 2

    iget-object v0, p0, Landroid/net/wifi/WifiConfiguration;->allowedKeyManagement:Ljava/util/BitSet;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->get(I)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    return v0

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method private ahS()V
    .locals 5

    const v4, 0x7f0a018b

    const/4 v3, 0x1

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/android/settings/wifi/EditTetherFragment;->getView()Landroid/view/View;

    move-result-object v0

    iget v1, p0, Lcom/android/settings/wifi/EditTetherFragment;->asf:I

    if-nez v1, :cond_0

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    return-void

    :cond_0
    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/settings/wifi/EditTetherFragment;->ase:Landroid/widget/EditText;

    invoke-virtual {v0, v3}, Landroid/widget/EditText;->setFocusable(Z)V

    iget-object v0, p0, Lcom/android/settings/wifi/EditTetherFragment;->ase:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->requestFocus()Z

    iget-object v0, p0, Lcom/android/settings/wifi/EditTetherFragment;->ase:Landroid/widget/EditText;

    const/high16 v1, 0x41700000    # 15.0f

    invoke-virtual {v0, v3, v1}, Landroid/widget/EditText;->setTextSize(IF)V

    return-void
.end method

.method private ahT()V
    .locals 3

    iget-object v0, p0, Lcom/android/settings/wifi/EditTetherFragment;->ase:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getSelectionStart()I

    move-result v1

    iget-object v2, p0, Lcom/android/settings/wifi/EditTetherFragment;->ase:Landroid/widget/EditText;

    iget-boolean v0, p0, Lcom/android/settings/wifi/EditTetherFragment;->asd:Z

    if-eqz v0, :cond_1

    const/16 v0, 0x90

    :goto_0
    or-int/lit8 v0, v0, 0x1

    invoke-virtual {v2, v0}, Landroid/widget/EditText;->setInputType(I)V

    if-ltz v1, :cond_0

    iget-object v0, p0, Lcom/android/settings/wifi/EditTetherFragment;->ase:Landroid/widget/EditText;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setSelection(I)V

    :cond_0
    return-void

    :cond_1
    const/16 v0, 0x80

    goto :goto_0
.end method

.method private ahU()V
    .locals 4

    const/4 v3, 0x1

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/android/settings/wifi/EditTetherFragment;->ash:Landroid/widget/TextView;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/settings/wifi/EditTetherFragment;->ash:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->length()I

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    invoke-virtual {p0, v2}, Lcom/android/settings/wifi/EditTetherFragment;->agW(Z)V

    :goto_0
    return-void

    :cond_1
    iget v0, p0, Lcom/android/settings/wifi/EditTetherFragment;->asf:I

    if-ne v0, v3, :cond_2

    iget-object v0, p0, Lcom/android/settings/wifi/EditTetherFragment;->ase:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->length()I

    move-result v0

    const/16 v1, 0x8

    if-lt v0, v1, :cond_0

    :cond_2
    invoke-virtual {p0, v3}, Lcom/android/settings/wifi/EditTetherFragment;->agW(Z)V

    goto :goto_0
.end method

.method static synthetic ahV(Lcom/android/settings/wifi/EditTetherFragment;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/wifi/EditTetherFragment;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic ahW(Lcom/android/settings/wifi/EditTetherFragment;)I
    .locals 1

    iget v0, p0, Lcom/android/settings/wifi/EditTetherFragment;->asa:I

    return v0
.end method

.method static synthetic ahX(Lcom/android/settings/wifi/EditTetherFragment;)I
    .locals 1

    iget v0, p0, Lcom/android/settings/wifi/EditTetherFragment;->asb:I

    return v0
.end method

.method static synthetic ahY(Lcom/android/settings/wifi/EditTetherFragment;I)I
    .locals 0

    iput p1, p0, Lcom/android/settings/wifi/EditTetherFragment;->asa:I

    return p1
.end method

.method static synthetic ahZ(Lcom/android/settings/wifi/EditTetherFragment;I)I
    .locals 0

    iput p1, p0, Lcom/android/settings/wifi/EditTetherFragment;->asb:I

    return p1
.end method

.method static synthetic aia(Lcom/android/settings/wifi/EditTetherFragment;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/android/settings/wifi/EditTetherFragment;->asc:Z

    return p1
.end method


# virtual methods
.method public adN()V
    .locals 3

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/android/settings/wifi/EditTetherFragment;->ahP()Landroid/net/wifi/WifiConfiguration;

    move-result-object v1

    if-eqz v1, :cond_0

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string/jumbo v2, "config"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    :cond_0
    iget-boolean v1, p0, Lcom/android/settings/wifi/EditTetherFragment;->asc:Z

    if-eqz v1, :cond_1

    iget v1, p0, Lcom/android/settings/wifi/EditTetherFragment;->asb:I

    invoke-virtual {p0, v1}, Lcom/android/settings/wifi/EditTetherFragment;->ahR(I)V

    :cond_1
    invoke-virtual {p0, v0}, Lcom/android/settings/wifi/EditTetherFragment;->beb(Landroid/os/Bundle;)V

    return-void
.end method

.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/wifi/EditTetherFragment;->ahU()V

    return-void
.end method

.method public agW(Z)V
    .locals 0

    invoke-virtual {p0, p1}, Lcom/android/settings/wifi/EditTetherFragment;->bea(Z)V

    return-void
.end method

.method public ahP()Landroid/net/wifi/WifiConfiguration;
    .locals 4

    const/4 v3, 0x0

    new-instance v0, Landroid/net/wifi/WifiConfiguration;

    invoke-direct {v0}, Landroid/net/wifi/WifiConfiguration;-><init>()V

    iget-object v1, p0, Lcom/android/settings/wifi/EditTetherFragment;->ash:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Landroid/net/wifi/WifiConfiguration;->SSID:Ljava/lang/String;

    iget v1, p0, Lcom/android/settings/wifi/EditTetherFragment;->asa:I

    iput v1, v0, Landroid/net/wifi/WifiConfiguration;->apBand:I

    iget v1, p0, Lcom/android/settings/wifi/EditTetherFragment;->asf:I

    packed-switch v1, :pswitch_data_0

    const/4 v0, 0x0

    return-object v0

    :pswitch_0
    iget-object v1, v0, Landroid/net/wifi/WifiConfiguration;->allowedKeyManagement:Ljava/util/BitSet;

    invoke-virtual {v1, v3}, Ljava/util/BitSet;->set(I)V

    return-object v0

    :pswitch_1
    iget-object v1, v0, Landroid/net/wifi/WifiConfiguration;->allowedKeyManagement:Ljava/util/BitSet;

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Ljava/util/BitSet;->set(I)V

    iget-object v1, v0, Landroid/net/wifi/WifiConfiguration;->allowedAuthAlgorithms:Ljava/util/BitSet;

    invoke-virtual {v1, v3}, Ljava/util/BitSet;->set(I)V

    iget-object v1, p0, Lcom/android/settings/wifi/EditTetherFragment;->ase:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->length()I

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/settings/wifi/EditTetherFragment;->ase:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-interface {v1}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Landroid/net/wifi/WifiConfiguration;->preSharedKey:Ljava/lang/String;

    :cond_0
    return-object v0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public ahR(I)V
    .locals 2

    invoke-virtual {p0}, Lcom/android/settings/wifi/EditTetherFragment;->getContext()Landroid/content/Context;

    move-result-object v1

    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    const-string/jumbo v0, "DD0A0017F206010103010000"

    :goto_0
    invoke-static {v1, v0}, Landroid/provider/MiuiSettings$System;->setHotSpotVendorSpecific(Landroid/content/Context;Ljava/lang/String;)V

    return-void

    :cond_0
    const-string/jumbo v0, ""

    goto :goto_0
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    return-void
.end method

.method public getTitle()Ljava/lang/String;
    .locals 1

    const v0, 0x7f121618

    invoke-virtual {p0, v0}, Lcom/android/settings/wifi/EditTetherFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 3

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const v1, 0x7f0a0400

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/android/settings/wifi/EditTetherFragment;->asd:Z

    xor-int/lit8 v0, v0, 0x1

    iput-boolean v0, p0, Lcom/android/settings/wifi/EditTetherFragment;->asd:Z

    iget-object v1, p0, Lcom/android/settings/wifi/EditTetherFragment;->asg:Landroid/widget/ImageView;

    iget-boolean v0, p0, Lcom/android/settings/wifi/EditTetherFragment;->asd:Z

    if-eqz v0, :cond_1

    const v0, 0x7f08045d

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    iget-object v1, p0, Lcom/android/settings/wifi/EditTetherFragment;->asg:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/android/settings/wifi/EditTetherFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    iget-boolean v0, p0, Lcom/android/settings/wifi/EditTetherFragment;->asd:Z

    if-eqz v0, :cond_2

    const v0, 0x7f121540

    :goto_1
    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    invoke-direct {p0}, Lcom/android/settings/wifi/EditTetherFragment;->ahT()V

    :cond_0
    return-void

    :cond_1
    const v0, 0x7f08045a

    goto :goto_0

    :cond_2
    const v0, 0x7f121600

    goto :goto_1
.end method

.method public onItemSelected(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 0

    iput p3, p0, Lcom/android/settings/wifi/EditTetherFragment;->asf:I

    invoke-direct {p0}, Lcom/android/settings/wifi/EditTetherFragment;->ahS()V

    invoke-direct {p0}, Lcom/android/settings/wifi/EditTetherFragment;->ahU()V

    return-void
.end method

.method public onNothingSelected(Landroid/widget/AdapterView;)V
    .locals 0

    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 9

    const v8, 0x1090008

    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-super {p0, p1, p2}, Lcom/android/settings/BaseEditFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/android/settings/wifi/EditTetherFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string/jumbo v1, "config"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/WifiConfiguration;

    iput-object v0, p0, Lcom/android/settings/wifi/EditTetherFragment;->asi:Landroid/net/wifi/WifiConfiguration;

    iget-object v0, p0, Lcom/android/settings/wifi/EditTetherFragment;->asi:Landroid/net/wifi/WifiConfiguration;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/wifi/EditTetherFragment;->asi:Landroid/net/wifi/WifiConfiguration;

    invoke-static {v0}, Lcom/android/settings/wifi/EditTetherFragment;->ahQ(Landroid/net/wifi/WifiConfiguration;)I

    move-result v0

    iput v0, p0, Lcom/android/settings/wifi/EditTetherFragment;->asf:I

    :cond_0
    const v0, 0x7f0a03cb

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    const v1, 0x7f0a04c3

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    const v1, 0x7f0a0425

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/android/settings/wifi/EditTetherFragment;->ash:Landroid/widget/TextView;

    const v1, 0x7f0a0306

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/EditText;

    iput-object v1, p0, Lcom/android/settings/wifi/EditTetherFragment;->ase:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/android/settings/wifi/EditTetherFragment;->asi:Landroid/net/wifi/WifiConfiguration;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/android/settings/wifi/EditTetherFragment;->ash:Landroid/widget/TextView;

    iget-object v4, p0, Lcom/android/settings/wifi/EditTetherFragment;->asi:Landroid/net/wifi/WifiConfiguration;

    iget-object v4, v4, Landroid/net/wifi/WifiConfiguration;->SSID:Ljava/lang/String;

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/android/settings/wifi/EditTetherFragment;->asi:Landroid/net/wifi/WifiConfiguration;

    iget v1, v1, Landroid/net/wifi/WifiConfiguration;->apBand:I

    iput v1, p0, Lcom/android/settings/wifi/EditTetherFragment;->asa:I

    iget-object v1, p0, Lcom/android/settings/wifi/EditTetherFragment;->ash:Landroid/widget/TextView;

    check-cast v1, Landroid/widget/EditText;

    iget-object v4, p0, Lcom/android/settings/wifi/EditTetherFragment;->ash:Landroid/widget/TextView;

    invoke-virtual {v4}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v4

    invoke-interface {v4}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    invoke-virtual {v1, v4}, Landroid/widget/EditText;->setSelection(I)V

    iget v1, p0, Lcom/android/settings/wifi/EditTetherFragment;->asf:I

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setSelection(I)V

    iget v1, p0, Lcom/android/settings/wifi/EditTetherFragment;->asf:I

    if-ne v1, v2, :cond_1

    iget-object v1, p0, Lcom/android/settings/wifi/EditTetherFragment;->ase:Landroid/widget/EditText;

    iget-object v4, p0, Lcom/android/settings/wifi/EditTetherFragment;->asi:Landroid/net/wifi/WifiConfiguration;

    iget-object v4, v4, Landroid/net/wifi/WifiConfiguration;->preSharedKey:Ljava/lang/String;

    invoke-virtual {v1, v4}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    :cond_1
    iget-object v1, p0, Lcom/android/settings/wifi/EditTetherFragment;->ash:Landroid/widget/TextView;

    invoke-virtual {v1, p0}, Landroid/widget/TextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    iget-object v1, p0, Lcom/android/settings/wifi/EditTetherFragment;->ash:Landroid/widget/TextView;

    new-array v4, v2, [Landroid/text/InputFilter;

    new-instance v5, Lcom/android/settings/bN;

    new-instance v6, Lcom/android/settings/wifi/bq;

    invoke-direct {v6, p0}, Lcom/android/settings/wifi/bq;-><init>(Lcom/android/settings/wifi/EditTetherFragment;)V

    const/16 v7, 0x20

    invoke-direct {v5, v6, v7}, Lcom/android/settings/bN;-><init>(Lcom/android/settings/bO;I)V

    aput-object v5, v4, v3

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setFilters([Landroid/text/InputFilter;)V

    iget-object v1, p0, Lcom/android/settings/wifi/EditTetherFragment;->ase:Landroid/widget/EditText;

    invoke-virtual {v1, p0}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    const v1, 0x7f0a0400

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/android/settings/wifi/EditTetherFragment;->asg:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/android/settings/wifi/EditTetherFragment;->asg:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/android/settings/wifi/EditTetherFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f121600

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/android/settings/wifi/EditTetherFragment;->asg:Landroid/widget/ImageView;

    const v4, 0x7f08045a

    invoke-virtual {v1, v4}, Landroid/widget/ImageView;->setImageResource(I)V

    iget-object v1, p0, Lcom/android/settings/wifi/EditTetherFragment;->asg:Landroid/widget/ImageView;

    invoke-virtual {v1, p0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {v0, p0}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    invoke-virtual {v0}, Landroid/widget/Spinner;->getPrompt()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setPrompt(Ljava/lang/CharSequence;)V

    invoke-virtual {v0}, Landroid/widget/Spinner;->getAdapter()Landroid/widget/SpinnerAdapter;

    move-result-object v0

    check-cast v0, Landroid/widget/ArrayAdapter;

    sget v1, Lmiui/R$layout;->simple_spinner_dropdown_item:I

    invoke-virtual {v0, v1}, Landroid/widget/ArrayAdapter;->setDropDownViewResource(I)V

    invoke-direct {p0}, Lcom/android/settings/wifi/EditTetherFragment;->ahS()V

    invoke-direct {p0}, Lcom/android/settings/wifi/EditTetherFragment;->ahU()V

    const-string/jumbo v0, "mediatek"

    const-string/jumbo v1, "vendor"

    invoke-static {v1}, Lmiui/util/FeatureParser;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_5

    invoke-virtual {p0}, Lcom/android/settings/wifi/EditTetherFragment;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/provider/MiuiSettings$System;->getHotSpotVendorSpecific(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_4

    const-string/jumbo v1, "DD0A0017F206010103010000"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    move v0, v2

    :goto_0
    iput v0, p0, Lcom/android/settings/wifi/EditTetherFragment;->asb:I

    :goto_1
    const v0, 0x7f0a0171

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    invoke-virtual {v0}, Landroid/widget/Spinner;->getAdapter()Landroid/widget/SpinnerAdapter;

    move-result-object v1

    check-cast v1, Landroid/widget/ArrayAdapter;

    sget v2, Lmiui/R$layout;->simple_spinner_dropdown_item:I

    invoke-virtual {v1, v2}, Landroid/widget/ArrayAdapter;->setDropDownViewResource(I)V

    iget v1, p0, Lcom/android/settings/wifi/EditTetherFragment;->asb:I

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setSelection(I)V

    new-instance v1, Lcom/android/settings/wifi/br;

    invoke-direct {v1, p0}, Lcom/android/settings/wifi/br;-><init>(Lcom/android/settings/wifi/EditTetherFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    :goto_2
    const v0, 0x7f0a00dd

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    invoke-virtual {p0}, Lcom/android/settings/wifi/EditTetherFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    const-string/jumbo v2, "wifi"

    invoke-virtual {v1, v2}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/net/wifi/WifiManager;

    iput-object v1, p0, Lcom/android/settings/wifi/EditTetherFragment;->asj:Landroid/net/wifi/WifiManager;

    iget-object v1, p0, Lcom/android/settings/wifi/EditTetherFragment;->asj:Landroid/net/wifi/WifiManager;

    invoke-virtual {v1}, Landroid/net/wifi/WifiManager;->getCountryCode()Ljava/lang/String;

    move-result-object v2

    iget-object v1, p0, Lcom/android/settings/wifi/EditTetherFragment;->asj:Landroid/net/wifi/WifiManager;

    invoke-virtual {v1}, Landroid/net/wifi/WifiManager;->isDualBandSupported()Z

    move-result v1

    if-eqz v1, :cond_2

    if-nez v2, :cond_8

    :cond_2
    iget-object v4, p0, Lcom/android/settings/wifi/EditTetherFragment;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/android/settings/wifi/EditTetherFragment;->asj:Landroid/net/wifi/WifiManager;

    invoke-virtual {v1}, Landroid/net/wifi/WifiManager;->isDualBandSupported()Z

    move-result v1

    if-nez v1, :cond_6

    const-string/jumbo v1, "Device do not support 5GHz "

    :goto_3
    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    if-nez v2, :cond_7

    const-string/jumbo v1, " NO country code"

    :goto_4
    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " forbid 5GHz"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v4, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lcom/android/settings/wifi/EditTetherFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    const v2, 0x7f03011e

    invoke-static {v1, v2, v8}, Landroid/widget/ArrayAdapter;->createFromResource(Landroid/content/Context;II)Landroid/widget/ArrayAdapter;

    move-result-object v1

    iget-object v2, p0, Lcom/android/settings/wifi/EditTetherFragment;->asi:Landroid/net/wifi/WifiConfiguration;

    iput v3, v2, Landroid/net/wifi/WifiConfiguration;->apBand:I

    iput v3, p0, Lcom/android/settings/wifi/EditTetherFragment;->asa:I

    :goto_5
    sget v2, Lmiui/R$layout;->simple_spinner_dropdown_item:I

    invoke-virtual {v1, v2}, Landroid/widget/ArrayAdapter;->setDropDownViewResource(I)V

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    iget v1, p0, Lcom/android/settings/wifi/EditTetherFragment;->asa:I

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setSelection(I)V

    new-instance v1, Lcom/android/settings/wifi/bs;

    invoke-direct {v1, p0}, Lcom/android/settings/wifi/bs;-><init>(Lcom/android/settings/wifi/EditTetherFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    return-void

    :cond_3
    move v0, v3

    goto/16 :goto_0

    :cond_4
    invoke-virtual {p0}, Lcom/android/settings/wifi/EditTetherFragment;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string/jumbo v1, "DD0A0017F206010103010000"

    invoke-static {v0, v1}, Landroid/provider/MiuiSettings$System;->setHotSpotVendorSpecific(Landroid/content/Context;Ljava/lang/String;)V

    iput v2, p0, Lcom/android/settings/wifi/EditTetherFragment;->asb:I

    goto/16 :goto_1

    :cond_5
    const v0, 0x7f0a01f6

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->removeAllViews()V

    goto/16 :goto_2

    :cond_6
    const-string/jumbo v1, ""

    goto :goto_3

    :cond_7
    const-string/jumbo v1, ""

    goto :goto_4

    :cond_8
    invoke-virtual {p0}, Lcom/android/settings/wifi/EditTetherFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x11200d0

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v1

    if-nez v1, :cond_9

    invoke-virtual {p0}, Lcom/android/settings/wifi/EditTetherFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    const v2, 0x7f03011d

    invoke-static {v1, v2, v8}, Landroid/widget/ArrayAdapter;->createFromResource(Landroid/content/Context;II)Landroid/widget/ArrayAdapter;

    move-result-object v1

    goto :goto_5

    :cond_9
    invoke-virtual {p0}, Lcom/android/settings/wifi/EditTetherFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    const v2, 0x7f03011f

    invoke-static {v1, v2, v8}, Landroid/widget/ArrayAdapter;->createFromResource(Landroid/content/Context;II)Landroid/widget/ArrayAdapter;

    move-result-object v1

    goto :goto_5
.end method

.method public onViewStateRestored(Landroid/os/Bundle;)V
    .locals 0

    invoke-super {p0, p1}, Lcom/android/settings/BaseEditFragment;->onViewStateRestored(Landroid/os/Bundle;)V

    invoke-direct {p0}, Lcom/android/settings/wifi/EditTetherFragment;->ahT()V

    return-void
.end method

.method public vB(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2

    const v0, 0x7f0d026e

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method
