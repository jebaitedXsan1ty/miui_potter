.class public Lcom/android/settings/wifi/S;
.super Landroid/app/AlertDialog;
.source "WpsDialog.java"


# instance fields
.field private arA:Landroid/widget/ProgressBar;

.field private arB:Landroid/widget/TextView;

.field private arC:Landroid/widget/ProgressBar;

.field private arD:Ljava/util/Timer;

.field private arE:Landroid/view/View;

.field private arF:Landroid/net/wifi/WifiManager;

.field private arG:Landroid/net/wifi/WifiManager$WpsCallback;

.field private arH:I

.field private arv:Landroid/widget/Button;

.field arw:Lcom/android/settings/wifi/WpsDialog$DialogState;

.field private final arx:Landroid/content/IntentFilter;

.field private ary:Landroid/os/Handler;

.field private arz:Ljava/lang/String;

.field private mContext:Landroid/content/Context;

.field private mReceiver:Landroid/content/BroadcastReceiver;


# direct methods
.method public constructor <init>(Landroid/content/Context;I)V
    .locals 2

    invoke-direct {p0, p1}, Landroid/app/AlertDialog;-><init>(Landroid/content/Context;)V

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/android/settings/wifi/S;->ary:Landroid/os/Handler;

    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/android/settings/wifi/S;->arz:Ljava/lang/String;

    sget-object v0, Lcom/android/settings/wifi/WpsDialog$DialogState;->arM:Lcom/android/settings/wifi/WpsDialog$DialogState;

    iput-object v0, p0, Lcom/android/settings/wifi/S;->arw:Lcom/android/settings/wifi/WpsDialog$DialogState;

    iput-object p1, p0, Lcom/android/settings/wifi/S;->mContext:Landroid/content/Context;

    iput p2, p0, Lcom/android/settings/wifi/S;->arH:I

    new-instance v0, Lcom/android/settings/wifi/bj;

    invoke-direct {v0, p0}, Lcom/android/settings/wifi/bj;-><init>(Lcom/android/settings/wifi/S;)V

    iput-object v0, p0, Lcom/android/settings/wifi/S;->arG:Landroid/net/wifi/WifiManager$WpsCallback;

    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    iput-object v0, p0, Lcom/android/settings/wifi/S;->arx:Landroid/content/IntentFilter;

    iget-object v0, p0, Lcom/android/settings/wifi/S;->arx:Landroid/content/IntentFilter;

    const-string/jumbo v1, "android.net.wifi.STATE_CHANGE"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    new-instance v0, Lcom/android/settings/wifi/bk;

    invoke-direct {v0, p0}, Lcom/android/settings/wifi/bk;-><init>(Lcom/android/settings/wifi/S;)V

    iput-object v0, p0, Lcom/android/settings/wifi/S;->mReceiver:Landroid/content/BroadcastReceiver;

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/android/settings/wifi/S;->setCanceledOnTouchOutside(Z)V

    return-void
.end method

.method static synthetic ahA(Lcom/android/settings/wifi/S;)Landroid/os/Handler;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/wifi/S;->ary:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic ahB(Lcom/android/settings/wifi/S;)Landroid/widget/ProgressBar;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/wifi/S;->arA:Landroid/widget/ProgressBar;

    return-object v0
.end method

.method static synthetic ahC(Lcom/android/settings/wifi/S;)Landroid/content/BroadcastReceiver;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/wifi/S;->mReceiver:Landroid/content/BroadcastReceiver;

    return-object v0
.end method

.method static synthetic ahD(Lcom/android/settings/wifi/S;)Landroid/widget/TextView;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/wifi/S;->arB:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic ahE(Lcom/android/settings/wifi/S;)Landroid/widget/ProgressBar;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/wifi/S;->arC:Landroid/widget/ProgressBar;

    return-object v0
.end method

.method static synthetic ahF(Lcom/android/settings/wifi/S;Landroid/content/BroadcastReceiver;)Landroid/content/BroadcastReceiver;
    .locals 0

    iput-object p1, p0, Lcom/android/settings/wifi/S;->mReceiver:Landroid/content/BroadcastReceiver;

    return-object p1
.end method

.method static synthetic ahG(Lcom/android/settings/wifi/S;Landroid/content/Context;Landroid/content/Intent;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/android/settings/wifi/S;->ahw(Landroid/content/Context;Landroid/content/Intent;)V

    return-void
.end method

.method static synthetic ahH(Lcom/android/settings/wifi/S;Lcom/android/settings/wifi/WpsDialog$DialogState;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/android/settings/wifi/S;->ahx(Lcom/android/settings/wifi/WpsDialog$DialogState;Ljava/lang/String;)V

    return-void
.end method

.method private ahw(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 4

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "android.net.wifi.STATE_CHANGE"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string/jumbo v0, "networkInfo"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/NetworkInfo;

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->getDetailedState()Landroid/net/NetworkInfo$DetailedState;

    move-result-object v0

    sget-object v1, Landroid/net/NetworkInfo$DetailedState;->CONNECTED:Landroid/net/NetworkInfo$DetailedState;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/android/settings/wifi/S;->arw:Lcom/android/settings/wifi/WpsDialog$DialogState;

    sget-object v1, Lcom/android/settings/wifi/WpsDialog$DialogState;->arK:Lcom/android/settings/wifi/WpsDialog$DialogState;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/android/settings/wifi/S;->arF:Landroid/net/wifi/WifiManager;

    invoke-virtual {v0}, Landroid/net/wifi/WifiManager;->getConnectionInfo()Landroid/net/wifi/WifiInfo;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/android/settings/wifi/S;->mContext:Landroid/content/Context;

    const v2, 0x7f121645

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0}, Landroid/net/wifi/WifiInfo;->getSSID()Ljava/lang/String;

    move-result-object v0

    const/4 v3, 0x0

    aput-object v0, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/android/settings/wifi/WpsDialog$DialogState;->arJ:Lcom/android/settings/wifi/WpsDialog$DialogState;

    invoke-direct {p0, v1, v0}, Lcom/android/settings/wifi/S;->ahx(Lcom/android/settings/wifi/WpsDialog$DialogState;Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method private ahx(Lcom/android/settings/wifi/WpsDialog$DialogState;Ljava/lang/String;)V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/wifi/S;->arw:Lcom/android/settings/wifi/WpsDialog$DialogState;

    invoke-virtual {v0}, Lcom/android/settings/wifi/WpsDialog$DialogState;->ordinal()I

    move-result v0

    invoke-virtual {p1}, Lcom/android/settings/wifi/WpsDialog$DialogState;->ordinal()I

    move-result v1

    if-lt v0, v1, :cond_0

    return-void

    :cond_0
    iput-object p1, p0, Lcom/android/settings/wifi/S;->arw:Lcom/android/settings/wifi/WpsDialog$DialogState;

    iput-object p2, p0, Lcom/android/settings/wifi/S;->arz:Ljava/lang/String;

    iget-object v0, p0, Lcom/android/settings/wifi/S;->ary:Landroid/os/Handler;

    new-instance v1, Lcom/android/settings/wifi/bo;

    invoke-direct {v1, p0, p1, p2}, Lcom/android/settings/wifi/bo;-><init>(Lcom/android/settings/wifi/S;Lcom/android/settings/wifi/WpsDialog$DialogState;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method static synthetic ahy(Lcom/android/settings/wifi/S;)Landroid/widget/Button;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/wifi/S;->arv:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic ahz(Lcom/android/settings/wifi/S;)Landroid/content/Context;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/wifi/S;->mContext:Landroid/content/Context;

    return-object v0
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/android/settings/wifi/S;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0d0287

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/wifi/S;->arE:Landroid/view/View;

    iget-object v0, p0, Lcom/android/settings/wifi/S;->arE:Landroid/view/View;

    const v1, 0x7f0a0538

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/settings/wifi/S;->arB:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/android/settings/wifi/S;->arB:Landroid/widget/TextView;

    const v1, 0x7f121650

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    iget-object v0, p0, Lcom/android/settings/wifi/S;->arE:Landroid/view/View;

    const v1, 0x7f0a053a

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/android/settings/wifi/S;->arC:Landroid/widget/ProgressBar;

    iget-object v0, p0, Lcom/android/settings/wifi/S;->arC:Landroid/widget/ProgressBar;

    const/16 v1, 0x78

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setMax(I)V

    iget-object v0, p0, Lcom/android/settings/wifi/S;->arC:Landroid/widget/ProgressBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setProgress(I)V

    iget-object v0, p0, Lcom/android/settings/wifi/S;->arE:Landroid/view/View;

    const v1, 0x7f0a0539

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/android/settings/wifi/S;->arA:Landroid/widget/ProgressBar;

    iget-object v0, p0, Lcom/android/settings/wifi/S;->arA:Landroid/widget/ProgressBar;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/settings/wifi/S;->arE:Landroid/view/View;

    const v1, 0x7f0a0537

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/android/settings/wifi/S;->arv:Landroid/widget/Button;

    iget-object v0, p0, Lcom/android/settings/wifi/S;->arv:Landroid/widget/Button;

    const v1, 0x7f1214e4

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(I)V

    iget-object v0, p0, Lcom/android/settings/wifi/S;->arv:Landroid/widget/Button;

    new-instance v1, Lcom/android/settings/wifi/bl;

    invoke-direct {v1, p0}, Lcom/android/settings/wifi/bl;-><init>(Lcom/android/settings/wifi/S;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/android/settings/wifi/S;->mContext:Landroid/content/Context;

    const-string/jumbo v1, "wifi"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/WifiManager;

    iput-object v0, p0, Lcom/android/settings/wifi/S;->arF:Landroid/net/wifi/WifiManager;

    iget-object v0, p0, Lcom/android/settings/wifi/S;->arE:Landroid/view/View;

    invoke-virtual {p0, v0}, Lcom/android/settings/wifi/S;->setView(Landroid/view/View;)V

    if-nez p1, :cond_0

    new-instance v0, Landroid/net/wifi/WpsInfo;

    invoke-direct {v0}, Landroid/net/wifi/WpsInfo;-><init>()V

    iget v1, p0, Lcom/android/settings/wifi/S;->arH:I

    iput v1, v0, Landroid/net/wifi/WpsInfo;->setup:I

    iget-object v1, p0, Lcom/android/settings/wifi/S;->arF:Landroid/net/wifi/WifiManager;

    iget-object v2, p0, Lcom/android/settings/wifi/S;->arG:Landroid/net/wifi/WifiManager$WpsCallback;

    invoke-virtual {v1, v0, v2}, Landroid/net/wifi/WifiManager;->startWps(Landroid/net/wifi/WpsInfo;Landroid/net/wifi/WifiManager$WpsCallback;)V

    :cond_0
    invoke-super {p0, p1}, Landroid/app/AlertDialog;->onCreate(Landroid/os/Bundle;)V

    return-void
.end method

.method public onRestoreInstanceState(Landroid/os/Bundle;)V
    .locals 2

    if-eqz p1, :cond_0

    invoke-super {p0, p1}, Landroid/app/AlertDialog;->onRestoreInstanceState(Landroid/os/Bundle;)V

    iget-object v0, p0, Lcom/android/settings/wifi/S;->arw:Lcom/android/settings/wifi/WpsDialog$DialogState;

    const-string/jumbo v0, "android:dialogState"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/android/settings/wifi/WpsDialog$DialogState;->valueOf(Ljava/lang/String;)Lcom/android/settings/wifi/WpsDialog$DialogState;

    move-result-object v0

    const-string/jumbo v1, "android:dialogMsg"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/android/settings/wifi/S;->ahx(Lcom/android/settings/wifi/WpsDialog$DialogState;Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public onSaveInstanceState()Landroid/os/Bundle;
    .locals 3

    invoke-super {p0}, Landroid/app/AlertDialog;->onSaveInstanceState()Landroid/os/Bundle;

    move-result-object v0

    const-string/jumbo v1, "android:dialogState"

    iget-object v2, p0, Lcom/android/settings/wifi/S;->arw:Lcom/android/settings/wifi/WpsDialog$DialogState;

    invoke-virtual {v2}, Lcom/android/settings/wifi/WpsDialog$DialogState;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string/jumbo v1, "android:dialogMsg"

    iget-object v2, p0, Lcom/android/settings/wifi/S;->arz:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method protected onStart()V
    .locals 6

    const-wide/16 v2, 0x3e8

    new-instance v0, Ljava/util/Timer;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/Timer;-><init>(Z)V

    iput-object v0, p0, Lcom/android/settings/wifi/S;->arD:Ljava/util/Timer;

    iget-object v0, p0, Lcom/android/settings/wifi/S;->arD:Ljava/util/Timer;

    new-instance v1, Lcom/android/settings/wifi/bm;

    invoke-direct {v1, p0}, Lcom/android/settings/wifi/bm;-><init>(Lcom/android/settings/wifi/S;)V

    move-wide v4, v2

    invoke-virtual/range {v0 .. v5}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;JJ)V

    iget-object v0, p0, Lcom/android/settings/wifi/S;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/android/settings/wifi/S;->mReceiver:Landroid/content/BroadcastReceiver;

    iget-object v2, p0, Lcom/android/settings/wifi/S;->arx:Landroid/content/IntentFilter;

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    return-void
.end method

.method protected onStop()V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/android/settings/wifi/S;->arw:Lcom/android/settings/wifi/WpsDialog$DialogState;

    sget-object v1, Lcom/android/settings/wifi/WpsDialog$DialogState;->arK:Lcom/android/settings/wifi/WpsDialog$DialogState;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/android/settings/wifi/S;->arF:Landroid/net/wifi/WifiManager;

    invoke-virtual {v0, v2}, Landroid/net/wifi/WifiManager;->cancelWps(Landroid/net/wifi/WifiManager$WpsCallback;)V

    :cond_0
    iget-object v0, p0, Lcom/android/settings/wifi/S;->mReceiver:Landroid/content/BroadcastReceiver;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/settings/wifi/S;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/android/settings/wifi/S;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    iput-object v2, p0, Lcom/android/settings/wifi/S;->mReceiver:Landroid/content/BroadcastReceiver;

    :cond_1
    iget-object v0, p0, Lcom/android/settings/wifi/S;->arD:Ljava/util/Timer;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/settings/wifi/S;->arD:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    :cond_2
    return-void
.end method
