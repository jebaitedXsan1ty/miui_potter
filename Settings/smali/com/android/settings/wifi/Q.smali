.class public Lcom/android/settings/wifi/Q;
.super Lcom/android/settings/core/c;
.source "WifiMasterSwitchPreferenceController.java"

# interfaces
.implements Lcom/android/settings/widget/x;
.implements Lcom/android/settings/core/lifecycle/b;
.implements Lcom/android/settings/core/lifecycle/a/b;
.implements Lcom/android/settings/core/lifecycle/a/d;
.implements Lcom/android/settings/core/lifecycle/a/c;
.implements Lcom/android/settings/core/lifecycle/a/j;


# instance fields
.field private art:Lcom/android/settings/wifi/n;

.field private aru:Lcom/android/settings/widget/MasterSwitchPreference;

.field private final mMetricsFeatureProvider:Lcom/android/settings/core/instrumentation/e;

.field private final mSummaryHelper:Lcom/android/settings/wifi/U;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/android/settings/core/instrumentation/e;)V
    .locals 2

    invoke-direct {p0, p1}, Lcom/android/settings/core/c;-><init>(Landroid/content/Context;)V

    iput-object p2, p0, Lcom/android/settings/wifi/Q;->mMetricsFeatureProvider:Lcom/android/settings/core/instrumentation/e;

    new-instance v0, Lcom/android/settings/wifi/U;

    iget-object v1, p0, Lcom/android/settings/wifi/Q;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1, p0}, Lcom/android/settings/wifi/U;-><init>(Landroid/content/Context;Lcom/android/settings/widget/x;)V

    iput-object v0, p0, Lcom/android/settings/wifi/Q;->mSummaryHelper:Lcom/android/settings/wifi/U;

    return-void
.end method


# virtual methods
.method public a(Landroid/support/v7/preference/PreferenceScreen;)V
    .locals 1

    invoke-super {p0, p1}, Lcom/android/settings/core/c;->a(Landroid/support/v7/preference/PreferenceScreen;)V

    const-string/jumbo v0, "toggle_wifi"

    invoke-virtual {p1, v0}, Landroid/support/v7/preference/PreferenceScreen;->dlg(Ljava/lang/CharSequence;)Landroid/support/v7/preference/Preference;

    move-result-object v0

    check-cast v0, Lcom/android/settings/widget/MasterSwitchPreference;

    iput-object v0, p0, Lcom/android/settings/wifi/Q;->aru:Lcom/android/settings/widget/MasterSwitchPreference;

    return-void
.end method

.method public ahr(Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lcom/android/settings/wifi/Q;->aru:Lcom/android/settings/widget/MasterSwitchPreference;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/wifi/Q;->aru:Lcom/android/settings/widget/MasterSwitchPreference;

    invoke-virtual {v0, p1}, Lcom/android/settings/widget/MasterSwitchPreference;->setSummary(Ljava/lang/CharSequence;)V

    :cond_0
    return-void
.end method

.method public b()Ljava/lang/String;
    .locals 1

    const-string/jumbo v0, "toggle_wifi"

    return-object v0
.end method

.method public c()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public onPause()V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/wifi/Q;->art:Lcom/android/settings/wifi/n;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/wifi/Q;->art:Lcom/android/settings/wifi/n;

    invoke-virtual {v0}, Lcom/android/settings/wifi/n;->pause()V

    :cond_0
    iget-object v0, p0, Lcom/android/settings/wifi/Q;->mSummaryHelper:Lcom/android/settings/wifi/U;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/android/settings/wifi/U;->aib(Z)V

    return-void
.end method

.method public onResume()V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/wifi/Q;->mSummaryHelper:Lcom/android/settings/wifi/U;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/android/settings/wifi/U;->aib(Z)V

    iget-object v0, p0, Lcom/android/settings/wifi/Q;->art:Lcom/android/settings/wifi/n;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/wifi/Q;->art:Lcom/android/settings/wifi/n;

    iget-object v1, p0, Lcom/android/settings/wifi/Q;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/android/settings/wifi/n;->acC(Landroid/content/Context;)V

    :cond_0
    return-void
.end method

.method public onStart()V
    .locals 4

    new-instance v0, Lcom/android/settings/wifi/n;

    iget-object v1, p0, Lcom/android/settings/wifi/Q;->mContext:Landroid/content/Context;

    new-instance v2, Lcom/android/settings/widget/A;

    iget-object v3, p0, Lcom/android/settings/wifi/Q;->aru:Lcom/android/settings/widget/MasterSwitchPreference;

    invoke-direct {v2, v3}, Lcom/android/settings/widget/A;-><init>(Lcom/android/settings/widget/MasterSwitchPreference;)V

    iget-object v3, p0, Lcom/android/settings/wifi/Q;->mMetricsFeatureProvider:Lcom/android/settings/core/instrumentation/e;

    invoke-direct {v0, v1, v2, v3}, Lcom/android/settings/wifi/n;-><init>(Landroid/content/Context;Lcom/android/settings/widget/i;Lcom/android/settings/core/instrumentation/e;)V

    iput-object v0, p0, Lcom/android/settings/wifi/Q;->art:Lcom/android/settings/wifi/n;

    return-void
.end method

.method public onStop()V
    .locals 1

    iget-object v0, p0, Lcom/android/settings/wifi/Q;->art:Lcom/android/settings/wifi/n;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/wifi/Q;->art:Lcom/android/settings/wifi/n;

    invoke-virtual {v0}, Lcom/android/settings/wifi/n;->acF()V

    :cond_0
    return-void
.end method
