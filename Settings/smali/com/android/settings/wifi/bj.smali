.class Lcom/android/settings/wifi/bj;
.super Landroid/net/wifi/WifiManager$WpsCallback;
.source "WpsDialog.java"


# instance fields
.field final synthetic auE:Lcom/android/settings/wifi/S;


# direct methods
.method constructor <init>(Lcom/android/settings/wifi/S;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/wifi/bj;->auE:Lcom/android/settings/wifi/S;

    invoke-direct {p0}, Landroid/net/wifi/WifiManager$WpsCallback;-><init>()V

    return-void
.end method


# virtual methods
.method public onFailed(I)V
    .locals 3

    packed-switch p1, :pswitch_data_0

    :pswitch_0
    iget-object v0, p0, Lcom/android/settings/wifi/bj;->auE:Lcom/android/settings/wifi/S;

    invoke-static {v0}, Lcom/android/settings/wifi/S;->ahz(Lcom/android/settings/wifi/S;)Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f121647

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    iget-object v1, p0, Lcom/android/settings/wifi/bj;->auE:Lcom/android/settings/wifi/S;

    sget-object v2, Lcom/android/settings/wifi/WpsDialog$DialogState;->arL:Lcom/android/settings/wifi/WpsDialog$DialogState;

    invoke-static {v1, v2, v0}, Lcom/android/settings/wifi/S;->ahH(Lcom/android/settings/wifi/S;Lcom/android/settings/wifi/WpsDialog$DialogState;Ljava/lang/String;)V

    return-void

    :pswitch_1
    iget-object v0, p0, Lcom/android/settings/wifi/bj;->auE:Lcom/android/settings/wifi/S;

    invoke-static {v0}, Lcom/android/settings/wifi/S;->ahz(Lcom/android/settings/wifi/S;)Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f121648

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :pswitch_2
    iget-object v0, p0, Lcom/android/settings/wifi/bj;->auE:Lcom/android/settings/wifi/S;

    invoke-static {v0}, Lcom/android/settings/wifi/S;->ahz(Lcom/android/settings/wifi/S;)Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f12164a

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :pswitch_3
    iget-object v0, p0, Lcom/android/settings/wifi/bj;->auE:Lcom/android/settings/wifi/S;

    invoke-static {v0}, Lcom/android/settings/wifi/S;->ahz(Lcom/android/settings/wifi/S;)Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f121649

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :pswitch_4
    iget-object v0, p0, Lcom/android/settings/wifi/bj;->auE:Lcom/android/settings/wifi/S;

    invoke-static {v0}, Lcom/android/settings/wifi/S;->ahz(Lcom/android/settings/wifi/S;)Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f12164b

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_4
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public onStarted(Ljava/lang/String;)V
    .locals 5

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/android/settings/wifi/bj;->auE:Lcom/android/settings/wifi/S;

    sget-object v1, Lcom/android/settings/wifi/WpsDialog$DialogState;->arN:Lcom/android/settings/wifi/WpsDialog$DialogState;

    iget-object v2, p0, Lcom/android/settings/wifi/bj;->auE:Lcom/android/settings/wifi/S;

    invoke-static {v2}, Lcom/android/settings/wifi/S;->ahz(Lcom/android/settings/wifi/S;)Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f12164e    # 1.941831E38f

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/android/settings/wifi/S;->ahH(Lcom/android/settings/wifi/S;Lcom/android/settings/wifi/WpsDialog$DialogState;Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/settings/wifi/bj;->auE:Lcom/android/settings/wifi/S;

    sget-object v1, Lcom/android/settings/wifi/WpsDialog$DialogState;->arN:Lcom/android/settings/wifi/WpsDialog$DialogState;

    iget-object v2, p0, Lcom/android/settings/wifi/bj;->auE:Lcom/android/settings/wifi/S;

    invoke-static {v2}, Lcom/android/settings/wifi/S;->ahz(Lcom/android/settings/wifi/S;)Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f12164d

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/android/settings/wifi/S;->ahH(Lcom/android/settings/wifi/S;Lcom/android/settings/wifi/WpsDialog$DialogState;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onSucceeded()V
    .locals 4

    iget-object v0, p0, Lcom/android/settings/wifi/bj;->auE:Lcom/android/settings/wifi/S;

    sget-object v1, Lcom/android/settings/wifi/WpsDialog$DialogState;->arK:Lcom/android/settings/wifi/WpsDialog$DialogState;

    iget-object v2, p0, Lcom/android/settings/wifi/bj;->auE:Lcom/android/settings/wifi/S;

    invoke-static {v2}, Lcom/android/settings/wifi/S;->ahz(Lcom/android/settings/wifi/S;)Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f121644

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/android/settings/wifi/S;->ahH(Lcom/android/settings/wifi/S;Lcom/android/settings/wifi/WpsDialog$DialogState;Ljava/lang/String;)V

    return-void
.end method
