.class Lcom/android/settings/wifi/c;
.super Landroid/database/ContentObserver;
.source "NotifyOpenNetworksPreferenceController.java"


# instance fields
.field private final aku:Landroid/net/Uri;

.field private final akv:Landroid/preference/Preference;

.field final synthetic akw:Lcom/android/settings/wifi/b;


# direct methods
.method public constructor <init>(Lcom/android/settings/wifi/b;Landroid/preference/Preference;)V
    .locals 1

    iput-object p1, p0, Lcom/android/settings/wifi/c;->akw:Lcom/android/settings/wifi/b;

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    invoke-direct {p0, v0}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    const-string/jumbo v0, "wifi_networks_available_notification_on"

    invoke-static {v0}, Landroid/provider/Settings$Global;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/wifi/c;->aku:Landroid/net/Uri;

    iput-object p2, p0, Lcom/android/settings/wifi/c;->akv:Landroid/preference/Preference;

    return-void
.end method


# virtual methods
.method public aaL(Landroid/content/ContentResolver;Z)V
    .locals 2

    if-eqz p2, :cond_0

    iget-object v0, p0, Lcom/android/settings/wifi/c;->aku:Landroid/net/Uri;

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1, p0}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p1, p0}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    goto :goto_0
.end method

.method public onChange(ZLandroid/net/Uri;)V
    .locals 2

    invoke-super {p0, p1, p2}, Landroid/database/ContentObserver;->onChange(ZLandroid/net/Uri;)V

    iget-object v0, p0, Lcom/android/settings/wifi/c;->aku:Landroid/net/Uri;

    invoke-virtual {v0, p2}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/wifi/c;->akw:Lcom/android/settings/wifi/b;

    iget-object v1, p0, Lcom/android/settings/wifi/c;->akv:Landroid/preference/Preference;

    invoke-virtual {v0, v1}, Lcom/android/settings/wifi/b;->cz(Landroid/preference/Preference;)V

    :cond_0
    return-void
.end method
