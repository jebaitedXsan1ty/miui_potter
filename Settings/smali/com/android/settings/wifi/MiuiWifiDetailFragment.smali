.class public Lcom/android/settings/wifi/MiuiWifiDetailFragment;
.super Lcom/android/settings/BaseEditFragment;
.source "MiuiWifiDetailFragment.java"

# interfaces
.implements Lcom/android/settings/wifi/I;


# instance fields
.field private asB:Lcom/android/settingslib/wifi/i;

.field private asC:Lcom/android/settings/wifi/H;

.field private asD:Z

.field private asE:Z

.field private asF:Z

.field private asG:Landroid/net/wifi/WifiConfiguration;

.field private asH:Lcom/android/settings/wifi/t;

.field private asI:I

.field private asJ:Lcom/android/settings/wifi/s;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/android/settings/BaseEditFragment;-><init>()V

    new-instance v0, Lcom/android/settings/wifi/bu;

    invoke-direct {v0, p0}, Lcom/android/settings/wifi/bu;-><init>(Lcom/android/settings/wifi/MiuiWifiDetailFragment;)V

    iput-object v0, p0, Lcom/android/settings/wifi/MiuiWifiDetailFragment;->asH:Lcom/android/settings/wifi/t;

    return-void
.end method

.method static synthetic aiA(Lcom/android/settings/wifi/MiuiWifiDetailFragment;)I
    .locals 1

    iget v0, p0, Lcom/android/settings/wifi/MiuiWifiDetailFragment;->asI:I

    return v0
.end method

.method static synthetic aiB(Lcom/android/settings/wifi/MiuiWifiDetailFragment;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/android/settings/wifi/MiuiWifiDetailFragment;->asD:Z

    return p1
.end method

.method static synthetic aiC(Lcom/android/settings/wifi/MiuiWifiDetailFragment;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/android/settings/wifi/MiuiWifiDetailFragment;->asE:Z

    return p1
.end method

.method static synthetic aiD(Lcom/android/settings/wifi/MiuiWifiDetailFragment;Landroid/net/wifi/WifiConfiguration;)Landroid/net/wifi/WifiConfiguration;
    .locals 0

    iput-object p1, p0, Lcom/android/settings/wifi/MiuiWifiDetailFragment;->asG:Landroid/net/wifi/WifiConfiguration;

    return-object p1
.end method

.method static synthetic aiE(Lcom/android/settings/wifi/MiuiWifiDetailFragment;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/wifi/MiuiWifiDetailFragment;->aix()V

    return-void
.end method

.method private aiu()V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/android/settings/wifi/MiuiWifiDetailFragment;->asJ:Lcom/android/settings/wifi/s;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/wifi/MiuiWifiDetailFragment;->asJ:Lcom/android/settings/wifi/s;

    invoke-virtual {v0}, Lcom/android/settings/wifi/s;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/wifi/MiuiWifiDetailFragment;->asJ:Lcom/android/settings/wifi/s;

    invoke-virtual {v0}, Lcom/android/settings/wifi/s;->dismiss()V

    :cond_0
    iput-object v1, p0, Lcom/android/settings/wifi/MiuiWifiDetailFragment;->asJ:Lcom/android/settings/wifi/s;

    return-void
.end method

.method private aiv()V
    .locals 8

    const/16 v2, 0x8

    const/4 v7, -0x1

    const/4 v3, 0x0

    invoke-virtual {p0}, Lcom/android/settings/wifi/MiuiWifiDetailFragment;->getView()Landroid/view/View;

    move-result-object v4

    const v0, 0x7f0a00bb

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iget v1, p0, Lcom/android/settings/wifi/MiuiWifiDetailFragment;->asI:I

    if-eq v1, v7, :cond_1

    invoke-virtual {p0}, Lcom/android/settings/wifi/MiuiWifiDetailFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    iget-object v5, p0, Lcom/android/settings/wifi/MiuiWifiDetailFragment;->asB:Lcom/android/settingslib/wifi/i;

    invoke-virtual {v5}, Lcom/android/settingslib/wifi/i;->chL()Landroid/net/wifi/WifiConfiguration;

    move-result-object v5

    invoke-static {v1, v5}, Lcom/android/settings/wifi/WifiSettings;->ahf(Landroid/content/Context;Landroid/net/wifi/WifiConfiguration;)Z

    move-result v5

    const v1, 0x7f0a00be

    invoke-virtual {v4, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    invoke-virtual {v1, v3}, Landroid/widget/Button;->setVisibility(I)V

    xor-int/lit8 v6, v5, 0x1

    invoke-virtual {v1, v6}, Landroid/widget/Button;->setEnabled(Z)V

    if-nez v5, :cond_0

    new-instance v6, Lcom/android/settings/wifi/bv;

    invoke-direct {v6, p0}, Lcom/android/settings/wifi/bv;-><init>(Lcom/android/settings/wifi/MiuiWifiDetailFragment;)V

    invoke-virtual {v1, v6}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_0
    sget-boolean v1, Lmiui/os/Build;->IS_CM_CUSTOMIZATION:Z

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/android/settings/wifi/MiuiWifiDetailFragment;->asB:Lcom/android/settingslib/wifi/i;

    iget-object v1, v1, Lcom/android/settingslib/wifi/i;->cCN:Landroid/net/wifi/WifiConfiguration;

    if-eqz v1, :cond_3

    const-string/jumbo v1, "CMCC"

    iget-object v6, p0, Lcom/android/settings/wifi/MiuiWifiDetailFragment;->asB:Lcom/android/settingslib/wifi/i;

    iget-object v6, v6, Lcom/android/settingslib/wifi/i;->cCN:Landroid/net/wifi/WifiConfiguration;

    iget-object v6, v6, Landroid/net/wifi/WifiConfiguration;->SSID:Ljava/lang/String;

    invoke-virtual {v1, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    :goto_0
    if-eqz v1, :cond_4

    move v1, v2

    :goto_1
    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    xor-int/lit8 v1, v5, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    if-nez v5, :cond_1

    new-instance v1, Lcom/android/settings/wifi/bw;

    invoke-direct {v1, p0}, Lcom/android/settings/wifi/bw;-><init>(Lcom/android/settings/wifi/MiuiWifiDetailFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_1
    const v0, 0x7f0a00bc

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iget v1, p0, Lcom/android/settings/wifi/MiuiWifiDetailFragment;->asI:I

    if-eq v1, v7, :cond_2

    if-eqz v0, :cond_2

    sget-boolean v1, Lmiui/os/Build;->IS_CM_CUSTOMIZATION:Z

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/android/settings/wifi/MiuiWifiDetailFragment;->asB:Lcom/android/settingslib/wifi/i;

    invoke-virtual {v1}, Lcom/android/settingslib/wifi/i;->chB()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setVisibility(I)V

    new-instance v1, Lcom/android/settings/wifi/bz;

    invoke-direct {v1, p0}, Lcom/android/settings/wifi/bz;-><init>(Lcom/android/settings/wifi/MiuiWifiDetailFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_2
    return-void

    :cond_3
    move v1, v3

    goto :goto_0

    :cond_4
    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v1

    if-eqz v1, :cond_5

    move v1, v2

    goto :goto_1

    :cond_5
    move v1, v3

    goto :goto_1
.end method

.method private aiw()V
    .locals 2

    iget v0, p0, Lcom/android/settings/wifi/MiuiWifiDetailFragment;->asI:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_1

    iget-object v0, p0, Lcom/android/settings/wifi/MiuiWifiDetailFragment;->asB:Lcom/android/settingslib/wifi/i;

    invoke-virtual {v0}, Lcom/android/settingslib/wifi/i;->chv()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/settings/wifi/MiuiWifiDetailFragment;->asB:Lcom/android/settingslib/wifi/i;

    iget-object v0, v0, Lcom/android/settingslib/wifi/i;->cCI:Landroid/net/wifi/ScanResult;

    invoke-static {v0}, Lcom/android/settings/wifi/g;->aaP(Landroid/net/wifi/ScanResult;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/android/settings/wifi/MiuiWifiDetailFragment;->getView()Landroid/view/View;

    move-result-object v0

    const v1, 0x7f0a0286

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    if-nez v0, :cond_0

    return-void

    :cond_0
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    new-instance v1, Lcom/android/settings/wifi/bA;

    invoke-direct {v1, p0}, Lcom/android/settings/wifi/bA;-><init>(Lcom/android/settings/wifi/MiuiWifiDetailFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_1
    return-void
.end method

.method private aix()V
    .locals 6

    invoke-direct {p0}, Lcom/android/settings/wifi/MiuiWifiDetailFragment;->aiu()V

    new-instance v0, Lcom/android/settings/wifi/s;

    invoke-virtual {p0}, Lcom/android/settings/wifi/MiuiWifiDetailFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    iget-object v2, p0, Lcom/android/settings/wifi/MiuiWifiDetailFragment;->asH:Lcom/android/settings/wifi/t;

    iget-object v3, p0, Lcom/android/settings/wifi/MiuiWifiDetailFragment;->asB:Lcom/android/settingslib/wifi/i;

    const/4 v4, 0x2

    const/4 v5, 0x0

    invoke-direct/range {v0 .. v5}, Lcom/android/settings/wifi/s;-><init>(Landroid/content/Context;Lcom/android/settings/wifi/t;Lcom/android/settingslib/wifi/i;IZ)V

    iput-object v0, p0, Lcom/android/settings/wifi/MiuiWifiDetailFragment;->asJ:Lcom/android/settings/wifi/s;

    iget-object v0, p0, Lcom/android/settings/wifi/MiuiWifiDetailFragment;->asJ:Lcom/android/settings/wifi/s;

    const v1, 0x1040013

    invoke-virtual {p0, v1}, Lcom/android/settings/wifi/MiuiWifiDetailFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/settings/wifi/s;->adC(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/android/settings/wifi/MiuiWifiDetailFragment;->asJ:Lcom/android/settings/wifi/s;

    invoke-virtual {v0}, Lcom/android/settings/wifi/s;->show()V

    return-void
.end method

.method private aiy()V
    .locals 3

    iget-object v0, p0, Lcom/android/settings/wifi/MiuiWifiDetailFragment;->asB:Lcom/android/settingslib/wifi/i;

    iget-object v0, v0, Lcom/android/settingslib/wifi/i;->cCN:Landroid/net/wifi/WifiConfiguration;

    if-nez v0, :cond_0

    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/android/settings/wifi/MiuiWifiDetailFragment;->getView()Landroid/view/View;

    move-result-object v0

    const v1, 0x7f0a0079

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    const v1, 0x7f0a007a

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lmiui/widget/SlidingButton;

    iget-boolean v1, p0, Lcom/android/settings/wifi/MiuiWifiDetailFragment;->asF:Z

    if-eqz v1, :cond_1

    iget-boolean v1, p0, Lcom/android/settings/wifi/MiuiWifiDetailFragment;->asD:Z

    :goto_0
    iput-boolean v1, p0, Lcom/android/settings/wifi/MiuiWifiDetailFragment;->asD:Z

    iget-boolean v1, p0, Lcom/android/settings/wifi/MiuiWifiDetailFragment;->asD:Z

    invoke-virtual {v0, v1}, Lmiui/widget/SlidingButton;->setChecked(Z)V

    new-instance v1, Lcom/android/settings/wifi/bB;

    invoke-direct {v1, p0}, Lcom/android/settings/wifi/bB;-><init>(Lcom/android/settings/wifi/MiuiWifiDetailFragment;)V

    invoke-virtual {v0, v1}, Lmiui/widget/SlidingButton;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    return-void

    :cond_1
    invoke-virtual {p0}, Lcom/android/settings/wifi/MiuiWifiDetailFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-static {v1}, Lcom/android/settings/wifi/k;->getInstance(Landroid/content/Context;)Lcom/android/settings/wifi/k;

    move-result-object v1

    iget-object v2, p0, Lcom/android/settings/wifi/MiuiWifiDetailFragment;->asB:Lcom/android/settingslib/wifi/i;

    iget-object v2, v2, Lcom/android/settingslib/wifi/i;->cCN:Landroid/net/wifi/WifiConfiguration;

    iget-object v2, v2, Landroid/net/wifi/WifiConfiguration;->SSID:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/android/settings/wifi/k;->abX(Ljava/lang/String;)Z

    move-result v1

    goto :goto_0
.end method

.method private aiz()V
    .locals 8

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/android/settings/wifi/MiuiWifiDetailFragment;->asB:Lcom/android/settingslib/wifi/i;

    iget-object v0, v0, Lcom/android/settingslib/wifi/i;->cCN:Landroid/net/wifi/WifiConfiguration;

    if-nez v0, :cond_0

    return-void

    :cond_0
    new-instance v0, Lcom/android/settingslib/D;

    invoke-virtual {p0}, Lcom/android/settings/wifi/MiuiWifiDetailFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-static {v2}, Landroid/net/NetworkPolicyManager;->from(Landroid/content/Context;)Landroid/net/NetworkPolicyManager;

    move-result-object v2

    invoke-direct {v0, v2}, Lcom/android/settingslib/D;-><init>(Landroid/net/NetworkPolicyManager;)V

    invoke-virtual {v0}, Lcom/android/settingslib/D;->csb()V

    iget-object v2, p0, Lcom/android/settings/wifi/MiuiWifiDetailFragment;->asB:Lcom/android/settingslib/wifi/i;

    iget-object v2, v2, Lcom/android/settingslib/wifi/i;->cCN:Landroid/net/wifi/WifiConfiguration;

    iget-object v2, v2, Landroid/net/wifi/WifiConfiguration;->SSID:Ljava/lang/String;

    invoke-static {v2}, Landroid/net/NetworkTemplate;->buildTemplateWifi(Ljava/lang/String;)Landroid/net/NetworkTemplate;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/android/settingslib/D;->crQ(Landroid/net/NetworkTemplate;)Landroid/net/NetworkPolicy;

    move-result-object v2

    invoke-virtual {p0}, Lcom/android/settings/wifi/MiuiWifiDetailFragment;->getView()Landroid/view/View;

    move-result-object v3

    const v0, 0x7f0a011c

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v5, 0x1b

    if-ge v0, v5, :cond_2

    move v0, v1

    :goto_0
    invoke-virtual {v4, v0}, Landroid/view/View;->setVisibility(I)V

    const v0, 0x7f0a011e

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lmiui/widget/SlidingButton;

    iget-boolean v3, p0, Lcom/android/settings/wifi/MiuiWifiDetailFragment;->asF:Z

    if-nez v3, :cond_1

    if-eqz v2, :cond_4

    iget-wide v4, v2, Landroid/net/NetworkPolicy;->limitBytes:J

    const-wide/16 v6, -0x1

    cmp-long v1, v4, v6

    if-eqz v1, :cond_3

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/android/settings/wifi/MiuiWifiDetailFragment;->asE:Z

    :cond_1
    :goto_1
    iget-boolean v1, p0, Lcom/android/settings/wifi/MiuiWifiDetailFragment;->asE:Z

    invoke-virtual {v0, v1}, Lmiui/widget/SlidingButton;->setChecked(Z)V

    new-instance v1, Lcom/android/settings/wifi/bC;

    invoke-direct {v1, p0}, Lcom/android/settings/wifi/bC;-><init>(Lcom/android/settings/wifi/MiuiWifiDetailFragment;)V

    invoke-virtual {v0, v1}, Lmiui/widget/SlidingButton;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    return-void

    :cond_2
    const/16 v0, 0x8

    goto :goto_0

    :cond_3
    iget-boolean v1, v2, Landroid/net/NetworkPolicy;->metered:Z

    iput-boolean v1, p0, Lcom/android/settings/wifi/MiuiWifiDetailFragment;->asE:Z

    goto :goto_1

    :cond_4
    iput-boolean v1, p0, Lcom/android/settings/wifi/MiuiWifiDetailFragment;->asE:Z

    goto :goto_1
.end method


# virtual methods
.method public PM(Z)V
    .locals 7

    const/4 v1, 0x0

    if-eqz p1, :cond_5

    iget-object v0, p0, Lcom/android/settings/wifi/MiuiWifiDetailFragment;->asB:Lcom/android/settingslib/wifi/i;

    iget-object v0, v0, Lcom/android/settingslib/wifi/i;->cCN:Landroid/net/wifi/WifiConfiguration;

    :goto_0
    iget-object v2, p0, Lcom/android/settings/wifi/MiuiWifiDetailFragment;->asG:Landroid/net/wifi/WifiConfiguration;

    if-eqz v2, :cond_0

    if-eqz v0, :cond_0

    iget-object v2, p0, Lcom/android/settings/wifi/MiuiWifiDetailFragment;->asG:Landroid/net/wifi/WifiConfiguration;

    new-instance v3, Landroid/net/IpConfiguration;

    invoke-virtual {v0}, Landroid/net/wifi/WifiConfiguration;->getIpAssignment()Landroid/net/IpConfiguration$IpAssignment;

    move-result-object v4

    invoke-virtual {v0}, Landroid/net/wifi/WifiConfiguration;->getProxySettings()Landroid/net/IpConfiguration$ProxySettings;

    move-result-object v5

    invoke-virtual {v0}, Landroid/net/wifi/WifiConfiguration;->getStaticIpConfiguration()Landroid/net/StaticIpConfiguration;

    move-result-object v6

    invoke-virtual {v0}, Landroid/net/wifi/WifiConfiguration;->getHttpProxy()Landroid/net/ProxyInfo;

    move-result-object v0

    invoke-direct {v3, v4, v5, v6, v0}, Landroid/net/IpConfiguration;-><init>(Landroid/net/IpConfiguration$IpAssignment;Landroid/net/IpConfiguration$ProxySettings;Landroid/net/StaticIpConfiguration;Landroid/net/ProxyInfo;)V

    invoke-virtual {v2, v3}, Landroid/net/wifi/WifiConfiguration;->setIpConfiguration(Landroid/net/IpConfiguration;)V

    iget-object v0, p0, Lcom/android/settings/wifi/MiuiWifiDetailFragment;->asG:Landroid/net/wifi/WifiConfiguration;

    :cond_0
    if-eqz v0, :cond_2

    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    const-string/jumbo v2, "network_id"

    iget v3, p0, Lcom/android/settings/wifi/MiuiWifiDetailFragment;->asI:I

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string/jumbo v2, "is_delete"

    invoke-virtual {v1, v2, p1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string/jumbo v2, "config"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    iget-object v2, p0, Lcom/android/settings/wifi/MiuiWifiDetailFragment;->asB:Lcom/android/settingslib/wifi/i;

    iget-object v2, v2, Lcom/android/settingslib/wifi/i;->cCN:Landroid/net/wifi/WifiConfiguration;

    if-eq v0, v2, :cond_2

    iget-object v2, p0, Lcom/android/settings/wifi/MiuiWifiDetailFragment;->asB:Lcom/android/settingslib/wifi/i;

    iget-object v2, v2, Lcom/android/settingslib/wifi/i;->cCN:Landroid/net/wifi/WifiConfiguration;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/android/settings/wifi/MiuiWifiDetailFragment;->asB:Lcom/android/settingslib/wifi/i;

    iget-object v2, v2, Lcom/android/settingslib/wifi/i;->cCN:Landroid/net/wifi/WifiConfiguration;

    iget-object v2, v2, Landroid/net/wifi/WifiConfiguration;->SSID:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    iget-object v2, p0, Lcom/android/settings/wifi/MiuiWifiDetailFragment;->asB:Lcom/android/settingslib/wifi/i;

    iget-object v2, v2, Lcom/android/settingslib/wifi/i;->cCN:Landroid/net/wifi/WifiConfiguration;

    iget-object v2, v2, Landroid/net/wifi/WifiConfiguration;->SSID:Ljava/lang/String;

    iput-object v2, v0, Landroid/net/wifi/WifiConfiguration;->SSID:Ljava/lang/String;

    iget-object v2, p0, Lcom/android/settings/wifi/MiuiWifiDetailFragment;->asB:Lcom/android/settingslib/wifi/i;

    iget-object v2, v2, Lcom/android/settingslib/wifi/i;->cCN:Landroid/net/wifi/WifiConfiguration;

    iget-object v2, v2, Landroid/net/wifi/WifiConfiguration;->BSSID:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    iget-object v2, p0, Lcom/android/settings/wifi/MiuiWifiDetailFragment;->asB:Lcom/android/settingslib/wifi/i;

    iget-object v2, v2, Lcom/android/settingslib/wifi/i;->cCN:Landroid/net/wifi/WifiConfiguration;

    iget-object v2, v2, Landroid/net/wifi/WifiConfiguration;->BSSID:Ljava/lang/String;

    iput-object v2, v0, Landroid/net/wifi/WifiConfiguration;->BSSID:Ljava/lang/String;

    :cond_1
    iget-object v2, p0, Lcom/android/settings/wifi/MiuiWifiDetailFragment;->asB:Lcom/android/settingslib/wifi/i;

    iget-object v2, v2, Lcom/android/settingslib/wifi/i;->cCN:Landroid/net/wifi/WifiConfiguration;

    iget-object v2, v2, Landroid/net/wifi/WifiConfiguration;->allowedKeyManagement:Ljava/util/BitSet;

    iput-object v2, v0, Landroid/net/wifi/WifiConfiguration;->allowedKeyManagement:Ljava/util/BitSet;

    iget-object v2, p0, Lcom/android/settings/wifi/MiuiWifiDetailFragment;->asB:Lcom/android/settingslib/wifi/i;

    iget-object v2, v2, Lcom/android/settingslib/wifi/i;->cCN:Landroid/net/wifi/WifiConfiguration;

    iget-boolean v2, v2, Landroid/net/wifi/WifiConfiguration;->hiddenSSID:Z

    iput-boolean v2, v0, Landroid/net/wifi/WifiConfiguration;->hiddenSSID:Z

    iget-object v2, p0, Lcom/android/settings/wifi/MiuiWifiDetailFragment;->asB:Lcom/android/settingslib/wifi/i;

    iget-object v2, v2, Lcom/android/settingslib/wifi/i;->cCN:Landroid/net/wifi/WifiConfiguration;

    iget v2, v2, Landroid/net/wifi/WifiConfiguration;->creatorUid:I

    iput v2, v0, Landroid/net/wifi/WifiConfiguration;->creatorUid:I

    iget-object v2, p0, Lcom/android/settings/wifi/MiuiWifiDetailFragment;->asG:Landroid/net/wifi/WifiConfiguration;

    if-nez v2, :cond_2

    iget-object v2, p0, Lcom/android/settings/wifi/MiuiWifiDetailFragment;->asB:Lcom/android/settingslib/wifi/i;

    iget-object v2, v2, Lcom/android/settingslib/wifi/i;->cCN:Landroid/net/wifi/WifiConfiguration;

    iget-object v2, v2, Landroid/net/wifi/WifiConfiguration;->enterpriseConfig:Landroid/net/wifi/WifiEnterpriseConfig;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/android/settings/wifi/MiuiWifiDetailFragment;->asB:Lcom/android/settingslib/wifi/i;

    iget-object v2, v2, Lcom/android/settingslib/wifi/i;->cCN:Landroid/net/wifi/WifiConfiguration;

    iget-object v2, v2, Landroid/net/wifi/WifiConfiguration;->enterpriseConfig:Landroid/net/wifi/WifiEnterpriseConfig;

    iput-object v2, v0, Landroid/net/wifi/WifiConfiguration;->enterpriseConfig:Landroid/net/wifi/WifiEnterpriseConfig;

    :cond_2
    iget-object v0, p0, Lcom/android/settings/wifi/MiuiWifiDetailFragment;->asB:Lcom/android/settingslib/wifi/i;

    iget-object v0, v0, Lcom/android/settingslib/wifi/i;->cCN:Landroid/net/wifi/WifiConfiguration;

    if-eqz v0, :cond_3

    invoke-virtual {p0}, Lcom/android/settings/wifi/MiuiWifiDetailFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/android/settings/wifi/k;->getInstance(Landroid/content/Context;)Lcom/android/settings/wifi/k;

    move-result-object v0

    invoke-virtual {p0}, Lcom/android/settings/wifi/MiuiWifiDetailFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    iget-object v3, p0, Lcom/android/settings/wifi/MiuiWifiDetailFragment;->asB:Lcom/android/settingslib/wifi/i;

    iget-object v3, v3, Lcom/android/settingslib/wifi/i;->cCN:Landroid/net/wifi/WifiConfiguration;

    iget-object v3, v3, Landroid/net/wifi/WifiConfiguration;->SSID:Ljava/lang/String;

    iget-boolean v4, p0, Lcom/android/settings/wifi/MiuiWifiDetailFragment;->asD:Z

    invoke-virtual {v0, v2, v3, v4}, Lcom/android/settings/wifi/k;->abV(Landroid/content/Context;Ljava/lang/String;Z)V

    new-instance v0, Lcom/android/settingslib/D;

    invoke-virtual {p0}, Lcom/android/settings/wifi/MiuiWifiDetailFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-static {v2}, Landroid/net/NetworkPolicyManager;->from(Landroid/content/Context;)Landroid/net/NetworkPolicyManager;

    move-result-object v2

    invoke-direct {v0, v2}, Lcom/android/settingslib/D;-><init>(Landroid/net/NetworkPolicyManager;)V

    invoke-virtual {v0}, Lcom/android/settingslib/D;->csb()V

    iget-object v2, p0, Lcom/android/settings/wifi/MiuiWifiDetailFragment;->asB:Lcom/android/settingslib/wifi/i;

    iget-object v2, v2, Lcom/android/settingslib/wifi/i;->cCN:Landroid/net/wifi/WifiConfiguration;

    iget-object v2, v2, Landroid/net/wifi/WifiConfiguration;->SSID:Ljava/lang/String;

    invoke-static {v2}, Landroid/net/NetworkTemplate;->buildTemplateWifi(Ljava/lang/String;)Landroid/net/NetworkTemplate;

    move-result-object v2

    iget-boolean v3, p0, Lcom/android/settings/wifi/MiuiWifiDetailFragment;->asE:Z

    invoke-virtual {v0, v2, v3}, Lcom/android/settingslib/D;->csa(Landroid/net/NetworkTemplate;Z)V

    :cond_3
    if-eqz p1, :cond_4

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/android/settings/wifi/MiuiWifiDetailFragment;->bea(Z)V

    :cond_4
    invoke-virtual {p0, v1}, Lcom/android/settings/wifi/MiuiWifiDetailFragment;->beb(Landroid/os/Bundle;)V

    return-void

    :cond_5
    iget-object v0, p0, Lcom/android/settings/wifi/MiuiWifiDetailFragment;->asC:Lcom/android/settings/wifi/H;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/android/settings/wifi/MiuiWifiDetailFragment;->asC:Lcom/android/settings/wifi/H;

    invoke-virtual {v0}, Lcom/android/settings/wifi/H;->afa()Landroid/net/wifi/WifiConfiguration;

    move-result-object v0

    goto/16 :goto_0

    :cond_6
    move-object v0, v1

    goto/16 :goto_0
.end method

.method public agW(Z)V
    .locals 0

    invoke-virtual {p0, p1}, Lcom/android/settings/wifi/MiuiWifiDetailFragment;->bea(Z)V

    return-void
.end method

.method public getTitle()Ljava/lang/String;
    .locals 3

    invoke-virtual {p0}, Lcom/android/settings/wifi/MiuiWifiDetailFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string/jumbo v1, "ssid"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object v0, v1, v2

    const v0, 0x7f120b3d

    invoke-virtual {p0, v0, v1}, Lcom/android/settings/wifi/MiuiWifiDetailFragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 6

    const/4 v4, 0x1

    invoke-super {p0, p1}, Lcom/android/settings/BaseEditFragment;->onActivityCreated(Landroid/os/Bundle;)V

    if-eqz p1, :cond_0

    const-string/jumbo v0, "is_autoConnect"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/settings/wifi/MiuiWifiDetailFragment;->asD:Z

    const-string/jumbo v0, "is_metered"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/settings/wifi/MiuiWifiDetailFragment;->asE:Z

    iput-boolean v4, p0, Lcom/android/settings/wifi/MiuiWifiDetailFragment;->asF:Z

    :cond_0
    invoke-virtual {p0}, Lcom/android/settings/wifi/MiuiWifiDetailFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {p0}, Lcom/android/settings/wifi/MiuiWifiDetailFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    const/4 v2, -0x1

    iput v2, p0, Lcom/android/settings/wifi/MiuiWifiDetailFragment;->asI:I

    new-instance v2, Lcom/android/settingslib/wifi/i;

    invoke-direct {v2, v0, v1}, Lcom/android/settingslib/wifi/i;-><init>(Landroid/content/Context;Landroid/os/Bundle;)V

    iput-object v2, p0, Lcom/android/settings/wifi/MiuiWifiDetailFragment;->asB:Lcom/android/settingslib/wifi/i;

    invoke-virtual {p0, v4}, Lcom/android/settings/wifi/MiuiWifiDetailFragment;->setHasOptionsMenu(Z)V

    invoke-virtual {p0}, Lcom/android/settings/wifi/MiuiWifiDetailFragment;->getView()Landroid/view/View;

    move-result-object v2

    iget-object v0, p0, Lcom/android/settings/wifi/MiuiWifiDetailFragment;->asB:Lcom/android/settingslib/wifi/i;

    iget v0, v0, Lcom/android/settingslib/wifi/i;->cCO:I

    iput v0, p0, Lcom/android/settings/wifi/MiuiWifiDetailFragment;->asI:I

    new-instance v0, Lcom/android/settings/wifi/H;

    invoke-virtual {p0}, Lcom/android/settings/wifi/MiuiWifiDetailFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    iget-object v3, p0, Lcom/android/settings/wifi/MiuiWifiDetailFragment;->asB:Lcom/android/settingslib/wifi/i;

    move-object v5, p0

    invoke-direct/range {v0 .. v5}, Lcom/android/settings/wifi/H;-><init>(Landroid/app/Activity;Landroid/view/View;Lcom/android/settingslib/wifi/i;ZLcom/android/settings/wifi/I;)V

    iput-object v0, p0, Lcom/android/settings/wifi/MiuiWifiDetailFragment;->asC:Lcom/android/settings/wifi/H;

    invoke-direct {p0}, Lcom/android/settings/wifi/MiuiWifiDetailFragment;->aiv()V

    invoke-direct {p0}, Lcom/android/settings/wifi/MiuiWifiDetailFragment;->aiw()V

    invoke-virtual {p0}, Lcom/android/settings/wifi/MiuiWifiDetailFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/android/settings/wifi/MiuiWifiDetailFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/app/Activity;->setRequestedOrientation(I)V

    :cond_1
    return-void
.end method

.method public onDestroy()V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/wifi/MiuiWifiDetailFragment;->aiu()V

    invoke-super {p0}, Lcom/android/settings/BaseEditFragment;->onDestroy()V

    return-void
.end method

.method public onResume()V
    .locals 1

    invoke-super {p0}, Lcom/android/settings/BaseEditFragment;->onResume()V

    invoke-direct {p0}, Lcom/android/settings/wifi/MiuiWifiDetailFragment;->aiy()V

    invoke-direct {p0}, Lcom/android/settings/wifi/MiuiWifiDetailFragment;->aiz()V

    iget-object v0, p0, Lcom/android/settings/wifi/MiuiWifiDetailFragment;->asC:Lcom/android/settings/wifi/H;

    invoke-virtual {v0}, Lcom/android/settings/wifi/H;->aeS()V

    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Lcom/android/settings/BaseEditFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    const-string/jumbo v0, "is_metered"

    iget-boolean v1, p0, Lcom/android/settings/wifi/MiuiWifiDetailFragment;->asE:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string/jumbo v0, "is_autoConnect"

    iget-boolean v1, p0, Lcom/android/settings/wifi/MiuiWifiDetailFragment;->asD:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    return-void
.end method

.method public vB(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2

    const v0, 0x7f0d0275

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method
