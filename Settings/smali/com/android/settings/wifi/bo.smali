.class final Lcom/android/settings/wifi/bo;
.super Ljava/lang/Object;
.source "WpsDialog.java"

# interfaces
.implements Ljava/lang/Runnable;


# static fields
.field private static final synthetic auM:[I


# instance fields
.field final synthetic auJ:Lcom/android/settings/wifi/S;

.field final synthetic auK:Lcom/android/settings/wifi/WpsDialog$DialogState;

.field final synthetic auL:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/android/settings/wifi/S;Lcom/android/settings/wifi/WpsDialog$DialogState;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/wifi/bo;->auJ:Lcom/android/settings/wifi/S;

    iput-object p2, p0, Lcom/android/settings/wifi/bo;->auK:Lcom/android/settings/wifi/WpsDialog$DialogState;

    iput-object p3, p0, Lcom/android/settings/wifi/bo;->auL:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static synthetic ajn()[I
    .locals 3

    sget-object v0, Lcom/android/settings/wifi/bo;->auM:[I

    if-eqz v0, :cond_0

    sget-object v0, Lcom/android/settings/wifi/bo;->auM:[I

    return-object v0

    :cond_0
    invoke-static {}, Lcom/android/settings/wifi/WpsDialog$DialogState;->values()[Lcom/android/settings/wifi/WpsDialog$DialogState;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Lcom/android/settings/wifi/WpsDialog$DialogState;->arJ:Lcom/android/settings/wifi/WpsDialog$DialogState;

    invoke-virtual {v1}, Lcom/android/settings/wifi/WpsDialog$DialogState;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_4

    :goto_0
    :try_start_1
    sget-object v1, Lcom/android/settings/wifi/WpsDialog$DialogState;->arK:Lcom/android/settings/wifi/WpsDialog$DialogState;

    invoke-virtual {v1}, Lcom/android/settings/wifi/WpsDialog$DialogState;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_3

    :goto_1
    :try_start_2
    sget-object v1, Lcom/android/settings/wifi/WpsDialog$DialogState;->arL:Lcom/android/settings/wifi/WpsDialog$DialogState;

    invoke-virtual {v1}, Lcom/android/settings/wifi/WpsDialog$DialogState;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_2

    :goto_2
    :try_start_3
    sget-object v1, Lcom/android/settings/wifi/WpsDialog$DialogState;->arM:Lcom/android/settings/wifi/WpsDialog$DialogState;

    invoke-virtual {v1}, Lcom/android/settings/wifi/WpsDialog$DialogState;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_1

    :goto_3
    :try_start_4
    sget-object v1, Lcom/android/settings/wifi/WpsDialog$DialogState;->arN:Lcom/android/settings/wifi/WpsDialog$DialogState;

    invoke-virtual {v1}, Lcom/android/settings/wifi/WpsDialog$DialogState;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_0

    :goto_4
    sput-object v0, Lcom/android/settings/wifi/bo;->auM:[I

    return-object v0

    :catch_0
    move-exception v1

    goto :goto_4

    :catch_1
    move-exception v1

    goto :goto_3

    :catch_2
    move-exception v1

    goto :goto_2

    :catch_3
    move-exception v1

    goto :goto_1

    :catch_4
    move-exception v1

    goto :goto_0
.end method


# virtual methods
.method public run()V
    .locals 5

    const/4 v4, 0x0

    const/16 v3, 0x8

    invoke-static {}, Lcom/android/settings/wifi/bo;->ajn()[I

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/wifi/bo;->auK:Lcom/android/settings/wifi/WpsDialog$DialogState;

    invoke-virtual {v1}, Lcom/android/settings/wifi/WpsDialog$DialogState;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/android/settings/wifi/bo;->auJ:Lcom/android/settings/wifi/S;

    invoke-static {v0}, Lcom/android/settings/wifi/S;->ahD(Lcom/android/settings/wifi/S;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/wifi/bo;->auL:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void

    :pswitch_0
    iget-object v0, p0, Lcom/android/settings/wifi/bo;->auJ:Lcom/android/settings/wifi/S;

    invoke-static {v0}, Lcom/android/settings/wifi/S;->ahE(Lcom/android/settings/wifi/S;)Landroid/widget/ProgressBar;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/widget/ProgressBar;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/settings/wifi/bo;->auJ:Lcom/android/settings/wifi/S;

    invoke-static {v0}, Lcom/android/settings/wifi/S;->ahB(Lcom/android/settings/wifi/S;)Landroid/widget/ProgressBar;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Lcom/android/settings/wifi/bo;->auJ:Lcom/android/settings/wifi/S;

    invoke-static {v0}, Lcom/android/settings/wifi/S;->ahy(Lcom/android/settings/wifi/S;)Landroid/widget/Button;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/wifi/bo;->auJ:Lcom/android/settings/wifi/S;

    invoke-static {v1}, Lcom/android/settings/wifi/S;->ahz(Lcom/android/settings/wifi/S;)Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f1205f7

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/android/settings/wifi/bo;->auJ:Lcom/android/settings/wifi/S;

    invoke-static {v0}, Lcom/android/settings/wifi/S;->ahE(Lcom/android/settings/wifi/S;)Landroid/widget/ProgressBar;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/widget/ProgressBar;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/settings/wifi/bo;->auJ:Lcom/android/settings/wifi/S;

    invoke-static {v0}, Lcom/android/settings/wifi/S;->ahB(Lcom/android/settings/wifi/S;)Landroid/widget/ProgressBar;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/widget/ProgressBar;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/settings/wifi/bo;->auJ:Lcom/android/settings/wifi/S;

    invoke-static {v0}, Lcom/android/settings/wifi/S;->ahC(Lcom/android/settings/wifi/S;)Landroid/content/BroadcastReceiver;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/wifi/bo;->auJ:Lcom/android/settings/wifi/S;

    invoke-static {v0}, Lcom/android/settings/wifi/S;->ahz(Lcom/android/settings/wifi/S;)Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/wifi/bo;->auJ:Lcom/android/settings/wifi/S;

    invoke-static {v1}, Lcom/android/settings/wifi/S;->ahC(Lcom/android/settings/wifi/S;)Landroid/content/BroadcastReceiver;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    iget-object v0, p0, Lcom/android/settings/wifi/bo;->auJ:Lcom/android/settings/wifi/S;

    invoke-static {v0, v4}, Lcom/android/settings/wifi/S;->ahF(Lcom/android/settings/wifi/S;Landroid/content/BroadcastReceiver;)Landroid/content/BroadcastReceiver;

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
