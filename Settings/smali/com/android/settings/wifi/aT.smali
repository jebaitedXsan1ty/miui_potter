.class final Lcom/android/settings/wifi/aT;
.super Ljava/lang/Object;
.source "SavedAccessPointsWifiSettings.java"

# interfaces
.implements Ljava/util/Comparator;


# instance fields
.field final auo:Landroid/icu/text/Collator;


# direct methods
.method constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {}, Landroid/icu/text/Collator;->getInstance()Landroid/icu/text/Collator;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/wifi/aT;->auo:Landroid/icu/text/Collator;

    return-void
.end method

.method private ajm(Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    if-nez p1, :cond_0

    const-string/jumbo p1, ""

    :cond_0
    return-object p1
.end method


# virtual methods
.method public ajl(Lcom/android/settingslib/wifi/i;Lcom/android/settingslib/wifi/i;)I
    .locals 3

    iget-object v0, p0, Lcom/android/settings/wifi/aT;->auo:Landroid/icu/text/Collator;

    invoke-virtual {p1}, Lcom/android/settingslib/wifi/i;->chx()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/android/settings/wifi/aT;->ajm(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2}, Lcom/android/settingslib/wifi/i;->chx()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/android/settings/wifi/aT;->ajm(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/icu/text/Collator;->compare(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1

    check-cast p1, Lcom/android/settingslib/wifi/i;

    check-cast p2, Lcom/android/settingslib/wifi/i;

    invoke-virtual {p0, p1, p2}, Lcom/android/settings/wifi/aT;->ajl(Lcom/android/settingslib/wifi/i;Lcom/android/settingslib/wifi/i;)I

    move-result v0

    return v0
.end method
