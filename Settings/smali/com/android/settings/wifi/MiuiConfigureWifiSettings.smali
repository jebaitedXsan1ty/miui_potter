.class public Lcom/android/settings/wifi/MiuiConfigureWifiSettings;
.super Lcom/android/settings/wifi/ConfigureWifiSettings;
.source "MiuiConfigureWifiSettings.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/wifi/ConfigureWifiSettings;-><init>()V

    return-void
.end method


# virtual methods
.method protected ar()Ljava/lang/String;
    .locals 1

    const-string/jumbo v0, "MiuiConfigureWifiSettings"

    return-object v0
.end method

.method protected getPreferenceControllers(Landroid/content/Context;)Ljava/util/List;
    .locals 2

    invoke-super {p0, p1}, Lcom/android/settings/wifi/ConfigureWifiSettings;->getPreferenceControllers(Landroid/content/Context;)Ljava/util/List;

    move-result-object v0

    new-instance v1, Lcom/android/settings/wifi/R;

    invoke-direct {v1, p1}, Lcom/android/settings/wifi/R;-><init>(Landroid/content/Context;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v1, Lcom/android/settings/wifi/z;

    invoke-direct {v1, p1}, Lcom/android/settings/wifi/z;-><init>(Landroid/content/Context;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object v0
.end method
