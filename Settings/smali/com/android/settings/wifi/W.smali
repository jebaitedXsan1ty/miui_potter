.class public Lcom/android/settings/wifi/W;
.super Lcom/android/settings/core/e;
.source "UseOpenWifiPreferenceController.java"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceChangeListener;
.implements Lcom/android/settings/core/lifecycle/b;
.implements Lcom/android/settings/core/lifecycle/a/b;
.implements Lcom/android/settings/core/lifecycle/a/d;


# static fields
.field static final REQUEST_CODE_OPEN_WIFI_AUTOMATICALLY:I = 0x190


# instance fields
.field private asm:Landroid/content/ComponentName;

.field private final asn:Landroid/app/Fragment;

.field private final aso:Lcom/android/settings/network/a;

.field private asp:Landroid/preference/Preference;

.field private final asq:Lcom/android/settings/wifi/X;

.field private final mContentResolver:Landroid/content/ContentResolver;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/app/Fragment;Lcom/android/settings/network/a;Lcom/android/settings/core/lifecycle/c;)V
    .locals 1

    invoke-direct {p0, p1}, Lcom/android/settings/core/e;-><init>(Landroid/content/Context;)V

    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/wifi/W;->mContentResolver:Landroid/content/ContentResolver;

    iput-object p2, p0, Lcom/android/settings/wifi/W;->asn:Landroid/app/Fragment;

    iput-object p3, p0, Lcom/android/settings/wifi/W;->aso:Lcom/android/settings/network/a;

    new-instance v0, Lcom/android/settings/wifi/X;

    invoke-direct {v0, p0}, Lcom/android/settings/wifi/X;-><init>(Lcom/android/settings/wifi/W;)V

    iput-object v0, p0, Lcom/android/settings/wifi/W;->asq:Lcom/android/settings/wifi/X;

    invoke-direct {p0}, Lcom/android/settings/wifi/W;->aii()V

    invoke-virtual {p4, p0}, Lcom/android/settings/core/lifecycle/c;->ajv(Lcom/android/settings/core/lifecycle/b;)Lcom/android/settings/core/lifecycle/b;

    return-void
.end method

.method private aih()Z
    .locals 3

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/android/settings/wifi/W;->mContentResolver:Landroid/content/ContentResolver;

    const-string/jumbo v2, "use_open_wifi_package"

    invoke-static {v1, v2}, Landroid/provider/Settings$Global;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/android/settings/wifi/W;->asm:Landroid/content/ComponentName;

    if-nez v2, :cond_0

    :goto_0
    invoke-static {v1, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    return v0

    :cond_0
    iget-object v0, p0, Lcom/android/settings/wifi/W;->asm:Landroid/content/ComponentName;

    invoke-virtual {v0}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private aii()V
    .locals 2

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/android/settings/wifi/W;->aso:Lcom/android/settings/network/a;

    invoke-virtual {v1}, Lcom/android/settings/network/a;->aMr()Landroid/net/NetworkScorerAppData;

    move-result-object v1

    if-nez v1, :cond_0

    :goto_0
    iput-object v0, p0, Lcom/android/settings/wifi/W;->asm:Landroid/content/ComponentName;

    return-void

    :cond_0
    invoke-virtual {v1}, Landroid/net/NetworkScorerAppData;->getEnableUseOpenWifiActivity()Landroid/content/ComponentName;

    move-result-object v0

    goto :goto_0
.end method

.method static synthetic aij(Lcom/android/settings/wifi/W;)Landroid/preference/Preference;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/wifi/W;->asp:Landroid/preference/Preference;

    return-object v0
.end method

.method static synthetic aik(Lcom/android/settings/wifi/W;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/wifi/W;->aii()V

    return-void
.end method


# virtual methods
.method public aig(II)Z
    .locals 3

    const/16 v0, 0x190

    if-eq p1, v0, :cond_0

    const/4 v0, 0x0

    return v0

    :cond_0
    const/4 v0, -0x1

    if-ne p2, v0, :cond_1

    iget-object v0, p0, Lcom/android/settings/wifi/W;->mContentResolver:Landroid/content/ContentResolver;

    const-string/jumbo v1, "use_open_wifi_package"

    iget-object v2, p0, Lcom/android/settings/wifi/W;->asm:Landroid/content/ComponentName;

    invoke-virtual {v2}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$Global;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    :cond_1
    const/4 v0, 0x1

    return v0
.end method

.method public cz(Landroid/preference/Preference;)V
    .locals 1

    instance-of v0, p1, Landroid/preference/CheckBoxPreference;

    if-nez v0, :cond_0

    return-void

    :cond_0
    check-cast p1, Landroid/preference/CheckBoxPreference;

    invoke-direct {p0}, Lcom/android/settings/wifi/W;->aih()Z

    move-result v0

    invoke-virtual {p1, v0}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    return-void
.end method

.method public i(Landroid/preference/PreferenceScreen;)V
    .locals 1

    invoke-super {p0, p1}, Lcom/android/settings/core/e;->i(Landroid/preference/PreferenceScreen;)V

    const-string/jumbo v0, "use_open_wifi_automatically"

    invoke-virtual {p1, v0}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/wifi/W;->asp:Landroid/preference/Preference;

    return-void
.end method

.method public l()Ljava/lang/String;
    .locals 1

    const-string/jumbo v0, "use_open_wifi_automatically"

    return-object v0
.end method

.method public onPause()V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/wifi/W;->asq:Lcom/android/settings/wifi/X;

    iget-object v1, p0, Lcom/android/settings/wifi/W;->mContentResolver:Landroid/content/ContentResolver;

    invoke-virtual {v0, v1}, Lcom/android/settings/wifi/X;->aim(Landroid/content/ContentResolver;)V

    return-void
.end method

.method public onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 4

    const/4 v3, 0x0

    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "use_open_wifi_automatically"

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/android/settings/wifi/W;->p()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_1

    :cond_0
    return v3

    :cond_1
    invoke-direct {p0}, Lcom/android/settings/wifi/W;->aih()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/settings/wifi/W;->mContentResolver:Landroid/content/ContentResolver;

    const-string/jumbo v1, "use_open_wifi_package"

    const-string/jumbo v2, ""

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$Global;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    const/4 v0, 0x1

    return v0

    :cond_2
    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v1, "android.net.scoring.CUSTOM_ENABLE"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/android/settings/wifi/W;->asm:Landroid/content/ComponentName;

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    iget-object v1, p0, Lcom/android/settings/wifi/W;->asn:Landroid/app/Fragment;

    const/16 v2, 0x190

    invoke-virtual {v1, v0, v2}, Landroid/app/Fragment;->startActivityForResult(Landroid/content/Intent;I)V

    return v3
.end method

.method public onResume()V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/wifi/W;->asq:Lcom/android/settings/wifi/X;

    iget-object v1, p0, Lcom/android/settings/wifi/W;->mContentResolver:Landroid/content/ContentResolver;

    invoke-virtual {v0, v1}, Lcom/android/settings/wifi/X;->ail(Landroid/content/ContentResolver;)V

    return-void
.end method

.method public p()Z
    .locals 1

    iget-object v0, p0, Lcom/android/settings/wifi/W;->asm:Landroid/content/ComponentName;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
