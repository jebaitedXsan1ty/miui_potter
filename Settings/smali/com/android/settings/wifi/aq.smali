.class final Lcom/android/settings/wifi/aq;
.super Landroid/database/ContentObserver;
.source "AutoConnectUtils.java"


# instance fields
.field final synthetic atD:Lcom/android/settings/wifi/k;

.field final synthetic val$context:Landroid/content/Context;


# direct methods
.method constructor <init>(Lcom/android/settings/wifi/k;Landroid/os/Handler;Landroid/content/Context;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/wifi/aq;->atD:Lcom/android/settings/wifi/k;

    iput-object p3, p0, Lcom/android/settings/wifi/aq;->val$context:Landroid/content/Context;

    invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    return-void
.end method


# virtual methods
.method public onChange(Z)V
    .locals 3

    iget-object v0, p0, Lcom/android/settings/wifi/aq;->atD:Lcom/android/settings/wifi/k;

    invoke-static {v0}, Lcom/android/settings/wifi/k;->aca(Lcom/android/settings/wifi/k;)Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/android/settings/wifi/aq;->atD:Lcom/android/settings/wifi/k;

    iget-object v2, p0, Lcom/android/settings/wifi/aq;->val$context:Landroid/content/Context;

    invoke-static {v2}, Landroid/provider/MiuiSettings$System;->getDisableWifiAutoConnectSsid(Landroid/content/Context;)Ljava/util/HashSet;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/android/settings/wifi/k;->acb(Lcom/android/settings/wifi/k;Ljava/util/HashSet;)Ljava/util/HashSet;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method
