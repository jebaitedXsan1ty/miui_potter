.class public Lcom/android/settings/wifi/p2p/a;
.super Landroid/preference/Preference;
.source "WifiP2pPeer.java"


# static fields
.field private static final aiO:[I


# instance fields
.field public aiP:Landroid/net/wifi/p2p/WifiP2pDevice;

.field private final aiQ:I

.field private aiR:Landroid/widget/ImageView;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/4 v0, 0x1

    new-array v0, v0, [I

    const v1, 0x7f04013f

    const/4 v2, 0x0

    aput v1, v0, v2

    sput-object v0, Lcom/android/settings/wifi/p2p/a;->aiO:[I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/net/wifi/p2p/WifiP2pDevice;)V
    .locals 2

    invoke-direct {p0, p1}, Landroid/preference/Preference;-><init>(Landroid/content/Context;)V

    iput-object p2, p0, Lcom/android/settings/wifi/p2p/a;->aiP:Landroid/net/wifi/p2p/WifiP2pDevice;

    const v0, 0x7f0d0162

    invoke-virtual {p0, v0}, Lcom/android/settings/wifi/p2p/a;->setWidgetLayoutResource(I)V

    const/16 v0, 0x3c

    iput v0, p0, Lcom/android/settings/wifi/p2p/a;->aiQ:I

    iget-object v0, p0, Lcom/android/settings/wifi/p2p/a;->aiP:Landroid/net/wifi/p2p/WifiP2pDevice;

    iget-object v0, v0, Landroid/net/wifi/p2p/WifiP2pDevice;->deviceName:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/wifi/p2p/a;->aiP:Landroid/net/wifi/p2p/WifiP2pDevice;

    iget-object v0, v0, Landroid/net/wifi/p2p/WifiP2pDevice;->deviceAddress:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/android/settings/wifi/p2p/a;->setTitle(Ljava/lang/CharSequence;)V

    :goto_0
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f030130

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/wifi/p2p/a;->aiP:Landroid/net/wifi/p2p/WifiP2pDevice;

    iget v1, v1, Landroid/net/wifi/p2p/WifiP2pDevice;->status:I

    aget-object v0, v0, v1

    invoke-virtual {p0, v0}, Lcom/android/settings/wifi/p2p/a;->setSummary(Ljava/lang/CharSequence;)V

    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/settings/wifi/p2p/a;->aiP:Landroid/net/wifi/p2p/WifiP2pDevice;

    iget-object v0, v0, Landroid/net/wifi/p2p/WifiP2pDevice;->deviceName:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/android/settings/wifi/p2p/a;->setTitle(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method


# virtual methods
.method Zp()I
    .locals 2

    iget v0, p0, Lcom/android/settings/wifi/p2p/a;->aiQ:I

    const v1, 0x7fffffff

    if-ne v0, v1, :cond_0

    const/4 v0, -0x1

    return v0

    :cond_0
    iget v0, p0, Lcom/android/settings/wifi/p2p/a;->aiQ:I

    const/4 v1, 0x5

    invoke-static {v0, v1}, Landroid/net/wifi/MiuiWifiManager;->calculateSignalLevel(II)I

    move-result v0

    return v0
.end method

.method public compareTo(Landroid/preference/Preference;)I
    .locals 3

    const/4 v0, 0x1

    instance-of v1, p1, Lcom/android/settings/wifi/p2p/a;

    if-nez v1, :cond_0

    return v0

    :cond_0
    check-cast p1, Lcom/android/settings/wifi/p2p/a;

    iget-object v1, p0, Lcom/android/settings/wifi/p2p/a;->aiP:Landroid/net/wifi/p2p/WifiP2pDevice;

    iget v1, v1, Landroid/net/wifi/p2p/WifiP2pDevice;->status:I

    iget-object v2, p1, Lcom/android/settings/wifi/p2p/a;->aiP:Landroid/net/wifi/p2p/WifiP2pDevice;

    iget v2, v2, Landroid/net/wifi/p2p/WifiP2pDevice;->status:I

    if-eq v1, v2, :cond_2

    iget-object v1, p0, Lcom/android/settings/wifi/p2p/a;->aiP:Landroid/net/wifi/p2p/WifiP2pDevice;

    iget v1, v1, Landroid/net/wifi/p2p/WifiP2pDevice;->status:I

    iget-object v2, p1, Lcom/android/settings/wifi/p2p/a;->aiP:Landroid/net/wifi/p2p/WifiP2pDevice;

    iget v2, v2, Landroid/net/wifi/p2p/WifiP2pDevice;->status:I

    if-ge v1, v2, :cond_1

    const/4 v0, -0x1

    :cond_1
    return v0

    :cond_2
    iget-object v0, p0, Lcom/android/settings/wifi/p2p/a;->aiP:Landroid/net/wifi/p2p/WifiP2pDevice;

    iget-object v0, v0, Landroid/net/wifi/p2p/WifiP2pDevice;->deviceName:Ljava/lang/String;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/android/settings/wifi/p2p/a;->aiP:Landroid/net/wifi/p2p/WifiP2pDevice;

    iget-object v0, v0, Landroid/net/wifi/p2p/WifiP2pDevice;->deviceName:Ljava/lang/String;

    iget-object v1, p1, Lcom/android/settings/wifi/p2p/a;->aiP:Landroid/net/wifi/p2p/WifiP2pDevice;

    iget-object v1, v1, Landroid/net/wifi/p2p/WifiP2pDevice;->deviceName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v0

    return v0

    :cond_3
    iget-object v0, p0, Lcom/android/settings/wifi/p2p/a;->aiP:Landroid/net/wifi/p2p/WifiP2pDevice;

    iget-object v0, v0, Landroid/net/wifi/p2p/WifiP2pDevice;->deviceAddress:Ljava/lang/String;

    iget-object v1, p1, Lcom/android/settings/wifi/p2p/a;->aiP:Landroid/net/wifi/p2p/WifiP2pDevice;

    iget-object v1, v1, Landroid/net/wifi/p2p/WifiP2pDevice;->deviceAddress:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public onBindView(Landroid/view/View;)V
    .locals 3

    invoke-super {p0, p1}, Landroid/preference/Preference;->onBindView(Landroid/view/View;)V

    const v0, 0x7f0a0403

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/android/settings/wifi/p2p/a;->aiR:Landroid/widget/ImageView;

    iget v0, p0, Lcom/android/settings/wifi/p2p/a;->aiQ:I

    const v1, 0x7fffffff

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/android/settings/wifi/p2p/a;->aiR:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    :goto_0
    const v0, 0x7f0a0133

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/settings/wifi/p2p/a;->aiR:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/android/settings/wifi/p2p/a;->Zp()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageLevel(I)V

    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/settings/wifi/p2p/a;->aiR:Landroid/widget/ImageView;

    const v1, 0x7f08045e

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    iget-object v0, p0, Lcom/android/settings/wifi/p2p/a;->aiR:Landroid/widget/ImageView;

    sget-object v1, Lcom/android/settings/wifi/p2p/a;->aiO:[I

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/widget/ImageView;->setImageState([IZ)V

    goto :goto_0
.end method
