.class Lcom/android/settings/wifi/v;
.super Ljava/lang/Object;
.source "MiuiTetherDeviceSettings.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;
.implements Landroid/content/DialogInterface$OnDismissListener;


# instance fields
.field private anA:Landroid/app/AlertDialog;

.field final synthetic anB:Lcom/android/settings/wifi/MiuiTetherDeviceSettings;

.field private any:Z

.field private anz:Lcom/android/settings/wifi/D;


# direct methods
.method private constructor <init>(Lcom/android/settings/wifi/MiuiTetherDeviceSettings;Lcom/android/settings/wifi/D;)V
    .locals 5

    iput-object p1, p0, Lcom/android/settings/wifi/v;->anB:Lcom/android/settings/wifi/MiuiTetherDeviceSettings;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/android/settings/wifi/v;->anz:Lcom/android/settings/wifi/D;

    invoke-virtual {p1}, Lcom/android/settings/wifi/MiuiTetherDeviceSettings;->getActivity()Landroid/app/Activity;

    move-result-object v1

    iget-object v0, p0, Lcom/android/settings/wifi/v;->anz:Lcom/android/settings/wifi/D;

    invoke-virtual {v0}, Lcom/android/settings/wifi/D;->afO()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v2, ""

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/wifi/v;->anz:Lcom/android/settings/wifi/D;

    invoke-virtual {v0}, Lcom/android/settings/wifi/D;->afP()Ljava/lang/String;

    move-result-object v0

    :goto_0
    const v2, 0x7f120279

    invoke-virtual {v1, v2}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v2, Landroid/app/AlertDialog$Builder;

    invoke-direct {v2, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v3, 0x7f12027a

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x1010355

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setIconAttribute(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x104000a

    invoke-virtual {v0, v1, p0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const/high16 v1, 0x1040000

    invoke-virtual {v0, v1, p0}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/wifi/v;->anA:Landroid/app/AlertDialog;

    iget-object v0, p0, Lcom/android/settings/wifi/v;->anA:Landroid/app/AlertDialog;

    invoke-virtual {v0, p0}, Landroid/app/AlertDialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/settings/wifi/v;->anz:Lcom/android/settings/wifi/D;

    invoke-virtual {v0}, Lcom/android/settings/wifi/D;->afO()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method synthetic constructor <init>(Lcom/android/settings/wifi/MiuiTetherDeviceSettings;Lcom/android/settings/wifi/D;Lcom/android/settings/wifi/v;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/android/settings/wifi/v;-><init>(Lcom/android/settings/wifi/MiuiTetherDeviceSettings;Lcom/android/settings/wifi/D;)V

    return-void
.end method


# virtual methods
.method public aet()Lcom/android/settings/wifi/D;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/wifi/v;->anz:Lcom/android/settings/wifi/D;

    return-object v0
.end method

.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 1

    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/android/settings/wifi/v;->any:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onDismiss(Landroid/content/DialogInterface;)V
    .locals 4

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/android/settings/wifi/v;->anB:Lcom/android/settings/wifi/MiuiTetherDeviceSettings;

    invoke-static {v0, v3}, Lcom/android/settings/wifi/MiuiTetherDeviceSettings;->aeq(Lcom/android/settings/wifi/MiuiTetherDeviceSettings;Z)Z

    iget-boolean v0, p0, Lcom/android/settings/wifi/v;->any:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/wifi/v;->anB:Lcom/android/settings/wifi/MiuiTetherDeviceSettings;

    invoke-static {v0}, Lcom/android/settings/wifi/MiuiTetherDeviceSettings;->ael(Lcom/android/settings/wifi/MiuiTetherDeviceSettings;)Ljava/util/Set;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/wifi/v;->anz:Lcom/android/settings/wifi/D;

    invoke-virtual {v1}, Lcom/android/settings/wifi/D;->afP()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/android/settings/wifi/v;->anB:Lcom/android/settings/wifi/MiuiTetherDeviceSettings;

    iget-object v1, p0, Lcom/android/settings/wifi/v;->anz:Lcom/android/settings/wifi/D;

    invoke-static {v0, v1}, Lcom/android/settings/wifi/MiuiTetherDeviceSettings;->aes(Lcom/android/settings/wifi/MiuiTetherDeviceSettings;Lcom/android/settings/wifi/D;)V

    invoke-static {}, Lcom/android/settings/dc;->getInstance()Lcom/android/settings/dc;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/wifi/v;->anB:Lcom/android/settings/wifi/MiuiTetherDeviceSettings;

    invoke-virtual {v1}, Lcom/android/settings/wifi/MiuiTetherDeviceSettings;->getActivity()Landroid/app/Activity;

    move-result-object v1

    iget-object v2, p0, Lcom/android/settings/wifi/v;->anB:Lcom/android/settings/wifi/MiuiTetherDeviceSettings;

    invoke-static {v2}, Lcom/android/settings/wifi/MiuiTetherDeviceSettings;->ael(Lcom/android/settings/wifi/MiuiTetherDeviceSettings;)Ljava/util/Set;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/android/settings/dc;->bsi(Landroid/content/Context;Ljava/util/Set;)V

    iput-boolean v3, p0, Lcom/android/settings/wifi/v;->any:Z

    :cond_0
    return-void
.end method

.method public show()V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/wifi/v;->anB:Lcom/android/settings/wifi/MiuiTetherDeviceSettings;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/android/settings/wifi/MiuiTetherDeviceSettings;->aeq(Lcom/android/settings/wifi/MiuiTetherDeviceSettings;Z)Z

    iget-object v0, p0, Lcom/android/settings/wifi/v;->anA:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    return-void
.end method
