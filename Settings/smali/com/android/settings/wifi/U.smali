.class public final Lcom/android/settings/wifi/U;
.super Lcom/android/settings/widget/w;
.source "WifiSummaryUpdater.java"


# static fields
.field private static final ask:Landroid/content/IntentFilter;


# instance fields
.field private final asl:Lcom/android/settingslib/wifi/k;

.field private final mReceiver:Landroid/content/BroadcastReceiver;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    sput-object v0, Lcom/android/settings/wifi/U;->ask:Landroid/content/IntentFilter;

    sget-object v0, Lcom/android/settings/wifi/U;->ask:Landroid/content/IntentFilter;

    const-string/jumbo v1, "android.net.wifi.WIFI_STATE_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    sget-object v0, Lcom/android/settings/wifi/U;->ask:Landroid/content/IntentFilter;

    const-string/jumbo v1, "android.net.wifi.STATE_CHANGE"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    sget-object v0, Lcom/android/settings/wifi/U;->ask:Landroid/content/IntentFilter;

    const-string/jumbo v1, "android.net.wifi.RSSI_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/android/settings/widget/x;)V
    .locals 2

    new-instance v1, Lcom/android/settingslib/wifi/k;

    const-class v0, Landroid/net/wifi/WifiManager;

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/WifiManager;

    invoke-direct {v1, v0}, Lcom/android/settingslib/wifi/k;-><init>(Landroid/net/wifi/WifiManager;)V

    invoke-direct {p0, p1, p2, v1}, Lcom/android/settings/wifi/U;-><init>(Landroid/content/Context;Lcom/android/settings/widget/x;Lcom/android/settingslib/wifi/k;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/android/settings/widget/x;Lcom/android/settingslib/wifi/k;)V
    .locals 1

    invoke-direct {p0, p1, p2}, Lcom/android/settings/widget/w;-><init>(Landroid/content/Context;Lcom/android/settings/widget/x;)V

    iput-object p3, p0, Lcom/android/settings/wifi/U;->asl:Lcom/android/settingslib/wifi/k;

    new-instance v0, Lcom/android/settings/wifi/bt;

    invoke-direct {v0, p0}, Lcom/android/settings/wifi/bt;-><init>(Lcom/android/settings/wifi/U;)V

    iput-object v0, p0, Lcom/android/settings/wifi/U;->mReceiver:Landroid/content/BroadcastReceiver;

    return-void
.end method

.method static synthetic aic(Lcom/android/settings/wifi/U;)Lcom/android/settingslib/wifi/k;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/wifi/U;->asl:Lcom/android/settingslib/wifi/k;

    return-object v0
.end method

.method static synthetic aid(Lcom/android/settings/wifi/U;)V
    .locals 0

    invoke-virtual {p0}, Lcom/android/settings/wifi/U;->aBK()V

    return-void
.end method


# virtual methods
.method public aib(Z)V
    .locals 3

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/android/settings/wifi/U;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/android/settings/wifi/U;->mReceiver:Landroid/content/BroadcastReceiver;

    sget-object v2, Lcom/android/settings/wifi/U;->ask:Landroid/content/IntentFilter;

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/settings/wifi/U;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/android/settings/wifi/U;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    goto :goto_0
.end method

.method public getSummary()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/android/settings/wifi/U;->asl:Lcom/android/settingslib/wifi/k;

    iget-boolean v0, v0, Lcom/android/settingslib/wifi/k;->enabled:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/wifi/U;->mContext:Landroid/content/Context;

    const v1, 0x7f12120f

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/android/settings/wifi/U;->asl:Lcom/android/settingslib/wifi/k;

    iget-boolean v0, v0, Lcom/android/settingslib/wifi/k;->cDg:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/android/settings/wifi/U;->mContext:Landroid/content/Context;

    const v1, 0x7f1205e0

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/android/settings/wifi/U;->asl:Lcom/android/settingslib/wifi/k;

    iget-object v0, v0, Lcom/android/settingslib/wifi/k;->cDa:Ljava/lang/String;

    invoke-static {v0}, Landroid/net/wifi/WifiInfo;->removeDoubleQuotes(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
