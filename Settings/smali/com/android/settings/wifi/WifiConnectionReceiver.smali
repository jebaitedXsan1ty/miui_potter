.class public Lcom/android/settings/wifi/WifiConnectionReceiver;
.super Landroid/content/BroadcastReceiver;
.source "WifiConnectionReceiver.java"


# static fields
.field private static apB:Z

.field private static apC:Z

.field private static apD:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput-boolean v0, Lcom/android/settings/wifi/WifiConnectionReceiver;->apD:Z

    sput-boolean v0, Lcom/android/settings/wifi/WifiConnectionReceiver;->apC:Z

    const/4 v0, 0x1

    sput-boolean v0, Lcom/android/settings/wifi/WifiConnectionReceiver;->apB:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method private afX(Landroid/content/Context;Z)Landroid/net/wifi/WifiConfiguration;
    .locals 10

    const/4 v9, 0x1

    const/4 v1, 0x0

    const-string/jumbo v0, "wifi"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/WifiManager;

    invoke-virtual {v0}, Landroid/net/wifi/WifiManager;->getConfiguredNetworks()Ljava/util/List;

    move-result-object v2

    invoke-virtual {v0}, Landroid/net/wifi/WifiManager;->getScanResults()Ljava/util/List;

    move-result-object v4

    invoke-static {}, Lcom/android/settings/dc;->getInstance()Lcom/android/settings/dc;

    move-result-object v5

    const/high16 v0, -0x80000000

    if-eqz v2, :cond_4

    if-eqz v4, :cond_4

    invoke-interface {v2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v6

    move-object v2, v1

    move v3, v0

    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/WifiConfiguration;

    if-eqz p2, :cond_0

    if-eqz v2, :cond_0

    iget v1, v2, Landroid/net/wifi/WifiConfiguration;->status:I

    if-eq v1, v9, :cond_0

    iget v1, v0, Landroid/net/wifi/WifiConfiguration;->status:I

    if-ne v1, v9, :cond_0

    move-object v0, v2

    move v1, v3

    :goto_1
    move-object v2, v0

    move v3, v1

    goto :goto_0

    :cond_0
    invoke-interface {v4}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_1
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_6

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/net/wifi/ScanResult;

    invoke-virtual {v5, v0, v1}, Lcom/android/settings/dc;->bYK(Landroid/net/wifi/WifiConfiguration;Landroid/net/wifi/ScanResult;)Z

    move-result v8

    if-eqz v8, :cond_1

    if-eqz p2, :cond_2

    if-eqz v2, :cond_2

    iget v7, v2, Landroid/net/wifi/WifiConfiguration;->status:I

    if-ne v7, v9, :cond_2

    iget v7, v0, Landroid/net/wifi/WifiConfiguration;->status:I

    if-eq v7, v9, :cond_2

    iget v3, v1, Landroid/net/wifi/ScanResult;->level:I

    :goto_2
    invoke-interface {v4, v1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    move v1, v3

    goto :goto_1

    :cond_2
    iget v7, v1, Landroid/net/wifi/ScanResult;->level:I

    if-le v7, v3, :cond_5

    iget v3, v1, Landroid/net/wifi/ScanResult;->level:I

    goto :goto_2

    :cond_3
    move-object v1, v2

    :cond_4
    return-object v1

    :cond_5
    move-object v0, v2

    goto :goto_2

    :cond_6
    move-object v0, v2

    move v1, v3

    goto :goto_1
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 7

    const/4 v6, -0x1

    const v5, 0x10008000

    const/4 v4, 0x1

    const/4 v1, 0x0

    const/4 v3, 0x0

    sget-boolean v0, Lmiui/os/Build;->IS_CM_CUSTOMIZATION:Z

    if-nez v0, :cond_0

    return-void

    :cond_0
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v2, "miui.intent.action.CONNECTIVITY_CHANGED"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    const-string/jumbo v0, "networkInfo"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/NetworkInfo;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->getType()I

    move-result v2

    if-ne v2, v4, :cond_2

    sget-boolean v2, Lcom/android/settings/wifi/WifiConnectionReceiver;->apD:Z

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v0

    sput-boolean v0, Lcom/android/settings/wifi/WifiConnectionReceiver;->apD:Z

    if-eqz v2, :cond_1

    sget-boolean v0, Lcom/android/settings/wifi/WifiConnectionReceiver;->apD:Z

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_1

    sput-boolean v4, Lcom/android/settings/wifi/WifiConnectionReceiver;->apB:Z

    invoke-static {p1}, Lcom/android/settings/wifi/WifiConnectionDialog;->agL(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-static {p1}, Landroid/telephony/TelephonyManager;->from(Landroid/content/Context;)Landroid/telephony/TelephonyManager;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/telephony/TelephonyManager;->setDataEnabled(Z)V

    const-string/jumbo v0, "wifi"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/WifiManager;

    invoke-virtual {v0}, Landroid/net/wifi/WifiManager;->isWifiEnabled()Z

    move-result v0

    if-nez v0, :cond_1

    sput-boolean v3, Lcom/android/settings/wifi/WifiConnectionReceiver;->apB:Z

    new-instance v2, Landroid/content/Intent;

    const-string/jumbo v0, "miui.intent.action.SELECT_WIFI_AP"

    invoke-direct {v2, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string/jumbo v3, "extra_best_ap"

    move-object v0, v1

    check-cast v0, Landroid/net/wifi/WifiConfiguration;

    invoke-virtual {v2, v3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    invoke-virtual {v2, v5}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    invoke-virtual {p1, v2}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    :cond_1
    :goto_0
    return-void

    :cond_2
    invoke-virtual {v0}, Landroid/net/NetworkInfo;->getType()I

    move-result v1

    if-nez v1, :cond_1

    sget-boolean v1, Lcom/android/settings/wifi/WifiConnectionReceiver;->apC:Z

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v0

    sput-boolean v0, Lcom/android/settings/wifi/WifiConnectionReceiver;->apC:Z

    if-nez v1, :cond_1

    sget-boolean v0, Lcom/android/settings/wifi/WifiConnectionReceiver;->apC:Z

    if-eqz v0, :cond_1

    sput-boolean v4, Lcom/android/settings/wifi/WifiConnectionReceiver;->apB:Z

    goto :goto_0

    :cond_3
    const-string/jumbo v1, "miui.intent.action.SWITCH_TO_WIFI"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_8

    invoke-static {p1}, Lcom/android/settings/wifi/WifiConnectionDialog;->agL(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_8

    sget-boolean v0, Lcom/android/settings/wifi/WifiConnectionReceiver;->apB:Z

    if-nez v0, :cond_4

    invoke-static {p1}, Lcom/android/settings/wifi/WifiConnectionDialog;->agM(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-static {p1}, Lcom/android/settings/wifi/WifiConnectionDialog;->agN(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_4
    const-string/jumbo v0, "wifi"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/WifiManager;

    invoke-virtual {v0}, Landroid/net/wifi/WifiManager;->getConnectionInfo()Landroid/net/wifi/WifiInfo;

    move-result-object v0

    if-eqz v0, :cond_5

    invoke-virtual {v0}, Landroid/net/wifi/WifiInfo;->getNetworkId()I

    move-result v0

    if-ne v0, v6, :cond_7

    :cond_5
    invoke-direct {p0, p1, v3}, Lcom/android/settings/wifi/WifiConnectionReceiver;->afX(Landroid/content/Context;Z)Landroid/net/wifi/WifiConfiguration;

    move-result-object v0

    if-eqz v0, :cond_6

    sput-boolean v3, Lcom/android/settings/wifi/WifiConnectionReceiver;->apB:Z

    new-instance v1, Landroid/content/Intent;

    const-string/jumbo v2, "miui.intent.action.SWITCH_TO_WIFI"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string/jumbo v2, "extra_best_ap"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    invoke-virtual {v1, v5}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    invoke-virtual {p1, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    :cond_6
    return-void

    :cond_7
    sput-boolean v3, Lcom/android/settings/wifi/WifiConnectionReceiver;->apB:Z

    goto :goto_0

    :cond_8
    const-string/jumbo v1, "miui.intent.action.SELECT_WIFI_AP"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    sget-boolean v0, Lcom/android/settings/wifi/WifiConnectionReceiver;->apB:Z

    if-eqz v0, :cond_1

    invoke-static {p1}, Lcom/android/settings/wifi/WifiConnectionDialog;->agL(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    sput-boolean v3, Lcom/android/settings/wifi/WifiConnectionReceiver;->apB:Z

    const-string/jumbo v0, "wifi"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/WifiManager;

    invoke-virtual {v0}, Landroid/net/wifi/WifiManager;->getConnectionInfo()Landroid/net/wifi/WifiInfo;

    move-result-object v0

    if-eqz v0, :cond_9

    invoke-virtual {v0}, Landroid/net/wifi/WifiInfo;->getNetworkId()I

    move-result v0

    if-ne v0, v6, :cond_1

    :cond_9
    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v1, "miui.intent.action.SELECT_WIFI_AP"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string/jumbo v1, "extra_best_ap"

    invoke-direct {p0, p1, v4}, Lcom/android/settings/wifi/WifiConnectionReceiver;->afX(Landroid/content/Context;Z)Landroid/net/wifi/WifiConfiguration;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    invoke-virtual {v0, v5}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    invoke-virtual {p1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0
.end method
