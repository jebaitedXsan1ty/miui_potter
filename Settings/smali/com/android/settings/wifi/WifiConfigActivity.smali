.class public Lcom/android/settings/wifi/WifiConfigActivity;
.super Landroid/app/Activity;
.source "WifiConfigActivity.java"

# interfaces
.implements Lcom/android/settings/wifi/t;


# instance fields
.field private apf:Lcom/android/settingslib/wifi/i;

.field private apg:Lcom/android/settings/wifi/s;

.field private aph:Landroid/net/wifi/WifiManager;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method


# virtual methods
.method public adG(Lcom/android/settings/wifi/s;)V
    .locals 0

    return-void
.end method

.method public adH(Lcom/android/settings/wifi/s;)V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/android/settings/wifi/WifiConfigActivity;->apg:Lcom/android/settings/wifi/s;

    invoke-virtual {v0}, Lcom/android/settings/wifi/s;->adw()Lcom/android/settings/wifi/A;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/settings/wifi/A;->afa()Landroid/net/wifi/WifiConfiguration;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/android/settings/wifi/WifiConfigActivity;->aph:Landroid/net/wifi/WifiManager;

    invoke-virtual {v1, v0, v2}, Landroid/net/wifi/WifiManager;->save(Landroid/net/wifi/WifiConfiguration;Landroid/net/wifi/WifiManager$ActionListener;)V

    iget-object v1, p0, Lcom/android/settings/wifi/WifiConfigActivity;->aph:Landroid/net/wifi/WifiManager;

    iget v0, v0, Landroid/net/wifi/WifiConfiguration;->networkId:I

    invoke-virtual {v1, v0, v2}, Landroid/net/wifi/WifiManager;->connect(ILandroid/net/wifi/WifiManager$ActionListener;)V

    :cond_0
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    const-string/jumbo v0, "wifi"

    invoke-virtual {p0, v0}, Lcom/android/settings/wifi/WifiConfigActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/WifiManager;

    iput-object v0, p0, Lcom/android/settings/wifi/WifiConfigActivity;->aph:Landroid/net/wifi/WifiManager;

    const-string/jumbo v0, "statusbar"

    invoke-virtual {p0, v0}, Lcom/android/settings/wifi/WifiConfigActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/StatusBarManager;

    invoke-virtual {v0}, Landroid/app/StatusBarManager;->collapsePanels()V

    invoke-virtual {p0}, Lcom/android/settings/wifi/WifiConfigActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "wifi_config"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/WifiConfiguration;

    new-instance v1, Lcom/android/settingslib/wifi/i;

    invoke-direct {v1, p0, v0}, Lcom/android/settingslib/wifi/i;-><init>(Landroid/content/Context;Landroid/net/wifi/WifiConfiguration;)V

    iput-object v1, p0, Lcom/android/settings/wifi/WifiConfigActivity;->apf:Lcom/android/settingslib/wifi/i;

    return-void
.end method

.method protected onDestroy()V
    .locals 1

    iget-object v0, p0, Lcom/android/settings/wifi/WifiConfigActivity;->apg:Lcom/android/settings/wifi/s;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/wifi/WifiConfigActivity;->apg:Lcom/android/settings/wifi/s;

    invoke-virtual {v0}, Lcom/android/settings/wifi/s;->dismiss()V

    :cond_0
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    return-void
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 2

    invoke-super {p0, p1}, Landroid/app/Activity;->onNewIntent(Landroid/content/Intent;)V

    const-string/jumbo v0, "wifi_config"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/WifiConfiguration;

    new-instance v1, Lcom/android/settingslib/wifi/i;

    invoke-direct {v1, p0, v0}, Lcom/android/settingslib/wifi/i;-><init>(Landroid/content/Context;Landroid/net/wifi/WifiConfiguration;)V

    iput-object v1, p0, Lcom/android/settings/wifi/WifiConfigActivity;->apf:Lcom/android/settingslib/wifi/i;

    return-void
.end method

.method public onResume()V
    .locals 6

    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    new-instance v0, Lcom/android/settings/wifi/s;

    iget-object v3, p0, Lcom/android/settings/wifi/WifiConfigActivity;->apf:Lcom/android/settingslib/wifi/i;

    const/4 v4, 0x3

    const/4 v5, 0x0

    move-object v1, p0

    move-object v2, p0

    invoke-direct/range {v0 .. v5}, Lcom/android/settings/wifi/s;-><init>(Landroid/content/Context;Lcom/android/settings/wifi/t;Lcom/android/settingslib/wifi/i;IZ)V

    iput-object v0, p0, Lcom/android/settings/wifi/WifiConfigActivity;->apg:Lcom/android/settings/wifi/s;

    iget-object v0, p0, Lcom/android/settings/wifi/WifiConfigActivity;->apg:Lcom/android/settings/wifi/s;

    new-instance v1, Lcom/android/settings/wifi/aK;

    invoke-direct {v1, p0}, Lcom/android/settings/wifi/aK;-><init>(Lcom/android/settings/wifi/WifiConfigActivity;)V

    invoke-virtual {v0, v1}, Lcom/android/settings/wifi/s;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    iget-object v0, p0, Lcom/android/settings/wifi/WifiConfigActivity;->apg:Lcom/android/settings/wifi/s;

    invoke-virtual {v0}, Lcom/android/settings/wifi/s;->show()V

    return-void
.end method

.method protected onStart()V
    .locals 1

    invoke-super {p0}, Landroid/app/Activity;->onStart()V

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/android/settings/wifi/WifiConfigActivity;->setVisible(Z)V

    return-void
.end method
