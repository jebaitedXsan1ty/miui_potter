.class Lcom/android/settings/wifi/e;
.super Landroid/database/ContentObserver;
.source "WifiWakeupPreferenceController.java"


# instance fields
.field final synthetic akA:Lcom/android/settings/wifi/d;

.field private final aky:Landroid/net/Uri;

.field private final akz:Landroid/preference/Preference;


# direct methods
.method public constructor <init>(Lcom/android/settings/wifi/d;Landroid/preference/Preference;)V
    .locals 1

    iput-object p1, p0, Lcom/android/settings/wifi/e;->akA:Lcom/android/settings/wifi/d;

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    invoke-direct {p0, v0}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    const-string/jumbo v0, "network_recommendations_enabled"

    invoke-static {v0}, Landroid/provider/Settings$Global;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/wifi/e;->aky:Landroid/net/Uri;

    iput-object p2, p0, Lcom/android/settings/wifi/e;->akz:Landroid/preference/Preference;

    return-void
.end method


# virtual methods
.method public aaM(Landroid/content/ContentResolver;Z)V
    .locals 2

    if-eqz p2, :cond_0

    iget-object v0, p0, Lcom/android/settings/wifi/e;->aky:Landroid/net/Uri;

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1, p0}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p1, p0}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    goto :goto_0
.end method

.method public onChange(ZLandroid/net/Uri;)V
    .locals 2

    invoke-super {p0, p1, p2}, Landroid/database/ContentObserver;->onChange(ZLandroid/net/Uri;)V

    iget-object v0, p0, Lcom/android/settings/wifi/e;->aky:Landroid/net/Uri;

    invoke-virtual {v0, p2}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/wifi/e;->akA:Lcom/android/settings/wifi/d;

    iget-object v1, p0, Lcom/android/settings/wifi/e;->akz:Landroid/preference/Preference;

    invoke-virtual {v0, v1}, Lcom/android/settings/wifi/d;->cz(Landroid/preference/Preference;)V

    :cond_0
    return-void
.end method
