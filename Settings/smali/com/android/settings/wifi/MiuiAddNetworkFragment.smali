.class public Lcom/android/settings/wifi/MiuiAddNetworkFragment;
.super Lcom/android/settings/BaseEditFragment;
.source "MiuiAddNetworkFragment.java"

# interfaces
.implements Landroid/text/TextWatcher;
.implements Landroid/widget/AdapterView$OnItemSelectedListener;
.implements Landroid/widget/CompoundButton$OnCheckedChangeListener;


# instance fields
.field private amK:I

.field private amL:Ljava/lang/String;

.field private amM:Ljava/lang/String;

.field private amN:Landroid/widget/TextView;

.field private amO:Landroid/widget/Spinner;

.field private amP:Landroid/widget/TextView;

.field private amQ:Landroid/widget/TextView;

.field private amR:Landroid/widget/Spinner;

.field private amS:Landroid/widget/Spinner;

.field private amT:Ljava/lang/String;

.field private amU:Landroid/widget/TextView;

.field private amV:Landroid/widget/ArrayAdapter;

.field private amW:Landroid/widget/ArrayAdapter;

.field private amX:Landroid/widget/ArrayAdapter;

.field private amY:Landroid/widget/Spinner;

.field private amZ:Landroid/widget/Spinner;

.field ana:Lcom/android/settings/wifi/J;

.field private anb:Landroid/widget/Spinner;

.field private anc:Ljava/util/ArrayList;

.field private and:Landroid/widget/TextView;

.field private ane:Landroid/telephony/TelephonyManager;

.field private anf:Landroid/os/Handler;

.field private ang:Ljava/lang/String;

.field private anh:Ljava/lang/String;

.field private ani:Landroid/view/View;

.field private mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/BaseEditFragment;-><init>()V

    return-void
.end method

.method private adI()V
    .locals 5

    const/16 v4, 0x8

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->and:Landroid/widget/TextView;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->and:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->length()I

    move-result v0

    if-lez v0, :cond_1

    iget-object v0, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->and:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getVisibility()I

    move-result v0

    if-eq v0, v4, :cond_1

    move v0, v1

    :goto_0
    if-eqz v0, :cond_3

    iget-object v3, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->amU:Landroid/widget/TextView;

    if-eqz v3, :cond_3

    iget v3, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->amK:I

    if-ne v3, v1, :cond_2

    iget-object v1, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->amU:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->length()I

    move-result v1

    if-nez v1, :cond_2

    :cond_0
    :goto_1
    invoke-virtual {p0, v2}, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->bea(Z)V

    return-void

    :cond_1
    move v0, v2

    goto :goto_0

    :cond_2
    iget v1, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->amK:I

    const/4 v3, 0x2

    if-ne v1, v3, :cond_3

    iget-object v1, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->amU:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->length()I

    move-result v1

    if-lt v1, v4, :cond_0

    :cond_3
    move v2, v0

    goto :goto_1
.end method

.method private adJ()Landroid/net/wifi/WifiConfiguration;
    .locals 10

    const/4 v9, 0x3

    const/16 v4, 0x22

    const/4 v8, 0x1

    const/4 v7, 0x0

    const/4 v6, 0x0

    new-instance v1, Landroid/net/wifi/WifiConfiguration;

    invoke-direct {v1}, Landroid/net/wifi/WifiConfiguration;-><init>()V

    iget-object v0, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->and:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/android/settingslib/wifi/i;->chG(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Landroid/net/wifi/WifiConfiguration;->SSID:Ljava/lang/String;

    iput-boolean v8, v1, Landroid/net/wifi/WifiConfiguration;->hiddenSSID:Z

    iget v0, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->amK:I

    packed-switch v0, :pswitch_data_0

    return-object v7

    :pswitch_0
    iget-object v0, v1, Landroid/net/wifi/WifiConfiguration;->allowedKeyManagement:Ljava/util/BitSet;

    invoke-virtual {v0, v6}, Ljava/util/BitSet;->set(I)V

    :cond_0
    :goto_0
    return-object v1

    :pswitch_1
    iget-object v0, v1, Landroid/net/wifi/WifiConfiguration;->allowedKeyManagement:Ljava/util/BitSet;

    invoke-virtual {v0, v6}, Ljava/util/BitSet;->set(I)V

    iget-object v0, v1, Landroid/net/wifi/WifiConfiguration;->allowedAuthAlgorithms:Ljava/util/BitSet;

    invoke-virtual {v0, v6}, Ljava/util/BitSet;->set(I)V

    iget-object v0, v1, Landroid/net/wifi/WifiConfiguration;->allowedAuthAlgorithms:Ljava/util/BitSet;

    invoke-virtual {v0, v8}, Ljava/util/BitSet;->set(I)V

    iget-object v0, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->amU:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->length()I

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->amU:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->length()I

    move-result v0

    iget-object v2, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->amU:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    const/16 v3, 0xa

    if-eq v0, v3, :cond_1

    const/16 v3, 0x1a

    if-ne v0, v3, :cond_2

    :cond_1
    const-string/jumbo v0, "[0-9A-Fa-f]*"

    invoke-virtual {v2, v0}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, v1, Landroid/net/wifi/WifiConfiguration;->wepKeys:[Ljava/lang/String;

    aput-object v2, v0, v6

    goto :goto_0

    :cond_2
    const/16 v3, 0x20

    if-eq v0, v3, :cond_1

    :cond_3
    iget-object v0, v1, Landroid/net/wifi/WifiConfiguration;->wepKeys:[Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v6

    goto :goto_0

    :pswitch_2
    iget-object v0, v1, Landroid/net/wifi/WifiConfiguration;->allowedKeyManagement:Ljava/util/BitSet;

    invoke-virtual {v0, v8}, Ljava/util/BitSet;->set(I)V

    iget-object v0, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->amU:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->length()I

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->amU:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v2, "[0-9A-Fa-f]{64}"

    invoke-virtual {v0, v2}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_4

    iput-object v0, v1, Landroid/net/wifi/WifiConfiguration;->preSharedKey:Ljava/lang/String;

    goto :goto_0

    :cond_4
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Landroid/net/wifi/WifiConfiguration;->preSharedKey:Ljava/lang/String;

    goto/16 :goto_0

    :pswitch_3
    iget-object v0, v1, Landroid/net/wifi/WifiConfiguration;->allowedKeyManagement:Ljava/util/BitSet;

    const/4 v2, 0x2

    invoke-virtual {v0, v2}, Ljava/util/BitSet;->set(I)V

    iget-object v0, v1, Landroid/net/wifi/WifiConfiguration;->allowedKeyManagement:Ljava/util/BitSet;

    invoke-virtual {v0, v9}, Ljava/util/BitSet;->set(I)V

    new-instance v0, Landroid/net/wifi/WifiEnterpriseConfig;

    invoke-direct {v0}, Landroid/net/wifi/WifiEnterpriseConfig;-><init>()V

    iput-object v0, v1, Landroid/net/wifi/WifiConfiguration;->enterpriseConfig:Landroid/net/wifi/WifiEnterpriseConfig;

    iget-object v0, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->amR:Landroid/widget/Spinner;

    invoke-virtual {v0}, Landroid/widget/Spinner;->getSelectedItemPosition()I

    move-result v2

    iget-object v0, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->amY:Landroid/widget/Spinner;

    invoke-virtual {v0}, Landroid/widget/Spinner;->getSelectedItemPosition()I

    move-result v0

    iget-object v3, v1, Landroid/net/wifi/WifiConfiguration;->enterpriseConfig:Landroid/net/wifi/WifiEnterpriseConfig;

    invoke-virtual {v3, v2}, Landroid/net/wifi/WifiEnterpriseConfig;->setEapMethod(I)V

    packed-switch v2, :pswitch_data_1

    :pswitch_4
    iget-object v3, v1, Landroid/net/wifi/WifiConfiguration;->enterpriseConfig:Landroid/net/wifi/WifiEnterpriseConfig;

    invoke-virtual {v3, v0}, Landroid/net/wifi/WifiEnterpriseConfig;->setPhase2Method(I)V

    :goto_1
    iget-object v0, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->amO:Landroid/widget/Spinner;

    invoke-virtual {v0}, Landroid/widget/Spinner;->getSelectedItem()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iget-object v3, v1, Landroid/net/wifi/WifiConfiguration;->enterpriseConfig:Landroid/net/wifi/WifiEnterpriseConfig;

    invoke-virtual {v3, v7}, Landroid/net/wifi/WifiEnterpriseConfig;->setCaCertificateAliases([Ljava/lang/String;)V

    iget-object v3, v1, Landroid/net/wifi/WifiConfiguration;->enterpriseConfig:Landroid/net/wifi/WifiEnterpriseConfig;

    invoke-virtual {v3, v7}, Landroid/net/wifi/WifiEnterpriseConfig;->setCaPath(Ljava/lang/String;)V

    iget-object v3, v1, Landroid/net/wifi/WifiConfiguration;->enterpriseConfig:Landroid/net/wifi/WifiEnterpriseConfig;

    iget-object v4, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->amP:Landroid/widget/TextView;

    invoke-virtual {v4}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v4

    invoke-interface {v4}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/net/wifi/WifiEnterpriseConfig;->setDomainSuffixMatch(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->ang:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_5

    iget-object v3, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->amM:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_a

    :cond_5
    :goto_2
    iget-object v0, v1, Landroid/net/wifi/WifiConfiguration;->enterpriseConfig:Landroid/net/wifi/WifiEnterpriseConfig;

    invoke-virtual {v0}, Landroid/net/wifi/WifiEnterpriseConfig;->getCaCertificateAliases()[Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_6

    iget-object v0, v1, Landroid/net/wifi/WifiConfiguration;->enterpriseConfig:Landroid/net/wifi/WifiEnterpriseConfig;

    invoke-virtual {v0}, Landroid/net/wifi/WifiEnterpriseConfig;->getCaPath()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_6

    const-string/jumbo v0, "MiuiAddNetworkFragment"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "ca_cert ("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, v1, Landroid/net/wifi/WifiConfiguration;->enterpriseConfig:Landroid/net/wifi/WifiEnterpriseConfig;

    invoke-virtual {v4}, Landroid/net/wifi/WifiEnterpriseConfig;->getCaCertificateAliases()[Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, ") and ca_path ("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, v1, Landroid/net/wifi/WifiConfiguration;->enterpriseConfig:Landroid/net/wifi/WifiEnterpriseConfig;

    invoke-virtual {v4}, Landroid/net/wifi/WifiEnterpriseConfig;->getCaPath()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, ") should not both be non-null"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_6
    iget-object v0, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->amS:Landroid/widget/Spinner;

    invoke-virtual {v0}, Landroid/widget/Spinner;->getSelectedItem()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iget-object v3, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->ang:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_7

    iget-object v3, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->amL:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_8

    :cond_7
    const-string/jumbo v0, ""

    :cond_8
    iget-object v3, v1, Landroid/net/wifi/WifiConfiguration;->enterpriseConfig:Landroid/net/wifi/WifiEnterpriseConfig;

    invoke-virtual {v3, v0}, Landroid/net/wifi/WifiEnterpriseConfig;->setClientCertificateAlias(Ljava/lang/String;)V

    const/4 v0, 0x4

    if-eq v2, v0, :cond_9

    const/4 v0, 0x5

    if-ne v2, v0, :cond_c

    :cond_9
    iget-object v0, v1, Landroid/net/wifi/WifiConfiguration;->enterpriseConfig:Landroid/net/wifi/WifiEnterpriseConfig;

    const-string/jumbo v2, ""

    invoke-virtual {v0, v2}, Landroid/net/wifi/WifiEnterpriseConfig;->setIdentity(Ljava/lang/String;)V

    iget-object v0, v1, Landroid/net/wifi/WifiConfiguration;->enterpriseConfig:Landroid/net/wifi/WifiEnterpriseConfig;

    const-string/jumbo v2, ""

    invoke-virtual {v0, v2}, Landroid/net/wifi/WifiEnterpriseConfig;->setAnonymousIdentity(Ljava/lang/String;)V

    :goto_3
    iget-object v0, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->amU:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->isShown()Z

    move-result v0

    if-eqz v0, :cond_e

    iget-object v0, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->amU:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->length()I

    move-result v0

    if-lez v0, :cond_0

    iget-object v0, v1, Landroid/net/wifi/WifiConfiguration;->enterpriseConfig:Landroid/net/wifi/WifiEnterpriseConfig;

    iget-object v2, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->amU:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/net/wifi/WifiEnterpriseConfig;->setPassword(Ljava/lang/String;)V

    goto/16 :goto_0

    :pswitch_5
    packed-switch v0, :pswitch_data_2

    const-string/jumbo v3, "MiuiAddNetworkFragment"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "Unknown phase2 method"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    :pswitch_6
    iget-object v0, v1, Landroid/net/wifi/WifiConfiguration;->enterpriseConfig:Landroid/net/wifi/WifiEnterpriseConfig;

    invoke-virtual {v0, v6}, Landroid/net/wifi/WifiEnterpriseConfig;->setPhase2Method(I)V

    goto/16 :goto_1

    :pswitch_7
    iget-object v0, v1, Landroid/net/wifi/WifiConfiguration;->enterpriseConfig:Landroid/net/wifi/WifiEnterpriseConfig;

    invoke-virtual {v0, v9}, Landroid/net/wifi/WifiEnterpriseConfig;->setPhase2Method(I)V

    goto/16 :goto_1

    :pswitch_8
    iget-object v0, v1, Landroid/net/wifi/WifiConfiguration;->enterpriseConfig:Landroid/net/wifi/WifiEnterpriseConfig;

    const/4 v3, 0x4

    invoke-virtual {v0, v3}, Landroid/net/wifi/WifiEnterpriseConfig;->setPhase2Method(I)V

    goto/16 :goto_1

    :pswitch_9
    iget-object v0, v1, Landroid/net/wifi/WifiConfiguration;->enterpriseConfig:Landroid/net/wifi/WifiEnterpriseConfig;

    const/4 v3, 0x5

    invoke-virtual {v0, v3}, Landroid/net/wifi/WifiEnterpriseConfig;->setPhase2Method(I)V

    goto/16 :goto_1

    :pswitch_a
    iget-object v0, v1, Landroid/net/wifi/WifiConfiguration;->enterpriseConfig:Landroid/net/wifi/WifiEnterpriseConfig;

    const/4 v3, 0x6

    invoke-virtual {v0, v3}, Landroid/net/wifi/WifiEnterpriseConfig;->setPhase2Method(I)V

    goto/16 :goto_1

    :pswitch_b
    iget-object v0, v1, Landroid/net/wifi/WifiConfiguration;->enterpriseConfig:Landroid/net/wifi/WifiEnterpriseConfig;

    const/4 v3, 0x7

    invoke-virtual {v0, v3}, Landroid/net/wifi/WifiEnterpriseConfig;->setPhase2Method(I)V

    goto/16 :goto_1

    :pswitch_c
    iget-object v0, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->ana:Lcom/android/settings/wifi/J;

    iget-object v3, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->anb:Landroid/widget/Spinner;

    invoke-virtual {v3}, Landroid/widget/Spinner;->getSelectedItemPosition()I

    move-result v3

    invoke-virtual {v0, v3}, Lcom/android/settings/wifi/J;->getItemId(I)J

    move-result-wide v4

    long-to-int v0, v4

    invoke-static {}, Lcom/android/settings/dc;->getInstance()Lcom/android/settings/dc;

    move-result-object v3

    iget-object v4, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mContext:Landroid/content/Context;

    invoke-virtual {v3, v4, v1, v0}, Lcom/android/settings/dc;->bsh(Landroid/content/Context;Landroid/net/wifi/WifiConfiguration;I)V

    goto/16 :goto_1

    :cond_a
    iget-object v3, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->anh:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_b

    iget-object v0, v1, Landroid/net/wifi/WifiConfiguration;->enterpriseConfig:Landroid/net/wifi/WifiEnterpriseConfig;

    const-string/jumbo v3, "/system/etc/security/cacerts"

    invoke-virtual {v0, v3}, Landroid/net/wifi/WifiEnterpriseConfig;->setCaPath(Ljava/lang/String;)V

    goto/16 :goto_2

    :cond_b
    iget-object v3, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->amT:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_5

    iget-object v3, v1, Landroid/net/wifi/WifiConfiguration;->enterpriseConfig:Landroid/net/wifi/WifiEnterpriseConfig;

    new-array v4, v8, [Ljava/lang/String;

    aput-object v0, v4, v6

    invoke-virtual {v3, v4}, Landroid/net/wifi/WifiEnterpriseConfig;->setCaCertificateAliases([Ljava/lang/String;)V

    goto/16 :goto_2

    :cond_c
    const/4 v0, 0x6

    if-eq v2, v0, :cond_9

    if-ne v2, v9, :cond_d

    iget-object v0, v1, Landroid/net/wifi/WifiConfiguration;->enterpriseConfig:Landroid/net/wifi/WifiEnterpriseConfig;

    iget-object v2, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->amQ:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/net/wifi/WifiEnterpriseConfig;->setIdentity(Ljava/lang/String;)V

    iget-object v0, v1, Landroid/net/wifi/WifiConfiguration;->enterpriseConfig:Landroid/net/wifi/WifiEnterpriseConfig;

    const-string/jumbo v2, ""

    invoke-virtual {v0, v2}, Landroid/net/wifi/WifiEnterpriseConfig;->setAnonymousIdentity(Ljava/lang/String;)V

    goto/16 :goto_3

    :cond_d
    iget-object v0, v1, Landroid/net/wifi/WifiConfiguration;->enterpriseConfig:Landroid/net/wifi/WifiEnterpriseConfig;

    iget-object v2, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->amQ:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/net/wifi/WifiEnterpriseConfig;->setIdentity(Ljava/lang/String;)V

    iget-object v0, v1, Landroid/net/wifi/WifiConfiguration;->enterpriseConfig:Landroid/net/wifi/WifiEnterpriseConfig;

    iget-object v2, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->amN:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/net/wifi/WifiEnterpriseConfig;->setAnonymousIdentity(Ljava/lang/String;)V

    goto/16 :goto_3

    :cond_e
    iget-object v0, v1, Landroid/net/wifi/WifiConfiguration;->enterpriseConfig:Landroid/net/wifi/WifiEnterpriseConfig;

    iget-object v2, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->amU:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/net/wifi/WifiEnterpriseConfig;->setPassword(Ljava/lang/String;)V

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_5
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_c
        :pswitch_c
        :pswitch_c
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x0
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
    .end packed-switch
.end method

.method private adK()V
    .locals 6

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mContext:Landroid/content/Context;

    invoke-static {v0}, Landroid/telephony/SubscriptionManager;->from(Landroid/content/Context;)Landroid/telephony/SubscriptionManager;

    move-result-object v3

    move v0, v1

    :goto_0
    iget-object v2, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->ane:Landroid/telephony/TelephonyManager;

    invoke-virtual {v2}, Landroid/telephony/TelephonyManager;->getSimCount()I

    move-result v2

    if-ge v0, v2, :cond_1

    invoke-virtual {v3, v0}, Landroid/telephony/SubscriptionManager;->getActiveSubscriptionInfoForSimSlotIndex(I)Landroid/telephony/SubscriptionInfo;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {v2}, Landroid/telephony/SubscriptionInfo;->getDisplayName()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    :goto_1
    iget-object v4, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->anc:Ljava/util/ArrayList;

    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    iget-object v2, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mContext:Landroid/content/Context;

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    add-int/lit8 v5, v0, 0x1

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v1

    const v5, 0x7f1210a0

    invoke-virtual {v2, v5, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    goto :goto_1

    :cond_1
    return-void
.end method

.method private adL()V
    .locals 5

    const v4, 0x1090008

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->getView()Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->ani:Landroid/view/View;

    iput v2, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->amK:I

    iget-object v0, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->ani:Landroid/view/View;

    const v1, 0x7f0a0425

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->and:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->and:Landroid/widget/TextView;

    invoke-virtual {v0, p0}, Landroid/widget/TextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    iget-object v0, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->ani:Landroid/view/View;

    const v1, 0x7f0a04c3

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->anf:Landroid/os/Handler;

    iget-object v0, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->ani:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mContext:Landroid/content/Context;

    iget-object v0, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->ani:Landroid/view/View;

    const v1, 0x7f0a0306

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->amU:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->amU:Landroid/widget/TextView;

    invoke-virtual {v0, p0}, Landroid/widget/TextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    iget-object v0, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mContext:Landroid/content/Context;

    const-string/jumbo v1, "phone"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    iput-object v0, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->ane:Landroid/telephony/TelephonyManager;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->anc:Ljava/util/ArrayList;

    iget-object v0, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mContext:Landroid/content/Context;

    const v1, 0x7f121626

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->ang:Ljava/lang/String;

    iget-object v0, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mContext:Landroid/content/Context;

    const v1, 0x7f121564

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->amT:Ljava/lang/String;

    iget-object v0, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mContext:Landroid/content/Context;

    const v1, 0x7f121628

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->anh:Ljava/lang/String;

    iget-object v0, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mContext:Landroid/content/Context;

    const v1, 0x7f121526

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->amL:Ljava/lang/String;

    iget-object v0, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mContext:Landroid/content/Context;

    const v1, 0x7f121527

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->amM:Ljava/lang/String;

    iget-object v0, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->ani:Landroid/view/View;

    const v1, 0x7f0a03cb

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    iput-object v0, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->amZ:Landroid/widget/Spinner;

    iget-object v0, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->amZ:Landroid/widget/Spinner;

    invoke-virtual {v0, v2}, Landroid/widget/Spinner;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->amZ:Landroid/widget/Spinner;

    invoke-virtual {v0, p0}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    iget-object v0, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->amZ:Landroid/widget/Spinner;

    iget-object v1, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->amZ:Landroid/widget/Spinner;

    invoke-virtual {v1}, Landroid/widget/Spinner;->getPrompt()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setPrompt(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->amZ:Landroid/widget/Spinner;

    invoke-virtual {v0}, Landroid/widget/Spinner;->getAdapter()Landroid/widget/SpinnerAdapter;

    move-result-object v0

    check-cast v0, Landroid/widget/ArrayAdapter;

    sget v1, Lmiui/R$layout;->simple_spinner_dropdown_item:I

    invoke-virtual {v0, v1}, Landroid/widget/ArrayAdapter;->setDropDownViewResource(I)V

    iget-object v0, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->ani:Landroid/view/View;

    const v1, 0x7f0a0408

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    iput-object v0, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->anb:Landroid/widget/Spinner;

    iget-object v0, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->anb:Landroid/widget/Spinner;

    invoke-virtual {v0, p0}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    iget-object v0, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->ani:Landroid/view/View;

    const v1, 0x7f0a0317

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    iput-object v0, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->amY:Landroid/widget/Spinner;

    iget-object v0, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->amY:Landroid/widget/Spinner;

    iget-object v1, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->amY:Landroid/widget/Spinner;

    invoke-virtual {v1}, Landroid/widget/Spinner;->getPrompt()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setPrompt(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->ani:Landroid/view/View;

    const v1, 0x7f0a00c1

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    iput-object v0, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->amO:Landroid/widget/Spinner;

    iget-object v0, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->amO:Landroid/widget/Spinner;

    iget-object v1, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->amO:Landroid/widget/Spinner;

    invoke-virtual {v1}, Landroid/widget/Spinner;->getPrompt()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setPrompt(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->ani:Landroid/view/View;

    const v1, 0x7f0a04dc

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    iput-object v0, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->amS:Landroid/widget/Spinner;

    iget-object v0, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->amS:Landroid/widget/Spinner;

    iget-object v1, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->amS:Landroid/widget/Spinner;

    invoke-virtual {v1}, Landroid/widget/Spinner;->getPrompt()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setPrompt(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->ani:Landroid/view/View;

    const v1, 0x7f0a01f5

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->amQ:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->ani:Landroid/view/View;

    const v1, 0x7f0a005a

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->amN:Landroid/widget/TextView;

    new-instance v0, Landroid/widget/ArrayAdapter;

    iget-object v1, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f030132

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v4, v2}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I[Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->amX:Landroid/widget/ArrayAdapter;

    iget-object v0, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->amX:Landroid/widget/ArrayAdapter;

    sget v1, Lmiui/R$layout;->simple_spinner_dropdown_item:I

    invoke-virtual {v0, v1}, Landroid/widget/ArrayAdapter;->setDropDownViewResource(I)V

    new-instance v0, Landroid/widget/ArrayAdapter;

    iget-object v1, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f030134

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v4, v2}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I[Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->amW:Landroid/widget/ArrayAdapter;

    iget-object v0, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->amW:Landroid/widget/ArrayAdapter;

    sget v1, Lmiui/R$layout;->simple_spinner_dropdown_item:I

    invoke-virtual {v0, v1}, Landroid/widget/ArrayAdapter;->setDropDownViewResource(I)V

    new-instance v0, Lcom/android/settings/wifi/J;

    iget-object v1, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/android/settings/wifi/J;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->ana:Lcom/android/settings/wifi/J;

    iget-object v0, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->anb:Landroid/widget/Spinner;

    iget-object v1, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->ana:Lcom/android/settings/wifi/J;

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    invoke-direct {p0}, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->adI()V

    return-void
.end method

.method private adM(Landroid/widget/Spinner;Ljava/lang/String;Ljava/lang/String;ZZ)V
    .locals 5

    invoke-virtual {p1}, Landroid/widget/Spinner;->getContext()Landroid/content/Context;

    move-result-object v1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iget-object v2, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->ang:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    if-eqz p4, :cond_0

    iget-object v2, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->amT:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    if-eqz p5, :cond_1

    iget-object v2, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->anh:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_1
    invoke-static {}, Landroid/security/KeyStore;->getInstance()Landroid/security/KeyStore;

    move-result-object v2

    const/16 v3, 0x3f2

    invoke-virtual {v2, p2, v3}, Landroid/security/KeyStore;->list(Ljava/lang/String;I)[Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    invoke-virtual {v0, p3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v2, Landroid/widget/ArrayAdapter;

    sget v3, Lmiui/R$layout;->simple_spinner_dropdown_item:I

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v4

    new-array v4, v4, [Ljava/lang/String;

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    invoke-direct {v2, v1, v3, v0}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I[Ljava/lang/Object;)V

    sget v0, Lmiui/R$layout;->simple_spinner_dropdown_item:I

    invoke-virtual {v2, v0}, Landroid/widget/ArrayAdapter;->setDropDownViewResource(I)V

    invoke-virtual {p1, v2}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    return-void
.end method

.method private adO()V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->ani:Landroid/view/View;

    const v1, 0x7f0a023f

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->amN:Landroid/widget/TextView;

    const-string/jumbo v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method private adP()V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->ani:Landroid/view/View;

    const v1, 0x7f0a0240

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->amO:Landroid/widget/Spinner;

    iget-object v1, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->ang:Ljava/lang/String;

    invoke-direct {p0, v0, v1}, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->adX(Landroid/widget/Spinner;Ljava/lang/String;)V

    return-void
.end method

.method private adQ()V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->ani:Landroid/view/View;

    const v1, 0x7f0a0241

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->amP:Landroid/widget/TextView;

    const-string/jumbo v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method private adR()V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->ani:Landroid/view/View;

    const v1, 0x7f0a0242

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->amY:Landroid/widget/Spinner;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setSelection(I)V

    return-void
.end method

.method private adS()V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->amU:Landroid/widget/TextView;

    const-string/jumbo v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->ani:Landroid/view/View;

    const v1, 0x7f0a0246

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method private adT()V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->amU:Landroid/widget/TextView;

    const-string/jumbo v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->ani:Landroid/view/View;

    const v1, 0x7f0a0246

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method private adU()V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->amU:Landroid/widget/TextView;

    const-string/jumbo v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->ani:Landroid/view/View;

    const v1, 0x7f0a030b

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method private adV()V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->amU:Landroid/widget/TextView;

    const-string/jumbo v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->ani:Landroid/view/View;

    const v1, 0x7f0a030b

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method private adW()V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->ani:Landroid/view/View;

    const v1, 0x7f0a0248

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->amY:Landroid/widget/Spinner;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setSelection(I)V

    return-void
.end method

.method private adX(Landroid/widget/Spinner;Ljava/lang/String;)V
    .locals 3

    if-eqz p2, :cond_0

    invoke-virtual {p1}, Landroid/widget/Spinner;->getAdapter()Landroid/widget/SpinnerAdapter;

    move-result-object v0

    check-cast v0, Landroid/widget/ArrayAdapter;

    invoke-virtual {v0}, Landroid/widget/ArrayAdapter;->getCount()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    :goto_0
    if-ltz v1, :cond_0

    invoke-virtual {v0, v1}, Landroid/widget/ArrayAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {p2, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {p1, v1}, Landroid/widget/Spinner;->setSelection(I)V

    :cond_0
    return-void

    :cond_1
    add-int/lit8 v1, v1, -0x1

    goto :goto_0
.end method

.method private adY()V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->ani:Landroid/view/View;

    const v1, 0x7f0a024a

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method private adZ()V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->ani:Landroid/view/View;

    const v1, 0x7f0a024a

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method private aea()V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->ani:Landroid/view/View;

    const v1, 0x7f0a024b

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->amS:Landroid/widget/Spinner;

    iget-object v1, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->ang:Ljava/lang/String;

    invoke-direct {p0, v0, v1}, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->adX(Landroid/widget/Spinner;Ljava/lang/String;)V

    return-void
.end method

.method private aeb(I)V
    .locals 6

    const v5, 0x7f0a0248

    const v4, 0x7f0a0240

    const v3, 0x7f0a023f

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->ani:Landroid/view/View;

    const v1, 0x7f0a0244

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->ani:Landroid/view/View;

    const v1, 0x7f0a0242

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->ani:Landroid/view/View;

    const v1, 0x7f0a0241

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->ani:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    invoke-direct {p0}, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->adU()V

    packed-switch p1, :pswitch_data_0

    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->ani:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    const/16 v1, 0x8

    if-eq v0, v1, :cond_2

    iget-object v0, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->amO:Landroid/widget/Spinner;

    invoke-virtual {v0}, Landroid/widget/Spinner;->getSelectedItem()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iget-object v1, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->amM:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->ang:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    invoke-direct {p0}, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->adQ()V

    :cond_2
    return-void

    :pswitch_0
    invoke-direct {p0}, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->adW()V

    invoke-direct {p0}, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->adP()V

    invoke-direct {p0}, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->adQ()V

    invoke-direct {p0}, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->adO()V

    invoke-direct {p0}, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->aea()V

    invoke-direct {p0}, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->adY()V

    invoke-direct {p0}, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->adT()V

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->ani:Landroid/view/View;

    const v1, 0x7f0a024b

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    invoke-direct {p0}, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->adW()V

    invoke-direct {p0}, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->adO()V

    invoke-direct {p0}, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->adS()V

    invoke-direct {p0}, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->adY()V

    goto :goto_0

    :pswitch_2
    iget-object v0, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->amV:Landroid/widget/ArrayAdapter;

    iget-object v1, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->amX:Landroid/widget/ArrayAdapter;

    if-eq v0, v1, :cond_3

    iget-object v0, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->amX:Landroid/widget/ArrayAdapter;

    iput-object v0, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->amV:Landroid/widget/ArrayAdapter;

    iget-object v0, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->amY:Landroid/widget/Spinner;

    iget-object v1, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->amV:Landroid/widget/ArrayAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    :cond_3
    iget-object v0, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->ani:Landroid/view/View;

    invoke-virtual {v0, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->ani:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    invoke-direct {p0}, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->aec()V

    invoke-direct {p0}, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->aea()V

    invoke-direct {p0}, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->adY()V

    invoke-direct {p0}, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->adT()V

    goto/16 :goto_0

    :pswitch_3
    iget-object v0, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->amV:Landroid/widget/ArrayAdapter;

    iget-object v1, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->amW:Landroid/widget/ArrayAdapter;

    if-eq v0, v1, :cond_4

    iget-object v0, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->amW:Landroid/widget/ArrayAdapter;

    iput-object v0, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->amV:Landroid/widget/ArrayAdapter;

    iget-object v0, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->amY:Landroid/widget/Spinner;

    iget-object v1, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->amV:Landroid/widget/ArrayAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    :cond_4
    iget-object v0, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->ani:Landroid/view/View;

    invoke-virtual {v0, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->ani:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    invoke-direct {p0}, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->aea()V

    invoke-direct {p0}, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->adY()V

    invoke-direct {p0}, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->adT()V

    goto/16 :goto_0

    :pswitch_4
    invoke-direct {p0}, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->adW()V

    invoke-direct {p0}, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->adO()V

    invoke-direct {p0}, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->adP()V

    invoke-direct {p0}, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->adQ()V

    invoke-direct {p0}, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->aea()V

    invoke-direct {p0}, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->adR()V

    invoke-direct {p0}, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->adS()V

    iget-object v0, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->ana:Lcom/android/settings/wifi/J;

    invoke-virtual {v0}, Lcom/android/settings/wifi/J;->getCount()I

    move-result v0

    const/4 v1, 0x1

    if-lt v0, v1, :cond_0

    invoke-direct {p0}, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->adZ()V

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_1
        :pswitch_3
        :pswitch_0
        :pswitch_4
        :pswitch_4
        :pswitch_4
    .end packed-switch
.end method

.method private aec()V
    .locals 4

    const v3, 0x7f0a0242

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->amY:Landroid/widget/Spinner;

    invoke-virtual {v0}, Landroid/widget/Spinner;->getSelectedItemPosition()I

    move-result v0

    const/4 v1, 0x3

    if-eq v0, v1, :cond_0

    const/4 v1, 0x4

    if-ne v0, v1, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->amQ:Landroid/widget/TextView;

    const-string/jumbo v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->ani:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    :goto_0
    return-void

    :cond_1
    const/4 v1, 0x5

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->ani:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->ani:Landroid/view/View;

    const v1, 0x7f0a023f

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method private aed()V
    .locals 7

    const v1, 0x7f0a03cc

    const v6, 0x7f0a0317

    const v3, 0x7f0a015d

    const/16 v2, 0x8

    const/4 v4, 0x0

    iget v0, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->amK:I

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->ani:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->ani:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    iget v0, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->amK:I

    const/4 v1, 0x3

    if-eq v0, v1, :cond_1

    invoke-direct {p0}, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->adV()V

    iget-object v0, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->ani:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->ani:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->ani:Landroid/view/View;

    const v1, 0x7f0a0245

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->amU:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->amU:Landroid/widget/TextView;

    sget-object v1, Landroid/graphics/Typeface;->DEFAULT:Landroid/graphics/Typeface;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    iget-object v0, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->amU:Landroid/widget/TextView;

    invoke-virtual {v0, p0}, Landroid/widget/TextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    iget-object v0, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->amR:Landroid/widget/Spinner;

    if-nez v0, :cond_4

    invoke-direct {p0}, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->adK()V

    iget-object v0, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->ani:Landroid/view/View;

    const v1, 0x7f0a0297

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    iput-object v0, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->amR:Landroid/widget/Spinner;

    iget-object v0, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->amR:Landroid/widget/Spinner;

    invoke-virtual {v0, p0}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    iget-object v0, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/settings/aq;->bqv(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x1120053

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_3

    :cond_2
    iget-object v0, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f030072

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    new-instance v1, Landroid/widget/ArrayAdapter;

    iget-object v2, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mContext:Landroid/content/Context;

    sget v3, Lmiui/R$layout;->simple_spinner_dropdown_item:I

    invoke-direct {v1, v2, v3, v0}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I[Ljava/lang/Object;)V

    sget v0, Lmiui/R$layout;->simple_spinner_dropdown_item:I

    invoke-virtual {v1, v0}, Landroid/widget/ArrayAdapter;->setDropDownViewResource(I)V

    iget-object v0, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->amR:Landroid/widget/Spinner;

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    :cond_3
    iget-object v0, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->amR:Landroid/widget/Spinner;

    iget-object v1, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->amR:Landroid/widget/Spinner;

    invoke-virtual {v1}, Landroid/widget/Spinner;->getPrompt()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setPrompt(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->ani:Landroid/view/View;

    invoke-virtual {v0, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    iput-object v0, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->amY:Landroid/widget/Spinner;

    iget-object v0, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->amY:Landroid/widget/Spinner;

    iget-object v1, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->amY:Landroid/widget/Spinner;

    invoke-virtual {v1}, Landroid/widget/Spinner;->getPrompt()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setPrompt(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->ani:Landroid/view/View;

    const v1, 0x7f0a00c1

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    iput-object v0, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->amO:Landroid/widget/Spinner;

    iget-object v0, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->ani:Landroid/view/View;

    const v1, 0x7f0a014d

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->amP:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->amP:Landroid/widget/TextView;

    invoke-virtual {v0, p0}, Landroid/widget/TextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    iget-object v0, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->ani:Landroid/view/View;

    const v1, 0x7f0a04dc

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    iput-object v0, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->amS:Landroid/widget/Spinner;

    iget-object v0, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->amS:Landroid/widget/Spinner;

    invoke-virtual {v0, p0}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    iget-object v0, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->ani:Landroid/view/View;

    const v1, 0x7f0a0408

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    iput-object v0, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->anb:Landroid/widget/Spinner;

    iget-object v0, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->ani:Landroid/view/View;

    const v1, 0x7f0a01f5

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->amQ:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->ani:Landroid/view/View;

    const v1, 0x7f0a005a

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->amN:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->anb:Landroid/widget/Spinner;

    invoke-virtual {v0, p0}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    iget-object v1, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->amO:Landroid/widget/Spinner;

    const-string/jumbo v2, "CACERT_"

    iget-object v3, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->amM:Ljava/lang/String;

    const/4 v5, 0x1

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->adM(Landroid/widget/Spinner;Ljava/lang/String;Ljava/lang/String;ZZ)V

    iget-object v1, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->amS:Landroid/widget/Spinner;

    const-string/jumbo v2, "USRPKEY_"

    iget-object v3, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->amL:Ljava/lang/String;

    move-object v0, p0

    move v5, v4

    invoke-direct/range {v0 .. v5}, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->adM(Landroid/widget/Spinner;Ljava/lang/String;Ljava/lang/String;ZZ)V

    iget-object v0, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->ani:Landroid/view/View;

    invoke-virtual {v0, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    iput-object v0, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->amY:Landroid/widget/Spinner;

    iget-object v0, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->amR:Landroid/widget/Spinner;

    invoke-virtual {v0}, Landroid/widget/Spinner;->getSelectedItemPosition()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->aeb(I)V

    :goto_0
    return-void

    :cond_4
    iget-object v0, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->amR:Landroid/widget/Spinner;

    invoke-virtual {v0}, Landroid/widget/Spinner;->getSelectedItemPosition()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->aeb(I)V

    goto :goto_0
.end method

.method static synthetic aee(Lcom/android/settings/wifi/MiuiAddNetworkFragment;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->adI()V

    return-void
.end method


# virtual methods
.method public adN()V
    .locals 3

    const/4 v0, 0x0

    invoke-direct {p0}, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->adJ()Landroid/net/wifi/WifiConfiguration;

    move-result-object v1

    if-eqz v1, :cond_0

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string/jumbo v2, "config"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    :cond_0
    invoke-virtual {p0, v0}, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->beb(Landroid/os/Bundle;)V

    return-void
.end method

.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->anf:Landroid/os/Handler;

    new-instance v1, Lcom/android/settings/wifi/aC;

    invoke-direct {v1, p0}, Lcom/android/settings/wifi/aC;-><init>(Lcom/android/settings/wifi/MiuiAddNetworkFragment;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    return-void
.end method

.method public getTitle()Ljava/lang/String;
    .locals 1

    const v0, 0x7f120a21

    invoke-virtual {p0, v0}, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Lcom/android/settings/BaseEditFragment;->onActivityCreated(Landroid/os/Bundle;)V

    invoke-direct {p0}, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->adL()V

    invoke-virtual {p0}, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/Activity;->setRequestedOrientation(I)V

    :cond_0
    return-void
.end method

.method public onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 3

    invoke-virtual {p1}, Landroid/widget/CompoundButton;->getId()I

    move-result v0

    const v1, 0x7f0a03ff

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->amU:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getSelectionEnd()I

    move-result v1

    iget-object v2, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->amU:Landroid/widget/TextView;

    if-eqz p2, :cond_1

    const/16 v0, 0x90

    :goto_0
    or-int/lit8 v0, v0, 0x1

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setInputType(I)V

    if-ltz v1, :cond_0

    iget-object v0, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->amU:Landroid/widget/TextView;

    check-cast v0, Landroid/widget/EditText;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setSelection(I)V

    :cond_0
    return-void

    :cond_1
    const/16 v0, 0x80

    goto :goto_0
.end method

.method public onItemSelected(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->amZ:Landroid/widget/Spinner;

    if-ne p1, v0, :cond_1

    iput p3, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->amK:I

    invoke-direct {p0}, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->aed()V

    :cond_0
    :goto_0
    invoke-direct {p0}, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->adI()V

    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->amR:Landroid/widget/Spinner;

    if-eq p1, v0, :cond_2

    iget-object v0, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->amO:Landroid/widget/Spinner;

    if-ne p1, v0, :cond_3

    :cond_2
    invoke-direct {p0}, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->aed()V

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->anb:Landroid/widget/Spinner;

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->ana:Lcom/android/settings/wifi/J;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->ana:Lcom/android/settings/wifi/J;

    iget-object v1, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->anb:Landroid/widget/Spinner;

    invoke-virtual {v1}, Landroid/widget/Spinner;->getSelectedItemPosition()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/android/settings/wifi/J;->agX(I)V

    goto :goto_0
.end method

.method public onNothingSelected(Landroid/widget/AdapterView;)V
    .locals 0

    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    return-void
.end method

.method public vB(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2

    const v0, 0x7f0d026d

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method
