.class Lcom/android/settings/wifi/s;
.super Landroid/app/AlertDialog;
.source "WifiDialog.java"

# interfaces
.implements Lcom/android/settings/wifi/G;
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field private final amE:Lcom/android/settingslib/wifi/i;

.field private amF:Lcom/android/settings/wifi/A;

.field private amG:Z

.field private final amH:Lcom/android/settings/wifi/t;

.field private final amI:I

.field private amJ:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/android/settings/wifi/t;Lcom/android/settingslib/wifi/i;I)V
    .locals 1

    const v0, 0x7f13020b

    invoke-direct {p0, p1, v0}, Landroid/app/AlertDialog;-><init>(Landroid/content/Context;I)V

    iput p4, p0, Lcom/android/settings/wifi/s;->amI:I

    iput-object p2, p0, Lcom/android/settings/wifi/s;->amH:Lcom/android/settings/wifi/t;

    iput-object p3, p0, Lcom/android/settings/wifi/s;->amE:Lcom/android/settingslib/wifi/i;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/settings/wifi/s;->amG:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/android/settings/wifi/t;Lcom/android/settingslib/wifi/i;IZ)V
    .locals 0

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/android/settings/wifi/s;-><init>(Landroid/content/Context;Lcom/android/settings/wifi/t;Lcom/android/settingslib/wifi/i;I)V

    iput-boolean p5, p0, Lcom/android/settings/wifi/s;->amG:Z

    return-void
.end method

.method private adD(Z)V
    .locals 3

    iget-object v0, p0, Lcom/android/settings/wifi/s;->amJ:Landroid/view/View;

    const v1, 0x7f0a0306

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getSelectionEnd()I

    move-result v2

    if-eqz p1, :cond_1

    const/16 v1, 0x90

    :goto_0
    or-int/lit8 v1, v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setInputType(I)V

    sget-object v1, Landroid/graphics/Typeface;->DEFAULT:Landroid/graphics/Typeface;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setTypeface(Landroid/graphics/Typeface;)V

    if-ltz v2, :cond_0

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->setSelection(I)V

    :cond_0
    return-void

    :cond_1
    const/16 v1, 0x80

    goto :goto_0
.end method

.method static synthetic adE(Lcom/android/settings/wifi/s;)Lcom/android/settingslib/wifi/i;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/wifi/s;->amE:Lcom/android/settingslib/wifi/i;

    return-object v0
.end method

.method static synthetic adF(Lcom/android/settings/wifi/s;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/settings/wifi/s;->adD(Z)V

    return-void
.end method


# virtual methods
.method public adA(Ljava/lang/CharSequence;)V
    .locals 1

    const/4 v0, -0x2

    invoke-virtual {p0, v0, p1, p0}, Lcom/android/settings/wifi/s;->setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    return-void
.end method

.method public adB(Ljava/lang/CharSequence;)V
    .locals 1

    const/4 v0, -0x3

    invoke-virtual {p0, v0, p1, p0}, Lcom/android/settings/wifi/s;->setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    return-void
.end method

.method public adC(Ljava/lang/CharSequence;)V
    .locals 1

    const/4 v0, -0x1

    invoke-virtual {p0, v0, p1, p0}, Lcom/android/settings/wifi/s;->setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    return-void
.end method

.method public adv()V
    .locals 1

    iget-object v0, p0, Lcom/android/settings/wifi/s;->amH:Lcom/android/settings/wifi/t;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/wifi/s;->amH:Lcom/android/settings/wifi/t;

    invoke-interface {v0, p0}, Lcom/android/settings/wifi/t;->adH(Lcom/android/settings/wifi/s;)V

    :cond_0
    invoke-virtual {p0}, Lcom/android/settings/wifi/s;->dismiss()V

    return-void
.end method

.method public adw()Lcom/android/settings/wifi/A;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/wifi/s;->amF:Lcom/android/settings/wifi/A;

    return-object v0
.end method

.method public adx()Landroid/widget/Button;
    .locals 1

    const/4 v0, -0x3

    invoke-virtual {p0, v0}, Lcom/android/settings/wifi/s;->getButton(I)Landroid/widget/Button;

    move-result-object v0

    return-object v0
.end method

.method public ady()Landroid/widget/Button;
    .locals 1

    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lcom/android/settings/wifi/s;->getButton(I)Landroid/widget/Button;

    move-result-object v0

    return-object v0
.end method

.method public adz()V
    .locals 3

    iget-object v0, p0, Lcom/android/settings/wifi/s;->amE:Lcom/android/settingslib/wifi/i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/wifi/s;->amE:Lcom/android/settingslib/wifi/i;

    iget-boolean v0, v0, Lcom/android/settingslib/wifi/i;->cCT:Z

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/android/settings/wifi/s;->amJ:Landroid/view/View;

    const v2, 0x7f0a0400

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    if-eqz v1, :cond_1

    const v2, 0x7f08045d

    :goto_1
    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    invoke-direct {p0, v1}, Lcom/android/settings/wifi/s;->adD(Z)V

    new-instance v2, Lcom/android/settings/wifi/aB;

    invoke-direct {v2, p0, v1}, Lcom/android/settings/wifi/aB;-><init>(Lcom/android/settings/wifi/s;Z)V

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void

    :cond_0
    const/4 v0, 0x0

    move v1, v0

    goto :goto_0

    :cond_1
    const v2, 0x7f08045a

    goto :goto_1
.end method

.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/wifi/s;->amH:Lcom/android/settings/wifi/t;

    if-eqz v0, :cond_0

    packed-switch p2, :pswitch_data_0

    :cond_0
    :goto_0
    :pswitch_0
    return-void

    :pswitch_1
    iget-object v0, p0, Lcom/android/settings/wifi/s;->amH:Lcom/android/settings/wifi/t;

    invoke-interface {v0, p0}, Lcom/android/settings/wifi/t;->adH(Lcom/android/settings/wifi/s;)V

    goto :goto_0

    :pswitch_2
    invoke-virtual {p0}, Lcom/android/settings/wifi/s;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/wifi/s;->amE:Lcom/android/settingslib/wifi/i;

    invoke-virtual {v1}, Lcom/android/settingslib/wifi/i;->chL()Landroid/net/wifi/WifiConfiguration;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/settings/wifi/WifiSettings;->ahf(Landroid/content/Context;Landroid/net/wifi/WifiConfiguration;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/android/settings/wifi/s;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {p0}, Lcom/android/settings/wifi/s;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/android/settingslib/w;->cqN(Landroid/content/Context;)Lcom/android/settingslib/n;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/settingslib/w;->cqW(Landroid/content/Context;Lcom/android/settingslib/n;)V

    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/settings/wifi/s;->amH:Lcom/android/settings/wifi/t;

    invoke-interface {v0, p0}, Lcom/android/settings/wifi/t;->adG(Lcom/android/settings/wifi/s;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch -0x3
        :pswitch_2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 6

    const/4 v3, 0x0

    const/4 v5, 0x3

    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/android/settings/wifi/s;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f0d0271

    invoke-virtual {v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/android/settings/wifi/s;->amJ:Landroid/view/View;

    iget-object v1, p0, Lcom/android/settings/wifi/s;->amJ:Landroid/view/View;

    invoke-virtual {p0, v1}, Lcom/android/settings/wifi/s;->setView(Landroid/view/View;)V

    invoke-virtual {p0, v0}, Lcom/android/settings/wifi/s;->setInverseBackgroundForced(Z)V

    new-instance v1, Lcom/android/settings/wifi/A;

    iget-object v2, p0, Lcom/android/settings/wifi/s;->amJ:Landroid/view/View;

    iget-object v3, p0, Lcom/android/settings/wifi/s;->amE:Lcom/android/settingslib/wifi/i;

    iget v4, p0, Lcom/android/settings/wifi/s;->amI:I

    if-ne v4, v5, :cond_2

    :goto_0
    invoke-direct {v1, p0, v2, v3, v0}, Lcom/android/settings/wifi/A;-><init>(Lcom/android/settings/wifi/G;Landroid/view/View;Lcom/android/settingslib/wifi/i;I)V

    iput-object v1, p0, Lcom/android/settings/wifi/s;->amF:Lcom/android/settings/wifi/A;

    invoke-super {p0, p1}, Landroid/app/AlertDialog;->onCreate(Landroid/os/Bundle;)V

    iget-boolean v0, p0, Lcom/android/settings/wifi/s;->amG:Z

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/android/settings/wifi/s;->amF:Lcom/android/settings/wifi/A;

    invoke-virtual {v0}, Lcom/android/settings/wifi/A;->aeR()V

    :goto_1
    invoke-virtual {p0}, Lcom/android/settings/wifi/s;->adz()V

    iget-object v0, p0, Lcom/android/settings/wifi/s;->amE:Lcom/android/settingslib/wifi/i;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/wifi/s;->amF:Lcom/android/settings/wifi/A;

    invoke-virtual {v0}, Lcom/android/settings/wifi/A;->aeT()V

    :cond_0
    iget v0, p0, Lcom/android/settings/wifi/s;->amI:I

    if-ne v0, v5, :cond_1

    iget-object v0, p0, Lcom/android/settings/wifi/s;->amJ:Landroid/view/View;

    const v1, 0x7f0a0243

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/settings/wifi/s;->amJ:Landroid/view/View;

    const v1, 0x7f0a020b

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/android/settings/wifi/s;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f12154a

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_1
    return-void

    :cond_2
    iget v0, p0, Lcom/android/settings/wifi/s;->amI:I

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/android/settings/wifi/s;->amF:Lcom/android/settings/wifi/A;

    invoke-virtual {v0}, Lcom/android/settings/wifi/A;->aeS()V

    goto :goto_1
.end method

.method public onRestoreInstanceState(Landroid/os/Bundle;)V
    .locals 1

    invoke-super {p0, p1}, Landroid/app/AlertDialog;->onRestoreInstanceState(Landroid/os/Bundle;)V

    iget-object v0, p0, Lcom/android/settings/wifi/s;->amF:Lcom/android/settings/wifi/A;

    invoke-virtual {v0}, Lcom/android/settings/wifi/A;->aeU()V

    return-void
.end method
