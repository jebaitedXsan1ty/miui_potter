.class public Lcom/android/settings/wifi/ad;
.super Lcom/android/settings/core/e;
.source "WifiInfoPreferenceController.java"

# interfaces
.implements Lcom/android/settings/core/lifecycle/b;
.implements Lcom/android/settings/core/lifecycle/a/b;
.implements Lcom/android/settings/core/lifecycle/a/d;


# instance fields
.field private final ath:Landroid/content/IntentFilter;

.field private ati:Landroid/preference/Preference;

.field private atj:Landroid/preference/Preference;

.field private final atk:Landroid/net/wifi/WifiManager;

.field private final mReceiver:Landroid/content/BroadcastReceiver;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/android/settings/core/lifecycle/c;Landroid/net/wifi/WifiManager;)V
    .locals 2

    invoke-direct {p0, p1}, Lcom/android/settings/core/e;-><init>(Landroid/content/Context;)V

    new-instance v0, Lcom/android/settings/wifi/bJ;

    invoke-direct {v0, p0}, Lcom/android/settings/wifi/bJ;-><init>(Lcom/android/settings/wifi/ad;)V

    iput-object v0, p0, Lcom/android/settings/wifi/ad;->mReceiver:Landroid/content/BroadcastReceiver;

    iput-object p3, p0, Lcom/android/settings/wifi/ad;->atk:Landroid/net/wifi/WifiManager;

    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    iput-object v0, p0, Lcom/android/settings/wifi/ad;->ath:Landroid/content/IntentFilter;

    iget-object v0, p0, Lcom/android/settings/wifi/ad;->ath:Landroid/content/IntentFilter;

    const-string/jumbo v1, "android.net.wifi.LINK_CONFIGURATION_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/settings/wifi/ad;->ath:Landroid/content/IntentFilter;

    const-string/jumbo v1, "android.net.wifi.STATE_CHANGE"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    invoke-virtual {p2, p0}, Lcom/android/settings/core/lifecycle/c;->ajv(Lcom/android/settings/core/lifecycle/b;)Lcom/android/settings/core/lifecycle/b;

    return-void
.end method


# virtual methods
.method public ajh()V
    .locals 4

    const v3, 0x7f121167

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/android/settings/wifi/ad;->atj:Landroid/preference/Preference;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/settings/wifi/ad;->atk:Landroid/net/wifi/WifiManager;

    invoke-virtual {v1}, Landroid/net/wifi/WifiManager;->getConnectionInfo()Landroid/net/wifi/WifiInfo;

    move-result-object v1

    if-nez v1, :cond_3

    :goto_0
    iget-object v1, p0, Lcom/android/settings/wifi/ad;->atj:Landroid/preference/Preference;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_4

    :goto_1
    invoke-virtual {v1, v0}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    :cond_0
    iget-object v0, p0, Lcom/android/settings/wifi/ad;->ati:Landroid/preference/Preference;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/settings/wifi/ad;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/settings/aq;->brw(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/wifi/ad;->ati:Landroid/preference/Preference;

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/android/settings/wifi/ad;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    :cond_1
    invoke-virtual {v1, v0}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    :cond_2
    return-void

    :cond_3
    invoke-virtual {v1}, Landroid/net/wifi/WifiInfo;->getMacAddress()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_4
    iget-object v0, p0, Lcom/android/settings/wifi/ad;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method

.method public i(Landroid/preference/PreferenceScreen;)V
    .locals 2

    const/4 v1, 0x0

    invoke-super {p0, p1}, Lcom/android/settings/core/e;->i(Landroid/preference/PreferenceScreen;)V

    const-string/jumbo v0, "mac_address"

    invoke-virtual {p1, v0}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/wifi/ad;->atj:Landroid/preference/Preference;

    iget-object v0, p0, Lcom/android/settings/wifi/ad;->atj:Landroid/preference/Preference;

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setSelectable(Z)V

    const-string/jumbo v0, "current_ip_address"

    invoke-virtual {p1, v0}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/wifi/ad;->ati:Landroid/preference/Preference;

    iget-object v0, p0, Lcom/android/settings/wifi/ad;->ati:Landroid/preference/Preference;

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setSelectable(Z)V

    invoke-virtual {p0}, Lcom/android/settings/wifi/ad;->ajh()V

    return-void
.end method

.method public l()Ljava/lang/String;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public onPause()V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/wifi/ad;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/android/settings/wifi/ad;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    return-void
.end method

.method public onResume()V
    .locals 3

    iget-object v0, p0, Lcom/android/settings/wifi/ad;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/android/settings/wifi/ad;->mReceiver:Landroid/content/BroadcastReceiver;

    iget-object v2, p0, Lcom/android/settings/wifi/ad;->ath:Landroid/content/IntentFilter;

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/android/settings/wifi/ad;->ajh()V

    return-void
.end method

.method public p()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method
