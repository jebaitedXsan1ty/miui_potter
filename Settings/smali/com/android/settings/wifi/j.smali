.class public Lcom/android/settings/wifi/j;
.super Landroid/preference/CheckBoxPreference;
.source "SavedAccessPointPreference.java"


# instance fields
.field private alI:Lcom/android/settingslib/wifi/i;

.field private alJ:Z

.field private alK:Z

.field private alL:Landroid/view/View;


# direct methods
.method public constructor <init>(Lcom/android/settingslib/wifi/i;Landroid/content/Context;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p2}, Landroid/preference/CheckBoxPreference;-><init>(Landroid/content/Context;)V

    iput-boolean v0, p0, Lcom/android/settings/wifi/j;->alJ:Z

    iput-boolean v0, p0, Lcom/android/settings/wifi/j;->alK:Z

    iput-object p1, p0, Lcom/android/settings/wifi/j;->alI:Lcom/android/settingslib/wifi/i;

    iget-object v0, p0, Lcom/android/settings/wifi/j;->alI:Lcom/android/settingslib/wifi/i;

    invoke-virtual {v0, p0}, Lcom/android/settingslib/wifi/i;->chK(Ljava/lang/Object;)V

    const v0, 0x7f0d012d

    invoke-virtual {p0, v0}, Lcom/android/settings/wifi/j;->setWidgetLayoutResource(I)V

    return-void
.end method


# virtual methods
.method public abS()Lcom/android/settingslib/wifi/i;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/wifi/j;->alI:Lcom/android/settingslib/wifi/i;

    return-object v0
.end method

.method public abT(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/android/settings/wifi/j;->alK:Z

    return-void
.end method

.method public abU(Z)V
    .locals 2

    iput-boolean p1, p0, Lcom/android/settings/wifi/j;->alJ:Z

    iget-object v0, p0, Lcom/android/settings/wifi/j;->alL:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/wifi/j;->alL:Landroid/view/View;

    const v1, 0x7f0a00db

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    invoke-virtual {v0, p1}, Landroid/widget/CheckBox;->setChecked(Z)V

    :cond_0
    return-void
.end method

.method public isChecked()Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/settings/wifi/j;->alJ:Z

    return v0
.end method

.method protected onBindView(Landroid/view/View;)V
    .locals 2

    invoke-super {p0, p1}, Landroid/preference/CheckBoxPreference;->onBindView(Landroid/view/View;)V

    iget-object v0, p0, Lcom/android/settings/wifi/j;->alI:Lcom/android/settingslib/wifi/i;

    if-nez v0, :cond_0

    return-void

    :cond_0
    iput-object p1, p0, Lcom/android/settings/wifi/j;->alL:Landroid/view/View;

    const v0, 0x7f0a00db

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iget-boolean v1, p0, Lcom/android/settings/wifi/j;->alK:Z

    if-nez v1, :cond_1

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setVisibility(I)V

    :cond_1
    iget-boolean v1, p0, Lcom/android/settings/wifi/j;->alJ:Z

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    return-void
.end method
