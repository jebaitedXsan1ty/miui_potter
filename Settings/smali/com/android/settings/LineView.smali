.class public Lcom/android/settings/LineView;
.super Landroid/view/View;
.source "LineView.java"


# instance fields
.field private cck:Z

.field private ccl:Landroid/graphics/Paint;

.field private ccm:I

.field private ccn:I

.field private cco:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/settings/LineView;->cck:Z

    const/4 v0, 0x5

    iput v0, p0, Lcom/android/settings/LineView;->ccm:I

    invoke-virtual {p0}, Lcom/android/settings/LineView;->bUg()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/settings/LineView;->cck:Z

    const/4 v0, 0x5

    iput v0, p0, Lcom/android/settings/LineView;->ccm:I

    invoke-virtual {p0}, Lcom/android/settings/LineView;->bUg()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/settings/LineView;->cck:Z

    const/4 v0, 0x5

    iput v0, p0, Lcom/android/settings/LineView;->ccm:I

    invoke-virtual {p0}, Lcom/android/settings/LineView;->bUg()V

    return-void
.end method


# virtual methods
.method bUg()V
    .locals 2

    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/android/settings/LineView;->ccl:Landroid/graphics/Paint;

    iget-object v0, p0, Lcom/android/settings/LineView;->ccl:Landroid/graphics/Paint;

    const v1, -0x777778

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    iget-object v0, p0, Lcom/android/settings/LineView;->ccl:Landroid/graphics/Paint;

    const/high16 v1, 0x40a00000    # 5.0f

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    iget-object v0, p0, Lcom/android/settings/LineView;->ccl:Landroid/graphics/Paint;

    const/16 v1, 0x32

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAlpha(I)V

    return-void
.end method

.method public onDraw(Landroid/graphics/Canvas;)V
    .locals 7

    const/4 v2, 0x0

    iget-boolean v0, p0, Lcom/android/settings/LineView;->cck:Z

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/android/settings/LineView;->ccn:I

    iget v1, p0, Lcom/android/settings/LineView;->ccm:I

    sub-int/2addr v0, v1

    int-to-float v1, v0

    iget v0, p0, Lcom/android/settings/LineView;->ccn:I

    iget v3, p0, Lcom/android/settings/LineView;->ccm:I

    sub-int/2addr v0, v3

    int-to-float v3, v0

    iget v0, p0, Lcom/android/settings/LineView;->cco:I

    int-to-float v4, v0

    iget-object v5, p0, Lcom/android/settings/LineView;->ccl:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    :goto_0
    return-void

    :cond_0
    iget v0, p0, Lcom/android/settings/LineView;->cco:I

    iget v1, p0, Lcom/android/settings/LineView;->ccm:I

    sub-int/2addr v0, v1

    int-to-float v3, v0

    iget v0, p0, Lcom/android/settings/LineView;->ccn:I

    int-to-float v4, v0

    iget v0, p0, Lcom/android/settings/LineView;->cco:I

    iget v1, p0, Lcom/android/settings/LineView;->ccm:I

    sub-int/2addr v0, v1

    int-to-float v5, v0

    iget-object v6, p0, Lcom/android/settings/LineView;->ccl:Landroid/graphics/Paint;

    move-object v1, p1

    invoke-virtual/range {v1 .. v6}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    goto :goto_0
.end method

.method protected onSizeChanged(IIII)V
    .locals 0

    iput p1, p0, Lcom/android/settings/LineView;->ccn:I

    iput p2, p0, Lcom/android/settings/LineView;->cco:I

    return-void
.end method

.method public setColor(I)V
    .locals 1

    iget-object v0, p0, Lcom/android/settings/LineView;->ccl:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setColor(I)V

    return-void
.end method

.method public setOrientation(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/android/settings/LineView;->cck:Z

    return-void
.end method

.method public setStrokeWidth(I)V
    .locals 2

    iput p1, p0, Lcom/android/settings/LineView;->ccm:I

    iget-object v0, p0, Lcom/android/settings/LineView;->ccl:Landroid/graphics/Paint;

    int-to-float v1, p1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    return-void
.end method
