.class final Lcom/android/settings/fingerprint/O;
.super Landroid/hardware/fingerprint/FingerprintManager$EnrollmentCallback;
.source "FingerprintEnrollSidecar.java"


# instance fields
.field final synthetic bci:Lcom/android/settings/fingerprint/FingerprintEnrollSidecar;


# direct methods
.method constructor <init>(Lcom/android/settings/fingerprint/FingerprintEnrollSidecar;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/fingerprint/O;->bci:Lcom/android/settings/fingerprint/FingerprintEnrollSidecar;

    invoke-direct {p0}, Landroid/hardware/fingerprint/FingerprintManager$EnrollmentCallback;-><init>()V

    return-void
.end method


# virtual methods
.method public onEnrollmentError(ILjava/lang/CharSequence;)V
    .locals 3

    iget-object v0, p0, Lcom/android/settings/fingerprint/O;->bci:Lcom/android/settings/fingerprint/FingerprintEnrollSidecar;

    invoke-static {v0}, Lcom/android/settings/fingerprint/FingerprintEnrollSidecar;->aPJ(Lcom/android/settings/fingerprint/FingerprintEnrollSidecar;)Lcom/android/settings/fingerprint/i;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/fingerprint/O;->bci:Lcom/android/settings/fingerprint/FingerprintEnrollSidecar;

    invoke-static {v0}, Lcom/android/settings/fingerprint/FingerprintEnrollSidecar;->aPJ(Lcom/android/settings/fingerprint/FingerprintEnrollSidecar;)Lcom/android/settings/fingerprint/i;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Lcom/android/settings/fingerprint/i;->onEnrollmentError(ILjava/lang/CharSequence;)V

    :goto_0
    iget-object v0, p0, Lcom/android/settings/fingerprint/O;->bci:Lcom/android/settings/fingerprint/FingerprintEnrollSidecar;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/android/settings/fingerprint/FingerprintEnrollSidecar;->aPM(Lcom/android/settings/fingerprint/FingerprintEnrollSidecar;Z)Z

    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/settings/fingerprint/O;->bci:Lcom/android/settings/fingerprint/FingerprintEnrollSidecar;

    invoke-static {v0}, Lcom/android/settings/fingerprint/FingerprintEnrollSidecar;->aPK(Lcom/android/settings/fingerprint/FingerprintEnrollSidecar;)Ljava/util/ArrayList;

    move-result-object v0

    new-instance v1, Lcom/android/settings/fingerprint/h;

    iget-object v2, p0, Lcom/android/settings/fingerprint/O;->bci:Lcom/android/settings/fingerprint/FingerprintEnrollSidecar;

    invoke-direct {v1, v2, p1, p2}, Lcom/android/settings/fingerprint/h;-><init>(Lcom/android/settings/fingerprint/FingerprintEnrollSidecar;ILjava/lang/CharSequence;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public onEnrollmentHelp(ILjava/lang/CharSequence;)V
    .locals 3

    iget-object v0, p0, Lcom/android/settings/fingerprint/O;->bci:Lcom/android/settings/fingerprint/FingerprintEnrollSidecar;

    invoke-static {v0}, Lcom/android/settings/fingerprint/FingerprintEnrollSidecar;->aPJ(Lcom/android/settings/fingerprint/FingerprintEnrollSidecar;)Lcom/android/settings/fingerprint/i;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/fingerprint/O;->bci:Lcom/android/settings/fingerprint/FingerprintEnrollSidecar;

    invoke-static {v0}, Lcom/android/settings/fingerprint/FingerprintEnrollSidecar;->aPJ(Lcom/android/settings/fingerprint/FingerprintEnrollSidecar;)Lcom/android/settings/fingerprint/i;

    move-result-object v0

    invoke-interface {v0, p2}, Lcom/android/settings/fingerprint/i;->aOL(Ljava/lang/CharSequence;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/settings/fingerprint/O;->bci:Lcom/android/settings/fingerprint/FingerprintEnrollSidecar;

    invoke-static {v0}, Lcom/android/settings/fingerprint/FingerprintEnrollSidecar;->aPK(Lcom/android/settings/fingerprint/FingerprintEnrollSidecar;)Ljava/util/ArrayList;

    move-result-object v0

    new-instance v1, Lcom/android/settings/fingerprint/g;

    iget-object v2, p0, Lcom/android/settings/fingerprint/O;->bci:Lcom/android/settings/fingerprint/FingerprintEnrollSidecar;

    invoke-direct {v1, v2, p1, p2}, Lcom/android/settings/fingerprint/g;-><init>(Lcom/android/settings/fingerprint/FingerprintEnrollSidecar;ILjava/lang/CharSequence;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public onEnrollmentProgress(I)V
    .locals 4

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/android/settings/fingerprint/O;->bci:Lcom/android/settings/fingerprint/FingerprintEnrollSidecar;

    invoke-static {v1}, Lcom/android/settings/fingerprint/FingerprintEnrollSidecar;->aPI(Lcom/android/settings/fingerprint/FingerprintEnrollSidecar;)I

    move-result v1

    const/4 v2, -0x1

    if-ne v1, v2, :cond_0

    iget-object v1, p0, Lcom/android/settings/fingerprint/O;->bci:Lcom/android/settings/fingerprint/FingerprintEnrollSidecar;

    invoke-static {v1, p1}, Lcom/android/settings/fingerprint/FingerprintEnrollSidecar;->aPO(Lcom/android/settings/fingerprint/FingerprintEnrollSidecar;I)I

    :cond_0
    iget-object v1, p0, Lcom/android/settings/fingerprint/O;->bci:Lcom/android/settings/fingerprint/FingerprintEnrollSidecar;

    invoke-static {v1, p1}, Lcom/android/settings/fingerprint/FingerprintEnrollSidecar;->aPN(Lcom/android/settings/fingerprint/FingerprintEnrollSidecar;I)I

    iget-object v1, p0, Lcom/android/settings/fingerprint/O;->bci:Lcom/android/settings/fingerprint/FingerprintEnrollSidecar;

    if-nez p1, :cond_1

    const/4 v0, 0x1

    :cond_1
    invoke-static {v1, v0}, Lcom/android/settings/fingerprint/FingerprintEnrollSidecar;->aPL(Lcom/android/settings/fingerprint/FingerprintEnrollSidecar;Z)Z

    iget-object v0, p0, Lcom/android/settings/fingerprint/O;->bci:Lcom/android/settings/fingerprint/FingerprintEnrollSidecar;

    invoke-static {v0}, Lcom/android/settings/fingerprint/FingerprintEnrollSidecar;->aPJ(Lcom/android/settings/fingerprint/FingerprintEnrollSidecar;)Lcom/android/settings/fingerprint/i;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/settings/fingerprint/O;->bci:Lcom/android/settings/fingerprint/FingerprintEnrollSidecar;

    invoke-static {v0}, Lcom/android/settings/fingerprint/FingerprintEnrollSidecar;->aPJ(Lcom/android/settings/fingerprint/FingerprintEnrollSidecar;)Lcom/android/settings/fingerprint/i;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/fingerprint/O;->bci:Lcom/android/settings/fingerprint/FingerprintEnrollSidecar;

    invoke-static {v1}, Lcom/android/settings/fingerprint/FingerprintEnrollSidecar;->aPI(Lcom/android/settings/fingerprint/FingerprintEnrollSidecar;)I

    move-result v1

    invoke-interface {v0, v1, p1}, Lcom/android/settings/fingerprint/i;->aOM(II)V

    :goto_0
    return-void

    :cond_2
    iget-object v0, p0, Lcom/android/settings/fingerprint/O;->bci:Lcom/android/settings/fingerprint/FingerprintEnrollSidecar;

    invoke-static {v0}, Lcom/android/settings/fingerprint/FingerprintEnrollSidecar;->aPK(Lcom/android/settings/fingerprint/FingerprintEnrollSidecar;)Ljava/util/ArrayList;

    move-result-object v0

    new-instance v1, Lcom/android/settings/fingerprint/f;

    iget-object v2, p0, Lcom/android/settings/fingerprint/O;->bci:Lcom/android/settings/fingerprint/FingerprintEnrollSidecar;

    iget-object v3, p0, Lcom/android/settings/fingerprint/O;->bci:Lcom/android/settings/fingerprint/FingerprintEnrollSidecar;

    invoke-static {v3}, Lcom/android/settings/fingerprint/FingerprintEnrollSidecar;->aPI(Lcom/android/settings/fingerprint/FingerprintEnrollSidecar;)I

    move-result v3

    invoke-direct {v1, v2, v3, p1}, Lcom/android/settings/fingerprint/f;-><init>(Lcom/android/settings/fingerprint/FingerprintEnrollSidecar;II)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method
