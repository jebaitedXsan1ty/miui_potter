.class public Lcom/android/settings/fingerprint/FingerprintRemoveSidecar;
.super Lcom/android/settings/core/InstrumentedPreferenceFragment;
.source "FingerprintRemoveSidecar.java"


# instance fields
.field bbq:Landroid/hardware/fingerprint/FingerprintManager;

.field private bbr:Landroid/hardware/fingerprint/Fingerprint;

.field private bbs:Ljava/util/Queue;

.field private bbt:Lcom/android/settings/fingerprint/k;

.field private bbu:Landroid/hardware/fingerprint/FingerprintManager$RemovalCallback;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/android/settings/core/InstrumentedPreferenceFragment;-><init>()V

    new-instance v0, Lcom/android/settings/fingerprint/Q;

    invoke-direct {v0, p0}, Lcom/android/settings/fingerprint/Q;-><init>(Lcom/android/settings/fingerprint/FingerprintRemoveSidecar;)V

    iput-object v0, p0, Lcom/android/settings/fingerprint/FingerprintRemoveSidecar;->bbu:Landroid/hardware/fingerprint/FingerprintManager$RemovalCallback;

    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/android/settings/fingerprint/FingerprintRemoveSidecar;->bbs:Ljava/util/Queue;

    return-void
.end method

.method static synthetic aPV(Lcom/android/settings/fingerprint/FingerprintRemoveSidecar;)Ljava/util/Queue;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/fingerprint/FingerprintRemoveSidecar;->bbs:Ljava/util/Queue;

    return-object v0
.end method

.method static synthetic aPW(Lcom/android/settings/fingerprint/FingerprintRemoveSidecar;)Lcom/android/settings/fingerprint/k;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/fingerprint/FingerprintRemoveSidecar;->bbt:Lcom/android/settings/fingerprint/k;

    return-object v0
.end method

.method static synthetic aPX(Lcom/android/settings/fingerprint/FingerprintRemoveSidecar;Landroid/hardware/fingerprint/Fingerprint;)Landroid/hardware/fingerprint/Fingerprint;
    .locals 0

    iput-object p1, p0, Lcom/android/settings/fingerprint/FingerprintRemoveSidecar;->bbr:Landroid/hardware/fingerprint/Fingerprint;

    return-object p1
.end method


# virtual methods
.method public EW(Landroid/os/Bundle;Ljava/lang/String;)V
    .locals 0

    return-void
.end method

.method final aPQ()Z
    .locals 1

    iget-object v0, p0, Lcom/android/settings/fingerprint/FingerprintRemoveSidecar;->bbr:Landroid/hardware/fingerprint/Fingerprint;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public aPR(Landroid/hardware/fingerprint/FingerprintManager;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/fingerprint/FingerprintRemoveSidecar;->bbq:Landroid/hardware/fingerprint/FingerprintManager;

    return-void
.end method

.method public aPS(Lcom/android/settings/fingerprint/k;)V
    .locals 3

    iget-object v0, p0, Lcom/android/settings/fingerprint/FingerprintRemoveSidecar;->bbt:Lcom/android/settings/fingerprint/k;

    if-nez v0, :cond_2

    if-eqz p1, :cond_2

    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/android/settings/fingerprint/FingerprintRemoveSidecar;->bbs:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/android/settings/fingerprint/FingerprintRemoveSidecar;->bbs:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->poll()Ljava/lang/Object;

    move-result-object v0

    instance-of v1, v0, Landroid/hardware/fingerprint/Fingerprint;

    if-eqz v1, :cond_1

    check-cast v0, Landroid/hardware/fingerprint/Fingerprint;

    invoke-interface {p1, v0}, Lcom/android/settings/fingerprint/k;->aPY(Landroid/hardware/fingerprint/Fingerprint;)V

    goto :goto_0

    :cond_1
    instance-of v1, v0, Lcom/android/settings/fingerprint/j;

    if-eqz v1, :cond_0

    check-cast v0, Lcom/android/settings/fingerprint/j;

    iget-object v1, v0, Lcom/android/settings/fingerprint/j;->bbx:Landroid/hardware/fingerprint/Fingerprint;

    iget v2, v0, Lcom/android/settings/fingerprint/j;->bbv:I

    iget-object v0, v0, Lcom/android/settings/fingerprint/j;->bbw:Ljava/lang/CharSequence;

    invoke-interface {p1, v1, v2, v0}, Lcom/android/settings/fingerprint/k;->onRemovalError(Landroid/hardware/fingerprint/Fingerprint;ILjava/lang/CharSequence;)V

    goto :goto_0

    :cond_2
    iput-object p1, p0, Lcom/android/settings/fingerprint/FingerprintRemoveSidecar;->bbt:Lcom/android/settings/fingerprint/k;

    return-void
.end method

.method final aPT(I)Z
    .locals 2

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/android/settings/fingerprint/FingerprintRemoveSidecar;->aPQ()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/settings/fingerprint/FingerprintRemoveSidecar;->bbr:Landroid/hardware/fingerprint/Fingerprint;

    invoke-virtual {v1}, Landroid/hardware/fingerprint/Fingerprint;->getFingerId()I

    move-result v1

    if-ne v1, p1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method public aPU(Landroid/hardware/fingerprint/Fingerprint;I)V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/fingerprint/FingerprintRemoveSidecar;->bbr:Landroid/hardware/fingerprint/Fingerprint;

    if-eqz v0, :cond_0

    const-string/jumbo v0, "FingerprintRemoveSidecar"

    const-string/jumbo v1, "Remove already in progress"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :cond_0
    const/16 v0, -0x2710

    if-eq p2, v0, :cond_1

    iget-object v0, p0, Lcom/android/settings/fingerprint/FingerprintRemoveSidecar;->bbq:Landroid/hardware/fingerprint/FingerprintManager;

    invoke-virtual {v0, p2}, Landroid/hardware/fingerprint/FingerprintManager;->setActiveUser(I)V

    :cond_1
    iput-object p1, p0, Lcom/android/settings/fingerprint/FingerprintRemoveSidecar;->bbr:Landroid/hardware/fingerprint/Fingerprint;

    iget-object v0, p0, Lcom/android/settings/fingerprint/FingerprintRemoveSidecar;->bbq:Landroid/hardware/fingerprint/FingerprintManager;

    iget-object v1, p0, Lcom/android/settings/fingerprint/FingerprintRemoveSidecar;->bbu:Landroid/hardware/fingerprint/FingerprintManager$RemovalCallback;

    invoke-virtual {v0, p1, p2, v1}, Landroid/hardware/fingerprint/FingerprintManager;->remove(Landroid/hardware/fingerprint/Fingerprint;ILandroid/hardware/fingerprint/FingerprintManager$RemovalCallback;)V

    return-void
.end method

.method public getMetricsCategory()I
    .locals 1

    const/16 v0, 0x3a6

    return v0
.end method

.method public onAttach(Landroid/content/Context;)V
    .locals 0

    invoke-super {p0, p1}, Lcom/android/settings/core/InstrumentedPreferenceFragment;->onAttach(Landroid/content/Context;)V

    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1

    invoke-super {p0, p1}, Lcom/android/settings/core/InstrumentedPreferenceFragment;->onCreate(Landroid/os/Bundle;)V

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/android/settings/fingerprint/FingerprintRemoveSidecar;->setRetainInstance(Z)V

    return-void
.end method

.method public onResume()V
    .locals 0

    invoke-super {p0}, Lcom/android/settings/core/InstrumentedPreferenceFragment;->onResume()V

    return-void
.end method
