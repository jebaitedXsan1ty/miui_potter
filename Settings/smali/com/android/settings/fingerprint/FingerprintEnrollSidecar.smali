.class public Lcom/android/settings/fingerprint/FingerprintEnrollSidecar;
.super Lcom/android/settings/core/InstrumentedFragment;
.source "FingerprintEnrollSidecar.java"


# instance fields
.field private baU:Z

.field private baV:Z

.field private baW:Landroid/hardware/fingerprint/FingerprintManager$EnrollmentCallback;

.field private baX:Landroid/os/CancellationSignal;

.field private baY:I

.field private baZ:I

.field private bba:Lcom/android/settings/c/b;

.field private bbb:Landroid/os/Handler;

.field private bbc:Lcom/android/settings/fingerprint/i;

.field private bbd:Ljava/util/ArrayList;

.field private final bbe:Ljava/lang/Runnable;

.field private bbf:[B

.field private mUserId:I


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/android/settings/core/InstrumentedFragment;-><init>()V

    const/4 v0, -0x1

    iput v0, p0, Lcom/android/settings/fingerprint/FingerprintEnrollSidecar;->baZ:I

    const/4 v0, 0x0

    iput v0, p0, Lcom/android/settings/fingerprint/FingerprintEnrollSidecar;->baY:I

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/android/settings/fingerprint/FingerprintEnrollSidecar;->bbb:Landroid/os/Handler;

    new-instance v0, Lcom/android/settings/fingerprint/O;

    invoke-direct {v0, p0}, Lcom/android/settings/fingerprint/O;-><init>(Lcom/android/settings/fingerprint/FingerprintEnrollSidecar;)V

    iput-object v0, p0, Lcom/android/settings/fingerprint/FingerprintEnrollSidecar;->baW:Landroid/hardware/fingerprint/FingerprintManager$EnrollmentCallback;

    new-instance v0, Lcom/android/settings/fingerprint/P;

    invoke-direct {v0, p0}, Lcom/android/settings/fingerprint/P;-><init>(Lcom/android/settings/fingerprint/FingerprintEnrollSidecar;)V

    iput-object v0, p0, Lcom/android/settings/fingerprint/FingerprintEnrollSidecar;->bbe:Ljava/lang/Runnable;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/settings/fingerprint/FingerprintEnrollSidecar;->bbd:Ljava/util/ArrayList;

    return-void
.end method

.method private aPH()V
    .locals 6

    iget-object v0, p0, Lcom/android/settings/fingerprint/FingerprintEnrollSidecar;->bbb:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/settings/fingerprint/FingerprintEnrollSidecar;->bbe:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    const/4 v0, -0x1

    iput v0, p0, Lcom/android/settings/fingerprint/FingerprintEnrollSidecar;->baZ:I

    new-instance v0, Landroid/os/CancellationSignal;

    invoke-direct {v0}, Landroid/os/CancellationSignal;-><init>()V

    iput-object v0, p0, Lcom/android/settings/fingerprint/FingerprintEnrollSidecar;->baX:Landroid/os/CancellationSignal;

    iget v0, p0, Lcom/android/settings/fingerprint/FingerprintEnrollSidecar;->mUserId:I

    const/16 v1, -0x2710

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/android/settings/fingerprint/FingerprintEnrollSidecar;->bba:Lcom/android/settings/c/b;

    iget v1, p0, Lcom/android/settings/fingerprint/FingerprintEnrollSidecar;->mUserId:I

    invoke-interface {v0, v1}, Lcom/android/settings/c/b;->aTT(I)V

    :cond_0
    iget-object v0, p0, Lcom/android/settings/fingerprint/FingerprintEnrollSidecar;->bba:Lcom/android/settings/c/b;

    iget-object v1, p0, Lcom/android/settings/fingerprint/FingerprintEnrollSidecar;->bbf:[B

    iget-object v2, p0, Lcom/android/settings/fingerprint/FingerprintEnrollSidecar;->baX:Landroid/os/CancellationSignal;

    iget v4, p0, Lcom/android/settings/fingerprint/FingerprintEnrollSidecar;->mUserId:I

    iget-object v5, p0, Lcom/android/settings/fingerprint/FingerprintEnrollSidecar;->baW:Landroid/hardware/fingerprint/FingerprintManager$EnrollmentCallback;

    const/4 v3, 0x0

    invoke-interface/range {v0 .. v5}, Lcom/android/settings/c/b;->aTS([BLandroid/os/CancellationSignal;IILandroid/hardware/fingerprint/FingerprintManager$EnrollmentCallback;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/settings/fingerprint/FingerprintEnrollSidecar;->baV:Z

    return-void
.end method

.method static synthetic aPI(Lcom/android/settings/fingerprint/FingerprintEnrollSidecar;)I
    .locals 1

    iget v0, p0, Lcom/android/settings/fingerprint/FingerprintEnrollSidecar;->baZ:I

    return v0
.end method

.method static synthetic aPJ(Lcom/android/settings/fingerprint/FingerprintEnrollSidecar;)Lcom/android/settings/fingerprint/i;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/fingerprint/FingerprintEnrollSidecar;->bbc:Lcom/android/settings/fingerprint/i;

    return-object v0
.end method

.method static synthetic aPK(Lcom/android/settings/fingerprint/FingerprintEnrollSidecar;)Ljava/util/ArrayList;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/fingerprint/FingerprintEnrollSidecar;->bbd:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic aPL(Lcom/android/settings/fingerprint/FingerprintEnrollSidecar;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/android/settings/fingerprint/FingerprintEnrollSidecar;->baU:Z

    return p1
.end method

.method static synthetic aPM(Lcom/android/settings/fingerprint/FingerprintEnrollSidecar;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/android/settings/fingerprint/FingerprintEnrollSidecar;->baV:Z

    return p1
.end method

.method static synthetic aPN(Lcom/android/settings/fingerprint/FingerprintEnrollSidecar;I)I
    .locals 0

    iput p1, p0, Lcom/android/settings/fingerprint/FingerprintEnrollSidecar;->baY:I

    return p1
.end method

.method static synthetic aPO(Lcom/android/settings/fingerprint/FingerprintEnrollSidecar;I)I
    .locals 0

    iput p1, p0, Lcom/android/settings/fingerprint/FingerprintEnrollSidecar;->baZ:I

    return p1
.end method


# virtual methods
.method public aPD(Lcom/android/settings/fingerprint/i;)V
    .locals 3

    iput-object p1, p0, Lcom/android/settings/fingerprint/FingerprintEnrollSidecar;->bbc:Lcom/android/settings/fingerprint/i;

    iget-object v0, p0, Lcom/android/settings/fingerprint/FingerprintEnrollSidecar;->bbc:Lcom/android/settings/fingerprint/i;

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/android/settings/fingerprint/FingerprintEnrollSidecar;->bbd:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/fingerprint/FingerprintEnrollSidecar;->bbd:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settings/fingerprint/e;

    iget-object v2, p0, Lcom/android/settings/fingerprint/FingerprintEnrollSidecar;->bbc:Lcom/android/settings/fingerprint/i;

    invoke-virtual {v0, v2}, Lcom/android/settings/fingerprint/e;->aPP(Lcom/android/settings/fingerprint/i;)V

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/android/settings/fingerprint/FingerprintEnrollSidecar;->bbd:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    :cond_1
    return-void
.end method

.method aPE()Z
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/android/settings/fingerprint/FingerprintEnrollSidecar;->bbb:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/settings/fingerprint/FingerprintEnrollSidecar;->bbe:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    iget-boolean v0, p0, Lcom/android/settings/fingerprint/FingerprintEnrollSidecar;->baV:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/fingerprint/FingerprintEnrollSidecar;->baX:Landroid/os/CancellationSignal;

    invoke-virtual {v0}, Landroid/os/CancellationSignal;->cancel()V

    iput-boolean v2, p0, Lcom/android/settings/fingerprint/FingerprintEnrollSidecar;->baV:Z

    const/4 v0, -0x1

    iput v0, p0, Lcom/android/settings/fingerprint/FingerprintEnrollSidecar;->baZ:I

    const/4 v0, 0x1

    return v0

    :cond_0
    return v2
.end method

.method public aPF()I
    .locals 1

    iget v0, p0, Lcom/android/settings/fingerprint/FingerprintEnrollSidecar;->baZ:I

    return v0
.end method

.method public aPG()I
    .locals 1

    iget v0, p0, Lcom/android/settings/fingerprint/FingerprintEnrollSidecar;->baY:I

    return v0
.end method

.method public getMetricsCategory()I
    .locals 1

    const/16 v0, 0xf5

    return v0
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 3

    invoke-super {p0, p1}, Lcom/android/settings/core/InstrumentedFragment;->onAttach(Landroid/app/Activity;)V

    invoke-static {p1}, Lcom/android/settings/aq;->brn(Landroid/content/Context;)Lcom/android/settings/c/b;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/fingerprint/FingerprintEnrollSidecar;->bba:Lcom/android/settings/c/b;

    invoke-virtual {p1}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "hw_auth_token"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getByteArrayExtra(Ljava/lang/String;)[B

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/fingerprint/FingerprintEnrollSidecar;->bbf:[B

    invoke-virtual {p1}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "android.intent.extra.USER_ID"

    const/16 v2, -0x2710

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/android/settings/fingerprint/FingerprintEnrollSidecar;->mUserId:I

    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1

    invoke-super {p0, p1}, Lcom/android/settings/core/InstrumentedFragment;->onCreate(Landroid/os/Bundle;)V

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/android/settings/fingerprint/FingerprintEnrollSidecar;->setRetainInstance(Z)V

    return-void
.end method

.method public onStart()V
    .locals 1

    invoke-super {p0}, Lcom/android/settings/core/InstrumentedFragment;->onStart()V

    iget-boolean v0, p0, Lcom/android/settings/fingerprint/FingerprintEnrollSidecar;->baV:Z

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/android/settings/fingerprint/FingerprintEnrollSidecar;->aPH()V

    :cond_0
    return-void
.end method

.method public onStop()V
    .locals 1

    invoke-super {p0}, Lcom/android/settings/core/InstrumentedFragment;->onStop()V

    invoke-virtual {p0}, Lcom/android/settings/fingerprint/FingerprintEnrollSidecar;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->isChangingConfigurations()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/android/settings/fingerprint/FingerprintEnrollSidecar;->aPE()Z

    :cond_0
    return-void
.end method
