.class final Lcom/android/settings/fingerprint/R;
.super Ljava/lang/Object;
.source "FingerprintLocationAnimationVideoView.java"

# interfaces
.implements Landroid/view/TextureView$SurfaceTextureListener;


# instance fields
.field private bcl:Landroid/graphics/SurfaceTexture;

.field final synthetic bcm:Lcom/android/settings/fingerprint/FingerprintLocationAnimationVideoView;


# direct methods
.method constructor <init>(Lcom/android/settings/fingerprint/FingerprintLocationAnimationVideoView;)V
    .locals 1

    iput-object p1, p0, Lcom/android/settings/fingerprint/R;->bcm:Lcom/android/settings/fingerprint/FingerprintLocationAnimationVideoView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/settings/fingerprint/R;->bcl:Landroid/graphics/SurfaceTexture;

    return-void
.end method


# virtual methods
.method public onSurfaceTextureAvailable(Landroid/graphics/SurfaceTexture;II)V
    .locals 4

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/android/settings/fingerprint/R;->bcm:Lcom/android/settings/fingerprint/FingerprintLocationAnimationVideoView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/android/settings/fingerprint/FingerprintLocationAnimationVideoView;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/settings/fingerprint/R;->bcm:Lcom/android/settings/fingerprint/FingerprintLocationAnimationVideoView;

    invoke-virtual {v0}, Lcom/android/settings/fingerprint/FingerprintLocationAnimationVideoView;->getFingerprintLocationAnimation()Landroid/net/Uri;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/fingerprint/R;->bcm:Lcom/android/settings/fingerprint/FingerprintLocationAnimationVideoView;

    iget-object v1, v1, Lcom/android/settings/fingerprint/FingerprintLocationAnimationVideoView;->bbA:Landroid/media/MediaPlayer;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/settings/fingerprint/R;->bcm:Lcom/android/settings/fingerprint/FingerprintLocationAnimationVideoView;

    iget-object v1, v1, Lcom/android/settings/fingerprint/FingerprintLocationAnimationVideoView;->bbA:Landroid/media/MediaPlayer;

    invoke-virtual {v1}, Landroid/media/MediaPlayer;->release()V

    :cond_0
    iget-object v1, p0, Lcom/android/settings/fingerprint/R;->bcl:Landroid/graphics/SurfaceTexture;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/android/settings/fingerprint/R;->bcl:Landroid/graphics/SurfaceTexture;

    invoke-virtual {v1}, Landroid/graphics/SurfaceTexture;->release()V

    iput-object v2, p0, Lcom/android/settings/fingerprint/R;->bcl:Landroid/graphics/SurfaceTexture;

    :cond_1
    iget-object v1, p0, Lcom/android/settings/fingerprint/R;->bcm:Lcom/android/settings/fingerprint/FingerprintLocationAnimationVideoView;

    iget-object v2, p0, Lcom/android/settings/fingerprint/R;->bcm:Lcom/android/settings/fingerprint/FingerprintLocationAnimationVideoView;

    iget-object v3, p0, Lcom/android/settings/fingerprint/R;->bcm:Lcom/android/settings/fingerprint/FingerprintLocationAnimationVideoView;

    invoke-static {v3}, Lcom/android/settings/fingerprint/FingerprintLocationAnimationVideoView;->aQa(Lcom/android/settings/fingerprint/FingerprintLocationAnimationVideoView;)Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v2, v3, v0}, Lcom/android/settings/fingerprint/FingerprintLocationAnimationVideoView;->createMediaPlayer(Landroid/content/Context;Landroid/net/Uri;)Landroid/media/MediaPlayer;

    move-result-object v0

    iput-object v0, v1, Lcom/android/settings/fingerprint/FingerprintLocationAnimationVideoView;->bbA:Landroid/media/MediaPlayer;

    iget-object v0, p0, Lcom/android/settings/fingerprint/R;->bcm:Lcom/android/settings/fingerprint/FingerprintLocationAnimationVideoView;

    iget-object v0, v0, Lcom/android/settings/fingerprint/FingerprintLocationAnimationVideoView;->bbA:Landroid/media/MediaPlayer;

    if-nez v0, :cond_2

    return-void

    :cond_2
    iget-object v0, p0, Lcom/android/settings/fingerprint/R;->bcm:Lcom/android/settings/fingerprint/FingerprintLocationAnimationVideoView;

    iget-object v0, v0, Lcom/android/settings/fingerprint/FingerprintLocationAnimationVideoView;->bbA:Landroid/media/MediaPlayer;

    new-instance v1, Landroid/view/Surface;

    invoke-direct {v1, p1}, Landroid/view/Surface;-><init>(Landroid/graphics/SurfaceTexture;)V

    invoke-virtual {v0, v1}, Landroid/media/MediaPlayer;->setSurface(Landroid/view/Surface;)V

    iget-object v0, p0, Lcom/android/settings/fingerprint/R;->bcm:Lcom/android/settings/fingerprint/FingerprintLocationAnimationVideoView;

    iget-object v0, v0, Lcom/android/settings/fingerprint/FingerprintLocationAnimationVideoView;->bbA:Landroid/media/MediaPlayer;

    new-instance v1, Lcom/android/settings/fingerprint/S;

    invoke-direct {v1, p0}, Lcom/android/settings/fingerprint/S;-><init>(Lcom/android/settings/fingerprint/R;)V

    invoke-virtual {v0, v1}, Landroid/media/MediaPlayer;->setOnPreparedListener(Landroid/media/MediaPlayer$OnPreparedListener;)V

    iget-object v0, p0, Lcom/android/settings/fingerprint/R;->bcm:Lcom/android/settings/fingerprint/FingerprintLocationAnimationVideoView;

    iget-object v0, v0, Lcom/android/settings/fingerprint/FingerprintLocationAnimationVideoView;->bbA:Landroid/media/MediaPlayer;

    new-instance v1, Lcom/android/settings/fingerprint/T;

    invoke-direct {v1, p0}, Lcom/android/settings/fingerprint/T;-><init>(Lcom/android/settings/fingerprint/R;)V

    invoke-virtual {v0, v1}, Landroid/media/MediaPlayer;->setOnInfoListener(Landroid/media/MediaPlayer$OnInfoListener;)V

    iget-object v0, p0, Lcom/android/settings/fingerprint/R;->bcm:Lcom/android/settings/fingerprint/FingerprintLocationAnimationVideoView;

    iget-object v1, p0, Lcom/android/settings/fingerprint/R;->bcm:Lcom/android/settings/fingerprint/FingerprintLocationAnimationVideoView;

    iget-object v1, v1, Lcom/android/settings/fingerprint/FingerprintLocationAnimationVideoView;->bbA:Landroid/media/MediaPlayer;

    invoke-virtual {v1}, Landroid/media/MediaPlayer;->getVideoHeight()I

    move-result v1

    int-to-float v1, v1

    iget-object v2, p0, Lcom/android/settings/fingerprint/R;->bcm:Lcom/android/settings/fingerprint/FingerprintLocationAnimationVideoView;

    iget-object v2, v2, Lcom/android/settings/fingerprint/FingerprintLocationAnimationVideoView;->bbA:Landroid/media/MediaPlayer;

    invoke-virtual {v2}, Landroid/media/MediaPlayer;->getVideoWidth()I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v1, v2

    iput v1, v0, Lcom/android/settings/fingerprint/FingerprintLocationAnimationVideoView;->bbz:F

    iget-object v0, p0, Lcom/android/settings/fingerprint/R;->bcm:Lcom/android/settings/fingerprint/FingerprintLocationAnimationVideoView;

    invoke-virtual {v0}, Lcom/android/settings/fingerprint/FingerprintLocationAnimationVideoView;->requestLayout()V

    iget-object v0, p0, Lcom/android/settings/fingerprint/R;->bcm:Lcom/android/settings/fingerprint/FingerprintLocationAnimationVideoView;

    invoke-virtual {v0}, Lcom/android/settings/fingerprint/FingerprintLocationAnimationVideoView;->aPn()V

    return-void
.end method

.method public onSurfaceTextureDestroyed(Landroid/graphics/SurfaceTexture;)Z
    .locals 1

    iput-object p1, p0, Lcom/android/settings/fingerprint/R;->bcl:Landroid/graphics/SurfaceTexture;

    const/4 v0, 0x0

    return v0
.end method

.method public onSurfaceTextureSizeChanged(Landroid/graphics/SurfaceTexture;II)V
    .locals 0

    return-void
.end method

.method public onSurfaceTextureUpdated(Landroid/graphics/SurfaceTexture;)V
    .locals 0

    return-void
.end method
