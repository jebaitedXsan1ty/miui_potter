.class public Lcom/android/settings/datetime/l;
.super Lcom/android/settings/core/e;
.source "DatePreferenceController.java"

# interfaces
.implements Lmiui/app/DatePickerDialog$OnDateSetListener;


# instance fields
.field private final aBU:Lcom/android/settings/datetime/p;

.field private final aBV:Lcom/android/settings/datetime/m;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/android/settings/datetime/m;Lcom/android/settings/datetime/p;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/settings/core/e;-><init>(Landroid/content/Context;)V

    iput-object p2, p0, Lcom/android/settings/datetime/l;->aBV:Lcom/android/settings/datetime/m;

    iput-object p3, p0, Lcom/android/settings/datetime/l;->aBU:Lcom/android/settings/datetime/p;

    return-void
.end method


# virtual methods
.method public apH(Landroid/app/Activity;)Lmiui/app/DatePickerDialog;
    .locals 8

    const/4 v7, 0x1

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v6

    new-instance v0, Lmiui/app/DatePickerDialog;

    invoke-virtual {v6, v7}, Ljava/util/Calendar;->get(I)I

    move-result v3

    const/4 v1, 0x2

    invoke-virtual {v6, v1}, Ljava/util/Calendar;->get(I)I

    move-result v4

    const/4 v1, 0x5

    invoke-virtual {v6, v1}, Ljava/util/Calendar;->get(I)I

    move-result v5

    move-object v1, p1

    move-object v2, p0

    invoke-direct/range {v0 .. v5}, Lmiui/app/DatePickerDialog;-><init>(Landroid/content/Context;Lmiui/app/DatePickerDialog$OnDateSetListener;III)V

    invoke-virtual {v6}, Ljava/util/Calendar;->clear()V

    const/16 v1, 0x7d7

    const/4 v2, 0x0

    invoke-virtual {v6, v1, v2, v7}, Ljava/util/Calendar;->set(III)V

    invoke-virtual {v0}, Lmiui/app/DatePickerDialog;->getDatePicker()Lmiui/widget/DatePicker;

    move-result-object v1

    invoke-virtual {v6}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lmiui/widget/DatePicker;->setMinDate(J)V

    invoke-virtual {v6}, Ljava/util/Calendar;->clear()V

    const/16 v1, 0x7f5

    const/16 v2, 0xb

    const/16 v3, 0x1f

    invoke-virtual {v6, v1, v2, v3}, Ljava/util/Calendar;->set(III)V

    invoke-virtual {v0}, Lmiui/app/DatePickerDialog;->getDatePicker()Lmiui/widget/DatePicker;

    move-result-object v1

    invoke-virtual {v6}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lmiui/widget/DatePicker;->setMaxDate(J)V

    return-object v0
.end method

.method apI(III)V
    .locals 6

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1, p1}, Ljava/util/Calendar;->set(II)V

    const/4 v1, 0x2

    invoke-virtual {v0, v1, p2}, Ljava/util/Calendar;->set(II)V

    const/4 v1, 0x5

    invoke-virtual {v0, v1, p3}, Ljava/util/Calendar;->set(II)V

    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v0

    const-wide v2, 0x1160d1b4800L

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v2

    const-wide/16 v0, 0x3e8

    div-long v0, v2, v0

    const-wide/32 v4, 0x7fffffff

    cmp-long v0, v0, v4

    if-gez v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/datetime/l;->mContext:Landroid/content/Context;

    const-string/jumbo v1, "alarm"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    invoke-virtual {v0, v2, v3}, Landroid/app/AlarmManager;->setTime(J)V

    :cond_0
    return-void
.end method

.method public cz(Landroid/preference/Preference;)V
    .locals 2

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/datetime/l;->mContext:Landroid/content/Context;

    invoke-static {v1}, Landroid/text/format/DateFormat;->getLongDateFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v1

    invoke-virtual {v0}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/android/settings/datetime/l;->aBU:Lcom/android/settings/datetime/p;

    invoke-virtual {v0}, Lcom/android/settings/datetime/p;->isEnabled()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    invoke-virtual {p1, v0}, Landroid/preference/Preference;->setEnabled(Z)V

    iget-object v0, p0, Lcom/android/settings/datetime/l;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-static {v0}, Landroid/provider/MiuiSettings$Secure;->isTimeChangeDisallow(Landroid/content/ContentResolver;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/preference/Preference;->setEnabled(Z)V

    :cond_0
    return-void
.end method

.method public fm(Landroid/preference/Preference;)Z
    .locals 2

    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "date"

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    return v0

    :cond_0
    iget-object v0, p0, Lcom/android/settings/datetime/l;->aBV:Lcom/android/settings/datetime/m;

    invoke-interface {v0}, Lcom/android/settings/datetime/m;->apJ()V

    const/4 v0, 0x1

    return v0
.end method

.method public l()Ljava/lang/String;
    .locals 1

    const-string/jumbo v0, "date"

    return-object v0
.end method

.method public onDateSet(Lmiui/widget/DatePicker;III)V
    .locals 2

    invoke-virtual {p0, p2, p3, p4}, Lcom/android/settings/datetime/l;->apI(III)V

    iget-object v0, p0, Lcom/android/settings/datetime/l;->aBV:Lcom/android/settings/datetime/m;

    iget-object v1, p0, Lcom/android/settings/datetime/l;->mContext:Landroid/content/Context;

    invoke-interface {v0, v1}, Lcom/android/settings/datetime/m;->apz(Landroid/content/Context;)V

    return-void
.end method

.method public p()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method
