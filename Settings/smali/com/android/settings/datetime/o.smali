.class Lcom/android/settings/datetime/o;
.super Landroid/widget/BaseAdapter;
.source "MiuiZonePickerSettings.java"


# instance fields
.field private aCj:Landroid/view/LayoutInflater;

.field private aCk:Ljava/util/List;

.field final synthetic aCl:Lcom/android/settings/datetime/MiuiZonePickerSettings;


# direct methods
.method public constructor <init>(Lcom/android/settings/datetime/MiuiZonePickerSettings;)V
    .locals 2

    iput-object p1, p0, Lcom/android/settings/datetime/o;->aCl:Lcom/android/settings/datetime/MiuiZonePickerSettings;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    invoke-static {p1}, Lcom/android/settings/datetime/MiuiZonePickerSettings;->apQ(Lcom/android/settings/datetime/MiuiZonePickerSettings;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/datetime/o;->aCj:Landroid/view/LayoutInflater;

    invoke-static {p1}, Lcom/android/settings/datetime/MiuiZonePickerSettings;->apU(Lcom/android/settings/datetime/MiuiZonePickerSettings;)Lcom/android/settings/datetime/a;

    move-result-object v0

    const-string/jumbo v1, ""

    invoke-virtual {v0, v1}, Lcom/android/settings/datetime/a;->apq(Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/datetime/o;->aCk:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public apY(Ljava/util/List;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/datetime/o;->aCk:Ljava/util/List;

    return-void
.end method

.method public getCount()I
    .locals 1

    iget-object v0, p0, Lcom/android/settings/datetime/o;->aCk:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/datetime/o;->aCk:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2

    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 5

    if-nez p2, :cond_0

    iget-object v0, p0, Lcom/android/settings/datetime/o;->aCj:Landroid/view/LayoutInflater;

    const v1, 0x7f0d0245

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    :cond_0
    invoke-virtual {p0, p1}, Lcom/android/settings/datetime/o;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settings/datetime/n;

    invoke-virtual {v0}, Lcom/android/settings/datetime/n;->apL()Ljava/lang/String;

    move-result-object v2

    iget-object v1, p0, Lcom/android/settings/datetime/o;->aCl:Lcom/android/settings/datetime/MiuiZonePickerSettings;

    iget-object v3, p0, Lcom/android/settings/datetime/o;->aCl:Lcom/android/settings/datetime/MiuiZonePickerSettings;

    invoke-static {v3}, Lcom/android/settings/datetime/MiuiZonePickerSettings;->apS(Lcom/android/settings/datetime/MiuiZonePickerSettings;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/android/settings/datetime/MiuiZonePickerSettings;->apW(Lcom/android/settings/datetime/MiuiZonePickerSettings;Ljava/lang/String;Ljava/lang/String;)Landroid/text/SpannableStringBuilder;

    move-result-object v3

    const v1, 0x7f0a0486

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_1

    :goto_0
    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v1, 0x7f0a0487

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    invoke-virtual {v0}, Lcom/android/settings/datetime/n;->apM()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-object p2

    :cond_1
    move-object v2, v3

    goto :goto_0
.end method
