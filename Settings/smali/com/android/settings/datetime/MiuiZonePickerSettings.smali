.class public Lcom/android/settings/datetime/MiuiZonePickerSettings;
.super Lmiui/app/Fragment;
.source "MiuiZonePickerSettings.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# instance fields
.field private aCa:Lmiui/app/ActionBar;

.field private aCb:Landroid/view/inputmethod/InputMethodManager;

.field private aCc:Landroid/widget/AbsListView$OnScrollListener;

.field private aCd:Ljava/lang/String;

.field private aCe:Landroid/widget/EditText;

.field private aCf:Landroid/text/TextWatcher;

.field private aCg:Landroid/widget/ListView;

.field private aCh:Lcom/android/settings/datetime/o;

.field private aCi:Lcom/android/settings/datetime/a;

.field private mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lmiui/app/Fragment;-><init>()V

    new-instance v0, Lcom/android/settings/datetime/r;

    invoke-direct {v0, p0}, Lcom/android/settings/datetime/r;-><init>(Lcom/android/settings/datetime/MiuiZonePickerSettings;)V

    iput-object v0, p0, Lcom/android/settings/datetime/MiuiZonePickerSettings;->aCf:Landroid/text/TextWatcher;

    return-void
.end method

.method private apO(Ljava/lang/String;Ljava/lang/String;)Landroid/text/SpannableStringBuilder;
    .locals 6

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x0

    return-object v0

    :cond_1
    new-instance v0, Landroid/text/SpannableStringBuilder;

    invoke-direct {v0, p1}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    invoke-virtual {p2}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v1

    const/16 v2, 0x10

    invoke-static {v1, v2}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;I)Ljava/util/regex/Pattern;

    move-result-object v1

    invoke-virtual {p1}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v1

    :goto_0
    invoke-virtual {v1}, Ljava/util/regex/Matcher;->find()Z

    move-result v2

    if-eqz v2, :cond_2

    new-instance v2, Landroid/text/style/ForegroundColorSpan;

    invoke-virtual {p0}, Lcom/android/settings/datetime/MiuiZonePickerSettings;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0600d6

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-direct {v2, v3}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    invoke-virtual {v1}, Ljava/util/regex/Matcher;->start()I

    move-result v3

    invoke-virtual {v1}, Ljava/util/regex/Matcher;->end()I

    move-result v4

    const/16 v5, 0x21

    invoke-virtual {v0, v2, v3, v4, v5}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    goto :goto_0

    :cond_2
    return-object v0
.end method

.method private apP(I)V
    .locals 1

    iget-object v0, p0, Lcom/android/settings/datetime/MiuiZonePickerSettings;->aCi:Lcom/android/settings/datetime/a;

    invoke-virtual {v0, p1}, Lcom/android/settings/datetime/a;->apr(I)V

    iget-object v0, p0, Lcom/android/settings/datetime/MiuiZonePickerSettings;->aCd:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/android/settings/datetime/MiuiZonePickerSettings;->onQueryTextSubmit(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic apQ(Lcom/android/settings/datetime/MiuiZonePickerSettings;)Landroid/content/Context;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/datetime/MiuiZonePickerSettings;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic apR(Lcom/android/settings/datetime/MiuiZonePickerSettings;)Landroid/view/inputmethod/InputMethodManager;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/datetime/MiuiZonePickerSettings;->aCb:Landroid/view/inputmethod/InputMethodManager;

    return-object v0
.end method

.method static synthetic apS(Lcom/android/settings/datetime/MiuiZonePickerSettings;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/datetime/MiuiZonePickerSettings;->aCd:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic apT(Lcom/android/settings/datetime/MiuiZonePickerSettings;)Landroid/widget/EditText;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/datetime/MiuiZonePickerSettings;->aCe:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic apU(Lcom/android/settings/datetime/MiuiZonePickerSettings;)Lcom/android/settings/datetime/a;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/datetime/MiuiZonePickerSettings;->aCi:Lcom/android/settings/datetime/a;

    return-object v0
.end method

.method static synthetic apV(Lcom/android/settings/datetime/MiuiZonePickerSettings;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    iput-object p1, p0, Lcom/android/settings/datetime/MiuiZonePickerSettings;->aCd:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic apW(Lcom/android/settings/datetime/MiuiZonePickerSettings;Ljava/lang/String;Ljava/lang/String;)Landroid/text/SpannableStringBuilder;
    .locals 1

    invoke-direct {p0, p1, p2}, Lcom/android/settings/datetime/MiuiZonePickerSettings;->apO(Ljava/lang/String;Ljava/lang/String;)Landroid/text/SpannableStringBuilder;

    move-result-object v0

    return-object v0
.end method

.method static synthetic apX(Lcom/android/settings/datetime/MiuiZonePickerSettings;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/datetime/MiuiZonePickerSettings;->finish()V

    return-void
.end method

.method private finish()V
    .locals 3

    iget-object v0, p0, Lcom/android/settings/datetime/MiuiZonePickerSettings;->aCb:Landroid/view/inputmethod/InputMethodManager;

    iget-object v1, p0, Lcom/android/settings/datetime/MiuiZonePickerSettings;->aCe:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    invoke-virtual {p0}, Lcom/android/settings/datetime/MiuiZonePickerSettings;->isResumed()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/android/settings/datetime/MiuiZonePickerSettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->onBackPressed()V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/android/settings/datetime/MiuiZonePickerSettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    goto :goto_0
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 1

    invoke-super {p0, p1}, Lmiui/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    const v0, 0x7f13021a

    invoke-virtual {p0, v0}, Lcom/android/settings/datetime/MiuiZonePickerSettings;->setThemeRes(I)V

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/android/settings/datetime/MiuiZonePickerSettings;->setHasOptionsMenu(Z)V

    invoke-virtual {p0}, Lcom/android/settings/datetime/MiuiZonePickerSettings;->getContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/datetime/MiuiZonePickerSettings;->mContext:Landroid/content/Context;

    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 2

    invoke-virtual {p0}, Lcom/android/settings/datetime/MiuiZonePickerSettings;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const v1, 0x7f0e0008

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    invoke-super {p0, p1}, Lmiui/app/Fragment;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    move-result v0

    return v0
.end method

.method public onInflateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    const/4 v4, 0x0

    const v0, 0x7f0d0244

    invoke-virtual {p1, v0, p2, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const v0, 0x7f0a026f

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/android/settings/datetime/MiuiZonePickerSettings;->aCg:Landroid/widget/ListView;

    iget-object v0, p0, Lcom/android/settings/datetime/MiuiZonePickerSettings;->aCg:Landroid/widget/ListView;

    invoke-virtual {v0, p0}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    invoke-virtual {p0}, Lcom/android/settings/datetime/MiuiZonePickerSettings;->getActionBar()Lmiui/app/ActionBar;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/datetime/MiuiZonePickerSettings;->aCa:Lmiui/app/ActionBar;

    iget-object v0, p0, Lcom/android/settings/datetime/MiuiZonePickerSettings;->aCa:Lmiui/app/ActionBar;

    const v2, 0x7f0d0246

    invoke-virtual {v0, v2}, Lmiui/app/ActionBar;->setCustomView(I)V

    iget-object v0, p0, Lcom/android/settings/datetime/MiuiZonePickerSettings;->aCa:Lmiui/app/ActionBar;

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Lmiui/app/ActionBar;->setDisplayShowCustomEnabled(Z)V

    iget-object v0, p0, Lcom/android/settings/datetime/MiuiZonePickerSettings;->aCa:Lmiui/app/ActionBar;

    invoke-virtual {v0, v4}, Lmiui/app/ActionBar;->setDisplayShowTitleEnabled(Z)V

    iget-object v0, p0, Lcom/android/settings/datetime/MiuiZonePickerSettings;->aCa:Lmiui/app/ActionBar;

    invoke-virtual {v0}, Lmiui/app/ActionBar;->getCustomView()Landroid/view/View;

    move-result-object v2

    const v0, 0x1020009

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/android/settings/datetime/MiuiZonePickerSettings;->aCe:Landroid/widget/EditText;

    iget-object v0, p0, Lcom/android/settings/datetime/MiuiZonePickerSettings;->aCe:Landroid/widget/EditText;

    iget-object v3, p0, Lcom/android/settings/datetime/MiuiZonePickerSettings;->aCf:Landroid/text/TextWatcher;

    invoke-virtual {v0, v3}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    const v0, 0x102002c

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    new-instance v2, Lcom/android/settings/datetime/s;

    invoke-direct {v2, p0}, Lcom/android/settings/datetime/s;-><init>(Lcom/android/settings/datetime/MiuiZonePickerSettings;)V

    invoke-virtual {v0, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/android/settings/datetime/MiuiZonePickerSettings;->mContext:Landroid/content/Context;

    const-string/jumbo v2, "input_method"

    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    iput-object v0, p0, Lcom/android/settings/datetime/MiuiZonePickerSettings;->aCb:Landroid/view/inputmethod/InputMethodManager;

    new-instance v0, Lcom/android/settings/datetime/a;

    iget-object v2, p0, Lcom/android/settings/datetime/MiuiZonePickerSettings;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v0, v2}, Lcom/android/settings/datetime/a;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/settings/datetime/MiuiZonePickerSettings;->aCi:Lcom/android/settings/datetime/a;

    new-instance v0, Lcom/android/settings/datetime/o;

    invoke-direct {v0, p0}, Lcom/android/settings/datetime/o;-><init>(Lcom/android/settings/datetime/MiuiZonePickerSettings;)V

    iput-object v0, p0, Lcom/android/settings/datetime/MiuiZonePickerSettings;->aCh:Lcom/android/settings/datetime/o;

    iget-object v0, p0, Lcom/android/settings/datetime/MiuiZonePickerSettings;->aCg:Landroid/widget/ListView;

    iget-object v2, p0, Lcom/android/settings/datetime/MiuiZonePickerSettings;->aCh:Lcom/android/settings/datetime/o;

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    new-instance v0, Lcom/android/settings/datetime/t;

    invoke-direct {v0, p0}, Lcom/android/settings/datetime/t;-><init>(Lcom/android/settings/datetime/MiuiZonePickerSettings;)V

    iput-object v0, p0, Lcom/android/settings/datetime/MiuiZonePickerSettings;->aCc:Landroid/widget/AbsListView$OnScrollListener;

    iget-object v0, p0, Lcom/android/settings/datetime/MiuiZonePickerSettings;->aCi:Lcom/android/settings/datetime/a;

    invoke-virtual {v0}, Lcom/android/settings/datetime/a;->apo()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/android/settings/datetime/MiuiZonePickerSettings;->apP(I)V

    return-object v1
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 4

    iget-object v0, p0, Lcom/android/settings/datetime/MiuiZonePickerSettings;->aCh:Lcom/android/settings/datetime/o;

    invoke-virtual {v0, p3}, Lcom/android/settings/datetime/o;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settings/datetime/n;

    iget-object v1, p0, Lcom/android/settings/datetime/MiuiZonePickerSettings;->mContext:Landroid/content/Context;

    const-string/jumbo v2, "alarm"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/AlarmManager;

    invoke-virtual {v0}, Lcom/android/settings/datetime/n;->apN()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/AlarmManager;->setTimeZone(Ljava/lang/String;)V

    const-string/jumbo v1, "MiuiZonePickerSettings"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "Timezone changed: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Lcom/android/settings/datetime/n;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/android/settings/datetime/MiuiZonePickerSettings;->finish()V

    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :goto_0
    invoke-super {p0, p1}, Lmiui/app/Fragment;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    return v0

    :pswitch_0
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/android/settings/datetime/MiuiZonePickerSettings;->apP(I)V

    goto :goto_0

    :pswitch_1
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/android/settings/datetime/MiuiZonePickerSettings;->apP(I)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x7f0a0417
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public onQueryTextSubmit(Ljava/lang/String;)V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/datetime/MiuiZonePickerSettings;->aCh:Lcom/android/settings/datetime/o;

    iget-object v1, p0, Lcom/android/settings/datetime/MiuiZonePickerSettings;->aCi:Lcom/android/settings/datetime/a;

    invoke-virtual {v1, p1}, Lcom/android/settings/datetime/a;->apq(Ljava/lang/String;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/settings/datetime/o;->apY(Ljava/util/List;)V

    iget-object v0, p0, Lcom/android/settings/datetime/MiuiZonePickerSettings;->aCh:Lcom/android/settings/datetime/o;

    invoke-virtual {v0}, Lcom/android/settings/datetime/o;->notifyDataSetChanged()V

    return-void
.end method
