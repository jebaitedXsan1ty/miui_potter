.class public Lcom/android/settings/datetime/b;
.super Ljava/lang/Object;
.source "ZonePickerHelper.java"

# interfaces
.implements Ljava/util/Comparator;


# instance fields
.field private aBG:Lcom/android/settings/datetime/c;

.field final synthetic aBH:Lcom/android/settings/datetime/a;


# direct methods
.method public constructor <init>(Lcom/android/settings/datetime/a;)V
    .locals 1

    iput-object p1, p0, Lcom/android/settings/datetime/b;->aBH:Lcom/android/settings/datetime/a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/settings/datetime/b;->aBG:Lcom/android/settings/datetime/c;

    return-void
.end method

.method private apu()Ljava/util/Comparator;
    .locals 2

    iget-object v0, p0, Lcom/android/settings/datetime/b;->aBG:Lcom/android/settings/datetime/c;

    if-nez v0, :cond_0

    new-instance v0, Lcom/android/settings/datetime/c;

    iget-object v1, p0, Lcom/android/settings/datetime/b;->aBH:Lcom/android/settings/datetime/a;

    invoke-direct {v0, v1}, Lcom/android/settings/datetime/c;-><init>(Lcom/android/settings/datetime/a;)V

    iput-object v0, p0, Lcom/android/settings/datetime/b;->aBG:Lcom/android/settings/datetime/c;

    :cond_0
    iget-object v0, p0, Lcom/android/settings/datetime/b;->aBG:Lcom/android/settings/datetime/c;

    return-object v0
.end method


# virtual methods
.method public apt(Lcom/android/settings/datetime/n;Lcom/android/settings/datetime/n;)I
    .locals 4

    const/4 v1, 0x1

    const/4 v0, -0x1

    if-nez p1, :cond_0

    if-nez p2, :cond_0

    const/4 v0, 0x0

    return v0

    :cond_0
    if-nez p1, :cond_1

    return v0

    :cond_1
    if-nez p2, :cond_2

    return v1

    :cond_2
    invoke-virtual {p1}, Lcom/android/settings/datetime/n;->apK()I

    move-result v2

    invoke-virtual {p2}, Lcom/android/settings/datetime/n;->apK()I

    move-result v3

    if-ge v2, v3, :cond_3

    :goto_0
    return v0

    :cond_3
    if-le v2, v3, :cond_4

    move v0, v1

    goto :goto_0

    :cond_4
    invoke-direct {p0}, Lcom/android/settings/datetime/b;->apu()Ljava/util/Comparator;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Ljava/util/Comparator;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v0

    goto :goto_0
.end method

.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1

    check-cast p1, Lcom/android/settings/datetime/n;

    check-cast p2, Lcom/android/settings/datetime/n;

    invoke-virtual {p0, p1, p2}, Lcom/android/settings/datetime/b;->apt(Lcom/android/settings/datetime/n;Lcom/android/settings/datetime/n;)I

    move-result v0

    return v0
.end method
