.class final enum Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$LeftButtonMode;
.super Ljava/lang/Enum;
.source "SetUpChooseLockPattern.java"


# static fields
.field public static final enum bNs:Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$LeftButtonMode;

.field public static final enum bNt:Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$LeftButtonMode;

.field public static final enum bNu:Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$LeftButtonMode;

.field public static final enum bNv:Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$LeftButtonMode;

.field public static final enum bNw:Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$LeftButtonMode;

.field private static final synthetic bNx:[Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$LeftButtonMode;


# instance fields
.field final enabled:Z

.field final text:I


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    new-instance v0, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$LeftButtonMode;

    const-string/jumbo v1, "Cancel"

    const v2, 0x7f1203c7

    invoke-direct {v0, v1, v3, v2, v4}, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$LeftButtonMode;-><init>(Ljava/lang/String;IIZ)V

    sput-object v0, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$LeftButtonMode;->bNs:Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$LeftButtonMode;

    new-instance v0, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$LeftButtonMode;

    const-string/jumbo v1, "CancelDisabled"

    const v2, 0x7f1203c7

    invoke-direct {v0, v1, v4, v2, v3}, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$LeftButtonMode;-><init>(Ljava/lang/String;IIZ)V

    sput-object v0, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$LeftButtonMode;->bNt:Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$LeftButtonMode;

    new-instance v0, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$LeftButtonMode;

    const-string/jumbo v1, "Retry"

    const v2, 0x7f1209d7

    invoke-direct {v0, v1, v5, v2, v4}, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$LeftButtonMode;-><init>(Ljava/lang/String;IIZ)V

    sput-object v0, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$LeftButtonMode;->bNv:Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$LeftButtonMode;

    new-instance v0, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$LeftButtonMode;

    const-string/jumbo v1, "RetryDisabled"

    const v2, 0x7f1209d7

    invoke-direct {v0, v1, v6, v2, v3}, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$LeftButtonMode;-><init>(Ljava/lang/String;IIZ)V

    sput-object v0, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$LeftButtonMode;->bNw:Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$LeftButtonMode;

    new-instance v0, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$LeftButtonMode;

    const-string/jumbo v1, "Gone"

    const/4 v2, -0x1

    invoke-direct {v0, v1, v7, v2, v3}, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$LeftButtonMode;-><init>(Ljava/lang/String;IIZ)V

    sput-object v0, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$LeftButtonMode;->bNu:Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$LeftButtonMode;

    const/4 v0, 0x5

    new-array v0, v0, [Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$LeftButtonMode;

    sget-object v1, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$LeftButtonMode;->bNs:Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$LeftButtonMode;

    aput-object v1, v0, v3

    sget-object v1, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$LeftButtonMode;->bNt:Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$LeftButtonMode;

    aput-object v1, v0, v4

    sget-object v1, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$LeftButtonMode;->bNv:Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$LeftButtonMode;

    aput-object v1, v0, v5

    sget-object v1, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$LeftButtonMode;->bNw:Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$LeftButtonMode;

    aput-object v1, v0, v6

    sget-object v1, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$LeftButtonMode;->bNu:Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$LeftButtonMode;

    aput-object v1, v0, v7

    sput-object v0, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$LeftButtonMode;->bNx:[Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$LeftButtonMode;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IIZ)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput p3, p0, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$LeftButtonMode;->text:I

    iput-boolean p4, p0, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$LeftButtonMode;->enabled:Z

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$LeftButtonMode;
    .locals 1

    const-class v0, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$LeftButtonMode;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$LeftButtonMode;

    return-object v0
.end method

.method public static values()[Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$LeftButtonMode;
    .locals 1

    sget-object v0, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$LeftButtonMode;->bNx:[Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$LeftButtonMode;

    return-object v0
.end method
