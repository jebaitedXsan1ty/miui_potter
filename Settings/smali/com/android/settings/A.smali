.class Lcom/android/settings/A;
.super Ljava/lang/Object;
.source "TrustedCredentialsDialogBuilder.java"

# interfaces
.implements Landroid/content/DialogInterface$OnShowListener;
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private final bvX:Landroid/app/Activity;

.field private bvY:[Lcom/android/settings/p;

.field private bvZ:I

.field private bwa:Landroid/view/View;

.field private final bwb:Lcom/android/settings/z;

.field private bwc:Landroid/app/AlertDialog;

.field private final bwd:Landroid/app/admin/DevicePolicyManager;

.field private bwe:Z

.field private bwf:Landroid/widget/Button;

.field private bwg:Landroid/widget/Button;

.field private final bwh:Landroid/widget/LinearLayout;

.field private final mUserManager:Landroid/os/UserManager;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Lcom/android/settings/z;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, -0x1

    iput v0, p0, Lcom/android/settings/A;->bvZ:I

    const/4 v0, 0x0

    new-array v0, v0, [Lcom/android/settings/p;

    iput-object v0, p0, Lcom/android/settings/A;->bvY:[Lcom/android/settings/p;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/settings/A;->bwa:Landroid/view/View;

    iput-object p1, p0, Lcom/android/settings/A;->bvX:Landroid/app/Activity;

    const-class v0, Landroid/app/admin/DevicePolicyManager;

    invoke-virtual {p1, v0}, Landroid/app/Activity;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/admin/DevicePolicyManager;

    iput-object v0, p0, Lcom/android/settings/A;->bwd:Landroid/app/admin/DevicePolicyManager;

    const-class v0, Landroid/os/UserManager;

    invoke-virtual {p1, v0}, Landroid/app/Activity;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/UserManager;

    iput-object v0, p0, Lcom/android/settings/A;->mUserManager:Landroid/os/UserManager;

    iput-object p2, p0, Lcom/android/settings/A;->bwb:Lcom/android/settings/z;

    new-instance v0, Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/android/settings/A;->bvX:Landroid/app/Activity;

    invoke-direct {v0, v1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/settings/A;->bwh:Landroid/widget/LinearLayout;

    iget-object v0, p0, Lcom/android/settings/A;->bwh:Landroid/widget/LinearLayout;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOrientation(I)V

    return-void
.end method

.method private bjA(Ljava/lang/Runnable;)V
    .locals 4

    iget-object v0, p0, Lcom/android/settings/A;->bwa:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const-wide/16 v2, 0x12c

    invoke-virtual {v0, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/A;->bvX:Landroid/app/Activity;

    const v2, 0x10c000f

    invoke-static {v1, v2}, Landroid/view/animation/AnimationUtils;->loadInterpolator(Landroid/content/Context;I)Landroid/view/animation/Interpolator;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/view/ViewPropertyAnimator;->withEndAction(Ljava/lang/Runnable;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->start()V

    return-void
.end method

.method private bjB(Landroid/view/View;)V
    .locals 1

    new-instance v0, Lcom/android/settings/dO;

    invoke-direct {v0, p0, p1}, Lcom/android/settings/dO;-><init>(Lcom/android/settings/A;Landroid/view/View;)V

    invoke-direct {p0, v0}, Lcom/android/settings/A;->bjA(Ljava/lang/Runnable;)V

    return-void
.end method

.method private static bjC(Lcom/android/settings/p;)I
    .locals 1

    invoke-virtual {p0}, Lcom/android/settings/p;->bic()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/android/settings/p;->bib()Z

    move-result v0

    if-eqz v0, :cond_0

    const v0, 0x7f121296

    :goto_0
    return v0

    :cond_0
    const v0, 0x7f121294

    goto :goto_0

    :cond_1
    const v0, 0x7f121298

    goto :goto_0
.end method

.method private bjD(Lcom/android/settings/p;)Landroid/widget/LinearLayout;
    .locals 6

    const/4 v2, 0x0

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iget-object v0, p0, Lcom/android/settings/A;->bwb:Lcom/android/settings/z;

    invoke-interface {v0, p1}, Lcom/android/settings/z;->bgP(Lcom/android/settings/p;)Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/security/cert/X509Certificate;

    new-instance v5, Landroid/net/http/SslCertificate;

    invoke-direct {v5, v0}, Landroid/net/http/SslCertificate;-><init>(Ljava/security/cert/X509Certificate;)V

    iget-object v0, p0, Lcom/android/settings/A;->bvX:Landroid/app/Activity;

    invoke-virtual {v5, v0}, Landroid/net/http/SslCertificate;->inflateCertificateView(Landroid/content/Context;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual {v5}, Landroid/net/http/SslCertificate;->getIssuedTo()Landroid/net/http/SslCertificate$DName;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/http/SslCertificate$DName;->getCName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    new-instance v0, Landroid/widget/ArrayAdapter;

    iget-object v3, p0, Lcom/android/settings/A;->bvX:Landroid/app/Activity;

    const v5, 0x1090008

    invoke-direct {v0, v3, v5, v1}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    const v1, 0x1090009

    invoke-virtual {v0, v1}, Landroid/widget/ArrayAdapter;->setDropDownViewResource(I)V

    new-instance v1, Landroid/widget/Spinner;

    iget-object v3, p0, Lcom/android/settings/A;->bvX:Landroid/app/Activity;

    invoke-direct {v1, v3}, Landroid/widget/Spinner;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, v0}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    new-instance v0, Lcom/android/settings/dN;

    invoke-direct {v0, p0, v4}, Lcom/android/settings/dN;-><init>(Lcom/android/settings/A;Ljava/util/ArrayList;)V

    invoke-virtual {v1, v0}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    new-instance v5, Landroid/widget/LinearLayout;

    iget-object v0, p0, Lcom/android/settings/A;->bvX:Landroid/app/Activity;

    invoke-direct {v5, v0}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    const/4 v0, 0x1

    invoke-virtual {v5, v0}, Landroid/widget/LinearLayout;->setOrientation(I)V

    invoke-virtual {v5, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    move v1, v2

    :goto_1
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    if-nez v1, :cond_1

    move v3, v2

    :goto_2
    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {v5, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    :cond_1
    const/16 v3, 0x8

    goto :goto_2

    :cond_2
    return-object v5
.end method

.method private bjE()Lcom/android/settings/p;
    .locals 2

    iget v0, p0, Lcom/android/settings/A;->bvZ:I

    iget-object v1, p0, Lcom/android/settings/A;->bvY:[Lcom/android/settings/p;

    array-length v1, v1

    if-ge v0, v1, :cond_0

    iget-object v0, p0, Lcom/android/settings/A;->bvY:[Lcom/android/settings/p;

    iget v1, p0, Lcom/android/settings/A;->bvZ:I

    aget-object v0, v0, v1

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private bjF(I)Z
    .locals 2

    new-instance v0, Lcom/android/internal/widget/LockPatternUtils;

    iget-object v1, p0, Lcom/android/settings/A;->bvX:Landroid/app/Activity;

    invoke-direct {v0, v1}, Lcom/android/internal/widget/LockPatternUtils;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, p1}, Lcom/android/internal/widget/LockPatternUtils;->isSecure(I)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    return v0

    :cond_0
    iget-object v1, p0, Lcom/android/settings/A;->mUserManager:Landroid/os/UserManager;

    invoke-virtual {v1, p1}, Landroid/os/UserManager;->getProfileParent(I)Landroid/content/pm/UserInfo;

    move-result-object v1

    if-nez v1, :cond_1

    const/4 v0, 0x0

    return v0

    :cond_1
    iget v1, v1, Landroid/content/pm/UserInfo;->id:I

    invoke-virtual {v0, v1}, Lcom/android/internal/widget/LockPatternUtils;->isSecure(I)Z

    move-result v0

    return v0
.end method

.method private bjG()V
    .locals 2

    iget v0, p0, Lcom/android/settings/A;->bvZ:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/android/settings/A;->bvZ:I

    :goto_0
    iget v0, p0, Lcom/android/settings/A;->bvZ:I

    iget-object v1, p0, Lcom/android/settings/A;->bvY:[Lcom/android/settings/p;

    array-length v1, v1

    if-ge v0, v1, :cond_0

    invoke-direct {p0}, Lcom/android/settings/A;->bjE()Lcom/android/settings/p;

    move-result-object v0

    if-nez v0, :cond_0

    iget v0, p0, Lcom/android/settings/A;->bvZ:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/android/settings/A;->bvZ:I

    goto :goto_0

    :cond_0
    iget v0, p0, Lcom/android/settings/A;->bvZ:I

    iget-object v1, p0, Lcom/android/settings/A;->bvY:[Lcom/android/settings/p;

    array-length v1, v1

    if-lt v0, v1, :cond_1

    iget-object v0, p0, Lcom/android/settings/A;->bwc:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    return-void

    :cond_1
    invoke-direct {p0}, Lcom/android/settings/A;->bjQ()V

    invoke-direct {p0}, Lcom/android/settings/A;->bjP()V

    invoke-direct {p0}, Lcom/android/settings/A;->bjO()V

    return-void
.end method

.method private bjH()V
    .locals 4

    const/4 v3, 0x0

    invoke-direct {p0}, Lcom/android/settings/A;->bjE()Lcom/android/settings/p;

    move-result-object v0

    new-instance v1, Lcom/android/settings/dM;

    invoke-direct {v1, p0, v0}, Lcom/android/settings/dM;-><init>(Lcom/android/settings/A;Lcom/android/settings/p;)V

    invoke-virtual {v0}, Lcom/android/settings/p;->bic()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, -0x1

    invoke-interface {v1, v3, v0}, Landroid/content/DialogInterface$OnClickListener;->onClick(Landroid/content/DialogInterface;I)V

    :goto_0
    return-void

    :cond_0
    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v2, p0, Lcom/android/settings/A;->bvX:Landroid/app/Activity;

    invoke-direct {v0, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v2, 0x7f121297

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v2, 0x1040013

    invoke-virtual {v0, v2, v1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x1040009

    invoke-virtual {v0, v1, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    goto :goto_0
.end method

.method private bjI()V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/A;->bjG()V

    return-void
.end method

.method private bjJ()V
    .locals 4

    invoke-direct {p0}, Lcom/android/settings/A;->bjE()Lcom/android/settings/p;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/A;->bwb:Lcom/android/settings/z;

    invoke-virtual {v0}, Lcom/android/settings/p;->bia()I

    move-result v2

    new-instance v3, Lcom/android/settings/-$Lambda$0SC140FQhubwQ8GlO-iyDSfYKFQ;

    invoke-direct {v3, p0}, Lcom/android/settings/-$Lambda$0SC140FQhubwQ8GlO-iyDSfYKFQ;-><init>(Ljava/lang/Object;)V

    invoke-interface {v1, v2, v3}, Lcom/android/settings/z;->bgV(ILjava/util/function/IntConsumer;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/android/settings/A;->bwd:Landroid/app/admin/DevicePolicyManager;

    invoke-virtual {v0}, Lcom/android/settings/p;->bhZ()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Lcom/android/settings/p;->bia()I

    move-result v0

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v0, v3}, Landroid/app/admin/DevicePolicyManager;->approveCaCert(Ljava/lang/String;IZ)Z

    invoke-direct {p0}, Lcom/android/settings/A;->bjG()V

    :cond_0
    return-void
.end method

.method private bjK(I)V
    .locals 1

    iget-object v0, p0, Lcom/android/settings/A;->bwc:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/android/settings/A;->bwe:Z

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/android/settings/A;->bjE()Lcom/android/settings/p;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/android/settings/A;->bjE()Lcom/android/settings/p;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/settings/p;->bia()I

    move-result v0

    if-ne v0, p1, :cond_0

    invoke-direct {p0}, Lcom/android/settings/A;->bjJ()V

    :cond_0
    return-void
.end method

.method private bjN(ILjava/lang/CharSequence;)Landroid/widget/Button;
    .locals 2

    iget-object v1, p0, Lcom/android/settings/A;->bwc:Landroid/app/AlertDialog;

    const/4 v0, 0x0

    check-cast v0, Landroid/content/DialogInterface$OnClickListener;

    invoke-virtual {v1, p1, p2, v0}, Landroid/app/AlertDialog;->setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    iget-object v0, p0, Lcom/android/settings/A;->bwc:Landroid/app/AlertDialog;

    invoke-virtual {v0, p1}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0, p2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-object v0
.end method

.method private bjO()V
    .locals 5

    invoke-direct {p0}, Lcom/android/settings/A;->bjE()Lcom/android/settings/p;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/A;->mUserManager:Landroid/os/UserManager;

    const-string/jumbo v2, "no_config_credentials"

    new-instance v3, Landroid/os/UserHandle;

    invoke-virtual {v0}, Lcom/android/settings/p;->bia()I

    move-result v4

    invoke-direct {v3, v4}, Landroid/os/UserHandle;-><init>(I)V

    invoke-virtual {v1, v2, v3}, Landroid/os/UserManager;->hasUserRestriction(Ljava/lang/String;Landroid/os/UserHandle;)Z

    move-result v1

    xor-int/lit8 v1, v1, 0x1

    iget-object v2, p0, Lcom/android/settings/A;->bvX:Landroid/app/Activity;

    invoke-static {v0}, Lcom/android/settings/A;->bjC(Lcom/android/settings/p;)I

    move-result v0

    invoke-virtual {v2, v0}, Landroid/app/Activity;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    const/4 v2, -0x2

    invoke-direct {p0, v2, v0}, Lcom/android/settings/A;->bjN(ILjava/lang/CharSequence;)Landroid/widget/Button;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/A;->bwf:Landroid/widget/Button;

    iget-object v2, p0, Lcom/android/settings/A;->bwf:Landroid/widget/Button;

    if-eqz v1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v2, v0}, Landroid/widget/Button;->setVisibility(I)V

    return-void

    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method private bjP()V
    .locals 5

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/android/settings/A;->bjE()Lcom/android/settings/p;

    move-result-object v2

    invoke-virtual {v2}, Lcom/android/settings/p;->bic()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {v2}, Lcom/android/settings/p;->bia()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/android/settings/A;->bjF(I)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/settings/A;->bwd:Landroid/app/admin/DevicePolicyManager;

    invoke-virtual {v2}, Lcom/android/settings/p;->bhZ()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2}, Lcom/android/settings/p;->bia()I

    move-result v4

    invoke-virtual {v0, v3, v4}, Landroid/app/admin/DevicePolicyManager;->isCaCertApproved(Ljava/lang/String;I)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/android/settings/A;->bwe:Z

    iget-object v0, p0, Lcom/android/settings/A;->bvX:Landroid/app/Activity;

    invoke-virtual {v2}, Lcom/android/settings/p;->bia()I

    move-result v2

    invoke-static {v0, v2}, Lcom/android/settingslib/w;->cqO(Landroid/content/Context;I)Lcom/android/settingslib/n;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v1, 0x1

    :cond_0
    iget-object v2, p0, Lcom/android/settings/A;->bvX:Landroid/app/Activity;

    if-nez v1, :cond_2

    iget-boolean v0, p0, Lcom/android/settings/A;->bwe:Z

    if-eqz v0, :cond_2

    const v0, 0x7f12129b

    :goto_1
    invoke-virtual {v2, v0}, Landroid/app/Activity;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    const/4 v1, -0x1

    invoke-direct {p0, v1, v0}, Lcom/android/settings/A;->bjN(ILjava/lang/CharSequence;)Landroid/widget/Button;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/A;->bwg:Landroid/widget/Button;

    return-void

    :cond_1
    move v0, v1

    goto :goto_0

    :cond_2
    const v0, 0x104000a

    goto :goto_1
.end method

.method private bjQ()V
    .locals 2

    invoke-direct {p0}, Lcom/android/settings/A;->bjE()Lcom/android/settings/p;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/android/settings/A;->bjD(Lcom/android/settings/p;)Landroid/widget/LinearLayout;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/A;->bwa:Landroid/view/View;

    if-nez v1, :cond_0

    iput-object v0, p0, Lcom/android/settings/A;->bwa:Landroid/view/View;

    iget-object v0, p0, Lcom/android/settings/A;->bwh:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/android/settings/A;->bwa:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0, v0}, Lcom/android/settings/A;->bjB(Landroid/view/View;)V

    goto :goto_0
.end method

.method static synthetic bjS(Lcom/android/settings/A;)Landroid/app/Activity;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/A;->bvX:Landroid/app/Activity;

    return-object v0
.end method

.method static synthetic bjT(Lcom/android/settings/A;)Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/A;->bwa:Landroid/view/View;

    return-object v0
.end method

.method static synthetic bjU(Lcom/android/settings/A;)Lcom/android/settings/z;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/A;->bwb:Lcom/android/settings/z;

    return-object v0
.end method

.method static synthetic bjV(Lcom/android/settings/A;)Landroid/widget/LinearLayout;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/A;->bwh:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic bjW(Lcom/android/settings/A;Landroid/view/View;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/settings/A;->bjz(Landroid/view/View;)V

    return-void
.end method

.method static synthetic bjX(Lcom/android/settings/A;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/A;->bjG()V

    return-void
.end method

.method private bjz(Landroid/view/View;)V
    .locals 2

    iput-object p1, p0, Lcom/android/settings/A;->bwa:Landroid/view/View;

    iget-object v0, p0, Lcom/android/settings/A;->bwh:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->removeAllViews()V

    iget-object v0, p0, Lcom/android/settings/A;->bwh:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    iget-object v0, p0, Lcom/android/settings/A;->bwh:Landroid/widget/LinearLayout;

    new-instance v1, Lcom/android/settings/dP;

    invoke-direct {v1, p0}, Lcom/android/settings/dP;-><init>(Lcom/android/settings/A;)V

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    return-void
.end method


# virtual methods
.method public bjL([Lcom/android/settings/p;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/A;->bvY:[Lcom/android/settings/p;

    return-void
.end method

.method public bjM(Landroid/app/AlertDialog;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/A;->bwc:Landroid/app/AlertDialog;

    return-void
.end method

.method synthetic bjR(I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/settings/A;->bjK(I)V

    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 1

    iget-object v0, p0, Lcom/android/settings/A;->bwg:Landroid/widget/Button;

    if-ne p1, v0, :cond_2

    iget-boolean v0, p0, Lcom/android/settings/A;->bwe:Z

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lcom/android/settings/A;->bjJ()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-direct {p0}, Lcom/android/settings/A;->bjI()V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/android/settings/A;->bwf:Landroid/widget/Button;

    if-ne p1, v0, :cond_0

    invoke-direct {p0}, Lcom/android/settings/A;->bjH()V

    goto :goto_0
.end method

.method public onShow(Landroid/content/DialogInterface;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/A;->bjG()V

    return-void
.end method
