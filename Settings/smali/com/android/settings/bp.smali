.class Lcom/android/settings/bp;
.super Ljava/lang/Object;
.source "PreviewSeekBarPreferenceFragment.java"

# interfaces
.implements Landroid/widget/SeekBar$OnSeekBarChangeListener;


# instance fields
.field private bQB:Z

.field final synthetic bQC:Lcom/android/settings/PreviewSeekBarPreferenceFragment;


# direct methods
.method private constructor <init>(Lcom/android/settings/PreviewSeekBarPreferenceFragment;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/bp;->bQC:Lcom/android/settings/PreviewSeekBarPreferenceFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/settings/PreviewSeekBarPreferenceFragment;Lcom/android/settings/bp;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/settings/bp;-><init>(Lcom/android/settings/PreviewSeekBarPreferenceFragment;)V

    return-void
.end method


# virtual methods
.method public onProgressChanged(Landroid/widget/SeekBar;IZ)V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/bp;->bQC:Lcom/android/settings/PreviewSeekBarPreferenceFragment;

    const/4 v1, 0x1

    invoke-static {v0, p2, v1}, Lcom/android/settings/PreviewSeekBarPreferenceFragment;->bJc(Lcom/android/settings/PreviewSeekBarPreferenceFragment;IZ)V

    iget-boolean v0, p0, Lcom/android/settings/bp;->bQB:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/bp;->bQC:Lcom/android/settings/PreviewSeekBarPreferenceFragment;

    invoke-virtual {v0}, Lcom/android/settings/PreviewSeekBarPreferenceFragment;->commit()V

    :cond_0
    return-void
.end method

.method public onStartTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/settings/bp;->bQB:Z

    return-void
.end method

.method public onStopTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/bp;->bQC:Lcom/android/settings/PreviewSeekBarPreferenceFragment;

    invoke-static {v0}, Lcom/android/settings/PreviewSeekBarPreferenceFragment;->bJa(Lcom/android/settings/PreviewSeekBarPreferenceFragment;)Lcom/android/settings/ci;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/settings/ci;->bQH()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/bp;->bQC:Lcom/android/settings/PreviewSeekBarPreferenceFragment;

    invoke-static {v0}, Lcom/android/settings/PreviewSeekBarPreferenceFragment;->bJa(Lcom/android/settings/PreviewSeekBarPreferenceFragment;)Lcom/android/settings/ci;

    move-result-object v0

    new-instance v1, Lcom/android/settings/iL;

    invoke-direct {v1, p0}, Lcom/android/settings/iL;-><init>(Lcom/android/settings/bp;)V

    invoke-virtual {v0, v1}, Lcom/android/settings/ci;->bQI(Ljava/lang/Runnable;)V

    :goto_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/settings/bp;->bQB:Z

    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/settings/bp;->bQC:Lcom/android/settings/PreviewSeekBarPreferenceFragment;

    invoke-virtual {v0}, Lcom/android/settings/PreviewSeekBarPreferenceFragment;->commit()V

    goto :goto_0
.end method
