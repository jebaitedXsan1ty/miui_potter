.class Lcom/android/settings/Y;
.super Ljava/lang/Object;
.source "DateTimeSettings.java"

# interfaces
.implements Lcom/android/settings/dashboard/D;


# instance fields
.field private final byY:Lcom/android/settings/dashboard/C;

.field private final mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/android/settings/dashboard/C;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/android/settings/Y;->mContext:Landroid/content/Context;

    iput-object p2, p0, Lcom/android/settings/Y;->byY:Lcom/android/settings/dashboard/C;

    return-void
.end method


# virtual methods
.method public jt(Z)V
    .locals 4

    if-eqz p1, :cond_0

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/Y;->byY:Lcom/android/settings/dashboard/C;

    iget-object v2, p0, Lcom/android/settings/Y;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeZone()Ljava/util/TimeZone;

    move-result-object v3

    invoke-virtual {v0}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v0

    invoke-static {v2, v3, v0}, Lcom/android/settingslib/j/a;->cpy(Landroid/content/Context;Ljava/util/TimeZone;Ljava/util/Date;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v1, p0, v0}, Lcom/android/settings/dashboard/C;->Fd(Lcom/android/settings/dashboard/D;Ljava/lang/CharSequence;)V

    :cond_0
    return-void
.end method
