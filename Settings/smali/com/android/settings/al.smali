.class public Lcom/android/settings/al;
.super Landroid/widget/BaseAdapter;
.source "NotificationAppListSettings.java"


# instance fields
.field bBJ:Landroid/view/View$OnClickListener;

.field private bBK:Ljava/util/List;

.field final synthetic bBL:Lcom/android/settings/NotificationAppListSettings;


# direct methods
.method public constructor <init>(Lcom/android/settings/NotificationAppListSettings;)V
    .locals 1

    iput-object p1, p0, Lcom/android/settings/al;->bBL:Lcom/android/settings/NotificationAppListSettings;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    new-instance v0, Lcom/android/settings/eV;

    invoke-direct {v0, p0}, Lcom/android/settings/eV;-><init>(Lcom/android/settings/al;)V

    iput-object v0, p0, Lcom/android/settings/al;->bBJ:Landroid/view/View$OnClickListener;

    return-void
.end method


# virtual methods
.method public bpC(Ljava/util/List;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/al;->bBK:Ljava/util/List;

    return-void
.end method

.method public getCount()I
    .locals 1

    iget-object v0, p0, Lcom/android/settings/al;->bBK:Ljava/util/List;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/android/settings/al;->bBK:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    goto :goto_0
.end method

.method public getItem(I)Lcom/android/settings/ak;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/al;->bBK:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settings/ak;

    return-object v0
.end method

.method public bridge synthetic getItem(I)Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0, p1}, Lcom/android/settings/al;->getItem(I)Lcom/android/settings/ak;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2

    int-to-long v0, p1

    return-wide v0
.end method

.method public getItemViewType(I)I
    .locals 1

    iget-object v0, p0, Lcom/android/settings/al;->bBK:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settings/ak;

    invoke-static {v0}, Lcom/android/settings/ak;->bpB(Lcom/android/settings/ak;)I

    move-result v0

    return v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 6

    const/4 v5, 0x0

    const/4 v4, 0x0

    iget-object v0, p0, Lcom/android/settings/al;->bBK:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settings/ak;

    invoke-static {v0}, Lcom/android/settings/ak;->bpB(Lcom/android/settings/ak;)I

    move-result v1

    if-nez v1, :cond_3

    if-nez p2, :cond_1

    new-instance v2, Lcom/android/settings/am;

    invoke-direct {v2, p0, v4}, Lcom/android/settings/am;-><init>(Lcom/android/settings/al;Lcom/android/settings/am;)V

    iget-object v1, p0, Lcom/android/settings/al;->bBL:Lcom/android/settings/NotificationAppListSettings;

    invoke-static {v1}, Lcom/android/settings/NotificationAppListSettings;->bpv(Lcom/android/settings/NotificationAppListSettings;)Landroid/view/LayoutInflater;

    move-result-object v1

    const v3, 0x7f0d01da

    invoke-virtual {v1, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    const v1, 0x1020006

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, v2, Lcom/android/settings/am;->bBN:Landroid/widget/ImageView;

    const v1, 0x1020016

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, v2, Lcom/android/settings/am;->bBP:Landroid/widget/TextView;

    const v1, 0x1020010

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, v2, Lcom/android/settings/am;->bBO:Landroid/widget/TextView;

    invoke-virtual {p2, v2}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    move-object v1, v2

    :goto_0
    invoke-static {v0}, Lcom/android/settings/ak;->bpA(Lcom/android/settings/ak;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, v1, Lcom/android/settings/am;->bBO:Landroid/widget/TextView;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setVisibility(I)V

    :goto_1
    iget-object v2, p0, Lcom/android/settings/al;->bBL:Lcom/android/settings/NotificationAppListSettings;

    invoke-static {v2}, Lcom/android/settings/NotificationAppListSettings;->bpu(Lcom/android/settings/NotificationAppListSettings;)Lcom/android/settings/cN;

    move-result-object v2

    iget-object v3, v1, Lcom/android/settings/am;->bBN:Landroid/widget/ImageView;

    invoke-virtual {v0}, Lcom/android/settings/ak;->bpz()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcom/android/settings/cN;->bWl(Landroid/widget/ImageView;Ljava/lang/String;)Z

    iget-object v1, v1, Lcom/android/settings/am;->bBP:Landroid/widget/TextView;

    iget-object v2, v0, Lcom/android/settings/ak;->bBD:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v1, 0x7f12112b

    invoke-virtual {p2, v1, v0}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    iget-object v0, p0, Lcom/android/settings/al;->bBJ:Landroid/view/View$OnClickListener;

    invoke-virtual {p2, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_0
    :goto_2
    return-object p2

    :cond_1
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/settings/am;

    goto :goto_0

    :cond_2
    iget-object v2, v1, Lcom/android/settings/am;->bBO:Landroid/widget/TextView;

    invoke-static {v0}, Lcom/android/settings/ak;->bpA(Lcom/android/settings/ak;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v2, v1, Lcom/android/settings/am;->bBO:Landroid/widget/TextView;

    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_1

    :cond_3
    invoke-static {v0}, Lcom/android/settings/ak;->bpB(Lcom/android/settings/ak;)I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    if-nez p2, :cond_4

    new-instance v2, Lcom/android/settings/am;

    invoke-direct {v2, p0, v4}, Lcom/android/settings/am;-><init>(Lcom/android/settings/al;Lcom/android/settings/am;)V

    iget-object v1, p0, Lcom/android/settings/al;->bBL:Lcom/android/settings/NotificationAppListSettings;

    invoke-static {v1}, Lcom/android/settings/NotificationAppListSettings;->bpv(Lcom/android/settings/NotificationAppListSettings;)Landroid/view/LayoutInflater;

    move-result-object v1

    const v3, 0x7f0d01d9

    invoke-virtual {v1, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    const v1, 0x7f0a01db

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, v2, Lcom/android/settings/am;->bBM:Landroid/widget/TextView;

    invoke-virtual {p2, v5}, Landroid/view/View;->setEnabled(Z)V

    invoke-virtual {p2, v2}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    move-object v1, v2

    :goto_3
    iget-object v1, v1, Lcom/android/settings/am;->bBM:Landroid/widget/TextView;

    iget-object v0, v0, Lcom/android/settings/ak;->bBD:Ljava/lang/String;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_2

    :cond_4
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/settings/am;

    goto :goto_3
.end method

.method public getViewTypeCount()I
    .locals 1

    const/4 v0, 0x2

    return v0
.end method
