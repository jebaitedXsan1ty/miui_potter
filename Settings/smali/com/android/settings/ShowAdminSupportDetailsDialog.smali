.class public Lcom/android/settings/ShowAdminSupportDetailsDialog;
.super Landroid/app/Activity;
.source "ShowAdminSupportDetailsDialog.java"

# interfaces
.implements Landroid/content/DialogInterface$OnDismissListener;


# instance fields
.field private cfC:Landroid/view/View;

.field private cfD:Lcom/android/settingslib/n;

.field private cfE:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/settings/ShowAdminSupportDetailsDialog;->cfE:Ljava/lang/String;

    return-void
.end method

.method public static bXZ(Landroid/app/Activity;Landroid/view/View;Lcom/android/settingslib/n;Z)V
    .locals 4

    const/4 v1, 0x0

    if-nez p2, :cond_0

    return-void

    :cond_0
    iget-object v0, p2, Lcom/android/settingslib/n;->cPP:Landroid/content/ComponentName;

    if-eqz v0, :cond_2

    const-string/jumbo v0, "device_policy"

    invoke-virtual {p0, v0}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/admin/DevicePolicyManager;

    iget-object v2, p2, Lcom/android/settingslib/n;->cPP:Landroid/content/ComponentName;

    invoke-static {p0, v2}, Lcom/android/settingslib/w;->crm(Landroid/content/Context;Landroid/content/ComponentName;)Z

    move-result v2

    if-eqz v2, :cond_1

    iget v2, p2, Lcom/android/settingslib/n;->userId:I

    invoke-static {p0, v2}, Lcom/android/settingslib/w;->cqP(Landroid/content/Context;I)Z

    move-result v2

    xor-int/lit8 v2, v2, 0x1

    if-eqz v2, :cond_3

    :cond_1
    iput-object v1, p2, Lcom/android/settingslib/n;->cPP:Landroid/content/ComponentName;

    :cond_2
    :goto_0
    const v0, 0x7f0a0049

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/android/settings/lg;

    invoke-direct {v1, p2, p0, p3}, Lcom/android/settings/lg;-><init>(Lcom/android/settingslib/n;Landroid/app/Activity;Z)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void

    :cond_3
    iget v2, p2, Lcom/android/settingslib/n;->userId:I

    const/16 v3, -0x2710

    if-ne v2, v3, :cond_4

    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v2

    iput v2, p2, Lcom/android/settingslib/n;->userId:I

    :cond_4
    invoke-static {}, Landroid/os/Process;->myUid()I

    move-result v2

    const/16 v3, 0x3e8

    invoke-static {v2, v3}, Landroid/os/UserHandle;->isSameApp(II)Z

    move-result v2

    if-eqz v2, :cond_5

    iget-object v1, p2, Lcom/android/settingslib/n;->cPP:Landroid/content/ComponentName;

    iget v2, p2, Lcom/android/settingslib/n;->userId:I

    invoke-virtual {v0, v1, v2}, Landroid/app/admin/DevicePolicyManager;->getShortSupportMessageForUser(Landroid/content/ComponentName;I)Ljava/lang/CharSequence;

    move-result-object v0

    move-object v1, v0

    :cond_5
    if-eqz v1, :cond_2

    const v0, 0x7f0a0047

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method private bYa(Landroid/content/Intent;)Lcom/android/settingslib/n;
    .locals 3

    const/4 v2, 0x0

    new-instance v1, Lcom/android/settingslib/n;

    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v0

    invoke-direct {v1, v2, v0}, Lcom/android/settingslib/n;-><init>(Landroid/content/ComponentName;I)V

    if-nez p1, :cond_0

    return-object v1

    :cond_0
    const-string/jumbo v0, "android.app.extra.DEVICE_ADMIN"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/content/ComponentName;

    iput-object v0, v1, Lcom/android/settingslib/n;->cPP:Landroid/content/ComponentName;

    const-string/jumbo v0, "android.intent.extra.USER_ID"

    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v2

    invoke-virtual {p1, v0, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    iput v0, v1, Lcom/android/settingslib/n;->userId:I

    return-object v1
.end method

.method private bYb(Landroid/content/Intent;)Ljava/lang/String;
    .locals 1

    const/4 v0, 0x0

    if-nez p1, :cond_0

    return-object v0

    :cond_0
    const-string/jumbo v0, "android.app.extra.RESTRICTION"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private bYc(Landroid/view/View;Landroid/content/ComponentName;ILjava/lang/String;)V
    .locals 4

    const/4 v1, 0x0

    if-eqz p2, :cond_1

    invoke-static {p0, p2}, Lcom/android/settingslib/w;->crm(Landroid/content/Context;Landroid/content/ComponentName;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p0, p3}, Lcom/android/settingslib/w;->cqP(Landroid/content/Context;I)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_2

    :cond_0
    move-object p2, v1

    :cond_1
    :goto_0
    invoke-direct {p0, p1, p4}, Lcom/android/settings/ShowAdminSupportDetailsDialog;->bYd(Landroid/view/View;Ljava/lang/String;)V

    new-instance v0, Lcom/android/settingslib/n;

    invoke-direct {v0, p2, p3}, Lcom/android/settingslib/n;-><init>(Landroid/content/ComponentName;I)V

    const/4 v1, 0x1

    invoke-static {p0, p1, v0, v1}, Lcom/android/settings/ShowAdminSupportDetailsDialog;->bXZ(Landroid/app/Activity;Landroid/view/View;Lcom/android/settingslib/n;Z)V

    return-void

    :cond_2
    :try_start_0
    invoke-static {}, Landroid/app/AppGlobals;->getPackageManager()Landroid/content/pm/IPackageManager;

    move-result-object v0

    const/4 v2, 0x0

    invoke-interface {v0, p2, v2, p3}, Landroid/content/pm/IPackageManager;->getReceiverInfo(Landroid/content/ComponentName;II)Landroid/content/pm/ActivityInfo;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    :goto_1
    if-eqz v1, :cond_1

    invoke-virtual {p0}, Lcom/android/settings/ShowAdminSupportDetailsDialog;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/content/pm/ActivityInfo;->loadIcon(Landroid/content/pm/PackageManager;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p0}, Lcom/android/settings/ShowAdminSupportDetailsDialog;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    new-instance v2, Landroid/os/UserHandle;

    invoke-direct {v2, p3}, Landroid/os/UserHandle;-><init>(I)V

    invoke-virtual {v1, v0, v2}, Landroid/content/pm/PackageManager;->getUserBadgedIcon(Landroid/graphics/drawable/Drawable;Landroid/os/UserHandle;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    const v0, 0x7f0a0045

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    :catch_0
    move-exception v0

    const-string/jumbo v2, "AdminSupportDialog"

    const-string/jumbo v3, "Missing reciever info"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1
.end method

.method private bYd(Landroid/view/View;Ljava/lang/String;)V
    .locals 3

    const v2, 0x7f1205d8

    const v0, 0x7f0a0044

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    if-nez v0, :cond_0

    return-void

    :cond_0
    if-nez p2, :cond_1

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(I)V

    return-void

    :cond_1
    const-string/jumbo v1, "no_adjust_volume"

    invoke-virtual {p2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    const v1, 0x7f1205d9

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    :goto_0
    return-void

    :cond_2
    const-string/jumbo v1, "no_outgoing_calls"

    invoke-virtual {p2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    const v1, 0x7f1205db

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0

    :cond_3
    const-string/jumbo v1, "no_sms"

    invoke-virtual {p2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    const v1, 0x7f1205dd

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0

    :cond_4
    const-string/jumbo v1, "policy_disable_camera"

    invoke-virtual {p2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    const v1, 0x7f1205da

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0

    :cond_5
    const-string/jumbo v1, "policy_disable_screen_capture"

    invoke-virtual {p2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    const v1, 0x7f1205dc

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0

    :cond_6
    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 6

    const/4 v5, 0x0

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/android/settings/ShowAdminSupportDetailsDialog;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/android/settings/ShowAdminSupportDetailsDialog;->bYa(Landroid/content/Intent;)Lcom/android/settingslib/n;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/ShowAdminSupportDetailsDialog;->cfD:Lcom/android/settingslib/n;

    invoke-virtual {p0}, Lcom/android/settings/ShowAdminSupportDetailsDialog;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/android/settings/ShowAdminSupportDetailsDialog;->bYb(Landroid/content/Intent;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/ShowAdminSupportDetailsDialog;->cfE:Ljava/lang/String;

    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f0d0028

    invoke-virtual {v1, v2, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/android/settings/ShowAdminSupportDetailsDialog;->cfC:Landroid/view/View;

    iget-object v1, p0, Lcom/android/settings/ShowAdminSupportDetailsDialog;->cfC:Landroid/view/View;

    iget-object v2, p0, Lcom/android/settings/ShowAdminSupportDetailsDialog;->cfD:Lcom/android/settingslib/n;

    iget-object v2, v2, Lcom/android/settingslib/n;->cPP:Landroid/content/ComponentName;

    iget-object v3, p0, Lcom/android/settings/ShowAdminSupportDetailsDialog;->cfD:Lcom/android/settingslib/n;

    iget v3, v3, Lcom/android/settingslib/n;->userId:I

    iget-object v4, p0, Lcom/android/settings/ShowAdminSupportDetailsDialog;->cfE:Ljava/lang/String;

    invoke-direct {p0, v1, v2, v3, v4}, Lcom/android/settings/ShowAdminSupportDetailsDialog;->bYc(Landroid/view/View;Landroid/content/ComponentName;ILjava/lang/String;)V

    invoke-virtual {v0, p0}, Landroid/app/AlertDialog$Builder;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f120c11

    invoke-virtual {v0, v1, v5}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/ShowAdminSupportDetailsDialog;->cfC:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    return-void
.end method

.method public onDismiss(Landroid/content/DialogInterface;)V
    .locals 0

    invoke-virtual {p0}, Lcom/android/settings/ShowAdminSupportDetailsDialog;->finish()V

    return-void
.end method

.method public onNewIntent(Landroid/content/Intent;)V
    .locals 4

    invoke-super {p0, p1}, Landroid/app/Activity;->onNewIntent(Landroid/content/Intent;)V

    invoke-direct {p0, p1}, Lcom/android/settings/ShowAdminSupportDetailsDialog;->bYa(Landroid/content/Intent;)Lcom/android/settingslib/n;

    move-result-object v0

    invoke-direct {p0, p1}, Lcom/android/settings/ShowAdminSupportDetailsDialog;->bYb(Landroid/content/Intent;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/android/settings/ShowAdminSupportDetailsDialog;->cfD:Lcom/android/settingslib/n;

    invoke-virtual {v2, v0}, Lcom/android/settingslib/n;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/android/settings/ShowAdminSupportDetailsDialog;->cfE:Ljava/lang/String;

    invoke-static {v2, v1}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    xor-int/lit8 v2, v2, 0x1

    if-eqz v2, :cond_1

    :cond_0
    iput-object v0, p0, Lcom/android/settings/ShowAdminSupportDetailsDialog;->cfD:Lcom/android/settingslib/n;

    iput-object v1, p0, Lcom/android/settings/ShowAdminSupportDetailsDialog;->cfE:Ljava/lang/String;

    iget-object v0, p0, Lcom/android/settings/ShowAdminSupportDetailsDialog;->cfC:Landroid/view/View;

    iget-object v1, p0, Lcom/android/settings/ShowAdminSupportDetailsDialog;->cfD:Lcom/android/settingslib/n;

    iget-object v1, v1, Lcom/android/settingslib/n;->cPP:Landroid/content/ComponentName;

    iget-object v2, p0, Lcom/android/settings/ShowAdminSupportDetailsDialog;->cfD:Lcom/android/settingslib/n;

    iget v2, v2, Lcom/android/settingslib/n;->userId:I

    iget-object v3, p0, Lcom/android/settings/ShowAdminSupportDetailsDialog;->cfE:Ljava/lang/String;

    invoke-direct {p0, v0, v1, v2, v3}, Lcom/android/settings/ShowAdminSupportDetailsDialog;->bYc(Landroid/view/View;Landroid/content/ComponentName;ILjava/lang/String;)V

    :cond_1
    return-void
.end method
