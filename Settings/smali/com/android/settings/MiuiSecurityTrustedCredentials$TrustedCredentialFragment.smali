.class public Lcom/android/settings/MiuiSecurityTrustedCredentials$TrustedCredentialFragment;
.super Lcom/android/settings/BaseListFragment;
.source "MiuiSecurityTrustedCredentials.java"


# instance fields
.field private bVK:Lcom/android/settings/MiuiSecurityTrustedCredentials$Tab;

.field private mUserManager:Landroid/os/UserManager;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/BaseListFragment;-><init>()V

    return-void
.end method

.method private bPL(Lcom/android/settings/bX;)V
    .locals 3

    invoke-static {p1}, Lcom/android/settings/bX;->bPy(Lcom/android/settings/bX;)Landroid/net/http/SslCertificate;

    move-result-object v0

    invoke-virtual {p0}, Lcom/android/settings/MiuiSecurityTrustedCredentials$TrustedCredentialFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/net/http/SslCertificate;->inflateCertificateView(Landroid/content/Context;)Landroid/view/View;

    move-result-object v0

    new-instance v1, Lmiui/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/android/settings/MiuiSecurityTrustedCredentials$TrustedCredentialFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-direct {v1, v2}, Lmiui/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v2, 0x11080048

    invoke-virtual {v1, v2}, Lmiui/app/AlertDialog$Builder;->setTitle(I)Lmiui/app/AlertDialog$Builder;

    invoke-virtual {v1, v0}, Lmiui/app/AlertDialog$Builder;->setView(Landroid/view/View;)Lmiui/app/AlertDialog$Builder;

    new-instance v0, Lcom/android/settings/jQ;

    invoke-direct {v0, p0}, Lcom/android/settings/jQ;-><init>(Lcom/android/settings/MiuiSecurityTrustedCredentials$TrustedCredentialFragment;)V

    const v2, 0x104000a

    invoke-virtual {v1, v2, v0}, Lmiui/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Lmiui/app/AlertDialog$Builder;

    invoke-static {p1}, Lcom/android/settings/bX;->bPB(Lcom/android/settings/bX;)Lcom/android/settings/MiuiSecurityTrustedCredentials$Tab;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/android/settings/MiuiSecurityTrustedCredentials$Tab;->bPj(Lcom/android/settings/MiuiSecurityTrustedCredentials$Tab;Lcom/android/settings/bX;)I

    move-result v0

    new-instance v2, Lcom/android/settings/jR;

    invoke-direct {v2, p0, p1}, Lcom/android/settings/jR;-><init>(Lcom/android/settings/MiuiSecurityTrustedCredentials$TrustedCredentialFragment;Lcom/android/settings/bX;)V

    invoke-virtual {v1, v0, v2}, Lmiui/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Lmiui/app/AlertDialog$Builder;

    invoke-virtual {v1}, Lmiui/app/AlertDialog$Builder;->create()Lmiui/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    return-void
.end method

.method static synthetic bPM(Lcom/android/settings/MiuiSecurityTrustedCredentials$TrustedCredentialFragment;Lcom/android/settings/bX;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/settings/MiuiSecurityTrustedCredentials$TrustedCredentialFragment;->bPL(Lcom/android/settings/bX;)V

    return-void
.end method


# virtual methods
.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 6

    invoke-super {p0, p1}, Lcom/android/settings/BaseListFragment;->onActivityCreated(Landroid/os/Bundle;)V

    new-instance v0, Lcom/android/settings/bV;

    invoke-virtual {p0}, Lcom/android/settings/MiuiSecurityTrustedCredentials$TrustedCredentialFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    iget-object v2, p0, Lcom/android/settings/MiuiSecurityTrustedCredentials$TrustedCredentialFragment;->bVK:Lcom/android/settings/MiuiSecurityTrustedCredentials$Tab;

    invoke-virtual {p0}, Lcom/android/settings/MiuiSecurityTrustedCredentials$TrustedCredentialFragment;->getListView()Landroid/widget/ListView;

    move-result-object v3

    new-instance v4, Lcom/android/org/conscrypt/TrustedCertificateStore;

    invoke-direct {v4}, Lcom/android/org/conscrypt/TrustedCertificateStore;-><init>()V

    const/4 v5, 0x0

    invoke-direct/range {v0 .. v5}, Lcom/android/settings/bV;-><init>(Landroid/app/Activity;Lcom/android/settings/MiuiSecurityTrustedCredentials$Tab;Landroid/view/View;Lcom/android/org/conscrypt/TrustedCertificateStore;Lcom/android/settings/bV;)V

    invoke-virtual {p0, v0}, Lcom/android/settings/MiuiSecurityTrustedCredentials$TrustedCredentialFragment;->setListAdapter(Landroid/widget/ListAdapter;)V

    invoke-virtual {p0}, Lcom/android/settings/MiuiSecurityTrustedCredentials$TrustedCredentialFragment;->getListView()Landroid/widget/ListView;

    move-result-object v1

    new-instance v2, Lcom/android/settings/jP;

    invoke-direct {v2, p0, v0}, Lcom/android/settings/jP;-><init>(Lcom/android/settings/MiuiSecurityTrustedCredentials$TrustedCredentialFragment;Lcom/android/settings/bV;)V

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Lcom/android/settings/BaseListFragment;->onCreate(Landroid/os/Bundle;)V

    sget v0, Lmiui/R$style;->Theme_Light_Settings_NoTitle:I

    invoke-virtual {p0, v0}, Lcom/android/settings/MiuiSecurityTrustedCredentials$TrustedCredentialFragment;->setThemeRes(I)V

    invoke-virtual {p0}, Lcom/android/settings/MiuiSecurityTrustedCredentials$TrustedCredentialFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string/jumbo v1, "tab_tag"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    const-string/jumbo v1, "user"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/android/settings/MiuiSecurityTrustedCredentials$Tab;->bVm:Lcom/android/settings/MiuiSecurityTrustedCredentials$Tab;

    iput-object v0, p0, Lcom/android/settings/MiuiSecurityTrustedCredentials$TrustedCredentialFragment;->bVK:Lcom/android/settings/MiuiSecurityTrustedCredentials$Tab;

    :goto_0
    invoke-virtual {p0}, Lcom/android/settings/MiuiSecurityTrustedCredentials$TrustedCredentialFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const-string/jumbo v1, "user"

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/UserManager;

    iput-object v0, p0, Lcom/android/settings/MiuiSecurityTrustedCredentials$TrustedCredentialFragment;->mUserManager:Landroid/os/UserManager;

    return-void

    :cond_0
    sget-object v0, Lcom/android/settings/MiuiSecurityTrustedCredentials$Tab;->bVl:Lcom/android/settings/MiuiSecurityTrustedCredentials$Tab;

    iput-object v0, p0, Lcom/android/settings/MiuiSecurityTrustedCredentials$TrustedCredentialFragment;->bVK:Lcom/android/settings/MiuiSecurityTrustedCredentials$Tab;

    goto :goto_0
.end method

.method public onInflateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 1

    invoke-super {p0, p1, p2, p3}, Lcom/android/settings/BaseListFragment;->onInflateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method
