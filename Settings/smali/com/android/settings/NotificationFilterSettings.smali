.class public Lcom/android/settings/NotificationFilterSettings;
.super Lcom/android/settings/StatusBarSettingsPreferenceFragment;
.source "NotificationFilterSettings.java"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceChangeListener;


# static fields
.field private static final cfK:Landroid/content/Intent;


# instance fields
.field private cfL:Lcom/android/settings/ImportanceListPreference;

.field private cfM:Landroid/preference/PreferenceCategory;

.field private cfN:Ljava/lang/String;

.field private cfO:Landroid/preference/CheckBoxPreference;

.field private cfP:Landroid/preference/CheckBoxPreference;

.field private cfQ:Landroid/preference/CheckBoxPreference;

.field private cfR:Landroid/preference/CheckBoxPreference;

.field private cfS:Landroid/preference/CheckBoxPreference;

.field private cfT:Landroid/preference/CheckBoxPreference;

.field private cfU:Landroid/preference/CheckBoxPreference;

.field private cfV:Landroid/preference/CheckBoxPreference;

.field private cfW:Z

.field private cfX:Z

.field private cfY:I

.field private mPackageName:Ljava/lang/String;

.field private mUserId:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v1, "android.intent.action.MAIN"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string/jumbo v1, "android.intent.category.NOTIFICATION_PREFERENCES"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    sput-object v0, Lcom/android/settings/NotificationFilterSettings;->cfK:Landroid/content/Intent;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/android/settings/StatusBarSettingsPreferenceFragment;-><init>()V

    const/4 v0, -0x1

    iput v0, p0, Lcom/android/settings/NotificationFilterSettings;->mUserId:I

    return-void
.end method

.method private bYS(I)V
    .locals 5

    const/4 v1, 0x1

    const/4 v0, 0x0

    if-eqz p1, :cond_3

    iget-object v2, p0, Lcom/android/settings/NotificationFilterSettings;->cfM:Landroid/preference/PreferenceCategory;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/preference/PreferenceCategory;->setTitle(Ljava/lang/CharSequence;)V

    if-ne p1, v1, :cond_2

    move v2, v1

    :goto_0
    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/android/settings/NotificationFilterSettings;->cfO:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v2, v0}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    iget-object v2, p0, Lcom/android/settings/NotificationFilterSettings;->cfP:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v2, v0}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    iget-object v2, p0, Lcom/android/settings/NotificationFilterSettings;->cfQ:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v2, v0}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    iget-object v2, p0, Lcom/android/settings/NotificationFilterSettings;->cfT:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v2, v0}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    iget-object v2, p0, Lcom/android/settings/NotificationFilterSettings;->cfU:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v2, v0}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    iget-object v2, p0, Lcom/android/settings/NotificationFilterSettings;->cfV:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v2, v0}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    iget-object v2, p0, Lcom/android/settings/NotificationFilterSettings;->cfS:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v2, v0}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    :cond_0
    :goto_1
    invoke-virtual {p0}, Lcom/android/settings/NotificationFilterSettings;->getActivity()Landroid/app/Activity;

    move-result-object v2

    iget-object v3, p0, Lcom/android/settings/NotificationFilterSettings;->mPackageName:Ljava/lang/String;

    iget v4, p0, Lcom/android/settings/NotificationFilterSettings;->cfY:I

    invoke-static {v2, v3, v4, v1}, Lmiui/util/NotificationFilterHelper;->getAppFlag(Landroid/content/Context;Ljava/lang/String;IZ)I

    move-result v2

    const/4 v3, 0x3

    if-eq v2, v3, :cond_1

    const/4 v2, -0x1

    if-ne p1, v2, :cond_4

    :goto_2
    iget-object v1, p0, Lcom/android/settings/NotificationFilterSettings;->cfO:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v1, v0}, Landroid/preference/CheckBoxPreference;->setEnabled(Z)V

    iget-object v1, p0, Lcom/android/settings/NotificationFilterSettings;->cfP:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v1, v0}, Landroid/preference/CheckBoxPreference;->setEnabled(Z)V

    iget-object v1, p0, Lcom/android/settings/NotificationFilterSettings;->cfQ:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v1, v0}, Landroid/preference/CheckBoxPreference;->setEnabled(Z)V

    iget-object v1, p0, Lcom/android/settings/NotificationFilterSettings;->cfT:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v1, v0}, Landroid/preference/CheckBoxPreference;->setEnabled(Z)V

    iget-object v1, p0, Lcom/android/settings/NotificationFilterSettings;->cfU:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v1, v0}, Landroid/preference/CheckBoxPreference;->setEnabled(Z)V

    iget-object v1, p0, Lcom/android/settings/NotificationFilterSettings;->cfV:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v1, v0}, Landroid/preference/CheckBoxPreference;->setEnabled(Z)V

    iget-object v1, p0, Lcom/android/settings/NotificationFilterSettings;->cfS:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v1, v0}, Landroid/preference/CheckBoxPreference;->setEnabled(Z)V

    :cond_1
    return-void

    :cond_2
    move v2, v0

    goto :goto_0

    :cond_3
    iget-object v2, p0, Lcom/android/settings/NotificationFilterSettings;->cfM:Landroid/preference/PreferenceCategory;

    invoke-virtual {p0}, Lcom/android/settings/NotificationFilterSettings;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f12116c

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/preference/PreferenceCategory;->setTitle(Ljava/lang/CharSequence;)V

    goto :goto_1

    :cond_4
    move v0, v1

    goto :goto_2
.end method

.method private bYU()V
    .locals 7

    const/4 v0, 0x0

    iget-boolean v1, p0, Lcom/android/settings/NotificationFilterSettings;->cfW:Z

    if-nez v1, :cond_0

    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/android/settings/NotificationFilterSettings;->getActivity()Landroid/app/Activity;

    move-result-object v1

    iget-object v2, p0, Lcom/android/settings/NotificationFilterSettings;->mPackageName:Ljava/lang/String;

    invoke-static {v1, v2}, Lmiui/util/NotificationFilterHelper;->getImportance(Landroid/content/Context;Ljava/lang/String;)I

    move-result v2

    iget-object v1, p0, Lcom/android/settings/NotificationFilterSettings;->cfL:Lcom/android/settings/ImportanceListPreference;

    invoke-virtual {v1}, Lcom/android/settings/ImportanceListPreference;->getEntryValues()[Ljava/lang/CharSequence;

    move-result-object v3

    move v1, v0

    :goto_0
    array-length v4, v3

    if-ge v0, v4, :cond_2

    aget-object v4, v3, v0

    invoke-interface {v4}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, ""

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    move v1, v0

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/android/settings/NotificationFilterSettings;->cfL:Lcom/android/settings/ImportanceListPreference;

    invoke-virtual {v0, v1}, Lcom/android/settings/ImportanceListPreference;->setValueIndex(I)V

    iget-object v0, p0, Lcom/android/settings/NotificationFilterSettings;->cfL:Lcom/android/settings/ImportanceListPreference;

    iget-object v1, p0, Lcom/android/settings/NotificationFilterSettings;->cfL:Lcom/android/settings/ImportanceListPreference;

    invoke-virtual {v1}, Lcom/android/settings/ImportanceListPreference;->getEntry()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/settings/ImportanceListPreference;->setSummary(Ljava/lang/CharSequence;)V

    invoke-direct {p0, v2}, Lcom/android/settings/NotificationFilterSettings;->bYS(I)V

    return-void
.end method

.method private bYV()V
    .locals 6

    const/4 v5, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/android/settings/NotificationFilterSettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    iget-object v2, p0, Lcom/android/settings/NotificationFilterSettings;->mPackageName:Ljava/lang/String;

    iget v3, p0, Lcom/android/settings/NotificationFilterSettings;->cfY:I

    invoke-static {v0, v2, v3, v5}, Lmiui/util/NotificationFilterHelper;->getAppFlag(Landroid/content/Context;Ljava/lang/String;IZ)I

    move-result v3

    packed-switch v3, :pswitch_data_0

    :goto_0
    invoke-virtual {p0}, Lcom/android/settings/NotificationFilterSettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    iget-object v2, p0, Lcom/android/settings/NotificationFilterSettings;->mPackageName:Ljava/lang/String;

    invoke-static {v0, v2}, Lmiui/util/NotificationFilterHelper;->isNotificationForcedFor(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/settings/NotificationFilterSettings;->cfR:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/android/settings/NotificationFilterSettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    iget-object v2, p0, Lcom/android/settings/NotificationFilterSettings;->mPackageName:Ljava/lang/String;

    iget v4, p0, Lcom/android/settings/NotificationFilterSettings;->cfY:I

    invoke-static {v0, v2, v4, v5}, Lmiui/util/NotificationFilterHelper;->enableNotifications(Landroid/content/Context;Ljava/lang/String;IZ)V

    iget-object v0, p0, Lcom/android/settings/NotificationFilterSettings;->cfR:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, v5}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    :cond_0
    iget-object v0, p0, Lcom/android/settings/NotificationFilterSettings;->cfR:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setEnabled(Z)V

    :cond_1
    iget-object v2, p0, Lcom/android/settings/NotificationFilterSettings;->cfQ:Landroid/preference/CheckBoxPreference;

    iget-object v0, p0, Lcom/android/settings/NotificationFilterSettings;->cfR:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p0}, Lcom/android/settings/NotificationFilterSettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    iget-object v4, p0, Lcom/android/settings/NotificationFilterSettings;->mPackageName:Ljava/lang/String;

    const-string/jumbo v5, "_keyguard"

    invoke-static {v0, v4, v5}, Lmiui/util/NotificationFilterHelper;->isAllowed(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    :goto_1
    invoke-virtual {v2, v0}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    iget-object v2, p0, Lcom/android/settings/NotificationFilterSettings;->cfT:Landroid/preference/CheckBoxPreference;

    iget-object v0, p0, Lcom/android/settings/NotificationFilterSettings;->cfR:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-virtual {p0}, Lcom/android/settings/NotificationFilterSettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    iget-object v4, p0, Lcom/android/settings/NotificationFilterSettings;->mPackageName:Ljava/lang/String;

    const-string/jumbo v5, "_message"

    invoke-static {v0, v4, v5}, Lmiui/util/NotificationFilterHelper;->isAllowed(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    :goto_2
    invoke-virtual {v2, v0}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    invoke-virtual {p0}, Lcom/android/settings/NotificationFilterSettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    iget-object v2, p0, Lcom/android/settings/NotificationFilterSettings;->mPackageName:Ljava/lang/String;

    const-string/jumbo v4, "_sound"

    invoke-static {v0, v2, v4}, Lmiui/util/NotificationFilterHelper;->isAllowed(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    invoke-virtual {p0}, Lcom/android/settings/NotificationFilterSettings;->getActivity()Landroid/app/Activity;

    move-result-object v2

    iget-object v4, p0, Lcom/android/settings/NotificationFilterSettings;->mPackageName:Ljava/lang/String;

    const-string/jumbo v5, "_vibrate"

    invoke-static {v2, v4, v5}, Lmiui/util/NotificationFilterHelper;->isAllowed(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    iget-object v4, p0, Lcom/android/settings/NotificationFilterSettings;->cfU:Landroid/preference/CheckBoxPreference;

    iget-object v5, p0, Lcom/android/settings/NotificationFilterSettings;->cfR:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v5}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v5

    if-eqz v5, :cond_5

    :goto_3
    invoke-virtual {v4, v0}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    iget-object v4, p0, Lcom/android/settings/NotificationFilterSettings;->cfV:Landroid/preference/CheckBoxPreference;

    iget-object v0, p0, Lcom/android/settings/NotificationFilterSettings;->cfR:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_6

    move v0, v2

    :goto_4
    invoke-virtual {v4, v0}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    iget-object v2, p0, Lcom/android/settings/NotificationFilterSettings;->cfS:Landroid/preference/CheckBoxPreference;

    iget-object v0, p0, Lcom/android/settings/NotificationFilterSettings;->cfR:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-virtual {p0}, Lcom/android/settings/NotificationFilterSettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    iget-object v4, p0, Lcom/android/settings/NotificationFilterSettings;->mPackageName:Ljava/lang/String;

    const-string/jumbo v5, "_led"

    invoke-static {v0, v4, v5}, Lmiui/util/NotificationFilterHelper;->isAllowed(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    :goto_5
    invoke-virtual {v2, v0}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    iget-boolean v0, p0, Lcom/android/settings/NotificationFilterSettings;->cfX:Z

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    if-ne v3, v0, :cond_8

    iget-object v0, p0, Lcom/android/settings/NotificationFilterSettings;->cfO:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    iget-object v0, p0, Lcom/android/settings/NotificationFilterSettings;->cfO:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setEnabled(Z)V

    :cond_2
    :goto_6
    iget-object v0, p0, Lcom/android/settings/NotificationFilterSettings;->cfQ:Landroid/preference/CheckBoxPreference;

    iget-object v1, p0, Lcom/android/settings/NotificationFilterSettings;->cfR:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v1}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setEnabled(Z)V

    iget-object v0, p0, Lcom/android/settings/NotificationFilterSettings;->cfT:Landroid/preference/CheckBoxPreference;

    iget-object v1, p0, Lcom/android/settings/NotificationFilterSettings;->cfR:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v1}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setEnabled(Z)V

    iget-object v0, p0, Lcom/android/settings/NotificationFilterSettings;->cfU:Landroid/preference/CheckBoxPreference;

    iget-object v1, p0, Lcom/android/settings/NotificationFilterSettings;->cfR:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v1}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setEnabled(Z)V

    iget-object v0, p0, Lcom/android/settings/NotificationFilterSettings;->cfV:Landroid/preference/CheckBoxPreference;

    iget-object v1, p0, Lcom/android/settings/NotificationFilterSettings;->cfR:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v1}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setEnabled(Z)V

    iget-object v0, p0, Lcom/android/settings/NotificationFilterSettings;->cfS:Landroid/preference/CheckBoxPreference;

    iget-object v1, p0, Lcom/android/settings/NotificationFilterSettings;->cfR:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v1}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setEnabled(Z)V

    invoke-virtual {p0}, Lcom/android/settings/NotificationFilterSettings;->bWA()Landroid/content/pm/PackageManager;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/settings/NotificationFilterSettings;->bYT(Landroid/content/pm/PackageManager;)Landroid/content/Intent;

    move-result-object v0

    if-eqz v0, :cond_9

    const-string/jumbo v1, "app_settings"

    invoke-virtual {p0, v1}, Lcom/android/settings/NotificationFilterSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    new-instance v2, Lcom/android/settings/lm;

    invoke-direct {v2, p0, v0}, Lcom/android/settings/lm;-><init>(Lcom/android/settings/NotificationFilterSettings;Landroid/content/Intent;)V

    invoke-virtual {v1, v2}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    :goto_7
    invoke-direct {p0}, Lcom/android/settings/NotificationFilterSettings;->bYU()V

    return-void

    :pswitch_0
    iget-object v0, p0, Lcom/android/settings/NotificationFilterSettings;->cfL:Lcom/android/settings/ImportanceListPreference;

    invoke-virtual {v0, v1}, Lcom/android/settings/ImportanceListPreference;->setEnabled(Z)V

    iget-object v0, p0, Lcom/android/settings/NotificationFilterSettings;->cfR:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    iget-object v0, p0, Lcom/android/settings/NotificationFilterSettings;->cfP:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    iget-object v0, p0, Lcom/android/settings/NotificationFilterSettings;->cfP:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setEnabled(Z)V

    goto/16 :goto_0

    :pswitch_1
    iget-object v0, p0, Lcom/android/settings/NotificationFilterSettings;->cfL:Lcom/android/settings/ImportanceListPreference;

    invoke-virtual {v0, v5}, Lcom/android/settings/ImportanceListPreference;->setEnabled(Z)V

    iget-object v0, p0, Lcom/android/settings/NotificationFilterSettings;->cfP:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, v5}, Landroid/preference/CheckBoxPreference;->setEnabled(Z)V

    iget-object v0, p0, Lcom/android/settings/NotificationFilterSettings;->cfP:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    iget-object v0, p0, Lcom/android/settings/NotificationFilterSettings;->cfR:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, v5}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    goto/16 :goto_0

    :pswitch_2
    iget-object v0, p0, Lcom/android/settings/NotificationFilterSettings;->cfL:Lcom/android/settings/ImportanceListPreference;

    invoke-virtual {v0, v5}, Lcom/android/settings/ImportanceListPreference;->setEnabled(Z)V

    iget-object v0, p0, Lcom/android/settings/NotificationFilterSettings;->cfP:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, v5}, Landroid/preference/CheckBoxPreference;->setEnabled(Z)V

    iget-object v0, p0, Lcom/android/settings/NotificationFilterSettings;->cfR:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, v5}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    iget-object v0, p0, Lcom/android/settings/NotificationFilterSettings;->cfP:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, v5}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    goto/16 :goto_0

    :cond_3
    move v0, v1

    goto/16 :goto_1

    :cond_4
    move v0, v1

    goto/16 :goto_2

    :cond_5
    move v0, v1

    goto/16 :goto_3

    :cond_6
    move v0, v1

    goto/16 :goto_4

    :cond_7
    move v0, v1

    goto/16 :goto_5

    :cond_8
    iget-object v0, p0, Lcom/android/settings/NotificationFilterSettings;->cfO:Landroid/preference/CheckBoxPreference;

    iget-object v1, p0, Lcom/android/settings/NotificationFilterSettings;->mPackageName:Ljava/lang/String;

    iget v2, p0, Lcom/android/settings/NotificationFilterSettings;->cfY:I

    invoke-static {v1, v2}, Lcom/android/settings/aU;->byI(Ljava/lang/String;I)Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    iget-object v0, p0, Lcom/android/settings/NotificationFilterSettings;->cfO:Landroid/preference/CheckBoxPreference;

    iget-object v1, p0, Lcom/android/settings/NotificationFilterSettings;->cfR:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v1}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setEnabled(Z)V

    goto/16 :goto_6

    :cond_9
    const-string/jumbo v0, "app_settings"

    invoke-virtual {p0, v0}, Lcom/android/settings/NotificationFilterSettings;->bWK(Ljava/lang/String;)V

    goto :goto_7

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method public bYT(Landroid/content/pm/PackageManager;)Landroid/content/Intent;
    .locals 4

    const/4 v3, 0x0

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/android/settings/NotificationFilterSettings;->mPackageName:Ljava/lang/String;

    if-nez v0, :cond_0

    return-object v3

    :cond_0
    new-instance v0, Landroid/content/Intent;

    sget-object v1, Lcom/android/settings/NotificationFilterSettings;->cfK:Landroid/content/Intent;

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Intent;)V

    iget-object v1, p0, Lcom/android/settings/NotificationFilterSettings;->mPackageName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p1, v0, v2}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_1

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/ResolveInfo;

    iget-object v0, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    new-instance v1, Landroid/content/Intent;

    sget-object v2, Lcom/android/settings/NotificationFilterSettings;->cfK:Landroid/content/Intent;

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Intent;)V

    iget-object v2, v0, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    iget-object v0, v0, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    return-object v0

    :cond_1
    return-object v3
.end method

.method public cC()Ljava/lang/String;
    .locals 1

    const-class v0, Lcom/android/settings/NotificationFilterSettings;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 6

    const/4 v2, 0x0

    invoke-super {p0, p1}, Lcom/android/settings/StatusBarSettingsPreferenceFragment;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/android/settings/NotificationFilterSettings;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/android/settings/NotificationFilterSettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    :cond_0
    const-string/jumbo v1, "appName"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v3, "packageName"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/android/settings/NotificationFilterSettings;->mPackageName:Ljava/lang/String;

    const-string/jumbo v3, "messageId"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/android/settings/NotificationFilterSettings;->cfN:Ljava/lang/String;

    const-string/jumbo v3, "userId"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    const-string/jumbo v3, "userId"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/android/settings/NotificationFilterSettings;->mUserId:I

    :cond_1
    iget-object v0, p0, Lcom/android/settings/NotificationFilterSettings;->mPackageName:Ljava/lang/String;

    if-nez v0, :cond_9

    invoke-virtual {p0}, Lcom/android/settings/NotificationFilterSettings;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string/jumbo v3, ":android:show_fragment_args"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v3

    if-eqz v3, :cond_9

    const-string/jumbo v0, "appName"

    invoke-virtual {v3, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "packageName"

    invoke-virtual {v3, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/android/settings/NotificationFilterSettings;->mPackageName:Ljava/lang/String;

    const-string/jumbo v1, "messageId"

    invoke-virtual {v3, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/android/settings/NotificationFilterSettings;->cfN:Ljava/lang/String;

    const-string/jumbo v1, "userId"

    invoke-virtual {v3, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    const-string/jumbo v1, "userId"

    invoke-virtual {v3, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Lcom/android/settings/NotificationFilterSettings;->mUserId:I

    :cond_2
    :goto_0
    invoke-static {}, Landroid/app/ActivityThread;->getPackageManager()Landroid/content/pm/IPackageManager;

    move-result-object v1

    iget v3, p0, Lcom/android/settings/NotificationFilterSettings;->mUserId:I

    const/4 v4, -0x1

    if-eq v4, v3, :cond_4

    :try_start_0
    iget-object v3, p0, Lcom/android/settings/NotificationFilterSettings;->mPackageName:Ljava/lang/String;

    iget v4, p0, Lcom/android/settings/NotificationFilterSettings;->mUserId:I

    const/4 v5, 0x0

    invoke-interface {v1, v3, v5, v4}, Landroid/content/pm/IPackageManager;->getApplicationInfo(Ljava/lang/String;II)Landroid/content/pm/ApplicationInfo;

    move-result-object v2

    iget v1, v2, Landroid/content/pm/ApplicationInfo;->uid:I

    iput v1, p0, Lcom/android/settings/NotificationFilterSettings;->cfY:I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_1
    iget-object v1, p0, Lcom/android/settings/NotificationFilterSettings;->mPackageName:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_3

    if-nez v2, :cond_5

    :cond_3
    const-string/jumbo v0, "NotificationFilterSettings"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "packagename ="

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v3, p0, Lcom/android/settings/NotificationFilterSettings;->mPackageName:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v3, "; info = "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lcom/android/settings/NotificationFilterSettings;->finish()V

    return-void

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1

    :cond_4
    :try_start_1
    invoke-virtual {p0}, Lcom/android/settings/NotificationFilterSettings;->bWA()Landroid/content/pm/PackageManager;

    move-result-object v1

    iget-object v3, p0, Lcom/android/settings/NotificationFilterSettings;->mPackageName:Ljava/lang/String;

    const/4 v4, 0x0

    invoke-virtual {v1, v3, v4}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v2

    iget v1, v2, Landroid/content/pm/ApplicationInfo;->uid:I

    iput v1, p0, Lcom/android/settings/NotificationFilterSettings;->cfY:I
    :try_end_1
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_1

    :catch_1
    move-exception v1

    const-string/jumbo v1, "NotificationFilterSettings"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/android/settings/NotificationFilterSettings;->mPackageName:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, " not found"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    :cond_5
    invoke-static {}, Lcom/android/settings/aU;->byH()Z

    move-result v1

    iput-boolean v1, p0, Lcom/android/settings/NotificationFilterSettings;->cfX:Z

    const v1, 0x7f1500e8

    invoke-virtual {p0, v1}, Lcom/android/settings/NotificationFilterSettings;->addPreferencesFromResource(I)V

    invoke-virtual {p0}, Lcom/android/settings/NotificationFilterSettings;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/app/Activity;->setTitle(Ljava/lang/CharSequence;)V

    const-string/jumbo v0, "enable_notification"

    invoke-virtual {p0, v0}, Lcom/android/settings/NotificationFilterSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    iput-object v0, p0, Lcom/android/settings/NotificationFilterSettings;->cfR:Landroid/preference/CheckBoxPreference;

    iget-object v0, p0, Lcom/android/settings/NotificationFilterSettings;->cfR:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, p0}, Landroid/preference/CheckBoxPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    const-string/jumbo v0, "priority"

    invoke-virtual {p0, v0}, Lcom/android/settings/NotificationFilterSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    iput-object v0, p0, Lcom/android/settings/NotificationFilterSettings;->cfO:Landroid/preference/CheckBoxPreference;

    iget-boolean v0, p0, Lcom/android/settings/NotificationFilterSettings;->cfX:Z

    if-nez v0, :cond_6

    const-string/jumbo v0, "main_container"

    invoke-virtual {p0, v0}, Lcom/android/settings/NotificationFilterSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceCategory;

    iget-object v1, p0, Lcom/android/settings/NotificationFilterSettings;->cfO:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceCategory;->removePreference(Landroid/preference/Preference;)Z

    :goto_2
    const-string/jumbo v0, "main_container"

    invoke-virtual {p0, v0}, Lcom/android/settings/NotificationFilterSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceCategory;

    iput-object v0, p0, Lcom/android/settings/NotificationFilterSettings;->cfM:Landroid/preference/PreferenceCategory;

    const-string/jumbo v0, "show_floating_notification"

    invoke-virtual {p0, v0}, Lcom/android/settings/NotificationFilterSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    iput-object v0, p0, Lcom/android/settings/NotificationFilterSettings;->cfP:Landroid/preference/CheckBoxPreference;

    iget-object v0, p0, Lcom/android/settings/NotificationFilterSettings;->cfP:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, p0}, Landroid/preference/CheckBoxPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    const-string/jumbo v0, "show_keyguard_notification"

    invoke-virtual {p0, v0}, Lcom/android/settings/NotificationFilterSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    iput-object v0, p0, Lcom/android/settings/NotificationFilterSettings;->cfQ:Landroid/preference/CheckBoxPreference;

    iget-object v0, p0, Lcom/android/settings/NotificationFilterSettings;->cfQ:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, p0}, Landroid/preference/CheckBoxPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    const-string/jumbo v0, "show_home_message"

    invoke-virtual {p0, v0}, Lcom/android/settings/NotificationFilterSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    iput-object v0, p0, Lcom/android/settings/NotificationFilterSettings;->cfT:Landroid/preference/CheckBoxPreference;

    iget-object v0, p0, Lcom/android/settings/NotificationFilterSettings;->cfT:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, p0}, Landroid/preference/CheckBoxPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    const-string/jumbo v0, "enable_notification_sound"

    invoke-virtual {p0, v0}, Lcom/android/settings/NotificationFilterSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    iput-object v0, p0, Lcom/android/settings/NotificationFilterSettings;->cfU:Landroid/preference/CheckBoxPreference;

    iget-object v0, p0, Lcom/android/settings/NotificationFilterSettings;->cfU:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, p0}, Landroid/preference/CheckBoxPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    const-string/jumbo v0, "enable_notification_vibrate"

    invoke-virtual {p0, v0}, Lcom/android/settings/NotificationFilterSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    iput-object v0, p0, Lcom/android/settings/NotificationFilterSettings;->cfV:Landroid/preference/CheckBoxPreference;

    const-string/jumbo v0, "vibrator"

    invoke-virtual {p0, v0}, Lcom/android/settings/NotificationFilterSettings;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Vibrator;

    invoke-virtual {v0}, Landroid/os/Vibrator;->hasVibrator()Z

    move-result v0

    if-nez v0, :cond_7

    const-string/jumbo v0, "main_container"

    invoke-virtual {p0, v0}, Lcom/android/settings/NotificationFilterSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceCategory;

    iget-object v1, p0, Lcom/android/settings/NotificationFilterSettings;->cfV:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceCategory;->removePreference(Landroid/preference/Preference;)Z

    :goto_3
    const-string/jumbo v0, "enable_notification_led"

    invoke-virtual {p0, v0}, Lcom/android/settings/NotificationFilterSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    iput-object v0, p0, Lcom/android/settings/NotificationFilterSettings;->cfS:Landroid/preference/CheckBoxPreference;

    iget-object v0, p0, Lcom/android/settings/NotificationFilterSettings;->cfS:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, p0}, Landroid/preference/CheckBoxPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    invoke-virtual {p0}, Lcom/android/settings/NotificationFilterSettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/android/settings/g;->bfH(Landroid/content/Context;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/settings/NotificationFilterSettings;->cfW:Z

    const-string/jumbo v0, "importance"

    invoke-virtual {p0, v0}, Lcom/android/settings/NotificationFilterSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Lcom/android/settings/ImportanceListPreference;

    iput-object v0, p0, Lcom/android/settings/NotificationFilterSettings;->cfL:Lcom/android/settings/ImportanceListPreference;

    iget-boolean v0, p0, Lcom/android/settings/NotificationFilterSettings;->cfW:Z

    if-nez v0, :cond_8

    const-string/jumbo v0, "importance_container"

    invoke-virtual {p0, v0}, Lcom/android/settings/NotificationFilterSettings;->bWK(Ljava/lang/String;)V

    :goto_4
    return-void

    :cond_6
    iget-object v0, p0, Lcom/android/settings/NotificationFilterSettings;->cfO:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, p0}, Landroid/preference/CheckBoxPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    goto/16 :goto_2

    :cond_7
    iget-object v0, p0, Lcom/android/settings/NotificationFilterSettings;->cfV:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, p0}, Landroid/preference/CheckBoxPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    goto :goto_3

    :cond_8
    iget-object v0, p0, Lcom/android/settings/NotificationFilterSettings;->cfL:Lcom/android/settings/ImportanceListPreference;

    invoke-virtual {v0, p0}, Lcom/android/settings/ImportanceListPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    goto :goto_4

    :cond_9
    move-object v0, v1

    goto/16 :goto_0
.end method

.method public onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 6

    const/4 v5, 0x1

    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "importance"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iget-object v1, p0, Lcom/android/settings/NotificationFilterSettings;->cfL:Lcom/android/settings/ImportanceListPreference;

    invoke-virtual {v1, v0}, Lcom/android/settings/ImportanceListPreference;->setValueIndex(I)V

    iget-object v0, p0, Lcom/android/settings/NotificationFilterSettings;->cfL:Lcom/android/settings/ImportanceListPreference;

    iget-object v1, p0, Lcom/android/settings/NotificationFilterSettings;->cfL:Lcom/android/settings/ImportanceListPreference;

    invoke-virtual {v1}, Lcom/android/settings/ImportanceListPreference;->getEntry()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/settings/ImportanceListPreference;->setSummary(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/android/settings/NotificationFilterSettings;->cfL:Lcom/android/settings/ImportanceListPreference;

    invoke-virtual {v0}, Lcom/android/settings/ImportanceListPreference;->getValue()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p0}, Lcom/android/settings/NotificationFilterSettings;->getActivity()Landroid/app/Activity;

    move-result-object v1

    iget-object v2, p0, Lcom/android/settings/NotificationFilterSettings;->mPackageName:Ljava/lang/String;

    invoke-static {v1, v2, v0}, Lmiui/util/NotificationFilterHelper;->setImportance(Landroid/content/Context;Ljava/lang/String;I)V

    invoke-direct {p0}, Lcom/android/settings/NotificationFilterSettings;->bYV()V

    new-instance v1, Landroid/content/Intent;

    const-string/jumbo v2, "com.miui.app.ExtraStatusBarManager.action_refresh_notification"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string/jumbo v2, "com.android.systemui"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    const-string/jumbo v2, "new_value"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string/jumbo v0, "change_importance"

    iget-object v2, p0, Lcom/android/settings/NotificationFilterSettings;->mPackageName:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/android/settings/NotificationFilterSettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/app/Activity;->sendBroadcast(Landroid/content/Intent;)V

    return v5

    :cond_0
    check-cast p2, Ljava/lang/Boolean;

    iget-object v1, p0, Lcom/android/settings/NotificationFilterSettings;->mPackageName:Ljava/lang/String;

    if-nez v1, :cond_1

    const-string/jumbo v1, "NotificationFilterSettings"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "onPreferenceChange() mPackageName == null; key="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v2, "; newValue="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    return v0

    :cond_1
    const-string/jumbo v1, "show_floating_notification"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    invoke-virtual {p0}, Lcom/android/settings/NotificationFilterSettings;->getActivity()Landroid/app/Activity;

    move-result-object v1

    iget-object v2, p0, Lcom/android/settings/NotificationFilterSettings;->mPackageName:Ljava/lang/String;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    invoke-static {v1, v2, v3}, Lmiui/util/NotificationFilterHelper;->enableStatusIcon(Landroid/content/Context;Ljava/lang/String;Z)V

    :cond_2
    :goto_0
    const-string/jumbo v1, "enable_notification"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    xor-int/lit8 v1, v1, 0x1

    if-nez v1, :cond_4

    :cond_3
    const-string/jumbo v1, "show_home_message"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    :cond_4
    new-instance v1, Landroid/content/Intent;

    const-string/jumbo v2, "com.miui.app.ExtraStatusBarManager.action_refresh_notification"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string/jumbo v2, "com.android.systemui"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    const-string/jumbo v2, "com.miui.app.ExtraStatusBarManager.extra_forbid_notification"

    const-string/jumbo v3, "enable_notification"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string/jumbo v0, "app_packageName"

    iget-object v2, p0, Lcom/android/settings/NotificationFilterSettings;->mPackageName:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    iget-object v0, p0, Lcom/android/settings/NotificationFilterSettings;->cfN:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_5

    const-string/jumbo v0, "messageId"

    iget-object v2, p0, Lcom/android/settings/NotificationFilterSettings;->cfN:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    :cond_5
    invoke-virtual {p0}, Lcom/android/settings/NotificationFilterSettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/app/Activity;->sendBroadcast(Landroid/content/Intent;)V

    :cond_6
    invoke-direct {p0}, Lcom/android/settings/NotificationFilterSettings;->bYV()V

    return v5

    :cond_7
    const-string/jumbo v1, "show_keyguard_notification"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_8

    invoke-virtual {p0}, Lcom/android/settings/NotificationFilterSettings;->getActivity()Landroid/app/Activity;

    move-result-object v1

    iget-object v2, p0, Lcom/android/settings/NotificationFilterSettings;->mPackageName:Ljava/lang/String;

    const-string/jumbo v3, "_keyguard"

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    invoke-static {v1, v2, v3, v4}, Lmiui/util/NotificationFilterHelper;->setAllow(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Z)V

    goto :goto_0

    :cond_8
    const-string/jumbo v1, "enable_notification"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_9

    invoke-virtual {p0}, Lcom/android/settings/NotificationFilterSettings;->getActivity()Landroid/app/Activity;

    move-result-object v1

    iget-object v2, p0, Lcom/android/settings/NotificationFilterSettings;->mPackageName:Ljava/lang/String;

    iget v3, p0, Lcom/android/settings/NotificationFilterSettings;->cfY:I

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    invoke-static {v1, v2, v3, v4}, Lmiui/util/NotificationFilterHelper;->enableNotifications(Landroid/content/Context;Ljava/lang/String;IZ)V

    goto/16 :goto_0

    :cond_9
    const-string/jumbo v1, "priority"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_a

    invoke-virtual {p0}, Lcom/android/settings/NotificationFilterSettings;->getActivity()Landroid/app/Activity;

    move-result-object v1

    iget-object v2, p0, Lcom/android/settings/NotificationFilterSettings;->mPackageName:Ljava/lang/String;

    iget v3, p0, Lcom/android/settings/NotificationFilterSettings;->cfY:I

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    invoke-static {v1, v2, v3, v4}, Lcom/android/settings/aU;->byJ(Landroid/content/Context;Ljava/lang/String;IZ)Z

    goto/16 :goto_0

    :cond_a
    const-string/jumbo v1, "show_home_message"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_b

    invoke-virtual {p0}, Lcom/android/settings/NotificationFilterSettings;->getActivity()Landroid/app/Activity;

    move-result-object v1

    iget-object v2, p0, Lcom/android/settings/NotificationFilterSettings;->mPackageName:Ljava/lang/String;

    const-string/jumbo v3, "_message"

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    invoke-static {v1, v2, v3, v4}, Lmiui/util/NotificationFilterHelper;->setAllow(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Z)V

    goto/16 :goto_0

    :cond_b
    const-string/jumbo v1, "enable_notification_sound"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_c

    invoke-virtual {p0}, Lcom/android/settings/NotificationFilterSettings;->getActivity()Landroid/app/Activity;

    move-result-object v1

    iget-object v2, p0, Lcom/android/settings/NotificationFilterSettings;->mPackageName:Ljava/lang/String;

    const-string/jumbo v3, "_sound"

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    invoke-static {v1, v2, v3, v4}, Lmiui/util/NotificationFilterHelper;->setAllow(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Z)V

    goto/16 :goto_0

    :cond_c
    const-string/jumbo v1, "enable_notification_vibrate"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_d

    invoke-virtual {p0}, Lcom/android/settings/NotificationFilterSettings;->getActivity()Landroid/app/Activity;

    move-result-object v1

    iget-object v2, p0, Lcom/android/settings/NotificationFilterSettings;->mPackageName:Ljava/lang/String;

    const-string/jumbo v3, "_vibrate"

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    invoke-static {v1, v2, v3, v4}, Lmiui/util/NotificationFilterHelper;->setAllow(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Z)V

    goto/16 :goto_0

    :cond_d
    const-string/jumbo v1, "enable_notification_led"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {p0}, Lcom/android/settings/NotificationFilterSettings;->getActivity()Landroid/app/Activity;

    move-result-object v1

    iget-object v2, p0, Lcom/android/settings/NotificationFilterSettings;->mPackageName:Ljava/lang/String;

    const-string/jumbo v3, "_led"

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    invoke-static {v1, v2, v3, v4}, Lmiui/util/NotificationFilterHelper;->setAllow(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Z)V

    goto/16 :goto_0
.end method

.method public onResume()V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/NotificationFilterSettings;->bYV()V

    invoke-super {p0}, Lcom/android/settings/StatusBarSettingsPreferenceFragment;->onResume()V

    return-void
.end method
