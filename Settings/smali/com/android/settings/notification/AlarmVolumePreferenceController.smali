.class public Lcom/android/settings/notification/AlarmVolumePreferenceController;
.super Lcom/android/settings/notification/VolumeSeekBarPreferenceController;
.source "AlarmVolumePreferenceController.java"


# instance fields
.field private cJ:Lcom/android/settings/notification/AudioHelper;


# direct methods
.method constructor <init>(Landroid/content/Context;Lcom/android/settings/notification/VolumeSeekBarPreference$Callback;Lcom/android/settings/core/lifecycle/a;Lcom/android/settings/notification/AudioHelper;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/android/settings/notification/VolumeSeekBarPreferenceController;-><init>(Landroid/content/Context;Lcom/android/settings/notification/VolumeSeekBarPreference$Callback;Lcom/android/settings/core/lifecycle/a;)V

    iput-object p4, p0, Lcom/android/settings/notification/AlarmVolumePreferenceController;->cJ:Lcom/android/settings/notification/AudioHelper;

    return-void
.end method


# virtual methods
.method public l()Ljava/lang/String;
    .locals 1

    const-string/jumbo v0, "alarm_volume"

    return-object v0
.end method

.method public p()Z
    .locals 1

    iget-object v0, p0, Lcom/android/settings/notification/AlarmVolumePreferenceController;->cJ:Lcom/android/settings/notification/AudioHelper;

    invoke-virtual {v0}, Lcom/android/settings/notification/AudioHelper;->hs()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    return v0
.end method
