.class public Lcom/android/settings/notification/MiuiRestrictedDropDownPreference$ReselectionSpinner;
.super Landroid/widget/Spinner;
.source "MiuiRestrictedDropDownPreference.java"


# instance fields
.field private ak:Lcom/android/settings/notification/MiuiRestrictedDropDownPreference;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Landroid/widget/Spinner;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method


# virtual methods
.method public setPreference(Lcom/android/settings/notification/MiuiRestrictedDropDownPreference;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/notification/MiuiRestrictedDropDownPreference$ReselectionSpinner;->ak:Lcom/android/settings/notification/MiuiRestrictedDropDownPreference;

    return-void
.end method

.method public setSelection(I)V
    .locals 2

    invoke-virtual {p0}, Lcom/android/settings/notification/MiuiRestrictedDropDownPreference$ReselectionSpinner;->getSelectedItemPosition()I

    move-result v0

    invoke-super {p0, p1}, Landroid/widget/Spinner;->setSelection(I)V

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/notification/MiuiRestrictedDropDownPreference$ReselectionSpinner;->ak:Lcom/android/settings/notification/MiuiRestrictedDropDownPreference;

    invoke-static {v0}, Lcom/android/settings/notification/MiuiRestrictedDropDownPreference;->aO(Lcom/android/settings/notification/MiuiRestrictedDropDownPreference;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/notification/MiuiRestrictedDropDownPreference$ReselectionSpinner;->ak:Lcom/android/settings/notification/MiuiRestrictedDropDownPreference;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/android/settings/notification/MiuiRestrictedDropDownPreference;->aR(Lcom/android/settings/notification/MiuiRestrictedDropDownPreference;Z)V

    iget-object v0, p0, Lcom/android/settings/notification/MiuiRestrictedDropDownPreference$ReselectionSpinner;->ak:Lcom/android/settings/notification/MiuiRestrictedDropDownPreference;

    invoke-static {v0, p1}, Lcom/android/settings/notification/MiuiRestrictedDropDownPreference;->aQ(Lcom/android/settings/notification/MiuiRestrictedDropDownPreference;I)Lcom/android/settings/notification/MiuiRestrictedDropDownPreference$RestrictedItem;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/android/settings/notification/MiuiRestrictedDropDownPreference$ReselectionSpinner;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v0, v0, Lcom/android/settings/notification/MiuiRestrictedDropDownPreference$RestrictedItem;->al:Lcom/android/settingslib/n;

    invoke-static {v1, v0}, Lcom/android/settingslib/w;->cqW(Landroid/content/Context;Lcom/android/settingslib/n;)V

    :cond_0
    return-void
.end method
