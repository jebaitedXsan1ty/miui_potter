.class final Lcom/android/settings/notification/ZenAccessSettings$1;
.super Ljava/lang/Object;
.source "ZenAccessSettings.java"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceChangeListener;


# instance fields
.field final synthetic hf:Lcom/android/settings/notification/ZenAccessSettings;

.field final synthetic hg:Ljava/lang/String;

.field final synthetic hh:Ljava/lang/CharSequence;


# direct methods
.method constructor <init>(Lcom/android/settings/notification/ZenAccessSettings;Ljava/lang/String;Ljava/lang/CharSequence;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/notification/ZenAccessSettings$1;->hf:Lcom/android/settings/notification/ZenAccessSettings;

    iput-object p2, p0, Lcom/android/settings/notification/ZenAccessSettings$1;->hg:Ljava/lang/String;

    iput-object p3, p0, Lcom/android/settings/notification/ZenAccessSettings$1;->hh:Ljava/lang/CharSequence;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 3

    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Lcom/android/settings/notification/ZenAccessSettings$ScaryWarningDialogFragment;

    invoke-direct {v0}, Lcom/android/settings/notification/ZenAccessSettings$ScaryWarningDialogFragment;-><init>()V

    iget-object v1, p0, Lcom/android/settings/notification/ZenAccessSettings$1;->hg:Ljava/lang/String;

    iget-object v2, p0, Lcom/android/settings/notification/ZenAccessSettings$1;->hh:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1, v2}, Lcom/android/settings/notification/ZenAccessSettings$ScaryWarningDialogFragment;->dz(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/android/settings/notification/ZenAccessSettings$ScaryWarningDialogFragment;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/notification/ZenAccessSettings$1;->hf:Lcom/android/settings/notification/ZenAccessSettings;

    invoke-virtual {v1}, Lcom/android/settings/notification/ZenAccessSettings;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const-string/jumbo v2, "dialog"

    invoke-virtual {v0, v1, v2}, Lcom/android/settings/notification/ZenAccessSettings$ScaryWarningDialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    :goto_0
    const/4 v0, 0x0

    return v0

    :cond_0
    new-instance v0, Lcom/android/settings/notification/ZenAccessSettings$FriendlyWarningDialogFragment;

    invoke-direct {v0}, Lcom/android/settings/notification/ZenAccessSettings$FriendlyWarningDialogFragment;-><init>()V

    iget-object v1, p0, Lcom/android/settings/notification/ZenAccessSettings$1;->hg:Ljava/lang/String;

    iget-object v2, p0, Lcom/android/settings/notification/ZenAccessSettings$1;->hh:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1, v2}, Lcom/android/settings/notification/ZenAccessSettings$FriendlyWarningDialogFragment;->dA(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/android/settings/notification/ZenAccessSettings$FriendlyWarningDialogFragment;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/notification/ZenAccessSettings$1;->hf:Lcom/android/settings/notification/ZenAccessSettings;

    invoke-virtual {v1}, Lcom/android/settings/notification/ZenAccessSettings;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const-string/jumbo v2, "dialog"

    invoke-virtual {v0, v1, v2}, Lcom/android/settings/notification/ZenAccessSettings$FriendlyWarningDialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_0
.end method
