.class public abstract Lcom/android/settings/notification/SilentModeRuleBaseSettings;
.super Lcom/android/settings/notification/SilentModeSettingsBase;
.source "SilentModeRuleBaseSettings.java"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceClickListener;
.implements Landroid/preference/Preference$OnPreferenceChangeListener;


# instance fields
.field protected bS:Landroid/app/Activity;

.field protected bT:Lcom/android/settings/dndmode/e;

.field protected bU:Ljava/lang/String;

.field protected bV:Landroid/preference/EditTextPreference;

.field protected bW:I

.field protected bX:Lcom/android/settings/dndmode/LabelPreference;

.field protected bY:Ljava/lang/String;

.field protected bZ:I

.field protected ca:I

.field private cb:Lmiui/app/TimePickerDialog$OnTimeSetListener;

.field protected cc:Landroid/preference/CheckBoxPreference;

.field protected cd:Landroid/preference/PreferenceCategory;

.field protected ce:Lcom/android/settings/dndmode/RepeatPreference;

.field protected cf:Landroid/preference/PreferenceScreen;

.field protected cg:Landroid/service/notification/ZenModeConfig$ZenRule;

.field protected ch:Ljava/lang/String;

.field protected ci:Landroid/service/notification/ZenModeConfig$ScheduleInfo;

.field protected cj:Landroid/preference/ListPreference;

.field protected ck:I

.field protected cl:Lcom/android/settings/dndmode/LabelPreference;

.field protected cm:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/android/settings/notification/SilentModeSettingsBase;-><init>()V

    new-instance v0, Lcom/android/settings/notification/SilentModeRuleBaseSettings$1;

    invoke-direct {v0, p0}, Lcom/android/settings/notification/SilentModeRuleBaseSettings$1;-><init>(Lcom/android/settings/notification/SilentModeRuleBaseSettings;)V

    iput-object v0, p0, Lcom/android/settings/notification/SilentModeRuleBaseSettings;->cb:Lmiui/app/TimePickerDialog$OnTimeSetListener;

    return-void
.end method

.method private cR()Ljava/util/Set;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/notification/SilentModeRuleBaseSettings;->mContext:Landroid/content/Context;

    invoke-static {v0}, Landroid/app/NotificationManager;->from(Landroid/content/Context;)Landroid/app/NotificationManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/NotificationManager;->getAutomaticZenRules()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method private cT(I)Ljava/lang/String;
    .locals 2

    const/16 v0, 0xa

    if-ge p1, v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "0"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private cY(I)Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    div-int/lit8 v1, p1, 0x3c

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    rem-int/lit8 v1, p1, 0x3c

    invoke-direct {p0, v1}, Lcom/android/settings/notification/SilentModeRuleBaseSettings;->cT(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic cZ(Lcom/android/settings/notification/SilentModeRuleBaseSettings;I)Ljava/lang/String;
    .locals 1

    invoke-direct {p0, p1}, Lcom/android/settings/notification/SilentModeRuleBaseSettings;->cY(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method protected cN(Landroid/app/AutomaticZenRule;)Ljava/lang/String;
    .locals 4

    const/4 v3, 0x0

    const/4 v0, 0x1

    :try_start_0
    iget-object v1, p0, Lcom/android/settings/notification/SilentModeRuleBaseSettings;->mContext:Landroid/content/Context;

    invoke-static {v1}, Landroid/app/NotificationManager;->from(Landroid/content/Context;)Landroid/app/NotificationManager;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/app/NotificationManager;->addAutomaticZenRule(Landroid/app/AutomaticZenRule;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/android/settings/notification/SilentModeRuleBaseSettings;->mContext:Landroid/content/Context;

    invoke-static {v2}, Landroid/app/NotificationManager;->from(Landroid/content/Context;)Landroid/app/NotificationManager;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/app/NotificationManager;->getAutomaticZenRule(Ljava/lang/String;)Landroid/app/AutomaticZenRule;

    move-result-object v2

    if-eqz v2, :cond_0

    :goto_0
    const/4 v2, 0x1

    invoke-virtual {p0, v0, v2}, Lcom/android/settings/notification/SilentModeRuleBaseSettings;->cS(ZZ)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object v1

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catch_0
    move-exception v0

    return-object v3
.end method

.method protected cO(Landroid/service/notification/ZenModeConfig$ZenRule;)Landroid/app/AutomaticZenRule;
    .locals 8

    new-instance v0, Landroid/app/AutomaticZenRule;

    iget-object v1, p1, Landroid/service/notification/ZenModeConfig$ZenRule;->name:Ljava/lang/String;

    iget-object v2, p1, Landroid/service/notification/ZenModeConfig$ZenRule;->component:Landroid/content/ComponentName;

    iget-object v3, p1, Landroid/service/notification/ZenModeConfig$ZenRule;->conditionId:Landroid/net/Uri;

    iget v4, p1, Landroid/service/notification/ZenModeConfig$ZenRule;->zenMode:I

    invoke-static {v4}, Landroid/app/NotificationManager;->zenModeToInterruptionFilter(I)I

    move-result v4

    iget-boolean v5, p1, Landroid/service/notification/ZenModeConfig$ZenRule;->enabled:Z

    iget-wide v6, p1, Landroid/service/notification/ZenModeConfig$ZenRule;->creationTime:J

    invoke-direct/range {v0 .. v7}, Landroid/app/AutomaticZenRule;-><init>(Ljava/lang/String;Landroid/content/ComponentName;Landroid/net/Uri;IZJ)V

    return-object v0
.end method

.method protected cP()Lcom/android/settings/notification/SilentModeRuleBaseSettings$RuleInfo;
    .locals 3

    new-instance v0, Landroid/service/notification/ZenModeConfig$ScheduleInfo;

    invoke-direct {v0}, Landroid/service/notification/ZenModeConfig$ScheduleInfo;-><init>()V

    iget-object v1, p0, Lcom/android/settings/notification/SilentModeRuleBaseSettings;->ce:Lcom/android/settings/dndmode/RepeatPreference;

    invoke-virtual {v1}, Lcom/android/settings/dndmode/RepeatPreference;->me()Lcom/android/settings/dndmode/e;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/settings/dndmode/e;->mi()[Z

    move-result-object v1

    invoke-static {v1}, Lcom/android/settings/notification/SilentModeUtils;->dK([Z)[I

    move-result-object v1

    iput-object v1, v0, Landroid/service/notification/ZenModeConfig$ScheduleInfo;->days:[I

    iget v1, p0, Lcom/android/settings/notification/SilentModeRuleBaseSettings;->ck:I

    div-int/lit8 v1, v1, 0x3c

    iput v1, v0, Landroid/service/notification/ZenModeConfig$ScheduleInfo;->startHour:I

    iget v1, p0, Lcom/android/settings/notification/SilentModeRuleBaseSettings;->ck:I

    rem-int/lit8 v1, v1, 0x3c

    iput v1, v0, Landroid/service/notification/ZenModeConfig$ScheduleInfo;->startMinute:I

    iget v1, p0, Lcom/android/settings/notification/SilentModeRuleBaseSettings;->bW:I

    div-int/lit8 v1, v1, 0x3c

    iput v1, v0, Landroid/service/notification/ZenModeConfig$ScheduleInfo;->endHour:I

    iget v1, p0, Lcom/android/settings/notification/SilentModeRuleBaseSettings;->bW:I

    rem-int/lit8 v1, v1, 0x3c

    iput v1, v0, Landroid/service/notification/ZenModeConfig$ScheduleInfo;->endMinute:I

    const/4 v1, 0x0

    iput-boolean v1, v0, Landroid/service/notification/ZenModeConfig$ScheduleInfo;->exitAtAlarm:Z

    new-instance v1, Lcom/android/settings/notification/SilentModeRuleBaseSettings$RuleInfo;

    invoke-direct {v1}, Lcom/android/settings/notification/SilentModeRuleBaseSettings$RuleInfo;-><init>()V

    const-string/jumbo v2, "SilentModeRuleSettings"

    iput-object v2, v1, Lcom/android/settings/notification/SilentModeRuleBaseSettings$RuleInfo;->cp:Ljava/lang/String;

    invoke-static {v0}, Landroid/service/notification/ZenModeConfig;->toScheduleConditionId(Landroid/service/notification/ZenModeConfig$ScheduleInfo;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, v1, Lcom/android/settings/notification/SilentModeRuleBaseSettings$RuleInfo;->cn:Landroid/net/Uri;

    invoke-static {}, Landroid/service/notification/ZenModeConfig;->getScheduleConditionProvider()Landroid/content/ComponentName;

    move-result-object v0

    iput-object v0, v1, Lcom/android/settings/notification/SilentModeRuleBaseSettings$RuleInfo;->co:Landroid/content/ComponentName;

    return-object v1
.end method

.method protected cQ()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/notification/SilentModeRuleBaseSettings;->bV:Landroid/preference/EditTextPreference;

    invoke-virtual {v0}, Landroid/preference/EditTextPreference;->getText()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/notification/SilentModeRuleBaseSettings;->bV:Landroid/preference/EditTextPreference;

    invoke-virtual {v0}, Landroid/preference/EditTextPreference;->getEditText()Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/EditText;->getHint()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/android/settings/notification/SilentModeRuleBaseSettings;->bV:Landroid/preference/EditTextPreference;

    invoke-virtual {v0}, Landroid/preference/EditTextPreference;->getText()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method protected cS(ZZ)V
    .locals 4

    if-eqz p1, :cond_0

    invoke-direct {p0}, Lcom/android/settings/notification/SilentModeRuleBaseSettings;->cR()Ljava/util/Set;

    move-result-object v0

    const-string/jumbo v1, "ZenModeSettings"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "Refreshed mRules="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    if-eqz p2, :cond_0

    invoke-virtual {p0}, Lcom/android/settings/notification/SilentModeRuleBaseSettings;->cW()V

    :cond_0
    return-void
.end method

.method protected abstract cU()V
.end method

.method protected cV()V
    .locals 0

    return-void
.end method

.method protected cW()V
    .locals 0

    return-void
.end method

.method protected cX(Ljava/lang/String;Landroid/app/AutomaticZenRule;)Z
    .locals 2

    iget-object v0, p0, Lcom/android/settings/notification/SilentModeRuleBaseSettings;->mContext:Landroid/content/Context;

    invoke-static {v0}, Landroid/app/NotificationManager;->from(Landroid/content/Context;)Landroid/app/NotificationManager;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Landroid/app/NotificationManager;->updateAutomaticZenRule(Ljava/lang/String;Landroid/app/AutomaticZenRule;)Z

    move-result v0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/android/settings/notification/SilentModeRuleBaseSettings;->cS(ZZ)V

    return v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3

    invoke-super {p0, p1}, Lcom/android/settings/notification/SilentModeSettingsBase;->onCreate(Landroid/os/Bundle;)V

    const v0, 0x7f150095

    invoke-virtual {p0, v0}, Lcom/android/settings/notification/SilentModeRuleBaseSettings;->addPreferencesFromResource(I)V

    invoke-virtual {p0}, Lcom/android/settings/notification/SilentModeRuleBaseSettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/notification/SilentModeRuleBaseSettings;->bS:Landroid/app/Activity;

    iget-object v0, p0, Lcom/android/settings/notification/SilentModeRuleBaseSettings;->bS:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "mode"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/android/settings/notification/SilentModeRuleBaseSettings;->bZ:I

    iget-object v0, p0, Lcom/android/settings/notification/SilentModeRuleBaseSettings;->bS:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "rule_id"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/notification/SilentModeRuleBaseSettings;->ch:Ljava/lang/String;

    iget-object v0, p0, Lcom/android/settings/notification/SilentModeRuleBaseSettings;->ch:Ljava/lang/String;

    if-nez v0, :cond_0

    const-string/jumbo v0, "ZenModeSettings"

    const-string/jumbo v1, "rule id is null"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lcom/android/settings/notification/SilentModeRuleBaseSettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    return-void

    :cond_0
    const-string/jumbo v0, "time_setting_root"

    invoke-virtual {p0, v0}, Lcom/android/settings/notification/SilentModeRuleBaseSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceScreen;

    iput-object v0, p0, Lcom/android/settings/notification/SilentModeRuleBaseSettings;->cf:Landroid/preference/PreferenceScreen;

    const-string/jumbo v0, "start_time"

    invoke-virtual {p0, v0}, Lcom/android/settings/notification/SilentModeRuleBaseSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Lcom/android/settings/dndmode/LabelPreference;

    iput-object v0, p0, Lcom/android/settings/notification/SilentModeRuleBaseSettings;->cl:Lcom/android/settings/dndmode/LabelPreference;

    const-string/jumbo v0, "end_time"

    invoke-virtual {p0, v0}, Lcom/android/settings/notification/SilentModeRuleBaseSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Lcom/android/settings/dndmode/LabelPreference;

    iput-object v0, p0, Lcom/android/settings/notification/SilentModeRuleBaseSettings;->bX:Lcom/android/settings/dndmode/LabelPreference;

    const-string/jumbo v0, "repeat"

    invoke-virtual {p0, v0}, Lcom/android/settings/notification/SilentModeRuleBaseSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Lcom/android/settings/dndmode/RepeatPreference;

    iput-object v0, p0, Lcom/android/settings/notification/SilentModeRuleBaseSettings;->ce:Lcom/android/settings/dndmode/RepeatPreference;

    const-string/jumbo v0, "silent_mode"

    invoke-virtual {p0, v0}, Lcom/android/settings/notification/SilentModeRuleBaseSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/ListPreference;

    iput-object v0, p0, Lcom/android/settings/notification/SilentModeRuleBaseSettings;->cj:Landroid/preference/ListPreference;

    const-string/jumbo v0, "key_edittitle"

    invoke-virtual {p0, v0}, Lcom/android/settings/notification/SilentModeRuleBaseSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/EditTextPreference;

    iput-object v0, p0, Lcom/android/settings/notification/SilentModeRuleBaseSettings;->bV:Landroid/preference/EditTextPreference;

    const-string/jumbo v0, "key_quiet_wristband_category"

    invoke-virtual {p0, v0}, Lcom/android/settings/notification/SilentModeRuleBaseSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceCategory;

    iput-object v0, p0, Lcom/android/settings/notification/SilentModeRuleBaseSettings;->cd:Landroid/preference/PreferenceCategory;

    const-string/jumbo v0, "key_quiet_wristband"

    invoke-virtual {p0, v0}, Lcom/android/settings/notification/SilentModeRuleBaseSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    iput-object v0, p0, Lcom/android/settings/notification/SilentModeRuleBaseSettings;->cc:Landroid/preference/CheckBoxPreference;

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/android/settings/notification/SilentModeRuleBaseSettings;->setHasOptionsMenu(Z)V

    invoke-virtual {p0}, Lcom/android/settings/notification/SilentModeRuleBaseSettings;->cU()V

    iget-object v0, p0, Lcom/android/settings/notification/SilentModeRuleBaseSettings;->bV:Landroid/preference/EditTextPreference;

    invoke-virtual {v0}, Landroid/preference/EditTextPreference;->getEditText()Landroid/widget/EditText;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/notification/SilentModeRuleBaseSettings;->bY:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setHint(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/android/settings/notification/SilentModeRuleBaseSettings;->bV:Landroid/preference/EditTextPreference;

    iget-object v1, p0, Lcom/android/settings/notification/SilentModeRuleBaseSettings;->bY:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/preference/EditTextPreference;->setSummary(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/android/settings/notification/SilentModeRuleBaseSettings;->bV:Landroid/preference/EditTextPreference;

    invoke-virtual {v0, p0}, Landroid/preference/EditTextPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    iget-object v0, p0, Lcom/android/settings/notification/SilentModeRuleBaseSettings;->cl:Lcom/android/settings/dndmode/LabelPreference;

    iget v1, p0, Lcom/android/settings/notification/SilentModeRuleBaseSettings;->ck:I

    invoke-direct {p0, v1}, Lcom/android/settings/notification/SilentModeRuleBaseSettings;->cY(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/settings/dndmode/LabelPreference;->mp(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/settings/notification/SilentModeRuleBaseSettings;->cl:Lcom/android/settings/dndmode/LabelPreference;

    invoke-virtual {v0, p0}, Lcom/android/settings/dndmode/LabelPreference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    iget-object v0, p0, Lcom/android/settings/notification/SilentModeRuleBaseSettings;->bX:Lcom/android/settings/dndmode/LabelPreference;

    iget v1, p0, Lcom/android/settings/notification/SilentModeRuleBaseSettings;->bW:I

    invoke-direct {p0, v1}, Lcom/android/settings/notification/SilentModeRuleBaseSettings;->cY(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/settings/dndmode/LabelPreference;->mp(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/settings/notification/SilentModeRuleBaseSettings;->bX:Lcom/android/settings/dndmode/LabelPreference;

    invoke-virtual {v0, p0}, Lcom/android/settings/dndmode/LabelPreference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    iget-object v0, p0, Lcom/android/settings/notification/SilentModeRuleBaseSettings;->ce:Lcom/android/settings/dndmode/RepeatPreference;

    iget-object v1, p0, Lcom/android/settings/notification/SilentModeRuleBaseSettings;->bU:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/android/settings/dndmode/RepeatPreference;->mc(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/settings/notification/SilentModeRuleBaseSettings;->ce:Lcom/android/settings/dndmode/RepeatPreference;

    iget-object v1, p0, Lcom/android/settings/notification/SilentModeRuleBaseSettings;->bT:Lcom/android/settings/dndmode/e;

    invoke-virtual {v0, v1}, Lcom/android/settings/dndmode/RepeatPreference;->md(Lcom/android/settings/dndmode/e;)V

    iget-object v0, p0, Lcom/android/settings/notification/SilentModeRuleBaseSettings;->cj:Landroid/preference/ListPreference;

    invoke-virtual {v0, p0}, Landroid/preference/ListPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    iget-object v0, p0, Lcom/android/settings/notification/SilentModeRuleBaseSettings;->cj:Landroid/preference/ListPreference;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget v2, p0, Lcom/android/settings/notification/SilentModeRuleBaseSettings;->ca:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/ListPreference;->setValue(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/settings/notification/SilentModeRuleBaseSettings;->cj:Landroid/preference/ListPreference;

    iget-object v1, p0, Lcom/android/settings/notification/SilentModeRuleBaseSettings;->cj:Landroid/preference/ListPreference;

    invoke-virtual {v1}, Landroid/preference/ListPreference;->getEntry()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/ListPreference;->setSummary(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/android/settings/notification/SilentModeRuleBaseSettings;->cf:Landroid/preference/PreferenceScreen;

    iget-object v1, p0, Lcom/android/settings/notification/SilentModeRuleBaseSettings;->cd:Landroid/preference/PreferenceCategory;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    return-void
.end method

.method public onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 3

    const/4 v2, 0x1

    iget-object v0, p0, Lcom/android/settings/notification/SilentModeRuleBaseSettings;->cj:Landroid/preference/ListPreference;

    if-ne p1, v0, :cond_0

    check-cast p2, Ljava/lang/String;

    iget-object v0, p0, Lcom/android/settings/notification/SilentModeRuleBaseSettings;->cj:Landroid/preference/ListPreference;

    invoke-virtual {v0, p2}, Landroid/preference/ListPreference;->setValue(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/settings/notification/SilentModeRuleBaseSettings;->cj:Landroid/preference/ListPreference;

    iget-object v1, p0, Lcom/android/settings/notification/SilentModeRuleBaseSettings;->cj:Landroid/preference/ListPreference;

    invoke-virtual {v1}, Landroid/preference/ListPreference;->getEntry()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/ListPreference;->setSummary(Ljava/lang/CharSequence;)V

    return v2

    :cond_0
    iget-object v0, p0, Lcom/android/settings/notification/SilentModeRuleBaseSettings;->bV:Landroid/preference/EditTextPreference;

    if-ne p1, v0, :cond_1

    iget-object v0, p0, Lcom/android/settings/notification/SilentModeRuleBaseSettings;->bV:Landroid/preference/EditTextPreference;

    iget-object v1, p0, Lcom/android/settings/notification/SilentModeRuleBaseSettings;->bV:Landroid/preference/EditTextPreference;

    invoke-virtual {v1}, Landroid/preference/EditTextPreference;->getEditText()Landroid/widget/EditText;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-interface {v1}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/EditTextPreference;->setSummary(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/android/settings/notification/SilentModeRuleBaseSettings;->bV:Landroid/preference/EditTextPreference;

    iget-object v1, p0, Lcom/android/settings/notification/SilentModeRuleBaseSettings;->bV:Landroid/preference/EditTextPreference;

    invoke-virtual {v1}, Landroid/preference/EditTextPreference;->getEditText()Landroid/widget/EditText;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-interface {v1}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/EditTextPreference;->setText(Ljava/lang/String;)V

    return v2

    :cond_1
    const/4 v0, 0x0

    return v0
.end method

.method public onPreferenceClick(Landroid/preference/Preference;)Z
    .locals 7

    const/4 v5, 0x1

    const/4 v6, 0x0

    new-instance v0, Lmiui/app/TimePickerDialog;

    iget-object v1, p0, Lcom/android/settings/notification/SilentModeRuleBaseSettings;->bS:Landroid/app/Activity;

    iget-object v2, p0, Lcom/android/settings/notification/SilentModeRuleBaseSettings;->cb:Lmiui/app/TimePickerDialog$OnTimeSetListener;

    iget v3, p0, Lcom/android/settings/notification/SilentModeRuleBaseSettings;->ck:I

    div-int/lit8 v3, v3, 0x3c

    iget v4, p0, Lcom/android/settings/notification/SilentModeRuleBaseSettings;->ck:I

    rem-int/lit8 v4, v4, 0x3c

    invoke-direct/range {v0 .. v5}, Lmiui/app/TimePickerDialog;-><init>(Landroid/content/Context;Lmiui/app/TimePickerDialog$OnTimeSetListener;IIZ)V

    iget-object v1, p0, Lcom/android/settings/notification/SilentModeRuleBaseSettings;->cl:Lcom/android/settings/dndmode/LabelPreference;

    if-ne p1, v1, :cond_0

    iput-boolean v6, p0, Lcom/android/settings/notification/SilentModeRuleBaseSettings;->cm:Z

    iget v1, p0, Lcom/android/settings/notification/SilentModeRuleBaseSettings;->ck:I

    :goto_0
    if-lez v1, :cond_1

    :goto_1
    div-int/lit8 v2, v1, 0x3c

    rem-int/lit8 v1, v1, 0x3c

    invoke-virtual {v0, v2, v1}, Lmiui/app/TimePickerDialog;->updateTime(II)V

    invoke-virtual {v0}, Lmiui/app/TimePickerDialog;->show()V

    return v6

    :cond_0
    iget-object v1, p0, Lcom/android/settings/notification/SilentModeRuleBaseSettings;->bX:Lcom/android/settings/dndmode/LabelPreference;

    if-ne p1, v1, :cond_2

    iput-boolean v5, p0, Lcom/android/settings/notification/SilentModeRuleBaseSettings;->cm:Z

    iget v1, p0, Lcom/android/settings/notification/SilentModeRuleBaseSettings;->bW:I

    goto :goto_0

    :cond_1
    move v1, v6

    goto :goto_1

    :cond_2
    move v1, v6

    goto :goto_0
.end method
