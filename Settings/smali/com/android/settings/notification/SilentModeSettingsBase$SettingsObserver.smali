.class final Lcom/android/settings/notification/SilentModeSettingsBase$SettingsObserver;
.super Landroid/database/ContentObserver;
.source "SilentModeSettingsBase.java"


# instance fields
.field private final cB:Landroid/net/Uri;

.field private final cC:Landroid/net/Uri;

.field final synthetic cD:Lcom/android/settings/notification/SilentModeSettingsBase;


# direct methods
.method private constructor <init>(Lcom/android/settings/notification/SilentModeSettingsBase;)V
    .locals 1

    iput-object p1, p0, Lcom/android/settings/notification/SilentModeSettingsBase$SettingsObserver;->cD:Lcom/android/settings/notification/SilentModeSettingsBase;

    invoke-static {p1}, Lcom/android/settings/notification/SilentModeSettingsBase;->dm(Lcom/android/settings/notification/SilentModeSettingsBase;)Landroid/os/Handler;

    move-result-object v0

    invoke-direct {p0, v0}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    const-string/jumbo v0, "zen_mode"

    invoke-static {v0}, Landroid/provider/Settings$Global;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/notification/SilentModeSettingsBase$SettingsObserver;->cC:Landroid/net/Uri;

    const-string/jumbo v0, "zen_mode_config_etag"

    invoke-static {v0}, Landroid/provider/Settings$Global;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/notification/SilentModeSettingsBase$SettingsObserver;->cB:Landroid/net/Uri;

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/settings/notification/SilentModeSettingsBase;Lcom/android/settings/notification/SilentModeSettingsBase$SettingsObserver;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/settings/notification/SilentModeSettingsBase$SettingsObserver;-><init>(Lcom/android/settings/notification/SilentModeSettingsBase;)V

    return-void
.end method


# virtual methods
.method public dp()V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/android/settings/notification/SilentModeSettingsBase$SettingsObserver;->cD:Lcom/android/settings/notification/SilentModeSettingsBase;

    invoke-virtual {v0}, Lcom/android/settings/notification/SilentModeSettingsBase;->dg()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/notification/SilentModeSettingsBase$SettingsObserver;->cC:Landroid/net/Uri;

    invoke-virtual {v0, v1, v2, p0}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    iget-object v0, p0, Lcom/android/settings/notification/SilentModeSettingsBase$SettingsObserver;->cD:Lcom/android/settings/notification/SilentModeSettingsBase;

    invoke-virtual {v0}, Lcom/android/settings/notification/SilentModeSettingsBase;->dg()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/notification/SilentModeSettingsBase$SettingsObserver;->cB:Landroid/net/Uri;

    invoke-virtual {v0, v1, v2, p0}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    return-void
.end method

.method public dq()V
    .locals 1

    iget-object v0, p0, Lcom/android/settings/notification/SilentModeSettingsBase$SettingsObserver;->cD:Lcom/android/settings/notification/SilentModeSettingsBase;

    invoke-virtual {v0}, Lcom/android/settings/notification/SilentModeSettingsBase;->dg()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    return-void
.end method

.method public onChange(ZLandroid/net/Uri;)V
    .locals 2

    const/4 v1, 0x1

    invoke-super {p0, p1, p2}, Landroid/database/ContentObserver;->onChange(ZLandroid/net/Uri;)V

    iget-object v0, p0, Lcom/android/settings/notification/SilentModeSettingsBase$SettingsObserver;->cC:Landroid/net/Uri;

    invoke-virtual {v0, p2}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/notification/SilentModeSettingsBase$SettingsObserver;->cD:Lcom/android/settings/notification/SilentModeSettingsBase;

    invoke-static {v0, v1}, Lcom/android/settings/notification/SilentModeSettingsBase;->do(Lcom/android/settings/notification/SilentModeSettingsBase;Z)V

    :cond_0
    iget-object v0, p0, Lcom/android/settings/notification/SilentModeSettingsBase$SettingsObserver;->cB:Landroid/net/Uri;

    invoke-virtual {v0, p2}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/settings/notification/SilentModeSettingsBase$SettingsObserver;->cD:Lcom/android/settings/notification/SilentModeSettingsBase;

    invoke-static {v0, v1}, Lcom/android/settings/notification/SilentModeSettingsBase;->dn(Lcom/android/settings/notification/SilentModeSettingsBase;Z)V

    :cond_1
    return-void
.end method
