.class final Lcom/android/settings/notification/NotificationAccessSettings$1;
.super Ljava/lang/Object;
.source "NotificationAccessSettings.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic gn:Lcom/android/settings/notification/NotificationAccessSettings;

.field final synthetic go:Landroid/content/ComponentName;


# direct methods
.method constructor <init>(Lcom/android/settings/notification/NotificationAccessSettings;Landroid/content/ComponentName;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/notification/NotificationAccessSettings$1;->gn:Lcom/android/settings/notification/NotificationAccessSettings;

    iput-object p2, p0, Lcom/android/settings/notification/NotificationAccessSettings$1;->go:Landroid/content/ComponentName;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/notification/NotificationAccessSettings$1;->gn:Lcom/android/settings/notification/NotificationAccessSettings;

    invoke-static {v0}, Lcom/android/settings/notification/NotificationAccessSettings;->H(Lcom/android/settings/notification/NotificationAccessSettings;)Landroid/app/NotificationManager;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/notification/NotificationAccessSettings$1;->go:Landroid/content/ComponentName;

    invoke-virtual {v1}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/NotificationManager;->isNotificationPolicyAccessGrantedForPackage(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/notification/NotificationAccessSettings$1;->gn:Lcom/android/settings/notification/NotificationAccessSettings;

    invoke-static {v0}, Lcom/android/settings/notification/NotificationAccessSettings;->H(Lcom/android/settings/notification/NotificationAccessSettings;)Landroid/app/NotificationManager;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/notification/NotificationAccessSettings$1;->go:Landroid/content/ComponentName;

    invoke-virtual {v1}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/NotificationManager;->removeAutomaticZenRules(Ljava/lang/String;)Z

    :cond_0
    return-void
.end method
