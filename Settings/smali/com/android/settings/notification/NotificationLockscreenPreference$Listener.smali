.class Lcom/android/settings/notification/NotificationLockscreenPreference$Listener;
.super Ljava/lang/Object;
.source "NotificationLockscreenPreference.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;
.implements Landroid/widget/CompoundButton$OnCheckedChangeListener;
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private final eE:Landroid/content/DialogInterface$OnClickListener;

.field private eF:Landroid/view/View;

.field final synthetic eG:Lcom/android/settings/notification/NotificationLockscreenPreference;


# direct methods
.method public constructor <init>(Lcom/android/settings/notification/NotificationLockscreenPreference;Landroid/content/DialogInterface$OnClickListener;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/notification/NotificationLockscreenPreference$Listener;->eG:Lcom/android/settings/notification/NotificationLockscreenPreference;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/android/settings/notification/NotificationLockscreenPreference$Listener;->eE:Landroid/content/DialogInterface$OnClickListener;

    return-void
.end method


# virtual methods
.method public fU(Landroid/view/View;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/notification/NotificationLockscreenPreference$Listener;->eF:Landroid/view/View;

    return-void
.end method

.method public onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/notification/NotificationLockscreenPreference$Listener;->eG:Lcom/android/settings/notification/NotificationLockscreenPreference;

    xor-int/lit8 v1, p2, 0x1

    invoke-static {v0, v1}, Lcom/android/settings/notification/NotificationLockscreenPreference;->fS(Lcom/android/settings/notification/NotificationLockscreenPreference;Z)Z

    return-void
.end method

.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 4

    iget-object v0, p0, Lcom/android/settings/notification/NotificationLockscreenPreference$Listener;->eE:Landroid/content/DialogInterface$OnClickListener;

    invoke-interface {v0, p1, p2}, Landroid/content/DialogInterface$OnClickListener;->onClick(Landroid/content/DialogInterface;I)V

    check-cast p1, Landroid/app/AlertDialog;

    invoke-virtual {p1}, Landroid/app/AlertDialog;->getListView()Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ListView;->getCheckedItemPosition()I

    move-result v0

    iget-object v1, p0, Lcom/android/settings/notification/NotificationLockscreenPreference$Listener;->eF:Landroid/view/View;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/settings/notification/NotificationLockscreenPreference$Listener;->eF:Landroid/view/View;

    iget-object v2, p0, Lcom/android/settings/notification/NotificationLockscreenPreference$Listener;->eG:Lcom/android/settings/notification/NotificationLockscreenPreference;

    iget-object v3, p0, Lcom/android/settings/notification/NotificationLockscreenPreference$Listener;->eG:Lcom/android/settings/notification/NotificationLockscreenPreference;

    invoke-static {v3}, Lcom/android/settings/notification/NotificationLockscreenPreference;->fR(Lcom/android/settings/notification/NotificationLockscreenPreference;)Z

    move-result v3

    invoke-static {v2, v0, v3}, Lcom/android/settings/notification/NotificationLockscreenPreference;->fT(Lcom/android/settings/notification/NotificationLockscreenPreference;IZ)I

    move-result v0

    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    :cond_0
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const v1, 0x1020212

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/android/settings/notification/NotificationLockscreenPreference$Listener;->eG:Lcom/android/settings/notification/NotificationLockscreenPreference;

    invoke-virtual {v0}, Lcom/android/settings/notification/NotificationLockscreenPreference;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/notification/NotificationLockscreenPreference$Listener;->eG:Lcom/android/settings/notification/NotificationLockscreenPreference;

    invoke-static {v1}, Lcom/android/settings/notification/NotificationLockscreenPreference;->fQ(Lcom/android/settings/notification/NotificationLockscreenPreference;)Lcom/android/settingslib/n;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/settingslib/w;->cqW(Landroid/content/Context;Lcom/android/settingslib/n;)V

    :cond_0
    return-void
.end method
