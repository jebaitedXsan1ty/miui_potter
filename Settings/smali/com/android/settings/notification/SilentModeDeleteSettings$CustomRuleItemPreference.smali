.class Lcom/android/settings/notification/SilentModeDeleteSettings$CustomRuleItemPreference;
.super Landroid/preference/Preference;
.source "SilentModeDeleteSettings.java"


# instance fields
.field private fZ:Landroid/widget/ImageView;

.field ga:Landroid/view/View$OnClickListener;

.field private gb:Ljava/lang/String;

.field private gc:Lcom/android/settings/notification/SilentModeDeleteSettings$OnClickDeleteBtnListener;

.field final synthetic gd:Lcom/android/settings/notification/SilentModeDeleteSettings;

.field private mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>(Lcom/android/settings/notification/SilentModeDeleteSettings;Landroid/content/Context;)V
    .locals 1

    iput-object p1, p0, Lcom/android/settings/notification/SilentModeDeleteSettings$CustomRuleItemPreference;->gd:Lcom/android/settings/notification/SilentModeDeleteSettings;

    invoke-direct {p0, p2}, Landroid/preference/Preference;-><init>(Landroid/content/Context;)V

    new-instance v0, Lcom/android/settings/notification/SilentModeDeleteSettings$CustomRuleItemPreference$1;

    invoke-direct {v0, p0}, Lcom/android/settings/notification/SilentModeDeleteSettings$CustomRuleItemPreference$1;-><init>(Lcom/android/settings/notification/SilentModeDeleteSettings$CustomRuleItemPreference;)V

    iput-object v0, p0, Lcom/android/settings/notification/SilentModeDeleteSettings$CustomRuleItemPreference;->ga:Landroid/view/View$OnClickListener;

    iput-object p2, p0, Lcom/android/settings/notification/SilentModeDeleteSettings$CustomRuleItemPreference;->mContext:Landroid/content/Context;

    const v0, 0x7f150049

    invoke-virtual {p0, v0}, Lcom/android/settings/notification/SilentModeDeleteSettings$CustomRuleItemPreference;->setLayoutResource(I)V

    return-void
.end method

.method static synthetic hC(Lcom/android/settings/notification/SilentModeDeleteSettings$CustomRuleItemPreference;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/notification/SilentModeDeleteSettings$CustomRuleItemPreference;->gb:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic hD(Lcom/android/settings/notification/SilentModeDeleteSettings$CustomRuleItemPreference;)Lcom/android/settings/notification/SilentModeDeleteSettings$OnClickDeleteBtnListener;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/notification/SilentModeDeleteSettings$CustomRuleItemPreference;->gc:Lcom/android/settings/notification/SilentModeDeleteSettings$OnClickDeleteBtnListener;

    return-object v0
.end method


# virtual methods
.method public hA(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/notification/SilentModeDeleteSettings$CustomRuleItemPreference;->gb:Ljava/lang/String;

    invoke-virtual {p0, p2}, Lcom/android/settings/notification/SilentModeDeleteSettings$CustomRuleItemPreference;->setTitle(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public hB(Lcom/android/settings/notification/SilentModeDeleteSettings$OnClickDeleteBtnListener;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/notification/SilentModeDeleteSettings$CustomRuleItemPreference;->gc:Lcom/android/settings/notification/SilentModeDeleteSettings$OnClickDeleteBtnListener;

    return-void
.end method

.method protected onCreateView(Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    const/4 v2, 0x1

    invoke-super {p0, p1}, Landroid/preference/Preference;->onCreateView(Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    const v0, 0x7f0a0127

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/android/settings/notification/SilentModeDeleteSettings$CustomRuleItemPreference;->fZ:Landroid/widget/ImageView;

    iget-object v0, p0, Lcom/android/settings/notification/SilentModeDeleteSettings$CustomRuleItemPreference;->fZ:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setClickable(Z)V

    iget-object v0, p0, Lcom/android/settings/notification/SilentModeDeleteSettings$CustomRuleItemPreference;->fZ:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setFocusable(Z)V

    iget-object v0, p0, Lcom/android/settings/notification/SilentModeDeleteSettings$CustomRuleItemPreference;->fZ:Landroid/widget/ImageView;

    iget-object v2, p0, Lcom/android/settings/notification/SilentModeDeleteSettings$CustomRuleItemPreference;->ga:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-object v1
.end method
