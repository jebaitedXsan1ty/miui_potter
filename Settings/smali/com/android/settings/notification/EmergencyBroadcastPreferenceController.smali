.class public Lcom/android/settings/notification/EmergencyBroadcastPreferenceController;
.super Lcom/android/settings/core/e;
.source "EmergencyBroadcastPreferenceController.java"


# instance fields
.field private dW:Z

.field private dX:Lcom/android/settings/accounts/AccountRestrictionHelper;

.field private dY:Landroid/content/pm/PackageManager;

.field private final dZ:Ljava/lang/String;

.field private mUserManager:Landroid/os/UserManager;


# direct methods
.method constructor <init>(Landroid/content/Context;Lcom/android/settings/accounts/AccountRestrictionHelper;Ljava/lang/String;)V
    .locals 1

    invoke-direct {p0, p1}, Lcom/android/settings/core/e;-><init>(Landroid/content/Context;)V

    iput-object p3, p0, Lcom/android/settings/notification/EmergencyBroadcastPreferenceController;->dZ:Ljava/lang/String;

    iput-object p2, p0, Lcom/android/settings/notification/EmergencyBroadcastPreferenceController;->dX:Lcom/android/settings/accounts/AccountRestrictionHelper;

    const-string/jumbo v0, "user"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/UserManager;

    iput-object v0, p0, Lcom/android/settings/notification/EmergencyBroadcastPreferenceController;->mUserManager:Landroid/os/UserManager;

    iget-object v0, p0, Lcom/android/settings/notification/EmergencyBroadcastPreferenceController;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/notification/EmergencyBroadcastPreferenceController;->dY:Landroid/content/pm/PackageManager;

    invoke-direct {p0}, Lcom/android/settings/notification/EmergencyBroadcastPreferenceController;->fn()Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/settings/notification/EmergencyBroadcastPreferenceController;->dW:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 1

    new-instance v0, Lcom/android/settings/accounts/AccountRestrictionHelper;

    invoke-direct {v0, p1}, Lcom/android/settings/accounts/AccountRestrictionHelper;-><init>(Landroid/content/Context;)V

    invoke-direct {p0, p1, v0, p2}, Lcom/android/settings/notification/EmergencyBroadcastPreferenceController;-><init>(Landroid/content/Context;Lcom/android/settings/accounts/AccountRestrictionHelper;Ljava/lang/String;)V

    return-void
.end method

.method private fn()Z
    .locals 4

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/android/settings/notification/EmergencyBroadcastPreferenceController;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x1120035

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v1

    if-eqz v1, :cond_0

    :try_start_0
    iget-object v2, p0, Lcom/android/settings/notification/EmergencyBroadcastPreferenceController;->dY:Landroid/content/pm/PackageManager;

    const-string/jumbo v3, "com.android.cellbroadcastreceiver"

    invoke-virtual {v2, v3}, Landroid/content/pm/PackageManager;->getApplicationEnabledSetting(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_0

    :goto_0
    return v0

    :catch_0
    move-exception v1

    goto :goto_0

    :cond_0
    move v0, v1

    goto :goto_0
.end method


# virtual methods
.method public cz(Landroid/preference/Preference;)V
    .locals 1

    instance-of v0, p1, Lcom/android/settingslib/MiuiRestrictedPreference;

    if-nez v0, :cond_0

    return-void

    :cond_0
    check-cast p1, Lcom/android/settingslib/MiuiRestrictedPreference;

    const-string/jumbo v0, "no_config_cell_broadcasts"

    invoke-virtual {p1, v0}, Lcom/android/settingslib/MiuiRestrictedPreference;->cqc(Ljava/lang/String;)V

    return-void
.end method

.method public fm(Landroid/preference/Preference;)Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public l()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/notification/EmergencyBroadcastPreferenceController;->dZ:Ljava/lang/String;

    return-object v0
.end method

.method public p()Z
    .locals 3

    iget-object v0, p0, Lcom/android/settings/notification/EmergencyBroadcastPreferenceController;->mUserManager:Landroid/os/UserManager;

    invoke-virtual {v0}, Landroid/os/UserManager;->isAdminUser()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/android/settings/notification/EmergencyBroadcastPreferenceController;->dW:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/notification/EmergencyBroadcastPreferenceController;->dX:Lcom/android/settings/accounts/AccountRestrictionHelper;

    const-string/jumbo v1, "no_config_cell_broadcasts"

    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/android/settings/accounts/AccountRestrictionHelper;->alL(Ljava/lang/String;I)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
