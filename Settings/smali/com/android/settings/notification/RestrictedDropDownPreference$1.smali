.class final Lcom/android/settings/notification/RestrictedDropDownPreference$1;
.super Ljava/lang/Object;
.source "RestrictedDropDownPreference.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemSelectedListener;


# instance fields
.field final synthetic id:Lcom/android/settings/notification/RestrictedDropDownPreference;


# direct methods
.method constructor <init>(Lcom/android/settings/notification/RestrictedDropDownPreference;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/notification/RestrictedDropDownPreference$1;->id:Lcom/android/settings/notification/RestrictedDropDownPreference;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemSelected(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 3

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/android/settings/notification/RestrictedDropDownPreference$1;->id:Lcom/android/settings/notification/RestrictedDropDownPreference;

    invoke-static {v0}, Lcom/android/settings/notification/RestrictedDropDownPreference;->hi(Lcom/android/settings/notification/RestrictedDropDownPreference;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/settings/notification/RestrictedDropDownPreference$1;->id:Lcom/android/settings/notification/RestrictedDropDownPreference;

    invoke-static {v0, v1}, Lcom/android/settings/notification/RestrictedDropDownPreference;->hj(Lcom/android/settings/notification/RestrictedDropDownPreference;Z)Z

    if-ltz p3, :cond_0

    iget-object v0, p0, Lcom/android/settings/notification/RestrictedDropDownPreference$1;->id:Lcom/android/settings/notification/RestrictedDropDownPreference;

    invoke-virtual {v0}, Lcom/android/settings/notification/RestrictedDropDownPreference;->dmv()[Ljava/lang/CharSequence;

    move-result-object v0

    array-length v0, v0

    if-ge p3, v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/notification/RestrictedDropDownPreference$1;->id:Lcom/android/settings/notification/RestrictedDropDownPreference;

    invoke-virtual {v0}, Lcom/android/settings/notification/RestrictedDropDownPreference;->dmv()[Ljava/lang/CharSequence;

    move-result-object v0

    aget-object v0, v0, p3

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/notification/RestrictedDropDownPreference$1;->id:Lcom/android/settings/notification/RestrictedDropDownPreference;

    invoke-static {v1, v0}, Lcom/android/settings/notification/RestrictedDropDownPreference;->hm(Lcom/android/settings/notification/RestrictedDropDownPreference;Ljava/lang/CharSequence;)Lcom/android/settings/notification/RestrictedDropDownPreference$RestrictedItem;

    move-result-object v1

    if-eqz v1, :cond_2

    iget-object v0, p0, Lcom/android/settings/notification/RestrictedDropDownPreference$1;->id:Lcom/android/settings/notification/RestrictedDropDownPreference;

    invoke-virtual {v0}, Lcom/android/settings/notification/RestrictedDropDownPreference;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, v1, Lcom/android/settings/notification/RestrictedDropDownPreference$RestrictedItem;->fP:Lcom/android/settingslib/n;

    invoke-static {v0, v1}, Lcom/android/settingslib/w;->cqW(Landroid/content/Context;Lcom/android/settingslib/n;)V

    iget-object v0, p0, Lcom/android/settings/notification/RestrictedDropDownPreference$1;->id:Lcom/android/settings/notification/RestrictedDropDownPreference;

    invoke-static {v0}, Lcom/android/settings/notification/RestrictedDropDownPreference;->hh(Lcom/android/settings/notification/RestrictedDropDownPreference;)Lcom/android/settings/notification/RestrictedDropDownPreference$ReselectionSpinner;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/notification/RestrictedDropDownPreference$1;->id:Lcom/android/settings/notification/RestrictedDropDownPreference;

    iget-object v2, p0, Lcom/android/settings/notification/RestrictedDropDownPreference$1;->id:Lcom/android/settings/notification/RestrictedDropDownPreference;

    invoke-virtual {v2}, Lcom/android/settings/notification/RestrictedDropDownPreference;->getValue()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/android/settings/notification/RestrictedDropDownPreference;->dmu(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/android/settings/notification/RestrictedDropDownPreference$ReselectionSpinner;->setSelection(I)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    return-void

    :cond_2
    iget-object v1, p0, Lcom/android/settings/notification/RestrictedDropDownPreference$1;->id:Lcom/android/settings/notification/RestrictedDropDownPreference;

    invoke-virtual {v1}, Lcom/android/settings/notification/RestrictedDropDownPreference;->getValue()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/android/settings/notification/RestrictedDropDownPreference$1;->id:Lcom/android/settings/notification/RestrictedDropDownPreference;

    invoke-virtual {v1, v0}, Lcom/android/settings/notification/RestrictedDropDownPreference;->callChangeListener(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/settings/notification/RestrictedDropDownPreference$1;->id:Lcom/android/settings/notification/RestrictedDropDownPreference;

    invoke-virtual {v1, v0}, Lcom/android/settings/notification/RestrictedDropDownPreference;->setValue(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onNothingSelected(Landroid/widget/AdapterView;)V
    .locals 0

    return-void
.end method
