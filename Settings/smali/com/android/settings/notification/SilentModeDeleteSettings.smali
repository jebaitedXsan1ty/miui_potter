.class public Lcom/android/settings/notification/SilentModeDeleteSettings;
.super Lcom/android/settings/notification/SilentModeSettingsBase;
.source "SilentModeDeleteSettings.java"


# instance fields
.field private fW:Landroid/preference/PreferenceCategory;

.field private fX:Ljava/util/List;

.field fY:Lcom/android/settings/notification/SilentModeDeleteSettings$OnClickDeleteBtnListener;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/android/settings/notification/SilentModeSettingsBase;-><init>()V

    new-instance v0, Lcom/android/settings/notification/SilentModeDeleteSettings$1;

    invoke-direct {v0, p0}, Lcom/android/settings/notification/SilentModeDeleteSettings$1;-><init>(Lcom/android/settings/notification/SilentModeDeleteSettings;)V

    iput-object v0, p0, Lcom/android/settings/notification/SilentModeDeleteSettings;->fY:Lcom/android/settings/notification/SilentModeDeleteSettings$OnClickDeleteBtnListener;

    return-void
.end method

.method private hx()V
    .locals 6

    iget-object v0, p0, Lcom/android/settings/notification/SilentModeDeleteSettings;->fW:Landroid/preference/PreferenceCategory;

    invoke-virtual {v0}, Landroid/preference/PreferenceCategory;->removeAll()V

    iget-object v0, p0, Lcom/android/settings/notification/SilentModeDeleteSettings;->cw:Landroid/service/notification/ZenModeConfig;

    if-nez v0, :cond_0

    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/android/settings/notification/SilentModeDeleteSettings;->dj()[Lcom/android/settings/notification/SilentModeSettingsBase$ZenRuleInfo;

    move-result-object v1

    const/4 v0, 0x0

    :goto_0
    array-length v2, v1

    if-ge v0, v2, :cond_2

    iget-object v2, p0, Lcom/android/settings/notification/SilentModeDeleteSettings;->fX:Ljava/util/List;

    aget-object v3, v1, v0

    iget-object v3, v3, Lcom/android/settings/notification/SilentModeSettingsBase$ZenRuleInfo;->id:Ljava/lang/String;

    invoke-interface {v2, v3}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    aget-object v2, v1, v0

    iget-object v2, v2, Lcom/android/settings/notification/SilentModeSettingsBase$ZenRuleInfo;->id:Ljava/lang/String;

    aget-object v3, v1, v0

    iget-object v3, v3, Lcom/android/settings/notification/SilentModeSettingsBase$ZenRuleInfo;->cE:Landroid/service/notification/ZenModeConfig$ZenRule;

    new-instance v4, Lcom/android/settings/notification/SilentModeDeleteSettings$CustomRuleItemPreference;

    iget-object v5, p0, Lcom/android/settings/notification/SilentModeDeleteSettings;->mContext:Landroid/content/Context;

    invoke-direct {v4, p0, v5}, Lcom/android/settings/notification/SilentModeDeleteSettings$CustomRuleItemPreference;-><init>(Lcom/android/settings/notification/SilentModeDeleteSettings;Landroid/content/Context;)V

    iget-object v3, v3, Landroid/service/notification/ZenModeConfig$ZenRule;->name:Ljava/lang/String;

    invoke-virtual {v4, v2, v3}, Lcom/android/settings/notification/SilentModeDeleteSettings$CustomRuleItemPreference;->hA(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v2, p0, Lcom/android/settings/notification/SilentModeDeleteSettings;->fY:Lcom/android/settings/notification/SilentModeDeleteSettings$OnClickDeleteBtnListener;

    invoke-virtual {v4, v2}, Lcom/android/settings/notification/SilentModeDeleteSettings$CustomRuleItemPreference;->hB(Lcom/android/settings/notification/SilentModeDeleteSettings$OnClickDeleteBtnListener;)V

    iget-object v2, p0, Lcom/android/settings/notification/SilentModeDeleteSettings;->fW:Landroid/preference/PreferenceCategory;

    invoke-virtual {v2, v4}, Landroid/preference/PreferenceCategory;->addPreference(Landroid/preference/Preference;)Z

    goto :goto_1

    :cond_2
    return-void
.end method

.method static synthetic hy(Lcom/android/settings/notification/SilentModeDeleteSettings;)Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/notification/SilentModeDeleteSettings;->fX:Ljava/util/List;

    return-object v0
.end method

.method static synthetic hz(Lcom/android/settings/notification/SilentModeDeleteSettings;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/notification/SilentModeDeleteSettings;->hx()V

    return-void
.end method


# virtual methods
.method protected cV()V
    .locals 0

    return-void
.end method

.method protected cW()V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/notification/SilentModeDeleteSettings;->hx()V

    return-void
.end method

.method public hw()Z
    .locals 3

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/android/settings/notification/SilentModeDeleteSettings;->fX:Ljava/util/List;

    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/android/settings/notification/SilentModeDeleteSettings;->di(Ljava/lang/String;)Z

    move-result v0

    or-int/2addr v0, v1

    move v1, v0

    goto :goto_0

    :cond_0
    return v1
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1

    invoke-super {p0, p1}, Lcom/android/settings/notification/SilentModeSettingsBase;->onCreate(Landroid/os/Bundle;)V

    const v0, 0x7f150023

    invoke-virtual {p0, v0}, Lcom/android/settings/notification/SilentModeDeleteSettings;->addPreferencesFromResource(I)V

    const-string/jumbo v0, "key_auto_rules"

    invoke-virtual {p0, v0}, Lcom/android/settings/notification/SilentModeDeleteSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceCategory;

    iput-object v0, p0, Lcom/android/settings/notification/SilentModeDeleteSettings;->fW:Landroid/preference/PreferenceCategory;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/settings/notification/SilentModeDeleteSettings;->fX:Ljava/util/List;

    invoke-direct {p0}, Lcom/android/settings/notification/SilentModeDeleteSettings;->hx()V

    return-void
.end method
