.class final Lcom/android/settings/notification/NotificationSettingsBase$3;
.super Ljava/lang/Object;
.source "NotificationSettingsBase.java"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceChangeListener;


# instance fields
.field final synthetic hM:Lcom/android/settings/notification/NotificationSettingsBase;


# direct methods
.method constructor <init>(Lcom/android/settings/notification/NotificationSettingsBase;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/notification/NotificationSettingsBase$3;->hM:Lcom/android/settings/notification/NotificationSettingsBase;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 4

    check-cast p2, Ljava/lang/String;

    invoke-static {p2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    iget-object v1, p0, Lcom/android/settings/notification/NotificationSettingsBase$3;->hM:Lcom/android/settings/notification/NotificationSettingsBase;

    invoke-static {v1}, Lcom/android/settings/notification/NotificationSettingsBase;->fJ(Lcom/android/settings/notification/NotificationSettingsBase;)I

    move-result v1

    if-ne v0, v1, :cond_0

    const/16 v0, -0x3e8

    :cond_0
    iget-object v1, p0, Lcom/android/settings/notification/NotificationSettingsBase$3;->hM:Lcom/android/settings/notification/NotificationSettingsBase;

    iget-object v1, v1, Lcom/android/settings/notification/NotificationSettingsBase;->eh:Landroid/app/NotificationChannel;

    invoke-virtual {v1, v0}, Landroid/app/NotificationChannel;->setLockscreenVisibility(I)V

    iget-object v0, p0, Lcom/android/settings/notification/NotificationSettingsBase$3;->hM:Lcom/android/settings/notification/NotificationSettingsBase;

    iget-object v0, v0, Lcom/android/settings/notification/NotificationSettingsBase;->eh:Landroid/app/NotificationChannel;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/app/NotificationChannel;->lockFields(I)V

    iget-object v0, p0, Lcom/android/settings/notification/NotificationSettingsBase$3;->hM:Lcom/android/settings/notification/NotificationSettingsBase;

    iget-object v0, v0, Lcom/android/settings/notification/NotificationSettingsBase;->ef:Lcom/android/settings/notification/NotificationBackend;

    iget-object v1, p0, Lcom/android/settings/notification/NotificationSettingsBase$3;->hM:Lcom/android/settings/notification/NotificationSettingsBase;

    iget-object v1, v1, Lcom/android/settings/notification/NotificationSettingsBase;->ec:Ljava/lang/String;

    iget-object v2, p0, Lcom/android/settings/notification/NotificationSettingsBase$3;->hM:Lcom/android/settings/notification/NotificationSettingsBase;

    iget v2, v2, Lcom/android/settings/notification/NotificationSettingsBase;->eb:I

    iget-object v3, p0, Lcom/android/settings/notification/NotificationSettingsBase$3;->hM:Lcom/android/settings/notification/NotificationSettingsBase;

    iget-object v3, v3, Lcom/android/settings/notification/NotificationSettingsBase;->eh:Landroid/app/NotificationChannel;

    invoke-virtual {v0, v1, v2, v3}, Lcom/android/settings/notification/NotificationBackend;->bu(Ljava/lang/String;ILandroid/app/NotificationChannel;)V

    const/4 v0, 0x1

    return v0
.end method
