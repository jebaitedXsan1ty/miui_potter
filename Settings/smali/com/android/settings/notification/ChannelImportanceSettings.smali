.class public Lcom/android/settings/notification/ChannelImportanceSettings;
.super Lcom/android/settings/notification/NotificationSettingsBase;
.source "ChannelImportanceSettings.java"

# interfaces
.implements Lcom/android/settings/widget/L;
.implements Lcom/android/settings/search/Indexable;


# static fields
.field public static final SEARCH_INDEX_DATA_PROVIDER:Lcom/android/settings/search/Indexable$SearchIndexProvider;


# instance fields
.field eT:Ljava/util/List;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/android/settings/notification/ChannelImportanceSettings$1;

    invoke-direct {v0}, Lcom/android/settings/notification/ChannelImportanceSettings$1;-><init>()V

    sput-object v0, Lcom/android/settings/notification/ChannelImportanceSettings;->SEARCH_INDEX_DATA_PROVIDER:Lcom/android/settings/search/Indexable$SearchIndexProvider;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/android/settings/notification/NotificationSettingsBase;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/settings/notification/ChannelImportanceSettings;->eT:Ljava/util/List;

    return-void
.end method

.method private gk()Landroid/preference/PreferenceScreen;
    .locals 3

    invoke-virtual {p0}, Lcom/android/settings/notification/ChannelImportanceSettings;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/preference/PreferenceScreen;->removeAll()V

    :cond_0
    const v0, 0x7f150098

    invoke-virtual {p0, v0}, Lcom/android/settings/notification/ChannelImportanceSettings;->addPreferencesFromResource(I)V

    invoke-virtual {p0}, Lcom/android/settings/notification/ChannelImportanceSettings;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v1

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1}, Landroid/preference/PreferenceScreen;->getPreferenceCount()I

    move-result v2

    if-ge v0, v2, :cond_1

    invoke-virtual {v1, v0}, Landroid/preference/PreferenceScreen;->getPreference(I)Landroid/preference/Preference;

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/android/settings/notification/ChannelImportanceSettings;->eh:Landroid/app/NotificationChannel;

    invoke-virtual {v0}, Landroid/app/NotificationChannel;->getImportance()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :goto_1
    return-object v1

    :pswitch_0
    const-string/jumbo v0, "importance_min"

    invoke-direct {p0, v0}, Lcom/android/settings/notification/ChannelImportanceSettings;->gm(Ljava/lang/String;)V

    goto :goto_1

    :pswitch_1
    const-string/jumbo v0, "importance_low"

    invoke-direct {p0, v0}, Lcom/android/settings/notification/ChannelImportanceSettings;->gm(Ljava/lang/String;)V

    goto :goto_1

    :pswitch_2
    const-string/jumbo v0, "importance_default"

    invoke-direct {p0, v0}, Lcom/android/settings/notification/ChannelImportanceSettings;->gm(Ljava/lang/String;)V

    goto :goto_1

    :pswitch_3
    const-string/jumbo v0, "importance_high"

    invoke-direct {p0, v0}, Lcom/android/settings/notification/ChannelImportanceSettings;->gm(Ljava/lang/String;)V

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_3
    .end packed-switch
.end method

.method private gm(Ljava/lang/String;)V
    .locals 3

    iget-object v0, p0, Lcom/android/settings/notification/ChannelImportanceSettings;->eT:Ljava/util/List;

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settings/widget/RadioButtonPreference;

    invoke-virtual {v0}, Lcom/android/settings/widget/RadioButtonPreference;->getKey()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Lcom/android/settings/widget/RadioButtonPreference;->setChecked(Z)V

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lcom/android/settings/widget/RadioButtonPreference;->setChecked(Z)V

    goto :goto_0

    :cond_1
    return-void
.end method


# virtual methods
.method O()V
    .locals 0

    return-void
.end method

.method Q(Z)V
    .locals 0

    return-void
.end method

.method public getMetricsCategory()I
    .locals 1

    const/16 v0, 0x3ad

    return v0
.end method

.method public gl(Lcom/android/settings/widget/RadioButtonPreference;)V
    .locals 4

    const/4 v2, 0x4

    invoke-virtual {p1}, Lcom/android/settings/widget/RadioButtonPreference;->getKey()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "importance_high"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v0, p0, Lcom/android/settings/notification/ChannelImportanceSettings;->eh:Landroid/app/NotificationChannel;

    invoke-virtual {v0, v2}, Landroid/app/NotificationChannel;->setImportance(I)V

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/android/settings/widget/RadioButtonPreference;->getKey()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/android/settings/notification/ChannelImportanceSettings;->gm(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/settings/notification/ChannelImportanceSettings;->eh:Landroid/app/NotificationChannel;

    invoke-virtual {v0, v2}, Landroid/app/NotificationChannel;->lockFields(I)V

    iget-object v0, p0, Lcom/android/settings/notification/ChannelImportanceSettings;->ef:Lcom/android/settings/notification/NotificationBackend;

    iget-object v1, p0, Lcom/android/settings/notification/ChannelImportanceSettings;->eg:Lcom/android/settings/notification/NotificationBackend$AppRow;

    iget-object v1, v1, Lcom/android/settings/notification/NotificationBackend$AppRow;->pkg:Ljava/lang/String;

    iget-object v2, p0, Lcom/android/settings/notification/ChannelImportanceSettings;->eg:Lcom/android/settings/notification/NotificationBackend$AppRow;

    iget v2, v2, Lcom/android/settings/notification/NotificationBackend$AppRow;->aE:I

    iget-object v3, p0, Lcom/android/settings/notification/ChannelImportanceSettings;->eh:Landroid/app/NotificationChannel;

    invoke-virtual {v0, v1, v2, v3}, Lcom/android/settings/notification/NotificationBackend;->bu(Ljava/lang/String;ILandroid/app/NotificationChannel;)V

    return-void

    :cond_1
    const-string/jumbo v1, "importance_default"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v0, p0, Lcom/android/settings/notification/ChannelImportanceSettings;->eh:Landroid/app/NotificationChannel;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/app/NotificationChannel;->setImportance(I)V

    goto :goto_0

    :cond_2
    const-string/jumbo v1, "importance_low"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v0, p0, Lcom/android/settings/notification/ChannelImportanceSettings;->eh:Landroid/app/NotificationChannel;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/app/NotificationChannel;->setImportance(I)V

    goto :goto_0

    :cond_3
    const-string/jumbo v1, "importance_min"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/notification/ChannelImportanceSettings;->eh:Landroid/app/NotificationChannel;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/NotificationChannel;->setImportance(I)V

    goto :goto_0
.end method

.method public onPause()V
    .locals 0

    invoke-super {p0}, Lcom/android/settings/notification/NotificationSettingsBase;->onPause()V

    return-void
.end method

.method public onResume()V
    .locals 2

    invoke-super {p0}, Lcom/android/settings/notification/NotificationSettingsBase;->onResume()V

    iget v0, p0, Lcom/android/settings/notification/ChannelImportanceSettings;->eb:I

    if-ltz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/notification/ChannelImportanceSettings;->ec:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/notification/ChannelImportanceSettings;->ed:Landroid/content/pm/PackageInfo;

    if-nez v0, :cond_1

    :cond_0
    const-string/jumbo v0, "NotiImportance"

    const-string/jumbo v1, "Missing package or uid or packageinfo or channel"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lcom/android/settings/notification/ChannelImportanceSettings;->finish()V

    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/settings/notification/ChannelImportanceSettings;->eh:Landroid/app/NotificationChannel;

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/android/settings/notification/ChannelImportanceSettings;->gk()Landroid/preference/PreferenceScreen;

    return-void
.end method
