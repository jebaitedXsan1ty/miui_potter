.class public Lcom/android/settings/notification/ZenModeSettings;
.super Lcom/android/settings/notification/ZenModeSettingsBase;
.source "ZenModeSettings.java"


# static fields
.field static final aY:Lcom/android/settings/utils/b;

.field private static final aZ:Ljava/util/Comparator;


# instance fields
.field private ba:Landroid/preference/PreferenceCategory;

.field private bb:Landroid/content/pm/PackageManager;

.field private bc:Landroid/app/NotificationManager$Policy;

.field private bd:Landroid/preference/Preference;

.field private be:Lcom/android/settings/utils/i;

.field private bf:Lcom/android/settings/notification/ZenModeSettings$SummaryBuilder;

.field private bg:Landroid/preference/Preference;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    invoke-static {}, Lcom/android/settings/notification/ZenModeSettings;->bW()Lcom/android/settings/utils/b;

    move-result-object v0

    sput-object v0, Lcom/android/settings/notification/ZenModeSettings;->aY:Lcom/android/settings/utils/b;

    new-instance v0, Lcom/android/settings/notification/ZenModeSettings$1;

    invoke-direct {v0}, Lcom/android/settings/notification/ZenModeSettings$1;-><init>()V

    sput-object v0, Lcom/android/settings/notification/ZenModeSettings;->aZ:Ljava/util/Comparator;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/notification/ZenModeSettingsBase;-><init>()V

    return-void
.end method

.method private bU(Landroid/app/AutomaticZenRule;ZLjava/lang/CharSequence;)Ljava/lang/String;
    .locals 3

    invoke-virtual {p0}, Lcom/android/settings/notification/ZenModeSettings;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {p1}, Landroid/app/AutomaticZenRule;->getInterruptionFilter()I

    move-result v1

    invoke-static {v0, v1}, Lcom/android/settings/notification/ZenModeSettings;->bV(Landroid/content/res/Resources;I)Ljava/lang/String;

    move-result-object v0

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/app/AutomaticZenRule;->isEnabled()Z

    move-result v1

    xor-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_1

    :cond_0
    const v0, 0x7f12120f

    invoke-virtual {p0, v0}, Lcom/android/settings/notification/ZenModeSettings;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_1
    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object v0, v1, v2

    const v0, 0x7f1216c4

    invoke-virtual {p0, v0, v1}, Lcom/android/settings/notification/ZenModeSettings;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private static bV(Landroid/content/res/Resources;I)Ljava/lang/String;
    .locals 1

    packed-switch p1, :pswitch_data_0

    const/4 v0, 0x0

    return-object v0

    :pswitch_0
    const v0, 0x7f1216b4

    invoke-virtual {p0, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0

    :pswitch_1
    const v0, 0x7f1216b5

    invoke-virtual {p0, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0

    :pswitch_2
    const v0, 0x7f1216b8

    invoke-virtual {p0, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0

    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_2
        :pswitch_0
    .end packed-switch
.end method

.method private static bW()Lcom/android/settings/utils/b;
    .locals 2

    new-instance v0, Lcom/android/settings/utils/b;

    invoke-direct {v0}, Lcom/android/settings/utils/b;-><init>()V

    const-string/jumbo v1, "ZenModeSettings"

    iput-object v1, v0, Lcom/android/settings/utils/b;->aMd:Ljava/lang/String;

    const-string/jumbo v1, "android.service.notification.ConditionProviderService"

    iput-object v1, v0, Lcom/android/settings/utils/b;->intentAction:Ljava/lang/String;

    const-string/jumbo v1, "android.permission.BIND_CONDITION_PROVIDER_SERVICE"

    iput-object v1, v0, Lcom/android/settings/utils/b;->aMf:Ljava/lang/String;

    const-string/jumbo v1, "condition provider"

    iput-object v1, v0, Lcom/android/settings/utils/b;->aMg:Ljava/lang/String;

    return-object v0
.end method

.method public static bX(Landroid/content/pm/PackageManager;Landroid/content/pm/ServiceInfo;)Lcom/android/settings/notification/ZenRuleInfo;
    .locals 5

    const/4 v3, 0x0

    if-eqz p1, :cond_0

    iget-object v0, p1, Landroid/content/pm/ServiceInfo;->metaData:Landroid/os/Bundle;

    if-nez v0, :cond_1

    :cond_0
    return-object v3

    :cond_1
    iget-object v0, p1, Landroid/content/pm/ServiceInfo;->metaData:Landroid/os/Bundle;

    const-string/jumbo v1, "android.service.zen.automatic.ruleType"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {p1}, Lcom/android/settings/notification/ZenModeSettings;->bZ(Landroid/content/pm/ServiceInfo;)Landroid/content/ComponentName;

    move-result-object v1

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    xor-int/lit8 v2, v2, 0x1

    if-eqz v2, :cond_2

    if-eqz v1, :cond_2

    new-instance v1, Lcom/android/settings/notification/ZenRuleInfo;

    invoke-direct {v1}, Lcom/android/settings/notification/ZenRuleInfo;-><init>()V

    new-instance v2, Landroid/content/ComponentName;

    iget-object v3, p1, Landroid/content/pm/ServiceInfo;->packageName:Ljava/lang/String;

    iget-object v4, p1, Landroid/content/pm/ServiceInfo;->name:Ljava/lang/String;

    invoke-direct {v2, v3, v4}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v2, v1, Lcom/android/settings/notification/ZenRuleInfo;->x:Landroid/content/ComponentName;

    const-string/jumbo v2, "android.settings.ZEN_MODE_EXTERNAL_RULE_SETTINGS"

    iput-object v2, v1, Lcom/android/settings/notification/ZenRuleInfo;->y:Ljava/lang/String;

    iput-object v0, v1, Lcom/android/settings/notification/ZenRuleInfo;->title:Ljava/lang/String;

    iget-object v0, p1, Landroid/content/pm/ServiceInfo;->packageName:Ljava/lang/String;

    iput-object v0, v1, Lcom/android/settings/notification/ZenRuleInfo;->packageName:Ljava/lang/String;

    invoke-static {p1}, Lcom/android/settings/notification/ZenModeSettings;->bZ(Landroid/content/pm/ServiceInfo;)Landroid/content/ComponentName;

    move-result-object v0

    iput-object v0, v1, Lcom/android/settings/notification/ZenRuleInfo;->s:Landroid/content/ComponentName;

    iget-object v0, p1, Landroid/content/pm/ServiceInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    invoke-virtual {v0, p0}, Landroid/content/pm/ApplicationInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, v1, Lcom/android/settings/notification/ZenRuleInfo;->v:Ljava/lang/CharSequence;

    iget-object v0, p1, Landroid/content/pm/ServiceInfo;->metaData:Landroid/os/Bundle;

    const-string/jumbo v2, "android.service.zen.automatic.ruleInstanceLimit"

    const/4 v3, -0x1

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, v1, Lcom/android/settings/notification/ZenRuleInfo;->w:I

    return-object v1

    :cond_2
    return-object v3
.end method

.method private bY(Ljava/lang/String;Landroid/content/ComponentName;Ljava/lang/String;)Landroid/content/Intent;
    .locals 2

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const/high16 v1, 0x4000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "android.service.notification.extra.RULE_ID"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    if-eqz p2, :cond_0

    invoke-virtual {v0, p2}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {v0, p1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_0
.end method

.method private static bZ(Landroid/content/pm/ServiceInfo;)Landroid/content/ComponentName;
    .locals 3

    const/4 v2, 0x0

    if-eqz p0, :cond_0

    iget-object v0, p0, Landroid/content/pm/ServiceInfo;->metaData:Landroid/os/Bundle;

    if-nez v0, :cond_1

    :cond_0
    return-object v2

    :cond_1
    iget-object v0, p0, Landroid/content/pm/ServiceInfo;->metaData:Landroid/os/Bundle;

    const-string/jumbo v1, "android.service.zen.automatic.configurationActivity"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-static {v0}, Landroid/content/ComponentName;->unflattenFromString(Ljava/lang/String;)Landroid/content/ComponentName;

    move-result-object v0

    return-object v0

    :cond_2
    return-object v2
.end method

.method private cc()V
    .locals 3

    new-instance v0, Lcom/android/settings/notification/ZenModeSettings$3;

    iget-object v1, p0, Lcom/android/settings/notification/ZenModeSettings;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/android/settings/notification/ZenModeSettings;->be:Lcom/android/settings/utils/i;

    invoke-direct {v0, p0, v1, v2}, Lcom/android/settings/notification/ZenModeSettings$3;-><init>(Lcom/android/settings/notification/ZenModeSettings;Landroid/content/Context;Lcom/android/settings/utils/i;)V

    invoke-virtual {v0}, Lcom/android/settings/notification/ZenModeSettings$3;->show()V

    return-void
.end method

.method private cd(Ljava/lang/String;Ljava/lang/CharSequence;)V
    .locals 3

    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Lcom/android/settings/notification/ZenModeSettings;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p2, v1, v2

    const v2, 0x7f12169d

    invoke-virtual {p0, v2, v1}, Lcom/android/settings/notification/ZenModeSettings;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f1203c7

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    new-instance v1, Lcom/android/settings/notification/ZenModeSettings$5;

    invoke-direct {v1, p0, p1}, Lcom/android/settings/notification/ZenModeSettings$5;-><init>(Lcom/android/settings/notification/ZenModeSettings;Ljava/lang/String;)V

    const v2, 0x7f12169c

    invoke-virtual {v0, v2, v1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    return-void
.end method

.method private ce(Lcom/android/settings/notification/ZenRuleInfo;)V
    .locals 3

    new-instance v0, Lcom/android/settings/notification/ZenModeSettings$4;

    iget-object v1, p0, Lcom/android/settings/notification/ZenModeSettings;->mContext:Landroid/content/Context;

    const/4 v2, 0x0

    invoke-direct {v0, p0, v1, v2, p1}, Lcom/android/settings/notification/ZenModeSettings$4;-><init>(Lcom/android/settings/notification/ZenModeSettings;Landroid/content/Context;Ljava/lang/CharSequence;Lcom/android/settings/notification/ZenRuleInfo;)V

    invoke-virtual {v0}, Lcom/android/settings/notification/ZenModeSettings$4;->show()V

    return-void
.end method

.method private cf()[Ljava/util/Map$Entry;
    .locals 2

    iget-object v0, p0, Lcom/android/settings/notification/ZenModeSettings;->eI:Ljava/util/Set;

    iget-object v1, p0, Lcom/android/settings/notification/ZenModeSettings;->eI:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->size()I

    move-result v1

    new-array v1, v1, [Ljava/util/Map$Entry;

    invoke-interface {v0, v1}, Ljava/util/Set;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/util/Map$Entry;

    sget-object v1, Lcom/android/settings/notification/ZenModeSettings;->aZ:Ljava/util/Comparator;

    invoke-static {v0, v1}, Ljava/util/Arrays;->sort([Ljava/lang/Object;Ljava/util/Comparator;)V

    return-object v0
.end method

.method private cg()V
    .locals 7

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/android/settings/notification/ZenModeSettings;->ba:Landroid/preference/PreferenceCategory;

    invoke-virtual {v0}, Landroid/preference/PreferenceCategory;->removeAll()V

    invoke-direct {p0}, Lcom/android/settings/notification/ZenModeSettings;->cf()[Ljava/util/Map$Entry;

    move-result-object v2

    array-length v3, v2

    move v0, v1

    :goto_0
    if-ge v0, v3, :cond_1

    aget-object v4, v2, v0

    new-instance v5, Lcom/android/settings/notification/ZenModeSettings$ZenRulePreference;

    invoke-virtual {p0}, Lcom/android/settings/notification/ZenModeSettings;->bWz()Landroid/content/Context;

    move-result-object v6

    invoke-direct {v5, p0, v6, v4}, Lcom/android/settings/notification/ZenModeSettings$ZenRulePreference;-><init>(Lcom/android/settings/notification/ZenModeSettings;Landroid/content/Context;Ljava/util/Map$Entry;)V

    iget-boolean v4, v5, Lcom/android/settings/notification/ZenModeSettings$ZenRulePreference;->bh:Z

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/android/settings/notification/ZenModeSettings;->ba:Landroid/preference/PreferenceCategory;

    invoke-virtual {v4, v5}, Landroid/preference/PreferenceCategory;->addPreference(Landroid/preference/Preference;)Z

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    new-instance v0, Landroid/preference/Preference;

    invoke-virtual {p0}, Lcom/android/settings/notification/ZenModeSettings;->bWz()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v0, v2}, Landroid/preference/Preference;-><init>(Landroid/content/Context;)V

    const v2, 0x7f080202

    invoke-virtual {v0, v2}, Landroid/preference/Preference;->setIcon(I)V

    const v2, 0x7f12168f

    invoke-virtual {v0, v2}, Landroid/preference/Preference;->setTitle(I)V

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setPersistent(Z)V

    new-instance v1, Lcom/android/settings/notification/ZenModeSettings$2;

    invoke-direct {v1, p0}, Lcom/android/settings/notification/ZenModeSettings$2;-><init>(Lcom/android/settings/notification/ZenModeSettings;)V

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    iget-object v1, p0, Lcom/android/settings/notification/ZenModeSettings;->ba:Landroid/preference/PreferenceCategory;

    invoke-virtual {v1, v0}, Landroid/preference/PreferenceCategory;->addPreference(Landroid/preference/Preference;)Z

    return-void
.end method

.method private ch()V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/notification/ZenModeSettings;->ci()V

    invoke-direct {p0}, Lcom/android/settings/notification/ZenModeSettings;->cj()V

    invoke-direct {p0}, Lcom/android/settings/notification/ZenModeSettings;->cg()V

    return-void
.end method

.method private ci()V
    .locals 3

    iget-object v0, p0, Lcom/android/settings/notification/ZenModeSettings;->bd:Landroid/preference/Preference;

    iget-object v1, p0, Lcom/android/settings/notification/ZenModeSettings;->bf:Lcom/android/settings/notification/ZenModeSettings$SummaryBuilder;

    iget-object v2, p0, Lcom/android/settings/notification/ZenModeSettings;->bc:Landroid/app/NotificationManager$Policy;

    invoke-virtual {v1, v2}, Lcom/android/settings/notification/ZenModeSettings$SummaryBuilder;->cv(Landroid/app/NotificationManager$Policy;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    return-void
.end method

.method private cj()V
    .locals 3

    iget-object v0, p0, Lcom/android/settings/notification/ZenModeSettings;->bg:Landroid/preference/Preference;

    iget-object v1, p0, Lcom/android/settings/notification/ZenModeSettings;->bf:Lcom/android/settings/notification/ZenModeSettings$SummaryBuilder;

    iget-object v2, p0, Lcom/android/settings/notification/ZenModeSettings;->bc:Landroid/app/NotificationManager$Policy;

    invoke-virtual {v1, v2}, Lcom/android/settings/notification/ZenModeSettings$SummaryBuilder;->cw(Landroid/app/NotificationManager$Policy;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    return-void
.end method

.method static synthetic ck(Lcom/android/settings/notification/ZenModeSettings;)Landroid/content/pm/PackageManager;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/notification/ZenModeSettings;->bb:Landroid/content/pm/PackageManager;

    return-object v0
.end method

.method static synthetic cl(Lcom/android/settings/notification/ZenModeSettings;)Lcom/android/settings/utils/i;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/notification/ZenModeSettings;->be:Lcom/android/settings/utils/i;

    return-object v0
.end method

.method static synthetic cm(Landroid/content/pm/ServiceInfo;)Landroid/content/ComponentName;
    .locals 1

    invoke-static {p0}, Lcom/android/settings/notification/ZenModeSettings;->bZ(Landroid/content/pm/ServiceInfo;)Landroid/content/ComponentName;

    move-result-object v0

    return-object v0
.end method

.method static synthetic cn(Lcom/android/settings/notification/ZenModeSettings;Ljava/lang/String;Landroid/content/ComponentName;Ljava/lang/String;)Landroid/content/Intent;
    .locals 1

    invoke-direct {p0, p1, p2, p3}, Lcom/android/settings/notification/ZenModeSettings;->bY(Ljava/lang/String;Landroid/content/ComponentName;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method static synthetic co(Lcom/android/settings/notification/ZenModeSettings;Landroid/app/AutomaticZenRule;ZLjava/lang/CharSequence;)Ljava/lang/String;
    .locals 1

    invoke-direct {p0, p1, p2, p3}, Lcom/android/settings/notification/ZenModeSettings;->bU(Landroid/app/AutomaticZenRule;ZLjava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic cp(Lcom/android/settings/notification/ZenModeSettings;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/notification/ZenModeSettings;->cc()V

    return-void
.end method

.method static synthetic cq(Lcom/android/settings/notification/ZenModeSettings;Ljava/lang/String;Ljava/lang/CharSequence;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/android/settings/notification/ZenModeSettings;->cd(Ljava/lang/String;Ljava/lang/CharSequence;)V

    return-void
.end method

.method static synthetic cr(Lcom/android/settings/notification/ZenModeSettings;Lcom/android/settings/notification/ZenRuleInfo;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/settings/notification/ZenModeSettings;->ce(Lcom/android/settings/notification/ZenRuleInfo;)V

    return-void
.end method


# virtual methods
.method protected aq()I
    .locals 1

    const v0, 0x7f12080d

    return v0
.end method

.method protected ca()V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/notification/ZenModeSettings;->ch()V

    return-void
.end method

.method protected cb()V
    .locals 1

    iget-object v0, p0, Lcom/android/settings/notification/ZenModeSettings;->mContext:Landroid/content/Context;

    invoke-static {v0}, Landroid/app/NotificationManager;->from(Landroid/content/Context;)Landroid/app/NotificationManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/NotificationManager;->getNotificationPolicy()Landroid/app/NotificationManager$Policy;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/notification/ZenModeSettings;->bc:Landroid/app/NotificationManager$Policy;

    invoke-direct {p0}, Lcom/android/settings/notification/ZenModeSettings;->ch()V

    return-void
.end method

.method public getMetricsCategory()I
    .locals 1

    const/16 v0, 0x4c

    return v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3

    invoke-super {p0, p1}, Lcom/android/settings/notification/ZenModeSettingsBase;->onCreate(Landroid/os/Bundle;)V

    const v0, 0x7f15011b

    invoke-virtual {p0, v0}, Lcom/android/settings/notification/ZenModeSettings;->addPreferencesFromResource(I)V

    invoke-virtual {p0}, Lcom/android/settings/notification/ZenModeSettings;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v1

    const-string/jumbo v0, "automatic_rules"

    invoke-virtual {v1, v0}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceCategory;

    iput-object v0, p0, Lcom/android/settings/notification/ZenModeSettings;->ba:Landroid/preference/PreferenceCategory;

    const-string/jumbo v0, "priority_settings"

    invoke-virtual {v1, v0}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/notification/ZenModeSettings;->bd:Landroid/preference/Preference;

    const-string/jumbo v0, "visual_interruptions_settings"

    invoke-virtual {v1, v0}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/notification/ZenModeSettings;->bg:Landroid/preference/Preference;

    iget-object v0, p0, Lcom/android/settings/notification/ZenModeSettings;->mContext:Landroid/content/Context;

    invoke-static {v0}, Landroid/app/NotificationManager;->from(Landroid/content/Context;)Landroid/app/NotificationManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/NotificationManager;->getNotificationPolicy()Landroid/app/NotificationManager$Policy;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/notification/ZenModeSettings;->bc:Landroid/app/NotificationManager$Policy;

    new-instance v0, Lcom/android/settings/notification/ZenModeSettings$SummaryBuilder;

    invoke-virtual {p0}, Lcom/android/settings/notification/ZenModeSettings;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/android/settings/notification/ZenModeSettings$SummaryBuilder;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/settings/notification/ZenModeSettings;->bf:Lcom/android/settings/notification/ZenModeSettings$SummaryBuilder;

    iget-object v0, p0, Lcom/android/settings/notification/ZenModeSettings;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/notification/ZenModeSettings;->bb:Landroid/content/pm/PackageManager;

    new-instance v0, Lcom/android/settings/utils/i;

    iget-object v1, p0, Lcom/android/settings/notification/ZenModeSettings;->mContext:Landroid/content/Context;

    sget-object v2, Lcom/android/settings/notification/ZenModeSettings;->aY:Lcom/android/settings/utils/b;

    invoke-direct {v0, v1, v2}, Lcom/android/settings/utils/i;-><init>(Landroid/content/Context;Lcom/android/settings/utils/b;)V

    iput-object v0, p0, Lcom/android/settings/notification/ZenModeSettings;->be:Lcom/android/settings/utils/i;

    iget-object v0, p0, Lcom/android/settings/notification/ZenModeSettings;->be:Lcom/android/settings/utils/i;

    invoke-virtual {v0}, Lcom/android/settings/utils/i;->ayH()V

    return-void
.end method

.method public onResume()V
    .locals 1

    invoke-super {p0}, Lcom/android/settings/notification/ZenModeSettingsBase;->onResume()V

    invoke-virtual {p0}, Lcom/android/settings/notification/ZenModeSettings;->bXB()Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    :cond_0
    invoke-direct {p0}, Lcom/android/settings/notification/ZenModeSettings;->ch()V

    return-void
.end method
