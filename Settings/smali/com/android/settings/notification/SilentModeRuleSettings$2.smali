.class final Lcom/android/settings/notification/SilentModeRuleSettings$2;
.super Ljava/lang/Object;
.source "SilentModeRuleSettings.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic gh:Lcom/android/settings/notification/SilentModeRuleSettings;

.field final synthetic gi:I


# direct methods
.method constructor <init>(Lcom/android/settings/notification/SilentModeRuleSettings;I)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/notification/SilentModeRuleSettings$2;->gh:Lcom/android/settings/notification/SilentModeRuleSettings;

    iput p2, p0, Lcom/android/settings/notification/SilentModeRuleSettings$2;->gi:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 2

    iget v0, p0, Lcom/android/settings/notification/SilentModeRuleSettings$2;->gi:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/android/settings/notification/SilentModeRuleSettings$2;->gh:Lcom/android/settings/notification/SilentModeRuleSettings;

    invoke-static {v0}, Lcom/android/settings/notification/SilentModeRuleSettings;->g(Lcom/android/settings/notification/SilentModeRuleSettings;)Lcom/android/settings/notification/SilentModeAddRuleSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/settings/notification/SilentModeAddRuleSettings;->da()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/notification/SilentModeRuleSettings$2;->gh:Lcom/android/settings/notification/SilentModeRuleSettings;

    invoke-virtual {v0}, Lcom/android/settings/notification/SilentModeRuleSettings;->finish()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget v0, p0, Lcom/android/settings/notification/SilentModeRuleSettings$2;->gi:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/android/settings/notification/SilentModeRuleSettings$2;->gh:Lcom/android/settings/notification/SilentModeRuleSettings;

    invoke-static {v0}, Lcom/android/settings/notification/SilentModeRuleSettings;->h(Lcom/android/settings/notification/SilentModeRuleSettings;)Lcom/android/settings/notification/SilentModeEditRuleSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/settings/notification/SilentModeEditRuleSettings;->da()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/notification/SilentModeRuleSettings$2;->gh:Lcom/android/settings/notification/SilentModeRuleSettings;

    invoke-virtual {v0}, Lcom/android/settings/notification/SilentModeRuleSettings;->finish()V

    goto :goto_0
.end method
