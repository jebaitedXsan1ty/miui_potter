.class final Lcom/android/settings/notification/AppNotificationSettings$5;
.super Ljava/lang/Object;
.source "AppNotificationSettings.java"

# interfaces
.implements Lcom/android/settings/widget/t;


# instance fields
.field final synthetic gt:Lcom/android/settings/notification/AppNotificationSettings;


# direct methods
.method constructor <init>(Lcom/android/settings/notification/AppNotificationSettings;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/notification/AppNotificationSettings$5;->gt:Lcom/android/settings/notification/AppNotificationSettings;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public gG(Landroid/widget/Switch;Z)V
    .locals 5

    const/4 v3, 0x1

    const/4 v2, 0x0

    const/16 v1, -0x3e8

    iget-object v0, p0, Lcom/android/settings/notification/AppNotificationSettings$5;->gt:Lcom/android/settings/notification/AppNotificationSettings;

    iget-boolean v0, v0, Lcom/android/settings/notification/AppNotificationSettings;->ee:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/settings/notification/AppNotificationSettings$5;->gt:Lcom/android/settings/notification/AppNotificationSettings;

    iget-object v0, v0, Lcom/android/settings/notification/AppNotificationSettings;->eh:Landroid/app/NotificationChannel;

    if-eqz v0, :cond_1

    if-eqz p2, :cond_2

    move v0, v1

    :goto_0
    iget-object v4, p0, Lcom/android/settings/notification/AppNotificationSettings$5;->gt:Lcom/android/settings/notification/AppNotificationSettings;

    iget-object v4, v4, Lcom/android/settings/notification/AppNotificationSettings;->el:Lcom/android/settingslib/MiuiRestrictedSwitchPreference;

    if-ne v0, v1, :cond_0

    move v2, v3

    :cond_0
    invoke-virtual {v4, v2}, Lcom/android/settingslib/MiuiRestrictedSwitchPreference;->setChecked(Z)V

    iget-object v1, p0, Lcom/android/settings/notification/AppNotificationSettings$5;->gt:Lcom/android/settings/notification/AppNotificationSettings;

    iget-object v1, v1, Lcom/android/settings/notification/AppNotificationSettings;->eh:Landroid/app/NotificationChannel;

    invoke-virtual {v1, v0}, Landroid/app/NotificationChannel;->setImportance(I)V

    iget-object v0, p0, Lcom/android/settings/notification/AppNotificationSettings$5;->gt:Lcom/android/settings/notification/AppNotificationSettings;

    iget-object v0, v0, Lcom/android/settings/notification/AppNotificationSettings;->eh:Landroid/app/NotificationChannel;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/app/NotificationChannel;->lockFields(I)V

    iget-object v0, p0, Lcom/android/settings/notification/AppNotificationSettings$5;->gt:Lcom/android/settings/notification/AppNotificationSettings;

    iget-object v0, v0, Lcom/android/settings/notification/AppNotificationSettings;->ef:Lcom/android/settings/notification/NotificationBackend;

    iget-object v1, p0, Lcom/android/settings/notification/AppNotificationSettings$5;->gt:Lcom/android/settings/notification/AppNotificationSettings;

    iget-object v1, v1, Lcom/android/settings/notification/AppNotificationSettings;->ec:Ljava/lang/String;

    iget-object v2, p0, Lcom/android/settings/notification/AppNotificationSettings$5;->gt:Lcom/android/settings/notification/AppNotificationSettings;

    iget v2, v2, Lcom/android/settings/notification/AppNotificationSettings;->eb:I

    iget-object v4, p0, Lcom/android/settings/notification/AppNotificationSettings$5;->gt:Lcom/android/settings/notification/AppNotificationSettings;

    iget-object v4, v4, Lcom/android/settings/notification/AppNotificationSettings;->eh:Landroid/app/NotificationChannel;

    invoke-virtual {v0, v1, v2, v4}, Lcom/android/settings/notification/NotificationBackend;->bu(Ljava/lang/String;ILandroid/app/NotificationChannel;)V

    :cond_1
    iget-object v0, p0, Lcom/android/settings/notification/AppNotificationSettings$5;->gt:Lcom/android/settings/notification/AppNotificationSettings;

    iget-object v0, v0, Lcom/android/settings/notification/AppNotificationSettings;->ef:Lcom/android/settings/notification/NotificationBackend;

    iget-object v1, p0, Lcom/android/settings/notification/AppNotificationSettings$5;->gt:Lcom/android/settings/notification/AppNotificationSettings;

    iget-object v1, v1, Lcom/android/settings/notification/AppNotificationSettings;->ed:Landroid/content/pm/PackageInfo;

    iget-object v1, v1, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    iget-object v2, p0, Lcom/android/settings/notification/AppNotificationSettings$5;->gt:Lcom/android/settings/notification/AppNotificationSettings;

    iget v2, v2, Lcom/android/settings/notification/AppNotificationSettings;->eb:I

    invoke-virtual {v0, v1, v2, p2}, Lcom/android/settings/notification/NotificationBackend;->bv(Ljava/lang/String;IZ)Z

    iget-object v0, p0, Lcom/android/settings/notification/AppNotificationSettings$5;->gt:Lcom/android/settings/notification/AppNotificationSettings;

    iget-object v0, v0, Lcom/android/settings/notification/AppNotificationSettings;->eg:Lcom/android/settings/notification/NotificationBackend$AppRow;

    iput-boolean v3, v0, Lcom/android/settings/notification/NotificationBackend$AppRow;->aF:Z

    iget-object v0, p0, Lcom/android/settings/notification/AppNotificationSettings$5;->gt:Lcom/android/settings/notification/AppNotificationSettings;

    xor-int/lit8 v1, p2, 0x1

    invoke-virtual {v0, v1}, Lcom/android/settings/notification/AppNotificationSettings;->Q(Z)V

    return-void

    :cond_2
    move v0, v2

    goto :goto_0
.end method
