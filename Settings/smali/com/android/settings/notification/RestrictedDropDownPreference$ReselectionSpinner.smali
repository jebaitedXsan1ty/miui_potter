.class public Lcom/android/settings/notification/RestrictedDropDownPreference$ReselectionSpinner;
.super Landroid/widget/Spinner;
.source "RestrictedDropDownPreference.java"


# instance fields
.field private fO:Lcom/android/settings/notification/RestrictedDropDownPreference;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Landroid/widget/Spinner;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method


# virtual methods
.method public setPreference(Lcom/android/settings/notification/RestrictedDropDownPreference;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/notification/RestrictedDropDownPreference$ReselectionSpinner;->fO:Lcom/android/settings/notification/RestrictedDropDownPreference;

    return-void
.end method

.method public setSelection(I)V
    .locals 2

    invoke-virtual {p0}, Lcom/android/settings/notification/RestrictedDropDownPreference$ReselectionSpinner;->getSelectedItemPosition()I

    move-result v0

    invoke-super {p0, p1}, Landroid/widget/Spinner;->setSelection(I)V

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/notification/RestrictedDropDownPreference$ReselectionSpinner;->fO:Lcom/android/settings/notification/RestrictedDropDownPreference;

    invoke-static {v0}, Lcom/android/settings/notification/RestrictedDropDownPreference;->hl(Lcom/android/settings/notification/RestrictedDropDownPreference;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/notification/RestrictedDropDownPreference$ReselectionSpinner;->fO:Lcom/android/settings/notification/RestrictedDropDownPreference;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/android/settings/notification/RestrictedDropDownPreference;->ho(Lcom/android/settings/notification/RestrictedDropDownPreference;Z)V

    iget-object v0, p0, Lcom/android/settings/notification/RestrictedDropDownPreference$ReselectionSpinner;->fO:Lcom/android/settings/notification/RestrictedDropDownPreference;

    invoke-static {v0, p1}, Lcom/android/settings/notification/RestrictedDropDownPreference;->hn(Lcom/android/settings/notification/RestrictedDropDownPreference;I)Lcom/android/settings/notification/RestrictedDropDownPreference$RestrictedItem;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/android/settings/notification/RestrictedDropDownPreference$ReselectionSpinner;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v0, v0, Lcom/android/settings/notification/RestrictedDropDownPreference$RestrictedItem;->fP:Lcom/android/settingslib/n;

    invoke-static {v1, v0}, Lcom/android/settingslib/w;->cqW(Landroid/content/Context;Lcom/android/settingslib/n;)V

    :cond_0
    return-void
.end method
