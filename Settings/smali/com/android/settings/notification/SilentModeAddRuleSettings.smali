.class public Lcom/android/settings/notification/SilentModeAddRuleSettings;
.super Lcom/android/settings/notification/SilentModeRuleBaseSettings;
.source "SilentModeAddRuleSettings.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/notification/SilentModeRuleBaseSettings;-><init>()V

    return-void
.end method


# virtual methods
.method protected cU()V
    .locals 4

    const/4 v2, 0x1

    const/16 v0, 0x564

    iput v0, p0, Lcom/android/settings/notification/SilentModeAddRuleSettings;->ck:I

    const/16 v0, 0x1a4

    iput v0, p0, Lcom/android/settings/notification/SilentModeAddRuleSettings;->bW:I

    new-instance v0, Lcom/android/settings/dndmode/e;

    const/16 v1, 0x7f

    invoke-direct {v0, v1}, Lcom/android/settings/dndmode/e;-><init>(I)V

    iput-object v0, p0, Lcom/android/settings/notification/SilentModeAddRuleSettings;->bT:Lcom/android/settings/dndmode/e;

    iget-object v0, p0, Lcom/android/settings/notification/SilentModeAddRuleSettings;->bT:Lcom/android/settings/dndmode/e;

    iget-object v1, p0, Lcom/android/settings/notification/SilentModeAddRuleSettings;->bS:Landroid/app/Activity;

    invoke-virtual {v0, v1, v2}, Lcom/android/settings/dndmode/e;->mj(Landroid/content/Context;Z)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/notification/SilentModeAddRuleSettings;->bU:Ljava/lang/String;

    iput v2, p0, Lcom/android/settings/notification/SilentModeAddRuleSettings;->ca:I

    invoke-virtual {p0}, Lcom/android/settings/notification/SilentModeAddRuleSettings;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f12126d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-array v1, v2, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/android/settings/notification/SilentModeAddRuleSettings;->cw:Landroid/service/notification/ZenModeConfig;

    iget-object v2, v2, Landroid/service/notification/ZenModeConfig;->automaticRules:Landroid/util/ArrayMap;

    invoke-virtual {v2}, Landroid/util/ArrayMap;->size()I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/notification/SilentModeAddRuleSettings;->bY:Ljava/lang/String;

    return-void
.end method

.method public da()Z
    .locals 4

    const/4 v0, 0x1

    new-instance v1, Landroid/service/notification/ZenModeConfig$ZenRule;

    invoke-direct {v1}, Landroid/service/notification/ZenModeConfig$ZenRule;-><init>()V

    invoke-virtual {p0}, Lcom/android/settings/notification/SilentModeAddRuleSettings;->cP()Lcom/android/settings/notification/SilentModeRuleBaseSettings$RuleInfo;

    move-result-object v2

    invoke-virtual {p0}, Lcom/android/settings/notification/SilentModeAddRuleSettings;->cQ()Ljava/lang/String;

    move-result-object v3

    iput-object v3, v1, Landroid/service/notification/ZenModeConfig$ZenRule;->name:Ljava/lang/String;

    iput-boolean v0, v1, Landroid/service/notification/ZenModeConfig$ZenRule;->enabled:Z

    iget-object v3, p0, Lcom/android/settings/notification/SilentModeAddRuleSettings;->cj:Landroid/preference/ListPreference;

    invoke-virtual {v3}, Landroid/preference/ListPreference;->getValue()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    iput v3, v1, Landroid/service/notification/ZenModeConfig$ZenRule;->zenMode:I

    iget-object v3, v2, Lcom/android/settings/notification/SilentModeRuleBaseSettings$RuleInfo;->cn:Landroid/net/Uri;

    iput-object v3, v1, Landroid/service/notification/ZenModeConfig$ZenRule;->conditionId:Landroid/net/Uri;

    iget-object v2, v2, Lcom/android/settings/notification/SilentModeRuleBaseSettings$RuleInfo;->co:Landroid/content/ComponentName;

    iput-object v2, v1, Landroid/service/notification/ZenModeConfig$ZenRule;->component:Landroid/content/ComponentName;

    invoke-virtual {p0, v1}, Lcom/android/settings/notification/SilentModeAddRuleSettings;->cO(Landroid/service/notification/ZenModeConfig$ZenRule;)Landroid/app/AutomaticZenRule;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/android/settings/notification/SilentModeAddRuleSettings;->cN(Landroid/app/AutomaticZenRule;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
