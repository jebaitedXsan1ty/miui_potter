.class Lcom/android/settings/notification/MiuiZenModeSettings$UpdateVipLabelTask;
.super Landroid/os/AsyncTask;
.source "MiuiZenModeSettings.java"


# instance fields
.field final synthetic fn:Lcom/android/settings/notification/MiuiZenModeSettings;


# direct methods
.method private constructor <init>(Lcom/android/settings/notification/MiuiZenModeSettings;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/notification/MiuiZenModeSettings$UpdateVipLabelTask;->fn:Lcom/android/settings/notification/MiuiZenModeSettings;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/settings/notification/MiuiZenModeSettings;Lcom/android/settings/notification/MiuiZenModeSettings$UpdateVipLabelTask;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/settings/notification/MiuiZenModeSettings$UpdateVipLabelTask;-><init>(Lcom/android/settings/notification/MiuiZenModeSettings;)V

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/android/settings/notification/MiuiZenModeSettings$UpdateVipLabelTask;->doInBackground([Ljava/lang/Void;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/String;
    .locals 4

    invoke-virtual {p0}, Lcom/android/settings/notification/MiuiZenModeSettings$UpdateVipLabelTask;->isCancelled()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    return-object v0

    :cond_0
    sget-boolean v0, Landroid/provider/MiuiSettings$SilenceMode;->isSupported:Z

    if-eqz v0, :cond_1

    const v0, 0x7f0300ac

    :goto_0
    iget-object v1, p0, Lcom/android/settings/notification/MiuiZenModeSettings$UpdateVipLabelTask;->fn:Lcom/android/settings/notification/MiuiZenModeSettings;

    invoke-static {v1}, Lcom/android/settings/notification/MiuiZenModeSettings;->gu(Lcom/android/settings/notification/MiuiZenModeSettings;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    sget-boolean v1, Landroid/provider/MiuiSettings$SilenceMode;->isSupported:Z

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/android/settings/notification/MiuiZenModeSettings$UpdateVipLabelTask;->fn:Lcom/android/settings/notification/MiuiZenModeSettings;

    invoke-static {v1}, Lcom/android/settings/notification/MiuiZenModeSettings;->gt(Lcom/android/settings/notification/MiuiZenModeSettings;)Landroid/service/notification/ZenModeConfig;

    move-result-object v1

    iget v1, v1, Landroid/service/notification/ZenModeConfig;->allowCallsFrom:I

    add-int/lit8 v1, v1, -0x1

    aget-object v0, v0, v1

    :goto_1
    return-object v0

    :cond_1
    const v0, 0x7f03006f

    goto :goto_0

    :cond_2
    iget-object v1, p0, Lcom/android/settings/notification/MiuiZenModeSettings$UpdateVipLabelTask;->fn:Lcom/android/settings/notification/MiuiZenModeSettings;

    invoke-static {v1}, Lcom/android/settings/notification/MiuiZenModeSettings;->gt(Lcom/android/settings/notification/MiuiZenModeSettings;)Landroid/service/notification/ZenModeConfig;

    move-result-object v1

    iget v1, v1, Landroid/service/notification/ZenModeConfig;->allowCallsFrom:I

    const/4 v2, 0x3

    if-ne v1, v2, :cond_3

    iget-object v0, p0, Lcom/android/settings/notification/MiuiZenModeSettings$UpdateVipLabelTask;->fn:Lcom/android/settings/notification/MiuiZenModeSettings;

    invoke-static {v0}, Lcom/android/settings/notification/MiuiZenModeSettings;->gu(Lcom/android/settings/notification/MiuiZenModeSettings;)Landroid/content/Context;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/android/settings/notification/MiuiZenModeSettings$UpdateVipLabelTask;->fn:Lcom/android/settings/notification/MiuiZenModeSettings;

    invoke-static {v2}, Lcom/android/settings/notification/MiuiZenModeSettings;->gu(Lcom/android/settings/notification/MiuiZenModeSettings;)Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/android/settings/notification/MiuiZenModeSettings;->gn(Landroid/content/Context;)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    const v2, 0x7f12061f

    invoke-virtual {v0, v2, v1}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_3
    iget-object v1, p0, Lcom/android/settings/notification/MiuiZenModeSettings$UpdateVipLabelTask;->fn:Lcom/android/settings/notification/MiuiZenModeSettings;

    invoke-static {v1}, Lcom/android/settings/notification/MiuiZenModeSettings;->gt(Lcom/android/settings/notification/MiuiZenModeSettings;)Landroid/service/notification/ZenModeConfig;

    move-result-object v1

    iget v1, v1, Landroid/service/notification/ZenModeConfig;->allowCallsFrom:I

    aget-object v0, v0, v1

    goto :goto_1
.end method

.method protected gD(Ljava/lang/String;)V
    .locals 1

    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/android/settings/notification/MiuiZenModeSettings$UpdateVipLabelTask;->fn:Lcom/android/settings/notification/MiuiZenModeSettings;

    invoke-static {v0}, Lcom/android/settings/notification/MiuiZenModeSettings;->gw(Lcom/android/settings/notification/MiuiZenModeSettings;)Lcom/android/settings/dndmode/LabelPreference;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/android/settings/dndmode/LabelPreference;->mp(Ljava/lang/String;)V

    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/android/settings/notification/MiuiZenModeSettings$UpdateVipLabelTask;->gD(Ljava/lang/String;)V

    return-void
.end method
