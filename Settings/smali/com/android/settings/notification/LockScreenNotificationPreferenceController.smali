.class public Lcom/android/settings/notification/LockScreenNotificationPreferenceController;
.super Lcom/android/settings/core/e;
.source "LockScreenNotificationPreferenceController.java"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceChangeListener;
.implements Lcom/android/settings/core/lifecycle/b;
.implements Lcom/android/settings/core/lifecycle/a/b;
.implements Lcom/android/settings/core/lifecycle/a/d;


# instance fields
.field private f:Lcom/android/settings/notification/MiuiRestrictedDropDownPreference;

.field private g:Lcom/android/settings/notification/MiuiRestrictedDropDownPreference;

.field private h:I

.field private i:I

.field private final j:I

.field private final k:Z

.field private final l:Z

.field private m:Lcom/android/settings/notification/LockScreenNotificationPreferenceController$SettingObserver;

.field private final mSettingKey:Ljava/lang/String;

.field private final n:Ljava/lang/String;

.field private final o:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0, v0, v0}, Lcom/android/settings/notification/LockScreenNotificationPreferenceController;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 5

    const/4 v0, 0x0

    invoke-direct {p0, p1}, Lcom/android/settings/core/e;-><init>(Landroid/content/Context;)V

    iput-object p2, p0, Lcom/android/settings/notification/LockScreenNotificationPreferenceController;->mSettingKey:Ljava/lang/String;

    iput-object p3, p0, Lcom/android/settings/notification/LockScreenNotificationPreferenceController;->n:Ljava/lang/String;

    iput-object p4, p0, Lcom/android/settings/notification/LockScreenNotificationPreferenceController;->o:Ljava/lang/String;

    invoke-static {p1}, Landroid/os/UserManager;->get(Landroid/content/Context;)Landroid/os/UserManager;

    move-result-object v1

    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v2

    invoke-static {v1, v2}, Lcom/android/settings/aq;->bqt(Landroid/os/UserManager;I)I

    move-result v1

    iput v1, p0, Lcom/android/settings/notification/LockScreenNotificationPreferenceController;->j:I

    new-instance v1, Lcom/android/internal/widget/LockPatternUtils;

    invoke-direct {v1, p1}, Lcom/android/internal/widget/LockPatternUtils;-><init>(Landroid/content/Context;)V

    iget v2, p0, Lcom/android/settings/notification/LockScreenNotificationPreferenceController;->j:I

    invoke-virtual {v1, v2}, Lcom/android/internal/widget/LockPatternUtils;->isSeparateProfileChallengeEnabled(I)Z

    move-result v2

    xor-int/lit8 v2, v2, 0x1

    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v3

    invoke-virtual {v1, v3}, Lcom/android/internal/widget/LockPatternUtils;->isSecure(I)Z

    move-result v3

    iput-boolean v3, p0, Lcom/android/settings/notification/LockScreenNotificationPreferenceController;->k:Z

    iget v3, p0, Lcom/android/settings/notification/LockScreenNotificationPreferenceController;->j:I

    const/16 v4, -0x2710

    if-eq v3, v4, :cond_0

    iget v3, p0, Lcom/android/settings/notification/LockScreenNotificationPreferenceController;->j:I

    invoke-virtual {v1, v3}, Lcom/android/internal/widget/LockPatternUtils;->isSecure(I)Z

    move-result v1

    if-nez v1, :cond_1

    if-eqz v2, :cond_0

    iget-boolean v0, p0, Lcom/android/settings/notification/LockScreenNotificationPreferenceController;->k:Z

    :cond_0
    :goto_0
    iput-boolean v0, p0, Lcom/android/settings/notification/LockScreenNotificationPreferenceController;->l:Z

    return-void

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private j(I)Z
    .locals 3

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/android/settings/notification/LockScreenNotificationPreferenceController;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string/jumbo v2, "lock_screen_allow_private_notifications"

    invoke-static {v1, v2, v0, p1}, Landroid/provider/Settings$Secure;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method private k(I)Z
    .locals 3

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/android/settings/notification/LockScreenNotificationPreferenceController;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string/jumbo v2, "lock_screen_show_notifications"

    invoke-static {v1, v2, v0, p1}, Landroid/provider/Settings$Secure;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method private n()V
    .locals 6

    const v4, 0x7f120978

    const v5, 0x7f120976

    const v3, 0x7f120974

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iget-object v2, p0, Lcom/android/settings/notification/LockScreenNotificationPreferenceController;->mContext:Landroid/content/Context;

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v2, p0, Lcom/android/settings/notification/LockScreenNotificationPreferenceController;->mContext:Landroid/content/Context;

    invoke-virtual {v2, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/16 v4, 0xc

    invoke-direct {p0, v2, v3, v4}, Lcom/android/settings/notification/LockScreenNotificationPreferenceController;->q(Ljava/lang/CharSequence;Ljava/lang/CharSequence;I)V

    iget-boolean v2, p0, Lcom/android/settings/notification/LockScreenNotificationPreferenceController;->k:Z

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/android/settings/notification/LockScreenNotificationPreferenceController;->mContext:Landroid/content/Context;

    invoke-virtual {v2, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v5}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/4 v4, 0x4

    invoke-direct {p0, v2, v3, v4}, Lcom/android/settings/notification/LockScreenNotificationPreferenceController;->q(Ljava/lang/CharSequence;Ljava/lang/CharSequence;I)V

    :cond_0
    iget-object v2, p0, Lcom/android/settings/notification/LockScreenNotificationPreferenceController;->f:Lcom/android/settings/notification/MiuiRestrictedDropDownPreference;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v3

    new-array v3, v3, [Ljava/lang/CharSequence;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/CharSequence;

    invoke-virtual {v2, v0}, Lcom/android/settings/notification/MiuiRestrictedDropDownPreference;->setEntries([Ljava/lang/CharSequence;)V

    iget-object v2, p0, Lcom/android/settings/notification/LockScreenNotificationPreferenceController;->f:Lcom/android/settings/notification/MiuiRestrictedDropDownPreference;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v0, v0, [Ljava/lang/CharSequence;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/CharSequence;

    invoke-virtual {v2, v0}, Lcom/android/settings/notification/MiuiRestrictedDropDownPreference;->setEntryValues([Ljava/lang/CharSequence;)V

    invoke-direct {p0}, Lcom/android/settings/notification/LockScreenNotificationPreferenceController;->r()V

    iget-object v0, p0, Lcom/android/settings/notification/LockScreenNotificationPreferenceController;->f:Lcom/android/settings/notification/MiuiRestrictedDropDownPreference;

    invoke-virtual {v0}, Lcom/android/settings/notification/MiuiRestrictedDropDownPreference;->getEntries()[Ljava/lang/CharSequence;

    move-result-object v0

    array-length v0, v0

    const/4 v1, 0x1

    if-le v0, v1, :cond_1

    iget-object v0, p0, Lcom/android/settings/notification/LockScreenNotificationPreferenceController;->f:Lcom/android/settings/notification/MiuiRestrictedDropDownPreference;

    invoke-virtual {v0, p0}, Lcom/android/settings/notification/MiuiRestrictedDropDownPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/settings/notification/LockScreenNotificationPreferenceController;->f:Lcom/android/settings/notification/MiuiRestrictedDropDownPreference;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/android/settings/notification/MiuiRestrictedDropDownPreference;->setEnabled(Z)V

    goto :goto_0
.end method

.method private o()V
    .locals 6

    const v4, 0x7f120979

    const v5, 0x7f120977

    const v3, 0x7f120975

    iget-object v0, p0, Lcom/android/settings/notification/LockScreenNotificationPreferenceController;->g:Lcom/android/settings/notification/MiuiRestrictedDropDownPreference;

    if-nez v0, :cond_0

    const-string/jumbo v0, "LockScreenNotifPref"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Preference not found: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/settings/notification/LockScreenNotificationPreferenceController;->o:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iget-object v2, p0, Lcom/android/settings/notification/LockScreenNotificationPreferenceController;->mContext:Landroid/content/Context;

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v2, p0, Lcom/android/settings/notification/LockScreenNotificationPreferenceController;->mContext:Landroid/content/Context;

    invoke-virtual {v2, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/16 v4, 0xc

    invoke-direct {p0, v2, v3, v4}, Lcom/android/settings/notification/LockScreenNotificationPreferenceController;->q(Ljava/lang/CharSequence;Ljava/lang/CharSequence;I)V

    iget-boolean v2, p0, Lcom/android/settings/notification/LockScreenNotificationPreferenceController;->l:Z

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/android/settings/notification/LockScreenNotificationPreferenceController;->mContext:Landroid/content/Context;

    invoke-virtual {v2, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v5}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/4 v4, 0x4

    invoke-direct {p0, v2, v3, v4}, Lcom/android/settings/notification/LockScreenNotificationPreferenceController;->q(Ljava/lang/CharSequence;Ljava/lang/CharSequence;I)V

    :cond_1
    iget-object v2, p0, Lcom/android/settings/notification/LockScreenNotificationPreferenceController;->g:Lcom/android/settings/notification/MiuiRestrictedDropDownPreference;

    new-instance v3, Lcom/android/settings/notification/-$Lambda$vDj-466JyPhDhTUz7JYRoBi50lM;

    invoke-direct {v3, p0}, Lcom/android/settings/notification/-$Lambda$vDj-466JyPhDhTUz7JYRoBi50lM;-><init>(Ljava/lang/Object;)V

    invoke-virtual {v2, v3}, Lcom/android/settings/notification/MiuiRestrictedDropDownPreference;->aA(Landroid/preference/Preference$OnPreferenceClickListener;)V

    iget-object v2, p0, Lcom/android/settings/notification/LockScreenNotificationPreferenceController;->g:Lcom/android/settings/notification/MiuiRestrictedDropDownPreference;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v3

    new-array v3, v3, [Ljava/lang/CharSequence;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/CharSequence;

    invoke-virtual {v2, v0}, Lcom/android/settings/notification/MiuiRestrictedDropDownPreference;->setEntries([Ljava/lang/CharSequence;)V

    iget-object v2, p0, Lcom/android/settings/notification/LockScreenNotificationPreferenceController;->g:Lcom/android/settings/notification/MiuiRestrictedDropDownPreference;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v0, v0, [Ljava/lang/CharSequence;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/CharSequence;

    invoke-virtual {v2, v0}, Lcom/android/settings/notification/MiuiRestrictedDropDownPreference;->setEntryValues([Ljava/lang/CharSequence;)V

    invoke-direct {p0}, Lcom/android/settings/notification/LockScreenNotificationPreferenceController;->s()V

    iget-object v0, p0, Lcom/android/settings/notification/LockScreenNotificationPreferenceController;->g:Lcom/android/settings/notification/MiuiRestrictedDropDownPreference;

    invoke-virtual {v0}, Lcom/android/settings/notification/MiuiRestrictedDropDownPreference;->getEntries()[Ljava/lang/CharSequence;

    move-result-object v0

    array-length v0, v0

    const/4 v1, 0x1

    if-le v0, v1, :cond_2

    iget-object v0, p0, Lcom/android/settings/notification/LockScreenNotificationPreferenceController;->g:Lcom/android/settings/notification/MiuiRestrictedDropDownPreference;

    invoke-virtual {v0, p0}, Lcom/android/settings/notification/MiuiRestrictedDropDownPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    :goto_0
    return-void

    :cond_2
    iget-object v0, p0, Lcom/android/settings/notification/LockScreenNotificationPreferenceController;->g:Lcom/android/settings/notification/MiuiRestrictedDropDownPreference;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/android/settings/notification/MiuiRestrictedDropDownPreference;->setEnabled(Z)V

    goto :goto_0
.end method

.method private q(Ljava/lang/CharSequence;Ljava/lang/CharSequence;I)V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/notification/LockScreenNotificationPreferenceController;->mContext:Landroid/content/Context;

    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v1

    invoke-static {v0, p3, v1}, Lcom/android/settingslib/w;->cqR(Landroid/content/Context;II)Lcom/android/settingslib/n;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/android/settings/notification/LockScreenNotificationPreferenceController;->f:Lcom/android/settings/notification/MiuiRestrictedDropDownPreference;

    if-eqz v1, :cond_0

    new-instance v1, Lcom/android/settings/notification/MiuiRestrictedDropDownPreference$RestrictedItem;

    invoke-direct {v1, p1, p2, v0}, Lcom/android/settings/notification/MiuiRestrictedDropDownPreference$RestrictedItem;-><init>(Ljava/lang/CharSequence;Ljava/lang/CharSequence;Lcom/android/settingslib/n;)V

    iget-object v0, p0, Lcom/android/settings/notification/LockScreenNotificationPreferenceController;->f:Lcom/android/settings/notification/MiuiRestrictedDropDownPreference;

    invoke-virtual {v0, v1}, Lcom/android/settings/notification/MiuiRestrictedDropDownPreference;->aB(Lcom/android/settings/notification/MiuiRestrictedDropDownPreference$RestrictedItem;)V

    :cond_0
    iget v0, p0, Lcom/android/settings/notification/LockScreenNotificationPreferenceController;->j:I

    const/16 v1, -0x2710

    if-eq v0, v1, :cond_1

    iget-object v0, p0, Lcom/android/settings/notification/LockScreenNotificationPreferenceController;->mContext:Landroid/content/Context;

    iget v1, p0, Lcom/android/settings/notification/LockScreenNotificationPreferenceController;->j:I

    invoke-static {v0, p3, v1}, Lcom/android/settingslib/w;->cqR(Landroid/content/Context;II)Lcom/android/settingslib/n;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/android/settings/notification/LockScreenNotificationPreferenceController;->g:Lcom/android/settings/notification/MiuiRestrictedDropDownPreference;

    if-eqz v1, :cond_1

    new-instance v1, Lcom/android/settings/notification/MiuiRestrictedDropDownPreference$RestrictedItem;

    invoke-direct {v1, p1, p2, v0}, Lcom/android/settings/notification/MiuiRestrictedDropDownPreference$RestrictedItem;-><init>(Ljava/lang/CharSequence;Ljava/lang/CharSequence;Lcom/android/settingslib/n;)V

    iget-object v0, p0, Lcom/android/settings/notification/LockScreenNotificationPreferenceController;->g:Lcom/android/settings/notification/MiuiRestrictedDropDownPreference;

    invoke-virtual {v0, v1}, Lcom/android/settings/notification/MiuiRestrictedDropDownPreference;->aB(Lcom/android/settings/notification/MiuiRestrictedDropDownPreference$RestrictedItem;)V

    :cond_1
    return-void
.end method

.method private r()V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/notification/LockScreenNotificationPreferenceController;->f:Lcom/android/settings/notification/MiuiRestrictedDropDownPreference;

    if-nez v0, :cond_0

    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/android/settings/notification/LockScreenNotificationPreferenceController;->m()I

    move-result v0

    iput v0, p0, Lcom/android/settings/notification/LockScreenNotificationPreferenceController;->h:I

    iget-object v0, p0, Lcom/android/settings/notification/LockScreenNotificationPreferenceController;->f:Lcom/android/settings/notification/MiuiRestrictedDropDownPreference;

    const-string/jumbo v1, "%s"

    invoke-virtual {v0, v1}, Lcom/android/settings/notification/MiuiRestrictedDropDownPreference;->setSummary(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/android/settings/notification/LockScreenNotificationPreferenceController;->f:Lcom/android/settings/notification/MiuiRestrictedDropDownPreference;

    iget v1, p0, Lcom/android/settings/notification/LockScreenNotificationPreferenceController;->h:I

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/settings/notification/MiuiRestrictedDropDownPreference;->setValue(Ljava/lang/String;)V

    return-void
.end method

.method private s()V
    .locals 4

    iget v0, p0, Lcom/android/settings/notification/LockScreenNotificationPreferenceController;->j:I

    const/16 v1, -0x2710

    if-ne v0, v1, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/settings/notification/LockScreenNotificationPreferenceController;->g:Lcom/android/settings/notification/MiuiRestrictedDropDownPreference;

    if-nez v0, :cond_1

    return-void

    :cond_1
    iget v0, p0, Lcom/android/settings/notification/LockScreenNotificationPreferenceController;->j:I

    invoke-direct {p0, v0}, Lcom/android/settings/notification/LockScreenNotificationPreferenceController;->k(I)Z

    move-result v1

    iget-boolean v0, p0, Lcom/android/settings/notification/LockScreenNotificationPreferenceController;->l:Z

    if-eqz v0, :cond_2

    iget v0, p0, Lcom/android/settings/notification/LockScreenNotificationPreferenceController;->j:I

    invoke-direct {p0, v0}, Lcom/android/settings/notification/LockScreenNotificationPreferenceController;->j(I)Z

    move-result v0

    :goto_0
    iget-object v2, p0, Lcom/android/settings/notification/LockScreenNotificationPreferenceController;->g:Lcom/android/settings/notification/MiuiRestrictedDropDownPreference;

    const-string/jumbo v3, "%s"

    invoke-virtual {v2, v3}, Lcom/android/settings/notification/MiuiRestrictedDropDownPreference;->setSummary(Ljava/lang/CharSequence;)V

    if-nez v1, :cond_3

    const v0, 0x7f120975

    :goto_1
    iput v0, p0, Lcom/android/settings/notification/LockScreenNotificationPreferenceController;->i:I

    iget-object v0, p0, Lcom/android/settings/notification/LockScreenNotificationPreferenceController;->g:Lcom/android/settings/notification/MiuiRestrictedDropDownPreference;

    iget v1, p0, Lcom/android/settings/notification/LockScreenNotificationPreferenceController;->i:I

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/settings/notification/MiuiRestrictedDropDownPreference;->setValue(Ljava/lang/String;)V

    return-void

    :cond_2
    const/4 v0, 0x1

    goto :goto_0

    :cond_3
    if-eqz v0, :cond_4

    const v0, 0x7f120979

    goto :goto_1

    :cond_4
    const v0, 0x7f120977

    goto :goto_1
.end method

.method static synthetic u(Lcom/android/settings/notification/LockScreenNotificationPreferenceController;)I
    .locals 1

    iget v0, p0, Lcom/android/settings/notification/LockScreenNotificationPreferenceController;->j:I

    return v0
.end method

.method static synthetic v(Lcom/android/settings/notification/LockScreenNotificationPreferenceController;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/notification/LockScreenNotificationPreferenceController;->s()V

    return-void
.end method

.method static synthetic w(Lcom/android/settings/notification/LockScreenNotificationPreferenceController;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/notification/LockScreenNotificationPreferenceController;->r()V

    return-void
.end method


# virtual methods
.method public i(Landroid/preference/PreferenceScreen;)V
    .locals 3

    invoke-super {p0, p1}, Lcom/android/settings/core/e;->i(Landroid/preference/PreferenceScreen;)V

    iget-object v0, p0, Lcom/android/settings/notification/LockScreenNotificationPreferenceController;->mSettingKey:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Lcom/android/settings/notification/MiuiRestrictedDropDownPreference;

    iput-object v0, p0, Lcom/android/settings/notification/LockScreenNotificationPreferenceController;->f:Lcom/android/settings/notification/MiuiRestrictedDropDownPreference;

    iget-object v0, p0, Lcom/android/settings/notification/LockScreenNotificationPreferenceController;->f:Lcom/android/settings/notification/MiuiRestrictedDropDownPreference;

    if-nez v0, :cond_0

    const-string/jumbo v0, "LockScreenNotifPref"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Preference not found: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/settings/notification/LockScreenNotificationPreferenceController;->mSettingKey:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :cond_0
    iget v0, p0, Lcom/android/settings/notification/LockScreenNotificationPreferenceController;->j:I

    const/16 v1, -0x2710

    if-eq v0, v1, :cond_1

    iget-object v0, p0, Lcom/android/settings/notification/LockScreenNotificationPreferenceController;->o:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Lcom/android/settings/notification/MiuiRestrictedDropDownPreference;

    iput-object v0, p0, Lcom/android/settings/notification/LockScreenNotificationPreferenceController;->g:Lcom/android/settings/notification/MiuiRestrictedDropDownPreference;

    :goto_0
    new-instance v0, Lcom/android/settings/notification/LockScreenNotificationPreferenceController$SettingObserver;

    invoke-direct {v0, p0}, Lcom/android/settings/notification/LockScreenNotificationPreferenceController$SettingObserver;-><init>(Lcom/android/settings/notification/LockScreenNotificationPreferenceController;)V

    iput-object v0, p0, Lcom/android/settings/notification/LockScreenNotificationPreferenceController;->m:Lcom/android/settings/notification/LockScreenNotificationPreferenceController$SettingObserver;

    invoke-direct {p0}, Lcom/android/settings/notification/LockScreenNotificationPreferenceController;->n()V

    invoke-direct {p0}, Lcom/android/settings/notification/LockScreenNotificationPreferenceController;->o()V

    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/settings/notification/LockScreenNotificationPreferenceController;->o:Ljava/lang/String;

    invoke-virtual {p0, p1, v0}, Lcom/android/settings/notification/LockScreenNotificationPreferenceController;->cdK(Landroid/preference/PreferenceScreen;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/settings/notification/LockScreenNotificationPreferenceController;->n:Ljava/lang/String;

    invoke-virtual {p0, p1, v0}, Lcom/android/settings/notification/LockScreenNotificationPreferenceController;->cdK(Landroid/preference/PreferenceScreen;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public l()Ljava/lang/String;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public m()I
    .locals 2

    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/android/settings/notification/LockScreenNotificationPreferenceController;->k(I)Z

    move-result v1

    iget-boolean v0, p0, Lcom/android/settings/notification/LockScreenNotificationPreferenceController;->k:Z

    if-eqz v0, :cond_0

    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/android/settings/notification/LockScreenNotificationPreferenceController;->j(I)Z

    move-result v0

    :goto_0
    if-nez v1, :cond_1

    const v0, 0x7f120974

    :goto_1
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    if-eqz v0, :cond_2

    const v0, 0x7f120978

    goto :goto_1

    :cond_2
    const v0, 0x7f120976

    goto :goto_1
.end method

.method public onPause()V
    .locals 3

    iget-object v0, p0, Lcom/android/settings/notification/LockScreenNotificationPreferenceController;->m:Lcom/android/settings/notification/LockScreenNotificationPreferenceController$SettingObserver;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/notification/LockScreenNotificationPreferenceController;->m:Lcom/android/settings/notification/LockScreenNotificationPreferenceController$SettingObserver;

    iget-object v1, p0, Lcom/android/settings/notification/LockScreenNotificationPreferenceController;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/android/settings/notification/LockScreenNotificationPreferenceController$SettingObserver;->x(Landroid/content/ContentResolver;Z)V

    :cond_0
    return-void
.end method

.method public onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 8

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v0

    iget-object v3, p0, Lcom/android/settings/notification/LockScreenNotificationPreferenceController;->o:Ljava/lang/String;

    invoke-static {v3, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_5

    check-cast p2, Ljava/lang/String;

    invoke-static {p2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    iget v0, p0, Lcom/android/settings/notification/LockScreenNotificationPreferenceController;->i:I

    if-ne v4, v0, :cond_0

    return v2

    :cond_0
    const v0, 0x7f120975

    if-eq v4, v0, :cond_2

    move v3, v1

    :goto_0
    const v0, 0x7f120979

    if-ne v4, v0, :cond_3

    move v0, v1

    :goto_1
    iget-object v5, p0, Lcom/android/settings/notification/LockScreenNotificationPreferenceController;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    const-string/jumbo v6, "lock_screen_allow_private_notifications"

    if-eqz v0, :cond_4

    move v0, v1

    :goto_2
    iget v7, p0, Lcom/android/settings/notification/LockScreenNotificationPreferenceController;->j:I

    invoke-static {v5, v6, v0, v7}, Landroid/provider/Settings$Secure;->putIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)Z

    iget-object v0, p0, Lcom/android/settings/notification/LockScreenNotificationPreferenceController;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v5, "lock_screen_show_notifications"

    if-eqz v3, :cond_1

    move v2, v1

    :cond_1
    iget v3, p0, Lcom/android/settings/notification/LockScreenNotificationPreferenceController;->j:I

    invoke-static {v0, v5, v2, v3}, Landroid/provider/Settings$Secure;->putIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)Z

    iput v4, p0, Lcom/android/settings/notification/LockScreenNotificationPreferenceController;->i:I

    return v1

    :cond_2
    move v3, v2

    goto :goto_0

    :cond_3
    move v0, v2

    goto :goto_1

    :cond_4
    move v0, v2

    goto :goto_2

    :cond_5
    iget-object v3, p0, Lcom/android/settings/notification/LockScreenNotificationPreferenceController;->mSettingKey:Ljava/lang/String;

    invoke-static {v3, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_b

    check-cast p2, Ljava/lang/String;

    invoke-static {p2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    iget v0, p0, Lcom/android/settings/notification/LockScreenNotificationPreferenceController;->h:I

    if-ne v4, v0, :cond_6

    return v2

    :cond_6
    const v0, 0x7f120974

    if-eq v4, v0, :cond_8

    move v3, v1

    :goto_3
    const v0, 0x7f120978

    if-ne v4, v0, :cond_9

    move v0, v1

    :goto_4
    iget-object v5, p0, Lcom/android/settings/notification/LockScreenNotificationPreferenceController;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    const-string/jumbo v6, "lock_screen_allow_private_notifications"

    if-eqz v0, :cond_a

    move v0, v1

    :goto_5
    invoke-static {v5, v6, v0}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    iget-object v0, p0, Lcom/android/settings/notification/LockScreenNotificationPreferenceController;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v5, "lock_screen_show_notifications"

    if-eqz v3, :cond_7

    move v2, v1

    :cond_7
    invoke-static {v0, v5, v2}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    iput v4, p0, Lcom/android/settings/notification/LockScreenNotificationPreferenceController;->h:I

    return v1

    :cond_8
    move v3, v2

    goto :goto_3

    :cond_9
    move v0, v2

    goto :goto_4

    :cond_a
    move v0, v2

    goto :goto_5

    :cond_b
    return v2
.end method

.method public onResume()V
    .locals 3

    iget-object v0, p0, Lcom/android/settings/notification/LockScreenNotificationPreferenceController;->m:Lcom/android/settings/notification/LockScreenNotificationPreferenceController$SettingObserver;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/notification/LockScreenNotificationPreferenceController;->m:Lcom/android/settings/notification/LockScreenNotificationPreferenceController$SettingObserver;

    iget-object v1, p0, Lcom/android/settings/notification/LockScreenNotificationPreferenceController;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/android/settings/notification/LockScreenNotificationPreferenceController$SettingObserver;->x(Landroid/content/ContentResolver;Z)V

    :cond_0
    return-void
.end method

.method public p()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method synthetic t(Landroid/preference/Preference;)Z
    .locals 3

    iget-object v0, p0, Lcom/android/settings/notification/LockScreenNotificationPreferenceController;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/android/settings/notification/LockScreenNotificationPreferenceController;->mContext:Landroid/content/Context;

    invoke-static {v1}, Landroid/os/UserManager;->get(Landroid/content/Context;)Landroid/os/UserManager;

    move-result-object v1

    iget v2, p0, Lcom/android/settings/notification/LockScreenNotificationPreferenceController;->j:I

    invoke-static {v0, v1, v2}, Lcom/android/settings/aq;->bqu(Landroid/content/Context;Landroid/os/UserManager;I)Z

    move-result v0

    return v0
.end method
