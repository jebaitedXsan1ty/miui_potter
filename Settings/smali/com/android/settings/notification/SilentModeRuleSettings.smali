.class public Lcom/android/settings/notification/SilentModeRuleSettings;
.super Lmiui/app/Activity;
.source "SilentModeRuleSettings.java"


# instance fields
.field private d:Lcom/android/settings/notification/SilentModeAddRuleSettings;

.field private e:Lcom/android/settings/notification/SilentModeEditRuleSettings;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lmiui/app/Activity;-><init>()V

    return-void
.end method

.method private f(Lmiui/app/ActionBar;I)V
    .locals 4

    const v0, 0x1020016

    invoke-virtual {p1}, Lmiui/app/ActionBar;->getCustomView()Landroid/view/View;

    move-result-object v1

    packed-switch p2, :pswitch_data_0

    :goto_0
    const v0, 0x1020019

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const/high16 v2, 0x1040000

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(I)V

    new-instance v2, Lcom/android/settings/notification/SilentModeRuleSettings$1;

    invoke-direct {v2, p0}, Lcom/android/settings/notification/SilentModeRuleSettings$1;-><init>(Lcom/android/settings/notification/SilentModeRuleSettings;)V

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x102001a

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const v1, 0x104000a

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    new-instance v1, Lcom/android/settings/notification/SilentModeRuleSettings$2;

    invoke-direct {v1, p0, p2}, Lcom/android/settings/notification/SilentModeRuleSettings$2;-><init>(Lcom/android/settings/notification/SilentModeRuleSettings;I)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void

    :pswitch_0
    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/android/settings/notification/SilentModeRuleSettings;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f1200b4

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    :pswitch_1
    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/android/settings/notification/SilentModeRuleSettings;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f120661

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method static synthetic g(Lcom/android/settings/notification/SilentModeRuleSettings;)Lcom/android/settings/notification/SilentModeAddRuleSettings;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/notification/SilentModeRuleSettings;->d:Lcom/android/settings/notification/SilentModeAddRuleSettings;

    return-object v0
.end method

.method static synthetic h(Lcom/android/settings/notification/SilentModeRuleSettings;)Lcom/android/settings/notification/SilentModeEditRuleSettings;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/notification/SilentModeRuleSettings;->e:Lcom/android/settings/notification/SilentModeEditRuleSettings;

    return-object v0
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4

    const v3, 0x7f0a01af

    invoke-super {p0, p1}, Lmiui/app/Activity;->onCreate(Landroid/os/Bundle;)V

    const v0, 0x7f0d009f

    invoke-virtual {p0, v0}, Lcom/android/settings/notification/SilentModeRuleSettings;->setContentView(I)V

    invoke-virtual {p0}, Lcom/android/settings/notification/SilentModeRuleSettings;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "mode"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    invoke-virtual {p0}, Lcom/android/settings/notification/SilentModeRuleSettings;->getActionBar()Lmiui/app/ActionBar;

    move-result-object v1

    invoke-direct {p0, v1, v0}, Lcom/android/settings/notification/SilentModeRuleSettings;->f(Lmiui/app/ActionBar;I)V

    if-nez p1, :cond_0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    new-instance v0, Lcom/android/settings/notification/SilentModeAddRuleSettings;

    invoke-direct {v0}, Lcom/android/settings/notification/SilentModeAddRuleSettings;-><init>()V

    iput-object v0, p0, Lcom/android/settings/notification/SilentModeRuleSettings;->d:Lcom/android/settings/notification/SilentModeAddRuleSettings;

    invoke-virtual {p0}, Lcom/android/settings/notification/SilentModeRuleSettings;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/notification/SilentModeRuleSettings;->d:Lcom/android/settings/notification/SilentModeAddRuleSettings;

    invoke-virtual {v0, v3, v1}, Landroid/app/FragmentTransaction;->add(ILandroid/app/Fragment;)Landroid/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentTransaction;->commit()I

    :cond_0
    :goto_0
    return-void

    :cond_1
    new-instance v0, Lcom/android/settings/notification/SilentModeEditRuleSettings;

    invoke-direct {v0}, Lcom/android/settings/notification/SilentModeEditRuleSettings;-><init>()V

    iput-object v0, p0, Lcom/android/settings/notification/SilentModeRuleSettings;->e:Lcom/android/settings/notification/SilentModeEditRuleSettings;

    invoke-virtual {p0}, Lcom/android/settings/notification/SilentModeRuleSettings;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/notification/SilentModeRuleSettings;->e:Lcom/android/settings/notification/SilentModeEditRuleSettings;

    invoke-virtual {v0, v3, v1}, Landroid/app/FragmentTransaction;->add(ILandroid/app/Fragment;)Landroid/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentTransaction;->commit()I

    goto :goto_0
.end method
