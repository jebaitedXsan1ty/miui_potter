.class final Lcom/android/settings/notification/MiuiSilentSettingsFragment$3;
.super Ljava/lang/Object;
.source "MiuiSilentSettingsFragment.java"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceChangeListener;


# instance fields
.field final synthetic gS:Lcom/android/settings/notification/MiuiSilentSettingsFragment;


# direct methods
.method constructor <init>(Lcom/android/settings/notification/MiuiSilentSettingsFragment;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/notification/MiuiSilentSettingsFragment$3;->gS:Lcom/android/settings/notification/MiuiSilentSettingsFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 4

    const/4 v1, 0x0

    check-cast p2, Ljava/lang/String;

    invoke-static {p2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    sget v0, Landroid/service/notification/ZenModeConfig;->MAX_SOURCE:I

    if-gt v2, v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    iget-object v3, p0, Lcom/android/settings/notification/MiuiSilentSettingsFragment$3;->gS:Lcom/android/settings/notification/MiuiSilentSettingsFragment;

    invoke-static {v3}, Lcom/android/settings/notification/MiuiSilentSettingsFragment;->cG(Lcom/android/settings/notification/MiuiSilentSettingsFragment;)Landroid/content/Context;

    move-result-object v3

    invoke-static {v3, v0}, Landroid/provider/MiuiSettings$SilenceMode;->enableVIPMode(Landroid/content/Context;Z)V

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/notification/MiuiSilentSettingsFragment$3;->gS:Lcom/android/settings/notification/MiuiSilentSettingsFragment;

    invoke-static {v0}, Lcom/android/settings/notification/MiuiSilentSettingsFragment;->cG(Lcom/android/settings/notification/MiuiSilentSettingsFragment;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/app/ExtraNotificationManager;->getZenModeConfig(Landroid/content/Context;)Landroid/service/notification/ZenModeConfig;

    move-result-object v0

    iput v2, v0, Landroid/service/notification/ZenModeConfig;->allowCallsFrom:I

    iput v2, v0, Landroid/service/notification/ZenModeConfig;->allowMessagesFrom:I

    iget-object v2, p0, Lcom/android/settings/notification/MiuiSilentSettingsFragment$3;->gS:Lcom/android/settings/notification/MiuiSilentSettingsFragment;

    invoke-static {v2}, Lcom/android/settings/notification/MiuiSilentSettingsFragment;->cG(Lcom/android/settings/notification/MiuiSilentSettingsFragment;)Landroid/content/Context;

    move-result-object v2

    invoke-static {v2, v0}, Landroid/app/ExtraNotificationManager;->setZenModeConfig(Landroid/content/Context;Landroid/service/notification/ZenModeConfig;)Z

    :cond_0
    iget-object v0, p0, Lcom/android/settings/notification/MiuiSilentSettingsFragment$3;->gS:Lcom/android/settings/notification/MiuiSilentSettingsFragment;

    invoke-static {v0}, Lcom/android/settings/notification/MiuiSilentSettingsFragment;->cK(Lcom/android/settings/notification/MiuiSilentSettingsFragment;)V

    return v1

    :cond_1
    move v0, v1

    goto :goto_0
.end method
