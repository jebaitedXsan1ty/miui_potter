.class public abstract Lcom/android/settings/notification/SilentModeSettingsBase;
.super Lcom/android/settings/dndmode/MiuiPreferenceFragment;
.source "SilentModeSettingsBase.java"


# static fields
.field protected static final cx:Ljava/util/Comparator;


# instance fields
.field protected cA:I

.field protected cw:Landroid/service/notification/ZenModeConfig;

.field private final cy:Landroid/os/Handler;

.field private final cz:Lcom/android/settings/notification/SilentModeSettingsBase$SettingsObserver;

.field protected mContentResolver:Landroid/content/ContentResolver;

.field protected mContext:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/android/settings/notification/SilentModeSettingsBase$1;

    invoke-direct {v0}, Lcom/android/settings/notification/SilentModeSettingsBase$1;-><init>()V

    sput-object v0, Lcom/android/settings/notification/SilentModeSettingsBase;->cx:Ljava/util/Comparator;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Lcom/android/settings/dndmode/MiuiPreferenceFragment;-><init>()V

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/android/settings/notification/SilentModeSettingsBase;->cy:Landroid/os/Handler;

    new-instance v0, Lcom/android/settings/notification/SilentModeSettingsBase$SettingsObserver;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/android/settings/notification/SilentModeSettingsBase$SettingsObserver;-><init>(Lcom/android/settings/notification/SilentModeSettingsBase;Lcom/android/settings/notification/SilentModeSettingsBase$SettingsObserver;)V

    iput-object v0, p0, Lcom/android/settings/notification/SilentModeSettingsBase;->cz:Lcom/android/settings/notification/SilentModeSettingsBase$SettingsObserver;

    return-void
.end method

.method private dh()Ljava/util/Set;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/notification/SilentModeSettingsBase;->mContext:Landroid/content/Context;

    invoke-static {v0}, Landroid/app/NotificationManager;->from(Landroid/content/Context;)Landroid/app/NotificationManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/NotificationManager;->getAutomaticZenRules()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method private dk(Z)V
    .locals 3

    invoke-virtual {p0}, Lcom/android/settings/notification/SilentModeSettingsBase;->dg()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v1, "zen_mode"

    iget v2, p0, Lcom/android/settings/notification/SilentModeSettingsBase;->cA:I

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    iget v1, p0, Lcom/android/settings/notification/SilentModeSettingsBase;->cA:I

    if-ne v0, v1, :cond_0

    return-void

    :cond_0
    iput v0, p0, Lcom/android/settings/notification/SilentModeSettingsBase;->cA:I

    const-string/jumbo v0, "ZenModeSettings"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "updateZenMode mZenMode="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/android/settings/notification/SilentModeSettingsBase;->cA:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    if-eqz p1, :cond_1

    invoke-virtual {p0}, Lcom/android/settings/notification/SilentModeSettingsBase;->cV()V

    :cond_1
    return-void
.end method

.method private dl(Z)V
    .locals 3

    iget-object v0, p0, Lcom/android/settings/notification/SilentModeSettingsBase;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/settings/notification/SilentModeUtils;->dL(Landroid/content/Context;)Landroid/service/notification/ZenModeConfig;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/notification/SilentModeSettingsBase;->cw:Landroid/service/notification/ZenModeConfig;

    invoke-static {v0, v1}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    return-void

    :cond_0
    iput-object v0, p0, Lcom/android/settings/notification/SilentModeSettingsBase;->cw:Landroid/service/notification/ZenModeConfig;

    const-string/jumbo v0, "ZenModeSettings"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "updateZenModeConfig mConfig="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/settings/notification/SilentModeSettingsBase;->cw:Landroid/service/notification/ZenModeConfig;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    if-eqz p1, :cond_1

    invoke-virtual {p0}, Lcom/android/settings/notification/SilentModeSettingsBase;->cW()V

    :cond_1
    return-void
.end method

.method static synthetic dm(Lcom/android/settings/notification/SilentModeSettingsBase;)Landroid/os/Handler;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/notification/SilentModeSettingsBase;->cy:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic dn(Lcom/android/settings/notification/SilentModeSettingsBase;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/settings/notification/SilentModeSettingsBase;->dl(Z)V

    return-void
.end method

.method static synthetic do(Lcom/android/settings/notification/SilentModeSettingsBase;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/settings/notification/SilentModeSettingsBase;->dk(Z)V

    return-void
.end method


# virtual methods
.method protected cS(ZZ)V
    .locals 4

    if-eqz p1, :cond_0

    invoke-direct {p0}, Lcom/android/settings/notification/SilentModeSettingsBase;->dh()Ljava/util/Set;

    move-result-object v0

    const-string/jumbo v1, "ZenModeSettings"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "Refreshed mRules="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    if-eqz p2, :cond_0

    invoke-virtual {p0}, Lcom/android/settings/notification/SilentModeSettingsBase;->cW()V

    :cond_0
    return-void
.end method

.method protected abstract cV()V
.end method

.method protected abstract cW()V
.end method

.method protected dg()Landroid/content/ContentResolver;
    .locals 1

    invoke-virtual {p0}, Lcom/android/settings/notification/SilentModeSettingsBase;->getActivity()Landroid/app/Activity;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/notification/SilentModeSettingsBase;->mContentResolver:Landroid/content/ContentResolver;

    :cond_0
    iget-object v0, p0, Lcom/android/settings/notification/SilentModeSettingsBase;->mContentResolver:Landroid/content/ContentResolver;

    return-object v0
.end method

.method protected di(Ljava/lang/String;)Z
    .locals 2

    iget-object v0, p0, Lcom/android/settings/notification/SilentModeSettingsBase;->mContext:Landroid/content/Context;

    invoke-static {v0}, Landroid/app/NotificationManager;->from(Landroid/content/Context;)Landroid/app/NotificationManager;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/app/NotificationManager;->removeAutomaticZenRule(Ljava/lang/String;)Z

    move-result v0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/android/settings/notification/SilentModeSettingsBase;->cS(ZZ)V

    return v0
.end method

.method protected dj()[Lcom/android/settings/notification/SilentModeSettingsBase$ZenRuleInfo;
    .locals 4

    iget-object v0, p0, Lcom/android/settings/notification/SilentModeSettingsBase;->cw:Landroid/service/notification/ZenModeConfig;

    iget-object v0, v0, Landroid/service/notification/ZenModeConfig;->automaticRules:Landroid/util/ArrayMap;

    invoke-virtual {v0}, Landroid/util/ArrayMap;->size()I

    move-result v0

    new-array v2, v0, [Lcom/android/settings/notification/SilentModeSettingsBase$ZenRuleInfo;

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    array-length v0, v2

    if-ge v1, v0, :cond_0

    new-instance v3, Lcom/android/settings/notification/SilentModeSettingsBase$ZenRuleInfo;

    invoke-direct {v3}, Lcom/android/settings/notification/SilentModeSettingsBase$ZenRuleInfo;-><init>()V

    iget-object v0, p0, Lcom/android/settings/notification/SilentModeSettingsBase;->cw:Landroid/service/notification/ZenModeConfig;

    iget-object v0, v0, Landroid/service/notification/ZenModeConfig;->automaticRules:Landroid/util/ArrayMap;

    invoke-virtual {v0, v1}, Landroid/util/ArrayMap;->keyAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lcom/android/settings/notification/SilentModeSettingsBase$ZenRuleInfo;->id:Ljava/lang/String;

    iget-object v0, p0, Lcom/android/settings/notification/SilentModeSettingsBase;->cw:Landroid/service/notification/ZenModeConfig;

    iget-object v0, v0, Landroid/service/notification/ZenModeConfig;->automaticRules:Landroid/util/ArrayMap;

    invoke-virtual {v0, v1}, Landroid/util/ArrayMap;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/service/notification/ZenModeConfig$ZenRule;

    iput-object v0, v3, Lcom/android/settings/notification/SilentModeSettingsBase$ZenRuleInfo;->cE:Landroid/service/notification/ZenModeConfig$ZenRule;

    aput-object v3, v2, v1

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    sget-object v0, Lcom/android/settings/notification/SilentModeSettingsBase;->cx:Ljava/util/Comparator;

    invoke-static {v2, v0}, Ljava/util/Arrays;->sort([Ljava/lang/Object;Ljava/util/Comparator;)V

    return-object v2
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3

    const/4 v1, 0x0

    invoke-super {p0, p1}, Lcom/android/settings/dndmode/MiuiPreferenceFragment;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/android/settings/notification/SilentModeSettingsBase;->getActivity()Landroid/app/Activity;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/notification/SilentModeSettingsBase;->mContext:Landroid/content/Context;

    invoke-direct {p0, v1}, Lcom/android/settings/notification/SilentModeSettingsBase;->dl(Z)V

    invoke-direct {p0, v1}, Lcom/android/settings/notification/SilentModeSettingsBase;->dk(Z)V

    const-string/jumbo v0, "ZenModeSettings"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Loaded mConfig="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/settings/notification/SilentModeSettingsBase;->cw:Landroid/service/notification/ZenModeConfig;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public onPause()V
    .locals 1

    invoke-super {p0}, Lcom/android/settings/dndmode/MiuiPreferenceFragment;->onPause()V

    iget-object v0, p0, Lcom/android/settings/notification/SilentModeSettingsBase;->cz:Lcom/android/settings/notification/SilentModeSettingsBase$SettingsObserver;

    invoke-virtual {v0}, Lcom/android/settings/notification/SilentModeSettingsBase$SettingsObserver;->dq()V

    return-void
.end method

.method public onResume()V
    .locals 1

    const/4 v0, 0x1

    invoke-super {p0}, Lcom/android/settings/dndmode/MiuiPreferenceFragment;->onResume()V

    invoke-direct {p0, v0}, Lcom/android/settings/notification/SilentModeSettingsBase;->dk(Z)V

    invoke-direct {p0, v0}, Lcom/android/settings/notification/SilentModeSettingsBase;->dl(Z)V

    iget-object v0, p0, Lcom/android/settings/notification/SilentModeSettingsBase;->cz:Lcom/android/settings/notification/SilentModeSettingsBase$SettingsObserver;

    invoke-virtual {v0}, Lcom/android/settings/notification/SilentModeSettingsBase$SettingsObserver;->dp()V

    return-void
.end method
