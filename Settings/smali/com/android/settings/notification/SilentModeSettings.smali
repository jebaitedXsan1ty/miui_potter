.class public Lcom/android/settings/notification/SilentModeSettings;
.super Lmiui/app/Activity;
.source "SilentModeSettings.java"


# instance fields
.field private fy:Lcom/android/settings/notification/SilentModeAutomationSettings;

.field private fz:Lcom/android/settings/notification/SilentModeDeleteSettings;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lmiui/app/Activity;-><init>()V

    return-void
.end method

.method private gP(Landroid/app/ActionBar;)V
    .locals 4

    const/16 v2, 0x10

    const/4 v3, -0x2

    new-instance v0, Landroid/widget/Button;

    invoke-direct {v0, p0}, Landroid/widget/Button;-><init>(Landroid/content/Context;)V

    const v1, 0x7f080066

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setBackgroundResource(I)V

    new-instance v1, Lcom/android/settings/notification/SilentModeSettings$1;

    invoke-direct {v1, p0}, Lcom/android/settings/notification/SilentModeSettings$1;-><init>(Lcom/android/settings/notification/SilentModeSettings;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {p1, v2, v2}, Landroid/app/ActionBar;->setDisplayOptions(II)V

    new-instance v1, Landroid/app/ActionBar$LayoutParams;

    const v2, 0x800015

    invoke-direct {v1, v3, v3, v2}, Landroid/app/ActionBar$LayoutParams;-><init>(III)V

    invoke-virtual {p1, v0, v1}, Landroid/app/ActionBar;->setCustomView(Landroid/view/View;Landroid/app/ActionBar$LayoutParams;)V

    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3

    invoke-super {p0, p1}, Lmiui/app/Activity;->onCreate(Landroid/os/Bundle;)V

    const v0, 0x7f0d009f

    invoke-virtual {p0, v0}, Lcom/android/settings/notification/SilentModeSettings;->setContentView(I)V

    new-instance v0, Lcom/android/settings/notification/SilentModeAutomationSettings;

    invoke-direct {v0}, Lcom/android/settings/notification/SilentModeAutomationSettings;-><init>()V

    iput-object v0, p0, Lcom/android/settings/notification/SilentModeSettings;->fy:Lcom/android/settings/notification/SilentModeAutomationSettings;

    new-instance v0, Lcom/android/settings/notification/SilentModeDeleteSettings;

    invoke-direct {v0}, Lcom/android/settings/notification/SilentModeDeleteSettings;-><init>()V

    iput-object v0, p0, Lcom/android/settings/notification/SilentModeSettings;->fz:Lcom/android/settings/notification/SilentModeDeleteSettings;

    invoke-virtual {p0}, Lcom/android/settings/notification/SilentModeSettings;->getActionBar()Lmiui/app/ActionBar;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/android/settings/notification/SilentModeSettings;->gP(Landroid/app/ActionBar;)V

    if-nez p1, :cond_0

    invoke-virtual {p0}, Lcom/android/settings/notification/SilentModeSettings;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/notification/SilentModeSettings;->fy:Lcom/android/settings/notification/SilentModeAutomationSettings;

    const v2, 0x7f0a01af

    invoke-virtual {v0, v2, v1}, Landroid/app/FragmentTransaction;->add(ILandroid/app/Fragment;)Landroid/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentTransaction;->commit()I

    :cond_0
    return-void
.end method
