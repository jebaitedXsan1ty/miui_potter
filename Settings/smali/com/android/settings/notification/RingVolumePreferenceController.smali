.class public Lcom/android/settings/notification/RingVolumePreferenceController;
.super Lcom/android/settings/notification/VolumeSeekBarPreferenceController;
.source "RingVolumePreferenceController.java"


# instance fields
.field private ao:Landroid/media/AudioManager;

.field private final ap:Lcom/android/settings/notification/RingVolumePreferenceController$H;

.field private aq:Lcom/android/settings/notification/AudioHelper;

.field private final ar:Lcom/android/settings/notification/RingVolumePreferenceController$RingReceiver;

.field private as:I

.field private at:Landroid/content/ComponentName;

.field private au:Landroid/os/Vibrator;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/android/settings/notification/VolumeSeekBarPreference$Callback;Lcom/android/settings/core/lifecycle/a;)V
    .locals 1

    new-instance v0, Lcom/android/settings/notification/AudioHelper;

    invoke-direct {v0, p1}, Lcom/android/settings/notification/AudioHelper;-><init>(Landroid/content/Context;)V

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/android/settings/notification/RingVolumePreferenceController;-><init>(Landroid/content/Context;Lcom/android/settings/notification/VolumeSeekBarPreference$Callback;Lcom/android/settings/core/lifecycle/a;Lcom/android/settings/notification/AudioHelper;)V

    return-void
.end method

.method constructor <init>(Landroid/content/Context;Lcom/android/settings/notification/VolumeSeekBarPreference$Callback;Lcom/android/settings/core/lifecycle/a;Lcom/android/settings/notification/AudioHelper;)V
    .locals 3

    const/4 v2, 0x0

    invoke-direct {p0, p1, p2, p3}, Lcom/android/settings/notification/VolumeSeekBarPreferenceController;-><init>(Landroid/content/Context;Lcom/android/settings/notification/VolumeSeekBarPreference$Callback;Lcom/android/settings/core/lifecycle/a;)V

    const/4 v0, -0x1

    iput v0, p0, Lcom/android/settings/notification/RingVolumePreferenceController;->as:I

    new-instance v0, Lcom/android/settings/notification/RingVolumePreferenceController$RingReceiver;

    invoke-direct {v0, p0, v2}, Lcom/android/settings/notification/RingVolumePreferenceController$RingReceiver;-><init>(Lcom/android/settings/notification/RingVolumePreferenceController;Lcom/android/settings/notification/RingVolumePreferenceController$RingReceiver;)V

    iput-object v0, p0, Lcom/android/settings/notification/RingVolumePreferenceController;->ar:Lcom/android/settings/notification/RingVolumePreferenceController$RingReceiver;

    new-instance v0, Lcom/android/settings/notification/RingVolumePreferenceController$H;

    invoke-direct {v0, p0, v2}, Lcom/android/settings/notification/RingVolumePreferenceController$H;-><init>(Lcom/android/settings/notification/RingVolumePreferenceController;Lcom/android/settings/notification/RingVolumePreferenceController$H;)V

    iput-object v0, p0, Lcom/android/settings/notification/RingVolumePreferenceController;->ap:Lcom/android/settings/notification/RingVolumePreferenceController$H;

    iput-object p4, p0, Lcom/android/settings/notification/RingVolumePreferenceController;->aq:Lcom/android/settings/notification/AudioHelper;

    iget-object v0, p0, Lcom/android/settings/notification/RingVolumePreferenceController;->mContext:Landroid/content/Context;

    const-string/jumbo v1, "audio"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    iput-object v0, p0, Lcom/android/settings/notification/RingVolumePreferenceController;->ao:Landroid/media/AudioManager;

    iget-object v0, p0, Lcom/android/settings/notification/RingVolumePreferenceController;->mContext:Landroid/content/Context;

    const-string/jumbo v1, "vibrator"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Vibrator;

    iput-object v0, p0, Lcom/android/settings/notification/RingVolumePreferenceController;->au:Landroid/os/Vibrator;

    iget-object v0, p0, Lcom/android/settings/notification/RingVolumePreferenceController;->au:Landroid/os/Vibrator;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/notification/RingVolumePreferenceController;->au:Landroid/os/Vibrator;

    invoke-virtual {v0}, Landroid/os/Vibrator;->hasVibrator()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    iput-object v2, p0, Lcom/android/settings/notification/RingVolumePreferenceController;->au:Landroid/os/Vibrator;

    :cond_0
    invoke-direct {p0}, Lcom/android/settings/notification/RingVolumePreferenceController;->aU()V

    return-void
.end method

.method private aS()V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/notification/RingVolumePreferenceController;->mContext:Landroid/content/Context;

    invoke-static {v0}, Landroid/app/NotificationManager;->from(Landroid/content/Context;)Landroid/app/NotificationManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/NotificationManager;->getEffectsSuppressor()Landroid/content/ComponentName;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/notification/RingVolumePreferenceController;->at:Landroid/content/ComponentName;

    invoke-static {v0, v1}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    return-void

    :cond_0
    iput-object v0, p0, Lcom/android/settings/notification/RingVolumePreferenceController;->at:Landroid/content/ComponentName;

    iget-object v1, p0, Lcom/android/settings/notification/RingVolumePreferenceController;->dm:Lcom/android/settings/notification/VolumeSeekBarPreference;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/android/settings/notification/RingVolumePreferenceController;->mContext:Landroid/content/Context;

    invoke-static {v1, v0}, Lcom/android/settings/notification/SuppressorHelper;->gZ(Landroid/content/Context;Landroid/content/ComponentName;)Ljava/lang/String;

    :cond_1
    invoke-direct {p0}, Lcom/android/settings/notification/RingVolumePreferenceController;->aT()V

    return-void
.end method

.method private aT()V
    .locals 3

    iget-object v0, p0, Lcom/android/settings/notification/RingVolumePreferenceController;->dm:Lcom/android/settings/notification/VolumeSeekBarPreference;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/android/settings/notification/RingVolumePreferenceController;->dm:Lcom/android/settings/notification/VolumeSeekBarPreference;

    iget-object v0, p0, Lcom/android/settings/notification/RingVolumePreferenceController;->at:Landroid/content/ComponentName;

    if-eqz v0, :cond_1

    const v0, 0x1080301

    :goto_0
    invoke-virtual {v1, v0}, Lcom/android/settings/notification/VolumeSeekBarPreference;->bC(I)V

    :cond_0
    return-void

    :cond_1
    iget v0, p0, Lcom/android/settings/notification/RingVolumePreferenceController;->as:I

    const/4 v2, 0x1

    if-eq v0, v2, :cond_2

    invoke-direct {p0}, Lcom/android/settings/notification/RingVolumePreferenceController;->aV()Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_2
    const v0, 0x1080302

    goto :goto_0

    :cond_3
    const v0, 0x1080300

    goto :goto_0
.end method

.method private aU()V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/notification/RingVolumePreferenceController;->ao:Landroid/media/AudioManager;

    invoke-virtual {v0}, Landroid/media/AudioManager;->getRingerModeInternal()I

    move-result v0

    iget v1, p0, Lcom/android/settings/notification/RingVolumePreferenceController;->as:I

    if-ne v1, v0, :cond_0

    return-void

    :cond_0
    iput v0, p0, Lcom/android/settings/notification/RingVolumePreferenceController;->as:I

    invoke-direct {p0}, Lcom/android/settings/notification/RingVolumePreferenceController;->aT()V

    return-void
.end method

.method private aV()Z
    .locals 3

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/android/settings/notification/RingVolumePreferenceController;->au:Landroid/os/Vibrator;

    if-eqz v1, :cond_0

    iget v1, p0, Lcom/android/settings/notification/RingVolumePreferenceController;->as:I

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/android/settings/notification/RingVolumePreferenceController;->ao:Landroid/media/AudioManager;

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Landroid/media/AudioManager;->getLastAudibleStreamVolume(I)I

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method static synthetic aW(Lcom/android/settings/notification/RingVolumePreferenceController;)Landroid/content/Context;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/notification/RingVolumePreferenceController;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic aX(Lcom/android/settings/notification/RingVolumePreferenceController;)Lcom/android/settings/notification/RingVolumePreferenceController$H;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/notification/RingVolumePreferenceController;->ap:Lcom/android/settings/notification/RingVolumePreferenceController$H;

    return-object v0
.end method

.method static synthetic aY(Lcom/android/settings/notification/RingVolumePreferenceController;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/notification/RingVolumePreferenceController;->aS()V

    return-void
.end method

.method static synthetic aZ(Lcom/android/settings/notification/RingVolumePreferenceController;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/notification/RingVolumePreferenceController;->aU()V

    return-void
.end method


# virtual methods
.method public l()Ljava/lang/String;
    .locals 1

    const-string/jumbo v0, "ring_volume"

    return-object v0
.end method

.method public onPause()V
    .locals 2

    invoke-super {p0}, Lcom/android/settings/notification/VolumeSeekBarPreferenceController;->onPause()V

    iget-object v0, p0, Lcom/android/settings/notification/RingVolumePreferenceController;->ar:Lcom/android/settings/notification/RingVolumePreferenceController$RingReceiver;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/android/settings/notification/RingVolumePreferenceController$RingReceiver;->ba(Z)V

    return-void
.end method

.method public onResume()V
    .locals 2

    invoke-super {p0}, Lcom/android/settings/notification/VolumeSeekBarPreferenceController;->onResume()V

    iget-object v0, p0, Lcom/android/settings/notification/RingVolumePreferenceController;->ar:Lcom/android/settings/notification/RingVolumePreferenceController$RingReceiver;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/android/settings/notification/RingVolumePreferenceController$RingReceiver;->ba(Z)V

    invoke-direct {p0}, Lcom/android/settings/notification/RingVolumePreferenceController;->aS()V

    invoke-direct {p0}, Lcom/android/settings/notification/RingVolumePreferenceController;->aT()V

    return-void
.end method

.method public p()Z
    .locals 1

    iget-object v0, p0, Lcom/android/settings/notification/RingVolumePreferenceController;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/settings/aq;->bqw(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/notification/RingVolumePreferenceController;->aq:Lcom/android/settings/notification/AudioHelper;

    invoke-virtual {v0}, Lcom/android/settings/notification/AudioHelper;->hs()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
