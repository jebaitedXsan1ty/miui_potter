.class public Lcom/android/settings/notification/MiuiZenModeSettings;
.super Lcom/android/settings/MiuiSettingsPreferenceFragment;
.source "MiuiZenModeSettings.java"

# interfaces
.implements Lcom/android/settings/search/Indexable;


# instance fields
.field private eV:Z

.field private eW:Lcom/android/settings/dndmode/LabelPreference;

.field private eX:Landroid/preference/PreferenceCategory;

.field private eY:Landroid/preference/CheckBoxPreference;

.field private eZ:Lcom/android/settings/dndmode/LabelPreference;

.field private fa:Landroid/service/notification/ZenModeConfig;

.field private fb:Landroid/preference/PreferenceCategory;

.field private final fc:Landroid/os/Handler;

.field private fd:Landroid/preference/CheckBoxPreference;

.field private fe:Landroid/content/pm/PackageManager;

.field private ff:Landroid/preference/CheckBoxPreference;

.field private fg:Landroid/preference/PreferenceScreen;

.field private final fh:Lcom/android/settings/notification/MiuiZenModeSettings$SettingsObserver;

.field private fi:Lcom/android/settings/notification/MiuiZenModeSettings$UpdateVipLabelTask;

.field private fj:Lcom/android/settings/dndmode/LabelPreference;

.field private mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/android/settings/MiuiSettingsPreferenceFragment;-><init>()V

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/android/settings/notification/MiuiZenModeSettings;->fc:Landroid/os/Handler;

    new-instance v0, Lcom/android/settings/notification/MiuiZenModeSettings$SettingsObserver;

    invoke-direct {v0, p0}, Lcom/android/settings/notification/MiuiZenModeSettings$SettingsObserver;-><init>(Lcom/android/settings/notification/MiuiZenModeSettings;)V

    iput-object v0, p0, Lcom/android/settings/notification/MiuiZenModeSettings;->fh:Lcom/android/settings/notification/MiuiZenModeSettings$SettingsObserver;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/settings/notification/MiuiZenModeSettings;->eV:Z

    return-void
.end method

.method static synthetic gA(Lcom/android/settings/notification/MiuiZenModeSettings;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/notification/MiuiZenModeSettings;->gr()V

    return-void
.end method

.method public static gn(Landroid/content/Context;)I
    .locals 8

    const/4 v7, 0x0

    const/4 v6, 0x0

    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lmiui/provider/ExtraTelephony$Phonelist;->CONTENT_URI:Landroid/net/Uri;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const-string/jumbo v3, "count(*)"

    const/4 v4, 0x0

    aput-object v3, v2, v4

    const-string/jumbo v3, "type=\'3\' and sync_dirty <> 1"

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    if-eqz v1, :cond_1

    :try_start_1
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getInt(I)I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result v0

    if-eqz v1, :cond_0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_0
    return v0

    :cond_1
    if-eqz v1, :cond_2

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_2
    :goto_0
    return v7

    :catch_0
    move-exception v0

    move-object v1, v6

    :goto_1
    :try_start_2
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    if-eqz v1, :cond_2

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_0

    :catchall_0
    move-exception v0

    :goto_2
    if-eqz v6, :cond_3

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v0

    :catchall_1
    move-exception v0

    move-object v6, v1

    goto :goto_2

    :catch_1
    move-exception v0

    goto :goto_1
.end method

.method private go()V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/notification/MiuiZenModeSettings;->eZ:Lcom/android/settings/dndmode/LabelPreference;

    const-string/jumbo v1, ""

    invoke-virtual {v0, v1}, Lcom/android/settings/dndmode/LabelPreference;->mp(Ljava/lang/String;)V

    return-void
.end method

.method private gp()V
    .locals 6

    iget-object v0, p0, Lcom/android/settings/notification/MiuiZenModeSettings;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/android/settings/notification/MiuiZenModeSettings;->mContext:Landroid/content/Context;

    invoke-static {v1}, Landroid/provider/MiuiSettings$AntiSpam;->getStartTimeForQuietMode(Landroid/content/Context;)I

    move-result v1

    invoke-static {v0, v1}, Lcom/android/settings/dndmode/a;->lg(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/notification/MiuiZenModeSettings;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/android/settings/notification/MiuiZenModeSettings;->mContext:Landroid/content/Context;

    invoke-static {v2}, Landroid/provider/MiuiSettings$AntiSpam;->getEndTimeForQuietMode(Landroid/content/Context;)I

    move-result v2

    invoke-static {v1, v2}, Lcom/android/settings/dndmode/a;->lg(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/android/settings/notification/MiuiZenModeSettings;->eZ:Lcom/android/settings/dndmode/LabelPreference;

    iget-object v3, p0, Lcom/android/settings/notification/MiuiZenModeSettings;->mContext:Landroid/content/Context;

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v0, v4, v5

    const/4 v0, 0x1

    aput-object v1, v4, v0

    const v0, 0x7f120608

    invoke-virtual {v3, v0, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/android/settings/dndmode/LabelPreference;->mp(Ljava/lang/String;)V

    return-void
.end method

.method private gq()V
    .locals 3

    iget-object v0, p0, Lcom/android/settings/notification/MiuiZenModeSettings;->fa:Landroid/service/notification/ZenModeConfig;

    iget-boolean v0, v0, Landroid/service/notification/ZenModeConfig;->allowCalls:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/notification/MiuiZenModeSettings;->fa:Landroid/service/notification/ZenModeConfig;

    iget-boolean v0, v0, Landroid/service/notification/ZenModeConfig;->allowMessages:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/notification/MiuiZenModeSettings;->fa:Landroid/service/notification/ZenModeConfig;

    iget-boolean v0, v0, Landroid/service/notification/ZenModeConfig;->allowEvents:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/notification/MiuiZenModeSettings;->eW:Lcom/android/settings/dndmode/LabelPreference;

    iget-object v1, p0, Lcom/android/settings/notification/MiuiZenModeSettings;->mContext:Landroid/content/Context;

    const v2, 0x7f1205fd

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/settings/dndmode/LabelPreference;->mp(Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/settings/notification/MiuiZenModeSettings;->fa:Landroid/service/notification/ZenModeConfig;

    iget-boolean v0, v0, Landroid/service/notification/ZenModeConfig;->allowCalls:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/settings/notification/MiuiZenModeSettings;->fa:Landroid/service/notification/ZenModeConfig;

    iget-boolean v0, v0, Landroid/service/notification/ZenModeConfig;->allowMessages:Z

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/settings/notification/MiuiZenModeSettings;->fa:Landroid/service/notification/ZenModeConfig;

    iget-boolean v0, v0, Landroid/service/notification/ZenModeConfig;->allowEvents:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/settings/notification/MiuiZenModeSettings;->eW:Lcom/android/settings/dndmode/LabelPreference;

    iget-object v1, p0, Lcom/android/settings/notification/MiuiZenModeSettings;->mContext:Landroid/content/Context;

    const v2, 0x7f120601

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/settings/dndmode/LabelPreference;->mp(Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/android/settings/notification/MiuiZenModeSettings;->fa:Landroid/service/notification/ZenModeConfig;

    iget-boolean v0, v0, Landroid/service/notification/ZenModeConfig;->allowCalls:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/android/settings/notification/MiuiZenModeSettings;->fa:Landroid/service/notification/ZenModeConfig;

    iget-boolean v0, v0, Landroid/service/notification/ZenModeConfig;->allowMessages:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/settings/notification/MiuiZenModeSettings;->fa:Landroid/service/notification/ZenModeConfig;

    iget-boolean v0, v0, Landroid/service/notification/ZenModeConfig;->allowEvents:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/settings/notification/MiuiZenModeSettings;->eW:Lcom/android/settings/dndmode/LabelPreference;

    iget-object v1, p0, Lcom/android/settings/notification/MiuiZenModeSettings;->mContext:Landroid/content/Context;

    const v2, 0x7f120602

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/settings/dndmode/LabelPreference;->mp(Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/android/settings/notification/MiuiZenModeSettings;->fa:Landroid/service/notification/ZenModeConfig;

    iget-boolean v0, v0, Landroid/service/notification/ZenModeConfig;->allowCalls:Z

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/android/settings/notification/MiuiZenModeSettings;->fa:Landroid/service/notification/ZenModeConfig;

    iget-boolean v0, v0, Landroid/service/notification/ZenModeConfig;->allowMessages:Z

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/android/settings/notification/MiuiZenModeSettings;->fa:Landroid/service/notification/ZenModeConfig;

    iget-boolean v0, v0, Landroid/service/notification/ZenModeConfig;->allowEvents:Z

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/android/settings/notification/MiuiZenModeSettings;->eW:Lcom/android/settings/dndmode/LabelPreference;

    iget-object v1, p0, Lcom/android/settings/notification/MiuiZenModeSettings;->mContext:Landroid/content/Context;

    const v2, 0x7f1205ff

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/settings/dndmode/LabelPreference;->mp(Ljava/lang/String;)V

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/android/settings/notification/MiuiZenModeSettings;->fa:Landroid/service/notification/ZenModeConfig;

    iget-boolean v0, v0, Landroid/service/notification/ZenModeConfig;->allowCalls:Z

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/android/settings/notification/MiuiZenModeSettings;->fa:Landroid/service/notification/ZenModeConfig;

    iget-boolean v0, v0, Landroid/service/notification/ZenModeConfig;->allowMessages:Z

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/android/settings/notification/MiuiZenModeSettings;->fa:Landroid/service/notification/ZenModeConfig;

    iget-boolean v0, v0, Landroid/service/notification/ZenModeConfig;->allowEvents:Z

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/android/settings/notification/MiuiZenModeSettings;->eW:Lcom/android/settings/dndmode/LabelPreference;

    iget-object v1, p0, Lcom/android/settings/notification/MiuiZenModeSettings;->mContext:Landroid/content/Context;

    const v2, 0x7f120600

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/settings/dndmode/LabelPreference;->mp(Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_4
    iget-object v0, p0, Lcom/android/settings/notification/MiuiZenModeSettings;->fa:Landroid/service/notification/ZenModeConfig;

    iget-boolean v0, v0, Landroid/service/notification/ZenModeConfig;->allowCalls:Z

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/android/settings/notification/MiuiZenModeSettings;->fa:Landroid/service/notification/ZenModeConfig;

    iget-boolean v0, v0, Landroid/service/notification/ZenModeConfig;->allowMessages:Z

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/android/settings/notification/MiuiZenModeSettings;->fa:Landroid/service/notification/ZenModeConfig;

    iget-boolean v0, v0, Landroid/service/notification/ZenModeConfig;->allowEvents:Z

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/android/settings/notification/MiuiZenModeSettings;->eW:Lcom/android/settings/dndmode/LabelPreference;

    iget-object v1, p0, Lcom/android/settings/notification/MiuiZenModeSettings;->mContext:Landroid/content/Context;

    const v2, 0x7f1205fe

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/settings/dndmode/LabelPreference;->mp(Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_5
    iget-object v0, p0, Lcom/android/settings/notification/MiuiZenModeSettings;->fa:Landroid/service/notification/ZenModeConfig;

    iget-boolean v0, v0, Landroid/service/notification/ZenModeConfig;->allowCalls:Z

    if-nez v0, :cond_6

    iget-object v0, p0, Lcom/android/settings/notification/MiuiZenModeSettings;->fa:Landroid/service/notification/ZenModeConfig;

    iget-boolean v0, v0, Landroid/service/notification/ZenModeConfig;->allowMessages:Z

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/android/settings/notification/MiuiZenModeSettings;->fa:Landroid/service/notification/ZenModeConfig;

    iget-boolean v0, v0, Landroid/service/notification/ZenModeConfig;->allowEvents:Z

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/android/settings/notification/MiuiZenModeSettings;->eW:Lcom/android/settings/dndmode/LabelPreference;

    iget-object v1, p0, Lcom/android/settings/notification/MiuiZenModeSettings;->mContext:Landroid/content/Context;

    const v2, 0x7f120604

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/settings/dndmode/LabelPreference;->mp(Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_6
    iget-object v0, p0, Lcom/android/settings/notification/MiuiZenModeSettings;->eW:Lcom/android/settings/dndmode/LabelPreference;

    iget-object v1, p0, Lcom/android/settings/notification/MiuiZenModeSettings;->mContext:Landroid/content/Context;

    const v2, 0x7f120603

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/settings/dndmode/LabelPreference;->mp(Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method private gr()V
    .locals 4

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/android/settings/notification/MiuiZenModeSettings;->fd:Landroid/preference/CheckBoxPreference;

    iget-object v1, p0, Lcom/android/settings/notification/MiuiZenModeSettings;->mContext:Landroid/content/Context;

    invoke-static {v1}, Landroid/provider/MiuiSettings$AntiSpam;->isQuietModeEnable(Landroid/content/Context;)Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    iget-object v0, p0, Lcom/android/settings/notification/MiuiZenModeSettings;->mContext:Landroid/content/Context;

    invoke-static {v0}, Landroid/app/ExtraNotificationManager;->getZenModeConfig(Landroid/content/Context;)Landroid/service/notification/ZenModeConfig;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/notification/MiuiZenModeSettings;->fa:Landroid/service/notification/ZenModeConfig;

    const-string/jumbo v0, "ZenModeSettings"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Loaded mConfig="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/settings/notification/MiuiZenModeSettings;->fa:Landroid/service/notification/ZenModeConfig;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    sget-boolean v0, Lmiui/os/Build;->IS_TABLET:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/android/settings/notification/MiuiZenModeSettings;->fi:Lcom/android/settings/notification/MiuiZenModeSettings$UpdateVipLabelTask;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/notification/MiuiZenModeSettings;->fi:Lcom/android/settings/notification/MiuiZenModeSettings$UpdateVipLabelTask;

    invoke-virtual {v0}, Lcom/android/settings/notification/MiuiZenModeSettings$UpdateVipLabelTask;->isCancelled()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/notification/MiuiZenModeSettings;->fi:Lcom/android/settings/notification/MiuiZenModeSettings$UpdateVipLabelTask;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/android/settings/notification/MiuiZenModeSettings$UpdateVipLabelTask;->cancel(Z)Z

    :cond_0
    new-instance v0, Lcom/android/settings/notification/MiuiZenModeSettings$UpdateVipLabelTask;

    invoke-direct {v0, p0, v3}, Lcom/android/settings/notification/MiuiZenModeSettings$UpdateVipLabelTask;-><init>(Lcom/android/settings/notification/MiuiZenModeSettings;Lcom/android/settings/notification/MiuiZenModeSettings$UpdateVipLabelTask;)V

    iput-object v0, p0, Lcom/android/settings/notification/MiuiZenModeSettings;->fi:Lcom/android/settings/notification/MiuiZenModeSettings$UpdateVipLabelTask;

    iget-object v0, p0, Lcom/android/settings/notification/MiuiZenModeSettings;->fi:Lcom/android/settings/notification/MiuiZenModeSettings$UpdateVipLabelTask;

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/android/settings/notification/MiuiZenModeSettings$UpdateVipLabelTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    :cond_1
    invoke-direct {p0}, Lcom/android/settings/notification/MiuiZenModeSettings;->gq()V

    iget-object v0, p0, Lcom/android/settings/notification/MiuiZenModeSettings;->mContext:Landroid/content/Context;

    invoke-static {v0}, Landroid/provider/MiuiSettings$AntiSpam;->isAutoTimerOfQuietModeEnable(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-direct {p0}, Lcom/android/settings/notification/MiuiZenModeSettings;->gp()V

    :goto_0
    return-void

    :cond_2
    invoke-direct {p0}, Lcom/android/settings/notification/MiuiZenModeSettings;->go()V

    goto :goto_0
.end method

.method static synthetic gs(Lcom/android/settings/notification/MiuiZenModeSettings;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/settings/notification/MiuiZenModeSettings;->eV:Z

    return v0
.end method

.method static synthetic gt(Lcom/android/settings/notification/MiuiZenModeSettings;)Landroid/service/notification/ZenModeConfig;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/notification/MiuiZenModeSettings;->fa:Landroid/service/notification/ZenModeConfig;

    return-object v0
.end method

.method static synthetic gu(Lcom/android/settings/notification/MiuiZenModeSettings;)Landroid/content/Context;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/notification/MiuiZenModeSettings;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic gv(Lcom/android/settings/notification/MiuiZenModeSettings;)Landroid/os/Handler;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/notification/MiuiZenModeSettings;->fc:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic gw(Lcom/android/settings/notification/MiuiZenModeSettings;)Lcom/android/settings/dndmode/LabelPreference;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/notification/MiuiZenModeSettings;->fj:Lcom/android/settings/dndmode/LabelPreference;

    return-object v0
.end method

.method static synthetic gx(Lcom/android/settings/notification/MiuiZenModeSettings;)Landroid/content/ContentResolver;
    .locals 1

    invoke-virtual {p0}, Lcom/android/settings/notification/MiuiZenModeSettings;->bWB()Landroid/content/ContentResolver;

    move-result-object v0

    return-object v0
.end method

.method static synthetic gy(Lcom/android/settings/notification/MiuiZenModeSettings;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/notification/MiuiZenModeSettings;->go()V

    return-void
.end method

.method static synthetic gz(Lcom/android/settings/notification/MiuiZenModeSettings;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/notification/MiuiZenModeSettings;->gp()V

    return-void
.end method


# virtual methods
.method public cC()Ljava/lang/String;
    .locals 1

    const-class v0, Lcom/android/settings/notification/MiuiZenModeSettings;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 4

    const/4 v3, 0x0

    invoke-super {p0, p1}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->onCreate(Landroid/os/Bundle;)V

    const v0, 0x7f15011b

    invoke-virtual {p0, v0}, Lcom/android/settings/notification/MiuiZenModeSettings;->addPreferencesFromResource(I)V

    invoke-virtual {p0}, Lcom/android/settings/notification/MiuiZenModeSettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/notification/MiuiZenModeSettings;->mContext:Landroid/content/Context;

    iget-object v0, p0, Lcom/android/settings/notification/MiuiZenModeSettings;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/notification/MiuiZenModeSettings;->fe:Landroid/content/pm/PackageManager;

    invoke-virtual {p0}, Lcom/android/settings/notification/MiuiZenModeSettings;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/notification/MiuiZenModeSettings;->fg:Landroid/preference/PreferenceScreen;

    iget-object v0, p0, Lcom/android/settings/notification/MiuiZenModeSettings;->fh:Lcom/android/settings/notification/MiuiZenModeSettings$SettingsObserver;

    invoke-virtual {v0}, Lcom/android/settings/notification/MiuiZenModeSettings$SettingsObserver;->gB()V

    iget-object v0, p0, Lcom/android/settings/notification/MiuiZenModeSettings;->mContext:Landroid/content/Context;

    invoke-static {v0}, Landroid/app/ExtraNotificationManager;->getZenModeConfig(Landroid/content/Context;)Landroid/service/notification/ZenModeConfig;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/notification/MiuiZenModeSettings;->fa:Landroid/service/notification/ZenModeConfig;

    :try_start_0
    iget-object v0, p0, Lcom/android/settings/notification/MiuiZenModeSettings;->fe:Landroid/content/pm/PackageManager;

    const-string/jumbo v1, "com.android.cts.verifier"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/settings/notification/MiuiZenModeSettings;->eV:Z
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    sget-boolean v0, Lmiui/os/Build;->IS_TABLET:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/notification/MiuiZenModeSettings;->fa:Landroid/service/notification/ZenModeConfig;

    iget-boolean v0, v0, Landroid/service/notification/ZenModeConfig;->allowCalls:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/notification/MiuiZenModeSettings;->fa:Landroid/service/notification/ZenModeConfig;

    invoke-virtual {v0}, Landroid/service/notification/ZenModeConfig;->copy()Landroid/service/notification/ZenModeConfig;

    move-result-object v0

    iput-boolean v3, v0, Landroid/service/notification/ZenModeConfig;->allowCalls:Z

    iget-object v1, p0, Lcom/android/settings/notification/MiuiZenModeSettings;->mContext:Landroid/content/Context;

    invoke-static {v1, v0}, Landroid/app/ExtraNotificationManager;->setZenModeConfig(Landroid/content/Context;Landroid/service/notification/ZenModeConfig;)Z

    :cond_0
    iget-object v0, p0, Lcom/android/settings/notification/MiuiZenModeSettings;->fg:Landroid/preference/PreferenceScreen;

    const-string/jumbo v1, "key_do_not_disturb_mode"

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    iput-object v0, p0, Lcom/android/settings/notification/MiuiZenModeSettings;->fd:Landroid/preference/CheckBoxPreference;

    iget-object v0, p0, Lcom/android/settings/notification/MiuiZenModeSettings;->fd:Landroid/preference/CheckBoxPreference;

    new-instance v1, Lcom/android/settings/notification/MiuiZenModeSettings$1;

    invoke-direct {v1, p0}, Lcom/android/settings/notification/MiuiZenModeSettings$1;-><init>(Lcom/android/settings/notification/MiuiZenModeSettings;)V

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    iget-object v0, p0, Lcom/android/settings/notification/MiuiZenModeSettings;->fg:Landroid/preference/PreferenceScreen;

    const-string/jumbo v1, "key_auto_setting_group"

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceCategory;

    iput-object v0, p0, Lcom/android/settings/notification/MiuiZenModeSettings;->fb:Landroid/preference/PreferenceCategory;

    iget-object v0, p0, Lcom/android/settings/notification/MiuiZenModeSettings;->fb:Landroid/preference/PreferenceCategory;

    const-string/jumbo v1, "key_auto_button"

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceCategory;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    iput-object v0, p0, Lcom/android/settings/notification/MiuiZenModeSettings;->eY:Landroid/preference/CheckBoxPreference;

    iget-object v0, p0, Lcom/android/settings/notification/MiuiZenModeSettings;->eY:Landroid/preference/CheckBoxPreference;

    iget-object v1, p0, Lcom/android/settings/notification/MiuiZenModeSettings;->mContext:Landroid/content/Context;

    invoke-static {v1}, Landroid/provider/MiuiSettings$AntiSpam;->isAutoTimerOfQuietModeEnable(Landroid/content/Context;)Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    iget-object v0, p0, Lcom/android/settings/notification/MiuiZenModeSettings;->eY:Landroid/preference/CheckBoxPreference;

    new-instance v1, Lcom/android/settings/notification/MiuiZenModeSettings$2;

    invoke-direct {v1, p0}, Lcom/android/settings/notification/MiuiZenModeSettings$2;-><init>(Lcom/android/settings/notification/MiuiZenModeSettings;)V

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    iget-object v0, p0, Lcom/android/settings/notification/MiuiZenModeSettings;->fb:Landroid/preference/PreferenceCategory;

    const-string/jumbo v1, "key_auto_time_setting"

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceCategory;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Lcom/android/settings/dndmode/LabelPreference;

    iput-object v0, p0, Lcom/android/settings/notification/MiuiZenModeSettings;->eZ:Lcom/android/settings/dndmode/LabelPreference;

    iget-object v0, p0, Lcom/android/settings/notification/MiuiZenModeSettings;->eZ:Lcom/android/settings/dndmode/LabelPreference;

    new-instance v1, Lcom/android/settings/notification/MiuiZenModeSettings$3;

    invoke-direct {v1, p0}, Lcom/android/settings/notification/MiuiZenModeSettings$3;-><init>(Lcom/android/settings/notification/MiuiZenModeSettings;)V

    invoke-virtual {v0, v1}, Lcom/android/settings/dndmode/LabelPreference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    iget-object v0, p0, Lcom/android/settings/notification/MiuiZenModeSettings;->fg:Landroid/preference/PreferenceScreen;

    const-string/jumbo v1, "alarm_use"

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceCategory;

    iput-object v0, p0, Lcom/android/settings/notification/MiuiZenModeSettings;->eX:Landroid/preference/PreferenceCategory;

    iget-object v0, p0, Lcom/android/settings/notification/MiuiZenModeSettings;->eX:Landroid/preference/PreferenceCategory;

    const-string/jumbo v1, "vip"

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceCategory;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Lcom/android/settings/dndmode/LabelPreference;

    iput-object v0, p0, Lcom/android/settings/notification/MiuiZenModeSettings;->fj:Lcom/android/settings/dndmode/LabelPreference;

    iget-object v0, p0, Lcom/android/settings/notification/MiuiZenModeSettings;->fj:Lcom/android/settings/dndmode/LabelPreference;

    new-instance v1, Lcom/android/settings/notification/MiuiZenModeSettings$4;

    invoke-direct {v1, p0}, Lcom/android/settings/notification/MiuiZenModeSettings$4;-><init>(Lcom/android/settings/notification/MiuiZenModeSettings;)V

    invoke-virtual {v0, v1}, Lcom/android/settings/dndmode/LabelPreference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    iget-object v0, p0, Lcom/android/settings/notification/MiuiZenModeSettings;->eX:Landroid/preference/PreferenceCategory;

    const-string/jumbo v1, "alarm_content"

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceCategory;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Lcom/android/settings/dndmode/LabelPreference;

    iput-object v0, p0, Lcom/android/settings/notification/MiuiZenModeSettings;->eW:Lcom/android/settings/dndmode/LabelPreference;

    iget-object v0, p0, Lcom/android/settings/notification/MiuiZenModeSettings;->eW:Lcom/android/settings/dndmode/LabelPreference;

    new-instance v1, Lcom/android/settings/notification/MiuiZenModeSettings$5;

    invoke-direct {v1, p0}, Lcom/android/settings/notification/MiuiZenModeSettings$5;-><init>(Lcom/android/settings/notification/MiuiZenModeSettings;)V

    invoke-virtual {v0, v1}, Lcom/android/settings/dndmode/LabelPreference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    iget-object v0, p0, Lcom/android/settings/notification/MiuiZenModeSettings;->eX:Landroid/preference/PreferenceCategory;

    const-string/jumbo v1, "repeat"

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceCategory;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    iput-object v0, p0, Lcom/android/settings/notification/MiuiZenModeSettings;->ff:Landroid/preference/CheckBoxPreference;

    iget-object v0, p0, Lcom/android/settings/notification/MiuiZenModeSettings;->ff:Landroid/preference/CheckBoxPreference;

    iget-object v1, p0, Lcom/android/settings/notification/MiuiZenModeSettings;->mContext:Landroid/content/Context;

    invoke-static {v1}, Landroid/provider/MiuiSettings$AntiSpam;->isRepeatedCallActionEnable(Landroid/content/Context;)Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    iget-object v0, p0, Lcom/android/settings/notification/MiuiZenModeSettings;->ff:Landroid/preference/CheckBoxPreference;

    new-instance v1, Lcom/android/settings/notification/MiuiZenModeSettings$6;

    invoke-direct {v1, p0}, Lcom/android/settings/notification/MiuiZenModeSettings$6;-><init>(Lcom/android/settings/notification/MiuiZenModeSettings;)V

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    sget-boolean v0, Lmiui/os/Build;->IS_TABLET:Z

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/android/settings/notification/MiuiZenModeSettings;->eV:Z

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/settings/notification/MiuiZenModeSettings;->fd:Landroid/preference/CheckBoxPreference;

    const v1, 0x7f120618

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setSummary(I)V

    iget-object v0, p0, Lcom/android/settings/notification/MiuiZenModeSettings;->fg:Landroid/preference/PreferenceScreen;

    iget-object v1, p0, Lcom/android/settings/notification/MiuiZenModeSettings;->eX:Landroid/preference/PreferenceCategory;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    :cond_1
    invoke-direct {p0}, Lcom/android/settings/notification/MiuiZenModeSettings;->gr()V

    return-void

    :catch_0
    move-exception v0

    iput-boolean v3, p0, Lcom/android/settings/notification/MiuiZenModeSettings;->eV:Z

    goto/16 :goto_0
.end method

.method public onDestroy()V
    .locals 2

    invoke-super {p0}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->onDestroy()V

    iget-object v0, p0, Lcom/android/settings/notification/MiuiZenModeSettings;->fh:Lcom/android/settings/notification/MiuiZenModeSettings$SettingsObserver;

    invoke-virtual {v0}, Lcom/android/settings/notification/MiuiZenModeSettings$SettingsObserver;->gC()V

    iget-object v0, p0, Lcom/android/settings/notification/MiuiZenModeSettings;->fi:Lcom/android/settings/notification/MiuiZenModeSettings$UpdateVipLabelTask;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/notification/MiuiZenModeSettings;->fi:Lcom/android/settings/notification/MiuiZenModeSettings$UpdateVipLabelTask;

    invoke-virtual {v0}, Lcom/android/settings/notification/MiuiZenModeSettings$UpdateVipLabelTask;->isCancelled()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/notification/MiuiZenModeSettings;->fi:Lcom/android/settings/notification/MiuiZenModeSettings$UpdateVipLabelTask;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/android/settings/notification/MiuiZenModeSettings$UpdateVipLabelTask;->cancel(Z)Z

    :cond_0
    return-void
.end method

.method public onResume()V
    .locals 0

    invoke-super {p0}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->onResume()V

    invoke-direct {p0}, Lcom/android/settings/notification/MiuiZenModeSettings;->gr()V

    return-void
.end method
