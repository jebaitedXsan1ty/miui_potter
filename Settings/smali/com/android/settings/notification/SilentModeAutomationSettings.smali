.class public Lcom/android/settings/notification/SilentModeAutomationSettings;
.super Lcom/android/settings/notification/SilentModeSettingsBase;
.source "SilentModeAutomationSettings.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/widget/CompoundButton$OnCheckedChangeListener;


# instance fields
.field private ge:Landroid/view/MenuItem;

.field private gf:Landroid/preference/PreferenceCategory;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/notification/SilentModeSettingsBase;-><init>()V

    return-void
.end method

.method private hF()V
    .locals 9

    iget-object v0, p0, Lcom/android/settings/notification/SilentModeAutomationSettings;->gf:Landroid/preference/PreferenceCategory;

    invoke-virtual {v0}, Landroid/preference/PreferenceCategory;->removeAll()V

    iget-object v0, p0, Lcom/android/settings/notification/SilentModeAutomationSettings;->cw:Landroid/service/notification/ZenModeConfig;

    if-nez v0, :cond_0

    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/android/settings/notification/SilentModeAutomationSettings;->dj()[Lcom/android/settings/notification/SilentModeSettingsBase$ZenRuleInfo;

    move-result-object v8

    const/4 v0, 0x0

    move v7, v0

    :goto_0
    array-length v0, v8

    if-ge v7, v0, :cond_1

    aget-object v0, v8, v7

    iget-object v4, v0, Lcom/android/settings/notification/SilentModeSettingsBase$ZenRuleInfo;->id:Ljava/lang/String;

    aget-object v0, v8, v7

    iget-object v3, v0, Lcom/android/settings/notification/SilentModeSettingsBase$ZenRuleInfo;->cE:Landroid/service/notification/ZenModeConfig$ZenRule;

    new-instance v0, Lcom/android/settings/notification/RuleItemPreference;

    iget-object v1, p0, Lcom/android/settings/notification/SilentModeAutomationSettings;->mContext:Landroid/content/Context;

    iget-object v2, v3, Landroid/service/notification/ZenModeConfig$ZenRule;->name:Ljava/lang/String;

    iget-boolean v3, v3, Landroid/service/notification/ZenModeConfig$ZenRule;->enabled:Z

    move-object v5, p0

    move-object v6, p0

    invoke-direct/range {v0 .. v6}, Lcom/android/settings/notification/RuleItemPreference;-><init>(Landroid/content/Context;Ljava/lang/String;ZLjava/lang/String;Landroid/view/View$OnClickListener;Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    iget-object v1, p0, Lcom/android/settings/notification/SilentModeAutomationSettings;->gf:Landroid/preference/PreferenceCategory;

    invoke-virtual {v1, v0}, Landroid/preference/PreferenceCategory;->addPreference(Landroid/preference/Preference;)Z

    add-int/lit8 v0, v7, 0x1

    move v7, v0

    goto :goto_0

    :cond_1
    return-void
.end method


# virtual methods
.method protected cV()V
    .locals 0

    return-void
.end method

.method protected cW()V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/notification/SilentModeAutomationSettings;->hF()V

    return-void
.end method

.method public onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 3

    instance-of v0, p1, Landroid/widget/CheckBox;

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Landroid/widget/CompoundButton;->getTag()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/notification/SilentModeAutomationSettings;->mContext:Landroid/content/Context;

    invoke-static {v1}, Landroid/app/NotificationManager;->from(Landroid/content/Context;)Landroid/app/NotificationManager;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/app/NotificationManager;->getAutomaticZenRule(Ljava/lang/String;)Landroid/app/AutomaticZenRule;

    move-result-object v1

    invoke-virtual {v1, p2}, Landroid/app/AutomaticZenRule;->setEnabled(Z)V

    iget-object v2, p0, Lcom/android/settings/notification/SilentModeAutomationSettings;->mContext:Landroid/content/Context;

    invoke-static {v2}, Landroid/app/NotificationManager;->from(Landroid/content/Context;)Landroid/app/NotificationManager;

    move-result-object v2

    invoke-virtual {v2, v0, v1}, Landroid/app/NotificationManager;->updateAutomaticZenRule(Ljava/lang/String;Landroid/app/AutomaticZenRule;)Z

    :cond_0
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 3

    instance-of v0, p1, Landroid/widget/TextView;

    if-eqz v0, :cond_0

    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/android/settings/notification/SilentModeAutomationSettings;->getContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/android/settings/notification/SilentModeRuleSettings;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v1, "rule_id"

    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string/jumbo v1, "mode"

    const/4 v2, 0x3

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/android/settings/notification/SilentModeAutomationSettings;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    :cond_0
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1

    invoke-super {p0, p1}, Lcom/android/settings/notification/SilentModeSettingsBase;->onCreate(Landroid/os/Bundle;)V

    const v0, 0x7f150023

    invoke-virtual {p0, v0}, Lcom/android/settings/notification/SilentModeAutomationSettings;->addPreferencesFromResource(I)V

    const-string/jumbo v0, "key_auto_rules"

    invoke-virtual {p0, v0}, Lcom/android/settings/notification/SilentModeAutomationSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceCategory;

    iput-object v0, p0, Lcom/android/settings/notification/SilentModeAutomationSettings;->gf:Landroid/preference/PreferenceCategory;

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/android/settings/notification/SilentModeAutomationSettings;->setHasOptionsMenu(Z)V

    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V
    .locals 3

    const/4 v2, 0x1

    const/4 v1, 0x0

    const v0, 0x7f1200b3

    invoke-interface {p1, v1, v1, v1, v0}, Landroid/view/Menu;->add(IIII)Landroid/view/MenuItem;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/notification/SilentModeAutomationSettings;->ge:Landroid/view/MenuItem;

    iget-object v0, p0, Lcom/android/settings/notification/SilentModeAutomationSettings;->ge:Landroid/view/MenuItem;

    sget v1, Lmiui/R$drawable;->action_button_new_light:I

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setShowAsAction(I)V

    iget-object v0, p0, Lcom/android/settings/notification/SilentModeAutomationSettings;->ge:Landroid/view/MenuItem;

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 3

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :goto_0
    invoke-super {p0, p1}, Lcom/android/settings/notification/SilentModeSettingsBase;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    return v0

    :pswitch_0
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/android/settings/notification/SilentModeAutomationSettings;->getContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/android/settings/notification/SilentModeRuleSettings;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v1, "rule_id"

    const-string/jumbo v2, "new_rule"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string/jumbo v1, "mode"

    const/4 v2, 0x2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/android/settings/notification/SilentModeAutomationSettings;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method public onResume()V
    .locals 0

    invoke-super {p0}, Lcom/android/settings/notification/SilentModeSettingsBase;->onResume()V

    invoke-direct {p0}, Lcom/android/settings/notification/SilentModeAutomationSettings;->hF()V

    return-void
.end method
