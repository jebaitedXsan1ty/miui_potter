.class final Lcom/android/settings/notification/ZenModePrioritySettings$4;
.super Ljava/lang/Object;
.source "ZenModePrioritySettings.java"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceChangeListener;


# instance fields
.field final synthetic ib:Lcom/android/settings/notification/ZenModePrioritySettings;


# direct methods
.method constructor <init>(Lcom/android/settings/notification/ZenModePrioritySettings;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/notification/ZenModePrioritySettings$4;->ib:Lcom/android/settings/notification/ZenModePrioritySettings;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 6

    const/4 v2, 0x1

    const/4 v1, 0x0

    const/4 v3, -0x1

    iget-object v0, p0, Lcom/android/settings/notification/ZenModePrioritySettings$4;->ib:Lcom/android/settings/notification/ZenModePrioritySettings;

    invoke-static {v0}, Lcom/android/settings/notification/ZenModePrioritySettings;->gV(Lcom/android/settings/notification/ZenModePrioritySettings;)Z

    move-result v0

    if-eqz v0, :cond_0

    return v1

    :cond_0
    check-cast p2, Ljava/lang/String;

    invoke-static {p2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    if-eq v0, v3, :cond_1

    move v1, v2

    :cond_1
    if-ne v0, v3, :cond_2

    iget-object v0, p0, Lcom/android/settings/notification/ZenModePrioritySettings$4;->ib:Lcom/android/settings/notification/ZenModePrioritySettings;

    invoke-static {v0}, Lcom/android/settings/notification/ZenModePrioritySettings;->gW(Lcom/android/settings/notification/ZenModePrioritySettings;)Landroid/app/NotificationManager$Policy;

    move-result-object v0

    iget v0, v0, Landroid/app/NotificationManager$Policy;->priorityCallSenders:I

    :cond_2
    sget-boolean v3, Lcom/android/settings/notification/ZenModePrioritySettings;->eJ:Z

    if-eqz v3, :cond_3

    const-string/jumbo v3, "ZenModeSettings"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "onPrefChange allowCalls="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, " allowCallsFrom="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {v0}, Landroid/service/notification/ZenModeConfig;->sourceToString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_3
    iget-object v3, p0, Lcom/android/settings/notification/ZenModePrioritySettings$4;->ib:Lcom/android/settings/notification/ZenModePrioritySettings;

    iget-object v4, p0, Lcom/android/settings/notification/ZenModePrioritySettings$4;->ib:Lcom/android/settings/notification/ZenModePrioritySettings;

    const/16 v5, 0x8

    invoke-static {v4, v1, v5}, Lcom/android/settings/notification/ZenModePrioritySettings;->gX(Lcom/android/settings/notification/ZenModePrioritySettings;ZI)I

    move-result v1

    iget-object v4, p0, Lcom/android/settings/notification/ZenModePrioritySettings$4;->ib:Lcom/android/settings/notification/ZenModePrioritySettings;

    invoke-static {v4}, Lcom/android/settings/notification/ZenModePrioritySettings;->gW(Lcom/android/settings/notification/ZenModePrioritySettings;)Landroid/app/NotificationManager$Policy;

    move-result-object v4

    iget v4, v4, Landroid/app/NotificationManager$Policy;->priorityMessageSenders:I

    iget-object v5, p0, Lcom/android/settings/notification/ZenModePrioritySettings$4;->ib:Lcom/android/settings/notification/ZenModePrioritySettings;

    invoke-static {v5}, Lcom/android/settings/notification/ZenModePrioritySettings;->gW(Lcom/android/settings/notification/ZenModePrioritySettings;)Landroid/app/NotificationManager$Policy;

    move-result-object v5

    iget v5, v5, Landroid/app/NotificationManager$Policy;->suppressedVisualEffects:I

    invoke-static {v3, v1, v0, v4, v5}, Lcom/android/settings/notification/ZenModePrioritySettings;->gY(Lcom/android/settings/notification/ZenModePrioritySettings;IIII)V

    return v2
.end method
