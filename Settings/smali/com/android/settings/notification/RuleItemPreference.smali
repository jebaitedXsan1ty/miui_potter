.class public Lcom/android/settings/notification/RuleItemPreference;
.super Landroid/preference/Preference;
.source "RuleItemPreference.java"


# instance fields
.field private bM:Landroid/widget/CompoundButton$OnCheckedChangeListener;

.field private bN:Z

.field private bO:Landroid/view/View$OnClickListener;

.field private bP:Lmiui/widget/SlidingButton;

.field private bQ:Landroid/widget/TextView;

.field private bR:Ljava/lang/String;

.field private mContext:Landroid/content/Context;

.field private title:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    invoke-direct {p0, p1}, Landroid/preference/Preference;-><init>(Landroid/content/Context;)V

    iput-object p1, p0, Lcom/android/settings/notification/RuleItemPreference;->mContext:Landroid/content/Context;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Landroid/preference/Preference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    iput-object p1, p0, Lcom/android/settings/notification/RuleItemPreference;->mContext:Landroid/content/Context;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Landroid/preference/Preference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iput-object p1, p0, Lcom/android/settings/notification/RuleItemPreference;->mContext:Landroid/content/Context;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;ZLjava/lang/String;Landroid/view/View$OnClickListener;Landroid/widget/CompoundButton$OnCheckedChangeListener;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/settings/notification/RuleItemPreference;-><init>(Landroid/content/Context;)V

    iput-object p2, p0, Lcom/android/settings/notification/RuleItemPreference;->title:Ljava/lang/String;

    iput-object p5, p0, Lcom/android/settings/notification/RuleItemPreference;->bO:Landroid/view/View$OnClickListener;

    iput-object p6, p0, Lcom/android/settings/notification/RuleItemPreference;->bM:Landroid/widget/CompoundButton$OnCheckedChangeListener;

    iput-boolean p3, p0, Lcom/android/settings/notification/RuleItemPreference;->bN:Z

    iput-object p4, p0, Lcom/android/settings/notification/RuleItemPreference;->bR:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method protected onBindView(Landroid/view/View;)V
    .locals 2

    invoke-super {p0, p1}, Landroid/preference/Preference;->onBindView(Landroid/view/View;)V

    const v0, 0x7f0a0170

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lmiui/widget/SlidingButton;

    iput-object v0, p0, Lcom/android/settings/notification/RuleItemPreference;->bP:Lmiui/widget/SlidingButton;

    const v0, 0x7f0a03a2

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/settings/notification/RuleItemPreference;->bQ:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/android/settings/notification/RuleItemPreference;->bQ:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/android/settings/notification/RuleItemPreference;->title:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/android/settings/notification/RuleItemPreference;->bQ:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/android/settings/notification/RuleItemPreference;->bR:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTag(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/android/settings/notification/RuleItemPreference;->bQ:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/android/settings/notification/RuleItemPreference;->bO:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/android/settings/notification/RuleItemPreference;->bP:Lmiui/widget/SlidingButton;

    iget-boolean v1, p0, Lcom/android/settings/notification/RuleItemPreference;->bN:Z

    invoke-virtual {v0, v1}, Lmiui/widget/SlidingButton;->setChecked(Z)V

    iget-object v0, p0, Lcom/android/settings/notification/RuleItemPreference;->bP:Lmiui/widget/SlidingButton;

    iget-object v1, p0, Lcom/android/settings/notification/RuleItemPreference;->bR:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lmiui/widget/SlidingButton;->setTag(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/android/settings/notification/RuleItemPreference;->bP:Lmiui/widget/SlidingButton;

    iget-object v1, p0, Lcom/android/settings/notification/RuleItemPreference;->bM:Landroid/widget/CompoundButton$OnCheckedChangeListener;

    invoke-virtual {v0, v1}, Lmiui/widget/SlidingButton;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    return-void
.end method

.method protected onCreateView(Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 1

    const v0, 0x7f0d0197

    invoke-virtual {p0, v0}, Lcom/android/settings/notification/RuleItemPreference;->setLayoutResource(I)V

    invoke-super {p0, p1}, Landroid/preference/Preference;->onCreateView(Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method
