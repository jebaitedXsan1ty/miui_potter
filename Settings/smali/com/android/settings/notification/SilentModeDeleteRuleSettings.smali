.class public Lcom/android/settings/notification/SilentModeDeleteRuleSettings;
.super Lmiui/app/Activity;
.source "SilentModeDeleteRuleSettings.java"


# instance fields
.field private bs:Lcom/android/settings/notification/SilentModeDeleteSettings;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lmiui/app/Activity;-><init>()V

    return-void
.end method

.method static synthetic cB(Lcom/android/settings/notification/SilentModeDeleteRuleSettings;)Lcom/android/settings/notification/SilentModeDeleteSettings;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/notification/SilentModeDeleteRuleSettings;->bs:Lcom/android/settings/notification/SilentModeDeleteSettings;

    return-object v0
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4

    invoke-super {p0, p1}, Lmiui/app/Activity;->onCreate(Landroid/os/Bundle;)V

    const v0, 0x7f0d009f

    invoke-virtual {p0, v0}, Lcom/android/settings/notification/SilentModeDeleteRuleSettings;->setContentView(I)V

    new-instance v0, Lcom/android/settings/notification/SilentModeDeleteSettings;

    invoke-direct {v0}, Lcom/android/settings/notification/SilentModeDeleteSettings;-><init>()V

    iput-object v0, p0, Lcom/android/settings/notification/SilentModeDeleteRuleSettings;->bs:Lcom/android/settings/notification/SilentModeDeleteSettings;

    invoke-virtual {p0}, Lcom/android/settings/notification/SilentModeDeleteRuleSettings;->getActionBar()Lmiui/app/ActionBar;

    move-result-object v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    invoke-virtual {v0}, Landroid/app/ActionBar;->getCustomView()Landroid/view/View;

    move-result-object v1

    const v0, 0x1020016

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/android/settings/notification/SilentModeDeleteRuleSettings;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f12056a

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v0, 0x1020019

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const/high16 v2, 0x1040000

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(I)V

    new-instance v2, Lcom/android/settings/notification/SilentModeDeleteRuleSettings$1;

    invoke-direct {v2, p0}, Lcom/android/settings/notification/SilentModeDeleteRuleSettings$1;-><init>(Lcom/android/settings/notification/SilentModeDeleteRuleSettings;)V

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x102001a

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const v1, 0x104000a

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    new-instance v1, Lcom/android/settings/notification/SilentModeDeleteRuleSettings$2;

    invoke-direct {v1, p0}, Lcom/android/settings/notification/SilentModeDeleteRuleSettings$2;-><init>(Lcom/android/settings/notification/SilentModeDeleteRuleSettings;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    if-nez p1, :cond_1

    invoke-virtual {p0}, Lcom/android/settings/notification/SilentModeDeleteRuleSettings;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/notification/SilentModeDeleteRuleSettings;->bs:Lcom/android/settings/notification/SilentModeDeleteSettings;

    const v2, 0x7f0a01af

    invoke-virtual {v0, v2, v1}, Landroid/app/FragmentTransaction;->add(ILandroid/app/Fragment;)Landroid/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentTransaction;->commit()I

    :cond_1
    return-void
.end method
