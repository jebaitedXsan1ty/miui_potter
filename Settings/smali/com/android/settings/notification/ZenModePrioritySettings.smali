.class public Lcom/android/settings/notification/ZenModePrioritySettings;
.super Lcom/android/settings/notification/ZenModeSettingsBase;
.source "ZenModePrioritySettings.java"

# interfaces
.implements Lcom/android/settings/search/Indexable;


# instance fields
.field private fA:Lcom/android/settings/MiuiDropDownPreference;

.field private fB:Z

.field private fC:Landroid/preference/SwitchPreference;

.field private fD:Lcom/android/settings/MiuiDropDownPreference;

.field private fE:Landroid/app/NotificationManager$Policy;

.field private fF:Landroid/preference/SwitchPreference;

.field private fG:Landroid/preference/SwitchPreference;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/notification/ZenModeSettingsBase;-><init>()V

    return-void
.end method

.method private static gQ(Lcom/android/settings/MiuiDropDownPreference;)V
    .locals 8

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    new-array v0, v7, [Ljava/lang/CharSequence;

    invoke-virtual {p0}, Lcom/android/settings/MiuiDropDownPreference;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f1216aa

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v3

    invoke-virtual {p0}, Lcom/android/settings/MiuiDropDownPreference;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f1216ac

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v4

    invoke-virtual {p0}, Lcom/android/settings/MiuiDropDownPreference;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f1216af

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v5

    invoke-virtual {p0}, Lcom/android/settings/MiuiDropDownPreference;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f1216ad

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v6

    invoke-virtual {p0, v0}, Lcom/android/settings/MiuiDropDownPreference;->setEntries([Ljava/lang/CharSequence;)V

    new-array v0, v7, [Ljava/lang/CharSequence;

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v3

    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v4

    invoke-static {v5}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v5

    const/4 v1, -0x1

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v6

    invoke-virtual {p0, v0}, Lcom/android/settings/MiuiDropDownPreference;->setEntryValues([Ljava/lang/CharSequence;)V

    return-void
.end method

.method private gR(ZI)I
    .locals 2

    iget-object v0, p0, Lcom/android/settings/notification/ZenModePrioritySettings;->fE:Landroid/app/NotificationManager$Policy;

    iget v0, v0, Landroid/app/NotificationManager$Policy;->priorityCategories:I

    if-eqz p1, :cond_0

    or-int/2addr v0, p2

    :goto_0
    return v0

    :cond_0
    not-int v1, p2

    and-int/2addr v0, v1

    goto :goto_0
.end method

.method private gS(I)Z
    .locals 2

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/android/settings/notification/ZenModePrioritySettings;->fE:Landroid/app/NotificationManager$Policy;

    iget v1, v1, Landroid/app/NotificationManager$Policy;->priorityCategories:I

    and-int/2addr v1, p1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method private gT(IIII)V
    .locals 2

    new-instance v0, Landroid/app/NotificationManager$Policy;

    invoke-direct {v0, p1, p2, p3, p4}, Landroid/app/NotificationManager$Policy;-><init>(IIII)V

    iput-object v0, p0, Lcom/android/settings/notification/ZenModePrioritySettings;->fE:Landroid/app/NotificationManager$Policy;

    iget-object v0, p0, Lcom/android/settings/notification/ZenModePrioritySettings;->mContext:Landroid/content/Context;

    invoke-static {v0}, Landroid/app/NotificationManager;->from(Landroid/content/Context;)Landroid/app/NotificationManager;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/notification/ZenModePrioritySettings;->fE:Landroid/app/NotificationManager$Policy;

    invoke-virtual {v0, v1}, Landroid/app/NotificationManager;->setNotificationPolicy(Landroid/app/NotificationManager$Policy;)V

    return-void
.end method

.method private gU()V
    .locals 4

    const/4 v3, 0x1

    const/4 v1, -0x1

    iput-boolean v3, p0, Lcom/android/settings/notification/ZenModePrioritySettings;->fB:Z

    iget-object v0, p0, Lcom/android/settings/notification/ZenModePrioritySettings;->fA:Lcom/android/settings/MiuiDropDownPreference;

    if-eqz v0, :cond_0

    iget-object v2, p0, Lcom/android/settings/notification/ZenModePrioritySettings;->fA:Lcom/android/settings/MiuiDropDownPreference;

    const/16 v0, 0x8

    invoke-direct {p0, v0}, Lcom/android/settings/notification/ZenModePrioritySettings;->gS(I)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/settings/notification/ZenModePrioritySettings;->fE:Landroid/app/NotificationManager$Policy;

    iget v0, v0, Landroid/app/NotificationManager$Policy;->priorityCallSenders:I

    :goto_0
    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/android/settings/MiuiDropDownPreference;->setValue(Ljava/lang/String;)V

    :cond_0
    iget-object v0, p0, Lcom/android/settings/notification/ZenModePrioritySettings;->fD:Lcom/android/settings/MiuiDropDownPreference;

    const/4 v2, 0x4

    invoke-direct {p0, v2}, Lcom/android/settings/notification/ZenModePrioritySettings;->gS(I)Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v1, p0, Lcom/android/settings/notification/ZenModePrioritySettings;->fE:Landroid/app/NotificationManager$Policy;

    iget v1, v1, Landroid/app/NotificationManager$Policy;->priorityMessageSenders:I

    :cond_1
    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/settings/MiuiDropDownPreference;->setValue(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/settings/notification/ZenModePrioritySettings;->fF:Landroid/preference/SwitchPreference;

    invoke-direct {p0, v3}, Lcom/android/settings/notification/ZenModePrioritySettings;->gS(I)Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/preference/SwitchPreference;->setChecked(Z)V

    iget-object v0, p0, Lcom/android/settings/notification/ZenModePrioritySettings;->fC:Landroid/preference/SwitchPreference;

    const/4 v1, 0x2

    invoke-direct {p0, v1}, Lcom/android/settings/notification/ZenModePrioritySettings;->gS(I)Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/preference/SwitchPreference;->setChecked(Z)V

    iget-object v0, p0, Lcom/android/settings/notification/ZenModePrioritySettings;->fG:Landroid/preference/SwitchPreference;

    const/16 v1, 0x10

    invoke-direct {p0, v1}, Lcom/android/settings/notification/ZenModePrioritySettings;->gS(I)Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/preference/SwitchPreference;->setChecked(Z)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/settings/notification/ZenModePrioritySettings;->fB:Z

    return-void

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method static synthetic gV(Lcom/android/settings/notification/ZenModePrioritySettings;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/settings/notification/ZenModePrioritySettings;->fB:Z

    return v0
.end method

.method static synthetic gW(Lcom/android/settings/notification/ZenModePrioritySettings;)Landroid/app/NotificationManager$Policy;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/notification/ZenModePrioritySettings;->fE:Landroid/app/NotificationManager$Policy;

    return-object v0
.end method

.method static synthetic gX(Lcom/android/settings/notification/ZenModePrioritySettings;ZI)I
    .locals 1

    invoke-direct {p0, p1, p2}, Lcom/android/settings/notification/ZenModePrioritySettings;->gR(ZI)I

    move-result v0

    return v0
.end method

.method static synthetic gY(Lcom/android/settings/notification/ZenModePrioritySettings;IIII)V
    .locals 0

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/android/settings/notification/ZenModePrioritySettings;->gT(IIII)V

    return-void
.end method


# virtual methods
.method protected ca()V
    .locals 0

    return-void
.end method

.method protected cb()V
    .locals 1

    iget-object v0, p0, Lcom/android/settings/notification/ZenModePrioritySettings;->mContext:Landroid/content/Context;

    invoke-static {v0}, Landroid/app/NotificationManager;->from(Landroid/content/Context;)Landroid/app/NotificationManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/NotificationManager;->getNotificationPolicy()Landroid/app/NotificationManager$Policy;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/notification/ZenModePrioritySettings;->fE:Landroid/app/NotificationManager$Policy;

    invoke-direct {p0}, Lcom/android/settings/notification/ZenModePrioritySettings;->gU()V

    return-void
.end method

.method public getMetricsCategory()I
    .locals 1

    const/16 v0, 0x8d

    return v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 5

    invoke-super {p0, p1}, Lcom/android/settings/notification/ZenModeSettingsBase;->onCreate(Landroid/os/Bundle;)V

    const v0, 0x7f150119

    invoke-virtual {p0, v0}, Lcom/android/settings/notification/ZenModePrioritySettings;->addPreferencesFromResource(I)V

    invoke-virtual {p0}, Lcom/android/settings/notification/ZenModePrioritySettings;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v1

    iget-object v0, p0, Lcom/android/settings/notification/ZenModePrioritySettings;->mContext:Landroid/content/Context;

    invoke-static {v0}, Landroid/app/NotificationManager;->from(Landroid/content/Context;)Landroid/app/NotificationManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/NotificationManager;->getNotificationPolicy()Landroid/app/NotificationManager$Policy;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/notification/ZenModePrioritySettings;->fE:Landroid/app/NotificationManager$Policy;

    const-string/jumbo v0, "reminders"

    invoke-virtual {v1, v0}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/SwitchPreference;

    iput-object v0, p0, Lcom/android/settings/notification/ZenModePrioritySettings;->fF:Landroid/preference/SwitchPreference;

    iget-object v0, p0, Lcom/android/settings/notification/ZenModePrioritySettings;->fF:Landroid/preference/SwitchPreference;

    new-instance v2, Lcom/android/settings/notification/ZenModePrioritySettings$1;

    invoke-direct {v2, p0}, Lcom/android/settings/notification/ZenModePrioritySettings$1;-><init>(Lcom/android/settings/notification/ZenModePrioritySettings;)V

    invoke-virtual {v0, v2}, Landroid/preference/SwitchPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    const-string/jumbo v0, "events"

    invoke-virtual {v1, v0}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/SwitchPreference;

    iput-object v0, p0, Lcom/android/settings/notification/ZenModePrioritySettings;->fC:Landroid/preference/SwitchPreference;

    iget-object v0, p0, Lcom/android/settings/notification/ZenModePrioritySettings;->fC:Landroid/preference/SwitchPreference;

    new-instance v2, Lcom/android/settings/notification/ZenModePrioritySettings$2;

    invoke-direct {v2, p0}, Lcom/android/settings/notification/ZenModePrioritySettings$2;-><init>(Lcom/android/settings/notification/ZenModePrioritySettings;)V

    invoke-virtual {v0, v2}, Landroid/preference/SwitchPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    const-string/jumbo v0, "messages"

    invoke-virtual {v1, v0}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Lcom/android/settings/MiuiDropDownPreference;

    iput-object v0, p0, Lcom/android/settings/notification/ZenModePrioritySettings;->fD:Lcom/android/settings/MiuiDropDownPreference;

    iget-object v0, p0, Lcom/android/settings/notification/ZenModePrioritySettings;->fD:Lcom/android/settings/MiuiDropDownPreference;

    invoke-static {v0}, Lcom/android/settings/notification/ZenModePrioritySettings;->gQ(Lcom/android/settings/MiuiDropDownPreference;)V

    iget-object v0, p0, Lcom/android/settings/notification/ZenModePrioritySettings;->fD:Lcom/android/settings/MiuiDropDownPreference;

    new-instance v2, Lcom/android/settings/notification/ZenModePrioritySettings$3;

    invoke-direct {v2, p0}, Lcom/android/settings/notification/ZenModePrioritySettings$3;-><init>(Lcom/android/settings/notification/ZenModePrioritySettings;)V

    invoke-virtual {v0, v2}, Lcom/android/settings/MiuiDropDownPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    const-string/jumbo v0, "calls"

    invoke-virtual {v1, v0}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Lcom/android/settings/MiuiDropDownPreference;

    iput-object v0, p0, Lcom/android/settings/notification/ZenModePrioritySettings;->fA:Lcom/android/settings/MiuiDropDownPreference;

    iget-object v0, p0, Lcom/android/settings/notification/ZenModePrioritySettings;->fA:Lcom/android/settings/MiuiDropDownPreference;

    invoke-static {v0}, Lcom/android/settings/notification/ZenModePrioritySettings;->gQ(Lcom/android/settings/MiuiDropDownPreference;)V

    iget-object v0, p0, Lcom/android/settings/notification/ZenModePrioritySettings;->fA:Lcom/android/settings/MiuiDropDownPreference;

    new-instance v2, Lcom/android/settings/notification/ZenModePrioritySettings$4;

    invoke-direct {v2, p0}, Lcom/android/settings/notification/ZenModePrioritySettings$4;-><init>(Lcom/android/settings/notification/ZenModePrioritySettings;)V

    invoke-virtual {v0, v2}, Lcom/android/settings/MiuiDropDownPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    const-string/jumbo v0, "repeat_callers"

    invoke-virtual {v1, v0}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/SwitchPreference;

    iput-object v0, p0, Lcom/android/settings/notification/ZenModePrioritySettings;->fG:Landroid/preference/SwitchPreference;

    iget-object v0, p0, Lcom/android/settings/notification/ZenModePrioritySettings;->fG:Landroid/preference/SwitchPreference;

    iget-object v1, p0, Lcom/android/settings/notification/ZenModePrioritySettings;->mContext:Landroid/content/Context;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/android/settings/notification/ZenModePrioritySettings;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x10e00db

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    const/4 v4, 0x0

    aput-object v3, v2, v4

    const v3, 0x7f1216bd

    invoke-virtual {v1, v3, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/SwitchPreference;->setSummary(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/android/settings/notification/ZenModePrioritySettings;->fG:Landroid/preference/SwitchPreference;

    new-instance v1, Lcom/android/settings/notification/ZenModePrioritySettings$5;

    invoke-direct {v1, p0}, Lcom/android/settings/notification/ZenModePrioritySettings$5;-><init>(Lcom/android/settings/notification/ZenModePrioritySettings;)V

    invoke-virtual {v0, v1}, Landroid/preference/SwitchPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    invoke-direct {p0}, Lcom/android/settings/notification/ZenModePrioritySettings;->gU()V

    return-void
.end method
