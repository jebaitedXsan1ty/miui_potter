.class public abstract Lcom/android/settings/notification/SettingPrefController;
.super Lcom/android/settings/core/c;
.source "SettingPrefController.java"

# interfaces
.implements Lcom/android/settings/core/lifecycle/b;
.implements Lcom/android/settings/core/lifecycle/a/b;
.implements Lcom/android/settings/core/lifecycle/a/d;


# instance fields
.field protected a:Lcom/android/settings/notification/SettingPref;

.field protected b:Lcom/android/settings/notification/SettingPrefController$SettingsObserver;


# direct methods
.method static synthetic d(Lcom/android/settings/notification/SettingPrefController;)Landroid/content/Context;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/notification/SettingPrefController;->mContext:Landroid/content/Context;

    return-object v0
.end method


# virtual methods
.method public a(Landroid/support/v7/preference/PreferenceScreen;)V
    .locals 1

    invoke-virtual {p0}, Lcom/android/settings/notification/SettingPrefController;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Lcom/android/settings/notification/SettingPrefController$SettingsObserver;

    invoke-direct {v0, p0}, Lcom/android/settings/notification/SettingPrefController$SettingsObserver;-><init>(Lcom/android/settings/notification/SettingPrefController;)V

    iput-object v0, p0, Lcom/android/settings/notification/SettingPrefController;->b:Lcom/android/settings/notification/SettingPrefController$SettingsObserver;

    :cond_0
    return-void
.end method

.method public b()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/notification/SettingPrefController;->a:Lcom/android/settings/notification/SettingPref;

    invoke-virtual {v0}, Lcom/android/settings/notification/SettingPref;->getKey()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public c()Z
    .locals 2

    iget-object v0, p0, Lcom/android/settings/notification/SettingPrefController;->a:Lcom/android/settings/notification/SettingPref;

    iget-object v1, p0, Lcom/android/settings/notification/SettingPrefController;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/android/settings/notification/SettingPref;->fe(Landroid/content/Context;)Z

    move-result v0

    return v0
.end method

.method public onPause()V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/notification/SettingPrefController;->b:Lcom/android/settings/notification/SettingPrefController$SettingsObserver;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/notification/SettingPrefController;->b:Lcom/android/settings/notification/SettingPrefController$SettingsObserver;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/android/settings/notification/SettingPrefController$SettingsObserver;->e(Z)V

    :cond_0
    return-void
.end method

.method public onResume()V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/notification/SettingPrefController;->b:Lcom/android/settings/notification/SettingPrefController$SettingsObserver;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/notification/SettingPrefController;->b:Lcom/android/settings/notification/SettingPrefController$SettingsObserver;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/android/settings/notification/SettingPrefController$SettingsObserver;->e(Z)V

    :cond_0
    return-void
.end method
