.class final Lcom/android/settings/notification/ChannelNotificationSettings$2;
.super Ljava/lang/Object;
.source "ChannelNotificationSettings.java"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceChangeListener;


# instance fields
.field final synthetic gB:Lcom/android/settings/notification/ChannelNotificationSettings;


# direct methods
.method constructor <init>(Lcom/android/settings/notification/ChannelNotificationSettings;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/notification/ChannelNotificationSettings$2;->gB:Lcom/android/settings/notification/ChannelNotificationSettings;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 4

    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iget-object v1, p0, Lcom/android/settings/notification/ChannelNotificationSettings$2;->gB:Lcom/android/settings/notification/ChannelNotificationSettings;

    iget-object v1, v1, Lcom/android/settings/notification/ChannelNotificationSettings;->eh:Landroid/app/NotificationChannel;

    invoke-virtual {v1, v0}, Landroid/app/NotificationChannel;->setShowBadge(Z)V

    iget-object v0, p0, Lcom/android/settings/notification/ChannelNotificationSettings$2;->gB:Lcom/android/settings/notification/ChannelNotificationSettings;

    iget-object v0, v0, Lcom/android/settings/notification/ChannelNotificationSettings;->eh:Landroid/app/NotificationChannel;

    const/16 v1, 0x80

    invoke-virtual {v0, v1}, Landroid/app/NotificationChannel;->lockFields(I)V

    iget-object v0, p0, Lcom/android/settings/notification/ChannelNotificationSettings$2;->gB:Lcom/android/settings/notification/ChannelNotificationSettings;

    iget-object v0, v0, Lcom/android/settings/notification/ChannelNotificationSettings;->ef:Lcom/android/settings/notification/NotificationBackend;

    iget-object v1, p0, Lcom/android/settings/notification/ChannelNotificationSettings$2;->gB:Lcom/android/settings/notification/ChannelNotificationSettings;

    iget-object v1, v1, Lcom/android/settings/notification/ChannelNotificationSettings;->ec:Ljava/lang/String;

    iget-object v2, p0, Lcom/android/settings/notification/ChannelNotificationSettings$2;->gB:Lcom/android/settings/notification/ChannelNotificationSettings;

    iget v2, v2, Lcom/android/settings/notification/ChannelNotificationSettings;->eb:I

    iget-object v3, p0, Lcom/android/settings/notification/ChannelNotificationSettings$2;->gB:Lcom/android/settings/notification/ChannelNotificationSettings;

    iget-object v3, v3, Lcom/android/settings/notification/ChannelNotificationSettings;->eh:Landroid/app/NotificationChannel;

    invoke-virtual {v0, v1, v2, v3}, Lcom/android/settings/notification/NotificationBackend;->bu(Ljava/lang/String;ILandroid/app/NotificationChannel;)V

    const/4 v0, 0x1

    return v0
.end method
