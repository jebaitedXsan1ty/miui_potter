.class public Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;
.super Lcom/android/settings/KeyguardSettingsPreferenceFragment;
.source "SetUpChooseLockPattern.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# static fields
.field private static final synthetic bNr:[I


# instance fields
.field private final bMZ:Ljava/util/List;

.field protected bNa:Landroid/security/ChooseLockSettingsHelper;

.field protected bNb:Lcom/android/settings/h;

.field protected bNc:Ljava/util/List;

.field private bNd:Ljava/lang/Runnable;

.field private bNe:Z

.field private bNf:Lcom/android/settings/bM;

.field private bNg:Landroid/widget/TextView;

.field private bNh:Landroid/widget/TextView;

.field private bNi:Landroid/widget/TextView;

.field private bNj:Landroid/os/Handler;

.field protected bNk:Landroid/widget/TextView;

.field private bNl:Z

.field protected bNm:Lcom/android/settings/LockPatternView;

.field private bNn:Landroid/os/AsyncTask;

.field private bNo:I

.field private bNp:Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$Stage;

.field private bNq:I


# direct methods
.method public constructor <init>()V
    .locals 5

    const/4 v1, 0x0

    const/4 v4, 0x2

    const/4 v3, 0x0

    const/4 v2, 0x1

    invoke-direct {p0}, Lcom/android/settings/KeyguardSettingsPreferenceFragment;-><init>()V

    iput-object v1, p0, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;->bNc:Ljava/util/List;

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;->bNj:Landroid/os/Handler;

    iput-object v1, p0, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;->bNf:Lcom/android/settings/bM;

    const/4 v0, 0x4

    new-array v0, v0, [Lcom/android/internal/widget/LockPatternView$Cell;

    invoke-static {v3, v3}, Lcom/android/internal/widget/LockPatternView$Cell;->of(II)Lcom/android/internal/widget/LockPatternView$Cell;

    move-result-object v1

    aput-object v1, v0, v3

    invoke-static {v3, v2}, Lcom/android/internal/widget/LockPatternView$Cell;->of(II)Lcom/android/internal/widget/LockPatternView$Cell;

    move-result-object v1

    aput-object v1, v0, v2

    invoke-static {v2, v2}, Lcom/android/internal/widget/LockPatternView$Cell;->of(II)Lcom/android/internal/widget/LockPatternView$Cell;

    move-result-object v1

    aput-object v1, v0, v4

    invoke-static {v4, v2}, Lcom/android/internal/widget/LockPatternView$Cell;->of(II)Lcom/android/internal/widget/LockPatternView$Cell;

    move-result-object v1

    const/4 v2, 0x3

    aput-object v1, v0, v2

    invoke-static {v0}, Lcom/google/android/collect/Lists;->newArrayList([Ljava/lang/Object;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;->bMZ:Ljava/util/List;

    new-instance v0, Lcom/android/settings/hR;

    invoke-direct {v0, p0}, Lcom/android/settings/hR;-><init>(Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;)V

    iput-object v0, p0, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;->bNb:Lcom/android/settings/h;

    sget-object v0, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$Stage;->bNL:Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$Stage;

    iput-object v0, p0, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;->bNp:Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$Stage;

    new-instance v0, Lcom/android/settings/hT;

    invoke-direct {v0, p0}, Lcom/android/settings/hT;-><init>(Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;)V

    iput-object v0, p0, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;->bNd:Ljava/lang/Runnable;

    return-void
.end method

.method private bEA([B)V
    .locals 4

    const/4 v3, 0x0

    invoke-virtual {p0}, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    if-eqz v0, :cond_0

    const-string/jumbo v1, "has_challenge"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0, p1}, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;->bEx([B)V

    return-void

    :cond_0
    invoke-direct {p0, v3}, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;->bEx([B)V

    :cond_1
    return-void
.end method

.method private bEC(I)V
    .locals 3

    invoke-virtual {p0}, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;->getTargetFragment()Landroid/app/Fragment;

    move-result-object v0

    if-eqz v0, :cond_0

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string/jumbo v1, "miui_security_fragment_result"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    invoke-virtual {p0}, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;->getTargetFragment()Landroid/app/Fragment;

    move-result-object v1

    invoke-virtual {p0}, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;->getTargetRequestCode()I

    move-result v2

    invoke-static {v1, v2, v0}, Lcom/android/settings/aN;->bwR(Landroid/app/Fragment;ILandroid/os/Bundle;)V

    :cond_0
    return-void
.end method

.method static synthetic bEE(Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;)Ljava/lang/Runnable;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;->bNd:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic bEF(Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;)Lcom/android/settings/bM;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;->bNf:Lcom/android/settings/bM;

    return-object v0
.end method

.method static synthetic bEG(Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;)Landroid/widget/TextView;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;->bNh:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic bEH(Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;)Landroid/widget/TextView;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;->bNi:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic bEI(Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;)Landroid/os/AsyncTask;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;->bNn:Landroid/os/AsyncTask;

    return-object v0
.end method

.method static synthetic bEJ(Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;)I
    .locals 1

    iget v0, p0, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;->bNo:I

    return v0
.end method

.method static synthetic bEK(Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;)Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$Stage;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;->bNp:Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$Stage;

    return-object v0
.end method

.method static synthetic bEL(Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;)I
    .locals 1

    iget v0, p0, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;->bNq:I

    return v0
.end method

.method static synthetic bEM(Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;Landroid/os/AsyncTask;)Landroid/os/AsyncTask;
    .locals 0

    iput-object p1, p0, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;->bNn:Landroid/os/AsyncTask;

    return-object p1
.end method

.method static synthetic bEN(Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;[B)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;->bEA([B)V

    return-void
.end method

.method private static synthetic bEO()[I
    .locals 3

    sget-object v0, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;->bNr:[I

    if-eqz v0, :cond_0

    sget-object v0, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;->bNr:[I

    return-object v0

    :cond_0
    invoke-static {}, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$Stage;->values()[Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$Stage;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$Stage;->bNG:Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$Stage;

    invoke-virtual {v1}, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$Stage;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_9

    :goto_0
    :try_start_1
    sget-object v1, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$Stage;->bNH:Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$Stage;

    invoke-virtual {v1}, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$Stage;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_8

    :goto_1
    :try_start_2
    sget-object v1, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$Stage;->bNI:Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$Stage;

    invoke-virtual {v1}, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$Stage;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_7

    :goto_2
    :try_start_3
    sget-object v1, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$Stage;->bNJ:Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$Stage;

    invoke-virtual {v1}, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$Stage;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_6

    :goto_3
    :try_start_4
    sget-object v1, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$Stage;->bNK:Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$Stage;

    invoke-virtual {v1}, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$Stage;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_5

    :goto_4
    :try_start_5
    sget-object v1, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$Stage;->bNL:Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$Stage;

    invoke-virtual {v1}, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$Stage;->ordinal()I

    move-result v1

    const/4 v2, 0x6

    aput v2, v0, v1
    :try_end_5
    .catch Ljava/lang/NoSuchFieldError; {:try_start_5 .. :try_end_5} :catch_4

    :goto_5
    :try_start_6
    sget-object v1, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$Stage;->bNM:Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$Stage;

    invoke-virtual {v1}, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$Stage;->ordinal()I

    move-result v1

    const/4 v2, 0x7

    aput v2, v0, v1
    :try_end_6
    .catch Ljava/lang/NoSuchFieldError; {:try_start_6 .. :try_end_6} :catch_3

    :goto_6
    :try_start_7
    sget-object v1, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$Stage;->bNN:Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$Stage;

    invoke-virtual {v1}, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$Stage;->ordinal()I

    move-result v1

    const/16 v2, 0x8

    aput v2, v0, v1
    :try_end_7
    .catch Ljava/lang/NoSuchFieldError; {:try_start_7 .. :try_end_7} :catch_2

    :goto_7
    :try_start_8
    sget-object v1, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$Stage;->bNO:Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$Stage;

    invoke-virtual {v1}, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$Stage;->ordinal()I

    move-result v1

    const/16 v2, 0x9

    aput v2, v0, v1
    :try_end_8
    .catch Ljava/lang/NoSuchFieldError; {:try_start_8 .. :try_end_8} :catch_1

    :goto_8
    :try_start_9
    sget-object v1, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$Stage;->bNP:Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$Stage;

    invoke-virtual {v1}, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$Stage;->ordinal()I

    move-result v1

    const/16 v2, 0xa

    aput v2, v0, v1
    :try_end_9
    .catch Ljava/lang/NoSuchFieldError; {:try_start_9 .. :try_end_9} :catch_0

    :goto_9
    sput-object v0, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;->bNr:[I

    return-object v0

    :catch_0
    move-exception v1

    goto :goto_9

    :catch_1
    move-exception v1

    goto :goto_8

    :catch_2
    move-exception v1

    goto :goto_7

    :catch_3
    move-exception v1

    goto :goto_6

    :catch_4
    move-exception v1

    goto :goto_5

    :catch_5
    move-exception v1

    goto :goto_4

    :catch_6
    move-exception v1

    goto :goto_3

    :catch_7
    move-exception v1

    goto :goto_2

    :catch_8
    move-exception v1

    goto :goto_1

    :catch_9
    move-exception v1

    goto :goto_0
.end method

.method private bEx([B)V
    .locals 3

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    if-eqz p1, :cond_0

    const-string/jumbo v1, "hw_auth_token"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[B)Landroid/content/Intent;

    :cond_0
    invoke-virtual {p0}, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    const/4 v2, -0x1

    invoke-virtual {v1, v2, v0}, Landroid/app/Activity;->setResult(ILandroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;->finish()V

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;->bEC(I)V

    return-void
.end method

.method private bEy()V
    .locals 4

    iget-object v0, p0, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;->bNm:Lcom/android/settings/LockPatternView;

    iget-object v1, p0, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;->bNd:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Lcom/android/settings/LockPatternView;->removeCallbacks(Ljava/lang/Runnable;)Z

    iget-object v0, p0, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;->bNm:Lcom/android/settings/LockPatternView;

    iget-object v1, p0, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;->bNd:Ljava/lang/Runnable;

    const-wide/16 v2, 0x7d0

    invoke-virtual {v0, v1, v2, v3}, Lcom/android/settings/LockPatternView;->postDelayed(Ljava/lang/Runnable;J)Z

    return-void
.end method


# virtual methods
.method public QH(ILandroid/os/Bundle;)V
    .locals 3

    const/4 v1, 0x0

    if-eqz p2, :cond_2

    const-string/jumbo v0, "miui_security_fragment_result"

    const/4 v2, -0x1

    invoke-virtual {p2, v0, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    packed-switch p1, :pswitch_data_0

    :cond_0
    :goto_1
    return-void

    :cond_1
    move v0, v1

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_0

    :pswitch_0
    if-nez v0, :cond_3

    invoke-virtual {p0}, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;->finish()V

    invoke-direct {p0, v1}, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;->bEC(I)V

    goto :goto_1

    :cond_3
    sget-object v0, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$Stage;->bNL:Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$Stage;

    invoke-virtual {p0, v0}, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;->bED(Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$Stage;)V

    goto :goto_1

    :pswitch_1
    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;->finish()V

    invoke-direct {p0, v1}, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;->bEC(I)V

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x37
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public Vg(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 4

    const/4 v3, 0x0

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;->getActionBar()Lmiui/app/ActionBar;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ActionBar;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Landroid/app/ActionBar;->hide()V

    :cond_0
    invoke-virtual {p0}, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;->bEz()V

    const v0, 0x7f0d01c6

    invoke-virtual {p1, v0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    const v0, 0x7f0a03e8

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;->bNk:Landroid/widget/TextView;

    const v0, 0x7f0a03e9

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/settings/LockPatternView;

    iput-object v0, p0, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;->bNm:Lcom/android/settings/LockPatternView;

    iget-object v0, p0, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;->bNm:Lcom/android/settings/LockPatternView;

    iget-object v2, p0, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;->bNb:Lcom/android/settings/h;

    invoke-virtual {v0, v2}, Lcom/android/settings/LockPatternView;->setOnPatternListener(Lcom/android/settings/h;)V

    iget-object v0, p0, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;->bNm:Lcom/android/settings/LockPatternView;

    iget-object v2, p0, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;->bNa:Landroid/security/ChooseLockSettingsHelper;

    invoke-virtual {v2}, Landroid/security/ChooseLockSettingsHelper;->utils()Landroid/security/MiuiLockPatternUtils;

    move-result-object v2

    invoke-virtual {v2}, Landroid/security/MiuiLockPatternUtils;->isTactileFeedbackEnabled()Z

    move-result v2

    invoke-virtual {v0, v2}, Lcom/android/settings/LockPatternView;->setTactileFeedbackEnabled(Z)V

    const v0, 0x7f0a00a5

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;->bNg:Landroid/widget/TextView;

    const v0, 0x7f0a00ac

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;->bNi:Landroid/widget/TextView;

    const v0, 0x7f0a00aa

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;->bNh:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;->bNg:Landroid/widget/TextView;

    invoke-virtual {v0, p0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;->bNi:Landroid/widget/TextView;

    invoke-virtual {v0, p0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;->bNh:Landroid/widget/TextView;

    invoke-virtual {v0, p0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;->bNi:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;->bNi:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setEnabled(Z)V

    iget-object v0, p0, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;->bNh:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setEnabled(Z)V

    const v0, 0x7f0a03f0

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/internal/widget/LinearLayoutWithDefaultTouchRecepient;

    iget-object v2, p0, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;->bNm:Lcom/android/settings/LockPatternView;

    invoke-virtual {v0, v2}, Lcom/android/internal/widget/LinearLayoutWithDefaultTouchRecepient;->setDefaultTouchRecepient(Landroid/view/View;)V

    if-nez p3, :cond_2

    invoke-virtual {p0}, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;->getTargetFragment()Landroid/app/Fragment;

    move-result-object v0

    if-nez v0, :cond_1

    iget v0, p0, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;->bNq:I

    const/16 v2, 0x37

    invoke-static {p0, v2, v0}, Lcom/android/settings/SetUpChooseLockPassword;->bYW(Lcom/android/settings/KeyguardSettingsPreferenceFragment;II)V

    :cond_1
    invoke-virtual {p0}, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;->bEw()V

    :goto_0
    invoke-virtual {p0}, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/high16 v2, 0x20000

    invoke-virtual {v0, v2}, Landroid/view/Window;->addFlags(I)V

    return-object v1

    :cond_2
    const-string/jumbo v0, "chosenPattern"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-static {v0}, Lcom/android/internal/widget/LockPatternUtils;->stringToPattern(Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;->bNc:Ljava/util/List;

    :cond_3
    invoke-static {}, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$Stage;->values()[Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$Stage;

    move-result-object v0

    const-string/jumbo v2, "uiStage"

    invoke-virtual {p3, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    aget-object v0, v0, v2

    invoke-virtual {p0, v0}, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;->bED(Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$Stage;)V

    goto :goto_0
.end method

.method protected bEB()V
    .locals 7

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;->bNa:Landroid/security/ChooseLockSettingsHelper;

    invoke-virtual {v0}, Landroid/security/ChooseLockSettingsHelper;->utils()Landroid/security/MiuiLockPatternUtils;

    move-result-object v3

    iget v0, p0, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;->bNq:I

    invoke-static {v3, v0}, Lcom/android/settings/bn;->bFi(Lcom/android/internal/widget/LockPatternUtils;I)Z

    move-result v0

    xor-int/lit8 v4, v0, 0x1

    invoke-virtual {p0}, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;->bWB()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v5, "is_security_encryption_enabled"

    invoke-static {v0, v5, v2}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    if-ne v0, v1, :cond_3

    move v0, v1

    :goto_0
    sget v5, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v6, 0x17

    if-ge v5, v6, :cond_4

    iget v5, p0, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;->bNq:I

    invoke-static {v3, v5, v2}, Lcom/android/settings/bn;->bFj(Lcom/android/internal/widget/LockPatternUtils;IZ)V

    :goto_1
    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v5, "use_lock_password_to_encrypt_device"

    invoke-virtual {v0, v5, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    invoke-static {v3, v1}, Lcom/android/settings/bn;->bFk(Lcom/android/internal/widget/LockPatternUtils;Z)V

    :cond_1
    iget-object v0, p0, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;->bNg:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setEnabled(Z)V

    iget-object v0, p0, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;->bNi:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setEnabled(Z)V

    iget-object v0, p0, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;->bNh:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setEnabled(Z)V

    invoke-virtual {p0}, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v5, "lockscreen.biometric_weak_fallback"

    invoke-virtual {v0, v5, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v4, :cond_2

    iget v4, p0, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;->bNq:I

    invoke-static {v3, v1, v4}, Lcom/android/settings/bn;->bFl(Lcom/android/internal/widget/LockPatternUtils;ZI)V

    :cond_2
    new-instance v4, Lcom/android/settings/bM;

    invoke-virtual {p0}, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;->getActivity()Landroid/app/Activity;

    move-result-object v5

    invoke-direct {v4, v5}, Lcom/android/settings/bM;-><init>(Landroid/content/Context;)V

    iput-object v4, p0, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;->bNf:Lcom/android/settings/bM;

    iget-object v4, p0, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;->bNf:Lcom/android/settings/bM;

    invoke-virtual {v4}, Lcom/android/settings/bM;->bNd()Z

    move-result v4

    if-eqz v4, :cond_5

    invoke-static {}, Lcom/android/settings/SetUpChooseLockPattern;->-get0()Z

    move-result v4

    if-eqz v4, :cond_5

    iget-object v4, p0, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;->bNf:Lcom/android/settings/bM;

    invoke-virtual {v4}, Lcom/android/settings/bM;->bNe()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    if-nez v4, :cond_5

    :goto_2
    new-instance v4, Lcom/android/settings/hV;

    invoke-direct {v4, p0, v3, v0, v1}, Lcom/android/settings/hV;-><init>(Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;Lcom/android/internal/widget/LockPatternUtils;ZZ)V

    new-array v0, v2, [Ljava/lang/Void;

    invoke-virtual {v4, v0}, Lcom/android/settings/hV;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    return-void

    :cond_3
    move v0, v2

    goto :goto_0

    :cond_4
    invoke-static {v3, v2}, Lcom/android/settings/bn;->bFk(Lcom/android/internal/widget/LockPatternUtils;Z)V

    goto :goto_1

    :cond_5
    move v1, v2

    goto :goto_2
.end method

.method protected bED(Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$Stage;)V
    .locals 6

    const/4 v5, 0x0

    iput-object p1, p0, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;->bNp:Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$Stage;

    sget-object v0, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$Stage;->bNH:Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$Stage;

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;->bNk:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    iget v2, p1, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$Stage;->headerMessage:I

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_0
    iget-object v0, p1, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$Stage;->leftMode:Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$LeftButtonMode;

    sget-object v1, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$LeftButtonMode;->bNu:Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$LeftButtonMode;

    if-ne v0, v1, :cond_4

    iget-object v0, p0, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;->bNi:Landroid/widget/TextView;

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setEnabled(Z)V

    :goto_1
    iget-object v0, p1, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$Stage;->rightMode:Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$RightButtonMode;

    sget-object v1, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$RightButtonMode;->bNC:Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$RightButtonMode;

    if-ne v0, v1, :cond_5

    iget-object v0, p0, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;->bNh:Landroid/widget/TextView;

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setEnabled(Z)V

    :goto_2
    iget-boolean v0, p1, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$Stage;->patternEnabled:Z

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;->bNm:Lcom/android/settings/LockPatternView;

    invoke-virtual {v0}, Lcom/android/settings/LockPatternView;->bfP()V

    :goto_3
    iget-object v0, p0, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;->bNm:Lcom/android/settings/LockPatternView;

    sget-object v1, Lcom/android/settings/LockPatternView$DisplayMode;->btG:Lcom/android/settings/LockPatternView$DisplayMode;

    invoke-virtual {v0, v1}, Lcom/android/settings/LockPatternView;->setDisplayMode(Lcom/android/settings/LockPatternView$DisplayMode;)V

    invoke-static {}, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;->bEO()[I

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;->bNp:Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$Stage;

    invoke-virtual {v1}, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$Stage;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    :goto_4
    :pswitch_0
    return-void

    :cond_0
    sget-object v0, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$Stage;->bNO:Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$Stage;

    if-ne p1, v0, :cond_1

    iget-object v0, p0, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;->bNk:Landroid/widget/TextView;

    const v1, 0x7f1209cf

    invoke-virtual {p0, v1}, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_1
    sget-object v0, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$Stage;->bNP:Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$Stage;

    if-ne p1, v0, :cond_2

    iget-object v0, p0, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;->bNk:Landroid/widget/TextView;

    const v1, 0x7f1209d0

    invoke-virtual {p0, v1}, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_2
    sget-object v0, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$Stage;->bNN:Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$Stage;

    if-ne p1, v0, :cond_3

    iget-object v0, p0, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;->bNk:Landroid/widget/TextView;

    const v1, 0x7f1209ce

    invoke-virtual {p0, v1}, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;->bNk:Landroid/widget/TextView;

    iget v1, p1, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$Stage;->headerMessage:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0

    :cond_4
    iget-object v0, p0, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;->bNi:Landroid/widget/TextView;

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;->bNi:Landroid/widget/TextView;

    iget-object v1, p1, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$Stage;->leftMode:Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$LeftButtonMode;

    iget v1, v1, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$LeftButtonMode;->text:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    iget-object v0, p0, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;->bNi:Landroid/widget/TextView;

    iget-object v1, p1, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$Stage;->leftMode:Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$LeftButtonMode;

    iget-boolean v1, v1, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$LeftButtonMode;->enabled:Z

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setEnabled(Z)V

    goto :goto_1

    :cond_5
    iget-object v0, p0, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;->bNh:Landroid/widget/TextView;

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;->bNh:Landroid/widget/TextView;

    iget-object v1, p1, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$Stage;->rightMode:Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$RightButtonMode;

    iget v1, v1, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$RightButtonMode;->text:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    iget-object v0, p0, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;->bNh:Landroid/widget/TextView;

    iget-object v1, p1, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$Stage;->rightMode:Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$RightButtonMode;

    iget-boolean v1, v1, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$RightButtonMode;->enabled:Z

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setEnabled(Z)V

    goto/16 :goto_2

    :cond_6
    iget-object v0, p0, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;->bNm:Lcom/android/settings/LockPatternView;

    invoke-virtual {v0}, Lcom/android/settings/LockPatternView;->bfN()V

    goto/16 :goto_3

    :pswitch_1
    iget-object v0, p0, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;->bNm:Lcom/android/settings/LockPatternView;

    invoke-virtual {v0}, Lcom/android/settings/LockPatternView;->bfK()V

    goto :goto_4

    :pswitch_2
    iget-object v0, p0, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;->bNm:Lcom/android/settings/LockPatternView;

    sget-object v1, Lcom/android/settings/LockPatternView$DisplayMode;->btF:Lcom/android/settings/LockPatternView$DisplayMode;

    iget-object v2, p0, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;->bMZ:Ljava/util/List;

    invoke-virtual {v0, v1, v2}, Lcom/android/settings/LockPatternView;->setPattern(Lcom/android/settings/LockPatternView$DisplayMode;Ljava/util/List;)V

    goto/16 :goto_4

    :pswitch_3
    iget-object v0, p0, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;->bNm:Lcom/android/settings/LockPatternView;

    sget-object v1, Lcom/android/settings/LockPatternView$DisplayMode;->btH:Lcom/android/settings/LockPatternView$DisplayMode;

    invoke-virtual {v0, v1}, Lcom/android/settings/LockPatternView;->setDisplayMode(Lcom/android/settings/LockPatternView$DisplayMode;)V

    invoke-direct {p0}, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;->bEy()V

    goto/16 :goto_4

    :pswitch_4
    iget-object v0, p0, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;->bNm:Lcom/android/settings/LockPatternView;

    new-instance v1, Lcom/android/settings/hU;

    invoke-direct {v1, p0}, Lcom/android/settings/hU;-><init>(Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;)V

    const-wide/16 v2, 0x1f4

    invoke-virtual {v0, v1, v2, v3}, Lcom/android/settings/LockPatternView;->postDelayed(Ljava/lang/Runnable;J)Z

    goto/16 :goto_4

    :pswitch_5
    iget-object v0, p0, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;->bNm:Lcom/android/settings/LockPatternView;

    invoke-virtual {v0}, Lcom/android/settings/LockPatternView;->bfK()V

    goto/16 :goto_4

    :pswitch_6
    iget-object v0, p0, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;->bNm:Lcom/android/settings/LockPatternView;

    sget-object v1, Lcom/android/settings/LockPatternView$DisplayMode;->btH:Lcom/android/settings/LockPatternView$DisplayMode;

    invoke-virtual {v0, v1}, Lcom/android/settings/LockPatternView;->setDisplayMode(Lcom/android/settings/LockPatternView$DisplayMode;)V

    invoke-direct {p0}, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;->bEy()V

    goto/16 :goto_4

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_3
        :pswitch_6
        :pswitch_4
        :pswitch_2
        :pswitch_1
        :pswitch_5
        :pswitch_3
        :pswitch_3
        :pswitch_3
    .end packed-switch
.end method

.method protected bEv(Landroid/content/Context;)Ljava/util/List;
    .locals 1

    const-string/jumbo v0, "user"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/UserManager;

    invoke-virtual {v0}, Landroid/os/UserManager;->getUsers()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method protected bEw()V
    .locals 1

    sget-object v0, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$Stage;->bNL:Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$Stage;

    invoke-virtual {p0, v0}, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;->bED(Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$Stage;)V

    return-void
.end method

.method protected bEz()V
    .locals 2

    sget-object v0, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$Stage;->bNL:Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$Stage;

    const v1, 0x7f1209d4

    iput v1, v0, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$Stage;->headerMessage:I

    return-void
.end method

.method public cC()Ljava/lang/String;
    .locals 1

    const-class v0, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 2

    const/4 v1, -0x1

    invoke-super {p0, p1, p2, p3}, Lcom/android/settings/KeyguardSettingsPreferenceFragment;->onActivityResult(IILandroid/content/Intent;)V

    packed-switch p1, :pswitch_data_0

    :cond_0
    :goto_0
    return-void

    :pswitch_0
    if-eq p2, v1, :cond_1

    invoke-virtual {p0}, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/Activity;->setResult(I)V

    invoke-virtual {p0}, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    :cond_1
    sget-object v0, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$Stage;->bNL:Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$Stage;

    invoke-virtual {p0, v0}, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;->bED(Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$Stage;)V

    goto :goto_0

    :pswitch_1
    if-ne p2, v1, :cond_0

    invoke-virtual {p0}, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/app/Activity;->setResult(I)V

    invoke-virtual {p0}, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x37
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onClick(Landroid/view/View;)V
    .locals 3

    iget-object v0, p0, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;->bNi:Landroid/widget/TextView;

    if-ne p1, v0, :cond_3

    iget-object v0, p0, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;->bNp:Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$Stage;

    iget-object v0, v0, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$Stage;->leftMode:Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$LeftButtonMode;

    sget-object v1, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$LeftButtonMode;->bNv:Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$LeftButtonMode;

    if-ne v0, v1, :cond_1

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;->bNc:Ljava/util/List;

    iget-object v0, p0, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;->bNm:Lcom/android/settings/LockPatternView;

    invoke-virtual {v0}, Lcom/android/settings/LockPatternView;->bfK()V

    sget-object v0, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$Stage;->bNL:Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$Stage;

    invoke-virtual {p0, v0}, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;->bED(Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$Stage;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;->bNp:Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$Stage;

    iget-object v0, v0, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$Stage;->leftMode:Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$LeftButtonMode;

    sget-object v1, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$LeftButtonMode;->bNs:Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$LeftButtonMode;

    if-ne v0, v1, :cond_2

    invoke-virtual {p0}, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/Activity;->setResult(I)V

    invoke-virtual {p0}, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    goto :goto_0

    :cond_2
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "left footer button pressed, but stage of "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;->bNp:Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$Stage;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " doesn\'t make sense"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_3
    iget-object v0, p0, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;->bNh:Landroid/widget/TextView;

    if-ne p1, v0, :cond_9

    iget-object v0, p0, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;->bNp:Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$Stage;

    iget-object v0, v0, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$Stage;->rightMode:Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$RightButtonMode;

    sget-object v1, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$RightButtonMode;->bNA:Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$RightButtonMode;

    if-ne v0, v1, :cond_5

    iget-object v0, p0, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;->bNp:Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$Stage;

    sget-object v1, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$Stage;->bNJ:Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$Stage;

    if-eq v0, v1, :cond_4

    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "expected ui stage "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$Stage;->bNJ:Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$Stage;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " when button is "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$RightButtonMode;->bNA:Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$RightButtonMode;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_4
    sget-object v0, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$Stage;->bNM:Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$Stage;

    invoke-virtual {p0, v0}, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;->bED(Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$Stage;)V

    goto :goto_0

    :cond_5
    iget-object v0, p0, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;->bNp:Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$Stage;

    iget-object v0, v0, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$Stage;->rightMode:Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$RightButtonMode;

    sget-object v1, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$RightButtonMode;->bNy:Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$RightButtonMode;

    if-ne v0, v1, :cond_7

    iget-object v0, p0, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;->bNp:Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$Stage;

    sget-object v1, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$Stage;->bNG:Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$Stage;

    if-eq v0, v1, :cond_6

    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "expected ui stage "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$Stage;->bNG:Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$Stage;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " when button is "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$RightButtonMode;->bNy:Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$RightButtonMode;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_6
    invoke-virtual {p0}, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;->bEB()V

    invoke-virtual {p0}, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/android/settings/aH;->bva(Landroid/content/Context;)V

    goto/16 :goto_0

    :cond_7
    iget-object v0, p0, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;->bNp:Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$Stage;

    iget-object v0, v0, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$Stage;->rightMode:Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$RightButtonMode;

    sget-object v1, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$RightButtonMode;->bND:Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$RightButtonMode;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;->bNp:Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$Stage;

    sget-object v1, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$Stage;->bNK:Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$Stage;

    if-eq v0, v1, :cond_8

    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Help screen is only mode with ok button, but stage is "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;->bNp:Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$Stage;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_8
    iget-object v0, p0, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;->bNm:Lcom/android/settings/LockPatternView;

    invoke-virtual {v0}, Lcom/android/settings/LockPatternView;->bfK()V

    iget-object v0, p0, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;->bNm:Lcom/android/settings/LockPatternView;

    sget-object v1, Lcom/android/settings/LockPatternView$DisplayMode;->btG:Lcom/android/settings/LockPatternView$DisplayMode;

    invoke-virtual {v0, v1}, Lcom/android/settings/LockPatternView;->setDisplayMode(Lcom/android/settings/LockPatternView$DisplayMode;)V

    sget-object v0, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$Stage;->bNL:Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$Stage;

    invoke-virtual {p0, v0}, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;->bED(Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$Stage;)V

    goto/16 :goto_0

    :cond_9
    iget-object v0, p0, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;->bNg:Landroid/widget/TextView;

    if-ne p1, v0, :cond_0

    invoke-virtual {p0}, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;->finish()V

    goto/16 :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 5

    const/4 v4, 0x1

    const/4 v3, 0x0

    const/16 v2, -0x2710

    invoke-super {p0, p1}, Lcom/android/settings/KeyguardSettingsPreferenceFragment;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "set_keyguard_password"

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;->bNe:Z

    invoke-virtual {p0}, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;->bWB()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v1, "second_user_id"

    invoke-static {v0, v1, v2, v3}, Landroid/provider/Settings$Secure;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v0

    iput v0, p0, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;->bNo:I

    invoke-virtual {p0}, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "user_id_to_set_password"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;->bNq:I

    iget v0, p0, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;->bNq:I

    if-ne v0, v2, :cond_2

    invoke-virtual {p0}, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {p0}, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/settings/aN;->bwP(Landroid/content/Context;Landroid/os/Bundle;)I

    move-result v0

    :goto_0
    iput v0, p0, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;->bNq:I

    iget v0, p0, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;->bNq:I

    if-nez v0, :cond_0

    iput-boolean v4, p0, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;->bNl:Z

    :cond_0
    new-instance v0, Landroid/security/ChooseLockSettingsHelper;

    invoke-virtual {p0}, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, v1, p0}, Landroid/security/ChooseLockSettingsHelper;-><init>(Landroid/app/Activity;Landroid/app/Fragment;)V

    iput-object v0, p0, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;->bNa:Landroid/security/ChooseLockSettingsHelper;

    invoke-virtual {p0}, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;->getTargetFragment()Landroid/app/Fragment;

    move-result-object v0

    invoke-static {v0}, Lcom/android/settings/aN;->bwQ(Landroid/app/Fragment;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget v0, p0, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;->bNq:I

    const/16 v1, 0x37

    invoke-static {p0, v1, v0}, Lcom/android/settings/SetUpChooseLockPassword;->bYW(Lcom/android/settings/KeyguardSettingsPreferenceFragment;II)V

    :cond_1
    return-void

    :cond_2
    iget v0, p0, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;->bNq:I

    goto :goto_0
.end method

.method public onPause()V
    .locals 3

    const/4 v2, 0x0

    invoke-super {p0}, Lcom/android/settings/KeyguardSettingsPreferenceFragment;->onPause()V

    iget-object v0, p0, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;->bNn:Landroid/os/AsyncTask;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;->bNn:Landroid/os/AsyncTask;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/AsyncTask;->cancel(Z)Z

    iput-object v2, p0, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;->bNn:Landroid/os/AsyncTask;

    :cond_0
    return-void
.end method

.method public onResume()V
    .locals 0

    invoke-super {p0}, Lcom/android/settings/KeyguardSettingsPreferenceFragment;->onResume()V

    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Lcom/android/settings/KeyguardSettingsPreferenceFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    const-string/jumbo v0, "uiStage"

    iget-object v1, p0, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;->bNp:Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$Stage;

    invoke-virtual {v1}, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$Stage;->ordinal()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    iget-object v0, p0, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;->bNc:Ljava/util/List;

    if-eqz v0, :cond_0

    const-string/jumbo v0, "chosenPattern"

    iget-object v1, p0, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;->bNc:Ljava/util/List;

    invoke-static {v1}, Lcom/android/internal/widget/LockPatternUtils;->patternToString(Ljava/util/List;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public onStart()V
    .locals 0

    invoke-super {p0}, Lcom/android/settings/KeyguardSettingsPreferenceFragment;->onStart()V

    invoke-static {p0}, Lcom/android/settings/bn;->bFg(Landroid/app/Fragment;)V

    return-void
.end method

.method public onStop()V
    .locals 0

    invoke-static {p0}, Lcom/android/settings/bn;->bFh(Landroid/app/Fragment;)V

    invoke-super {p0}, Lcom/android/settings/KeyguardSettingsPreferenceFragment;->onStop()V

    return-void
.end method
