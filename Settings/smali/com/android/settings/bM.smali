.class public Lcom/android/settings/bM;
.super Ljava/lang/Object;
.source "FingerprintHelper.java"


# instance fields
.field private bTS:Landroid/os/CancellationSignal;

.field private bTT:Z

.field private bTU:Z

.field private bTV:Landroid/hardware/fingerprint/FingerprintManager;

.field private final mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/settings/bM;->bTT:Z

    iput-object p1, p0, Lcom/android/settings/bM;->mContext:Landroid/content/Context;

    return-void
.end method

.method private bNl()V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/bM;->bTV:Landroid/hardware/fingerprint/FingerprintManager;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/bM;->mContext:Landroid/content/Context;

    const-string/jumbo v1, "fingerprint"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/fingerprint/FingerprintManager;

    iput-object v0, p0, Lcom/android/settings/bM;->bTV:Landroid/hardware/fingerprint/FingerprintManager;

    :cond_0
    return-void
.end method

.method private bNn(ILjava/util/List;Ljava/util/List;)V
    .locals 4

    invoke-interface {p3}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-interface {p2, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/android/settings/bM;->mContext:Landroid/content/Context;

    invoke-static {v1, p1}, Landroid/security/FingerprintIdUtils;->getUserFingerprintIds(Landroid/content/Context;I)Ljava/util/HashMap;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/util/HashMap;->size()I

    move-result v3

    if-nez v3, :cond_2

    :cond_1
    new-instance v1, Ljava/util/HashMap;

    const/4 v3, 0x1

    invoke-direct {v1, v3}, Ljava/util/HashMap;-><init>(I)V

    :cond_2
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v0, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/android/settings/bM;->mContext:Landroid/content/Context;

    invoke-static {v0, v1}, Landroid/security/FingerprintIdUtils;->putUserFingerprintIds(Landroid/content/Context;Ljava/util/HashMap;)V

    goto :goto_0

    :cond_3
    return-void
.end method

.method static synthetic bNp(Lcom/android/settings/bM;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/settings/bM;->bTU:Z

    return v0
.end method

.method static synthetic bNq(Lcom/android/settings/bM;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/android/settings/bM;->bTT:Z

    return p1
.end method

.method static synthetic bNr(Lcom/android/settings/bM;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/android/settings/bM;->bTU:Z

    return p1
.end method

.method static synthetic bNs(Lcom/android/settings/bM;ILjava/util/List;Ljava/util/List;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/android/settings/bM;->bNn(ILjava/util/List;Ljava/util/List;)V

    return-void
.end method


# virtual methods
.method public bNd()Z
    .locals 1

    invoke-direct {p0}, Lcom/android/settings/bM;->bNl()V

    iget-object v0, p0, Lcom/android/settings/bM;->bTV:Landroid/hardware/fingerprint/FingerprintManager;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/android/settings/bM;->bTV:Landroid/hardware/fingerprint/FingerprintManager;

    invoke-virtual {v0}, Landroid/hardware/fingerprint/FingerprintManager;->isHardwareDetected()Z

    move-result v0

    goto :goto_0
.end method

.method public bNe()Ljava/util/List;
    .locals 3

    invoke-direct {p0}, Lcom/android/settings/bM;->bNl()V

    iget-object v0, p0, Lcom/android/settings/bM;->bTV:Landroid/hardware/fingerprint/FingerprintManager;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/android/settings/bM;->bTV:Landroid/hardware/fingerprint/FingerprintManager;

    invoke-virtual {v0}, Landroid/hardware/fingerprint/FingerprintManager;->getEnrolledFingerprints()Ljava/util/List;

    move-result-object v0

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    if-eqz v0, :cond_1

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    if-lez v2, :cond_1

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/fingerprint/Fingerprint;

    invoke-virtual {v0}, Landroid/hardware/fingerprint/Fingerprint;->getFingerId()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    return-object v1
.end method

.method public bNf()V
    .locals 1

    iget-object v0, p0, Lcom/android/settings/bM;->bTS:Landroid/os/CancellationSignal;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/bM;->bTS:Landroid/os/CancellationSignal;

    invoke-virtual {v0}, Landroid/os/CancellationSignal;->cancel()V

    :cond_0
    return-void
.end method

.method public bNg(Lcom/android/settings/bl;Ljava/util/List;)V
    .locals 6

    const/4 v3, 0x0

    const/4 v1, 0x0

    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "callback can not be null, and ids can not be null or empty"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/android/settings/bM;->bNl()V

    iget-object v0, p0, Lcom/android/settings/bM;->bTV:Landroid/hardware/fingerprint/FingerprintManager;

    if-nez v0, :cond_2

    return-void

    :cond_2
    new-instance v0, Landroid/os/CancellationSignal;

    invoke-direct {v0}, Landroid/os/CancellationSignal;-><init>()V

    iput-object v0, p0, Lcom/android/settings/bM;->bTS:Landroid/os/CancellationSignal;

    new-instance v4, Lcom/android/settings/jz;

    invoke-direct {v4, p0, p1}, Lcom/android/settings/jz;-><init>(Lcom/android/settings/bM;Lcom/android/settings/bl;)V

    iget-object v0, p0, Lcom/android/settings/bM;->bTV:Landroid/hardware/fingerprint/FingerprintManager;

    iget-object v2, p0, Lcom/android/settings/bM;->bTS:Landroid/os/CancellationSignal;

    move-object v5, v1

    invoke-virtual/range {v0 .. v5}, Landroid/hardware/fingerprint/FingerprintManager;->authenticate(Landroid/hardware/fingerprint/FingerprintManager$CryptoObject;Landroid/os/CancellationSignal;ILandroid/hardware/fingerprint/FingerprintManager$AuthenticationCallback;Landroid/os/Handler;)V

    return-void
.end method

.method public bNh(Lcom/android/settings/bx;[B)V
    .locals 1

    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v0

    invoke-virtual {p0, p1, p2, v0}, Lcom/android/settings/bM;->bNo(Lcom/android/settings/bx;[BI)V

    return-void
.end method

.method public bNi()J
    .locals 2

    invoke-direct {p0}, Lcom/android/settings/bM;->bNl()V

    iget-object v0, p0, Lcom/android/settings/bM;->bTV:Landroid/hardware/fingerprint/FingerprintManager;

    if-nez v0, :cond_0

    const-wide/16 v0, 0x0

    :goto_0
    return-wide v0

    :cond_0
    iget-object v0, p0, Lcom/android/settings/bM;->bTV:Landroid/hardware/fingerprint/FingerprintManager;

    invoke-virtual {v0}, Landroid/hardware/fingerprint/FingerprintManager;->preEnroll()J

    move-result-wide v0

    goto :goto_0
.end method

.method public bNj()V
    .locals 1

    iget-object v0, p0, Lcom/android/settings/bM;->bTV:Landroid/hardware/fingerprint/FingerprintManager;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/android/settings/bM;->bTT:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/bM;->bTV:Landroid/hardware/fingerprint/FingerprintManager;

    invoke-virtual {v0}, Landroid/hardware/fingerprint/FingerprintManager;->postEnroll()I

    :cond_0
    iget-object v0, p0, Lcom/android/settings/bM;->bTS:Landroid/os/CancellationSignal;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/settings/bM;->bTS:Landroid/os/CancellationSignal;

    invoke-virtual {v0}, Landroid/os/CancellationSignal;->cancel()V

    :cond_1
    return-void
.end method

.method public bNk(Ljava/lang/String;Lcom/android/settings/f;)V
    .locals 1

    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v0

    invoke-virtual {p0, p1, p2, v0}, Lcom/android/settings/bM;->bNm(Ljava/lang/String;Lcom/android/settings/f;I)V

    return-void
.end method

.method public bNm(Ljava/lang/String;Lcom/android/settings/f;I)V
    .locals 6

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/android/settings/bM;->bNl()V

    iget-object v0, p0, Lcom/android/settings/bM;->bTV:Landroid/hardware/fingerprint/FingerprintManager;

    if-nez v0, :cond_0

    return-void

    :cond_0
    new-instance v0, Landroid/hardware/fingerprint/Fingerprint;

    invoke-static {p1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    const-wide/16 v4, 0x0

    const/4 v2, 0x0

    invoke-direct/range {v0 .. v5}, Landroid/hardware/fingerprint/Fingerprint;-><init>(Ljava/lang/CharSequence;IIJ)V

    new-instance v1, Lcom/android/settings/jy;

    invoke-direct {v1, p0, p2}, Lcom/android/settings/jy;-><init>(Lcom/android/settings/bM;Lcom/android/settings/f;)V

    iget-object v2, p0, Lcom/android/settings/bM;->bTV:Landroid/hardware/fingerprint/FingerprintManager;

    invoke-virtual {v2, v0, p3, v1}, Landroid/hardware/fingerprint/FingerprintManager;->remove(Landroid/hardware/fingerprint/Fingerprint;ILandroid/hardware/fingerprint/FingerprintManager$RemovalCallback;)V

    return-void
.end method

.method public bNo(Lcom/android/settings/bx;[BI)V
    .locals 7

    const/4 v3, 0x0

    invoke-direct {p0}, Lcom/android/settings/bM;->bNl()V

    iget-object v0, p0, Lcom/android/settings/bM;->bTV:Landroid/hardware/fingerprint/FingerprintManager;

    if-nez v0, :cond_0

    return-void

    :cond_0
    new-instance v0, Landroid/os/CancellationSignal;

    invoke-direct {v0}, Landroid/os/CancellationSignal;-><init>()V

    iput-object v0, p0, Lcom/android/settings/bM;->bTS:Landroid/os/CancellationSignal;

    invoke-virtual {p0}, Lcom/android/settings/bM;->bNe()Ljava/util/List;

    move-result-object v0

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/android/settings/bM;->bTU:Z

    new-instance v5, Lcom/android/settings/jx;

    invoke-direct {v5, p0, p1, v0}, Lcom/android/settings/jx;-><init>(Lcom/android/settings/bM;Lcom/android/settings/bx;Ljava/util/List;)V

    if-nez p2, :cond_1

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v0

    array-length v1, v0

    :goto_0
    if-ge v3, v1, :cond_2

    aget-object v2, v0, v3

    invoke-virtual {p0}, Lcom/android/settings/bM;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2}, Ljava/lang/StackTraceElement;->getFileName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, " "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v2}, Ljava/lang/StackTraceElement;->getMethodName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, " "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v2}, Ljava/lang/StackTraceElement;->getLineNumber()I

    move-result v2

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v4, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/android/settings/bM;->bTV:Landroid/hardware/fingerprint/FingerprintManager;

    iget-object v2, p0, Lcom/android/settings/bM;->bTS:Landroid/os/CancellationSignal;

    move-object v1, p2

    move v4, p3

    invoke-virtual/range {v0 .. v5}, Landroid/hardware/fingerprint/FingerprintManager;->enroll([BLandroid/os/CancellationSignal;IILandroid/hardware/fingerprint/FingerprintManager$EnrollmentCallback;)V

    :cond_2
    return-void
.end method
