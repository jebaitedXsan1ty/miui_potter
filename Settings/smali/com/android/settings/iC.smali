.class final Lcom/android/settings/iC;
.super Ljava/lang/Object;
.source "SetUpNewFingerprintInternalActivity.java"

# interfaces
.implements Landroid/media/MediaPlayer$OnCompletionListener;


# instance fields
.field final synthetic coD:Lcom/android/settings/SetUpNewFingerprintInternalActivity$SetUpNewFingerprintFragment;


# direct methods
.method constructor <init>(Lcom/android/settings/SetUpNewFingerprintInternalActivity$SetUpNewFingerprintFragment;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/iC;->coD:Lcom/android/settings/SetUpNewFingerprintInternalActivity$SetUpNewFingerprintFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCompletion(Landroid/media/MediaPlayer;)V
    .locals 3

    iget-object v0, p0, Lcom/android/settings/iC;->coD:Lcom/android/settings/SetUpNewFingerprintInternalActivity$SetUpNewFingerprintFragment;

    invoke-static {v0}, Lcom/android/settings/SetUpNewFingerprintInternalActivity$SetUpNewFingerprintFragment;->bIj(Lcom/android/settings/SetUpNewFingerprintInternalActivity$SetUpNewFingerprintFragment;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/settings/iC;->coD:Lcom/android/settings/SetUpNewFingerprintInternalActivity$SetUpNewFingerprintFragment;

    invoke-static {v0}, Lcom/android/settings/SetUpNewFingerprintInternalActivity$SetUpNewFingerprintFragment;->bIl(Lcom/android/settings/SetUpNewFingerprintInternalActivity$SetUpNewFingerprintFragment;)Landroid/widget/VideoView;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/VideoView;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/settings/iC;->coD:Lcom/android/settings/SetUpNewFingerprintInternalActivity$SetUpNewFingerprintFragment;

    invoke-static {v0}, Lcom/android/settings/SetUpNewFingerprintInternalActivity$SetUpNewFingerprintFragment;->bIa(Lcom/android/settings/SetUpNewFingerprintInternalActivity$SetUpNewFingerprintFragment;)Landroid/widget/ImageView;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/settings/iC;->coD:Lcom/android/settings/SetUpNewFingerprintInternalActivity$SetUpNewFingerprintFragment;

    invoke-static {v0}, Lcom/android/settings/SetUpNewFingerprintInternalActivity$SetUpNewFingerprintFragment;->bIa(Lcom/android/settings/SetUpNewFingerprintInternalActivity$SetUpNewFingerprintFragment;)Landroid/widget/ImageView;

    move-result-object v1

    iget-object v0, p0, Lcom/android/settings/iC;->coD:Lcom/android/settings/SetUpNewFingerprintInternalActivity$SetUpNewFingerprintFragment;

    invoke-static {v0}, Lcom/android/settings/SetUpNewFingerprintInternalActivity$SetUpNewFingerprintFragment;->bHX(Lcom/android/settings/SetUpNewFingerprintInternalActivity$SetUpNewFingerprintFragment;)Z

    move-result v0

    if-eqz v0, :cond_1

    const v0, 0x7f080111

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    iget-object v0, p0, Lcom/android/settings/iC;->coD:Lcom/android/settings/SetUpNewFingerprintInternalActivity$SetUpNewFingerprintFragment;

    invoke-static {v0}, Lcom/android/settings/SetUpNewFingerprintInternalActivity$SetUpNewFingerprintFragment;->bIb(Lcom/android/settings/SetUpNewFingerprintInternalActivity$SetUpNewFingerprintFragment;)Landroid/widget/TextView;

    move-result-object v0

    const v1, 0x7f1200a4

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    iget-object v0, p0, Lcom/android/settings/iC;->coD:Lcom/android/settings/SetUpNewFingerprintInternalActivity$SetUpNewFingerprintFragment;

    invoke-static {v0}, Lcom/android/settings/SetUpNewFingerprintInternalActivity$SetUpNewFingerprintFragment;->bIc(Lcom/android/settings/SetUpNewFingerprintInternalActivity$SetUpNewFingerprintFragment;)Landroid/widget/TextView;

    move-result-object v0

    const v1, 0x7f1200a5

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    :cond_0
    :goto_1
    return-void

    :cond_1
    const v0, 0x7f0800df

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/android/settings/iC;->coD:Lcom/android/settings/SetUpNewFingerprintInternalActivity$SetUpNewFingerprintFragment;

    invoke-static {v0}, Lcom/android/settings/SetUpNewFingerprintInternalActivity$SetUpNewFingerprintFragment;->bIg(Lcom/android/settings/SetUpNewFingerprintInternalActivity$SetUpNewFingerprintFragment;)Landroid/net/Uri;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/iC;->coD:Lcom/android/settings/SetUpNewFingerprintInternalActivity$SetUpNewFingerprintFragment;

    iget-object v1, p0, Lcom/android/settings/iC;->coD:Lcom/android/settings/SetUpNewFingerprintInternalActivity$SetUpNewFingerprintFragment;

    invoke-static {v1}, Lcom/android/settings/SetUpNewFingerprintInternalActivity$SetUpNewFingerprintFragment;->bIg(Lcom/android/settings/SetUpNewFingerprintInternalActivity$SetUpNewFingerprintFragment;)Landroid/net/Uri;

    move-result-object v1

    iget-object v2, p0, Lcom/android/settings/iC;->coD:Lcom/android/settings/SetUpNewFingerprintInternalActivity$SetUpNewFingerprintFragment;

    invoke-static {v2}, Lcom/android/settings/SetUpNewFingerprintInternalActivity$SetUpNewFingerprintFragment;->bIl(Lcom/android/settings/SetUpNewFingerprintInternalActivity$SetUpNewFingerprintFragment;)Landroid/widget/VideoView;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/android/settings/SetUpNewFingerprintInternalActivity$SetUpNewFingerprintFragment;->bIC(Lcom/android/settings/SetUpNewFingerprintInternalActivity$SetUpNewFingerprintFragment;Landroid/net/Uri;Landroid/widget/VideoView;)V

    goto :goto_1
.end method
