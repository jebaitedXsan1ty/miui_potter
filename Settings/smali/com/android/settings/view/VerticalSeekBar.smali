.class public Lcom/android/settings/view/VerticalSeekBar;
.super Landroid/widget/SeekBar;
.source "VerticalSeekBar.java"


# instance fields
.field private final bmH:Landroid/graphics/Rect;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/android/settings/view/VerticalSeekBar;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    const v0, 0x101007b

    invoke-direct {p0, p1, p2, v0}, Lcom/android/settings/view/VerticalSeekBar;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    invoke-direct {p0, p1, p2, p3}, Landroid/widget/SeekBar;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/android/settings/view/VerticalSeekBar;->bmH:Landroid/graphics/Rect;

    return-void
.end method

.method private bao(Landroid/graphics/Canvas;)V
    .locals 7

    invoke-virtual {p0}, Lcom/android/settings/view/VerticalSeekBar;->getProgressDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    const/high16 v1, -0x3d4c0000    # -90.0f

    invoke-virtual {p0}, Lcom/android/settings/view/VerticalSeekBar;->getWidth()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    int-to-float v2, v2

    invoke-virtual {p0}, Lcom/android/settings/view/VerticalSeekBar;->getHeight()I

    move-result v3

    div-int/lit8 v3, v3, 0x2

    int-to-float v3, v3

    invoke-virtual {p1, v1, v2, v3}, Landroid/graphics/Canvas;->rotate(FFF)V

    invoke-virtual {p0}, Lcom/android/settings/view/VerticalSeekBar;->getWidth()I

    move-result v1

    invoke-virtual {p0}, Lcom/android/settings/view/VerticalSeekBar;->getHeight()I

    move-result v2

    sub-int v3, v2, v1

    neg-int v3, v3

    div-int/lit8 v3, v3, 0x2

    invoke-virtual {p0}, Lcom/android/settings/view/VerticalSeekBar;->getPaddingBottom()I

    move-result v4

    add-int/2addr v3, v4

    sub-int v4, v2, v1

    div-int/lit8 v4, v4, 0x2

    invoke-virtual {p0}, Lcom/android/settings/view/VerticalSeekBar;->getPaddingStart()I

    move-result v5

    add-int/2addr v4, v5

    add-int v5, v2, v1

    div-int/lit8 v5, v5, 0x2

    invoke-virtual {p0}, Lcom/android/settings/view/VerticalSeekBar;->getPaddingTop()I

    move-result v6

    sub-int/2addr v5, v6

    add-int/2addr v1, v2

    div-int/lit8 v1, v1, 0x2

    invoke-virtual {p0}, Lcom/android/settings/view/VerticalSeekBar;->getPaddingEnd()I

    move-result v2

    sub-int/2addr v1, v2

    invoke-virtual {v0, v3, v4, v5, v1}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    return-void
.end method

.method private bap(Landroid/graphics/Canvas;)V
    .locals 7

    invoke-virtual {p0}, Lcom/android/settings/view/VerticalSeekBar;->getThumb()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    if-nez v1, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/settings/view/VerticalSeekBar;->bmH:Landroid/graphics/Rect;

    invoke-virtual {v1, v0}, Landroid/graphics/drawable/Drawable;->copyBounds(Landroid/graphics/Rect;)V

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v2

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v3

    invoke-virtual {p0}, Lcom/android/settings/view/VerticalSeekBar;->getWidth()I

    move-result v0

    invoke-virtual {p0}, Lcom/android/settings/view/VerticalSeekBar;->getPaddingStart()I

    move-result v4

    sub-int/2addr v0, v4

    invoke-virtual {p0}, Lcom/android/settings/view/VerticalSeekBar;->getPaddingEnd()I

    move-result v4

    sub-int v4, v0, v4

    invoke-virtual {p0}, Lcom/android/settings/view/VerticalSeekBar;->getHeight()I

    move-result v0

    invoke-virtual {p0}, Lcom/android/settings/view/VerticalSeekBar;->getPaddingTop()I

    move-result v5

    sub-int/2addr v0, v5

    invoke-virtual {p0}, Lcom/android/settings/view/VerticalSeekBar;->getPaddingBottom()I

    move-result v5

    sub-int v5, v0, v5

    invoke-virtual {p0}, Lcom/android/settings/view/VerticalSeekBar;->getMax()I

    move-result v0

    if-lez v0, :cond_1

    invoke-virtual {p0}, Lcom/android/settings/view/VerticalSeekBar;->getProgress()I

    move-result v6

    int-to-float v6, v6

    int-to-float v0, v0

    div-float v0, v6, v0

    const/high16 v6, 0x3f800000    # 1.0f

    sub-float v0, v6, v0

    :goto_0
    int-to-float v5, v5

    mul-float/2addr v0, v5

    float-to-int v0, v0

    invoke-virtual {p0}, Lcom/android/settings/view/VerticalSeekBar;->getPaddingTop()I

    move-result v5

    add-int/2addr v0, v5

    div-int/lit8 v5, v3, 0x2

    sub-int/2addr v0, v5

    sub-int/2addr v4, v2

    div-int/lit8 v4, v4, 0x2

    invoke-virtual {p0}, Lcom/android/settings/view/VerticalSeekBar;->getPaddingStart()I

    move-result v5

    add-int/2addr v4, v5

    add-int/2addr v2, v4

    add-int/2addr v3, v0

    invoke-virtual {v1, v4, v0, v2, v3}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    invoke-virtual {v1, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    iget-object v0, p0, Lcom/android/settings/view/VerticalSeekBar;->bmH:Landroid/graphics/Rect;

    invoke-virtual {v1, v0}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method protected declared-synchronized onDraw(Landroid/graphics/Canvas;)V
    .locals 1

    monitor-enter p0

    :try_start_0
    invoke-direct {p0, p1}, Lcom/android/settings/view/VerticalSeekBar;->bao(Landroid/graphics/Canvas;)V

    invoke-direct {p0, p1}, Lcom/android/settings/view/VerticalSeekBar;->bap(Landroid/graphics/Canvas;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 4

    invoke-virtual {p0}, Lcom/android/settings/view/VerticalSeekBar;->getHeight()I

    move-result v0

    int-to-float v0, v0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    sub-float/2addr v0, v1

    invoke-virtual {p0}, Lcom/android/settings/view/VerticalSeekBar;->getPaddingBottom()I

    move-result v1

    int-to-float v1, v1

    sub-float/2addr v0, v1

    invoke-virtual {p0}, Lcom/android/settings/view/VerticalSeekBar;->getHeight()I

    move-result v1

    invoke-virtual {p0}, Lcom/android/settings/view/VerticalSeekBar;->getPaddingTop()I

    move-result v2

    sub-int/2addr v1, v2

    invoke-virtual {p0}, Lcom/android/settings/view/VerticalSeekBar;->getPaddingBottom()I

    move-result v2

    sub-int/2addr v1, v2

    int-to-float v1, v1

    invoke-virtual {p0}, Lcom/android/settings/view/VerticalSeekBar;->getWidth()I

    move-result v2

    invoke-virtual {p0}, Lcom/android/settings/view/VerticalSeekBar;->getPaddingStart()I

    move-result v3

    sub-int/2addr v2, v3

    invoke-virtual {p0}, Lcom/android/settings/view/VerticalSeekBar;->getPaddingEnd()I

    move-result v3

    sub-int/2addr v2, v3

    int-to-float v2, v2

    div-float/2addr v0, v1

    mul-float/2addr v0, v2

    invoke-virtual {p0}, Lcom/android/settings/view/VerticalSeekBar;->getLayoutDirection()I

    move-result v1

    const/4 v3, 0x1

    if-ne v1, v3, :cond_0

    sub-float v0, v2, v0

    :cond_0
    invoke-virtual {p0}, Lcom/android/settings/view/VerticalSeekBar;->getPaddingStart()I

    move-result v1

    int-to-float v1, v1

    add-float/2addr v0, v1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/view/MotionEvent;->setLocation(FF)V

    invoke-super {p0, p1}, Landroid/widget/SeekBar;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method
