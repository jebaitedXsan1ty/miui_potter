.class public Lcom/android/settings/enterprise/i;
.super Ljava/lang/Object;
.source "EnterprisePrivacyFeatureProviderImpl.java"

# interfaces
.implements Lcom/android/settings/enterprise/v;


# static fields
.field private static final aaQ:I


# instance fields
.field private final aaR:Lcom/android/settings/vpn2/ConnectivityManagerWrapper;

.field private final aaS:Lcom/android/settings/enterprise/q;

.field private final aaT:Lcom/android/settings/applications/PackageManagerWrapper;

.field private final aaU:Landroid/content/res/Resources;

.field private final aaV:Landroid/os/UserManager;

.field private final mContext:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v0

    sput v0, Lcom/android/settings/enterprise/i;->aaQ:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/android/settings/enterprise/q;Lcom/android/settings/applications/PackageManagerWrapper;Landroid/os/UserManager;Lcom/android/settings/vpn2/ConnectivityManagerWrapper;Landroid/content/res/Resources;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/enterprise/i;->mContext:Landroid/content/Context;

    iput-object p2, p0, Lcom/android/settings/enterprise/i;->aaS:Lcom/android/settings/enterprise/q;

    iput-object p3, p0, Lcom/android/settings/enterprise/i;->aaT:Lcom/android/settings/applications/PackageManagerWrapper;

    iput-object p4, p0, Lcom/android/settings/enterprise/i;->aaV:Landroid/os/UserManager;

    iput-object p5, p0, Lcom/android/settings/enterprise/i;->aaR:Lcom/android/settings/vpn2/ConnectivityManagerWrapper;

    iput-object p6, p0, Lcom/android/settings/enterprise/i;->aaU:Landroid/content/res/Resources;

    return-void
.end method

.method private getManagedProfileUserId()I
    .locals 3

    iget-object v0, p0, Lcom/android/settings/enterprise/i;->aaV:Landroid/os/UserManager;

    sget v1, Lcom/android/settings/enterprise/i;->aaQ:I

    invoke-virtual {v0, v1}, Landroid/os/UserManager;->getProfiles(I)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/UserInfo;

    invoke-virtual {v0}, Landroid/content/pm/UserInfo;->isManagedProfile()Z

    move-result v2

    if-eqz v2, :cond_0

    iget v0, v0, Landroid/content/pm/UserInfo;->id:I

    return v0

    :cond_1
    const/16 v0, -0x2710

    return v0
.end method


# virtual methods
.method public QX()Ljava/lang/CharSequence;
    .locals 5

    const/4 v1, 0x0

    const/4 v4, 0x0

    invoke-virtual {p0}, Lcom/android/settings/enterprise/i;->Rg()Z

    move-result v0

    if-nez v0, :cond_0

    return-object v1

    :cond_0
    new-instance v0, Landroid/text/SpannableStringBuilder;

    invoke-direct {v0}, Landroid/text/SpannableStringBuilder;-><init>()V

    iget-object v1, p0, Lcom/android/settings/enterprise/i;->aaS:Lcom/android/settings/enterprise/q;

    invoke-interface {v1}, Lcom/android/settings/enterprise/q;->Rr()Ljava/lang/CharSequence;

    move-result-object v1

    if-eqz v1, :cond_1

    iget-object v2, p0, Lcom/android/settings/enterprise/i;->aaU:Landroid/content/res/Resources;

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    aput-object v1, v3, v4

    const v1, 0x7f12062c

    invoke-virtual {v2, v1, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    :goto_0
    iget-object v1, p0, Lcom/android/settings/enterprise/i;->aaU:Landroid/content/res/Resources;

    const v2, 0x7f12062b

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    iget-object v1, p0, Lcom/android/settings/enterprise/i;->aaU:Landroid/content/res/Resources;

    const v2, 0x7f12062a

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/android/settings/enterprise/j;

    iget-object v3, p0, Lcom/android/settings/enterprise/i;->mContext:Landroid/content/Context;

    invoke-direct {v2, v3}, Lcom/android/settings/enterprise/j;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1, v2, v4}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;Ljava/lang/Object;I)Landroid/text/SpannableStringBuilder;

    return-object v0

    :cond_1
    iget-object v1, p0, Lcom/android/settings/enterprise/i;->aaU:Landroid/content/res/Resources;

    const v2, 0x7f120629

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    goto :goto_0
.end method

.method public QY()Ljava/lang/String;
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/android/settings/enterprise/i;->aaS:Lcom/android/settings/enterprise/q;

    invoke-interface {v0}, Lcom/android/settings/enterprise/q;->Rr()Ljava/lang/CharSequence;

    move-result-object v0

    if-nez v0, :cond_0

    return-object v1

    :cond_0
    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public QZ()Ljava/lang/String;
    .locals 5

    const/4 v4, 0x0

    iget-object v0, p0, Lcom/android/settings/enterprise/i;->aaS:Lcom/android/settings/enterprise/q;

    invoke-interface {v0}, Lcom/android/settings/enterprise/q;->Rz()Z

    move-result v0

    if-nez v0, :cond_0

    return-object v4

    :cond_0
    iget-object v0, p0, Lcom/android/settings/enterprise/i;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v1, "default_input_method"

    sget v2, Lcom/android/settings/enterprise/i;->aaQ:I

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$Secure;->getStringForUser(Landroid/content/ContentResolver;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    return-object v4

    :cond_1
    :try_start_0
    iget-object v1, p0, Lcom/android/settings/enterprise/i;->aaT:Lcom/android/settings/applications/PackageManagerWrapper;

    sget v2, Lcom/android/settings/enterprise/i;->aaQ:I

    const/4 v3, 0x0

    invoke-interface {v1, v0, v3, v2}, Lcom/android/settings/applications/PackageManagerWrapper;->uR(Ljava/lang/String;II)Landroid/content/pm/ApplicationInfo;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/enterprise/i;->aaT:Lcom/android/settings/applications/PackageManagerWrapper;

    invoke-interface {v1}, Lcom/android/settings/applications/PackageManagerWrapper;->uW()Landroid/content/pm/PackageManager;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/pm/ApplicationInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    :catch_0
    move-exception v0

    return-object v4
.end method

.method public Ra()Ljava/util/Date;
    .locals 4

    iget-object v0, p0, Lcom/android/settings/enterprise/i;->aaS:Lcom/android/settings/enterprise/q;

    invoke-interface {v0}, Lcom/android/settings/enterprise/q;->Rt()J

    move-result-wide v2

    const-wide/16 v0, 0x0

    cmp-long v0, v2, v0

    if-gez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Ljava/util/Date;

    invoke-direct {v0, v2, v3}, Ljava/util/Date;-><init>(J)V

    goto :goto_0
.end method

.method public Rb()Ljava/util/Date;
    .locals 4

    iget-object v0, p0, Lcom/android/settings/enterprise/i;->aaS:Lcom/android/settings/enterprise/q;

    invoke-interface {v0}, Lcom/android/settings/enterprise/q;->Ru()J

    move-result-wide v2

    const-wide/16 v0, 0x0

    cmp-long v0, v2, v0

    if-gez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Ljava/util/Date;

    invoke-direct {v0, v2, v3}, Ljava/util/Date;-><init>(J)V

    goto :goto_0
.end method

.method public Rc()Ljava/util/Date;
    .locals 4

    iget-object v0, p0, Lcom/android/settings/enterprise/i;->aaS:Lcom/android/settings/enterprise/q;

    invoke-interface {v0}, Lcom/android/settings/enterprise/q;->Rs()J

    move-result-wide v2

    const-wide/16 v0, 0x0

    cmp-long v0, v2, v0

    if-gez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Ljava/util/Date;

    invoke-direct {v0, v2, v3}, Ljava/util/Date;-><init>(J)V

    goto :goto_0
.end method

.method public Rd()I
    .locals 3

    iget-object v0, p0, Lcom/android/settings/enterprise/i;->aaS:Lcom/android/settings/enterprise/q;

    sget v1, Lcom/android/settings/enterprise/i;->aaQ:I

    invoke-interface {v0, v1}, Lcom/android/settings/enterprise/q;->Rx(I)Landroid/content/ComponentName;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    return v0

    :cond_0
    iget-object v1, p0, Lcom/android/settings/enterprise/i;->aaS:Lcom/android/settings/enterprise/q;

    sget v2, Lcom/android/settings/enterprise/i;->aaQ:I

    invoke-interface {v1, v0, v2}, Lcom/android/settings/enterprise/q;->Ry(Landroid/content/ComponentName;I)I

    move-result v0

    return v0
.end method

.method public Re()I
    .locals 3

    const/4 v2, 0x0

    invoke-direct {p0}, Lcom/android/settings/enterprise/i;->getManagedProfileUserId()I

    move-result v0

    const/16 v1, -0x2710

    if-ne v0, v1, :cond_0

    return v2

    :cond_0
    iget-object v1, p0, Lcom/android/settings/enterprise/i;->aaS:Lcom/android/settings/enterprise/q;

    invoke-interface {v1, v0}, Lcom/android/settings/enterprise/q;->Rx(I)Landroid/content/ComponentName;

    move-result-object v1

    if-nez v1, :cond_1

    return v2

    :cond_1
    iget-object v2, p0, Lcom/android/settings/enterprise/i;->aaS:Lcom/android/settings/enterprise/q;

    invoke-interface {v2, v1, v0}, Lcom/android/settings/enterprise/q;->Ry(Landroid/content/ComponentName;I)I

    move-result v0

    return v0
.end method

.method public Rf()I
    .locals 4

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/android/settings/enterprise/i;->aaS:Lcom/android/settings/enterprise/q;

    new-instance v2, Landroid/os/UserHandle;

    sget v3, Lcom/android/settings/enterprise/i;->aaQ:I

    invoke-direct {v2, v3}, Landroid/os/UserHandle;-><init>(I)V

    invoke-interface {v1, v2}, Lcom/android/settings/enterprise/q;->RA(Landroid/os/UserHandle;)Ljava/util/List;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    :cond_0
    invoke-direct {p0}, Lcom/android/settings/enterprise/i;->getManagedProfileUserId()I

    move-result v1

    const/16 v2, -0x2710

    if-eq v1, v2, :cond_1

    iget-object v2, p0, Lcom/android/settings/enterprise/i;->aaS:Lcom/android/settings/enterprise/q;

    new-instance v3, Landroid/os/UserHandle;

    invoke-direct {v3, v1}, Landroid/os/UserHandle;-><init>(I)V

    invoke-interface {v2, v3}, Lcom/android/settings/enterprise/q;->RA(Landroid/os/UserHandle;)Ljava/util/List;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    return v0
.end method

.method public Rg()Z
    .locals 3

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/android/settings/enterprise/i;->aaT:Lcom/android/settings/applications/PackageManagerWrapper;

    const-string/jumbo v2, "android.software.device_admin"

    invoke-interface {v1, v2}, Lcom/android/settings/applications/PackageManagerWrapper;->uY(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    return v0

    :cond_0
    iget-object v1, p0, Lcom/android/settings/enterprise/i;->aaS:Lcom/android/settings/enterprise/q;

    invoke-interface {v1}, Lcom/android/settings/enterprise/q;->Rq()Landroid/content/ComponentName;

    move-result-object v1

    if-eqz v1, :cond_1

    const/4 v0, 0x1

    :cond_1
    return v0
.end method

.method public Rh()Z
    .locals 2

    iget-object v0, p0, Lcom/android/settings/enterprise/i;->aaR:Lcom/android/settings/vpn2/ConnectivityManagerWrapper;

    sget v1, Lcom/android/settings/enterprise/i;->aaQ:I

    invoke-static {v0, v1}, Lcom/android/settings/vpn2/VpnUtils;->Pr(Lcom/android/settings/vpn2/ConnectivityManagerWrapper;I)Z

    move-result v0

    return v0
.end method

.method public Ri()Z
    .locals 2

    invoke-direct {p0}, Lcom/android/settings/enterprise/i;->getManagedProfileUserId()I

    move-result v0

    const/16 v1, -0x2710

    if-eq v0, v1, :cond_0

    iget-object v1, p0, Lcom/android/settings/enterprise/i;->aaR:Lcom/android/settings/vpn2/ConnectivityManagerWrapper;

    invoke-static {v1, v0}, Lcom/android/settings/vpn2/VpnUtils;->Pr(Lcom/android/settings/vpn2/ConnectivityManagerWrapper;I)Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public Rj()Z
    .locals 1

    iget-object v0, p0, Lcom/android/settings/enterprise/i;->aaR:Lcom/android/settings/vpn2/ConnectivityManagerWrapper;

    invoke-interface {v0}, Lcom/android/settings/vpn2/ConnectivityManagerWrapper;->Pj()Landroid/net/ProxyInfo;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public Rk()Z
    .locals 3

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/android/settings/enterprise/i;->Rg()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-direct {p0}, Lcom/android/settings/enterprise/i;->getManagedProfileUserId()I

    move-result v1

    const/16 v2, -0x2710

    if-eq v1, v2, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method public Rl()Z
    .locals 2

    iget-object v0, p0, Lcom/android/settings/enterprise/i;->aaS:Lcom/android/settings/enterprise/q;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/android/settings/enterprise/q;->Rw(Landroid/content/ComponentName;)Z

    move-result v0

    return v0
.end method

.method public Rm()Z
    .locals 2

    iget-object v0, p0, Lcom/android/settings/enterprise/i;->aaS:Lcom/android/settings/enterprise/q;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/android/settings/enterprise/q;->Rv(Landroid/content/ComponentName;)Z

    move-result v0

    return v0
.end method
