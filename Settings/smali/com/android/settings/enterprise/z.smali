.class public Lcom/android/settings/enterprise/z;
.super Ljava/lang/Object;
.source "DevicePolicyManagerWrapperImpl.java"

# interfaces
.implements Lcom/android/settings/enterprise/q;


# instance fields
.field private final abm:Landroid/app/admin/DevicePolicyManager;


# direct methods
.method public constructor <init>(Landroid/app/admin/DevicePolicyManager;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/android/settings/enterprise/z;->abm:Landroid/app/admin/DevicePolicyManager;

    return-void
.end method


# virtual methods
.method public RA(Landroid/os/UserHandle;)Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/enterprise/z;->abm:Landroid/app/admin/DevicePolicyManager;

    invoke-virtual {v0, p1}, Landroid/app/admin/DevicePolicyManager;->getOwnerInstalledCaCerts(Landroid/os/UserHandle;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public RB(Ljava/lang/String;)Z
    .locals 1

    iget-object v0, p0, Lcom/android/settings/enterprise/z;->abm:Landroid/app/admin/DevicePolicyManager;

    invoke-virtual {v0, p1}, Landroid/app/admin/DevicePolicyManager;->packageHasActiveAdmins(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public RC(Ljava/lang/String;)Z
    .locals 1

    iget-object v0, p0, Lcom/android/settings/enterprise/z;->abm:Landroid/app/admin/DevicePolicyManager;

    invoke-virtual {v0, p1}, Landroid/app/admin/DevicePolicyManager;->isUninstallInQueue(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public RD(Ljava/lang/String;)Z
    .locals 1

    iget-object v0, p0, Lcom/android/settings/enterprise/z;->abm:Landroid/app/admin/DevicePolicyManager;

    invoke-virtual {v0, p1}, Landroid/app/admin/DevicePolicyManager;->isDeviceOwnerAppOnAnyUser(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public RE(Landroid/content/ComponentName;Ljava/lang/String;Ljava/lang/String;)I
    .locals 1

    iget-object v0, p0, Lcom/android/settings/enterprise/z;->abm:Landroid/app/admin/DevicePolicyManager;

    invoke-virtual {v0, p1, p2, p3}, Landroid/app/admin/DevicePolicyManager;->getPermissionGrantState(Landroid/content/ComponentName;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public Rq()Landroid/content/ComponentName;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/enterprise/z;->abm:Landroid/app/admin/DevicePolicyManager;

    invoke-virtual {v0}, Landroid/app/admin/DevicePolicyManager;->getDeviceOwnerComponentOnAnyUser()Landroid/content/ComponentName;

    move-result-object v0

    return-object v0
.end method

.method public Rr()Ljava/lang/CharSequence;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/enterprise/z;->abm:Landroid/app/admin/DevicePolicyManager;

    invoke-virtual {v0}, Landroid/app/admin/DevicePolicyManager;->getDeviceOwnerOrganizationName()Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method public Rs()J
    .locals 2

    iget-object v0, p0, Lcom/android/settings/enterprise/z;->abm:Landroid/app/admin/DevicePolicyManager;

    invoke-virtual {v0}, Landroid/app/admin/DevicePolicyManager;->getLastSecurityLogRetrievalTime()J

    move-result-wide v0

    return-wide v0
.end method

.method public Rt()J
    .locals 2

    iget-object v0, p0, Lcom/android/settings/enterprise/z;->abm:Landroid/app/admin/DevicePolicyManager;

    invoke-virtual {v0}, Landroid/app/admin/DevicePolicyManager;->getLastBugReportRequestTime()J

    move-result-wide v0

    return-wide v0
.end method

.method public Ru()J
    .locals 2

    iget-object v0, p0, Lcom/android/settings/enterprise/z;->abm:Landroid/app/admin/DevicePolicyManager;

    invoke-virtual {v0}, Landroid/app/admin/DevicePolicyManager;->getLastNetworkLogRetrievalTime()J

    move-result-wide v0

    return-wide v0
.end method

.method public Rv(Landroid/content/ComponentName;)Z
    .locals 1

    iget-object v0, p0, Lcom/android/settings/enterprise/z;->abm:Landroid/app/admin/DevicePolicyManager;

    invoke-virtual {v0, p1}, Landroid/app/admin/DevicePolicyManager;->isSecurityLoggingEnabled(Landroid/content/ComponentName;)Z

    move-result v0

    return v0
.end method

.method public Rw(Landroid/content/ComponentName;)Z
    .locals 1

    iget-object v0, p0, Lcom/android/settings/enterprise/z;->abm:Landroid/app/admin/DevicePolicyManager;

    invoke-virtual {v0, p1}, Landroid/app/admin/DevicePolicyManager;->isNetworkLoggingEnabled(Landroid/content/ComponentName;)Z

    move-result v0

    return v0
.end method

.method public Rx(I)Landroid/content/ComponentName;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/enterprise/z;->abm:Landroid/app/admin/DevicePolicyManager;

    invoke-virtual {v0, p1}, Landroid/app/admin/DevicePolicyManager;->getProfileOwnerAsUser(I)Landroid/content/ComponentName;

    move-result-object v0

    return-object v0
.end method

.method public Ry(Landroid/content/ComponentName;I)I
    .locals 1

    iget-object v0, p0, Lcom/android/settings/enterprise/z;->abm:Landroid/app/admin/DevicePolicyManager;

    invoke-virtual {v0, p1, p2}, Landroid/app/admin/DevicePolicyManager;->getMaximumFailedPasswordsForWipe(Landroid/content/ComponentName;I)I

    move-result v0

    return v0
.end method

.method public Rz()Z
    .locals 1

    iget-object v0, p0, Lcom/android/settings/enterprise/z;->abm:Landroid/app/admin/DevicePolicyManager;

    invoke-virtual {v0}, Landroid/app/admin/DevicePolicyManager;->isCurrentInputMethodSetByOwner()Z

    move-result v0

    return v0
.end method
