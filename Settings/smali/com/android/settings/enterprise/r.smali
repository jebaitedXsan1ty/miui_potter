.class public Lcom/android/settings/enterprise/r;
.super Lcom/android/settings/core/c;
.source "ApplicationListPreferenceController.java"

# interfaces
.implements Lcom/android/settings/applications/ApplicationFeatureProvider$ListOfAppsCallback;


# instance fields
.field private abe:Lcom/android/settings/SettingsPreferenceFragment;

.field private final abf:Landroid/content/pm/PackageManager;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/android/settings/enterprise/s;Landroid/content/pm/PackageManager;Lcom/android/settings/SettingsPreferenceFragment;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/settings/core/c;-><init>(Landroid/content/Context;)V

    iput-object p3, p0, Lcom/android/settings/enterprise/r;->abf:Landroid/content/pm/PackageManager;

    iput-object p4, p0, Lcom/android/settings/enterprise/r;->abe:Lcom/android/settings/SettingsPreferenceFragment;

    invoke-interface {p2, p1, p0}, Lcom/android/settings/enterprise/s;->RF(Landroid/content/Context;Lcom/android/settings/applications/ApplicationFeatureProvider$ListOfAppsCallback;)V

    return-void
.end method


# virtual methods
.method public b()Ljava/lang/String;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public c()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public xX(Ljava/util/List;)V
    .locals 8

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/android/settings/enterprise/r;->abe:Lcom/android/settings/SettingsPreferenceFragment;

    invoke-virtual {v0}, Lcom/android/settings/SettingsPreferenceFragment;->djr()Landroid/support/v7/preference/PreferenceScreen;

    move-result-object v3

    if-nez v3, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/settings/enterprise/r;->abe:Lcom/android/settings/SettingsPreferenceFragment;

    invoke-virtual {v0}, Lcom/android/settings/SettingsPreferenceFragment;->dju()Landroid/support/v7/preference/k;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v7/preference/k;->getContext()Landroid/content/Context;

    move-result-object v4

    move v1, v2

    :goto_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settings/applications/UserAppInfo;

    new-instance v5, Landroid/support/v7/preference/Preference;

    invoke-direct {v5, v4}, Landroid/support/v7/preference/Preference;-><init>(Landroid/content/Context;)V

    const v6, 0x7f0d0122

    invoke-virtual {v5, v6}, Landroid/support/v7/preference/Preference;->setLayoutResource(I)V

    iget-object v6, v0, Lcom/android/settings/applications/UserAppInfo;->DB:Landroid/content/pm/ApplicationInfo;

    iget-object v7, p0, Lcom/android/settings/enterprise/r;->abf:Landroid/content/pm/PackageManager;

    invoke-virtual {v6, v7}, Landroid/content/pm/ApplicationInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/support/v7/preference/Preference;->setTitle(Ljava/lang/CharSequence;)V

    iget-object v0, v0, Lcom/android/settings/applications/UserAppInfo;->DB:Landroid/content/pm/ApplicationInfo;

    iget-object v6, p0, Lcom/android/settings/enterprise/r;->abf:Landroid/content/pm/PackageManager;

    invoke-virtual {v0, v6}, Landroid/content/pm/ApplicationInfo;->loadIcon(Landroid/content/pm/PackageManager;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v5, v0}, Landroid/support/v7/preference/Preference;->setIcon(Landroid/graphics/drawable/Drawable;)V

    invoke-virtual {v5, v1}, Landroid/support/v7/preference/Preference;->setOrder(I)V

    invoke-virtual {v5, v2}, Landroid/support/v7/preference/Preference;->boV(Z)V

    invoke-virtual {v3, v5}, Landroid/support/v7/preference/PreferenceScreen;->im(Landroid/support/v7/preference/Preference;)Z

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    return-void
.end method
