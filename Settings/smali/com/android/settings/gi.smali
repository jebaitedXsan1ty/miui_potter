.class final Lcom/android/settings/gi;
.super Landroid/os/AsyncTask;
.source "MasterClearConfirm.java"


# instance fields
.field clw:I

.field clx:Landroid/app/ProgressDialog;

.field final synthetic cly:Landroid/service/persistentdata/PersistentDataBlockManager;

.field final synthetic clz:Lcom/android/settings/aL;

.field final synthetic val$activity:Landroid/app/Activity;


# direct methods
.method constructor <init>(Landroid/service/persistentdata/PersistentDataBlockManager;Landroid/app/Activity;Lcom/android/settings/aL;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/gi;->cly:Landroid/service/persistentdata/PersistentDataBlockManager;

    iput-object p2, p0, Lcom/android/settings/gi;->val$activity:Landroid/app/Activity;

    iput-object p3, p0, Lcom/android/settings/gi;->clz:Lcom/android/settings/aL;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/android/settings/gi;->doInBackground([Ljava/lang/Void;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Void;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/gi;->cly:Landroid/service/persistentdata/PersistentDataBlockManager;

    invoke-virtual {v0}, Landroid/service/persistentdata/PersistentDataBlockManager;->wipe()V

    const/4 v0, 0x0

    return-object v0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    check-cast p1, Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/android/settings/gi;->onPostExecute(Ljava/lang/Void;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/Void;)V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/gi;->clx:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->hide()V

    iget-object v0, p0, Lcom/android/settings/gi;->val$activity:Landroid/app/Activity;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/gi;->val$activity:Landroid/app/Activity;

    iget v1, p0, Lcom/android/settings/gi;->clw:I

    invoke-virtual {v0, v1}, Landroid/app/Activity;->setRequestedOrientation(I)V

    iget-object v0, p0, Lcom/android/settings/gi;->clz:Lcom/android/settings/aL;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/settings/gi;->clz:Lcom/android/settings/aL;

    invoke-interface {v0}, Lcom/android/settings/aL;->bwc()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/settings/gi;->val$activity:Landroid/app/Activity;

    invoke-static {v0}, Lcom/android/settings/MasterClearConfirm;->bwb(Landroid/app/Activity;)V

    goto :goto_0
.end method

.method protected onPreExecute()V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/gi;->val$activity:Landroid/app/Activity;

    invoke-static {v0}, Lcom/android/settings/MasterClearConfirm;->bwa(Landroid/app/Activity;)Landroid/app/ProgressDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/gi;->clx:Landroid/app/ProgressDialog;

    iget-object v0, p0, Lcom/android/settings/gi;->clx:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    iget-object v0, p0, Lcom/android/settings/gi;->val$activity:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getRequestedOrientation()I

    move-result v0

    iput v0, p0, Lcom/android/settings/gi;->clw:I

    iget-object v0, p0, Lcom/android/settings/gi;->val$activity:Landroid/app/Activity;

    const/16 v1, 0xe

    invoke-virtual {v0, v1}, Landroid/app/Activity;->setRequestedOrientation(I)V

    return-void
.end method
