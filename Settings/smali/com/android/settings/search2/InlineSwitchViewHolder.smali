.class public Lcom/android/settings/search2/InlineSwitchViewHolder;
.super Lcom/android/settings/search2/SearchViewHolder;
.source "InlineSwitchViewHolder.java"


# instance fields
.field private final mContext:Landroid/content/Context;

.field public final switchView:Landroid/widget/Switch;


# direct methods
.method public constructor <init>(Landroid/view/View;Landroid/content/Context;)V
    .locals 1

    invoke-direct {p0, p1}, Lcom/android/settings/search2/SearchViewHolder;-><init>(Landroid/view/View;)V

    iput-object p2, p0, Lcom/android/settings/search2/InlineSwitchViewHolder;->mContext:Landroid/content/Context;

    const v0, 0x7f0a046b

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Switch;

    iput-object v0, p0, Lcom/android/settings/search2/InlineSwitchViewHolder;->switchView:Landroid/widget/Switch;

    return-void
.end method


# virtual methods
.method public getClickActionMetricName()I
    .locals 1

    const/16 v0, 0x36e

    return v0
.end method

.method synthetic lambda$-com_android_settings_search2_InlineSwitchViewHolder_3071(Lcom/android/settings/search2/SearchFragment;Lcom/android/settings/search2/SearchResult;Lcom/android/settings/search2/InlineSwitchPayload;Landroid/widget/CompoundButton;Z)V
    .locals 5

    const/4 v2, 0x1

    const/4 v3, 0x0

    const/16 v0, 0x370

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    if-eqz p5, :cond_0

    const-wide/16 v0, 0x1

    :goto_0
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-static {v4, v0}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    new-array v1, v2, [Landroid/util/Pair;

    aput-object v0, v1, v3

    invoke-virtual {p1, p0, p2, v1}, Lcom/android/settings/search2/SearchFragment;->onSearchResultClicked(Lcom/android/settings/search2/SearchViewHolder;Lcom/android/settings/search2/SearchResult;[Landroid/util/Pair;)V

    if-eqz p5, :cond_1

    move v0, v2

    :goto_1
    iget-object v1, p0, Lcom/android/settings/search2/InlineSwitchViewHolder;->mContext:Landroid/content/Context;

    invoke-virtual {p3, v1, v0}, Lcom/android/settings/search2/InlineSwitchPayload;->setValue(Landroid/content/Context;I)Z

    return-void

    :cond_0
    const-wide/16 v0, 0x0

    goto :goto_0

    :cond_1
    move v0, v3

    goto :goto_1
.end method

.method public onBind(Lcom/android/settings/search2/SearchFragment;Lcom/android/settings/search2/SearchResult;)V
    .locals 4

    const/4 v1, 0x1

    invoke-super {p0, p1, p2}, Lcom/android/settings/search2/SearchViewHolder;->onBind(Lcom/android/settings/search2/SearchFragment;Lcom/android/settings/search2/SearchResult;)V

    iget-object v0, p0, Lcom/android/settings/search2/InlineSwitchViewHolder;->mContext:Landroid/content/Context;

    if-nez v0, :cond_0

    return-void

    :cond_0
    iget-object v0, p2, Lcom/android/settings/search2/SearchResult;->payload:Lcom/android/settings/search2/ResultPayload;

    check-cast v0, Lcom/android/settings/search2/InlineSwitchPayload;

    iget-object v2, p0, Lcom/android/settings/search2/InlineSwitchViewHolder;->switchView:Landroid/widget/Switch;

    iget-object v3, p0, Lcom/android/settings/search2/InlineSwitchViewHolder;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v3}, Lcom/android/settings/search2/InlineSwitchPayload;->getValue(Landroid/content/Context;)I

    move-result v3

    if-ne v3, v1, :cond_1

    :goto_0
    invoke-virtual {v2, v1}, Landroid/widget/Switch;->setChecked(Z)V

    iget-object v1, p0, Lcom/android/settings/search2/InlineSwitchViewHolder;->switchView:Landroid/widget/Switch;

    new-instance v2, Lcom/android/settings/search2/-$Lambda$4dSYu1oUzywOEH2IlPt1zFrPXyg;

    invoke-direct {v2, p0, p1, p2, v0}, Lcom/android/settings/search2/-$Lambda$4dSYu1oUzywOEH2IlPt1zFrPXyg;-><init>(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-virtual {v1, v2}, Landroid/widget/Switch;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    return-void

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method
