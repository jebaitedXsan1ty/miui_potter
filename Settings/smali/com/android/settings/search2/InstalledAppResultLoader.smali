.class public Lcom/android/settings/search2/InstalledAppResultLoader;
.super Lcom/android/settings/utils/k;
.source "InstalledAppResultLoader.java"


# static fields
.field private static final LAUNCHER_PROBE:Landroid/content/Intent;

.field private static final NAME_NO_MATCH:I = -0x1


# instance fields
.field private mBreadcrumb:Ljava/util/List;

.field private final mPackageManager:Lcom/android/settings/applications/PackageManagerWrapper;

.field private final mQuery:Ljava/lang/String;

.field private mSiteMapManager:Lcom/android/settings/dashboard/l;

.field private final mUserManager:Landroid/os/UserManager;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v1, "android.intent.action.MAIN"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string/jumbo v1, "android.intent.category.LAUNCHER"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    sput-object v0, Lcom/android/settings/search2/InstalledAppResultLoader;->LAUNCHER_PROBE:Landroid/content/Intent;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/android/settings/applications/PackageManagerWrapper;Ljava/lang/String;Lcom/android/settings/dashboard/l;)V
    .locals 1

    invoke-direct {p0, p1}, Lcom/android/settings/utils/k;-><init>(Landroid/content/Context;)V

    iput-object p4, p0, Lcom/android/settings/search2/InstalledAppResultLoader;->mSiteMapManager:Lcom/android/settings/dashboard/l;

    const-string/jumbo v0, "user"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/UserManager;

    iput-object v0, p0, Lcom/android/settings/search2/InstalledAppResultLoader;->mUserManager:Landroid/os/UserManager;

    iput-object p2, p0, Lcom/android/settings/search2/InstalledAppResultLoader;->mPackageManager:Lcom/android/settings/applications/PackageManagerWrapper;

    iput-object p3, p0, Lcom/android/settings/search2/InstalledAppResultLoader;->mQuery:Ljava/lang/String;

    return-void
.end method

.method private getBreadCrumb()Ljava/util/List;
    .locals 4

    iget-object v0, p0, Lcom/android/settings/search2/InstalledAppResultLoader;->mBreadcrumb:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/search2/InstalledAppResultLoader;->mBreadcrumb:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    invoke-virtual {p0}, Lcom/android/settings/search2/InstalledAppResultLoader;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/search2/InstalledAppResultLoader;->mSiteMapManager:Lcom/android/settings/dashboard/l;

    const-class v2, Lcom/android/settings/applications/ManageApplications;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    const v3, 0x7f12016c

    invoke-virtual {v0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v0, v2, v3}, Lcom/android/settings/dashboard/l;->Dj(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/search2/InstalledAppResultLoader;->mBreadcrumb:Ljava/util/List;

    :cond_1
    iget-object v0, p0, Lcom/android/settings/search2/InstalledAppResultLoader;->mBreadcrumb:Ljava/util/List;

    return-object v0
.end method

.method private getRank(I)I
    .locals 1

    const/4 v0, 0x6

    if-ge p1, v0, :cond_0

    const/4 v0, 0x2

    return v0

    :cond_0
    const/4 v0, 0x3

    return v0
.end method

.method private getUsersToCount()Ljava/util/List;
    .locals 2

    iget-object v0, p0, Lcom/android/settings/search2/InstalledAppResultLoader;->mUserManager:Landroid/os/UserManager;

    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/os/UserManager;->getProfiles(I)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method private getWordDifference(Ljava/lang/String;Ljava/lang/String;)I
    .locals 9

    const/4 v2, 0x0

    const/4 v8, -0x1

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    return v8

    :cond_1
    invoke-virtual {p2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v3

    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v4

    array-length v5, v4

    array-length v0, v3

    if-le v0, v5, :cond_2

    return v8

    :cond_2
    move v1, v2

    :cond_3
    if-ge v1, v5, :cond_9

    move v0, v2

    :cond_4
    add-int v6, v1, v0

    if-ge v6, v5, :cond_5

    aget-char v6, v3, v0

    add-int v7, v1, v0

    aget-char v7, v4, v7

    if-ne v6, v7, :cond_5

    add-int/lit8 v0, v0, 0x1

    array-length v6, v3

    if-lt v0, v6, :cond_4

    array-length v0, v3

    sub-int v0, v5, v0

    return v0

    :cond_5
    add-int/2addr v1, v0

    array-length v0, v3

    sub-int v6, v5, v1

    if-le v0, v6, :cond_6

    return v8

    :cond_6
    :goto_0
    if-ge v1, v5, :cond_8

    add-int/lit8 v0, v1, 0x1

    aget-char v1, v4, v1

    invoke-static {v1}, Ljava/lang/Character;->isWhitespace(C)Z

    move-result v1

    xor-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_a

    move v1, v0

    goto :goto_0

    :cond_7
    const/4 v0, 0x1

    :goto_1
    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_3

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    :cond_8
    :goto_2
    if-ge v1, v5, :cond_3

    aget-char v0, v4, v1

    invoke-static {v0}, Ljava/lang/Character;->isLetter(C)Z

    move-result v0

    if-nez v0, :cond_7

    aget-char v0, v4, v1

    invoke-static {v0}, Ljava/lang/Character;->isDigit(C)Z

    move-result v0

    goto :goto_1

    :cond_9
    return v8

    :cond_a
    move v1, v0

    goto :goto_2
.end method

.method private shouldIncludeAsCandidate(Landroid/content/pm/ApplicationInfo;Landroid/content/pm/UserInfo;)Z
    .locals 6

    const/4 v0, 0x1

    const/4 v1, 0x0

    iget v2, p1, Landroid/content/pm/ApplicationInfo;->flags:I

    and-int/lit16 v2, v2, 0x80

    if-nez v2, :cond_0

    iget v2, p1, Landroid/content/pm/ApplicationInfo;->flags:I

    and-int/lit8 v2, v2, 0x1

    if-nez v2, :cond_1

    :cond_0
    return v0

    :cond_1
    new-instance v2, Landroid/content/Intent;

    sget-object v3, Lcom/android/settings/search2/InstalledAppResultLoader;->LAUNCHER_PROBE:Landroid/content/Intent;

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Intent;)V

    iget-object v3, p1, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v2

    iget-object v3, p0, Lcom/android/settings/search2/InstalledAppResultLoader;->mPackageManager:Lcom/android/settings/applications/PackageManagerWrapper;

    iget v4, p2, Landroid/content/pm/UserInfo;->id:I

    const v5, 0xc0200

    invoke-interface {v3, v2, v5, v4}, Lcom/android/settings/applications/PackageManagerWrapper;->uZ(Landroid/content/Intent;II)Ljava/util/List;

    move-result-object v2

    if-eqz v2, :cond_2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-eqz v2, :cond_2

    :goto_0
    return v0

    :cond_2
    move v0, v1

    goto :goto_0
.end method


# virtual methods
.method public bridge synthetic loadInBackground()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/android/settings/search2/InstalledAppResultLoader;->loadInBackground()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public loadInBackground()Ljava/util/List;
    .locals 11

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iget-object v0, p0, Lcom/android/settings/search2/InstalledAppResultLoader;->mPackageManager:Lcom/android/settings/applications/PackageManagerWrapper;

    invoke-interface {v0}, Lcom/android/settings/applications/PackageManagerWrapper;->uW()Landroid/content/pm/PackageManager;

    move-result-object v3

    invoke-direct {p0}, Lcom/android/settings/search2/InstalledAppResultLoader;->getUsersToCount()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/UserInfo;

    iget-object v5, p0, Lcom/android/settings/search2/InstalledAppResultLoader;->mPackageManager:Lcom/android/settings/applications/PackageManagerWrapper;

    invoke-virtual {v0}, Landroid/content/pm/UserInfo;->isAdmin()Z

    move-result v1

    if-eqz v1, :cond_2

    const/high16 v1, 0x400000

    :goto_0
    const v6, 0x8200

    or-int/2addr v1, v6

    iget v6, v0, Landroid/content/pm/UserInfo;->id:I

    invoke-interface {v5, v1, v6}, Lcom/android/settings/applications/PackageManagerWrapper;->uV(II)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_1
    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/pm/ApplicationInfo;

    invoke-direct {p0, v1, v0}, Lcom/android/settings/search2/InstalledAppResultLoader;->shouldIncludeAsCandidate(Landroid/content/pm/ApplicationInfo;Landroid/content/pm/UserInfo;)Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-virtual {v1, v3}, Landroid/content/pm/ApplicationInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v6

    invoke-interface {v6}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v6

    iget-object v7, p0, Lcom/android/settings/search2/InstalledAppResultLoader;->mQuery:Ljava/lang/String;

    invoke-direct {p0, v6, v7}, Lcom/android/settings/search2/InstalledAppResultLoader;->getWordDifference(Ljava/lang/String;Ljava/lang/String;)I

    move-result v6

    const/4 v7, -0x1

    if-eq v6, v7, :cond_1

    new-instance v7, Landroid/content/Intent;

    const-string/jumbo v8, "android.settings.APPLICATION_DETAILS_SETTINGS"

    invoke-direct {v7, v8}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string/jumbo v8, "android.settings.APPLICATION_DETAILS_SETTINGS"

    invoke-virtual {v7, v8}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v7

    const-string/jumbo v8, "package"

    iget-object v9, v1, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    const/4 v10, 0x0

    invoke-static {v8, v9, v10}, Landroid/net/Uri;->fromParts(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v7

    const-string/jumbo v8, ":settings:source_metrics"

    const/16 v9, 0x22

    invoke-virtual {v7, v8, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v7

    new-instance v8, Lcom/android/settings/search2/AppSearchResult$Builder;

    invoke-direct {v8}, Lcom/android/settings/search2/AppSearchResult$Builder;-><init>()V

    invoke-virtual {v8, v1}, Lcom/android/settings/search2/AppSearchResult$Builder;->setAppInfo(Landroid/content/pm/ApplicationInfo;)Lcom/android/settings/search2/SearchResult$Builder;

    move-result-object v9

    invoke-virtual {v1, v3}, Landroid/content/pm/ApplicationInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v9, v1}, Lcom/android/settings/search2/SearchResult$Builder;->addTitle(Ljava/lang/CharSequence;)Lcom/android/settings/search2/SearchResult$Builder;

    move-result-object v1

    invoke-direct {p0, v6}, Lcom/android/settings/search2/InstalledAppResultLoader;->getRank(I)I

    move-result v6

    invoke-virtual {v1, v6}, Lcom/android/settings/search2/SearchResult$Builder;->addRank(I)Lcom/android/settings/search2/SearchResult$Builder;

    move-result-object v1

    invoke-direct {p0}, Lcom/android/settings/search2/InstalledAppResultLoader;->getBreadCrumb()Ljava/util/List;

    move-result-object v6

    invoke-virtual {v1, v6}, Lcom/android/settings/search2/SearchResult$Builder;->addBreadcrumbs(Ljava/util/List;)Lcom/android/settings/search2/SearchResult$Builder;

    move-result-object v1

    new-instance v6, Lcom/android/settings/search2/IntentPayload;

    invoke-direct {v6, v7}, Lcom/android/settings/search2/IntentPayload;-><init>(Landroid/content/Intent;)V

    invoke-virtual {v1, v6}, Lcom/android/settings/search2/SearchResult$Builder;->addPayload(Lcom/android/settings/search2/ResultPayload;)Lcom/android/settings/search2/SearchResult$Builder;

    invoke-virtual {v8}, Lcom/android/settings/search2/AppSearchResult$Builder;->build()Lcom/android/settings/search2/AppSearchResult;

    move-result-object v1

    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_2
    const/4 v1, 0x0

    goto/16 :goto_0

    :cond_3
    invoke-static {v2}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    return-object v2
.end method

.method protected bridge synthetic onDiscardResult(Ljava/lang/Object;)V
    .locals 0

    check-cast p1, Ljava/util/List;

    invoke-virtual {p0, p1}, Lcom/android/settings/search2/InstalledAppResultLoader;->onDiscardResult(Ljava/util/List;)V

    return-void
.end method

.method protected onDiscardResult(Ljava/util/List;)V
    .locals 0

    return-void
.end method
