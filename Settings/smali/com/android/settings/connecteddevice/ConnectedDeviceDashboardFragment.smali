.class public Lcom/android/settings/connecteddevice/ConnectedDeviceDashboardFragment;
.super Lcom/android/settings/dashboard/DashboardFragment;
.source "ConnectedDeviceDashboardFragment.java"


# static fields
.field public static final SEARCH_INDEX_DATA_PROVIDER:Lcom/android/settings/search/Indexable$SearchIndexProvider;


# instance fields
.field private bpO:Lcom/android/settings/connecteddevice/a;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/android/settings/connecteddevice/c;

    invoke-direct {v0}, Lcom/android/settings/connecteddevice/c;-><init>()V

    sput-object v0, Lcom/android/settings/connecteddevice/ConnectedDeviceDashboardFragment;->SEARCH_INDEX_DATA_PROVIDER:Lcom/android/settings/search/Indexable$SearchIndexProvider;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/dashboard/DashboardFragment;-><init>()V

    return-void
.end method


# virtual methods
.method protected EU()Ljava/lang/String;
    .locals 1

    const-string/jumbo v0, "ConnectedDeviceFrag"

    return-object v0
.end method

.method protected EV()I
    .locals 1

    const v0, 0x7f150031

    return v0
.end method

.method public getMetricsCategory()I
    .locals 1

    const/16 v0, 0x2eb

    return v0
.end method

.method protected getPreferenceControllers(Landroid/content/Context;)Ljava/util/List;
    .locals 4

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {p0}, Lcom/android/settings/connecteddevice/ConnectedDeviceDashboardFragment;->getLifecycle()Lcom/android/settings/core/lifecycle/a;

    move-result-object v1

    new-instance v2, Lcom/android/settings/nfc/NfcPreferenceController;

    invoke-direct {v2, p1}, Lcom/android/settings/nfc/NfcPreferenceController;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, v2}, Lcom/android/settings/core/lifecycle/a;->ajt(Lcom/android/settings/core/lifecycle/b;)Lcom/android/settings/core/lifecycle/b;

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v2, Lcom/android/settings/connecteddevice/a;

    new-instance v3, Lcom/android/settings/deviceinfo/UsbBackend;

    invoke-direct {v3, p1}, Lcom/android/settings/deviceinfo/UsbBackend;-><init>(Landroid/content/Context;)V

    invoke-direct {v2, p1, v3}, Lcom/android/settings/connecteddevice/a;-><init>(Landroid/content/Context;Lcom/android/settings/deviceinfo/UsbBackend;)V

    iput-object v2, p0, Lcom/android/settings/connecteddevice/ConnectedDeviceDashboardFragment;->bpO:Lcom/android/settings/connecteddevice/a;

    iget-object v2, p0, Lcom/android/settings/connecteddevice/ConnectedDeviceDashboardFragment;->bpO:Lcom/android/settings/connecteddevice/a;

    invoke-virtual {v1, v2}, Lcom/android/settings/core/lifecycle/a;->ajt(Lcom/android/settings/core/lifecycle/b;)Lcom/android/settings/core/lifecycle/b;

    iget-object v2, p0, Lcom/android/settings/connecteddevice/ConnectedDeviceDashboardFragment;->bpO:Lcom/android/settings/connecteddevice/a;

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v2, Lcom/android/settings/bluetooth/BluetoothMasterSwitchPreferenceController;

    invoke-static {p1}, Lcom/android/settings/bluetooth/Utils;->arq(Landroid/content/Context;)Lcom/android/settingslib/bluetooth/q;

    move-result-object v3

    invoke-direct {v2, p1, v3}, Lcom/android/settings/bluetooth/BluetoothMasterSwitchPreferenceController;-><init>(Landroid/content/Context;Lcom/android/settingslib/bluetooth/q;)V

    invoke-virtual {v1, v2}, Lcom/android/settings/core/lifecycle/a;->ajt(Lcom/android/settings/core/lifecycle/b;)Lcom/android/settings/core/lifecycle/b;

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object v0
.end method
