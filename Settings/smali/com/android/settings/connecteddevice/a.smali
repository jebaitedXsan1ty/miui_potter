.class public Lcom/android/settings/connecteddevice/a;
.super Lcom/android/settings/core/c;
.source "UsbModePreferenceController.java"

# interfaces
.implements Lcom/android/settings/core/lifecycle/b;
.implements Lcom/android/settings/core/lifecycle/a/b;
.implements Lcom/android/settings/core/lifecycle/a/d;


# instance fields
.field private bpP:Lcom/android/settings/deviceinfo/UsbBackend;

.field private bpQ:Landroid/support/v7/preference/Preference;

.field private bpR:Lcom/android/settings/connecteddevice/b;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/android/settings/deviceinfo/UsbBackend;)V
    .locals 2

    invoke-direct {p0, p1}, Lcom/android/settings/core/c;-><init>(Landroid/content/Context;)V

    iput-object p2, p0, Lcom/android/settings/connecteddevice/a;->bpP:Lcom/android/settings/deviceinfo/UsbBackend;

    new-instance v0, Lcom/android/settings/connecteddevice/b;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/android/settings/connecteddevice/b;-><init>(Lcom/android/settings/connecteddevice/a;Lcom/android/settings/connecteddevice/b;)V

    iput-object v0, p0, Lcom/android/settings/connecteddevice/a;->bpR:Lcom/android/settings/connecteddevice/b;

    return-void
.end method

.method private bde(Landroid/support/v7/preference/Preference;)V
    .locals 1

    iget-object v0, p0, Lcom/android/settings/connecteddevice/a;->bpP:Lcom/android/settings/deviceinfo/UsbBackend;

    invoke-virtual {v0}, Lcom/android/settings/deviceinfo/UsbBackend;->auZ()I

    move-result v0

    invoke-direct {p0, p1, v0}, Lcom/android/settings/connecteddevice/a;->bdf(Landroid/support/v7/preference/Preference;I)V

    return-void
.end method

.method private bdf(Landroid/support/v7/preference/Preference;I)V
    .locals 2

    const/4 v1, 0x0

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/android/settings/connecteddevice/a;->bpR:Lcom/android/settings/connecteddevice/b;

    invoke-virtual {v0}, Lcom/android/settings/connecteddevice/b;->bdj()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Landroid/support/v7/preference/Preference;->setEnabled(Z)V

    invoke-virtual {p0, p2}, Lcom/android/settings/connecteddevice/a;->getSummary(I)I

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0, p2}, Lcom/android/settings/connecteddevice/a;->getSummary(I)I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/support/v7/preference/Preference;->dks(I)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const v0, 0x7f1205e0

    invoke-virtual {p1, v0}, Landroid/support/v7/preference/Preference;->dks(I)V

    invoke-virtual {p1, v1}, Landroid/support/v7/preference/Preference;->setEnabled(Z)V

    goto :goto_0
.end method

.method static synthetic bdg(Lcom/android/settings/connecteddevice/a;)Landroid/content/Context;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/connecteddevice/a;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic bdh(Lcom/android/settings/connecteddevice/a;)Landroid/support/v7/preference/Preference;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/connecteddevice/a;->bpQ:Landroid/support/v7/preference/Preference;

    return-object v0
.end method

.method static synthetic bdi(Lcom/android/settings/connecteddevice/a;Landroid/support/v7/preference/Preference;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/settings/connecteddevice/a;->bde(Landroid/support/v7/preference/Preference;)V

    return-void
.end method


# virtual methods
.method public a(Landroid/support/v7/preference/PreferenceScreen;)V
    .locals 1

    invoke-super {p0, p1}, Lcom/android/settings/core/c;->a(Landroid/support/v7/preference/PreferenceScreen;)V

    const-string/jumbo v0, "usb_mode"

    invoke-virtual {p1, v0}, Landroid/support/v7/preference/PreferenceScreen;->dlg(Ljava/lang/CharSequence;)Landroid/support/v7/preference/Preference;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/connecteddevice/a;->bpQ:Landroid/support/v7/preference/Preference;

    iget-object v0, p0, Lcom/android/settings/connecteddevice/a;->bpQ:Landroid/support/v7/preference/Preference;

    invoke-direct {p0, v0}, Lcom/android/settings/connecteddevice/a;->bde(Landroid/support/v7/preference/Preference;)V

    return-void
.end method

.method public b()Ljava/lang/String;
    .locals 1

    const-string/jumbo v0, "usb_mode"

    return-object v0
.end method

.method public c()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method getSummary(I)I
    .locals 1

    packed-switch p1, :pswitch_data_0

    :pswitch_0
    const/4 v0, 0x0

    return v0

    :pswitch_1
    const v0, 0x7f121362

    return v0

    :pswitch_2
    const v0, 0x7f121365

    return v0

    :pswitch_3
    const v0, 0x7f121363

    return v0

    :pswitch_4
    const v0, 0x7f121364

    return v0

    :pswitch_5
    const v0, 0x7f121361

    return v0

    :pswitch_6
    const v0, 0x7f120470

    return v0

    nop

    :pswitch_data_0
    .packed-switch -0x1
        :pswitch_6
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_4
        :pswitch_0
        :pswitch_5
    .end packed-switch
.end method

.method public onPause()V
    .locals 1

    iget-object v0, p0, Lcom/android/settings/connecteddevice/a;->bpR:Lcom/android/settings/connecteddevice/b;

    invoke-virtual {v0}, Lcom/android/settings/connecteddevice/b;->bdl()V

    return-void
.end method

.method public onResume()V
    .locals 1

    iget-object v0, p0, Lcom/android/settings/connecteddevice/a;->bpR:Lcom/android/settings/connecteddevice/b;

    invoke-virtual {v0}, Lcom/android/settings/connecteddevice/b;->bdk()V

    return-void
.end method
