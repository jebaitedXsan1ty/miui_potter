.class Lcom/android/settings/connecteddevice/b;
.super Landroid/content/BroadcastReceiver;
.source "UsbModePreferenceController.java"


# instance fields
.field private bpS:Z

.field private bpT:Z

.field final synthetic bpU:Lcom/android/settings/connecteddevice/a;


# direct methods
.method private constructor <init>(Lcom/android/settings/connecteddevice/a;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/connecteddevice/b;->bpU:Lcom/android/settings/connecteddevice/a;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/settings/connecteddevice/a;Lcom/android/settings/connecteddevice/b;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/settings/connecteddevice/b;-><init>(Lcom/android/settings/connecteddevice/a;)V

    return-void
.end method


# virtual methods
.method public bdj()Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/settings/connecteddevice/b;->bpS:Z

    return v0
.end method

.method public bdk()V
    .locals 2

    iget-boolean v0, p0, Lcom/android/settings/connecteddevice/b;->bpT:Z

    if-nez v0, :cond_0

    new-instance v0, Landroid/content/IntentFilter;

    const-string/jumbo v1, "android.hardware.usb.action.USB_STATE"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/android/settings/connecteddevice/b;->bpU:Lcom/android/settings/connecteddevice/a;

    invoke-static {v1}, Lcom/android/settings/connecteddevice/a;->bdg(Lcom/android/settings/connecteddevice/a;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, p0, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string/jumbo v1, "connected"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    :goto_0
    iput-boolean v0, p0, Lcom/android/settings/connecteddevice/b;->bpS:Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/settings/connecteddevice/b;->bpT:Z

    :cond_0
    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public bdl()V
    .locals 1

    iget-boolean v0, p0, Lcom/android/settings/connecteddevice/b;->bpT:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/connecteddevice/b;->bpU:Lcom/android/settings/connecteddevice/a;

    invoke-static {v0}, Lcom/android/settings/connecteddevice/a;->bdg(Lcom/android/settings/connecteddevice/a;)Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/settings/connecteddevice/b;->bpT:Z

    :cond_0
    return-void
.end method

.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 2

    if-eqz p2, :cond_1

    invoke-virtual {p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string/jumbo v1, "connected"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    :goto_0
    iget-boolean v1, p0, Lcom/android/settings/connecteddevice/b;->bpS:Z

    if-eq v0, v1, :cond_0

    iput-boolean v0, p0, Lcom/android/settings/connecteddevice/b;->bpS:Z

    iget-object v0, p0, Lcom/android/settings/connecteddevice/b;->bpU:Lcom/android/settings/connecteddevice/a;

    iget-object v1, p0, Lcom/android/settings/connecteddevice/b;->bpU:Lcom/android/settings/connecteddevice/a;

    invoke-static {v1}, Lcom/android/settings/connecteddevice/a;->bdh(Lcom/android/settings/connecteddevice/a;)Landroid/support/v7/preference/Preference;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/settings/connecteddevice/a;->bdi(Lcom/android/settings/connecteddevice/a;Landroid/support/v7/preference/Preference;)V

    :cond_0
    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
