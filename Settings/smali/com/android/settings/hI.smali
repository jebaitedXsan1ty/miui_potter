.class final Lcom/android/settings/hI;
.super Ljava/lang/Object;
.source "MiuiSecuritySettings.java"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceClickListener;


# instance fields
.field final synthetic cns:Lcom/android/settings/MiuiSecuritySettings;

.field final synthetic cnt:I


# direct methods
.method constructor <init>(Lcom/android/settings/MiuiSecuritySettings;I)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/hI;->cns:Lcom/android/settings/MiuiSecuritySettings;

    iput p2, p0, Lcom/android/settings/hI;->cnt:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPreferenceClick(Landroid/preference/Preference;)Z
    .locals 3

    iget v0, p0, Lcom/android/settings/hI;->cnt:I

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/settings/hI;->cns:Lcom/android/settings/MiuiSecuritySettings;

    invoke-static {v0}, Lcom/android/settings/MiuiSecuritySettings;->bDR(Lcom/android/settings/MiuiSecuritySettings;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/android/settings/hI;->cns:Lcom/android/settings/MiuiSecuritySettings;

    invoke-virtual {v1}, Lcom/android/settings/MiuiSecuritySettings;->getActivity()Landroid/app/Activity;

    move-result-object v1

    const-class v2, Lcom/android/settings/MiuiConfirmCommonPassword;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    iget-object v1, p0, Lcom/android/settings/hI;->cns:Lcom/android/settings/MiuiSecuritySettings;

    const/16 v2, 0x3e9

    invoke-virtual {v1, v0, v2}, Lcom/android/settings/MiuiSecuritySettings;->startActivityForResult(Landroid/content/Intent;I)V

    :goto_0
    const/4 v0, 0x1

    return v0

    :cond_0
    iget-object v0, p0, Lcom/android/settings/hI;->cns:Lcom/android/settings/MiuiSecuritySettings;

    invoke-static {v0}, Lcom/android/settings/MiuiSecuritySettings;->bDU(Lcom/android/settings/MiuiSecuritySettings;)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/android/settings/hI;->cns:Lcom/android/settings/MiuiSecuritySettings;

    iget v1, p0, Lcom/android/settings/hI;->cnt:I

    invoke-static {v0, v1}, Lcom/android/settings/MiuiSecuritySettings;->bDS(Lcom/android/settings/MiuiSecuritySettings;I)V

    goto :goto_0
.end method
