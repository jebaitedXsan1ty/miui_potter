.class public Lcom/android/settings/datausage/AppDataUsage;
.super Lcom/android/settings/datausage/DataUsageBase;
.source "AppDataUsage.java"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceChangeListener;
.implements Lcom/android/settings/datausage/DataSaverBackend$Listener;


# instance fields
.field private jT:Lcom/android/settingslib/AppItem;

.field private jU:Landroid/preference/PreferenceCategory;

.field private final jV:Landroid/app/LoaderManager$LoaderCallbacks;

.field private jW:Landroid/preference/Preference;

.field private jX:Landroid/content/Intent;

.field private jY:Landroid/preference/Preference;

.field private jZ:Lcom/android/settingslib/g/e;

.field private final ka:Landroid/app/LoaderManager$LoaderCallbacks;

.field private kb:Lcom/android/settings/datausage/SpinnerPreference;

.field private kc:Lcom/android/settings/datausage/CycleAdapter;

.field private kd:Landroid/widget/AdapterView$OnItemSelectedListener;

.field private ke:Lcom/android/settings/datausage/DataSaverBackend;

.field private kf:J

.field private kg:Landroid/preference/Preference;

.field private kh:Ljava/lang/CharSequence;

.field private final ki:Landroid/util/ArraySet;

.field private kj:Landroid/net/NetworkPolicy;

.field private kk:Landroid/preference/SwitchPreference;

.field private kl:J

.field private km:Landroid/net/INetworkStatsSession;

.field private kn:Landroid/net/NetworkTemplate;

.field private ko:Landroid/preference/Preference;

.field private kp:Landroid/preference/SwitchPreference;

.field private mIcon:Landroid/graphics/drawable/Drawable;

.field private mPackageName:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/android/settings/datausage/DataUsageBase;-><init>()V

    new-instance v0, Landroid/util/ArraySet;

    invoke-direct {v0}, Landroid/util/ArraySet;-><init>()V

    iput-object v0, p0, Lcom/android/settings/datausage/AppDataUsage;->ki:Landroid/util/ArraySet;

    new-instance v0, Lcom/android/settings/datausage/AppDataUsage$1;

    invoke-direct {v0, p0}, Lcom/android/settings/datausage/AppDataUsage$1;-><init>(Lcom/android/settings/datausage/AppDataUsage;)V

    iput-object v0, p0, Lcom/android/settings/datausage/AppDataUsage;->kd:Landroid/widget/AdapterView$OnItemSelectedListener;

    new-instance v0, Lcom/android/settings/datausage/AppDataUsage$2;

    invoke-direct {v0, p0}, Lcom/android/settings/datausage/AppDataUsage$2;-><init>(Lcom/android/settings/datausage/AppDataUsage;)V

    iput-object v0, p0, Lcom/android/settings/datausage/AppDataUsage;->ka:Landroid/app/LoaderManager$LoaderCallbacks;

    new-instance v0, Lcom/android/settings/datausage/AppDataUsage$3;

    invoke-direct {v0, p0}, Lcom/android/settings/datausage/AppDataUsage$3;-><init>(Lcom/android/settings/datausage/AppDataUsage;)V

    iput-object v0, p0, Lcom/android/settings/datausage/AppDataUsage;->jV:Landroid/app/LoaderManager$LoaderCallbacks;

    return-void
.end method

.method static synthetic jA(Lcom/android/settings/datausage/AppDataUsage;)Landroid/preference/PreferenceCategory;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/datausage/AppDataUsage;->jU:Landroid/preference/PreferenceCategory;

    return-object v0
.end method

.method static synthetic jB(Lcom/android/settings/datausage/AppDataUsage;)Lcom/android/settingslib/g/e;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/datausage/AppDataUsage;->jZ:Lcom/android/settingslib/g/e;

    return-object v0
.end method

.method static synthetic jC(Lcom/android/settings/datausage/AppDataUsage;)Lcom/android/settings/datausage/SpinnerPreference;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/datausage/AppDataUsage;->kb:Lcom/android/settings/datausage/SpinnerPreference;

    return-object v0
.end method

.method static synthetic jD(Lcom/android/settings/datausage/AppDataUsage;)Lcom/android/settings/datausage/CycleAdapter;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/datausage/AppDataUsage;->kc:Lcom/android/settings/datausage/CycleAdapter;

    return-object v0
.end method

.method static synthetic jE(Lcom/android/settings/datausage/AppDataUsage;)Landroid/util/ArraySet;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/datausage/AppDataUsage;->ki:Landroid/util/ArraySet;

    return-object v0
.end method

.method static synthetic jF(Lcom/android/settings/datausage/AppDataUsage;)Landroid/net/NetworkPolicy;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/datausage/AppDataUsage;->kj:Landroid/net/NetworkPolicy;

    return-object v0
.end method

.method static synthetic jG(Lcom/android/settings/datausage/AppDataUsage;)Landroid/net/INetworkStatsSession;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/datausage/AppDataUsage;->km:Landroid/net/INetworkStatsSession;

    return-object v0
.end method

.method static synthetic jH(Lcom/android/settings/datausage/AppDataUsage;Lcom/android/settingslib/g/e;)Lcom/android/settingslib/g/e;
    .locals 0

    iput-object p1, p0, Lcom/android/settings/datausage/AppDataUsage;->jZ:Lcom/android/settingslib/g/e;

    return-object p1
.end method

.method static synthetic jI(Lcom/android/settings/datausage/AppDataUsage;J)J
    .locals 1

    iput-wide p1, p0, Lcom/android/settings/datausage/AppDataUsage;->kf:J

    return-wide p1
.end method

.method static synthetic jJ(Lcom/android/settings/datausage/AppDataUsage;J)J
    .locals 1

    iput-wide p1, p0, Lcom/android/settings/datausage/AppDataUsage;->kl:J

    return-wide p1
.end method

.method static synthetic jK(Lcom/android/settings/datausage/AppDataUsage;)Landroid/content/Context;
    .locals 1

    invoke-virtual {p0}, Lcom/android/settings/datausage/AppDataUsage;->bWz()Landroid/content/Context;

    move-result-object v0

    return-object v0
.end method

.method static synthetic jL(Lcom/android/settings/datausage/AppDataUsage;)Landroid/content/pm/PackageManager;
    .locals 1

    invoke-virtual {p0}, Lcom/android/settings/datausage/AppDataUsage;->bWA()Landroid/content/pm/PackageManager;

    move-result-object v0

    return-object v0
.end method

.method static synthetic jM(Lcom/android/settings/datausage/AppDataUsage;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/datausage/AppDataUsage;->jv()V

    return-void
.end method

.method private ju(I)V
    .locals 4

    invoke-virtual {p0}, Lcom/android/settings/datausage/AppDataUsage;->bWA()Landroid/content/pm/PackageManager;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/pm/PackageManager;->getPackagesForUid(I)[Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    const/4 v0, 0x0

    :goto_0
    array-length v2, v1

    if-ge v0, v2, :cond_0

    iget-object v2, p0, Lcom/android/settings/datausage/AppDataUsage;->ki:Landroid/util/ArraySet;

    aget-object v3, v1, v0

    invoke-virtual {v2, v3}, Landroid/util/ArraySet;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method private jv()V
    .locals 12

    const/4 v8, 0x0

    const-wide/16 v0, 0x0

    iget-object v2, p0, Lcom/android/settings/datausage/AppDataUsage;->jZ:Lcom/android/settingslib/g/e;

    if-eqz v2, :cond_0

    iget-wide v2, p0, Lcom/android/settings/datausage/AppDataUsage;->kl:J

    cmp-long v2, v2, v0

    if-nez v2, :cond_1

    :cond_0
    invoke-virtual {p0}, Lcom/android/settings/datausage/AppDataUsage;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v2

    iget-object v3, p0, Lcom/android/settings/datausage/AppDataUsage;->kb:Lcom/android/settings/datausage/SpinnerPreference;

    invoke-virtual {v2, v3}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    move-wide v2, v0

    :goto_0
    add-long v4, v0, v2

    invoke-virtual {p0}, Lcom/android/settings/datausage/AppDataUsage;->getContext()Landroid/content/Context;

    move-result-object v6

    iget-object v7, p0, Lcom/android/settings/datausage/AppDataUsage;->ko:Landroid/preference/Preference;

    invoke-static {v6, v4, v5}, Landroid/text/format/Formatter;->formatFileSize(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v7, v4}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    iget-object v4, p0, Lcom/android/settings/datausage/AppDataUsage;->kg:Landroid/preference/Preference;

    invoke-static {v6, v2, v3}, Landroid/text/format/Formatter;->formatFileSize(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v2}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    iget-object v2, p0, Lcom/android/settings/datausage/AppDataUsage;->jY:Landroid/preference/Preference;

    invoke-static {v6, v0, v1}, Landroid/text/format/Formatter;->formatFileSize(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    return-void

    :cond_1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    iget-object v0, p0, Lcom/android/settings/datausage/AppDataUsage;->jZ:Lcom/android/settingslib/g/e;

    iget-object v1, v0, Lcom/android/settingslib/g/e;->cJq:Landroid/net/NetworkStatsHistory;

    iget-wide v2, p0, Lcom/android/settings/datausage/AppDataUsage;->kl:J

    iget-wide v4, p0, Lcom/android/settings/datausage/AppDataUsage;->kf:J

    invoke-virtual/range {v1 .. v8}, Landroid/net/NetworkStatsHistory;->getValues(JJJLandroid/net/NetworkStatsHistory$Entry;)Landroid/net/NetworkStatsHistory$Entry;

    move-result-object v8

    iget-wide v0, v8, Landroid/net/NetworkStatsHistory$Entry;->rxBytes:J

    iget-wide v2, v8, Landroid/net/NetworkStatsHistory$Entry;->txBytes:J

    add-long v10, v0, v2

    iget-object v0, p0, Lcom/android/settings/datausage/AppDataUsage;->jZ:Lcom/android/settingslib/g/e;

    iget-object v1, v0, Lcom/android/settingslib/g/e;->cJt:Landroid/net/NetworkStatsHistory;

    iget-wide v2, p0, Lcom/android/settings/datausage/AppDataUsage;->kl:J

    iget-wide v4, p0, Lcom/android/settings/datausage/AppDataUsage;->kf:J

    invoke-virtual/range {v1 .. v8}, Landroid/net/NetworkStatsHistory;->getValues(JJJLandroid/net/NetworkStatsHistory$Entry;)Landroid/net/NetworkStatsHistory$Entry;

    move-result-object v0

    iget-wide v2, v0, Landroid/net/NetworkStatsHistory$Entry;->rxBytes:J

    iget-wide v0, v0, Landroid/net/NetworkStatsHistory$Entry;->txBytes:J

    add-long/2addr v0, v2

    move-wide v2, v0

    move-wide v0, v10

    goto :goto_0
.end method

.method private jw()Z
    .locals 3

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/android/settings/datausage/AppDataUsage;->jT:Lcom/android/settingslib/AppItem;

    iget v1, v1, Lcom/android/settingslib/AppItem;->key:I

    iget-object v2, p0, Lcom/android/settings/datausage/AppDataUsage;->jr:Lcom/android/settings/datausage/TemplatePreference$NetworkServices;

    iget-object v2, v2, Lcom/android/settings/datausage/TemplatePreference$NetworkServices;->jm:Landroid/net/NetworkPolicyManager;

    invoke-virtual {v2, v1}, Landroid/net/NetworkPolicyManager;->getUidPolicy(I)I

    move-result v1

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method private jx()Z
    .locals 2

    iget-object v0, p0, Lcom/android/settings/datausage/AppDataUsage;->ke:Lcom/android/settings/datausage/DataSaverBackend;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/datausage/AppDataUsage;->ke:Lcom/android/settings/datausage/DataSaverBackend;

    iget-object v1, p0, Lcom/android/settings/datausage/AppDataUsage;->jT:Lcom/android/settingslib/AppItem;

    iget v1, v1, Lcom/android/settingslib/AppItem;->key:I

    invoke-virtual {v0, v1}, Lcom/android/settings/datausage/DataSaverBackend;->kv(I)Z

    move-result v0

    return v0

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method private jy()V
    .locals 2

    invoke-direct {p0}, Lcom/android/settings/datausage/AppDataUsage;->jw()Z

    move-result v0

    invoke-direct {p0}, Lcom/android/settings/datausage/AppDataUsage;->jx()Z

    move-result v1

    invoke-direct {p0, v0, v1}, Lcom/android/settings/datausage/AppDataUsage;->jz(ZZ)V

    return-void
.end method

.method private jz(ZZ)V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/datausage/AppDataUsage;->kk:Landroid/preference/SwitchPreference;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/datausage/AppDataUsage;->kk:Landroid/preference/SwitchPreference;

    xor-int/lit8 v1, p1, 0x1

    invoke-virtual {v0, v1}, Landroid/preference/SwitchPreference;->setChecked(Z)V

    :cond_0
    iget-object v0, p0, Lcom/android/settings/datausage/AppDataUsage;->kp:Landroid/preference/SwitchPreference;

    if-eqz v0, :cond_1

    if-eqz p1, :cond_2

    iget-object v0, p0, Lcom/android/settings/datausage/AppDataUsage;->kp:Landroid/preference/SwitchPreference;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/preference/SwitchPreference;->setEnabled(Z)V

    :cond_1
    :goto_0
    return-void

    :cond_2
    iget-object v0, p0, Lcom/android/settings/datausage/AppDataUsage;->kp:Landroid/preference/SwitchPreference;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/preference/SwitchPreference;->setEnabled(Z)V

    iget-object v0, p0, Lcom/android/settings/datausage/AppDataUsage;->kp:Landroid/preference/SwitchPreference;

    invoke-virtual {v0, p2}, Landroid/preference/SwitchPreference;->setChecked(Z)V

    goto :goto_0
.end method


# virtual methods
.method public at(Landroid/preference/Preference;)Z
    .locals 4

    iget-object v0, p0, Lcom/android/settings/datausage/AppDataUsage;->jW:Landroid/preference/Preference;

    if-ne p1, v0, :cond_0

    invoke-virtual {p0}, Lcom/android/settings/datausage/AppDataUsage;->getActivity()Landroid/app/Activity;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/datausage/AppDataUsage;->jX:Landroid/content/Intent;

    new-instance v2, Landroid/os/UserHandle;

    iget-object v3, p0, Lcom/android/settings/datausage/AppDataUsage;->jT:Lcom/android/settingslib/AppItem;

    iget v3, v3, Lcom/android/settingslib/AppItem;->key:I

    invoke-static {v3}, Landroid/os/UserHandle;->getUserId(I)I

    move-result v3

    invoke-direct {v2, v3}, Landroid/os/UserHandle;-><init>(I)V

    invoke-virtual {v0, v1, v2}, Landroid/app/Activity;->startActivityAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    const/4 v0, 0x1

    return v0

    :cond_0
    invoke-super {p0, p1}, Lcom/android/settings/datausage/DataUsageBase;->at(Landroid/preference/Preference;)Z

    move-result v0

    return v0
.end method

.method public getMetricsCategory()I
    .locals 1

    const/16 v0, 0x157

    return v0
.end method

.method public iA(IZ)V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/datausage/AppDataUsage;->jT:Lcom/android/settingslib/AppItem;

    iget-object v0, v0, Lcom/android/settingslib/AppItem;->cQL:Landroid/util/SparseBooleanArray;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Landroid/util/SparseBooleanArray;->get(IZ)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/android/settings/datausage/AppDataUsage;->jw()Z

    move-result v0

    invoke-direct {p0, v0, p2}, Lcom/android/settings/datausage/AppDataUsage;->jz(ZZ)V

    :cond_0
    return-void
.end method

.method public iy(IZ)V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/datausage/AppDataUsage;->jT:Lcom/android/settingslib/AppItem;

    iget-object v0, v0, Lcom/android/settingslib/AppItem;->cQL:Landroid/util/SparseBooleanArray;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Landroid/util/SparseBooleanArray;->get(IZ)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/android/settings/datausage/AppDataUsage;->jx()Z

    move-result v0

    invoke-direct {p0, p2, v0}, Lcom/android/settings/datausage/AppDataUsage;->jz(ZZ)V

    :cond_0
    return-void
.end method

.method public iz(Z)V
    .locals 0

    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 7

    const/4 v3, 0x1

    const/4 v6, -0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    invoke-super {p0, p1}, Lcom/android/settings/datausage/DataUsageBase;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/android/settings/datausage/AppDataUsage;->getArguments()Landroid/os/Bundle;

    move-result-object v4

    :try_start_0
    iget-object v0, p0, Lcom/android/settings/datausage/AppDataUsage;->jr:Lcom/android/settings/datausage/TemplatePreference$NetworkServices;

    iget-object v0, v0, Lcom/android/settings/datausage/TemplatePreference$NetworkServices;->jn:Landroid/net/INetworkStatsService;

    invoke-interface {v0}, Landroid/net/INetworkStatsService;->openSession()Landroid/net/INetworkStatsSession;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/datausage/AppDataUsage;->km:Landroid/net/INetworkStatsSession;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    if-eqz v4, :cond_6

    const-string/jumbo v0, "app_item"

    invoke-virtual {v4, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/android/settingslib/AppItem;

    :goto_0
    iput-object v0, p0, Lcom/android/settings/datausage/AppDataUsage;->jT:Lcom/android/settingslib/AppItem;

    if-eqz v4, :cond_7

    const-string/jumbo v0, "network_template"

    invoke-virtual {v4, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/NetworkTemplate;

    :goto_1
    iput-object v0, p0, Lcom/android/settings/datausage/AppDataUsage;->kn:Landroid/net/NetworkTemplate;

    iget-object v0, p0, Lcom/android/settings/datausage/AppDataUsage;->kn:Landroid/net/NetworkTemplate;

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/android/settings/datausage/AppDataUsage;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/android/settings/datausage/DataUsageSummary;->jm(Landroid/content/Context;)I

    move-result v5

    invoke-static {v0, v5}, Lcom/android/settings/datausage/DataUsageSummary;->jn(Landroid/content/Context;I)Landroid/net/NetworkTemplate;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/datausage/AppDataUsage;->kn:Landroid/net/NetworkTemplate;

    :cond_0
    iget-object v0, p0, Lcom/android/settings/datausage/AppDataUsage;->jT:Lcom/android/settingslib/AppItem;

    if-nez v0, :cond_a

    if-eqz v4, :cond_8

    const-string/jumbo v0, "uid"

    invoke-virtual {v4, v0, v6}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    :goto_2
    if-ne v0, v6, :cond_9

    invoke-virtual {p0}, Lcom/android/settings/datausage/AppDataUsage;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    :cond_1
    :goto_3
    const v0, 0x7f150017

    invoke-virtual {p0, v0}, Lcom/android/settings/datausage/AppDataUsage;->addPreferencesFromResource(I)V

    const-string/jumbo v0, "total_usage"

    invoke-virtual {p0, v0}, Lcom/android/settings/datausage/AppDataUsage;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/datausage/AppDataUsage;->ko:Landroid/preference/Preference;

    const-string/jumbo v0, "foreground_usage"

    invoke-virtual {p0, v0}, Lcom/android/settings/datausage/AppDataUsage;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/datausage/AppDataUsage;->kg:Landroid/preference/Preference;

    const-string/jumbo v0, "background_usage"

    invoke-virtual {p0, v0}, Lcom/android/settings/datausage/AppDataUsage;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/datausage/AppDataUsage;->jY:Landroid/preference/Preference;

    const-string/jumbo v0, "cycle"

    invoke-virtual {p0, v0}, Lcom/android/settings/datausage/AppDataUsage;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Lcom/android/settings/datausage/SpinnerPreference;

    iput-object v0, p0, Lcom/android/settings/datausage/AppDataUsage;->kb:Lcom/android/settings/datausage/SpinnerPreference;

    new-instance v0, Lcom/android/settings/datausage/CycleAdapter;

    invoke-virtual {p0}, Lcom/android/settings/datausage/AppDataUsage;->getContext()Landroid/content/Context;

    move-result-object v4

    iget-object v5, p0, Lcom/android/settings/datausage/AppDataUsage;->kb:Lcom/android/settings/datausage/SpinnerPreference;

    iget-object v6, p0, Lcom/android/settings/datausage/AppDataUsage;->kd:Landroid/widget/AdapterView$OnItemSelectedListener;

    invoke-direct {v0, v4, v5, v6, v2}, Lcom/android/settings/datausage/CycleAdapter;-><init>(Landroid/content/Context;Lcom/android/settings/datausage/CycleAdapter$SpinnerInterface;Landroid/widget/AdapterView$OnItemSelectedListener;Z)V

    iput-object v0, p0, Lcom/android/settings/datausage/AppDataUsage;->kc:Lcom/android/settings/datausage/CycleAdapter;

    iget-object v0, p0, Lcom/android/settings/datausage/AppDataUsage;->jT:Lcom/android/settingslib/AppItem;

    iget v0, v0, Lcom/android/settingslib/AppItem;->key:I

    if-lez v0, :cond_d

    iget-object v0, p0, Lcom/android/settings/datausage/AppDataUsage;->ki:Landroid/util/ArraySet;

    invoke-virtual {v0}, Landroid/util/ArraySet;->size()I

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lcom/android/settings/datausage/AppDataUsage;->bWA()Landroid/content/pm/PackageManager;

    move-result-object v4

    :try_start_1
    iget-object v0, p0, Lcom/android/settings/datausage/AppDataUsage;->ki:Landroid/util/ArraySet;

    const/4 v5, 0x0

    invoke-virtual {v0, v5}, Landroid/util/ArraySet;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const/4 v5, 0x0

    invoke-virtual {v4, v0, v5}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v0

    invoke-virtual {p0}, Lcom/android/settings/datausage/AppDataUsage;->getActivity()Landroid/app/Activity;

    move-result-object v5

    invoke-static {v5}, Landroid/util/IconDrawableFactory;->newInstance(Landroid/content/Context;)Landroid/util/IconDrawableFactory;

    move-result-object v5

    invoke-virtual {v5, v0}, Landroid/util/IconDrawableFactory;->getBadgedIcon(Landroid/content/pm/ApplicationInfo;)Landroid/graphics/drawable/Drawable;

    move-result-object v5

    iput-object v5, p0, Lcom/android/settings/datausage/AppDataUsage;->mIcon:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v4}, Landroid/content/pm/ApplicationInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v4

    iput-object v4, p0, Lcom/android/settings/datausage/AppDataUsage;->kh:Ljava/lang/CharSequence;

    iget-object v0, v0, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    iput-object v0, p0, Lcom/android/settings/datausage/AppDataUsage;->mPackageName:Ljava/lang/String;
    :try_end_1
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_1 .. :try_end_1} :catch_1

    :cond_2
    :goto_4
    iget-object v0, p0, Lcom/android/settings/datausage/AppDataUsage;->jT:Lcom/android/settingslib/AppItem;

    iget v0, v0, Lcom/android/settingslib/AppItem;->key:I

    invoke-static {v0}, Landroid/os/UserHandle;->isApp(I)Z

    move-result v0

    if-nez v0, :cond_b

    const-string/jumbo v0, "unrestricted_data_saver"

    invoke-virtual {p0, v0}, Lcom/android/settings/datausage/AppDataUsage;->bWK(Ljava/lang/String;)V

    const-string/jumbo v0, "restrict_background"

    invoke-virtual {p0, v0}, Lcom/android/settings/datausage/AppDataUsage;->bWK(Ljava/lang/String;)V

    :goto_5
    new-instance v0, Lcom/android/settings/datausage/DataSaverBackend;

    invoke-virtual {p0}, Lcom/android/settings/datausage/AppDataUsage;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-direct {v0, v4}, Lcom/android/settings/datausage/DataSaverBackend;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/settings/datausage/AppDataUsage;->ke:Lcom/android/settings/datausage/DataSaverBackend;

    const-string/jumbo v0, "app_settings"

    invoke-virtual {p0, v0}, Lcom/android/settings/datausage/AppDataUsage;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/datausage/AppDataUsage;->jW:Landroid/preference/Preference;

    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v4, "android.intent.action.MANAGE_NETWORK_USAGE"

    invoke-direct {v0, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/android/settings/datausage/AppDataUsage;->jX:Landroid/content/Intent;

    iget-object v0, p0, Lcom/android/settings/datausage/AppDataUsage;->jX:Landroid/content/Intent;

    const-string/jumbo v4, "android.intent.category.DEFAULT"

    invoke-virtual {v0, v4}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/android/settings/datausage/AppDataUsage;->bWA()Landroid/content/pm/PackageManager;

    move-result-object v4

    iget-object v0, p0, Lcom/android/settings/datausage/AppDataUsage;->ki:Landroid/util/ArraySet;

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_3
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iget-object v6, p0, Lcom/android/settings/datausage/AppDataUsage;->jX:Landroid/content/Intent;

    invoke-virtual {v6, v0}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    iget-object v0, p0, Lcom/android/settings/datausage/AppDataUsage;->jX:Landroid/content/Intent;

    invoke-virtual {v4, v0, v2}, Landroid/content/pm/PackageManager;->resolveActivity(Landroid/content/Intent;I)Landroid/content/pm/ResolveInfo;

    move-result-object v0

    if-eqz v0, :cond_3

    move v2, v3

    :cond_4
    if-nez v2, :cond_5

    const-string/jumbo v0, "app_settings"

    invoke-virtual {p0, v0}, Lcom/android/settings/datausage/AppDataUsage;->bWK(Ljava/lang/String;)V

    iput-object v1, p0, Lcom/android/settings/datausage/AppDataUsage;->jW:Landroid/preference/Preference;

    :cond_5
    iget-object v0, p0, Lcom/android/settings/datausage/AppDataUsage;->ki:Landroid/util/ArraySet;

    invoke-virtual {v0}, Landroid/util/ArraySet;->size()I

    move-result v0

    if-le v0, v3, :cond_c

    const-string/jumbo v0, "app_list"

    invoke-virtual {p0, v0}, Lcom/android/settings/datausage/AppDataUsage;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceCategory;

    iput-object v0, p0, Lcom/android/settings/datausage/AppDataUsage;->jU:Landroid/preference/PreferenceCategory;

    invoke-virtual {p0}, Lcom/android/settings/datausage/AppDataUsage;->getLoaderManager()Landroid/app/LoaderManager;

    move-result-object v0

    sget-object v1, Landroid/os/Bundle;->EMPTY:Landroid/os/Bundle;

    iget-object v2, p0, Lcom/android/settings/datausage/AppDataUsage;->jV:Landroid/app/LoaderManager$LoaderCallbacks;

    const/4 v3, 0x3

    invoke-virtual {v0, v3, v1, v2}, Landroid/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/app/LoaderManager$LoaderCallbacks;)Landroid/content/Loader;

    :goto_6
    return-void

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    :cond_6
    move-object v0, v1

    goto/16 :goto_0

    :cond_7
    move-object v0, v1

    goto/16 :goto_1

    :cond_8
    invoke-virtual {p0}, Lcom/android/settings/datausage/AppDataUsage;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v4, "uid"

    invoke-virtual {v0, v4, v6}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    goto/16 :goto_2

    :cond_9
    invoke-direct {p0, v0}, Lcom/android/settings/datausage/AppDataUsage;->ju(I)V

    new-instance v4, Lcom/android/settingslib/AppItem;

    invoke-direct {v4, v0}, Lcom/android/settingslib/AppItem;-><init>(I)V

    iput-object v4, p0, Lcom/android/settings/datausage/AppDataUsage;->jT:Lcom/android/settingslib/AppItem;

    iget-object v4, p0, Lcom/android/settings/datausage/AppDataUsage;->jT:Lcom/android/settingslib/AppItem;

    invoke-virtual {v4, v0}, Lcom/android/settingslib/AppItem;->cse(I)V

    goto/16 :goto_3

    :cond_a
    move v0, v2

    :goto_7
    iget-object v4, p0, Lcom/android/settings/datausage/AppDataUsage;->jT:Lcom/android/settingslib/AppItem;

    iget-object v4, v4, Lcom/android/settingslib/AppItem;->cQL:Landroid/util/SparseBooleanArray;

    invoke-virtual {v4}, Landroid/util/SparseBooleanArray;->size()I

    move-result v4

    if-ge v0, v4, :cond_1

    iget-object v4, p0, Lcom/android/settings/datausage/AppDataUsage;->jT:Lcom/android/settingslib/AppItem;

    iget-object v4, v4, Lcom/android/settingslib/AppItem;->cQL:Landroid/util/SparseBooleanArray;

    invoke-virtual {v4, v0}, Landroid/util/SparseBooleanArray;->keyAt(I)I

    move-result v4

    invoke-direct {p0, v4}, Lcom/android/settings/datausage/AppDataUsage;->ju(I)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_7

    :cond_b
    const-string/jumbo v0, "restrict_background"

    invoke-virtual {p0, v0}, Lcom/android/settings/datausage/AppDataUsage;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/SwitchPreference;

    iput-object v0, p0, Lcom/android/settings/datausage/AppDataUsage;->kk:Landroid/preference/SwitchPreference;

    iget-object v0, p0, Lcom/android/settings/datausage/AppDataUsage;->kk:Landroid/preference/SwitchPreference;

    invoke-virtual {v0, p0}, Landroid/preference/SwitchPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    const-string/jumbo v0, "unrestricted_data_saver"

    invoke-virtual {p0, v0}, Lcom/android/settings/datausage/AppDataUsage;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/SwitchPreference;

    iput-object v0, p0, Lcom/android/settings/datausage/AppDataUsage;->kp:Landroid/preference/SwitchPreference;

    iget-object v0, p0, Lcom/android/settings/datausage/AppDataUsage;->kp:Landroid/preference/SwitchPreference;

    invoke-virtual {v0, p0}, Landroid/preference/SwitchPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    goto/16 :goto_5

    :cond_c
    const-string/jumbo v0, "app_list"

    invoke-virtual {p0, v0}, Lcom/android/settings/datausage/AppDataUsage;->bWK(Ljava/lang/String;)V

    goto :goto_6

    :cond_d
    invoke-virtual {p0}, Lcom/android/settings/datausage/AppDataUsage;->getActivity()Landroid/app/Activity;

    move-result-object v0

    new-instance v1, Lcom/android/settingslib/g/f;

    invoke-direct {v1, v0}, Lcom/android/settingslib/g/f;-><init>(Landroid/content/Context;)V

    iget-object v0, p0, Lcom/android/settings/datausage/AppDataUsage;->jT:Lcom/android/settingslib/AppItem;

    iget v0, v0, Lcom/android/settingslib/AppItem;->key:I

    invoke-virtual {v1, v0, v3}, Lcom/android/settingslib/g/f;->coN(IZ)Lcom/android/settingslib/g/a;

    move-result-object v0

    iget-object v1, v0, Lcom/android/settingslib/g/a;->icon:Landroid/graphics/drawable/Drawable;

    iput-object v1, p0, Lcom/android/settings/datausage/AppDataUsage;->mIcon:Landroid/graphics/drawable/Drawable;

    iget-object v0, v0, Lcom/android/settingslib/g/a;->cJa:Ljava/lang/CharSequence;

    iput-object v0, p0, Lcom/android/settings/datausage/AppDataUsage;->kh:Ljava/lang/CharSequence;

    invoke-virtual {p0}, Lcom/android/settings/datausage/AppDataUsage;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getPackageName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/datausage/AppDataUsage;->mPackageName:Ljava/lang/String;

    const-string/jumbo v0, "unrestricted_data_saver"

    invoke-virtual {p0, v0}, Lcom/android/settings/datausage/AppDataUsage;->bWK(Ljava/lang/String;)V

    const-string/jumbo v0, "app_settings"

    invoke-virtual {p0, v0}, Lcom/android/settings/datausage/AppDataUsage;->bWK(Ljava/lang/String;)V

    const-string/jumbo v0, "restrict_background"

    invoke-virtual {p0, v0}, Lcom/android/settings/datausage/AppDataUsage;->bWK(Ljava/lang/String;)V

    const-string/jumbo v0, "app_list"

    invoke-virtual {p0, v0}, Lcom/android/settings/datausage/AppDataUsage;->bWK(Ljava/lang/String;)V

    goto/16 :goto_6

    :catch_1
    move-exception v0

    goto/16 :goto_4
.end method

.method public onDestroy()V
    .locals 1

    iget-object v0, p0, Lcom/android/settings/datausage/AppDataUsage;->km:Landroid/net/INetworkStatsSession;

    invoke-static {v0}, Landroid/net/TrafficStats;->closeQuietly(Landroid/net/INetworkStatsSession;)V

    invoke-super {p0}, Lcom/android/settings/datausage/DataUsageBase;->onDestroy()V

    return-void
.end method

.method public onPause()V
    .locals 1

    invoke-super {p0}, Lcom/android/settings/datausage/DataUsageBase;->onPause()V

    iget-object v0, p0, Lcom/android/settings/datausage/AppDataUsage;->ke:Lcom/android/settings/datausage/DataSaverBackend;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/datausage/AppDataUsage;->ke:Lcom/android/settings/datausage/DataSaverBackend;

    invoke-virtual {v0, p0}, Lcom/android/settings/datausage/DataSaverBackend;->ko(Lcom/android/settings/datausage/DataSaverBackend$Listener;)V

    :cond_0
    return-void
.end method

.method public onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 5

    const/4 v4, 0x1

    iget-object v0, p0, Lcom/android/settings/datausage/AppDataUsage;->kk:Landroid/preference/SwitchPreference;

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/datausage/AppDataUsage;->ke:Lcom/android/settings/datausage/DataSaverBackend;

    iget-object v1, p0, Lcom/android/settings/datausage/AppDataUsage;->jT:Lcom/android/settingslib/AppItem;

    iget v1, v1, Lcom/android/settingslib/AppItem;->key:I

    iget-object v2, p0, Lcom/android/settings/datausage/AppDataUsage;->mPackageName:Ljava/lang/String;

    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    xor-int/lit8 v3, v3, 0x1

    invoke-virtual {v0, v1, v2, v3}, Lcom/android/settings/datausage/DataSaverBackend;->kx(ILjava/lang/String;Z)V

    return v4

    :cond_0
    iget-object v0, p0, Lcom/android/settings/datausage/AppDataUsage;->kp:Landroid/preference/SwitchPreference;

    if-ne p1, v0, :cond_1

    iget-object v0, p0, Lcom/android/settings/datausage/AppDataUsage;->ke:Lcom/android/settings/datausage/DataSaverBackend;

    iget-object v1, p0, Lcom/android/settings/datausage/AppDataUsage;->jT:Lcom/android/settingslib/AppItem;

    iget v1, v1, Lcom/android/settingslib/AppItem;->key:I

    iget-object v2, p0, Lcom/android/settings/datausage/AppDataUsage;->mPackageName:Ljava/lang/String;

    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/android/settings/datausage/DataSaverBackend;->kp(ILjava/lang/String;Z)V

    return v4

    :cond_1
    const/4 v0, 0x0

    return v0
.end method

.method public onResume()V
    .locals 4

    invoke-super {p0}, Lcom/android/settings/datausage/DataUsageBase;->onResume()V

    iget-object v0, p0, Lcom/android/settings/datausage/AppDataUsage;->ke:Lcom/android/settings/datausage/DataSaverBackend;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/datausage/AppDataUsage;->ke:Lcom/android/settings/datausage/DataSaverBackend;

    invoke-virtual {v0, p0}, Lcom/android/settings/datausage/DataSaverBackend;->kn(Lcom/android/settings/datausage/DataSaverBackend$Listener;)V

    :cond_0
    iget-object v0, p0, Lcom/android/settings/datausage/AppDataUsage;->jr:Lcom/android/settings/datausage/TemplatePreference$NetworkServices;

    iget-object v0, v0, Lcom/android/settings/datausage/TemplatePreference$NetworkServices;->jj:Lcom/android/settingslib/D;

    iget-object v1, p0, Lcom/android/settings/datausage/AppDataUsage;->kn:Landroid/net/NetworkTemplate;

    invoke-virtual {v0, v1}, Lcom/android/settingslib/D;->crU(Landroid/net/NetworkTemplate;)Landroid/net/NetworkPolicy;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/datausage/AppDataUsage;->kj:Landroid/net/NetworkPolicy;

    invoke-virtual {p0}, Lcom/android/settings/datausage/AppDataUsage;->getLoaderManager()Landroid/app/LoaderManager;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/datausage/AppDataUsage;->kn:Landroid/net/NetworkTemplate;

    iget-object v2, p0, Lcom/android/settings/datausage/AppDataUsage;->jT:Lcom/android/settingslib/AppItem;

    invoke-static {v1, v2}, Lcom/android/settingslib/g/g;->coP(Landroid/net/NetworkTemplate;Lcom/android/settingslib/AppItem;)Landroid/os/Bundle;

    move-result-object v1

    iget-object v2, p0, Lcom/android/settings/datausage/AppDataUsage;->ka:Landroid/app/LoaderManager$LoaderCallbacks;

    const/4 v3, 0x2

    invoke-virtual {v0, v3, v1, v2}, Landroid/app/LoaderManager;->restartLoader(ILandroid/os/Bundle;Landroid/app/LoaderManager$LoaderCallbacks;)Landroid/content/Loader;

    invoke-direct {p0}, Lcom/android/settings/datausage/AppDataUsage;->jy()V

    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 3

    const/4 v0, 0x0

    const/4 v2, 0x0

    invoke-super {p0, p1, p2}, Lcom/android/settings/datausage/DataUsageBase;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    iget-object v1, p0, Lcom/android/settings/datausage/AppDataUsage;->ki:Landroid/util/ArraySet;

    invoke-virtual {v1}, Landroid/util/ArraySet;->size()I

    move-result v1

    if-eqz v1, :cond_0

    iget-object v0, p0, Lcom/android/settings/datausage/AppDataUsage;->ki:Landroid/util/ArraySet;

    invoke-virtual {v0, v2}, Landroid/util/ArraySet;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    :cond_0
    if-eqz v0, :cond_1

    :try_start_0
    invoke-virtual {p0}, Lcom/android/settings/datausage/AppDataUsage;->bWA()Landroid/content/pm/PackageManager;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Landroid/content/pm/PackageManager;->getPackageUid(Ljava/lang/String;I)I
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_1
    :goto_0
    invoke-virtual {p0}, Lcom/android/settings/datausage/AppDataUsage;->getActivity()Landroid/app/Activity;

    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method
