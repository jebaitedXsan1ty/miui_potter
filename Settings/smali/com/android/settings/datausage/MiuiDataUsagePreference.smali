.class public Lcom/android/settings/datausage/MiuiDataUsagePreference;
.super Landroid/preference/Preference;
.source "MiuiDataUsagePreference.java"

# interfaces
.implements Lcom/android/settings/datausage/TemplatePreference;


# instance fields
.field private iI:I

.field private iJ:Landroid/net/NetworkTemplate;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Landroid/preference/Preference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method


# virtual methods
.method public getIntent()Landroid/content/Intent;
    .locals 8

    const/4 v4, 0x0

    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    const-string/jumbo v0, "network_template"

    iget-object v1, p0, Lcom/android/settings/datausage/MiuiDataUsagePreference;->iJ:Landroid/net/NetworkTemplate;

    invoke-virtual {v2, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    const-string/jumbo v0, "sub_id"

    iget v1, p0, Lcom/android/settings/datausage/MiuiDataUsagePreference;->iI:I

    invoke-virtual {v2, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    invoke-virtual {p0}, Lcom/android/settings/datausage/MiuiDataUsagePreference;->getContext()Landroid/content/Context;

    move-result-object v0

    const-class v1, Lcom/android/settings/datausage/DataUsageList;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/android/settings/datausage/MiuiDataUsagePreference;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0}, Lcom/android/settings/datausage/MiuiDataUsagePreference;->getTitle()Ljava/lang/CharSequence;

    move-result-object v5

    move v6, v4

    move v7, v4

    invoke-static/range {v0 .. v7}, Lcom/android/settings/aq;->bqs(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;Ljava/lang/String;ILjava/lang/CharSequence;ZI)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public hR(Landroid/net/NetworkTemplate;ILcom/android/settings/datausage/TemplatePreference$NetworkServices;)V
    .locals 6

    iput-object p1, p0, Lcom/android/settings/datausage/MiuiDataUsagePreference;->iJ:Landroid/net/NetworkTemplate;

    iput p2, p0, Lcom/android/settings/datausage/MiuiDataUsagePreference;->iI:I

    new-instance v0, Lcom/android/settingslib/g/b;

    invoke-virtual {p0}, Lcom/android/settings/datausage/MiuiDataUsagePreference;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/android/settingslib/g/b;-><init>(Landroid/content/Context;)V

    iget-object v1, p0, Lcom/android/settings/datausage/MiuiDataUsagePreference;->iJ:Landroid/net/NetworkTemplate;

    invoke-virtual {v0, v1}, Lcom/android/settingslib/g/b;->coG(Landroid/net/NetworkTemplate;)Lcom/android/settingslib/g/d;

    move-result-object v0

    invoke-virtual {p0}, Lcom/android/settings/datausage/MiuiDataUsagePreference;->getContext()Landroid/content/Context;

    move-result-object v1

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {p0}, Lcom/android/settings/datausage/MiuiDataUsagePreference;->getContext()Landroid/content/Context;

    move-result-object v3

    iget-wide v4, v0, Lcom/android/settingslib/g/d;->cJl:J

    invoke-static {v3, v4, v5}, Landroid/text/format/Formatter;->formatFileSize(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    aput-object v3, v2, v4

    iget-object v0, v0, Lcom/android/settingslib/g/d;->cJp:Ljava/lang/String;

    const/4 v3, 0x1

    aput-object v0, v2, v3

    const v0, 0x7f120523

    invoke-virtual {v1, v0, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/settings/datausage/MiuiDataUsagePreference;->setSummary(Ljava/lang/CharSequence;)V

    invoke-virtual {p0}, Lcom/android/settings/datausage/MiuiDataUsagePreference;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/settings/datausage/MiuiDataUsagePreference;->setIntent(Landroid/content/Intent;)V

    return-void
.end method
