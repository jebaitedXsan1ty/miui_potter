.class final Lcom/android/settings/datausage/DataUsageList$3;
.super Ljava/lang/Object;
.source "DataUsageList.java"

# interfaces
.implements Landroid/app/LoaderManager$LoaderCallbacks;


# instance fields
.field final synthetic lt:Lcom/android/settings/datausage/DataUsageList;


# direct methods
.method constructor <init>(Lcom/android/settings/datausage/DataUsageList;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/datausage/DataUsageList$3;->lt:Lcom/android/settings/datausage/DataUsageList;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public kS(Landroid/content/Loader;Lcom/android/settingslib/g/e;)V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/datausage/DataUsageList$3;->lt:Lcom/android/settings/datausage/DataUsageList;

    invoke-static {v0, p2}, Lcom/android/settings/datausage/DataUsageList;->ki(Lcom/android/settings/datausage/DataUsageList;Lcom/android/settingslib/g/e;)Lcom/android/settingslib/g/e;

    iget-object v0, p0, Lcom/android/settings/datausage/DataUsageList$3;->lt:Lcom/android/settings/datausage/DataUsageList;

    invoke-static {v0}, Lcom/android/settings/datausage/DataUsageList;->kd(Lcom/android/settings/datausage/DataUsageList;)Lcom/android/settings/datausage/ChartDataUsagePreference;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/datausage/DataUsageList$3;->lt:Lcom/android/settings/datausage/DataUsageList;

    invoke-static {v1}, Lcom/android/settings/datausage/DataUsageList;->ke(Lcom/android/settings/datausage/DataUsageList;)Lcom/android/settingslib/g/e;

    move-result-object v1

    iget-object v1, v1, Lcom/android/settingslib/g/e;->cJs:Landroid/net/NetworkStatsHistory;

    invoke-virtual {v0, v1}, Lcom/android/settings/datausage/ChartDataUsagePreference;->ik(Landroid/net/NetworkStatsHistory;)V

    iget-object v0, p0, Lcom/android/settings/datausage/DataUsageList$3;->lt:Lcom/android/settings/datausage/DataUsageList;

    invoke-static {v0}, Lcom/android/settings/datausage/DataUsageList;->km(Lcom/android/settings/datausage/DataUsageList;)V

    return-void
.end method

.method public onCreateLoader(ILandroid/os/Bundle;)Landroid/content/Loader;
    .locals 3

    new-instance v0, Lcom/android/settingslib/g/g;

    iget-object v1, p0, Lcom/android/settings/datausage/DataUsageList$3;->lt:Lcom/android/settings/datausage/DataUsageList;

    invoke-virtual {v1}, Lcom/android/settings/datausage/DataUsageList;->getActivity()Landroid/app/Activity;

    move-result-object v1

    iget-object v2, p0, Lcom/android/settings/datausage/DataUsageList$3;->lt:Lcom/android/settings/datausage/DataUsageList;

    invoke-static {v2}, Lcom/android/settings/datausage/DataUsageList;->kg(Lcom/android/settings/datausage/DataUsageList;)Landroid/net/INetworkStatsSession;

    move-result-object v2

    invoke-direct {v0, v1, v2, p2}, Lcom/android/settingslib/g/g;-><init>(Landroid/content/Context;Landroid/net/INetworkStatsSession;Landroid/os/Bundle;)V

    return-object v0
.end method

.method public bridge synthetic onLoadFinished(Landroid/content/Loader;Ljava/lang/Object;)V
    .locals 0

    check-cast p2, Lcom/android/settingslib/g/e;

    invoke-virtual {p0, p1, p2}, Lcom/android/settings/datausage/DataUsageList$3;->kS(Landroid/content/Loader;Lcom/android/settingslib/g/e;)V

    return-void
.end method

.method public onLoaderReset(Landroid/content/Loader;)V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/android/settings/datausage/DataUsageList$3;->lt:Lcom/android/settings/datausage/DataUsageList;

    invoke-static {v0, v1}, Lcom/android/settings/datausage/DataUsageList;->ki(Lcom/android/settings/datausage/DataUsageList;Lcom/android/settingslib/g/e;)Lcom/android/settingslib/g/e;

    iget-object v0, p0, Lcom/android/settings/datausage/DataUsageList$3;->lt:Lcom/android/settings/datausage/DataUsageList;

    invoke-static {v0}, Lcom/android/settings/datausage/DataUsageList;->kd(Lcom/android/settings/datausage/DataUsageList;)Lcom/android/settings/datausage/ChartDataUsagePreference;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/android/settings/datausage/ChartDataUsagePreference;->ik(Landroid/net/NetworkStatsHistory;)V

    return-void
.end method
