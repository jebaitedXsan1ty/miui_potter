.class public Lcom/android/settings/datausage/CycleAdapter;
.super Landroid/widget/ArrayAdapter;
.source "CycleAdapter.java"


# instance fields
.field private final jB:Landroid/widget/AdapterView$OnItemSelectedListener;

.field private final jC:Lcom/android/settings/datausage/CycleAdapter$SpinnerInterface;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/android/settings/datausage/CycleAdapter$SpinnerInterface;Landroid/widget/AdapterView$OnItemSelectedListener;Z)V
    .locals 2

    if-eqz p4, :cond_0

    const v0, 0x7f0d00b3

    :goto_0
    invoke-direct {p0, p1, v0}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I)V

    const v0, 0x1090009

    invoke-virtual {p0, v0}, Lcom/android/settings/datausage/CycleAdapter;->setDropDownViewResource(I)V

    iput-object p2, p0, Lcom/android/settings/datausage/CycleAdapter;->jC:Lcom/android/settings/datausage/CycleAdapter$SpinnerInterface;

    iput-object p3, p0, Lcom/android/settings/datausage/CycleAdapter;->jB:Landroid/widget/AdapterView$OnItemSelectedListener;

    iget-object v0, p0, Lcom/android/settings/datausage/CycleAdapter;->jC:Lcom/android/settings/datausage/CycleAdapter$SpinnerInterface;

    invoke-interface {v0, p0}, Lcom/android/settings/datausage/CycleAdapter$SpinnerInterface;->hV(Lcom/android/settings/datausage/CycleAdapter;)V

    iget-object v0, p0, Lcom/android/settings/datausage/CycleAdapter;->jC:Lcom/android/settings/datausage/CycleAdapter$SpinnerInterface;

    iget-object v1, p0, Lcom/android/settings/datausage/CycleAdapter;->jB:Landroid/widget/AdapterView$OnItemSelectedListener;

    invoke-interface {v0, v1}, Lcom/android/settings/datausage/CycleAdapter$SpinnerInterface;->hW(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    return-void

    :cond_0
    const v0, 0x7f0d008c

    goto :goto_0
.end method


# virtual methods
.method public je(Lcom/android/settings/datausage/CycleAdapter$CycleItem;)I
    .locals 3

    const/4 v2, 0x0

    if-eqz p1, :cond_1

    invoke-virtual {p0}, Lcom/android/settings/datausage/CycleAdapter;->getCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_0
    if-ltz v1, :cond_1

    invoke-virtual {p0, v1}, Lcom/android/settings/datausage/CycleAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settings/datausage/CycleAdapter$CycleItem;

    invoke-virtual {v0, p1}, Lcom/android/settings/datausage/CycleAdapter$CycleItem;->jg(Lcom/android/settings/datausage/CycleAdapter$CycleItem;)I

    move-result v0

    if-ltz v0, :cond_0

    return v1

    :cond_0
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0

    :cond_1
    return v2
.end method

.method public jf(Landroid/net/NetworkPolicy;Lcom/android/settingslib/g/e;)Z
    .locals 20

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/settings/datausage/CycleAdapter;->jC:Lcom/android/settings/datausage/CycleAdapter$SpinnerInterface;

    invoke-interface {v2}, Lcom/android/settings/datausage/CycleAdapter$SpinnerInterface;->hU()Ljava/lang/Object;

    move-result-object v2

    move-object v9, v2

    check-cast v9, Lcom/android/settings/datausage/CycleAdapter$CycleItem;

    invoke-virtual/range {p0 .. p0}, Lcom/android/settings/datausage/CycleAdapter;->clear()V

    invoke-virtual/range {p0 .. p0}, Lcom/android/settings/datausage/CycleAdapter;->getContext()Landroid/content/Context;

    move-result-object v11

    const/4 v8, 0x0

    const-wide v4, 0x7fffffffffffffffL

    const-wide/high16 v2, -0x8000000000000000L

    if-eqz p2, :cond_0

    move-object/from16 v0, p2

    iget-object v2, v0, Lcom/android/settingslib/g/e;->cJs:Landroid/net/NetworkStatsHistory;

    invoke-virtual {v2}, Landroid/net/NetworkStatsHistory;->getStart()J

    move-result-wide v4

    move-object/from16 v0, p2

    iget-object v2, v0, Lcom/android/settingslib/g/e;->cJs:Landroid/net/NetworkStatsHistory;

    invoke-virtual {v2}, Landroid/net/NetworkStatsHistory;->getEnd()J

    move-result-wide v2

    :cond_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    const-wide v12, 0x7fffffffffffffffL

    cmp-long v10, v4, v12

    if-nez v10, :cond_c

    move-wide v14, v6

    :goto_0
    const-wide/high16 v4, -0x8000000000000000L

    cmp-long v4, v2, v4

    if-nez v4, :cond_b

    const-wide/16 v2, 0x1

    add-long v12, v6, v2

    :goto_1
    const/4 v2, 0x0

    if-eqz p1, :cond_3

    invoke-static/range {p1 .. p1}, Landroid/net/NetworkPolicyManager;->cycleIterator(Landroid/net/NetworkPolicy;)Ljava/util/Iterator;

    move-result-object v16

    move v10, v2

    :goto_2
    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/util/Pair;

    iget-object v3, v2, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v3, Ljava/time/ZonedDateTime;

    invoke-virtual {v3}, Ljava/time/ZonedDateTime;->toInstant()Ljava/time/Instant;

    move-result-object v3

    invoke-virtual {v3}, Ljava/time/Instant;->toEpochMilli()J

    move-result-wide v4

    iget-object v2, v2, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v2, Ljava/time/ZonedDateTime;

    invoke-virtual {v2}, Ljava/time/ZonedDateTime;->toInstant()Ljava/time/Instant;

    move-result-object v2

    invoke-virtual {v2}, Ljava/time/Instant;->toEpochMilli()J

    move-result-wide v6

    if-eqz p2, :cond_2

    move-object/from16 v0, p2

    iget-object v3, v0, Lcom/android/settingslib/g/e;->cJs:Landroid/net/NetworkStatsHistory;

    invoke-virtual/range {v3 .. v8}, Landroid/net/NetworkStatsHistory;->getValues(JJLandroid/net/NetworkStatsHistory$Entry;)Landroid/net/NetworkStatsHistory$Entry;

    move-result-object v8

    iget-wide v2, v8, Landroid/net/NetworkStatsHistory$Entry;->rxBytes:J

    iget-wide v0, v8, Landroid/net/NetworkStatsHistory$Entry;->txBytes:J

    move-wide/from16 v18, v0

    add-long v2, v2, v18

    const-wide/16 v18, 0x0

    cmp-long v2, v2, v18

    if-lez v2, :cond_1

    const/4 v2, 0x1

    :goto_3
    if-eqz v2, :cond_a

    new-instance v2, Lcom/android/settings/datausage/CycleAdapter$CycleItem;

    move-object v3, v11

    invoke-direct/range {v2 .. v7}, Lcom/android/settings/datausage/CycleAdapter$CycleItem;-><init>(Landroid/content/Context;JJ)V

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/android/settings/datausage/CycleAdapter;->add(Ljava/lang/Object;)V

    const/4 v2, 0x1

    :goto_4
    move v10, v2

    goto :goto_2

    :cond_1
    const/4 v2, 0x0

    goto :goto_3

    :cond_2
    const/4 v2, 0x1

    goto :goto_3

    :cond_3
    move v10, v2

    :cond_4
    if-nez v10, :cond_8

    move-wide v6, v12

    :goto_5
    cmp-long v2, v6, v14

    if-lez v2, :cond_8

    const-wide v2, 0x90321000L

    sub-long v4, v6, v2

    if-eqz p2, :cond_7

    move-object/from16 v0, p2

    iget-object v3, v0, Lcom/android/settingslib/g/e;->cJs:Landroid/net/NetworkStatsHistory;

    invoke-virtual/range {v3 .. v8}, Landroid/net/NetworkStatsHistory;->getValues(JJLandroid/net/NetworkStatsHistory$Entry;)Landroid/net/NetworkStatsHistory$Entry;

    move-result-object v8

    iget-wide v2, v8, Landroid/net/NetworkStatsHistory$Entry;->rxBytes:J

    iget-wide v12, v8, Landroid/net/NetworkStatsHistory$Entry;->txBytes:J

    add-long/2addr v2, v12

    const-wide/16 v12, 0x0

    cmp-long v2, v2, v12

    if-lez v2, :cond_6

    const/4 v2, 0x1

    :goto_6
    if-eqz v2, :cond_5

    new-instance v2, Lcom/android/settings/datausage/CycleAdapter$CycleItem;

    move-object v3, v11

    invoke-direct/range {v2 .. v7}, Lcom/android/settings/datausage/CycleAdapter$CycleItem;-><init>(Landroid/content/Context;JJ)V

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/android/settings/datausage/CycleAdapter;->add(Ljava/lang/Object;)V

    :cond_5
    move-wide v6, v4

    goto :goto_5

    :cond_6
    const/4 v2, 0x0

    goto :goto_6

    :cond_7
    const/4 v2, 0x1

    goto :goto_6

    :cond_8
    invoke-virtual/range {p0 .. p0}, Lcom/android/settings/datausage/CycleAdapter;->getCount()I

    move-result v2

    if-lez v2, :cond_9

    move-object/from16 v0, p0

    invoke-virtual {v0, v9}, Lcom/android/settings/datausage/CycleAdapter;->je(Lcom/android/settings/datausage/CycleAdapter$CycleItem;)I

    move-result v5

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/settings/datausage/CycleAdapter;->jC:Lcom/android/settings/datausage/CycleAdapter$SpinnerInterface;

    invoke-interface {v2, v5}, Lcom/android/settings/datausage/CycleAdapter$SpinnerInterface;->setSelection(I)V

    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Lcom/android/settings/datausage/CycleAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/settings/datausage/CycleAdapter$CycleItem;

    invoke-static {v2, v9}, Llibcore/util/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/settings/datausage/CycleAdapter;->jB:Landroid/widget/AdapterView$OnItemSelectedListener;

    const-wide/16 v6, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-interface/range {v2 .. v7}, Landroid/widget/AdapterView$OnItemSelectedListener;->onItemSelected(Landroid/widget/AdapterView;Landroid/view/View;IJ)V

    const/4 v2, 0x0

    return v2

    :cond_9
    const/4 v2, 0x1

    return v2

    :cond_a
    move v2, v10

    goto :goto_4

    :cond_b
    move-wide v12, v2

    goto/16 :goto_1

    :cond_c
    move-wide v14, v4

    goto/16 :goto_0
.end method
