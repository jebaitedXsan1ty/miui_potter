.class public Lcom/android/settings/datausage/AppStateDataUsageBridge;
.super Lcom/android/settings/applications/AppStateBaseBridge;
.source "AppStateDataUsageBridge.java"


# instance fields
.field private final jg:Lcom/android/settings/datausage/DataSaverBackend;


# direct methods
.method public constructor <init>(Lcom/android/settingslib/b/a;Lcom/android/settings/applications/AppStateBaseBridge$Callback;Lcom/android/settings/datausage/DataSaverBackend;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/android/settings/applications/AppStateBaseBridge;-><init>(Lcom/android/settingslib/b/a;Lcom/android/settings/applications/AppStateBaseBridge$Callback;)V

    iput-object p3, p0, Lcom/android/settings/datausage/AppStateDataUsageBridge;->jg:Lcom/android/settings/datausage/DataSaverBackend;

    return-void
.end method


# virtual methods
.method protected iO()V
    .locals 8

    iget-object v0, p0, Lcom/android/settings/datausage/AppStateDataUsageBridge;->Ap:Lcom/android/settingslib/b/b;

    invoke-virtual {v0}, Lcom/android/settingslib/b/b;->cfs()Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settingslib/b/h;

    new-instance v4, Lcom/android/settings/datausage/AppStateDataUsageBridge$DataUsageState;

    iget-object v5, p0, Lcom/android/settings/datausage/AppStateDataUsageBridge;->jg:Lcom/android/settings/datausage/DataSaverBackend;

    iget-object v6, v0, Lcom/android/settingslib/b/h;->info:Landroid/content/pm/ApplicationInfo;

    iget v6, v6, Landroid/content/pm/ApplicationInfo;->uid:I

    invoke-virtual {v5, v6}, Lcom/android/settings/datausage/DataSaverBackend;->kv(I)Z

    move-result v5

    iget-object v6, p0, Lcom/android/settings/datausage/AppStateDataUsageBridge;->jg:Lcom/android/settings/datausage/DataSaverBackend;

    iget-object v7, v0, Lcom/android/settingslib/b/h;->info:Landroid/content/pm/ApplicationInfo;

    iget v7, v7, Landroid/content/pm/ApplicationInfo;->uid:I

    invoke-virtual {v6, v7}, Lcom/android/settings/datausage/DataSaverBackend;->kw(I)Z

    move-result v6

    invoke-direct {v4, v5, v6}, Lcom/android/settings/datausage/AppStateDataUsageBridge$DataUsageState;-><init>(ZZ)V

    iput-object v4, v0, Lcom/android/settingslib/b/h;->cBd:Ljava/lang/Object;

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    return-void
.end method

.method protected iP(Lcom/android/settingslib/b/h;Ljava/lang/String;I)V
    .locals 3

    new-instance v0, Lcom/android/settings/datausage/AppStateDataUsageBridge$DataUsageState;

    iget-object v1, p0, Lcom/android/settings/datausage/AppStateDataUsageBridge;->jg:Lcom/android/settings/datausage/DataSaverBackend;

    invoke-virtual {v1, p3}, Lcom/android/settings/datausage/DataSaverBackend;->kv(I)Z

    move-result v1

    iget-object v2, p0, Lcom/android/settings/datausage/AppStateDataUsageBridge;->jg:Lcom/android/settings/datausage/DataSaverBackend;

    invoke-virtual {v2, p3}, Lcom/android/settings/datausage/DataSaverBackend;->kw(I)Z

    move-result v2

    invoke-direct {v0, v1, v2}, Lcom/android/settings/datausage/AppStateDataUsageBridge$DataUsageState;-><init>(ZZ)V

    iput-object v0, p1, Lcom/android/settingslib/b/h;->cBd:Ljava/lang/Object;

    return-void
.end method
