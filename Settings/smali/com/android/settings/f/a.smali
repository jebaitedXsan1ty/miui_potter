.class public Lcom/android/settings/f/a;
.super Lcom/android/settings/wifi/f;
.source "TetherRestrictionController.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/widget/TextView;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/android/settings/wifi/f;-><init>(Landroid/content/Context;Landroid/widget/TextView;)V

    return-void
.end method


# virtual methods
.method public pause()V
    .locals 0

    invoke-super {p0}, Lcom/android/settings/wifi/f;->pause()V

    return-void
.end method

.method public wt()V
    .locals 0

    invoke-super {p0}, Lcom/android/settings/wifi/f;->wt()V

    return-void
.end method

.method public wv()V
    .locals 3

    invoke-super {p0}, Lcom/android/settings/wifi/f;->wv()V

    iget-object v0, p0, Lcom/android/settings/f/a;->bRZ:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/f/a;->mContext:Landroid/content/Context;

    const-string/jumbo v1, "no_config_tethering"

    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v2

    invoke-static {v0, v1, v2}, Lcom/android/settingslib/w;->crb(Landroid/content/Context;Ljava/lang/String;I)Lcom/android/settingslib/n;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/f/a;->bRZ:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/android/settings/f/a;->mContext:Landroid/content/Context;

    const v2, 0x7f1205d6

    invoke-virtual {v1, v2}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_0
    return-void
.end method
