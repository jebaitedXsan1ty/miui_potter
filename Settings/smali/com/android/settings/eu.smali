.class final Lcom/android/settings/eu;
.super Landroid/os/AsyncTask;
.source "LockPatternChecker.java"


# instance fields
.field private ciV:I

.field final synthetic ciW:Lcom/android/internal/widget/LockPatternUtils;

.field final synthetic ciX:I

.field final synthetic ciY:Ljava/lang/String;

.field final synthetic ciZ:Lcom/android/settings/M;


# direct methods
.method constructor <init>(Lcom/android/internal/widget/LockPatternUtils;ILjava/lang/String;Lcom/android/settings/M;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/eu;->ciW:Lcom/android/internal/widget/LockPatternUtils;

    iput p2, p0, Lcom/android/settings/eu;->ciX:I

    iput-object p3, p0, Lcom/android/settings/eu;->ciY:Ljava/lang/String;

    iput-object p4, p0, Lcom/android/settings/eu;->ciZ:Lcom/android/settings/M;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected bZT(Ljava/lang/Boolean;)V
    .locals 3

    iget-object v0, p0, Lcom/android/settings/eu;->ciZ:Lcom/android/settings/M;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    iget v2, p0, Lcom/android/settings/eu;->ciV:I

    invoke-interface {v0, v1, v2}, Lcom/android/settings/M;->blX(ZI)V

    return-void
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Boolean;
    .locals 4

    const/4 v3, 0x0

    :try_start_0
    iget-object v0, p0, Lcom/android/settings/eu;->ciW:Lcom/android/internal/widget/LockPatternUtils;

    iget v1, p0, Lcom/android/settings/eu;->ciX:I

    iget-object v2, p0, Lcom/android/settings/eu;->ciY:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/android/settings/bn;->bFs(Lcom/android/internal/widget/LockPatternUtils;ILjava/lang/String;)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    :catch_0
    move-exception v0

    iput v3, p0, Lcom/android/settings/eu;->ciV:I

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/android/settings/eu;->doInBackground([Ljava/lang/Void;)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Lcom/android/settings/eu;->bZT(Ljava/lang/Boolean;)V

    return-void
.end method
