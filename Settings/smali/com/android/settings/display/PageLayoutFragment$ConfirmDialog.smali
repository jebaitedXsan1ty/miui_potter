.class public Lcom/android/settings/display/PageLayoutFragment$ConfirmDialog;
.super Landroid/app/DialogFragment;
.source "PageLayoutFragment.java"


# static fields
.field private static final Xh:Ljava/lang/String;


# instance fields
.field private Xi:I


# direct methods
.method static synthetic -get0()Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/android/settings/display/PageLayoutFragment$ConfirmDialog;->Xh:Ljava/lang/String;

    return-object v0
.end method

.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/android/settings/display/PageLayoutFragment$ConfirmDialog;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/settings/display/PageLayoutFragment$ConfirmDialog;->Xh:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    return-void
.end method

.method static synthetic Ou(Lcom/android/settings/display/PageLayoutFragment$ConfirmDialog;)I
    .locals 1

    iget v0, p0, Lcom/android/settings/display/PageLayoutFragment$ConfirmDialog;->Xi:I

    return v0
.end method


# virtual methods
.method public Ot(I)V
    .locals 0

    iput p1, p0, Lcom/android/settings/display/PageLayoutFragment$ConfirmDialog;->Xi:I

    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1

    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onCreate(Landroid/os/Bundle;)V

    if-eqz p1, :cond_0

    const-string/jumbo v0, "com.android.settings.display.PageLayoutFragment:STATE_UI_MODE"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/android/settings/display/PageLayoutFragment$ConfirmDialog;->Xi:I

    :cond_0
    return-void
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 3

    invoke-virtual {p0}, Lcom/android/settings/display/PageLayoutFragment$ConfirmDialog;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/android/settings/display/PageLayoutFragment;->Or(Landroid/content/Context;)Z

    move-result v0

    xor-int/lit8 v1, v0, 0x1

    new-instance v2, Lmiui/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/android/settings/display/PageLayoutFragment$ConfirmDialog;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-direct {v2, v0}, Lmiui/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v0, 0x7f120c57

    invoke-virtual {v2, v0}, Lmiui/app/AlertDialog$Builder;->setTitle(I)Lmiui/app/AlertDialog$Builder;

    if-eqz v1, :cond_0

    const v0, 0x7f120d8e

    :goto_0
    invoke-virtual {v2, v0}, Lmiui/app/AlertDialog$Builder;->setMessage(I)Lmiui/app/AlertDialog$Builder;

    new-instance v0, Lcom/android/settings/display/ae;

    invoke-direct {v0, p0, v1}, Lcom/android/settings/display/ae;-><init>(Lcom/android/settings/display/PageLayoutFragment$ConfirmDialog;Z)V

    const v1, 0x104000a

    invoke-virtual {v2, v1, v0}, Lmiui/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Lmiui/app/AlertDialog$Builder;

    const/high16 v0, 0x1040000

    const/4 v1, 0x0

    invoke-virtual {v2, v0, v1}, Lmiui/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Lmiui/app/AlertDialog$Builder;

    invoke-virtual {v2}, Lmiui/app/AlertDialog$Builder;->create()Lmiui/app/AlertDialog;

    move-result-object v0

    return-object v0

    :cond_0
    const v0, 0x7f120c56

    goto :goto_0
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    const-string/jumbo v0, "com.android.settings.display.PageLayoutFragment:STATE_UI_MODE"

    iget v1, p0, Lcom/android/settings/display/PageLayoutFragment$ConfirmDialog;->Xi:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    return-void
.end method
