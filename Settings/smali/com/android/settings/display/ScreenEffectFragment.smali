.class public Lcom/android/settings/display/ScreenEffectFragment;
.super Lcom/android/settings/MiuiSettingsPreferenceFragment;
.source "ScreenEffectFragment.java"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceChangeListener;


# static fields
.field private static final UO:[Ljava/lang/String;

.field private static final UP:[I

.field private static final UQ:[Ljava/lang/String;

.field private static final UR:[I

.field private static final US:Z

.field private static final UZ:I


# instance fields
.field private UT:I

.field private UU:Lmiui/hareware/display/DisplayFeatureManager;

.field private UV:Lcom/android/settings/display/m;

.field private UW:Landroid/os/HandlerThread;

.field private UX:Lcom/android/settings/display/n;

.field private UY:Lcom/android/settings/display/ScreenColorPreference;

.field private Va:Landroid/widget/Toast;

.field private mContext:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const/4 v5, 0x0

    const/4 v4, 0x3

    const/4 v3, 0x2

    const/4 v2, 0x1

    new-array v0, v4, [Ljava/lang/String;

    const-string/jumbo v1, "screen_optimize_adapt"

    aput-object v1, v0, v5

    const-string/jumbo v1, "screen_optimize_enhance"

    aput-object v1, v0, v2

    const-string/jumbo v1, "screen_optimize_standard"

    aput-object v1, v0, v3

    sput-object v0, Lcom/android/settings/display/ScreenEffectFragment;->UQ:[Ljava/lang/String;

    filled-new-array {v2, v3, v4}, [I

    move-result-object v0

    sput-object v0, Lcom/android/settings/display/ScreenEffectFragment;->UR:[I

    new-array v0, v4, [Ljava/lang/String;

    const-string/jumbo v1, "screen_color_warm"

    aput-object v1, v0, v5

    const-string/jumbo v1, "screen_color_nature"

    aput-object v1, v0, v2

    const-string/jumbo v1, "screen_color_cool"

    aput-object v1, v0, v3

    sput-object v0, Lcom/android/settings/display/ScreenEffectFragment;->UO:[Ljava/lang/String;

    filled-new-array {v2, v3, v4}, [I

    move-result-object v0

    sput-object v0, Lcom/android/settings/display/ScreenEffectFragment;->UP:[I

    sget v0, Landroid/provider/MiuiSettings$ScreenEffect;->SCREEN_EFFECT_SUPPORTED:I

    sput v0, Lcom/android/settings/display/ScreenEffectFragment;->UZ:I

    const-string/jumbo v0, "ro.colorpick_adjust"

    invoke-static {v0, v5}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    sput-boolean v0, Lcom/android/settings/display/ScreenEffectFragment;->US:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/android/settings/MiuiSettingsPreferenceFragment;-><init>()V

    invoke-static {}, Lmiui/hareware/display/DisplayFeatureManager;->getInstance()Lmiui/hareware/display/DisplayFeatureManager;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/display/ScreenEffectFragment;->UU:Lmiui/hareware/display/DisplayFeatureManager;

    return-void
.end method

.method private MA()V
    .locals 6

    const/4 v5, 0x3

    const/4 v4, 0x1

    const/4 v3, 0x2

    iget-object v0, p0, Lcom/android/settings/display/ScreenEffectFragment;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v1, "screen_optimize_mode"

    sget v2, Landroid/provider/MiuiSettings$ScreenEffect;->DEFAULT_SCREEN_OPTIMIZE_MODE:I

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    if-eq v0, v4, :cond_0

    if-eq v0, v3, :cond_0

    if-eq v0, v5, :cond_0

    iget-object v0, p0, Lcom/android/settings/display/ScreenEffectFragment;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v1, "screen_optimize_mode"

    sget v2, Landroid/provider/MiuiSettings$ScreenEffect;->DEFAULT_SCREEN_OPTIMIZE_MODE:I

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    :cond_0
    iget-object v0, p0, Lcom/android/settings/display/ScreenEffectFragment;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v1, "screen_color_level"

    invoke-static {v0, v1, v3}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    if-eq v0, v3, :cond_1

    if-eq v0, v4, :cond_1

    if-eq v0, v5, :cond_1

    iget-object v0, p0, Lcom/android/settings/display/ScreenEffectFragment;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v1, "screen_color_level"

    invoke-static {v0, v1, v3}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    :cond_1
    return-void
.end method

.method private MB(Ljava/lang/String;I)Landroid/preference/PreferenceCategory;
    .locals 3

    invoke-virtual {p0}, Lcom/android/settings/display/ScreenEffectFragment;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    new-instance v1, Landroid/preference/PreferenceCategory;

    iget-object v2, p0, Lcom/android/settings/display/ScreenEffectFragment;->mContext:Landroid/content/Context;

    invoke-direct {v1, v2}, Landroid/preference/PreferenceCategory;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, p1}, Landroid/preference/PreferenceCategory;->setKey(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Landroid/preference/PreferenceCategory;->setTitle(I)V

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/preference/PreferenceCategory;->setPersistent(Z)V

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    return-object v1
.end method

.method private MC()V
    .locals 7

    const v4, 0x7f120e75

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/android/settings/display/ScreenEffectFragment;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v2, "screen_color_level"

    const/4 v3, 0x2

    invoke-static {v0, v2, v3}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v3

    sget-boolean v0, Lcom/android/settings/display/ScreenEffectFragment;->US:Z

    if-nez v0, :cond_1

    const-string/jumbo v0, "screen_color"

    invoke-direct {p0, v0, v4}, Lcom/android/settings/display/ScreenEffectFragment;->MB(Ljava/lang/String;I)Landroid/preference/PreferenceCategory;

    move-result-object v4

    invoke-virtual {p0}, Lcom/android/settings/display/ScreenEffectFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0300d0

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v5

    move v0, v1

    :goto_0
    array-length v2, v5

    if-ge v0, v2, :cond_2

    new-instance v6, Lcom/android/settings/display/RadioButtonPreference;

    iget-object v2, p0, Lcom/android/settings/display/ScreenEffectFragment;->mContext:Landroid/content/Context;

    invoke-direct {v6, v2}, Lcom/android/settings/display/RadioButtonPreference;-><init>(Landroid/content/Context;)V

    sget-object v2, Lcom/android/settings/display/ScreenEffectFragment;->UO:[Ljava/lang/String;

    aget-object v2, v2, v0

    invoke-virtual {v6, v2}, Lcom/android/settings/display/RadioButtonPreference;->setKey(Ljava/lang/String;)V

    aget-object v2, v5, v0

    invoke-virtual {v6, v2}, Lcom/android/settings/display/RadioButtonPreference;->setTitle(Ljava/lang/CharSequence;)V

    invoke-virtual {v6, v1}, Lcom/android/settings/display/RadioButtonPreference;->setPersistent(Z)V

    invoke-virtual {v6, p0}, Lcom/android/settings/display/RadioButtonPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    invoke-virtual {v4, v6}, Landroid/preference/PreferenceCategory;->addPreference(Landroid/preference/Preference;)Z

    sget-object v2, Lcom/android/settings/display/ScreenEffectFragment;->UP:[I

    aget v2, v2, v0

    if-ne v2, v3, :cond_0

    const/4 v2, 0x1

    :goto_1
    invoke-virtual {v6, v2}, Lcom/android/settings/display/RadioButtonPreference;->setChecked(Z)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    move v2, v1

    goto :goto_1

    :cond_1
    const-string/jumbo v0, "screen_color"

    invoke-direct {p0, v0, v4}, Lcom/android/settings/display/ScreenEffectFragment;->MB(Ljava/lang/String;I)Landroid/preference/PreferenceCategory;

    move-result-object v0

    new-instance v2, Lcom/android/settings/display/ScreenColorPreference;

    iget-object v3, p0, Lcom/android/settings/display/ScreenEffectFragment;->mContext:Landroid/content/Context;

    invoke-direct {v2, v3}, Lcom/android/settings/display/ScreenColorPreference;-><init>(Landroid/content/Context;)V

    const-string/jumbo v3, "screen_color_pre"

    invoke-virtual {v2, v3}, Lcom/android/settings/display/ScreenColorPreference;->setKey(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Lcom/android/settings/display/ScreenColorPreference;->setPersistent(Z)V

    invoke-virtual {v0, v2}, Landroid/preference/PreferenceCategory;->addPreference(Landroid/preference/Preference;)Z

    :cond_2
    return-void
.end method

.method private MD()V
    .locals 9

    const/4 v3, 0x1

    const/4 v1, 0x0

    const-string/jumbo v0, "screen_optimize"

    const v2, 0x7f120e91

    invoke-direct {p0, v0, v2}, Lcom/android/settings/display/ScreenEffectFragment;->MB(Ljava/lang/String;I)Landroid/preference/PreferenceCategory;

    move-result-object v4

    invoke-virtual {p0}, Lcom/android/settings/display/ScreenEffectFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0300db

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0}, Lcom/android/settings/display/ScreenEffectFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0300da

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v6

    invoke-direct {p0}, Lcom/android/settings/display/ScreenEffectFragment;->MF()I

    move-result v0

    iput v0, p0, Lcom/android/settings/display/ScreenEffectFragment;->UT:I

    move v0, v1

    :goto_0
    array-length v2, v5

    if-ge v0, v2, :cond_2

    sget v2, Lcom/android/settings/display/ScreenEffectFragment;->UZ:I

    shl-int v7, v3, v0

    and-int/2addr v2, v7

    if-nez v2, :cond_0

    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    new-instance v7, Lcom/android/settings/display/RadioButtonPreference;

    iget-object v2, p0, Lcom/android/settings/display/ScreenEffectFragment;->mContext:Landroid/content/Context;

    invoke-direct {v7, v2}, Lcom/android/settings/display/RadioButtonPreference;-><init>(Landroid/content/Context;)V

    sget-object v2, Lcom/android/settings/display/ScreenEffectFragment;->UQ:[Ljava/lang/String;

    aget-object v2, v2, v0

    invoke-virtual {v7, v2}, Lcom/android/settings/display/RadioButtonPreference;->setKey(Ljava/lang/String;)V

    aget-object v2, v5, v0

    invoke-virtual {v7, v2}, Lcom/android/settings/display/RadioButtonPreference;->setTitle(Ljava/lang/CharSequence;)V

    aget-object v2, v6, v0

    invoke-virtual {v7, v2}, Lcom/android/settings/display/RadioButtonPreference;->setSummary(Ljava/lang/CharSequence;)V

    invoke-virtual {v7, v1}, Lcom/android/settings/display/RadioButtonPreference;->setPersistent(Z)V

    invoke-virtual {v7, p0}, Lcom/android/settings/display/RadioButtonPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    invoke-virtual {v4, v7}, Landroid/preference/PreferenceCategory;->addPreference(Landroid/preference/Preference;)Z

    sget-object v2, Lcom/android/settings/display/ScreenEffectFragment;->UR:[I

    aget v2, v2, v0

    iget v8, p0, Lcom/android/settings/display/ScreenEffectFragment;->UT:I

    if-ne v2, v8, :cond_1

    move v2, v3

    :goto_2
    invoke-virtual {v7, v2}, Lcom/android/settings/display/RadioButtonPreference;->setChecked(Z)V

    goto :goto_1

    :cond_1
    move v2, v1

    goto :goto_2

    :cond_2
    return-void
.end method

.method private ME()I
    .locals 3

    iget-object v0, p0, Lcom/android/settings/display/ScreenEffectFragment;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v1, "screen_color_level"

    const/4 v2, 0x2

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method private MF()I
    .locals 3

    sget v0, Lcom/android/settings/display/ScreenEffectFragment;->UZ:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iget-object v1, p0, Lcom/android/settings/display/ScreenEffectFragment;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string/jumbo v2, "screen_optimize_mode"

    invoke-static {v1, v2, v0}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    return v0

    :cond_0
    const/4 v0, 0x2

    goto :goto_0
.end method

.method private MG(I)V
    .locals 2

    invoke-direct {p0}, Lcom/android/settings/display/ScreenEffectFragment;->ME()I

    move-result v0

    if-eq v0, p1, :cond_0

    iget-object v0, p0, Lcom/android/settings/display/ScreenEffectFragment;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v1, "screen_color_level"

    invoke-static {v0, v1, p1}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    :cond_0
    return-void
.end method

.method private MH(I)V
    .locals 2

    invoke-direct {p0}, Lcom/android/settings/display/ScreenEffectFragment;->MF()I

    move-result v0

    if-eq v0, p1, :cond_0

    iput p1, p0, Lcom/android/settings/display/ScreenEffectFragment;->UT:I

    iget-object v0, p0, Lcom/android/settings/display/ScreenEffectFragment;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v1, "screen_optimize_mode"

    invoke-static {v0, v1, p1}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    :cond_0
    return-void
.end method

.method private MI(Ljava/lang/String;Ljava/lang/String;)V
    .locals 5

    invoke-virtual {p0}, Lcom/android/settings/display/ScreenEffectFragment;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceCategory;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/preference/PreferenceCategory;->getPreferenceCount()I

    move-result v3

    const/4 v1, 0x0

    move v2, v1

    :goto_0
    if-ge v2, v3, :cond_1

    invoke-virtual {v0, v2}, Landroid/preference/PreferenceCategory;->getPreference(I)Landroid/preference/Preference;

    move-result-object v1

    check-cast v1, Lcom/android/settings/display/RadioButtonPreference;

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/android/settings/display/RadioButtonPreference;->getKey()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    invoke-virtual {v1, v4}, Lcom/android/settings/display/RadioButtonPreference;->setChecked(Z)V

    :cond_0
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_0

    :cond_1
    return-void
.end method

.method private MJ(I)V
    .locals 4

    const/4 v3, 0x0

    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/android/settings/display/ScreenEffectFragment;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    const-string/jumbo v1, "screen_color"

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceCategory;

    if-nez v0, :cond_0

    return-void

    :cond_0
    sget v1, Lcom/android/settings/display/ScreenEffectFragment;->UZ:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_1

    sget-boolean v1, Lcom/android/settings/display/ScreenEffectFragment;->US:Z

    if-nez v1, :cond_3

    if-ne p1, v2, :cond_2

    invoke-virtual {v0, v2}, Landroid/preference/PreferenceCategory;->setEnabled(Z)V

    invoke-virtual {v0, v2}, Landroid/preference/PreferenceCategory;->getPreference(I)Landroid/preference/Preference;

    move-result-object v1

    check-cast v1, Lcom/android/settings/display/RadioButtonPreference;

    invoke-virtual {v1, v3}, Lcom/android/settings/display/RadioButtonPreference;->setChecked(Z)V

    invoke-direct {p0}, Lcom/android/settings/display/ScreenEffectFragment;->ME()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-static {v1}, Ljava/lang/Math;->abs(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceCategory;->getPreference(I)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Lcom/android/settings/display/RadioButtonPreference;

    invoke-virtual {v0, v2}, Lcom/android/settings/display/RadioButtonPreference;->setChecked(Z)V

    :cond_1
    :goto_0
    return-void

    :cond_2
    invoke-virtual {v0, v3}, Landroid/preference/PreferenceCategory;->setEnabled(Z)V

    invoke-direct {p0}, Lcom/android/settings/display/ScreenEffectFragment;->ME()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-static {v1}, Ljava/lang/Math;->abs(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceCategory;->getPreference(I)Landroid/preference/Preference;

    move-result-object v1

    check-cast v1, Lcom/android/settings/display/RadioButtonPreference;

    invoke-virtual {v1, v3}, Lcom/android/settings/display/RadioButtonPreference;->setChecked(Z)V

    invoke-virtual {v0, v2}, Landroid/preference/PreferenceCategory;->getPreference(I)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Lcom/android/settings/display/RadioButtonPreference;

    invoke-virtual {v0, v2}, Lcom/android/settings/display/RadioButtonPreference;->setChecked(Z)V

    goto :goto_0

    :cond_3
    if-ne p1, v2, :cond_4

    invoke-virtual {v0, v2}, Landroid/preference/PreferenceCategory;->setEnabled(Z)V

    goto :goto_0

    :cond_4
    invoke-virtual {v0, v3}, Landroid/preference/PreferenceCategory;->setEnabled(Z)V

    goto :goto_0
.end method

.method static synthetic MK(Lcom/android/settings/display/ScreenEffectFragment;)Landroid/content/Context;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/display/ScreenEffectFragment;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic ML(Lcom/android/settings/display/ScreenEffectFragment;)Landroid/widget/Toast;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/display/ScreenEffectFragment;->Va:Landroid/widget/Toast;

    return-object v0
.end method

.method static synthetic MM(Lcom/android/settings/display/ScreenEffectFragment;)Landroid/content/ContentResolver;
    .locals 1

    invoke-virtual {p0}, Lcom/android/settings/display/ScreenEffectFragment;->bWB()Landroid/content/ContentResolver;

    move-result-object v0

    return-object v0
.end method

.method static synthetic MN(Lcom/android/settings/display/ScreenEffectFragment;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/settings/display/ScreenEffectFragment;->MG(I)V

    return-void
.end method

.method static synthetic MO(Lcom/android/settings/display/ScreenEffectFragment;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/settings/display/ScreenEffectFragment;->MH(I)V

    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 4

    const/4 v3, 0x0

    invoke-super {p0, p1}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->onCreate(Landroid/os/Bundle;)V

    new-instance v0, Landroid/os/HandlerThread;

    const-string/jumbo v1, "ScreenEffectHandler"

    invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/android/settings/display/ScreenEffectFragment;->UW:Landroid/os/HandlerThread;

    iget-object v0, p0, Lcom/android/settings/display/ScreenEffectFragment;->UW:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    new-instance v0, Lcom/android/settings/display/m;

    iget-object v1, p0, Lcom/android/settings/display/ScreenEffectFragment;->UW:Landroid/os/HandlerThread;

    invoke-virtual {v1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/android/settings/display/m;-><init>(Lcom/android/settings/display/ScreenEffectFragment;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/android/settings/display/ScreenEffectFragment;->UV:Lcom/android/settings/display/m;

    invoke-virtual {p0}, Lcom/android/settings/display/ScreenEffectFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/display/ScreenEffectFragment;->mContext:Landroid/content/Context;

    const v0, 0x7f1500b5

    invoke-virtual {p0, v0}, Lcom/android/settings/display/ScreenEffectFragment;->addPreferencesFromResource(I)V

    sget-boolean v0, Lcom/android/settings/display/ScreenEffectFragment;->US:Z

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/android/settings/display/ScreenEffectFragment;->MA()V

    :cond_0
    invoke-direct {p0}, Lcom/android/settings/display/ScreenEffectFragment;->MC()V

    invoke-direct {p0}, Lcom/android/settings/display/ScreenEffectFragment;->MD()V

    iget v0, p0, Lcom/android/settings/display/ScreenEffectFragment;->UT:I

    invoke-direct {p0, v0}, Lcom/android/settings/display/ScreenEffectFragment;->MJ(I)V

    sget-boolean v0, Lcom/android/settings/display/ScreenEffectFragment;->US:Z

    if-eqz v0, :cond_1

    const-string/jumbo v0, "screen_color_pre"

    invoke-virtual {p0, v0}, Lcom/android/settings/display/ScreenEffectFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Lcom/android/settings/display/ScreenColorPreference;

    iput-object v0, p0, Lcom/android/settings/display/ScreenEffectFragment;->UY:Lcom/android/settings/display/ScreenColorPreference;

    :cond_1
    const-string/jumbo v0, "support_screen_paper_mode"

    invoke-static {v0, v3}, Lmiui/util/FeatureParser;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/settings/display/ScreenEffectFragment;->mContext:Landroid/content/Context;

    invoke-virtual {p0}, Lcom/android/settings/display/ScreenEffectFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f120e74

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-static {v0, v1, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/display/ScreenEffectFragment;->Va:Landroid/widget/Toast;

    new-instance v0, Lcom/android/settings/display/n;

    invoke-direct {v0, p0}, Lcom/android/settings/display/n;-><init>(Lcom/android/settings/display/ScreenEffectFragment;)V

    iput-object v0, p0, Lcom/android/settings/display/ScreenEffectFragment;->UX:Lcom/android/settings/display/n;

    iget-object v0, p0, Lcom/android/settings/display/ScreenEffectFragment;->UX:Lcom/android/settings/display/n;

    invoke-virtual {v0}, Lcom/android/settings/display/n;->MR()V

    iget-object v0, p0, Lcom/android/settings/display/ScreenEffectFragment;->UX:Lcom/android/settings/display/n;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/android/settings/display/n;->onChange(Z)V

    :cond_2
    return-void
.end method

.method public onDestroy()V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/android/settings/display/ScreenEffectFragment;->UX:Lcom/android/settings/display/n;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/display/ScreenEffectFragment;->UX:Lcom/android/settings/display/n;

    invoke-virtual {v0}, Lcom/android/settings/display/n;->MS()V

    :cond_0
    iget-object v0, p0, Lcom/android/settings/display/ScreenEffectFragment;->UW:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->quit()Z

    iput-object v1, p0, Lcom/android/settings/display/ScreenEffectFragment;->mContext:Landroid/content/Context;

    invoke-super {p0}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->onDestroy()V

    return-void
.end method

.method public onPause()V
    .locals 1

    invoke-super {p0}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->onPause()V

    iget-object v0, p0, Lcom/android/settings/display/ScreenEffectFragment;->Va:Landroid/widget/Toast;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/display/ScreenEffectFragment;->Va:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->cancel()V

    :cond_0
    return-void
.end method

.method public onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 9

    const/4 v1, 0x2

    const/4 v4, -0x1

    const/4 v3, 0x3

    const/4 v2, 0x1

    const-string/jumbo v0, ""

    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v5

    const-string/jumbo v6, "screen_color_warm"

    invoke-virtual {v6, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    const-string/jumbo v0, "screen_color"

    move v1, v2

    move v4, v2

    :goto_0
    iget-object v6, p0, Lcom/android/settings/display/ScreenEffectFragment;->UV:Lcom/android/settings/display/m;

    iget-object v7, p0, Lcom/android/settings/display/ScreenEffectFragment;->UV:Lcom/android/settings/display/m;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v7, v4, v8}, Lcom/android/settings/display/m;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/android/settings/display/m;->sendMessage(Landroid/os/Message;)Z

    if-ne v4, v3, :cond_0

    invoke-direct {p0, v1}, Lcom/android/settings/display/ScreenEffectFragment;->MJ(I)V

    :cond_0
    instance-of v1, p1, Lcom/android/settings/display/RadioButtonPreference;

    if-eqz v1, :cond_6

    invoke-direct {p0, v0, v5}, Lcom/android/settings/display/ScreenEffectFragment;->MI(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x0

    return v0

    :cond_1
    const-string/jumbo v6, "screen_color_nature"

    invoke-virtual {v6, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    const-string/jumbo v0, "screen_color"

    move v4, v2

    goto :goto_0

    :cond_2
    const-string/jumbo v6, "screen_color_cool"

    invoke-virtual {v6, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    const-string/jumbo v0, "screen_color"

    move v1, v3

    move v4, v2

    goto :goto_0

    :cond_3
    const-string/jumbo v6, "screen_optimize_adapt"

    invoke-virtual {v6, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_4

    const-string/jumbo v0, "screen_optimize"

    move v1, v2

    move v4, v3

    goto :goto_0

    :cond_4
    const-string/jumbo v6, "screen_optimize_enhance"

    invoke-virtual {v6, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_5

    const-string/jumbo v0, "screen_optimize"

    move v4, v3

    goto :goto_0

    :cond_5
    const-string/jumbo v1, "screen_optimize_standard"

    invoke-virtual {v1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    const-string/jumbo v0, "screen_optimize"

    move v1, v3

    move v4, v3

    goto :goto_0

    :cond_6
    return v2

    :cond_7
    move v1, v4

    goto :goto_0
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 2

    const v0, 0x102000a

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOverScrollMode(I)V

    invoke-super {p0, p1, p2}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    return-void
.end method
