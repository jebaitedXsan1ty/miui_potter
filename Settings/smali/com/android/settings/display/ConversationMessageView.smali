.class public Lcom/android/settings/display/ConversationMessageView;
.super Landroid/widget/FrameLayout;
.source "ConversationMessageView.java"


# instance fields
.field private TA:Landroid/widget/TextView;

.field private TB:Landroid/widget/TextView;

.field private final TC:Ljava/lang/CharSequence;

.field private Ts:Landroid/widget/TextView;

.field private final Tt:I

.field private final Tu:Ljava/lang/CharSequence;

.field private final Tv:I

.field private final Tw:Z

.field private Tx:Landroid/widget/LinearLayout;

.field private final Ty:Ljava/lang/CharSequence;

.field private Tz:Landroid/view/ViewGroup;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/android/settings/display/ConversationMessageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/android/settings/display/ConversationMessageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/android/settings/display/ConversationMessageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V
    .locals 4

    const/4 v3, 0x1

    const/4 v2, 0x0

    invoke-direct {p0, p1, p2, p3, p4}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V

    sget-object v0, Lcom/android/settings/cw;->bYF:[I

    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    const/4 v1, 0x3

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    iput-boolean v1, p0, Lcom/android/settings/display/ConversationMessageView;->Tw:Z

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/android/settings/display/ConversationMessageView;->Ty:Ljava/lang/CharSequence;

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/android/settings/display/ConversationMessageView;->TC:Ljava/lang/CharSequence;

    invoke-virtual {v0, v3}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/android/settings/display/ConversationMessageView;->Tu:Ljava/lang/CharSequence;

    const/4 v1, 0x2

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v1

    iput v1, p0, Lcom/android/settings/display/ConversationMessageView;->Tv:I

    invoke-virtual {v0, v2, v2}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v0

    iput v0, p0, Lcom/android/settings/display/ConversationMessageView;->Tt:I

    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0d0070

    invoke-virtual {v0, v1, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0d006f

    invoke-virtual {v0, v1, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    return-void
.end method

.method private static KR(Landroid/content/Context;Landroid/graphics/drawable/Drawable;I)Landroid/graphics/drawable/Drawable;
    .locals 2

    invoke-virtual {p1}, Landroid/graphics/drawable/Drawable;->getConstantState()Landroid/graphics/drawable/Drawable$ConstantState;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable$ConstantState;->newDrawable(Landroid/content/res/Resources;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->mutate()Landroid/graphics/drawable/Drawable;

    move-result-object p1

    :cond_0
    sget-object v0, Landroid/graphics/PorterDuff$Mode;->SRC_ATOP:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {p1, p2, v0}, Landroid/graphics/drawable/Drawable;->setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V

    return-object p1
.end method

.method private static KS(Landroid/view/View;)Z
    .locals 2

    const/4 v0, 0x1

    invoke-virtual {p0}, Landroid/view/View;->getLayoutDirection()I

    move-result v1

    if-ne v0, v1, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private KT()V
    .locals 3

    iget-boolean v0, p0, Lcom/android/settings/display/ConversationMessageView;->Tw:Z

    if-eqz v0, :cond_0

    const v0, 0x7f0600a6

    :goto_0
    iget-boolean v1, p0, Lcom/android/settings/display/ConversationMessageView;->Tw:Z

    if-eqz v1, :cond_1

    const v1, 0x7f06010f

    :goto_1
    invoke-virtual {p0}, Lcom/android/settings/display/ConversationMessageView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/content/Context;->getColor(I)I

    move-result v0

    iget-object v2, p0, Lcom/android/settings/display/ConversationMessageView;->TA:Landroid/widget/TextView;

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v2, p0, Lcom/android/settings/display/ConversationMessageView;->TA:Landroid/widget/TextView;

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setLinkTextColor(I)V

    iget-object v0, p0, Lcom/android/settings/display/ConversationMessageView;->TB:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    return-void

    :cond_0
    const v0, 0x7f0600a7

    goto :goto_0

    :cond_1
    const v1, 0x7f060110

    goto :goto_1
.end method

.method private KU()V
    .locals 11

    invoke-virtual {p0}, Lcom/android/settings/display/ConversationMessageView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v0, 0x7f07013f

    invoke-virtual {v3, v0}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    const v0, 0x7f070143

    invoke-virtual {v3, v0}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    const v2, 0x7f070144

    invoke-virtual {v3, v2}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v5

    const v2, 0x7f070142

    invoke-virtual {v3, v2}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v6

    iget-boolean v2, p0, Lcom/android/settings/display/ConversationMessageView;->Tw:Z

    if-eqz v2, :cond_0

    add-int/2addr v1, v0

    :goto_0
    iget-boolean v2, p0, Lcom/android/settings/display/ConversationMessageView;->Tw:Z

    if-eqz v2, :cond_1

    const v2, 0x800013

    :goto_1
    const v4, 0x7f070141

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v7

    const v4, 0x7f070140

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v8

    iget-boolean v3, p0, Lcom/android/settings/display/ConversationMessageView;->Tw:Z

    if-eqz v3, :cond_2

    const v3, 0x7f08032a

    :goto_2
    iget-boolean v4, p0, Lcom/android/settings/display/ConversationMessageView;->Tw:Z

    if-eqz v4, :cond_3

    const v4, 0x7f0600a0

    :goto_3
    invoke-virtual {p0}, Lcom/android/settings/display/ConversationMessageView;->getContext()Landroid/content/Context;

    move-result-object v9

    invoke-virtual {v9, v3}, Landroid/content/Context;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {v9, v4}, Landroid/content/Context;->getColor(I)I

    move-result v4

    invoke-static {v9, v3, v4}, Lcom/android/settings/display/ConversationMessageView;->KR(Landroid/content/Context;Landroid/graphics/drawable/Drawable;I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    iget-object v4, p0, Lcom/android/settings/display/ConversationMessageView;->Tz:Landroid/view/ViewGroup;

    invoke-virtual {v4, v3}, Landroid/view/ViewGroup;->setBackground(Landroid/graphics/drawable/Drawable;)V

    invoke-static {p0}, Lcom/android/settings/display/ConversationMessageView;->KS(Landroid/view/View;)Z

    move-result v3

    if-eqz v3, :cond_4

    iget-object v3, p0, Lcom/android/settings/display/ConversationMessageView;->Tz:Landroid/view/ViewGroup;

    add-int v4, v5, v8

    invoke-virtual {v3, v0, v4, v1, v6}, Landroid/view/ViewGroup;->setPadding(IIII)V

    :goto_4
    invoke-virtual {p0}, Lcom/android/settings/display/ConversationMessageView;->getPaddingLeft()I

    move-result v0

    invoke-virtual {p0}, Lcom/android/settings/display/ConversationMessageView;->getPaddingRight()I

    move-result v1

    const/4 v3, 0x0

    invoke-virtual {p0, v0, v7, v1, v3}, Lcom/android/settings/display/ConversationMessageView;->setPadding(IIII)V

    iget-object v0, p0, Lcom/android/settings/display/ConversationMessageView;->Tx:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setGravity(I)V

    invoke-direct {p0}, Lcom/android/settings/display/ConversationMessageView;->KT()V

    return-void

    :cond_0
    add-int/2addr v1, v0

    move v10, v1

    move v1, v0

    move v0, v10

    goto :goto_0

    :cond_1
    const v2, 0x800015

    goto :goto_1

    :cond_2
    const v3, 0x7f08032b

    goto :goto_2

    :cond_3
    const v4, 0x7f0600a1

    goto :goto_3

    :cond_4
    iget-object v3, p0, Lcom/android/settings/display/ConversationMessageView;->Tz:Landroid/view/ViewGroup;

    add-int v4, v5, v8

    invoke-virtual {v3, v1, v4, v0, v6}, Landroid/view/ViewGroup;->setPadding(IIII)V

    goto :goto_4
.end method

.method private KV()V
    .locals 4

    iget-object v0, p0, Lcom/android/settings/display/ConversationMessageView;->TA:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/android/settings/display/ConversationMessageView;->Ty:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/android/settings/display/ConversationMessageView;->TB:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/android/settings/display/ConversationMessageView;->TC:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/android/settings/display/ConversationMessageView;->Ts:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/android/settings/display/ConversationMessageView;->Tu:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/android/settings/display/ConversationMessageView;->Ts:Landroid/widget/TextView;

    iget v1, p0, Lcom/android/settings/display/ConversationMessageView;->Tv:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    invoke-virtual {p0}, Lcom/android/settings/display/ConversationMessageView;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0800de

    invoke-virtual {v0, v1}, Landroid/content/Context;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/display/ConversationMessageView;->Ts:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/android/settings/display/ConversationMessageView;->getContext()Landroid/content/Context;

    move-result-object v2

    iget v3, p0, Lcom/android/settings/display/ConversationMessageView;->Tt:I

    invoke-static {v2, v0, v3}, Lcom/android/settings/display/ConversationMessageView;->KR(Landroid/content/Context;Landroid/graphics/drawable/Drawable;I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    return-void
.end method


# virtual methods
.method protected onFinishInflate()V
    .locals 1

    const v0, 0x7f0a0290

    invoke-virtual {p0, v0}, Lcom/android/settings/display/ConversationMessageView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/android/settings/display/ConversationMessageView;->Tx:Landroid/widget/LinearLayout;

    const v0, 0x7f0a0294

    invoke-virtual {p0, v0}, Lcom/android/settings/display/ConversationMessageView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/android/settings/display/ConversationMessageView;->Tz:Landroid/view/ViewGroup;

    const v0, 0x7f0a0293

    invoke-virtual {p0, v0}, Lcom/android/settings/display/ConversationMessageView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/settings/display/ConversationMessageView;->TA:Landroid/widget/TextView;

    const v0, 0x7f0a0292

    invoke-virtual {p0, v0}, Lcom/android/settings/display/ConversationMessageView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/settings/display/ConversationMessageView;->TB:Landroid/widget/TextView;

    const v0, 0x7f0a0108

    invoke-virtual {p0, v0}, Lcom/android/settings/display/ConversationMessageView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/settings/display/ConversationMessageView;->Ts:Landroid/widget/TextView;

    invoke-direct {p0}, Lcom/android/settings/display/ConversationMessageView;->KV()V

    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 8

    invoke-static {p0}, Lcom/android/settings/display/ConversationMessageView;->KS(Landroid/view/View;)Z

    move-result v0

    iget-object v1, p0, Lcom/android/settings/display/ConversationMessageView;->Ts:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v2

    iget-object v1, p0, Lcom/android/settings/display/ConversationMessageView;->Ts:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v3

    invoke-virtual {p0}, Lcom/android/settings/display/ConversationMessageView;->getPaddingTop()I

    move-result v4

    sub-int v1, p4, p2

    sub-int/2addr v1, v2

    invoke-virtual {p0}, Lcom/android/settings/display/ConversationMessageView;->getPaddingLeft()I

    move-result v5

    sub-int/2addr v1, v5

    invoke-virtual {p0}, Lcom/android/settings/display/ConversationMessageView;->getPaddingRight()I

    move-result v5

    sub-int v5, v1, v5

    iget-object v1, p0, Lcom/android/settings/display/ConversationMessageView;->Tx:Landroid/widget/LinearLayout;

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getMeasuredHeight()I

    move-result v6

    iget-boolean v1, p0, Lcom/android/settings/display/ConversationMessageView;->Tw:Z

    if-eqz v1, :cond_1

    if-eqz v0, :cond_0

    sub-int v0, p4, p2

    invoke-virtual {p0}, Lcom/android/settings/display/ConversationMessageView;->getPaddingRight()I

    move-result v1

    sub-int/2addr v0, v1

    sub-int v1, v0, v2

    sub-int v0, v1, v5

    :goto_0
    iget-object v7, p0, Lcom/android/settings/display/ConversationMessageView;->Ts:Landroid/widget/TextView;

    add-int/2addr v2, v1

    add-int/2addr v3, v4

    invoke-virtual {v7, v1, v4, v2, v3}, Landroid/widget/TextView;->layout(IIII)V

    iget-object v1, p0, Lcom/android/settings/display/ConversationMessageView;->Tx:Landroid/widget/LinearLayout;

    add-int v2, v0, v5

    add-int v3, v4, v6

    invoke-virtual {v1, v0, v4, v2, v3}, Landroid/widget/LinearLayout;->layout(IIII)V

    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/android/settings/display/ConversationMessageView;->getPaddingLeft()I

    move-result v1

    add-int v0, v1, v2

    goto :goto_0

    :cond_1
    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lcom/android/settings/display/ConversationMessageView;->getPaddingLeft()I

    move-result v1

    add-int v0, v1, v2

    goto :goto_0

    :cond_2
    sub-int v0, p4, p2

    invoke-virtual {p0}, Lcom/android/settings/display/ConversationMessageView;->getPaddingRight()I

    move-result v1

    sub-int/2addr v0, v1

    sub-int v1, v0, v2

    sub-int v0, v1, v5

    goto :goto_0
.end method

.method protected onMeasure(II)V
    .locals 4

    const/4 v2, 0x0

    invoke-direct {p0}, Lcom/android/settings/display/ConversationMessageView;->KU()V

    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    invoke-static {v2, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    invoke-static {v2, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    iget-object v3, p0, Lcom/android/settings/display/ConversationMessageView;->Ts:Landroid/widget/TextView;

    invoke-virtual {v3, v2, v2}, Landroid/widget/TextView;->measure(II)V

    iget-object v2, p0, Lcom/android/settings/display/ConversationMessageView;->Ts:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v2

    iget-object v3, p0, Lcom/android/settings/display/ConversationMessageView;->Ts:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v3

    invoke-static {v2, v3}, Ljava/lang/Math;->max(II)I

    move-result v2

    const/high16 v3, 0x40000000    # 2.0f

    invoke-static {v2, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    iget-object v3, p0, Lcom/android/settings/display/ConversationMessageView;->Ts:Landroid/widget/TextView;

    invoke-virtual {v3, v2, v2}, Landroid/widget/TextView;->measure(II)V

    invoke-virtual {p0}, Lcom/android/settings/display/ConversationMessageView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f07013f

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    iget-object v3, p0, Lcom/android/settings/display/ConversationMessageView;->Ts:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v3

    mul-int/lit8 v3, v3, 0x2

    sub-int v3, v0, v3

    sub-int v2, v3, v2

    invoke-virtual {p0}, Lcom/android/settings/display/ConversationMessageView;->getPaddingLeft()I

    move-result v3

    sub-int/2addr v2, v3

    invoke-virtual {p0}, Lcom/android/settings/display/ConversationMessageView;->getPaddingRight()I

    move-result v3

    sub-int/2addr v2, v3

    const/high16 v3, -0x80000000

    invoke-static {v2, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    iget-object v3, p0, Lcom/android/settings/display/ConversationMessageView;->Tx:Landroid/widget/LinearLayout;

    invoke-virtual {v3, v2, v1}, Landroid/widget/LinearLayout;->measure(II)V

    iget-object v1, p0, Lcom/android/settings/display/ConversationMessageView;->Ts:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v1

    iget-object v2, p0, Lcom/android/settings/display/ConversationMessageView;->Tx:Landroid/widget/LinearLayout;

    invoke-virtual {v2}, Landroid/widget/LinearLayout;->getMeasuredHeight()I

    move-result v2

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    invoke-virtual {p0}, Lcom/android/settings/display/ConversationMessageView;->getPaddingBottom()I

    move-result v2

    add-int/2addr v1, v2

    invoke-virtual {p0}, Lcom/android/settings/display/ConversationMessageView;->getPaddingTop()I

    move-result v2

    add-int/2addr v1, v2

    invoke-virtual {p0, v0, v1}, Lcom/android/settings/display/ConversationMessageView;->setMeasuredDimension(II)V

    return-void
.end method
