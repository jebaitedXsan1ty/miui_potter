.class Lcom/android/settings/display/p;
.super Landroid/os/FileObserver;
.source "BrightnessFragment.java"


# instance fields
.field private Wb:Landroid/os/Handler;

.field private Wc:Ljava/lang/Runnable;

.field final synthetic Wd:Lcom/android/settings/display/BrightnessFragment;


# direct methods
.method public constructor <init>(Lcom/android/settings/display/BrightnessFragment;Ljava/lang/String;)V
    .locals 1

    iput-object p1, p0, Lcom/android/settings/display/p;->Wd:Lcom/android/settings/display/BrightnessFragment;

    const/4 v0, 0x2

    invoke-direct {p0, p2, v0}, Landroid/os/FileObserver;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lcom/android/settings/display/U;

    invoke-direct {v0, p0}, Lcom/android/settings/display/U;-><init>(Lcom/android/settings/display/p;)V

    iput-object v0, p0, Lcom/android/settings/display/p;->Wc:Ljava/lang/Runnable;

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/android/settings/display/p;->Wb:Landroid/os/Handler;

    return-void
.end method


# virtual methods
.method public Nz(J)V
    .locals 3

    iget-object v0, p0, Lcom/android/settings/display/p;->Wb:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/settings/display/p;->Wc:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    iget-object v0, p0, Lcom/android/settings/display/p;->Wb:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/settings/display/p;->Wc:Ljava/lang/Runnable;

    invoke-virtual {v0, v1, p1, p2}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    return-void
.end method

.method public onEvent(ILjava/lang/String;)V
    .locals 2

    const/4 v0, 0x2

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/display/p;->Wd:Lcom/android/settings/display/BrightnessFragment;

    invoke-virtual {v0}, Lcom/android/settings/display/BrightnessFragment;->Ng()I

    move-result v0

    iget-object v1, p0, Lcom/android/settings/display/p;->Wd:Lcom/android/settings/display/BrightnessFragment;

    invoke-static {v1}, Lcom/android/settings/display/BrightnessFragment;->Nt(Lcom/android/settings/display/BrightnessFragment;)I

    move-result v1

    sub-int/2addr v0, v1

    int-to-float v0, v0

    iget-object v1, p0, Lcom/android/settings/display/p;->Wd:Lcom/android/settings/display/BrightnessFragment;

    invoke-static {v1}, Lcom/android/settings/display/BrightnessFragment;->Nr(Lcom/android/settings/display/BrightnessFragment;)F

    move-result v1

    div-float/2addr v0, v1

    float-to-int v0, v0

    iget-object v1, p0, Lcom/android/settings/display/p;->Wd:Lcom/android/settings/display/BrightnessFragment;

    invoke-static {v1}, Lcom/android/settings/display/BrightnessFragment;->Nu(Lcom/android/settings/display/BrightnessFragment;)Lmiui/widget/SeekBar;

    move-result-object v1

    invoke-virtual {v1, v0}, Lmiui/widget/SeekBar;->setProgress(I)V

    :cond_0
    return-void
.end method

.method public stopWatching()V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/display/p;->Wb:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/settings/display/p;->Wc:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    invoke-super {p0}, Landroid/os/FileObserver;->stopWatching()V

    return-void
.end method
