.class final Lcom/android/settings/display/H;
.super Ljava/lang/Object;
.source "PaperModeFragment.java"

# interfaces
.implements Lmiui/app/TimePickerDialog$OnTimeSetListener;


# instance fields
.field final synthetic XO:Lcom/android/settings/display/PaperModeFragment;


# direct methods
.method constructor <init>(Lcom/android/settings/display/PaperModeFragment;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/display/H;->XO:Lcom/android/settings/display/PaperModeFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onTimeSet(Lmiui/widget/TimePicker;II)V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/display/H;->XO:Lcom/android/settings/display/PaperModeFragment;

    invoke-static {v0}, Lcom/android/settings/display/PaperModeFragment;->Mi(Lcom/android/settings/display/PaperModeFragment;)Z

    move-result v0

    if-nez v0, :cond_1

    mul-int/lit8 v0, p2, 0x3c

    add-int/2addr v0, p3

    invoke-static {v0}, Lcom/android/settings/display/PaperModeFragment;->Mn(I)I

    iget-object v0, p0, Lcom/android/settings/display/H;->XO:Lcom/android/settings/display/PaperModeFragment;

    invoke-static {v0}, Lcom/android/settings/display/PaperModeFragment;->Mf(Lcom/android/settings/display/PaperModeFragment;)Landroid/content/Context;

    move-result-object v0

    invoke-static {}, Lcom/android/settings/display/PaperModeFragment;->Mh()I

    move-result v1

    invoke-static {v0, v1}, Lcom/android/settings/display/k;->LO(Landroid/content/Context;I)V

    iget-object v0, p0, Lcom/android/settings/display/H;->XO:Lcom/android/settings/display/PaperModeFragment;

    invoke-static {v0}, Lcom/android/settings/display/PaperModeFragment;->Ml(Lcom/android/settings/display/PaperModeFragment;)Lcom/android/settings/dndmode/LabelPreference;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/display/H;->XO:Lcom/android/settings/display/PaperModeFragment;

    invoke-static {v1, p2, p3}, Lcom/android/settings/display/PaperModeFragment;->Mq(Lcom/android/settings/display/PaperModeFragment;II)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/settings/dndmode/LabelPreference;->mp(Ljava/lang/String;)V

    :goto_0
    iget-object v0, p0, Lcom/android/settings/display/H;->XO:Lcom/android/settings/display/PaperModeFragment;

    invoke-static {v0}, Lcom/android/settings/display/PaperModeFragment;->Mf(Lcom/android/settings/display/PaperModeFragment;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/android/settings/display/PaperModeFragment;->LY(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/display/H;->XO:Lcom/android/settings/display/PaperModeFragment;

    invoke-static {v0}, Lcom/android/settings/display/PaperModeFragment;->Mf(Lcom/android/settings/display/PaperModeFragment;)Landroid/content/Context;

    move-result-object v0

    const/4 v1, 0x2

    invoke-static {v0, v1}, Lcom/android/settings/display/k;->LS(Landroid/content/Context;I)V

    :cond_0
    return-void

    :cond_1
    mul-int/lit8 v0, p2, 0x3c

    add-int/2addr v0, p3

    invoke-static {v0}, Lcom/android/settings/display/PaperModeFragment;->Mm(I)I

    iget-object v0, p0, Lcom/android/settings/display/H;->XO:Lcom/android/settings/display/PaperModeFragment;

    invoke-static {v0}, Lcom/android/settings/display/PaperModeFragment;->Mf(Lcom/android/settings/display/PaperModeFragment;)Landroid/content/Context;

    move-result-object v0

    invoke-static {}, Lcom/android/settings/display/PaperModeFragment;->Mg()I

    move-result v1

    invoke-static {v0, v1}, Lcom/android/settings/display/k;->LN(Landroid/content/Context;I)V

    iget-object v0, p0, Lcom/android/settings/display/H;->XO:Lcom/android/settings/display/PaperModeFragment;

    invoke-static {v0}, Lcom/android/settings/display/PaperModeFragment;->Mk(Lcom/android/settings/display/PaperModeFragment;)Lcom/android/settings/dndmode/LabelPreference;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/display/H;->XO:Lcom/android/settings/display/PaperModeFragment;

    invoke-static {v1, p2, p3}, Lcom/android/settings/display/PaperModeFragment;->Mq(Lcom/android/settings/display/PaperModeFragment;II)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/settings/dndmode/LabelPreference;->mp(Ljava/lang/String;)V

    goto :goto_0
.end method
