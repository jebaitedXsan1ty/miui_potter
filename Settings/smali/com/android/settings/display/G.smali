.class final Lcom/android/settings/display/G;
.super Ljava/lang/Object;
.source "NightDisplaySettings.java"

# interfaces
.implements Landroid/app/TimePickerDialog$OnTimeSetListener;


# instance fields
.field final synthetic XM:Lcom/android/settings/display/NightDisplaySettings;

.field final synthetic XN:I


# direct methods
.method constructor <init>(Lcom/android/settings/display/NightDisplaySettings;I)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/display/G;->XM:Lcom/android/settings/display/NightDisplaySettings;

    iput p2, p0, Lcom/android/settings/display/G;->XN:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onTimeSet(Landroid/widget/TimePicker;II)V
    .locals 2

    invoke-static {p2, p3}, Ljava/time/LocalTime;->of(II)Ljava/time/LocalTime;

    move-result-object v0

    iget v1, p0, Lcom/android/settings/display/G;->XN:I

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/android/settings/display/G;->XM:Lcom/android/settings/display/NightDisplaySettings;

    invoke-static {v1}, Lcom/android/settings/display/NightDisplaySettings;->Lz(Lcom/android/settings/display/NightDisplaySettings;)Lcom/android/internal/app/NightDisplayController;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/android/internal/app/NightDisplayController;->setCustomStartTime(Ljava/time/LocalTime;)Z

    :goto_0
    return-void

    :cond_0
    iget-object v1, p0, Lcom/android/settings/display/G;->XM:Lcom/android/settings/display/NightDisplaySettings;

    invoke-static {v1}, Lcom/android/settings/display/NightDisplaySettings;->Lz(Lcom/android/settings/display/NightDisplaySettings;)Lcom/android/internal/app/NightDisplayController;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/android/internal/app/NightDisplayController;->setCustomEndTime(Ljava/time/LocalTime;)Z

    goto :goto_0
.end method
