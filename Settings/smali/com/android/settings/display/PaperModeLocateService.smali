.class public Lcom/android/settings/display/PaperModeLocateService;
.super Landroid/app/job/JobService;
.source "PaperModeLocateService.java"


# instance fields
.field private Ua:Lcom/android/settings/display/s;

.field private Ub:Landroid/os/Handler;

.field private Uc:Landroid/app/job/JobParameters;

.field private Ud:Landroid/location/Location;

.field private Ue:Lcom/android/settings/display/h;

.field private Uf:Landroid/location/LocationManager;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/app/job/JobService;-><init>()V

    return-void
.end method

.method private Lq(Landroid/location/Location;)V
    .locals 8

    iget-object v1, p0, Lcom/android/settings/display/PaperModeLocateService;->Ua:Lcom/android/settings/display/s;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {p1}, Landroid/location/Location;->getLatitude()D

    move-result-wide v4

    invoke-virtual {p1}, Landroid/location/Location;->getLongitude()D

    move-result-wide v6

    invoke-virtual/range {v1 .. v7}, Lcom/android/settings/display/s;->NW(JDD)V

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string/jumbo v1, "miui.intent.action.LOCATION_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const-string/jumbo v1, "sunrise"

    iget-object v2, p0, Lcom/android/settings/display/PaperModeLocateService;->Ua:Lcom/android/settings/display/s;

    iget v2, v2, Lcom/android/settings/display/s;->WB:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string/jumbo v1, "sunset"

    iget-object v2, p0, Lcom/android/settings/display/PaperModeLocateService;->Ua:Lcom/android/settings/display/s;

    iget v2, v2, Lcom/android/settings/display/s;->WC:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string/jumbo v1, "state"

    iget-object v2, p0, Lcom/android/settings/display/PaperModeLocateService;->Ua:Lcom/android/settings/display/s;

    iget v2, v2, Lcom/android/settings/display/s;->WD:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const/high16 v1, 0x1000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Lcom/android/settings/display/PaperModeLocateService;->sendBroadcast(Landroid/content/Intent;)V

    return-void
.end method

.method private Lr()V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/android/settings/display/PaperModeLocateService;->Ue:Lcom/android/settings/display/h;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/display/PaperModeLocateService;->Uf:Landroid/location/LocationManager;

    iget-object v1, p0, Lcom/android/settings/display/PaperModeLocateService;->Ue:Lcom/android/settings/display/h;

    invoke-virtual {v0, v1}, Landroid/location/LocationManager;->removeUpdates(Landroid/location/LocationListener;)V

    iput-object v2, p0, Lcom/android/settings/display/PaperModeLocateService;->Ue:Lcom/android/settings/display/h;

    :cond_0
    return-void
.end method

.method private Ls()V
    .locals 5

    iget-object v0, p0, Lcom/android/settings/display/PaperModeLocateService;->Uf:Landroid/location/LocationManager;

    const-string/jumbo v1, "network"

    invoke-virtual {v0, v1}, Landroid/location/LocationManager;->getProvider(Ljava/lang/String;)Landroid/location/LocationProvider;

    move-result-object v0

    if-eqz v0, :cond_0

    :try_start_0
    const-string/jumbo v0, "PaperModeLocateService"

    const-string/jumbo v1, "requestLocationUpdates"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string/jumbo v0, "network"

    const-wide/16 v2, 0x3e8

    const/4 v1, 0x0

    const/4 v4, 0x1

    invoke-static {v0, v2, v3, v1, v4}, Landroid/location/LocationRequest;->createFromDeprecatedProvider(Ljava/lang/String;JFZ)Landroid/location/LocationRequest;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/display/PaperModeLocateService;->Uf:Landroid/location/LocationManager;

    iget-object v2, p0, Lcom/android/settings/display/PaperModeLocateService;->Ue:Lcom/android/settings/display/h;

    const/4 v3, 0x0

    invoke-virtual {v1, v0, v2, v3}, Landroid/location/LocationManager;->requestLocationUpdates(Landroid/location/LocationRequest;Landroid/location/LocationListener;Landroid/os/Looper;)V

    iget-object v0, p0, Lcom/android/settings/display/PaperModeLocateService;->Ub:Landroid/os/Handler;

    new-instance v1, Lcom/android/settings/display/F;

    invoke-direct {v1, p0}, Lcom/android/settings/display/F;-><init>(Lcom/android/settings/display/PaperModeLocateService;)V

    const-wide/32 v2, 0x493e0

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    iget-object v0, p0, Lcom/android/settings/display/PaperModeLocateService;->Uf:Landroid/location/LocationManager;

    const-string/jumbo v1, "network"

    invoke-virtual {v0, v1}, Landroid/location/LocationManager;->getLastKnownLocation(Ljava/lang/String;)Landroid/location/Location;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/display/PaperModeLocateService;->Ud:Landroid/location/Location;

    iget-object v0, p0, Lcom/android/settings/display/PaperModeLocateService;->Ud:Landroid/location/Location;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/display/PaperModeLocateService;->Ud:Landroid/location/Location;

    invoke-direct {p0, v0}, Lcom/android/settings/display/PaperModeLocateService;->Lq(Landroid/location/Location;)V

    :cond_0
    return-void

    :catch_0
    move-exception v0

    const-string/jumbo v1, "PaperModeLocateService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "register location listener error: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method static synthetic Lt(Lcom/android/settings/display/PaperModeLocateService;)Landroid/app/job/JobParameters;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/display/PaperModeLocateService;->Uc:Landroid/app/job/JobParameters;

    return-object v0
.end method

.method static synthetic Lu(Lcom/android/settings/display/PaperModeLocateService;Landroid/location/Location;)Landroid/location/Location;
    .locals 0

    iput-object p1, p0, Lcom/android/settings/display/PaperModeLocateService;->Ud:Landroid/location/Location;

    return-object p1
.end method

.method static synthetic Lv(Lcom/android/settings/display/PaperModeLocateService;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/display/PaperModeLocateService;->Lr()V

    return-void
.end method


# virtual methods
.method public onStartJob(Landroid/app/job/JobParameters;)Z
    .locals 2

    const-string/jumbo v0, "PaperModeLocateService"

    const-string/jumbo v1, "PaperModeLocateService start"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iput-object p1, p0, Lcom/android/settings/display/PaperModeLocateService;->Uc:Landroid/app/job/JobParameters;

    const-string/jumbo v0, "location"

    invoke-virtual {p0, v0}, Lcom/android/settings/display/PaperModeLocateService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/location/LocationManager;

    iput-object v0, p0, Lcom/android/settings/display/PaperModeLocateService;->Uf:Landroid/location/LocationManager;

    new-instance v0, Lcom/android/settings/display/s;

    invoke-direct {v0}, Lcom/android/settings/display/s;-><init>()V

    iput-object v0, p0, Lcom/android/settings/display/PaperModeLocateService;->Ua:Lcom/android/settings/display/s;

    new-instance v0, Lcom/android/settings/display/h;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/android/settings/display/h;-><init>(Lcom/android/settings/display/PaperModeLocateService;Lcom/android/settings/display/h;)V

    iput-object v0, p0, Lcom/android/settings/display/PaperModeLocateService;->Ue:Lcom/android/settings/display/h;

    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/android/settings/display/PaperModeLocateService;->Ub:Landroid/os/Handler;

    invoke-direct {p0}, Lcom/android/settings/display/PaperModeLocateService;->Ls()V

    const/4 v0, 0x0

    return v0
.end method

.method public onStopJob(Landroid/app/job/JobParameters;)Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method
