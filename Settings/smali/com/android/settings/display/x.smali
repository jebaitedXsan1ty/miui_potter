.class final Lcom/android/settings/display/x;
.super Ljava/lang/Object;
.source "FluencyModeListPreference.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field final synthetic Xx:Lcom/android/settings/display/FluencyModeListPreference;


# direct methods
.method constructor <init>(Lcom/android/settings/display/FluencyModeListPreference;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/display/x;->Xx:Lcom/android/settings/display/FluencyModeListPreference;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 1

    if-ltz p2, :cond_0

    iget-object v0, p0, Lcom/android/settings/display/x;->Xx:Lcom/android/settings/display/FluencyModeListPreference;

    invoke-static {v0}, Lcom/android/settings/display/FluencyModeListPreference;->KI(Lcom/android/settings/display/FluencyModeListPreference;)[Ljava/lang/String;

    move-result-object v0

    array-length v0, v0

    if-lt p2, v0, :cond_1

    :cond_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/settings/display/x;->Xx:Lcom/android/settings/display/FluencyModeListPreference;

    invoke-virtual {v0, p2}, Lcom/android/settings/display/FluencyModeListPreference;->setValueIndex(I)V

    iget-object v0, p0, Lcom/android/settings/display/x;->Xx:Lcom/android/settings/display/FluencyModeListPreference;

    invoke-static {v0, p2}, Lcom/android/settings/display/FluencyModeListPreference;->KL(Lcom/android/settings/display/FluencyModeListPreference;I)I

    iget-object v0, p0, Lcom/android/settings/display/x;->Xx:Lcom/android/settings/display/FluencyModeListPreference;

    invoke-static {v0}, Lcom/android/settings/display/FluencyModeListPreference;->KJ(Lcom/android/settings/display/FluencyModeListPreference;)I

    move-result v0

    if-eq p2, v0, :cond_2

    iget-object v0, p0, Lcom/android/settings/display/x;->Xx:Lcom/android/settings/display/FluencyModeListPreference;

    invoke-static {v0, p2}, Lcom/android/settings/display/FluencyModeListPreference;->KN(Lcom/android/settings/display/FluencyModeListPreference;I)V

    :cond_2
    iget-object v0, p0, Lcom/android/settings/display/x;->Xx:Lcom/android/settings/display/FluencyModeListPreference;

    invoke-virtual {v0}, Lcom/android/settings/display/FluencyModeListPreference;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    return-void
.end method
