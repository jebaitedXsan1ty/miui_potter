.class public Lcom/android/settings/display/ForceTouchGuideActivity;
.super Lmiui/preference/PreferenceActivity;
.source "ForceTouchGuideActivity.java"


# instance fields
.field private Wp:Lmiui/preference/ValuePreference;

.field private Wq:Landroid/preference/CheckBoxPreference;

.field Wr:Landroid/preference/Preference$OnPreferenceChangeListener;

.field private Ws:Landroid/widget/VideoView;

.field private mContext:Landroid/content/Context;

.field private mEnabled:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lmiui/preference/PreferenceActivity;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/settings/display/ForceTouchGuideActivity;->mEnabled:Z

    new-instance v0, Lcom/android/settings/display/Z;

    invoke-direct {v0, p0}, Lcom/android/settings/display/Z;-><init>(Lcom/android/settings/display/ForceTouchGuideActivity;)V

    iput-object v0, p0, Lcom/android/settings/display/ForceTouchGuideActivity;->Wr:Landroid/preference/Preference$OnPreferenceChangeListener;

    return-void
.end method

.method private NI(Ljava/lang/String;)Landroid/net/Uri;
    .locals 4

    invoke-virtual {p0}, Lcom/android/settings/display/ForceTouchGuideActivity;->getPackageName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lcom/android/settings/display/ForceTouchGuideActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const-string/jumbo v2, "raw"

    invoke-virtual {v1, p1, v2, v0}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x0

    return-object v0

    :cond_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "android.resource://"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v2, "/"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method private NJ()V
    .locals 2

    invoke-virtual {p0}, Lcom/android/settings/display/ForceTouchGuideActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/display/ForceTouchGuideActivity;->mContext:Landroid/content/Context;

    const-string/jumbo v0, "force_touch_demo"

    invoke-virtual {p0, v0}, Lcom/android/settings/display/ForceTouchGuideActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Lmiui/preference/ValuePreference;

    iput-object v0, p0, Lcom/android/settings/display/ForceTouchGuideActivity;->Wp:Lmiui/preference/ValuePreference;

    iget-object v0, p0, Lcom/android/settings/display/ForceTouchGuideActivity;->mContext:Landroid/content/Context;

    invoke-static {v0}, Landroid/provider/MiuiSettings$ForceTouch;->isEnabled(Landroid/content/Context;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/settings/display/ForceTouchGuideActivity;->mEnabled:Z

    const-string/jumbo v0, "force_touch_enable"

    invoke-virtual {p0, v0}, Lcom/android/settings/display/ForceTouchGuideActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    iput-object v0, p0, Lcom/android/settings/display/ForceTouchGuideActivity;->Wq:Landroid/preference/CheckBoxPreference;

    iget-object v0, p0, Lcom/android/settings/display/ForceTouchGuideActivity;->Wq:Landroid/preference/CheckBoxPreference;

    iget-object v1, p0, Lcom/android/settings/display/ForceTouchGuideActivity;->Wr:Landroid/preference/Preference$OnPreferenceChangeListener;

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    iget-object v0, p0, Lcom/android/settings/display/ForceTouchGuideActivity;->Wq:Landroid/preference/CheckBoxPreference;

    iget-boolean v1, p0, Lcom/android/settings/display/ForceTouchGuideActivity;->mEnabled:Z

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    const v0, 0x7f0a01a9

    invoke-virtual {p0, v0}, Lcom/android/settings/display/ForceTouchGuideActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/VideoView;

    iput-object v0, p0, Lcom/android/settings/display/ForceTouchGuideActivity;->Ws:Landroid/widget/VideoView;

    new-instance v0, Lcom/android/settings/display/aa;

    invoke-direct {v0, p0}, Lcom/android/settings/display/aa;-><init>(Lcom/android/settings/display/ForceTouchGuideActivity;)V

    iget-object v1, p0, Lcom/android/settings/display/ForceTouchGuideActivity;->Ws:Landroid/widget/VideoView;

    invoke-virtual {v1, v0}, Landroid/widget/VideoView;->setOnCompletionListener(Landroid/media/MediaPlayer$OnCompletionListener;)V

    iget-object v0, p0, Lcom/android/settings/display/ForceTouchGuideActivity;->Ws:Landroid/widget/VideoView;

    const-string/jumbo v1, "force_touch"

    invoke-direct {p0, v1}, Lcom/android/settings/display/ForceTouchGuideActivity;->NI(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/VideoView;->setVideoURI(Landroid/net/Uri;)V

    iget-object v0, p0, Lcom/android/settings/display/ForceTouchGuideActivity;->Ws:Landroid/widget/VideoView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/VideoView;->setZOrderOnTop(Z)V

    iget-object v0, p0, Lcom/android/settings/display/ForceTouchGuideActivity;->Ws:Landroid/widget/VideoView;

    invoke-virtual {v0}, Landroid/widget/VideoView;->requestFocus()Z

    return-void
.end method

.method private NK(Z)V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/display/ForceTouchGuideActivity;->Ws:Landroid/widget/VideoView;

    if-nez v0, :cond_0

    return-void

    :cond_0
    if-eqz p1, :cond_1

    iget-object v0, p0, Lcom/android/settings/display/ForceTouchGuideActivity;->Ws:Landroid/widget/VideoView;

    invoke-virtual {v0}, Landroid/widget/VideoView;->start()V

    invoke-virtual {p0}, Lcom/android/settings/display/ForceTouchGuideActivity;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/display/ForceTouchGuideActivity;->Wp:Lmiui/preference/ValuePreference;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    iget-object v0, p0, Lcom/android/settings/display/ForceTouchGuideActivity;->Ws:Landroid/widget/VideoView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/VideoView;->setVisibility(I)V

    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/settings/display/ForceTouchGuideActivity;->Ws:Landroid/widget/VideoView;

    invoke-virtual {v0}, Landroid/widget/VideoView;->pause()V

    invoke-virtual {p0}, Lcom/android/settings/display/ForceTouchGuideActivity;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/display/ForceTouchGuideActivity;->Wp:Lmiui/preference/ValuePreference;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    iget-object v0, p0, Lcom/android/settings/display/ForceTouchGuideActivity;->Ws:Landroid/widget/VideoView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/VideoView;->setVisibility(I)V

    goto :goto_0
.end method

.method static synthetic NL(Lcom/android/settings/display/ForceTouchGuideActivity;)Landroid/content/Context;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/display/ForceTouchGuideActivity;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic NM(Lcom/android/settings/display/ForceTouchGuideActivity;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/settings/display/ForceTouchGuideActivity;->mEnabled:Z

    return v0
.end method

.method static synthetic NN(Lcom/android/settings/display/ForceTouchGuideActivity;)Landroid/preference/CheckBoxPreference;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/display/ForceTouchGuideActivity;->Wq:Landroid/preference/CheckBoxPreference;

    return-object v0
.end method

.method static synthetic NO(Lcom/android/settings/display/ForceTouchGuideActivity;)Landroid/widget/VideoView;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/display/ForceTouchGuideActivity;->Ws:Landroid/widget/VideoView;

    return-object v0
.end method

.method static synthetic NP(Lcom/android/settings/display/ForceTouchGuideActivity;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/android/settings/display/ForceTouchGuideActivity;->mEnabled:Z

    return p1
.end method

.method static synthetic NQ(Lcom/android/settings/display/ForceTouchGuideActivity;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/settings/display/ForceTouchGuideActivity;->NK(Z)V

    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 1

    invoke-super {p0, p1}, Lmiui/preference/PreferenceActivity;->onCreate(Landroid/os/Bundle;)V

    const v0, 0x7f0d00c6

    invoke-virtual {p0, v0}, Lcom/android/settings/display/ForceTouchGuideActivity;->setContentView(I)V

    const v0, 0x7f15005f

    invoke-virtual {p0, v0}, Lcom/android/settings/display/ForceTouchGuideActivity;->addPreferencesFromResource(I)V

    invoke-direct {p0}, Lcom/android/settings/display/ForceTouchGuideActivity;->NJ()V

    return-void
.end method

.method protected onPause()V
    .locals 1

    invoke-super {p0}, Lmiui/preference/PreferenceActivity;->onPause()V

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/android/settings/display/ForceTouchGuideActivity;->NK(Z)V

    return-void
.end method

.method public onResume()V
    .locals 1

    invoke-super {p0}, Lmiui/preference/PreferenceActivity;->onResume()V

    iget-boolean v0, p0, Lcom/android/settings/display/ForceTouchGuideActivity;->mEnabled:Z

    invoke-direct {p0, v0}, Lcom/android/settings/display/ForceTouchGuideActivity;->NK(Z)V

    return-void
.end method
