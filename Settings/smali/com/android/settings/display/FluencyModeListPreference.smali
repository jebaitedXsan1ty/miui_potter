.class public Lcom/android/settings/display/FluencyModeListPreference;
.super Lcom/android/settings/MiuiListPreference;
.source "FluencyModeListPreference.java"


# instance fields
.field private Ti:[Ljava/lang/String;

.field private Tj:[Ljava/lang/String;

.field private Tk:Lcom/android/settings/display/b;

.field private Tl:I

.field private Tm:I

.field private Tn:[Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    invoke-direct {p0, p1, p2}, Lcom/android/settings/MiuiListPreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    new-instance v0, Lcom/android/settings/display/b;

    invoke-direct {v0, p0, p1}, Lcom/android/settings/display/b;-><init>(Lcom/android/settings/display/FluencyModeListPreference;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/settings/display/FluencyModeListPreference;->Tk:Lcom/android/settings/display/b;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f030079

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/android/settings/display/FluencyModeListPreference;->Tj:[Ljava/lang/String;

    const v1, 0x7f03007a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/android/settings/display/FluencyModeListPreference;->Tn:[Ljava/lang/String;

    const v1, 0x7f030078

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/display/FluencyModeListPreference;->Ti:[Ljava/lang/String;

    iget-object v0, p0, Lcom/android/settings/display/FluencyModeListPreference;->Tj:[Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/android/settings/display/FluencyModeListPreference;->setEntryValues([Ljava/lang/CharSequence;)V

    invoke-direct {p0}, Lcom/android/settings/display/FluencyModeListPreference;->getStatus()I

    move-result v0

    iput v0, p0, Lcom/android/settings/display/FluencyModeListPreference;->Tl:I

    iget v0, p0, Lcom/android/settings/display/FluencyModeListPreference;->Tl:I

    iput v0, p0, Lcom/android/settings/display/FluencyModeListPreference;->Tm:I

    iget-object v0, p0, Lcom/android/settings/display/FluencyModeListPreference;->Tj:[Ljava/lang/String;

    iget v1, p0, Lcom/android/settings/display/FluencyModeListPreference;->Tl:I

    aget-object v0, v0, v1

    invoke-virtual {p0, v0}, Lcom/android/settings/display/FluencyModeListPreference;->setSummary(Ljava/lang/CharSequence;)V

    return-void
.end method

.method private KG(I)V
    .locals 2

    if-nez p1, :cond_1

    const-string/jumbo v0, "persist.sys.miui_feature_config"

    const-string/jumbo v1, "/system/etc/miui_feature/lite.conf"

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    :goto_0
    invoke-virtual {p0}, Lcom/android/settings/display/FluencyModeListPreference;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string/jumbo v1, "power"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/PowerManager;->reboot(Ljava/lang/String;)V

    return-void

    :cond_1
    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    const-string/jumbo v0, "persist.sys.miui_feature_config"

    const-string/jumbo v1, "/system/etc/miui_feature/default.conf"

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private KH(I)V
    .locals 3

    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/android/settings/display/FluencyModeListPreference;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    iget-object v1, p0, Lcom/android/settings/display/FluencyModeListPreference;->Tj:[Ljava/lang/String;

    aget-object v1, v1, p1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x1010355

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setIconAttribute(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/display/FluencyModeListPreference;->Ti:[Ljava/lang/String;

    aget-object v1, v1, p1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    new-instance v1, Lcom/android/settings/display/y;

    invoke-direct {v1, p0, p1}, Lcom/android/settings/display/y;-><init>(Lcom/android/settings/display/FluencyModeListPreference;I)V

    const v2, 0x7f12075b

    invoke-virtual {v0, v2, v1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    new-instance v1, Lcom/android/settings/display/z;

    invoke-direct {v1, p0}, Lcom/android/settings/display/z;-><init>(Lcom/android/settings/display/FluencyModeListPreference;)V

    const v2, 0x1040009

    invoke-virtual {v0, v2, v1}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    new-instance v1, Lcom/android/settings/display/A;

    invoke-direct {v1, p0}, Lcom/android/settings/display/A;-><init>(Lcom/android/settings/display/FluencyModeListPreference;)V

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    return-void
.end method

.method static synthetic KI(Lcom/android/settings/display/FluencyModeListPreference;)[Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/display/FluencyModeListPreference;->Tj:[Ljava/lang/String;

    return-object v0
.end method

.method static synthetic KJ(Lcom/android/settings/display/FluencyModeListPreference;)I
    .locals 1

    iget v0, p0, Lcom/android/settings/display/FluencyModeListPreference;->Tl:I

    return v0
.end method

.method static synthetic KK(Lcom/android/settings/display/FluencyModeListPreference;)[Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/display/FluencyModeListPreference;->Tn:[Ljava/lang/String;

    return-object v0
.end method

.method static synthetic KL(Lcom/android/settings/display/FluencyModeListPreference;I)I
    .locals 0

    iput p1, p0, Lcom/android/settings/display/FluencyModeListPreference;->Tm:I

    return p1
.end method

.method static synthetic KM(Lcom/android/settings/display/FluencyModeListPreference;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/settings/display/FluencyModeListPreference;->KG(I)V

    return-void
.end method

.method static synthetic KN(Lcom/android/settings/display/FluencyModeListPreference;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/settings/display/FluencyModeListPreference;->KH(I)V

    return-void
.end method

.method private getStatus()I
    .locals 1

    invoke-static {}, Lmiui/util/MiuiFeatureUtils;->isLiteMode()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    return v0

    :cond_0
    const/4 v0, 0x1

    return v0
.end method


# virtual methods
.method protected onPrepareDialogBuilder(Landroid/app/AlertDialog$Builder;)V
    .locals 4

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/android/settings/display/FluencyModeListPreference;->Tk:Lcom/android/settings/display/b;

    iget v1, p0, Lcom/android/settings/display/FluencyModeListPreference;->Tl:I

    new-instance v2, Lcom/android/settings/display/x;

    invoke-direct {v2, p0}, Lcom/android/settings/display/x;-><init>(Lcom/android/settings/display/FluencyModeListPreference;)V

    invoke-virtual {p1, v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setSingleChoiceItems(Landroid/widget/ListAdapter;ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {p1, v3, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    return-void
.end method

.method protected onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 2

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-class v1, Lcom/android/settings/display/FluencyModeListPreference$SavedState;

    invoke-virtual {v0, v1}, Ljava/lang/Class;->equals(Ljava/lang/Object;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_1

    :cond_0
    invoke-super {p0, p1}, Lcom/android/settings/MiuiListPreference;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    return-void

    :cond_1
    check-cast p1, Lcom/android/settings/display/FluencyModeListPreference$SavedState;

    invoke-virtual {p1}, Lcom/android/settings/display/FluencyModeListPreference$SavedState;->getSuperState()Landroid/os/Parcelable;

    move-result-object v0

    invoke-super {p0, v0}, Lcom/android/settings/MiuiListPreference;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    iget v0, p1, Lcom/android/settings/display/FluencyModeListPreference$SavedState;->value:I

    iput v0, p0, Lcom/android/settings/display/FluencyModeListPreference;->Tm:I

    iget v0, p0, Lcom/android/settings/display/FluencyModeListPreference;->Tl:I

    iget v1, p0, Lcom/android/settings/display/FluencyModeListPreference;->Tm:I

    if-eq v0, v1, :cond_2

    iget v0, p0, Lcom/android/settings/display/FluencyModeListPreference;->Tm:I

    invoke-direct {p0, v0}, Lcom/android/settings/display/FluencyModeListPreference;->KH(I)V

    :cond_2
    return-void
.end method

.method protected onSaveInstanceState()Landroid/os/Parcelable;
    .locals 2

    new-instance v0, Lcom/android/settings/display/FluencyModeListPreference$SavedState;

    invoke-super {p0}, Lcom/android/settings/MiuiListPreference;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/android/settings/display/FluencyModeListPreference$SavedState;-><init>(Landroid/os/Parcelable;)V

    iget v1, p0, Lcom/android/settings/display/FluencyModeListPreference;->Tm:I

    iput v1, v0, Lcom/android/settings/display/FluencyModeListPreference$SavedState;->value:I

    return-object v0
.end method
