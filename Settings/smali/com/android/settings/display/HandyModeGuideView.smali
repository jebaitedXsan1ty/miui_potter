.class public Lcom/android/settings/display/HandyModeGuideView;
.super Landroid/view/View;
.source "HandyModeGuideView.java"


# instance fields
.field Vj:Landroid/animation/ValueAnimator;

.field Vk:Landroid/graphics/Rect;

.field Vl:Z

.field Vm:Landroid/graphics/Paint;

.field Vn:Z

.field Vo:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/android/settings/display/HandyModeGuideView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/android/settings/display/HandyModeGuideView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 5

    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    new-instance v0, Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/android/settings/display/HandyModeGuideView;->Vm:Landroid/graphics/Paint;

    const/4 v0, 0x2

    new-array v0, v0, [F

    fill-array-data v0, :array_0

    invoke-static {v0}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/display/HandyModeGuideView;->Vj:Landroid/animation/ValueAnimator;

    invoke-virtual {p0}, Lcom/android/settings/display/HandyModeGuideView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f070106

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    invoke-virtual {p0}, Lcom/android/settings/display/HandyModeGuideView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f070107

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    invoke-virtual {p0}, Lcom/android/settings/display/HandyModeGuideView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f070108

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v2

    invoke-virtual {p0}, Lcom/android/settings/display/HandyModeGuideView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f070105

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v3

    new-instance v4, Landroid/graphics/Rect;

    add-int/2addr v2, v0

    add-int/2addr v3, v1

    invoke-direct {v4, v0, v1, v2, v3}, Landroid/graphics/Rect;-><init>(IIII)V

    iput-object v4, p0, Lcom/android/settings/display/HandyModeGuideView;->Vk:Landroid/graphics/Rect;

    iget-object v0, p0, Lcom/android/settings/display/HandyModeGuideView;->Vj:Landroid/animation/ValueAnimator;

    new-instance v1, Lcom/android/settings/display/R;

    invoke-direct {v1, p0}, Lcom/android/settings/display/R;-><init>(Lcom/android/settings/display/HandyModeGuideView;)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    iget-object v0, p0, Lcom/android/settings/display/HandyModeGuideView;->Vj:Landroid/animation/ValueAnimator;

    new-instance v1, Lcom/android/settings/display/T;

    invoke-direct {v1, p0}, Lcom/android/settings/display/T;-><init>(Lcom/android/settings/display/HandyModeGuideView;)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    iget-object v0, p0, Lcom/android/settings/display/HandyModeGuideView;->Vj:Landroid/animation/ValueAnimator;

    invoke-virtual {p0}, Lcom/android/settings/display/HandyModeGuideView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b000e

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    int-to-long v2, v1

    invoke-virtual {v0, v2, v3}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    iget-object v0, p0, Lcom/android/settings/display/HandyModeGuideView;->Vj:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    return-void

    nop

    :array_0
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data
.end method

.method private MZ(Landroid/graphics/Canvas;)V
    .locals 4

    const/high16 v1, 0x3f800000    # 1.0f

    const/4 v0, 0x0

    iget v2, p0, Lcom/android/settings/display/HandyModeGuideView;->Vo:I

    packed-switch v2, :pswitch_data_0

    :goto_0
    iget-boolean v2, p0, Lcom/android/settings/display/HandyModeGuideView;->Vn:Z

    if-eqz v2, :cond_0

    sub-float v0, v1, v0

    :cond_0
    iget-object v2, p0, Lcom/android/settings/display/HandyModeGuideView;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lmiui/util/HandyModeUtils;->getInstance(Landroid/content/Context;)Lmiui/util/HandyModeUtils;

    move-result-object v2

    invoke-virtual {v2}, Lmiui/util/HandyModeUtils;->getScale()F

    move-result v2

    sub-float v2, v1, v2

    mul-float/2addr v0, v2

    sub-float/2addr v1, v0

    iget-object v0, p0, Lcom/android/settings/display/HandyModeGuideView;->Vm:Landroid/graphics/Paint;

    invoke-virtual {p0}, Lcom/android/settings/display/HandyModeGuideView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f060078

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setColor(I)V

    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    iget-boolean v0, p0, Lcom/android/settings/display/HandyModeGuideView;->Vl:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/settings/display/HandyModeGuideView;->Vk:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->left:I

    :goto_1
    int-to-float v0, v0

    iget-object v2, p0, Lcom/android/settings/display/HandyModeGuideView;->Vk:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->bottom:I

    int-to-float v2, v2

    invoke-virtual {p1, v1, v1, v0, v2}, Landroid/graphics/Canvas;->scale(FFFF)V

    iget-object v0, p0, Lcom/android/settings/display/HandyModeGuideView;->Vk:Landroid/graphics/Rect;

    iget-object v1, p0, Lcom/android/settings/display/HandyModeGuideView;->Vm:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    return-void

    :pswitch_0
    iget-object v0, p0, Lcom/android/settings/display/HandyModeGuideView;->Vj:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->getAnimatedValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    goto :goto_0

    :pswitch_1
    move v0, v1

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/android/settings/display/HandyModeGuideView;->Vk:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->right:I

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method private Na(Landroid/graphics/Canvas;)V
    .locals 9

    const/4 v5, 0x0

    const/high16 v4, 0x3f800000    # 1.0f

    iget-object v0, p0, Lcom/android/settings/display/HandyModeGuideView;->Vj:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->getAnimatedValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v2

    invoke-virtual {p0}, Lcom/android/settings/display/HandyModeGuideView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f070100

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v6

    invoke-virtual {p0}, Lcom/android/settings/display/HandyModeGuideView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f070101

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    invoke-virtual {p0}, Lcom/android/settings/display/HandyModeGuideView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0700ff

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    iget-boolean v1, p0, Lcom/android/settings/display/HandyModeGuideView;->Vl:Z

    if-eqz v1, :cond_0

    neg-float v0, v0

    :cond_0
    invoke-virtual {p0}, Lcom/android/settings/display/HandyModeGuideView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v7, 0x7f070102

    invoke-virtual {v1, v7}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v7

    invoke-virtual {p0}, Lcom/android/settings/display/HandyModeGuideView;->getWidth()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    int-to-float v1, v1

    iget v8, p0, Lcom/android/settings/display/HandyModeGuideView;->Vo:I

    packed-switch v8, :pswitch_data_0

    move v0, v3

    move v2, v5

    :goto_0
    cmpl-float v3, v2, v5

    if-lez v3, :cond_1

    invoke-virtual {p0}, Lcom/android/settings/display/HandyModeGuideView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f060079

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-static {v3}, Landroid/graphics/Color;->alpha(I)I

    move-result v4

    int-to-float v4, v4

    mul-float/2addr v2, v4

    float-to-int v2, v2

    shl-int/lit8 v2, v2, 0x18

    const v4, 0xffffff

    and-int/2addr v3, v4

    or-int/2addr v2, v3

    iget-object v3, p0, Lcom/android/settings/display/HandyModeGuideView;->Vm:Landroid/graphics/Paint;

    invoke-virtual {v3, v2}, Landroid/graphics/Paint;->setColor(I)V

    iget-object v2, p0, Lcom/android/settings/display/HandyModeGuideView;->Vm:Landroid/graphics/Paint;

    invoke-virtual {p1, v1, v7, v0, v2}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    :cond_1
    return-void

    :pswitch_0
    sub-float v3, v6, v3

    mul-float/2addr v3, v2

    sub-float v3, v6, v3

    iget-boolean v4, p0, Lcom/android/settings/display/HandyModeGuideView;->Vn:Z

    if-eqz v4, :cond_4

    add-float/2addr v0, v1

    :goto_1
    move v1, v0

    move v0, v3

    goto :goto_0

    :pswitch_1
    iget-boolean v6, p0, Lcom/android/settings/display/HandyModeGuideView;->Vn:Z

    if-eqz v6, :cond_2

    sub-float v2, v4, v2

    :cond_2
    mul-float/2addr v0, v2

    add-float/2addr v1, v0

    move v0, v3

    move v2, v4

    goto :goto_0

    :pswitch_2
    sub-float/2addr v4, v2

    sub-float/2addr v6, v3

    mul-float/2addr v2, v6

    add-float/2addr v2, v3

    iget-boolean v3, p0, Lcom/android/settings/display/HandyModeGuideView;->Vn:Z

    if-nez v3, :cond_3

    add-float/2addr v1, v0

    :cond_3
    move v0, v2

    move v2, v4

    goto :goto_0

    :cond_4
    move v0, v1

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method


# virtual methods
.method protected onDetachedFromWindow()V
    .locals 1

    invoke-super {p0}, Landroid/view/View;->onDetachedFromWindow()V

    iget-object v0, p0, Lcom/android/settings/display/HandyModeGuideView;->Vj:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->removeAllListeners()V

    iget-object v0, p0, Lcom/android/settings/display/HandyModeGuideView;->Vj:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->removeAllUpdateListeners()V

    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/settings/display/HandyModeGuideView;->MZ(Landroid/graphics/Canvas;)V

    invoke-direct {p0, p1}, Lcom/android/settings/display/HandyModeGuideView;->Na(Landroid/graphics/Canvas;)V

    return-void
.end method

.method protected onMeasure(II)V
    .locals 3

    invoke-virtual {p0}, Lcom/android/settings/display/HandyModeGuideView;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v0

    invoke-virtual {p0}, Lcom/android/settings/display/HandyModeGuideView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f070103

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/android/settings/display/HandyModeGuideView;->setMeasuredDimension(II)V

    return-void
.end method
