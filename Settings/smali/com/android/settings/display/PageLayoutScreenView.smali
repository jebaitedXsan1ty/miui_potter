.class public Lcom/android/settings/display/PageLayoutScreenView;
.super Lmiui/widget/ScreenView;
.source "PageLayoutScreenView.java"

# interfaces
.implements Lmiui/widget/ScreenView$SnapScreenOnceNotification;


# instance fields
.field private final Tf:I

.field private Tg:Landroid/app/Activity;

.field private final Th:[Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/android/settings/display/PageLayoutScreenView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3

    const/4 v0, 0x2

    const/4 v2, 0x1

    const/4 v1, 0x0

    invoke-direct {p0, p1, p2, p3}, Lmiui/widget/ScreenView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iput v0, p0, Lcom/android/settings/display/PageLayoutScreenView;->Tf:I

    new-array v0, v0, [Z

    iput-object v0, p0, Lcom/android/settings/display/PageLayoutScreenView;->Th:[Z

    check-cast p1, Landroid/app/Activity;

    iput-object p1, p0, Lcom/android/settings/display/PageLayoutScreenView;->Tg:Landroid/app/Activity;

    iget-object v0, p0, Lcom/android/settings/display/PageLayoutScreenView;->Th:[Z

    aput-boolean v2, v0, v1

    iget-object v0, p0, Lcom/android/settings/display/PageLayoutScreenView;->Th:[Z

    aput-boolean v1, v0, v2

    return-void
.end method

.method private setDarkMode(Z)V
    .locals 3

    const/16 v1, 0x10

    iget-object v0, p0, Lcom/android/settings/display/PageLayoutScreenView;->Tg:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v2

    if-eqz p1, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {v2, v0, v1}, Landroid/view/Window;->setExtraFlags(II)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public onSnapCancelled(Lmiui/widget/ScreenView;)V
    .locals 0

    return-void
.end method

.method public onSnapEnd(Lmiui/widget/ScreenView;)V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/display/PageLayoutScreenView;->Th:[Z

    invoke-virtual {p0}, Lcom/android/settings/display/PageLayoutScreenView;->getCurrentScreenIndex()I

    move-result v1

    aget-boolean v0, v0, v1

    invoke-direct {p0, v0}, Lcom/android/settings/display/PageLayoutScreenView;->setDarkMode(Z)V

    return-void
.end method

.method protected snapToScreen(IIZ)V
    .locals 0

    invoke-virtual {p0, p1, p2, p3, p0}, Lcom/android/settings/display/PageLayoutScreenView;->snapToScreen(IIZLmiui/widget/ScreenView$SnapScreenOnceNotification;)V

    return-void
.end method
