.class public Lcom/android/settings/display/NightDisplayPreference;
.super Landroid/preference/CheckBoxPreference;
.source "NightDisplayPreference.java"

# interfaces
.implements Lcom/android/internal/app/NightDisplayController$Callback;


# instance fields
.field private TM:Lcom/android/internal/app/NightDisplayController;

.field private TN:Ljava/text/DateFormat;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    invoke-direct {p0, p1, p2}, Landroid/preference/CheckBoxPreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    new-instance v0, Lcom/android/internal/app/NightDisplayController;

    invoke-direct {v0, p1}, Lcom/android/internal/app/NightDisplayController;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/settings/display/NightDisplayPreference;->TM:Lcom/android/internal/app/NightDisplayController;

    invoke-static {p1}, Landroid/text/format/DateFormat;->getTimeFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/display/NightDisplayPreference;->TN:Ljava/text/DateFormat;

    iget-object v0, p0, Lcom/android/settings/display/NightDisplayPreference;->TN:Ljava/text/DateFormat;

    const-string/jumbo v1, "UTC"

    invoke-static {v1}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/text/DateFormat;->setTimeZone(Ljava/util/TimeZone;)V

    return-void
.end method

.method private Lf(Ljava/time/LocalTime;)Ljava/lang/String;
    .locals 4

    const/4 v3, 0x0

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/display/NightDisplayPreference;->TN:Ljava/text/DateFormat;

    invoke-virtual {v1}, Ljava/text/DateFormat;->getTimeZone()Ljava/util/TimeZone;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/Calendar;->setTimeZone(Ljava/util/TimeZone;)V

    invoke-virtual {p1}, Ljava/time/LocalTime;->getHour()I

    move-result v1

    const/16 v2, 0xb

    invoke-virtual {v0, v2, v1}, Ljava/util/Calendar;->set(II)V

    invoke-virtual {p1}, Ljava/time/LocalTime;->getMinute()I

    move-result v1

    const/16 v2, 0xc

    invoke-virtual {v0, v2, v1}, Ljava/util/Calendar;->set(II)V

    const/16 v1, 0xd

    invoke-virtual {v0, v1, v3}, Ljava/util/Calendar;->set(II)V

    const/16 v1, 0xe

    invoke-virtual {v0, v1, v3}, Ljava/util/Calendar;->set(II)V

    iget-object v1, p0, Lcom/android/settings/display/NightDisplayPreference;->TN:Ljava/text/DateFormat;

    invoke-virtual {v0}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private updateSummary()V
    .locals 6

    const/4 v5, 0x1

    const/4 v4, 0x0

    invoke-virtual {p0}, Lcom/android/settings/display/NightDisplayPreference;->getContext()Landroid/content/Context;

    move-result-object v2

    iget-object v0, p0, Lcom/android/settings/display/NightDisplayPreference;->TM:Lcom/android/internal/app/NightDisplayController;

    invoke-virtual {v0}, Lcom/android/internal/app/NightDisplayController;->isActivated()Z

    move-result v3

    iget-object v0, p0, Lcom/android/settings/display/NightDisplayPreference;->TM:Lcom/android/internal/app/NightDisplayController;

    invoke-virtual {v0}, Lcom/android/internal/app/NightDisplayController;->getAutoMode()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :pswitch_0
    if-eqz v3, :cond_0

    const v0, 0x7f120b80

    :goto_0
    invoke-virtual {v2, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    :goto_1
    if-eqz v3, :cond_3

    const v0, 0x7f120b7e

    :goto_2
    new-array v3, v5, [Ljava/lang/Object;

    aput-object v1, v3, v4

    invoke-virtual {v2, v0, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/settings/display/NightDisplayPreference;->setSummary(Ljava/lang/CharSequence;)V

    return-void

    :cond_0
    const v0, 0x7f120b7c

    goto :goto_0

    :pswitch_1
    if-eqz v3, :cond_1

    new-array v0, v5, [Ljava/lang/Object;

    iget-object v1, p0, Lcom/android/settings/display/NightDisplayPreference;->TM:Lcom/android/internal/app/NightDisplayController;

    invoke-virtual {v1}, Lcom/android/internal/app/NightDisplayController;->getCustomEndTime()Ljava/time/LocalTime;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/android/settings/display/NightDisplayPreference;->Lf(Ljava/time/LocalTime;)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v4

    const v1, 0x7f120b7f

    invoke-virtual {v2, v1, v0}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    :goto_3
    move-object v1, v0

    goto :goto_1

    :cond_1
    new-array v0, v5, [Ljava/lang/Object;

    iget-object v1, p0, Lcom/android/settings/display/NightDisplayPreference;->TM:Lcom/android/internal/app/NightDisplayController;

    invoke-virtual {v1}, Lcom/android/internal/app/NightDisplayController;->getCustomStartTime()Ljava/time/LocalTime;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/android/settings/display/NightDisplayPreference;->Lf(Ljava/time/LocalTime;)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v4

    const v1, 0x7f120b7b

    invoke-virtual {v2, v1, v0}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_3

    :pswitch_2
    if-eqz v3, :cond_2

    const v0, 0x7f120b81

    :goto_4
    invoke-virtual {v2, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    goto :goto_1

    :cond_2
    const v0, 0x7f120b7d

    goto :goto_4

    :cond_3
    const v0, 0x7f120b7a

    goto :goto_2

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method


# virtual methods
.method public iM()V
    .locals 1

    invoke-super {p0}, Landroid/preference/CheckBoxPreference;->iM()V

    iget-object v0, p0, Lcom/android/settings/display/NightDisplayPreference;->TM:Lcom/android/internal/app/NightDisplayController;

    invoke-virtual {v0, p0}, Lcom/android/internal/app/NightDisplayController;->setListener(Lcom/android/internal/app/NightDisplayController$Callback;)V

    invoke-direct {p0}, Lcom/android/settings/display/NightDisplayPreference;->updateSummary()V

    return-void
.end method

.method public iN()V
    .locals 2

    invoke-super {p0}, Landroid/preference/CheckBoxPreference;->iN()V

    iget-object v0, p0, Lcom/android/settings/display/NightDisplayPreference;->TM:Lcom/android/internal/app/NightDisplayController;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/android/internal/app/NightDisplayController;->setListener(Lcom/android/internal/app/NightDisplayController$Callback;)V

    return-void
.end method

.method public onActivated(Z)V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/display/NightDisplayPreference;->updateSummary()V

    return-void
.end method

.method public onAutoModeChanged(I)V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/display/NightDisplayPreference;->updateSummary()V

    return-void
.end method

.method public onCustomEndTimeChanged(Ljava/time/LocalTime;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/display/NightDisplayPreference;->updateSummary()V

    return-void
.end method

.method public onCustomStartTimeChanged(Ljava/time/LocalTime;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/display/NightDisplayPreference;->updateSummary()V

    return-void
.end method
