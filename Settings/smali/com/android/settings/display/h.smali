.class Lcom/android/settings/display/h;
.super Ljava/lang/Object;
.source "PaperModeLocateService.java"

# interfaces
.implements Landroid/location/LocationListener;


# instance fields
.field final synthetic Ug:Lcom/android/settings/display/PaperModeLocateService;


# direct methods
.method private constructor <init>(Lcom/android/settings/display/PaperModeLocateService;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/display/h;->Ug:Lcom/android/settings/display/PaperModeLocateService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/settings/display/PaperModeLocateService;Lcom/android/settings/display/h;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/settings/display/h;-><init>(Lcom/android/settings/display/PaperModeLocateService;)V

    return-void
.end method


# virtual methods
.method public onLocationChanged(Landroid/location/Location;)V
    .locals 4

    const/4 v1, 0x0

    const-string/jumbo v2, "PaperModeLocateService"

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "requestLocationUpdates returns, location is null: "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    if-nez p1, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/android/settings/display/h;->Ug:Lcom/android/settings/display/PaperModeLocateService;

    invoke-static {v0, p1}, Lcom/android/settings/display/PaperModeLocateService;->Lu(Lcom/android/settings/display/PaperModeLocateService;Landroid/location/Location;)Landroid/location/Location;

    iget-object v0, p0, Lcom/android/settings/display/h;->Ug:Lcom/android/settings/display/PaperModeLocateService;

    invoke-static {v0}, Lcom/android/settings/display/PaperModeLocateService;->Lv(Lcom/android/settings/display/PaperModeLocateService;)V

    iget-object v0, p0, Lcom/android/settings/display/h;->Ug:Lcom/android/settings/display/PaperModeLocateService;

    iget-object v2, p0, Lcom/android/settings/display/h;->Ug:Lcom/android/settings/display/PaperModeLocateService;

    invoke-static {v2}, Lcom/android/settings/display/PaperModeLocateService;->Lt(Lcom/android/settings/display/PaperModeLocateService;)Landroid/app/job/JobParameters;

    move-result-object v2

    invoke-virtual {v0, v2, v1}, Lcom/android/settings/display/PaperModeLocateService;->jobFinished(Landroid/app/job/JobParameters;Z)V

    :cond_0
    return-void

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public onProviderDisabled(Ljava/lang/String;)V
    .locals 0

    return-void
.end method

.method public onProviderEnabled(Ljava/lang/String;)V
    .locals 0

    return-void
.end method

.method public onStatusChanged(Ljava/lang/String;ILandroid/os/Bundle;)V
    .locals 0

    return-void
.end method
