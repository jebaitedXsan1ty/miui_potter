.class Lcom/android/settings/display/ScreenColorBitMapView;
.super Landroid/view/View;
.source "ScreenColorBitMapView.java"


# instance fields
.field private Wf:Lcom/android/settings/display/r;

.field private Wg:Landroid/graphics/Bitmap;

.field private Wh:F

.field private Wi:Landroid/graphics/Paint;

.field private Wj:F

.field private Wk:F

.field private Wl:Landroid/graphics/Paint;

.field private Wm:F

.field private Wn:F

.field private Wo:F


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/android/settings/display/ScreenColorBitMapView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/android/settings/display/ScreenColorBitMapView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    invoke-direct {p0, p1}, Lcom/android/settings/display/ScreenColorBitMapView;->init(Landroid/content/Context;)V

    return-void
.end method

.method private ND(FF)F
    .locals 6

    const-wide/high16 v4, 0x4000000000000000L    # 2.0

    iget v0, p0, Lcom/android/settings/display/ScreenColorBitMapView;->Wm:F

    sub-float/2addr v0, p1

    float-to-double v0, v0

    invoke-static {v0, v1, v4, v5}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v0

    iget v2, p0, Lcom/android/settings/display/ScreenColorBitMapView;->Wn:F

    sub-float/2addr v2, p2

    float-to-double v2, v2

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v2

    add-double/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v0

    double-to-float v0, v0

    return v0
.end method

.method private NE(FF)Z
    .locals 2

    invoke-direct {p0, p1, p2}, Lcom/android/settings/display/ScreenColorBitMapView;->ND(FF)F

    move-result v0

    iget v1, p0, Lcom/android/settings/display/ScreenColorBitMapView;->Wo:F

    cmpg-float v0, v0, v1

    if-gtz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private NF(FF)V
    .locals 5

    const/4 v4, 0x0

    invoke-direct {p0, p1, p2}, Lcom/android/settings/display/ScreenColorBitMapView;->ND(FF)F

    move-result v0

    iget v1, p0, Lcom/android/settings/display/ScreenColorBitMapView;->Wm:F

    sub-float v1, p1, v1

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    iget v2, p0, Lcom/android/settings/display/ScreenColorBitMapView;->Wn:F

    sub-float v2, p2, v2

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    iget v3, p0, Lcom/android/settings/display/ScreenColorBitMapView;->Wo:F

    mul-float/2addr v1, v3

    div-float/2addr v1, v0

    iget v3, p0, Lcom/android/settings/display/ScreenColorBitMapView;->Wo:F

    mul-float/2addr v2, v3

    div-float v0, v2, v0

    iget v2, p0, Lcom/android/settings/display/ScreenColorBitMapView;->Wm:F

    sub-float v2, p1, v2

    cmpl-float v2, v2, v4

    if-lez v2, :cond_3

    iget v2, p0, Lcom/android/settings/display/ScreenColorBitMapView;->Wn:F

    sub-float v2, p2, v2

    cmpl-float v2, v2, v4

    if-lez v2, :cond_2

    iget v2, p0, Lcom/android/settings/display/ScreenColorBitMapView;->Wn:F

    add-float/2addr v0, v2

    iput v0, p0, Lcom/android/settings/display/ScreenColorBitMapView;->Wk:F

    :cond_0
    :goto_0
    iget v0, p0, Lcom/android/settings/display/ScreenColorBitMapView;->Wm:F

    add-float/2addr v0, v1

    iput v0, p0, Lcom/android/settings/display/ScreenColorBitMapView;->Wj:F

    :cond_1
    :goto_1
    return-void

    :cond_2
    iget v2, p0, Lcom/android/settings/display/ScreenColorBitMapView;->Wn:F

    sub-float v2, p2, v2

    cmpg-float v2, v2, v4

    if-gez v2, :cond_0

    iget v2, p0, Lcom/android/settings/display/ScreenColorBitMapView;->Wn:F

    sub-float v0, v2, v0

    iput v0, p0, Lcom/android/settings/display/ScreenColorBitMapView;->Wk:F

    goto :goto_0

    :cond_3
    iget v2, p0, Lcom/android/settings/display/ScreenColorBitMapView;->Wm:F

    sub-float v2, p1, v2

    cmpg-float v2, v2, v4

    if-gez v2, :cond_1

    iget v2, p0, Lcom/android/settings/display/ScreenColorBitMapView;->Wn:F

    sub-float v2, p2, v2

    cmpl-float v2, v2, v4

    if-lez v2, :cond_5

    iget v2, p0, Lcom/android/settings/display/ScreenColorBitMapView;->Wn:F

    add-float/2addr v0, v2

    iput v0, p0, Lcom/android/settings/display/ScreenColorBitMapView;->Wk:F

    :cond_4
    :goto_2
    iget v0, p0, Lcom/android/settings/display/ScreenColorBitMapView;->Wm:F

    sub-float/2addr v0, v1

    iput v0, p0, Lcom/android/settings/display/ScreenColorBitMapView;->Wj:F

    goto :goto_1

    :cond_5
    iget v2, p0, Lcom/android/settings/display/ScreenColorBitMapView;->Wn:F

    sub-float v2, p2, v2

    cmpg-float v2, v2, v4

    if-gez v2, :cond_4

    iget v2, p0, Lcom/android/settings/display/ScreenColorBitMapView;->Wn:F

    sub-float v0, v2, v0

    iput v0, p0, Lcom/android/settings/display/ScreenColorBitMapView;->Wk:F

    goto :goto_2
.end method

.method private NG()V
    .locals 3

    invoke-virtual {p0}, Lcom/android/settings/display/ScreenColorBitMapView;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string/jumbo v1, "circle_point"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string/jumbo v1, "last_circle_pointx"

    iget v2, p0, Lcom/android/settings/display/ScreenColorBitMapView;->Wj:F

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putFloat(Ljava/lang/String;F)Landroid/content/SharedPreferences$Editor;

    const-string/jumbo v1, "last_circle_pointy"

    iget v2, p0, Lcom/android/settings/display/ScreenColorBitMapView;->Wk:F

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putFloat(Ljava/lang/String;F)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    const-string/jumbo v0, "ScreenColorBitMapView"

    const-string/jumbo v1, "save_color_point"

    invoke-static {v0, v1}, Lcom/android/settings/E;->blF(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method private init(Landroid/content/Context;)V
    .locals 4

    const/high16 v3, 0x40000000    # 2.0f

    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/android/settings/display/ScreenColorBitMapView;->setLayerType(ILandroid/graphics/Paint;)V

    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/android/settings/display/ScreenColorBitMapView;->Wi:Landroid/graphics/Paint;

    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/android/settings/display/ScreenColorBitMapView;->Wl:Landroid/graphics/Paint;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0803bd

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/display/ScreenColorBitMapView;->Wg:Landroid/graphics/Bitmap;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0701f0

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    div-float v1, v0, v3

    iput v1, p0, Lcom/android/settings/display/ScreenColorBitMapView;->Wm:F

    div-float v1, v0, v3

    iput v1, p0, Lcom/android/settings/display/ScreenColorBitMapView;->Wn:F

    iget-object v1, p0, Lcom/android/settings/display/ScreenColorBitMapView;->Wg:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    int-to-float v1, v1

    div-float/2addr v1, v3

    const/high16 v2, 0x40400000    # 3.0f

    sub-float/2addr v1, v2

    iput v1, p0, Lcom/android/settings/display/ScreenColorBitMapView;->Wo:F

    iget-object v1, p0, Lcom/android/settings/display/ScreenColorBitMapView;->Wg:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    int-to-float v1, v1

    sub-float/2addr v0, v1

    div-float/2addr v0, v3

    iput v0, p0, Lcom/android/settings/display/ScreenColorBitMapView;->Wh:F

    const-string/jumbo v0, "circle_point"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string/jumbo v1, "last_circle_pointx"

    iget v2, p0, Lcom/android/settings/display/ScreenColorBitMapView;->Wm:F

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getFloat(Ljava/lang/String;F)F

    move-result v1

    iput v1, p0, Lcom/android/settings/display/ScreenColorBitMapView;->Wj:F

    const-string/jumbo v1, "last_circle_pointy"

    iget v2, p0, Lcom/android/settings/display/ScreenColorBitMapView;->Wn:F

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getFloat(Ljava/lang/String;F)F

    move-result v0

    iput v0, p0, Lcom/android/settings/display/ScreenColorBitMapView;->Wk:F

    return-void
.end method


# virtual methods
.method public NA(Lcom/android/settings/display/r;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/display/ScreenColorBitMapView;->Wf:Lcom/android/settings/display/r;

    return-void
.end method

.method public NB()F
    .locals 1

    iget v0, p0, Lcom/android/settings/display/ScreenColorBitMapView;->Wm:F

    return v0
.end method

.method public NC(FF)V
    .locals 0

    iput p1, p0, Lcom/android/settings/display/ScreenColorBitMapView;->Wj:F

    iput p2, p0, Lcom/android/settings/display/ScreenColorBitMapView;->Wk:F

    invoke-direct {p0}, Lcom/android/settings/display/ScreenColorBitMapView;->NG()V

    invoke-virtual {p0}, Lcom/android/settings/display/ScreenColorBitMapView;->postInvalidate()V

    return-void
.end method

.method public dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 5

    const/4 v4, 0x1

    invoke-virtual {p0}, Lcom/android/settings/display/ScreenColorBitMapView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    invoke-interface {v0, v4}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V

    invoke-virtual {p0}, Lcom/android/settings/display/ScreenColorBitMapView;->isEnabled()Z

    move-result v0

    if-nez v0, :cond_0

    return v4

    :cond_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    invoke-direct {p0, v0, v1}, Lcom/android/settings/display/ScreenColorBitMapView;->NE(FF)Z

    move-result v2

    if-nez v2, :cond_4

    invoke-direct {p0, v0, v1}, Lcom/android/settings/display/ScreenColorBitMapView;->NF(FF)V

    :goto_0
    iget-object v0, p0, Lcom/android/settings/display/ScreenColorBitMapView;->Wf:Lcom/android/settings/display/r;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/settings/display/ScreenColorBitMapView;->Wf:Lcom/android/settings/display/r;

    invoke-interface {v0}, Lcom/android/settings/display/r;->NH()V

    :cond_1
    iget-object v0, p0, Lcom/android/settings/display/ScreenColorBitMapView;->Wg:Landroid/graphics/Bitmap;

    iget v1, p0, Lcom/android/settings/display/ScreenColorBitMapView;->Wj:F

    iget v2, p0, Lcom/android/settings/display/ScreenColorBitMapView;->Wh:F

    sub-float/2addr v1, v2

    float-to-int v1, v1

    iget v2, p0, Lcom/android/settings/display/ScreenColorBitMapView;->Wk:F

    iget v3, p0, Lcom/android/settings/display/ScreenColorBitMapView;->Wh:F

    sub-float/2addr v2, v3

    float-to-int v2, v2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Bitmap;->getPixel(II)I

    move-result v0

    invoke-virtual {p0}, Lcom/android/settings/display/ScreenColorBitMapView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string/jumbo v2, "screen_color_level"

    invoke-static {v1, v2, v0}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    if-eq v0, v4, :cond_2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    const/4 v1, 0x3

    if-ne v0, v1, :cond_3

    :cond_2
    invoke-direct {p0}, Lcom/android/settings/display/ScreenColorBitMapView;->NG()V

    :cond_3
    invoke-virtual {p0}, Lcom/android/settings/display/ScreenColorBitMapView;->postInvalidate()V

    return v4

    :cond_4
    iput v0, p0, Lcom/android/settings/display/ScreenColorBitMapView;->Wj:F

    iput v1, p0, Lcom/android/settings/display/ScreenColorBitMapView;->Wk:F

    goto :goto_0
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 6

    const/4 v5, 0x0

    const/16 v4, 0x1e

    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    iget-object v0, p0, Lcom/android/settings/display/ScreenColorBitMapView;->Wi:Landroid/graphics/Paint;

    invoke-virtual {v0, v5}, Landroid/graphics/Paint;->setMaskFilter(Landroid/graphics/MaskFilter;)Landroid/graphics/MaskFilter;

    iget-object v0, p0, Lcom/android/settings/display/ScreenColorBitMapView;->Wg:Landroid/graphics/Bitmap;

    new-instance v1, Landroid/graphics/Rect;

    invoke-virtual {p0}, Lcom/android/settings/display/ScreenColorBitMapView;->getWidth()I

    move-result v2

    add-int/lit8 v2, v2, -0x1e

    invoke-virtual {p0}, Lcom/android/settings/display/ScreenColorBitMapView;->getHeight()I

    move-result v3

    add-int/lit8 v3, v3, -0x1e

    invoke-direct {v1, v4, v4, v2, v3}, Landroid/graphics/Rect;-><init>(IIII)V

    iget-object v2, p0, Lcom/android/settings/display/ScreenColorBitMapView;->Wi:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v5, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    iget-object v0, p0, Lcom/android/settings/display/ScreenColorBitMapView;->Wl:Landroid/graphics/Paint;

    const v1, -0xff0001

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    iget-object v0, p0, Lcom/android/settings/display/ScreenColorBitMapView;->Wl:Landroid/graphics/Paint;

    const/high16 v1, 0x40000000    # 2.0f

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    iget-object v0, p0, Lcom/android/settings/display/ScreenColorBitMapView;->Wl:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    iget v0, p0, Lcom/android/settings/display/ScreenColorBitMapView;->Wj:F

    iget v1, p0, Lcom/android/settings/display/ScreenColorBitMapView;->Wk:F

    const/high16 v2, 0x41f00000    # 30.0f

    iget-object v3, p0, Lcom/android/settings/display/ScreenColorBitMapView;->Wl:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    iget-object v0, p0, Lcom/android/settings/display/ScreenColorBitMapView;->Wl:Landroid/graphics/Paint;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    iget-object v0, p0, Lcom/android/settings/display/ScreenColorBitMapView;->Wl:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->FILL_AND_STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    iget v0, p0, Lcom/android/settings/display/ScreenColorBitMapView;->Wj:F

    iget v1, p0, Lcom/android/settings/display/ScreenColorBitMapView;->Wk:F

    const/high16 v2, 0x41e00000    # 28.0f

    iget-object v3, p0, Lcom/android/settings/display/ScreenColorBitMapView;->Wl:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    return-void
.end method
