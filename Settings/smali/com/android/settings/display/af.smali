.class final Lcom/android/settings/display/af;
.super Ljava/lang/Object;
.source "PageLayoutFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic Yt:Lcom/android/settings/display/PageLayoutFragment;


# direct methods
.method constructor <init>(Lcom/android/settings/display/PageLayoutFragment;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/display/af;->Yt:Lcom/android/settings/display/PageLayoutFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :goto_0
    :pswitch_0
    return-void

    :pswitch_1
    invoke-static {}, Lcom/android/settings/display/j;->LB()I

    move-result v0

    iget-object v1, p0, Lcom/android/settings/display/af;->Yt:Lcom/android/settings/display/PageLayoutFragment;

    invoke-static {v1}, Lcom/android/settings/display/PageLayoutFragment;->Op(Lcom/android/settings/display/PageLayoutFragment;)I

    move-result v1

    if-eq v0, v1, :cond_2

    iget-object v0, p0, Lcom/android/settings/display/af;->Yt:Lcom/android/settings/display/PageLayoutFragment;

    invoke-static {v0}, Lcom/android/settings/display/PageLayoutFragment;->Op(Lcom/android/settings/display/PageLayoutFragment;)I

    move-result v0

    invoke-static {v0}, Lcom/android/settings/display/j;->LD(I)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Lcom/android/settings/display/PageLayoutFragment$ConfirmDialog;

    invoke-direct {v0}, Lcom/android/settings/display/PageLayoutFragment$ConfirmDialog;-><init>()V

    iget-object v1, p0, Lcom/android/settings/display/af;->Yt:Lcom/android/settings/display/PageLayoutFragment;

    invoke-static {v1}, Lcom/android/settings/display/PageLayoutFragment;->Op(Lcom/android/settings/display/PageLayoutFragment;)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/android/settings/display/PageLayoutFragment$ConfirmDialog;->Ot(I)V

    iget-object v1, p0, Lcom/android/settings/display/af;->Yt:Lcom/android/settings/display/PageLayoutFragment;

    invoke-virtual {v1}, Lcom/android/settings/display/PageLayoutFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    invoke-static {}, Lcom/android/settings/display/PageLayoutFragment$ConfirmDialog;->-get0()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/android/settings/display/PageLayoutFragment$ConfirmDialog;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/android/settings/display/af;->Yt:Lcom/android/settings/display/PageLayoutFragment;

    invoke-static {v0}, Lcom/android/settings/display/PageLayoutFragment;->Oq(Lcom/android/settings/display/PageLayoutFragment;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/settings/display/af;->Yt:Lcom/android/settings/display/PageLayoutFragment;

    invoke-virtual {v0}, Lcom/android/settings/display/PageLayoutFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/display/af;->Yt:Lcom/android/settings/display/PageLayoutFragment;

    invoke-static {v1}, Lcom/android/settings/display/PageLayoutFragment;->Op(Lcom/android/settings/display/PageLayoutFragment;)I

    move-result v1

    invoke-static {v0, v1}, Lcom/android/settings/display/PageLayoutFragment;->Os(Landroid/app/Activity;I)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/android/settings/display/af;->Yt:Lcom/android/settings/display/PageLayoutFragment;

    invoke-virtual {v0}, Lcom/android/settings/display/PageLayoutFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/display/af;->Yt:Lcom/android/settings/display/PageLayoutFragment;

    invoke-virtual {v1}, Lcom/android/settings/display/PageLayoutFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->onBackPressed()V

    iget-object v1, p0, Lcom/android/settings/display/af;->Yt:Lcom/android/settings/display/PageLayoutFragment;

    invoke-static {v1}, Lcom/android/settings/display/PageLayoutFragment;->Op(Lcom/android/settings/display/PageLayoutFragment;)I

    move-result v1

    invoke-static {v0, v1}, Lcom/android/settings/display/j;->LA(Landroid/content/Context;I)Z

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/android/settings/display/af;->Yt:Lcom/android/settings/display/PageLayoutFragment;

    invoke-static {v0}, Lcom/android/settings/display/PageLayoutFragment;->Oq(Lcom/android/settings/display/PageLayoutFragment;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/android/settings/display/af;->Yt:Lcom/android/settings/display/PageLayoutFragment;

    invoke-virtual {v0}, Lcom/android/settings/display/PageLayoutFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/display/af;->Yt:Lcom/android/settings/display/PageLayoutFragment;

    invoke-static {v1}, Lcom/android/settings/display/PageLayoutFragment;->Op(Lcom/android/settings/display/PageLayoutFragment;)I

    move-result v1

    invoke-static {v0, v1}, Lcom/android/settings/display/PageLayoutFragment;->Os(Landroid/app/Activity;I)V

    goto/16 :goto_0

    :cond_3
    iget-object v0, p0, Lcom/android/settings/display/af;->Yt:Lcom/android/settings/display/PageLayoutFragment;

    invoke-virtual {v0}, Lcom/android/settings/display/PageLayoutFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->onBackPressed()V

    goto/16 :goto_0

    :pswitch_2
    iget-object v0, p0, Lcom/android/settings/display/af;->Yt:Lcom/android/settings/display/PageLayoutFragment;

    invoke-virtual {v0}, Lcom/android/settings/display/PageLayoutFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->onBackPressed()V

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x7f0a00a8
        :pswitch_2
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method
