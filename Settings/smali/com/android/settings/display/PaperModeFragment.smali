.class public Lcom/android/settings/display/PaperModeFragment;
.super Lcom/android/settings/MiuiSettingsPreferenceFragment;
.source "PaperModeFragment.java"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceChangeListener;
.implements Landroid/preference/Preference$OnPreferenceClickListener;


# static fields
.field private static UH:Ljava/lang/String;

.field private static UI:Ljava/lang/Boolean;

.field private static final Up:F

.field private static final Uq:F

.field private static Uu:I

.field private static Uz:I


# instance fields
.field private UA:Z

.field private UB:Lmiui/app/TimePickerDialog;

.field private UC:I

.field private UD:Landroid/preference/CheckBoxPreference;

.field private UE:Lcom/android/settings/dndmode/LabelPreference;

.field private UF:Lcom/android/settings/dndmode/LabelPreference;

.field private UG:Landroid/preference/CheckBoxPreference;

.field Ur:Landroid/content/DialogInterface$OnClickListener;

.field private Us:Lcom/android/settings/display/PaperModePreference;

.field private Ut:Lcom/android/settings/display/PaperModePreference;

.field Uv:Landroid/app/AlertDialog;

.field private Uw:Lmiui/app/TimePickerDialog$OnTimeSetListener;

.field private Ux:Landroid/database/ContentObserver;

.field private Uy:Landroid/database/ContentObserver;

.field private mContext:Landroid/content/Context;


# direct methods
.method static synthetic -get0()F
    .locals 1

    sget v0, Lcom/android/settings/display/PaperModeFragment;->Uq:F

    return v0
.end method

.method static constructor <clinit>()V
    .locals 2

    sget v0, Landroid/provider/MiuiSettings$ScreenEffect;->PAPER_MODE_MAX_LEVEL:I

    int-to-float v0, v0

    sput v0, Lcom/android/settings/display/PaperModeFragment;->Up:F

    sget v0, Lcom/android/settings/display/PaperModeFragment;->Up:F

    const/high16 v1, 0x3f800000    # 1.0f

    sub-float/2addr v0, v1

    const/high16 v1, 0x447a0000    # 1000.0f

    div-float/2addr v0, v1

    sput v0, Lcom/android/settings/display/PaperModeFragment;->Uq:F

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/android/settings/MiuiSettingsPreferenceFragment;-><init>()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/settings/display/PaperModeFragment;->UA:Z

    new-instance v0, Lcom/android/settings/display/H;

    invoke-direct {v0, p0}, Lcom/android/settings/display/H;-><init>(Lcom/android/settings/display/PaperModeFragment;)V

    iput-object v0, p0, Lcom/android/settings/display/PaperModeFragment;->Uw:Lmiui/app/TimePickerDialog$OnTimeSetListener;

    new-instance v0, Lcom/android/settings/display/I;

    invoke-direct {v0, p0}, Lcom/android/settings/display/I;-><init>(Lcom/android/settings/display/PaperModeFragment;)V

    iput-object v0, p0, Lcom/android/settings/display/PaperModeFragment;->Ur:Landroid/content/DialogInterface$OnClickListener;

    return-void
.end method

.method public static LT(Landroid/content/Context;)Z
    .locals 3

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v1, "screen_paper_mode_enabled"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/provider/MiuiSettings$System;->getBoolean(Landroid/content/ContentResolver;Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method private LU(II)Ljava/lang/String;
    .locals 3

    const/16 v2, 0xc

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    const/16 v1, 0xb

    invoke-virtual {v0, v1, p1}, Ljava/util/Calendar;->set(II)V

    invoke-virtual {v0, v2, p2}, Ljava/util/Calendar;->set(II)V

    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v0

    invoke-static {v0, v1, v2}, Lmiui/date/DateUtils;->formatDateTime(JI)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private LV()I
    .locals 3

    iget-object v0, p0, Lcom/android/settings/display/PaperModeFragment;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v1, "screen_paper_mode_level"

    sget v2, Landroid/provider/MiuiSettings$ScreenEffect;->DEFAULT_PAPER_MODE_LEVEL:I

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method private LW()I
    .locals 3

    iget-object v0, p0, Lcom/android/settings/display/PaperModeFragment;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v1, "paper_mode_scheduler_type"

    const/4 v2, 0x2

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public static LY(Landroid/content/Context;)Z
    .locals 3

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v1, "screen_paper_mode_time_enabled"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/provider/MiuiSettings$System;->getBoolean(Landroid/content/ContentResolver;Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method private static LZ()Z
    .locals 4

    const/4 v2, 0x0

    sget-object v0, Lcom/android/settings/display/PaperModeFragment;->UI:Ljava/lang/Boolean;

    if-nez v0, :cond_1

    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/android/settings/display/PaperModeFragment;->UI:Ljava/lang/Boolean;

    :try_start_0
    new-instance v1, Ljava/io/BufferedReader;

    new-instance v0, Ljava/io/FileReader;

    const-string/jumbo v3, "/sys/devices/soc0/soc_id"

    invoke-direct {v0, v3}, Ljava/io/FileReader;-><init>(Ljava/lang/String;)V

    invoke-direct {v1, v0}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    invoke-virtual {v1}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/settings/display/PaperModeFragment;->UH:Ljava/lang/String;

    sget-object v0, Lcom/android/settings/display/PaperModeFragment;->UH:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/android/settings/display/PaperModeFragment;->UH:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v2, "321"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/android/settings/display/PaperModeFragment;->UI:Ljava/lang/Boolean;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :cond_0
    if-eqz v1, :cond_1

    :try_start_2
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    :cond_1
    :goto_0
    sget-object v0, Lcom/android/settings/display/PaperModeFragment;->UI:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0

    :catch_0
    move-exception v0

    const-string/jumbo v1, "PaperModeFragment"

    const-string/jumbo v2, "IOException"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    :catch_1
    move-exception v0

    move-object v1, v2

    :goto_1
    :try_start_3
    const-string/jumbo v2, "PaperModeFragment"

    const-string/jumbo v3, "IOException"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    if-eqz v1, :cond_1

    :try_start_4
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    goto :goto_0

    :catch_2
    move-exception v0

    const-string/jumbo v1, "PaperModeFragment"

    const-string/jumbo v2, "IOException"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    :catchall_0
    move-exception v0

    move-object v1, v2

    :goto_2
    if-eqz v1, :cond_2

    :try_start_5
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3

    :cond_2
    :goto_3
    throw v0

    :catch_3
    move-exception v1

    const-string/jumbo v2, "PaperModeFragment"

    const-string/jumbo v3, "IOException"

    invoke-static {v2, v3, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_3

    :catchall_1
    move-exception v0

    goto :goto_2

    :catch_4
    move-exception v0

    goto :goto_1
.end method

.method private Mb(Z)V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/display/PaperModeFragment;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v1, "screen_paper_mode_enabled"

    invoke-static {v0, v1, p1}, Landroid/provider/MiuiSettings$System;->putBoolean(Landroid/content/ContentResolver;Ljava/lang/String;Z)Z

    return-void
.end method

.method private Mc(I)V
    .locals 2

    invoke-direct {p0}, Lcom/android/settings/display/PaperModeFragment;->LV()I

    move-result v0

    if-eq p1, v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/display/PaperModeFragment;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v1, "screen_paper_mode_level"

    invoke-static {v0, v1, p1}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    :cond_0
    return-void
.end method

.method private Md(I)V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/display/PaperModeFragment;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v1, "paper_mode_scheduler_type"

    invoke-static {v0, v1, p1}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    return-void
.end method

.method private Me(Z)V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/display/PaperModeFragment;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v1, "screen_paper_mode_time_enabled"

    invoke-static {v0, v1, p1}, Landroid/provider/MiuiSettings$System;->putBoolean(Landroid/content/ContentResolver;Ljava/lang/String;Z)Z

    return-void
.end method

.method static synthetic Mf(Lcom/android/settings/display/PaperModeFragment;)Landroid/content/Context;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/display/PaperModeFragment;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic Mg()I
    .locals 1

    sget v0, Lcom/android/settings/display/PaperModeFragment;->Uu:I

    return v0
.end method

.method static synthetic Mh()I
    .locals 1

    sget v0, Lcom/android/settings/display/PaperModeFragment;->Uz:I

    return v0
.end method

.method static synthetic Mi(Lcom/android/settings/display/PaperModeFragment;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/settings/display/PaperModeFragment;->UA:Z

    return v0
.end method

.method static synthetic Mj(Lcom/android/settings/display/PaperModeFragment;)Landroid/preference/CheckBoxPreference;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/display/PaperModeFragment;->UD:Landroid/preference/CheckBoxPreference;

    return-object v0
.end method

.method static synthetic Mk(Lcom/android/settings/display/PaperModeFragment;)Lcom/android/settings/dndmode/LabelPreference;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/display/PaperModeFragment;->UE:Lcom/android/settings/dndmode/LabelPreference;

    return-object v0
.end method

.method static synthetic Ml(Lcom/android/settings/display/PaperModeFragment;)Lcom/android/settings/dndmode/LabelPreference;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/display/PaperModeFragment;->UF:Lcom/android/settings/dndmode/LabelPreference;

    return-object v0
.end method

.method static synthetic Mm(I)I
    .locals 0

    sput p0, Lcom/android/settings/display/PaperModeFragment;->Uu:I

    return p0
.end method

.method static synthetic Mn(I)I
    .locals 0

    sput p0, Lcom/android/settings/display/PaperModeFragment;->Uz:I

    return p0
.end method

.method static synthetic Mo()Z
    .locals 1

    invoke-static {}, Lcom/android/settings/display/PaperModeFragment;->LZ()Z

    move-result v0

    return v0
.end method

.method static synthetic Mp(Lcom/android/settings/display/PaperModeFragment;)I
    .locals 1

    invoke-direct {p0}, Lcom/android/settings/display/PaperModeFragment;->LW()I

    move-result v0

    return v0
.end method

.method static synthetic Mq(Lcom/android/settings/display/PaperModeFragment;II)Ljava/lang/String;
    .locals 1

    invoke-direct {p0, p1, p2}, Lcom/android/settings/display/PaperModeFragment;->LU(II)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public LX()V
    .locals 3

    iget-object v0, p0, Lcom/android/settings/display/PaperModeFragment;->Uv:Landroid/app/AlertDialog;

    if-nez v0, :cond_0

    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Lcom/android/settings/display/PaperModeFragment;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v1, 0x7f120c75

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f120c72

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/display/PaperModeFragment;->Ur:Landroid/content/DialogInterface$OnClickListener;

    const v2, 0x7f120c74

    invoke-virtual {v0, v2, v1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/display/PaperModeFragment;->Ur:Landroid/content/DialogInterface$OnClickListener;

    const v2, 0x7f120c73

    invoke-virtual {v0, v2, v1}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/display/PaperModeFragment;->Uv:Landroid/app/AlertDialog;

    :cond_0
    return-void
.end method

.method public Ma()V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/display/PaperModeFragment;->mContext:Landroid/content/Context;

    const-string/jumbo v1, "jobscheduler"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/job/JobScheduler;

    const v1, 0xabe9

    invoke-virtual {v0, v1}, Landroid/app/job/JobScheduler;->cancel(I)V

    return-void
.end method

.method public cC()Ljava/lang/String;
    .locals 1

    const-class v0, Lcom/android/settings/display/PaperModeFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 7

    const/4 v1, 0x1

    const/4 v6, 0x0

    invoke-super {p0, p1}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->onCreate(Landroid/os/Bundle;)V

    const v0, 0x7f15009d

    invoke-virtual {p0, v0}, Lcom/android/settings/display/PaperModeFragment;->addPreferencesFromResource(I)V

    invoke-virtual {p0}, Lcom/android/settings/display/PaperModeFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/display/PaperModeFragment;->mContext:Landroid/content/Context;

    const-string/jumbo v0, "paper_mode_enable"

    invoke-virtual {p0, v0}, Lcom/android/settings/display/PaperModeFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    iput-object v0, p0, Lcom/android/settings/display/PaperModeFragment;->UD:Landroid/preference/CheckBoxPreference;

    iget-object v0, p0, Lcom/android/settings/display/PaperModeFragment;->UD:Landroid/preference/CheckBoxPreference;

    iget-object v2, p0, Lcom/android/settings/display/PaperModeFragment;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/android/settings/display/PaperModeFragment;->LT(Landroid/content/Context;)Z

    move-result v2

    invoke-virtual {v0, v2}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    iget-object v0, p0, Lcom/android/settings/display/PaperModeFragment;->UD:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, p0}, Landroid/preference/CheckBoxPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    const-string/jumbo v0, "paper_mode_time_enable"

    invoke-virtual {p0, v0}, Lcom/android/settings/display/PaperModeFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    iput-object v0, p0, Lcom/android/settings/display/PaperModeFragment;->UG:Landroid/preference/CheckBoxPreference;

    iget-object v0, p0, Lcom/android/settings/display/PaperModeFragment;->UG:Landroid/preference/CheckBoxPreference;

    iget-object v2, p0, Lcom/android/settings/display/PaperModeFragment;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/android/settings/display/PaperModeFragment;->LY(Landroid/content/Context;)Z

    move-result v2

    invoke-virtual {v0, v2}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    iget-object v0, p0, Lcom/android/settings/display/PaperModeFragment;->UG:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, p0}, Landroid/preference/CheckBoxPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    const-string/jumbo v0, "paper_mode_auto_twilight"

    invoke-virtual {p0, v0}, Lcom/android/settings/display/PaperModeFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Lcom/android/settings/display/PaperModePreference;

    iput-object v0, p0, Lcom/android/settings/display/PaperModeFragment;->Us:Lcom/android/settings/display/PaperModePreference;

    iget-object v2, p0, Lcom/android/settings/display/PaperModeFragment;->Us:Lcom/android/settings/display/PaperModePreference;

    invoke-direct {p0}, Lcom/android/settings/display/PaperModeFragment;->LW()I

    move-result v0

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {v2, v0}, Lcom/android/settings/display/PaperModePreference;->setChecked(Z)V

    iget-object v0, p0, Lcom/android/settings/display/PaperModeFragment;->Us:Lcom/android/settings/display/PaperModePreference;

    invoke-virtual {v0, p0}, Lcom/android/settings/display/PaperModePreference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    const-string/jumbo v0, "paper_mode_customize_time"

    invoke-virtual {p0, v0}, Lcom/android/settings/display/PaperModeFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Lcom/android/settings/display/PaperModePreference;

    iput-object v0, p0, Lcom/android/settings/display/PaperModeFragment;->Ut:Lcom/android/settings/display/PaperModePreference;

    iget-object v2, p0, Lcom/android/settings/display/PaperModeFragment;->Ut:Lcom/android/settings/display/PaperModePreference;

    invoke-direct {p0}, Lcom/android/settings/display/PaperModeFragment;->LW()I

    move-result v0

    const/4 v3, 0x2

    if-ne v0, v3, :cond_1

    move v0, v1

    :goto_1
    invoke-virtual {v2, v0}, Lcom/android/settings/display/PaperModePreference;->setChecked(Z)V

    iget-object v0, p0, Lcom/android/settings/display/PaperModeFragment;->Ut:Lcom/android/settings/display/PaperModePreference;

    invoke-virtual {v0, p0}, Lcom/android/settings/display/PaperModePreference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeZone()Ljava/util/TimeZone;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/TimeZone;->getRawOffset()I

    move-result v0

    iput v0, p0, Lcom/android/settings/display/PaperModeFragment;->UC:I

    iget-object v0, p0, Lcom/android/settings/display/PaperModeFragment;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/settings/display/k;->LI(Landroid/content/Context;)I

    move-result v0

    sput v0, Lcom/android/settings/display/PaperModeFragment;->Uz:I

    iget-object v0, p0, Lcom/android/settings/display/PaperModeFragment;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/settings/display/k;->LG(Landroid/content/Context;)I

    move-result v0

    sput v0, Lcom/android/settings/display/PaperModeFragment;->Uu:I

    const-string/jumbo v0, "paper_mode_start_time"

    invoke-virtual {p0, v0}, Lcom/android/settings/display/PaperModeFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Lcom/android/settings/dndmode/LabelPreference;

    iput-object v0, p0, Lcom/android/settings/display/PaperModeFragment;->UF:Lcom/android/settings/dndmode/LabelPreference;

    const-string/jumbo v0, "paper_mode_end_time"

    invoke-virtual {p0, v0}, Lcom/android/settings/display/PaperModeFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Lcom/android/settings/dndmode/LabelPreference;

    iput-object v0, p0, Lcom/android/settings/display/PaperModeFragment;->UE:Lcom/android/settings/dndmode/LabelPreference;

    iget-object v0, p0, Lcom/android/settings/display/PaperModeFragment;->UF:Lcom/android/settings/dndmode/LabelPreference;

    sget v2, Lcom/android/settings/display/PaperModeFragment;->Uz:I

    div-int/lit8 v2, v2, 0x3c

    sget v3, Lcom/android/settings/display/PaperModeFragment;->Uz:I

    rem-int/lit8 v3, v3, 0x3c

    invoke-direct {p0, v2, v3}, Lcom/android/settings/display/PaperModeFragment;->LU(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/android/settings/dndmode/LabelPreference;->mp(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/settings/display/PaperModeFragment;->UE:Lcom/android/settings/dndmode/LabelPreference;

    sget v2, Lcom/android/settings/display/PaperModeFragment;->Uu:I

    div-int/lit8 v2, v2, 0x3c

    sget v3, Lcom/android/settings/display/PaperModeFragment;->Uu:I

    rem-int/lit8 v3, v3, 0x3c

    invoke-direct {p0, v2, v3}, Lcom/android/settings/display/PaperModeFragment;->LU(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/android/settings/dndmode/LabelPreference;->mp(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/settings/display/PaperModeFragment;->UF:Lcom/android/settings/dndmode/LabelPreference;

    invoke-virtual {v0, p0}, Lcom/android/settings/dndmode/LabelPreference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    iget-object v0, p0, Lcom/android/settings/display/PaperModeFragment;->UE:Lcom/android/settings/dndmode/LabelPreference;

    invoke-virtual {v0, p0}, Lcom/android/settings/dndmode/LabelPreference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    const-string/jumbo v0, "paper_mode_adjust_level"

    invoke-virtual {p0, v0}, Lcom/android/settings/display/PaperModeFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Lcom/android/settings/MiuiSeekBarPreference;

    const/16 v2, 0x3e8

    invoke-virtual {v0, v2}, Lcom/android/settings/MiuiSeekBarPreference;->bzk(I)V

    invoke-direct {p0}, Lcom/android/settings/display/PaperModeFragment;->LV()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    int-to-float v2, v2

    sget v3, Lcom/android/settings/display/PaperModeFragment;->Uq:F

    div-float/2addr v2, v3

    float-to-int v2, v2

    invoke-virtual {v0, v2}, Lcom/android/settings/MiuiSeekBarPreference;->setProgress(I)V

    invoke-virtual {v0, p0}, Lcom/android/settings/MiuiSeekBarPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    invoke-virtual {v0, v1}, Lcom/android/settings/MiuiSeekBarPreference;->bzl(Z)V

    new-instance v1, Lcom/android/settings/display/J;

    invoke-direct {v1, p0, v0}, Lcom/android/settings/display/J;-><init>(Lcom/android/settings/display/PaperModeFragment;Lcom/android/settings/MiuiSeekBarPreference;)V

    invoke-virtual {v0, v1}, Lcom/android/settings/MiuiSeekBarPreference;->bzn(Lcom/android/settings/ba;)V

    new-instance v0, Lmiui/app/TimePickerDialog;

    iget-object v1, p0, Lcom/android/settings/display/PaperModeFragment;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/android/settings/display/PaperModeFragment;->Uw:Lmiui/app/TimePickerDialog$OnTimeSetListener;

    sget v3, Lcom/android/settings/display/PaperModeFragment;->Uz:I

    div-int/lit8 v3, v3, 0x3c

    sget v4, Lcom/android/settings/display/PaperModeFragment;->Uz:I

    rem-int/lit8 v4, v4, 0x3c

    iget-object v5, p0, Lcom/android/settings/display/PaperModeFragment;->mContext:Landroid/content/Context;

    invoke-static {v5}, Landroid/text/format/DateFormat;->is24HourFormat(Landroid/content/Context;)Z

    move-result v5

    invoke-direct/range {v0 .. v5}, Lmiui/app/TimePickerDialog;-><init>(Landroid/content/Context;Lmiui/app/TimePickerDialog$OnTimeSetListener;IIZ)V

    iput-object v0, p0, Lcom/android/settings/display/PaperModeFragment;->UB:Lmiui/app/TimePickerDialog;

    new-instance v0, Lcom/android/settings/display/K;

    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    invoke-direct {v0, p0, v1}, Lcom/android/settings/display/K;-><init>(Lcom/android/settings/display/PaperModeFragment;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/android/settings/display/PaperModeFragment;->Ux:Landroid/database/ContentObserver;

    new-instance v0, Lcom/android/settings/display/L;

    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    invoke-direct {v0, p0, v1}, Lcom/android/settings/display/L;-><init>(Lcom/android/settings/display/PaperModeFragment;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/android/settings/display/PaperModeFragment;->Uy:Landroid/database/ContentObserver;

    iget-object v0, p0, Lcom/android/settings/display/PaperModeFragment;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v1, "paper_mode_scheduler_type"

    invoke-static {v1}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iget-object v2, p0, Lcom/android/settings/display/PaperModeFragment;->Uy:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1, v6, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    iget-object v0, p0, Lcom/android/settings/display/PaperModeFragment;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v1, "screen_paper_mode_enabled"

    invoke-static {v1}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iget-object v2, p0, Lcom/android/settings/display/PaperModeFragment;->Ux:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1, v6, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    return-void

    :cond_0
    move v0, v6

    goto/16 :goto_0

    :cond_1
    move v0, v6

    goto/16 :goto_1
.end method

.method public onDestroy()V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/display/PaperModeFragment;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/display/PaperModeFragment;->Ux:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    iget-object v1, p0, Lcom/android/settings/display/PaperModeFragment;->Uy:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    invoke-super {p0}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->onDestroy()V

    return-void
.end method

.method public onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 3

    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "paper_mode_enable"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/android/settings/display/PaperModeFragment;->UD:Landroid/preference/CheckBoxPreference;

    move-object v0, p2

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-virtual {v1, v0}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/android/settings/display/PaperModeFragment;->Mb(Z)V

    :cond_0
    :goto_0
    const/4 v0, 0x1

    return v0

    :cond_1
    const-string/jumbo v1, "paper_mode_time_enable"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    move-object v0, p2

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/android/settings/display/PaperModeFragment;->Me(Z)V

    iget-object v1, p0, Lcom/android/settings/display/PaperModeFragment;->UG:Landroid/preference/CheckBoxPreference;

    move-object v0, p2

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-virtual {v1, v0}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    const-string/jumbo v1, "ScreenEffect"

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "PapermodeTimeControl"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object v0, p2

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_2

    const-string/jumbo v0, "on"

    :goto_1
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/android/settings/E;->blF(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/android/settings/display/PaperModeFragment;->mContext:Landroid/content/Context;

    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-direct {p0}, Lcom/android/settings/display/PaperModeFragment;->LW()I

    move-result v0

    :goto_2
    invoke-static {v1, v0}, Lcom/android/settings/display/k;->LS(Landroid/content/Context;I)V

    goto :goto_0

    :cond_2
    const-string/jumbo v0, "off"

    goto :goto_1

    :cond_3
    const/4 v0, 0x0

    goto :goto_2

    :cond_4
    const-string/jumbo v1, "paper_mode_adjust_level"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v0

    int-to-float v0, v0

    sget v1, Lcom/android/settings/display/PaperModeFragment;->Uq:F

    mul-float/2addr v0, v1

    const/high16 v1, 0x3f800000    # 1.0f

    add-float/2addr v0, v1

    float-to-int v0, v0

    invoke-direct {p0, v0}, Lcom/android/settings/display/PaperModeFragment;->Mc(I)V

    goto :goto_0
.end method

.method public onPreferenceClick(Landroid/preference/Preference;)Z
    .locals 4

    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "paper_mode_auto_twilight"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v0, p0, Lcom/android/settings/display/PaperModeFragment;->mContext:Landroid/content/Context;

    const-string/jumbo v1, "location"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/location/LocationManager;

    iget-object v0, p0, Lcom/android/settings/display/PaperModeFragment;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v1, "location_mode"

    invoke-static {v0, v1, v3}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/android/settings/display/PaperModeFragment;->LX()V

    iget-object v0, p0, Lcom/android/settings/display/PaperModeFragment;->Uv:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    :cond_0
    invoke-direct {p0, v2}, Lcom/android/settings/display/PaperModeFragment;->Md(I)V

    iget-object v0, p0, Lcom/android/settings/display/PaperModeFragment;->Us:Lcom/android/settings/display/PaperModePreference;

    invoke-virtual {v0, v2}, Lcom/android/settings/display/PaperModePreference;->setChecked(Z)V

    iget-object v0, p0, Lcom/android/settings/display/PaperModeFragment;->Ut:Lcom/android/settings/display/PaperModePreference;

    invoke-virtual {v0, v3}, Lcom/android/settings/display/PaperModePreference;->setChecked(Z)V

    :cond_1
    :goto_0
    return v3

    :cond_2
    const-string/jumbo v1, "paper_mode_customize_time"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/android/settings/display/PaperModeFragment;->Md(I)V

    iget-object v0, p0, Lcom/android/settings/display/PaperModeFragment;->Us:Lcom/android/settings/display/PaperModePreference;

    invoke-virtual {v0, v3}, Lcom/android/settings/display/PaperModePreference;->setChecked(Z)V

    iget-object v0, p0, Lcom/android/settings/display/PaperModeFragment;->Ut:Lcom/android/settings/display/PaperModePreference;

    invoke-virtual {v0, v2}, Lcom/android/settings/display/PaperModePreference;->setChecked(Z)V

    goto :goto_0

    :cond_3
    const-string/jumbo v1, "paper_mode_start_time"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    iput-boolean v3, p0, Lcom/android/settings/display/PaperModeFragment;->UA:Z

    sget v0, Lcom/android/settings/display/PaperModeFragment;->Uz:I

    if-lez v0, :cond_4

    iget-object v0, p0, Lcom/android/settings/display/PaperModeFragment;->UB:Lmiui/app/TimePickerDialog;

    sget v1, Lcom/android/settings/display/PaperModeFragment;->Uz:I

    div-int/lit8 v1, v1, 0x3c

    sget v2, Lcom/android/settings/display/PaperModeFragment;->Uz:I

    rem-int/lit8 v2, v2, 0x3c

    invoke-virtual {v0, v1, v2}, Lmiui/app/TimePickerDialog;->updateTime(II)V

    :goto_1
    iget-object v0, p0, Lcom/android/settings/display/PaperModeFragment;->UB:Lmiui/app/TimePickerDialog;

    invoke-virtual {v0}, Lmiui/app/TimePickerDialog;->show()V

    goto :goto_0

    :cond_4
    iget-object v0, p0, Lcom/android/settings/display/PaperModeFragment;->UB:Lmiui/app/TimePickerDialog;

    invoke-virtual {v0, v3, v3}, Lmiui/app/TimePickerDialog;->updateTime(II)V

    goto :goto_1

    :cond_5
    const-string/jumbo v1, "paper_mode_end_time"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iput-boolean v2, p0, Lcom/android/settings/display/PaperModeFragment;->UA:Z

    sget v0, Lcom/android/settings/display/PaperModeFragment;->Uu:I

    if-lez v0, :cond_6

    iget-object v0, p0, Lcom/android/settings/display/PaperModeFragment;->UB:Lmiui/app/TimePickerDialog;

    sget v1, Lcom/android/settings/display/PaperModeFragment;->Uu:I

    div-int/lit8 v1, v1, 0x3c

    sget v2, Lcom/android/settings/display/PaperModeFragment;->Uu:I

    rem-int/lit8 v2, v2, 0x3c

    invoke-virtual {v0, v1, v2}, Lmiui/app/TimePickerDialog;->updateTime(II)V

    :goto_2
    iget-object v0, p0, Lcom/android/settings/display/PaperModeFragment;->UB:Lmiui/app/TimePickerDialog;

    invoke-virtual {v0}, Lmiui/app/TimePickerDialog;->show()V

    goto :goto_0

    :cond_6
    iget-object v0, p0, Lcom/android/settings/display/PaperModeFragment;->UB:Lmiui/app/TimePickerDialog;

    invoke-virtual {v0, v3, v3}, Lmiui/app/TimePickerDialog;->updateTime(II)V

    goto :goto_2
.end method

.method public onResume()V
    .locals 0

    invoke-super {p0}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->onResume()V

    return-void
.end method
