.class public Lcom/android/settings/display/ScreenColorSeekBarPreference;
.super Landroid/preference/Preference;
.source "ScreenColorSeekBarPreference.java"

# interfaces
.implements Landroid/widget/SeekBar$OnSeekBarChangeListener;


# instance fields
.field private Wt:Landroid/content/res/ColorStateList;

.field private Wu:Landroid/content/res/ColorStateList;

.field private Wv:D

.field private Ww:I

.field private Wx:[Ljava/lang/CharSequence;

.field private Wy:I

.field private Wz:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/android/settings/display/ScreenColorSeekBarPreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    invoke-direct {p0, p1, p2, p3}, Landroid/preference/Preference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    const-wide/high16 v0, 0x4049000000000000L    # 50.0

    iput-wide v0, p0, Lcom/android/settings/display/ScreenColorSeekBarPreference;->Wv:D

    invoke-direct {p0}, Lcom/android/settings/display/ScreenColorSeekBarPreference;->NR()Landroid/content/res/ColorStateList;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/display/ScreenColorSeekBarPreference;->Wt:Landroid/content/res/ColorStateList;

    return-void
.end method

.method private NR()Landroid/content/res/ColorStateList;
    .locals 8

    const/4 v5, 0x2

    const/4 v7, 0x0

    const/4 v6, 0x1

    new-array v0, v6, [I

    const v1, 0x10100a1

    aput v1, v0, v7

    invoke-virtual {p0}, Lcom/android/settings/display/ScreenColorSeekBarPreference;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {p0}, Lcom/android/settings/display/ScreenColorSeekBarPreference;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v2

    new-instance v3, Landroid/util/TypedValue;

    invoke-direct {v3}, Landroid/util/TypedValue;-><init>()V

    new-array v4, v5, [[I

    new-array v5, v5, [I

    aput-object v0, v4, v7

    sget v0, Lmiui/R$attr;->textColorChecked:I

    invoke-virtual {v2, v0, v3, v6}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    iget v0, v3, Landroid/util/TypedValue;->resourceId:I

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    aput v0, v5, v7

    new-array v0, v6, [I

    aput-object v0, v4, v6

    sget v0, Lmiui/R$attr;->preferencePrimaryTextColor:I

    invoke-virtual {v2, v0, v3, v6}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    iget v0, v3, Landroid/util/TypedValue;->resourceId:I

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    aput v0, v5, v6

    new-instance v0, Landroid/content/res/ColorStateList;

    invoke-direct {v0, v4, v5}, Landroid/content/res/ColorStateList;-><init>([[I[I)V

    return-object v0
.end method

.method private NS(I)I
    .locals 4

    int-to-double v0, p1

    iget-wide v2, p0, Lcom/android/settings/display/ScreenColorSeekBarPreference;->Wv:D

    mul-double/2addr v0, v2

    const-wide/high16 v2, 0x3fe0000000000000L    # 0.5

    add-double/2addr v0, v2

    double-to-int v0, v0

    return v0
.end method

.method private NT(I)I
    .locals 6

    int-to-double v0, p1

    iget-wide v2, p0, Lcom/android/settings/display/ScreenColorSeekBarPreference;->Wv:D

    const-wide/high16 v4, 0x4000000000000000L    # 2.0

    div-double/2addr v2, v4

    add-double/2addr v0, v2

    iget-wide v2, p0, Lcom/android/settings/display/ScreenColorSeekBarPreference;->Wv:D

    div-double/2addr v0, v2

    double-to-int v0, v0

    return v0
.end method

.method private NU(Landroid/widget/SeekBar;I)V
    .locals 1

    if-nez p1, :cond_0

    return-void

    :cond_0
    if-eqz p2, :cond_1

    const/16 v0, 0x64

    if-ne p2, v0, :cond_2

    :cond_1
    iget v0, p0, Lcom/android/settings/display/ScreenColorSeekBarPreference;->Wy:I

    invoke-virtual {p1, v0}, Landroid/widget/SeekBar;->setThumbOffset(I)V

    :goto_0
    invoke-virtual {p1, p2}, Landroid/widget/SeekBar;->setProgress(I)V

    return-void

    :cond_2
    iget v0, p0, Lcom/android/settings/display/ScreenColorSeekBarPreference;->Wz:I

    div-int/lit8 v0, v0, 0x2

    invoke-virtual {p1, v0}, Landroid/widget/SeekBar;->setThumbOffset(I)V

    goto :goto_0
.end method


# virtual methods
.method protected onBindView(Landroid/view/View;)V
    .locals 7

    const/4 v3, 0x0

    invoke-super {p0, p1}, Landroid/preference/Preference;->onBindView(Landroid/view/View;)V

    const v0, 0x7f0a03d4

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/SeekBar;

    invoke-virtual {v0, p0}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    iget v1, p0, Lcom/android/settings/display/ScreenColorSeekBarPreference;->Ww:I

    invoke-direct {p0, v1}, Lcom/android/settings/display/ScreenColorSeekBarPreference;->NS(I)I

    move-result v1

    invoke-direct {p0, v0, v1}, Lcom/android/settings/display/ScreenColorSeekBarPreference;->NU(Landroid/widget/SeekBar;I)V

    const v0, 0x7f0a0264

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    move v2, v3

    :goto_0
    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v1

    if-ge v2, v1, :cond_3

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iget-object v4, p0, Lcom/android/settings/display/ScreenColorSeekBarPreference;->Wx:[Ljava/lang/CharSequence;

    aget-object v4, v4, v2

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget v4, p0, Lcom/android/settings/display/ScreenColorSeekBarPreference;->Ww:I

    if-ne v2, v4, :cond_1

    const/4 v4, 0x1

    :goto_1
    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setSelected(Z)V

    iget-object v4, p0, Lcom/android/settings/display/ScreenColorSeekBarPreference;->Wu:Landroid/content/res/ColorStateList;

    if-nez v4, :cond_0

    invoke-virtual {v1}, Landroid/widget/TextView;->getTextColors()Landroid/content/res/ColorStateList;

    move-result-object v4

    iput-object v4, p0, Lcom/android/settings/display/ScreenColorSeekBarPreference;->Wu:Landroid/content/res/ColorStateList;

    :cond_0
    invoke-virtual {v1}, Landroid/widget/TextView;->isEnabled()Z

    move-result v4

    if-eqz v4, :cond_2

    iget-object v4, p0, Lcom/android/settings/display/ScreenColorSeekBarPreference;->Wt:Landroid/content/res/ColorStateList;

    :goto_2
    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setTextColor(Landroid/content/res/ColorStateList;)V

    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_0

    :cond_1
    move v4, v3

    goto :goto_1

    :cond_2
    iget-object v4, p0, Lcom/android/settings/display/ScreenColorSeekBarPreference;->Wt:Landroid/content/res/ColorStateList;

    iget-object v5, p0, Lcom/android/settings/display/ScreenColorSeekBarPreference;->Wu:Landroid/content/res/ColorStateList;

    invoke-virtual {v1}, Landroid/widget/TextView;->getDrawableState()[I

    move-result-object v6

    invoke-virtual {v5, v6, v3}, Landroid/content/res/ColorStateList;->getColorForState([II)I

    move-result v5

    invoke-static {v5}, Landroid/graphics/Color;->alpha(I)I

    move-result v5

    invoke-virtual {v4, v5}, Landroid/content/res/ColorStateList;->withAlpha(I)Landroid/content/res/ColorStateList;

    move-result-object v4

    goto :goto_2

    :cond_3
    return-void
.end method

.method protected onCreateView(Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 11

    const/4 v5, 0x5

    const/high16 v10, 0x3f800000    # 1.0f

    const/4 v4, 0x3

    const/4 v3, 0x0

    invoke-virtual {p0}, Lcom/android/settings/display/ScreenColorSeekBarPreference;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string/jumbo v1, "layout_inflater"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    const v1, 0x7f0d014b

    invoke-virtual {v0, v1, p1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    const v1, 0x7f0a03d4

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/SeekBar;

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Landroid/widget/SeekBar;->getThumbOffset()I

    move-result v2

    iput v2, p0, Lcom/android/settings/display/ScreenColorSeekBarPreference;->Wy:I

    invoke-virtual {v1}, Landroid/widget/SeekBar;->getThumb()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    if-nez v2, :cond_1

    move v2, v3

    :goto_0
    iput v2, p0, Lcom/android/settings/display/ScreenColorSeekBarPreference;->Wz:I

    :cond_0
    const v2, 0x7f0a0264

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    invoke-virtual {v1}, Landroid/widget/SeekBar;->getPaddingStart()I

    move-result v6

    invoke-virtual {v2}, Landroid/widget/LinearLayout;->getPaddingTop()I

    move-result v7

    invoke-virtual {v1}, Landroid/widget/SeekBar;->getPaddingEnd()I

    move-result v1

    invoke-virtual {v2}, Landroid/widget/LinearLayout;->getPaddingBottom()I

    move-result v8

    invoke-virtual {v2, v6, v7, v1, v8}, Landroid/widget/LinearLayout;->setPaddingRelative(IIII)V

    invoke-virtual {v2}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v1

    if-nez v1, :cond_6

    invoke-virtual {p0}, Lcom/android/settings/display/ScreenColorSeekBarPreference;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    iget-object v1, v1, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-static {v1}, Landroid/text/TextUtils;->getLayoutDirectionFromLocale(Ljava/util/Locale;)I

    move-result v7

    move v6, v3

    :goto_1
    if-ge v6, v4, :cond_6

    new-instance v8, Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/android/settings/display/ScreenColorSeekBarPreference;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v8, v1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    new-instance v9, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v1, -0x2

    invoke-direct {v9, v3, v1}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    if-nez v6, :cond_3

    if-nez v7, :cond_2

    move v1, v4

    :goto_2
    or-int/lit8 v1, v1, 0x10

    invoke-virtual {v8, v1}, Landroid/widget/TextView;->setGravity(I)V

    iput v10, v9, Landroid/widget/LinearLayout$LayoutParams;->weight:F

    :goto_3
    invoke-virtual {v2, v8, v6, v9}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    add-int/lit8 v1, v6, 0x1

    move v6, v1

    goto :goto_1

    :cond_1
    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v2

    goto :goto_0

    :cond_2
    move v1, v5

    goto :goto_2

    :cond_3
    const/4 v1, 0x2

    if-ne v6, v1, :cond_5

    if-nez v7, :cond_4

    move v1, v5

    :goto_4
    or-int/lit8 v1, v1, 0x10

    invoke-virtual {v8, v1}, Landroid/widget/TextView;->setGravity(I)V

    iput v10, v9, Landroid/widget/LinearLayout$LayoutParams;->weight:F

    goto :goto_3

    :cond_4
    move v1, v4

    goto :goto_4

    :cond_5
    const/16 v1, 0x11

    invoke-virtual {v8, v1}, Landroid/widget/TextView;->setGravity(I)V

    const/high16 v1, 0x40000000    # 2.0f

    iput v1, v9, Landroid/widget/LinearLayout$LayoutParams;->weight:F

    goto :goto_3

    :cond_6
    return-object v0
.end method

.method public onProgressChanged(Landroid/widget/SeekBar;IZ)V
    .locals 2

    if-nez p3, :cond_0

    return-void

    :cond_0
    invoke-direct {p0, p2}, Lcom/android/settings/display/ScreenColorSeekBarPreference;->NT(I)I

    move-result v0

    iget v1, p0, Lcom/android/settings/display/ScreenColorSeekBarPreference;->Ww:I

    if-eq v0, v1, :cond_1

    iput v0, p0, Lcom/android/settings/display/ScreenColorSeekBarPreference;->Ww:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/settings/display/ScreenColorSeekBarPreference;->callChangeListener(Ljava/lang/Object;)Z

    :cond_1
    return-void
.end method

.method public onStartTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 0

    return-void
.end method

.method public onStopTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 0

    invoke-virtual {p0}, Lcom/android/settings/display/ScreenColorSeekBarPreference;->notifyChanged()V

    return-void
.end method
