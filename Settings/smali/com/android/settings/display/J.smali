.class final Lcom/android/settings/display/J;
.super Ljava/lang/Object;
.source "PaperModeFragment.java"

# interfaces
.implements Lcom/android/settings/ba;


# instance fields
.field final synthetic XQ:Lcom/android/settings/display/PaperModeFragment;

.field final synthetic XR:Lcom/android/settings/MiuiSeekBarPreference;


# direct methods
.method constructor <init>(Lcom/android/settings/display/PaperModeFragment;Lcom/android/settings/MiuiSeekBarPreference;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/display/J;->XQ:Lcom/android/settings/display/PaperModeFragment;

    iput-object p2, p0, Lcom/android/settings/display/J;->XR:Lcom/android/settings/MiuiSeekBarPreference;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public OE()V
    .locals 3

    iget-object v0, p0, Lcom/android/settings/display/J;->XQ:Lcom/android/settings/display/PaperModeFragment;

    invoke-static {v0}, Lcom/android/settings/display/PaperModeFragment;->Mj(Lcom/android/settings/display/PaperModeFragment;)Landroid/preference/CheckBoxPreference;

    move-result-object v0

    invoke-virtual {v0}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/display/J;->XR:Lcom/android/settings/MiuiSeekBarPreference;

    invoke-virtual {v0}, Lcom/android/settings/MiuiSeekBarPreference;->bzj()I

    move-result v0

    int-to-float v0, v0

    invoke-static {}, Lcom/android/settings/display/PaperModeFragment;->-get0()F

    move-result v1

    mul-float/2addr v0, v1

    const/high16 v1, 0x3f800000    # 1.0f

    add-float/2addr v0, v1

    const/high16 v1, 0x40800000    # 4.0f

    cmpl-float v0, v0, v1

    if-ltz v0, :cond_0

    invoke-static {}, Lcom/android/settings/display/PaperModeFragment;->Mo()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/display/J;->XQ:Lcom/android/settings/display/PaperModeFragment;

    invoke-virtual {v0}, Lcom/android/settings/display/PaperModeFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/display/J;->XQ:Lcom/android/settings/display/PaperModeFragment;

    invoke-virtual {v1}, Lcom/android/settings/display/PaperModeFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f120e97

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    :cond_0
    return-void
.end method
