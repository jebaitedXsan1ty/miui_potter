.class Lcom/android/settings/print/PrintSettingsFragment$PrintSummaryProvider;
.super Ljava/lang/Object;
.source "PrintSettingsFragment.java"

# interfaces
.implements Lcom/android/settings/dashboard/D;
.implements Landroid/print/PrintManager$PrintJobStateChangeListener;


# instance fields
.field private final bfs:Lcom/android/settings/print/f;

.field private final bft:Lcom/android/settings/dashboard/C;

.field private final mContext:Landroid/content/Context;


# direct methods
.method constructor <init>(Landroid/content/Context;Lcom/android/settings/dashboard/C;Lcom/android/settings/print/f;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/android/settings/print/PrintSettingsFragment$PrintSummaryProvider;->mContext:Landroid/content/Context;

    iput-object p2, p0, Lcom/android/settings/print/PrintSettingsFragment$PrintSummaryProvider;->bft:Lcom/android/settings/dashboard/C;

    iput-object p3, p0, Lcom/android/settings/print/PrintSettingsFragment$PrintSummaryProvider;->bfs:Lcom/android/settings/print/f;

    return-void
.end method


# virtual methods
.method public jt(Z)V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/android/settings/print/PrintSettingsFragment$PrintSummaryProvider;->bfs:Lcom/android/settings/print/f;

    if-eqz v0, :cond_0

    if-eqz p1, :cond_1

    iget-object v0, p0, Lcom/android/settings/print/PrintSettingsFragment$PrintSummaryProvider;->bfs:Lcom/android/settings/print/f;

    invoke-virtual {v0, p0}, Lcom/android/settings/print/f;->aTK(Landroid/print/PrintManager$PrintJobStateChangeListener;)V

    invoke-virtual {p0, v1}, Lcom/android/settings/print/PrintSettingsFragment$PrintSummaryProvider;->onPrintJobStateChanged(Landroid/print/PrintJobId;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/settings/print/PrintSettingsFragment$PrintSummaryProvider;->bfs:Lcom/android/settings/print/f;

    invoke-virtual {v0, p0}, Lcom/android/settings/print/f;->aTN(Landroid/print/PrintManager$PrintJobStateChangeListener;)V

    goto :goto_0
.end method

.method public onPrintJobStateChanged(Landroid/print/PrintJobId;)V
    .locals 6

    const/4 v4, 0x1

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/android/settings/print/PrintSettingsFragment$PrintSummaryProvider;->bfs:Lcom/android/settings/print/f;

    invoke-virtual {v0}, Lcom/android/settings/print/f;->aTL()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v1, v2

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/print/PrintJob;

    invoke-virtual {v0}, Landroid/print/PrintJob;->getInfo()Landroid/print/PrintJobInfo;

    move-result-object v0

    invoke-static {v0}, Lcom/android/settings/print/PrintSettingsFragment;->aTH(Landroid/print/PrintJobInfo;)Z

    move-result v0

    if-eqz v0, :cond_5

    add-int/lit8 v0, v1, 0x1

    :goto_1
    move v1, v0

    goto :goto_0

    :cond_0
    move v1, v2

    :cond_1
    if-lez v1, :cond_2

    iget-object v0, p0, Lcom/android/settings/print/PrintSettingsFragment$PrintSummaryProvider;->bft:Lcom/android/settings/dashboard/C;

    iget-object v3, p0, Lcom/android/settings/print/PrintSettingsFragment$PrintSummaryProvider;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v2

    const v2, 0x7f10002f

    invoke-virtual {v3, v2, v1, v4}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p0, v1}, Lcom/android/settings/dashboard/C;->Fd(Lcom/android/settings/dashboard/D;Ljava/lang/CharSequence;)V

    :goto_2
    return-void

    :cond_2
    iget-object v0, p0, Lcom/android/settings/print/PrintSettingsFragment$PrintSummaryProvider;->bfs:Lcom/android/settings/print/f;

    invoke-virtual {v0, v4}, Lcom/android/settings/print/f;->aTM(I)Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_4

    :cond_3
    iget-object v0, p0, Lcom/android/settings/print/PrintSettingsFragment$PrintSummaryProvider;->bft:Lcom/android/settings/dashboard/C;

    iget-object v1, p0, Lcom/android/settings/print/PrintSettingsFragment$PrintSummaryProvider;->mContext:Landroid/content/Context;

    const v2, 0x7f120d1c

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p0, v1}, Lcom/android/settings/dashboard/C;->Fd(Lcom/android/settings/dashboard/D;Ljava/lang/CharSequence;)V

    goto :goto_2

    :cond_4
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    iget-object v1, p0, Lcom/android/settings/print/PrintSettingsFragment$PrintSummaryProvider;->bft:Lcom/android/settings/dashboard/C;

    iget-object v3, p0, Lcom/android/settings/print/PrintSettingsFragment$PrintSummaryProvider;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v2

    const v2, 0x7f100030

    invoke-virtual {v3, v2, v0, v4}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, p0, v0}, Lcom/android/settings/dashboard/C;->Fd(Lcom/android/settings/dashboard/D;Ljava/lang/CharSequence;)V

    goto :goto_2

    :cond_5
    move v0, v1

    goto :goto_1
.end method
