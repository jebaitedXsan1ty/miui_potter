.class final Lcom/android/settings/print/j;
.super Landroid/database/DataSetObserver;
.source "PrintServiceSettingsFragment.java"


# instance fields
.field final synthetic bfE:Lcom/android/settings/print/PrintServiceSettingsFragment;


# direct methods
.method constructor <init>(Lcom/android/settings/print/PrintServiceSettingsFragment;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/print/j;->bfE:Lcom/android/settings/print/PrintServiceSettingsFragment;

    invoke-direct {p0}, Landroid/database/DataSetObserver;-><init>()V

    return-void
.end method

.method private aTR()V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/print/j;->bfE:Lcom/android/settings/print/PrintServiceSettingsFragment;

    invoke-static {v0}, Lcom/android/settings/print/PrintServiceSettingsFragment;->aTh(Lcom/android/settings/print/PrintServiceSettingsFragment;)Lcom/android/settings/print/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/settings/print/a;->aTo()I

    move-result v0

    iget-object v1, p0, Lcom/android/settings/print/j;->bfE:Lcom/android/settings/print/PrintServiceSettingsFragment;

    invoke-static {v1}, Lcom/android/settings/print/PrintServiceSettingsFragment;->aTf(Lcom/android/settings/print/PrintServiceSettingsFragment;)I

    move-result v1

    if-gtz v1, :cond_1

    if-lez v0, :cond_1

    :goto_0
    iget-object v1, p0, Lcom/android/settings/print/j;->bfE:Lcom/android/settings/print/PrintServiceSettingsFragment;

    invoke-virtual {v1}, Lcom/android/settings/print/PrintServiceSettingsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->invalidateOptionsMenu()V

    :cond_0
    iget-object v1, p0, Lcom/android/settings/print/j;->bfE:Lcom/android/settings/print/PrintServiceSettingsFragment;

    invoke-static {v1, v0}, Lcom/android/settings/print/PrintServiceSettingsFragment;->aTj(Lcom/android/settings/print/PrintServiceSettingsFragment;I)I

    return-void

    :cond_1
    iget-object v1, p0, Lcom/android/settings/print/j;->bfE:Lcom/android/settings/print/PrintServiceSettingsFragment;

    invoke-static {v1}, Lcom/android/settings/print/PrintServiceSettingsFragment;->aTf(Lcom/android/settings/print/PrintServiceSettingsFragment;)I

    move-result v1

    if-lez v1, :cond_0

    if-gtz v0, :cond_0

    goto :goto_0
.end method


# virtual methods
.method public onChanged()V
    .locals 1

    invoke-direct {p0}, Lcom/android/settings/print/j;->aTR()V

    iget-object v0, p0, Lcom/android/settings/print/j;->bfE:Lcom/android/settings/print/PrintServiceSettingsFragment;

    invoke-static {v0}, Lcom/android/settings/print/PrintServiceSettingsFragment;->aTl(Lcom/android/settings/print/PrintServiceSettingsFragment;)V

    return-void
.end method

.method public onInvalidated()V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/print/j;->aTR()V

    return-void
.end method
