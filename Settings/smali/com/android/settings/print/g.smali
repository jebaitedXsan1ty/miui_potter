.class final Lcom/android/settings/print/g;
.super Landroid/widget/Filter;
.source "PrintServiceSettingsFragment.java"


# instance fields
.field final synthetic bfA:Lcom/android/settings/print/a;


# direct methods
.method constructor <init>(Lcom/android/settings/print/a;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/print/g;->bfA:Lcom/android/settings/print/a;

    invoke-direct {p0}, Landroid/widget/Filter;-><init>()V

    return-void
.end method


# virtual methods
.method protected performFiltering(Ljava/lang/CharSequence;)Landroid/widget/Filter$FilterResults;
    .locals 8

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/android/settings/print/g;->bfA:Lcom/android/settings/print/a;

    invoke-static {v0}, Lcom/android/settings/print/a;->aTr(Lcom/android/settings/print/a;)Ljava/lang/Object;

    move-result-object v2

    monitor-enter v2

    :try_start_0
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_0

    monitor-exit v2

    return-object v1

    :cond_0
    :try_start_1
    new-instance v3, Landroid/widget/Filter$FilterResults;

    invoke-direct {v3}, Landroid/widget/Filter$FilterResults;-><init>()V

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v5

    iget-object v0, p0, Lcom/android/settings/print/g;->bfA:Lcom/android/settings/print/a;

    invoke-static {v0}, Lcom/android/settings/print/a;->aTs(Lcom/android/settings/print/a;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v6

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v6, :cond_2

    iget-object v0, p0, Lcom/android/settings/print/g;->bfA:Lcom/android/settings/print/a;

    invoke-static {v0}, Lcom/android/settings/print/a;->aTs(Lcom/android/settings/print/a;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/print/PrinterInfo;

    invoke-virtual {v0}, Landroid/print/PrinterInfo;->getName()Ljava/lang/String;

    move-result-object v7

    if-eqz v7, :cond_1

    invoke-virtual {v7}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_1

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_2
    iput-object v4, v3, Landroid/widget/Filter$FilterResults;->values:Ljava/lang/Object;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v0

    iput v0, v3, Landroid/widget/Filter$FilterResults;->count:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit v2

    return-object v3

    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0
.end method

.method protected publishResults(Ljava/lang/CharSequence;Landroid/widget/Filter$FilterResults;)V
    .locals 3

    iget-object v0, p0, Lcom/android/settings/print/g;->bfA:Lcom/android/settings/print/a;

    invoke-static {v0}, Lcom/android/settings/print/a;->aTr(Lcom/android/settings/print/a;)Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/android/settings/print/g;->bfA:Lcom/android/settings/print/a;

    invoke-static {v0, p1}, Lcom/android/settings/print/a;->aTt(Lcom/android/settings/print/a;Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    iget-object v0, p0, Lcom/android/settings/print/g;->bfA:Lcom/android/settings/print/a;

    invoke-static {v0}, Lcom/android/settings/print/a;->aTq(Lcom/android/settings/print/a;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->clear()V

    if-nez p2, :cond_0

    iget-object v0, p0, Lcom/android/settings/print/g;->bfA:Lcom/android/settings/print/a;

    invoke-static {v0}, Lcom/android/settings/print/a;->aTq(Lcom/android/settings/print/a;)Ljava/util/List;

    move-result-object v0

    iget-object v2, p0, Lcom/android/settings/print/g;->bfA:Lcom/android/settings/print/a;

    invoke-static {v2}, Lcom/android/settings/print/a;->aTs(Lcom/android/settings/print/a;)Ljava/util/List;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    monitor-exit v1

    iget-object v0, p0, Lcom/android/settings/print/g;->bfA:Lcom/android/settings/print/a;

    invoke-virtual {v0}, Lcom/android/settings/print/a;->notifyDataSetChanged()V

    return-void

    :cond_0
    :try_start_1
    iget-object v0, p2, Landroid/widget/Filter$FilterResults;->values:Ljava/lang/Object;

    check-cast v0, Ljava/util/List;

    iget-object v2, p0, Lcom/android/settings/print/g;->bfA:Lcom/android/settings/print/a;

    invoke-static {v2}, Lcom/android/settings/print/a;->aTq(Lcom/android/settings/print/a;)Ljava/util/List;

    move-result-object v2

    invoke-interface {v2, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method
