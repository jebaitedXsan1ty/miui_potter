.class final Lcom/android/settings/print/e;
.super Landroid/content/AsyncTaskLoader;
.source "PrintSettingsFragment.java"


# instance fields
.field private bfp:Landroid/print/PrintManager$PrintJobStateChangeListener;

.field private bfq:Ljava/util/List;

.field private final bfr:Landroid/print/PrintManager;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    invoke-direct {p0, p1}, Landroid/content/AsyncTaskLoader;-><init>(Landroid/content/Context;)V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/settings/print/e;->bfq:Ljava/util/List;

    const-string/jumbo v0, "print"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/print/PrintManager;

    invoke-virtual {p1}, Landroid/content/Context;->getUserId()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/print/PrintManager;->getGlobalPrintManagerForUser(I)Landroid/print/PrintManager;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/print/e;->bfr:Landroid/print/PrintManager;

    return-void
.end method

.method static synthetic aTJ(Lcom/android/settings/print/e;)V
    .locals 0

    invoke-virtual {p0}, Lcom/android/settings/print/e;->onForceLoad()V

    return-void
.end method


# virtual methods
.method public aTI(Ljava/util/List;)V
    .locals 1

    invoke-virtual {p0}, Lcom/android/settings/print/e;->isStarted()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-super {p0, p1}, Landroid/content/AsyncTaskLoader;->deliverResult(Ljava/lang/Object;)V

    :cond_0
    return-void
.end method

.method public bridge synthetic deliverResult(Ljava/lang/Object;)V
    .locals 0

    check-cast p1, Ljava/util/List;

    invoke-virtual {p0, p1}, Lcom/android/settings/print/e;->aTI(Ljava/util/List;)V

    return-void
.end method

.method public bridge synthetic loadInBackground()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/android/settings/print/e;->loadInBackground()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public loadInBackground()Ljava/util/List;
    .locals 6

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/android/settings/print/e;->bfr:Landroid/print/PrintManager;

    invoke-virtual {v0}, Landroid/print/PrintManager;->getPrintJobs()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v4

    const/4 v0, 0x0

    move v2, v0

    :goto_0
    if-ge v2, v4, :cond_0

    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/print/PrintJob;

    invoke-virtual {v0}, Landroid/print/PrintJob;->getInfo()Landroid/print/PrintJobInfo;

    move-result-object v5

    invoke-static {v5}, Lcom/android/settings/print/PrintSettingsFragment;->aTH(Landroid/print/PrintJobInfo;)Z

    move-result v0

    if-eqz v0, :cond_2

    if-nez v1, :cond_1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    :goto_1
    invoke-interface {v0, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :goto_2
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    move-object v1, v0

    goto :goto_0

    :cond_0
    return-object v1

    :cond_1
    move-object v0, v1

    goto :goto_1

    :cond_2
    move-object v0, v1

    goto :goto_2
.end method

.method protected onReset()V
    .locals 3

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/android/settings/print/e;->onStopLoading()V

    iget-object v0, p0, Lcom/android/settings/print/e;->bfq:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    iget-object v0, p0, Lcom/android/settings/print/e;->bfp:Landroid/print/PrintManager$PrintJobStateChangeListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/print/e;->bfr:Landroid/print/PrintManager;

    iget-object v1, p0, Lcom/android/settings/print/e;->bfp:Landroid/print/PrintManager$PrintJobStateChangeListener;

    invoke-virtual {v0, v1}, Landroid/print/PrintManager;->removePrintJobStateChangeListener(Landroid/print/PrintManager$PrintJobStateChangeListener;)V

    iput-object v2, p0, Lcom/android/settings/print/e;->bfp:Landroid/print/PrintManager$PrintJobStateChangeListener;

    :cond_0
    return-void
.end method

.method protected onStartLoading()V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/print/e;->bfq:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/android/settings/print/e;->bfq:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-virtual {p0, v0}, Lcom/android/settings/print/e;->aTI(Ljava/util/List;)V

    :cond_0
    iget-object v0, p0, Lcom/android/settings/print/e;->bfp:Landroid/print/PrintManager$PrintJobStateChangeListener;

    if-nez v0, :cond_1

    new-instance v0, Lcom/android/settings/print/p;

    invoke-direct {v0, p0}, Lcom/android/settings/print/p;-><init>(Lcom/android/settings/print/e;)V

    iput-object v0, p0, Lcom/android/settings/print/e;->bfp:Landroid/print/PrintManager$PrintJobStateChangeListener;

    iget-object v0, p0, Lcom/android/settings/print/e;->bfr:Landroid/print/PrintManager;

    iget-object v1, p0, Lcom/android/settings/print/e;->bfp:Landroid/print/PrintManager$PrintJobStateChangeListener;

    invoke-virtual {v0, v1}, Landroid/print/PrintManager;->addPrintJobStateChangeListener(Landroid/print/PrintManager$PrintJobStateChangeListener;)V

    :cond_1
    iget-object v0, p0, Lcom/android/settings/print/e;->bfq:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lcom/android/settings/print/e;->onForceLoad()V

    :cond_2
    return-void
.end method

.method protected onStopLoading()V
    .locals 0

    invoke-virtual {p0}, Lcom/android/settings/print/e;->onCancelLoad()Z

    return-void
.end method
