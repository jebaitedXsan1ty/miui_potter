.class Lcom/android/settings/print/f;
.super Ljava/lang/Object;
.source "PrintSettingsFragment.java"


# instance fields
.field private final bfu:Landroid/print/PrintManager;


# direct methods
.method constructor <init>(Landroid/content/Context;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string/jumbo v0, "print"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/print/PrintManager;

    invoke-virtual {p1}, Landroid/content/Context;->getUserId()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/print/PrintManager;->getGlobalPrintManagerForUser(I)Landroid/print/PrintManager;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/print/f;->bfu:Landroid/print/PrintManager;

    return-void
.end method


# virtual methods
.method public aTK(Landroid/print/PrintManager$PrintJobStateChangeListener;)V
    .locals 1

    iget-object v0, p0, Lcom/android/settings/print/f;->bfu:Landroid/print/PrintManager;

    invoke-virtual {v0, p1}, Landroid/print/PrintManager;->addPrintJobStateChangeListener(Landroid/print/PrintManager$PrintJobStateChangeListener;)V

    return-void
.end method

.method public aTL()Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/print/f;->bfu:Landroid/print/PrintManager;

    invoke-virtual {v0}, Landroid/print/PrintManager;->getPrintJobs()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public aTM(I)Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/print/f;->bfu:Landroid/print/PrintManager;

    invoke-virtual {v0, p1}, Landroid/print/PrintManager;->getPrintServices(I)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public aTN(Landroid/print/PrintManager$PrintJobStateChangeListener;)V
    .locals 1

    iget-object v0, p0, Lcom/android/settings/print/f;->bfu:Landroid/print/PrintManager;

    invoke-virtual {v0, p1}, Landroid/print/PrintManager;->removePrintJobStateChangeListener(Landroid/print/PrintManager$PrintJobStateChangeListener;)V

    return-void
.end method
