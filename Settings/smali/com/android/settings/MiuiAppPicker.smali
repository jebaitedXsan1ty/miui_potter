.class public Lcom/android/settings/MiuiAppPicker;
.super Lmiui/app/Activity;
.source "MiuiAppPicker.java"


# static fields
.field private static final bSS:Ljava/util/Comparator;


# instance fields
.field private bSR:Lcom/android/settings/bF;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/android/settings/jl;

    invoke-direct {v0}, Lcom/android/settings/jl;-><init>()V

    sput-object v0, Lcom/android/settings/MiuiAppPicker;->bSS:Ljava/util/Comparator;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lmiui/app/Activity;-><init>()V

    return-void
.end method

.method static synthetic bMe(Lcom/android/settings/MiuiAppPicker;)Lcom/android/settings/bF;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/MiuiAppPicker;->bSR:Lcom/android/settings/bF;

    return-object v0
.end method

.method static synthetic bMf()Ljava/util/Comparator;
    .locals 1

    sget-object v0, Lcom/android/settings/MiuiAppPicker;->bSS:Ljava/util/Comparator;

    return-object v0
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Lmiui/app/Activity;->onCreate(Landroid/os/Bundle;)V

    const v0, 0x7f0d0038

    invoke-virtual {p0, v0}, Lcom/android/settings/MiuiAppPicker;->setContentView(I)V

    const v0, 0x7f0a0267

    invoke-virtual {p0, v0}, Lcom/android/settings/MiuiAppPicker;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    new-instance v1, Lcom/android/settings/jm;

    invoke-direct {v1, p0}, Lcom/android/settings/jm;-><init>(Lcom/android/settings/MiuiAppPicker;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    new-instance v1, Lcom/android/settings/bF;

    invoke-direct {v1, p0, p0}, Lcom/android/settings/bF;-><init>(Lcom/android/settings/MiuiAppPicker;Landroid/content/Context;)V

    iput-object v1, p0, Lcom/android/settings/MiuiAppPicker;->bSR:Lcom/android/settings/bF;

    iget-object v1, p0, Lcom/android/settings/MiuiAppPicker;->bSR:Lcom/android/settings/bF;

    invoke-virtual {v1}, Lcom/android/settings/bF;->getCount()I

    move-result v1

    if-gtz v1, :cond_0

    invoke-virtual {p0}, Lcom/android/settings/MiuiAppPicker;->finish()V

    :goto_0
    return-void

    :cond_0
    iget-object v1, p0, Lcom/android/settings/MiuiAppPicker;->bSR:Lcom/android/settings/bF;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    goto :goto_0
.end method

.method protected onDestroy()V
    .locals 0

    invoke-super {p0}, Lmiui/app/Activity;->onDestroy()V

    return-void
.end method

.method protected onStart()V
    .locals 1

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/android/settings/MiuiAppPicker;->setVisible(Z)V

    invoke-super {p0}, Lmiui/app/Activity;->onStart()V

    return-void
.end method
