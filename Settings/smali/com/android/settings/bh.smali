.class final Lcom/android/settings/bh;
.super Landroid/net/ConnectivityManager$OnStartTetheringCallback;
.source "TetherSettings.java"


# instance fields
.field final bLi:Ljava/lang/ref/WeakReference;


# direct methods
.method constructor <init>(Lcom/android/settings/TetherSettings;)V
    .locals 1

    invoke-direct {p0}, Landroid/net/ConnectivityManager$OnStartTetheringCallback;-><init>()V

    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/android/settings/bh;->bLi:Ljava/lang/ref/WeakReference;

    return-void
.end method

.method private bBF()V
    .locals 1

    iget-object v0, p0, Lcom/android/settings/bh;->bLi:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settings/TetherSettings;

    if-eqz v0, :cond_0

    invoke-static {v0}, Lcom/android/settings/TetherSettings;->bBD(Lcom/android/settings/TetherSettings;)V

    :cond_0
    return-void
.end method


# virtual methods
.method public onTetheringFailed()V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/bh;->bBF()V

    return-void
.end method

.method public onTetheringStarted()V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/bh;->bBF()V

    return-void
.end method
