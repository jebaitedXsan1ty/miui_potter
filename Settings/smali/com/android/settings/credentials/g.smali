.class final Lcom/android/settings/credentials/g;
.super Ljava/lang/Object;
.source "MiuiCredentialsUpdater.java"

# interfaces
.implements Lcom/android/settings/credentials/d;


# instance fields
.field final synthetic bqj:Lcom/android/settings/credentials/a;

.field final synthetic bqk:Lcom/android/settings/credentials/e;

.field final synthetic val$context:Landroid/content/Context;


# direct methods
.method constructor <init>(Lcom/android/settings/credentials/a;Lcom/android/settings/credentials/e;Landroid/content/Context;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/credentials/g;->bqj:Lcom/android/settings/credentials/a;

    iput-object p2, p0, Lcom/android/settings/credentials/g;->bqk:Lcom/android/settings/credentials/e;

    iput-object p3, p0, Lcom/android/settings/credentials/g;->val$context:Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic bdF(Ljava/lang/Object;)V
    .locals 0

    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/android/settings/credentials/g;->bdI(Ljava/lang/String;)V

    return-void
.end method

.method public bdI(Ljava/lang/String;)V
    .locals 3

    iget-object v0, p0, Lcom/android/settings/credentials/g;->bqj:Lcom/android/settings/credentials/a;

    invoke-static {v0, p1}, Lcom/android/settings/credentials/a;->bdB(Lcom/android/settings/credentials/a;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    return-void

    :cond_0
    new-instance v1, Lcom/android/settings/credentials/h;

    iget-object v2, p0, Lcom/android/settings/credentials/g;->val$context:Landroid/content/Context;

    invoke-direct {v1, p0, v2}, Lcom/android/settings/credentials/h;-><init>(Lcom/android/settings/credentials/g;Landroid/content/Context;)V

    iget-object v2, p0, Lcom/android/settings/credentials/g;->bqj:Lcom/android/settings/credentials/a;

    invoke-static {v2, v0, v1}, Lcom/android/settings/credentials/a;->bdD(Lcom/android/settings/credentials/a;Ljava/lang/String;Lcom/android/settings/credentials/d;)V

    iget-object v0, p0, Lcom/android/settings/credentials/g;->bqk:Lcom/android/settings/credentials/e;

    invoke-interface {v0}, Lcom/android/settings/credentials/e;->bdG()V

    return-void
.end method
