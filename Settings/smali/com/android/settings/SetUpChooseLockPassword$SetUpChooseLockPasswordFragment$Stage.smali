.class final enum Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment$Stage;
.super Ljava/lang/Enum;
.source "SetUpChooseLockPassword.java"


# static fields
.field public static final enum cgG:Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment$Stage;

.field public static final enum cgH:Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment$Stage;

.field public static final enum cgI:Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment$Stage;

.field private static final synthetic cgJ:[Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment$Stage;


# instance fields
.field public final alphaHint:I

.field public final buttonText:I

.field public final numericHint:I


# direct methods
.method static constructor <clinit>()V
    .locals 16

    const v5, 0x7f1209ad

    const/4 v15, 0x2

    const/4 v8, 0x1

    const/4 v2, 0x0

    new-instance v0, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment$Stage;

    const-string/jumbo v1, "Introduction"

    const v3, 0x7f120997

    const v4, 0x7f12099a

    invoke-direct/range {v0 .. v5}, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment$Stage;-><init>(Ljava/lang/String;IIII)V

    sput-object v0, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment$Stage;->cgH:Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment$Stage;

    new-instance v6, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment$Stage;

    const-string/jumbo v7, "NeedToConfirm"

    const v9, 0x7f1209a4

    const v10, 0x7f1209aa

    const v11, 0x7f1209b1

    invoke-direct/range {v6 .. v11}, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment$Stage;-><init>(Ljava/lang/String;IIII)V

    sput-object v6, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment$Stage;->cgI:Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment$Stage;

    new-instance v9, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment$Stage;

    const-string/jumbo v10, "ConfirmWrong"

    const v12, 0x7f12099d

    const v13, 0x7f12099e

    move v11, v15

    move v14, v5

    invoke-direct/range {v9 .. v14}, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment$Stage;-><init>(Ljava/lang/String;IIII)V

    sput-object v9, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment$Stage;->cgG:Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment$Stage;

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment$Stage;

    sget-object v1, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment$Stage;->cgH:Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment$Stage;

    aput-object v1, v0, v2

    sget-object v1, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment$Stage;->cgI:Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment$Stage;

    aput-object v1, v0, v8

    sget-object v1, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment$Stage;->cgG:Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment$Stage;

    aput-object v1, v0, v15

    sput-object v0, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment$Stage;->cgJ:[Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment$Stage;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IIII)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput p3, p0, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment$Stage;->alphaHint:I

    iput p4, p0, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment$Stage;->numericHint:I

    iput p5, p0, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment$Stage;->buttonText:I

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment$Stage;
    .locals 1

    const-class v0, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment$Stage;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment$Stage;

    return-object v0
.end method

.method public static values()[Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment$Stage;
    .locals 1

    sget-object v0, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment$Stage;->cgJ:[Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment$Stage;

    return-object v0
.end method
