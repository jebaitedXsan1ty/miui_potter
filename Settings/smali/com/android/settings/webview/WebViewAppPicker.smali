.class public Lcom/android/settings/webview/WebViewAppPicker;
.super Lcom/android/settings/applications/defaultapps/DefaultAppPickerFragment;
.source "WebViewAppPicker.java"


# instance fields
.field private boI:Lcom/android/settings/webview/c;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/applications/defaultapps/DefaultAppPickerFragment;-><init>()V

    return-void
.end method

.method private bck()Lcom/android/settings/webview/c;
    .locals 1

    new-instance v0, Lcom/android/settings/webview/c;

    invoke-direct {v0}, Lcom/android/settings/webview/c;-><init>()V

    return-object v0
.end method

.method private bcl()Lcom/android/settings/webview/c;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/webview/WebViewAppPicker;->boI:Lcom/android/settings/webview/c;

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/android/settings/webview/WebViewAppPicker;->bck()Lcom/android/settings/webview/c;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/settings/webview/WebViewAppPicker;->setWebViewUpdateServiceWrapper(Lcom/android/settings/webview/c;)V

    :cond_0
    iget-object v0, p0, Lcom/android/settings/webview/WebViewAppPicker;->boI:Lcom/android/settings/webview/c;

    return-object v0
.end method


# virtual methods
.method protected aAD(Z)V
    .locals 2

    const/4 v0, 0x0

    if-eqz p1, :cond_2

    invoke-virtual {p0}, Lcom/android/settings/webview/WebViewAppPicker;->getActivity()Landroid/app/Activity;

    move-result-object v1

    if-nez v1, :cond_1

    :goto_0
    if-eqz v0, :cond_0

    const-string/jumbo v1, "android.settings.WEBVIEW_SETTINGS"

    invoke-virtual {v0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/android/settings/webview/WebViewAppPicker;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    :cond_0
    :goto_1
    return-void

    :cond_1
    invoke-virtual {v1}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    goto :goto_0

    :cond_2
    invoke-direct {p0}, Lcom/android/settings/webview/WebViewAppPicker;->bcl()Lcom/android/settings/webview/c;

    move-result-object v0

    invoke-virtual {p0}, Lcom/android/settings/webview/WebViewAppPicker;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/settings/webview/c;->bcp(Landroid/content/Context;)V

    invoke-virtual {p0}, Lcom/android/settings/webview/WebViewAppPicker;->updateCandidates()V

    goto :goto_1
.end method

.method createDefaultAppInfo(Lcom/android/settings/applications/PackageManagerWrapper;Landroid/content/pm/PackageItemInfo;Ljava/lang/String;)Lcom/android/settings/applications/defaultapps/a;
    .locals 2

    new-instance v0, Lcom/android/settings/webview/b;

    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    invoke-direct {v0, p1, p2, p3, v1}, Lcom/android/settings/webview/b;-><init>(Lcom/android/settings/applications/PackageManagerWrapper;Landroid/content/pm/PackageItemInfo;Ljava/lang/String;Z)V

    return-object v0
.end method

.method getDisabledReason(Lcom/android/settings/webview/c;Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;
    .locals 5

    const/4 v4, 0x1

    const/4 v3, 0x0

    invoke-virtual {p1, p2, p3}, Lcom/android/settings/webview/c;->bcq(Landroid/content/Context;Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settings/webview/d;

    invoke-interface {v0}, Lcom/android/settings/webview/d;->bcr()Z

    move-result v2

    if-nez v2, :cond_1

    new-array v1, v4, [Ljava/lang/Object;

    invoke-interface {v0}, Lcom/android/settings/webview/d;->bcs()Landroid/content/pm/UserInfo;

    move-result-object v0

    iget-object v0, v0, Landroid/content/pm/UserInfo;->name:Ljava/lang/String;

    aput-object v0, v1, v3

    const v0, 0x7f1214a9

    invoke-virtual {p2, v0, v1}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_1
    invoke-interface {v0}, Lcom/android/settings/webview/d;->bct()Z

    move-result v2

    if-nez v2, :cond_0

    new-array v1, v4, [Ljava/lang/Object;

    invoke-interface {v0}, Lcom/android/settings/webview/d;->bcs()Landroid/content/pm/UserInfo;

    move-result-object v0

    iget-object v0, v0, Landroid/content/pm/UserInfo;->name:Ljava/lang/String;

    aput-object v0, v1, v3

    const v0, 0x7f1214a7

    invoke-virtual {p2, v0, v1}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_2
    const/4 v0, 0x0

    return-object v0
.end method

.method public getMetricsCategory()I
    .locals 1

    const/16 v0, 0x195

    return v0
.end method

.method protected mB()Ljava/lang/String;
    .locals 2

    const/4 v0, 0x0

    invoke-direct {p0}, Lcom/android/settings/webview/WebViewAppPicker;->bcl()Lcom/android/settings/webview/c;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/settings/webview/c;->bcm()Landroid/content/pm/PackageInfo;

    move-result-object v1

    if-nez v1, :cond_0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, v1, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    goto :goto_0
.end method

.method protected mG(Ljava/lang/String;)Z
    .locals 1

    invoke-direct {p0}, Lcom/android/settings/webview/WebViewAppPicker;->bcl()Lcom/android/settings/webview/c;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/android/settings/webview/c;->bco(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method protected my()Ljava/util/List;
    .locals 7

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-direct {p0}, Lcom/android/settings/webview/WebViewAppPicker;->bcl()Lcom/android/settings/webview/c;

    move-result-object v0

    invoke-virtual {p0}, Lcom/android/settings/webview/WebViewAppPicker;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/android/settings/webview/c;->bcn(Landroid/content/Context;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/ApplicationInfo;

    iget-object v3, p0, Lcom/android/settings/webview/WebViewAppPicker;->oq:Lcom/android/settings/applications/PackageManagerWrapper;

    invoke-direct {p0}, Lcom/android/settings/webview/WebViewAppPicker;->bcl()Lcom/android/settings/webview/c;

    move-result-object v4

    invoke-virtual {p0}, Lcom/android/settings/webview/WebViewAppPicker;->getContext()Landroid/content/Context;

    move-result-object v5

    iget-object v6, v0, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    invoke-virtual {p0, v4, v5, v6}, Lcom/android/settings/webview/WebViewAppPicker;->getDisabledReason(Lcom/android/settings/webview/c;Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v3, v0, v4}, Lcom/android/settings/webview/WebViewAppPicker;->createDefaultAppInfo(Lcom/android/settings/applications/PackageManagerWrapper;Landroid/content/pm/PackageItemInfo;Ljava/lang/String;)Lcom/android/settings/applications/defaultapps/a;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    return-object v1
.end method

.method public onAttach(Landroid/content/Context;)V
    .locals 1

    invoke-super {p0, p1}, Lcom/android/settings/applications/defaultapps/DefaultAppPickerFragment;->onAttach(Landroid/content/Context;)V

    iget-object v0, p0, Lcom/android/settings/webview/WebViewAppPicker;->mUserManager:Landroid/os/UserManager;

    invoke-virtual {v0}, Landroid/os/UserManager;->isAdminUser()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/android/settings/webview/WebViewAppPicker;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    :cond_0
    return-void
.end method

.method setWebViewUpdateServiceWrapper(Lcom/android/settings/webview/c;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/webview/WebViewAppPicker;->boI:Lcom/android/settings/webview/c;

    return-void
.end method
