.class public final Lcom/android/settings/utils/f;
.super Ljava/lang/Object;
.source "FileSizeFormatter.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static ayu(Landroid/content/res/Resources;JIJ)Landroid/text/format/Formatter$BytesResult;
    .locals 9

    const/4 v5, 0x0

    const/4 v3, 0x1

    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-gez v0, :cond_2

    move v4, v3

    :goto_0
    if-eqz v4, :cond_0

    neg-long p1, p1

    :cond_0
    long-to-float v0, p1

    long-to-float v1, p4

    div-float/2addr v0, v1

    const-wide/16 v6, 0x1

    cmp-long v1, p4, v6

    if-nez v1, :cond_3

    const-string/jumbo v1, "%.0f"

    move v2, v3

    :goto_1
    if-eqz v4, :cond_1

    neg-float v0, v0

    :cond_1
    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-static {v1, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    int-to-float v3, v2

    mul-float/2addr v0, v3

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    int-to-long v4, v0

    mul-long/2addr v4, p4

    int-to-long v2, v2

    div-long v2, v4, v2

    invoke-virtual {p0, p3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-instance v4, Landroid/text/format/Formatter$BytesResult;

    invoke-direct {v4, v1, v0, v2, v3}, Landroid/text/format/Formatter$BytesResult;-><init>(Ljava/lang/String;Ljava/lang/String;J)V

    return-object v4

    :cond_2
    move v4, v5

    goto :goto_0

    :cond_3
    const/high16 v1, 0x3f800000    # 1.0f

    cmpg-float v1, v0, v1

    if-gez v1, :cond_4

    const/16 v2, 0x64

    const-string/jumbo v1, "%.2f"

    goto :goto_1

    :cond_4
    const/high16 v1, 0x41200000    # 10.0f

    cmpg-float v1, v0, v1

    if-gez v1, :cond_5

    const/16 v2, 0xa

    const-string/jumbo v1, "%.1f"

    goto :goto_1

    :cond_5
    const-string/jumbo v1, "%.0f"

    move v2, v3

    goto :goto_1
.end method

.method public static ayv(Landroid/content/Context;JIJ)Ljava/lang/String;
    .locals 6

    if-nez p0, :cond_0

    const-string/jumbo v0, ""

    return-object v0

    :cond_0
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    move-wide v1, p1

    move v3, p3

    move-wide v4, p4

    invoke-static/range {v0 .. v5}, Lcom/android/settings/utils/f;->ayu(Landroid/content/res/Resources;JIJ)Landroid/text/format/Formatter$BytesResult;

    move-result-object v0

    invoke-static {}, Landroid/text/BidiFormatter;->getInstance()Landroid/text/BidiFormatter;

    move-result-object v1

    invoke-static {p0}, Lcom/android/settings/utils/f;->ayw(Landroid/content/Context;)I

    move-result v2

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    iget-object v4, v0, Landroid/text/format/Formatter$BytesResult;->value:Ljava/lang/String;

    const/4 v5, 0x0

    aput-object v4, v3, v5

    iget-object v0, v0, Landroid/text/format/Formatter$BytesResult;->units:Ljava/lang/String;

    const/4 v4, 0x1

    aput-object v0, v3, v4

    invoke-virtual {p0, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/text/BidiFormatter;->unicodeWrap(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static ayw(Landroid/content/Context;)I
    .locals 4

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const-string/jumbo v1, "fileSizeSuffix"

    const-string/jumbo v2, "string"

    const-string/jumbo v3, "android"

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    return v0
.end method
