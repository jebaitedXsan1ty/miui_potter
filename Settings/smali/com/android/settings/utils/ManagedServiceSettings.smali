.class public abstract Lcom/android/settings/utils/ManagedServiceSettings;
.super Lcom/android/settings/notification/MiuiEmptyTextSettings;
.source "ManagedServiceSettings.java"


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field protected aLY:Landroid/app/NotificationManager;

.field private final aLZ:Lcom/android/settings/utils/b;

.field private aMa:Landroid/app/admin/DevicePolicyManager;

.field private aMb:Landroid/content/pm/PackageManager;

.field protected aMc:Lcom/android/settings/utils/g;

.field protected mContext:Landroid/content/Context;

.field private mIconDrawableFactory:Landroid/util/IconDrawableFactory;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/android/settings/utils/ManagedServiceSettings;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/settings/utils/ManagedServiceSettings;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/android/settings/notification/MiuiEmptyTextSettings;-><init>()V

    invoke-virtual {p0}, Lcom/android/settings/utils/ManagedServiceSettings;->D()Lcom/android/settings/utils/b;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/utils/ManagedServiceSettings;->aLZ:Lcom/android/settings/utils/b;

    return-void
.end method

.method private ayj(I)I
    .locals 1

    const/16 v0, -0x2710

    if-eq p1, v0, :cond_0

    return p1

    :cond_0
    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v0

    return v0
.end method

.method private ayk(Ljava/util/List;)V
    .locals 13

    const/4 v2, 0x0

    const/4 v12, 0x0

    iget-object v0, p0, Lcom/android/settings/utils/ManagedServiceSettings;->mContext:Landroid/content/Context;

    const-string/jumbo v1, "user"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/UserManager;

    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v1

    invoke-static {v0, v1}, Lcom/android/settings/aq;->bqt(Landroid/os/UserManager;I)I

    move-result v3

    invoke-virtual {p0}, Lcom/android/settings/utils/ManagedServiceSettings;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v4

    invoke-virtual {v4}, Landroid/preference/PreferenceScreen;->removeAll()V

    new-instance v0, Landroid/content/pm/PackageItemInfo$DisplayNameComparator;

    iget-object v1, p0, Lcom/android/settings/utils/ManagedServiceSettings;->aMb:Landroid/content/pm/PackageManager;

    invoke-direct {v0, v1}, Landroid/content/pm/PackageItemInfo$DisplayNameComparator;-><init>(Landroid/content/pm/PackageManager;)V

    invoke-static {p1, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/ServiceInfo;

    new-instance v6, Landroid/content/ComponentName;

    iget-object v1, v0, Landroid/content/pm/ServiceInfo;->packageName:Ljava/lang/String;

    iget-object v7, v0, Landroid/content/pm/ServiceInfo;->name:Ljava/lang/String;

    invoke-direct {v6, v1, v7}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    :try_start_0
    iget-object v1, p0, Lcom/android/settings/utils/ManagedServiceSettings;->aMb:Landroid/content/pm/PackageManager;

    iget-object v7, v0, Landroid/content/pm/ServiceInfo;->packageName:Ljava/lang/String;

    invoke-direct {p0, v3}, Lcom/android/settings/utils/ManagedServiceSettings;->ayj(I)I

    move-result v8

    const/4 v9, 0x0

    invoke-virtual {v1, v7, v9, v8}, Landroid/content/pm/PackageManager;->getApplicationInfoAsUser(Ljava/lang/String;II)Landroid/content/pm/ApplicationInfo;

    move-result-object v1

    iget-object v7, p0, Lcom/android/settings/utils/ManagedServiceSettings;->aMb:Landroid/content/pm/PackageManager;

    invoke-virtual {v1, v7}, Landroid/content/pm/ApplicationInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    :goto_1
    iget-object v7, p0, Lcom/android/settings/utils/ManagedServiceSettings;->aMb:Landroid/content/pm/PackageManager;

    invoke-virtual {v0, v7}, Landroid/content/pm/ServiceInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v7

    invoke-interface {v7}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v7

    new-instance v8, Landroid/preference/CheckBoxPreference;

    invoke-virtual {p0}, Lcom/android/settings/utils/ManagedServiceSettings;->bWz()Landroid/content/Context;

    move-result-object v9

    invoke-direct {v8, v9}, Landroid/preference/CheckBoxPreference;-><init>(Landroid/content/Context;)V

    invoke-virtual {v8, v12}, Landroid/preference/CheckBoxPreference;->setPersistent(Z)V

    iget-object v9, p0, Lcom/android/settings/utils/ManagedServiceSettings;->mIconDrawableFactory:Landroid/util/IconDrawableFactory;

    iget-object v10, v0, Landroid/content/pm/ServiceInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget-object v11, v0, Landroid/content/pm/ServiceInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget v11, v11, Landroid/content/pm/ApplicationInfo;->uid:I

    invoke-static {v11}, Landroid/os/UserHandle;->getUserId(I)I

    move-result v11

    invoke-virtual {v9, v0, v10, v11}, Landroid/util/IconDrawableFactory;->getBadgedIcon(Landroid/content/pm/PackageItemInfo;Landroid/content/pm/ApplicationInfo;I)Landroid/graphics/drawable/Drawable;

    move-result-object v9

    invoke-virtual {v8, v9}, Landroid/preference/CheckBoxPreference;->setIcon(Landroid/graphics/drawable/Drawable;)V

    if-eqz v1, :cond_1

    invoke-virtual {v1, v7}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v9

    xor-int/lit8 v9, v9, 0x1

    if-eqz v9, :cond_1

    invoke-virtual {v8, v1}, Landroid/preference/CheckBoxPreference;->setTitle(Ljava/lang/CharSequence;)V

    invoke-virtual {v8, v7}, Landroid/preference/CheckBoxPreference;->setSummary(Ljava/lang/CharSequence;)V

    :goto_2
    invoke-virtual {v6}, Landroid/content/ComponentName;->flattenToString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v8, v1}, Landroid/preference/CheckBoxPreference;->setKey(Ljava/lang/String;)V

    invoke-virtual {p0, v6}, Lcom/android/settings/utils/ManagedServiceSettings;->F(Landroid/content/ComponentName;)Z

    move-result v1

    invoke-virtual {v8, v1}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    const/16 v1, -0x2710

    if-eq v3, v1, :cond_0

    iget-object v1, p0, Lcom/android/settings/utils/ManagedServiceSettings;->aMa:Landroid/app/admin/DevicePolicyManager;

    iget-object v0, v0, Landroid/content/pm/ServiceInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v1, v0, v3}, Landroid/app/admin/DevicePolicyManager;->isNotificationListenerServicePermitted(Ljava/lang/String;I)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    const v0, 0x7f121661

    invoke-virtual {v8, v0}, Landroid/preference/CheckBoxPreference;->setSummary(I)V

    :cond_0
    new-instance v0, Lcom/android/settings/utils/p;

    invoke-direct {v0, p0, v6, v7}, Lcom/android/settings/utils/p;-><init>(Lcom/android/settings/utils/ManagedServiceSettings;Landroid/content/ComponentName;Ljava/lang/String;)V

    invoke-virtual {v8, v0}, Landroid/preference/CheckBoxPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    invoke-virtual {v4, v8}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    goto/16 :goto_0

    :catch_0
    move-exception v1

    sget-object v7, Lcom/android/settings/utils/ManagedServiceSettings;->TAG:Ljava/lang/String;

    const-string/jumbo v8, "can\'t find package name"

    invoke-static {v7, v8, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    move-object v1, v2

    goto :goto_1

    :cond_1
    invoke-virtual {v8, v7}, Landroid/preference/CheckBoxPreference;->setTitle(Ljava/lang/CharSequence;)V

    goto :goto_2

    :cond_2
    return-void
.end method

.method static synthetic ayl(Lcom/android/settings/utils/ManagedServiceSettings;)Lcom/android/settings/utils/b;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/utils/ManagedServiceSettings;->aLZ:Lcom/android/settings/utils/b;

    return-object v0
.end method

.method static synthetic aym(Lcom/android/settings/utils/ManagedServiceSettings;Ljava/util/List;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/settings/utils/ManagedServiceSettings;->ayk(Ljava/util/List;)V

    return-void
.end method


# virtual methods
.method protected C(Landroid/content/ComponentName;)V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/utils/ManagedServiceSettings;->aMc:Lcom/android/settings/utils/g;

    const/4 v1, 0x1

    invoke-virtual {v0, p1, v1}, Lcom/android/settings/utils/g;->ayB(Landroid/content/ComponentName;Z)V

    return-void
.end method

.method protected abstract D()Lcom/android/settings/utils/b;
.end method

.method protected F(Landroid/content/ComponentName;)Z
    .locals 1

    iget-object v0, p0, Lcom/android/settings/utils/ManagedServiceSettings;->aMc:Lcom/android/settings/utils/g;

    invoke-virtual {v0, p1}, Lcom/android/settings/utils/g;->ayA(Landroid/content/ComponentName;)Z

    move-result v0

    return v0
.end method

.method protected G(Landroid/content/ComponentName;Ljava/lang/String;Z)Z
    .locals 4

    const/4 v1, 0x1

    const/4 v3, 0x0

    if-nez p3, :cond_0

    iget-object v0, p0, Lcom/android/settings/utils/ManagedServiceSettings;->aMc:Lcom/android/settings/utils/g;

    invoke-virtual {v0, p1, v3}, Lcom/android/settings/utils/g;->ayB(Landroid/content/ComponentName;Z)V

    return v1

    :cond_0
    iget-object v0, p0, Lcom/android/settings/utils/ManagedServiceSettings;->aMc:Lcom/android/settings/utils/g;

    invoke-virtual {v0, p1}, Lcom/android/settings/utils/g;->ayA(Landroid/content/ComponentName;)Z

    move-result v0

    if-eqz v0, :cond_1

    return v1

    :cond_1
    new-instance v0, Lcom/android/settings/utils/ManagedServiceSettings$ScaryWarningDialogFragment;

    invoke-direct {v0}, Lcom/android/settings/utils/ManagedServiceSettings$ScaryWarningDialogFragment;-><init>()V

    invoke-virtual {v0, p1, p2, p0}, Lcom/android/settings/utils/ManagedServiceSettings$ScaryWarningDialogFragment;->ayn(Landroid/content/ComponentName;Ljava/lang/String;Landroid/app/Fragment;)Lcom/android/settings/utils/ManagedServiceSettings$ScaryWarningDialogFragment;

    move-result-object v0

    invoke-virtual {p0}, Lcom/android/settings/utils/ManagedServiceSettings;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const-string/jumbo v2, "dialog"

    invoke-virtual {v0, v1, v2}, Lcom/android/settings/utils/ManagedServiceSettings$ScaryWarningDialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    return v3
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3

    invoke-super {p0, p1}, Lcom/android/settings/notification/MiuiEmptyTextSettings;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/android/settings/utils/ManagedServiceSettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/utils/ManagedServiceSettings;->mContext:Landroid/content/Context;

    invoke-virtual {p0}, Lcom/android/settings/utils/ManagedServiceSettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/Activity;->setRequestedOrientation(I)V

    iget-object v0, p0, Lcom/android/settings/utils/ManagedServiceSettings;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/utils/ManagedServiceSettings;->aMb:Landroid/content/pm/PackageManager;

    iget-object v0, p0, Lcom/android/settings/utils/ManagedServiceSettings;->mContext:Landroid/content/Context;

    const-string/jumbo v1, "device_policy"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/admin/DevicePolicyManager;

    iput-object v0, p0, Lcom/android/settings/utils/ManagedServiceSettings;->aMa:Landroid/app/admin/DevicePolicyManager;

    iget-object v0, p0, Lcom/android/settings/utils/ManagedServiceSettings;->mContext:Landroid/content/Context;

    const-string/jumbo v1, "notification"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    iput-object v0, p0, Lcom/android/settings/utils/ManagedServiceSettings;->aLY:Landroid/app/NotificationManager;

    iget-object v0, p0, Lcom/android/settings/utils/ManagedServiceSettings;->mContext:Landroid/content/Context;

    invoke-static {v0}, Landroid/util/IconDrawableFactory;->newInstance(Landroid/content/Context;)Landroid/util/IconDrawableFactory;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/utils/ManagedServiceSettings;->mIconDrawableFactory:Landroid/util/IconDrawableFactory;

    new-instance v0, Lcom/android/settings/utils/g;

    iget-object v1, p0, Lcom/android/settings/utils/ManagedServiceSettings;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/android/settings/utils/ManagedServiceSettings;->aLZ:Lcom/android/settings/utils/b;

    invoke-direct {v0, v1, v2}, Lcom/android/settings/utils/g;-><init>(Landroid/content/Context;Lcom/android/settings/utils/b;)V

    iput-object v0, p0, Lcom/android/settings/utils/ManagedServiceSettings;->aMc:Lcom/android/settings/utils/g;

    iget-object v0, p0, Lcom/android/settings/utils/ManagedServiceSettings;->aMc:Lcom/android/settings/utils/g;

    new-instance v1, Lcom/android/settings/utils/o;

    invoke-direct {v1, p0}, Lcom/android/settings/utils/o;-><init>(Lcom/android/settings/utils/ManagedServiceSettings;)V

    invoke-virtual {v0, v1}, Lcom/android/settings/utils/g;->ayx(Lcom/android/settings/utils/h;)V

    invoke-virtual {p0}, Lcom/android/settings/utils/ManagedServiceSettings;->getPreferenceManager()Landroid/preference/PreferenceManager;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/utils/ManagedServiceSettings;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceManager;->createPreferenceScreen(Landroid/content/Context;)Landroid/preference/PreferenceScreen;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/settings/utils/ManagedServiceSettings;->setPreferenceScreen(Landroid/preference/PreferenceScreen;)V

    return-void
.end method

.method public onPause()V
    .locals 2

    invoke-super {p0}, Lcom/android/settings/notification/MiuiEmptyTextSettings;->onPause()V

    iget-object v0, p0, Lcom/android/settings/utils/ManagedServiceSettings;->aMc:Lcom/android/settings/utils/g;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/android/settings/utils/g;->ayz(Z)V

    return-void
.end method

.method public onResume()V
    .locals 2

    invoke-super {p0}, Lcom/android/settings/notification/MiuiEmptyTextSettings;->onResume()V

    invoke-static {}, Landroid/app/ActivityManager;->isLowRamDeviceStatic()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/utils/ManagedServiceSettings;->aMc:Lcom/android/settings/utils/g;

    invoke-virtual {v0}, Lcom/android/settings/utils/g;->ayy()Ljava/util/List;

    iget-object v0, p0, Lcom/android/settings/utils/ManagedServiceSettings;->aMc:Lcom/android/settings/utils/g;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/android/settings/utils/g;->ayz(Z)V

    :goto_0
    return-void

    :cond_0
    const v0, 0x7f1205de

    invoke-virtual {p0, v0}, Lcom/android/settings/utils/ManagedServiceSettings;->bB(I)V

    goto :goto_0
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 1

    invoke-super {p0, p1, p2}, Lcom/android/settings/notification/MiuiEmptyTextSettings;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    iget-object v0, p0, Lcom/android/settings/utils/ManagedServiceSettings;->aLZ:Lcom/android/settings/utils/b;

    iget v0, v0, Lcom/android/settings/utils/b;->aMj:I

    invoke-virtual {p0, v0}, Lcom/android/settings/utils/ManagedServiceSettings;->bB(I)V

    return-void
.end method
