.class public Lcom/android/settings/MiuiLabSettings;
.super Lcom/android/settings/MiuiSettingsPreferenceFragment;
.source "MiuiLabSettings.java"


# instance fields
.field private bVL:Lmiui/preference/ValuePreference;

.field private bVM:I

.field private bVN:Lmiui/preference/ValuePreference;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/MiuiSettingsPreferenceFragment;-><init>()V

    return-void
.end method

.method static synthetic bPN(Lcom/android/settings/MiuiLabSettings;)Lmiui/preference/ValuePreference;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/MiuiLabSettings;->bVL:Lmiui/preference/ValuePreference;

    return-object v0
.end method

.method static synthetic bPO(Lcom/android/settings/MiuiLabSettings;)I
    .locals 1

    iget v0, p0, Lcom/android/settings/MiuiLabSettings;->bVM:I

    return v0
.end method

.method static synthetic bPP(Lcom/android/settings/MiuiLabSettings;I)I
    .locals 0

    iput p1, p0, Lcom/android/settings/MiuiLabSettings;->bVM:I

    return p1
.end method


# virtual methods
.method public cC()Ljava/lang/String;
    .locals 1

    const-class v0, Lcom/android/settings/MiuiLabSettings;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public onInflateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 6

    const/4 v5, 0x1

    const/4 v4, 0x0

    const v0, 0x7f0d00f8

    invoke-virtual {p1, v0, p2, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    const v0, 0x7f150084

    invoke-virtual {p0, v0}, Lcom/android/settings/MiuiLabSettings;->addPreferencesFromResource(I)V

    invoke-virtual {p0}, Lcom/android/settings/MiuiLabSettings;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    const-string/jumbo v1, "miui_lab_features"

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceCategory;

    invoke-virtual {p0}, Lcom/android/settings/MiuiLabSettings;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v1

    const-string/jumbo v3, "miui_lab_taplus"

    invoke-virtual {v1, v3}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    check-cast v1, Lmiui/preference/ValuePreference;

    iput-object v1, p0, Lcom/android/settings/MiuiLabSettings;->bVN:Lmiui/preference/ValuePreference;

    invoke-virtual {p0}, Lcom/android/settings/MiuiLabSettings;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v1

    const-string/jumbo v3, "miui_lab_gallery_search"

    invoke-virtual {v1, v3}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    check-cast v1, Lmiui/preference/ValuePreference;

    iput-object v1, p0, Lcom/android/settings/MiuiLabSettings;->bVL:Lmiui/preference/ValuePreference;

    iget-object v1, p0, Lcom/android/settings/MiuiLabSettings;->bVN:Lmiui/preference/ValuePreference;

    if-eqz v1, :cond_1

    new-instance v1, Landroid/content/Intent;

    const-string/jumbo v3, "com.miui.contentextension.action.TAPLUS_SETTINGS"

    invoke-direct {v1, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/android/settings/MiuiLabSettings;->bWA()Landroid/content/pm/PackageManager;

    move-result-object v3

    invoke-virtual {v3, v1, v4}, Landroid/content/pm/PackageManager;->resolveActivity(Landroid/content/Intent;I)Landroid/content/pm/ResolveInfo;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, v1, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    if-nez v1, :cond_3

    :cond_0
    iget-object v1, p0, Lcom/android/settings/MiuiLabSettings;->bVN:Lmiui/preference/ValuePreference;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceCategory;->removePreference(Landroid/preference/Preference;)Z

    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/android/settings/MiuiLabSettings;->bVL:Lmiui/preference/ValuePreference;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/settings/MiuiLabSettings;->bVL:Lmiui/preference/ValuePreference;

    invoke-virtual {v0, v5}, Lmiui/preference/ValuePreference;->setShowRightArrow(Z)V

    const/4 v0, -0x1

    iput v0, p0, Lcom/android/settings/MiuiLabSettings;->bVM:I

    :cond_2
    return-object v2

    :cond_3
    iget-object v0, p0, Lcom/android/settings/MiuiLabSettings;->bVN:Lmiui/preference/ValuePreference;

    invoke-virtual {v0, v5}, Lmiui/preference/ValuePreference;->setShowRightArrow(Z)V

    goto :goto_0
.end method

.method public onPreferenceTreeClick(Landroid/preference/PreferenceScreen;Landroid/preference/Preference;)Z
    .locals 3

    invoke-virtual {p2}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "miui_lab_taplus"

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v1, "com.miui.contentextension.action.TAPLUS_SETTINGS"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string/jumbo v1, "com.miui.contentextension"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Lcom/android/settings/MiuiLabSettings;->startActivity(Landroid/content/Intent;)V

    :cond_0
    :goto_0
    invoke-super {p0, p1, p2}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->onPreferenceTreeClick(Landroid/preference/PreferenceScreen;Landroid/preference/Preference;)Z

    move-result v0

    return v0

    :cond_1
    const-string/jumbo v1, "miui_lab_gallery_search"

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string/jumbo v1, "com.miui.gallery"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    iget v1, p0, Lcom/android/settings/MiuiLabSettings;->bVM:I

    packed-switch v1, :pswitch_data_0

    const-string/jumbo v0, "MiuiLabSettings"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Gallery search, unknown status:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/android/settings/MiuiLabSettings;->bVM:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x1

    return v0

    :pswitch_0
    const-string/jumbo v1, "miui.intent.action.APP_SETTINGS"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const-string/jumbo v1, "com.miui.gallery"

    const-string/jumbo v2, "com.miui.gallery.activity.GallerySettingsActivity"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    :goto_1
    invoke-virtual {p0, v0}, Lcom/android/settings/MiuiLabSettings;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    :pswitch_1
    const-string/jumbo v1, "com.miui.gallery.action.VIEW_SEARCH_SETTINGS"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onResume()V
    .locals 3

    const/4 v2, 0x0

    invoke-super {p0}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->onResume()V

    invoke-virtual {p0}, Lcom/android/settings/MiuiLabSettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/settings/MiuiLabSettings;->bVN:Lmiui/preference/ValuePreference;

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/android/settings/MiuiLabSettings;->bWB()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v1, "key_enable_taplus"

    invoke-static {v0, v1, v2}, Landroid/provider/MiuiSettings$System;->getBoolean(Landroid/content/ContentResolver;Ljava/lang/String;Z)Z

    move-result v0

    iget-object v1, p0, Lcom/android/settings/MiuiLabSettings;->bVN:Lmiui/preference/ValuePreference;

    if-eqz v0, :cond_3

    const v0, 0x7f120a9b

    :goto_0
    invoke-virtual {v1, v0}, Lmiui/preference/ValuePreference;->setValue(I)V

    :cond_1
    iget-object v0, p0, Lcom/android/settings/MiuiLabSettings;->bVL:Lmiui/preference/ValuePreference;

    if-eqz v0, :cond_2

    new-instance v0, Lcom/android/settings/ca;

    invoke-virtual {p0}, Lcom/android/settings/MiuiLabSettings;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/android/settings/ca;-><init>(Lcom/android/settings/MiuiLabSettings;Landroid/content/Context;)V

    new-array v1, v2, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/android/settings/ca;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    :cond_2
    return-void

    :cond_3
    const v0, 0x7f120a9a

    goto :goto_0
.end method
