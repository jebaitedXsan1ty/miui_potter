.class public Lcom/android/settings/wfd/WifiDisplayStatusActivity;
.super Lmiui/app/Activity;
.source "WifiDisplayStatusActivity.java"


# instance fields
.field private bnH:Lmiui/app/AlertDialog;

.field private bnI:Landroid/hardware/display/DisplayManager;

.field private final bnJ:Landroid/content/BroadcastReceiver;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lmiui/app/Activity;-><init>()V

    new-instance v0, Lcom/android/settings/wfd/z;

    invoke-direct {v0, p0}, Lcom/android/settings/wfd/z;-><init>(Lcom/android/settings/wfd/WifiDisplayStatusActivity;)V

    iput-object v0, p0, Lcom/android/settings/wfd/WifiDisplayStatusActivity;->bnJ:Landroid/content/BroadcastReceiver;

    return-void
.end method

.method private bbR()V
    .locals 3

    new-instance v0, Lmiui/app/AlertDialog$Builder;

    const/4 v1, 0x3

    invoke-direct {v0, p0, v1}, Lmiui/app/AlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    const v1, 0x7f1214b9

    invoke-virtual {p0, v1}, Lcom/android/settings/wfd/WifiDisplayStatusActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lmiui/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Lmiui/app/AlertDialog$Builder;

    const v1, 0x7f1214b8

    invoke-virtual {p0, v1}, Lcom/android/settings/wfd/WifiDisplayStatusActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lmiui/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Lmiui/app/AlertDialog$Builder;

    new-instance v1, Lcom/android/settings/wfd/A;

    invoke-direct {v1, p0}, Lcom/android/settings/wfd/A;-><init>(Lcom/android/settings/wfd/WifiDisplayStatusActivity;)V

    invoke-virtual {v0, v1}, Lmiui/app/AlertDialog$Builder;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)Lmiui/app/AlertDialog$Builder;

    const v1, 0x7f1214b7

    invoke-virtual {p0, v1}, Lcom/android/settings/wfd/WifiDisplayStatusActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/android/settings/wfd/B;

    invoke-direct {v2, p0}, Lcom/android/settings/wfd/B;-><init>(Lcom/android/settings/wfd/WifiDisplayStatusActivity;)V

    invoke-virtual {v0, v1, v2}, Lmiui/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Lmiui/app/AlertDialog$Builder;

    const v1, 0x7f1205f1

    invoke-virtual {p0, v1}, Lcom/android/settings/wfd/WifiDisplayStatusActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/android/settings/wfd/C;

    invoke-direct {v2, p0}, Lcom/android/settings/wfd/C;-><init>(Lcom/android/settings/wfd/WifiDisplayStatusActivity;)V

    invoke-virtual {v0, v1, v2}, Lmiui/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Lmiui/app/AlertDialog$Builder;

    invoke-virtual {v0}, Lmiui/app/AlertDialog$Builder;->create()Lmiui/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/wfd/WifiDisplayStatusActivity;->bnH:Lmiui/app/AlertDialog;

    iget-object v0, p0, Lcom/android/settings/wfd/WifiDisplayStatusActivity;->bnH:Lmiui/app/AlertDialog;

    invoke-virtual {v0}, Lmiui/app/AlertDialog;->show()V

    return-void
.end method

.method static synthetic bbS(Lcom/android/settings/wfd/WifiDisplayStatusActivity;)Lmiui/app/AlertDialog;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/wfd/WifiDisplayStatusActivity;->bnH:Lmiui/app/AlertDialog;

    return-object v0
.end method

.method static synthetic bbT(Lcom/android/settings/wfd/WifiDisplayStatusActivity;)Landroid/hardware/display/DisplayManager;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/wfd/WifiDisplayStatusActivity;->bnI:Landroid/hardware/display/DisplayManager;

    return-object v0
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2

    const-string/jumbo v0, "WifiDisplayStatus"

    const-string/jumbo v1, "onCreate"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-super {p0, p1}, Lmiui/app/Activity;->onCreate(Landroid/os/Bundle;)V

    const-string/jumbo v0, "display"

    invoke-virtual {p0, v0}, Lcom/android/settings/wfd/WifiDisplayStatusActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/display/DisplayManager;

    iput-object v0, p0, Lcom/android/settings/wfd/WifiDisplayStatusActivity;->bnI:Landroid/hardware/display/DisplayManager;

    return-void
.end method

.method protected onStart()V
    .locals 2

    const-string/jumbo v0, "WifiDisplayStatus"

    const-string/jumbo v1, "onStart"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-super {p0}, Lmiui/app/Activity;->onStart()V

    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    const-string/jumbo v1, "android.hardware.display.action.WIFI_DISPLAY_STATUS_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/android/settings/wfd/WifiDisplayStatusActivity;->bnJ:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1, v0}, Lcom/android/settings/wfd/WifiDisplayStatusActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    invoke-direct {p0}, Lcom/android/settings/wfd/WifiDisplayStatusActivity;->bbR()V

    return-void
.end method

.method protected onStop()V
    .locals 3

    const/4 v2, 0x0

    const-string/jumbo v0, "WifiDisplayStatus"

    const-string/jumbo v1, "onStop"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-super {p0}, Lmiui/app/Activity;->onStop()V

    iget-object v0, p0, Lcom/android/settings/wfd/WifiDisplayStatusActivity;->bnJ:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/android/settings/wfd/WifiDisplayStatusActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    iget-object v0, p0, Lcom/android/settings/wfd/WifiDisplayStatusActivity;->bnH:Lmiui/app/AlertDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/wfd/WifiDisplayStatusActivity;->bnH:Lmiui/app/AlertDialog;

    invoke-virtual {v0}, Lmiui/app/AlertDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/wfd/WifiDisplayStatusActivity;->bnH:Lmiui/app/AlertDialog;

    invoke-virtual {v0}, Lmiui/app/AlertDialog;->dismiss()V

    :cond_0
    iput-object v2, p0, Lcom/android/settings/wfd/WifiDisplayStatusActivity;->bnH:Lmiui/app/AlertDialog;

    return-void
.end method
