.class public Lcom/android/settings/wfd/MiuiWifiDisplaySettings;
.super Lcom/android/settings/wfd/WifiDisplaySettings;
.source "MiuiWifiDisplaySettings.java"


# instance fields
.field private bnd:Landroid/net/wifi/WifiManager;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/wfd/WifiDisplaySettings;-><init>()V

    return-void
.end method

.method private bbb(Z)V
    .locals 3

    iput-boolean p1, p0, Lcom/android/settings/wfd/MiuiWifiDisplaySettings;->bne:Z

    invoke-virtual {p0}, Lcom/android/settings/wfd/MiuiWifiDisplaySettings;->bWB()Landroid/content/ContentResolver;

    move-result-object v1

    const-string/jumbo v2, "wifi_display_on"

    iget-boolean v0, p0, Lcom/android/settings/wfd/MiuiWifiDisplaySettings;->bne:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v1, v2, v0}, Landroid/provider/Settings$Global;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    const-string/jumbo v0, "enable_wifi_display"

    invoke-virtual {p0, v0}, Lcom/android/settings/wfd/MiuiWifiDisplaySettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    iget-boolean v1, p0, Lcom/android/settings/wfd/MiuiWifiDisplaySettings;->bne:Z

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private bbc()V
    .locals 4

    const/4 v3, 0x0

    const-string/jumbo v0, "wifip2p"

    invoke-virtual {p0, v0}, Lcom/android/settings/wfd/MiuiWifiDisplaySettings;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/p2p/WifiP2pManager;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/android/settings/wfd/MiuiWifiDisplaySettings;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {p0}, Lcom/android/settings/wfd/MiuiWifiDisplaySettings;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Activity;->getMainLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/net/wifi/p2p/WifiP2pManager;->initialize(Landroid/content/Context;Landroid/os/Looper;Landroid/net/wifi/p2p/WifiP2pManager$ChannelListener;)Landroid/net/wifi/p2p/WifiP2pManager$Channel;

    move-result-object v1

    invoke-virtual {p0}, Lcom/android/settings/wfd/MiuiWifiDisplaySettings;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-static {v2}, Lcom/android/settings/dc;->bYH(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/net/wifi/p2p/WifiP2pManager;->setDeviceName(Landroid/net/wifi/p2p/WifiP2pManager$Channel;Ljava/lang/String;Landroid/net/wifi/p2p/WifiP2pManager$ActionListener;)V

    :cond_0
    return-void
.end method

.method private bbd()V
    .locals 2

    const/16 v1, 0x2710

    iget-boolean v0, p0, Lcom/android/settings/wfd/MiuiWifiDisplaySettings;->bne:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/settings/wfd/MiuiWifiDisplaySettings;->bnd:Landroid/net/wifi/WifiManager;

    invoke-virtual {v0}, Landroid/net/wifi/WifiManager;->isWifiEnabled()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0, v1}, Lcom/android/settings/wfd/MiuiWifiDisplaySettings;->bWS(I)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    invoke-virtual {p0, v1}, Lcom/android/settings/wfd/MiuiWifiDisplaySettings;->alW(I)V

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/android/settings/wfd/MiuiWifiDisplaySettings;->bbb(Z)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0, v1}, Lcom/android/settings/wfd/MiuiWifiDisplaySettings;->bWO(I)V

    goto :goto_0
.end method

.method static synthetic bbe(Lcom/android/settings/wfd/MiuiWifiDisplaySettings;)Landroid/net/wifi/WifiManager;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/wfd/MiuiWifiDisplaySettings;->bnd:Landroid/net/wifi/WifiManager;

    return-object v0
.end method

.method static synthetic bbf(Lcom/android/settings/wfd/MiuiWifiDisplaySettings;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/settings/wfd/MiuiWifiDisplaySettings;->bbb(Z)V

    return-void
.end method


# virtual methods
.method public at(Landroid/preference/Preference;)Z
    .locals 4

    const/4 v1, 0x1

    const-string/jumbo v0, "enable_wifi_display"

    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    check-cast p1, Landroid/preference/CheckBoxPreference;

    invoke-virtual {p1}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/settings/wfd/MiuiWifiDisplaySettings;->bne:Z

    invoke-virtual {p0}, Lcom/android/settings/wfd/MiuiWifiDisplaySettings;->bWB()Landroid/content/ContentResolver;

    move-result-object v2

    const-string/jumbo v3, "wifi_display_on"

    iget-boolean v0, p0, Lcom/android/settings/wfd/MiuiWifiDisplaySettings;->bne:Z

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v2, v3, v0}, Landroid/provider/Settings$Global;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    invoke-direct {p0}, Lcom/android/settings/wfd/MiuiWifiDisplaySettings;->bbd()V

    return v1

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    invoke-super {p0, p1}, Lcom/android/settings/wfd/WifiDisplaySettings;->at(Landroid/preference/Preference;)Z

    move-result v0

    return v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1

    invoke-direct {p0}, Lcom/android/settings/wfd/MiuiWifiDisplaySettings;->bbc()V

    const-string/jumbo v0, "wifi"

    invoke-virtual {p0, v0}, Lcom/android/settings/wfd/MiuiWifiDisplaySettings;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/WifiManager;

    iput-object v0, p0, Lcom/android/settings/wfd/MiuiWifiDisplaySettings;->bnd:Landroid/net/wifi/WifiManager;

    invoke-super {p0, p1}, Lcom/android/settings/wfd/WifiDisplaySettings;->onCreate(Landroid/os/Bundle;)V

    return-void
.end method

.method public onCreateDialog(I)Landroid/app/Dialog;
    .locals 3

    packed-switch p1, :pswitch_data_0

    invoke-super {p0, p1}, Lcom/android/settings/wfd/WifiDisplaySettings;->onCreateDialog(I)Landroid/app/Dialog;

    move-result-object v0

    return-object v0

    :pswitch_0
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/android/settings/wfd/MiuiWifiDisplaySettings;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v1, 0x7f1214b0

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f1205f7

    invoke-virtual {p0, v1}, Lcom/android/settings/wfd/MiuiWifiDisplaySettings;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/android/settings/wfd/e;

    invoke-direct {v2, p0}, Lcom/android/settings/wfd/e;-><init>(Lcom/android/settings/wfd/MiuiWifiDisplaySettings;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f1205f1

    invoke-virtual {p0, v1}, Lcom/android/settings/wfd/MiuiWifiDisplaySettings;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/android/settings/wfd/f;

    invoke-direct {v2, p0}, Lcom/android/settings/wfd/f;-><init>(Lcom/android/settings/wfd/MiuiWifiDisplaySettings;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    return-object v0

    nop

    :pswitch_data_0
    .packed-switch 0x2710
        :pswitch_0
    .end packed-switch
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V
    .locals 0

    return-void
.end method

.method public onStart()V
    .locals 0

    invoke-super {p0}, Lcom/android/settings/wfd/WifiDisplaySettings;->onStart()V

    invoke-direct {p0}, Lcom/android/settings/wfd/MiuiWifiDisplaySettings;->bbd()V

    return-void
.end method
