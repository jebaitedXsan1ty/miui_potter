.class final Lcom/android/settings/wfd/z;
.super Landroid/content/BroadcastReceiver;
.source "WifiDisplayStatusActivity.java"


# instance fields
.field final synthetic boj:Lcom/android/settings/wfd/WifiDisplayStatusActivity;


# direct methods
.method constructor <init>(Lcom/android/settings/wfd/WifiDisplayStatusActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/wfd/z;->boj:Lcom/android/settings/wfd/WifiDisplayStatusActivity;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 2

    const-string/jumbo v0, "WifiDisplayStatus"

    const-string/jumbo v1, "Receive ACTION_WIFI_DISPLAY_STATUS_CHANGED"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string/jumbo v0, "android.hardware.display.extra.WIFI_DISPLAY_STATUS"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/hardware/display/WifiDisplayStatus;

    iget-object v1, p0, Lcom/android/settings/wfd/z;->boj:Lcom/android/settings/wfd/WifiDisplayStatusActivity;

    invoke-static {v1}, Lcom/android/settings/wfd/WifiDisplayStatusActivity;->bbS(Lcom/android/settings/wfd/WifiDisplayStatusActivity;)Lmiui/app/AlertDialog;

    move-result-object v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/android/settings/wfd/z;->boj:Lcom/android/settings/wfd/WifiDisplayStatusActivity;

    invoke-static {v1}, Lcom/android/settings/wfd/WifiDisplayStatusActivity;->bbS(Lcom/android/settings/wfd/WifiDisplayStatusActivity;)Lmiui/app/AlertDialog;

    move-result-object v1

    invoke-virtual {v1}, Lmiui/app/AlertDialog;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_1

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/hardware/display/WifiDisplayStatus;->getActiveDisplayState()I

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/android/settings/wfd/z;->boj:Lcom/android/settings/wfd/WifiDisplayStatusActivity;

    invoke-static {v0}, Lcom/android/settings/wfd/WifiDisplayStatusActivity;->bbS(Lcom/android/settings/wfd/WifiDisplayStatusActivity;)Lmiui/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Lmiui/app/AlertDialog;->dismiss()V

    :cond_1
    return-void
.end method
