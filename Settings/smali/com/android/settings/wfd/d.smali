.class Lcom/android/settings/wfd/d;
.super Ljava/lang/Object;
.source "WifiDisplaySettings.java"

# interfaces
.implements Lcom/android/settings/dashboard/D;


# instance fields
.field private final bnE:Landroid/media/MediaRouter;

.field private final bnF:Landroid/media/MediaRouter$Callback;

.field private final bnG:Lcom/android/settings/dashboard/C;

.field private final mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/android/settings/dashboard/C;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/android/settings/wfd/g;

    invoke-direct {v0, p0}, Lcom/android/settings/wfd/g;-><init>(Lcom/android/settings/wfd/d;)V

    iput-object v0, p0, Lcom/android/settings/wfd/d;->bnF:Landroid/media/MediaRouter$Callback;

    iput-object p1, p0, Lcom/android/settings/wfd/d;->mContext:Landroid/content/Context;

    iput-object p2, p0, Lcom/android/settings/wfd/d;->bnG:Lcom/android/settings/dashboard/C;

    const-string/jumbo v0, "media_router"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/MediaRouter;

    iput-object v0, p0, Lcom/android/settings/wfd/d;->bnE:Landroid/media/MediaRouter;

    return-void
.end method

.method static synthetic bbQ(Lcom/android/settings/wfd/d;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/wfd/d;->updateSummary()V

    return-void
.end method

.method private updateSummary()V
    .locals 5

    iget-object v0, p0, Lcom/android/settings/wfd/d;->mContext:Landroid/content/Context;

    const v1, 0x7f1205e0

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/wfd/d;->bnE:Landroid/media/MediaRouter;

    invoke-virtual {v1}, Landroid/media/MediaRouter;->getRouteCount()I

    move-result v2

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v2, :cond_0

    iget-object v3, p0, Lcom/android/settings/wfd/d;->bnE:Landroid/media/MediaRouter;

    invoke-virtual {v3, v1}, Landroid/media/MediaRouter;->getRouteAt(I)Landroid/media/MediaRouter$RouteInfo;

    move-result-object v3

    const/4 v4, 0x4

    invoke-virtual {v3, v4}, Landroid/media/MediaRouter$RouteInfo;->matchesTypes(I)Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-virtual {v3}, Landroid/media/MediaRouter$RouteInfo;->isSelected()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-virtual {v3}, Landroid/media/MediaRouter$RouteInfo;->isConnecting()Z

    move-result v3

    xor-int/lit8 v3, v3, 0x1

    if-eqz v3, :cond_1

    iget-object v0, p0, Lcom/android/settings/wfd/d;->mContext:Landroid/content/Context;

    const v1, 0x7f12151d

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    :cond_0
    iget-object v1, p0, Lcom/android/settings/wfd/d;->bnG:Lcom/android/settings/dashboard/C;

    invoke-virtual {v1, p0, v0}, Lcom/android/settings/dashboard/C;->Fd(Lcom/android/settings/dashboard/D;Ljava/lang/CharSequence;)V

    return-void

    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method


# virtual methods
.method public jt(Z)V
    .locals 3

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/android/settings/wfd/d;->bnE:Landroid/media/MediaRouter;

    iget-object v1, p0, Lcom/android/settings/wfd/d;->bnF:Landroid/media/MediaRouter$Callback;

    const/4 v2, 0x4

    invoke-virtual {v0, v2, v1}, Landroid/media/MediaRouter;->addCallback(ILandroid/media/MediaRouter$Callback;)V

    invoke-direct {p0}, Lcom/android/settings/wfd/d;->updateSummary()V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/settings/wfd/d;->bnE:Landroid/media/MediaRouter;

    iget-object v1, p0, Lcom/android/settings/wfd/d;->bnF:Landroid/media/MediaRouter$Callback;

    invoke-virtual {v0, v1}, Landroid/media/MediaRouter;->removeCallback(Landroid/media/MediaRouter$Callback;)V

    goto :goto_0
.end method
