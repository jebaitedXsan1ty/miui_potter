.class public Lcom/android/settings/privacypassword/SetPrivacyPasswordChooseAccessControl;
.super Lcom/android/settings/privacypassword/PrivacyPasswordChooseAccessControl;
.source "SetPrivacyPasswordChooseAccessControl.java"


# instance fields
.field private afA:Z

.field private afB:Lcom/android/settings/privacypassword/PrivacyPasswordManager;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/android/settings/privacypassword/PrivacyPasswordChooseAccessControl;-><init>()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/settings/privacypassword/SetPrivacyPasswordChooseAccessControl;->afA:Z

    return-void
.end method


# virtual methods
.method protected Wo()Z
    .locals 2

    invoke-virtual {p0}, Lcom/android/settings/privacypassword/SetPrivacyPasswordChooseAccessControl;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "extra_data"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string/jumbo v1, "choose_suspend"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public finish()V
    .locals 1

    invoke-virtual {p0}, Lcom/android/settings/privacypassword/SetPrivacyPasswordChooseAccessControl;->Wo()Z

    move-result v0

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/android/settings/privacypassword/SetPrivacyPasswordChooseAccessControl;->afA:Z

    if-nez v0, :cond_1

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/android/settings/privacypassword/SetPrivacyPasswordChooseAccessControl;->setResult(I)V

    :cond_0
    :goto_0
    invoke-super {p0}, Lcom/android/settings/privacypassword/PrivacyPasswordChooseAccessControl;->finish()V

    return-void

    :cond_1
    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lcom/android/settings/privacypassword/SetPrivacyPasswordChooseAccessControl;->setResult(I)V

    goto :goto_0
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 1

    invoke-super {p0, p1, p2, p3}, Lcom/android/settings/privacypassword/PrivacyPasswordChooseAccessControl;->onActivityResult(IILandroid/content/Intent;)V

    packed-switch p1, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/settings/privacypassword/SetPrivacyPasswordChooseAccessControl;->afA:Z

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/settings/privacypassword/SetPrivacyPasswordChooseAccessControl;->afA:Z

    invoke-virtual {p0}, Lcom/android/settings/privacypassword/SetPrivacyPasswordChooseAccessControl;->finish()V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x46dc1
        :pswitch_0
    .end packed-switch
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1

    invoke-super {p0, p1}, Lcom/android/settings/privacypassword/PrivacyPasswordChooseAccessControl;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/android/settings/privacypassword/SetPrivacyPasswordChooseAccessControl;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/android/settings/privacypassword/PrivacyPasswordManager;->getInstance(Landroid/content/Context;)Lcom/android/settings/privacypassword/PrivacyPasswordManager;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/privacypassword/SetPrivacyPasswordChooseAccessControl;->afB:Lcom/android/settings/privacypassword/PrivacyPasswordManager;

    iget-object v0, p0, Lcom/android/settings/privacypassword/SetPrivacyPasswordChooseAccessControl;->afB:Lcom/android/settings/privacypassword/PrivacyPasswordManager;

    invoke-virtual {v0}, Lcom/android/settings/privacypassword/PrivacyPasswordManager;->Xs()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/settings/privacypassword/SetPrivacyPasswordChooseAccessControl;->afA:Z

    :cond_0
    return-void
.end method

.method public onResume()V
    .locals 1

    invoke-super {p0}, Lcom/android/settings/privacypassword/PrivacyPasswordChooseAccessControl;->onResume()V

    iget-object v0, p0, Lcom/android/settings/privacypassword/SetPrivacyPasswordChooseAccessControl;->afB:Lcom/android/settings/privacypassword/PrivacyPasswordManager;

    invoke-virtual {v0}, Lcom/android/settings/privacypassword/PrivacyPasswordManager;->Xs()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/android/settings/privacypassword/SetPrivacyPasswordChooseAccessControl;->Wo()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/android/settings/privacypassword/SetPrivacyPasswordChooseAccessControl;->finish()V

    :cond_0
    return-void
.end method

.method protected onStart()V
    .locals 3

    invoke-virtual {p0}, Lcom/android/settings/privacypassword/SetPrivacyPasswordChooseAccessControl;->Wo()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/privacypassword/SetPrivacyPasswordChooseAccessControl;->afB:Lcom/android/settings/privacypassword/PrivacyPasswordManager;

    invoke-virtual {v0}, Lcom/android/settings/privacypassword/PrivacyPasswordManager;->Xs()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/android/settings/privacypassword/SetPrivacyPasswordChooseAccessControl;->afA:Z

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/android/settings/privacypassword/PrivacyPasswordConfirmAccessControl;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v1, "enter_from_settings"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const v1, 0x46dc1

    invoke-virtual {p0, v0, v1}, Lcom/android/settings/privacypassword/SetPrivacyPasswordChooseAccessControl;->startActivityForResult(Landroid/content/Intent;I)V

    :cond_0
    invoke-super {p0}, Lcom/android/settings/privacypassword/PrivacyPasswordChooseAccessControl;->onStart()V

    return-void
.end method

.method protected onStop()V
    .locals 1

    iget-boolean v0, p0, Lcom/android/settings/privacypassword/SetPrivacyPasswordChooseAccessControl;->afA:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/settings/privacypassword/SetPrivacyPasswordChooseAccessControl;->afA:Z

    :cond_0
    invoke-super {p0}, Lcom/android/settings/privacypassword/PrivacyPasswordChooseAccessControl;->onStop()V

    return-void
.end method
