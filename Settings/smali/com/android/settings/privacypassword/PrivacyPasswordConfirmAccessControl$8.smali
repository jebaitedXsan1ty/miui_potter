.class final Lcom/android/settings/privacypassword/PrivacyPasswordConfirmAccessControl$8;
.super Ljava/lang/Object;
.source "PrivacyPasswordConfirmAccessControl.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field final synthetic aiq:Lcom/android/settings/privacypassword/PrivacyPasswordConfirmAccessControl;


# direct methods
.method constructor <init>(Lcom/android/settings/privacypassword/PrivacyPasswordConfirmAccessControl;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/privacypassword/PrivacyPasswordConfirmAccessControl$8;->aiq:Lcom/android/settings/privacypassword/PrivacyPasswordConfirmAccessControl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 4

    iget-object v0, p0, Lcom/android/settings/privacypassword/PrivacyPasswordConfirmAccessControl$8;->aiq:Lcom/android/settings/privacypassword/PrivacyPasswordConfirmAccessControl;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/android/settings/privacypassword/PrivacyPasswordConfirmAccessControl;->Xg(Lcom/android/settings/privacypassword/PrivacyPasswordConfirmAccessControl;Z)Z

    check-cast p1, Lmiui/app/AlertDialog;

    invoke-virtual {p1}, Lmiui/app/AlertDialog;->isChecked()Z

    move-result v0

    iget-object v1, p0, Lcom/android/settings/privacypassword/PrivacyPasswordConfirmAccessControl$8;->aiq:Lcom/android/settings/privacypassword/PrivacyPasswordConfirmAccessControl;

    iget-object v1, v1, Lcom/android/settings/privacypassword/PrivacyPasswordConfirmAccessControl;->agf:Lcom/android/settings/privacypassword/PrivacyPasswordManager;

    invoke-virtual {v1, v0}, Lcom/android/settings/privacypassword/PrivacyPasswordManager;->XD(Z)V

    new-instance v1, Landroid/content/Intent;

    iget-object v2, p0, Lcom/android/settings/privacypassword/PrivacyPasswordConfirmAccessControl$8;->aiq:Lcom/android/settings/privacypassword/PrivacyPasswordConfirmAccessControl;

    const-class v3, Lcom/android/settings/privacypassword/TransparentHelper;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v2, "bind_account_extra"

    const-string/jumbo v3, "bind_account"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    iget-object v2, p0, Lcom/android/settings/privacypassword/PrivacyPasswordConfirmAccessControl$8;->aiq:Lcom/android/settings/privacypassword/PrivacyPasswordConfirmAccessControl;

    invoke-virtual {v2, v1}, Lcom/android/settings/privacypassword/PrivacyPasswordConfirmAccessControl;->startActivity(Landroid/content/Intent;)V

    if-eqz v0, :cond_0

    const-string/jumbo v0, "binding_forever"

    invoke-static {v0}, Lcom/android/settings/privacypassword/a/a;->Wd(Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    const-string/jumbo v0, "binding"

    invoke-static {v0}, Lcom/android/settings/privacypassword/a/a;->Wd(Ljava/lang/String;)V

    goto :goto_0
.end method
