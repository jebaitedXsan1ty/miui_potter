.class public Lcom/android/settings/privacypassword/BussinessPackageInfoCache;
.super Ljava/lang/Object;
.source "BussinessPackageInfoCache.java"


# static fields
.field private static afw:Ljava/util/Map;

.field private static afx:Ljava/util/Map;

.field private static afy:Ljava/util/Map;


# direct methods
.method static constructor <clinit>()V
    .locals 12

    const v11, 0x7f120d43

    const v1, 0x7f120d3e

    const v10, 0x7f120d3c

    const v9, 0x7f120d3b

    const/4 v5, 0x0

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/android/settings/privacypassword/BussinessPackageInfoCache;->afw:Ljava/util/Map;

    sget-object v0, Lcom/android/settings/privacypassword/BussinessPackageInfoCache;->afw:Ljava/util/Map;

    const-string/jumbo v2, "com.android.mms"

    new-instance v3, Lcom/android/settings/privacypassword/BussinessPackageInfo;

    const-string/jumbo v4, "privacy_mms"

    invoke-direct {v3, v1, v4}, Lcom/android/settings/privacypassword/BussinessPackageInfo;-><init>(ILjava/lang/String;)V

    invoke-interface {v0, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/android/settings/privacypassword/BussinessPackageInfoCache;->afw:Ljava/util/Map;

    const-string/jumbo v2, "com.miui.gallery"

    new-instance v3, Lcom/android/settings/privacypassword/BussinessPackageInfo;

    const-string/jumbo v4, "privacy_gallery"

    invoke-direct {v3, v10, v4}, Lcom/android/settings/privacypassword/BussinessPackageInfo;-><init>(ILjava/lang/String;)V

    invoke-interface {v0, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/android/settings/privacypassword/BussinessPackageInfoCache;->afw:Ljava/util/Map;

    const-string/jumbo v2, "com.android.fileexplorer"

    new-instance v3, Lcom/android/settings/privacypassword/BussinessPackageInfo;

    const-string/jumbo v4, "privacy_file"

    invoke-direct {v3, v9, v4}, Lcom/android/settings/privacypassword/BussinessPackageInfo;-><init>(ILjava/lang/String;)V

    invoke-interface {v0, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/android/settings/privacypassword/BussinessPackageInfoCache;->afw:Ljava/util/Map;

    const-string/jumbo v2, "com.miui.notes"

    new-instance v3, Lcom/android/settings/privacypassword/BussinessPackageInfo;

    const-string/jumbo v4, "privacy_notes"

    invoke-direct {v3, v11, v4}, Lcom/android/settings/privacypassword/BussinessPackageInfo;-><init>(ILjava/lang/String;)V

    invoke-interface {v0, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/android/settings/privacypassword/BussinessPackageInfoCache;->afy:Ljava/util/Map;

    sget-object v7, Lcom/android/settings/privacypassword/BussinessPackageInfoCache;->afy:Ljava/util/Map;

    const-string/jumbo v8, "privacy_mms"

    new-instance v0, Lcom/android/settings/privacypassword/BussinessSpecificationInfo;

    const-string/jumbo v4, "com.android.mms"

    const-string/jumbo v6, "android.intent.action.MAIN"

    const v2, 0x7f120aa9

    const v3, 0x7f08036f

    invoke-direct/range {v0 .. v6}, Lcom/android/settings/privacypassword/BussinessSpecificationInfo;-><init>(IIILjava/lang/String;ZLjava/lang/String;)V

    invoke-interface {v7, v8, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v7, Lcom/android/settings/privacypassword/BussinessPackageInfoCache;->afy:Ljava/util/Map;

    const-string/jumbo v8, "privacy_gallery"

    new-instance v0, Lcom/android/settings/privacypassword/BussinessSpecificationInfo;

    const-string/jumbo v4, "com.miui.gallery"

    const-string/jumbo v6, "android.intent.action.MAIN"

    const v2, 0x7f1207bd

    const v3, 0x7f08036e

    move v1, v10

    invoke-direct/range {v0 .. v6}, Lcom/android/settings/privacypassword/BussinessSpecificationInfo;-><init>(IIILjava/lang/String;ZLjava/lang/String;)V

    invoke-interface {v7, v8, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v7, Lcom/android/settings/privacypassword/BussinessPackageInfoCache;->afy:Ljava/util/Map;

    const-string/jumbo v8, "privacy_file"

    new-instance v0, Lcom/android/settings/privacypassword/BussinessSpecificationInfo;

    const-string/jumbo v4, "com.android.fileexplorer"

    const-string/jumbo v6, "android.intent.action.MAIN"

    const v2, 0x7f1206fd

    const v3, 0x7f08036d

    move v1, v9

    invoke-direct/range {v0 .. v6}, Lcom/android/settings/privacypassword/BussinessSpecificationInfo;-><init>(IIILjava/lang/String;ZLjava/lang/String;)V

    invoke-interface {v7, v8, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v7, Lcom/android/settings/privacypassword/BussinessPackageInfoCache;->afy:Ljava/util/Map;

    const-string/jumbo v8, "privacy_notes"

    new-instance v0, Lcom/android/settings/privacypassword/BussinessSpecificationInfo;

    const-string/jumbo v4, "com.miui.notes"

    const-string/jumbo v6, "android.intent.action.MAIN"

    const v2, 0x7f120ba9

    const v3, 0x7f080370

    move v1, v11

    invoke-direct/range {v0 .. v6}, Lcom/android/settings/privacypassword/BussinessSpecificationInfo;-><init>(IIILjava/lang/String;ZLjava/lang/String;)V

    invoke-interface {v7, v8, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/android/settings/privacypassword/BussinessPackageInfoCache;->afx:Ljava/util/Map;

    sget-object v0, Lcom/android/settings/privacypassword/BussinessPackageInfoCache;->afx:Ljava/util/Map;

    const-string/jumbo v1, "privacy_mms"

    const-string/jumbo v2, "privacy_mms"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/android/settings/privacypassword/BussinessPackageInfoCache;->afx:Ljava/util/Map;

    const-string/jumbo v1, "privacy_gallery"

    const-string/jumbo v2, "privacy_gallery"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/android/settings/privacypassword/BussinessPackageInfoCache;->afx:Ljava/util/Map;

    const-string/jumbo v1, "privacy_file"

    const-string/jumbo v2, "privacy_file"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/android/settings/privacypassword/BussinessPackageInfoCache;->afx:Ljava/util/Map;

    const-string/jumbo v1, "privacy_notes"

    const-string/jumbo v2, "privacy_notes"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static Wk()Ljava/util/Map;
    .locals 1

    sget-object v0, Lcom/android/settings/privacypassword/BussinessPackageInfoCache;->afw:Ljava/util/Map;

    return-object v0
.end method

.method public static Wl()Ljava/util/Map;
    .locals 1

    sget-object v0, Lcom/android/settings/privacypassword/BussinessPackageInfoCache;->afx:Ljava/util/Map;

    return-object v0
.end method

.method public static Wm()Ljava/util/Map;
    .locals 1

    sget-object v0, Lcom/android/settings/privacypassword/BussinessPackageInfoCache;->afy:Ljava/util/Map;

    return-object v0
.end method
