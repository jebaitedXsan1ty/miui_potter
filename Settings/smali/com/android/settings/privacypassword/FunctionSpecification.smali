.class public Lcom/android/settings/privacypassword/FunctionSpecification;
.super Lmiui/app/Activity;
.source "FunctionSpecification.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private agA:Landroid/widget/Button;

.field private agw:Landroid/widget/ImageView;

.field private agx:Landroid/widget/ImageView;

.field private agy:Landroid/widget/TextView;

.field private agz:Lcom/android/settings/privacypassword/PrivacyPasswordManager;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lmiui/app/Activity;-><init>()V

    return-void
.end method

.method private XO(Landroid/content/Context;Ljava/lang/String;)Z
    .locals 3

    const/4 v0, 0x0

    if-eqz p2, :cond_0

    const-string/jumbo v1, ""

    invoke-virtual {v1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    return v0

    :cond_1
    :try_start_0
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, p2, v2}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    if-eqz v1, :cond_2

    const/4 v0, 0x1

    :cond_2
    return v0

    :catch_0
    move-exception v1

    return v0
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3

    invoke-virtual {p0}, Lcom/android/settings/privacypassword/FunctionSpecification;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "privacy_password_function_specification"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-static {}, Lcom/android/settings/privacypassword/BussinessPackageInfoCache;->Wm()Ljava/util/Map;

    move-result-object v1

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settings/privacypassword/BussinessSpecificationInfo;

    if-eqz v0, :cond_3

    new-instance v1, Landroid/content/Intent;

    iget-object v2, v0, Lcom/android/settings/privacypassword/BussinessSpecificationInfo;->intentAction:Ljava/lang/String;

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iget-object v0, v0, Lcom/android/settings/privacypassword/BussinessSpecificationInfo;->ahs:Ljava/lang/String;

    sget-boolean v2, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z

    if-eqz v2, :cond_0

    if-eqz v0, :cond_0

    const-string/jumbo v2, "fileexplorer"

    invoke-virtual {v0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    const-string/jumbo v0, "com.mi.android.globalFileexplorer"

    invoke-direct {p0, p0, v0}, Lcom/android/settings/privacypassword/FunctionSpecification;->XO(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string/jumbo v0, "com.mi.android.globalFileexplorer"

    :cond_0
    :goto_0
    invoke-virtual {v1, v0}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p0, v1}, Lcom/android/settings/privacypassword/FunctionSpecification;->startActivity(Landroid/content/Intent;)V

    :cond_1
    :goto_1
    return-void

    :cond_2
    const-string/jumbo v0, "com.android.fileexplorer"

    goto :goto_0

    :cond_3
    invoke-virtual {p0}, Lcom/android/settings/privacypassword/FunctionSpecification;->finish()V

    goto :goto_1
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4

    invoke-super {p0, p1}, Lmiui/app/Activity;->onCreate(Landroid/os/Bundle;)V

    invoke-static {}, Lcom/android/settings/privacypassword/PrivacyPasswordUtils;->Yd()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/android/settings/privacypassword/FunctionSpecification;->setRequestedOrientation(I)V

    :cond_0
    const v0, 0x7f0d00c9

    invoke-virtual {p0, v0}, Lcom/android/settings/privacypassword/FunctionSpecification;->setContentView(I)V

    invoke-static {p0}, Lcom/android/settings/privacypassword/PrivacyPasswordManager;->getInstance(Landroid/content/Context;)Lcom/android/settings/privacypassword/PrivacyPasswordManager;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/privacypassword/FunctionSpecification;->agz:Lcom/android/settings/privacypassword/PrivacyPasswordManager;

    const v0, 0x7f0a01c0

    invoke-virtual {p0, v0}, Lcom/android/settings/privacypassword/FunctionSpecification;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/settings/privacypassword/FunctionSpecification;->agy:Landroid/widget/TextView;

    const v0, 0x7f0a04da

    invoke-virtual {p0, v0}, Lcom/android/settings/privacypassword/FunctionSpecification;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/android/settings/privacypassword/FunctionSpecification;->agA:Landroid/widget/Button;

    iget-object v0, p0, Lcom/android/settings/privacypassword/FunctionSpecification;->agA:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f0a01c1

    invoke-virtual {p0, v0}, Lcom/android/settings/privacypassword/FunctionSpecification;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/android/settings/privacypassword/FunctionSpecification;->agx:Landroid/widget/ImageView;

    const v0, 0x7f0a01be

    invoke-virtual {p0, v0}, Lcom/android/settings/privacypassword/FunctionSpecification;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/android/settings/privacypassword/FunctionSpecification;->agw:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/android/settings/privacypassword/FunctionSpecification;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "privacy_password_function_specification"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-static {}, Lcom/android/settings/privacypassword/BussinessPackageInfoCache;->Wm()Ljava/util/Map;

    move-result-object v1

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settings/privacypassword/BussinessSpecificationInfo;

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lcom/android/settings/privacypassword/FunctionSpecification;->getActionBar()Lmiui/app/ActionBar;

    move-result-object v1

    iget v2, v0, Lcom/android/settings/privacypassword/BussinessSpecificationInfo;->aho:I

    invoke-virtual {v1, v2}, Lmiui/app/ActionBar;->setTitle(I)V

    iget-object v1, p0, Lcom/android/settings/privacypassword/FunctionSpecification;->agy:Landroid/widget/TextView;

    iget v2, v0, Lcom/android/settings/privacypassword/BussinessSpecificationInfo;->ahp:I

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    iget-object v1, p0, Lcom/android/settings/privacypassword/FunctionSpecification;->agx:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/android/settings/privacypassword/FunctionSpecification;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    iget v3, v0, Lcom/android/settings/privacypassword/BussinessSpecificationInfo;->ahq:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    iget-boolean v0, v0, Lcom/android/settings/privacypassword/BussinessSpecificationInfo;->ahr:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/settings/privacypassword/FunctionSpecification;->agw:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    :cond_1
    :goto_0
    return-void

    :cond_2
    invoke-virtual {p0}, Lcom/android/settings/privacypassword/FunctionSpecification;->finish()V

    goto :goto_0
.end method

.method public onResume()V
    .locals 1

    invoke-super {p0}, Lmiui/app/Activity;->onResume()V

    iget-object v0, p0, Lcom/android/settings/privacypassword/FunctionSpecification;->agz:Lcom/android/settings/privacypassword/PrivacyPasswordManager;

    invoke-virtual {v0}, Lcom/android/settings/privacypassword/PrivacyPasswordManager;->Xs()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/android/settings/privacypassword/FunctionSpecification;->finish()V

    :cond_0
    return-void
.end method
