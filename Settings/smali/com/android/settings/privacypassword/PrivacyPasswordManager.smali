.class public Lcom/android/settings/privacypassword/PrivacyPasswordManager;
.super Ljava/lang/Object;
.source "PrivacyPasswordManager.java"


# static fields
.field private static agv:Lcom/android/settings/privacypassword/PrivacyPasswordManager;


# instance fields
.field private mContentResolver:Landroid/content/ContentResolver;

.field private mContext:Landroid/content/Context;


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/privacypassword/PrivacyPasswordManager;->mContext:Landroid/content/Context;

    iget-object v0, p0, Lcom/android/settings/privacypassword/PrivacyPasswordManager;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/privacypassword/PrivacyPasswordManager;->mContentResolver:Landroid/content/ContentResolver;

    return-void
.end method

.method public static declared-synchronized getInstance(Landroid/content/Context;)Lcom/android/settings/privacypassword/PrivacyPasswordManager;
    .locals 2

    const-class v1, Lcom/android/settings/privacypassword/PrivacyPasswordManager;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/android/settings/privacypassword/PrivacyPasswordManager;->agv:Lcom/android/settings/privacypassword/PrivacyPasswordManager;

    if-nez v0, :cond_0

    new-instance v0, Lcom/android/settings/privacypassword/PrivacyPasswordManager;

    invoke-direct {v0, p0}, Lcom/android/settings/privacypassword/PrivacyPasswordManager;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/android/settings/privacypassword/PrivacyPasswordManager;->agv:Lcom/android/settings/privacypassword/PrivacyPasswordManager;

    :cond_0
    sget-object v0, Lcom/android/settings/privacypassword/PrivacyPasswordManager;->agv:Lcom/android/settings/privacypassword/PrivacyPasswordManager;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public XA()Ljava/lang/String;
    .locals 4

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/android/settings/privacypassword/PrivacyPasswordManager;->mContentResolver:Landroid/content/ContentResolver;

    const-string/jumbo v1, "privacy_password_bind_xiaomi_account"

    invoke-static {v0, v1}, Landroid/provider/Settings$Secure;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/android/settings/privacypassword/PrivacyPasswordManager;->mContentResolver:Landroid/content/ContentResolver;

    const-string/jumbo v2, "privacy_add_account_md5"

    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    invoke-static {v0}, Lcom/android/settings/privacypassword/PrivacyPasswordUtils;->Yn([B)Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v2, v0}, Landroid/provider/Settings$Secure;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    iget-object v0, p0, Lcom/android/settings/privacypassword/PrivacyPasswordManager;->mContentResolver:Landroid/content/ContentResolver;

    const-string/jumbo v1, "privacy_password_bind_xiaomi_account"

    invoke-static {v0, v1, v3}, Landroid/provider/Settings$Secure;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    :cond_0
    iget-object v0, p0, Lcom/android/settings/privacypassword/PrivacyPasswordManager;->mContentResolver:Landroid/content/ContentResolver;

    const-string/jumbo v1, "privacy_add_account_md5"

    invoke-static {v0, v1}, Landroid/provider/Settings$Secure;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public XB()Z
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/android/settings/privacypassword/PrivacyPasswordManager;->mContext:Landroid/content/Context;

    const-string/jumbo v1, "privacy_password_sharedPreference"

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string/jumbo v1, "nerver_remind"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public XC()Z
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/android/settings/privacypassword/PrivacyPasswordManager;->mContext:Landroid/content/Context;

    const-string/jumbo v1, "privacy_password_sharedPreference"

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string/jumbo v1, "remind_open_fingerprint"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public XD(Z)V
    .locals 3

    iget-object v0, p0, Lcom/android/settings/privacypassword/PrivacyPasswordManager;->mContext:Landroid/content/Context;

    const-string/jumbo v1, "privacy_password_sharedPreference"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string/jumbo v1, "nerver_remind"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    return-void
.end method

.method public XE(Ljava/lang/String;)V
    .locals 3

    iget-object v1, p0, Lcom/android/settings/privacypassword/PrivacyPasswordManager;->mContentResolver:Landroid/content/ContentResolver;

    const-string/jumbo v2, "privacy_add_account_md5"

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    invoke-static {v0}, Lcom/android/settings/privacypassword/PrivacyPasswordUtils;->Yn([B)Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-static {v1, v2, v0}, Landroid/provider/Settings$Secure;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public XF(Z)V
    .locals 3

    iget-object v0, p0, Lcom/android/settings/privacypassword/PrivacyPasswordManager;->mContext:Landroid/content/Context;

    const-string/jumbo v1, "privacy_password_sharedPreference"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string/jumbo v1, "remind_open_fingerprint"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    return-void
.end method

.method public XG()Z
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/android/settings/privacypassword/PrivacyPasswordManager;->mContentResolver:Landroid/content/ContentResolver;

    const-string/jumbo v3, "privacy_password_status"

    invoke-static {v2, v3, v1}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    if-ne v2, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method public XH(Landroid/app/Activity;Z)V
    .locals 2

    new-instance v0, Landroid/security/ChooseLockSettingsHelper;

    const/4 v1, 0x3

    invoke-direct {v0, p1, v1}, Landroid/security/ChooseLockSettingsHelper;-><init>(Landroid/app/Activity;I)V

    invoke-virtual {v0, p2}, Landroid/security/ChooseLockSettingsHelper;->setPrivacyPasswordEnable(Z)V

    return-void
.end method

.method public XI(Z)V
    .locals 3

    iget-object v1, p0, Lcom/android/settings/privacypassword/PrivacyPasswordManager;->mContentResolver:Landroid/content/ContentResolver;

    const-string/jumbo v2, "privacy_password_status"

    if-eqz p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v1, v2, v0}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public XJ(Z)V
    .locals 3

    iget-object v1, p0, Lcom/android/settings/privacypassword/PrivacyPasswordManager;->mContentResolver:Landroid/content/ContentResolver;

    const-string/jumbo v2, "privacy_password_is_visible_pattern"

    if-eqz p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v1, v2, v0}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public Xs()Z
    .locals 2

    new-instance v0, Landroid/security/ChooseLockSettingsHelper;

    iget-object v1, p0, Lcom/android/settings/privacypassword/PrivacyPasswordManager;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/security/ChooseLockSettingsHelper;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0}, Landroid/security/ChooseLockSettingsHelper;->isPrivacyPasswordEnabled()Z

    move-result v0

    return v0
.end method

.method public Xt(J)V
    .locals 3

    iget-object v0, p0, Lcom/android/settings/privacypassword/PrivacyPasswordManager;->mContentResolver:Landroid/content/ContentResolver;

    const-string/jumbo v1, "privacy_password_countDownTimer_deadline"

    invoke-static {v0, v1, p1, p2}, Landroid/provider/Settings$Secure;->putLong(Landroid/content/ContentResolver;Ljava/lang/String;J)Z

    return-void
.end method

.method public Xu(Z)V
    .locals 3

    iget-object v1, p0, Lcom/android/settings/privacypassword/PrivacyPasswordManager;->mContentResolver:Landroid/content/ContentResolver;

    const-string/jumbo v2, "fingerprint_apply_to_privacy_password"

    if-eqz p1, :cond_0

    const/4 v0, 0x2

    :goto_0
    invoke-static {v1, v2, v0}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    return-void

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public Xv()Z
    .locals 3

    const/4 v0, 0x1

    iget-object v1, p0, Lcom/android/settings/privacypassword/PrivacyPasswordManager;->mContentResolver:Landroid/content/ContentResolver;

    const-string/jumbo v2, "privacy_password_is_visible_pattern"

    invoke-static {v1, v2, v0}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public Xw()Z
    .locals 3

    const/4 v0, 0x1

    iget-object v1, p0, Lcom/android/settings/privacypassword/PrivacyPasswordManager;->mContentResolver:Landroid/content/ContentResolver;

    const-string/jumbo v2, "fingerprint_apply_to_privacy_password"

    invoke-static {v1, v2, v0}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    const/4 v2, 0x2

    if-ne v1, v2, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public Xx()J
    .locals 4

    iget-object v0, p0, Lcom/android/settings/privacypassword/PrivacyPasswordManager;->mContentResolver:Landroid/content/ContentResolver;

    const-string/jumbo v1, "privacy_password_countDownTimer_deadline"

    const-wide/16 v2, 0x0

    invoke-static {v0, v1, v2, v3}, Landroid/provider/Settings$Secure;->getLong(Landroid/content/ContentResolver;Ljava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method

.method public Xy()I
    .locals 3

    iget-object v0, p0, Lcom/android/settings/privacypassword/PrivacyPasswordManager;->mContentResolver:Landroid/content/ContentResolver;

    const-string/jumbo v1, "access_control_lock_mode"

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public Xz()Z
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/android/settings/privacypassword/PrivacyPasswordManager;->mContentResolver:Landroid/content/ContentResolver;

    const-string/jumbo v3, "access_control_lock_convenient"

    invoke-static {v2, v3, v1}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    if-ne v2, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method
