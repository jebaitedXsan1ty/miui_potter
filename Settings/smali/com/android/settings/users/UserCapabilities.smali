.class public Lcom/android/settings/users/UserCapabilities;
.super Ljava/lang/Object;
.source "UserCapabilities.java"


# instance fields
.field bcK:Z

.field bcL:Z

.field bcM:Z

.field bcN:Z

.field bcO:Z

.field bcP:Z

.field bcQ:Z

.field bcR:Lcom/android/settingslib/n;

.field mEnabled:Z


# direct methods
.method private constructor <init>()V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean v0, p0, Lcom/android/settings/users/UserCapabilities;->mEnabled:Z

    iput-boolean v0, p0, Lcom/android/settings/users/UserCapabilities;->bcK:Z

    iput-boolean v0, p0, Lcom/android/settings/users/UserCapabilities;->bcM:Z

    return-void
.end method

.method public static aRa(Landroid/content/Context;)Lcom/android/settings/users/UserCapabilities;
    .locals 4

    const/4 v3, 0x0

    const-string/jumbo v0, "user"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/UserManager;

    new-instance v1, Lcom/android/settings/users/UserCapabilities;

    invoke-direct {v1}, Lcom/android/settings/users/UserCapabilities;-><init>()V

    invoke-static {}, Landroid/os/UserManager;->supportsMultipleUsers()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-static {}, Lcom/android/settings/aq;->bqE()Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    iput-boolean v3, v1, Lcom/android/settings/users/UserCapabilities;->mEnabled:Z

    return-object v1

    :cond_1
    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/os/UserManager;->getUserInfo(I)Landroid/content/pm/UserInfo;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/pm/UserInfo;->isGuest()Z

    move-result v2

    iput-boolean v2, v1, Lcom/android/settings/users/UserCapabilities;->bcO:Z

    invoke-virtual {v0}, Landroid/content/pm/UserInfo;->isAdmin()Z

    move-result v0

    iput-boolean v0, v1, Lcom/android/settings/users/UserCapabilities;->bcN:Z

    const-string/jumbo v0, "device_policy"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/admin/DevicePolicyManager;

    invoke-virtual {v0}, Landroid/app/admin/DevicePolicyManager;->isDeviceManaged()Z

    move-result v0

    if-nez v0, :cond_2

    invoke-static {p0}, Lcom/android/settings/aq;->bqw(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_2
    iput-boolean v3, v1, Lcom/android/settings/users/UserCapabilities;->bcM:Z

    :cond_3
    invoke-virtual {v1, p0}, Lcom/android/settings/users/UserCapabilities;->aRb(Landroid/content/Context;)V

    return-object v1
.end method


# virtual methods
.method public aRb(Landroid/content/Context;)V
    .locals 4

    const/4 v2, 0x1

    const/4 v1, 0x0

    const-string/jumbo v0, "no_add_user"

    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v3

    invoke-static {p1, v0, v3}, Lcom/android/settingslib/w;->crb(Landroid/content/Context;Ljava/lang/String;I)Lcom/android/settingslib/n;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/users/UserCapabilities;->bcR:Lcom/android/settingslib/n;

    const-string/jumbo v0, "no_add_user"

    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v3

    invoke-static {p1, v0, v3}, Lcom/android/settingslib/w;->crn(Landroid/content/Context;Ljava/lang/String;I)Z

    move-result v3

    iget-object v0, p0, Lcom/android/settings/users/UserCapabilities;->bcR:Lcom/android/settingslib/n;

    if-eqz v0, :cond_3

    xor-int/lit8 v0, v3, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/android/settings/users/UserCapabilities;->bcL:Z

    iget-object v0, p0, Lcom/android/settings/users/UserCapabilities;->bcR:Lcom/android/settingslib/n;

    if-nez v0, :cond_4

    move v0, v3

    :goto_1
    iput-boolean v0, p0, Lcom/android/settings/users/UserCapabilities;->bcQ:Z

    iput-boolean v2, p0, Lcom/android/settings/users/UserCapabilities;->bcK:Z

    iget-boolean v0, p0, Lcom/android/settings/users/UserCapabilities;->bcN:Z

    if-eqz v0, :cond_0

    invoke-static {}, Landroid/os/UserManager;->getMaxSupportedUsers()I

    move-result v0

    const/4 v3, 0x2

    if-ge v0, v3, :cond_5

    :cond_0
    :goto_2
    iput-boolean v1, p0, Lcom/android/settings/users/UserCapabilities;->bcK:Z

    :cond_1
    iget-boolean v0, p0, Lcom/android/settings/users/UserCapabilities;->bcN:Z

    if-nez v0, :cond_2

    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v3, "add_users_when_locked"

    invoke-static {v0, v3, v1}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    if-ne v0, v2, :cond_6

    :cond_2
    :goto_3
    iget-boolean v0, p0, Lcom/android/settings/users/UserCapabilities;->bcO:Z

    if-nez v0, :cond_7

    iget-boolean v0, p0, Lcom/android/settings/users/UserCapabilities;->bcQ:Z

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_7

    :goto_4
    iput-boolean v2, p0, Lcom/android/settings/users/UserCapabilities;->bcP:Z

    return-void

    :cond_3
    move v0, v1

    goto :goto_0

    :cond_4
    move v0, v2

    goto :goto_1

    :cond_5
    invoke-static {}, Landroid/os/UserManager;->supportsMultipleUsers()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/android/settings/users/UserCapabilities;->bcQ:Z

    if-eqz v0, :cond_1

    goto :goto_2

    :cond_6
    move v2, v1

    goto :goto_3

    :cond_7
    move v2, v1

    goto :goto_4
.end method

.method public aRc()Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/settings/users/UserCapabilities;->bcQ:Z

    return v0
.end method

.method public aRd()Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/settings/users/UserCapabilities;->bcL:Z

    return v0
.end method

.method public aRe()Lcom/android/settingslib/n;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/users/UserCapabilities;->bcR:Lcom/android/settingslib/n;

    return-object v0
.end method

.method public aRf()Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/settings/users/UserCapabilities;->bcN:Z

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "UserCapabilities{mEnabled="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/android/settings/users/UserCapabilities;->mEnabled:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", mCanAddUser="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/android/settings/users/UserCapabilities;->bcK:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", mCanAddRestrictedProfile="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/android/settings/users/UserCapabilities;->bcM:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", mIsAdmin="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/android/settings/users/UserCapabilities;->bcN:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", mIsGuest="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/android/settings/users/UserCapabilities;->bcO:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", mCanAddGuest="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/android/settings/users/UserCapabilities;->bcP:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", mDisallowAddUser="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/android/settings/users/UserCapabilities;->bcQ:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", mEnforcedAdmin="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/users/UserCapabilities;->bcR:Lcom/android/settingslib/n;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
