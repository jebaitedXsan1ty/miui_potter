.class final Lcom/android/settings/users/EditUserInfoController$1;
.super Ljava/lang/Object;
.source "EditUserInfoController.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field final synthetic bel:Lcom/android/settings/users/EditUserInfoController;

.field final synthetic bem:Landroid/widget/EditText;

.field final synthetic ben:Ljava/lang/CharSequence;

.field final synthetic beo:Lcom/android/settings/users/EditUserInfoController$OnContentChangedCallback;

.field final synthetic bep:Landroid/graphics/drawable/Drawable;

.field final synthetic beq:Landroid/app/Fragment;


# direct methods
.method constructor <init>(Lcom/android/settings/users/EditUserInfoController;Landroid/widget/EditText;Ljava/lang/CharSequence;Lcom/android/settings/users/EditUserInfoController$OnContentChangedCallback;Landroid/graphics/drawable/Drawable;Landroid/app/Fragment;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/users/EditUserInfoController$1;->bel:Lcom/android/settings/users/EditUserInfoController;

    iput-object p2, p0, Lcom/android/settings/users/EditUserInfoController$1;->bem:Landroid/widget/EditText;

    iput-object p3, p0, Lcom/android/settings/users/EditUserInfoController$1;->ben:Ljava/lang/CharSequence;

    iput-object p4, p0, Lcom/android/settings/users/EditUserInfoController$1;->beo:Lcom/android/settings/users/EditUserInfoController$OnContentChangedCallback;

    iput-object p5, p0, Lcom/android/settings/users/EditUserInfoController$1;->bep:Landroid/graphics/drawable/Drawable;

    iput-object p6, p0, Lcom/android/settings/users/EditUserInfoController$1;->beq:Landroid/app/Fragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 4

    const/4 v0, 0x0

    const/4 v1, -0x1

    if-ne p2, v1, :cond_5

    iget-object v1, p0, Lcom/android/settings/users/EditUserInfoController$1;->bem:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    iget-object v2, p0, Lcom/android/settings/users/EditUserInfoController$1;->ben:Ljava/lang/CharSequence;

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/android/settings/users/EditUserInfoController$1;->ben:Ljava/lang/CharSequence;

    invoke-interface {v3}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    xor-int/lit8 v2, v2, 0x1

    if-eqz v2, :cond_2

    :cond_0
    iget-object v2, p0, Lcom/android/settings/users/EditUserInfoController$1;->beo:Lcom/android/settings/users/EditUserInfoController$OnContentChangedCallback;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/android/settings/users/EditUserInfoController$1;->beo:Lcom/android/settings/users/EditUserInfoController$OnContentChangedCallback;

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lcom/android/settings/users/EditUserInfoController$OnContentChangedCallback;->aQw(Ljava/lang/CharSequence;)V

    :cond_1
    iget-object v2, p0, Lcom/android/settings/users/EditUserInfoController$1;->bel:Lcom/android/settings/users/EditUserInfoController;

    invoke-static {v2}, Lcom/android/settings/users/EditUserInfoController;->aRm(Lcom/android/settings/users/EditUserInfoController;)Landroid/os/UserManager;

    move-result-object v2

    iget-object v3, p0, Lcom/android/settings/users/EditUserInfoController$1;->bel:Lcom/android/settings/users/EditUserInfoController;

    invoke-static {v3}, Lcom/android/settings/users/EditUserInfoController;->aRl(Lcom/android/settings/users/EditUserInfoController;)Landroid/os/UserHandle;

    move-result-object v3

    invoke-virtual {v3}, Landroid/os/UserHandle;->getIdentifier()I

    move-result v3

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v3, v1}, Landroid/os/UserManager;->setUserName(ILjava/lang/String;)V

    :cond_2
    iget-object v1, p0, Lcom/android/settings/users/EditUserInfoController$1;->bel:Lcom/android/settings/users/EditUserInfoController;

    invoke-static {v1}, Lcom/android/settings/users/EditUserInfoController;->aRk(Lcom/android/settings/users/EditUserInfoController;)Lcom/android/settings/users/EditUserPhotoController;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/settings/users/EditUserPhotoController;->aRt()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iget-object v2, p0, Lcom/android/settings/users/EditUserInfoController$1;->bel:Lcom/android/settings/users/EditUserInfoController;

    invoke-static {v2}, Lcom/android/settings/users/EditUserInfoController;->aRk(Lcom/android/settings/users/EditUserInfoController;)Lcom/android/settings/users/EditUserPhotoController;

    move-result-object v2

    invoke-virtual {v2}, Lcom/android/settings/users/EditUserPhotoController;->aRu()Landroid/graphics/Bitmap;

    move-result-object v2

    if-eqz v1, :cond_4

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/android/settings/users/EditUserInfoController$1;->bep:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1, v2}, Landroid/graphics/drawable/Drawable;->equals(Ljava/lang/Object;)Z

    move-result v2

    xor-int/lit8 v2, v2, 0x1

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/android/settings/users/EditUserInfoController$1;->beo:Lcom/android/settings/users/EditUserInfoController$OnContentChangedCallback;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/android/settings/users/EditUserInfoController$1;->beo:Lcom/android/settings/users/EditUserInfoController$OnContentChangedCallback;

    invoke-interface {v2, v1}, Lcom/android/settings/users/EditUserInfoController$OnContentChangedCallback;->aQy(Landroid/graphics/drawable/Drawable;)V

    :cond_3
    new-instance v1, Lcom/android/settings/users/EditUserInfoController$1$1;

    invoke-direct {v1, p0}, Lcom/android/settings/users/EditUserInfoController$1$1;-><init>(Lcom/android/settings/users/EditUserInfoController$1;)V

    sget-object v2, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    check-cast v0, [Ljava/lang/Void;

    invoke-virtual {v1, v2, v0}, Lcom/android/settings/users/EditUserInfoController$1$1;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    :cond_4
    iget-object v0, p0, Lcom/android/settings/users/EditUserInfoController$1;->beq:Landroid/app/Fragment;

    invoke-virtual {v0}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/Activity;->removeDialog(I)V

    :cond_5
    iget-object v0, p0, Lcom/android/settings/users/EditUserInfoController$1;->bel:Lcom/android/settings/users/EditUserInfoController;

    invoke-virtual {v0}, Lcom/android/settings/users/EditUserInfoController;->clear()V

    return-void
.end method
