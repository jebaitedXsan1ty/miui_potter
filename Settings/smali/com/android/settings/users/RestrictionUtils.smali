.class public Lcom/android/settings/users/RestrictionUtils;
.super Ljava/lang/Object;
.source "RestrictionUtils.java"


# static fields
.field public static final bdr:[I

.field public static final bds:[Ljava/lang/String;

.field public static final bdt:[I


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-array v0, v3, [Ljava/lang/String;

    const-string/jumbo v1, "no_share_location"

    aput-object v1, v0, v2

    sput-object v0, Lcom/android/settings/users/RestrictionUtils;->bds:[Ljava/lang/String;

    new-array v0, v3, [I

    const v1, 0x7f120e32

    aput v1, v0, v2

    sput-object v0, Lcom/android/settings/users/RestrictionUtils;->bdt:[I

    new-array v0, v3, [I

    const v1, 0x7f120e31

    aput v1, v0, v2

    sput-object v0, Lcom/android/settings/users/RestrictionUtils;->bdr:[I

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static aRX(Landroid/content/Context;Landroid/os/UserHandle;)Ljava/util/ArrayList;
    .locals 8

    const/4 v1, 0x0

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    invoke-static {p0}, Landroid/os/UserManager;->get(Landroid/content/Context;)Landroid/os/UserManager;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/os/UserManager;->getUserRestrictions(Landroid/os/UserHandle;)Landroid/os/Bundle;

    move-result-object v4

    move v0, v1

    :goto_0
    sget-object v5, Lcom/android/settings/users/RestrictionUtils;->bds:[Ljava/lang/String;

    array-length v5, v5

    if-ge v0, v5, :cond_0

    new-instance v5, Landroid/content/RestrictionEntry;

    sget-object v6, Lcom/android/settings/users/RestrictionUtils;->bds:[Ljava/lang/String;

    aget-object v6, v6, v0

    sget-object v7, Lcom/android/settings/users/RestrictionUtils;->bds:[Ljava/lang/String;

    aget-object v7, v7, v0

    invoke-virtual {v4, v7, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v7

    xor-int/lit8 v7, v7, 0x1

    invoke-direct {v5, v6, v7}, Landroid/content/RestrictionEntry;-><init>(Ljava/lang/String;Z)V

    sget-object v6, Lcom/android/settings/users/RestrictionUtils;->bdt:[I

    aget v6, v6, v0

    invoke-virtual {v2, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/content/RestrictionEntry;->setTitle(Ljava/lang/String;)V

    sget-object v6, Lcom/android/settings/users/RestrictionUtils;->bdr:[I

    aget v6, v6, v0

    invoke-virtual {v2, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/content/RestrictionEntry;->setDescription(Ljava/lang/String;)V

    const/4 v6, 0x1

    invoke-virtual {v5, v6}, Landroid/content/RestrictionEntry;->setType(I)V

    invoke-virtual {v3, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-object v3
.end method

.method public static aRY(Landroid/content/Context;Ljava/util/ArrayList;Landroid/os/UserHandle;)V
    .locals 4

    invoke-static {p0}, Landroid/os/UserManager;->get(Landroid/content/Context;)Landroid/os/UserManager;

    move-result-object v1

    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/RestrictionEntry;

    invoke-virtual {v0}, Landroid/content/RestrictionEntry;->getKey()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0}, Landroid/content/RestrictionEntry;->getSelectedState()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    invoke-virtual {v1, v3, v0, p2}, Landroid/os/UserManager;->setUserRestriction(Ljava/lang/String;ZLandroid/os/UserHandle;)V

    goto :goto_0

    :cond_0
    return-void
.end method
