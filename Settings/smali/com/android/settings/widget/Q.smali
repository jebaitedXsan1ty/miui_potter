.class final Lcom/android/settings/widget/Q;
.super Landroid/animation/AnimatorListenerAdapter;
.source "DotsPageIndicator.java"


# instance fields
.field final synthetic aRF:Lcom/android/settings/widget/c;

.field final synthetic aRG:[I

.field final synthetic aRH:F

.field final synthetic aRI:F


# direct methods
.method constructor <init>(Lcom/android/settings/widget/c;[IFF)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/widget/Q;->aRF:Lcom/android/settings/widget/c;

    iput-object p2, p0, Lcom/android/settings/widget/Q;->aRG:[I

    iput p3, p0, Lcom/android/settings/widget/Q;->aRH:F

    iput p4, p0, Lcom/android/settings/widget/Q;->aRI:F

    invoke-direct {p0}, Landroid/animation/AnimatorListenerAdapter;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 2

    const/high16 v1, -0x40800000    # -1.0f

    iget-object v0, p0, Lcom/android/settings/widget/Q;->aRF:Lcom/android/settings/widget/c;

    iget-object v0, v0, Lcom/android/settings/widget/c;->aNP:Lcom/android/settings/widget/DotsPageIndicator;

    invoke-static {v0, v1}, Lcom/android/settings/widget/DotsPageIndicator;->azw(Lcom/android/settings/widget/DotsPageIndicator;F)F

    iget-object v0, p0, Lcom/android/settings/widget/Q;->aRF:Lcom/android/settings/widget/c;

    iget-object v0, v0, Lcom/android/settings/widget/c;->aNP:Lcom/android/settings/widget/DotsPageIndicator;

    invoke-static {v0, v1}, Lcom/android/settings/widget/DotsPageIndicator;->azx(Lcom/android/settings/widget/DotsPageIndicator;F)F

    iget-object v0, p0, Lcom/android/settings/widget/Q;->aRF:Lcom/android/settings/widget/c;

    iget-object v0, v0, Lcom/android/settings/widget/c;->aNP:Lcom/android/settings/widget/DotsPageIndicator;

    invoke-virtual {v0}, Lcom/android/settings/widget/DotsPageIndicator;->postInvalidateOnAnimation()V

    return-void
.end method

.method public onAnimationStart(Landroid/animation/Animator;)V
    .locals 6

    iget-object v0, p0, Lcom/android/settings/widget/Q;->aRF:Lcom/android/settings/widget/c;

    iget-object v0, v0, Lcom/android/settings/widget/c;->aNP:Lcom/android/settings/widget/DotsPageIndicator;

    invoke-static {v0}, Lcom/android/settings/widget/DotsPageIndicator;->azB(Lcom/android/settings/widget/DotsPageIndicator;)V

    iget-object v0, p0, Lcom/android/settings/widget/Q;->aRF:Lcom/android/settings/widget/c;

    iget-object v0, v0, Lcom/android/settings/widget/c;->aNP:Lcom/android/settings/widget/DotsPageIndicator;

    invoke-static {v0}, Lcom/android/settings/widget/DotsPageIndicator;->azC(Lcom/android/settings/widget/DotsPageIndicator;)V

    iget-object v1, p0, Lcom/android/settings/widget/Q;->aRG:[I

    const/4 v0, 0x0

    array-length v2, v1

    :goto_0
    if-ge v0, v2, :cond_0

    aget v3, v1, v0

    iget-object v4, p0, Lcom/android/settings/widget/Q;->aRF:Lcom/android/settings/widget/c;

    iget-object v4, v4, Lcom/android/settings/widget/c;->aNP:Lcom/android/settings/widget/DotsPageIndicator;

    const v5, 0x3727c5ac    # 1.0E-5f

    invoke-static {v4, v3, v5}, Lcom/android/settings/widget/DotsPageIndicator;->azD(Lcom/android/settings/widget/DotsPageIndicator;IF)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/android/settings/widget/Q;->aRF:Lcom/android/settings/widget/c;

    iget-object v0, v0, Lcom/android/settings/widget/c;->aNP:Lcom/android/settings/widget/DotsPageIndicator;

    iget v1, p0, Lcom/android/settings/widget/Q;->aRH:F

    invoke-static {v0, v1}, Lcom/android/settings/widget/DotsPageIndicator;->azw(Lcom/android/settings/widget/DotsPageIndicator;F)F

    iget-object v0, p0, Lcom/android/settings/widget/Q;->aRF:Lcom/android/settings/widget/c;

    iget-object v0, v0, Lcom/android/settings/widget/c;->aNP:Lcom/android/settings/widget/DotsPageIndicator;

    iget v1, p0, Lcom/android/settings/widget/Q;->aRI:F

    invoke-static {v0, v1}, Lcom/android/settings/widget/DotsPageIndicator;->azx(Lcom/android/settings/widget/DotsPageIndicator;F)F

    iget-object v0, p0, Lcom/android/settings/widget/Q;->aRF:Lcom/android/settings/widget/c;

    iget-object v0, v0, Lcom/android/settings/widget/c;->aNP:Lcom/android/settings/widget/DotsPageIndicator;

    invoke-virtual {v0}, Lcom/android/settings/widget/DotsPageIndicator;->postInvalidateOnAnimation()V

    return-void
.end method
