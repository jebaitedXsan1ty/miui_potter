.class public final Lcom/android/settings/widget/SlidingTabLayout;
.super Landroid/widget/FrameLayout;
.source "SlidingTabLayout.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private aQA:I

.field private aQB:F

.field private final aQC:Landroid/widget/LinearLayout;

.field private aQD:Lcom/android/settings/widget/RtlCompatibleViewPager;

.field private final aQy:Landroid/view/View;

.field private final aQz:Landroid/view/LayoutInflater;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3

    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/widget/SlidingTabLayout;->aQz:Landroid/view/LayoutInflater;

    new-instance v0, Landroid/widget/LinearLayout;

    invoke-direct {v0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/settings/widget/SlidingTabLayout;->aQC:Landroid/widget/LinearLayout;

    iget-object v0, p0, Lcom/android/settings/widget/SlidingTabLayout;->aQC:Landroid/widget/LinearLayout;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setGravity(I)V

    iget-object v0, p0, Lcom/android/settings/widget/SlidingTabLayout;->aQz:Landroid/view/LayoutInflater;

    const v1, 0x7f0d01d2

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/widget/SlidingTabLayout;->aQy:Landroid/view/View;

    iget-object v0, p0, Lcom/android/settings/widget/SlidingTabLayout;->aQC:Landroid/widget/LinearLayout;

    const/4 v1, -0x1

    const/4 v2, -0x2

    invoke-virtual {p0, v0, v1, v2}, Lcom/android/settings/widget/SlidingTabLayout;->addView(Landroid/view/View;II)V

    iget-object v0, p0, Lcom/android/settings/widget/SlidingTabLayout;->aQy:Landroid/view/View;

    iget-object v1, p0, Lcom/android/settings/widget/SlidingTabLayout;->aQy:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/android/settings/widget/SlidingTabLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    return-void
.end method

.method private aBU()Z
    .locals 2

    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/android/settings/widget/SlidingTabLayout;->getLayoutDirection()I

    move-result v1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private aBV(IF)V
    .locals 2

    iput p1, p0, Lcom/android/settings/widget/SlidingTabLayout;->aQA:I

    iput p2, p0, Lcom/android/settings/widget/SlidingTabLayout;->aQB:F

    invoke-direct {p0}, Lcom/android/settings/widget/SlidingTabLayout;->aBU()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/android/settings/widget/SlidingTabLayout;->getIndicatorLeft()I

    move-result v0

    neg-int v0, v0

    :goto_0
    iget-object v1, p0, Lcom/android/settings/widget/SlidingTabLayout;->aQy:Landroid/view/View;

    int-to-float v0, v0

    invoke-virtual {v1, v0}, Landroid/view/View;->setTranslationX(F)V

    return-void

    :cond_0
    invoke-direct {p0}, Lcom/android/settings/widget/SlidingTabLayout;->getIndicatorLeft()I

    move-result v0

    goto :goto_0
.end method

.method private aBW()V
    .locals 6

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/android/settings/widget/SlidingTabLayout;->aQD:Lcom/android/settings/widget/RtlCompatibleViewPager;

    invoke-virtual {v0}, Lcom/android/settings/widget/RtlCompatibleViewPager;->getAdapter()Landroid/support/v4/view/u;

    move-result-object v4

    move v1, v2

    :goto_0
    invoke-virtual {v4}, Landroid/support/v4/view/u;->getCount()I

    move-result v0

    if-ge v1, v0, :cond_1

    iget-object v0, p0, Lcom/android/settings/widget/SlidingTabLayout;->aQz:Landroid/view/LayoutInflater;

    iget-object v3, p0, Lcom/android/settings/widget/SlidingTabLayout;->aQC:Landroid/widget/LinearLayout;

    const v5, 0x7f0d01d3

    invoke-virtual {v0, v5, v3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v4, v1}, Landroid/support/v4/view/u;->vW(I)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v0, p0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v3, p0, Lcom/android/settings/widget/SlidingTabLayout;->aQC:Landroid/widget/LinearLayout;

    invoke-virtual {v3, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    iget-object v3, p0, Lcom/android/settings/widget/SlidingTabLayout;->aQD:Lcom/android/settings/widget/RtlCompatibleViewPager;

    invoke-virtual {v3}, Lcom/android/settings/widget/RtlCompatibleViewPager;->getCurrentItem()I

    move-result v3

    if-ne v1, v3, :cond_0

    const/4 v3, 0x1

    :goto_1
    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setSelected(Z)V

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    move v3, v2

    goto :goto_1

    :cond_1
    return-void
.end method

.method static synthetic aBX(Lcom/android/settings/widget/SlidingTabLayout;)Landroid/widget/LinearLayout;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/widget/SlidingTabLayout;->aQC:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic aBY(Lcom/android/settings/widget/SlidingTabLayout;)Lcom/android/settings/widget/RtlCompatibleViewPager;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/widget/SlidingTabLayout;->aQD:Lcom/android/settings/widget/RtlCompatibleViewPager;

    return-object v0
.end method

.method static synthetic aBZ(Lcom/android/settings/widget/SlidingTabLayout;IF)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/android/settings/widget/SlidingTabLayout;->aBV(IF)V

    return-void
.end method

.method private getIndicatorLeft()I
    .locals 4

    iget-object v0, p0, Lcom/android/settings/widget/SlidingTabLayout;->aQC:Landroid/widget/LinearLayout;

    iget v1, p0, Lcom/android/settings/widget/SlidingTabLayout;->aQA:I

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getLeft()I

    move-result v0

    iget v1, p0, Lcom/android/settings/widget/SlidingTabLayout;->aQB:F

    const/4 v2, 0x0

    cmpl-float v1, v1, v2

    if-lez v1, :cond_0

    iget v1, p0, Lcom/android/settings/widget/SlidingTabLayout;->aQA:I

    invoke-virtual {p0}, Lcom/android/settings/widget/SlidingTabLayout;->getChildCount()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    if-ge v1, v2, :cond_0

    iget-object v1, p0, Lcom/android/settings/widget/SlidingTabLayout;->aQC:Landroid/widget/LinearLayout;

    iget v2, p0, Lcom/android/settings/widget/SlidingTabLayout;->aQA:I

    add-int/lit8 v2, v2, 0x1

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    iget v2, p0, Lcom/android/settings/widget/SlidingTabLayout;->aQB:F

    invoke-virtual {v1}, Landroid/view/View;->getLeft()I

    move-result v1

    int-to-float v1, v1

    mul-float/2addr v1, v2

    iget v2, p0, Lcom/android/settings/widget/SlidingTabLayout;->aQB:F

    const/high16 v3, 0x3f800000    # 1.0f

    sub-float v2, v3, v2

    int-to-float v0, v0

    mul-float/2addr v0, v2

    add-float/2addr v0, v1

    float-to-int v0, v0

    :cond_0
    return v0
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3

    iget-object v0, p0, Lcom/android/settings/widget/SlidingTabLayout;->aQC:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_1

    iget-object v2, p0, Lcom/android/settings/widget/SlidingTabLayout;->aQC:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v0}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    if-ne p1, v2, :cond_0

    iget-object v1, p0, Lcom/android/settings/widget/SlidingTabLayout;->aQD:Lcom/android/settings/widget/RtlCompatibleViewPager;

    invoke-virtual {v1, v0}, Lcom/android/settings/widget/RtlCompatibleViewPager;->setCurrentItem(I)V

    return-void

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 9

    const/4 v8, 0x0

    iget-object v0, p0, Lcom/android/settings/widget/SlidingTabLayout;->aQC:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v0

    if-lez v0, :cond_0

    invoke-virtual {p0}, Lcom/android/settings/widget/SlidingTabLayout;->getMeasuredHeight()I

    move-result v0

    iget-object v1, p0, Lcom/android/settings/widget/SlidingTabLayout;->aQy:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v1

    iget-object v2, p0, Lcom/android/settings/widget/SlidingTabLayout;->aQy:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getMeasuredWidth()I

    move-result v2

    invoke-virtual {p0}, Lcom/android/settings/widget/SlidingTabLayout;->getMeasuredWidth()I

    move-result v3

    invoke-virtual {p0}, Lcom/android/settings/widget/SlidingTabLayout;->getPaddingLeft()I

    move-result v4

    invoke-virtual {p0}, Lcom/android/settings/widget/SlidingTabLayout;->getPaddingRight()I

    move-result v5

    iget-object v6, p0, Lcom/android/settings/widget/SlidingTabLayout;->aQC:Landroid/widget/LinearLayout;

    iget-object v7, p0, Lcom/android/settings/widget/SlidingTabLayout;->aQC:Landroid/widget/LinearLayout;

    invoke-virtual {v7}, Landroid/widget/LinearLayout;->getMeasuredWidth()I

    move-result v7

    add-int/2addr v5, v7

    iget-object v7, p0, Lcom/android/settings/widget/SlidingTabLayout;->aQC:Landroid/widget/LinearLayout;

    invoke-virtual {v7}, Landroid/widget/LinearLayout;->getMeasuredHeight()I

    move-result v7

    invoke-virtual {v6, v4, v8, v5, v7}, Landroid/widget/LinearLayout;->layout(IIII)V

    invoke-direct {p0}, Lcom/android/settings/widget/SlidingTabLayout;->aBU()Z

    move-result v4

    if-eqz v4, :cond_1

    iget-object v4, p0, Lcom/android/settings/widget/SlidingTabLayout;->aQy:Landroid/view/View;

    sub-int v2, v3, v2

    sub-int v1, v0, v1

    invoke-virtual {v4, v2, v1, v3, v0}, Landroid/view/View;->layout(IIII)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v3, p0, Lcom/android/settings/widget/SlidingTabLayout;->aQy:Landroid/view/View;

    sub-int v1, v0, v1

    invoke-virtual {v3, v8, v1, v2, v0}, Landroid/view/View;->layout(IIII)V

    goto :goto_0
.end method

.method protected onMeasure(II)V
    .locals 3

    const/high16 v2, 0x40000000    # 2.0f

    invoke-super {p0, p1, p2}, Landroid/widget/FrameLayout;->onMeasure(II)V

    iget-object v0, p0, Lcom/android/settings/widget/SlidingTabLayout;->aQC:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v0

    if-lez v0, :cond_0

    iget-object v1, p0, Lcom/android/settings/widget/SlidingTabLayout;->aQC:Landroid/widget/LinearLayout;

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getMeasuredWidth()I

    move-result v1

    div-int v0, v1, v0

    invoke-static {v0, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    iget-object v1, p0, Lcom/android/settings/widget/SlidingTabLayout;->aQy:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v1

    invoke-static {v1, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    iget-object v2, p0, Lcom/android/settings/widget/SlidingTabLayout;->aQy:Landroid/view/View;

    invoke-virtual {v2, v0, v1}, Landroid/view/View;->measure(II)V

    :cond_0
    return-void
.end method

.method public setViewPager(Lcom/android/settings/widget/RtlCompatibleViewPager;)V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/android/settings/widget/SlidingTabLayout;->aQC:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->removeAllViews()V

    iput-object p1, p0, Lcom/android/settings/widget/SlidingTabLayout;->aQD:Lcom/android/settings/widget/RtlCompatibleViewPager;

    if-eqz p1, :cond_0

    new-instance v0, Lcom/android/settings/widget/C;

    invoke-direct {v0, p0, v1}, Lcom/android/settings/widget/C;-><init>(Lcom/android/settings/widget/SlidingTabLayout;Lcom/android/settings/widget/C;)V

    invoke-virtual {p1, v0}, Lcom/android/settings/widget/RtlCompatibleViewPager;->dOB(Landroid/support/v4/view/j;)V

    invoke-direct {p0}, Lcom/android/settings/widget/SlidingTabLayout;->aBW()V

    :cond_0
    return-void
.end method
