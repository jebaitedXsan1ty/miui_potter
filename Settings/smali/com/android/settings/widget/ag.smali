.class final Lcom/android/settings/widget/ag;
.super Ljava/lang/Object;
.source "CarrierDemoPasswordDialogFragment.java"

# interfaces
.implements Landroid/content/DialogInterface$OnShowListener;


# instance fields
.field final synthetic aSc:Lcom/android/settings/widget/CarrierDemoPasswordDialogFragment;

.field final synthetic aSd:Landroid/app/AlertDialog;

.field final synthetic aSe:Landroid/widget/EditText;


# direct methods
.method constructor <init>(Lcom/android/settings/widget/CarrierDemoPasswordDialogFragment;Landroid/app/AlertDialog;Landroid/widget/EditText;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/widget/ag;->aSc:Lcom/android/settings/widget/CarrierDemoPasswordDialogFragment;

    iput-object p2, p0, Lcom/android/settings/widget/ag;->aSd:Landroid/app/AlertDialog;

    iput-object p3, p0, Lcom/android/settings/widget/ag;->aSe:Landroid/widget/EditText;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onShow(Landroid/content/DialogInterface;)V
    .locals 3

    iget-object v0, p0, Lcom/android/settings/widget/ag;->aSc:Lcom/android/settings/widget/CarrierDemoPasswordDialogFragment;

    iget-object v1, p0, Lcom/android/settings/widget/ag;->aSd:Landroid/app/AlertDialog;

    iget-object v2, p0, Lcom/android/settings/widget/ag;->aSe:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-interface {v2}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/android/settings/widget/CarrierDemoPasswordDialogFragment;->aBv(Lcom/android/settings/widget/CarrierDemoPasswordDialogFragment;Landroid/app/AlertDialog;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/settings/widget/ag;->aSe:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->requestFocus()Z

    iget-object v0, p0, Lcom/android/settings/widget/ag;->aSc:Lcom/android/settings/widget/CarrierDemoPasswordDialogFragment;

    invoke-virtual {v0}, Lcom/android/settings/widget/CarrierDemoPasswordDialogFragment;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string/jumbo v1, "input_method"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    iget-object v1, p0, Lcom/android/settings/widget/ag;->aSe:Landroid/widget/EditText;

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->showSoftInput(Landroid/view/View;I)Z

    return-void
.end method
