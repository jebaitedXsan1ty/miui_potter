.class public Lcom/android/settings/widget/MasterSwitchPreference;
.super Lcom/android/settingslib/TwoTargetPreference;
.source "MasterSwitchPreference.java"


# instance fields
.field private aOF:Z

.field private aOG:Z

.field private aOH:Landroid/widget/Switch;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    invoke-direct {p0, p1}, Lcom/android/settingslib/TwoTargetPreference;-><init>(Landroid/content/Context;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/settings/widget/MasterSwitchPreference;->aOG:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    invoke-direct {p0, p1, p2}, Lcom/android/settingslib/TwoTargetPreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/settings/widget/MasterSwitchPreference;->aOG:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    invoke-direct {p0, p1, p2, p3}, Lcom/android/settingslib/TwoTargetPreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/settings/widget/MasterSwitchPreference;->aOG:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V
    .locals 1

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/android/settingslib/TwoTargetPreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/settings/widget/MasterSwitchPreference;->aOG:Z

    return-void
.end method

.method static synthetic aAA(Lcom/android/settings/widget/MasterSwitchPreference;Z)Z
    .locals 1

    invoke-virtual {p0, p1}, Lcom/android/settings/widget/MasterSwitchPreference;->persistBoolean(Z)Z

    move-result v0

    return v0
.end method

.method static synthetic aAy(Lcom/android/settings/widget/MasterSwitchPreference;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/settings/widget/MasterSwitchPreference;->aOF:Z

    return v0
.end method

.method static synthetic aAz(Lcom/android/settings/widget/MasterSwitchPreference;)Landroid/widget/Switch;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/widget/MasterSwitchPreference;->aOH:Landroid/widget/Switch;

    return-object v0
.end method


# virtual methods
.method public aAx(Z)V
    .locals 1

    iput-boolean p1, p0, Lcom/android/settings/widget/MasterSwitchPreference;->aOG:Z

    iget-object v0, p0, Lcom/android/settings/widget/MasterSwitchPreference;->aOH:Landroid/widget/Switch;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/widget/MasterSwitchPreference;->aOH:Landroid/widget/Switch;

    invoke-virtual {v0, p1}, Landroid/widget/Switch;->setEnabled(Z)V

    :cond_0
    return-void
.end method

.method public al(Landroid/support/v7/preference/l;)V
    .locals 2

    invoke-super {p0, p1}, Lcom/android/settingslib/TwoTargetPreference;->al(Landroid/support/v7/preference/l;)V

    const v0, 0x1020018

    invoke-virtual {p1, v0}, Landroid/support/v7/preference/l;->dma(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    new-instance v1, Lcom/android/settings/widget/ab;

    invoke-direct {v1, p0}, Lcom/android/settings/widget/ab;-><init>(Lcom/android/settings/widget/MasterSwitchPreference;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_0
    const v0, 0x7f0a046c

    invoke-virtual {p1, v0}, Landroid/support/v7/preference/l;->dma(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Switch;

    iput-object v0, p0, Lcom/android/settings/widget/MasterSwitchPreference;->aOH:Landroid/widget/Switch;

    iget-object v0, p0, Lcom/android/settings/widget/MasterSwitchPreference;->aOH:Landroid/widget/Switch;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/settings/widget/MasterSwitchPreference;->aOH:Landroid/widget/Switch;

    invoke-virtual {p0}, Lcom/android/settings/widget/MasterSwitchPreference;->getTitle()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Switch;->setContentDescription(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/android/settings/widget/MasterSwitchPreference;->aOH:Landroid/widget/Switch;

    iget-boolean v1, p0, Lcom/android/settings/widget/MasterSwitchPreference;->aOF:Z

    invoke-virtual {v0, v1}, Landroid/widget/Switch;->setChecked(Z)V

    iget-object v0, p0, Lcom/android/settings/widget/MasterSwitchPreference;->aOH:Landroid/widget/Switch;

    iget-boolean v1, p0, Lcom/android/settings/widget/MasterSwitchPreference;->aOG:Z

    invoke-virtual {v0, v1}, Landroid/widget/Switch;->setEnabled(Z)V

    :cond_1
    return-void
.end method

.method protected ayU()I
    .locals 1

    const v0, 0x7f0d0159

    return v0
.end method

.method public getSwitch()Landroid/widget/Switch;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/widget/MasterSwitchPreference;->aOH:Landroid/widget/Switch;

    return-object v0
.end method

.method public isChecked()Z
    .locals 1

    iget-object v0, p0, Lcom/android/settings/widget/MasterSwitchPreference;->aOH:Landroid/widget/Switch;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/widget/MasterSwitchPreference;->aOH:Landroid/widget/Switch;

    invoke-virtual {v0}, Landroid/widget/Switch;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/android/settings/widget/MasterSwitchPreference;->aOF:Z

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setChecked(Z)V
    .locals 1

    iput-boolean p1, p0, Lcom/android/settings/widget/MasterSwitchPreference;->aOF:Z

    iget-object v0, p0, Lcom/android/settings/widget/MasterSwitchPreference;->aOH:Landroid/widget/Switch;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/widget/MasterSwitchPreference;->aOH:Landroid/widget/Switch;

    invoke-virtual {v0, p1}, Landroid/widget/Switch;->setChecked(Z)V

    :cond_0
    return-void
.end method

.method public setDisabledByAdmin(Lcom/android/settingslib/n;)V
    .locals 1

    if-nez p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p0, v0}, Lcom/android/settings/widget/MasterSwitchPreference;->aAx(Z)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
