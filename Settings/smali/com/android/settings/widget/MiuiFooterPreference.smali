.class public Lcom/android/settings/widget/MiuiFooterPreference;
.super Landroid/preference/Preference;
.source "MiuiFooterPreference.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/android/settings/widget/MiuiFooterPreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    const v0, 0x7f0400aa

    const v1, 0x101008e

    invoke-static {p1, v0, v1}, Landroid/support/v4/content/a/a;->dZs(Landroid/content/Context;II)I

    move-result v0

    invoke-direct {p0, p1, p2, v0}, Landroid/preference/Preference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    invoke-direct {p0}, Lcom/android/settings/widget/MiuiFooterPreference;->azJ()V

    return-void
.end method

.method private azJ()V
    .locals 1

    const v0, 0x7f0801ea

    invoke-virtual {p0, v0}, Lcom/android/settings/widget/MiuiFooterPreference;->setIcon(I)V

    const-string/jumbo v0, "footer_preference"

    invoke-virtual {p0, v0}, Lcom/android/settings/widget/MiuiFooterPreference;->setKey(Ljava/lang/String;)V

    const v0, 0x7ffffffe

    invoke-virtual {p0, v0}, Lcom/android/settings/widget/MiuiFooterPreference;->setOrder(I)V

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/android/settings/widget/MiuiFooterPreference;->setSelectable(Z)V

    return-void
.end method


# virtual methods
.method public onBindView(Landroid/view/View;)V
    .locals 3

    const/4 v2, 0x0

    invoke-super {p0, p1}, Landroid/preference/Preference;->onBindView(Landroid/view/View;)V

    const v0, 0x1020016

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    new-instance v1, Landroid/text/method/LinkMovementMethod;

    invoke-direct {v1}, Landroid/text/method/LinkMovementMethod;-><init>()V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setClickable(Z)V

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setLongClickable(Z)V

    return-void
.end method
