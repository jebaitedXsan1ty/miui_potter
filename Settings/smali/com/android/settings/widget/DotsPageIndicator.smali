.class public Lcom/android/settings/widget/DotsPageIndicator;
.super Landroid/view/View;
.source "DotsPageIndicator.java"

# interfaces
.implements Lcom/android/internal/widget/ViewPager$OnPageChangeListener;


# static fields
.field public static final TAG:Ljava/lang/String;


# instance fields
.field private aMU:J

.field private aMV:J

.field private aMW:Z

.field private final aMX:Landroid/graphics/Path;

.field aMY:F

.field aMZ:F

.field private aNA:F

.field private aNB:[Lcom/android/settings/widget/d;

.field private aNC:I

.field private aND:Z

.field private aNE:F

.field private final aNF:Landroid/graphics/Paint;

.field private aNG:I

.field private final aNH:Landroid/graphics/Path;

.field private final aNI:Landroid/graphics/Path;

.field private final aNJ:Landroid/graphics/Path;

.field private final aNK:Landroid/graphics/Paint;

.field private aNL:Lcom/android/internal/widget/ViewPager;

.field aNa:F

.field aNb:F

.field private aNc:I

.field private aNd:F

.field private aNe:[F

.field private aNf:F

.field private aNg:I

.field private aNh:F

.field private aNi:[F

.field private aNj:F

.field aNk:F

.field aNl:F

.field aNm:F

.field aNn:F

.field private aNo:I

.field private aNp:F

.field private final aNq:Landroid/view/animation/Interpolator;

.field private aNr:Landroid/animation/AnimatorSet;

.field private aNs:[Landroid/animation/ValueAnimator;

.field private aNt:[F

.field private aNu:Landroid/animation/ValueAnimator;

.field private aNv:Lcom/android/internal/widget/ViewPager$OnPageChangeListener;

.field private aNw:I

.field private final aNx:Landroid/graphics/RectF;

.field private aNy:Lcom/android/settings/widget/c;

.field private aNz:F


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/android/settings/widget/DotsPageIndicator;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/settings/widget/DotsPageIndicator;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/android/settings/widget/DotsPageIndicator;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/android/settings/widget/DotsPageIndicator;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 7

    const/4 v4, 0x0

    const/4 v6, 0x1

    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->scaledDensity:F

    float-to-int v0, v0

    invoke-virtual {p0}, Lcom/android/settings/widget/DotsPageIndicator;->getContext()Landroid/content/Context;

    move-result-object v1

    sget-object v2, Lcom/android/settings/cw;->bYD:[I

    invoke-virtual {v1, p2, v2, p3, v4}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v1

    mul-int/lit8 v2, v0, 0x8

    const/4 v3, 0x2

    invoke-virtual {v1, v3, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v2

    iput v2, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNg:I

    iget v2, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNg:I

    div-int/lit8 v2, v2, 0x2

    int-to-float v2, v2

    iput v2, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNh:F

    iget v2, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNh:F

    const/high16 v3, 0x40000000    # 2.0f

    div-float/2addr v2, v3

    iput v2, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNp:F

    mul-int/lit8 v0, v0, 0xc

    const/4 v2, 0x3

    invoke-virtual {v1, v2, v0}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v0

    iput v0, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNo:I

    const/16 v0, 0x190

    invoke-virtual {v1, v4, v0}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v0

    int-to-long v2, v0

    iput-wide v2, p0, Lcom/android/settings/widget/DotsPageIndicator;->aMU:J

    iget-wide v2, p0, Lcom/android/settings/widget/DotsPageIndicator;->aMU:J

    const-wide/16 v4, 0x2

    div-long/2addr v2, v4

    iput-wide v2, p0, Lcom/android/settings/widget/DotsPageIndicator;->aMV:J

    const/4 v0, 0x4

    const v2, -0x7f000001

    invoke-virtual {v1, v0, v2}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v0

    iput v0, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNG:I

    const/4 v0, -0x1

    invoke-virtual {v1, v6, v0}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v0

    iput v0, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNC:I

    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0, v6}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNK:Landroid/graphics/Paint;

    iget-object v0, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNK:Landroid/graphics/Paint;

    iget v1, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNG:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0, v6}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNF:Landroid/graphics/Paint;

    iget-object v0, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNF:Landroid/graphics/Paint;

    iget v1, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNC:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_0

    const v0, 0x10c000d

    invoke-static {p1, v0}, Landroid/view/animation/AnimationUtils;->loadInterpolator(Landroid/content/Context;I)Landroid/view/animation/Interpolator;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNq:Landroid/view/animation/Interpolator;

    :goto_0
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/android/settings/widget/DotsPageIndicator;->aMX:Landroid/graphics/Path;

    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNI:Landroid/graphics/Path;

    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNH:Landroid/graphics/Path;

    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNJ:Landroid/graphics/Path;

    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNx:Landroid/graphics/RectF;

    new-instance v0, Lcom/android/settings/widget/T;

    invoke-direct {v0, p0}, Lcom/android/settings/widget/T;-><init>(Lcom/android/settings/widget/DotsPageIndicator;)V

    invoke-virtual {p0, v0}, Lcom/android/settings/widget/DotsPageIndicator;->addOnAttachStateChangeListener(Landroid/view/View$OnAttachStateChangeListener;)V

    return-void

    :cond_0
    const v0, 0x10a0004

    invoke-static {p1, v0}, Landroid/view/animation/AnimationUtils;->loadInterpolator(Landroid/content/Context;I)Landroid/view/animation/Interpolator;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNq:Landroid/view/animation/Interpolator;

    goto :goto_0
.end method

.method private ayX()V
    .locals 6

    invoke-virtual {p0}, Lcom/android/settings/widget/DotsPageIndicator;->getPaddingLeft()I

    move-result v0

    invoke-virtual {p0}, Lcom/android/settings/widget/DotsPageIndicator;->getPaddingTop()I

    move-result v1

    invoke-virtual {p0}, Lcom/android/settings/widget/DotsPageIndicator;->getWidth()I

    move-result v2

    invoke-virtual {p0}, Lcom/android/settings/widget/DotsPageIndicator;->getPaddingRight()I

    move-result v3

    sub-int/2addr v2, v3

    invoke-direct {p0}, Lcom/android/settings/widget/DotsPageIndicator;->getRequiredWidth()I

    move-result v3

    sub-int/2addr v2, v0

    sub-int/2addr v2, v3

    div-int/lit8 v2, v2, 0x2

    add-int/2addr v0, v2

    int-to-float v0, v0

    iget v2, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNh:F

    add-float/2addr v2, v0

    iget v0, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNw:I

    new-array v0, v0, [F

    iput-object v0, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNe:[F

    const/4 v0, 0x0

    :goto_0
    iget v3, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNw:I

    if-ge v0, v3, :cond_0

    iget-object v3, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNe:[F

    iget v4, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNg:I

    iget v5, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNo:I

    add-int/2addr v4, v5

    mul-int/2addr v4, v0

    int-to-float v4, v4

    add-float/2addr v4, v2

    aput v4, v3, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    int-to-float v0, v1

    iput v0, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNj:F

    int-to-float v0, v1

    iget v2, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNh:F

    add-float/2addr v0, v2

    iput v0, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNf:F

    iget v0, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNg:I

    add-int/2addr v0, v1

    int-to-float v0, v0

    iput v0, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNd:F

    invoke-direct {p0}, Lcom/android/settings/widget/DotsPageIndicator;->setCurrentPageImmediate()V

    return-void
.end method

.method private ayY()V
    .locals 1

    iget-object v0, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNr:Landroid/animation/AnimatorSet;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNr:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->isRunning()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNr:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->cancel()V

    :cond_0
    return-void
.end method

.method private ayZ()V
    .locals 1

    iget-object v0, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNu:Landroid/animation/ValueAnimator;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNu:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->isRunning()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNu:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    :cond_0
    return-void
.end method

.method static synthetic azA(Lcom/android/settings/widget/DotsPageIndicator;F)F
    .locals 0

    iput p1, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNE:F

    return p1
.end method

.method static synthetic azB(Lcom/android/settings/widget/DotsPageIndicator;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/widget/DotsPageIndicator;->ayY()V

    return-void
.end method

.method static synthetic azC(Lcom/android/settings/widget/DotsPageIndicator;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/widget/DotsPageIndicator;->azd()V

    return-void
.end method

.method static synthetic azD(Lcom/android/settings/widget/DotsPageIndicator;IF)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/android/settings/widget/DotsPageIndicator;->setDotRevealFraction(IF)V

    return-void
.end method

.method static synthetic azE(Lcom/android/settings/widget/DotsPageIndicator;IF)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/android/settings/widget/DotsPageIndicator;->setJoiningFraction(IF)V

    return-void
.end method

.method static synthetic azF(Lcom/android/settings/widget/DotsPageIndicator;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/settings/widget/DotsPageIndicator;->setPageCount(I)V

    return-void
.end method

.method private aza()V
    .locals 1

    iget-object v0, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNy:Lcom/android/settings/widget/c;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNy:Lcom/android/settings/widget/c;

    invoke-virtual {v0}, Lcom/android/settings/widget/c;->isRunning()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNy:Lcom/android/settings/widget/c;

    invoke-virtual {v0}, Lcom/android/settings/widget/c;->cancel()V

    :cond_0
    return-void
.end method

.method private azb()V
    .locals 4

    iget-object v0, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNB:[Lcom/android/settings/widget/d;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNB:[Lcom/android/settings/widget/d;

    const/4 v0, 0x0

    array-length v2, v1

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    invoke-virtual {v3}, Lcom/android/settings/widget/d;->cancel()V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method private azc()V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/widget/DotsPageIndicator;->ayZ()V

    invoke-direct {p0}, Lcom/android/settings/widget/DotsPageIndicator;->ayY()V

    invoke-direct {p0}, Lcom/android/settings/widget/DotsPageIndicator;->aza()V

    invoke-direct {p0}, Lcom/android/settings/widget/DotsPageIndicator;->azb()V

    invoke-direct {p0}, Lcom/android/settings/widget/DotsPageIndicator;->azj()V

    return-void
.end method

.method private azd()V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNt:[F

    const/4 v1, 0x0

    invoke-static {v0, v1}, Ljava/util/Arrays;->fill([FF)V

    invoke-virtual {p0}, Lcom/android/settings/widget/DotsPageIndicator;->postInvalidateOnAnimation()V

    return-void
.end method

.method private aze(IJ)Landroid/animation/ValueAnimator;
    .locals 4

    const/4 v0, 0x2

    new-array v0, v0, [F

    fill-array-data v0, :array_0

    invoke-static {v0}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v0

    new-instance v1, Lcom/android/settings/widget/X;

    invoke-direct {v1, p0, p1}, Lcom/android/settings/widget/X;-><init>(Lcom/android/settings/widget/DotsPageIndicator;I)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    iget-wide v2, p0, Lcom/android/settings/widget/DotsPageIndicator;->aMV:J

    invoke-virtual {v0, v2, v3}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    invoke-virtual {v0, p2, p3}, Landroid/animation/ValueAnimator;->setStartDelay(J)V

    iget-object v1, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNq:Landroid/view/animation/Interpolator;

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    return-object v0

    :array_0
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data
.end method

.method private azf(FIII)Landroid/animation/ValueAnimator;
    .locals 10

    const-wide/16 v8, 0x4

    const/high16 v3, 0x3e800000    # 0.25f

    const/4 v0, 0x2

    new-array v0, v0, [F

    iget v1, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNE:F

    const/4 v2, 0x0

    aput v1, v0, v2

    const/4 v1, 0x1

    aput p1, v0, v1

    invoke-static {v0}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v6

    new-instance v0, Lcom/android/settings/widget/c;

    if-le p3, p2, :cond_0

    new-instance v5, Lcom/android/settings/widget/f;

    iget v1, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNE:F

    sub-float v1, p1, v1

    mul-float/2addr v1, v3

    sub-float v1, p1, v1

    invoke-direct {v5, p0, v1}, Lcom/android/settings/widget/f;-><init>(Lcom/android/settings/widget/DotsPageIndicator;F)V

    :goto_0
    move-object v1, p0

    move v2, p2

    move v3, p3

    move v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/android/settings/widget/c;-><init>(Lcom/android/settings/widget/DotsPageIndicator;IIILcom/android/settings/widget/e;)V

    iput-object v0, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNy:Lcom/android/settings/widget/c;

    new-instance v0, Lcom/android/settings/widget/V;

    invoke-direct {v0, p0}, Lcom/android/settings/widget/V;-><init>(Lcom/android/settings/widget/DotsPageIndicator;)V

    invoke-virtual {v6, v0}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    new-instance v0, Lcom/android/settings/widget/W;

    invoke-direct {v0, p0}, Lcom/android/settings/widget/W;-><init>(Lcom/android/settings/widget/DotsPageIndicator;)V

    invoke-virtual {v6, v0}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    iget-boolean v0, p0, Lcom/android/settings/widget/DotsPageIndicator;->aND:Z

    if-eqz v0, :cond_1

    iget-wide v0, p0, Lcom/android/settings/widget/DotsPageIndicator;->aMU:J

    div-long/2addr v0, v8

    :goto_1
    invoke-virtual {v6, v0, v1}, Landroid/animation/ValueAnimator;->setStartDelay(J)V

    iget-wide v0, p0, Lcom/android/settings/widget/DotsPageIndicator;->aMU:J

    const-wide/16 v2, 0x3

    mul-long/2addr v0, v2

    div-long/2addr v0, v8

    invoke-virtual {v6, v0, v1}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    iget-object v0, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNq:Landroid/view/animation/Interpolator;

    invoke-virtual {v6, v0}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    return-object v6

    :cond_0
    new-instance v5, Lcom/android/settings/widget/g;

    iget v1, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNE:F

    sub-float/2addr v1, p1

    mul-float/2addr v1, v3

    add-float/2addr v1, p1

    invoke-direct {v5, p0, v1}, Lcom/android/settings/widget/g;-><init>(Lcom/android/settings/widget/DotsPageIndicator;F)V

    goto :goto_0

    :cond_1
    const-wide/16 v0, 0x0

    goto :goto_1
.end method

.method private azg(Landroid/graphics/Canvas;)V
    .locals 4

    iget v0, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNE:F

    iget v1, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNf:F

    iget v2, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNh:F

    iget-object v3, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNF:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    return-void
.end method

.method private azh(Landroid/graphics/Canvas;)V
    .locals 8

    const/16 v7, 0x15

    const/high16 v6, -0x40800000    # -1.0f

    iget-object v0, p0, Lcom/android/settings/widget/DotsPageIndicator;->aMX:Landroid/graphics/Path;

    invoke-virtual {v0}, Landroid/graphics/Path;->rewind()V

    const/4 v1, 0x0

    :goto_0
    iget v0, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNw:I

    if-ge v1, v0, :cond_3

    iget v0, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNw:I

    add-int/lit8 v0, v0, -0x1

    if-ne v1, v0, :cond_0

    move v0, v1

    :goto_1
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v2, v7, :cond_2

    iget-object v2, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNe:[F

    aget v2, v2, v1

    iget-object v3, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNe:[F

    aget v3, v3, v0

    iget v0, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNw:I

    add-int/lit8 v0, v0, -0x1

    if-ne v1, v0, :cond_1

    move v4, v6

    :goto_2
    iget-object v0, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNi:[F

    aget v5, v0, v1

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/android/settings/widget/DotsPageIndicator;->azi(IFFFF)Landroid/graphics/Path;

    move-result-object v0

    iget-object v2, p0, Lcom/android/settings/widget/DotsPageIndicator;->aMX:Landroid/graphics/Path;

    sget-object v3, Landroid/graphics/Path$Op;->UNION:Landroid/graphics/Path$Op;

    invoke-virtual {v2, v0, v3}, Landroid/graphics/Path;->op(Landroid/graphics/Path;Landroid/graphics/Path$Op;)Z

    :goto_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    add-int/lit8 v0, v1, 0x1

    goto :goto_1

    :cond_1
    iget-object v0, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNt:[F

    aget v4, v0, v1

    goto :goto_2

    :cond_2
    iget-object v0, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNe:[F

    aget v0, v0, v1

    iget v2, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNf:F

    iget v3, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNh:F

    iget-object v4, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNK:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v2, v3, v4}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    goto :goto_3

    :cond_3
    iget v0, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNz:F

    cmpl-float v0, v0, v6

    if-eqz v0, :cond_4

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v0, v7, :cond_4

    iget-object v0, p0, Lcom/android/settings/widget/DotsPageIndicator;->aMX:Landroid/graphics/Path;

    invoke-direct {p0}, Lcom/android/settings/widget/DotsPageIndicator;->getRetreatingJoinPath()Landroid/graphics/Path;

    move-result-object v1

    sget-object v2, Landroid/graphics/Path$Op;->UNION:Landroid/graphics/Path$Op;

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->op(Landroid/graphics/Path;Landroid/graphics/Path$Op;)Z

    :cond_4
    iget-object v0, p0, Lcom/android/settings/widget/DotsPageIndicator;->aMX:Landroid/graphics/Path;

    iget-object v1, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNK:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    return-void
.end method

.method private azi(IFFFF)Landroid/graphics/Path;
    .locals 7

    iget-object v0, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNI:Landroid/graphics/Path;

    invoke-virtual {v0}, Landroid/graphics/Path;->rewind()V

    const/4 v0, 0x0

    cmpl-float v0, p4, v0

    if-eqz v0, :cond_0

    const/high16 v0, -0x40800000    # -1.0f

    cmpl-float v0, p4, v0

    if-nez v0, :cond_2

    :cond_0
    const/4 v0, 0x0

    cmpl-float v0, p5, v0

    if-nez v0, :cond_2

    iget v0, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNc:I

    if-ne p1, v0, :cond_1

    iget-boolean v0, p0, Lcom/android/settings/widget/DotsPageIndicator;->aND:Z

    if-nez v0, :cond_2

    :cond_1
    iget-object v0, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNI:Landroid/graphics/Path;

    iget-object v1, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNe:[F

    aget v1, v1, p1

    iget v2, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNf:F

    iget v3, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNh:F

    sget-object v4, Landroid/graphics/Path$Direction;->CW:Landroid/graphics/Path$Direction;

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Path;->addCircle(FFFLandroid/graphics/Path$Direction;)V

    :cond_2
    const/4 v0, 0x0

    cmpl-float v0, p4, v0

    if-lez v0, :cond_4

    const/high16 v0, 0x3f000000    # 0.5f

    cmpg-float v0, p4, v0

    if-gez v0, :cond_4

    iget v0, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNz:F

    const/high16 v1, -0x40800000    # -1.0f

    cmpl-float v0, v0, v1

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNH:Landroid/graphics/Path;

    invoke-virtual {v0}, Landroid/graphics/Path;->rewind()V

    iget-object v0, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNH:Landroid/graphics/Path;

    iget v1, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNd:F

    invoke-virtual {v0, p2, v1}, Landroid/graphics/Path;->moveTo(FF)V

    iget-object v0, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNx:Landroid/graphics/RectF;

    iget v1, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNh:F

    sub-float v1, p2, v1

    iget v2, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNj:F

    iget v3, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNh:F

    add-float/2addr v3, p2

    iget v4, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNd:F

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/RectF;->set(FFFF)V

    iget-object v0, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNH:Landroid/graphics/Path;

    iget-object v1, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNx:Landroid/graphics/RectF;

    const/high16 v2, 0x42b40000    # 90.0f

    const/high16 v3, 0x43340000    # 180.0f

    const/4 v4, 0x1

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FFZ)V

    iget v0, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNh:F

    add-float/2addr v0, p2

    iget v1, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNo:I

    int-to-float v1, v1

    mul-float/2addr v1, p4

    add-float/2addr v0, v1

    iput v0, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNk:F

    iget v0, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNf:F

    iput v0, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNm:F

    iget v0, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNp:F

    add-float/2addr v0, p2

    iput v0, p0, Lcom/android/settings/widget/DotsPageIndicator;->aMY:F

    iget v0, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNj:F

    iput v0, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNa:F

    iget v0, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNk:F

    iput v0, p0, Lcom/android/settings/widget/DotsPageIndicator;->aMZ:F

    iget v0, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNm:F

    iget v1, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNp:F

    sub-float/2addr v0, v1

    iput v0, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNb:F

    iget-object v0, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNH:Landroid/graphics/Path;

    iget v1, p0, Lcom/android/settings/widget/DotsPageIndicator;->aMY:F

    iget v2, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNa:F

    iget v3, p0, Lcom/android/settings/widget/DotsPageIndicator;->aMZ:F

    iget v4, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNb:F

    iget v5, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNk:F

    iget v6, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNm:F

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    iput p2, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNl:F

    iget v0, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNd:F

    iput v0, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNn:F

    iget v0, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNk:F

    iput v0, p0, Lcom/android/settings/widget/DotsPageIndicator;->aMY:F

    iget v0, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNm:F

    iget v1, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNp:F

    add-float/2addr v0, v1

    iput v0, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNa:F

    iget v0, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNp:F

    add-float/2addr v0, p2

    iput v0, p0, Lcom/android/settings/widget/DotsPageIndicator;->aMZ:F

    iget v0, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNd:F

    iput v0, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNb:F

    iget-object v0, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNH:Landroid/graphics/Path;

    iget v1, p0, Lcom/android/settings/widget/DotsPageIndicator;->aMY:F

    iget v2, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNa:F

    iget v3, p0, Lcom/android/settings/widget/DotsPageIndicator;->aMZ:F

    iget v4, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNb:F

    iget v5, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNl:F

    iget v6, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNn:F

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_3

    iget-object v0, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNI:Landroid/graphics/Path;

    iget-object v1, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNH:Landroid/graphics/Path;

    sget-object v2, Landroid/graphics/Path$Op;->UNION:Landroid/graphics/Path$Op;

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->op(Landroid/graphics/Path;Landroid/graphics/Path$Op;)Z

    :cond_3
    iget-object v0, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNJ:Landroid/graphics/Path;

    invoke-virtual {v0}, Landroid/graphics/Path;->rewind()V

    iget-object v0, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNJ:Landroid/graphics/Path;

    iget v1, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNd:F

    invoke-virtual {v0, p3, v1}, Landroid/graphics/Path;->moveTo(FF)V

    iget-object v0, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNx:Landroid/graphics/RectF;

    iget v1, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNh:F

    sub-float v1, p3, v1

    iget v2, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNj:F

    iget v3, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNh:F

    add-float/2addr v3, p3

    iget v4, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNd:F

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/RectF;->set(FFFF)V

    iget-object v0, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNJ:Landroid/graphics/Path;

    iget-object v1, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNx:Landroid/graphics/RectF;

    const/high16 v2, 0x42b40000    # 90.0f

    const/high16 v3, -0x3ccc0000    # -180.0f

    const/4 v4, 0x1

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FFZ)V

    iget v0, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNh:F

    sub-float v0, p3, v0

    iget v1, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNo:I

    int-to-float v1, v1

    mul-float/2addr v1, p4

    sub-float/2addr v0, v1

    iput v0, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNk:F

    iget v0, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNf:F

    iput v0, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNm:F

    iget v0, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNp:F

    sub-float v0, p3, v0

    iput v0, p0, Lcom/android/settings/widget/DotsPageIndicator;->aMY:F

    iget v0, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNj:F

    iput v0, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNa:F

    iget v0, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNk:F

    iput v0, p0, Lcom/android/settings/widget/DotsPageIndicator;->aMZ:F

    iget v0, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNm:F

    iget v1, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNp:F

    sub-float/2addr v0, v1

    iput v0, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNb:F

    iget-object v0, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNJ:Landroid/graphics/Path;

    iget v1, p0, Lcom/android/settings/widget/DotsPageIndicator;->aMY:F

    iget v2, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNa:F

    iget v3, p0, Lcom/android/settings/widget/DotsPageIndicator;->aMZ:F

    iget v4, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNb:F

    iget v5, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNk:F

    iget v6, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNm:F

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    iput p3, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNl:F

    iget v0, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNd:F

    iput v0, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNn:F

    iget v0, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNk:F

    iput v0, p0, Lcom/android/settings/widget/DotsPageIndicator;->aMY:F

    iget v0, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNm:F

    iget v1, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNp:F

    add-float/2addr v0, v1

    iput v0, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNa:F

    iget v0, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNl:F

    iget v1, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNp:F

    sub-float/2addr v0, v1

    iput v0, p0, Lcom/android/settings/widget/DotsPageIndicator;->aMZ:F

    iget v0, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNd:F

    iput v0, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNb:F

    iget-object v0, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNJ:Landroid/graphics/Path;

    iget v1, p0, Lcom/android/settings/widget/DotsPageIndicator;->aMY:F

    iget v2, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNa:F

    iget v3, p0, Lcom/android/settings/widget/DotsPageIndicator;->aMZ:F

    iget v4, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNb:F

    iget v5, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNl:F

    iget v6, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNn:F

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_4

    iget-object v0, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNI:Landroid/graphics/Path;

    iget-object v1, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNJ:Landroid/graphics/Path;

    sget-object v2, Landroid/graphics/Path$Op;->UNION:Landroid/graphics/Path$Op;

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->op(Landroid/graphics/Path;Landroid/graphics/Path$Op;)Z

    :cond_4
    const/high16 v0, 0x3f000000    # 0.5f

    cmpl-float v0, p4, v0

    if-lez v0, :cond_5

    const/high16 v0, 0x3f800000    # 1.0f

    cmpg-float v0, p4, v0

    if-gez v0, :cond_5

    iget v0, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNz:F

    const/high16 v1, -0x40800000    # -1.0f

    cmpl-float v0, v0, v1

    if-nez v0, :cond_5

    iget-object v0, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNI:Landroid/graphics/Path;

    iget v1, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNd:F

    invoke-virtual {v0, p2, v1}, Landroid/graphics/Path;->moveTo(FF)V

    iget-object v0, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNx:Landroid/graphics/RectF;

    iget v1, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNh:F

    sub-float v1, p2, v1

    iget v2, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNj:F

    iget v3, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNh:F

    add-float/2addr v3, p2

    iget v4, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNd:F

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/RectF;->set(FFFF)V

    iget-object v0, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNI:Landroid/graphics/Path;

    iget-object v1, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNx:Landroid/graphics/RectF;

    const/high16 v2, 0x42b40000    # 90.0f

    const/high16 v3, 0x43340000    # 180.0f

    const/4 v4, 0x1

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FFZ)V

    iget v0, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNh:F

    add-float/2addr v0, p2

    iget v1, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNo:I

    div-int/lit8 v1, v1, 0x2

    int-to-float v1, v1

    add-float/2addr v0, v1

    iput v0, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNk:F

    iget v0, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNf:F

    iget v1, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNh:F

    mul-float/2addr v1, p4

    sub-float/2addr v0, v1

    iput v0, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNm:F

    iget v0, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNk:F

    iget v1, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNh:F

    mul-float/2addr v1, p4

    sub-float/2addr v0, v1

    iput v0, p0, Lcom/android/settings/widget/DotsPageIndicator;->aMY:F

    iget v0, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNj:F

    iput v0, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNa:F

    iget v0, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNk:F

    const/high16 v1, 0x3f800000    # 1.0f

    sub-float/2addr v1, p4

    iget v2, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNh:F

    mul-float/2addr v1, v2

    sub-float/2addr v0, v1

    iput v0, p0, Lcom/android/settings/widget/DotsPageIndicator;->aMZ:F

    iget v0, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNm:F

    iput v0, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNb:F

    iget-object v0, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNI:Landroid/graphics/Path;

    iget v1, p0, Lcom/android/settings/widget/DotsPageIndicator;->aMY:F

    iget v2, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNa:F

    iget v3, p0, Lcom/android/settings/widget/DotsPageIndicator;->aMZ:F

    iget v4, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNb:F

    iget v5, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNk:F

    iget v6, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNm:F

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    iput p3, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNl:F

    iget v0, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNj:F

    iput v0, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNn:F

    iget v0, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNk:F

    const/high16 v1, 0x3f800000    # 1.0f

    sub-float/2addr v1, p4

    iget v2, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNh:F

    mul-float/2addr v1, v2

    add-float/2addr v0, v1

    iput v0, p0, Lcom/android/settings/widget/DotsPageIndicator;->aMY:F

    iget v0, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNm:F

    iput v0, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNa:F

    iget v0, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNk:F

    iget v1, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNh:F

    mul-float/2addr v1, p4

    add-float/2addr v0, v1

    iput v0, p0, Lcom/android/settings/widget/DotsPageIndicator;->aMZ:F

    iget v0, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNj:F

    iput v0, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNb:F

    iget-object v0, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNI:Landroid/graphics/Path;

    iget v1, p0, Lcom/android/settings/widget/DotsPageIndicator;->aMY:F

    iget v2, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNa:F

    iget v3, p0, Lcom/android/settings/widget/DotsPageIndicator;->aMZ:F

    iget v4, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNb:F

    iget v5, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNl:F

    iget v6, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNn:F

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    iget-object v0, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNx:Landroid/graphics/RectF;

    iget v1, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNh:F

    sub-float v1, p3, v1

    iget v2, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNj:F

    iget v3, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNh:F

    add-float/2addr v3, p3

    iget v4, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNd:F

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/RectF;->set(FFFF)V

    iget-object v0, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNI:Landroid/graphics/Path;

    iget-object v1, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNx:Landroid/graphics/RectF;

    const/high16 v2, 0x43870000    # 270.0f

    const/high16 v3, 0x43340000    # 180.0f

    const/4 v4, 0x1

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FFZ)V

    iget v0, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNf:F

    iget v1, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNh:F

    mul-float/2addr v1, p4

    add-float/2addr v0, v1

    iput v0, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNm:F

    iget v0, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNk:F

    iget v1, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNh:F

    mul-float/2addr v1, p4

    add-float/2addr v0, v1

    iput v0, p0, Lcom/android/settings/widget/DotsPageIndicator;->aMY:F

    iget v0, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNd:F

    iput v0, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNa:F

    iget v0, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNk:F

    const/high16 v1, 0x3f800000    # 1.0f

    sub-float/2addr v1, p4

    iget v2, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNh:F

    mul-float/2addr v1, v2

    add-float/2addr v0, v1

    iput v0, p0, Lcom/android/settings/widget/DotsPageIndicator;->aMZ:F

    iget v0, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNm:F

    iput v0, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNb:F

    iget-object v0, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNI:Landroid/graphics/Path;

    iget v1, p0, Lcom/android/settings/widget/DotsPageIndicator;->aMY:F

    iget v2, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNa:F

    iget v3, p0, Lcom/android/settings/widget/DotsPageIndicator;->aMZ:F

    iget v4, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNb:F

    iget v5, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNk:F

    iget v6, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNm:F

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    iput p2, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNl:F

    iget v0, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNd:F

    iput v0, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNn:F

    iget v0, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNk:F

    const/high16 v1, 0x3f800000    # 1.0f

    sub-float/2addr v1, p4

    iget v2, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNh:F

    mul-float/2addr v1, v2

    sub-float/2addr v0, v1

    iput v0, p0, Lcom/android/settings/widget/DotsPageIndicator;->aMY:F

    iget v0, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNm:F

    iput v0, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNa:F

    iget v0, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNk:F

    iget v1, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNh:F

    mul-float/2addr v1, p4

    sub-float/2addr v0, v1

    iput v0, p0, Lcom/android/settings/widget/DotsPageIndicator;->aMZ:F

    iget v0, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNn:F

    iput v0, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNb:F

    iget-object v0, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNI:Landroid/graphics/Path;

    iget v1, p0, Lcom/android/settings/widget/DotsPageIndicator;->aMY:F

    iget v2, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNa:F

    iget v3, p0, Lcom/android/settings/widget/DotsPageIndicator;->aMZ:F

    iget v4, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNb:F

    iget v5, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNl:F

    iget v6, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNn:F

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    :cond_5
    const/high16 v0, 0x3f800000    # 1.0f

    cmpl-float v0, p4, v0

    if-nez v0, :cond_6

    iget v0, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNz:F

    const/high16 v1, -0x40800000    # -1.0f

    cmpl-float v0, v0, v1

    if-nez v0, :cond_6

    iget-object v0, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNx:Landroid/graphics/RectF;

    iget v1, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNh:F

    sub-float v1, p2, v1

    iget v2, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNj:F

    iget v3, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNh:F

    add-float/2addr v3, p3

    iget v4, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNd:F

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/RectF;->set(FFFF)V

    iget-object v0, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNI:Landroid/graphics/Path;

    iget-object v1, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNx:Landroid/graphics/RectF;

    iget v2, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNh:F

    iget v3, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNh:F

    sget-object v4, Landroid/graphics/Path$Direction;->CW:Landroid/graphics/Path$Direction;

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Path;->addRoundRect(Landroid/graphics/RectF;FFLandroid/graphics/Path$Direction;)V

    :cond_6
    const v0, 0x3727c5ac    # 1.0E-5f

    cmpl-float v0, p5, v0

    if-lez v0, :cond_7

    iget-object v0, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNI:Landroid/graphics/Path;

    iget v1, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNf:F

    iget v2, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNh:F

    mul-float/2addr v2, p5

    sget-object v3, Landroid/graphics/Path$Direction;->CW:Landroid/graphics/Path$Direction;

    invoke-virtual {v0, p2, v1, v2, v3}, Landroid/graphics/Path;->addCircle(FFFLandroid/graphics/Path$Direction;)V

    :cond_7
    iget-object v0, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNI:Landroid/graphics/Path;

    return-object v0
.end method

.method private azj()V
    .locals 3

    const/4 v2, 0x0

    const/high16 v1, -0x40800000    # -1.0f

    iget v0, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNw:I

    if-lez v0, :cond_0

    iget v0, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNw:I

    add-int/lit8 v0, v0, -0x1

    new-array v0, v0, [F

    iput-object v0, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNt:[F

    iget-object v0, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNt:[F

    invoke-static {v0, v2}, Ljava/util/Arrays;->fill([FF)V

    iget v0, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNw:I

    new-array v0, v0, [F

    iput-object v0, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNi:[F

    iget-object v0, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNi:[F

    invoke-static {v0, v2}, Ljava/util/Arrays;->fill([FF)V

    iput v1, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNz:F

    iput v1, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNA:F

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/settings/widget/DotsPageIndicator;->aND:Z

    :cond_0
    return-void
.end method

.method private azk()V
    .locals 2

    new-instance v0, Landroid/animation/AnimatorSet;

    invoke-direct {v0}, Landroid/animation/AnimatorSet;-><init>()V

    iput-object v0, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNr:Landroid/animation/AnimatorSet;

    iget-object v0, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNr:Landroid/animation/AnimatorSet;

    iget-object v1, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNs:[Landroid/animation/ValueAnimator;

    invoke-virtual {v0, v1}, Landroid/animation/AnimatorSet;->playTogether([Landroid/animation/Animator;)V

    iget-object v0, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNr:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->start()V

    return-void
.end method

.method static synthetic azl(Lcom/android/settings/widget/DotsPageIndicator;)J
    .locals 2

    iget-wide v0, p0, Lcom/android/settings/widget/DotsPageIndicator;->aMV:J

    return-wide v0
.end method

.method static synthetic azm(Lcom/android/settings/widget/DotsPageIndicator;)[F
    .locals 1

    iget-object v0, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNe:[F

    return-object v0
.end method

.method static synthetic azn(Lcom/android/settings/widget/DotsPageIndicator;)F
    .locals 1

    iget v0, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNh:F

    return v0
.end method

.method static synthetic azo(Lcom/android/settings/widget/DotsPageIndicator;)Landroid/view/animation/Interpolator;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNq:Landroid/view/animation/Interpolator;

    return-object v0
.end method

.method static synthetic azp(Lcom/android/settings/widget/DotsPageIndicator;)Lcom/android/settings/widget/c;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNy:Lcom/android/settings/widget/c;

    return-object v0
.end method

.method static synthetic azq(Lcom/android/settings/widget/DotsPageIndicator;)F
    .locals 1

    iget v0, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNz:F

    return v0
.end method

.method static synthetic azr(Lcom/android/settings/widget/DotsPageIndicator;)F
    .locals 1

    iget v0, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNA:F

    return v0
.end method

.method static synthetic azs(Lcom/android/settings/widget/DotsPageIndicator;)[Lcom/android/settings/widget/d;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNB:[Lcom/android/settings/widget/d;

    return-object v0
.end method

.method static synthetic azt(Lcom/android/settings/widget/DotsPageIndicator;)F
    .locals 1

    iget v0, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNE:F

    return v0
.end method

.method static synthetic azu(Lcom/android/settings/widget/DotsPageIndicator;)Lcom/android/internal/widget/ViewPager;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNL:Lcom/android/internal/widget/ViewPager;

    return-object v0
.end method

.method static synthetic azv(Lcom/android/settings/widget/DotsPageIndicator;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/android/settings/widget/DotsPageIndicator;->aMW:Z

    return p1
.end method

.method static synthetic azw(Lcom/android/settings/widget/DotsPageIndicator;F)F
    .locals 0

    iput p1, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNz:F

    return p1
.end method

.method static synthetic azx(Lcom/android/settings/widget/DotsPageIndicator;F)F
    .locals 0

    iput p1, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNA:F

    return p1
.end method

.method static synthetic azy(Lcom/android/settings/widget/DotsPageIndicator;[Lcom/android/settings/widget/d;)[Lcom/android/settings/widget/d;
    .locals 0

    iput-object p1, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNB:[Lcom/android/settings/widget/d;

    return-object p1
.end method

.method static synthetic azz(Lcom/android/settings/widget/DotsPageIndicator;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/android/settings/widget/DotsPageIndicator;->aND:Z

    return p1
.end method

.method private getDesiredHeight()I
    .locals 2

    invoke-virtual {p0}, Lcom/android/settings/widget/DotsPageIndicator;->getPaddingTop()I

    move-result v0

    iget v1, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNg:I

    add-int/2addr v0, v1

    invoke-virtual {p0}, Lcom/android/settings/widget/DotsPageIndicator;->getPaddingBottom()I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method private getDesiredWidth()I
    .locals 2

    invoke-virtual {p0}, Lcom/android/settings/widget/DotsPageIndicator;->getPaddingLeft()I

    move-result v0

    invoke-direct {p0}, Lcom/android/settings/widget/DotsPageIndicator;->getRequiredWidth()I

    move-result v1

    add-int/2addr v0, v1

    invoke-virtual {p0}, Lcom/android/settings/widget/DotsPageIndicator;->getPaddingRight()I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method private getRequiredWidth()I
    .locals 3

    iget v0, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNw:I

    iget v1, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNg:I

    mul-int/2addr v0, v1

    iget v1, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNw:I

    add-int/lit8 v1, v1, -0x1

    iget v2, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNo:I

    mul-int/2addr v1, v2

    add-int/2addr v0, v1

    return v0
.end method

.method private getRetreatingJoinPath()Landroid/graphics/Path;
    .locals 5

    iget-object v0, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNI:Landroid/graphics/Path;

    invoke-virtual {v0}, Landroid/graphics/Path;->rewind()V

    iget-object v0, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNx:Landroid/graphics/RectF;

    iget v1, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNz:F

    iget v2, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNj:F

    iget v3, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNA:F

    iget v4, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNd:F

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/RectF;->set(FFFF)V

    iget-object v0, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNI:Landroid/graphics/Path;

    iget-object v1, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNx:Landroid/graphics/RectF;

    iget v2, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNh:F

    iget v3, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNh:F

    sget-object v4, Landroid/graphics/Path$Direction;->CW:Landroid/graphics/Path$Direction;

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Path;->addRoundRect(Landroid/graphics/RectF;FFLandroid/graphics/Path$Direction;)V

    iget-object v0, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNI:Landroid/graphics/Path;

    return-object v0
.end method

.method private setCurrentPageImmediate()V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNL:Lcom/android/internal/widget/ViewPager;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNL:Lcom/android/internal/widget/ViewPager;

    invoke-virtual {v0}, Lcom/android/internal/widget/ViewPager;->getCurrentItem()I

    move-result v0

    iput v0, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNc:I

    :goto_0
    iget v0, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNw:I

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNe:[F

    iget v1, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNc:I

    aget v0, v0, v1

    iput v0, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNE:F

    :cond_0
    return-void

    :cond_1
    iput v1, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNc:I

    goto :goto_0
.end method

.method private setDotRevealFraction(IF)V
    .locals 1

    iget-object v0, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNi:[F

    aput p2, v0, p1

    invoke-virtual {p0}, Lcom/android/settings/widget/DotsPageIndicator;->postInvalidateOnAnimation()V

    return-void
.end method

.method private setJoiningFraction(IF)V
    .locals 1

    iget-object v0, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNt:[F

    aput p2, v0, p1

    invoke-virtual {p0}, Lcom/android/settings/widget/DotsPageIndicator;->postInvalidateOnAnimation()V

    return-void
.end method

.method private setPageCount(I)V
    .locals 0

    iput p1, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNw:I

    invoke-direct {p0}, Lcom/android/settings/widget/DotsPageIndicator;->ayX()V

    invoke-direct {p0}, Lcom/android/settings/widget/DotsPageIndicator;->azj()V

    return-void
.end method

.method private setSelectedPage(I)V
    .locals 12

    const/4 v0, 0x0

    iget v1, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNc:I

    if-eq p1, v1, :cond_0

    iget v1, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNw:I

    if-nez v1, :cond_1

    :cond_0
    return-void

    :cond_1
    iget v2, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNc:I

    iput p1, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNc:I

    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x10

    if-lt v1, v3, :cond_4

    invoke-direct {p0}, Lcom/android/settings/widget/DotsPageIndicator;->azc()V

    sub-int v1, p1, v2

    invoke-static {v1}, Ljava/lang/Math;->abs(I)I

    move-result v3

    iget-object v1, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNe:[F

    aget v1, v1, p1

    invoke-direct {p0, v1, v2, p1, v3}, Lcom/android/settings/widget/DotsPageIndicator;->azf(FIII)Landroid/animation/ValueAnimator;

    move-result-object v1

    iput-object v1, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNu:Landroid/animation/ValueAnimator;

    new-array v1, v3, [Landroid/animation/ValueAnimator;

    iput-object v1, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNs:[Landroid/animation/ValueAnimator;

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_3

    iget-object v4, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNs:[Landroid/animation/ValueAnimator;

    if-le p1, v2, :cond_2

    add-int v0, v2, v1

    :goto_1
    int-to-long v6, v1

    iget-wide v8, p0, Lcom/android/settings/widget/DotsPageIndicator;->aMU:J

    const-wide/16 v10, 0x8

    div-long/2addr v8, v10

    mul-long/2addr v6, v8

    invoke-direct {p0, v0, v6, v7}, Lcom/android/settings/widget/DotsPageIndicator;->aze(IJ)Landroid/animation/ValueAnimator;

    move-result-object v0

    aput-object v0, v4, v1

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_2
    add-int/lit8 v0, v2, -0x1

    sub-int/2addr v0, v1

    goto :goto_1

    :cond_3
    iget-object v0, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNu:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    invoke-direct {p0}, Lcom/android/settings/widget/DotsPageIndicator;->azk()V

    :goto_2
    return-void

    :cond_4
    invoke-direct {p0}, Lcom/android/settings/widget/DotsPageIndicator;->setCurrentPageImmediate()V

    invoke-virtual {p0}, Lcom/android/settings/widget/DotsPageIndicator;->invalidate()V

    goto :goto_2
.end method


# virtual methods
.method public clearAnimation()V
    .locals 2

    invoke-super {p0}, Landroid/view/View;->clearAnimation()V

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-lt v0, v1, :cond_0

    invoke-direct {p0}, Lcom/android/settings/widget/DotsPageIndicator;->azc()V

    :cond_0
    return-void
.end method

.method getCurrentPage()I
    .locals 1

    iget v0, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNc:I

    return v0
.end method

.method getDotCenterY()F
    .locals 1

    iget v0, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNf:F

    return v0
.end method

.method getSelectedColour()I
    .locals 1

    iget v0, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNC:I

    return v0
.end method

.method getSelectedDotX()F
    .locals 1

    iget v0, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNE:F

    return v0
.end method

.method getUnselectedColour()I
    .locals 1

    iget v0, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNG:I

    return v0
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 1

    iget-object v0, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNL:Lcom/android/internal/widget/ViewPager;

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNw:I

    if-nez v0, :cond_1

    :cond_0
    return-void

    :cond_1
    invoke-direct {p0, p1}, Lcom/android/settings/widget/DotsPageIndicator;->azh(Landroid/graphics/Canvas;)V

    invoke-direct {p0, p1}, Lcom/android/settings/widget/DotsPageIndicator;->azg(Landroid/graphics/Canvas;)V

    return-void
.end method

.method protected onMeasure(II)V
    .locals 3

    invoke-direct {p0}, Lcom/android/settings/widget/DotsPageIndicator;->getDesiredHeight()I

    move-result v0

    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    :goto_0
    invoke-direct {p0}, Lcom/android/settings/widget/DotsPageIndicator;->getDesiredWidth()I

    move-result v1

    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v2

    sparse-switch v2, :sswitch_data_1

    :goto_1
    invoke-virtual {p0, v1, v0}, Lcom/android/settings/widget/DotsPageIndicator;->setMeasuredDimension(II)V

    invoke-direct {p0}, Lcom/android/settings/widget/DotsPageIndicator;->ayX()V

    return-void

    :sswitch_0
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    goto :goto_0

    :sswitch_1
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    goto :goto_0

    :sswitch_2
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    goto :goto_1

    :sswitch_3
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v2

    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v1

    goto :goto_1

    nop

    :sswitch_data_0
    .sparse-switch
        -0x80000000 -> :sswitch_1
        0x40000000 -> :sswitch_0
    .end sparse-switch

    :sswitch_data_1
    .sparse-switch
        -0x80000000 -> :sswitch_3
        0x40000000 -> :sswitch_2
    .end sparse-switch
.end method

.method public onPageScrollStateChanged(I)V
    .locals 1

    iget-object v0, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNv:Lcom/android/internal/widget/ViewPager$OnPageChangeListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNv:Lcom/android/internal/widget/ViewPager$OnPageChangeListener;

    invoke-interface {v0, p1}, Lcom/android/internal/widget/ViewPager$OnPageChangeListener;->onPageScrollStateChanged(I)V

    :cond_0
    return-void
.end method

.method public onPageScrolled(IFI)V
    .locals 1

    iget-object v0, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNv:Lcom/android/internal/widget/ViewPager$OnPageChangeListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNv:Lcom/android/internal/widget/ViewPager$OnPageChangeListener;

    invoke-interface {v0, p1, p2, p3}, Lcom/android/internal/widget/ViewPager$OnPageChangeListener;->onPageScrolled(IFI)V

    :cond_0
    return-void
.end method

.method public onPageSelected(I)V
    .locals 1

    iget-boolean v0, p0, Lcom/android/settings/widget/DotsPageIndicator;->aMW:Z

    if-eqz v0, :cond_1

    invoke-direct {p0, p1}, Lcom/android/settings/widget/DotsPageIndicator;->setSelectedPage(I)V

    :goto_0
    iget-object v0, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNv:Lcom/android/internal/widget/ViewPager$OnPageChangeListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNv:Lcom/android/internal/widget/ViewPager$OnPageChangeListener;

    invoke-interface {v0, p1}, Lcom/android/internal/widget/ViewPager$OnPageChangeListener;->onPageSelected(I)V

    :cond_0
    return-void

    :cond_1
    invoke-direct {p0}, Lcom/android/settings/widget/DotsPageIndicator;->setCurrentPageImmediate()V

    goto :goto_0
.end method

.method protected onSizeChanged(IIII)V
    .locals 0

    invoke-virtual {p0, p1, p2}, Lcom/android/settings/widget/DotsPageIndicator;->setMeasuredDimension(II)V

    invoke-direct {p0}, Lcom/android/settings/widget/DotsPageIndicator;->ayX()V

    return-void
.end method

.method public setOnPageChangeListener(Lcom/android/internal/widget/ViewPager$OnPageChangeListener;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNv:Lcom/android/internal/widget/ViewPager$OnPageChangeListener;

    return-void
.end method

.method public setViewPager(Lcom/android/internal/widget/ViewPager;)V
    .locals 2

    iput-object p1, p0, Lcom/android/settings/widget/DotsPageIndicator;->aNL:Lcom/android/internal/widget/ViewPager;

    invoke-virtual {p1, p0}, Lcom/android/internal/widget/ViewPager;->setOnPageChangeListener(Lcom/android/internal/widget/ViewPager$OnPageChangeListener;)V

    invoke-virtual {p1}, Lcom/android/internal/widget/ViewPager;->getAdapter()Lcom/android/internal/widget/PagerAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/internal/widget/PagerAdapter;->getCount()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/android/settings/widget/DotsPageIndicator;->setPageCount(I)V

    invoke-virtual {p1}, Lcom/android/internal/widget/ViewPager;->getAdapter()Lcom/android/internal/widget/PagerAdapter;

    move-result-object v0

    new-instance v1, Lcom/android/settings/widget/U;

    invoke-direct {v1, p0}, Lcom/android/settings/widget/U;-><init>(Lcom/android/settings/widget/DotsPageIndicator;)V

    invoke-virtual {v0, v1}, Lcom/android/internal/widget/PagerAdapter;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    invoke-direct {p0}, Lcom/android/settings/widget/DotsPageIndicator;->setCurrentPageImmediate()V

    return-void
.end method
