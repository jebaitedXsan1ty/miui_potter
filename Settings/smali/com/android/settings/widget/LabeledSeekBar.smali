.class public Lcom/android/settings/widget/LabeledSeekBar;
.super Landroid/widget/SeekBar;
.source "LabeledSeekBar.java"


# instance fields
.field private final aOp:Landroid/support/v4/widget/a;

.field private aOq:[Ljava/lang/String;

.field private aOr:Landroid/widget/SeekBar$OnSeekBarChangeListener;

.field private final aOs:Landroid/widget/SeekBar$OnSeekBarChangeListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    const v0, 0x101007b

    invoke-direct {p0, p1, p2, v0}, Lcom/android/settings/widget/LabeledSeekBar;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/android/settings/widget/LabeledSeekBar;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V
    .locals 1

    invoke-direct {p0, p1, p2, p3, p4}, Landroid/widget/SeekBar;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V

    new-instance v0, Lcom/android/settings/widget/Y;

    invoke-direct {v0, p0}, Lcom/android/settings/widget/Y;-><init>(Lcom/android/settings/widget/LabeledSeekBar;)V

    iput-object v0, p0, Lcom/android/settings/widget/LabeledSeekBar;->aOs:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    new-instance v0, Lcom/android/settings/widget/h;

    invoke-direct {v0, p0, p0}, Lcom/android/settings/widget/h;-><init>(Lcom/android/settings/widget/LabeledSeekBar;Lcom/android/settings/widget/LabeledSeekBar;)V

    iput-object v0, p0, Lcom/android/settings/widget/LabeledSeekBar;->aOp:Landroid/support/v4/widget/a;

    iget-object v0, p0, Lcom/android/settings/widget/LabeledSeekBar;->aOp:Landroid/support/v4/widget/a;

    invoke-static {p0, v0}, Landroid/support/v4/view/z;->dPu(Landroid/view/View;Landroid/support/v4/view/d;)V

    iget-object v0, p0, Lcom/android/settings/widget/LabeledSeekBar;->aOs:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    invoke-super {p0, v0}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    return-void
.end method

.method private azN(I)V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/widget/LabeledSeekBar;->aOp:Landroid/support/v4/widget/a;

    invoke-virtual {v0}, Landroid/support/v4/widget/a;->dTO()V

    iget-object v0, p0, Lcom/android/settings/widget/LabeledSeekBar;->aOp:Landroid/support/v4/widget/a;

    const/4 v1, 0x1

    invoke-virtual {v0, p1, v1}, Landroid/support/v4/widget/a;->dTJ(II)Z

    return-void
.end method

.method static synthetic azO(Lcom/android/settings/widget/LabeledSeekBar;)[Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/widget/LabeledSeekBar;->aOq:[Ljava/lang/String;

    return-object v0
.end method

.method static synthetic azP(Lcom/android/settings/widget/LabeledSeekBar;)Landroid/widget/SeekBar$OnSeekBarChangeListener;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/widget/LabeledSeekBar;->aOr:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    return-object v0
.end method

.method static synthetic azQ(Lcom/android/settings/widget/LabeledSeekBar;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/settings/widget/LabeledSeekBar;->azN(I)V

    return-void
.end method


# virtual methods
.method protected dispatchHoverEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    iget-object v0, p0, Lcom/android/settings/widget/LabeledSeekBar;->aOp:Landroid/support/v4/widget/a;

    invoke-virtual {v0, p1}, Landroid/support/v4/widget/a;->dispatchHoverEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-super {p0, p1}, Landroid/widget/SeekBar;->dispatchHoverEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public setLabels([Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/widget/LabeledSeekBar;->aOq:[Ljava/lang/String;

    return-void
.end method

.method public setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/widget/LabeledSeekBar;->aOr:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    return-void
.end method

.method public declared-synchronized setProgress(I)V
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/android/settings/widget/LabeledSeekBar;->aOp:Landroid/support/v4/widget/a;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/widget/LabeledSeekBar;->aOp:Landroid/support/v4/widget/a;

    invoke-virtual {v0}, Landroid/support/v4/widget/a;->dTO()V

    :cond_0
    invoke-super {p0, p1}, Landroid/widget/SeekBar;->setProgress(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
