.class public Lcom/android/settings/widget/ChartNetworkSeriesView;
.super Landroid/view/View;
.source "ChartNetworkSeriesView.java"


# instance fields
.field private aNW:J

.field private aNX:J

.field private aNY:Z

.field private aNZ:Lcom/android/settings/widget/k;

.field private aOa:J

.field private aOb:J

.field private aOc:Landroid/graphics/Paint;

.field private aOd:Landroid/graphics/Paint;

.field private aOe:Landroid/graphics/Paint;

.field private aOf:Landroid/graphics/Paint;

.field private aOg:Landroid/graphics/Path;

.field private aOh:Landroid/graphics/Path;

.field private aOi:Landroid/graphics/Path;

.field private aOj:Z

.field private aOk:I

.field private aOl:Z

.field private aOm:J

.field private aOn:Landroid/net/NetworkStatsHistory;

.field private aOo:Lcom/android/settings/widget/k;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/android/settings/widget/ChartNetworkSeriesView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/android/settings/widget/ChartNetworkSeriesView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 6

    const/high16 v4, -0x10000

    const/4 v5, 0x0

    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    const-wide/high16 v0, -0x8000000000000000L

    iput-wide v0, p0, Lcom/android/settings/widget/ChartNetworkSeriesView;->aNX:J

    iput-boolean v5, p0, Lcom/android/settings/widget/ChartNetworkSeriesView;->aOj:Z

    iput-boolean v5, p0, Lcom/android/settings/widget/ChartNetworkSeriesView;->aNY:Z

    iput-boolean v5, p0, Lcom/android/settings/widget/ChartNetworkSeriesView;->aOl:Z

    sget-object v0, Lcom/android/settings/cw;->bYG:[I

    invoke-virtual {p1, p2, v0, p3, v5}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    const/4 v1, 0x3

    invoke-virtual {v0, v1, v4}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v1

    invoke-virtual {v0, v5, v4}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v2

    const/4 v3, 0x1

    invoke-virtual {v0, v3, v4}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v3

    const/4 v4, 0x2

    invoke-virtual {v0, v4, v5}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v4

    invoke-virtual {p0, v1, v2, v3}, Lcom/android/settings/widget/ChartNetworkSeriesView;->setChartColor(III)V

    invoke-virtual {p0, v4}, Lcom/android/settings/widget/ChartNetworkSeriesView;->setSafeRegion(I)V

    invoke-virtual {p0, v5}, Lcom/android/settings/widget/ChartNetworkSeriesView;->setWillNotDraw(Z)V

    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/android/settings/widget/ChartNetworkSeriesView;->aOi:Landroid/graphics/Path;

    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/android/settings/widget/ChartNetworkSeriesView;->aOh:Landroid/graphics/Path;

    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/android/settings/widget/ChartNetworkSeriesView;->aOg:Landroid/graphics/Path;

    return-void
.end method

.method private azK()V
    .locals 23

    const-wide/16 v2, 0x0

    move-object/from16 v0, p0

    iput-wide v2, v0, Lcom/android/settings/widget/ChartNetworkSeriesView;->aOa:J

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/settings/widget/ChartNetworkSeriesView;->aOi:Landroid/graphics/Path;

    invoke-virtual {v2}, Landroid/graphics/Path;->reset()V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/settings/widget/ChartNetworkSeriesView;->aOh:Landroid/graphics/Path;

    invoke-virtual {v2}, Landroid/graphics/Path;->reset()V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/settings/widget/ChartNetworkSeriesView;->aOg:Landroid/graphics/Path;

    invoke-virtual {v2}, Landroid/graphics/Path;->reset()V

    const/4 v2, 0x1

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/android/settings/widget/ChartNetworkSeriesView;->aOj:Z

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/settings/widget/ChartNetworkSeriesView;->aOn:Landroid/net/NetworkStatsHistory;

    if-eqz v2, :cond_0

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/settings/widget/ChartNetworkSeriesView;->aOn:Landroid/net/NetworkStatsHistory;

    invoke-virtual {v2}, Landroid/net/NetworkStatsHistory;->size()I

    move-result v2

    const/4 v3, 0x2

    if-ge v2, v3, :cond_1

    :cond_0
    return-void

    :cond_1
    invoke-virtual/range {p0 .. p0}, Lcom/android/settings/widget/ChartNetworkSeriesView;->getWidth()I

    invoke-virtual/range {p0 .. p0}, Lcom/android/settings/widget/ChartNetworkSeriesView;->getHeight()I

    move-result v13

    const/4 v9, 0x0

    int-to-float v8, v13

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/settings/widget/ChartNetworkSeriesView;->aNZ:Lcom/android/settings/widget/k;

    invoke-interface {v2, v9}, Lcom/android/settings/widget/k;->aAr(F)J

    move-result-wide v6

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/settings/widget/ChartNetworkSeriesView;->aOi:Landroid/graphics/Path;

    invoke-virtual {v2, v9, v8}, Landroid/graphics/Path;->moveTo(FF)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/settings/widget/ChartNetworkSeriesView;->aOh:Landroid/graphics/Path;

    invoke-virtual {v2, v9, v8}, Landroid/graphics/Path;->moveTo(FF)V

    const-wide/16 v4, 0x0

    const/4 v3, 0x0

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/settings/widget/ChartNetworkSeriesView;->aOn:Landroid/net/NetworkStatsHistory;

    move-object/from16 v0, p0

    iget-wide v10, v0, Lcom/android/settings/widget/ChartNetworkSeriesView;->aOm:J

    invoke-virtual {v2, v10, v11}, Landroid/net/NetworkStatsHistory;->getIndexBefore(J)I

    move-result v2

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/android/settings/widget/ChartNetworkSeriesView;->aOn:Landroid/net/NetworkStatsHistory;

    move-object/from16 v0, p0

    iget-wide v14, v0, Lcom/android/settings/widget/ChartNetworkSeriesView;->aNW:J

    invoke-virtual {v10, v14, v15}, Landroid/net/NetworkStatsHistory;->getIndexAfter(J)I

    move-result v14

    move v11, v2

    move-object/from16 v22, v3

    move-wide v2, v4

    move-wide v4, v6

    move-object/from16 v6, v22

    :goto_0
    if-gt v11, v14, :cond_4

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/settings/widget/ChartNetworkSeriesView;->aOn:Landroid/net/NetworkStatsHistory;

    invoke-virtual {v7, v11, v6}, Landroid/net/NetworkStatsHistory;->getValues(ILandroid/net/NetworkStatsHistory$Entry;)Landroid/net/NetworkStatsHistory$Entry;

    move-result-object v12

    iget-wide v0, v12, Landroid/net/NetworkStatsHistory$Entry;->bucketStart:J

    move-wide/from16 v16, v0

    iget-wide v6, v12, Landroid/net/NetworkStatsHistory$Entry;->bucketDuration:J

    add-long v6, v6, v16

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/android/settings/widget/ChartNetworkSeriesView;->aNZ:Lcom/android/settings/widget/k;

    move-wide/from16 v0, v16

    invoke-interface {v10, v0, v1}, Lcom/android/settings/widget/k;->aAs(J)F

    move-result v15

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/android/settings/widget/ChartNetworkSeriesView;->aNZ:Lcom/android/settings/widget/k;

    invoke-interface {v10, v6, v7}, Lcom/android/settings/widget/k;->aAs(J)F

    move-result v10

    const/16 v18, 0x0

    cmpg-float v18, v10, v18

    if-gez v18, :cond_2

    move v6, v8

    move v7, v9

    :goto_1
    add-int/lit8 v8, v11, 0x1

    move v11, v8

    move v9, v7

    move v8, v6

    move-object v6, v12

    goto :goto_0

    :cond_2
    iget-wide v0, v12, Landroid/net/NetworkStatsHistory$Entry;->rxBytes:J

    move-wide/from16 v18, v0

    iget-wide v0, v12, Landroid/net/NetworkStatsHistory$Entry;->txBytes:J

    move-wide/from16 v20, v0

    add-long v18, v18, v20

    add-long v2, v2, v18

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/android/settings/widget/ChartNetworkSeriesView;->aOo:Lcom/android/settings/widget/k;

    invoke-interface {v9, v2, v3}, Lcom/android/settings/widget/k;->aAs(J)F

    move-result v9

    cmp-long v4, v4, v16

    if-eqz v4, :cond_3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/settings/widget/ChartNetworkSeriesView;->aOi:Landroid/graphics/Path;

    invoke-virtual {v4, v15, v8}, Landroid/graphics/Path;->lineTo(FF)V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/settings/widget/ChartNetworkSeriesView;->aOh:Landroid/graphics/Path;

    invoke-virtual {v4, v15, v8}, Landroid/graphics/Path;->lineTo(FF)V

    :cond_3
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/settings/widget/ChartNetworkSeriesView;->aOi:Landroid/graphics/Path;

    invoke-virtual {v4, v10, v9}, Landroid/graphics/Path;->lineTo(FF)V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/settings/widget/ChartNetworkSeriesView;->aOh:Landroid/graphics/Path;

    invoke-virtual {v4, v10, v9}, Landroid/graphics/Path;->lineTo(FF)V

    move-wide v4, v6

    move v6, v9

    move v7, v10

    goto :goto_1

    :cond_4
    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/android/settings/widget/ChartNetworkSeriesView;->aNX:J

    cmp-long v4, v4, v6

    if-gez v4, :cond_5

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/settings/widget/ChartNetworkSeriesView;->aNZ:Lcom/android/settings/widget/k;

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/android/settings/widget/ChartNetworkSeriesView;->aNX:J

    invoke-interface {v4, v6, v7}, Lcom/android/settings/widget/k;->aAs(J)F

    move-result v9

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/settings/widget/ChartNetworkSeriesView;->aOi:Landroid/graphics/Path;

    invoke-virtual {v4, v9, v8}, Landroid/graphics/Path;->lineTo(FF)V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/settings/widget/ChartNetworkSeriesView;->aOh:Landroid/graphics/Path;

    invoke-virtual {v4, v9, v8}, Landroid/graphics/Path;->lineTo(FF)V

    :cond_5
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/settings/widget/ChartNetworkSeriesView;->aOh:Landroid/graphics/Path;

    int-to-float v5, v13

    invoke-virtual {v4, v9, v5}, Landroid/graphics/Path;->lineTo(FF)V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/settings/widget/ChartNetworkSeriesView;->aOh:Landroid/graphics/Path;

    const/4 v5, 0x0

    int-to-float v6, v13

    invoke-virtual {v4, v5, v6}, Landroid/graphics/Path;->lineTo(FF)V

    move-object/from16 v0, p0

    iput-wide v2, v0, Lcom/android/settings/widget/ChartNetworkSeriesView;->aOa:J

    invoke-virtual/range {p0 .. p0}, Lcom/android/settings/widget/ChartNetworkSeriesView;->invalidate()V

    return-void
.end method


# virtual methods
.method azL(Lcom/android/settings/widget/k;Lcom/android/settings/widget/k;)V
    .locals 1

    const-string/jumbo v0, "missing horiz"

    invoke-static {p1, v0}, Lcom/android/internal/util/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settings/widget/k;

    iput-object v0, p0, Lcom/android/settings/widget/ChartNetworkSeriesView;->aNZ:Lcom/android/settings/widget/k;

    const-string/jumbo v0, "missing vert"

    invoke-static {p2, v0}, Lcom/android/internal/util/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settings/widget/k;

    iput-object v0, p0, Lcom/android/settings/widget/ChartNetworkSeriesView;->aOo:Lcom/android/settings/widget/k;

    return-void
.end method

.method public azM()V
    .locals 2

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/settings/widget/ChartNetworkSeriesView;->aOj:Z

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/android/settings/widget/ChartNetworkSeriesView;->aOa:J

    invoke-virtual {p0}, Lcom/android/settings/widget/ChartNetworkSeriesView;->invalidate()V

    return-void
.end method

.method public getMaxEstimate()J
    .locals 2

    iget-wide v0, p0, Lcom/android/settings/widget/ChartNetworkSeriesView;->aOb:J

    return-wide v0
.end method

.method public getMaxVisible()J
    .locals 7

    const/4 v6, 0x0

    iget-boolean v0, p0, Lcom/android/settings/widget/ChartNetworkSeriesView;->aNY:Z

    if-eqz v0, :cond_0

    iget-wide v0, p0, Lcom/android/settings/widget/ChartNetworkSeriesView;->aOb:J

    :goto_0
    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-gtz v2, :cond_1

    iget-object v2, p0, Lcom/android/settings/widget/ChartNetworkSeriesView;->aOn:Landroid/net/NetworkStatsHistory;

    if-eqz v2, :cond_1

    iget-object v1, p0, Lcom/android/settings/widget/ChartNetworkSeriesView;->aOn:Landroid/net/NetworkStatsHistory;

    iget-wide v2, p0, Lcom/android/settings/widget/ChartNetworkSeriesView;->aOm:J

    iget-wide v4, p0, Lcom/android/settings/widget/ChartNetworkSeriesView;->aNW:J

    invoke-virtual/range {v1 .. v6}, Landroid/net/NetworkStatsHistory;->getValues(JJLandroid/net/NetworkStatsHistory$Entry;)Landroid/net/NetworkStatsHistory$Entry;

    move-result-object v0

    iget-wide v2, v0, Landroid/net/NetworkStatsHistory$Entry;->rxBytes:J

    iget-wide v0, v0, Landroid/net/NetworkStatsHistory$Entry;->txBytes:J

    add-long/2addr v0, v2

    return-wide v0

    :cond_0
    iget-wide v0, p0, Lcom/android/settings/widget/ChartNetworkSeriesView;->aOa:J

    goto :goto_0

    :cond_1
    return-wide v0
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 7

    const/4 v6, 0x0

    iget-boolean v0, p0, Lcom/android/settings/widget/ChartNetworkSeriesView;->aOj:Z

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/android/settings/widget/ChartNetworkSeriesView;->azK()V

    :cond_0
    iget-boolean v0, p0, Lcom/android/settings/widget/ChartNetworkSeriesView;->aNY:Z

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    move-result v0

    invoke-virtual {p0}, Lcom/android/settings/widget/ChartNetworkSeriesView;->getWidth()I

    move-result v1

    invoke-virtual {p0}, Lcom/android/settings/widget/ChartNetworkSeriesView;->getHeight()I

    move-result v2

    invoke-virtual {p1, v6, v6, v1, v2}, Landroid/graphics/Canvas;->clipRect(IIII)Z

    iget-object v1, p0, Lcom/android/settings/widget/ChartNetworkSeriesView;->aOg:Landroid/graphics/Path;

    iget-object v2, p0, Lcom/android/settings/widget/ChartNetworkSeriesView;->aOc:Landroid/graphics/Paint;

    invoke-virtual {p1, v1, v2}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->restoreToCount(I)V

    :cond_1
    iget-boolean v0, p0, Lcom/android/settings/widget/ChartNetworkSeriesView;->aOl:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/settings/widget/ChartNetworkSeriesView;->aOe:Landroid/graphics/Paint;

    :goto_0
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    move-result v1

    iget v2, p0, Lcom/android/settings/widget/ChartNetworkSeriesView;->aOk:I

    invoke-virtual {p0}, Lcom/android/settings/widget/ChartNetworkSeriesView;->getWidth()I

    move-result v3

    invoke-virtual {p0}, Lcom/android/settings/widget/ChartNetworkSeriesView;->getHeight()I

    move-result v4

    iget v5, p0, Lcom/android/settings/widget/ChartNetworkSeriesView;->aOk:I

    sub-int/2addr v4, v5

    invoke-virtual {p1, v2, v6, v3, v4}, Landroid/graphics/Canvas;->clipRect(IIII)Z

    iget-object v2, p0, Lcom/android/settings/widget/ChartNetworkSeriesView;->aOh:Landroid/graphics/Path;

    invoke-virtual {p1, v2, v0}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    invoke-virtual {p1, v1}, Landroid/graphics/Canvas;->restoreToCount(I)V

    return-void

    :cond_2
    iget-object v0, p0, Lcom/android/settings/widget/ChartNetworkSeriesView;->aOd:Landroid/graphics/Paint;

    goto :goto_0
.end method

.method public setBounds(JJ)V
    .locals 1

    iput-wide p1, p0, Lcom/android/settings/widget/ChartNetworkSeriesView;->aOm:J

    iput-wide p3, p0, Lcom/android/settings/widget/ChartNetworkSeriesView;->aNW:J

    return-void
.end method

.method public setChartColor(III)V
    .locals 4

    const/4 v3, 0x1

    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/android/settings/widget/ChartNetworkSeriesView;->aOf:Landroid/graphics/Paint;

    iget-object v0, p0, Lcom/android/settings/widget/ChartNetworkSeriesView;->aOf:Landroid/graphics/Paint;

    invoke-virtual {p0}, Lcom/android/settings/widget/ChartNetworkSeriesView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v1, v1, Landroid/util/DisplayMetrics;->density:F

    const/high16 v2, 0x40800000    # 4.0f

    mul-float/2addr v1, v2

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    iget-object v0, p0, Lcom/android/settings/widget/ChartNetworkSeriesView;->aOf:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setColor(I)V

    iget-object v0, p0, Lcom/android/settings/widget/ChartNetworkSeriesView;->aOf:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    iget-object v0, p0, Lcom/android/settings/widget/ChartNetworkSeriesView;->aOf:Landroid/graphics/Paint;

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/android/settings/widget/ChartNetworkSeriesView;->aOd:Landroid/graphics/Paint;

    iget-object v0, p0, Lcom/android/settings/widget/ChartNetworkSeriesView;->aOd:Landroid/graphics/Paint;

    invoke-virtual {v0, p2}, Landroid/graphics/Paint;->setColor(I)V

    iget-object v0, p0, Lcom/android/settings/widget/ChartNetworkSeriesView;->aOd:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    iget-object v0, p0, Lcom/android/settings/widget/ChartNetworkSeriesView;->aOd:Landroid/graphics/Paint;

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/android/settings/widget/ChartNetworkSeriesView;->aOe:Landroid/graphics/Paint;

    iget-object v0, p0, Lcom/android/settings/widget/ChartNetworkSeriesView;->aOe:Landroid/graphics/Paint;

    invoke-virtual {v0, p3}, Landroid/graphics/Paint;->setColor(I)V

    iget-object v0, p0, Lcom/android/settings/widget/ChartNetworkSeriesView;->aOe:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    iget-object v0, p0, Lcom/android/settings/widget/ChartNetworkSeriesView;->aOe:Landroid/graphics/Paint;

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/android/settings/widget/ChartNetworkSeriesView;->aOc:Landroid/graphics/Paint;

    iget-object v0, p0, Lcom/android/settings/widget/ChartNetworkSeriesView;->aOc:Landroid/graphics/Paint;

    const/high16 v1, 0x40400000    # 3.0f

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    iget-object v0, p0, Lcom/android/settings/widget/ChartNetworkSeriesView;->aOc:Landroid/graphics/Paint;

    invoke-virtual {v0, p3}, Landroid/graphics/Paint;->setColor(I)V

    iget-object v0, p0, Lcom/android/settings/widget/ChartNetworkSeriesView;->aOc:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    iget-object v0, p0, Lcom/android/settings/widget/ChartNetworkSeriesView;->aOc:Landroid/graphics/Paint;

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    iget-object v0, p0, Lcom/android/settings/widget/ChartNetworkSeriesView;->aOc:Landroid/graphics/Paint;

    new-instance v1, Landroid/graphics/DashPathEffect;

    const/4 v2, 0x2

    new-array v2, v2, [F

    fill-array-data v2, :array_0

    const/high16 v3, 0x3f800000    # 1.0f

    invoke-direct {v1, v2, v3}, Landroid/graphics/DashPathEffect;-><init>([FF)V

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setPathEffect(Landroid/graphics/PathEffect;)Landroid/graphics/PathEffect;

    return-void

    nop

    :array_0
    .array-data 4
        0x41200000    # 10.0f
        0x41200000    # 10.0f
    .end array-data
.end method

.method public setEndTime(J)V
    .locals 1

    iput-wide p1, p0, Lcom/android/settings/widget/ChartNetworkSeriesView;->aNX:J

    return-void
.end method

.method public setEstimateVisible(Z)V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/settings/widget/ChartNetworkSeriesView;->aNY:Z

    invoke-virtual {p0}, Lcom/android/settings/widget/ChartNetworkSeriesView;->invalidate()V

    return-void
.end method

.method public setSafeRegion(I)V
    .locals 0

    iput p1, p0, Lcom/android/settings/widget/ChartNetworkSeriesView;->aOk:I

    return-void
.end method

.method public setSecondary(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/android/settings/widget/ChartNetworkSeriesView;->aOl:Z

    return-void
.end method
