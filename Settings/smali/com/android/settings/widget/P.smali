.class final Lcom/android/settings/widget/P;
.super Ljava/lang/Object;
.source "DotsPageIndicator.java"

# interfaces
.implements Landroid/animation/ValueAnimator$AnimatorUpdateListener;


# instance fields
.field final synthetic aRE:Lcom/android/settings/widget/c;


# direct methods
.method constructor <init>(Lcom/android/settings/widget/c;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/widget/P;->aRE:Lcom/android/settings/widget/c;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationUpdate(Landroid/animation/ValueAnimator;)V
    .locals 5

    iget-object v0, p0, Lcom/android/settings/widget/P;->aRE:Lcom/android/settings/widget/c;

    iget-object v1, v0, Lcom/android/settings/widget/c;->aNP:Lcom/android/settings/widget/DotsPageIndicator;

    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->getAnimatedValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    invoke-static {v1, v0}, Lcom/android/settings/widget/DotsPageIndicator;->azx(Lcom/android/settings/widget/DotsPageIndicator;F)F

    iget-object v0, p0, Lcom/android/settings/widget/P;->aRE:Lcom/android/settings/widget/c;

    iget-object v0, v0, Lcom/android/settings/widget/c;->aNP:Lcom/android/settings/widget/DotsPageIndicator;

    invoke-virtual {v0}, Lcom/android/settings/widget/DotsPageIndicator;->postInvalidateOnAnimation()V

    iget-object v0, p0, Lcom/android/settings/widget/P;->aRE:Lcom/android/settings/widget/c;

    iget-object v0, v0, Lcom/android/settings/widget/c;->aNP:Lcom/android/settings/widget/DotsPageIndicator;

    invoke-static {v0}, Lcom/android/settings/widget/DotsPageIndicator;->azs(Lcom/android/settings/widget/DotsPageIndicator;)[Lcom/android/settings/widget/d;

    move-result-object v1

    const/4 v0, 0x0

    array-length v2, v1

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    iget-object v4, p0, Lcom/android/settings/widget/P;->aRE:Lcom/android/settings/widget/c;

    iget-object v4, v4, Lcom/android/settings/widget/c;->aNP:Lcom/android/settings/widget/DotsPageIndicator;

    invoke-static {v4}, Lcom/android/settings/widget/DotsPageIndicator;->azr(Lcom/android/settings/widget/DotsPageIndicator;)F

    move-result v4

    invoke-virtual {v3, v4}, Lcom/android/settings/widget/d;->azG(F)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method
