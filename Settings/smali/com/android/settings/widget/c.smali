.class public Lcom/android/settings/widget/c;
.super Lcom/android/settings/widget/b;
.source "DotsPageIndicator.java"


# instance fields
.field final synthetic aNP:Lcom/android/settings/widget/DotsPageIndicator;


# direct methods
.method public constructor <init>(Lcom/android/settings/widget/DotsPageIndicator;IIILcom/android/settings/widget/e;)V
    .locals 10

    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v1, 0x0

    iput-object p1, p0, Lcom/android/settings/widget/c;->aNP:Lcom/android/settings/widget/DotsPageIndicator;

    invoke-direct {p0, p1, p5}, Lcom/android/settings/widget/b;-><init>(Lcom/android/settings/widget/DotsPageIndicator;Lcom/android/settings/widget/e;)V

    invoke-static {p1}, Lcom/android/settings/widget/DotsPageIndicator;->azl(Lcom/android/settings/widget/DotsPageIndicator;)J

    move-result-wide v2

    invoke-virtual {p0, v2, v3}, Lcom/android/settings/widget/c;->setDuration(J)Landroid/animation/ValueAnimator;

    invoke-static {p1}, Lcom/android/settings/widget/DotsPageIndicator;->azo(Lcom/android/settings/widget/DotsPageIndicator;)Landroid/view/animation/Interpolator;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/settings/widget/c;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    if-le p3, p2, :cond_0

    invoke-static {p1}, Lcom/android/settings/widget/DotsPageIndicator;->azm(Lcom/android/settings/widget/DotsPageIndicator;)[F

    move-result-object v0

    aget v0, v0, p2

    invoke-static {p1}, Lcom/android/settings/widget/DotsPageIndicator;->azt(Lcom/android/settings/widget/DotsPageIndicator;)F

    move-result v2

    invoke-static {v0, v2}, Ljava/lang/Math;->min(FF)F

    move-result v0

    invoke-static {p1}, Lcom/android/settings/widget/DotsPageIndicator;->azn(Lcom/android/settings/widget/DotsPageIndicator;)F

    move-result v2

    sub-float/2addr v0, v2

    move v4, v0

    :goto_0
    if-le p3, p2, :cond_1

    invoke-static {p1}, Lcom/android/settings/widget/DotsPageIndicator;->azm(Lcom/android/settings/widget/DotsPageIndicator;)[F

    move-result-object v0

    aget v0, v0, p3

    invoke-static {p1}, Lcom/android/settings/widget/DotsPageIndicator;->azn(Lcom/android/settings/widget/DotsPageIndicator;)F

    move-result v2

    sub-float/2addr v0, v2

    move v3, v0

    :goto_1
    if-le p3, p2, :cond_2

    invoke-static {p1}, Lcom/android/settings/widget/DotsPageIndicator;->azm(Lcom/android/settings/widget/DotsPageIndicator;)[F

    move-result-object v0

    aget v0, v0, p3

    invoke-static {p1}, Lcom/android/settings/widget/DotsPageIndicator;->azn(Lcom/android/settings/widget/DotsPageIndicator;)F

    move-result v2

    add-float/2addr v0, v2

    move v2, v0

    :goto_2
    if-le p3, p2, :cond_3

    invoke-static {p1}, Lcom/android/settings/widget/DotsPageIndicator;->azm(Lcom/android/settings/widget/DotsPageIndicator;)[F

    move-result-object v0

    aget v0, v0, p3

    invoke-static {p1}, Lcom/android/settings/widget/DotsPageIndicator;->azn(Lcom/android/settings/widget/DotsPageIndicator;)F

    move-result v5

    add-float/2addr v0, v5

    :goto_3
    new-array v5, p4, [Lcom/android/settings/widget/d;

    invoke-static {p1, v5}, Lcom/android/settings/widget/DotsPageIndicator;->azy(Lcom/android/settings/widget/DotsPageIndicator;[Lcom/android/settings/widget/d;)[Lcom/android/settings/widget/d;

    new-array v5, p4, [I

    cmpl-float v6, v4, v3

    if-eqz v6, :cond_5

    new-array v0, v8, [F

    aput v4, v0, v1

    aput v3, v0, v7

    invoke-virtual {p0, v0}, Lcom/android/settings/widget/c;->setFloatValues([F)V

    :goto_4
    if-ge v1, p4, :cond_4

    invoke-static {p1}, Lcom/android/settings/widget/DotsPageIndicator;->azs(Lcom/android/settings/widget/DotsPageIndicator;)[Lcom/android/settings/widget/d;

    move-result-object v0

    new-instance v3, Lcom/android/settings/widget/d;

    add-int v6, p2, v1

    new-instance v7, Lcom/android/settings/widget/f;

    invoke-static {p1}, Lcom/android/settings/widget/DotsPageIndicator;->azm(Lcom/android/settings/widget/DotsPageIndicator;)[F

    move-result-object v8

    add-int v9, p2, v1

    aget v8, v8, v9

    invoke-direct {v7, p1, v8}, Lcom/android/settings/widget/f;-><init>(Lcom/android/settings/widget/DotsPageIndicator;F)V

    invoke-direct {v3, p1, v6, v7}, Lcom/android/settings/widget/d;-><init>(Lcom/android/settings/widget/DotsPageIndicator;ILcom/android/settings/widget/e;)V

    aput-object v3, v0, v1

    add-int v0, p2, v1

    aput v0, v5, v1

    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    :cond_0
    invoke-static {p1}, Lcom/android/settings/widget/DotsPageIndicator;->azm(Lcom/android/settings/widget/DotsPageIndicator;)[F

    move-result-object v0

    aget v0, v0, p3

    invoke-static {p1}, Lcom/android/settings/widget/DotsPageIndicator;->azn(Lcom/android/settings/widget/DotsPageIndicator;)F

    move-result v2

    sub-float/2addr v0, v2

    move v4, v0

    goto :goto_0

    :cond_1
    invoke-static {p1}, Lcom/android/settings/widget/DotsPageIndicator;->azm(Lcom/android/settings/widget/DotsPageIndicator;)[F

    move-result-object v0

    aget v0, v0, p3

    invoke-static {p1}, Lcom/android/settings/widget/DotsPageIndicator;->azn(Lcom/android/settings/widget/DotsPageIndicator;)F

    move-result v2

    sub-float/2addr v0, v2

    move v3, v0

    goto :goto_1

    :cond_2
    invoke-static {p1}, Lcom/android/settings/widget/DotsPageIndicator;->azm(Lcom/android/settings/widget/DotsPageIndicator;)[F

    move-result-object v0

    aget v0, v0, p2

    invoke-static {p1}, Lcom/android/settings/widget/DotsPageIndicator;->azt(Lcom/android/settings/widget/DotsPageIndicator;)F

    move-result v2

    invoke-static {v0, v2}, Ljava/lang/Math;->max(FF)F

    move-result v0

    invoke-static {p1}, Lcom/android/settings/widget/DotsPageIndicator;->azn(Lcom/android/settings/widget/DotsPageIndicator;)F

    move-result v2

    add-float/2addr v0, v2

    move v2, v0

    goto :goto_2

    :cond_3
    invoke-static {p1}, Lcom/android/settings/widget/DotsPageIndicator;->azm(Lcom/android/settings/widget/DotsPageIndicator;)[F

    move-result-object v0

    aget v0, v0, p3

    invoke-static {p1}, Lcom/android/settings/widget/DotsPageIndicator;->azn(Lcom/android/settings/widget/DotsPageIndicator;)F

    move-result v5

    add-float/2addr v0, v5

    goto :goto_3

    :cond_4
    new-instance v0, Lcom/android/settings/widget/O;

    invoke-direct {v0, p0}, Lcom/android/settings/widget/O;-><init>(Lcom/android/settings/widget/c;)V

    invoke-virtual {p0, v0}, Lcom/android/settings/widget/c;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    :goto_5
    new-instance v0, Lcom/android/settings/widget/Q;

    invoke-direct {v0, p0, v5, v4, v2}, Lcom/android/settings/widget/Q;-><init>(Lcom/android/settings/widget/c;[IFF)V

    invoke-virtual {p0, v0}, Lcom/android/settings/widget/c;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    return-void

    :cond_5
    new-array v3, v8, [F

    aput v2, v3, v1

    aput v0, v3, v7

    invoke-virtual {p0, v3}, Lcom/android/settings/widget/c;->setFloatValues([F)V

    move v0, v1

    :goto_6
    if-ge v0, p4, :cond_6

    invoke-static {p1}, Lcom/android/settings/widget/DotsPageIndicator;->azs(Lcom/android/settings/widget/DotsPageIndicator;)[Lcom/android/settings/widget/d;

    move-result-object v1

    new-instance v3, Lcom/android/settings/widget/d;

    sub-int v6, p2, v0

    new-instance v7, Lcom/android/settings/widget/g;

    invoke-static {p1}, Lcom/android/settings/widget/DotsPageIndicator;->azm(Lcom/android/settings/widget/DotsPageIndicator;)[F

    move-result-object v8

    sub-int v9, p2, v0

    aget v8, v8, v9

    invoke-direct {v7, p1, v8}, Lcom/android/settings/widget/g;-><init>(Lcom/android/settings/widget/DotsPageIndicator;F)V

    invoke-direct {v3, p1, v6, v7}, Lcom/android/settings/widget/d;-><init>(Lcom/android/settings/widget/DotsPageIndicator;ILcom/android/settings/widget/e;)V

    aput-object v3, v1, v0

    sub-int v1, p2, v0

    aput v1, v5, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    :cond_6
    new-instance v0, Lcom/android/settings/widget/P;

    invoke-direct {v0, p0}, Lcom/android/settings/widget/P;-><init>(Lcom/android/settings/widget/c;)V

    invoke-virtual {p0, v0}, Lcom/android/settings/widget/c;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    goto :goto_5
.end method
