.class public Lcom/android/settings/widget/q;
.super Ljava/lang/Object;
.source "FooterPreferenceMixin.java"

# interfaces
.implements Lcom/android/settings/core/lifecycle/b;
.implements Lcom/android/settings/core/lifecycle/a/f;


# instance fields
.field private aPv:Lcom/android/settings/widget/FooterPreference;

.field private final aPw:Landroid/support/v14/preference/b;


# direct methods
.method public constructor <init>(Landroid/support/v14/preference/b;Lcom/android/settings/core/lifecycle/a;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/android/settings/widget/q;->aPw:Landroid/support/v14/preference/b;

    invoke-virtual {p2, p0}, Lcom/android/settings/core/lifecycle/a;->ajt(Lcom/android/settings/core/lifecycle/b;)Lcom/android/settings/core/lifecycle/b;

    return-void
.end method

.method private aBp()Landroid/content/Context;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/widget/q;->aPw:Landroid/support/v14/preference/b;

    invoke-virtual {v0}, Landroid/support/v14/preference/b;->dju()Landroid/support/v7/preference/k;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v7/preference/k;->getContext()Landroid/content/Context;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public aBn()Z
    .locals 1

    iget-object v0, p0, Lcom/android/settings/widget/q;->aPv:Lcom/android/settings/widget/FooterPreference;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public aBo()Lcom/android/settings/widget/FooterPreference;
    .locals 3

    iget-object v0, p0, Lcom/android/settings/widget/q;->aPw:Landroid/support/v14/preference/b;

    invoke-virtual {v0}, Landroid/support/v14/preference/b;->djr()Landroid/support/v7/preference/PreferenceScreen;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/widget/q;->aPv:Lcom/android/settings/widget/FooterPreference;

    if-eqz v1, :cond_0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/android/settings/widget/q;->aPv:Lcom/android/settings/widget/FooterPreference;

    invoke-virtual {v0, v1}, Landroid/support/v7/preference/PreferenceScreen;->dll(Landroid/support/v7/preference/Preference;)Z

    :cond_0
    new-instance v1, Lcom/android/settings/widget/FooterPreference;

    invoke-direct {p0}, Lcom/android/settings/widget/q;->aBp()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/android/settings/widget/FooterPreference;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/android/settings/widget/q;->aPv:Lcom/android/settings/widget/FooterPreference;

    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/android/settings/widget/q;->aPv:Lcom/android/settings/widget/FooterPreference;

    invoke-virtual {v0, v1}, Landroid/support/v7/preference/PreferenceScreen;->im(Landroid/support/v7/preference/Preference;)Z

    :cond_1
    iget-object v0, p0, Lcom/android/settings/widget/q;->aPv:Lcom/android/settings/widget/FooterPreference;

    return-object v0
.end method

.method public ajr(Landroid/support/v7/preference/PreferenceScreen;)V
    .locals 1

    iget-object v0, p0, Lcom/android/settings/widget/q;->aPv:Lcom/android/settings/widget/FooterPreference;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/widget/q;->aPv:Lcom/android/settings/widget/FooterPreference;

    invoke-virtual {p1, v0}, Landroid/support/v7/preference/PreferenceScreen;->im(Landroid/support/v7/preference/Preference;)Z

    :cond_0
    return-void
.end method
