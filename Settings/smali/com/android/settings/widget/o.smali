.class public Lcom/android/settings/widget/o;
.super Ljava/lang/Object;
.source "ChartDataUsageView.java"

# interfaces
.implements Lcom/android/settings/widget/k;


# static fields
.field private static final aPm:I


# instance fields
.field private aPn:J

.field private aPo:J

.field private aPp:F


# direct methods
.method static constructor <clinit>()V
    .locals 1

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Calendar;->getFirstDayOfWeek()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    sput v0, Lcom/android/settings/widget/o;->aPm:I

    return-void
.end method

.method public constructor <init>()V
    .locals 4

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    const-wide v2, 0x9a7ec800L

    sub-long v2, v0, v2

    invoke-virtual {p0, v2, v3, v0, v1}, Lcom/android/settings/widget/o;->setBounds(JJ)Z

    return-void
.end method


# virtual methods
.method public aAq(F)Z
    .locals 1

    iget v0, p0, Lcom/android/settings/widget/o;->aPp:F

    cmpl-float v0, v0, p1

    if-eqz v0, :cond_0

    iput p1, p0, Lcom/android/settings/widget/o;->aPp:F

    const/4 v0, 0x1

    return v0

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public aAr(F)J
    .locals 6

    iget-wide v0, p0, Lcom/android/settings/widget/o;->aPo:J

    long-to-float v0, v0

    iget-wide v2, p0, Lcom/android/settings/widget/o;->aPn:J

    iget-wide v4, p0, Lcom/android/settings/widget/o;->aPo:J

    sub-long/2addr v2, v4

    long-to-float v1, v2

    mul-float/2addr v1, p1

    iget v2, p0, Lcom/android/settings/widget/o;->aPp:F

    div-float/2addr v1, v2

    add-float/2addr v0, v1

    float-to-long v0, v0

    return-wide v0
.end method

.method public aAs(J)F
    .locals 7

    iget v0, p0, Lcom/android/settings/widget/o;->aPp:F

    iget-wide v2, p0, Lcom/android/settings/widget/o;->aPo:J

    sub-long v2, p1, v2

    long-to-float v1, v2

    mul-float/2addr v0, v1

    iget-wide v2, p0, Lcom/android/settings/widget/o;->aPn:J

    iget-wide v4, p0, Lcom/android/settings/widget/o;->aPo:J

    sub-long/2addr v2, v4

    long-to-float v1, v2

    div-float/2addr v0, v1

    return v0
.end method

.method public aAt(Landroid/content/res/Resources;Landroid/text/SpannableStringBuilder;J)J
    .locals 3

    invoke-virtual {p2}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v0

    invoke-static {p3, p4}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {p2, v2, v0, v1}, Landroid/text/SpannableStringBuilder;->replace(IILjava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    return-wide p3
.end method

.method public aAu()[F
    .locals 9

    const/4 v8, 0x1

    const/4 v2, 0x0

    const/16 v0, 0x20

    new-array v4, v0, [F

    new-instance v5, Landroid/text/format/Time;

    invoke-direct {v5}, Landroid/text/format/Time;-><init>()V

    iget-wide v0, p0, Lcom/android/settings/widget/o;->aPn:J

    invoke-virtual {v5, v0, v1}, Landroid/text/format/Time;->set(J)V

    iget v0, v5, Landroid/text/format/Time;->monthDay:I

    iget v1, v5, Landroid/text/format/Time;->weekDay:I

    sget v3, Lcom/android/settings/widget/o;->aPm:I

    sub-int/2addr v1, v3

    sub-int/2addr v0, v1

    iput v0, v5, Landroid/text/format/Time;->monthDay:I

    iput v2, v5, Landroid/text/format/Time;->second:I

    iput v2, v5, Landroid/text/format/Time;->minute:I

    iput v2, v5, Landroid/text/format/Time;->hour:I

    invoke-virtual {v5, v8}, Landroid/text/format/Time;->normalize(Z)J

    invoke-virtual {v5, v8}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v0

    :goto_0
    iget-wide v6, p0, Lcom/android/settings/widget/o;->aPo:J

    cmp-long v3, v0, v6

    if-lez v3, :cond_1

    iget-wide v6, p0, Lcom/android/settings/widget/o;->aPn:J

    cmp-long v3, v0, v6

    if-gtz v3, :cond_0

    add-int/lit8 v3, v2, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/android/settings/widget/o;->aAs(J)F

    move-result v0

    aput v0, v4, v2

    move v2, v3

    :cond_0
    iget v0, v5, Landroid/text/format/Time;->monthDay:I

    add-int/lit8 v0, v0, -0x7

    iput v0, v5, Landroid/text/format/Time;->monthDay:I

    invoke-virtual {v5, v8}, Landroid/text/format/Time;->normalize(Z)J

    invoke-virtual {v5, v8}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v0

    goto :goto_0

    :cond_1
    invoke-static {v4, v2}, Ljava/util/Arrays;->copyOf([FI)[F

    move-result-object v0

    return-object v0
.end method

.method public aAv(J)I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public hashCode()I
    .locals 4

    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/Object;

    iget-wide v2, p0, Lcom/android/settings/widget/o;->aPo:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    const/4 v2, 0x0

    aput-object v1, v0, v2

    iget-wide v2, p0, Lcom/android/settings/widget/o;->aPn:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    const/4 v2, 0x1

    aput-object v1, v0, v2

    iget v1, p0, Lcom/android/settings/widget/o;->aPp:F

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    const/4 v2, 0x2

    aput-object v1, v0, v2

    invoke-static {v0}, Ljava/util/Objects;->hash([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public setBounds(JJ)Z
    .locals 3

    iget-wide v0, p0, Lcom/android/settings/widget/o;->aPo:J

    cmp-long v0, v0, p1

    if-nez v0, :cond_0

    iget-wide v0, p0, Lcom/android/settings/widget/o;->aPn:J

    cmp-long v0, v0, p3

    if-eqz v0, :cond_1

    :cond_0
    iput-wide p1, p0, Lcom/android/settings/widget/o;->aPo:J

    iput-wide p3, p0, Lcom/android/settings/widget/o;->aPn:J

    const/4 v0, 0x1

    return v0

    :cond_1
    const/4 v0, 0x0

    return v0
.end method
