.class final Lcom/android/settings/widget/I;
.super Lcom/android/settings/widget/F;
.source "SettingsAppWidgetProvider.java"


# instance fields
.field private aRz:I


# direct methods
.method private constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/android/settings/widget/F;-><init>(Lcom/android/settings/widget/F;)V

    const/4 v0, 0x0

    iput v0, p0, Lcom/android/settings/widget/I;->aRz:I

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/settings/widget/I;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/widget/I;-><init>()V

    return-void
.end method


# virtual methods
.method public aCG(Landroid/content/Context;)I
    .locals 3

    const/4 v0, 0x0

    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string/jumbo v2, "location_mode"

    invoke-static {v1, v2, v0}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lcom/android/settings/widget/I;->aRz:I

    iget v1, p0, Lcom/android/settings/widget/I;->aRz:I

    if-nez v1, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public aCH()I
    .locals 1

    const v0, 0x7f1207b1

    return v0
.end method

.method public aCI()I
    .locals 1

    const v0, 0x7f0a01ff

    return v0
.end method

.method public aCJ(Z)I
    .locals 1

    if-eqz p1, :cond_0

    iget v0, p0, Lcom/android/settings/widget/I;->aRz:I

    packed-switch v0, :pswitch_data_0

    :pswitch_0
    const v0, 0x7f080181

    return v0

    :pswitch_1
    const v0, 0x7f080180

    return v0

    :cond_0
    const v0, 0x7f08017f

    return v0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public aCL()I
    .locals 1

    const v0, 0x7f0a0205

    return v0
.end method

.method public aCO(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 1

    invoke-virtual {p0, p1}, Lcom/android/settings/widget/I;->aCG(Landroid/content/Context;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/android/settings/widget/I;->aCQ(Landroid/content/Context;I)V

    return-void
.end method

.method public aCP(Landroid/content/Context;Z)V
    .locals 2

    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    new-instance v1, Lcom/android/settings/widget/am;

    invoke-direct {v1, p0, p1, v0}, Lcom/android/settings/widget/am;-><init>(Lcom/android/settings/widget/I;Landroid/content/Context;Landroid/content/ContentResolver;)V

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Void;

    invoke-virtual {v1, v0}, Lcom/android/settings/widget/am;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    return-void
.end method

.method public getContainerId()I
    .locals 1

    const v0, 0x7f0a00a9

    return v0
.end method
