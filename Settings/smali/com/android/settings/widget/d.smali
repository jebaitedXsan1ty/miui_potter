.class public Lcom/android/settings/widget/d;
.super Lcom/android/settings/widget/b;
.source "DotsPageIndicator.java"


# instance fields
.field private final aNQ:I

.field final synthetic aNR:Lcom/android/settings/widget/DotsPageIndicator;


# direct methods
.method public constructor <init>(Lcom/android/settings/widget/DotsPageIndicator;ILcom/android/settings/widget/e;)V
    .locals 2

    iput-object p1, p0, Lcom/android/settings/widget/d;->aNR:Lcom/android/settings/widget/DotsPageIndicator;

    invoke-direct {p0, p1, p3}, Lcom/android/settings/widget/b;-><init>(Lcom/android/settings/widget/DotsPageIndicator;Lcom/android/settings/widget/e;)V

    iput p2, p0, Lcom/android/settings/widget/d;->aNQ:I

    const/4 v0, 0x2

    new-array v0, v0, [F

    fill-array-data v0, :array_0

    invoke-virtual {p0, v0}, Lcom/android/settings/widget/d;->setFloatValues([F)V

    invoke-static {p1}, Lcom/android/settings/widget/DotsPageIndicator;->azl(Lcom/android/settings/widget/DotsPageIndicator;)J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/android/settings/widget/d;->setDuration(J)Landroid/animation/ValueAnimator;

    invoke-static {p1}, Lcom/android/settings/widget/DotsPageIndicator;->azo(Lcom/android/settings/widget/DotsPageIndicator;)Landroid/view/animation/Interpolator;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/settings/widget/d;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    new-instance v0, Lcom/android/settings/widget/R;

    invoke-direct {v0, p0}, Lcom/android/settings/widget/R;-><init>(Lcom/android/settings/widget/d;)V

    invoke-virtual {p0, v0}, Lcom/android/settings/widget/d;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    new-instance v0, Lcom/android/settings/widget/S;

    invoke-direct {v0, p0}, Lcom/android/settings/widget/S;-><init>(Lcom/android/settings/widget/d;)V

    invoke-virtual {p0, v0}, Lcom/android/settings/widget/d;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    return-void

    nop

    :array_0
    .array-data 4
        0x3727c5ac    # 1.0E-5f
        0x3f800000    # 1.0f
    .end array-data
.end method

.method static synthetic azH(Lcom/android/settings/widget/d;)I
    .locals 1

    iget v0, p0, Lcom/android/settings/widget/d;->aNQ:I

    return v0
.end method
