.class public Lcom/android/settings/widget/TogglePreference;
.super Landroid/preference/CheckBoxPreference;
.source "TogglePreference.java"


# instance fields
.field private aRB:Lcom/android/settings/widget/M;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    invoke-direct {p0, p1}, Landroid/preference/CheckBoxPreference;-><init>(Landroid/content/Context;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Landroid/preference/CheckBoxPreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Landroid/preference/CheckBoxPreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V
    .locals 0

    invoke-direct {p0, p1, p2, p3, p4}, Landroid/preference/CheckBoxPreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V

    return-void
.end method


# virtual methods
.method public aCY(Lcom/android/settings/widget/M;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/widget/TogglePreference;->aRB:Lcom/android/settings/widget/M;

    return-void
.end method

.method public setChecked(Z)V
    .locals 1

    iget-object v0, p0, Lcom/android/settings/widget/TogglePreference;->aRB:Lcom/android/settings/widget/M;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/widget/TogglePreference;->aRB:Lcom/android/settings/widget/M;

    invoke-interface {v0, p0, p1}, Lcom/android/settings/widget/M;->VR(Lcom/android/settings/widget/TogglePreference;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    :cond_0
    invoke-super {p0, p1}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    return-void
.end method

.method public setCheckedInternal(Z)V
    .locals 0

    invoke-super {p0, p1}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    return-void
.end method
