.class public Lcom/android/settings/widget/A;
.super Lcom/android/settings/widget/i;
.source "MasterSwitchController.java"

# interfaces
.implements Landroid/support/v7/preference/f;


# instance fields
.field private final aQm:Lcom/android/settings/widget/MasterSwitchPreference;


# direct methods
.method public constructor <init>(Lcom/android/settings/widget/MasterSwitchPreference;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/widget/i;-><init>()V

    iput-object p1, p0, Lcom/android/settings/widget/A;->aQm:Lcom/android/settings/widget/MasterSwitchPreference;

    return-void
.end method


# virtual methods
.method public aAk()V
    .locals 1

    iget-object v0, p0, Lcom/android/settings/widget/A;->aQm:Lcom/android/settings/widget/MasterSwitchPreference;

    invoke-virtual {v0, p0}, Lcom/android/settings/widget/MasterSwitchPreference;->dkJ(Landroid/support/v7/preference/f;)V

    return-void
.end method

.method public aAm()V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/widget/A;->aQm:Lcom/android/settings/widget/MasterSwitchPreference;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/android/settings/widget/MasterSwitchPreference;->dkJ(Landroid/support/v7/preference/f;)V

    return-void
.end method

.method public aAo(Z)V
    .locals 0

    return-void
.end method

.method public eH(Landroid/support/v7/preference/Preference;Ljava/lang/Object;)Z
    .locals 2

    iget-object v0, p0, Lcom/android/settings/widget/A;->aOA:Lcom/android/settings/widget/j;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/widget/A;->aOA:Lcom/android/settings/widget/j;

    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-interface {v0, v1}, Lcom/android/settings/widget/j;->acB(Z)Z

    move-result v0

    return v0

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public getSwitch()Landroid/widget/Switch;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/widget/A;->aQm:Lcom/android/settings/widget/MasterSwitchPreference;

    invoke-virtual {v0}, Lcom/android/settings/widget/MasterSwitchPreference;->getSwitch()Landroid/widget/Switch;

    move-result-object v0

    return-object v0
.end method

.method public isChecked()Z
    .locals 1

    iget-object v0, p0, Lcom/android/settings/widget/A;->aQm:Lcom/android/settings/widget/MasterSwitchPreference;

    invoke-virtual {v0}, Lcom/android/settings/widget/MasterSwitchPreference;->isChecked()Z

    move-result v0

    return v0
.end method

.method public setChecked(Z)V
    .locals 1

    iget-object v0, p0, Lcom/android/settings/widget/A;->aQm:Lcom/android/settings/widget/MasterSwitchPreference;

    invoke-virtual {v0, p1}, Lcom/android/settings/widget/MasterSwitchPreference;->setChecked(Z)V

    return-void
.end method

.method public setDisabledByAdmin(Lcom/android/settingslib/n;)V
    .locals 1

    iget-object v0, p0, Lcom/android/settings/widget/A;->aQm:Lcom/android/settings/widget/MasterSwitchPreference;

    invoke-virtual {v0, p1}, Lcom/android/settings/widget/MasterSwitchPreference;->setDisabledByAdmin(Lcom/android/settingslib/n;)V

    return-void
.end method

.method public setEnabled(Z)V
    .locals 1

    iget-object v0, p0, Lcom/android/settings/widget/A;->aQm:Lcom/android/settings/widget/MasterSwitchPreference;

    invoke-virtual {v0, p1}, Lcom/android/settings/widget/MasterSwitchPreference;->aAx(Z)V

    return-void
.end method
