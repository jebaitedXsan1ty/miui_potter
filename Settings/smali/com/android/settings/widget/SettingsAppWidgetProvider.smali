.class public Lcom/android/settings/widget/SettingsAppWidgetProvider;
.super Landroid/appwidget/AppWidgetProvider;
.source "SettingsAppWidgetProvider.java"


# static fields
.field private static final aRl:[I

.field private static final aRm:[I

.field private static final aRn:[I

.field static final aRo:Landroid/content/ComponentName;

.field private static final aRp:Lcom/android/settings/widget/F;

.field private static aRq:Lcom/android/settingslib/bluetooth/d;

.field private static final aRr:Lcom/android/settings/widget/F;

.field private static aRs:Lcom/android/settings/widget/K;

.field private static final aRt:Lcom/android/settings/widget/F;

.field private static final aRu:Lcom/android/settings/widget/F;


# direct methods
.method static synthetic -get0()[I
    .locals 1

    sget-object v0, Lcom/android/settings/widget/SettingsAppWidgetProvider;->aRl:[I

    return-object v0
.end method

.method static constructor <clinit>()V
    .locals 4

    const/4 v3, 0x0

    new-instance v0, Landroid/content/ComponentName;

    const-string/jumbo v1, "com.android.settings"

    const-string/jumbo v2, "com.android.settings.widget.SettingsAppWidgetProvider"

    invoke-direct {v0, v1, v2}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/android/settings/widget/SettingsAppWidgetProvider;->aRo:Landroid/content/ComponentName;

    sput-object v3, Lcom/android/settings/widget/SettingsAppWidgetProvider;->aRq:Lcom/android/settingslib/bluetooth/d;

    const v0, 0x7f08009a

    const v1, 0x7f080099

    const v2, 0x7f08009b

    filled-new-array {v0, v1, v2}, [I

    move-result-object v0

    sput-object v0, Lcom/android/settings/widget/SettingsAppWidgetProvider;->aRm:[I

    const v0, 0x7f080097

    const v1, 0x7f080096

    const v2, 0x7f080098

    filled-new-array {v0, v1, v2}, [I

    move-result-object v0

    sput-object v0, Lcom/android/settings/widget/SettingsAppWidgetProvider;->aRl:[I

    const v0, 0x7f08009d

    const v1, 0x7f08009c

    const v2, 0x7f08009e

    filled-new-array {v0, v1, v2}, [I

    move-result-object v0

    sput-object v0, Lcom/android/settings/widget/SettingsAppWidgetProvider;->aRn:[I

    new-instance v0, Lcom/android/settings/widget/G;

    invoke-direct {v0, v3}, Lcom/android/settings/widget/G;-><init>(Lcom/android/settings/widget/G;)V

    sput-object v0, Lcom/android/settings/widget/SettingsAppWidgetProvider;->aRu:Lcom/android/settings/widget/F;

    new-instance v0, Lcom/android/settings/widget/H;

    invoke-direct {v0, v3}, Lcom/android/settings/widget/H;-><init>(Lcom/android/settings/widget/H;)V

    sput-object v0, Lcom/android/settings/widget/SettingsAppWidgetProvider;->aRp:Lcom/android/settings/widget/F;

    new-instance v0, Lcom/android/settings/widget/I;

    invoke-direct {v0, v3}, Lcom/android/settings/widget/I;-><init>(Lcom/android/settings/widget/I;)V

    sput-object v0, Lcom/android/settings/widget/SettingsAppWidgetProvider;->aRr:Lcom/android/settings/widget/F;

    new-instance v0, Lcom/android/settings/widget/J;

    invoke-direct {v0, v3}, Lcom/android/settings/widget/J;-><init>(Lcom/android/settings/widget/J;)V

    sput-object v0, Lcom/android/settings/widget/SettingsAppWidgetProvider;->aRt:Lcom/android/settings/widget/F;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/appwidget/AppWidgetProvider;-><init>()V

    return-void
.end method

.method private static aCA(Landroid/widget/RemoteViews;Landroid/content/Context;)V
    .locals 9

    const v8, 0x7f1207b0

    const v7, 0x7f0a01fe

    const v6, 0x7f0a00a7

    const/4 v5, 0x1

    const/4 v4, 0x0

    sget-object v0, Lcom/android/settings/widget/SettingsAppWidgetProvider;->aRu:Lcom/android/settings/widget/F;

    invoke-virtual {v0, p1, p0}, Lcom/android/settings/widget/F;->aCR(Landroid/content/Context;Landroid/widget/RemoteViews;)V

    sget-object v0, Lcom/android/settings/widget/SettingsAppWidgetProvider;->aRp:Lcom/android/settings/widget/F;

    invoke-virtual {v0, p1, p0}, Lcom/android/settings/widget/F;->aCR(Landroid/content/Context;Landroid/widget/RemoteViews;)V

    sget-object v0, Lcom/android/settings/widget/SettingsAppWidgetProvider;->aRr:Lcom/android/settings/widget/F;

    invoke-virtual {v0, p1, p0}, Lcom/android/settings/widget/F;->aCR(Landroid/content/Context;Landroid/widget/RemoteViews;)V

    sget-object v0, Lcom/android/settings/widget/SettingsAppWidgetProvider;->aRt:Lcom/android/settings/widget/F;

    invoke-virtual {v0, p1, p0}, Lcom/android/settings/widget/F;->aCR(Landroid/content/Context;Landroid/widget/RemoteViews;)V

    invoke-static {p1}, Lcom/android/settings/widget/SettingsAppWidgetProvider;->aCx(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-array v0, v5, [Ljava/lang/Object;

    const v1, 0x7f1207ac

    invoke-virtual {p1, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v4

    invoke-virtual {p1, v8, v0}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v6, v0}, Landroid/widget/RemoteViews;->setContentDescription(ILjava/lang/CharSequence;)V

    const v0, 0x7f080179

    invoke-virtual {p0, v7, v0}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    const v0, 0x7f0a0204

    const v1, 0x7f08009e

    invoke-virtual {p0, v0, v1}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    :goto_0
    return-void

    :cond_0
    invoke-static {p1}, Lcom/android/settings/widget/SettingsAppWidgetProvider;->aCw(Landroid/content/Context;)I

    move-result v1

    const-string/jumbo v0, "power"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    invoke-virtual {v0}, Landroid/os/PowerManager;->getMaximumScreenBrightnessSetting()I

    move-result v2

    int-to-float v2, v2

    const v3, 0x3f4ccccd    # 0.8f

    mul-float/2addr v2, v3

    float-to-int v2, v2

    invoke-virtual {v0}, Landroid/os/PowerManager;->getMaximumScreenBrightnessSetting()I

    move-result v0

    int-to-float v0, v0

    const v3, 0x3e99999a    # 0.3f

    mul-float/2addr v0, v3

    float-to-int v0, v0

    if-le v1, v2, :cond_1

    new-array v2, v5, [Ljava/lang/Object;

    const v3, 0x7f1207ad

    invoke-virtual {p1, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-virtual {p1, v8, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v6, v2}, Landroid/widget/RemoteViews;->setContentDescription(ILjava/lang/CharSequence;)V

    const v2, 0x7f08017a

    invoke-virtual {p0, v7, v2}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    :goto_1
    if-le v1, v0, :cond_3

    const v0, 0x7f0a0204

    const v1, 0x7f08009e

    invoke-virtual {p0, v0, v1}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    goto :goto_0

    :cond_1
    if-le v1, v0, :cond_2

    new-array v2, v5, [Ljava/lang/Object;

    const v3, 0x7f1207ae

    invoke-virtual {p1, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-virtual {p1, v8, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v6, v2}, Landroid/widget/RemoteViews;->setContentDescription(ILjava/lang/CharSequence;)V

    const v2, 0x7f08017b

    invoke-virtual {p0, v7, v2}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    goto :goto_1

    :cond_2
    new-array v2, v5, [Ljava/lang/Object;

    const v3, 0x7f1207af

    invoke-virtual {p1, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-virtual {p1, v8, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v6, v2}, Landroid/widget/RemoteViews;->setContentDescription(ILjava/lang/CharSequence;)V

    const v2, 0x7f08017c

    invoke-virtual {p0, v7, v2}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    goto :goto_1

    :cond_3
    const v0, 0x7f0a0204

    const v1, 0x7f08009b

    invoke-virtual {p0, v0, v1}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    goto/16 :goto_0
.end method

.method public static aCB(Landroid/content/Context;)V
    .locals 3

    invoke-static {p0}, Lcom/android/settings/widget/SettingsAppWidgetProvider;->aCu(Landroid/content/Context;)Landroid/widget/RemoteViews;

    move-result-object v0

    invoke-static {p0}, Landroid/appwidget/AppWidgetManager;->getInstance(Landroid/content/Context;)Landroid/appwidget/AppWidgetManager;

    move-result-object v1

    sget-object v2, Lcom/android/settings/widget/SettingsAppWidgetProvider;->aRo:Landroid/content/ComponentName;

    invoke-virtual {v1, v2, v0}, Landroid/appwidget/AppWidgetManager;->updateAppWidget(Landroid/content/ComponentName;Landroid/widget/RemoteViews;)V

    invoke-static {p0}, Lcom/android/settings/widget/SettingsAppWidgetProvider;->aCv(Landroid/content/Context;)V

    return-void
.end method

.method static synthetic aCC()[I
    .locals 1

    sget-object v0, Lcom/android/settings/widget/SettingsAppWidgetProvider;->aRm:[I

    return-object v0
.end method

.method static synthetic aCD()[I
    .locals 1

    sget-object v0, Lcom/android/settings/widget/SettingsAppWidgetProvider;->aRn:[I

    return-object v0
.end method

.method static synthetic aCE()Lcom/android/settingslib/bluetooth/d;
    .locals 1

    sget-object v0, Lcom/android/settings/widget/SettingsAppWidgetProvider;->aRq:Lcom/android/settingslib/bluetooth/d;

    return-object v0
.end method

.method static synthetic aCF(Lcom/android/settingslib/bluetooth/d;)Lcom/android/settingslib/bluetooth/d;
    .locals 0

    sput-object p0, Lcom/android/settings/widget/SettingsAppWidgetProvider;->aRq:Lcom/android/settingslib/bluetooth/d;

    return-object p0
.end method

.method static aCu(Landroid/content/Context;)Landroid/widget/RemoteViews;
    .locals 3

    new-instance v0, Landroid/widget/RemoteViews;

    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    const v2, 0x7f0d026a

    invoke-direct {v0, v1, v2}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    const/4 v1, 0x0

    invoke-static {p0, v1}, Lcom/android/settings/widget/SettingsAppWidgetProvider;->aCy(Landroid/content/Context;I)Landroid/app/PendingIntent;

    move-result-object v1

    const v2, 0x7f0a00ae

    invoke-virtual {v0, v2, v1}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    const/4 v1, 0x1

    invoke-static {p0, v1}, Lcom/android/settings/widget/SettingsAppWidgetProvider;->aCy(Landroid/content/Context;I)Landroid/app/PendingIntent;

    move-result-object v1

    const v2, 0x7f0a00a7

    invoke-virtual {v0, v2, v1}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    const/4 v1, 0x2

    invoke-static {p0, v1}, Lcom/android/settings/widget/SettingsAppWidgetProvider;->aCy(Landroid/content/Context;I)Landroid/app/PendingIntent;

    move-result-object v1

    const v2, 0x7f0a00ad

    invoke-virtual {v0, v2, v1}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    const/4 v1, 0x3

    invoke-static {p0, v1}, Lcom/android/settings/widget/SettingsAppWidgetProvider;->aCy(Landroid/content/Context;I)Landroid/app/PendingIntent;

    move-result-object v1

    const v2, 0x7f0a00a9

    invoke-virtual {v0, v2, v1}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    const/4 v1, 0x4

    invoke-static {p0, v1}, Lcom/android/settings/widget/SettingsAppWidgetProvider;->aCy(Landroid/content/Context;I)Landroid/app/PendingIntent;

    move-result-object v1

    const v2, 0x7f0a00a6

    invoke-virtual {v0, v2, v1}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    invoke-static {v0, p0}, Lcom/android/settings/widget/SettingsAppWidgetProvider;->aCA(Landroid/widget/RemoteViews;Landroid/content/Context;)V

    return-object v0
.end method

.method private static aCv(Landroid/content/Context;)V
    .locals 3

    sget-object v0, Lcom/android/settings/widget/SettingsAppWidgetProvider;->aRs:Lcom/android/settings/widget/K;

    if-nez v0, :cond_0

    new-instance v0, Lcom/android/settings/widget/K;

    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/android/settings/widget/K;-><init>(Landroid/os/Handler;Landroid/content/Context;)V

    sput-object v0, Lcom/android/settings/widget/SettingsAppWidgetProvider;->aRs:Lcom/android/settings/widget/K;

    sget-object v0, Lcom/android/settings/widget/SettingsAppWidgetProvider;->aRs:Lcom/android/settings/widget/K;

    invoke-virtual {v0}, Lcom/android/settings/widget/K;->aCV()V

    :cond_0
    return-void
.end method

.method private static aCw(Landroid/content/Context;)I
    .locals 2

    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v1, "screen_brightness"

    invoke-static {v0, v1}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    return v0

    :catch_0
    move-exception v0

    const/4 v0, 0x0

    return v0
.end method

.method private static aCx(Landroid/content/Context;)Z
    .locals 5

    const/4 v0, 0x1

    const/4 v1, 0x0

    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string/jumbo v3, "screen_brightness_mode"

    invoke-static {v2, v3}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    if-ne v2, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0

    :catch_0
    move-exception v0

    const-string/jumbo v2, "SettingsAppWidgetProvider"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "getBrightnessMode: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return v1
.end method

.method private static aCy(Landroid/content/Context;I)Landroid/app/PendingIntent;
    .locals 4

    const/4 v3, 0x0

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-class v1, Lcom/android/settings/widget/SettingsAppWidgetProvider;

    invoke-virtual {v0, p0, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    const-string/jumbo v1, "android.intent.category.ALTERNATIVE"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "custom:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    invoke-static {p0, v3, v0, v3}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    return-object v0
.end method

.method private aCz(Landroid/content/Context;)V
    .locals 9

    const/4 v3, 0x1

    const/4 v1, 0x0

    :try_start_0
    const-string/jumbo v0, "power"

    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Landroid/os/IPowerManager$Stub;->asInterface(Landroid/os/IBinder;)Landroid/os/IPowerManager;

    move-result-object v4

    if-eqz v4, :cond_0

    const-string/jumbo v0, "power"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    const-string/jumbo v2, "screen_brightness"

    invoke-static {v5, v2}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;)I

    move-result v6

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v7, 0x112001e

    invoke-virtual {v2, v7}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v2

    if-eqz v2, :cond_5

    const-string/jumbo v2, "screen_brightness_mode"

    invoke-static {v5, v2}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;)I

    move-result v2

    :goto_0
    if-ne v2, v3, :cond_1

    invoke-virtual {v0}, Landroid/os/PowerManager;->getMinimumScreenBrightnessSetting()I

    move-result v0

    move v2, v0

    move v0, v1

    :goto_1
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v6, 0x112001e

    invoke-virtual {v3, v6}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string/jumbo v3, "screen_brightness_mode"

    invoke-static {v1, v3, v0}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    :goto_2
    if-nez v0, :cond_0

    invoke-interface {v4, v2}, Landroid/os/IPowerManager;->setTemporaryScreenBrightnessSettingOverride(I)V

    const-string/jumbo v0, "screen_brightness"

    invoke-static {v5, v0, v2}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    :cond_0
    :goto_3
    return-void

    :cond_1
    invoke-virtual {v0}, Landroid/os/PowerManager;->getDefaultScreenBrightnessSetting()I

    move-result v7

    if-ge v6, v7, :cond_2

    invoke-virtual {v0}, Landroid/os/PowerManager;->getDefaultScreenBrightnessSetting()I

    move-result v0

    move v8, v2

    move v2, v0

    move v0, v8

    goto :goto_1

    :cond_2
    invoke-virtual {v0}, Landroid/os/PowerManager;->getMaximumScreenBrightnessSetting()I

    move-result v7

    if-ge v6, v7, :cond_3

    invoke-virtual {v0}, Landroid/os/PowerManager;->getMaximumScreenBrightnessSetting()I

    move-result v0

    move v8, v2

    move v2, v0

    move v0, v8

    goto :goto_1

    :cond_3
    invoke-virtual {v0}, Landroid/os/PowerManager;->getMinimumScreenBrightnessSetting()I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Landroid/provider/Settings$SettingNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    move v2, v0

    move v0, v3

    goto :goto_1

    :cond_4
    move v0, v1

    goto :goto_2

    :catch_0
    move-exception v0

    const-string/jumbo v1, "SettingsAppWidgetProvider"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "toggleBrightness: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_3

    :catch_1
    move-exception v0

    const-string/jumbo v1, "SettingsAppWidgetProvider"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "toggleBrightness: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_3

    :cond_5
    move v2, v1

    goto/16 :goto_0
.end method


# virtual methods
.method public onDisabled(Landroid/content/Context;)V
    .locals 2

    const/4 v1, 0x0

    sget-object v0, Lcom/android/settings/widget/SettingsAppWidgetProvider;->aRs:Lcom/android/settings/widget/K;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/android/settings/widget/SettingsAppWidgetProvider;->aRs:Lcom/android/settings/widget/K;

    invoke-virtual {v0}, Lcom/android/settings/widget/K;->aCW()V

    sput-object v1, Lcom/android/settings/widget/SettingsAppWidgetProvider;->aRs:Lcom/android/settings/widget/K;

    :cond_0
    return-void
.end method

.method public onEnabled(Landroid/content/Context;)V
    .locals 0

    invoke-static {p1}, Lcom/android/settings/widget/SettingsAppWidgetProvider;->aCv(Landroid/content/Context;)V

    return-void
.end method

.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 2

    invoke-super {p0, p1, p2}, Landroid/appwidget/AppWidgetProvider;->onReceive(Landroid/content/Context;Landroid/content/Intent;)V

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "android.net.wifi.WIFI_STATE_CHANGED"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    sget-object v0, Lcom/android/settings/widget/SettingsAppWidgetProvider;->aRu:Lcom/android/settings/widget/F;

    invoke-virtual {v0, p1, p2}, Lcom/android/settings/widget/F;->aCO(Landroid/content/Context;Landroid/content/Intent;)V

    :cond_0
    :goto_0
    invoke-static {p1}, Lcom/android/settings/widget/SettingsAppWidgetProvider;->aCB(Landroid/content/Context;)V

    return-void

    :cond_1
    const-string/jumbo v1, "android.bluetooth.adapter.action.STATE_CHANGED"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    sget-object v0, Lcom/android/settings/widget/SettingsAppWidgetProvider;->aRp:Lcom/android/settings/widget/F;

    invoke-virtual {v0, p1, p2}, Lcom/android/settings/widget/F;->aCO(Landroid/content/Context;Landroid/content/Intent;)V

    goto :goto_0

    :cond_2
    const-string/jumbo v1, "android.location.MODE_CHANGED"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    sget-object v0, Lcom/android/settings/widget/SettingsAppWidgetProvider;->aRr:Lcom/android/settings/widget/F;

    invoke-virtual {v0, p1, p2}, Lcom/android/settings/widget/F;->aCO(Landroid/content/Context;Landroid/content/Intent;)V

    goto :goto_0

    :cond_3
    sget-object v1, Landroid/content/ContentResolver;->ACTION_SYNC_CONN_STATUS_CHANGED:Landroid/content/Intent;

    invoke-virtual {v1, v0}, Landroid/content/Intent;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    sget-object v0, Lcom/android/settings/widget/SettingsAppWidgetProvider;->aRt:Lcom/android/settings/widget/F;

    invoke-virtual {v0, p1, p2}, Lcom/android/settings/widget/F;->aCO(Landroid/content/Context;Landroid/content/Intent;)V

    goto :goto_0

    :cond_4
    const-string/jumbo v0, "android.intent.category.ALTERNATIVE"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->hasCategory(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_9

    invoke-virtual {p2}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->getSchemeSpecificPart()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_5

    sget-object v0, Lcom/android/settings/widget/SettingsAppWidgetProvider;->aRu:Lcom/android/settings/widget/F;

    invoke-virtual {v0, p1}, Lcom/android/settings/widget/F;->aCS(Landroid/content/Context;)V

    goto :goto_0

    :cond_5
    const/4 v1, 0x1

    if-ne v0, v1, :cond_6

    invoke-direct {p0, p1}, Lcom/android/settings/widget/SettingsAppWidgetProvider;->aCz(Landroid/content/Context;)V

    goto :goto_0

    :cond_6
    const/4 v1, 0x2

    if-ne v0, v1, :cond_7

    sget-object v0, Lcom/android/settings/widget/SettingsAppWidgetProvider;->aRt:Lcom/android/settings/widget/F;

    invoke-virtual {v0, p1}, Lcom/android/settings/widget/F;->aCS(Landroid/content/Context;)V

    goto :goto_0

    :cond_7
    const/4 v1, 0x3

    if-ne v0, v1, :cond_8

    sget-object v0, Lcom/android/settings/widget/SettingsAppWidgetProvider;->aRr:Lcom/android/settings/widget/F;

    invoke-virtual {v0, p1}, Lcom/android/settings/widget/F;->aCS(Landroid/content/Context;)V

    goto :goto_0

    :cond_8
    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    sget-object v0, Lcom/android/settings/widget/SettingsAppWidgetProvider;->aRp:Lcom/android/settings/widget/F;

    invoke-virtual {v0, p1}, Lcom/android/settings/widget/F;->aCS(Landroid/content/Context;)V

    goto :goto_0

    :cond_9
    return-void
.end method

.method public onUpdate(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;[I)V
    .locals 3

    invoke-static {p1}, Lcom/android/settings/widget/SettingsAppWidgetProvider;->aCu(Landroid/content/Context;)Landroid/widget/RemoteViews;

    move-result-object v1

    const/4 v0, 0x0

    :goto_0
    array-length v2, p3

    if-ge v0, v2, :cond_0

    aget v2, p3, v0

    invoke-virtual {p2, v2, v1}, Landroid/appwidget/AppWidgetManager;->updateAppWidget(ILandroid/widget/RemoteViews;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method
