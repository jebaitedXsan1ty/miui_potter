.class public abstract Lcom/android/settings/widget/b;
.super Landroid/animation/ValueAnimator;
.source "DotsPageIndicator.java"


# instance fields
.field protected aNM:Z

.field protected aNN:Lcom/android/settings/widget/e;

.field final synthetic aNO:Lcom/android/settings/widget/DotsPageIndicator;


# direct methods
.method public constructor <init>(Lcom/android/settings/widget/DotsPageIndicator;Lcom/android/settings/widget/e;)V
    .locals 1

    iput-object p1, p0, Lcom/android/settings/widget/b;->aNO:Lcom/android/settings/widget/DotsPageIndicator;

    invoke-direct {p0}, Landroid/animation/ValueAnimator;-><init>()V

    iput-object p2, p0, Lcom/android/settings/widget/b;->aNN:Lcom/android/settings/widget/e;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/settings/widget/b;->aNM:Z

    return-void
.end method


# virtual methods
.method public azG(F)V
    .locals 1

    iget-boolean v0, p0, Lcom/android/settings/widget/b;->aNM:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/widget/b;->aNN:Lcom/android/settings/widget/e;

    invoke-virtual {v0, p1}, Lcom/android/settings/widget/e;->azI(F)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/android/settings/widget/b;->start()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/settings/widget/b;->aNM:Z

    :cond_0
    return-void
.end method
