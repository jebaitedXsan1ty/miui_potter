.class final Lcom/android/settings/widget/ak;
.super Landroid/os/AsyncTask;
.source "SettingsAppWidgetProvider.java"


# instance fields
.field final synthetic aSh:Lcom/android/settings/widget/G;

.field final synthetic aSi:Landroid/net/wifi/WifiManager;

.field final synthetic aSj:Z

.field final synthetic val$context:Landroid/content/Context;


# direct methods
.method constructor <init>(Lcom/android/settings/widget/G;Landroid/net/wifi/WifiManager;ZLandroid/content/Context;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/widget/ak;->aSh:Lcom/android/settings/widget/G;

    iput-object p2, p0, Lcom/android/settings/widget/ak;->aSi:Landroid/net/wifi/WifiManager;

    iput-boolean p3, p0, Lcom/android/settings/widget/ak;->aSj:Z

    iput-object p4, p0, Lcom/android/settings/widget/ak;->val$context:Landroid/content/Context;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/android/settings/widget/ak;->doInBackground([Ljava/lang/Void;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Void;
    .locals 2

    iget-object v0, p0, Lcom/android/settings/widget/ak;->aSi:Landroid/net/wifi/WifiManager;

    invoke-virtual {v0}, Landroid/net/wifi/WifiManager;->getWifiApState()I

    move-result v0

    iget-boolean v1, p0, Lcom/android/settings/widget/ak;->aSj:Z

    if-eqz v1, :cond_1

    const/16 v1, 0xc

    if-eq v0, v1, :cond_0

    const/16 v1, 0xd

    if-ne v0, v1, :cond_1

    :cond_0
    invoke-static {}, Lcom/android/settings/dc;->getInstance()Lcom/android/settings/dc;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/widget/ak;->val$context:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/android/settings/dc;->bsf(Landroid/content/Context;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/settings/widget/ak;->val$context:Landroid/content/Context;

    const-string/jumbo v1, "connectivity"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/net/ConnectivityManager;->stopTethering(I)V

    :cond_1
    iget-object v0, p0, Lcom/android/settings/widget/ak;->aSi:Landroid/net/wifi/WifiManager;

    iget-boolean v1, p0, Lcom/android/settings/widget/ak;->aSj:Z

    invoke-virtual {v0, v1}, Landroid/net/wifi/WifiManager;->setWifiEnabled(Z)Z

    const/4 v0, 0x0

    return-object v0
.end method
