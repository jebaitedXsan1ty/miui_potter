.class public abstract Lcom/android/settings/widget/i;
.super Ljava/lang/Object;
.source "SwitchWidgetController.java"


# instance fields
.field protected aOA:Lcom/android/settings/widget/j;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public aAj(Lcom/android/settings/widget/j;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/widget/i;->aOA:Lcom/android/settings/widget/j;

    return-void
.end method

.method public abstract aAk()V
.end method

.method public aAl()V
    .locals 0

    return-void
.end method

.method public abstract aAm()V
.end method

.method public aAn()V
    .locals 0

    return-void
.end method

.method public abstract aAo(Z)V
.end method

.method public abstract getSwitch()Landroid/widget/Switch;
.end method

.method public abstract isChecked()Z
.end method

.method public abstract setChecked(Z)V
.end method

.method public abstract setDisabledByAdmin(Lcom/android/settingslib/n;)V
.end method

.method public abstract setEnabled(Z)V
.end method
