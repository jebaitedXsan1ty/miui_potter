.class Lcom/android/settings/widget/h;
.super Landroid/support/v4/widget/a;
.source "LabeledSeekBar.java"


# instance fields
.field private aOt:Z

.field final synthetic aOu:Lcom/android/settings/widget/LabeledSeekBar;


# direct methods
.method public constructor <init>(Lcom/android/settings/widget/LabeledSeekBar;Lcom/android/settings/widget/LabeledSeekBar;)V
    .locals 2

    const/4 v0, 0x1

    iput-object p1, p0, Lcom/android/settings/widget/h;->aOu:Lcom/android/settings/widget/LabeledSeekBar;

    invoke-direct {p0, p2}, Landroid/support/v4/widget/a;-><init>(Landroid/view/View;)V

    invoke-virtual {p2}, Lcom/android/settings/widget/LabeledSeekBar;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Configuration;->getLayoutDirection()I

    move-result v1

    if-ne v1, v0, :cond_0

    :goto_0
    iput-boolean v0, p0, Lcom/android/settings/widget/h;->aOt:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private azR(I)Landroid/graphics/Rect;
    .locals 5

    const/4 v1, 0x0

    iget-boolean v0, p0, Lcom/android/settings/widget/h;->aOt:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/widget/h;->aOu:Lcom/android/settings/widget/LabeledSeekBar;

    invoke-virtual {v0}, Lcom/android/settings/widget/LabeledSeekBar;->getMax()I

    move-result v0

    sub-int p1, v0, p1

    :cond_0
    mul-int/lit8 v0, p1, 0x2

    add-int/lit8 v0, v0, -0x1

    invoke-direct {p0}, Lcom/android/settings/widget/h;->azS()I

    move-result v2

    mul-int/2addr v0, v2

    iget-object v2, p0, Lcom/android/settings/widget/h;->aOu:Lcom/android/settings/widget/LabeledSeekBar;

    invoke-virtual {v2}, Lcom/android/settings/widget/LabeledSeekBar;->getPaddingStart()I

    move-result v2

    add-int/2addr v0, v2

    mul-int/lit8 v2, p1, 0x2

    add-int/lit8 v2, v2, 0x1

    invoke-direct {p0}, Lcom/android/settings/widget/h;->azS()I

    move-result v3

    mul-int/2addr v2, v3

    iget-object v3, p0, Lcom/android/settings/widget/h;->aOu:Lcom/android/settings/widget/LabeledSeekBar;

    invoke-virtual {v3}, Lcom/android/settings/widget/LabeledSeekBar;->getPaddingStart()I

    move-result v3

    add-int/2addr v2, v3

    if-nez p1, :cond_1

    move v0, v1

    :cond_1
    iget-object v3, p0, Lcom/android/settings/widget/h;->aOu:Lcom/android/settings/widget/LabeledSeekBar;

    invoke-virtual {v3}, Lcom/android/settings/widget/LabeledSeekBar;->getMax()I

    move-result v3

    if-ne p1, v3, :cond_2

    iget-object v2, p0, Lcom/android/settings/widget/h;->aOu:Lcom/android/settings/widget/LabeledSeekBar;

    invoke-virtual {v2}, Lcom/android/settings/widget/LabeledSeekBar;->getWidth()I

    move-result v2

    :cond_2
    new-instance v3, Landroid/graphics/Rect;

    invoke-direct {v3}, Landroid/graphics/Rect;-><init>()V

    iget-object v4, p0, Lcom/android/settings/widget/h;->aOu:Lcom/android/settings/widget/LabeledSeekBar;

    invoke-virtual {v4}, Lcom/android/settings/widget/LabeledSeekBar;->getHeight()I

    move-result v4

    invoke-virtual {v3, v0, v1, v2, v4}, Landroid/graphics/Rect;->set(IIII)V

    return-object v3
.end method

.method private azS()I
    .locals 2

    iget-object v0, p0, Lcom/android/settings/widget/h;->aOu:Lcom/android/settings/widget/LabeledSeekBar;

    invoke-virtual {v0}, Lcom/android/settings/widget/LabeledSeekBar;->getWidth()I

    move-result v0

    iget-object v1, p0, Lcom/android/settings/widget/h;->aOu:Lcom/android/settings/widget/LabeledSeekBar;

    invoke-virtual {v1}, Lcom/android/settings/widget/LabeledSeekBar;->getPaddingStart()I

    move-result v1

    sub-int/2addr v0, v1

    iget-object v1, p0, Lcom/android/settings/widget/h;->aOu:Lcom/android/settings/widget/LabeledSeekBar;

    invoke-virtual {v1}, Lcom/android/settings/widget/LabeledSeekBar;->getPaddingEnd()I

    move-result v1

    sub-int/2addr v0, v1

    iget-object v1, p0, Lcom/android/settings/widget/h;->aOu:Lcom/android/settings/widget/LabeledSeekBar;

    invoke-virtual {v1}, Lcom/android/settings/widget/LabeledSeekBar;->getMax()I

    move-result v1

    mul-int/lit8 v1, v1, 0x2

    div-int/2addr v0, v1

    const/4 v1, 0x0

    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    return v0
.end method

.method private azU(F)I
    .locals 2

    float-to-int v0, p1

    iget-object v1, p0, Lcom/android/settings/widget/h;->aOu:Lcom/android/settings/widget/LabeledSeekBar;

    invoke-virtual {v1}, Lcom/android/settings/widget/LabeledSeekBar;->getPaddingStart()I

    move-result v1

    sub-int/2addr v0, v1

    invoke-direct {p0}, Lcom/android/settings/widget/h;->azS()I

    move-result v1

    div-int/2addr v0, v1

    const/4 v1, 0x0

    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    div-int/lit8 v0, v0, 0x2

    iget-boolean v1, p0, Lcom/android/settings/widget/h;->aOt:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/settings/widget/h;->aOu:Lcom/android/settings/widget/LabeledSeekBar;

    invoke-virtual {v1}, Lcom/android/settings/widget/LabeledSeekBar;->getMax()I

    move-result v1

    sub-int v0, v1, v0

    :cond_0
    return v0
.end method


# virtual methods
.method protected aAa(ILandroid/support/v4/view/a/a;)V
    .locals 2

    const/4 v0, 0x1

    const-class v1, Landroid/widget/RadioButton;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v1}, Landroid/support/v4/view/a/a;->dNk(Ljava/lang/CharSequence;)V

    invoke-direct {p0, p1}, Lcom/android/settings/widget/h;->azR(I)Landroid/graphics/Rect;

    move-result-object v1

    invoke-virtual {p2, v1}, Landroid/support/v4/view/a/a;->dNp(Landroid/graphics/Rect;)V

    const/16 v1, 0x10

    invoke-virtual {p2, v1}, Landroid/support/v4/view/a/a;->dNf(I)V

    iget-object v1, p0, Lcom/android/settings/widget/h;->aOu:Lcom/android/settings/widget/LabeledSeekBar;

    invoke-static {v1}, Lcom/android/settings/widget/LabeledSeekBar;->azO(Lcom/android/settings/widget/LabeledSeekBar;)[Ljava/lang/String;

    move-result-object v1

    aget-object v1, v1, p1

    invoke-virtual {p2, v1}, Landroid/support/v4/view/a/a;->setContentDescription(Ljava/lang/CharSequence;)V

    invoke-virtual {p2, v0}, Landroid/support/v4/view/a/a;->dNh(Z)V

    invoke-virtual {p2, v0}, Landroid/support/v4/view/a/a;->setCheckable(Z)V

    iget-object v1, p0, Lcom/android/settings/widget/h;->aOu:Lcom/android/settings/widget/LabeledSeekBar;

    invoke-virtual {v1}, Lcom/android/settings/widget/LabeledSeekBar;->getProgress()I

    move-result v1

    if-ne p1, v1, :cond_0

    :goto_0
    invoke-virtual {p2, v0}, Landroid/support/v4/view/a/a;->setChecked(Z)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected azT(FF)I
    .locals 1

    invoke-direct {p0, p1}, Lcom/android/settings/widget/h;->azU(F)I

    move-result v0

    return v0
.end method

.method protected azV(Ljava/util/List;)V
    .locals 3

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/android/settings/widget/h;->aOu:Lcom/android/settings/widget/LabeledSeekBar;

    invoke-virtual {v1}, Lcom/android/settings/widget/LabeledSeekBar;->getMax()I

    move-result v1

    :goto_0
    if-gt v0, v1, :cond_0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {p1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method protected azW(IILandroid/os/Bundle;)Z
    .locals 3

    const/4 v2, 0x1

    const/4 v1, 0x0

    const/4 v0, -0x1

    if-ne p1, v0, :cond_0

    return v1

    :cond_0
    packed-switch p2, :pswitch_data_0

    return v1

    :pswitch_0
    iget-object v0, p0, Lcom/android/settings/widget/h;->aOu:Lcom/android/settings/widget/LabeledSeekBar;

    invoke-virtual {v0, p1}, Lcom/android/settings/widget/LabeledSeekBar;->setProgress(I)V

    invoke-virtual {p0, p1, v2}, Lcom/android/settings/widget/h;->dTJ(II)Z

    return v2

    nop

    :pswitch_data_0
    .packed-switch 0x10
        :pswitch_0
    .end packed-switch
.end method

.method protected azX(Landroid/view/accessibility/AccessibilityEvent;)V
    .locals 1

    const-class v0, Landroid/widget/RadioGroup;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityEvent;->setClassName(Ljava/lang/CharSequence;)V

    return-void
.end method

.method protected azY(ILandroid/view/accessibility/AccessibilityEvent;)V
    .locals 1

    const-class v0, Landroid/widget/RadioButton;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Landroid/view/accessibility/AccessibilityEvent;->setClassName(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/android/settings/widget/h;->aOu:Lcom/android/settings/widget/LabeledSeekBar;

    invoke-static {v0}, Lcom/android/settings/widget/LabeledSeekBar;->azO(Lcom/android/settings/widget/LabeledSeekBar;)[Ljava/lang/String;

    move-result-object v0

    aget-object v0, v0, p1

    invoke-virtual {p2, v0}, Landroid/view/accessibility/AccessibilityEvent;->setContentDescription(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/android/settings/widget/h;->aOu:Lcom/android/settings/widget/LabeledSeekBar;

    invoke-virtual {v0}, Lcom/android/settings/widget/LabeledSeekBar;->getProgress()I

    move-result v0

    if-ne p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p2, v0}, Landroid/view/accessibility/AccessibilityEvent;->setChecked(Z)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected azZ(Landroid/support/v4/view/a/a;)V
    .locals 1

    const-class v0, Landroid/widget/RadioGroup;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/support/v4/view/a/a;->dNk(Ljava/lang/CharSequence;)V

    return-void
.end method
