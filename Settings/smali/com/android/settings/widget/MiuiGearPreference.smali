.class public Lcom/android/settings/widget/MiuiGearPreference;
.super Lcom/android/settingslib/MiuiRestrictedPreference;
.source "MiuiGearPreference.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private aQl:Lcom/android/settings/widget/z;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/android/settingslib/MiuiRestrictedPreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method


# virtual methods
.method public aBO(Lcom/android/settings/widget/z;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/widget/MiuiGearPreference;->aQl:Lcom/android/settings/widget/z;

    invoke-virtual {p0}, Lcom/android/settings/widget/MiuiGearPreference;->notifyChanged()V

    return-void
.end method

.method protected aBP()Z
    .locals 1

    iget-object v0, p0, Lcom/android/settings/widget/MiuiGearPreference;->aQl:Lcom/android/settings/widget/z;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected ayP()I
    .locals 1

    const v0, 0x7f0d0158

    return v0
.end method

.method public onBindView(Landroid/view/View;)V
    .locals 3

    const/4 v2, 0x0

    invoke-super {p0, p1}, Lcom/android/settingslib/MiuiRestrictedPreference;->onBindView(Landroid/view/View;)V

    const v0, 0x7f0a03df

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    iget-object v1, p0, Lcom/android/settings/widget/MiuiGearPreference;->aQl:Lcom/android/settings/widget/z;

    if-eqz v1, :cond_1

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :goto_0
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/View;->setEnabled(Z)V

    return-void

    :cond_1
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {v0, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const v1, 0x7f0a03df

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/android/settings/widget/MiuiGearPreference;->aQl:Lcom/android/settings/widget/z;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/widget/MiuiGearPreference;->aQl:Lcom/android/settings/widget/z;

    invoke-interface {v0, p0}, Lcom/android/settings/widget/z;->QS(Lcom/android/settings/widget/MiuiGearPreference;)V

    :cond_0
    return-void
.end method
