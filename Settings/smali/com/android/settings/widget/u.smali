.class public Lcom/android/settings/widget/u;
.super Ljava/lang/Object;
.source "MiuiFooterPreferenceMixin.java"

# interfaces
.implements Lcom/android/settings/core/lifecycle/b;
.implements Lcom/android/settings/core/lifecycle/a/i;


# instance fields
.field private aPX:Lcom/android/settings/widget/MiuiFooterPreference;

.field private final aPY:Landroid/preference/PreferenceFragment;


# direct methods
.method public constructor <init>(Landroid/preference/PreferenceFragment;Lcom/android/settings/core/lifecycle/c;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/android/settings/widget/u;->aPY:Landroid/preference/PreferenceFragment;

    invoke-virtual {p2, p0}, Lcom/android/settings/core/lifecycle/c;->ajv(Lcom/android/settings/core/lifecycle/b;)Lcom/android/settings/core/lifecycle/b;

    return-void
.end method

.method private aBG()Landroid/content/Context;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/widget/u;->aPY:Landroid/preference/PreferenceFragment;

    invoke-virtual {v0}, Landroid/preference/PreferenceFragment;->getContext()Landroid/content/Context;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public aBF()Lcom/android/settings/widget/MiuiFooterPreference;
    .locals 3

    iget-object v0, p0, Lcom/android/settings/widget/u;->aPY:Landroid/preference/PreferenceFragment;

    invoke-virtual {v0}, Landroid/preference/PreferenceFragment;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/widget/u;->aPX:Lcom/android/settings/widget/MiuiFooterPreference;

    if-eqz v1, :cond_0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/android/settings/widget/u;->aPX:Lcom/android/settings/widget/MiuiFooterPreference;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    :cond_0
    new-instance v1, Lcom/android/settings/widget/MiuiFooterPreference;

    invoke-direct {p0}, Lcom/android/settings/widget/u;->aBG()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/android/settings/widget/MiuiFooterPreference;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/android/settings/widget/u;->aPX:Lcom/android/settings/widget/MiuiFooterPreference;

    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/android/settings/widget/u;->aPX:Lcom/android/settings/widget/MiuiFooterPreference;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    :cond_1
    iget-object v0, p0, Lcom/android/settings/widget/u;->aPX:Lcom/android/settings/widget/MiuiFooterPreference;

    return-object v0
.end method

.method public ajs(Landroid/preference/PreferenceScreen;)V
    .locals 1

    iget-object v0, p0, Lcom/android/settings/widget/u;->aPX:Lcom/android/settings/widget/MiuiFooterPreference;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/widget/u;->aPX:Lcom/android/settings/widget/MiuiFooterPreference;

    invoke-virtual {p1, v0}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    :cond_0
    return-void
.end method
