.class abstract Lcom/android/settings/widget/F;
.super Ljava/lang/Object;
.source "SettingsAppWidgetProvider.java"


# instance fields
.field private aRv:Ljava/lang/Boolean;

.field private aRw:Z

.field private aRx:Z

.field private aRy:Ljava/lang/Boolean;


# direct methods
.method private constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean v0, p0, Lcom/android/settings/widget/F;->aRx:Z

    iput-object v1, p0, Lcom/android/settings/widget/F;->aRv:Ljava/lang/Boolean;

    iput-object v1, p0, Lcom/android/settings/widget/F;->aRy:Ljava/lang/Boolean;

    iput-boolean v0, p0, Lcom/android/settings/widget/F;->aRw:Z

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/settings/widget/F;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/widget/F;-><init>()V

    return-void
.end method

.method private final aCK(Landroid/content/Context;I)Ljava/lang/String;
    .locals 4

    invoke-virtual {p0}, Lcom/android/settings/widget/F;->aCH()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, p2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object v0, v2, v3

    const/4 v0, 0x1

    aput-object v1, v2, v0

    const v0, 0x7f1207b5

    invoke-virtual {p1, v0, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public abstract aCG(Landroid/content/Context;)I
.end method

.method public abstract aCH()I
.end method

.method public abstract aCI()I
.end method

.method public abstract aCJ(Z)I
.end method

.method public abstract aCL()I
.end method

.method public final aCM(Landroid/content/Context;)I
    .locals 2

    const/4 v1, 0x5

    iget-boolean v0, p0, Lcom/android/settings/widget/F;->aRx:Z

    if-eqz v0, :cond_0

    return v1

    :cond_0
    invoke-virtual {p0, p1}, Lcom/android/settings/widget/F;->aCG(Landroid/content/Context;)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    return v1

    :pswitch_0
    const/4 v0, 0x0

    return v0

    :pswitch_1
    const/4 v0, 0x1

    return v0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final aCN()Z
    .locals 1

    iget-object v0, p0, Lcom/android/settings/widget/F;->aRy:Ljava/lang/Boolean;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/widget/F;->aRy:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public abstract aCO(Landroid/content/Context;Landroid/content/Intent;)V
.end method

.method protected abstract aCP(Landroid/content/Context;Z)V
.end method

.method protected final aCQ(Landroid/content/Context;I)V
    .locals 4

    const/4 v3, 0x1

    const/4 v2, 0x0

    iget-boolean v0, p0, Lcom/android/settings/widget/F;->aRx:Z

    packed-switch p2, :pswitch_data_0

    :goto_0
    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/android/settings/widget/F;->aRx:Z

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/android/settings/widget/F;->aRw:Z

    if-eqz v0, :cond_1

    const-string/jumbo v0, "SettingsAppWidgetProvider"

    const-string/jumbo v1, "processing deferred state change"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/settings/widget/F;->aRv:Ljava/lang/Boolean;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/settings/widget/F;->aRy:Ljava/lang/Boolean;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/settings/widget/F;->aRy:Ljava/lang/Boolean;

    iget-object v1, p0, Lcom/android/settings/widget/F;->aRv:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string/jumbo v0, "SettingsAppWidgetProvider"

    const-string/jumbo v1, "... but intended state matches, so no changes."

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_1
    iput-boolean v2, p0, Lcom/android/settings/widget/F;->aRw:Z

    :cond_1
    return-void

    :pswitch_0
    iput-boolean v2, p0, Lcom/android/settings/widget/F;->aRx:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, p0, Lcom/android/settings/widget/F;->aRv:Ljava/lang/Boolean;

    goto :goto_0

    :pswitch_1
    iput-boolean v2, p0, Lcom/android/settings/widget/F;->aRx:Z

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, p0, Lcom/android/settings/widget/F;->aRv:Ljava/lang/Boolean;

    goto :goto_0

    :pswitch_2
    iput-boolean v3, p0, Lcom/android/settings/widget/F;->aRx:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, p0, Lcom/android/settings/widget/F;->aRv:Ljava/lang/Boolean;

    goto :goto_0

    :pswitch_3
    iput-boolean v3, p0, Lcom/android/settings/widget/F;->aRx:Z

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, p0, Lcom/android/settings/widget/F;->aRv:Ljava/lang/Boolean;

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/android/settings/widget/F;->aRy:Ljava/lang/Boolean;

    if-eqz v0, :cond_0

    iput-boolean v3, p0, Lcom/android/settings/widget/F;->aRx:Z

    iget-object v0, p0, Lcom/android/settings/widget/F;->aRy:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/android/settings/widget/F;->aCP(Landroid/content/Context;Z)V

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public final aCR(Landroid/content/Context;Landroid/widget/RemoteViews;)V
    .locals 7

    const/4 v6, 0x1

    const/4 v5, 0x0

    invoke-virtual {p0}, Lcom/android/settings/widget/F;->getContainerId()I

    move-result v0

    invoke-virtual {p0}, Lcom/android/settings/widget/F;->aCI()I

    move-result v1

    invoke-virtual {p0}, Lcom/android/settings/widget/F;->aCL()I

    move-result v2

    invoke-virtual {p0}, Lcom/android/settings/widget/F;->getPosition()I

    move-result v3

    invoke-virtual {p0, p1}, Lcom/android/settings/widget/F;->aCM(Landroid/content/Context;)I

    move-result v4

    packed-switch v4, :pswitch_data_0

    :goto_0
    :pswitch_0
    return-void

    :pswitch_1
    const v4, 0x7f1207b3

    invoke-direct {p0, p1, v4}, Lcom/android/settings/widget/F;->aCK(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p2, v0, v4}, Landroid/widget/RemoteViews;->setContentDescription(ILjava/lang/CharSequence;)V

    invoke-virtual {p0, v5}, Lcom/android/settings/widget/F;->aCJ(Z)I

    move-result v0

    invoke-virtual {p2, v1, v0}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    invoke-static {}, Lcom/android/settings/widget/SettingsAppWidgetProvider;->aCC()[I

    move-result-object v0

    aget v0, v0, v3

    invoke-virtual {p2, v2, v0}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    goto :goto_0

    :pswitch_2
    const v4, 0x7f1207b4

    invoke-direct {p0, p1, v4}, Lcom/android/settings/widget/F;->aCK(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p2, v0, v4}, Landroid/widget/RemoteViews;->setContentDescription(ILjava/lang/CharSequence;)V

    invoke-virtual {p0, v6}, Lcom/android/settings/widget/F;->aCJ(Z)I

    move-result v0

    invoke-virtual {p2, v1, v0}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    invoke-static {}, Lcom/android/settings/widget/SettingsAppWidgetProvider;->aCD()[I

    move-result-object v0

    aget v0, v0, v3

    invoke-virtual {p2, v2, v0}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    goto :goto_0

    :pswitch_3
    invoke-virtual {p0}, Lcom/android/settings/widget/F;->aCN()Z

    move-result v4

    if-eqz v4, :cond_0

    const v4, 0x7f1207b7

    invoke-direct {p0, p1, v4}, Lcom/android/settings/widget/F;->aCK(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p2, v0, v4}, Landroid/widget/RemoteViews;->setContentDescription(ILjava/lang/CharSequence;)V

    invoke-virtual {p0, v6}, Lcom/android/settings/widget/F;->aCJ(Z)I

    move-result v0

    invoke-virtual {p2, v1, v0}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    invoke-static {}, Lcom/android/settings/widget/SettingsAppWidgetProvider;->-get0()[I

    move-result-object v0

    aget v0, v0, v3

    invoke-virtual {p2, v2, v0}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    goto :goto_0

    :cond_0
    const v4, 0x7f1207b6

    invoke-direct {p0, p1, v4}, Lcom/android/settings/widget/F;->aCK(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p2, v0, v4}, Landroid/widget/RemoteViews;->setContentDescription(ILjava/lang/CharSequence;)V

    invoke-virtual {p0, v5}, Lcom/android/settings/widget/F;->aCJ(Z)I

    move-result v0

    invoke-virtual {p2, v1, v0}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    invoke-static {}, Lcom/android/settings/widget/SettingsAppWidgetProvider;->aCC()[I

    move-result-object v0

    aget v0, v0, v3

    invoke-virtual {p2, v2, v0}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method public final aCS(Landroid/content/Context;)V
    .locals 3

    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, p1}, Lcom/android/settings/widget/F;->aCM(Landroid/content/Context;)I

    move-result v2

    packed-switch v2, :pswitch_data_0

    :cond_0
    :goto_0
    :pswitch_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    iput-object v2, p0, Lcom/android/settings/widget/F;->aRy:Ljava/lang/Boolean;

    iget-boolean v2, p0, Lcom/android/settings/widget/F;->aRx:Z

    if-eqz v2, :cond_1

    iput-boolean v1, p0, Lcom/android/settings/widget/F;->aRw:Z

    :goto_1
    return-void

    :pswitch_1
    move v0, v1

    goto :goto_0

    :pswitch_2
    iget-object v2, p0, Lcom/android/settings/widget/F;->aRy:Ljava/lang/Boolean;

    if-eqz v2, :cond_0

    iget-object v0, p0, Lcom/android/settings/widget/F;->aRy:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    iput-boolean v1, p0, Lcom/android/settings/widget/F;->aRx:Z

    invoke-virtual {p0, p1, v0}, Lcom/android/settings/widget/F;->aCP(Landroid/content/Context;Z)V

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public abstract getContainerId()I
.end method

.method public getPosition()I
    .locals 1

    const/4 v0, 0x1

    return v0
.end method
