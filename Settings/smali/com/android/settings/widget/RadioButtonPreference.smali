.class public Lcom/android/settings/widget/RadioButtonPreference;
.super Landroid/support/v7/preference/CheckBoxPreference;
.source "RadioButtonPreference.java"


# instance fields
.field private aRA:Lcom/android/settings/widget/L;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/android/settings/widget/RadioButtonPreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    const v0, 0x7f04010b

    const v1, 0x101008e

    invoke-static {p1, v0, v1}, Landroid/support/v4/content/a/a;->dZs(Landroid/content/Context;II)I

    move-result v0

    invoke-direct {p0, p1, p2, v0}, Lcom/android/settings/widget/RadioButtonPreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    invoke-direct {p0, p1, p2, p3}, Landroid/support/v7/preference/CheckBoxPreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/settings/widget/RadioButtonPreference;->aRA:Lcom/android/settings/widget/L;

    const v0, 0x7f0d015a

    invoke-virtual {p0, v0}, Lcom/android/settings/widget/RadioButtonPreference;->dkS(I)V

    return-void
.end method


# virtual methods
.method public aCX(Lcom/android/settings/widget/L;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/widget/RadioButtonPreference;->aRA:Lcom/android/settings/widget/L;

    return-void
.end method

.method public al(Landroid/support/v7/preference/l;)V
    .locals 2

    invoke-super {p0, p1}, Landroid/support/v7/preference/CheckBoxPreference;->al(Landroid/support/v7/preference/l;)V

    const v0, 0x1020016

    invoke-virtual {p1, v0}, Landroid/support/v7/preference/l;->dma(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    if-eqz v0, :cond_0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setSingleLine(Z)V

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setMaxLines(I)V

    :cond_0
    return-void
.end method

.method public onClick()V
    .locals 1

    iget-object v0, p0, Lcom/android/settings/widget/RadioButtonPreference;->aRA:Lcom/android/settings/widget/L;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/widget/RadioButtonPreference;->aRA:Lcom/android/settings/widget/L;

    invoke-interface {v0, p0}, Lcom/android/settings/widget/L;->gl(Lcom/android/settings/widget/RadioButtonPreference;)V

    :cond_0
    return-void
.end method
