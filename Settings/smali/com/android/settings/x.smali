.class Lcom/android/settings/x;
.super Ljava/lang/Object;
.source "DiracEqualizer.java"


# instance fields
.field private final bvV:Ljava/util/List;

.field private final mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 9

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/settings/x;->bvV:Ljava/util/List;

    iput-object p1, p0, Lcom/android/settings/x;->mContext:Landroid/content/Context;

    invoke-direct {p0, p1}, Lcom/android/settings/x;->bju(Landroid/content/Context;)V

    invoke-static {p1}, Lcom/android/settings/DiracEqualizer;->bje(Landroid/content/Context;)[Ljava/lang/String;

    move-result-object v2

    array-length v0, v2

    if-nez v0, :cond_0

    return-void

    :cond_0
    const/4 v0, 0x7

    new-array v3, v0, [F

    array-length v4, v2

    move v0, v1

    :goto_0
    if-ge v0, v4, :cond_2

    aget-object v5, v2, v0

    invoke-static {v5}, Lcom/android/settings/DiracEqualizer;->bjd(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-static {p1, v5, v3}, Lcom/android/settings/DiracEqualizer;->biN(Landroid/content/Context;Ljava/lang/String;[F)Z

    move-result v6

    if-eqz v6, :cond_1

    iget-object v6, p0, Lcom/android/settings/x;->bvV:Ljava/util/List;

    new-instance v7, Lcom/android/settings/w;

    invoke-static {v5}, Lcom/android/settings/DiracEqualizer;->bjg(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v5, v8, v3, v1}, Lcom/android/settings/w;-><init>(Ljava/lang/String;Ljava/lang/String;[FI)V

    invoke-interface {v6, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    return-void
.end method

.method private bju(Landroid/content/Context;)V
    .locals 9

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f03006c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v1

    const v2, 0x7f03006d

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v2

    const/4 v0, 0x7

    new-array v3, v0, [F

    const/4 v0, 0x0

    :goto_0
    array-length v4, v1

    if-ge v0, v4, :cond_1

    aget-object v4, v2, v0

    invoke-static {v4, v3}, Lcom/android/settings/DiracEqualizer;->biE(Ljava/lang/String;[F)Z

    move-result v4

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/android/settings/x;->bvV:Ljava/util/List;

    new-instance v5, Lcom/android/settings/w;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "dirac_eq_preset#"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    aget-object v7, v1, v0

    const/4 v8, 0x1

    invoke-direct {v5, v6, v7, v3, v8}, Lcom/android/settings/w;-><init>(Ljava/lang/String;Ljava/lang/String;[FI)V

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method


# virtual methods
.method public bjn(Ljava/lang/String;[F)Lcom/android/settings/w;
    .locals 3

    const/4 v2, 0x0

    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    :cond_0
    return-object v2

    :cond_1
    array-length v0, p2

    const/4 v1, 0x7

    if-ne v0, v1, :cond_0

    invoke-static {p1}, Lcom/android/settings/DiracEqualizer;->bjf(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/android/settings/x;->bjo(Ljava/lang/String;)Lcom/android/settings/w;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {v0, p2}, Lcom/android/settings/w;->bjm([F)V

    :goto_0
    iget-object v1, p0, Lcom/android/settings/x;->mContext:Landroid/content/Context;

    invoke-static {v1, v0}, Lcom/android/settings/DiracEqualizer;->biQ(Landroid/content/Context;Lcom/android/settings/w;)V

    return-object v0

    :cond_2
    new-instance v0, Lcom/android/settings/w;

    const/4 v2, 0x0

    invoke-direct {v0, v1, p1, p2, v2}, Lcom/android/settings/w;-><init>(Ljava/lang/String;Ljava/lang/String;[FI)V

    iget-object v1, p0, Lcom/android/settings/x;->bvV:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v1, p0, Lcom/android/settings/x;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/android/settings/x;->bvV:Ljava/util/List;

    invoke-static {v1, v2}, Lcom/android/settings/DiracEqualizer;->bjh(Landroid/content/Context;Ljava/util/List;)V

    goto :goto_0
.end method

.method public bjo(Ljava/lang/String;)Lcom/android/settings/w;
    .locals 4

    const/4 v3, 0x0

    if-eqz p1, :cond_1

    iget-object v0, p0, Lcom/android/settings/x;->bvV:Ljava/util/List;

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settings/w;

    iget-object v2, v0, Lcom/android/settings/w;->bvS:Ljava/lang/String;

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    return-object v0

    :cond_1
    return-object v3
.end method

.method public bjp(Ljava/lang/String;)Lcom/android/settings/w;
    .locals 1

    invoke-static {p1}, Lcom/android/settings/DiracEqualizer;->bjf(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/settings/x;->bjo(Ljava/lang/String;)Lcom/android/settings/w;

    move-result-object v0

    return-object v0
.end method

.method public bjq(I)Lcom/android/settings/w;
    .locals 1

    if-gez p1, :cond_0

    iget-object v0, p0, Lcom/android/settings/x;->bvV:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lt p1, v0, :cond_0

    const/4 v0, 0x0

    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/android/settings/x;->bvV:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settings/w;

    return-object v0
.end method

.method public bjr()Lcom/android/settings/w;
    .locals 2

    iget-object v0, p0, Lcom/android/settings/x;->bvV:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settings/w;

    return-object v0
.end method

.method public bjs()[Ljava/lang/String;
    .locals 5

    iget-object v0, p0, Lcom/android/settings/x;->bvV:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    new-array v3, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/android/settings/x;->bvV:Ljava/util/List;

    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move v1, v0

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settings/w;

    add-int/lit8 v2, v1, 0x1

    iget-object v0, v0, Lcom/android/settings/w;->mTitle:Ljava/lang/String;

    aput-object v0, v3, v1

    move v1, v2

    goto :goto_0

    :cond_0
    return-object v3
.end method

.method public bjt(Lcom/android/settings/w;)I
    .locals 5

    const/4 v2, -0x1

    if-eqz p1, :cond_0

    iget-object v0, p1, Lcom/android/settings/w;->bvS:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v3, p1, Lcom/android/settings/w;->bvS:Ljava/lang/String;

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/android/settings/x;->bvV:Ljava/util/List;

    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move v1, v0

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settings/w;

    iget-object v0, v0, Lcom/android/settings/w;->bvS:Ljava/lang/String;

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :goto_1
    return v1

    :cond_0
    move v1, v2

    goto :goto_1

    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_2
    move v1, v2

    goto :goto_1
.end method

.method public bjv(Lcom/android/settings/w;)Z
    .locals 2

    const/4 v1, 0x0

    if-eqz p1, :cond_1

    iget v0, p1, Lcom/android/settings/w;->bvT:I

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/android/settings/x;->bvV:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/x;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/android/settings/x;->bvV:Ljava/util/List;

    invoke-static {v0, v1}, Lcom/android/settings/DiracEqualizer;->bjh(Landroid/content/Context;Ljava/util/List;)V

    iget-object v0, p0, Lcom/android/settings/x;->mContext:Landroid/content/Context;

    iget-object v1, p1, Lcom/android/settings/w;->bvS:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/android/settings/DiracEqualizer;->bji(Landroid/content/Context;Ljava/lang/String;)V

    :cond_0
    const/4 v0, 0x1

    return v0

    :cond_1
    return v1
.end method
