.class Lcom/android/settings/bg;
.super Landroid/content/BroadcastReceiver;
.source "TetherSettings.java"


# instance fields
.field final synthetic bLh:Lcom/android/settings/TetherSettings;


# direct methods
.method private constructor <init>(Lcom/android/settings/TetherSettings;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/bg;->bLh:Lcom/android/settings/TetherSettings;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/settings/TetherSettings;Lcom/android/settings/bg;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/settings/bg;-><init>(Lcom/android/settings/TetherSettings;)V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 7

    const/16 v6, 0xb

    const/4 v5, 0x0

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "android.net.conn.TETHER_STATE_CHANGED"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const-string/jumbo v0, "availableArray"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getStringArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    const-string/jumbo v1, "tetherArray"

    invoke-virtual {p2, v1}, Landroid/content/Intent;->getStringArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v1

    const-string/jumbo v2, "erroredArray"

    invoke-virtual {p2, v2}, Landroid/content/Intent;->getStringArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v2

    iget-object v3, p0, Lcom/android/settings/bg;->bLh:Lcom/android/settings/TetherSettings;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v4

    new-array v4, v4, [Ljava/lang/String;

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v4

    new-array v4, v4, [Ljava/lang/String;

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Ljava/lang/String;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v4

    new-array v4, v4, [Ljava/lang/String;

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Ljava/lang/String;

    invoke-static {v3, v0, v1, v2}, Lcom/android/settings/TetherSettings;->bBE(Lcom/android/settings/TetherSettings;[Ljava/lang/String;[Ljava/lang/String;[Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/settings/bg;->bLh:Lcom/android/settings/TetherSettings;

    invoke-static {v0}, Lcom/android/settings/TetherSettings;->bBx(Lcom/android/settings/TetherSettings;)Landroid/net/wifi/WifiManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/wifi/WifiManager;->getWifiApState()I

    move-result v0

    if-ne v0, v6, :cond_0

    iget-object v0, p0, Lcom/android/settings/bg;->bLh:Lcom/android/settings/TetherSettings;

    invoke-static {v0}, Lcom/android/settings/TetherSettings;->bBw(Lcom/android/settings/TetherSettings;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/bg;->bLh:Lcom/android/settings/TetherSettings;

    invoke-static {v0, v5}, Lcom/android/settings/TetherSettings;->bBA(Lcom/android/settings/TetherSettings;Z)Z

    const-string/jumbo v0, "TetheringSettings"

    const-string/jumbo v1, "Restarting WifiAp due to prior config change."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/settings/bg;->bLh:Lcom/android/settings/TetherSettings;

    invoke-static {v0, v5}, Lcom/android/settings/TetherSettings;->bBC(Lcom/android/settings/TetherSettings;I)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string/jumbo v1, "android.net.wifi.WIFI_AP_STATE_CHANGED"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    const-string/jumbo v0, "wifi_state"

    invoke-virtual {p2, v0, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    if-ne v0, v6, :cond_0

    iget-object v0, p0, Lcom/android/settings/bg;->bLh:Lcom/android/settings/TetherSettings;

    invoke-static {v0}, Lcom/android/settings/TetherSettings;->bBw(Lcom/android/settings/TetherSettings;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/bg;->bLh:Lcom/android/settings/TetherSettings;

    invoke-static {v0, v5}, Lcom/android/settings/TetherSettings;->bBA(Lcom/android/settings/TetherSettings;Z)Z

    const-string/jumbo v0, "TetheringSettings"

    const-string/jumbo v1, "Restarting WifiAp due to prior config change."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/settings/bg;->bLh:Lcom/android/settings/TetherSettings;

    invoke-static {v0, v5}, Lcom/android/settings/TetherSettings;->bBC(Lcom/android/settings/TetherSettings;I)V

    goto :goto_0

    :cond_2
    const-string/jumbo v1, "android.intent.action.MEDIA_SHARED"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v0, p0, Lcom/android/settings/bg;->bLh:Lcom/android/settings/TetherSettings;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/android/settings/TetherSettings;->bBz(Lcom/android/settings/TetherSettings;Z)Z

    iget-object v0, p0, Lcom/android/settings/bg;->bLh:Lcom/android/settings/TetherSettings;

    invoke-static {v0}, Lcom/android/settings/TetherSettings;->bBD(Lcom/android/settings/TetherSettings;)V

    goto :goto_0

    :cond_3
    const-string/jumbo v1, "android.intent.action.MEDIA_UNSHARED"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    iget-object v0, p0, Lcom/android/settings/bg;->bLh:Lcom/android/settings/TetherSettings;

    invoke-static {v0, v5}, Lcom/android/settings/TetherSettings;->bBz(Lcom/android/settings/TetherSettings;Z)Z

    iget-object v0, p0, Lcom/android/settings/bg;->bLh:Lcom/android/settings/TetherSettings;

    invoke-static {v0}, Lcom/android/settings/TetherSettings;->bBD(Lcom/android/settings/TetherSettings;)V

    goto :goto_0

    :cond_4
    const-string/jumbo v1, "android.hardware.usb.action.USB_STATE"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    iget-object v0, p0, Lcom/android/settings/bg;->bLh:Lcom/android/settings/TetherSettings;

    const-string/jumbo v1, "connected"

    invoke-virtual {p2, v1, v5}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    invoke-static {v0, v1}, Lcom/android/settings/TetherSettings;->bBB(Lcom/android/settings/TetherSettings;Z)Z

    iget-object v0, p0, Lcom/android/settings/bg;->bLh:Lcom/android/settings/TetherSettings;

    invoke-static {v0}, Lcom/android/settings/TetherSettings;->bBD(Lcom/android/settings/TetherSettings;)V

    goto :goto_0

    :cond_5
    const-string/jumbo v1, "android.bluetooth.adapter.action.STATE_CHANGED"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/bg;->bLh:Lcom/android/settings/TetherSettings;

    invoke-static {v0}, Lcom/android/settings/TetherSettings;->bBu(Lcom/android/settings/TetherSettings;)Z

    move-result v0

    if-eqz v0, :cond_6

    const-string/jumbo v0, "android.bluetooth.adapter.extra.STATE"

    const/high16 v1, -0x80000000

    invoke-virtual {p2, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    :cond_6
    :goto_1
    iget-object v0, p0, Lcom/android/settings/bg;->bLh:Lcom/android/settings/TetherSettings;

    invoke-static {v0}, Lcom/android/settings/TetherSettings;->bBD(Lcom/android/settings/TetherSettings;)V

    goto/16 :goto_0

    :sswitch_0
    iget-object v0, p0, Lcom/android/settings/bg;->bLh:Lcom/android/settings/TetherSettings;

    const/4 v1, 0x2

    invoke-static {v0, v1}, Lcom/android/settings/TetherSettings;->bBC(Lcom/android/settings/TetherSettings;I)V

    iget-object v0, p0, Lcom/android/settings/bg;->bLh:Lcom/android/settings/TetherSettings;

    invoke-static {v0, v5}, Lcom/android/settings/TetherSettings;->bBy(Lcom/android/settings/TetherSettings;Z)Z

    goto :goto_1

    :sswitch_1
    iget-object v0, p0, Lcom/android/settings/bg;->bLh:Lcom/android/settings/TetherSettings;

    invoke-static {v0, v5}, Lcom/android/settings/TetherSettings;->bBy(Lcom/android/settings/TetherSettings;Z)Z

    goto :goto_1

    :sswitch_data_0
    .sparse-switch
        -0x80000000 -> :sswitch_1
        0xa -> :sswitch_1
        0xc -> :sswitch_0
    .end sparse-switch
.end method
