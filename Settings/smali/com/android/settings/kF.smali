.class final Lcom/android/settings/kF;
.super Ljava/lang/Object;
.source "MiuiConfirmCommonPassword.java"

# interfaces
.implements Lcom/android/settings/bl;


# instance fields
.field final synthetic crm:Lcom/android/settings/MiuiConfirmCommonPassword;


# direct methods
.method constructor <init>(Lcom/android/settings/MiuiConfirmCommonPassword;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/kF;->crm:Lcom/android/settings/MiuiConfirmCommonPassword;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public Zm()V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/kF;->crm:Lcom/android/settings/MiuiConfirmCommonPassword;

    invoke-static {v0}, Lcom/android/settings/MiuiConfirmCommonPassword;->bUt(Lcom/android/settings/MiuiConfirmCommonPassword;)Lcom/android/settings/bM;

    move-result-object v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/settings/kF;->crm:Lcom/android/settings/MiuiConfirmCommonPassword;

    invoke-static {v0}, Lcom/android/settings/MiuiConfirmCommonPassword;->bUu(Lcom/android/settings/MiuiConfirmCommonPassword;)Landroid/app/AlertDialog;

    move-result-object v0

    const v1, 0x7f0a00f2

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const v1, 0x7f120725

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    iget-object v0, p0, Lcom/android/settings/kF;->crm:Lcom/android/settings/MiuiConfirmCommonPassword;

    invoke-static {v0}, Lcom/android/settings/MiuiConfirmCommonPassword;->bUs(Lcom/android/settings/MiuiConfirmCommonPassword;)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    invoke-static {v0, v1}, Lcom/android/settings/MiuiConfirmCommonPassword;->bUv(Lcom/android/settings/MiuiConfirmCommonPassword;I)I

    iget-object v0, p0, Lcom/android/settings/kF;->crm:Lcom/android/settings/MiuiConfirmCommonPassword;

    invoke-static {v0}, Lcom/android/settings/MiuiConfirmCommonPassword;->bUs(Lcom/android/settings/MiuiConfirmCommonPassword;)I

    move-result v0

    const/4 v1, 0x5

    if-lt v0, v1, :cond_1

    iget-object v0, p0, Lcom/android/settings/kF;->crm:Lcom/android/settings/MiuiConfirmCommonPassword;

    invoke-static {v0}, Lcom/android/settings/MiuiConfirmCommonPassword;->bUy(Lcom/android/settings/MiuiConfirmCommonPassword;)V

    iget-object v0, p0, Lcom/android/settings/kF;->crm:Lcom/android/settings/MiuiConfirmCommonPassword;

    invoke-static {v0}, Lcom/android/settings/MiuiConfirmCommonPassword;->bUx(Lcom/android/settings/MiuiConfirmCommonPassword;)V

    iget-object v0, p0, Lcom/android/settings/kF;->crm:Lcom/android/settings/MiuiConfirmCommonPassword;

    invoke-static {v0}, Lcom/android/settings/MiuiConfirmCommonPassword;->bUz(Lcom/android/settings/MiuiConfirmCommonPassword;)V

    :cond_1
    return-void
.end method

.method public Zn(I)V
    .locals 2

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x17

    if-lt v0, v1, :cond_0

    iget-object v0, p0, Lcom/android/settings/kF;->crm:Lcom/android/settings/MiuiConfirmCommonPassword;

    invoke-static {v0}, Lcom/android/settings/MiuiConfirmCommonPassword;->bUw(Lcom/android/settings/MiuiConfirmCommonPassword;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/kF;->crm:Lcom/android/settings/MiuiConfirmCommonPassword;

    invoke-virtual {v0, p1}, Lcom/android/settings/MiuiConfirmCommonPassword;->bUo(I)V

    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/settings/kF;->crm:Lcom/android/settings/MiuiConfirmCommonPassword;

    invoke-static {v0}, Lcom/android/settings/MiuiConfirmCommonPassword;->bUy(Lcom/android/settings/MiuiConfirmCommonPassword;)V

    iget-object v0, p0, Lcom/android/settings/kF;->crm:Lcom/android/settings/MiuiConfirmCommonPassword;

    invoke-static {v0}, Lcom/android/settings/MiuiConfirmCommonPassword;->bUx(Lcom/android/settings/MiuiConfirmCommonPassword;)V

    iget-object v0, p0, Lcom/android/settings/kF;->crm:Lcom/android/settings/MiuiConfirmCommonPassword;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Lcom/android/settings/MiuiConfirmCommonPassword;->setResult(I)V

    iget-object v0, p0, Lcom/android/settings/kF;->crm:Lcom/android/settings/MiuiConfirmCommonPassword;

    invoke-virtual {v0}, Lcom/android/settings/MiuiConfirmCommonPassword;->finish()V

    return-void
.end method
