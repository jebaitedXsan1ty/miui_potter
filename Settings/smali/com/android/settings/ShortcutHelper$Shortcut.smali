.class public final enum Lcom/android/settings/ShortcutHelper$Shortcut;
.super Ljava/lang/Enum;
.source "SettingsApplication.java"


# static fields
.field private static final synthetic cdk:[Lcom/android/settings/ShortcutHelper$Shortcut;

.field public static final enum cdl:Lcom/android/settings/ShortcutHelper$Shortcut;

.field public static final enum cdm:Lcom/android/settings/ShortcutHelper$Shortcut;

.field public static final enum cdn:Lcom/android/settings/ShortcutHelper$Shortcut;

.field public static final enum cdo:Lcom/android/settings/ShortcutHelper$Shortcut;

.field public static final enum cdp:Lcom/android/settings/ShortcutHelper$Shortcut;

.field public static final enum cdq:Lcom/android/settings/ShortcutHelper$Shortcut;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    new-instance v0, Lcom/android/settings/ShortcutHelper$Shortcut;

    const-string/jumbo v1, "OPTIMIZE_CENTER"

    invoke-direct {v0, v1, v3}, Lcom/android/settings/ShortcutHelper$Shortcut;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/settings/ShortcutHelper$Shortcut;->cdn:Lcom/android/settings/ShortcutHelper$Shortcut;

    new-instance v0, Lcom/android/settings/ShortcutHelper$Shortcut;

    const-string/jumbo v1, "POWER_CENTER"

    invoke-direct {v0, v1, v4}, Lcom/android/settings/ShortcutHelper$Shortcut;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/settings/ShortcutHelper$Shortcut;->cdp:Lcom/android/settings/ShortcutHelper$Shortcut;

    new-instance v0, Lcom/android/settings/ShortcutHelper$Shortcut;

    const-string/jumbo v1, "VIRUS_CENTER"

    invoke-direct {v0, v1, v5}, Lcom/android/settings/ShortcutHelper$Shortcut;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/settings/ShortcutHelper$Shortcut;->cdq:Lcom/android/settings/ShortcutHelper$Shortcut;

    new-instance v0, Lcom/android/settings/ShortcutHelper$Shortcut;

    const-string/jumbo v1, "PERM_CENTER"

    invoke-direct {v0, v1, v6}, Lcom/android/settings/ShortcutHelper$Shortcut;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/settings/ShortcutHelper$Shortcut;->cdo:Lcom/android/settings/ShortcutHelper$Shortcut;

    new-instance v0, Lcom/android/settings/ShortcutHelper$Shortcut;

    const-string/jumbo v1, "NETWORK_ASSISTANT"

    invoke-direct {v0, v1, v7}, Lcom/android/settings/ShortcutHelper$Shortcut;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/settings/ShortcutHelper$Shortcut;->cdm:Lcom/android/settings/ShortcutHelper$Shortcut;

    new-instance v0, Lcom/android/settings/ShortcutHelper$Shortcut;

    const-string/jumbo v1, "ANTISPAM"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/android/settings/ShortcutHelper$Shortcut;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/settings/ShortcutHelper$Shortcut;->cdl:Lcom/android/settings/ShortcutHelper$Shortcut;

    const/4 v0, 0x6

    new-array v0, v0, [Lcom/android/settings/ShortcutHelper$Shortcut;

    sget-object v1, Lcom/android/settings/ShortcutHelper$Shortcut;->cdn:Lcom/android/settings/ShortcutHelper$Shortcut;

    aput-object v1, v0, v3

    sget-object v1, Lcom/android/settings/ShortcutHelper$Shortcut;->cdp:Lcom/android/settings/ShortcutHelper$Shortcut;

    aput-object v1, v0, v4

    sget-object v1, Lcom/android/settings/ShortcutHelper$Shortcut;->cdq:Lcom/android/settings/ShortcutHelper$Shortcut;

    aput-object v1, v0, v5

    sget-object v1, Lcom/android/settings/ShortcutHelper$Shortcut;->cdo:Lcom/android/settings/ShortcutHelper$Shortcut;

    aput-object v1, v0, v6

    sget-object v1, Lcom/android/settings/ShortcutHelper$Shortcut;->cdm:Lcom/android/settings/ShortcutHelper$Shortcut;

    aput-object v1, v0, v7

    sget-object v1, Lcom/android/settings/ShortcutHelper$Shortcut;->cdl:Lcom/android/settings/ShortcutHelper$Shortcut;

    const/4 v2, 0x5

    aput-object v1, v0, v2

    sput-object v0, Lcom/android/settings/ShortcutHelper$Shortcut;->cdk:[Lcom/android/settings/ShortcutHelper$Shortcut;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/android/settings/ShortcutHelper$Shortcut;
    .locals 1

    const-class v0, Lcom/android/settings/ShortcutHelper$Shortcut;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/android/settings/ShortcutHelper$Shortcut;

    return-object v0
.end method

.method public static values()[Lcom/android/settings/ShortcutHelper$Shortcut;
    .locals 1

    sget-object v0, Lcom/android/settings/ShortcutHelper$Shortcut;->cdk:[Lcom/android/settings/ShortcutHelper$Shortcut;

    return-object v0
.end method
