.class public Lcom/android/settings/vpn2/MiuiVpnGearPreference;
.super Lmiui/preference/RadioButtonPreference;
.source "MiuiVpnGearPreference.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private Zd:Lcom/android/settings/vpn2/MiuiVpnGearPreference$OnGearClickListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lmiui/preference/RadioButtonPreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method


# virtual methods
.method public onBindView(Landroid/view/View;)V
    .locals 2

    invoke-super {p0, p1}, Lmiui/preference/RadioButtonPreference;->onBindView(Landroid/view/View;)V

    const v0, 0x7f0a03df

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/View;->setEnabled(Z)V

    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const v1, 0x7f0a03df

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/android/settings/vpn2/MiuiVpnGearPreference;->Zd:Lcom/android/settings/vpn2/MiuiVpnGearPreference$OnGearClickListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/vpn2/MiuiVpnGearPreference;->Zd:Lcom/android/settings/vpn2/MiuiVpnGearPreference$OnGearClickListener;

    invoke-interface {v0, p0}, Lcom/android/settings/vpn2/MiuiVpnGearPreference$OnGearClickListener;->PC(Lcom/android/settings/vpn2/MiuiVpnGearPreference;)V

    :cond_0
    return-void
.end method
