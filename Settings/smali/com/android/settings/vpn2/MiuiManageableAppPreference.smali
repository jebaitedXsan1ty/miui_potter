.class public Lcom/android/settings/vpn2/MiuiManageableAppPreference;
.super Lcom/android/settings/widget/MiuiGearPreference;
.source "MiuiManageableAppPreference.java"


# static fields
.field public static STATE_NONE:I


# instance fields
.field YY:I

.field YZ:Z

.field mUserId:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, -0x1

    sput v0, Lcom/android/settings/vpn2/MiuiManageableAppPreference;->STATE_NONE:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0, p1, p2}, Lcom/android/settings/widget/MiuiGearPreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    iput-boolean v1, p0, Lcom/android/settings/vpn2/MiuiManageableAppPreference;->YZ:Z

    sget v0, Lcom/android/settings/vpn2/MiuiManageableAppPreference;->STATE_NONE:I

    iput v0, p0, Lcom/android/settings/vpn2/MiuiManageableAppPreference;->YY:I

    invoke-virtual {p0, v1}, Lcom/android/settings/vpn2/MiuiManageableAppPreference;->setPersistent(Z)V

    invoke-virtual {p0, v1}, Lcom/android/settings/vpn2/MiuiManageableAppPreference;->setOrder(I)V

    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/android/settings/vpn2/MiuiManageableAppPreference;->setUserId(I)V

    return-void
.end method


# virtual methods
.method public Pg(I)V
    .locals 0

    iput p1, p0, Lcom/android/settings/vpn2/MiuiManageableAppPreference;->YY:I

    invoke-virtual {p0}, Lcom/android/settings/vpn2/MiuiManageableAppPreference;->updateSummary()V

    invoke-virtual {p0}, Lcom/android/settings/vpn2/MiuiManageableAppPreference;->notifyHierarchyChanged()V

    return-void
.end method

.method public Ph(Z)V
    .locals 1

    iget-boolean v0, p0, Lcom/android/settings/vpn2/MiuiManageableAppPreference;->YZ:Z

    if-eq v0, p1, :cond_0

    iput-boolean p1, p0, Lcom/android/settings/vpn2/MiuiManageableAppPreference;->YZ:Z

    invoke-virtual {p0}, Lcom/android/settings/vpn2/MiuiManageableAppPreference;->updateSummary()V

    :cond_0
    return-void
.end method

.method public Pi()I
    .locals 1

    iget v0, p0, Lcom/android/settings/vpn2/MiuiManageableAppPreference;->mUserId:I

    return v0
.end method

.method public getState()I
    .locals 1

    iget v0, p0, Lcom/android/settings/vpn2/MiuiManageableAppPreference;->YY:I

    return v0
.end method

.method public setUserId(I)V
    .locals 1

    iput p1, p0, Lcom/android/settings/vpn2/MiuiManageableAppPreference;->mUserId:I

    const-string/jumbo v0, "no_config_vpn"

    invoke-virtual {p0, v0, p1}, Lcom/android/settings/vpn2/MiuiManageableAppPreference;->cqd(Ljava/lang/String;I)V

    return-void
.end method

.method protected updateSummary()V
    .locals 5

    invoke-virtual {p0}, Lcom/android/settings/vpn2/MiuiManageableAppPreference;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v0, 0x7f030116

    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    iget v1, p0, Lcom/android/settings/vpn2/MiuiManageableAppPreference;->YY:I

    sget v3, Lcom/android/settings/vpn2/MiuiManageableAppPreference;->STATE_NONE:I

    if-ne v1, v3, :cond_1

    const-string/jumbo v0, ""

    :goto_0
    iget-boolean v1, p0, Lcom/android/settings/vpn2/MiuiManageableAppPreference;->YZ:Z

    if-eqz v1, :cond_0

    const v1, 0x7f121439

    invoke-virtual {v2, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_2

    move-object v0, v1

    :cond_0
    :goto_1
    invoke-virtual {p0, v0}, Lcom/android/settings/vpn2/MiuiManageableAppPreference;->setSummary(Ljava/lang/CharSequence;)V

    return-void

    :cond_1
    iget v1, p0, Lcom/android/settings/vpn2/MiuiManageableAppPreference;->YY:I

    aget-object v0, v0, v1

    goto :goto_0

    :cond_2
    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    const/4 v0, 0x1

    aput-object v1, v3, v0

    const v0, 0x7f120888

    invoke-virtual {v2, v0, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method
