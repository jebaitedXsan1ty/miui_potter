.class Lcom/android/settings/nfc/PaymentSettings$SummaryProvider;
.super Ljava/lang/Object;
.source "PaymentSettings.java"

# interfaces
.implements Lcom/android/settings/dashboard/D;


# instance fields
.field private final aWR:Lcom/android/settings/dashboard/C;

.field private final mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/android/settings/dashboard/C;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/android/settings/nfc/PaymentSettings$SummaryProvider;->mContext:Landroid/content/Context;

    iput-object p2, p0, Lcom/android/settings/nfc/PaymentSettings$SummaryProvider;->aWR:Lcom/android/settings/dashboard/C;

    return-void
.end method


# virtual methods
.method public jt(Z)V
    .locals 5

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/android/settings/nfc/PaymentSettings$SummaryProvider;->mContext:Landroid/content/Context;

    invoke-static {v0}, Landroid/nfc/NfcAdapter;->getDefaultAdapter(Landroid/content/Context;)Landroid/nfc/NfcAdapter;

    move-result-object v0

    if-eqz v0, :cond_0

    new-instance v0, Lcom/android/settings/nfc/PaymentBackend;

    iget-object v1, p0, Lcom/android/settings/nfc/PaymentSettings$SummaryProvider;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/android/settings/nfc/PaymentBackend;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0}, Lcom/android/settings/nfc/PaymentBackend;->aLy()V

    invoke-virtual {v0}, Lcom/android/settings/nfc/PaymentBackend;->aLz()Lcom/android/settings/nfc/PaymentBackend$PaymentAppInfo;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/android/settings/nfc/PaymentSettings$SummaryProvider;->aWR:Lcom/android/settings/dashboard/C;

    iget-object v2, p0, Lcom/android/settings/nfc/PaymentSettings$SummaryProvider;->mContext:Landroid/content/Context;

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    iget-object v0, v0, Lcom/android/settings/nfc/PaymentBackend$PaymentAppInfo;->aWZ:Ljava/lang/CharSequence;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    const v0, 0x7f120c7e

    invoke-virtual {v2, v0, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, p0, v0}, Lcom/android/settings/dashboard/C;->Fd(Lcom/android/settings/dashboard/D;Ljava/lang/CharSequence;)V

    :cond_0
    return-void
.end method
