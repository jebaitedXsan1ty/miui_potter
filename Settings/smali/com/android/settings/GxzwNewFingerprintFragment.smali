.class public Lcom/android/settings/GxzwNewFingerprintFragment;
.super Lcom/android/settings/BaseEditFragment;
.source "GxzwNewFingerprintFragment.java"


# instance fields
.field private final bUA:I

.field private final bUB:Ljava/lang/String;

.field private final bUC:Ljava/lang/String;

.field private bUD:Landroid/app/Activity;

.field private bUE:Z

.field private bUF:Landroid/view/View;

.field private bUG:Landroid/view/View;

.field private bUH:Ljava/lang/String;

.field private bUI:I

.field private bUJ:Lcom/android/settings/bx;

.field private bUK:Lcom/android/settings/bM;

.field private bUL:Ljava/lang/String;

.field private bUM:Z

.field private bUN:Landroid/os/IBinder;

.field private bUO:Landroid/os/Handler;

.field private bUP:Ljava/lang/String;

.field private bUQ:I

.field private bUR:Landroid/widget/TextView;

.field private bUS:Landroid/view/View;

.field private bUT:Landroid/widget/TextView;

.field private bUU:Landroid/widget/TextView;

.field private bUV:Landroid/widget/TextView;

.field private bUW:Z

.field private bUX:Z

.field private bUY:Z

.field private bUZ:I

.field private final bUv:I

.field private final bUw:I

.field private final bUx:I

.field private final bUy:I

.field private final bUz:I

.field private bVa:Landroid/net/Uri;

.field private bVb:Landroid/view/View;

.field private bVc:Landroid/app/AlertDialog;

.field private bVd:Z

.field private bVe:Z

.field private bVf:Lcom/android/settings/MutedVideoView;

.field private bVg:I

.field private bVh:Landroid/os/Vibrator;

.field private mProgress:I


# direct methods
.method public constructor <init>()V
    .locals 4

    const/4 v3, 0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/android/settings/BaseEditFragment;-><init>()V

    iput-object v2, p0, Lcom/android/settings/GxzwNewFingerprintFragment;->bUK:Lcom/android/settings/bM;

    iput v3, p0, Lcom/android/settings/GxzwNewFingerprintFragment;->bUQ:I

    iput v1, p0, Lcom/android/settings/GxzwNewFingerprintFragment;->mProgress:I

    iput v1, p0, Lcom/android/settings/GxzwNewFingerprintFragment;->bVg:I

    iput-boolean v1, p0, Lcom/android/settings/GxzwNewFingerprintFragment;->bUY:Z

    iput-object v2, p0, Lcom/android/settings/GxzwNewFingerprintFragment;->bVh:Landroid/os/Vibrator;

    iput-object v2, p0, Lcom/android/settings/GxzwNewFingerprintFragment;->bVc:Landroid/app/AlertDialog;

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/android/settings/GxzwNewFingerprintFragment;->bUO:Landroid/os/Handler;

    iput-boolean v1, p0, Lcom/android/settings/GxzwNewFingerprintFragment;->bVe:Z

    iput v1, p0, Lcom/android/settings/GxzwNewFingerprintFragment;->bUI:I

    iput-object v2, p0, Lcom/android/settings/GxzwNewFingerprintFragment;->bVa:Landroid/net/Uri;

    iput-boolean v1, p0, Lcom/android/settings/GxzwNewFingerprintFragment;->bVd:Z

    iput-boolean v1, p0, Lcom/android/settings/GxzwNewFingerprintFragment;->bUX:Z

    iput-object v2, p0, Lcom/android/settings/GxzwNewFingerprintFragment;->bUD:Landroid/app/Activity;

    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/android/settings/GxzwNewFingerprintFragment;->bUL:Ljava/lang/String;

    const-string/jumbo v0, "service_name"

    iput-object v0, p0, Lcom/android/settings/GxzwNewFingerprintFragment;->bUB:Ljava/lang/String;

    const-string/jumbo v0, "interface_descriptor"

    iput-object v0, p0, Lcom/android/settings/GxzwNewFingerprintFragment;->bUC:Ljava/lang/String;

    iput v3, p0, Lcom/android/settings/GxzwNewFingerprintFragment;->bUz:I

    iput v1, p0, Lcom/android/settings/GxzwNewFingerprintFragment;->bUA:I

    iput v3, p0, Lcom/android/settings/GxzwNewFingerprintFragment;->bUv:I

    const/4 v0, 0x2

    iput v0, p0, Lcom/android/settings/GxzwNewFingerprintFragment;->bUw:I

    const/4 v0, 0x3

    iput v0, p0, Lcom/android/settings/GxzwNewFingerprintFragment;->bUx:I

    const/4 v0, 0x4

    iput v0, p0, Lcom/android/settings/GxzwNewFingerprintFragment;->bUy:I

    new-instance v0, Lcom/android/settings/jF;

    invoke-direct {v0, p0}, Lcom/android/settings/jF;-><init>(Lcom/android/settings/GxzwNewFingerprintFragment;)V

    iput-object v0, p0, Lcom/android/settings/GxzwNewFingerprintFragment;->bUJ:Lcom/android/settings/bx;

    return-void
.end method

.method private bNT(Landroid/app/AlertDialog;)V
    .locals 1

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Landroid/app/AlertDialog;->dismiss()V

    :cond_0
    return-void
.end method

.method private bNU(Ljava/lang/String;)Landroid/net/Uri;
    .locals 4

    iget-boolean v0, p0, Lcom/android/settings/GxzwNewFingerprintFragment;->bUW:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "_dark"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    :cond_0
    iget-object v0, p0, Lcom/android/settings/GxzwNewFingerprintFragment;->bUD:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getPackageName()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/GxzwNewFingerprintFragment;->bUD:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const-string/jumbo v2, "raw"

    invoke-virtual {v1, p1, v2, v0}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_1

    const/4 v0, 0x0

    return-object v0

    :cond_1
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "android.resource://"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v2, "/"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method private bNV(II)I
    .locals 6

    const/4 v0, -0x1

    iget-object v1, p0, Lcom/android/settings/GxzwNewFingerprintFragment;->bUN:Landroid/os/IBinder;

    if-nez v1, :cond_0

    const-string/jumbo v1, "service_name"

    invoke-static {v1}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v1

    iput-object v1, p0, Lcom/android/settings/GxzwNewFingerprintFragment;->bUN:Landroid/os/IBinder;

    :cond_0
    iget-object v1, p0, Lcom/android/settings/GxzwNewFingerprintFragment;->bUN:Landroid/os/IBinder;

    if-nez v1, :cond_1

    :goto_0
    return v0

    :cond_1
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v1

    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v2

    :try_start_0
    const-string/jumbo v3, "interface_descriptor"

    invoke-virtual {v1, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Landroid/os/Parcel;->writeInt(I)V

    invoke-virtual {v1, p2}, Landroid/os/Parcel;->writeInt(I)V

    iget-object v3, p0, Lcom/android/settings/GxzwNewFingerprintFragment;->bUN:Landroid/os/IBinder;

    const/4 v4, 0x1

    const/4 v5, 0x0

    invoke-interface {v3, v4, v1, v2, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    invoke-virtual {v2}, Landroid/os/Parcel;->readException()V

    invoke-virtual {v2}, Landroid/os/Parcel;->readInt()I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    goto :goto_0

    :catch_0
    move-exception v3

    const/4 v3, 0x0

    :try_start_1
    iput-object v3, p0, Lcom/android/settings/GxzwNewFingerprintFragment;->bUN:Landroid/os/IBinder;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    throw v0
.end method

.method private bNW(Landroid/net/Uri;Lcom/android/settings/MutedVideoView;)V
    .locals 1

    const/4 v0, 0x0

    if-eqz p1, :cond_0

    iput-object v0, p0, Lcom/android/settings/GxzwNewFingerprintFragment;->bVa:Landroid/net/Uri;

    invoke-virtual {p2, p1}, Lcom/android/settings/MutedVideoView;->setVideoURI(Landroid/net/Uri;)V

    new-instance v0, Lcom/android/settings/jN;

    invoke-direct {v0, p0, p2}, Lcom/android/settings/jN;-><init>(Lcom/android/settings/GxzwNewFingerprintFragment;Lcom/android/settings/MutedVideoView;)V

    invoke-virtual {p2, v0}, Lcom/android/settings/MutedVideoView;->setOnPreparedListener(Landroid/media/MediaPlayer$OnPreparedListener;)V

    :cond_0
    return-void
.end method

.method private bNX()V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/android/settings/GxzwNewFingerprintFragment;->bUK:Lcom/android/settings/bM;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/GxzwNewFingerprintFragment;->bUK:Lcom/android/settings/bM;

    invoke-virtual {v0}, Lcom/android/settings/bM;->bNj()V

    iput-object v1, p0, Lcom/android/settings/GxzwNewFingerprintFragment;->bUK:Lcom/android/settings/bM;

    :cond_0
    return-void
.end method

.method private bNY()V
    .locals 3

    iget-object v0, p0, Lcom/android/settings/GxzwNewFingerprintFragment;->bUL:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/android/settings/GxzwNewFingerprintFragment;->bUD:Landroid/app/Activity;

    iget-object v2, p0, Lcom/android/settings/GxzwNewFingerprintFragment;->bUP:Ljava/lang/String;

    invoke-static {v1, v2, v0}, Lcom/android/settings/bd;->bAa(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method private bNZ()V
    .locals 4

    const/16 v3, 0x8

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/android/settings/GxzwNewFingerprintFragment;->bUT:Landroid/widget/TextView;

    const-string/jumbo v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/android/settings/GxzwNewFingerprintFragment;->bUT:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/settings/GxzwNewFingerprintFragment;->bUF:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/settings/GxzwNewFingerprintFragment;->bVf:Lcom/android/settings/MutedVideoView;

    invoke-virtual {v0, v3}, Lcom/android/settings/MutedVideoView;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/settings/GxzwNewFingerprintFragment;->bUS:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/settings/GxzwNewFingerprintFragment;->bUR:Landroid/widget/TextView;

    const v1, 0x7f1207d5

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    iget-object v0, p0, Lcom/android/settings/GxzwNewFingerprintFragment;->bUR:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    invoke-direct {p0, v2}, Lcom/android/settings/GxzwNewFingerprintFragment;->bOb(Z)V

    return-void
.end method

.method static synthetic bOA(Lcom/android/settings/GxzwNewFingerprintFragment;I)I
    .locals 0

    iput p1, p0, Lcom/android/settings/GxzwNewFingerprintFragment;->bUI:I

    return p1
.end method

.method static synthetic bOB(Lcom/android/settings/GxzwNewFingerprintFragment;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    iput-object p1, p0, Lcom/android/settings/GxzwNewFingerprintFragment;->bUL:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic bOC(Lcom/android/settings/GxzwNewFingerprintFragment;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    iput-object p1, p0, Lcom/android/settings/GxzwNewFingerprintFragment;->bUP:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic bOD(Lcom/android/settings/GxzwNewFingerprintFragment;I)I
    .locals 0

    iput p1, p0, Lcom/android/settings/GxzwNewFingerprintFragment;->bUQ:I

    return p1
.end method

.method static synthetic bOE(Lcom/android/settings/GxzwNewFingerprintFragment;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/android/settings/GxzwNewFingerprintFragment;->bUX:Z

    return p1
.end method

.method static synthetic bOF(Lcom/android/settings/GxzwNewFingerprintFragment;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/android/settings/GxzwNewFingerprintFragment;->bUY:Z

    return p1
.end method

.method static synthetic bOG(Lcom/android/settings/GxzwNewFingerprintFragment;I)I
    .locals 0

    iput p1, p0, Lcom/android/settings/GxzwNewFingerprintFragment;->bUZ:I

    return p1
.end method

.method static synthetic bOH(Lcom/android/settings/GxzwNewFingerprintFragment;Landroid/net/Uri;)Landroid/net/Uri;
    .locals 0

    iput-object p1, p0, Lcom/android/settings/GxzwNewFingerprintFragment;->bVa:Landroid/net/Uri;

    return-object p1
.end method

.method static synthetic bOI(Lcom/android/settings/GxzwNewFingerprintFragment;Landroid/app/AlertDialog;)Landroid/app/AlertDialog;
    .locals 0

    iput-object p1, p0, Lcom/android/settings/GxzwNewFingerprintFragment;->bVc:Landroid/app/AlertDialog;

    return-object p1
.end method

.method static synthetic bOJ(Lcom/android/settings/GxzwNewFingerprintFragment;I)I
    .locals 0

    iput p1, p0, Lcom/android/settings/GxzwNewFingerprintFragment;->mProgress:I

    return p1
.end method

.method static synthetic bOK(Lcom/android/settings/GxzwNewFingerprintFragment;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/android/settings/GxzwNewFingerprintFragment;->bVd:Z

    return p1
.end method

.method static synthetic bOL(Lcom/android/settings/GxzwNewFingerprintFragment;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/android/settings/GxzwNewFingerprintFragment;->bVe:Z

    return p1
.end method

.method static synthetic bOM(Lcom/android/settings/GxzwNewFingerprintFragment;I)I
    .locals 0

    iput p1, p0, Lcom/android/settings/GxzwNewFingerprintFragment;->bVg:I

    return p1
.end method

.method static synthetic bON(Lcom/android/settings/GxzwNewFingerprintFragment;Ljava/lang/String;)Landroid/net/Uri;
    .locals 1

    invoke-direct {p0, p1}, Lcom/android/settings/GxzwNewFingerprintFragment;->bNU(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method static synthetic bOO(Lcom/android/settings/GxzwNewFingerprintFragment;Landroid/app/AlertDialog;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/settings/GxzwNewFingerprintFragment;->bNT(Landroid/app/AlertDialog;)V

    return-void
.end method

.method static synthetic bOP(Lcom/android/settings/GxzwNewFingerprintFragment;Landroid/net/Uri;Lcom/android/settings/MutedVideoView;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/android/settings/GxzwNewFingerprintFragment;->bNW(Landroid/net/Uri;Lcom/android/settings/MutedVideoView;)V

    return-void
.end method

.method static synthetic bOQ(Lcom/android/settings/GxzwNewFingerprintFragment;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/GxzwNewFingerprintFragment;->bNX()V

    return-void
.end method

.method static synthetic bOR(Lcom/android/settings/GxzwNewFingerprintFragment;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/GxzwNewFingerprintFragment;->bNZ()V

    return-void
.end method

.method static synthetic bOS(Lcom/android/settings/GxzwNewFingerprintFragment;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/GxzwNewFingerprintFragment;->bOa()V

    return-void
.end method

.method static synthetic bOT(Lcom/android/settings/GxzwNewFingerprintFragment;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/settings/GxzwNewFingerprintFragment;->bOb(Z)V

    return-void
.end method

.method private bOa()V
    .locals 3

    const/16 v1, 0x8

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/android/settings/GxzwNewFingerprintFragment;->bUT:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/settings/GxzwNewFingerprintFragment;->bUF:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/settings/GxzwNewFingerprintFragment;->bVf:Lcom/android/settings/MutedVideoView;

    invoke-virtual {v0, v1}, Lcom/android/settings/MutedVideoView;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/settings/GxzwNewFingerprintFragment;->bUS:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/settings/GxzwNewFingerprintFragment;->bUR:Landroid/widget/TextView;

    const v1, 0x7f1207da

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    iget-object v0, p0, Lcom/android/settings/GxzwNewFingerprintFragment;->bUR:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    invoke-direct {p0, v2}, Lcom/android/settings/GxzwNewFingerprintFragment;->bOb(Z)V

    return-void
.end method

.method private bOb(Z)V
    .locals 2

    if-eqz p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/android/settings/GxzwNewFingerprintFragment;->bNV(II)I

    return-void

    :cond_0
    const/4 v0, 0x2

    goto :goto_0
.end method

.method static synthetic bOc(Lcom/android/settings/GxzwNewFingerprintFragment;)Landroid/app/Activity;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/GxzwNewFingerprintFragment;->bUD:Landroid/app/Activity;

    return-object v0
.end method

.method static synthetic bOd(Lcom/android/settings/GxzwNewFingerprintFragment;)Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/GxzwNewFingerprintFragment;->bUF:Landroid/view/View;

    return-object v0
.end method

.method static synthetic bOe(Lcom/android/settings/GxzwNewFingerprintFragment;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/GxzwNewFingerprintFragment;->bUH:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic bOf(Lcom/android/settings/GxzwNewFingerprintFragment;)Lcom/android/settings/bM;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/GxzwNewFingerprintFragment;->bUK:Lcom/android/settings/bM;

    return-object v0
.end method

.method static synthetic bOg(Lcom/android/settings/GxzwNewFingerprintFragment;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/GxzwNewFingerprintFragment;->bUL:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic bOh(Lcom/android/settings/GxzwNewFingerprintFragment;)Landroid/os/Handler;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/GxzwNewFingerprintFragment;->bUO:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic bOi(Lcom/android/settings/GxzwNewFingerprintFragment;)I
    .locals 1

    iget v0, p0, Lcom/android/settings/GxzwNewFingerprintFragment;->bUQ:I

    return v0
.end method

.method static synthetic bOj(Lcom/android/settings/GxzwNewFingerprintFragment;)Landroid/widget/TextView;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/GxzwNewFingerprintFragment;->bUR:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic bOk(Lcom/android/settings/GxzwNewFingerprintFragment;)Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/GxzwNewFingerprintFragment;->bUS:Landroid/view/View;

    return-object v0
.end method

.method static synthetic bOl(Lcom/android/settings/GxzwNewFingerprintFragment;)Landroid/widget/TextView;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/GxzwNewFingerprintFragment;->bUT:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic bOm(Lcom/android/settings/GxzwNewFingerprintFragment;)Landroid/widget/TextView;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/GxzwNewFingerprintFragment;->bUU:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic bOn(Lcom/android/settings/GxzwNewFingerprintFragment;)Landroid/widget/TextView;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/GxzwNewFingerprintFragment;->bUV:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic bOo(Lcom/android/settings/GxzwNewFingerprintFragment;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/settings/GxzwNewFingerprintFragment;->bUX:Z

    return v0
.end method

.method static synthetic bOp(Lcom/android/settings/GxzwNewFingerprintFragment;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/settings/GxzwNewFingerprintFragment;->bUY:Z

    return v0
.end method

.method static synthetic bOq(Lcom/android/settings/GxzwNewFingerprintFragment;)I
    .locals 1

    iget v0, p0, Lcom/android/settings/GxzwNewFingerprintFragment;->bUZ:I

    return v0
.end method

.method static synthetic bOr(Lcom/android/settings/GxzwNewFingerprintFragment;)Landroid/net/Uri;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/GxzwNewFingerprintFragment;->bVa:Landroid/net/Uri;

    return-object v0
.end method

.method static synthetic bOs(Lcom/android/settings/GxzwNewFingerprintFragment;)Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/GxzwNewFingerprintFragment;->bVb:Landroid/view/View;

    return-object v0
.end method

.method static synthetic bOt(Lcom/android/settings/GxzwNewFingerprintFragment;)Landroid/app/AlertDialog;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/GxzwNewFingerprintFragment;->bVc:Landroid/app/AlertDialog;

    return-object v0
.end method

.method static synthetic bOu(Lcom/android/settings/GxzwNewFingerprintFragment;)I
    .locals 1

    iget v0, p0, Lcom/android/settings/GxzwNewFingerprintFragment;->mProgress:I

    return v0
.end method

.method static synthetic bOv(Lcom/android/settings/GxzwNewFingerprintFragment;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/settings/GxzwNewFingerprintFragment;->bVd:Z

    return v0
.end method

.method static synthetic bOw(Lcom/android/settings/GxzwNewFingerprintFragment;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/settings/GxzwNewFingerprintFragment;->bVe:Z

    return v0
.end method

.method static synthetic bOx(Lcom/android/settings/GxzwNewFingerprintFragment;)Lcom/android/settings/MutedVideoView;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/GxzwNewFingerprintFragment;->bVf:Lcom/android/settings/MutedVideoView;

    return-object v0
.end method

.method static synthetic bOy(Lcom/android/settings/GxzwNewFingerprintFragment;)I
    .locals 1

    iget v0, p0, Lcom/android/settings/GxzwNewFingerprintFragment;->bVg:I

    return v0
.end method

.method static synthetic bOz(Lcom/android/settings/GxzwNewFingerprintFragment;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    iput-object p1, p0, Lcom/android/settings/GxzwNewFingerprintFragment;->bUH:Ljava/lang/String;

    return-object p1
.end method


# virtual methods
.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 4

    const/4 v3, 0x1

    invoke-super {p0, p1, p2, p3}, Lcom/android/settings/BaseEditFragment;->onActivityResult(IILandroid/content/Intent;)V

    const/16 v0, 0x64

    if-ne p1, v0, :cond_0

    const/4 v0, -0x1

    if-ne p2, v0, :cond_1

    iput-boolean v3, p0, Lcom/android/settings/GxzwNewFingerprintFragment;->bVe:Z

    const-string/jumbo v0, "hw_auth_token"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getByteArrayExtra(Ljava/lang/String;)[B

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/GxzwNewFingerprintFragment;->bUK:Lcom/android/settings/bM;

    iget-object v2, p0, Lcom/android/settings/GxzwNewFingerprintFragment;->bUJ:Lcom/android/settings/bx;

    invoke-virtual {v1, v2, v0}, Lcom/android/settings/bM;->bNh(Lcom/android/settings/bx;[B)V

    iget-object v0, p0, Lcom/android/settings/GxzwNewFingerprintFragment;->bUD:Landroid/app/Activity;

    const-string/jumbo v1, "vibrator"

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Vibrator;

    iput-object v0, p0, Lcom/android/settings/GxzwNewFingerprintFragment;->bVh:Landroid/os/Vibrator;

    invoke-direct {p0, v3}, Lcom/android/settings/GxzwNewFingerprintFragment;->bOb(Z)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0}, Lcom/android/settings/GxzwNewFingerprintFragment;->finish()V

    goto :goto_0
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 1

    invoke-super {p0, p1}, Lcom/android/settings/BaseEditFragment;->onAttach(Landroid/app/Activity;)V

    iget-object v0, p0, Lcom/android/settings/GxzwNewFingerprintFragment;->bUD:Landroid/app/Activity;

    if-nez v0, :cond_0

    iput-object p1, p0, Lcom/android/settings/GxzwNewFingerprintFragment;->bUD:Landroid/app/Activity;

    :cond_0
    return-void
.end method

.method public onAttach(Landroid/content/Context;)V
    .locals 1

    invoke-super {p0, p1}, Lcom/android/settings/BaseEditFragment;->onAttach(Landroid/content/Context;)V

    instance-of v0, p1, Landroid/app/Activity;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/GxzwNewFingerprintFragment;->bUD:Landroid/app/Activity;

    if-nez v0, :cond_0

    check-cast p1, Landroid/app/Activity;

    iput-object p1, p0, Lcom/android/settings/GxzwNewFingerprintFragment;->bUD:Landroid/app/Activity;

    :cond_0
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 6

    const/4 v5, 0x1

    const/4 v4, 0x0

    invoke-super {p0, p1}, Lcom/android/settings/BaseEditFragment;->onCreate(Landroid/os/Bundle;)V

    sget v0, Lmiui/R$style;->Theme_Dark_Settings:I

    invoke-virtual {p0, v0}, Lcom/android/settings/GxzwNewFingerprintFragment;->setThemeRes(I)V

    new-instance v0, Lcom/android/settings/bM;

    iget-object v1, p0, Lcom/android/settings/GxzwNewFingerprintFragment;->bUD:Landroid/app/Activity;

    invoke-direct {v0, v1}, Lcom/android/settings/bM;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/settings/GxzwNewFingerprintFragment;->bUK:Lcom/android/settings/bM;

    iput-boolean v5, p0, Lcom/android/settings/GxzwNewFingerprintFragment;->bUW:Z

    const-string/jumbo v0, "front_fingerprint_sensor"

    invoke-static {v0, v4}, Lmiui/util/FeatureParser;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/settings/GxzwNewFingerprintFragment;->bUM:Z

    invoke-virtual {p0}, Lcom/android/settings/GxzwNewFingerprintFragment;->getIntent()Landroid/content/Intent;

    move-result-object v0

    if-eqz v0, :cond_0

    const-string/jumbo v1, "add_keyguard_password_then_add_fingerprint"

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    iput-boolean v1, p0, Lcom/android/settings/GxzwNewFingerprintFragment;->bUE:Z

    :goto_0
    iget-boolean v1, p0, Lcom/android/settings/GxzwNewFingerprintFragment;->bUE:Z

    if-eqz v1, :cond_1

    iput-boolean v5, p0, Lcom/android/settings/GxzwNewFingerprintFragment;->bVe:Z

    const-string/jumbo v1, "hw_auth_token"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getByteArrayExtra(Ljava/lang/String;)[B

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/GxzwNewFingerprintFragment;->bUK:Lcom/android/settings/bM;

    iget-object v2, p0, Lcom/android/settings/GxzwNewFingerprintFragment;->bUJ:Lcom/android/settings/bx;

    invoke-virtual {v1, v2, v0}, Lcom/android/settings/bM;->bNh(Lcom/android/settings/bx;[B)V

    iget-object v0, p0, Lcom/android/settings/GxzwNewFingerprintFragment;->bUD:Landroid/app/Activity;

    const-string/jumbo v1, "vibrator"

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Vibrator;

    iput-object v0, p0, Lcom/android/settings/GxzwNewFingerprintFragment;->bVh:Landroid/os/Vibrator;

    :goto_1
    iget-object v0, p0, Lcom/android/settings/GxzwNewFingerprintFragment;->bUD:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v1, 0x80

    invoke-virtual {v0, v1}, Landroid/view/Window;->addFlags(I)V

    return-void

    :cond_0
    iput-boolean v4, p0, Lcom/android/settings/GxzwNewFingerprintFragment;->bUE:Z

    goto :goto_0

    :cond_1
    new-instance v0, Landroid/security/MiuiLockPatternUtils;

    iget-object v1, p0, Lcom/android/settings/GxzwNewFingerprintFragment;->bUD:Landroid/app/Activity;

    invoke-direct {v0, v1}, Landroid/security/MiuiLockPatternUtils;-><init>(Landroid/content/Context;)V

    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v1

    invoke-static {v0, v1}, Lcom/android/settings/bn;->bFG(Lcom/android/internal/widget/LockPatternUtils;I)I

    move-result v1

    const-class v0, Lcom/android/settings/MiuiSecurityChooseUnlock$InternalActivity;

    if-eqz v1, :cond_2

    const/high16 v0, 0x10000

    if-ne v1, v0, :cond_3

    const-class v0, Lcom/android/settings/ConfirmLockPattern$InternalActivity;

    :cond_2
    :goto_2
    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v1

    if-nez v1, :cond_4

    new-instance v1, Landroid/content/Intent;

    iget-object v2, p0, Lcom/android/settings/GxzwNewFingerprintFragment;->bUD:Landroid/app/Activity;

    invoke-direct {v1, v2, v0}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    move-object v0, v1

    :goto_3
    const-string/jumbo v1, "has_challenge"

    invoke-virtual {v0, v1, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string/jumbo v1, "challenge"

    iget-object v2, p0, Lcom/android/settings/GxzwNewFingerprintFragment;->bUK:Lcom/android/settings/bM;

    invoke-virtual {v2}, Lcom/android/settings/bM;->bNi()J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    const/16 v1, 0x64

    invoke-virtual {p0, v0, v1}, Lcom/android/settings/GxzwNewFingerprintFragment;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_1

    :cond_3
    const-class v0, Lcom/android/settings/ConfirmLockPassword$InternalActivity;

    goto :goto_2

    :cond_4
    invoke-virtual {p0}, Lcom/android/settings/GxzwNewFingerprintFragment;->getIntent()Landroid/content/Intent;

    move-result-object v2

    new-instance v1, Landroid/content/Intent;

    iget-object v3, p0, Lcom/android/settings/GxzwNewFingerprintFragment;->bUD:Landroid/app/Activity;

    invoke-direct {v1, v3, v0}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v0, "com.android.settings.ConfirmLockPattern.header"

    const-string/jumbo v3, "com.android.settings.ConfirmLockPattern.header"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->getCharSequenceExtra(Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v1, v0, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/CharSequence;)Landroid/content/Intent;

    const-string/jumbo v0, "com.android.settings.titleColor"

    const-string/jumbo v3, "com.android.settings.titleColor"

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v3

    invoke-virtual {v1, v0, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string/jumbo v0, "com.android.settings.bgColor"

    const-string/jumbo v3, "com.android.settings.bgColor"

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v3

    invoke-virtual {v1, v0, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string/jumbo v0, "com.android.settings.lockBtnWhite"

    invoke-virtual {v1, v0, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string/jumbo v0, "com.android.settings.forgetPatternColor"

    const-string/jumbo v3, "com.android.settings.forgetPatternColor"

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v3

    invoke-virtual {v1, v0, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string/jumbo v0, "com.android.settings.footerTextColor"

    const-string/jumbo v3, "com.android.settings.footerTextColor"

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string/jumbo v0, "com.android.settings.forgetPassword"

    invoke-virtual {v1, v0, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-object v0, v1

    goto :goto_3
.end method

.method public onDetach()V
    .locals 1

    invoke-super {p0}, Lcom/android/settings/BaseEditFragment;->onDetach()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/settings/GxzwNewFingerprintFragment;->bUD:Landroid/app/Activity;

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/android/settings/GxzwNewFingerprintFragment;->bOb(Z)V

    return-void
.end method

.method public onPause()V
    .locals 1

    iget-boolean v0, p0, Lcom/android/settings/GxzwNewFingerprintFragment;->bVe:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/settings/GxzwNewFingerprintFragment;->bVe:Z

    invoke-direct {p0}, Lcom/android/settings/GxzwNewFingerprintFragment;->bNX()V

    iget-object v0, p0, Lcom/android/settings/GxzwNewFingerprintFragment;->bVc:Landroid/app/AlertDialog;

    invoke-direct {p0, v0}, Lcom/android/settings/GxzwNewFingerprintFragment;->bNT(Landroid/app/AlertDialog;)V

    invoke-virtual {p0}, Lcom/android/settings/GxzwNewFingerprintFragment;->finish()V

    :cond_0
    iget-object v0, p0, Lcom/android/settings/GxzwNewFingerprintFragment;->bUP:Ljava/lang/String;

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lcom/android/settings/GxzwNewFingerprintFragment;->bNY()V

    :cond_1
    invoke-super {p0}, Lcom/android/settings/BaseEditFragment;->onPause()V

    return-void
.end method

.method public onStart()V
    .locals 1

    invoke-super {p0}, Lcom/android/settings/BaseEditFragment;->onStart()V

    invoke-virtual {p0}, Lcom/android/settings/GxzwNewFingerprintFragment;->getActionBar()Lmiui/app/ActionBar;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/app/ActionBar;->hide()V

    :cond_0
    return-void
.end method

.method public vB(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 4

    const/4 v3, 0x0

    const/4 v2, 0x1

    const v0, 0x7f0d00cc

    invoke-virtual {p1, v0, p2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/GxzwNewFingerprintFragment;->bUG:Landroid/view/View;

    iget-object v0, p0, Lcom/android/settings/GxzwNewFingerprintFragment;->bUG:Landroid/view/View;

    const/16 v1, 0x1302

    invoke-virtual {v0, v1}, Landroid/view/View;->setSystemUiVisibility(I)V

    iget-object v0, p0, Lcom/android/settings/GxzwNewFingerprintFragment;->bUG:Landroid/view/View;

    const v1, 0x7f0a02c9

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/settings/GxzwNewFingerprintFragment;->bUV:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/android/settings/GxzwNewFingerprintFragment;->bUG:Landroid/view/View;

    const v1, 0x7f0a02c8

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/settings/GxzwNewFingerprintFragment;->bUU:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/android/settings/GxzwNewFingerprintFragment;->bUG:Landroid/view/View;

    const v1, 0x7f0a01d2

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/settings/GxzwNewFingerprintFragment;->bUT:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/android/settings/GxzwNewFingerprintFragment;->bUG:Landroid/view/View;

    const v1, 0x7f0a01d1

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/GxzwNewFingerprintFragment;->bUS:Landroid/view/View;

    iget-object v0, p0, Lcom/android/settings/GxzwNewFingerprintFragment;->bUG:Landroid/view/View;

    const v1, 0x7f0a01d0

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/settings/GxzwNewFingerprintFragment;->bUR:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/android/settings/GxzwNewFingerprintFragment;->bUG:Landroid/view/View;

    const v1, 0x7f0a02c6

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/settings/MutedVideoView;

    iput-object v0, p0, Lcom/android/settings/GxzwNewFingerprintFragment;->bVf:Lcom/android/settings/MutedVideoView;

    iget-object v0, p0, Lcom/android/settings/GxzwNewFingerprintFragment;->bUV:Landroid/widget/TextView;

    const v1, 0x7f1207e1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    iget-object v0, p0, Lcom/android/settings/GxzwNewFingerprintFragment;->bUU:Landroid/widget/TextView;

    const v1, 0x7f1207db

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    iget-object v0, p0, Lcom/android/settings/GxzwNewFingerprintFragment;->bUG:Landroid/view/View;

    const v1, 0x7f0a01d3

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/GxzwNewFingerprintFragment;->bVb:Landroid/view/View;

    iget-object v0, p0, Lcom/android/settings/GxzwNewFingerprintFragment;->bUG:Landroid/view/View;

    const v1, 0x7f0a01cf

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/GxzwNewFingerprintFragment;->bUF:Landroid/view/View;

    new-instance v0, Lcom/android/settings/jI;

    invoke-direct {v0, p0}, Lcom/android/settings/jI;-><init>(Lcom/android/settings/GxzwNewFingerprintFragment;)V

    iget-object v1, p0, Lcom/android/settings/GxzwNewFingerprintFragment;->bVf:Lcom/android/settings/MutedVideoView;

    invoke-virtual {v1, v2}, Lcom/android/settings/MutedVideoView;->setZOrderOnTop(Z)V

    iget-object v1, p0, Lcom/android/settings/GxzwNewFingerprintFragment;->bVf:Lcom/android/settings/MutedVideoView;

    invoke-virtual {v1, v0}, Lcom/android/settings/MutedVideoView;->setOnErrorListener(Landroid/media/MediaPlayer$OnErrorListener;)V

    iget-object v0, p0, Lcom/android/settings/GxzwNewFingerprintFragment;->bVf:Lcom/android/settings/MutedVideoView;

    new-instance v1, Lcom/android/settings/jJ;

    invoke-direct {v1, p0}, Lcom/android/settings/jJ;-><init>(Lcom/android/settings/GxzwNewFingerprintFragment;)V

    invoke-virtual {v0, v1}, Lcom/android/settings/MutedVideoView;->setOnCompletionListener(Landroid/media/MediaPlayer$OnCompletionListener;)V

    const-string/jumbo v0, "core_scan_output_%02d"

    new-array v1, v2, [Ljava/lang/Object;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/android/settings/GxzwNewFingerprintFragment;->bNU(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/android/settings/GxzwNewFingerprintFragment;->bVf:Lcom/android/settings/MutedVideoView;

    invoke-virtual {v1, v0}, Lcom/android/settings/MutedVideoView;->setVideoURI(Landroid/net/Uri;)V

    :cond_0
    iget-object v0, p0, Lcom/android/settings/GxzwNewFingerprintFragment;->bVb:Landroid/view/View;

    new-instance v1, Lcom/android/settings/jK;

    invoke-direct {v1, p0}, Lcom/android/settings/jK;-><init>(Lcom/android/settings/GxzwNewFingerprintFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/android/settings/GxzwNewFingerprintFragment;->bUF:Landroid/view/View;

    new-instance v1, Lcom/android/settings/jL;

    invoke-direct {v1, p0}, Lcom/android/settings/jL;-><init>(Lcom/android/settings/GxzwNewFingerprintFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/android/settings/GxzwNewFingerprintFragment;->bUR:Landroid/widget/TextView;

    new-instance v1, Lcom/android/settings/jM;

    invoke-direct {v1, p0}, Lcom/android/settings/jM;-><init>(Lcom/android/settings/GxzwNewFingerprintFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/android/settings/GxzwNewFingerprintFragment;->bUG:Landroid/view/View;

    return-object v0
.end method
