.class Lcom/android/settings/dndmode/b;
.super Landroid/preference/Preference;
.source "VipCallSettingsFragment.java"


# instance fields
.field private lS:Landroid/widget/ImageView;

.field private lT:J

.field final synthetic lU:Lcom/android/settings/dndmode/VipCallSettingsFragment;


# direct methods
.method public constructor <init>(Lcom/android/settings/dndmode/VipCallSettingsFragment;Landroid/content/Context;)V
    .locals 1

    iput-object p1, p0, Lcom/android/settings/dndmode/b;->lU:Lcom/android/settings/dndmode/VipCallSettingsFragment;

    invoke-direct {p0, p2}, Landroid/preference/Preference;-><init>(Landroid/content/Context;)V

    const v0, 0x7f15004a

    invoke-virtual {p0, v0}, Lcom/android/settings/dndmode/b;->setLayoutResource(I)V

    return-void
.end method


# virtual methods
.method public lz(Landroid/database/Cursor;)V
    .locals 3

    const/4 v0, 0x0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/android/settings/dndmode/b;->lT:J

    const/4 v0, 0x1

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v0, 0x0

    invoke-static {}, Lcom/android/settings/dndmode/VipCallSettingsFragment;->lt()Ljava/util/HashMap;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-static {}, Lcom/android/settings/dndmode/VipCallSettingsFragment;->lt()Ljava/util/HashMap;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    :cond_0
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {p0, v1}, Lcom/android/settings/dndmode/b;->setTitle(Ljava/lang/CharSequence;)V

    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0, v0}, Lcom/android/settings/dndmode/b;->setTitle(Ljava/lang/CharSequence;)V

    invoke-virtual {p0, v1}, Lcom/android/settings/dndmode/b;->setSummary(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method protected onCreateView(Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4

    const/4 v2, 0x1

    invoke-super {p0, p1}, Landroid/preference/Preference;->onCreateView(Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    const v0, 0x7f0a0127

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/android/settings/dndmode/b;->lS:Landroid/widget/ImageView;

    iget-object v0, p0, Lcom/android/settings/dndmode/b;->lS:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setClickable(Z)V

    iget-object v0, p0, Lcom/android/settings/dndmode/b;->lS:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setFocusable(Z)V

    iget-object v0, p0, Lcom/android/settings/dndmode/b;->lS:Landroid/widget/ImageView;

    iget-wide v2, p0, Lcom/android/settings/dndmode/b;->lT:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setTag(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/android/settings/dndmode/b;->lS:Landroid/widget/ImageView;

    new-instance v2, Lcom/android/settings/dndmode/g;

    invoke-direct {v2, p0}, Lcom/android/settings/dndmode/g;-><init>(Lcom/android/settings/dndmode/b;)V

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-object v1
.end method
