.class final Lcom/android/settings/dndmode/i;
.super Ljava/lang/Object;
.source "VipCallSettingsFragment.java"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceClickListener;


# instance fields
.field final synthetic mX:Lcom/android/settings/dndmode/VipCallSettingsFragment;

.field final synthetic mY:[Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/android/settings/dndmode/VipCallSettingsFragment;[Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/dndmode/i;->mX:Lcom/android/settings/dndmode/VipCallSettingsFragment;

    iput-object p2, p0, Lcom/android/settings/dndmode/i;->mY:[Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPreferenceClick(Landroid/preference/Preference;)Z
    .locals 2

    check-cast p1, Lmiui/preference/RadioButtonPreference;

    iget-object v0, p0, Lcom/android/settings/dndmode/i;->mY:[Ljava/lang/String;

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-virtual {p1}, Lmiui/preference/RadioButtonPreference;->getTitle()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v0

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    iget-object v1, p0, Lcom/android/settings/dndmode/i;->mX:Lcom/android/settings/dndmode/VipCallSettingsFragment;

    invoke-static {v1}, Lcom/android/settings/dndmode/VipCallSettingsFragment;->lv(Lcom/android/settings/dndmode/VipCallSettingsFragment;)V

    :goto_0
    iget-object v1, p0, Lcom/android/settings/dndmode/i;->mX:Lcom/android/settings/dndmode/VipCallSettingsFragment;

    invoke-static {v1}, Lcom/android/settings/dndmode/VipCallSettingsFragment;->ls(Lcom/android/settings/dndmode/VipCallSettingsFragment;)Landroid/service/notification/ZenModeConfig;

    move-result-object v1

    invoke-virtual {v1}, Landroid/service/notification/ZenModeConfig;->copy()Landroid/service/notification/ZenModeConfig;

    move-result-object v1

    iput v0, v1, Landroid/service/notification/ZenModeConfig;->allowCallsFrom:I

    iput v0, v1, Landroid/service/notification/ZenModeConfig;->allowMessagesFrom:I

    iget-object v0, p0, Lcom/android/settings/dndmode/i;->mX:Lcom/android/settings/dndmode/VipCallSettingsFragment;

    invoke-static {v0}, Lcom/android/settings/dndmode/VipCallSettingsFragment;->lu(Lcom/android/settings/dndmode/VipCallSettingsFragment;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, v1}, Landroid/app/ExtraNotificationManager;->setZenModeConfig(Landroid/content/Context;Landroid/service/notification/ZenModeConfig;)Z

    const/4 v0, 0x1

    return v0

    :cond_0
    iget-object v1, p0, Lcom/android/settings/dndmode/i;->mX:Lcom/android/settings/dndmode/VipCallSettingsFragment;

    invoke-static {v1}, Lcom/android/settings/dndmode/VipCallSettingsFragment;->lx(Lcom/android/settings/dndmode/VipCallSettingsFragment;)V

    goto :goto_0
.end method
