.class public Lcom/android/settings/dndmode/AlarmContentFragment;
.super Lcom/android/settings/MiuiSettingsPreferenceFragment;
.source "AlarmContentFragment.java"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceChangeListener;


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private mContext:Landroid/content/Context;

.field private ms:Landroid/preference/CheckBoxPreference;

.field private mt:Landroid/service/notification/ZenModeConfig;

.field private mu:Landroid/preference/CheckBoxPreference;

.field private final mv:Landroid/os/Handler;

.field private mw:Landroid/preference/CheckBoxPreference;

.field private final mx:Lcom/android/settings/dndmode/d;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/android/settings/dndmode/AlarmContentFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/settings/dndmode/AlarmContentFragment;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/android/settings/MiuiSettingsPreferenceFragment;-><init>()V

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/android/settings/dndmode/AlarmContentFragment;->mv:Landroid/os/Handler;

    new-instance v0, Lcom/android/settings/dndmode/d;

    invoke-direct {v0, p0}, Lcom/android/settings/dndmode/d;-><init>(Lcom/android/settings/dndmode/AlarmContentFragment;)V

    iput-object v0, p0, Lcom/android/settings/dndmode/AlarmContentFragment;->mx:Lcom/android/settings/dndmode/d;

    return-void
.end method

.method private lV()V
    .locals 1

    iget-object v0, p0, Lcom/android/settings/dndmode/AlarmContentFragment;->mContext:Landroid/content/Context;

    invoke-static {v0}, Landroid/app/ExtraNotificationManager;->getZenModeConfig(Landroid/content/Context;)Landroid/service/notification/ZenModeConfig;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/dndmode/AlarmContentFragment;->mt:Landroid/service/notification/ZenModeConfig;

    return-void
.end method

.method private lW()V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/dndmode/AlarmContentFragment;->ms:Landroid/preference/CheckBoxPreference;

    iget-object v1, p0, Lcom/android/settings/dndmode/AlarmContentFragment;->mt:Landroid/service/notification/ZenModeConfig;

    iget-boolean v1, v1, Landroid/service/notification/ZenModeConfig;->allowCalls:Z

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    iget-object v0, p0, Lcom/android/settings/dndmode/AlarmContentFragment;->mw:Landroid/preference/CheckBoxPreference;

    iget-object v1, p0, Lcom/android/settings/dndmode/AlarmContentFragment;->mt:Landroid/service/notification/ZenModeConfig;

    iget-boolean v1, v1, Landroid/service/notification/ZenModeConfig;->allowMessages:Z

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    iget-object v0, p0, Lcom/android/settings/dndmode/AlarmContentFragment;->mu:Landroid/preference/CheckBoxPreference;

    iget-object v1, p0, Lcom/android/settings/dndmode/AlarmContentFragment;->mt:Landroid/service/notification/ZenModeConfig;

    iget-boolean v1, v1, Landroid/service/notification/ZenModeConfig;->allowEvents:Z

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    return-void
.end method

.method static synthetic lX(Lcom/android/settings/dndmode/AlarmContentFragment;)Landroid/os/Handler;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/dndmode/AlarmContentFragment;->mv:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic lY(Lcom/android/settings/dndmode/AlarmContentFragment;)Landroid/content/ContentResolver;
    .locals 1

    invoke-virtual {p0}, Lcom/android/settings/dndmode/AlarmContentFragment;->bWB()Landroid/content/ContentResolver;

    move-result-object v0

    return-object v0
.end method

.method static synthetic lZ(Lcom/android/settings/dndmode/AlarmContentFragment;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/dndmode/AlarmContentFragment;->lV()V

    return-void
.end method


# virtual methods
.method public cC()Ljava/lang/String;
    .locals 1

    const-class v0, Lcom/android/settings/dndmode/AlarmContentFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3

    invoke-super {p0, p1}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/android/settings/dndmode/AlarmContentFragment;->getContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/dndmode/AlarmContentFragment;->mContext:Landroid/content/Context;

    invoke-virtual {p0}, Lcom/android/settings/dndmode/AlarmContentFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "isCts"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    iget-object v0, p0, Lcom/android/settings/dndmode/AlarmContentFragment;->mx:Lcom/android/settings/dndmode/d;

    invoke-virtual {v0}, Lcom/android/settings/dndmode/d;->ma()V

    const v0, 0x7f150047

    invoke-virtual {p0, v0}, Lcom/android/settings/dndmode/AlarmContentFragment;->addPreferencesFromResource(I)V

    invoke-virtual {p0}, Lcom/android/settings/dndmode/AlarmContentFragment;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v2

    const-string/jumbo v0, "events"

    invoke-virtual {p0, v0}, Lcom/android/settings/dndmode/AlarmContentFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    iput-object v0, p0, Lcom/android/settings/dndmode/AlarmContentFragment;->mu:Landroid/preference/CheckBoxPreference;

    iget-object v0, p0, Lcom/android/settings/dndmode/AlarmContentFragment;->mu:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, p0}, Landroid/preference/CheckBoxPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    const-string/jumbo v0, "phone_calls"

    invoke-virtual {p0, v0}, Lcom/android/settings/dndmode/AlarmContentFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    iput-object v0, p0, Lcom/android/settings/dndmode/AlarmContentFragment;->ms:Landroid/preference/CheckBoxPreference;

    iget-object v0, p0, Lcom/android/settings/dndmode/AlarmContentFragment;->ms:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, p0}, Landroid/preference/CheckBoxPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    const-string/jumbo v0, "messages"

    invoke-virtual {p0, v0}, Lcom/android/settings/dndmode/AlarmContentFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    iput-object v0, p0, Lcom/android/settings/dndmode/AlarmContentFragment;->mw:Landroid/preference/CheckBoxPreference;

    iget-object v0, p0, Lcom/android/settings/dndmode/AlarmContentFragment;->mw:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, p0}, Landroid/preference/CheckBoxPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    sget-boolean v0, Lmiui/os/Build;->IS_TABLET:Z

    if-eqz v0, :cond_0

    xor-int/lit8 v0, v1, 0x1

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/dndmode/AlarmContentFragment;->ms:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v2, v0}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    iget-object v0, p0, Lcom/android/settings/dndmode/AlarmContentFragment;->mw:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v2, v0}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    :cond_0
    invoke-direct {p0}, Lcom/android/settings/dndmode/AlarmContentFragment;->lV()V

    return-void
.end method

.method public onDestroy()V
    .locals 1

    invoke-super {p0}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->onDestroy()V

    iget-object v0, p0, Lcom/android/settings/dndmode/AlarmContentFragment;->mx:Lcom/android/settings/dndmode/d;

    invoke-virtual {v0}, Lcom/android/settings/dndmode/d;->mb()V

    return-void
.end method

.method public onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 2

    iget-object v0, p0, Lcom/android/settings/dndmode/AlarmContentFragment;->ms:Landroid/preference/CheckBoxPreference;

    if-ne p1, v0, :cond_1

    iget-object v0, p0, Lcom/android/settings/dndmode/AlarmContentFragment;->mt:Landroid/service/notification/ZenModeConfig;

    invoke-virtual {v0}, Landroid/service/notification/ZenModeConfig;->copy()Landroid/service/notification/ZenModeConfig;

    move-result-object v0

    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    iput-boolean v1, v0, Landroid/service/notification/ZenModeConfig;->allowCalls:Z

    iget-object v1, p0, Lcom/android/settings/dndmode/AlarmContentFragment;->mContext:Landroid/content/Context;

    invoke-static {v1, v0}, Landroid/app/ExtraNotificationManager;->setZenModeConfig(Landroid/content/Context;Landroid/service/notification/ZenModeConfig;)Z

    :cond_0
    :goto_0
    const/4 v0, 0x1

    return v0

    :cond_1
    iget-object v0, p0, Lcom/android/settings/dndmode/AlarmContentFragment;->mw:Landroid/preference/CheckBoxPreference;

    if-ne p1, v0, :cond_2

    iget-object v0, p0, Lcom/android/settings/dndmode/AlarmContentFragment;->mt:Landroid/service/notification/ZenModeConfig;

    invoke-virtual {v0}, Landroid/service/notification/ZenModeConfig;->copy()Landroid/service/notification/ZenModeConfig;

    move-result-object v0

    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    iput-boolean v1, v0, Landroid/service/notification/ZenModeConfig;->allowMessages:Z

    iget-object v1, p0, Lcom/android/settings/dndmode/AlarmContentFragment;->mContext:Landroid/content/Context;

    invoke-static {v1, v0}, Landroid/app/ExtraNotificationManager;->setZenModeConfig(Landroid/content/Context;Landroid/service/notification/ZenModeConfig;)Z

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/android/settings/dndmode/AlarmContentFragment;->mu:Landroid/preference/CheckBoxPreference;

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/dndmode/AlarmContentFragment;->mt:Landroid/service/notification/ZenModeConfig;

    invoke-virtual {v0}, Landroid/service/notification/ZenModeConfig;->copy()Landroid/service/notification/ZenModeConfig;

    move-result-object v0

    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    iput-boolean v1, v0, Landroid/service/notification/ZenModeConfig;->allowEvents:Z

    iget-object v1, p0, Lcom/android/settings/dndmode/AlarmContentFragment;->mContext:Landroid/content/Context;

    invoke-static {v1, v0}, Landroid/app/ExtraNotificationManager;->setZenModeConfig(Landroid/content/Context;Landroid/service/notification/ZenModeConfig;)Z

    goto :goto_0
.end method

.method public onResume()V
    .locals 0

    invoke-super {p0}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->onResume()V

    invoke-direct {p0}, Lcom/android/settings/dndmode/AlarmContentFragment;->lW()V

    return-void
.end method
