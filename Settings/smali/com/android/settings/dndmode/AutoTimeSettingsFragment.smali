.class public Lcom/android/settings/dndmode/AutoTimeSettingsFragment;
.super Lcom/android/settings/dndmode/MiuiPreferenceFragment;
.source "AutoTimeSettingsFragment.java"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceClickListener;
.implements Landroid/preference/Preference$OnPreferenceChangeListener;


# instance fields
.field private lY:Landroid/app/Activity;

.field private lZ:I

.field private ma:Lcom/android/settings/dndmode/LabelPreference;

.field private mb:Lmiui/app/TimePickerDialog$OnTimeSetListener;

.field private mc:Landroid/preference/CheckBoxPreference;

.field private md:Landroid/preference/PreferenceCategory;

.field private me:Lcom/android/settings/dndmode/RepeatPreference;

.field private mf:Landroid/preference/PreferenceScreen;

.field private mg:I

.field private mh:Lcom/android/settings/dndmode/LabelPreference;

.field private mi:Z

.field private mj:Landroid/database/ContentObserver;


# direct methods
.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Lcom/android/settings/dndmode/MiuiPreferenceFragment;-><init>()V

    new-instance v0, Lcom/android/settings/dndmode/m;

    invoke-direct {v0, p0}, Lcom/android/settings/dndmode/m;-><init>(Lcom/android/settings/dndmode/AutoTimeSettingsFragment;)V

    iput-object v0, p0, Lcom/android/settings/dndmode/AutoTimeSettingsFragment;->mb:Lmiui/app/TimePickerDialog$OnTimeSetListener;

    new-instance v0, Lcom/android/settings/dndmode/n;

    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    invoke-direct {v0, p0, v1}, Lcom/android/settings/dndmode/n;-><init>(Lcom/android/settings/dndmode/AutoTimeSettingsFragment;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/android/settings/dndmode/AutoTimeSettingsFragment;->mj:Landroid/database/ContentObserver;

    return-void
.end method

.method private lD()V
    .locals 4

    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v1, "quietWristband"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    new-instance v1, Landroid/content/ComponentName;

    const-string/jumbo v2, "com.miui.securitycenter"

    const-string/jumbo v3, "com.miui.antispam.service.AntiSpamService"

    invoke-direct {v1, v2, v3}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    const-string/jumbo v1, "check"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    iget-object v1, p0, Lcom/android/settings/dndmode/AutoTimeSettingsFragment;->lY:Landroid/app/Activity;

    invoke-virtual {v1, v0}, Landroid/app/Activity;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    return-void
.end method

.method private lE(I)Ljava/lang/String;
    .locals 5

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    const-string/jumbo v1, "%d:%02d"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    div-int/lit8 v3, p1, 0x3c

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    const/4 v4, 0x0

    aput-object v3, v2, v4

    rem-int/lit8 v3, p1, 0x3c

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    const/4 v4, 0x1

    aput-object v3, v2, v4

    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic lF(Lcom/android/settings/dndmode/AutoTimeSettingsFragment;)Landroid/app/Activity;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/dndmode/AutoTimeSettingsFragment;->lY:Landroid/app/Activity;

    return-object v0
.end method

.method static synthetic lG(Lcom/android/settings/dndmode/AutoTimeSettingsFragment;)I
    .locals 1

    iget v0, p0, Lcom/android/settings/dndmode/AutoTimeSettingsFragment;->lZ:I

    return v0
.end method

.method static synthetic lH(Lcom/android/settings/dndmode/AutoTimeSettingsFragment;)Lcom/android/settings/dndmode/LabelPreference;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/dndmode/AutoTimeSettingsFragment;->ma:Lcom/android/settings/dndmode/LabelPreference;

    return-object v0
.end method

.method static synthetic lI(Lcom/android/settings/dndmode/AutoTimeSettingsFragment;)Landroid/preference/CheckBoxPreference;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/dndmode/AutoTimeSettingsFragment;->mc:Landroid/preference/CheckBoxPreference;

    return-object v0
.end method

.method static synthetic lJ(Lcom/android/settings/dndmode/AutoTimeSettingsFragment;)I
    .locals 1

    iget v0, p0, Lcom/android/settings/dndmode/AutoTimeSettingsFragment;->mg:I

    return v0
.end method

.method static synthetic lK(Lcom/android/settings/dndmode/AutoTimeSettingsFragment;)Lcom/android/settings/dndmode/LabelPreference;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/dndmode/AutoTimeSettingsFragment;->mh:Lcom/android/settings/dndmode/LabelPreference;

    return-object v0
.end method

.method static synthetic lL(Lcom/android/settings/dndmode/AutoTimeSettingsFragment;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/settings/dndmode/AutoTimeSettingsFragment;->mi:Z

    return v0
.end method

.method static synthetic lM(Lcom/android/settings/dndmode/AutoTimeSettingsFragment;I)I
    .locals 0

    iput p1, p0, Lcom/android/settings/dndmode/AutoTimeSettingsFragment;->lZ:I

    return p1
.end method

.method static synthetic lN(Lcom/android/settings/dndmode/AutoTimeSettingsFragment;I)I
    .locals 0

    iput p1, p0, Lcom/android/settings/dndmode/AutoTimeSettingsFragment;->mg:I

    return p1
.end method

.method static synthetic lO(Lcom/android/settings/dndmode/AutoTimeSettingsFragment;I)Ljava/lang/String;
    .locals 1

    invoke-direct {p0, p1}, Lcom/android/settings/dndmode/AutoTimeSettingsFragment;->lE(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public lC(Ljava/lang/String;)V
    .locals 4

    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v1, "quietWristband"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    new-instance v1, Landroid/content/ComponentName;

    const-string/jumbo v2, "com.miui.securitycenter"

    const-string/jumbo v3, "com.miui.antispam.service.AntiSpamService"

    invoke-direct {v1, v2, v3}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    const-string/jumbo v1, "mac"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string/jumbo v1, "check"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    iget-object v1, p0, Lcom/android/settings/dndmode/AutoTimeSettingsFragment;->lY:Landroid/app/Activity;

    invoke-virtual {v1, v0}, Landroid/app/Activity;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 4

    invoke-super {p0, p1}, Lcom/android/settings/dndmode/MiuiPreferenceFragment;->onCreate(Landroid/os/Bundle;)V

    const v0, 0x7f15004c

    invoke-virtual {p0, v0}, Lcom/android/settings/dndmode/AutoTimeSettingsFragment;->addPreferencesFromResource(I)V

    invoke-virtual {p0}, Lcom/android/settings/dndmode/AutoTimeSettingsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/dndmode/AutoTimeSettingsFragment;->lY:Landroid/app/Activity;

    const-string/jumbo v0, "time_setting_root"

    invoke-virtual {p0, v0}, Lcom/android/settings/dndmode/AutoTimeSettingsFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceScreen;

    iput-object v0, p0, Lcom/android/settings/dndmode/AutoTimeSettingsFragment;->mf:Landroid/preference/PreferenceScreen;

    iget-object v0, p0, Lcom/android/settings/dndmode/AutoTimeSettingsFragment;->lY:Landroid/app/Activity;

    invoke-static {v0}, Landroid/provider/MiuiSettings$AntiSpam;->getStartTimeForQuietMode(Landroid/content/Context;)I

    move-result v0

    iput v0, p0, Lcom/android/settings/dndmode/AutoTimeSettingsFragment;->mg:I

    iget-object v0, p0, Lcom/android/settings/dndmode/AutoTimeSettingsFragment;->lY:Landroid/app/Activity;

    invoke-static {v0}, Landroid/provider/MiuiSettings$AntiSpam;->getEndTimeForQuietMode(Landroid/content/Context;)I

    move-result v0

    iput v0, p0, Lcom/android/settings/dndmode/AutoTimeSettingsFragment;->lZ:I

    const-string/jumbo v0, "start_time"

    invoke-virtual {p0, v0}, Lcom/android/settings/dndmode/AutoTimeSettingsFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Lcom/android/settings/dndmode/LabelPreference;

    iput-object v0, p0, Lcom/android/settings/dndmode/AutoTimeSettingsFragment;->mh:Lcom/android/settings/dndmode/LabelPreference;

    const-string/jumbo v0, "end_time"

    invoke-virtual {p0, v0}, Lcom/android/settings/dndmode/AutoTimeSettingsFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Lcom/android/settings/dndmode/LabelPreference;

    iput-object v0, p0, Lcom/android/settings/dndmode/AutoTimeSettingsFragment;->ma:Lcom/android/settings/dndmode/LabelPreference;

    const-string/jumbo v0, "repeat"

    invoke-virtual {p0, v0}, Lcom/android/settings/dndmode/AutoTimeSettingsFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Lcom/android/settings/dndmode/RepeatPreference;

    iput-object v0, p0, Lcom/android/settings/dndmode/AutoTimeSettingsFragment;->me:Lcom/android/settings/dndmode/RepeatPreference;

    const-string/jumbo v0, "key_quiet_wristband_category"

    invoke-virtual {p0, v0}, Lcom/android/settings/dndmode/AutoTimeSettingsFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceCategory;

    iput-object v0, p0, Lcom/android/settings/dndmode/AutoTimeSettingsFragment;->md:Landroid/preference/PreferenceCategory;

    const-string/jumbo v0, "key_quiet_wristband"

    invoke-virtual {p0, v0}, Lcom/android/settings/dndmode/AutoTimeSettingsFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    iput-object v0, p0, Lcom/android/settings/dndmode/AutoTimeSettingsFragment;->mc:Landroid/preference/CheckBoxPreference;

    iget-object v0, p0, Lcom/android/settings/dndmode/AutoTimeSettingsFragment;->mh:Lcom/android/settings/dndmode/LabelPreference;

    iget v1, p0, Lcom/android/settings/dndmode/AutoTimeSettingsFragment;->mg:I

    invoke-direct {p0, v1}, Lcom/android/settings/dndmode/AutoTimeSettingsFragment;->lE(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/settings/dndmode/LabelPreference;->mp(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/settings/dndmode/AutoTimeSettingsFragment;->mh:Lcom/android/settings/dndmode/LabelPreference;

    invoke-virtual {v0, p0}, Lcom/android/settings/dndmode/LabelPreference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    iget-object v0, p0, Lcom/android/settings/dndmode/AutoTimeSettingsFragment;->ma:Lcom/android/settings/dndmode/LabelPreference;

    iget v1, p0, Lcom/android/settings/dndmode/AutoTimeSettingsFragment;->lZ:I

    invoke-direct {p0, v1}, Lcom/android/settings/dndmode/AutoTimeSettingsFragment;->lE(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/settings/dndmode/LabelPreference;->mp(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/settings/dndmode/AutoTimeSettingsFragment;->ma:Lcom/android/settings/dndmode/LabelPreference;

    invoke-virtual {v0, p0}, Lcom/android/settings/dndmode/LabelPreference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    new-instance v0, Lcom/android/settings/dndmode/e;

    iget-object v1, p0, Lcom/android/settings/dndmode/AutoTimeSettingsFragment;->lY:Landroid/app/Activity;

    invoke-static {v1}, Landroid/provider/MiuiSettings$AntiSpam;->getQuietRepeatType(Landroid/content/Context;)I

    move-result v1

    invoke-direct {v0, v1}, Lcom/android/settings/dndmode/e;-><init>(I)V

    iget-object v1, p0, Lcom/android/settings/dndmode/AutoTimeSettingsFragment;->lY:Landroid/app/Activity;

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/android/settings/dndmode/e;->mj(Landroid/content/Context;Z)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/android/settings/dndmode/AutoTimeSettingsFragment;->me:Lcom/android/settings/dndmode/RepeatPreference;

    invoke-virtual {v2, v1}, Lcom/android/settings/dndmode/RepeatPreference;->mc(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/android/settings/dndmode/AutoTimeSettingsFragment;->me:Lcom/android/settings/dndmode/RepeatPreference;

    invoke-virtual {v1, v0}, Lcom/android/settings/dndmode/RepeatPreference;->md(Lcom/android/settings/dndmode/e;)V

    sget-boolean v0, Lmiui/os/Build;->IS_TABLET:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/dndmode/AutoTimeSettingsFragment;->mc:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, p0}, Landroid/preference/CheckBoxPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    iget-object v0, p0, Lcom/android/settings/dndmode/AutoTimeSettingsFragment;->mc:Landroid/preference/CheckBoxPreference;

    iget-object v1, p0, Lcom/android/settings/dndmode/AutoTimeSettingsFragment;->lY:Landroid/app/Activity;

    invoke-static {v1}, Landroid/provider/MiuiSettings$AntiSpam;->isQuietWristband(Landroid/content/Context;)Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    :goto_0
    iget-object v0, p0, Lcom/android/settings/dndmode/AutoTimeSettingsFragment;->lY:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v1, "quiet_wristband"

    invoke-static {v1}, Landroid/provider/Settings$Secure;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iget-object v2, p0, Lcom/android/settings/dndmode/AutoTimeSettingsFragment;->mj:Landroid/database/ContentObserver;

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v3, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/settings/dndmode/AutoTimeSettingsFragment;->mf:Landroid/preference/PreferenceScreen;

    iget-object v1, p0, Lcom/android/settings/dndmode/AutoTimeSettingsFragment;->md:Landroid/preference/PreferenceCategory;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    goto :goto_0
.end method

.method public onDestroy()V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/dndmode/AutoTimeSettingsFragment;->lY:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/dndmode/AutoTimeSettingsFragment;->mj:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    iget-object v0, p0, Lcom/android/settings/dndmode/AutoTimeSettingsFragment;->lY:Landroid/app/Activity;

    iget-object v1, p0, Lcom/android/settings/dndmode/AutoTimeSettingsFragment;->me:Lcom/android/settings/dndmode/RepeatPreference;

    invoke-virtual {v1}, Lcom/android/settings/dndmode/RepeatPreference;->me()Lcom/android/settings/dndmode/e;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/settings/dndmode/e;->mk()I

    move-result v1

    invoke-static {v0, v1}, Landroid/provider/MiuiSettings$AntiSpam;->setQuietRepeatType(Landroid/content/Context;I)V

    invoke-super {p0}, Lcom/android/settings/dndmode/MiuiPreferenceFragment;->onDestroy()V

    return-void
.end method

.method public onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 3

    const/4 v2, 0x1

    iget-object v0, p0, Lcom/android/settings/dndmode/AutoTimeSettingsFragment;->mc:Landroid/preference/CheckBoxPreference;

    if-ne p1, v0, :cond_0

    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_2

    new-instance v0, Landroid/security/MiuiLockPatternUtils;

    iget-object v1, p0, Lcom/android/settings/dndmode/AutoTimeSettingsFragment;->lY:Landroid/app/Activity;

    invoke-direct {v0, v1}, Landroid/security/MiuiLockPatternUtils;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0}, Landroid/security/MiuiLockPatternUtils;->getBluetoothAddressToUnlock()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    invoke-virtual {p0, v0}, Lcom/android/settings/dndmode/AutoTimeSettingsFragment;->lC(Ljava/lang/String;)V

    :cond_0
    :goto_0
    const/4 v0, 0x0

    return v0

    :cond_1
    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v1, "miui.bluetooth.action.PICK_DEVICE"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string/jumbo v1, "miui.bluetooth.extra.MIBLE_PROPERTY"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    iget-object v1, p0, Lcom/android/settings/dndmode/AutoTimeSettingsFragment;->lY:Landroid/app/Activity;

    invoke-virtual {v1, v0, v2}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0

    :cond_2
    invoke-direct {p0}, Lcom/android/settings/dndmode/AutoTimeSettingsFragment;->lD()V

    goto :goto_0
.end method

.method public onPreferenceClick(Landroid/preference/Preference;)Z
    .locals 7

    const/4 v5, 0x1

    const/4 v6, 0x0

    new-instance v0, Lmiui/app/TimePickerDialog;

    iget-object v1, p0, Lcom/android/settings/dndmode/AutoTimeSettingsFragment;->lY:Landroid/app/Activity;

    iget-object v2, p0, Lcom/android/settings/dndmode/AutoTimeSettingsFragment;->mb:Lmiui/app/TimePickerDialog$OnTimeSetListener;

    iget v3, p0, Lcom/android/settings/dndmode/AutoTimeSettingsFragment;->mg:I

    div-int/lit8 v3, v3, 0x3c

    iget v4, p0, Lcom/android/settings/dndmode/AutoTimeSettingsFragment;->mg:I

    rem-int/lit8 v4, v4, 0x3c

    invoke-direct/range {v0 .. v5}, Lmiui/app/TimePickerDialog;-><init>(Landroid/content/Context;Lmiui/app/TimePickerDialog$OnTimeSetListener;IIZ)V

    iget-object v1, p0, Lcom/android/settings/dndmode/AutoTimeSettingsFragment;->mh:Lcom/android/settings/dndmode/LabelPreference;

    if-ne p1, v1, :cond_2

    iput-boolean v6, p0, Lcom/android/settings/dndmode/AutoTimeSettingsFragment;->mi:Z

    iget v1, p0, Lcom/android/settings/dndmode/AutoTimeSettingsFragment;->mg:I

    if-lez v1, :cond_1

    iget v1, p0, Lcom/android/settings/dndmode/AutoTimeSettingsFragment;->mg:I

    div-int/lit8 v1, v1, 0x3c

    iget v2, p0, Lcom/android/settings/dndmode/AutoTimeSettingsFragment;->mg:I

    rem-int/lit8 v2, v2, 0x3c

    invoke-virtual {v0, v1, v2}, Lmiui/app/TimePickerDialog;->updateTime(II)V

    :goto_0
    invoke-virtual {v0}, Lmiui/app/TimePickerDialog;->show()V

    :cond_0
    :goto_1
    return v6

    :cond_1
    invoke-virtual {v0, v6, v6}, Lmiui/app/TimePickerDialog;->updateTime(II)V

    goto :goto_0

    :cond_2
    iget-object v1, p0, Lcom/android/settings/dndmode/AutoTimeSettingsFragment;->ma:Lcom/android/settings/dndmode/LabelPreference;

    if-ne p1, v1, :cond_0

    iput-boolean v5, p0, Lcom/android/settings/dndmode/AutoTimeSettingsFragment;->mi:Z

    iget v1, p0, Lcom/android/settings/dndmode/AutoTimeSettingsFragment;->lZ:I

    if-lez v1, :cond_3

    iget v1, p0, Lcom/android/settings/dndmode/AutoTimeSettingsFragment;->lZ:I

    div-int/lit8 v1, v1, 0x3c

    iget v2, p0, Lcom/android/settings/dndmode/AutoTimeSettingsFragment;->lZ:I

    rem-int/lit8 v2, v2, 0x3c

    invoke-virtual {v0, v1, v2}, Lmiui/app/TimePickerDialog;->updateTime(II)V

    :goto_2
    invoke-virtual {v0}, Lmiui/app/TimePickerDialog;->show()V

    goto :goto_1

    :cond_3
    invoke-virtual {v0, v6, v6}, Lmiui/app/TimePickerDialog;->updateTime(II)V

    goto :goto_2
.end method

.method public onResume()V
    .locals 0

    invoke-super {p0}, Lcom/android/settings/dndmode/MiuiPreferenceFragment;->onResume()V

    return-void
.end method
