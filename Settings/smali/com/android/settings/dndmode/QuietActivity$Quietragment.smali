.class public Lcom/android/settings/dndmode/QuietActivity$Quietragment;
.super Lcom/android/settings/dndmode/MiuiPreferenceFragment;
.source "QuietActivity.java"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceClickListener;
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private lE:Lmiui/app/Activity;

.field private lF:Landroid/database/ContentObserver;

.field private lG:Lcom/android/settings/dndmode/RadioButtonWithArrow;

.field private lH:Lcom/android/settings/dndmode/RadioButtonWithArrow;

.field private lI:Lcom/android/settings/dndmode/RadioButtonWithArrow;


# direct methods
.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Lcom/android/settings/dndmode/MiuiPreferenceFragment;-><init>()V

    new-instance v0, Lcom/android/settings/dndmode/f;

    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    invoke-direct {v0, p0, v1}, Lcom/android/settings/dndmode/f;-><init>(Lcom/android/settings/dndmode/QuietActivity$Quietragment;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/android/settings/dndmode/QuietActivity$Quietragment;->lF:Landroid/database/ContentObserver;

    return-void
.end method

.method private kV(I)V
    .locals 3

    const/4 v2, 0x1

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/android/settings/dndmode/QuietActivity$Quietragment;->lG:Lcom/android/settings/dndmode/RadioButtonWithArrow;

    invoke-virtual {v0, v1}, Lcom/android/settings/dndmode/RadioButtonWithArrow;->setChecked(Z)V

    iget-object v0, p0, Lcom/android/settings/dndmode/QuietActivity$Quietragment;->lI:Lcom/android/settings/dndmode/RadioButtonWithArrow;

    invoke-virtual {v0, v1}, Lcom/android/settings/dndmode/RadioButtonWithArrow;->setChecked(Z)V

    iget-object v0, p0, Lcom/android/settings/dndmode/QuietActivity$Quietragment;->lH:Lcom/android/settings/dndmode/RadioButtonWithArrow;

    invoke-virtual {v0, v1}, Lcom/android/settings/dndmode/RadioButtonWithArrow;->setChecked(Z)V

    packed-switch p1, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    iget-object v0, p0, Lcom/android/settings/dndmode/QuietActivity$Quietragment;->lG:Lcom/android/settings/dndmode/RadioButtonWithArrow;

    invoke-virtual {v0, v2}, Lcom/android/settings/dndmode/RadioButtonWithArrow;->setChecked(Z)V

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Lcom/android/settings/dndmode/QuietActivity$Quietragment;->lH:Lcom/android/settings/dndmode/RadioButtonWithArrow;

    invoke-virtual {v0, v2}, Lcom/android/settings/dndmode/RadioButtonWithArrow;->setChecked(Z)V

    goto :goto_0

    :pswitch_2
    iget-object v0, p0, Lcom/android/settings/dndmode/QuietActivity$Quietragment;->lI:Lcom/android/settings/dndmode/RadioButtonWithArrow;

    invoke-virtual {v0, v2}, Lcom/android/settings/dndmode/RadioButtonWithArrow;->setChecked(Z)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method

.method static synthetic kW(Lcom/android/settings/dndmode/QuietActivity$Quietragment;)Lmiui/app/Activity;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/dndmode/QuietActivity$Quietragment;->lE:Lmiui/app/Activity;

    return-object v0
.end method

.method static synthetic kX(Lcom/android/settings/dndmode/QuietActivity$Quietragment;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/settings/dndmode/QuietActivity$Quietragment;->kV(I)V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3

    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    new-instance v1, Landroid/content/Intent;

    const-string/jumbo v2, "android.settings.ZEN_MODE_SETTINGS"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string/jumbo v2, "silent"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    const-string/jumbo v0, "switch"

    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/android/settings/dndmode/QuietActivity$Quietragment;->lE:Lmiui/app/Activity;

    invoke-virtual {v0, v1}, Lmiui/app/Activity;->startActivity(Landroid/content/Intent;)V

    return-void

    :cond_1
    const-string/jumbo v2, "quiet"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string/jumbo v0, "switch"

    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 4

    const/4 v3, 0x0

    invoke-super {p0, p1}, Lcom/android/settings/dndmode/MiuiPreferenceFragment;->onCreate(Landroid/os/Bundle;)V

    const v0, 0x7f150048

    invoke-virtual {p0, v0}, Lcom/android/settings/dndmode/QuietActivity$Quietragment;->addPreferencesFromResource(I)V

    invoke-virtual {p0}, Lcom/android/settings/dndmode/QuietActivity$Quietragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lmiui/app/Activity;

    iput-object v0, p0, Lcom/android/settings/dndmode/QuietActivity$Quietragment;->lE:Lmiui/app/Activity;

    const-string/jumbo v0, "off"

    invoke-virtual {p0, v0}, Lcom/android/settings/dndmode/QuietActivity$Quietragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Lcom/android/settings/dndmode/RadioButtonWithArrow;

    iput-object v0, p0, Lcom/android/settings/dndmode/QuietActivity$Quietragment;->lG:Lcom/android/settings/dndmode/RadioButtonWithArrow;

    const-string/jumbo v0, "silent"

    invoke-virtual {p0, v0}, Lcom/android/settings/dndmode/QuietActivity$Quietragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Lcom/android/settings/dndmode/RadioButtonWithArrow;

    iput-object v0, p0, Lcom/android/settings/dndmode/QuietActivity$Quietragment;->lI:Lcom/android/settings/dndmode/RadioButtonWithArrow;

    const-string/jumbo v0, "quiet"

    invoke-virtual {p0, v0}, Lcom/android/settings/dndmode/QuietActivity$Quietragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Lcom/android/settings/dndmode/RadioButtonWithArrow;

    iput-object v0, p0, Lcom/android/settings/dndmode/QuietActivity$Quietragment;->lH:Lcom/android/settings/dndmode/RadioButtonWithArrow;

    iget-object v0, p0, Lcom/android/settings/dndmode/QuietActivity$Quietragment;->lG:Lcom/android/settings/dndmode/RadioButtonWithArrow;

    invoke-virtual {v0, p0}, Lcom/android/settings/dndmode/RadioButtonWithArrow;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    iget-object v0, p0, Lcom/android/settings/dndmode/QuietActivity$Quietragment;->lG:Lcom/android/settings/dndmode/RadioButtonWithArrow;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/android/settings/dndmode/RadioButtonWithArrow;->lA(I)V

    iget-object v0, p0, Lcom/android/settings/dndmode/QuietActivity$Quietragment;->lI:Lcom/android/settings/dndmode/RadioButtonWithArrow;

    invoke-virtual {v0, p0}, Lcom/android/settings/dndmode/RadioButtonWithArrow;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    iget-object v0, p0, Lcom/android/settings/dndmode/QuietActivity$Quietragment;->lI:Lcom/android/settings/dndmode/RadioButtonWithArrow;

    invoke-virtual {v0, p0}, Lcom/android/settings/dndmode/RadioButtonWithArrow;->lB(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/android/settings/dndmode/QuietActivity$Quietragment;->lH:Lcom/android/settings/dndmode/RadioButtonWithArrow;

    invoke-virtual {v0, p0}, Lcom/android/settings/dndmode/RadioButtonWithArrow;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    iget-object v0, p0, Lcom/android/settings/dndmode/QuietActivity$Quietragment;->lH:Lcom/android/settings/dndmode/RadioButtonWithArrow;

    invoke-virtual {v0, p0}, Lcom/android/settings/dndmode/RadioButtonWithArrow;->lB(Landroid/view/View$OnClickListener;)V

    invoke-virtual {p0}, Lcom/android/settings/dndmode/QuietActivity$Quietragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v1, "zen_mode"

    invoke-static {v1}, Landroid/provider/Settings$Global;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iget-object v2, p0, Lcom/android/settings/dndmode/QuietActivity$Quietragment;->lF:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1, v3, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    iget-object v0, p0, Lcom/android/settings/dndmode/QuietActivity$Quietragment;->lE:Lmiui/app/Activity;

    invoke-virtual {v0}, Lmiui/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v1, "zen_mode"

    invoke-static {v0, v1, v3}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    invoke-direct {p0, v0}, Lcom/android/settings/dndmode/QuietActivity$Quietragment;->kV(I)V

    return-void
.end method

.method public onPreferenceClick(Landroid/preference/Preference;)Z
    .locals 4

    const/4 v3, 0x0

    const/4 v2, 0x0

    const-string/jumbo v0, "off"

    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/settings/dndmode/QuietActivity$Quietragment;->lE:Lmiui/app/Activity;

    invoke-static {v0, v3, v2}, Landroid/app/ExtraNotificationManager;->setZenMode(Landroid/content/Context;ILandroid/net/Uri;)V

    :cond_0
    :goto_0
    return v3

    :cond_1
    const-string/jumbo v0, "silent"

    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/settings/dndmode/QuietActivity$Quietragment;->lE:Lmiui/app/Activity;

    const/4 v1, 0x2

    invoke-static {v0, v1, v2}, Landroid/app/ExtraNotificationManager;->setZenMode(Landroid/content/Context;ILandroid/net/Uri;)V

    goto :goto_0

    :cond_2
    const-string/jumbo v0, "quiet"

    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/dndmode/QuietActivity$Quietragment;->lE:Lmiui/app/Activity;

    const/4 v1, 0x1

    invoke-static {v0, v1, v2}, Landroid/app/ExtraNotificationManager;->setZenMode(Landroid/content/Context;ILandroid/net/Uri;)V

    goto :goto_0
.end method
