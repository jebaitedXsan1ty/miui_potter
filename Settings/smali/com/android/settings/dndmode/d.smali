.class final Lcom/android/settings/dndmode/d;
.super Landroid/database/ContentObserver;
.source "AlarmContentFragment.java"


# instance fields
.field private final my:Landroid/net/Uri;

.field final synthetic mz:Lcom/android/settings/dndmode/AlarmContentFragment;


# direct methods
.method public constructor <init>(Lcom/android/settings/dndmode/AlarmContentFragment;)V
    .locals 1

    iput-object p1, p0, Lcom/android/settings/dndmode/d;->mz:Lcom/android/settings/dndmode/AlarmContentFragment;

    invoke-static {p1}, Lcom/android/settings/dndmode/AlarmContentFragment;->lX(Lcom/android/settings/dndmode/AlarmContentFragment;)Landroid/os/Handler;

    move-result-object v0

    invoke-direct {p0, v0}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    const-string/jumbo v0, "zen_mode_config_etag"

    invoke-static {v0}, Landroid/provider/Settings$Global;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/dndmode/d;->my:Landroid/net/Uri;

    return-void
.end method


# virtual methods
.method public ma()V
    .locals 4

    iget-object v0, p0, Lcom/android/settings/dndmode/d;->mz:Lcom/android/settings/dndmode/AlarmContentFragment;

    invoke-static {v0}, Lcom/android/settings/dndmode/AlarmContentFragment;->lY(Lcom/android/settings/dndmode/AlarmContentFragment;)Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/dndmode/d;->my:Landroid/net/Uri;

    const/4 v2, 0x0

    const/4 v3, -0x1

    invoke-virtual {v0, v1, v2, p0, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    return-void
.end method

.method public mb()V
    .locals 1

    iget-object v0, p0, Lcom/android/settings/dndmode/d;->mz:Lcom/android/settings/dndmode/AlarmContentFragment;

    invoke-static {v0}, Lcom/android/settings/dndmode/AlarmContentFragment;->lY(Lcom/android/settings/dndmode/AlarmContentFragment;)Landroid/content/ContentResolver;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    return-void
.end method

.method public onChange(ZLandroid/net/Uri;)V
    .locals 1

    invoke-super {p0, p1, p2}, Landroid/database/ContentObserver;->onChange(ZLandroid/net/Uri;)V

    iget-object v0, p0, Lcom/android/settings/dndmode/d;->mz:Lcom/android/settings/dndmode/AlarmContentFragment;

    invoke-static {v0}, Lcom/android/settings/dndmode/AlarmContentFragment;->lZ(Lcom/android/settings/dndmode/AlarmContentFragment;)V

    return-void
.end method
