.class public Lcom/android/settings/dndmode/DoNotDisturbModeFragment;
.super Lcom/android/settings/dndmode/MiuiPreferenceFragment;
.source "DoNotDisturbModeFragment.java"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceChangeListener;


# instance fields
.field private mk:Lmiui/app/Activity;

.field private ml:Landroid/preference/CheckBoxPreference;

.field private mm:Landroid/preference/PreferenceGroup;

.field private mn:Landroid/preference/PreferenceScreen;

.field private mo:Landroid/preference/CheckBoxPreference;

.field private final mp:Lmiui/provider/ExtraTelephony$QuietModeEnableListener;

.field private mq:Landroid/preference/CheckBoxPreference;

.field private mr:Landroid/preference/PreferenceScreen;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/android/settings/dndmode/MiuiPreferenceFragment;-><init>()V

    new-instance v0, Lcom/android/settings/dndmode/o;

    invoke-direct {v0, p0}, Lcom/android/settings/dndmode/o;-><init>(Lcom/android/settings/dndmode/DoNotDisturbModeFragment;)V

    iput-object v0, p0, Lcom/android/settings/dndmode/DoNotDisturbModeFragment;->mp:Lmiui/provider/ExtraTelephony$QuietModeEnableListener;

    return-void
.end method

.method private lP()I
    .locals 8

    const/4 v7, 0x0

    const/4 v6, 0x0

    :try_start_0
    iget-object v0, p0, Lcom/android/settings/dndmode/DoNotDisturbModeFragment;->mk:Lmiui/app/Activity;

    invoke-virtual {v0}, Lmiui/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lmiui/provider/ExtraTelephony$Phonelist;->CONTENT_URI:Landroid/net/Uri;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const-string/jumbo v3, "count(*)"

    const/4 v4, 0x0

    aput-object v3, v2, v4

    const-string/jumbo v3, "type=\'3\'"

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    if-eqz v1, :cond_1

    :try_start_1
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getInt(I)I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result v0

    if-eqz v1, :cond_0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_0
    return v0

    :cond_1
    if-eqz v1, :cond_2

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_2
    :goto_0
    return v7

    :catch_0
    move-exception v0

    move-object v1, v6

    :goto_1
    :try_start_2
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    if-eqz v1, :cond_2

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_0

    :catchall_0
    move-exception v0

    :goto_2
    if-eqz v6, :cond_3

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v0

    :catchall_1
    move-exception v0

    move-object v6, v1

    goto :goto_2

    :catch_1
    move-exception v0

    goto :goto_1
.end method

.method private lQ()V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/dndmode/DoNotDisturbModeFragment;->mo:Landroid/preference/CheckBoxPreference;

    iget-object v1, p0, Lcom/android/settings/dndmode/DoNotDisturbModeFragment;->mk:Lmiui/app/Activity;

    invoke-static {v1}, Landroid/provider/MiuiSettings$AntiSpam;->isQuietModeEnable(Landroid/content/Context;)Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    return-void
.end method

.method private lR()V
    .locals 6

    iget-object v0, p0, Lcom/android/settings/dndmode/DoNotDisturbModeFragment;->mk:Lmiui/app/Activity;

    iget-object v1, p0, Lcom/android/settings/dndmode/DoNotDisturbModeFragment;->mk:Lmiui/app/Activity;

    invoke-static {v1}, Landroid/provider/MiuiSettings$AntiSpam;->getStartTimeForQuietMode(Landroid/content/Context;)I

    move-result v1

    invoke-static {v0, v1}, Lcom/android/settings/dndmode/a;->lg(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/dndmode/DoNotDisturbModeFragment;->mk:Lmiui/app/Activity;

    iget-object v2, p0, Lcom/android/settings/dndmode/DoNotDisturbModeFragment;->mk:Lmiui/app/Activity;

    invoke-static {v2}, Landroid/provider/MiuiSettings$AntiSpam;->getEndTimeForQuietMode(Landroid/content/Context;)I

    move-result v2

    invoke-static {v1, v2}, Lcom/android/settings/dndmode/a;->lg(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/android/settings/dndmode/DoNotDisturbModeFragment;->mn:Landroid/preference/PreferenceScreen;

    invoke-virtual {p0}, Lcom/android/settings/dndmode/DoNotDisturbModeFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v0, v4, v5

    const/4 v0, 0x1

    aput-object v1, v4, v0

    const v0, 0x7f120608

    invoke-virtual {v3, v0, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/preference/PreferenceScreen;->setSummary(Ljava/lang/CharSequence;)V

    return-void
.end method

.method static synthetic lS(Lcom/android/settings/dndmode/DoNotDisturbModeFragment;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/dndmode/DoNotDisturbModeFragment;->lQ()V

    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Lcom/android/settings/dndmode/MiuiPreferenceFragment;->onCreate(Landroid/os/Bundle;)V

    const v0, 0x7f15004b

    invoke-virtual {p0, v0}, Lcom/android/settings/dndmode/DoNotDisturbModeFragment;->addPreferencesFromResource(I)V

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/android/settings/dndmode/DoNotDisturbModeFragment;->setHasOptionsMenu(Z)V

    invoke-virtual {p0}, Lcom/android/settings/dndmode/DoNotDisturbModeFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lmiui/app/Activity;

    iput-object v0, p0, Lcom/android/settings/dndmode/DoNotDisturbModeFragment;->mk:Lmiui/app/Activity;

    const-string/jumbo v0, "key_do_not_disturb_mode"

    invoke-virtual {p0, v0}, Lcom/android/settings/dndmode/DoNotDisturbModeFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    iput-object v0, p0, Lcom/android/settings/dndmode/DoNotDisturbModeFragment;->mo:Landroid/preference/CheckBoxPreference;

    iget-object v0, p0, Lcom/android/settings/dndmode/DoNotDisturbModeFragment;->mo:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, p0}, Landroid/preference/CheckBoxPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    const-string/jumbo v0, "key_auto_setting_group"

    invoke-virtual {p0, v0}, Lcom/android/settings/dndmode/DoNotDisturbModeFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceGroup;

    iput-object v0, p0, Lcom/android/settings/dndmode/DoNotDisturbModeFragment;->mm:Landroid/preference/PreferenceGroup;

    const-string/jumbo v0, "key_auto_button"

    invoke-virtual {p0, v0}, Lcom/android/settings/dndmode/DoNotDisturbModeFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    iput-object v0, p0, Lcom/android/settings/dndmode/DoNotDisturbModeFragment;->ml:Landroid/preference/CheckBoxPreference;

    iget-object v0, p0, Lcom/android/settings/dndmode/DoNotDisturbModeFragment;->ml:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, p0}, Landroid/preference/CheckBoxPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    const-string/jumbo v0, "key_auto_time_setting"

    invoke-virtual {p0, v0}, Lcom/android/settings/dndmode/DoNotDisturbModeFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceScreen;

    iput-object v0, p0, Lcom/android/settings/dndmode/DoNotDisturbModeFragment;->mn:Landroid/preference/PreferenceScreen;

    const-string/jumbo v0, "key_vip_call_setting"

    invoke-virtual {p0, v0}, Lcom/android/settings/dndmode/DoNotDisturbModeFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceScreen;

    iput-object v0, p0, Lcom/android/settings/dndmode/DoNotDisturbModeFragment;->mr:Landroid/preference/PreferenceScreen;

    iget-object v0, p0, Lcom/android/settings/dndmode/DoNotDisturbModeFragment;->ml:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, p0}, Landroid/preference/CheckBoxPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    const-string/jumbo v0, "key_repeated_call_button"

    invoke-virtual {p0, v0}, Lcom/android/settings/dndmode/DoNotDisturbModeFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    iput-object v0, p0, Lcom/android/settings/dndmode/DoNotDisturbModeFragment;->mq:Landroid/preference/CheckBoxPreference;

    iget-object v0, p0, Lcom/android/settings/dndmode/DoNotDisturbModeFragment;->mq:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, p0}, Landroid/preference/CheckBoxPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    iget-object v0, p0, Lcom/android/settings/dndmode/DoNotDisturbModeFragment;->mk:Lmiui/app/Activity;

    iget-object v1, p0, Lcom/android/settings/dndmode/DoNotDisturbModeFragment;->mp:Lmiui/provider/ExtraTelephony$QuietModeEnableListener;

    invoke-static {v0, v1}, Lmiui/provider/ExtraTelephony;->registerQuietModeEnableListener(Landroid/content/Context;Lmiui/provider/ExtraTelephony$QuietModeEnableListener;)V

    return-void
.end method

.method public onDestroy()V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/dndmode/DoNotDisturbModeFragment;->mk:Lmiui/app/Activity;

    iget-object v1, p0, Lcom/android/settings/dndmode/DoNotDisturbModeFragment;->mp:Lmiui/provider/ExtraTelephony$QuietModeEnableListener;

    invoke-static {v0, v1}, Lmiui/provider/ExtraTelephony;->unRegisterQuietModeEnableListener(Landroid/content/Context;Lmiui/provider/ExtraTelephony$QuietModeEnableListener;)V

    invoke-super {p0}, Lcom/android/settings/dndmode/MiuiPreferenceFragment;->onDestroy()V

    return-void
.end method

.method public onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 2

    check-cast p2, Ljava/lang/Boolean;

    iget-object v0, p0, Lcom/android/settings/dndmode/DoNotDisturbModeFragment;->mo:Landroid/preference/CheckBoxPreference;

    if-ne p1, v0, :cond_1

    iget-object v0, p0, Lcom/android/settings/dndmode/DoNotDisturbModeFragment;->mk:Lmiui/app/Activity;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-static {v0, v1}, Landroid/provider/MiuiSettings$AntiSpam;->setQuietMode(Landroid/content/Context;Z)V

    :cond_0
    :goto_0
    const/4 v0, 0x1

    return v0

    :cond_1
    iget-object v0, p0, Lcom/android/settings/dndmode/DoNotDisturbModeFragment;->ml:Landroid/preference/CheckBoxPreference;

    if-ne p1, v0, :cond_3

    iget-object v0, p0, Lcom/android/settings/dndmode/DoNotDisturbModeFragment;->mk:Lmiui/app/Activity;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-static {v0, v1}, Landroid/provider/MiuiSettings$AntiSpam;->setAutoTimerOfQuietMode(Landroid/content/Context;Z)V

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/settings/dndmode/DoNotDisturbModeFragment;->mm:Landroid/preference/PreferenceGroup;

    iget-object v1, p0, Lcom/android/settings/dndmode/DoNotDisturbModeFragment;->mn:Landroid/preference/PreferenceScreen;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceGroup;->addPreference(Landroid/preference/Preference;)Z

    iget-object v0, p0, Lcom/android/settings/dndmode/DoNotDisturbModeFragment;->mk:Lmiui/app/Activity;

    invoke-static {v0}, Lcom/android/settings/dndmode/a;->lc(Landroid/content/Context;)V

    invoke-direct {p0}, Lcom/android/settings/dndmode/DoNotDisturbModeFragment;->lR()V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/android/settings/dndmode/DoNotDisturbModeFragment;->mm:Landroid/preference/PreferenceGroup;

    iget-object v1, p0, Lcom/android/settings/dndmode/DoNotDisturbModeFragment;->mn:Landroid/preference/PreferenceScreen;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceGroup;->removePreference(Landroid/preference/Preference;)Z

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/android/settings/dndmode/DoNotDisturbModeFragment;->mq:Landroid/preference/CheckBoxPreference;

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/dndmode/DoNotDisturbModeFragment;->mk:Lmiui/app/Activity;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-static {v0, v1}, Landroid/provider/MiuiSettings$AntiSpam;->setRepeatedCallActionEnable(Landroid/content/Context;Z)V

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/android/settings/dndmode/DoNotDisturbModeFragment;->mq:Landroid/preference/CheckBoxPreference;

    const v1, 0x7f120614

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setSummary(I)V

    goto :goto_0

    :cond_4
    iget-object v0, p0, Lcom/android/settings/dndmode/DoNotDisturbModeFragment;->mq:Landroid/preference/CheckBoxPreference;

    const v1, 0x7f120615

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setSummary(I)V

    goto :goto_0
.end method

.method public onResume()V
    .locals 6

    const/4 v5, 0x1

    const/4 v4, 0x0

    invoke-super {p0}, Lcom/android/settings/dndmode/MiuiPreferenceFragment;->onResume()V

    iget-object v0, p0, Lcom/android/settings/dndmode/DoNotDisturbModeFragment;->mo:Landroid/preference/CheckBoxPreference;

    iget-object v1, p0, Lcom/android/settings/dndmode/DoNotDisturbModeFragment;->mk:Lmiui/app/Activity;

    invoke-static {v1}, Landroid/provider/MiuiSettings$AntiSpam;->isQuietModeEnable(Landroid/content/Context;)Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    iget-object v0, p0, Lcom/android/settings/dndmode/DoNotDisturbModeFragment;->mk:Lmiui/app/Activity;

    invoke-static {v0}, Landroid/provider/MiuiSettings$AntiSpam;->isVipCallActionEnable(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/android/settings/dndmode/DoNotDisturbModeFragment;->mr:Landroid/preference/PreferenceScreen;

    const v1, 0x7f120620

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->setSummary(I)V

    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/android/settings/dndmode/DoNotDisturbModeFragment;->mk:Lmiui/app/Activity;

    invoke-static {v0}, Landroid/provider/MiuiSettings$AntiSpam;->isAutoTimerOfQuietModeEnable(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/android/settings/dndmode/DoNotDisturbModeFragment;->ml:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, v5}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    iget-object v0, p0, Lcom/android/settings/dndmode/DoNotDisturbModeFragment;->mm:Landroid/preference/PreferenceGroup;

    iget-object v1, p0, Lcom/android/settings/dndmode/DoNotDisturbModeFragment;->mn:Landroid/preference/PreferenceScreen;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceGroup;->addPreference(Landroid/preference/Preference;)Z

    invoke-direct {p0}, Lcom/android/settings/dndmode/DoNotDisturbModeFragment;->lR()V

    :goto_1
    iget-object v0, p0, Lcom/android/settings/dndmode/DoNotDisturbModeFragment;->mk:Lmiui/app/Activity;

    invoke-static {v0}, Landroid/provider/MiuiSettings$AntiSpam;->isRepeatedCallActionEnable(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/android/settings/dndmode/DoNotDisturbModeFragment;->mq:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, v4}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    iget-object v0, p0, Lcom/android/settings/dndmode/DoNotDisturbModeFragment;->mq:Landroid/preference/CheckBoxPreference;

    const v1, 0x7f120615

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setSummary(I)V

    :goto_2
    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/settings/dndmode/DoNotDisturbModeFragment;->mk:Lmiui/app/Activity;

    invoke-static {v0}, Landroid/provider/MiuiSettings$AntiSpam;->getVipListForQuietMode(Landroid/content/Context;)I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_2

    iget-object v0, p0, Lcom/android/settings/dndmode/DoNotDisturbModeFragment;->mr:Landroid/preference/PreferenceScreen;

    invoke-virtual {p0}, Lcom/android/settings/dndmode/DoNotDisturbModeFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    new-array v2, v5, [Ljava/lang/Object;

    invoke-direct {p0}, Lcom/android/settings/dndmode/DoNotDisturbModeFragment;->lP()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    const v3, 0x7f12061f

    invoke-virtual {v1, v3, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->setSummary(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_2
    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/dndmode/DoNotDisturbModeFragment;->mr:Landroid/preference/PreferenceScreen;

    const v1, 0x7f12061e

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->setSummary(I)V

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/android/settings/dndmode/DoNotDisturbModeFragment;->ml:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, v4}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    iget-object v0, p0, Lcom/android/settings/dndmode/DoNotDisturbModeFragment;->mm:Landroid/preference/PreferenceGroup;

    iget-object v1, p0, Lcom/android/settings/dndmode/DoNotDisturbModeFragment;->mn:Landroid/preference/PreferenceScreen;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceGroup;->removePreference(Landroid/preference/Preference;)Z

    goto :goto_1

    :cond_4
    iget-object v0, p0, Lcom/android/settings/dndmode/DoNotDisturbModeFragment;->mq:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, v5}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    iget-object v0, p0, Lcom/android/settings/dndmode/DoNotDisturbModeFragment;->mq:Landroid/preference/CheckBoxPreference;

    const v1, 0x7f120614

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setSummary(I)V

    goto :goto_2
.end method
