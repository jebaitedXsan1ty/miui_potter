.class public abstract Lcom/android/settings/gestures/b;
.super Lcom/android/settings/core/e;
.source "GesturePreferenceController.java"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceChangeListener;
.implements Lcom/android/settings/core/lifecycle/b;
.implements Lcom/android/settings/core/lifecycle/a/c;
.implements Lcom/android/settings/core/lifecycle/a/j;


# instance fields
.field private aYR:Lcom/android/settings/widget/VideoPreference;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/android/settings/core/lifecycle/c;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/settings/core/e;-><init>(Landroid/content/Context;)V

    if-eqz p2, :cond_0

    invoke-virtual {p2, p0}, Lcom/android/settings/core/lifecycle/c;->ajv(Lcom/android/settings/core/lifecycle/b;)Lcom/android/settings/core/lifecycle/b;

    :cond_0
    return-void
.end method


# virtual methods
.method protected abstract aML()Ljava/lang/String;
.end method

.method protected abstract aMM()Z
.end method

.method public cz(Landroid/preference/Preference;)V
    .locals 2

    invoke-super {p0, p1}, Lcom/android/settings/core/e;->cz(Landroid/preference/Preference;)V

    invoke-virtual {p0}, Lcom/android/settings/gestures/b;->aMM()Z

    move-result v0

    if-eqz p1, :cond_0

    instance-of v1, p1, Landroid/preference/TwoStatePreference;

    if-eqz v1, :cond_1

    check-cast p1, Landroid/preference/TwoStatePreference;

    invoke-virtual {p1, v0}, Landroid/preference/TwoStatePreference;->setChecked(Z)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    if-eqz v0, :cond_2

    const v0, 0x7f1207c6

    :goto_1
    invoke-virtual {p1, v0}, Landroid/preference/Preference;->setSummary(I)V

    goto :goto_0

    :cond_2
    const v0, 0x7f1207c5

    goto :goto_1
.end method

.method public i(Landroid/preference/PreferenceScreen;)V
    .locals 1

    invoke-super {p0, p1}, Lcom/android/settings/core/e;->i(Landroid/preference/PreferenceScreen;)V

    invoke-virtual {p0}, Lcom/android/settings/gestures/b;->p()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/android/settings/gestures/b;->aML()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Lcom/android/settings/widget/VideoPreference;

    iput-object v0, p0, Lcom/android/settings/gestures/b;->aYR:Lcom/android/settings/widget/VideoPreference;

    :cond_0
    return-void
.end method

.method public onStart()V
    .locals 1

    iget-object v0, p0, Lcom/android/settings/gestures/b;->aYR:Lcom/android/settings/widget/VideoPreference;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/gestures/b;->aYR:Lcom/android/settings/widget/VideoPreference;

    invoke-virtual {v0}, Lcom/android/settings/widget/VideoPreference;->aAc()V

    :cond_0
    return-void
.end method

.method public onStop()V
    .locals 1

    iget-object v0, p0, Lcom/android/settings/gestures/b;->aYR:Lcom/android/settings/widget/VideoPreference;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/gestures/b;->aYR:Lcom/android/settings/widget/VideoPreference;

    invoke-virtual {v0}, Lcom/android/settings/widget/VideoPreference;->aAb()V

    :cond_0
    return-void
.end method
