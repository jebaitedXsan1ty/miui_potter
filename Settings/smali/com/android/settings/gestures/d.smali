.class public Lcom/android/settings/gestures/d;
.super Lcom/android/settings/gestures/b;
.source "AssistGesturePreferenceController.java"

# interfaces
.implements Lcom/android/settings/core/lifecycle/a/d;
.implements Lcom/android/settings/core/lifecycle/a/b;


# instance fields
.field private final aYY:Ljava/lang/String;

.field private final aYZ:Lcom/android/settings/gestures/f;

.field private aZa:Landroid/preference/Preference;

.field private aZb:Landroid/preference/PreferenceScreen;

.field private final aZc:Lcom/android/settings/gestures/e;

.field private aZd:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/android/settings/core/lifecycle/c;Ljava/lang/String;)V
    .locals 1

    invoke-direct {p0, p1, p2}, Lcom/android/settings/gestures/b;-><init>(Landroid/content/Context;Lcom/android/settings/core/lifecycle/c;)V

    invoke-static {p1}, Lcom/android/settings/overlay/a;->aIk(Landroid/content/Context;)Lcom/android/settings/overlay/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/settings/overlay/a;->aIq()Lcom/android/settings/gestures/f;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/gestures/d;->aYZ:Lcom/android/settings/gestures/f;

    new-instance v0, Lcom/android/settings/gestures/e;

    invoke-direct {v0, p0}, Lcom/android/settings/gestures/e;-><init>(Lcom/android/settings/gestures/d;)V

    iput-object v0, p0, Lcom/android/settings/gestures/d;->aZc:Lcom/android/settings/gestures/e;

    invoke-virtual {p0}, Lcom/android/settings/gestures/d;->p()Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/settings/gestures/d;->aZd:Z

    iput-object p3, p0, Lcom/android/settings/gestures/d;->aYY:Ljava/lang/String;

    return-void
.end method

.method private aMZ()V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/gestures/d;->aZa:Landroid/preference/Preference;

    if-nez v0, :cond_0

    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/android/settings/gestures/d;->p()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/settings/gestures/d;->aZb:Landroid/preference/PreferenceScreen;

    invoke-virtual {p0}, Lcom/android/settings/gestures/d;->l()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/android/settings/gestures/d;->aZb:Landroid/preference/PreferenceScreen;

    iget-object v1, p0, Lcom/android/settings/gestures/d;->aZa:Landroid/preference/Preference;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    :cond_1
    :goto_0
    return-void

    :cond_2
    iget-object v0, p0, Lcom/android/settings/gestures/d;->aZb:Landroid/preference/PreferenceScreen;

    iget-object v1, p0, Lcom/android/settings/gestures/d;->aZa:Landroid/preference/Preference;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    goto :goto_0
.end method

.method static synthetic aNa(Lcom/android/settings/gestures/d;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/settings/gestures/d;->aZd:Z

    return v0
.end method

.method static synthetic aNb(Lcom/android/settings/gestures/d;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/android/settings/gestures/d;->aZd:Z

    return p1
.end method

.method static synthetic aNc(Lcom/android/settings/gestures/d;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/gestures/d;->aMZ()V

    return-void
.end method


# virtual methods
.method protected aML()Ljava/lang/String;
    .locals 1

    const-string/jumbo v0, "gesture_assist_video"

    return-object v0
.end method

.method protected aMM()Z
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/android/settings/gestures/d;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string/jumbo v3, "assist_gesture_enabled"

    invoke-static {v2, v3, v0}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    if-eqz v2, :cond_0

    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method public i(Landroid/preference/PreferenceScreen;)V
    .locals 1

    iput-object p1, p0, Lcom/android/settings/gestures/d;->aZb:Landroid/preference/PreferenceScreen;

    invoke-virtual {p0}, Lcom/android/settings/gestures/d;->l()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/gestures/d;->aZa:Landroid/preference/Preference;

    invoke-super {p0, p1}, Lcom/android/settings/gestures/b;->i(Landroid/preference/PreferenceScreen;)V

    return-void
.end method

.method public l()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/gestures/d;->aYY:Ljava/lang/String;

    return-object v0
.end method

.method public onPause()V
    .locals 3

    iget-object v0, p0, Lcom/android/settings/gestures/d;->aZc:Lcom/android/settings/gestures/e;

    iget-object v1, p0, Lcom/android/settings/gestures/d;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/android/settings/gestures/e;->mT(Landroid/content/ContentResolver;Z)V

    return-void
.end method

.method public onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 4

    const/4 v1, 0x1

    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iget-object v2, p0, Lcom/android/settings/gestures/d;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string/jumbo v3, "assist_gesture_enabled"

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v2, v3, v0}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    return v1

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onResume()V
    .locals 3

    iget-object v0, p0, Lcom/android/settings/gestures/d;->aZc:Lcom/android/settings/gestures/e;

    iget-object v1, p0, Lcom/android/settings/gestures/d;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/android/settings/gestures/e;->mT(Landroid/content/ContentResolver;Z)V

    iget-boolean v0, p0, Lcom/android/settings/gestures/d;->aZd:Z

    invoke-virtual {p0}, Lcom/android/settings/gestures/d;->p()Z

    move-result v1

    if-eq v0, v1, :cond_0

    invoke-direct {p0}, Lcom/android/settings/gestures/d;->aMZ()V

    invoke-virtual {p0}, Lcom/android/settings/gestures/d;->p()Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/settings/gestures/d;->aZd:Z

    :cond_0
    return-void
.end method

.method public p()Z
    .locals 2

    iget-object v0, p0, Lcom/android/settings/gestures/d;->aYZ:Lcom/android/settings/gestures/f;

    iget-object v1, p0, Lcom/android/settings/gestures/d;->mContext:Landroid/content/Context;

    invoke-interface {v0, v1}, Lcom/android/settings/gestures/f;->aMP(Landroid/content/Context;)Z

    move-result v0

    return v0
.end method
