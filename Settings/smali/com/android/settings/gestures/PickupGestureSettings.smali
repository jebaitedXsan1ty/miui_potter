.class public Lcom/android/settings/gestures/PickupGestureSettings;
.super Lcom/android/settings/dashboard/MiuiDashboardFragment;
.source "PickupGestureSettings.java"


# static fields
.field public static final SEARCH_INDEX_DATA_PROVIDER:Lcom/android/settings/search/Indexable$SearchIndexProvider;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/android/settings/gestures/u;

    invoke-direct {v0}, Lcom/android/settings/gestures/u;-><init>()V

    sput-object v0, Lcom/android/settings/gestures/PickupGestureSettings;->SEARCH_INDEX_DATA_PROVIDER:Lcom/android/settings/search/Indexable$SearchIndexProvider;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/dashboard/MiuiDashboardFragment;-><init>()V

    return-void
.end method

.method private static aNf(Landroid/content/Context;Lcom/android/settings/core/lifecycle/c;)Ljava/util/List;
    .locals 7

    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    new-instance v0, Lcom/android/settings/gestures/i;

    new-instance v3, Lcom/android/internal/hardware/AmbientDisplayConfiguration;

    invoke-direct {v3, p0}, Lcom/android/internal/hardware/AmbientDisplayConfiguration;-><init>(Landroid/content/Context;)V

    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v4

    const-string/jumbo v5, "gesture_pick_up"

    move-object v1, p0

    move-object v2, p1

    invoke-direct/range {v0 .. v5}, Lcom/android/settings/gestures/i;-><init>(Landroid/content/Context;Lcom/android/settings/core/lifecycle/c;Lcom/android/internal/hardware/AmbientDisplayConfiguration;ILjava/lang/String;)V

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object v6
.end method

.method static synthetic aNg(Landroid/content/Context;Lcom/android/settings/core/lifecycle/c;)Ljava/util/List;
    .locals 1

    invoke-static {p0, p1}, Lcom/android/settings/gestures/PickupGestureSettings;->aNf(Landroid/content/Context;Lcom/android/settings/core/lifecycle/c;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method protected ar()Ljava/lang/String;
    .locals 1

    const-string/jumbo v0, "PickupGestureSettings"

    return-object v0
.end method

.method protected as()I
    .locals 1

    const v0, 0x7f1500a0

    return v0
.end method

.method public getMetricsCategory()I
    .locals 1

    const/16 v0, 0x2f1

    return v0
.end method

.method protected getPreferenceControllers(Landroid/content/Context;)Ljava/util/List;
    .locals 1

    invoke-virtual {p0}, Lcom/android/settings/gestures/PickupGestureSettings;->getLifecycle()Lcom/android/settings/core/lifecycle/c;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/android/settings/gestures/PickupGestureSettings;->aNf(Landroid/content/Context;Lcom/android/settings/core/lifecycle/c;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method
