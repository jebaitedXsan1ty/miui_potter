.class public Lcom/android/settings/gestures/i;
.super Lcom/android/settings/gestures/b;
.source "PickupGesturePreferenceController.java"


# instance fields
.field private final aZn:Lcom/android/internal/hardware/AmbientDisplayConfiguration;

.field private final aZo:Ljava/lang/String;

.field private final mUserId:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/android/settings/core/lifecycle/c;Lcom/android/internal/hardware/AmbientDisplayConfiguration;ILjava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/android/settings/gestures/b;-><init>(Landroid/content/Context;Lcom/android/settings/core/lifecycle/c;)V

    iput-object p3, p0, Lcom/android/settings/gestures/i;->aZn:Lcom/android/internal/hardware/AmbientDisplayConfiguration;

    iput p4, p0, Lcom/android/settings/gestures/i;->mUserId:I

    iput-object p5, p0, Lcom/android/settings/gestures/i;->aZo:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method protected aML()Ljava/lang/String;
    .locals 1

    const-string/jumbo v0, "gesture_pick_up_video"

    return-object v0
.end method

.method protected aMM()Z
    .locals 2

    iget-object v0, p0, Lcom/android/settings/gestures/i;->aZn:Lcom/android/internal/hardware/AmbientDisplayConfiguration;

    iget v1, p0, Lcom/android/settings/gestures/i;->mUserId:I

    invoke-virtual {v0, v1}, Lcom/android/internal/hardware/AmbientDisplayConfiguration;->pulseOnPickupEnabled(I)Z

    move-result v0

    return v0
.end method

.method public l()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/gestures/i;->aZo:Ljava/lang/String;

    return-object v0
.end method

.method public onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 4

    const/4 v1, 0x1

    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iget-object v2, p0, Lcom/android/settings/gestures/i;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string/jumbo v3, "doze_pulse_on_pick_up"

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v2, v3, v0}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    return v1

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public p()Z
    .locals 1

    iget-object v0, p0, Lcom/android/settings/gestures/i;->aZn:Lcom/android/internal/hardware/AmbientDisplayConfiguration;

    invoke-virtual {v0}, Lcom/android/internal/hardware/AmbientDisplayConfiguration;->pulseOnPickupAvailable()Z

    move-result v0

    return v0
.end method
