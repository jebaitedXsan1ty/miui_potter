.class public Lcom/android/settings/gestures/g;
.super Lcom/android/settings/core/e;
.source "AssistGestureSensitivityPreferenceController.java"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceChangeListener;
.implements Lcom/android/settings/core/lifecycle/b;
.implements Lcom/android/settings/core/lifecycle/a/d;
.implements Lcom/android/settings/core/lifecycle/a/b;


# instance fields
.field private final aZg:Lcom/android/settings/gestures/f;

.field private aZh:Lcom/android/settings/MiuiSeekBarPreference;

.field private aZi:Landroid/preference/PreferenceScreen;

.field private final aZj:Lcom/android/settings/gestures/h;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/android/settings/core/lifecycle/c;)V
    .locals 1

    invoke-direct {p0, p1}, Lcom/android/settings/core/e;-><init>(Landroid/content/Context;)V

    invoke-static {p1}, Lcom/android/settings/overlay/a;->aIk(Landroid/content/Context;)Lcom/android/settings/overlay/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/settings/overlay/a;->aIq()Lcom/android/settings/gestures/f;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/gestures/g;->aZg:Lcom/android/settings/gestures/f;

    new-instance v0, Lcom/android/settings/gestures/h;

    invoke-direct {v0, p0}, Lcom/android/settings/gestures/h;-><init>(Lcom/android/settings/gestures/g;)V

    iput-object v0, p0, Lcom/android/settings/gestures/g;->aZj:Lcom/android/settings/gestures/h;

    if-eqz p2, :cond_0

    invoke-virtual {p2, p0}, Lcom/android/settings/core/lifecycle/c;->ajv(Lcom/android/settings/core/lifecycle/b;)Lcom/android/settings/core/lifecycle/b;

    :cond_0
    return-void
.end method

.method private aNh()V
    .locals 3

    iget-object v0, p0, Lcom/android/settings/gestures/g;->aZh:Lcom/android/settings/MiuiSeekBarPreference;

    if-nez v0, :cond_0

    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/android/settings/gestures/g;->p()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/settings/gestures/g;->aZi:Landroid/preference/PreferenceScreen;

    invoke-virtual {p0}, Lcom/android/settings/gestures/g;->l()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/android/settings/gestures/g;->aZi:Landroid/preference/PreferenceScreen;

    iget-object v1, p0, Lcom/android/settings/gestures/g;->aZh:Lcom/android/settings/MiuiSeekBarPreference;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/android/settings/gestures/g;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v1, "assist_gesture_sensitivity"

    iget-object v2, p0, Lcom/android/settings/gestures/g;->aZh:Lcom/android/settings/MiuiSeekBarPreference;

    invoke-virtual {v2}, Lcom/android/settings/MiuiSeekBarPreference;->bzj()I

    move-result v2

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    iget-object v1, p0, Lcom/android/settings/gestures/g;->aZh:Lcom/android/settings/MiuiSeekBarPreference;

    invoke-virtual {v1, v0}, Lcom/android/settings/MiuiSeekBarPreference;->setProgress(I)V

    return-void

    :cond_2
    iget-object v0, p0, Lcom/android/settings/gestures/g;->aZi:Landroid/preference/PreferenceScreen;

    iget-object v1, p0, Lcom/android/settings/gestures/g;->aZh:Lcom/android/settings/MiuiSeekBarPreference;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    goto :goto_0
.end method

.method static synthetic aNi(Lcom/android/settings/gestures/g;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/gestures/g;->aNh()V

    return-void
.end method


# virtual methods
.method public cz(Landroid/preference/Preference;)V
    .locals 0

    invoke-super {p0, p1}, Lcom/android/settings/core/e;->cz(Landroid/preference/Preference;)V

    invoke-direct {p0}, Lcom/android/settings/gestures/g;->aNh()V

    return-void
.end method

.method public i(Landroid/preference/PreferenceScreen;)V
    .locals 1

    iput-object p1, p0, Lcom/android/settings/gestures/g;->aZi:Landroid/preference/PreferenceScreen;

    invoke-virtual {p0}, Lcom/android/settings/gestures/g;->l()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Lcom/android/settings/MiuiSeekBarPreference;

    iput-object v0, p0, Lcom/android/settings/gestures/g;->aZh:Lcom/android/settings/MiuiSeekBarPreference;

    invoke-super {p0, p1}, Lcom/android/settings/core/e;->i(Landroid/preference/PreferenceScreen;)V

    return-void
.end method

.method public l()Ljava/lang/String;
    .locals 1

    const-string/jumbo v0, "gesture_assist_sensitivity"

    return-object v0
.end method

.method public onPause()V
    .locals 3

    iget-object v0, p0, Lcom/android/settings/gestures/g;->aZj:Lcom/android/settings/gestures/h;

    iget-object v1, p0, Lcom/android/settings/gestures/g;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/android/settings/gestures/h;->aNj(Landroid/content/ContentResolver;Z)V

    return-void
.end method

.method public onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 3

    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iget-object v1, p0, Lcom/android/settings/gestures/g;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string/jumbo v2, "assist_gesture_sensitivity"

    invoke-static {v1, v2, v0}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    const/4 v0, 0x1

    return v0
.end method

.method public onResume()V
    .locals 3

    iget-object v0, p0, Lcom/android/settings/gestures/g;->aZj:Lcom/android/settings/gestures/h;

    iget-object v1, p0, Lcom/android/settings/gestures/g;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/android/settings/gestures/h;->aNj(Landroid/content/ContentResolver;Z)V

    invoke-direct {p0}, Lcom/android/settings/gestures/g;->aNh()V

    return-void
.end method

.method public p()Z
    .locals 3

    const/4 v2, 0x1

    iget-object v0, p0, Lcom/android/settings/gestures/g;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v1, "assist_gesture_enabled"

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    if-ne v0, v2, :cond_0

    iget-object v0, p0, Lcom/android/settings/gestures/g;->aZg:Lcom/android/settings/gestures/f;

    iget-object v1, p0, Lcom/android/settings/gestures/g;->mContext:Landroid/content/Context;

    invoke-interface {v0, v1}, Lcom/android/settings/gestures/f;->aMP(Landroid/content/Context;)Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
