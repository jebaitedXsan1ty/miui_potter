.class public Lcom/android/settings/bL;
.super Lcom/android/settingslib/drawer/a;
.source "SettingsActivity.java"

# interfaces
.implements Landroid/preference/PreferenceManager$OnPreferenceTreeClickListener;
.implements Landroid/preference/PreferenceFragment$OnPreferenceStartFragmentCallback;
.implements Lcom/android/settings/cS;
.implements Landroid/app/FragmentManager$OnBackStackChangedListener;


# static fields
.field static final SAVE_KEY_SHOW_HOME_AS_UP:Ljava/lang/String; = ":settings:show_home_as_up"

.field static final SAVE_KEY_SHOW_SEARCH:Ljava/lang/String; = ":settings:show_search"

.field private static final bTx:[Ljava/lang/String;


# instance fields
.field private bTA:Ljava/util/ArrayList;

.field private bTB:Landroid/view/ViewGroup;

.field private bTC:Landroid/content/ComponentName;

.field private bTD:Lcom/android/settings/dashboard/n;

.field private bTE:Landroid/content/SharedPreferences;

.field private bTF:Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;

.field private bTG:Lcom/android/settings/search/DynamicIndexableContentMonitor;

.field private bTH:Landroid/content/BroadcastReceiver;

.field private bTI:Ljava/lang/String;

.field private bTJ:Ljava/lang/CharSequence;

.field private bTK:I

.field private bTL:Z

.field private bTM:Z

.field private bTN:Landroid/widget/Button;

.field private bTO:Lcom/android/settings/SmqSettings;

.field private bTP:Lcom/android/settings/widget/SwitchBar;

.field private bTy:Landroid/content/BroadcastReceiver;

.field private bTz:Z

.field mDisplayHomeAsUpEnabled:Z

.field mDisplaySearch:Z

.field private mMetricsFeatureProvider:Lcom/android/settings/core/instrumentation/e;

.field private mSearchFeatureProvider:Lcom/android/settings/search2/SearchFeatureProvider;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const-string/jumbo v1, "android.settings.APPLICATION_DETAILS_SETTINGS"

    const/4 v2, 0x0

    aput-object v1, v0, v2

    sput-object v0, Lcom/android/settings/bL;->bTx:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/android/settingslib/drawer/a;-><init>()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/settings/bL;->bTz:Z

    new-instance v0, Lcom/android/settings/jq;

    invoke-direct {v0, p0}, Lcom/android/settings/jq;-><init>(Lcom/android/settings/bL;)V

    iput-object v0, p0, Lcom/android/settings/bL;->bTy:Landroid/content/BroadcastReceiver;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/settings/bL;->bTA:Ljava/util/ArrayList;

    return-void
.end method

.method private bMJ()V
    .locals 11

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/android/settings/bL;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    invoke-static {p0}, Landroid/os/UserManager;->get(Landroid/content/Context;)Landroid/os/UserManager;

    move-result-object v3

    invoke-virtual {v3}, Landroid/os/UserManager;->isAdminUser()Z

    move-result v4

    invoke-virtual {p0}, Lcom/android/settings/bL;->getPackageName()Ljava/lang/String;

    move-result-object v5

    iget-object v0, p0, Lcom/android/settings/bL;->bTO:Lcom/android/settings/SmqSettings;

    invoke-virtual {v0}, Lcom/android/settings/SmqSettings;->bRg()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Landroid/content/ComponentName;

    const-class v6, Lcom/android/settings/Settings$SMQQtiFeedbackActivity;

    invoke-virtual {v6}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v0, v5, v6}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v6, p0, Lcom/android/settings/bL;->bTO:Lcom/android/settings/SmqSettings;

    invoke-virtual {v6}, Lcom/android/settings/SmqSettings;->bRg()Z

    move-result v6

    invoke-direct {p0, v0, v6, v4}, Lcom/android/settings/bL;->bMR(Landroid/content/ComponentName;ZZ)V

    :cond_0
    new-instance v0, Landroid/content/ComponentName;

    const-class v6, Lcom/android/settings/Settings$WifiSettingsActivity;

    invoke-virtual {v6}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v0, v5, v6}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    const-string/jumbo v6, "android.hardware.wifi"

    invoke-virtual {v2, v6}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v6

    invoke-direct {p0, v0, v6, v4}, Lcom/android/settings/bL;->bMR(Landroid/content/ComponentName;ZZ)V

    new-instance v0, Landroid/content/ComponentName;

    const-class v6, Lcom/android/settings/Settings$BluetoothSettingsActivity;

    invoke-virtual {v6}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v0, v5, v6}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    const-string/jumbo v6, "android.hardware.bluetooth"

    invoke-virtual {v2, v6}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v6

    invoke-direct {p0, v0, v6, v4}, Lcom/android/settings/bL;->bMR(Landroid/content/ComponentName;ZZ)V

    new-instance v0, Landroid/content/ComponentName;

    const-class v6, Lcom/android/settings/Settings$DataUsageSummaryActivity;

    invoke-virtual {v6}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v0, v5, v6}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {}, Lcom/android/settings/aq;->brz()Z

    move-result v6

    invoke-direct {p0, v0, v6, v4}, Lcom/android/settings/bL;->bMR(Landroid/content/ComponentName;ZZ)V

    new-instance v0, Landroid/content/ComponentName;

    const-class v6, Lcom/android/settings/Settings$SimSettingsActivity;

    invoke-virtual {v6}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v0, v5, v6}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {p0}, Lcom/android/settings/aq;->brJ(Landroid/content/Context;)Z

    move-result v6

    invoke-direct {p0, v0, v6, v4}, Lcom/android/settings/bL;->bMR(Landroid/content/ComponentName;ZZ)V

    new-instance v0, Landroid/content/ComponentName;

    const-class v6, Lcom/android/settings/Settings$PowerUsageSummaryActivity;

    invoke-virtual {v6}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v0, v5, v6}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iget-boolean v6, p0, Lcom/android/settings/bL;->bTz:Z

    invoke-direct {p0, v0, v6, v4}, Lcom/android/settings/bL;->bMR(Landroid/content/ComponentName;ZZ)V

    new-instance v6, Landroid/content/ComponentName;

    const-class v0, Lcom/android/settings/Settings$UserSettingsActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v6, v5, v0}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {}, Landroid/os/UserManager;->supportsMultipleUsers()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-static {}, Lcom/android/settings/aq;->bqE()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    :goto_0
    invoke-direct {p0, v6, v0, v4}, Lcom/android/settings/bL;->bMR(Landroid/content/ComponentName;ZZ)V

    new-instance v0, Landroid/content/ComponentName;

    const-class v6, Lcom/android/settings/Settings$NetworkDashboardActivity;

    invoke-virtual {v6}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v0, v5, v6}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {p0}, Landroid/os/UserManager;->isDeviceInDemoMode(Landroid/content/Context;)Z

    move-result v6

    xor-int/lit8 v6, v6, 0x1

    invoke-direct {p0, v0, v6, v4}, Lcom/android/settings/bL;->bMR(Landroid/content/ComponentName;ZZ)V

    new-instance v0, Landroid/content/ComponentName;

    const-class v6, Lcom/android/settings/Settings$ConnectedDeviceDashboardActivity;

    invoke-virtual {v6}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v0, v5, v6}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {p0}, Landroid/os/UserManager;->isDeviceInDemoMode(Landroid/content/Context;)Z

    move-result v6

    xor-int/lit8 v6, v6, 0x1

    invoke-direct {p0, v0, v6, v4}, Lcom/android/settings/bL;->bMR(Landroid/content/ComponentName;ZZ)V

    new-instance v0, Landroid/content/ComponentName;

    const-class v6, Lcom/android/settings/Settings$DateTimeSettingsActivity;

    invoke-virtual {v6}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v0, v5, v6}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {p0}, Landroid/os/UserManager;->isDeviceInDemoMode(Landroid/content/Context;)Z

    move-result v6

    xor-int/lit8 v6, v6, 0x1

    invoke-direct {p0, v0, v6, v4}, Lcom/android/settings/bL;->bMR(Landroid/content/ComponentName;ZZ)V

    invoke-static {p0}, Landroid/nfc/NfcAdapter;->getDefaultAdapter(Landroid/content/Context;)Landroid/nfc/NfcAdapter;

    move-result-object v0

    new-instance v6, Landroid/content/ComponentName;

    const-class v7, Lcom/android/settings/Settings$PaymentSettingsActivity;

    invoke-virtual {v7}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v5, v7}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    const-string/jumbo v7, "android.hardware.nfc"

    invoke-virtual {v2, v7}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_4

    const-string/jumbo v7, "android.hardware.nfc.hce"

    invoke-virtual {v2, v7}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_4

    if-eqz v0, :cond_4

    invoke-virtual {v0}, Landroid/nfc/NfcAdapter;->isEnabled()Z

    move-result v0

    :goto_1
    invoke-direct {p0, v6, v0, v4}, Lcom/android/settings/bL;->bMR(Landroid/content/ComponentName;ZZ)V

    new-instance v0, Landroid/content/ComponentName;

    const-class v6, Lcom/android/settings/Settings$PrintSettingsActivity;

    invoke-virtual {v6}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v0, v5, v6}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    const-string/jumbo v6, "android.software.print"

    invoke-virtual {v2, v6}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v2

    invoke-direct {p0, v0, v2, v4}, Lcom/android/settings/bL;->bMR(Landroid/content/ComponentName;ZZ)V

    iget-object v0, p0, Lcom/android/settings/bL;->bTE:Landroid/content/SharedPreferences;

    const-string/jumbo v2, "show"

    sget-object v6, Landroid/os/Build;->TYPE:Ljava/lang/String;

    const-string/jumbo v7, "eng"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    invoke-interface {v0, v2, v6}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_5

    const-string/jumbo v0, "no_debugging_features"

    invoke-virtual {v3, v0}, Landroid/os/UserManager;->hasUserRestriction(Ljava/lang/String;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    :goto_2
    new-instance v2, Landroid/content/ComponentName;

    const-class v3, Lcom/android/settings/Settings$DevelopmentSettingsActivity;

    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v5, v3}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0, v2, v0, v4}, Lcom/android/settings/bL;->bMR(Landroid/content/ComponentName;ZZ)V

    new-instance v0, Landroid/content/ComponentName;

    const-class v2, Lcom/android/settings/backup/BackupSettingsActivity;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v5, v2}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v2, 0x1

    invoke-direct {p0, v0, v2, v4}, Lcom/android/settings/bL;->bMR(Landroid/content/ComponentName;ZZ)V

    new-instance v0, Landroid/content/ComponentName;

    const-class v2, Lcom/android/settings/Settings$WifiDisplaySettingsActivity;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v5, v2}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {p0}, Lcom/android/settings/wfd/WifiDisplaySettings;->bbj(Landroid/content/Context;)Z

    move-result v2

    invoke-direct {p0, v0, v2, v4}, Lcom/android/settings/bL;->bMR(Landroid/content/ComponentName;ZZ)V

    xor-int/lit8 v0, v4, 0x1

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/android/settings/bL;->bTD:Lcom/android/settings/dashboard/n;

    invoke-interface {v0}, Lcom/android/settings/dashboard/n;->Da()Ljava/util/List;

    move-result-object v3

    monitor-enter v3

    :try_start_0
    invoke-interface {v3}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settingslib/drawer/DashboardCategory;

    invoke-virtual {v0}, Lcom/android/settingslib/drawer/DashboardCategory;->cek()I

    move-result v7

    move v2, v1

    :goto_3
    if-ge v2, v7, :cond_1

    invoke-virtual {v0, v2}, Lcom/android/settingslib/drawer/DashboardCategory;->cei(I)Lcom/android/settingslib/drawer/Tile;

    move-result-object v8

    iget-object v8, v8, Lcom/android/settingslib/drawer/Tile;->intent:Landroid/content/Intent;

    invoke-virtual {v8}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v8

    invoke-virtual {v8}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v9

    sget-object v10, Lcom/android/settings/core/a/a;->avH:[Ljava/lang/String;

    invoke-static {v10, v9}, Lcom/android/internal/util/ArrayUtils;->contains([Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v9

    invoke-virtual {v8}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v5, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_2

    xor-int/lit8 v9, v9, 0x1

    if-eqz v9, :cond_2

    const/4 v9, 0x0

    invoke-direct {p0, v8, v9, v4}, Lcom/android/settings/bL;->bMR(Landroid/content/ComponentName;ZZ)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    :cond_3
    move v0, v1

    goto/16 :goto_0

    :cond_4
    move v0, v1

    goto/16 :goto_1

    :cond_5
    move v0, v1

    goto/16 :goto_2

    :cond_6
    monitor-exit v3

    :cond_7
    invoke-virtual {p0}, Lcom/android/settings/bL;->cdO()V

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v3

    throw v0
.end method

.method private bMK()V
    .locals 3

    :try_start_0
    invoke-virtual {p0}, Lcom/android/settings/bL;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    invoke-virtual {p0}, Lcom/android/settings/bL;->getComponentName()Landroid/content/ComponentName;

    move-result-object v1

    const/16 v2, 0x80

    invoke-virtual {v0, v1, v2}, Landroid/content/pm/PackageManager;->getActivityInfo(Landroid/content/ComponentName;I)Landroid/content/pm/ActivityInfo;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, v0, Landroid/content/pm/ActivityInfo;->metaData:Landroid/os/Bundle;

    if-nez v1, :cond_1

    :cond_0
    return-void

    :cond_1
    iget-object v0, v0, Landroid/content/pm/ActivityInfo;->metaData:Landroid/os/Bundle;

    const-string/jumbo v1, "com.android.settings.FRAGMENT_CLASS"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/bL;->bTI:Ljava/lang/String;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string/jumbo v0, "Settings"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Cannot get Metadata for: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/android/settings/bL;->getComponentName()Landroid/content/ComponentName;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/ComponentName;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private bML()Ljava/lang/String;
    .locals 3

    invoke-virtual {p0}, Lcom/android/settings/bL;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lcom/android/settings/bL;->getIntent()Landroid/content/Intent;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/android/settings/bL;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string/jumbo v2, ":settings:show_fragment"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/android/settings/bL;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, ":settings:show_fragment"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :cond_0
    const-string/jumbo v1, "com.android.settings."

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    const-string/jumbo v1, "com.android.settings."

    const-string/jumbo v2, ""

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    :cond_1
    return-object v0
.end method

.method private bMM(Landroid/content/Intent;)Ljava/lang/String;
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/android/settings/bL;->bTI:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/bL;->bTI:Ljava/lang/String;

    return-object v0

    :cond_0
    invoke-virtual {p1}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lcom/android/settings/bL;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    return-object v2

    :cond_1
    const-string/jumbo v1, "com.android.settings.ManageApplications"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    const-string/jumbo v1, "com.android.settings.RunningServices"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    const-string/jumbo v1, "com.android.settings.applications.StorageUse"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    :cond_2
    const-class v0, Lcom/android/settings/applications/ManageApplications;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    :cond_3
    const-string/jumbo v1, "com.android.settings.wifi.WifiSettings"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    const-class v0, Lcom/android/settings/wifi/MiuiWifiSettings;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    :cond_4
    return-object v0
.end method

.method private static bMO(Landroid/content/Intent;)Z
    .locals 4

    const/4 v1, 0x0

    invoke-virtual {p0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_0

    return v1

    :cond_0
    move v0, v1

    :goto_0
    sget-object v3, Lcom/android/settings/bL;->bTx:[Ljava/lang/String;

    array-length v3, v3

    if-ge v0, v3, :cond_2

    sget-object v3, Lcom/android/settings/bL;->bTx:[Ljava/lang/String;

    aget-object v3, v3, v0

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    const/4 v0, 0x1

    return v0

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    return v1
.end method

.method private static bMP(Landroid/content/Intent;)Z
    .locals 2

    invoke-virtual {p0}, Landroid/content/Intent;->getCategories()Ljava/util/Set;

    move-result-object v0

    if-eqz v0, :cond_0

    const-string/jumbo v1, "com.android.settings.SHORTCUT"

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private bMQ()V
    .locals 9

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/android/settings/bL;->getIntent()Landroid/content/Intent;

    move-result-object v4

    const-string/jumbo v0, "extra_show_on_finddevice_keyguard"

    invoke-virtual {v4, v0, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_2

    const/4 v3, 0x2

    new-array v5, v3, [Landroid/os/Bundle;

    const-string/jumbo v3, ":android:show_fragment_args"

    invoke-virtual {v4, v3}, Landroid/content/Intent;->getBundleExtra(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v3

    aput-object v3, v5, v2

    const-string/jumbo v3, ":settings:show_fragment_args"

    invoke-virtual {v4, v3}, Landroid/content/Intent;->getBundleExtra(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v3

    aput-object v3, v5, v1

    array-length v6, v5

    move v3, v2

    :goto_0
    if-ge v3, v6, :cond_2

    aget-object v7, v5, v3

    if-nez v7, :cond_1

    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_1
    const-string/jumbo v8, "extra_show_on_finddevice_keyguard"

    invoke-virtual {v7, v8, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v7

    if-eqz v7, :cond_0

    move v0, v1

    :cond_2
    invoke-virtual {v4}, Landroid/content/Intent;->getFlags()I

    move-result v3

    const/high16 v4, 0x100000

    and-int/2addr v3, v4

    if-eqz v3, :cond_4

    :goto_1
    if-eqz v0, :cond_3

    if-eqz v1, :cond_5

    :cond_3
    return-void

    :cond_4
    move v1, v2

    goto :goto_1

    :cond_5
    invoke-virtual {p0}, Lcom/android/settings/bL;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v0

    new-instance v1, Landroid/view/WindowManager$LayoutParams;

    invoke-direct {v1}, Landroid/view/WindowManager$LayoutParams;-><init>()V

    invoke-virtual {v1, v0}, Landroid/view/WindowManager$LayoutParams;->copyFrom(Landroid/view/WindowManager$LayoutParams;)I

    iget v2, v1, Landroid/view/WindowManager$LayoutParams;->flags:I

    const/high16 v3, 0x80000

    or-int/2addr v2, v3

    iput v2, v1, Landroid/view/WindowManager$LayoutParams;->flags:I

    iget v2, v1, Landroid/view/WindowManager$LayoutParams;->extraFlags:I

    or-int/lit16 v2, v2, 0x1000

    iput v2, v1, Landroid/view/WindowManager$LayoutParams;->extraFlags:I

    invoke-virtual {p0}, Lcom/android/settings/bL;->getWindow()Landroid/view/Window;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    new-instance v1, Lcom/android/settings/jw;

    invoke-direct {v1, p0, v0}, Lcom/android/settings/jw;-><init>(Lcom/android/settings/bL;Landroid/view/WindowManager$LayoutParams;)V

    iput-object v1, p0, Lcom/android/settings/bL;->bTH:Landroid/content/BroadcastReceiver;

    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    const-string/jumbo v1, "com.xiaomi.finddevice.action.LAST_STATUS_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/android/settings/bL;->bTH:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1, v0}, Lcom/android/settings/bL;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    return-void
.end method

.method private bMR(Landroid/content/ComponentName;ZZ)V
    .locals 2

    xor-int/lit8 v0, p3, 0x1

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/android/settings/bL;->getPackageName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/android/settings/core/a/a;->avH:[Ljava/lang/String;

    invoke-virtual {p1}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/internal/util/ArrayUtils;->contains([Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    const/4 p2, 0x0

    :cond_0
    invoke-virtual {p0, p1, p2}, Lcom/android/settings/bL;->cdP(Landroid/content/ComponentName;Z)V

    return-void
.end method

.method private bMS()V
    .locals 2

    invoke-virtual {p0}, Lcom/android/settings/bL;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentManager;->getBackStackEntryCount()I

    move-result v0

    if-nez v0, :cond_1

    iget v0, p0, Lcom/android/settings/bL;->bTK:I

    if-lez v0, :cond_0

    iget v0, p0, Lcom/android/settings/bL;->bTK:I

    invoke-virtual {p0, v0}, Lcom/android/settings/bL;->setTitle(I)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/settings/bL;->bTJ:Ljava/lang/CharSequence;

    invoke-virtual {p0, v0}, Lcom/android/settings/bL;->setTitle(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lcom/android/settings/bL;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {v1, v0}, Landroid/app/FragmentManager;->getBackStackEntryAt(I)Landroid/app/FragmentManager$BackStackEntry;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/android/settings/bL;->bMT(Landroid/app/FragmentManager$BackStackEntry;)V

    return-void
.end method

.method private bMT(Landroid/app/FragmentManager$BackStackEntry;)V
    .locals 1

    invoke-interface {p1}, Landroid/app/FragmentManager$BackStackEntry;->getBreadCrumbTitleRes()I

    move-result v0

    if-lez v0, :cond_1

    invoke-virtual {p0, v0}, Lcom/android/settings/bL;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    :goto_0
    if-eqz v0, :cond_0

    invoke-virtual {p0, v0}, Lcom/android/settings/bL;->setTitle(Ljava/lang/CharSequence;)V

    :cond_0
    return-void

    :cond_1
    invoke-interface {p1}, Landroid/app/FragmentManager$BackStackEntry;->getBreadCrumbTitle()Ljava/lang/CharSequence;

    move-result-object v0

    goto :goto_0
.end method

.method private bMU(Landroid/content/Intent;)V
    .locals 4

    const/4 v2, 0x0

    const/4 v1, -0x1

    const-string/jumbo v0, ":settings:show_fragment_title_resid"

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    if-lez v0, :cond_1

    iput-object v2, p0, Lcom/android/settings/bL;->bTJ:Ljava/lang/CharSequence;

    iput v0, p0, Lcom/android/settings/bL;->bTK:I

    const-string/jumbo v0, ":settings:show_fragment_title_res_package_name"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    :try_start_0
    new-instance v1, Landroid/os/UserHandle;

    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v2

    invoke-direct {v1, v2}, Landroid/os/UserHandle;-><init>(I)V

    const/4 v2, 0x0

    invoke-virtual {p0, v0, v2, v1}, Lcom/android/settings/bL;->createPackageContextAsUser(Ljava/lang/String;ILandroid/os/UserHandle;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    iget v2, p0, Lcom/android/settings/bL;->bTK:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    iput-object v1, p0, Lcom/android/settings/bL;->bTJ:Ljava/lang/CharSequence;

    iget-object v1, p0, Lcom/android/settings/bL;->bTJ:Ljava/lang/CharSequence;

    invoke-virtual {p0, v1}, Lcom/android/settings/bL;->setTitle(Ljava/lang/CharSequence;)V

    const/4 v1, -0x1

    iput v1, p0, Lcom/android/settings/bL;->bTK:I
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception v1

    const-string/jumbo v1, "Settings"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "Could not find package"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_0
    iget v0, p0, Lcom/android/settings/bL;->bTK:I

    invoke-virtual {p0, v0}, Lcom/android/settings/bL;->setTitle(I)V

    goto :goto_0

    :cond_1
    iput v1, p0, Lcom/android/settings/bL;->bTK:I

    const-string/jumbo v0, ":settings:show_fragment_title"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    :goto_1
    iput-object v0, p0, Lcom/android/settings/bL;->bTJ:Ljava/lang/CharSequence;

    iget-object v0, p0, Lcom/android/settings/bL;->bTJ:Ljava/lang/CharSequence;

    invoke-virtual {p0, v0}, Lcom/android/settings/bL;->setTitle(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Lcom/android/settings/bL;->getTitle()Ljava/lang/CharSequence;

    move-result-object v0

    goto :goto_1
.end method

.method private bMV(Ljava/lang/String;Landroid/os/Bundle;ZZILjava/lang/CharSequence;Z)Landroid/app/Fragment;
    .locals 4

    const v1, 0x7f120da6

    const/4 v3, 0x0

    const/4 v2, 0x0

    if-nez p1, :cond_0

    invoke-virtual {p0}, Lcom/android/settings/bL;->finish()V

    return-object v2

    :cond_0
    invoke-virtual {p0, v1}, Lcom/android/settings/bL;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0, v1}, Lcom/android/settings/bL;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string/jumbo v1, "android.intent.category.DEFAULT"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Lcom/android/settings/bL;->startActivity(Landroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/android/settings/bL;->finish()V

    return-object v2

    :cond_1
    const-class v0, Lcom/android/settings/sim/SimSettings;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    const-string/jumbo v0, "Settings"

    const-string/jumbo v1, "switchToFragment, launch simSettings  "

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v1, "com.android.settings.sim.SIM_SUB_INFO_SETTINGS"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/android/settings/bL;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    invoke-virtual {v1, v0, v3}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_2

    invoke-virtual {p0, v0}, Lcom/android/settings/bL;->startActivity(Landroid/content/Intent;)V

    :cond_2
    invoke-virtual {p0}, Lcom/android/settings/bL;->finish()V

    return-object v2

    :cond_3
    if-eqz p3, :cond_4

    invoke-virtual {p0, p1}, Lcom/android/settings/bL;->isValidFragment(Ljava/lang/String;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_4

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Invalid fragment for this activity: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_4
    :try_start_0
    invoke-static {p1}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    if-nez p2, :cond_5

    new-instance p2, Landroid/os/Bundle;

    invoke-direct {p2}, Landroid/os/Bundle;-><init>()V

    :cond_5
    const-string/jumbo v0, ":android:show_fragment_title"

    invoke-virtual {p2, v0, p5}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    invoke-static {p0, p1, p2}, Landroid/app/Fragment;->instantiate(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;)Landroid/app/Fragment;

    move-result-object v0

    invoke-virtual {p0}, Lcom/android/settings/bL;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v1

    const v2, 0x7f0a0284

    invoke-virtual {v1, v2, v0}, Landroid/app/FragmentTransaction;->replace(ILandroid/app/Fragment;)Landroid/app/FragmentTransaction;

    if-eqz p7, :cond_6

    iget-object v2, p0, Lcom/android/settings/bL;->bTB:Landroid/view/ViewGroup;

    invoke-static {v2}, Landroid/transition/TransitionManager;->beginDelayedTransition(Landroid/view/ViewGroup;)V

    :cond_6
    if-eqz p4, :cond_7

    const-string/jumbo v2, ":settings:prefs"

    invoke-virtual {v1, v2}, Landroid/app/FragmentTransaction;->addToBackStack(Ljava/lang/String;)Landroid/app/FragmentTransaction;

    :cond_7
    if-lez p5, :cond_9

    invoke-virtual {v1, p5}, Landroid/app/FragmentTransaction;->setBreadCrumbTitle(I)Landroid/app/FragmentTransaction;

    :cond_8
    :goto_0
    invoke-virtual {v1}, Landroid/app/FragmentTransaction;->commitAllowingStateLoss()I

    invoke-virtual {p0}, Lcom/android/settings/bL;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/FragmentManager;->executePendingTransactions()Z

    return-object v0

    :catch_0
    move-exception v0

    return-object v2

    :cond_9
    if-eqz p6, :cond_8

    invoke-virtual {v1, p6}, Landroid/app/FragmentTransaction;->setBreadCrumbTitle(Ljava/lang/CharSequence;)Landroid/app/FragmentTransaction;

    goto :goto_0
.end method

.method private bMW()V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/android/settings/bL;->bTH:Landroid/content/BroadcastReceiver;

    if-nez v0, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/settings/bL;->bTH:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/android/settings/bL;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    iput-object v1, p0, Lcom/android/settings/bL;->bTH:Landroid/content/BroadcastReceiver;

    return-void
.end method

.method private bMX()V
    .locals 2

    sget-object v0, Lcom/android/settings/bL;->czg:Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lcom/android/settings/jv;

    invoke-direct {v1, p0}, Lcom/android/settings/jv;-><init>(Lcom/android/settings/bL;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method static synthetic bMY(Lcom/android/settings/bL;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/settings/bL;->bTz:Z

    return v0
.end method

.method static synthetic bMZ(Lcom/android/settings/bL;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/android/settings/bL;->bTz:Z

    return p1
.end method

.method static synthetic bNa(Lcom/android/settings/bL;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/bL;->bMJ()V

    return-void
.end method

.method static synthetic bNb(Lcom/android/settings/bL;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/bL;->bMX()V

    return-void
.end method


# virtual methods
.method public Uv(Landroid/app/Fragment;Ljava/lang/String;Landroid/os/Bundle;ILjava/lang/CharSequence;Landroid/app/Fragment;I)V
    .locals 9

    const/4 v6, 0x0

    if-gez p4, :cond_0

    if-eqz p5, :cond_1

    invoke-interface {p5}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v6

    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/android/settings/bL;->mMetricsFeatureProvider:Lcom/android/settings/core/instrumentation/e;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/settings/bL;->mMetricsFeatureProvider:Lcom/android/settings/core/instrumentation/e;

    invoke-virtual {v0, p1}, Lcom/android/settings/core/instrumentation/e;->ajT(Ljava/lang/Object;)I

    move-result v8

    :goto_1
    iget-boolean v7, p0, Lcom/android/settings/bL;->bTL:Z

    move-object v0, p0

    move-object v1, p2

    move-object v2, p3

    move-object v3, p6

    move/from16 v4, p7

    move v5, p4

    invoke-static/range {v0 .. v8}, Lcom/android/settings/aq;->brK(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;Landroid/app/Fragment;IILjava/lang/CharSequence;ZI)V

    return-void

    :cond_1
    const-string/jumbo v6, ""

    goto :goto_0

    :cond_2
    const/4 v8, 0x0

    goto :goto_1
.end method

.method public bME(Landroid/app/Fragment;ILandroid/content/Intent;)V
    .locals 0

    invoke-virtual {p0, p2, p3}, Lcom/android/settings/bL;->setResult(ILandroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/android/settings/bL;->finish()V

    return-void
.end method

.method public bMF()Lcom/android/settings/widget/SwitchBar;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/bL;->bTP:Lcom/android/settings/widget/SwitchBar;

    return-object v0
.end method

.method public bMG(Landroid/app/Fragment;Ljava/lang/String;Landroid/os/Bundle;ILjava/lang/CharSequence;Landroid/os/UserHandle;)V
    .locals 8

    const/4 v7, 0x0

    const/4 v6, 0x0

    invoke-virtual {p6}, Landroid/os/UserHandle;->getIdentifier()I

    move-result v0

    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v1

    if-ne v0, v1, :cond_0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    move-object v5, p5

    invoke-virtual/range {v0 .. v7}, Lcom/android/settings/bL;->Uv(Landroid/app/Fragment;Ljava/lang/String;Landroid/os/Bundle;ILjava/lang/CharSequence;Landroid/app/Fragment;I)V

    :goto_0
    return-void

    :cond_0
    if-gez p4, :cond_3

    if-eqz p5, :cond_1

    invoke-interface {p5}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v4

    :goto_1
    iget-object v0, p0, Lcom/android/settings/bL;->mMetricsFeatureProvider:Lcom/android/settings/core/instrumentation/e;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/settings/bL;->mMetricsFeatureProvider:Lcom/android/settings/core/instrumentation/e;

    invoke-virtual {v0, p1}, Lcom/android/settings/core/instrumentation/e;->ajT(Ljava/lang/Object;)I

    move-result v6

    :goto_2
    iget-boolean v5, p0, Lcom/android/settings/bL;->bTL:Z

    move-object v0, p0

    move-object v1, p2

    move-object v2, p3

    move v3, p4

    move-object v7, p6

    invoke-static/range {v0 .. v7}, Lcom/android/settings/aq;->brM(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;ILjava/lang/CharSequence;ZILandroid/os/UserHandle;)V

    goto :goto_0

    :cond_1
    const-string/jumbo v4, ""

    goto :goto_1

    :cond_2
    move v6, v7

    goto :goto_2

    :cond_3
    move-object v4, v6

    goto :goto_1
.end method

.method public bMH(Landroid/app/Fragment;Z)V
    .locals 2

    invoke-virtual {p0}, Lcom/android/settings/bL;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v0

    const v1, 0x7f0a0284

    invoke-virtual {v0, v1, p1}, Landroid/app/FragmentTransaction;->replace(ILandroid/app/Fragment;)Landroid/app/FragmentTransaction;

    if-eqz p2, :cond_0

    const/16 v1, 0x1001

    invoke-virtual {v0, v1}, Landroid/app/FragmentTransaction;->setTransition(I)Landroid/app/FragmentTransaction;

    const-string/jumbo v1, ":settings:prefs"

    invoke-virtual {v0, v1}, Landroid/app/FragmentTransaction;->addToBackStack(Ljava/lang/String;)Landroid/app/FragmentTransaction;

    :goto_0
    invoke-virtual {v0}, Landroid/app/FragmentTransaction;->commitAllowingStateLoss()I

    return-void

    :cond_0
    const/16 v1, 0x1003

    invoke-virtual {v0, v1}, Landroid/app/FragmentTransaction;->setTransition(I)Landroid/app/FragmentTransaction;

    goto :goto_0
.end method

.method public bMI(Landroid/content/Intent;)V
    .locals 2

    if-eqz p1, :cond_0

    invoke-static {}, Landroid/app/ActivityManager;->isUserAMonkey()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/android/settings/bL;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    return-void

    :cond_1
    invoke-virtual {p1}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/bL;->bTC:Landroid/content/ComponentName;

    const/16 v0, 0x2a

    invoke-virtual {p0, p1, v0}, Lcom/android/settings/bL;->startActivityForResult(Landroid/content/Intent;I)V

    return-void
.end method

.method public bMN()Z
    .locals 1

    iget-object v0, p0, Lcom/android/settings/bL;->bTN:Landroid/widget/Button;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method getBitmapFromXmlResource(I)Landroid/graphics/Bitmap;
    .locals 6

    const/4 v5, 0x0

    invoke-virtual {p0}, Lcom/android/settings/bL;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {p0}, Lcom/android/settings/bL;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Landroid/content/res/Resources;->getDrawable(ILandroid/content/res/Resources$Theme;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-static {v0}, Lmiui/content/res/IconCustomizer;->generateIconStyleDrawable(Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/BitmapDrawable;

    move-result-object v0

    new-instance v1, Landroid/graphics/Canvas;

    invoke-direct {v1}, Landroid/graphics/Canvas;-><init>()V

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v2

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v3

    sget-object v4, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v2, v3, v4}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/graphics/Canvas;->setBitmap(Landroid/graphics/Bitmap;)V

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v3

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v4

    invoke-virtual {v0, v5, v5, v3, v4}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    return-object v2
.end method

.method public getIntent()Landroid/content/Intent;
    .locals 4

    invoke-super {p0}, Lcom/android/settingslib/drawer/a;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/android/settings/bL;->bMM(Landroid/content/Intent;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2, v1}, Landroid/content/Intent;-><init>(Landroid/content/Intent;)V

    const-string/jumbo v3, ":settings:show_fragment"

    invoke-virtual {v2, v3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {v1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v3

    if-eqz v3, :cond_0

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0, v3}, Landroid/os/Bundle;-><init>(Landroid/os/Bundle;)V

    :goto_0
    const-string/jumbo v3, "intent"

    invoke-virtual {v0, v3, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    const-string/jumbo v1, ":settings:show_fragment_args"

    invoke-virtual {v2, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Bundle;)Landroid/content/Intent;

    return-object v2

    :cond_0
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    goto :goto_0

    :cond_1
    return-object v1
.end method

.method public getNextButton()Landroid/widget/Button;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/bL;->bTN:Landroid/widget/Button;

    return-object v0
.end method

.method public getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/android/settings/bL;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "_preferences"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Lcom/android/settings/core/instrumentation/a;

    invoke-direct {p0}, Lcom/android/settings/bL;->bML()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/android/settings/core/instrumentation/a;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    return-object v0

    :cond_0
    invoke-super {p0, p1, p2}, Lcom/android/settingslib/drawer/a;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    return-object v0
.end method

.method protected isValidFragment(Ljava/lang/String;)Z
    .locals 3

    const/4 v2, 0x1

    const/4 v0, 0x0

    :goto_0
    sget-object v1, Lcom/android/settings/core/a/a;->avG:[Ljava/lang/String;

    array-length v1, v1

    if-ge v0, v1, :cond_1

    sget-object v1, Lcom/android/settings/core/a/a;->avG:[Ljava/lang/String;

    aget-object v1, v1, v0

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    return v2

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    return v2
.end method

.method launchSettingFragment(Ljava/lang/String;ZLandroid/content/Intent;)V
    .locals 8

    const/4 v3, 0x1

    const/4 v4, 0x0

    iget-boolean v0, p0, Lcom/android/settings/bL;->bTM:Z

    if-nez v0, :cond_3

    if-eqz p1, :cond_3

    iput-boolean v4, p0, Lcom/android/settings/bL;->mDisplaySearch:Z

    iget-boolean v0, p0, Lcom/android/settings/bL;->bTL:Z

    if-eqz v0, :cond_1

    iput-boolean p2, p0, Lcom/android/settings/bL;->mDisplayHomeAsUpEnabled:Z

    :goto_0
    invoke-direct {p0, p3}, Lcom/android/settings/bL;->bMU(Landroid/content/Intent;)V

    const-string/jumbo v0, ":android:show_fragment_args"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getBundleExtra(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v2

    if-nez v2, :cond_0

    const-string/jumbo v0, ":settings:show_fragment_args"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getBundleExtra(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v2

    :cond_0
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v1, p0, Lcom/android/settings/bL;->bTI:Ljava/lang/String;

    :goto_1
    iget v5, p0, Lcom/android/settings/bL;->bTK:I

    iget-object v6, p0, Lcom/android/settings/bL;->bTJ:Ljava/lang/CharSequence;

    move-object v0, p0

    move v7, v4

    invoke-direct/range {v0 .. v7}, Lcom/android/settings/bL;->bMV(Ljava/lang/String;Landroid/os/Bundle;ZZILjava/lang/CharSequence;Z)Landroid/app/Fragment;

    :goto_2
    return-void

    :cond_1
    if-eqz p2, :cond_2

    iput-boolean v3, p0, Lcom/android/settings/bL;->mDisplayHomeAsUpEnabled:Z

    goto :goto_0

    :cond_2
    iput-boolean v4, p0, Lcom/android/settings/bL;->mDisplayHomeAsUpEnabled:Z

    goto :goto_0

    :cond_3
    iput-boolean v4, p0, Lcom/android/settings/bL;->mDisplayHomeAsUpEnabled:Z

    iput-boolean v3, p0, Lcom/android/settings/bL;->mDisplaySearch:Z

    const v0, 0x7f1204cf

    iput v0, p0, Lcom/android/settings/bL;->bTK:I

    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v1, "android.settings.SETTINGS"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/android/settings/bL;->startActivity(Landroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/android/settings/bL;->finish()V

    goto :goto_2

    :cond_4
    move-object v1, p1

    goto :goto_1
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 4

    const/16 v0, 0x2a

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/bL;->bTC:Landroid/content/ComponentName;

    if-eqz v0, :cond_0

    if-eqz p2, :cond_0

    invoke-virtual {p0}, Lcom/android/settings/bL;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/bL;->bTC:Landroid/content/ComponentName;

    const/4 v2, 0x2

    const/4 v3, 0x1

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/pm/PackageManager;->setComponentEnabledSetting(Landroid/content/ComponentName;II)V

    :cond_0
    invoke-super {p0, p1, p2, p3}, Lcom/android/settingslib/drawer/a;->onActivityResult(IILandroid/content/Intent;)V

    return-void
.end method

.method public onBackStackChanged()V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/bL;->bMS()V

    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 7

    const/16 v6, 0x8

    const/4 v2, 0x1

    const/4 v5, 0x0

    invoke-direct {p0}, Lcom/android/settings/bL;->bMK()V

    invoke-virtual {p0}, Lcom/android/settings/bL;->getIntent()Landroid/content/Intent;

    move-result-object v3

    const-string/jumbo v0, ":settings:show_fragment"

    invoke-virtual {v3, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_e

    const-string/jumbo v0, ":android:show_fragment"

    invoke-virtual {v3, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    :goto_0
    sget v0, Lmiui/R$style;->Theme_Light_Settings_NoTitle:I

    invoke-virtual {p0, v0}, Lcom/android/settings/bL;->setTheme(I)V

    invoke-direct {p0}, Lcom/android/settings/bL;->bMQ()V

    invoke-super {p0, p1}, Lcom/android/settingslib/drawer/a;->onCreate(Landroid/os/Bundle;)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    invoke-static {p0}, Lcom/android/settings/overlay/a;->aIk(Landroid/content/Context;)Lcom/android/settings/overlay/a;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/android/settings/overlay/a;->aIn(Landroid/content/Context;)Lcom/android/settings/dashboard/n;

    move-result-object v4

    iput-object v4, p0, Lcom/android/settings/bL;->bTD:Lcom/android/settings/dashboard/n;

    invoke-virtual {v0}, Lcom/android/settings/overlay/a;->aIt()Lcom/android/settings/search2/SearchFeatureProvider;

    move-result-object v4

    iput-object v4, p0, Lcom/android/settings/bL;->mSearchFeatureProvider:Lcom/android/settings/search2/SearchFeatureProvider;

    invoke-virtual {v0}, Lcom/android/settings/overlay/a;->aIm()Lcom/android/settings/core/instrumentation/e;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/bL;->mMetricsFeatureProvider:Lcom/android/settings/core/instrumentation/e;

    const-string/jumbo v0, "settings:ui_options"

    invoke-virtual {v3, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/android/settings/bL;->getWindow()Landroid/view/Window;

    move-result-object v0

    const-string/jumbo v4, "settings:ui_options"

    invoke-virtual {v3, v4, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v4

    invoke-virtual {v0, v4}, Landroid/view/Window;->setUiOptions(I)V

    :cond_0
    new-instance v0, Lcom/android/settings/SmqSettings;

    invoke-virtual {p0}, Lcom/android/settings/bL;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    invoke-direct {v0, v4}, Lcom/android/settings/SmqSettings;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/settings/bL;->bTO:Lcom/android/settings/SmqSettings;

    const-string/jumbo v0, "development"

    invoke-virtual {p0, v0, v5}, Lcom/android/settings/bL;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/bL;->bTE:Landroid/content/SharedPreferences;

    invoke-static {v3}, Lcom/android/settings/bL;->bMP(Landroid/content/Intent;)Z

    move-result v0

    if-nez v0, :cond_9

    invoke-static {v3}, Lcom/android/settings/bL;->bMO(Landroid/content/Intent;)Z

    move-result v0

    if-nez v0, :cond_9

    const-string/jumbo v0, ":settings:show_fragment_as_shortcut"

    invoke-virtual {v3, v0, v5}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    :goto_1
    iput-boolean v0, p0, Lcom/android/settings/bL;->bTL:Z

    invoke-virtual {v3}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v0

    const-class v4, Lcom/android/settings/Settings;

    invoke-virtual {v4}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/settings/bL;->bTM:Z

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    iput-boolean v5, p0, Lcom/android/settings/bL;->bTM:Z

    :cond_1
    instance-of v0, p0, Lcom/android/settings/SubSettings;

    if-nez v0, :cond_2

    const-string/jumbo v0, ":settings:show_fragment_as_subsetting"

    invoke-virtual {v3, v0, v5}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    :cond_2
    iget-boolean v0, p0, Lcom/android/settings/bL;->bTM:Z

    if-eqz v0, :cond_a

    const v0, 0x7f0d01bd

    :goto_2
    invoke-virtual {p0, v0}, Lcom/android/settings/bL;->setContentView(I)V

    const v0, 0x7f0a0284

    invoke-virtual {p0, v0}, Lcom/android/settings/bL;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/android/settings/bL;->bTB:Landroid/view/ViewGroup;

    invoke-virtual {p0}, Lcom/android/settings/bL;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/app/FragmentManager;->addOnBackStackChangedListener(Landroid/app/FragmentManager$OnBackStackChangedListener;)V

    if-eqz p1, :cond_b

    invoke-direct {p0, v3}, Lcom/android/settings/bL;->bMU(Landroid/content/Intent;)V

    const-string/jumbo v0, ":settings:categories"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    if-eqz v0, :cond_3

    iget-object v1, p0, Lcom/android/settings/bL;->bTA:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    iget-object v1, p0, Lcom/android/settings/bL;->bTA:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    invoke-direct {p0}, Lcom/android/settings/bL;->bMS()V

    :cond_3
    const-string/jumbo v0, ":settings:show_home_as_up"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/settings/bL;->mDisplayHomeAsUpEnabled:Z

    :goto_3
    const v0, 0x7f0a046d

    invoke-virtual {p0, v0}, Lcom/android/settings/bL;->findViewById(I)Landroid/view/View;

    move-result-object v0

    instance-of v1, v0, Lcom/android/settings/widget/SwitchBar;

    if-eqz v1, :cond_4

    check-cast v0, Lcom/android/settings/widget/SwitchBar;

    iput-object v0, p0, Lcom/android/settings/bL;->bTP:Lcom/android/settings/widget/SwitchBar;

    :cond_4
    iget-object v0, p0, Lcom/android/settings/bL;->bTP:Lcom/android/settings/widget/SwitchBar;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/android/settings/bL;->bTP:Lcom/android/settings/widget/SwitchBar;

    invoke-direct {p0}, Lcom/android/settings/bL;->bML()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/settings/widget/SwitchBar;->setMetricsTag(Ljava/lang/String;)V

    :cond_5
    const-string/jumbo v0, "extra_prefs_show_button_bar"

    invoke-virtual {v3, v0, v5}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_8

    const v0, 0x7f0a00b8

    invoke-virtual {p0, v0}, Lcom/android/settings/bL;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_8

    invoke-virtual {v0, v5}, Landroid/view/View;->setVisibility(I)V

    const v0, 0x7f0a007e

    invoke-virtual {p0, v0}, Lcom/android/settings/bL;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    new-instance v1, Lcom/android/settings/jr;

    invoke-direct {v1, p0}, Lcom/android/settings/jr;-><init>(Lcom/android/settings/bL;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v1, 0x7f0a040e

    invoke-virtual {p0, v1}, Lcom/android/settings/bL;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    new-instance v2, Lcom/android/settings/js;

    invoke-direct {v2, p0}, Lcom/android/settings/js;-><init>(Lcom/android/settings/bL;)V

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v2, 0x7f0a02cb

    invoke-virtual {p0, v2}, Lcom/android/settings/bL;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Button;

    iput-object v2, p0, Lcom/android/settings/bL;->bTN:Landroid/widget/Button;

    iget-object v2, p0, Lcom/android/settings/bL;->bTN:Landroid/widget/Button;

    new-instance v4, Lcom/android/settings/jt;

    invoke-direct {v4, p0}, Lcom/android/settings/jt;-><init>(Lcom/android/settings/bL;)V

    invoke-virtual {v2, v4}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const-string/jumbo v2, "extra_prefs_set_next_text"

    invoke-virtual {v3, v2}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_6

    const-string/jumbo v2, "extra_prefs_set_next_text"

    invoke-virtual {v3, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_c

    iget-object v2, p0, Lcom/android/settings/bL;->bTN:Landroid/widget/Button;

    invoke-virtual {v2, v6}, Landroid/widget/Button;->setVisibility(I)V

    :cond_6
    :goto_4
    const-string/jumbo v2, "extra_prefs_set_back_text"

    invoke-virtual {v3, v2}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_7

    const-string/jumbo v2, "extra_prefs_set_back_text"

    invoke-virtual {v3, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_d

    invoke-virtual {v0, v6}, Landroid/widget/Button;->setVisibility(I)V

    :cond_7
    :goto_5
    const-string/jumbo v0, "extra_prefs_show_skip"

    invoke-virtual {v3, v0, v5}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-virtual {v1, v5}, Landroid/widget/Button;->setVisibility(I)V

    :cond_8
    return-void

    :cond_9
    move v0, v2

    goto/16 :goto_1

    :cond_a
    const v0, 0x7f0d01be

    goto/16 :goto_2

    :cond_b
    invoke-virtual {p0, v1, v2, v3}, Lcom/android/settings/bL;->launchSettingFragment(Ljava/lang/String;ZLandroid/content/Intent;)V

    goto/16 :goto_3

    :cond_c
    iget-object v4, p0, Lcom/android/settings/bL;->bTN:Landroid/widget/Button;

    invoke-virtual {v4, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    goto :goto_4

    :cond_d
    invoke-virtual {v0, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    goto :goto_5

    :cond_e
    move-object v1, v0

    goto/16 :goto_0
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/settings/bL;->mDisplaySearch:Z

    if-nez v0, :cond_0

    const/4 v0, 0x0

    return v0

    :cond_0
    iget-object v0, p0, Lcom/android/settings/bL;->mSearchFeatureProvider:Lcom/android/settings/search2/SearchFeatureProvider;

    invoke-interface {v0, p1, p0}, Lcom/android/settings/search2/SearchFeatureProvider;->setUpSearchMenu(Landroid/view/Menu;Landroid/app/Activity;)V

    const/4 v0, 0x1

    return v0
.end method

.method protected onPause()V
    .locals 3

    const/4 v2, 0x0

    invoke-super {p0}, Lcom/android/settingslib/drawer/a;->onPause()V

    iget-object v0, p0, Lcom/android/settings/bL;->bTE:Landroid/content/SharedPreferences;

    iget-object v1, p0, Lcom/android/settings/bL;->bTF:Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences;->unregisterOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    iput-object v2, p0, Lcom/android/settings/bL;->bTF:Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;

    iget-object v0, p0, Lcom/android/settings/bL;->bTy:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/android/settings/bL;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    iget-object v0, p0, Lcom/android/settings/bL;->bTG:Lcom/android/settings/search/DynamicIndexableContentMonitor;

    invoke-direct {p0}, Lcom/android/settings/bL;->bMW()V

    return-void
.end method

.method public onPreferenceStartFragment(Landroid/preference/PreferenceFragment;Landroid/preference/Preference;)Z
    .locals 8

    invoke-virtual {p2}, Landroid/preference/Preference;->getFragment()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p2}, Landroid/preference/Preference;->getExtras()Landroid/os/Bundle;

    move-result-object v3

    invoke-virtual {p2}, Landroid/preference/Preference;->getTitleRes()I

    move-result v4

    invoke-virtual {p2}, Landroid/preference/Preference;->getTitle()Ljava/lang/CharSequence;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object v0, p0

    move-object v1, p1

    invoke-virtual/range {v0 .. v7}, Lcom/android/settings/bL;->Uv(Landroid/app/Fragment;Ljava/lang/String;Landroid/os/Bundle;ILjava/lang/CharSequence;Landroid/app/Fragment;I)V

    const/4 v0, 0x1

    return v0
.end method

.method public onPreferenceTreeClick(Landroid/preference/PreferenceScreen;Landroid/preference/Preference;)Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method protected onRestoreInstanceState(Landroid/os/Bundle;)V
    .locals 1

    invoke-super {p0, p1}, Lcom/android/settingslib/drawer/a;->onRestoreInstanceState(Landroid/os/Bundle;)V

    const-string/jumbo v0, ":settings:show_home_as_up"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/settings/bL;->mDisplayHomeAsUpEnabled:Z

    const-string/jumbo v0, ":settings:show_search"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/settings/bL;->mDisplaySearch:Z

    return-void
.end method

.method protected onResume()V
    .locals 3

    invoke-super {p0}, Lcom/android/settingslib/drawer/a;->onResume()V

    new-instance v0, Lcom/android/settings/ju;

    invoke-direct {v0, p0}, Lcom/android/settings/ju;-><init>(Lcom/android/settings/bL;)V

    iput-object v0, p0, Lcom/android/settings/bL;->bTF:Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;

    iget-object v0, p0, Lcom/android/settings/bL;->bTE:Landroid/content/SharedPreferences;

    iget-object v1, p0, Lcom/android/settings/bL;->bTF:Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences;->registerOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    iget-object v0, p0, Lcom/android/settings/bL;->bTy:Landroid/content/BroadcastReceiver;

    new-instance v1, Landroid/content/IntentFilter;

    const-string/jumbo v2, "android.intent.action.BATTERY_CHANGED"

    invoke-direct {v1, v2}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0, v1}, Lcom/android/settings/bL;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    iget-object v0, p0, Lcom/android/settings/bL;->bTG:Lcom/android/settings/search/DynamicIndexableContentMonitor;

    if-nez v0, :cond_0

    new-instance v0, Lcom/android/settings/search/DynamicIndexableContentMonitor;

    invoke-direct {v0}, Lcom/android/settings/search/DynamicIndexableContentMonitor;-><init>()V

    iput-object v0, p0, Lcom/android/settings/bL;->bTG:Lcom/android/settings/search/DynamicIndexableContentMonitor;

    :cond_0
    invoke-direct {p0}, Lcom/android/settings/bL;->bMX()V

    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 0

    invoke-super {p0, p1}, Lcom/android/settingslib/drawer/a;->onSaveInstanceState(Landroid/os/Bundle;)V

    invoke-virtual {p0, p1}, Lcom/android/settings/bL;->saveState(Landroid/os/Bundle;)V

    return-void
.end method

.method saveState(Landroid/os/Bundle;)V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/bL;->bTA:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_0

    const-string/jumbo v0, ":settings:categories"

    iget-object v1, p0, Lcom/android/settings/bL;->bTA:Ljava/util/ArrayList;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    :cond_0
    const-string/jumbo v0, ":settings:show_home_as_up"

    iget-boolean v1, p0, Lcom/android/settings/bL;->mDisplayHomeAsUpEnabled:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string/jumbo v0, ":settings:show_search"

    iget-boolean v1, p0, Lcom/android/settings/bL;->mDisplaySearch:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    return-void
.end method

.method public setTaskDescription(Landroid/app/ActivityManager$TaskDescription;)V
    .locals 1

    const v0, 0x7f0801f6

    invoke-virtual {p0, v0}, Lcom/android/settings/bL;->getBitmapFromXmlResource(I)Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/app/ActivityManager$TaskDescription;->setIcon(Landroid/graphics/Bitmap;)V

    invoke-super {p0, p1}, Lcom/android/settingslib/drawer/a;->setTaskDescription(Landroid/app/ActivityManager$TaskDescription;)V

    return-void
.end method

.method public shouldUpRecreateTask(Landroid/content/Intent;)Z
    .locals 2

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/android/settings/bL;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-super {p0, v0}, Lcom/android/settingslib/drawer/a;->shouldUpRecreateTask(Landroid/content/Intent;)Z

    move-result v0

    return v0
.end method
