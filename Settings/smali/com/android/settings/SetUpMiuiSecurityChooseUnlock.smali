.class public Lcom/android/settings/SetUpMiuiSecurityChooseUnlock;
.super Lcom/android/settings/Settings;
.source "SetUpMiuiSecurityChooseUnlock.java"


# static fields
.field public static final TAG:Ljava/lang/String;

.field private static bML:Z

.field private static bMM:Landroid/os/CountDownTimer;

.field private static bMN:Lcom/android/settings/SetUpRadioButtonPreference;

.field private static bMO:Lcom/android/settings/SetUpRadioButtonPreference;

.field private static bMP:Lcom/android/settings/SetUpRadioButtonPreference;

.field private static mUserId:I


# direct methods
.method static synthetic -get0()Z
    .locals 1

    sget-boolean v0, Lcom/android/settings/SetUpMiuiSecurityChooseUnlock;->bML:Z

    return v0
.end method

.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/android/settings/SetUpMiuiSecurityChooseUnlock;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/settings/SetUpMiuiSecurityChooseUnlock;->TAG:Ljava/lang/String;

    const/4 v0, 0x0

    sput-boolean v0, Lcom/android/settings/SetUpMiuiSecurityChooseUnlock;->bML:Z

    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v0

    sput v0, Lcom/android/settings/SetUpMiuiSecurityChooseUnlock;->mUserId:I

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/Settings;-><init>()V

    return-void
.end method

.method public static bDY(ILcom/android/settings/KeyguardSettingsPreferenceFragment;)V
    .locals 12

    invoke-virtual {p1}, Lcom/android/settings/KeyguardSettingsPreferenceFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "lockscreen.biometric_weak_fallback"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v6

    invoke-virtual {p1}, Lcom/android/settings/KeyguardSettingsPreferenceFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "use_lock_password_to_encrypt_device"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v7

    invoke-virtual {p1}, Lcom/android/settings/KeyguardSettingsPreferenceFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "add_keyguard_password_then_add_fingerprint"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    sput-boolean v0, Lcom/android/settings/SetUpMiuiSecurityChooseUnlock;->bML:Z

    const-string/jumbo v0, "device_policy"

    invoke-virtual {p1, v0}, Lcom/android/settings/KeyguardSettingsPreferenceFragment;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/admin/DevicePolicyManager;

    const/4 v1, 0x0

    const-wide/16 v2, 0x0

    invoke-virtual {p1}, Lcom/android/settings/KeyguardSettingsPreferenceFragment;->getActivity()Landroid/app/Activity;

    move-result-object v8

    new-instance v4, Lcom/android/settings/bM;

    invoke-direct {v4, v8}, Lcom/android/settings/bM;-><init>(Landroid/content/Context;)V

    invoke-virtual {v4}, Lcom/android/settings/bM;->bNd()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-virtual {v8}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v4

    instance-of v5, v8, Lcom/android/settings/SetUpMiuiSecurityChooseUnlock$InternalActivity;

    if-eqz v5, :cond_0

    if-eqz v4, :cond_0

    const-string/jumbo v1, "has_challenge"

    const/4 v5, 0x0

    invoke-virtual {v4, v1, v5}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string/jumbo v2, "challenge"

    const-wide/16 v10, 0x0

    invoke-virtual {v4, v2, v10, v11}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v2

    :cond_0
    invoke-static {p0, v0, p1}, Lcom/android/settings/SetUpMiuiSecurityChooseUnlock;->bEd(ILandroid/app/admin/DevicePolicyManager;Lcom/android/settings/KeyguardSettingsPreferenceFragment;)I

    move-result v4

    const/high16 v5, 0x20000

    if-lt v4, v5, :cond_7

    sget v5, Lcom/android/settings/SetUpMiuiSecurityChooseUnlock;->mUserId:I

    const/4 v9, 0x0

    invoke-virtual {v0, v9, v5}, Landroid/app/admin/DevicePolicyManager;->getPasswordMinimumLength(Landroid/content/ComponentName;I)I

    move-result v5

    const/4 v9, 0x4

    if-ge v5, v9, :cond_1

    const/4 v5, 0x4

    :cond_1
    const/high16 v9, 0x40000

    if-le v4, v9, :cond_2

    const/high16 v4, 0x40000

    :cond_2
    const/high16 v9, 0x20000

    if-eq v4, v9, :cond_3

    const/high16 v9, 0x30000

    if-ne v4, v9, :cond_6

    :cond_3
    const/4 v0, 0x4

    :goto_0
    new-instance v9, Landroid/content/Intent;

    invoke-direct {v9}, Landroid/content/Intent;-><init>()V

    invoke-virtual {p1}, Lcom/android/settings/KeyguardSettingsPreferenceFragment;->getActivity()Landroid/app/Activity;

    move-result-object v10

    const-class v11, Lcom/android/settings/SetUpChooseLockPassword;

    invoke-virtual {v9, v10, v11}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v9

    const-string/jumbo v10, "lockscreen.password_type"

    invoke-virtual {v9, v10, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string/jumbo v4, "lockscreen.password_min"

    invoke-virtual {v9, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string/jumbo v4, "lockscreen.password_max"

    invoke-virtual {v9, v4, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    sget v0, Lcom/android/settings/SetUpMiuiSecurityChooseUnlock;->mUserId:I

    invoke-static {v9, v0}, Lcom/android/settings/aN;->bwX(Landroid/content/Intent;I)V

    const-string/jumbo v0, "lockscreen.biometric_weak_fallback"

    invoke-virtual {v9, v0, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string/jumbo v0, "setup_userid_to_set_password"

    invoke-virtual {p1}, Lcom/android/settings/KeyguardSettingsPreferenceFragment;->getActivity()Landroid/app/Activity;

    move-result-object v4

    invoke-virtual {v4}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v4

    const-string/jumbo v5, "setup_userid_to_set_password"

    const/16 v10, -0x2710

    invoke-virtual {v4, v5, v10}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v4

    invoke-virtual {v9, v0, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string/jumbo v0, "use_lock_password_to_encrypt_device"

    invoke-virtual {v9, v0, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string/jumbo v0, "set_keyguard_password"

    invoke-virtual {v8}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v4

    const-string/jumbo v5, "set_keyguard_password"

    const/4 v7, 0x1

    invoke-virtual {v4, v5, v7}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v4

    invoke-virtual {v9, v0, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string/jumbo v0, "add_keyguard_password_then_add_fingerprint"

    sget-boolean v4, Lcom/android/settings/SetUpMiuiSecurityChooseUnlock;->bML:Z

    invoke-virtual {v9, v0, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    if-eqz v1, :cond_4

    const-string/jumbo v0, "has_challenge"

    const/4 v1, 0x1

    invoke-virtual {v9, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string/jumbo v0, "challenge"

    invoke-virtual {v9, v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    :cond_4
    if-nez v6, :cond_5

    const/16 v0, 0x12e

    invoke-virtual {p1, v9, v0}, Lcom/android/settings/KeyguardSettingsPreferenceFragment;->startActivityForResult(Landroid/content/Intent;I)V

    :cond_5
    :goto_1
    return-void

    :cond_6
    invoke-virtual {v0, v4}, Landroid/app/admin/DevicePolicyManager;->getPasswordMaximumLength(I)I

    move-result v0

    goto :goto_0

    :cond_7
    const/high16 v0, 0x10000

    if-ne v4, v0, :cond_5

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    invoke-virtual {p1}, Lcom/android/settings/KeyguardSettingsPreferenceFragment;->getActivity()Landroid/app/Activity;

    move-result-object v4

    const-class v5, Lcom/android/settings/SetUpChooseLockPattern;

    invoke-virtual {v0, v4, v5}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    const-string/jumbo v4, "key_lock_method"

    const-string/jumbo v5, "pattern"

    invoke-virtual {v0, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    sget v4, Lcom/android/settings/SetUpMiuiSecurityChooseUnlock;->mUserId:I

    invoke-static {v0, v4}, Lcom/android/settings/aN;->bwX(Landroid/content/Intent;I)V

    const-string/jumbo v4, "use_lock_password_to_encrypt_device"

    invoke-virtual {v0, v4, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string/jumbo v4, "lockscreen.biometric_weak_fallback"

    invoke-virtual {v0, v4, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string/jumbo v4, "setup_userid_to_set_password"

    invoke-virtual {p1}, Lcom/android/settings/KeyguardSettingsPreferenceFragment;->getActivity()Landroid/app/Activity;

    move-result-object v5

    invoke-virtual {v5}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v5

    const-string/jumbo v7, "setup_userid_to_set_password"

    const/16 v9, -0x2710

    invoke-virtual {v5, v7, v9}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v5

    invoke-virtual {v0, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string/jumbo v4, "set_keyguard_password"

    invoke-virtual {v8}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v5

    const-string/jumbo v7, "set_keyguard_password"

    const/4 v8, 0x1

    invoke-virtual {v5, v7, v8}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v5

    invoke-virtual {v0, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string/jumbo v4, "add_keyguard_password_then_add_fingerprint"

    sget-boolean v5, Lcom/android/settings/SetUpMiuiSecurityChooseUnlock;->bML:Z

    invoke-virtual {v0, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    if-eqz v1, :cond_8

    const-string/jumbo v1, "has_challenge"

    const/4 v4, 0x1

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string/jumbo v1, "challenge"

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    :cond_8
    if-nez v6, :cond_5

    const/16 v1, 0x12e

    invoke-virtual {p1, v0, v1}, Lcom/android/settings/KeyguardSettingsPreferenceFragment;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_1
.end method

.method public static bDZ(Lcom/android/settings/KeyguardSettingsPreferenceFragment;I)V
    .locals 5

    const/high16 v4, 0x40000

    const/4 v1, 0x4

    const-string/jumbo v0, "device_policy"

    invoke-virtual {p0, v0}, Lcom/android/settings/KeyguardSettingsPreferenceFragment;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/admin/DevicePolicyManager;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/app/admin/DevicePolicyManager;->getPasswordMinimumLength(Landroid/content/ComponentName;)I

    move-result v2

    invoke-virtual {v0, v4}, Landroid/app/admin/DevicePolicyManager;->getPasswordMaximumLength(I)I

    move-result v3

    if-ge v2, v1, :cond_0

    move v0, v1

    :goto_0
    invoke-static {p0, p1, v0, v3, v4}, Lcom/android/settings/SetUpMiuiSecurityChooseUnlock;->bEb(Lcom/android/settings/KeyguardSettingsPreferenceFragment;IIII)V

    return-void

    :cond_0
    move v0, v2

    goto :goto_0
.end method

.method public static bEa(Lcom/android/settings/KeyguardSettingsPreferenceFragment;I)V
    .locals 2

    const/4 v1, 0x4

    const/high16 v0, 0x20000

    invoke-static {p0, p1, v1, v1, v0}, Lcom/android/settings/SetUpMiuiSecurityChooseUnlock;->bEb(Lcom/android/settings/KeyguardSettingsPreferenceFragment;IIII)V

    return-void
.end method

.method public static bEb(Lcom/android/settings/KeyguardSettingsPreferenceFragment;IIII)V
    .locals 5

    const/4 v3, 0x0

    invoke-virtual {p0}, Lcom/android/settings/KeyguardSettingsPreferenceFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "use_lock_password_to_encrypt_device"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    invoke-virtual {p0}, Lcom/android/settings/KeyguardSettingsPreferenceFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v2, "add_keyguard_password_then_add_fingerprint"

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    sput-boolean v0, Lcom/android/settings/SetUpMiuiSecurityChooseUnlock;->bML:Z

    invoke-virtual {p0}, Lcom/android/settings/KeyguardSettingsPreferenceFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v2, "set_keyguard_password"

    const/4 v3, 0x1

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    invoke-virtual {p0}, Lcom/android/settings/KeyguardSettingsPreferenceFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceActivity;

    invoke-virtual {v0}, Landroid/preference/PreferenceActivity;->onIsMultiPane()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string/jumbo v3, "lockscreen.password_min"

    invoke-virtual {v0, v3, p2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string/jumbo v3, "lockscreen.password_max"

    invoke-virtual {v0, v3, p3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string/jumbo v3, "lockscreen.password_type"

    invoke-virtual {v0, v3, p4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string/jumbo v3, "use_lock_password_to_encrypt_device"

    invoke-virtual {v0, v3, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string/jumbo v1, "set_keyguard_password"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string/jumbo v1, "add_keyguard_password_then_add_fingerprint"

    sget-boolean v2, Lcom/android/settings/SetUpMiuiSecurityChooseUnlock;->bML:Z

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-class v1, Lcom/android/settings/SetUpChooseLockPassword;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    const v2, 0x7f120997

    invoke-static {p0, v1, p1, v0, v2}, Lcom/android/settings/bn;->bFJ(Lcom/android/settings/KeyguardSettingsPreferenceFragment;Ljava/lang/String;ILandroid/os/Bundle;I)Z

    :goto_0
    return-void

    :cond_0
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/android/settings/KeyguardSettingsPreferenceFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    const-class v4, Lcom/android/settings/SetUpChooseLockPassword;

    invoke-direct {v0, v3, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v3, "lockscreen.password_min"

    invoke-virtual {v0, v3, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string/jumbo v3, "lockscreen.password_max"

    invoke-virtual {v0, v3, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string/jumbo v3, "lockscreen.password_type"

    invoke-virtual {v0, v3, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string/jumbo v3, "use_lock_password_to_encrypt_device"

    invoke-virtual {v0, v3, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string/jumbo v1, "set_keyguard_password"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string/jumbo v1, "add_keyguard_password_then_add_fingerprint"

    sget-boolean v2, Lcom/android/settings/SetUpMiuiSecurityChooseUnlock;->bML:Z

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    invoke-virtual {p0, v0, p1}, Lcom/android/settings/KeyguardSettingsPreferenceFragment;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0
.end method

.method public static bEc(Lcom/android/settings/KeyguardSettingsPreferenceFragment;I)V
    .locals 5

    const/4 v3, 0x0

    invoke-virtual {p0}, Lcom/android/settings/KeyguardSettingsPreferenceFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "use_lock_password_to_encrypt_device"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    const-string/jumbo v2, "add_keyguard_password_then_add_fingerprint"

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    sput-boolean v2, Lcom/android/settings/SetUpMiuiSecurityChooseUnlock;->bML:Z

    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    const-string/jumbo v3, "use_lock_password_to_encrypt_device"

    invoke-virtual {v2, v3, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string/jumbo v1, "set_keyguard_password"

    const-string/jumbo v3, "set_keyguard_password"

    const/4 v4, 0x1

    invoke-virtual {v0, v3, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    invoke-virtual {v2, v1, v0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string/jumbo v0, "add_keyguard_password_then_add_fingerprint"

    sget-boolean v1, Lcom/android/settings/SetUpMiuiSecurityChooseUnlock;->bML:Z

    invoke-virtual {v2, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-class v0, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    const v1, 0x7f120999

    invoke-static {p0, v0, p1, v2, v1}, Lcom/android/settings/bn;->bFJ(Lcom/android/settings/KeyguardSettingsPreferenceFragment;Ljava/lang/String;ILandroid/os/Bundle;I)Z

    return-void
.end method

.method private static bEd(ILandroid/app/admin/DevicePolicyManager;Lcom/android/settings/KeyguardSettingsPreferenceFragment;)I
    .locals 4

    const/4 v3, 0x0

    invoke-static {p0, p1}, Lcom/android/settings/SetUpMiuiSecurityChooseUnlock;->bEe(ILandroid/app/admin/DevicePolicyManager;)I

    move-result v0

    invoke-virtual {p2}, Lcom/android/settings/KeyguardSettingsPreferenceFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string/jumbo v2, "vpn_password_enable"

    invoke-static {v1, v2, v3}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    if-lez v1, :cond_0

    invoke-static {v0}, Lcom/android/settings/SetUpMiuiSecurityChooseUnlock;->bEf(I)I

    move-result v0

    :cond_0
    return v0
.end method

.method private static bEe(ILandroid/app/admin/DevicePolicyManager;)I
    .locals 2

    sget v0, Lcom/android/settings/SetUpMiuiSecurityChooseUnlock;->mUserId:I

    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, Landroid/app/admin/DevicePolicyManager;->getPasswordQuality(Landroid/content/ComponentName;I)I

    move-result v0

    if-ge p0, v0, :cond_0

    move p0, v0

    :cond_0
    return p0
.end method

.method private static bEf(I)I
    .locals 2

    const/high16 v0, 0x10000

    invoke-static {}, Landroid/security/KeyStore;->getInstance()Landroid/security/KeyStore;

    move-result-object v1

    invoke-virtual {v1}, Landroid/security/KeyStore;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    if-ge p0, v0, :cond_0

    move p0, v0

    :cond_0
    return p0
.end method

.method static synthetic bEg()Lcom/android/settings/SetUpRadioButtonPreference;
    .locals 1

    sget-object v0, Lcom/android/settings/SetUpMiuiSecurityChooseUnlock;->bMN:Lcom/android/settings/SetUpRadioButtonPreference;

    return-object v0
.end method

.method static synthetic bEh()Lcom/android/settings/SetUpRadioButtonPreference;
    .locals 1

    sget-object v0, Lcom/android/settings/SetUpMiuiSecurityChooseUnlock;->bMO:Lcom/android/settings/SetUpRadioButtonPreference;

    return-object v0
.end method

.method static synthetic bEi()Lcom/android/settings/SetUpRadioButtonPreference;
    .locals 1

    sget-object v0, Lcom/android/settings/SetUpMiuiSecurityChooseUnlock;->bMP:Lcom/android/settings/SetUpRadioButtonPreference;

    return-object v0
.end method

.method static synthetic bEj(Z)Z
    .locals 0

    sput-boolean p0, Lcom/android/settings/SetUpMiuiSecurityChooseUnlock;->bML:Z

    return p0
.end method

.method static synthetic bEk(Landroid/os/CountDownTimer;)Landroid/os/CountDownTimer;
    .locals 0

    sput-object p0, Lcom/android/settings/SetUpMiuiSecurityChooseUnlock;->bMM:Landroid/os/CountDownTimer;

    return-object p0
.end method

.method static synthetic bEl(Lcom/android/settings/SetUpRadioButtonPreference;)Lcom/android/settings/SetUpRadioButtonPreference;
    .locals 0

    sput-object p0, Lcom/android/settings/SetUpMiuiSecurityChooseUnlock;->bMN:Lcom/android/settings/SetUpRadioButtonPreference;

    return-object p0
.end method

.method static synthetic bEm(Lcom/android/settings/SetUpRadioButtonPreference;)Lcom/android/settings/SetUpRadioButtonPreference;
    .locals 0

    sput-object p0, Lcom/android/settings/SetUpMiuiSecurityChooseUnlock;->bMO:Lcom/android/settings/SetUpRadioButtonPreference;

    return-object p0
.end method

.method static synthetic bEn(Lcom/android/settings/SetUpRadioButtonPreference;)Lcom/android/settings/SetUpRadioButtonPreference;
    .locals 0

    sput-object p0, Lcom/android/settings/SetUpMiuiSecurityChooseUnlock;->bMP:Lcom/android/settings/SetUpRadioButtonPreference;

    return-object p0
.end method

.method static synthetic bEo(I)I
    .locals 0

    sput p0, Lcom/android/settings/SetUpMiuiSecurityChooseUnlock;->mUserId:I

    return p0
.end method

.method static synthetic bEp(ILandroid/app/admin/DevicePolicyManager;Lcom/android/settings/KeyguardSettingsPreferenceFragment;)I
    .locals 1

    invoke-static {p0, p1, p2}, Lcom/android/settings/SetUpMiuiSecurityChooseUnlock;->bEd(ILandroid/app/admin/DevicePolicyManager;Lcom/android/settings/KeyguardSettingsPreferenceFragment;)I

    move-result v0

    return v0
.end method


# virtual methods
.method public getIntent()Landroid/content/Intent;
    .locals 3

    new-instance v0, Landroid/content/Intent;

    invoke-super {p0}, Lcom/android/settings/Settings;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Intent;)V

    const-string/jumbo v1, ":settings:show_fragment"

    const-class v2, Lcom/android/settings/SetUpMiuiSecurityChooseUnlock$SetUpMiuiSecurityChooseUnlockFragment;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    return-object v0
.end method
