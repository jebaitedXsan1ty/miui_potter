.class Lcom/android/settings/bV;
.super Landroid/widget/BaseAdapter;
.source "MiuiSecurityTrustedCredentials.java"


# instance fields
.field private final bVo:Landroid/app/Activity;

.field private final bVp:Ljava/util/List;

.field private bVq:Landroid/view/View;

.field private final bVr:Lcom/android/org/conscrypt/TrustedCertificateStore;

.field private final bVs:Lcom/android/settings/MiuiSecurityTrustedCredentials$Tab;


# direct methods
.method private constructor <init>(Landroid/app/Activity;Lcom/android/settings/MiuiSecurityTrustedCredentials$Tab;Landroid/view/View;Lcom/android/org/conscrypt/TrustedCertificateStore;)V
    .locals 1

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    iput-object p2, p0, Lcom/android/settings/bV;->bVs:Lcom/android/settings/MiuiSecurityTrustedCredentials$Tab;

    iput-object p1, p0, Lcom/android/settings/bV;->bVo:Landroid/app/Activity;

    iput-object p3, p0, Lcom/android/settings/bV;->bVq:Landroid/view/View;

    iput-object p4, p0, Lcom/android/settings/bV;->bVr:Lcom/android/org/conscrypt/TrustedCertificateStore;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/settings/bV;->bVp:Ljava/util/List;

    invoke-direct {p0}, Lcom/android/settings/bV;->load()V

    return-void
.end method

.method synthetic constructor <init>(Landroid/app/Activity;Lcom/android/settings/MiuiSecurityTrustedCredentials$Tab;Landroid/view/View;Lcom/android/org/conscrypt/TrustedCertificateStore;Lcom/android/settings/bV;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/android/settings/bV;-><init>(Landroid/app/Activity;Lcom/android/settings/MiuiSecurityTrustedCredentials$Tab;Landroid/view/View;Lcom/android/org/conscrypt/TrustedCertificateStore;)V

    return-void
.end method

.method static synthetic bPn(Lcom/android/settings/bV;)Landroid/app/Activity;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/bV;->bVo:Landroid/app/Activity;

    return-object v0
.end method

.method static synthetic bPo(Lcom/android/settings/bV;)Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/bV;->bVp:Ljava/util/List;

    return-object v0
.end method

.method static synthetic bPp(Lcom/android/settings/bV;)Lcom/android/org/conscrypt/TrustedCertificateStore;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/bV;->bVr:Lcom/android/org/conscrypt/TrustedCertificateStore;

    return-object v0
.end method

.method static synthetic bPq(Lcom/android/settings/bV;)Lcom/android/settings/MiuiSecurityTrustedCredentials$Tab;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/bV;->bVs:Lcom/android/settings/MiuiSecurityTrustedCredentials$Tab;

    return-object v0
.end method

.method static synthetic bPr(Lcom/android/settings/bV;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/bV;->load()V

    return-void
.end method

.method private load()V
    .locals 2

    new-instance v0, Lcom/android/settings/bW;

    iget-object v1, p0, Lcom/android/settings/bV;->bVq:Landroid/view/View;

    invoke-direct {v0, p0, v1}, Lcom/android/settings/bW;-><init>(Lcom/android/settings/bV;Landroid/view/View;)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/android/settings/bW;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 1

    iget-object v0, p0, Lcom/android/settings/bV;->bVp:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getItem(I)Lcom/android/settings/bX;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/bV;->bVp:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settings/bX;

    return-object v0
.end method

.method public bridge synthetic getItem(I)Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0, p1}, Lcom/android/settings/bV;->getItem(I)Lcom/android/settings/bX;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2

    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 5

    const/4 v2, 0x0

    const/4 v4, 0x0

    if-nez p2, :cond_1

    iget-object v0, p0, Lcom/android/settings/bV;->bVo:Landroid/app/Activity;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0d00fb

    invoke-virtual {v0, v1, p3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    new-instance v1, Lcom/android/settings/bY;

    invoke-direct {v1, v2}, Lcom/android/settings/bY;-><init>(Lcom/android/settings/bY;)V

    const v0, 0x7f0a04b0

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-static {v1, v0}, Lcom/android/settings/bY;->bPI(Lcom/android/settings/bY;Landroid/widget/TextView;)Landroid/widget/TextView;

    const v0, 0x7f0a04b1

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-static {v1, v0}, Lcom/android/settings/bY;->bPJ(Lcom/android/settings/bY;Landroid/widget/TextView;)Landroid/widget/TextView;

    const v0, 0x7f0a00ce

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    invoke-static {v1, v0}, Lcom/android/settings/bY;->bPH(Lcom/android/settings/bY;Landroid/widget/CheckBox;)Landroid/widget/CheckBox;

    invoke-virtual {p2, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    :goto_0
    iget-object v0, p0, Lcom/android/settings/bV;->bVp:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settings/bX;

    invoke-static {v1}, Lcom/android/settings/bY;->bPF(Lcom/android/settings/bY;)Landroid/widget/TextView;

    move-result-object v2

    invoke-static {v0}, Lcom/android/settings/bX;->bPz(Lcom/android/settings/bX;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-static {v1}, Lcom/android/settings/bY;->bPG(Lcom/android/settings/bY;)Landroid/widget/TextView;

    move-result-object v2

    invoke-static {v0}, Lcom/android/settings/bX;->bPA(Lcom/android/settings/bX;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v2, p0, Lcom/android/settings/bV;->bVs:Lcom/android/settings/MiuiSecurityTrustedCredentials$Tab;

    invoke-static {v2}, Lcom/android/settings/MiuiSecurityTrustedCredentials$Tab;->bPe(Lcom/android/settings/MiuiSecurityTrustedCredentials$Tab;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-static {v1}, Lcom/android/settings/bY;->bPE(Lcom/android/settings/bY;)Landroid/widget/CheckBox;

    move-result-object v2

    invoke-static {v0}, Lcom/android/settings/bX;->bPx(Lcom/android/settings/bX;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    invoke-virtual {v2, v0}, Landroid/widget/CheckBox;->setChecked(Z)V

    invoke-static {v1}, Lcom/android/settings/bY;->bPE(Lcom/android/settings/bY;)Landroid/widget/CheckBox;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/widget/CheckBox;->setVisibility(I)V

    :cond_0
    return-object p2

    :cond_1
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settings/bY;

    move-object v1, v0

    goto :goto_0
.end method
