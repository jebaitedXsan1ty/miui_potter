.class public Lcom/android/settings/RestrictedCheckBox;
.super Landroid/widget/CheckBox;
.source "RestrictedCheckBox.java"


# instance fields
.field private bAj:Z

.field private bAk:Lcom/android/settingslib/n;

.field private mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/android/settings/RestrictedCheckBox;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Landroid/widget/CheckBox;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    iput-object p1, p0, Lcom/android/settings/RestrictedCheckBox;->mContext:Landroid/content/Context;

    return-void
.end method


# virtual methods
.method public performClick()Z
    .locals 2

    iget-boolean v0, p0, Lcom/android/settings/RestrictedCheckBox;->bAj:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/RestrictedCheckBox;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/android/settings/RestrictedCheckBox;->bAk:Lcom/android/settingslib/n;

    invoke-static {v0, v1}, Lcom/android/settingslib/w;->cqW(Landroid/content/Context;Lcom/android/settingslib/n;)V

    const/4 v0, 0x1

    return v0

    :cond_0
    invoke-super {p0}, Landroid/widget/CheckBox;->performClick()Z

    move-result v0

    return v0
.end method

.method public setDisabledByAdmin(Lcom/android/settingslib/n;)V
    .locals 3

    if-eqz p1, :cond_1

    const/4 v0, 0x1

    :goto_0
    iput-object p1, p0, Lcom/android/settings/RestrictedCheckBox;->bAk:Lcom/android/settingslib/n;

    iget-boolean v1, p0, Lcom/android/settings/RestrictedCheckBox;->bAj:Z

    if-eq v1, v0, :cond_0

    iput-boolean v0, p0, Lcom/android/settings/RestrictedCheckBox;->bAj:Z

    iget-object v0, p0, Lcom/android/settings/RestrictedCheckBox;->mContext:Landroid/content/Context;

    iget-boolean v1, p0, Lcom/android/settings/RestrictedCheckBox;->bAj:Z

    invoke-static {v0, p0, v1}, Lcom/android/settingslib/w;->crg(Landroid/content/Context;Landroid/widget/TextView;Z)V

    iget-boolean v0, p0, Lcom/android/settings/RestrictedCheckBox;->bAj:Z

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lcom/android/settings/RestrictedCheckBox;->getButtonDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/RestrictedCheckBox;->mContext:Landroid/content/Context;

    const v2, 0x7f060064

    invoke-virtual {v1, v2}, Landroid/content/Context;->getColor(I)I

    move-result v1

    sget-object v2, Landroid/graphics/PorterDuff$Mode;->MULTIPLY:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/Drawable;->setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V

    :cond_0
    :goto_1
    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Lcom/android/settings/RestrictedCheckBox;->getButtonDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->clearColorFilter()V

    goto :goto_1
.end method
