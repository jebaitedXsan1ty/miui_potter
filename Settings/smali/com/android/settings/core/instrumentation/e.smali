.class public Lcom/android/settings/core/instrumentation/e;
.super Ljava/lang/Object;
.source "MetricsFeatureProvider.java"


# instance fields
.field private avy:Ljava/util/List;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/settings/core/instrumentation/e;->avy:Ljava/util/List;

    invoke-virtual {p0}, Lcom/android/settings/core/instrumentation/e;->akb()V

    return-void
.end method


# virtual methods
.method public varargs ajQ(Landroid/content/Context;ILjava/lang/String;[Landroid/util/Pair;)V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/core/instrumentation/e;->avy:Ljava/util/List;

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settings/core/instrumentation/g;

    invoke-interface {v0, p1, p2, p3, p4}, Lcom/android/settings/core/instrumentation/g;->ajK(Landroid/content/Context;ILjava/lang/String;[Landroid/util/Pair;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public ajR(Landroid/content/Context;Landroid/content/Intent;I)V
    .locals 7

    const/16 v6, 0x341

    const/16 v5, 0x33e

    const/4 v3, 0x1

    const/4 v4, 0x0

    if-nez p2, :cond_0

    return-void

    :cond_0
    invoke-virtual {p2}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v0

    if-nez v0, :cond_2

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    return-void

    :cond_1
    new-array v1, v3, [Landroid/util/Pair;

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-virtual {p0, p1, v5, v0, v1}, Lcom/android/settings/core/instrumentation/e;->ajQ(Landroid/content/Context;ILjava/lang/String;[Landroid/util/Pair;)V

    return-void

    :cond_2
    invoke-virtual {v0}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_3

    return-void

    :cond_3
    invoke-virtual {v0}, Landroid/content/ComponentName;->flattenToString()Ljava/lang/String;

    move-result-object v0

    new-array v1, v3, [Landroid/util/Pair;

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-virtual {p0, p1, v5, v0, v1}, Lcom/android/settings/core/instrumentation/e;->ajQ(Landroid/content/Context;ILjava/lang/String;[Landroid/util/Pair;)V

    return-void
.end method

.method public varargs ajS(Landroid/content/Context;I[Landroid/util/Pair;)V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/core/instrumentation/e;->avy:Ljava/util/List;

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settings/core/instrumentation/g;

    invoke-interface {v0, p1, p2, p3}, Lcom/android/settings/core/instrumentation/g;->ajH(Landroid/content/Context;I[Landroid/util/Pair;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public ajT(Ljava/lang/Object;)I
    .locals 1

    if-eqz p1, :cond_0

    instance-of v0, p1, Lcom/android/settings/core/instrumentation/f;

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x0

    return v0

    :cond_1
    check-cast p1, Lcom/android/settings/core/instrumentation/f;

    invoke-interface {p1}, Lcom/android/settings/core/instrumentation/f;->getMetricsCategory()I

    move-result v0

    return v0
.end method

.method public ajU(Landroid/content/Context;II)V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/core/instrumentation/e;->avy:Ljava/util/List;

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settings/core/instrumentation/g;

    invoke-interface {v0, p1, p2, p3}, Lcom/android/settings/core/instrumentation/g;->ajI(Landroid/content/Context;II)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public ajV(Landroid/content/Context;IZ)V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/core/instrumentation/e;->avy:Ljava/util/List;

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settings/core/instrumentation/g;

    invoke-interface {v0, p1, p2, p3}, Lcom/android/settings/core/instrumentation/g;->ajJ(Landroid/content/Context;IZ)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public ajW(Landroid/content/Context;II)V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/core/instrumentation/e;->avy:Ljava/util/List;

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settings/core/instrumentation/g;

    invoke-interface {v0, p1, p2, p3}, Lcom/android/settings/core/instrumentation/g;->ajP(Landroid/content/Context;II)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public ajX(Landroid/content/Context;I)V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/core/instrumentation/e;->avy:Ljava/util/List;

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settings/core/instrumentation/g;

    invoke-interface {v0, p1, p2}, Lcom/android/settings/core/instrumentation/g;->ajN(Landroid/content/Context;I)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public ajY(Landroid/content/Context;Ljava/lang/String;I)V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/core/instrumentation/e;->avy:Ljava/util/List;

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settings/core/instrumentation/g;

    invoke-interface {v0, p1, p2, p3}, Lcom/android/settings/core/instrumentation/g;->ajM(Landroid/content/Context;Ljava/lang/String;I)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public ajZ(Landroid/content/Context;II)V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/core/instrumentation/e;->avy:Ljava/util/List;

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settings/core/instrumentation/g;

    invoke-interface {v0, p1, p2, p3}, Lcom/android/settings/core/instrumentation/g;->ajL(Landroid/content/Context;II)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public aka(Landroid/content/Context;Ljava/lang/String;I)V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/core/instrumentation/e;->avy:Ljava/util/List;

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settings/core/instrumentation/g;

    invoke-interface {v0, p1, p2, p3}, Lcom/android/settings/core/instrumentation/g;->ajO(Landroid/content/Context;Ljava/lang/String;I)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method protected akb()V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/core/instrumentation/e;->avy:Ljava/util/List;

    new-instance v1, Lcom/android/settings/core/instrumentation/d;

    invoke-direct {v1}, Lcom/android/settings/core/instrumentation/d;-><init>()V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/android/settings/core/instrumentation/e;->avy:Ljava/util/List;

    new-instance v1, Lcom/android/settings/core/instrumentation/i;

    invoke-direct {v1}, Lcom/android/settings/core/instrumentation/i;-><init>()V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method
