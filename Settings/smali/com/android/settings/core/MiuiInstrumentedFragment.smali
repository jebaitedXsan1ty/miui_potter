.class public abstract Lcom/android/settings/core/MiuiInstrumentedFragment;
.super Lcom/android/settings/core/lifecycle/MiuiObservableFragment;
.source "MiuiInstrumentedFragment.java"

# interfaces
.implements Lcom/android/settings/core/instrumentation/f;


# instance fields
.field private final avN:Lcom/android/settings/core/instrumentation/j;

.field protected mMetricsFeatureProvider:Lcom/android/settings/core/instrumentation/e;


# direct methods
.method public constructor <init>()V
    .locals 3

    invoke-direct {p0}, Lcom/android/settings/core/lifecycle/MiuiObservableFragment;-><init>()V

    new-instance v0, Lcom/android/settings/core/instrumentation/j;

    invoke-virtual {p0}, Lcom/android/settings/core/MiuiInstrumentedFragment;->getMetricsCategory()I

    move-result v1

    invoke-direct {v0, v1}, Lcom/android/settings/core/instrumentation/j;-><init>(I)V

    iput-object v0, p0, Lcom/android/settings/core/MiuiInstrumentedFragment;->avN:Lcom/android/settings/core/instrumentation/j;

    invoke-virtual {p0}, Lcom/android/settings/core/MiuiInstrumentedFragment;->getLifecycle()Lcom/android/settings/core/lifecycle/c;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/core/MiuiInstrumentedFragment;->avN:Lcom/android/settings/core/instrumentation/j;

    invoke-virtual {v0, v1}, Lcom/android/settings/core/lifecycle/c;->ajv(Lcom/android/settings/core/lifecycle/b;)Lcom/android/settings/core/lifecycle/b;

    invoke-virtual {p0}, Lcom/android/settings/core/MiuiInstrumentedFragment;->getLifecycle()Lcom/android/settings/core/lifecycle/c;

    move-result-object v0

    new-instance v1, Lcom/android/settings/a/b;

    invoke-virtual {p0}, Lcom/android/settings/core/MiuiInstrumentedFragment;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, p0, v2}, Lcom/android/settings/a/b;-><init>(Landroid/app/Fragment;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/android/settings/core/lifecycle/c;->ajv(Lcom/android/settings/core/lifecycle/b;)Lcom/android/settings/core/lifecycle/b;

    return-void
.end method

.method private akg()V
    .locals 2

    invoke-virtual {p0}, Lcom/android/settings/core/MiuiInstrumentedFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_0

    const-string/jumbo v1, ":android:show_fragment_title"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p0}, Lcom/android/settings/core/MiuiInstrumentedFragment;->getActionBar()Lmiui/app/ActionBar;

    move-result-object v1

    if-eqz v1, :cond_0

    if-lez v0, :cond_0

    invoke-virtual {v1, v0}, Landroid/app/ActionBar;->setTitle(I)V

    :cond_0
    return-void
.end method

.method private finish()V
    .locals 2

    invoke-virtual {p0}, Lcom/android/settings/core/MiuiInstrumentedFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/android/settings/core/MiuiInstrumentedFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    instance-of v0, v0, Lcom/android/settings/MiuiSettings;

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lcom/android/settings/core/MiuiInstrumentedFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/android/settings/core/MiuiInstrumentedFragment;->isResumed()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {v0}, Landroid/app/FragmentManager;->popBackStackImmediate()Z

    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0}, Lcom/android/settings/core/MiuiInstrumentedFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentManager;->popBackStack()V

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Lcom/android/settings/core/MiuiInstrumentedFragment;->isResumed()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p0}, Lcom/android/settings/core/MiuiInstrumentedFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->onBackPressed()V

    goto :goto_0

    :cond_3
    invoke-virtual {p0}, Lcom/android/settings/core/MiuiInstrumentedFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    goto :goto_0
.end method


# virtual methods
.method public onAttach(Landroid/content/Context;)V
    .locals 1

    invoke-super {p0, p1}, Lcom/android/settings/core/lifecycle/MiuiObservableFragment;->onAttach(Landroid/content/Context;)V

    invoke-static {p1}, Lcom/android/settings/overlay/a;->aIk(Landroid/content/Context;)Lcom/android/settings/overlay/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/settings/overlay/a;->aIm()Lcom/android/settings/core/instrumentation/e;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/core/MiuiInstrumentedFragment;->mMetricsFeatureProvider:Lcom/android/settings/core/instrumentation/e;

    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1

    invoke-super {p0, p1}, Lcom/android/settings/core/lifecycle/MiuiObservableFragment;->onCreate(Landroid/os/Bundle;)V

    sget v0, Lmiui/R$style;->Theme_Light:I

    invoke-virtual {p0, v0}, Lcom/android/settings/core/MiuiInstrumentedFragment;->setThemeRes(I)V

    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 1

    invoke-virtual {p0}, Lcom/android/settings/core/MiuiInstrumentedFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/android/settings/core/MiuiInstrumentedFragment;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lcom/android/settings/core/MiuiInstrumentedFragment;->onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V

    :cond_0
    const/4 v0, 0x1

    return v0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    invoke-super {p0, p1}, Lcom/android/settings/core/lifecycle/MiuiObservableFragment;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    return v0

    :pswitch_0
    invoke-direct {p0}, Lcom/android/settings/core/MiuiInstrumentedFragment;->finish()V

    const/4 v0, 0x1

    return v0

    nop

    :pswitch_data_0
    .packed-switch 0x102002c
        :pswitch_0
    .end packed-switch
.end method

.method public onResume()V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/core/MiuiInstrumentedFragment;->avN:Lcom/android/settings/core/instrumentation/j;

    invoke-virtual {p0}, Lcom/android/settings/core/MiuiInstrumentedFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/settings/core/instrumentation/j;->akd(Landroid/app/Activity;)V

    invoke-super {p0}, Lcom/android/settings/core/lifecycle/MiuiObservableFragment;->onResume()V

    return-void
.end method

.method public onStart()V
    .locals 0

    invoke-super {p0}, Lcom/android/settings/core/lifecycle/MiuiObservableFragment;->onStart()V

    invoke-direct {p0}, Lcom/android/settings/core/MiuiInstrumentedFragment;->akg()V

    return-void
.end method
