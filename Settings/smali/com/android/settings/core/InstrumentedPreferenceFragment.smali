.class public abstract Lcom/android/settings/core/InstrumentedPreferenceFragment;
.super Lcom/android/settings/core/lifecycle/ObservablePreferenceFragment;
.source "InstrumentedPreferenceFragment.java"

# interfaces
.implements Lcom/android/settings/core/instrumentation/f;


# instance fields
.field protected final avO:I

.field protected mMetricsFeatureProvider:Lcom/android/settings/core/instrumentation/e;

.field private final mVisibilityLoggerMixin:Lcom/android/settings/core/instrumentation/h;


# direct methods
.method public constructor <init>()V
    .locals 3

    invoke-direct {p0}, Lcom/android/settings/core/lifecycle/ObservablePreferenceFragment;-><init>()V

    const/16 v0, 0x2710

    iput v0, p0, Lcom/android/settings/core/InstrumentedPreferenceFragment;->avO:I

    new-instance v0, Lcom/android/settings/core/instrumentation/h;

    invoke-virtual {p0}, Lcom/android/settings/core/InstrumentedPreferenceFragment;->getMetricsCategory()I

    move-result v1

    invoke-direct {v0, v1}, Lcom/android/settings/core/instrumentation/h;-><init>(I)V

    iput-object v0, p0, Lcom/android/settings/core/InstrumentedPreferenceFragment;->mVisibilityLoggerMixin:Lcom/android/settings/core/instrumentation/h;

    invoke-virtual {p0}, Lcom/android/settings/core/InstrumentedPreferenceFragment;->getLifecycle()Lcom/android/settings/core/lifecycle/a;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/core/InstrumentedPreferenceFragment;->mVisibilityLoggerMixin:Lcom/android/settings/core/instrumentation/h;

    invoke-virtual {v0, v1}, Lcom/android/settings/core/lifecycle/a;->ajt(Lcom/android/settings/core/lifecycle/b;)Lcom/android/settings/core/lifecycle/b;

    invoke-virtual {p0}, Lcom/android/settings/core/InstrumentedPreferenceFragment;->getLifecycle()Lcom/android/settings/core/lifecycle/a;

    move-result-object v0

    new-instance v1, Lcom/android/settings/a/a;

    invoke-virtual {p0}, Lcom/android/settings/core/InstrumentedPreferenceFragment;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, p0, v2}, Lcom/android/settings/a/a;-><init>(Landroid/app/Fragment;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/android/settings/core/lifecycle/a;->ajt(Lcom/android/settings/core/lifecycle/b;)Lcom/android/settings/core/lifecycle/b;

    return-void
.end method


# virtual methods
.method public EW(Landroid/os/Bundle;Ljava/lang/String;)V
    .locals 0

    return-void
.end method

.method protected final aki()Landroid/content/Context;
    .locals 1

    invoke-virtual {p0}, Lcom/android/settings/core/InstrumentedPreferenceFragment;->dju()Landroid/support/v7/preference/k;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v7/preference/k;->getContext()Landroid/content/Context;

    move-result-object v0

    return-object v0
.end method

.method public onAttach(Landroid/content/Context;)V
    .locals 1

    invoke-super {p0, p1}, Lcom/android/settings/core/lifecycle/ObservablePreferenceFragment;->onAttach(Landroid/content/Context;)V

    invoke-static {p1}, Lcom/android/settings/overlay/a;->aIk(Landroid/content/Context;)Lcom/android/settings/overlay/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/settings/overlay/a;->aIm()Lcom/android/settings/core/instrumentation/e;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/core/InstrumentedPreferenceFragment;->mMetricsFeatureProvider:Lcom/android/settings/core/instrumentation/e;

    return-void
.end method

.method public onResume()V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/core/InstrumentedPreferenceFragment;->mVisibilityLoggerMixin:Lcom/android/settings/core/instrumentation/h;

    invoke-virtual {p0}, Lcom/android/settings/core/InstrumentedPreferenceFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/settings/core/instrumentation/h;->akc(Landroid/app/Activity;)V

    invoke-super {p0}, Lcom/android/settings/core/lifecycle/ObservablePreferenceFragment;->onResume()V

    return-void
.end method
