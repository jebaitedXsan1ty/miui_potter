.class public Lcom/android/settings/core/lifecycle/MiuiObservableFragment;
.super Lmiui/app/Fragment;
.source "MiuiObservableFragment.java"


# instance fields
.field private final avq:Lcom/android/settings/core/lifecycle/c;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lmiui/app/Fragment;-><init>()V

    new-instance v0, Lcom/android/settings/core/lifecycle/c;

    invoke-direct {v0}, Lcom/android/settings/core/lifecycle/c;-><init>()V

    iput-object v0, p0, Lcom/android/settings/core/lifecycle/MiuiObservableFragment;->avq:Lcom/android/settings/core/lifecycle/c;

    return-void
.end method


# virtual methods
.method protected getLifecycle()Lcom/android/settings/core/lifecycle/c;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/core/lifecycle/MiuiObservableFragment;->avq:Lcom/android/settings/core/lifecycle/c;

    return-object v0
.end method

.method public onAttach(Landroid/content/Context;)V
    .locals 1

    invoke-super {p0, p1}, Lmiui/app/Fragment;->onAttach(Landroid/content/Context;)V

    iget-object v0, p0, Lcom/android/settings/core/lifecycle/MiuiObservableFragment;->avq:Lcom/android/settings/core/lifecycle/c;

    invoke-virtual {v0, p1}, Lcom/android/settings/core/lifecycle/c;->onAttach(Landroid/content/Context;)V

    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1

    iget-object v0, p0, Lcom/android/settings/core/lifecycle/MiuiObservableFragment;->avq:Lcom/android/settings/core/lifecycle/c;

    invoke-virtual {v0, p1}, Lcom/android/settings/core/lifecycle/c;->onCreate(Landroid/os/Bundle;)V

    invoke-super {p0, p1}, Lmiui/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V
    .locals 1

    iget-object v0, p0, Lcom/android/settings/core/lifecycle/MiuiObservableFragment;->avq:Lcom/android/settings/core/lifecycle/c;

    invoke-virtual {v0, p1, p2}, Lcom/android/settings/core/lifecycle/c;->onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V

    invoke-super {p0, p1, p2}, Lmiui/app/Fragment;->onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V

    return-void
.end method

.method public onDestroy()V
    .locals 1

    iget-object v0, p0, Lcom/android/settings/core/lifecycle/MiuiObservableFragment;->avq:Lcom/android/settings/core/lifecycle/c;

    invoke-virtual {v0}, Lcom/android/settings/core/lifecycle/c;->onDestroy()V

    invoke-super {p0}, Lmiui/app/Fragment;->onDestroy()V

    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1

    iget-object v0, p0, Lcom/android/settings/core/lifecycle/MiuiObservableFragment;->avq:Lcom/android/settings/core/lifecycle/c;

    invoke-virtual {v0, p1}, Lcom/android/settings/core/lifecycle/c;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-super {p0, p1}, Lmiui/app/Fragment;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    return v0

    :cond_0
    return v0
.end method

.method public onPause()V
    .locals 1

    iget-object v0, p0, Lcom/android/settings/core/lifecycle/MiuiObservableFragment;->avq:Lcom/android/settings/core/lifecycle/c;

    invoke-virtual {v0}, Lcom/android/settings/core/lifecycle/c;->onPause()V

    invoke-super {p0}, Lmiui/app/Fragment;->onPause()V

    return-void
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)V
    .locals 1

    iget-object v0, p0, Lcom/android/settings/core/lifecycle/MiuiObservableFragment;->avq:Lcom/android/settings/core/lifecycle/c;

    invoke-virtual {v0, p1}, Lcom/android/settings/core/lifecycle/c;->onPrepareOptionsMenu(Landroid/view/Menu;)V

    invoke-super {p0, p1}, Lmiui/app/Fragment;->onPrepareOptionsMenu(Landroid/view/Menu;)V

    return-void
.end method

.method public onResume()V
    .locals 1

    iget-object v0, p0, Lcom/android/settings/core/lifecycle/MiuiObservableFragment;->avq:Lcom/android/settings/core/lifecycle/c;

    invoke-virtual {v0}, Lcom/android/settings/core/lifecycle/c;->onResume()V

    invoke-super {p0}, Lmiui/app/Fragment;->onResume()V

    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 1

    invoke-super {p0, p1}, Lmiui/app/Fragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    iget-object v0, p0, Lcom/android/settings/core/lifecycle/MiuiObservableFragment;->avq:Lcom/android/settings/core/lifecycle/c;

    invoke-virtual {v0, p1}, Lcom/android/settings/core/lifecycle/c;->onSaveInstanceState(Landroid/os/Bundle;)V

    return-void
.end method

.method public onStart()V
    .locals 1

    iget-object v0, p0, Lcom/android/settings/core/lifecycle/MiuiObservableFragment;->avq:Lcom/android/settings/core/lifecycle/c;

    invoke-virtual {v0}, Lcom/android/settings/core/lifecycle/c;->onStart()V

    invoke-super {p0}, Lmiui/app/Fragment;->onStart()V

    return-void
.end method

.method public onStop()V
    .locals 1

    iget-object v0, p0, Lcom/android/settings/core/lifecycle/MiuiObservableFragment;->avq:Lcom/android/settings/core/lifecycle/c;

    invoke-virtual {v0}, Lcom/android/settings/core/lifecycle/c;->onStop()V

    invoke-super {p0}, Lmiui/app/Fragment;->onStop()V

    return-void
.end method
