.class public Lcom/android/settings/RestrictedListPreference;
.super Lcom/android/settings/CustomListPreference;
.source "RestrictedListPreference.java"


# instance fields
.field private final bRQ:Lcom/android/settingslib/m;

.field private final bRR:Ljava/util/List;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    invoke-direct {p0, p1, p2}, Lcom/android/settings/CustomListPreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/settings/RestrictedListPreference;->bRR:Ljava/util/List;

    const v0, 0x7f0d0190

    invoke-virtual {p0, v0}, Lcom/android/settings/RestrictedListPreference;->dkS(I)V

    new-instance v0, Lcom/android/settingslib/m;

    invoke-direct {v0, p1, p0, p2}, Lcom/android/settingslib/m;-><init>(Landroid/content/Context;Landroid/support/v7/preference/Preference;Landroid/util/AttributeSet;)V

    iput-object v0, p0, Lcom/android/settings/RestrictedListPreference;->bRQ:Lcom/android/settingslib/m;

    return-void
.end method

.method private bKC(Ljava/lang/CharSequence;)Lcom/android/settings/bz;
    .locals 4

    const/4 v3, 0x0

    if-nez p1, :cond_0

    return-object v3

    :cond_0
    iget-object v0, p0, Lcom/android/settings/RestrictedListPreference;->bRR:Ljava/util/List;

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settings/bz;

    iget-object v2, v0, Lcom/android/settings/bz;->bRX:Ljava/lang/CharSequence;

    invoke-virtual {p1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    return-object v0

    :cond_2
    return-object v3
.end method

.method static synthetic bKG(Lcom/android/settings/RestrictedListPreference;Ljava/lang/CharSequence;)Lcom/android/settings/bz;
    .locals 1

    invoke-direct {p0, p1}, Lcom/android/settings/RestrictedListPreference;->bKC(Ljava/lang/CharSequence;)Lcom/android/settings/bz;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public al(Landroid/support/v7/preference/l;)V
    .locals 2

    invoke-super {p0, p1}, Lcom/android/settings/CustomListPreference;->al(Landroid/support/v7/preference/l;)V

    iget-object v0, p0, Lcom/android/settings/RestrictedListPreference;->bRQ:Lcom/android/settingslib/m;

    invoke-virtual {v0, p1}, Lcom/android/settingslib/m;->cpW(Landroid/support/v7/preference/l;)V

    const v0, 0x7f0a038d

    invoke-virtual {p1, v0}, Landroid/support/v7/preference/l;->dma(I)Landroid/view/View;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/android/settings/RestrictedListPreference;->bKE()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    :cond_0
    return-void

    :cond_1
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public bKD()I
    .locals 1

    invoke-virtual {p0}, Lcom/android/settings/RestrictedListPreference;->getValue()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, -0x1

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0, v0}, Lcom/android/settings/RestrictedListPreference;->dmu(Ljava/lang/String;)I

    move-result v0

    goto :goto_0
.end method

.method public bKE()Z
    .locals 1

    iget-object v0, p0, Lcom/android/settings/RestrictedListPreference;->bRQ:Lcom/android/settingslib/m;

    invoke-virtual {v0}, Lcom/android/settingslib/m;->cpZ()Z

    move-result v0

    return v0
.end method

.method public bKF(Ljava/lang/CharSequence;)Z
    .locals 3

    const/4 v2, 0x0

    if-nez p1, :cond_0

    return v2

    :cond_0
    iget-object v0, p0, Lcom/android/settings/RestrictedListPreference;->bRR:Ljava/util/List;

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settings/bz;

    iget-object v0, v0, Lcom/android/settings/bz;->bRW:Ljava/lang/CharSequence;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    return v0

    :cond_2
    return v2
.end method

.method protected fL()Landroid/widget/ListAdapter;
    .locals 4

    new-instance v0, Lcom/android/settings/by;

    invoke-virtual {p0}, Lcom/android/settings/RestrictedListPreference;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {p0}, Lcom/android/settings/RestrictedListPreference;->dmx()[Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {p0}, Lcom/android/settings/RestrictedListPreference;->bKD()I

    move-result v3

    invoke-direct {v0, p0, v1, v2, v3}, Lcom/android/settings/by;-><init>(Lcom/android/settings/RestrictedListPreference;Landroid/content/Context;[Ljava/lang/CharSequence;I)V

    return-object v0
.end method

.method protected fP(Landroid/app/AlertDialog$Builder;Landroid/content/DialogInterface$OnClickListener;)V
    .locals 1

    invoke-virtual {p0}, Lcom/android/settings/RestrictedListPreference;->fL()Landroid/widget/ListAdapter;

    move-result-object v0

    invoke-virtual {p1, v0, p2}, Landroid/app/AlertDialog$Builder;->setAdapter(Landroid/widget/ListAdapter;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    return-void
.end method

.method public performClick()V
    .locals 1

    iget-object v0, p0, Lcom/android/settings/RestrictedListPreference;->bRQ:Lcom/android/settingslib/m;

    invoke-virtual {v0}, Lcom/android/settingslib/m;->performClick()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-super {p0}, Lcom/android/settings/CustomListPreference;->performClick()V

    :cond_0
    return-void
.end method

.method public setEnabled(Z)V
    .locals 2

    if-eqz p1, :cond_0

    invoke-virtual {p0}, Lcom/android/settings/RestrictedListPreference;->bKE()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/RestrictedListPreference;->bRQ:Lcom/android/settingslib/m;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/android/settingslib/m;->setDisabledByAdmin(Lcom/android/settingslib/n;)Z

    return-void

    :cond_0
    invoke-super {p0, p1}, Lcom/android/settings/CustomListPreference;->setEnabled(Z)V

    return-void
.end method
