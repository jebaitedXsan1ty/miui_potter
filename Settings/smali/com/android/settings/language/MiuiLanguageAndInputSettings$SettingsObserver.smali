.class Lcom/android/settings/language/MiuiLanguageAndInputSettings$SettingsObserver;
.super Landroid/database/ContentObserver;
.source "MiuiLanguageAndInputSettings.java"


# instance fields
.field final synthetic blP:Lcom/android/settings/language/MiuiLanguageAndInputSettings;

.field private mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>(Lcom/android/settings/language/MiuiLanguageAndInputSettings;Landroid/os/Handler;Landroid/content/Context;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/language/MiuiLanguageAndInputSettings$SettingsObserver;->blP:Lcom/android/settings/language/MiuiLanguageAndInputSettings;

    invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    iput-object p3, p0, Lcom/android/settings/language/MiuiLanguageAndInputSettings$SettingsObserver;->mContext:Landroid/content/Context;

    return-void
.end method


# virtual methods
.method public aZk()V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/android/settings/language/MiuiLanguageAndInputSettings$SettingsObserver;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v1, "default_input_method"

    invoke-static {v1}, Landroid/provider/Settings$Secure;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1, v2, p0}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    const-string/jumbo v1, "selected_input_method_subtype"

    invoke-static {v1}, Landroid/provider/Settings$Secure;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1, v2, p0}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    return-void
.end method

.method public onChange(Z)V
    .locals 1

    iget-object v0, p0, Lcom/android/settings/language/MiuiLanguageAndInputSettings$SettingsObserver;->blP:Lcom/android/settings/language/MiuiLanguageAndInputSettings;

    invoke-static {v0}, Lcom/android/settings/language/MiuiLanguageAndInputSettings;->aZj(Lcom/android/settings/language/MiuiLanguageAndInputSettings;)V

    return-void
.end method

.method public pause()V
    .locals 1

    iget-object v0, p0, Lcom/android/settings/language/MiuiLanguageAndInputSettings$SettingsObserver;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    return-void
.end method
