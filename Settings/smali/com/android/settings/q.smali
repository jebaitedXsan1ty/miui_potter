.class Lcom/android/settings/q;
.super Landroid/os/AsyncTask;
.source "TrustedCredentialsSettings.java"


# instance fields
.field private final bvl:Lcom/android/settings/p;

.field final synthetic bvm:Lcom/android/settings/TrustedCredentialsSettings;


# direct methods
.method private constructor <init>(Lcom/android/settings/TrustedCredentialsSettings;Lcom/android/settings/p;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/q;->bvm:Lcom/android/settings/TrustedCredentialsSettings;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    iput-object p2, p0, Lcom/android/settings/q;->bvl:Lcom/android/settings/p;

    invoke-static {p1, p0}, Lcom/android/settings/TrustedCredentialsSettings;->bhc(Lcom/android/settings/TrustedCredentialsSettings;Lcom/android/settings/q;)Lcom/android/settings/q;

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/settings/TrustedCredentialsSettings;Lcom/android/settings/p;Lcom/android/settings/q;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/android/settings/q;-><init>(Lcom/android/settings/TrustedCredentialsSettings;Lcom/android/settings/p;)V

    return-void
.end method


# virtual methods
.method protected bil(Ljava/lang/Boolean;)V
    .locals 2

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/settings/q;->bvl:Lcom/android/settings/p;

    invoke-static {v0}, Lcom/android/settings/p;->bii(Lcom/android/settings/p;)Lcom/android/settings/TrustedCredentialsSettings$Tab;

    move-result-object v0

    invoke-static {v0}, Lcom/android/settings/TrustedCredentialsSettings$Tab;->bhn(Lcom/android/settings/TrustedCredentialsSettings$Tab;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/q;->bvl:Lcom/android/settings/p;

    iget-object v1, p0, Lcom/android/settings/q;->bvl:Lcom/android/settings/p;

    invoke-static {v1}, Lcom/android/settings/p;->bif(Lcom/android/settings/p;)Z

    move-result v1

    xor-int/lit8 v1, v1, 0x1

    invoke-static {v0, v1}, Lcom/android/settings/p;->bik(Lcom/android/settings/p;Z)Z

    :goto_0
    iget-object v0, p0, Lcom/android/settings/q;->bvl:Lcom/android/settings/p;

    invoke-static {v0}, Lcom/android/settings/p;->bid(Lcom/android/settings/p;)Lcom/android/settings/k;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/settings/k;->notifyDataSetChanged()V

    :goto_1
    iget-object v0, p0, Lcom/android/settings/q;->bvm:Lcom/android/settings/TrustedCredentialsSettings;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/android/settings/TrustedCredentialsSettings;->bhc(Lcom/android/settings/TrustedCredentialsSettings;Lcom/android/settings/q;)Lcom/android/settings/q;

    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/settings/q;->bvl:Lcom/android/settings/p;

    invoke-static {v0}, Lcom/android/settings/p;->bid(Lcom/android/settings/p;)Lcom/android/settings/k;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/q;->bvl:Lcom/android/settings/p;

    invoke-virtual {v0, v1}, Lcom/android/settings/k;->bhz(Lcom/android/settings/p;)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/android/settings/q;->bvl:Lcom/android/settings/p;

    invoke-static {v0}, Lcom/android/settings/p;->bid(Lcom/android/settings/p;)Lcom/android/settings/k;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/settings/k;->load()V

    goto :goto_1
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Boolean;
    .locals 4

    :try_start_0
    iget-object v0, p0, Lcom/android/settings/q;->bvm:Lcom/android/settings/TrustedCredentialsSettings;

    invoke-static {v0}, Lcom/android/settings/TrustedCredentialsSettings;->bgY(Lcom/android/settings/TrustedCredentialsSettings;)Landroid/util/SparseArray;

    move-result-object v1

    monitor-enter v1
    :try_end_0
    .catch Ljava/security/cert/CertificateEncodingException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :try_start_1
    iget-object v0, p0, Lcom/android/settings/q;->bvm:Lcom/android/settings/TrustedCredentialsSettings;

    invoke-static {v0}, Lcom/android/settings/TrustedCredentialsSettings;->bgY(Lcom/android/settings/TrustedCredentialsSettings;)Landroid/util/SparseArray;

    move-result-object v0

    iget-object v2, p0, Lcom/android/settings/q;->bvl:Lcom/android/settings/p;

    iget v2, v2, Lcom/android/settings/p;->bve:I

    invoke-virtual {v0, v2}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/security/KeyChain$KeyChainConnection;

    invoke-virtual {v0}, Landroid/security/KeyChain$KeyChainConnection;->getService()Landroid/security/IKeyChainService;

    move-result-object v0

    iget-object v2, p0, Lcom/android/settings/q;->bvl:Lcom/android/settings/p;

    invoke-static {v2}, Lcom/android/settings/p;->bif(Lcom/android/settings/p;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/android/settings/q;->bvl:Lcom/android/settings/p;

    invoke-static {v2}, Lcom/android/settings/p;->bij(Lcom/android/settings/p;)Ljava/security/cert/X509Certificate;

    move-result-object v2

    invoke-virtual {v2}, Ljava/security/cert/X509Certificate;->getEncoded()[B

    move-result-object v2

    invoke-interface {v0, v2}, Landroid/security/IKeyChainService;->installCaCertificate([B)Ljava/lang/String;

    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    :try_start_2
    monitor-exit v1
    :try_end_2
    .catch Ljava/security/cert/CertificateEncodingException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/SecurityException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/IllegalStateException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_0

    return-object v0

    :cond_0
    :try_start_3
    iget-object v2, p0, Lcom/android/settings/q;->bvl:Lcom/android/settings/p;

    invoke-static {v2}, Lcom/android/settings/p;->bie(Lcom/android/settings/p;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2}, Landroid/security/IKeyChainService;->deleteCaCertificate(Ljava/lang/String;)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result-object v0

    :try_start_4
    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
    :try_end_4
    .catch Ljava/security/cert/CertificateEncodingException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/lang/SecurityException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/lang/IllegalStateException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Landroid/os/RemoteException; {:try_start_4 .. :try_end_4} :catch_0

    :catch_0
    move-exception v0

    const-string/jumbo v1, "TrustedCredentialsSettings"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "Error while toggling alias "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/settings/q;->bvl:Lcom/android/settings/p;

    invoke-static {v3}, Lcom/android/settings/p;->bie(Lcom/android/settings/p;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/android/settings/q;->doInBackground([Ljava/lang/Void;)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Lcom/android/settings/q;->bil(Ljava/lang/Boolean;)V

    return-void
.end method
