.class public Lcom/android/settings/TrustedCredentialsSettings;
.super Lcom/android/settings/MiuiOptionsMenuFragment;
.source "TrustedCredentialsSettings.java"

# interfaces
.implements Lcom/android/settings/z;


# instance fields
.field private bul:Ljava/util/Set;

.field private bum:Lcom/android/settings/q;

.field private bun:Landroid/util/ArraySet;

.field private buo:Ljava/util/function/IntConsumer;

.field private bup:I

.field private buq:Ljava/util/ArrayList;

.field private final bur:Landroid/util/SparseArray;

.field private bus:Landroid/app/KeyguardManager;

.field private but:Landroid/widget/TabHost;

.field private buu:I

.field private buv:Landroid/content/BroadcastReceiver;

.field private mUserManager:Landroid/os/UserManager;


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x2

    invoke-direct {p0}, Lcom/android/settings/MiuiOptionsMenuFragment;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/android/settings/TrustedCredentialsSettings;->buq:Ljava/util/ArrayList;

    new-instance v0, Landroid/util/ArraySet;

    invoke-direct {v0, v1}, Landroid/util/ArraySet;-><init>(I)V

    iput-object v0, p0, Lcom/android/settings/TrustedCredentialsSettings;->bul:Ljava/util/Set;

    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/android/settings/TrustedCredentialsSettings;->bur:Landroid/util/SparseArray;

    new-instance v0, Lcom/android/settings/dz;

    invoke-direct {v0, p0}, Lcom/android/settings/dz;-><init>(Lcom/android/settings/TrustedCredentialsSettings;)V

    iput-object v0, p0, Lcom/android/settings/TrustedCredentialsSettings;->buv:Landroid/content/BroadcastReceiver;

    return-void
.end method

.method private bgN(Lcom/android/settings/TrustedCredentialsSettings$Tab;)V
    .locals 11

    const/4 v5, 0x1

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/android/settings/TrustedCredentialsSettings;->but:Landroid/widget/TabHost;

    invoke-static {p1}, Lcom/android/settings/TrustedCredentialsSettings$Tab;->bho(Lcom/android/settings/TrustedCredentialsSettings$Tab;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TabHost;->newTabSpec(Ljava/lang/String;)Landroid/widget/TabHost$TabSpec;

    move-result-object v0

    invoke-virtual {p0}, Lcom/android/settings/TrustedCredentialsSettings;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-static {p1}, Lcom/android/settings/TrustedCredentialsSettings$Tab;->bhl(Lcom/android/settings/TrustedCredentialsSettings$Tab;)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TabHost$TabSpec;->setIndicator(Ljava/lang/CharSequence;)Landroid/widget/TabHost$TabSpec;

    move-result-object v0

    invoke-static {p1}, Lcom/android/settings/TrustedCredentialsSettings$Tab;->bhp(Lcom/android/settings/TrustedCredentialsSettings$Tab;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TabHost$TabSpec;->setContent(I)Landroid/widget/TabHost$TabSpec;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/TrustedCredentialsSettings;->but:Landroid/widget/TabHost;

    invoke-virtual {v1, v0}, Landroid/widget/TabHost;->addTab(Landroid/widget/TabHost$TabSpec;)V

    new-instance v6, Lcom/android/settings/k;

    const/4 v0, 0x0

    invoke-direct {v6, p0, p1, v0}, Lcom/android/settings/k;-><init>(Lcom/android/settings/TrustedCredentialsSettings;Lcom/android/settings/TrustedCredentialsSettings$Tab;Lcom/android/settings/k;)V

    iget-object v0, p0, Lcom/android/settings/TrustedCredentialsSettings;->buq:Ljava/util/ArrayList;

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual {v6}, Lcom/android/settings/k;->getGroupCount()I

    move-result v7

    iget-object v0, p0, Lcom/android/settings/TrustedCredentialsSettings;->but:Landroid/widget/TabHost;

    invoke-static {p1}, Lcom/android/settings/TrustedCredentialsSettings$Tab;->bhk(Lcom/android/settings/TrustedCredentialsSettings$Tab;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TabHost;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getLayoutTransition()Landroid/animation/LayoutTransition;

    move-result-object v1

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Landroid/animation/LayoutTransition;->enableTransitionType(I)V

    invoke-virtual {p0}, Lcom/android/settings/TrustedCredentialsSettings;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v8

    move v2, v3

    :goto_0
    invoke-virtual {v6}, Lcom/android/settings/k;->getGroupCount()I

    move-result v1

    if-ge v2, v1, :cond_3

    invoke-virtual {v6, v2}, Lcom/android/settings/k;->bhx(I)Landroid/content/pm/UserInfo;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/pm/UserInfo;->isManagedProfile()Z

    move-result v9

    invoke-virtual {v6, v2}, Lcom/android/settings/k;->bhv(I)Lcom/android/settings/m;

    move-result-object v10

    const v1, 0x7f0d024d

    invoke-virtual {v8, v1, v0, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    invoke-virtual {v10, v1}, Lcom/android/settings/m;->bhJ(Landroid/widget/LinearLayout;)V

    if-le v7, v5, :cond_0

    move v4, v5

    :goto_1
    invoke-virtual {v10, v4}, Lcom/android/settings/m;->bhM(Z)V

    invoke-virtual {v10, v9}, Lcom/android/settings/m;->bhL(Z)V

    const/4 v4, 0x2

    if-gt v7, v4, :cond_1

    move v4, v5

    :goto_2
    invoke-virtual {v10, v4}, Lcom/android/settings/m;->bhK(Z)V

    if-eqz v9, :cond_2

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    :goto_3
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_0

    :cond_0
    move v4, v3

    goto :goto_1

    :cond_1
    xor-int/lit8 v4, v9, 0x1

    goto :goto_2

    :cond_2
    invoke-virtual {v0, v1, v3}, Landroid/view/ViewGroup;->addView(Landroid/view/View;I)V

    goto :goto_3

    :cond_3
    return-void
.end method

.method private bgO()V
    .locals 4

    iget-object v2, p0, Lcom/android/settings/TrustedCredentialsSettings;->bur:Landroid/util/SparseArray;

    monitor-enter v2

    :try_start_0
    iget-object v0, p0, Lcom/android/settings/TrustedCredentialsSettings;->bur:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    iget-object v0, p0, Lcom/android/settings/TrustedCredentialsSettings;->bur:Landroid/util/SparseArray;

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/security/KeyChain$KeyChainConnection;

    invoke-virtual {v0}, Landroid/security/KeyChain$KeyChainConnection;->close()V

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/android/settings/TrustedCredentialsSettings;->bur:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->clear()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v2

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0
.end method

.method private bgQ()Z
    .locals 2

    iget v0, p0, Lcom/android/settings/TrustedCredentialsSettings;->buu:I

    const/16 v1, -0x2710

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private bgS(Lcom/android/settings/p;)V
    .locals 2

    new-instance v0, Lcom/android/settings/y;

    invoke-virtual {p0}, Lcom/android/settings/TrustedCredentialsSettings;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, v1, p0}, Lcom/android/settings/y;-><init>(Landroid/app/Activity;Lcom/android/settings/z;)V

    invoke-virtual {v0, p1}, Lcom/android/settings/y;->bjx(Lcom/android/settings/p;)Lcom/android/settings/y;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/settings/y;->show()Landroid/app/AlertDialog;

    return-void
.end method

.method private bgT(Ljava/util/List;)V
    .locals 3

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [Lcom/android/settings/p;

    invoke-interface {p1, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/android/settings/p;

    new-instance v1, Lcom/android/settings/y;

    invoke-virtual {p0}, Lcom/android/settings/TrustedCredentialsSettings;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-direct {v1, v2, p0}, Lcom/android/settings/y;-><init>(Landroid/app/Activity;Lcom/android/settings/z;)V

    invoke-virtual {v1, v0}, Lcom/android/settings/y;->bjw([Lcom/android/settings/p;)Lcom/android/settings/y;

    move-result-object v0

    new-instance v1, Lcom/android/settings/dA;

    invoke-direct {v1, p0}, Lcom/android/settings/dA;-><init>(Lcom/android/settings/TrustedCredentialsSettings;)V

    invoke-virtual {v0, v1}, Lcom/android/settings/y;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    return-void
.end method

.method private bgU(I)Z
    .locals 3

    const/4 v2, 0x1

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/android/settings/TrustedCredentialsSettings;->bus:Landroid/app/KeyguardManager;

    invoke-virtual {v0, v1, v1, p1}, Landroid/app/KeyguardManager;->createConfirmDeviceCredentialIntent(Ljava/lang/CharSequence;Ljava/lang/CharSequence;I)Landroid/content/Intent;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    return v0

    :cond_0
    iput p1, p0, Lcom/android/settings/TrustedCredentialsSettings;->bup:I

    invoke-virtual {p0, v0, v2}, Lcom/android/settings/TrustedCredentialsSettings;->startActivityForResult(Landroid/content/Intent;I)V

    return v2
.end method

.method static synthetic bgW(Lcom/android/settings/TrustedCredentialsSettings;)Ljava/util/Set;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/TrustedCredentialsSettings;->bul:Ljava/util/Set;

    return-object v0
.end method

.method static synthetic bgX(Lcom/android/settings/TrustedCredentialsSettings;)Ljava/util/ArrayList;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/TrustedCredentialsSettings;->buq:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic bgY(Lcom/android/settings/TrustedCredentialsSettings;)Landroid/util/SparseArray;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/TrustedCredentialsSettings;->bur:Landroid/util/SparseArray;

    return-object v0
.end method

.method static synthetic bgZ(Lcom/android/settings/TrustedCredentialsSettings;)Landroid/widget/TabHost;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/TrustedCredentialsSettings;->but:Landroid/widget/TabHost;

    return-object v0
.end method

.method static synthetic bha(Lcom/android/settings/TrustedCredentialsSettings;)I
    .locals 1

    iget v0, p0, Lcom/android/settings/TrustedCredentialsSettings;->buu:I

    return v0
.end method

.method static synthetic bhb(Lcom/android/settings/TrustedCredentialsSettings;)Landroid/os/UserManager;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/TrustedCredentialsSettings;->mUserManager:Landroid/os/UserManager;

    return-object v0
.end method

.method static synthetic bhc(Lcom/android/settings/TrustedCredentialsSettings;Lcom/android/settings/q;)Lcom/android/settings/q;
    .locals 0

    iput-object p1, p0, Lcom/android/settings/TrustedCredentialsSettings;->bum:Lcom/android/settings/q;

    return-object p1
.end method

.method static synthetic bhd(Lcom/android/settings/TrustedCredentialsSettings;I)I
    .locals 0

    iput p1, p0, Lcom/android/settings/TrustedCredentialsSettings;->buu:I

    return p1
.end method

.method static synthetic bhe(Lcom/android/settings/TrustedCredentialsSettings;)Z
    .locals 1

    invoke-direct {p0}, Lcom/android/settings/TrustedCredentialsSettings;->bgQ()Z

    move-result v0

    return v0
.end method

.method static synthetic bhf(Lcom/android/settings/TrustedCredentialsSettings;I)Z
    .locals 1

    invoke-direct {p0, p1}, Lcom/android/settings/TrustedCredentialsSettings;->bgU(I)Z

    move-result v0

    return v0
.end method

.method static synthetic bhg(Lcom/android/settings/TrustedCredentialsSettings;Lcom/android/settings/p;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/settings/TrustedCredentialsSettings;->bgS(Lcom/android/settings/p;)V

    return-void
.end method

.method static synthetic bhh(Lcom/android/settings/TrustedCredentialsSettings;Ljava/util/List;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/settings/TrustedCredentialsSettings;->bgT(Ljava/util/List;)V

    return-void
.end method


# virtual methods
.method public bgP(Lcom/android/settings/p;)Ljava/util/List;
    .locals 8

    const/4 v2, 0x0

    :try_start_0
    iget-object v3, p0, Lcom/android/settings/TrustedCredentialsSettings;->bur:Landroid/util/SparseArray;

    monitor-enter v3
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_1

    :try_start_1
    iget-object v0, p0, Lcom/android/settings/TrustedCredentialsSettings;->bur:Landroid/util/SparseArray;

    iget v1, p1, Lcom/android/settings/p;->bve:I

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/security/KeyChain$KeyChainConnection;

    invoke-virtual {v0}, Landroid/security/KeyChain$KeyChainConnection;->getService()Landroid/security/IKeyChainService;

    move-result-object v4

    invoke-static {p1}, Lcom/android/settings/p;->bie(Lcom/android/settings/p;)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    invoke-interface {v4, v0, v1}, Landroid/security/IKeyChainService;->getCaCertificateChainAliases(Ljava/lang/String;Z)Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v6

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1, v6}, Ljava/util/ArrayList;-><init>(I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    const/4 v0, 0x0

    move v2, v0

    :goto_0
    if-ge v2, v6, :cond_0

    :try_start_2
    invoke-interface {v5, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const/4 v7, 0x1

    invoke-interface {v4, v0, v7}, Landroid/security/IKeyChainService;->getEncodedCaCertificate(Ljava/lang/String;Z)[B

    move-result-object v0

    invoke-static {v0}, Landroid/security/KeyChain;->toCertificate([B)Ljava/security/cert/X509Certificate;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_0
    :try_start_3
    monitor-exit v3

    :goto_1
    return-object v1

    :catchall_0
    move-exception v0

    move-object v1, v2

    :goto_2
    monitor-exit v3

    throw v0
    :try_end_3
    .catch Landroid/os/RemoteException; {:try_start_3 .. :try_end_3} :catch_0

    :catch_0
    move-exception v0

    :goto_3
    const-string/jumbo v2, "TrustedCredentialsSettings"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "RemoteException while retrieving certificate chain for root "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {p1}, Lcom/android/settings/p;->bie(Lcom/android/settings/p;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1

    :catch_1
    move-exception v0

    move-object v1, v2

    goto :goto_3

    :catchall_1
    move-exception v0

    goto :goto_2
.end method

.method public bgR(Lcom/android/settings/p;)V
    .locals 2

    new-instance v0, Lcom/android/settings/q;

    const/4 v1, 0x0

    invoke-direct {v0, p0, p1, v1}, Lcom/android/settings/q;-><init>(Lcom/android/settings/TrustedCredentialsSettings;Lcom/android/settings/p;Lcom/android/settings/q;)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/android/settings/q;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    return-void
.end method

.method public bgV(ILjava/util/function/IntConsumer;)Z
    .locals 2

    iget-object v0, p0, Lcom/android/settings/TrustedCredentialsSettings;->bun:Landroid/util/ArraySet;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/util/ArraySet;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    return v0

    :cond_0
    invoke-direct {p0, p1}, Lcom/android/settings/TrustedCredentialsSettings;->bgU(I)Z

    move-result v0

    if-eqz v0, :cond_1

    iput-object p2, p0, Lcom/android/settings/TrustedCredentialsSettings;->buo:Ljava/util/function/IntConsumer;

    :cond_1
    return v0
.end method

.method public getMetricsCategory()I
    .locals 1

    const/16 v0, 0x5c

    return v0
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 4

    const/4 v3, 0x0

    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    iget v0, p0, Lcom/android/settings/TrustedCredentialsSettings;->bup:I

    iget-object v1, p0, Lcom/android/settings/TrustedCredentialsSettings;->buo:Ljava/util/function/IntConsumer;

    const/16 v2, -0x2710

    iput v2, p0, Lcom/android/settings/TrustedCredentialsSettings;->bup:I

    iput-object v3, p0, Lcom/android/settings/TrustedCredentialsSettings;->buo:Ljava/util/function/IntConsumer;

    const/4 v2, -0x1

    if-ne p2, v2, :cond_0

    iget-object v2, p0, Lcom/android/settings/TrustedCredentialsSettings;->bun:Landroid/util/ArraySet;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/util/ArraySet;->add(Ljava/lang/Object;)Z

    if-eqz v1, :cond_0

    invoke-interface {v1, v0}, Ljava/util/function/IntConsumer;->accept(I)V

    :cond_0
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 4

    const/4 v3, 0x0

    const/16 v2, -0x2710

    invoke-super {p0, p1}, Lcom/android/settings/MiuiOptionsMenuFragment;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/android/settings/TrustedCredentialsSettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const-string/jumbo v1, "user"

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/UserManager;

    iput-object v0, p0, Lcom/android/settings/TrustedCredentialsSettings;->mUserManager:Landroid/os/UserManager;

    invoke-virtual {p0}, Lcom/android/settings/TrustedCredentialsSettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const-string/jumbo v1, "keyguard"

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/KeyguardManager;

    iput-object v0, p0, Lcom/android/settings/TrustedCredentialsSettings;->bus:Landroid/app/KeyguardManager;

    invoke-virtual {p0}, Lcom/android/settings/TrustedCredentialsSettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "ARG_SHOW_NEW_FOR_USER"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/android/settings/TrustedCredentialsSettings;->buu:I

    new-instance v0, Landroid/util/ArraySet;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, Landroid/util/ArraySet;-><init>(I)V

    iput-object v0, p0, Lcom/android/settings/TrustedCredentialsSettings;->bun:Landroid/util/ArraySet;

    iput v2, p0, Lcom/android/settings/TrustedCredentialsSettings;->bup:I

    if-eqz p1, :cond_0

    const-string/jumbo v0, "ConfirmingCredentialUser"

    invoke-virtual {p1, v0, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/android/settings/TrustedCredentialsSettings;->bup:I

    const-string/jumbo v0, "ConfirmedCredentialUsers"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getIntegerArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/android/settings/TrustedCredentialsSettings;->bun:Landroid/util/ArraySet;

    invoke-virtual {v1, v0}, Landroid/util/ArraySet;->addAll(Ljava/util/Collection;)Z

    :cond_0
    iput-object v3, p0, Lcom/android/settings/TrustedCredentialsSettings;->buo:Ljava/util/function/IntConsumer;

    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    const-string/jumbo v1, "android.intent.action.MANAGED_PROFILE_AVAILABLE"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string/jumbo v1, "android.intent.action.MANAGED_PROFILE_UNAVAILABLE"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string/jumbo v1, "android.intent.action.MANAGED_PROFILE_UNLOCKED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/android/settings/TrustedCredentialsSettings;->getActivity()Landroid/app/Activity;

    move-result-object v1

    iget-object v2, p0, Lcom/android/settings/TrustedCredentialsSettings;->buv:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/app/Activity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    return-void
.end method

.method public onDestroy()V
    .locals 4

    const/4 v3, 0x0

    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/android/settings/TrustedCredentialsSettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/TrustedCredentialsSettings;->buv:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/app/Activity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    iget-object v0, p0, Lcom/android/settings/TrustedCredentialsSettings;->bul:Ljava/util/Set;

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settings/o;

    invoke-virtual {v0, v2}, Lcom/android/settings/o;->cancel(Z)Z

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/android/settings/TrustedCredentialsSettings;->bul:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    iget-object v0, p0, Lcom/android/settings/TrustedCredentialsSettings;->buq:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    iget-object v0, p0, Lcom/android/settings/TrustedCredentialsSettings;->bum:Lcom/android/settings/q;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/settings/TrustedCredentialsSettings;->bum:Lcom/android/settings/q;

    invoke-virtual {v0, v2}, Lcom/android/settings/q;->cancel(Z)Z

    iput-object v3, p0, Lcom/android/settings/TrustedCredentialsSettings;->bum:Lcom/android/settings/q;

    :cond_1
    invoke-direct {p0}, Lcom/android/settings/TrustedCredentialsSettings;->bgO()V

    invoke-super {p0}, Lcom/android/settings/MiuiOptionsMenuFragment;->onDestroy()V

    return-void
.end method

.method public onInflateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2

    const v0, 0x7f0d024e

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TabHost;

    iput-object v0, p0, Lcom/android/settings/TrustedCredentialsSettings;->but:Landroid/widget/TabHost;

    iget-object v0, p0, Lcom/android/settings/TrustedCredentialsSettings;->but:Landroid/widget/TabHost;

    invoke-virtual {v0}, Landroid/widget/TabHost;->setup()V

    sget-object v0, Lcom/android/settings/TrustedCredentialsSettings$Tab;->bux:Lcom/android/settings/TrustedCredentialsSettings$Tab;

    invoke-direct {p0, v0}, Lcom/android/settings/TrustedCredentialsSettings;->bgN(Lcom/android/settings/TrustedCredentialsSettings$Tab;)V

    sget-object v0, Lcom/android/settings/TrustedCredentialsSettings$Tab;->buy:Lcom/android/settings/TrustedCredentialsSettings$Tab;

    invoke-direct {p0, v0}, Lcom/android/settings/TrustedCredentialsSettings;->bgN(Lcom/android/settings/TrustedCredentialsSettings$Tab;)V

    invoke-virtual {p0}, Lcom/android/settings/TrustedCredentialsSettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    if-eqz v0, :cond_0

    const-string/jumbo v0, "com.android.settings.TRUSTED_CREDENTIALS_USER"

    invoke-virtual {p0}, Lcom/android/settings/TrustedCredentialsSettings;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/TrustedCredentialsSettings;->but:Landroid/widget/TabHost;

    sget-object v1, Lcom/android/settings/TrustedCredentialsSettings$Tab;->buy:Lcom/android/settings/TrustedCredentialsSettings$Tab;

    invoke-static {v1}, Lcom/android/settings/TrustedCredentialsSettings$Tab;->bho(Lcom/android/settings/TrustedCredentialsSettings$Tab;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TabHost;->setCurrentTabByTag(Ljava/lang/String;)V

    :cond_0
    iget-object v0, p0, Lcom/android/settings/TrustedCredentialsSettings;->but:Landroid/widget/TabHost;

    return-object v0
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 3

    invoke-super {p0, p1}, Lcom/android/settings/MiuiOptionsMenuFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    const-string/jumbo v0, "ConfirmedCredentialUsers"

    new-instance v1, Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/android/settings/TrustedCredentialsSettings;->bun:Landroid/util/ArraySet;

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putIntegerArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    const-string/jumbo v0, "ConfirmingCredentialUser"

    iget v1, p0, Lcom/android/settings/TrustedCredentialsSettings;->bup:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    return-void
.end method
