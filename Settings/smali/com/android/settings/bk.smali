.class public Lcom/android/settings/bk;
.super Ljava/lang/Object;
.source "CompatibilityO.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static bEP(Landroid/app/Activity;Landroid/app/Fragment;Landroid/os/Bundle;Ljava/lang/String;Landroid/app/Fragment;)V
    .locals 8

    const/4 v6, 0x0

    const/4 v4, 0x0

    instance-of v0, p0, Lcom/android/settings/bL;

    if-eqz v0, :cond_1

    move-object v0, p0

    check-cast v0, Lcom/android/settings/bL;

    const-string/jumbo v2, "com.android.settings.NotificationFilterSettings"

    move-object v1, p1

    move-object v3, p2

    move-object v5, p3

    move v7, v4

    invoke-virtual/range {v0 .. v7}, Lcom/android/settings/bL;->Uv(Landroid/app/Fragment;Ljava/lang/String;Landroid/os/Bundle;ILjava/lang/CharSequence;Landroid/app/Fragment;I)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    instance-of v0, p0, Lcom/android/settings/MiuiSettings;

    if-eqz v0, :cond_2

    move-object v1, p0

    check-cast v1, Lcom/android/settings/MiuiSettings;

    const-string/jumbo v2, "com.android.settings.NotificationFilterSettings"

    move-object v3, p2

    move-object v5, p3

    move v7, v4

    invoke-virtual/range {v1 .. v7}, Lcom/android/settings/MiuiSettings;->startPreferencePanel(Ljava/lang/String;Landroid/os/Bundle;ILjava/lang/CharSequence;Landroid/app/Fragment;I)V

    goto :goto_0

    :cond_2
    :try_start_0
    invoke-virtual {p0}, Landroid/app/Activity;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-string/jumbo v1, "startPreferencePanel"

    const/4 v2, 0x6

    new-array v2, v2, [Ljava/lang/Class;

    const-class v3, Ljava/lang/String;

    const/4 v4, 0x0

    aput-object v3, v2, v4

    const-class v3, Landroid/os/Bundle;

    const/4 v4, 0x1

    aput-object v3, v2, v4

    sget-object v3, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    const/4 v4, 0x2

    aput-object v3, v2, v4

    const-class v3, Ljava/lang/CharSequence;

    const/4 v4, 0x3

    aput-object v3, v2, v4

    const-class v3, Landroid/app/Fragment;

    const/4 v4, 0x4

    aput-object v3, v2, v4

    sget-object v3, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    const/4 v4, 0x5

    aput-object v3, v2, v4

    invoke-virtual {v0, v1, v2}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v1, 0x6

    new-array v1, v1, [Ljava/lang/Object;

    const-string/jumbo v2, "com.android.settings.NotificationFilterSettings"

    const/4 v3, 0x0

    aput-object v2, v1, v3

    const/4 v2, 0x1

    aput-object p2, v1, v2

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x2

    aput-object v2, v1, v3

    const/4 v2, 0x3

    aput-object p3, v1, v2

    const/4 v2, 0x4

    aput-object p4, v1, v2

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x5

    aput-object v2, v1, v3

    invoke-virtual {v0, p0, v1}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method
