.class public Lcom/android/settings/DiracEqualizer;
.super Landroid/app/Activity;
.source "DiracEqualizer.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/widget/SeekBar$OnSeekBarChangeListener;


# static fields
.field private static final bvG:[I


# instance fields
.field final bvH:[Lcom/android/settings/view/VerticalSeekBar;

.field private bvI:Lcom/android/settings/x;

.field private bvJ:Lcom/android/settings/w;

.field private final bvK:Landroid/content/DialogInterface$OnClickListener;

.field private bvL:Lcom/miui/a/a/a/h;

.field private bvM:Lcom/android/settings/w;

.field private bvN:Lcom/android/settings/EqualizerView;

.field private final bvO:Landroid/content/DialogInterface$OnClickListener;

.field private bvP:Landroid/widget/TextView;

.field private bvQ:Landroid/view/View;

.field private bvR:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x7

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/android/settings/DiracEqualizer;->bvG:[I

    return-void

    nop

    :array_0
    .array-data 4
        0x7f0a034e
        0x7f0a034f
        0x7f0a0350
        0x7f0a0351
        0x7f0a0352
        0x7f0a0353
        0x7f0a0354
    .end array-data
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    const/4 v0, 0x7

    new-array v0, v0, [Lcom/android/settings/view/VerticalSeekBar;

    iput-object v0, p0, Lcom/android/settings/DiracEqualizer;->bvH:[Lcom/android/settings/view/VerticalSeekBar;

    new-instance v0, Lcom/android/settings/dH;

    invoke-direct {v0, p0}, Lcom/android/settings/dH;-><init>(Lcom/android/settings/DiracEqualizer;)V

    iput-object v0, p0, Lcom/android/settings/DiracEqualizer;->bvK:Landroid/content/DialogInterface$OnClickListener;

    new-instance v0, Lcom/android/settings/dI;

    invoke-direct {v0, p0}, Lcom/android/settings/dI;-><init>(Lcom/android/settings/DiracEqualizer;)V

    iput-object v0, p0, Lcom/android/settings/DiracEqualizer;->bvO:Landroid/content/DialogInterface$OnClickListener;

    return-void
.end method

.method static biE(Ljava/lang/String;[F)Z
    .locals 5

    const/4 v1, 0x0

    if-eqz p0, :cond_0

    if-nez p1, :cond_1

    :cond_0
    return v1

    :cond_1
    const-string/jumbo v0, ","

    invoke-virtual {p0, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    array-length v0, v2

    array-length v3, p1

    invoke-static {v0, v3}, Ljava/lang/Math;->min(II)I

    move-result v3

    move v0, v1

    :goto_0
    if-ge v0, v3, :cond_2

    :try_start_0
    aget-object v4, v2, v0

    invoke-static {v4}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v4

    aput v4, p1, v0
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    const/4 v0, 0x1

    return v0

    :catch_0
    move-exception v0

    const-string/jumbo v2, "DiracEqualizer_Setting"

    const-string/jumbo v3, ""

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    return v1
.end method

.method static biF([F)Ljava/lang/String;
    .locals 4

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v0, 0x0

    array-length v2, p0

    :goto_0
    if-ge v0, v2, :cond_0

    aget v3, p0, v0

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    const-string/jumbo v3, ","

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static biH(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "dirac_eq_custom#"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static biI(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    const-string/jumbo v0, "dirac_eq_custom#"

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    invoke-virtual {p0, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private biJ()Ljava/lang/CharSequence;
    .locals 7

    const/4 v1, 0x1

    const v0, 0x7f1206cc

    invoke-virtual {p0, v0}, Lcom/android/settings/DiracEqualizer;->getString(I)Ljava/lang/String;

    move-result-object v3

    move v0, v1

    :goto_0
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v4

    new-array v5, v1, [Ljava/lang/Object;

    add-int/lit8 v2, v0, 0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const/4 v6, 0x0

    aput-object v0, v5, v6

    invoke-static {v4, v3, v5}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iget-object v4, p0, Lcom/android/settings/DiracEqualizer;->bvI:Lcom/android/settings/x;

    invoke-virtual {v4, v0}, Lcom/android/settings/x;->bjp(Ljava/lang/String;)Lcom/android/settings/w;

    move-result-object v4

    if-nez v4, :cond_0

    return-object v0

    :cond_0
    move v0, v2

    goto :goto_0
.end method

.method private static biK(Ljava/lang/String;)Z
    .locals 1

    if-eqz p0, :cond_0

    const-string/jumbo v0, "dirac_eq_custom#"

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static biM(Landroid/content/Context;)[Ljava/lang/String;
    .locals 2

    const-string/jumbo v0, "dirac_eq_ids"

    invoke-static {p0, v0}, Lcom/android/settings/cs;->bRE(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/String;

    return-object v0

    :cond_0
    const-string/jumbo v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static biN(Landroid/content/Context;Ljava/lang/String;[F)Z
    .locals 1

    invoke-static {p0, p1}, Lcom/android/settings/cs;->bRE(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {v0, p2}, Lcom/android/settings/DiracEqualizer;->biE(Ljava/lang/String;[F)Z

    move-result v0

    return v0

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method private static biP(Landroid/content/Context;Ljava/util/List;)V
    .locals 3

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settings/w;

    iget-object v0, v0, Lcom/android/settings/w;->bvS:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string/jumbo v0, ","

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    :cond_0
    const-string/jumbo v0, "dirac_eq_ids"

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v0, v1}, Lcom/android/settings/cs;->bRD(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method static biQ(Landroid/content/Context;Lcom/android/settings/w;)V
    .locals 2

    if-nez p1, :cond_0

    return-void

    :cond_0
    iget-object v0, p1, Lcom/android/settings/w;->bvS:Ljava/lang/String;

    iget-object v1, p1, Lcom/android/settings/w;->bvU:[F

    invoke-static {v1}, Lcom/android/settings/DiracEqualizer;->biF([F)Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v0, v1}, Lcom/android/settings/cs;->bRD(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method private static biT(Landroid/content/Context;Ljava/lang/String;)V
    .locals 1

    const/4 v0, 0x0

    if-eqz p1, :cond_0

    invoke-static {p0, p1, v0}, Lcom/android/settings/cs;->bRD(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method private biU(IF)V
    .locals 4

    if-ltz p1, :cond_0

    const/4 v0, 0x7

    if-lt p1, v0, :cond_1

    :cond_0
    const-string/jumbo v0, "DiracEqualizer_Setting"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Invalid band="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/settings/DiracEqualizer;->bvN:Lcom/android/settings/EqualizerView;

    invoke-virtual {v0}, Lcom/android/settings/EqualizerView;->getMaxLevel()I

    move-result v0

    iget-object v1, p0, Lcom/android/settings/DiracEqualizer;->bvN:Lcom/android/settings/EqualizerView;

    invoke-virtual {v1}, Lcom/android/settings/EqualizerView;->getMinLevel()I

    move-result v1

    int-to-float v0, v0

    int-to-float v1, v1

    invoke-static {v1, p2}, Ljava/lang/Math;->max(FF)F

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(FF)F

    move-result v0

    const-string/jumbo v1, "DiracEqualizer_Setting"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "setBandLevel band="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, ", level="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/android/settings/DiracEqualizer;->bvL:Lcom/miui/a/a/a/h;

    invoke-virtual {v1, p0, p1, v0}, Lcom/miui/a/a/a/h;->csB(Landroid/content/Context;IF)V

    return-void
.end method

.method public static biW(Landroid/view/Window;I)V
    .locals 5

    :try_start_0
    invoke-virtual {p0}, Landroid/view/Window;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-string/jumbo v1, "setNavigationBarColor"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Class;

    sget-object v3, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    const/4 v4, 0x0

    aput-object v3, v2, v4

    invoke-virtual {v0, v1, v2}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-virtual {v0, p0, v1}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    const/high16 v0, -0x80000000

    invoke-virtual {p0, v0}, Landroid/view/Window;->addFlags(I)V

    const-string/jumbo v0, "DiracEqualizer_Setting"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "setNavigationBarColor success, color="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string/jumbo v1, "DiracEqualizer_Setting"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "setNavigationBarColor "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/reflect/InvocationTargetException;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :catch_1
    move-exception v0

    const-string/jumbo v1, "DiracEqualizer_Setting"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "setNavigationBarColor "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :catch_2
    move-exception v0

    const-string/jumbo v1, "DiracEqualizer_Setting"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "setNavigationBarColor "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/IllegalAccessException;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :catch_3
    move-exception v0

    const-string/jumbo v1, "DiracEqualizer_Setting"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "setNavigationBarColor "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/NoSuchMethodException;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private biX()V
    .locals 2

    invoke-virtual {p0}, Lcom/android/settings/DiracEqualizer;->biG()V

    iget-object v0, p0, Lcom/android/settings/DiracEqualizer;->bvN:Lcom/android/settings/EqualizerView;

    iget-object v1, p0, Lcom/android/settings/DiracEqualizer;->bvJ:Lcom/android/settings/w;

    iget-object v1, v1, Lcom/android/settings/w;->bvU:[F

    invoke-virtual {v0, v1}, Lcom/android/settings/EqualizerView;->setBands([F)V

    return-void
.end method

.method private biY()V
    .locals 4

    invoke-virtual {p0}, Lcom/android/settings/DiracEqualizer;->biG()V

    iget-object v0, p0, Lcom/android/settings/DiracEqualizer;->bvJ:Lcom/android/settings/w;

    iget-object v1, v0, Lcom/android/settings/w;->bvU:[F

    const/4 v0, 0x0

    :goto_0
    const/4 v2, 0x7

    if-ge v0, v2, :cond_0

    iget-object v2, p0, Lcom/android/settings/DiracEqualizer;->bvH:[Lcom/android/settings/view/VerticalSeekBar;

    aget-object v2, v2, v0

    aget v3, v1, v0

    invoke-virtual {p0, v3}, Lcom/android/settings/DiracEqualizer;->biL(F)I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/android/settings/view/VerticalSeekBar;->setProgress(I)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method private biZ()V
    .locals 2

    invoke-virtual {p0}, Lcom/android/settings/DiracEqualizer;->biG()V

    iget-object v0, p0, Lcom/android/settings/DiracEqualizer;->bvP:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/android/settings/DiracEqualizer;->bvJ:Lcom/android/settings/w;

    iget-object v1, v1, Lcom/android/settings/w;->mTitle:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method static synthetic bja(Lcom/android/settings/DiracEqualizer;)Lcom/android/settings/x;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/DiracEqualizer;->bvI:Lcom/android/settings/x;

    return-object v0
.end method

.method static synthetic bjb(Lcom/android/settings/DiracEqualizer;)Lcom/android/settings/w;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/DiracEqualizer;->bvJ:Lcom/android/settings/w;

    return-object v0
.end method

.method static synthetic bjc(Lcom/android/settings/DiracEqualizer;Lcom/android/settings/w;)Lcom/android/settings/w;
    .locals 0

    iput-object p1, p0, Lcom/android/settings/DiracEqualizer;->bvJ:Lcom/android/settings/w;

    return-object p1
.end method

.method static synthetic bjd(Ljava/lang/String;)Z
    .locals 1

    invoke-static {p0}, Lcom/android/settings/DiracEqualizer;->biK(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method static synthetic bje(Landroid/content/Context;)[Ljava/lang/String;
    .locals 1

    invoke-static {p0}, Lcom/android/settings/DiracEqualizer;->biM(Landroid/content/Context;)[Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic bjf(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    invoke-static {p0}, Lcom/android/settings/DiracEqualizer;->biH(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic bjg(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    invoke-static {p0}, Lcom/android/settings/DiracEqualizer;->biI(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic bjh(Landroid/content/Context;Ljava/util/List;)V
    .locals 0

    invoke-static {p0, p1}, Lcom/android/settings/DiracEqualizer;->biP(Landroid/content/Context;Ljava/util/List;)V

    return-void
.end method

.method static synthetic bji(Landroid/content/Context;Ljava/lang/String;)V
    .locals 0

    invoke-static {p0, p1}, Lcom/android/settings/DiracEqualizer;->biT(Landroid/content/Context;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic bjj(Lcom/android/settings/DiracEqualizer;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/DiracEqualizer;->biX()V

    return-void
.end method

.method static synthetic bjk(Lcom/android/settings/DiracEqualizer;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/DiracEqualizer;->biY()V

    return-void
.end method

.method static synthetic bjl(Lcom/android/settings/DiracEqualizer;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/DiracEqualizer;->biZ()V

    return-void
.end method


# virtual methods
.method biD(Lcom/android/settings/w;)V
    .locals 3

    if-nez p1, :cond_0

    return-void

    :cond_0
    iget-object v1, p1, Lcom/android/settings/w;->bvU:[F

    const/4 v0, 0x0

    :goto_0
    array-length v2, v1

    if-ge v0, v2, :cond_1

    aget v2, v1, v0

    invoke-direct {p0, v0, v2}, Lcom/android/settings/DiracEqualizer;->biU(IF)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method biG()V
    .locals 4

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/android/settings/DiracEqualizer;->bvJ:Lcom/android/settings/w;

    if-eqz v1, :cond_0

    return-void

    :cond_0
    const-string/jumbo v1, "dirac_eq_current"

    invoke-static {p0, v1}, Lcom/android/settings/cs;->bRE(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "dirac_eq_auto"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {p0, v0}, Lcom/android/settings/DiracEqualizer;->biO(Lcom/android/settings/w;)V

    iget-object v2, p0, Lcom/android/settings/DiracEqualizer;->bvM:Lcom/android/settings/w;

    iget-object v2, v2, Lcom/android/settings/w;->bvS:Ljava/lang/String;

    invoke-static {p0, v2}, Lcom/android/settings/cs;->bRE(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/android/settings/DiracEqualizer;->bvM:Lcom/android/settings/w;

    iget-object v3, v3, Lcom/android/settings/w;->bvU:[F

    invoke-static {v2, v3}, Lcom/android/settings/DiracEqualizer;->biE(Ljava/lang/String;[F)Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v0, p0, Lcom/android/settings/DiracEqualizer;->bvM:Lcom/android/settings/w;

    :cond_1
    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/android/settings/DiracEqualizer;->bvI:Lcom/android/settings/x;

    invoke-virtual {v0, v1}, Lcom/android/settings/x;->bjo(Ljava/lang/String;)Lcom/android/settings/w;

    move-result-object v0

    :cond_2
    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/android/settings/DiracEqualizer;->bvI:Lcom/android/settings/x;

    invoke-virtual {v0}, Lcom/android/settings/x;->bjr()Lcom/android/settings/w;

    move-result-object v0

    :cond_3
    invoke-virtual {p0, v0}, Lcom/android/settings/DiracEqualizer;->biV(Lcom/android/settings/w;)V

    return-void
.end method

.method biL(F)I
    .locals 2

    iget-object v0, p0, Lcom/android/settings/DiracEqualizer;->bvN:Lcom/android/settings/EqualizerView;

    invoke-virtual {v0}, Lcom/android/settings/EqualizerView;->getMinLevel()I

    move-result v0

    int-to-float v0, v0

    sub-float v0, p1, v0

    iget v1, p0, Lcom/android/settings/DiracEqualizer;->bvR:I

    int-to-float v1, v1

    mul-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    return v0
.end method

.method biO(Lcom/android/settings/w;)V
    .locals 5

    const/4 v0, 0x0

    if-eqz p1, :cond_0

    iget-object v0, p1, Lcom/android/settings/w;->bvU:[F

    :cond_0
    iget-object v1, p0, Lcom/android/settings/DiracEqualizer;->bvM:Lcom/android/settings/w;

    if-nez v1, :cond_1

    new-instance v1, Lcom/android/settings/w;

    const-string/jumbo v2, "dirac_eq_auto"

    const v3, 0x7f1206bc

    invoke-virtual {p0, v3}, Lcom/android/settings/DiracEqualizer;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x2

    invoke-direct {v1, v2, v3, v0, v4}, Lcom/android/settings/w;-><init>(Ljava/lang/String;Ljava/lang/String;[FI)V

    iput-object v1, p0, Lcom/android/settings/DiracEqualizer;->bvM:Lcom/android/settings/w;

    :goto_0
    return-void

    :cond_1
    iget-object v1, p0, Lcom/android/settings/DiracEqualizer;->bvM:Lcom/android/settings/w;

    invoke-virtual {v1, v0}, Lcom/android/settings/w;->bjm([F)V

    goto :goto_0
.end method

.method biR(I)F
    .locals 2

    iget-object v0, p0, Lcom/android/settings/DiracEqualizer;->bvN:Lcom/android/settings/EqualizerView;

    invoke-virtual {v0}, Lcom/android/settings/EqualizerView;->getMinLevel()I

    move-result v0

    iget v1, p0, Lcom/android/settings/DiracEqualizer;->bvR:I

    mul-int/2addr v0, v1

    add-int/2addr v0, p1

    int-to-float v0, v0

    iget v1, p0, Lcom/android/settings/DiracEqualizer;->bvR:I

    int-to-float v1, v1

    div-float/2addr v0, v1

    return v0
.end method

.method biS()V
    .locals 3

    iget-object v0, p0, Lcom/android/settings/DiracEqualizer;->bvQ:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/android/settings/DiracEqualizer;->bvQ:Landroid/view/View;

    iget-object v0, p0, Lcom/android/settings/DiracEqualizer;->bvJ:Lcom/android/settings/w;

    iget v0, v0, Lcom/android/settings/w;->bvT:I

    const/4 v2, 0x2

    if-ne v0, v2, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, v0}, Landroid/view/View;->setEnabled(Z)V

    :cond_0
    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method biV(Lcom/android/settings/w;)V
    .locals 4

    const/4 v3, 0x0

    if-eqz p1, :cond_1

    iget-object v0, p0, Lcom/android/settings/DiracEqualizer;->bvJ:Lcom/android/settings/w;

    if-eq v0, p1, :cond_1

    const-string/jumbo v0, "DiracEqualizer_Setting"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "setCurrentConfig id="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p1, Lcom/android/settings/w;->bvS:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ",title="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p1, Lcom/android/settings/w;->mTitle:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/settings/DiracEqualizer;->bvJ:Lcom/android/settings/w;

    iget-object v1, p0, Lcom/android/settings/DiracEqualizer;->bvM:Lcom/android/settings/w;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/android/settings/DiracEqualizer;->bvM:Lcom/android/settings/w;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/DiracEqualizer;->bvM:Lcom/android/settings/w;

    iget-object v0, v0, Lcom/android/settings/w;->bvS:Ljava/lang/String;

    invoke-static {p0, v0}, Lcom/android/settings/DiracEqualizer;->biT(Landroid/content/Context;Ljava/lang/String;)V

    iput-object v3, p0, Lcom/android/settings/DiracEqualizer;->bvM:Lcom/android/settings/w;

    :cond_0
    iput-object p1, p0, Lcom/android/settings/DiracEqualizer;->bvJ:Lcom/android/settings/w;

    invoke-virtual {p0}, Lcom/android/settings/DiracEqualizer;->biS()V

    const-string/jumbo v0, "dirac_eq_current"

    iget-object v1, p0, Lcom/android/settings/DiracEqualizer;->bvJ:Lcom/android/settings/w;

    iget-object v1, v1, Lcom/android/settings/w;->bvS:Ljava/lang/String;

    invoke-static {p0, v0, v1}, Lcom/android/settings/cs;->bRD(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 8

    const/4 v2, 0x0

    const/4 v7, 0x1

    const/4 v6, 0x0

    iget-object v0, p0, Lcom/android/settings/DiracEqualizer;->bvP:Landroid/widget/TextView;

    if-ne p1, v0, :cond_1

    const-string/jumbo v0, "DiracEqualizer_Setting"

    const-string/jumbo v1, "onClick presetSpinner"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v1, 0x7f1206cf

    invoke-virtual {p0, v1}, Lcom/android/settings/DiracEqualizer;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0, v7}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Lcom/android/settings/DiracEqualizer;->bvI:Lcom/android/settings/x;

    invoke-virtual {v1}, Lcom/android/settings/x;->bjs()[Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/android/settings/DiracEqualizer;->bvO:Landroid/content/DialogInterface$OnClickListener;

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setItems([Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Lcom/android/settings/DiracEqualizer;->bvI:Lcom/android/settings/x;

    invoke-virtual {v1}, Lcom/android/settings/x;->bjs()[Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/android/settings/DiracEqualizer;->bvI:Lcom/android/settings/x;

    iget-object v3, p0, Lcom/android/settings/DiracEqualizer;->bvJ:Lcom/android/settings/w;

    invoke-virtual {v2, v3}, Lcom/android/settings/x;->bjt(Lcom/android/settings/w;)I

    move-result v2

    iget-object v3, p0, Lcom/android/settings/DiracEqualizer;->bvO:Landroid/content/DialogInterface$OnClickListener;

    invoke-virtual {v0, v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setSingleChoiceItems([Ljava/lang/CharSequence;ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    const/high16 v1, 0x1040000

    invoke-virtual {p0, v1}, Lcom/android/settings/DiracEqualizer;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/android/settings/dJ;

    invoke-direct {v2, p0}, Lcom/android/settings/dJ;-><init>(Lcom/android/settings/DiracEqualizer;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const v1, 0x7f0a049e

    if-ne v0, v1, :cond_2

    const-string/jumbo v0, "DiracEqualizer_Setting"

    const-string/jumbo v1, "onClick home"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lcom/android/settings/DiracEqualizer;->finish()V

    goto :goto_0

    :cond_2
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const v1, 0x7f0a049f

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/android/settings/DiracEqualizer;->bvJ:Lcom/android/settings/w;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/DiracEqualizer;->bvJ:Lcom/android/settings/w;

    iget v0, v0, Lcom/android/settings/w;->bvT:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const-string/jumbo v0, "DiracEqualizer_Setting"

    const-string/jumbo v1, "onClick save currentConfig"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Landroid/view/ContextThemeWrapper;

    sget v1, Lmiui/R$style;->Theme_Light:I

    invoke-direct {v0, p0, v1}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0d00af

    invoke-virtual {v0, v1, v2, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const v0, 0x7f0a020f

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    invoke-direct {p0}, Lcom/android/settings/DiracEqualizer;->biJ()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v0}, Landroid/widget/EditText;->length()I

    move-result v2

    invoke-virtual {v0, v6, v2}, Landroid/widget/EditText;->setSelection(II)V

    const v2, 0x7f1206cd

    invoke-virtual {p0, v2}, Lcom/android/settings/DiracEqualizer;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Landroid/app/AlertDialog$Builder;

    invoke-direct {v3, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v3, v2}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    const v4, 0x7f120856

    invoke-virtual {p0, v4}, Lcom/android/settings/DiracEqualizer;->getString(I)Ljava/lang/String;

    move-result-object v4

    new-array v5, v7, [Ljava/lang/Object;

    aput-object v2, v5, v6

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    invoke-virtual {v2, v7}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    const v3, 0x7f1206c9

    invoke-virtual {p0, v3}, Lcom/android/settings/DiracEqualizer;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Lcom/android/settings/dK;

    invoke-direct {v4, p0, v0}, Lcom/android/settings/dK;-><init>(Lcom/android/settings/DiracEqualizer;Landroid/widget/EditText;)V

    invoke-virtual {v2, v3, v4}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v2, 0x7f1206be

    invoke-virtual {p0, v2}, Lcom/android/settings/DiracEqualizer;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/android/settings/dL;

    invoke-direct {v3, p0}, Lcom/android/settings/dL;-><init>(Lcom/android/settings/DiracEqualizer;)V

    invoke-virtual {v0, v2, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v1

    const/4 v2, 0x5

    invoke-virtual {v1, v2}, Landroid/view/Window;->setSoftInputMode(I)V

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    goto/16 :goto_0
.end method

.method public onContextItemSelected(Landroid/view/MenuItem;)Z
    .locals 4

    const/4 v3, 0x1

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    invoke-super {p0, p1}, Landroid/app/Activity;->onContextItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    return v0

    :pswitch_0
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v1, 0x7f1206c2

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/DiracEqualizer;->bvK:Landroid/content/DialogInterface$OnClickListener;

    const v2, 0x104000a

    invoke-virtual {v0, v2, v1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/DiracEqualizer;->bvK:Landroid/content/DialogInterface$OnClickListener;

    const/high16 v2, 0x1040000

    invoke-virtual {v0, v2, v1}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    return v3

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 6

    const/4 v5, 0x1

    const-string/jumbo v0, "DiracEqualizer_Setting"

    const-string/jumbo v1, "onCreate"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    const v0, 0x7f13021b

    invoke-virtual {p0, v0}, Lcom/android/settings/DiracEqualizer;->setTheme(I)V

    invoke-static {}, Lcom/miui/a/a/a/h;->cth()Lcom/miui/a/a/a/h;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/DiracEqualizer;->bvL:Lcom/miui/a/a/a/h;

    iget-object v0, p0, Lcom/android/settings/DiracEqualizer;->bvL:Lcom/miui/a/a/a/h;

    invoke-virtual {v0}, Lcom/miui/a/a/a/h;->initialize()V

    invoke-virtual {p0}, Lcom/android/settings/DiracEqualizer;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0008

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    iput v0, p0, Lcom/android/settings/DiracEqualizer;->bvR:I

    new-instance v0, Lcom/android/settings/x;

    invoke-direct {v0, p0}, Lcom/android/settings/x;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/settings/DiracEqualizer;->bvI:Lcom/android/settings/x;

    invoke-virtual {p0}, Lcom/android/settings/DiracEqualizer;->biG()V

    const v0, 0x7f0d009d

    invoke-virtual {p0, v0}, Lcom/android/settings/DiracEqualizer;->setContentView(I)V

    const v0, 0x7f0a044f

    invoke-virtual {p0, v0}, Lcom/android/settings/DiracEqualizer;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/settings/EqualizerView;

    iput-object v0, p0, Lcom/android/settings/DiracEqualizer;->bvN:Lcom/android/settings/EqualizerView;

    iget-object v0, p0, Lcom/android/settings/DiracEqualizer;->bvN:Lcom/android/settings/EqualizerView;

    invoke-virtual {p0}, Lcom/android/settings/DiracEqualizer;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0007

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    invoke-virtual {p0}, Lcom/android/settings/DiracEqualizer;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b0006

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/android/settings/EqualizerView;->bPZ(II)V

    const v0, 0x7f0a04a3

    invoke-virtual {p0, v0}, Lcom/android/settings/DiracEqualizer;->findViewById(I)Landroid/view/View;

    move-result-object v1

    const v0, 0x7f0a049e

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    invoke-virtual {v0, p0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v2, 0x7f0a049f

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/android/settings/DiracEqualizer;->bvQ:Landroid/view/View;

    iget-object v1, p0, Lcom/android/settings/DiracEqualizer;->bvQ:Landroid/view/View;

    invoke-virtual {v1, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {p0}, Lcom/android/settings/DiracEqualizer;->biS()V

    iget-object v1, p0, Lcom/android/settings/DiracEqualizer;->bvN:Lcom/android/settings/EqualizerView;

    invoke-virtual {v1}, Lcom/android/settings/EqualizerView;->getMaxLevel()I

    move-result v1

    iget-object v2, p0, Lcom/android/settings/DiracEqualizer;->bvN:Lcom/android/settings/EqualizerView;

    invoke-virtual {v2}, Lcom/android/settings/EqualizerView;->getMinLevel()I

    move-result v2

    sub-int/2addr v1, v2

    iget v2, p0, Lcom/android/settings/DiracEqualizer;->bvR:I

    mul-int v3, v1, v2

    const/4 v1, 0x0

    move v2, v1

    :goto_0
    const/4 v1, 0x7

    if-ge v2, v1, :cond_0

    sget-object v1, Lcom/android/settings/DiracEqualizer;->bvG:[I

    aget v1, v1, v2

    invoke-virtual {p0, v1}, Lcom/android/settings/DiracEqualizer;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/android/settings/view/VerticalSeekBar;

    iget-object v4, p0, Lcom/android/settings/DiracEqualizer;->bvH:[Lcom/android/settings/view/VerticalSeekBar;

    aput-object v1, v4, v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/android/settings/view/VerticalSeekBar;->setTag(Ljava/lang/Object;)V

    invoke-virtual {v1, v3}, Lcom/android/settings/view/VerticalSeekBar;->setMax(I)V

    invoke-virtual {v1, p0}, Lcom/android/settings/view/VerticalSeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_0

    :cond_0
    const v1, 0x7f0a0164

    invoke-virtual {p0, v1}, Lcom/android/settings/DiracEqualizer;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/android/settings/DiracEqualizer;->bvP:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/android/settings/DiracEqualizer;->bvP:Landroid/widget/TextView;

    invoke-virtual {v1, p0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v1, p0, Lcom/android/settings/DiracEqualizer;->bvP:Landroid/widget/TextView;

    invoke-virtual {p0, v1}, Lcom/android/settings/DiracEqualizer;->registerForContextMenu(Landroid/view/View;)V

    invoke-direct {p0}, Lcom/android/settings/DiracEqualizer;->biY()V

    invoke-direct {p0}, Lcom/android/settings/DiracEqualizer;->biZ()V

    invoke-direct {p0}, Lcom/android/settings/DiracEqualizer;->biX()V

    invoke-virtual {v0}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/graphics/drawable/Drawable;->setAutoMirrored(Z)V

    iget-object v0, p0, Lcom/android/settings/DiracEqualizer;->bvP:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/graphics/drawable/Drawable;->setAutoMirrored(Z)V

    invoke-virtual {p0}, Lcom/android/settings/DiracEqualizer;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/high16 v1, -0x1000000

    invoke-static {v0, v1}, Lcom/android/settings/DiracEqualizer;->biW(Landroid/view/Window;I)V

    return-void
.end method

.method public onCreateContextMenu(Landroid/view/ContextMenu;Landroid/view/View;Landroid/view/ContextMenu$ContextMenuInfo;)V
    .locals 3

    const/4 v2, 0x0

    invoke-super {p0, p1, p2, p3}, Landroid/app/Activity;->onCreateContextMenu(Landroid/view/ContextMenu;Landroid/view/View;Landroid/view/ContextMenu$ContextMenuInfo;)V

    invoke-interface {p1}, Landroid/view/ContextMenu;->clear()V

    iget-object v0, p0, Lcom/android/settings/DiracEqualizer;->bvJ:Lcom/android/settings/w;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/DiracEqualizer;->bvJ:Lcom/android/settings/w;

    iget v0, v0, Lcom/android/settings/w;->bvT:I

    if-nez v0, :cond_0

    const/4 v0, 0x1

    const v1, 0x7f1206c2

    invoke-interface {p1, v2, v0, v2, v1}, Landroid/view/ContextMenu;->add(IIII)Landroid/view/MenuItem;

    :cond_0
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    invoke-super {p0, p1}, Landroid/app/Activity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    return v0

    :pswitch_0
    invoke-virtual {p0}, Lcom/android/settings/DiracEqualizer;->finish()V

    const/4 v0, 0x1

    return v0

    nop

    :pswitch_data_0
    .packed-switch 0x102002c
        :pswitch_0
    .end packed-switch
.end method

.method public onPause()V
    .locals 1

    iget-object v0, p0, Lcom/android/settings/DiracEqualizer;->bvL:Lcom/miui/a/a/a/h;

    invoke-virtual {v0}, Lcom/miui/a/a/a/h;->csx()V

    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    return-void
.end method

.method public onProgressChanged(Landroid/widget/SeekBar;IZ)V
    .locals 3

    invoke-virtual {p1}, Landroid/widget/SeekBar;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p0, p2}, Lcom/android/settings/DiracEqualizer;->biR(I)F

    move-result v1

    iget-object v2, p0, Lcom/android/settings/DiracEqualizer;->bvJ:Lcom/android/settings/w;

    iget-object v2, v2, Lcom/android/settings/w;->bvU:[F

    aput v1, v2, v0

    invoke-direct {p0, v0, v1}, Lcom/android/settings/DiracEqualizer;->biU(IF)V

    invoke-direct {p0}, Lcom/android/settings/DiracEqualizer;->biX()V

    return-void
.end method

.method public onResume()V
    .locals 1

    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    iget-object v0, p0, Lcom/android/settings/DiracEqualizer;->bvL:Lcom/miui/a/a/a/h;

    invoke-virtual {v0}, Lcom/miui/a/a/a/h;->initialize()V

    return-void
.end method

.method public onStartTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/DiracEqualizer;->bvJ:Lcom/android/settings/w;

    iget-object v1, p0, Lcom/android/settings/DiracEqualizer;->bvM:Lcom/android/settings/w;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/android/settings/DiracEqualizer;->bvJ:Lcom/android/settings/w;

    invoke-virtual {p0, v0}, Lcom/android/settings/DiracEqualizer;->biO(Lcom/android/settings/w;)V

    iget-object v0, p0, Lcom/android/settings/DiracEqualizer;->bvM:Lcom/android/settings/w;

    invoke-virtual {p0, v0}, Lcom/android/settings/DiracEqualizer;->biV(Lcom/android/settings/w;)V

    invoke-direct {p0}, Lcom/android/settings/DiracEqualizer;->biZ()V

    :cond_0
    return-void
.end method

.method public onStopTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 5

    invoke-virtual {p1}, Landroid/widget/SeekBar;->getProgress()I

    move-result v1

    invoke-virtual {p1}, Landroid/widget/SeekBar;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p0, v1}, Lcom/android/settings/DiracEqualizer;->biR(I)F

    move-result v1

    const-string/jumbo v2, "DiracEqualizer_Setting"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "onProgressChanged userBand="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, ", level="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/android/settings/DiracEqualizer;->bvJ:Lcom/android/settings/w;

    iget-object v2, v2, Lcom/android/settings/w;->bvU:[F

    aput v1, v2, v0

    invoke-direct {p0, v0, v1}, Lcom/android/settings/DiracEqualizer;->biU(IF)V

    invoke-direct {p0}, Lcom/android/settings/DiracEqualizer;->biX()V

    iget-object v0, p0, Lcom/android/settings/DiracEqualizer;->bvJ:Lcom/android/settings/w;

    invoke-static {p0, v0}, Lcom/android/settings/DiracEqualizer;->biQ(Landroid/content/Context;Lcom/android/settings/w;)V

    return-void
.end method
