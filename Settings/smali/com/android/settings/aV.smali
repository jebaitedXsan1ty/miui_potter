.class Lcom/android/settings/aV;
.super Landroid/support/v7/preference/Preference;
.source "DreamSettings.java"


# instance fields
.field private final bIw:Lcom/android/settingslib/h/b;

.field final synthetic bIx:Lcom/android/settings/DreamSettings;


# direct methods
.method public constructor <init>(Lcom/android/settings/DreamSettings;Landroid/content/Context;Lcom/android/settingslib/h/b;)V
    .locals 1

    iput-object p1, p0, Lcom/android/settings/aV;->bIx:Lcom/android/settings/DreamSettings;

    invoke-direct {p0, p2}, Landroid/support/v7/preference/Preference;-><init>(Landroid/content/Context;)V

    iput-object p3, p0, Lcom/android/settings/aV;->bIw:Lcom/android/settingslib/h/b;

    const v0, 0x7f0d00a5

    invoke-virtual {p0, v0}, Lcom/android/settings/aV;->setLayoutResource(I)V

    iget-object v0, p0, Lcom/android/settings/aV;->bIw:Lcom/android/settingslib/h/b;

    iget-object v0, v0, Lcom/android/settingslib/h/b;->cJE:Ljava/lang/CharSequence;

    invoke-virtual {p0, v0}, Lcom/android/settings/aV;->setTitle(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/android/settings/aV;->bIw:Lcom/android/settingslib/h/b;

    iget-object v0, v0, Lcom/android/settingslib/h/b;->icon:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0, v0}, Lcom/android/settings/aV;->setIcon(Landroid/graphics/drawable/Drawable;)V

    return-void
.end method

.method static synthetic bzc(Lcom/android/settings/aV;)Lcom/android/settingslib/h/b;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/aV;->bIw:Lcom/android/settingslib/h/b;

    return-object v0
.end method


# virtual methods
.method public al(Landroid/support/v7/preference/l;)V
    .locals 5

    const/4 v3, 0x4

    const/4 v2, 0x0

    invoke-super {p0, p1}, Landroid/support/v7/preference/Preference;->al(Landroid/support/v7/preference/l;)V

    const v0, 0x1020019

    invoke-virtual {p1, v0}, Landroid/support/v7/preference/l;->dma(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioButton;

    iget-object v1, p0, Lcom/android/settings/aV;->bIw:Lcom/android/settingslib/h/b;

    iget-boolean v1, v1, Lcom/android/settingslib/h/b;->cJG:Z

    invoke-virtual {v0, v1}, Landroid/widget/RadioButton;->setChecked(Z)V

    new-instance v1, Lcom/android/settings/gH;

    invoke-direct {v1, p0, p1}, Lcom/android/settings/gH;-><init>(Lcom/android/settings/aV;Landroid/support/v7/preference/l;)V

    invoke-virtual {v0, v1}, Landroid/widget/RadioButton;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    iget-object v0, p0, Lcom/android/settings/aV;->bIw:Lcom/android/settingslib/h/b;

    iget-object v0, v0, Lcom/android/settingslib/h/b;->cJF:Landroid/content/ComponentName;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    move v1, v0

    :goto_0
    const v0, 0x7f0a0143

    invoke-virtual {p1, v0}, Landroid/support/v7/preference/l;->dma(I)Landroid/view/View;

    move-result-object v4

    if-eqz v1, :cond_1

    move v0, v2

    :goto_1
    invoke-virtual {v4, v0}, Landroid/view/View;->setVisibility(I)V

    const v0, 0x102001a

    invoke-virtual {p1, v0}, Landroid/support/v7/preference/l;->dma(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    if-eqz v1, :cond_2

    :goto_2
    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v1, p0, Lcom/android/settings/aV;->bIw:Lcom/android/settingslib/h/b;

    iget-boolean v1, v1, Lcom/android/settingslib/h/b;->cJG:Z

    if-eqz v1, :cond_3

    const/high16 v1, 0x3f800000    # 1.0f

    :goto_3
    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setAlpha(F)V

    iget-object v1, p0, Lcom/android/settings/aV;->bIw:Lcom/android/settingslib/h/b;

    iget-boolean v1, v1, Lcom/android/settingslib/h/b;->cJG:Z

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setEnabled(Z)V

    iget-object v1, p0, Lcom/android/settings/aV;->bIw:Lcom/android/settingslib/h/b;

    iget-boolean v1, v1, Lcom/android/settingslib/h/b;->cJG:Z

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setFocusable(Z)V

    new-instance v1, Lcom/android/settings/gI;

    invoke-direct {v1, p0}, Lcom/android/settings/gI;-><init>(Lcom/android/settings/aV;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void

    :cond_0
    move v1, v2

    goto :goto_0

    :cond_1
    move v0, v3

    goto :goto_1

    :cond_2
    move v2, v3

    goto :goto_2

    :cond_3
    const v1, 0x3ecccccd    # 0.4f

    goto :goto_3
.end method

.method public performClick()V
    .locals 4

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/android/settings/aV;->bIw:Lcom/android/settingslib/h/b;

    iget-boolean v0, v0, Lcom/android/settingslib/h/b;->cJG:Z

    if-eqz v0, :cond_0

    return-void

    :cond_0
    move v1, v2

    :goto_0
    iget-object v0, p0, Lcom/android/settings/aV;->bIx:Lcom/android/settings/DreamSettings;

    invoke-virtual {v0}, Lcom/android/settings/DreamSettings;->djr()Landroid/support/v7/preference/PreferenceScreen;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v7/preference/PreferenceScreen;->dln()I

    move-result v0

    if-ge v1, v0, :cond_1

    iget-object v0, p0, Lcom/android/settings/aV;->bIx:Lcom/android/settings/DreamSettings;

    invoke-virtual {v0}, Lcom/android/settings/DreamSettings;->djr()Landroid/support/v7/preference/PreferenceScreen;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/support/v7/preference/PreferenceScreen;->dlf(I)Landroid/support/v7/preference/Preference;

    move-result-object v0

    check-cast v0, Lcom/android/settings/aV;

    iget-object v3, v0, Lcom/android/settings/aV;->bIw:Lcom/android/settingslib/h/b;

    iput-boolean v2, v3, Lcom/android/settingslib/h/b;->cJG:Z

    invoke-virtual {v0}, Lcom/android/settings/aV;->notifyChanged()V

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/android/settings/aV;->bIw:Lcom/android/settingslib/h/b;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/android/settingslib/h/b;->cJG:Z

    iget-object v0, p0, Lcom/android/settings/aV;->bIx:Lcom/android/settings/DreamSettings;

    invoke-static {v0}, Lcom/android/settings/DreamSettings;->byZ(Lcom/android/settings/DreamSettings;)Lcom/android/settingslib/h/a;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/aV;->bIw:Lcom/android/settingslib/h/b;

    iget-object v1, v1, Lcom/android/settingslib/h/b;->cJH:Landroid/content/ComponentName;

    invoke-virtual {v0, v1}, Lcom/android/settingslib/h/a;->cpa(Landroid/content/ComponentName;)V

    invoke-virtual {p0}, Lcom/android/settings/aV;->notifyChanged()V

    return-void
.end method
