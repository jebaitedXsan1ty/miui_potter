.class final Lcom/android/settings/eK;
.super Landroid/os/AsyncTask;
.source "ChooseLockPassword.java"


# instance fields
.field final synthetic cju:Lcom/android/settings/ChooseLockPassword$ChooseLockPasswordFragment;

.field final synthetic cjv:Ljava/lang/String;

.field final synthetic cjw:Z

.field final synthetic cjx:Z


# direct methods
.method constructor <init>(Lcom/android/settings/ChooseLockPassword$ChooseLockPasswordFragment;Ljava/lang/String;ZZ)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/eK;->cju:Lcom/android/settings/ChooseLockPassword$ChooseLockPasswordFragment;

    iput-object p2, p0, Lcom/android/settings/eK;->cjv:Ljava/lang/String;

    iput-boolean p3, p0, Lcom/android/settings/eK;->cjw:Z

    iput-boolean p4, p0, Lcom/android/settings/eK;->cjx:Z

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected bZV([B)V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/android/settings/eK;->cju:Lcom/android/settings/ChooseLockPassword$ChooseLockPasswordFragment;

    invoke-static {v0}, Lcom/android/settings/ChooseLockPassword$ChooseLockPasswordFragment;->bnz(Lcom/android/settings/ChooseLockPassword$ChooseLockPasswordFragment;)Landroid/app/ProgressDialog;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/eK;->cju:Lcom/android/settings/ChooseLockPassword$ChooseLockPasswordFragment;

    invoke-static {v0}, Lcom/android/settings/ChooseLockPassword$ChooseLockPasswordFragment;->bnz(Lcom/android/settings/ChooseLockPassword$ChooseLockPasswordFragment;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    iget-object v0, p0, Lcom/android/settings/eK;->cju:Lcom/android/settings/ChooseLockPassword$ChooseLockPasswordFragment;

    invoke-static {v0, v1}, Lcom/android/settings/ChooseLockPassword$ChooseLockPasswordFragment;->bnJ(Lcom/android/settings/ChooseLockPassword$ChooseLockPasswordFragment;Landroid/app/ProgressDialog;)Landroid/app/ProgressDialog;

    :cond_0
    iget-object v0, p0, Lcom/android/settings/eK;->cju:Lcom/android/settings/ChooseLockPassword$ChooseLockPasswordFragment;

    invoke-virtual {v0}, Lcom/android/settings/ChooseLockPassword$ChooseLockPasswordFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/settings/eK;->cju:Lcom/android/settings/ChooseLockPassword$ChooseLockPasswordFragment;

    invoke-virtual {v0}, Lcom/android/settings/ChooseLockPassword$ChooseLockPasswordFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/android/settings/aH;->bva(Landroid/content/Context;)V

    :cond_1
    iget-boolean v0, p0, Lcom/android/settings/eK;->cjx:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/settings/eK;->cju:Lcom/android/settings/ChooseLockPassword$ChooseLockPasswordFragment;

    invoke-virtual {v0, p1}, Lcom/android/settings/ChooseLockPassword$ChooseLockPasswordFragment;->bnq([B)V

    :goto_0
    return-void

    :cond_2
    invoke-static {}, Lcom/android/settings/ChooseLockPassword;->-get0()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/android/settings/eK;->cju:Lcom/android/settings/ChooseLockPassword$ChooseLockPasswordFragment;

    invoke-virtual {v0}, Lcom/android/settings/ChooseLockPassword$ChooseLockPasswordFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/app/Activity;->setResult(I)V

    iget-object v0, p0, Lcom/android/settings/eK;->cju:Lcom/android/settings/ChooseLockPassword$ChooseLockPasswordFragment;

    invoke-virtual {v0}, Lcom/android/settings/ChooseLockPassword$ChooseLockPasswordFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/android/settings/eK;->cju:Lcom/android/settings/ChooseLockPassword$ChooseLockPasswordFragment;

    invoke-static {v0, p1}, Lcom/android/settings/ChooseLockPassword$ChooseLockPasswordFragment;->bnN(Lcom/android/settings/ChooseLockPassword$ChooseLockPasswordFragment;[B)V

    goto :goto_0
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/android/settings/eK;->doInBackground([Ljava/lang/Void;)[B

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)[B
    .locals 9

    const/4 v0, 0x0

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/android/settings/eK;->cju:Lcom/android/settings/ChooseLockPassword$ChooseLockPasswordFragment;

    invoke-virtual {v2}, Lcom/android/settings/ChooseLockPassword$ChooseLockPasswordFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/android/settings/eK;->cju:Lcom/android/settings/ChooseLockPassword$ChooseLockPasswordFragment;

    invoke-virtual {v2}, Lcom/android/settings/ChooseLockPassword$ChooseLockPasswordFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    iget-object v3, p0, Lcom/android/settings/eK;->cju:Lcom/android/settings/ChooseLockPassword$ChooseLockPasswordFragment;

    invoke-static {v3}, Lcom/android/settings/ChooseLockPassword$ChooseLockPasswordFragment;->bnv(Lcom/android/settings/ChooseLockPassword$ChooseLockPasswordFragment;)Lcom/android/settings/bM;

    move-result-object v3

    invoke-static {v3}, Lcom/android/settings/bn;->bFE(Lcom/android/settings/bM;)J

    move-result-wide v4

    if-eqz v2, :cond_0

    const-string/jumbo v3, "has_challenge"

    invoke-virtual {v2, v3, v0}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    :cond_0
    iget-object v2, p0, Lcom/android/settings/eK;->cju:Lcom/android/settings/ChooseLockPassword$ChooseLockPasswordFragment;

    invoke-static {v2}, Lcom/android/settings/ChooseLockPassword$ChooseLockPasswordFragment;->bnA(Lcom/android/settings/ChooseLockPassword$ChooseLockPasswordFragment;)Lcom/android/internal/widget/LockPatternUtils;

    move-result-object v2

    iget-object v3, p0, Lcom/android/settings/eK;->cjv:Ljava/lang/String;

    iget-object v6, p0, Lcom/android/settings/eK;->cju:Lcom/android/settings/ChooseLockPassword$ChooseLockPasswordFragment;

    invoke-static {v6}, Lcom/android/settings/ChooseLockPassword$ChooseLockPasswordFragment;->bnC(Lcom/android/settings/ChooseLockPassword$ChooseLockPasswordFragment;)I

    move-result v6

    iget-object v7, p0, Lcom/android/settings/eK;->cju:Lcom/android/settings/ChooseLockPassword$ChooseLockPasswordFragment;

    invoke-static {v7}, Lcom/android/settings/ChooseLockPassword$ChooseLockPasswordFragment;->bnH(Lcom/android/settings/ChooseLockPassword$ChooseLockPasswordFragment;)I

    move-result v7

    iget-boolean v8, p0, Lcom/android/settings/eK;->cjw:Z

    invoke-static {v2, v3, v6, v7, v8}, Lcom/android/settings/bn;->bFF(Lcom/android/internal/widget/LockPatternUtils;Ljava/lang/String;IIZ)V

    iget-boolean v2, p0, Lcom/android/settings/eK;->cjx:Z

    if-nez v2, :cond_1

    if-eqz v0, :cond_2

    :cond_1
    :try_start_0
    iget-object v0, p0, Lcom/android/settings/eK;->cju:Lcom/android/settings/ChooseLockPassword$ChooseLockPasswordFragment;

    invoke-static {v0}, Lcom/android/settings/ChooseLockPassword$ChooseLockPasswordFragment;->bnA(Lcom/android/settings/ChooseLockPassword$ChooseLockPasswordFragment;)Lcom/android/internal/widget/LockPatternUtils;

    move-result-object v0

    iget-object v2, p0, Lcom/android/settings/eK;->cjv:Ljava/lang/String;

    iget-object v3, p0, Lcom/android/settings/eK;->cju:Lcom/android/settings/ChooseLockPassword$ChooseLockPasswordFragment;

    invoke-static {v3}, Lcom/android/settings/ChooseLockPassword$ChooseLockPasswordFragment;->bnH(Lcom/android/settings/ChooseLockPassword$ChooseLockPasswordFragment;)I

    move-result v3

    invoke-static {v0, v2, v4, v5, v3}, Lcom/android/settings/bn;->bFr(Lcom/android/internal/widget/LockPatternUtils;Ljava/lang/String;JI)[B
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    const-string/jumbo v0, "ChooseLockPassword"

    const-string/jumbo v2, "critical: no token returned for known good password"

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move-object v0, v1

    goto :goto_0

    :cond_2
    move-object v0, v1

    goto :goto_0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    check-cast p1, [B

    invoke-virtual {p0, p1}, Lcom/android/settings/eK;->bZV([B)V

    return-void
.end method
