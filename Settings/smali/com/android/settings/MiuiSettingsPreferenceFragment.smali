.class public abstract Lcom/android/settings/MiuiSettingsPreferenceFragment;
.super Lcom/android/settings/core/MiuiInstrumentedPreferenceFragment;
.source "MiuiSettingsPreferenceFragment.java"

# interfaces
.implements Lcom/android/settings/d;


# instance fields
.field protected final ceA:Lcom/android/settings/widget/u;

.field private ceB:Landroid/view/ViewGroup;

.field private ceC:Landroid/widget/ListAdapter;

.field private ceD:Landroid/database/DataSetObserver;

.field private ceE:Lcom/android/settings/MiuiSettingsPreferenceFragment$SettingsDialogFragment;

.field private ceF:Landroid/view/View;

.field private ceG:Ljava/lang/String;

.field private ceH:Landroid/graphics/drawable/Drawable;

.field private ceI:Z

.field private ceJ:Landroid/view/ViewGroup;

.field private ceK:Landroid/util/ArrayMap;

.field private ceL:Ljava/lang/String;

.field private mContentResolver:Landroid/content/ContentResolver;

.field private mPreferenceHighlighted:Z


# direct methods
.method public constructor <init>()V
    .locals 3

    const/4 v2, 0x0

    invoke-direct {p0}, Lcom/android/settings/core/MiuiInstrumentedPreferenceFragment;-><init>()V

    new-instance v0, Lcom/android/settings/widget/u;

    invoke-virtual {p0}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->getLifecycle()Lcom/android/settings/core/lifecycle/c;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/android/settings/widget/u;-><init>(Landroid/preference/PreferenceFragment;Lcom/android/settings/core/lifecycle/c;)V

    iput-object v0, p0, Lcom/android/settings/MiuiSettingsPreferenceFragment;->ceA:Lcom/android/settings/widget/u;

    iput-boolean v2, p0, Lcom/android/settings/MiuiSettingsPreferenceFragment;->mPreferenceHighlighted:Z

    iput-boolean v2, p0, Lcom/android/settings/MiuiSettingsPreferenceFragment;->ceI:Z

    new-instance v0, Lcom/android/settings/kW;

    invoke-direct {v0, p0}, Lcom/android/settings/kW;-><init>(Lcom/android/settings/MiuiSettingsPreferenceFragment;)V

    iput-object v0, p0, Lcom/android/settings/MiuiSettingsPreferenceFragment;->ceD:Landroid/database/DataSetObserver;

    return-void
.end method

.method private bWU(Ljava/lang/String;)I
    .locals 3

    const/4 v2, -0x1

    invoke-virtual {p0}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->hasListView()Z

    move-result v0

    if-nez v0, :cond_0

    return v2

    :cond_0
    invoke-virtual {p0}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->getListView()Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    if-eqz v0, :cond_1

    instance-of v1, v0, Landroid/preference/PreferenceGroupAdapter;

    if-eqz v1, :cond_1

    invoke-direct {p0, v0, p1}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->bWV(Landroid/widget/ListAdapter;Ljava/lang/String;)I

    move-result v0

    return v0

    :cond_1
    return v2
.end method

.method private bWV(Landroid/widget/ListAdapter;Ljava/lang/String;)I
    .locals 4

    invoke-interface {p1}, Landroid/widget/ListAdapter;->getCount()I

    move-result v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_1

    invoke-interface {p1, v1}, Landroid/widget/ListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    instance-of v3, v0, Landroid/preference/Preference;

    if-eqz v3, :cond_0

    check-cast v0, Landroid/preference/Preference;

    invoke-virtual {v0}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    return v1

    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    const/4 v0, -0x1

    return v0
.end method

.method private bWW()Landroid/graphics/drawable/Drawable;
    .locals 2

    iget-object v0, p0, Lcom/android/settings/MiuiSettingsPreferenceFragment;->ceH:Landroid/graphics/drawable/Drawable;

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const v1, 0x7f080368

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/MiuiSettingsPreferenceFragment;->ceH:Landroid/graphics/drawable/Drawable;

    :cond_0
    iget-object v0, p0, Lcom/android/settings/MiuiSettingsPreferenceFragment;->ceH:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method private bWX(Ljava/lang/String;)V
    .locals 5

    invoke-direct {p0}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->bWW()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-direct {p0, p1}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->bWU(Ljava/lang/String;)I

    move-result v3

    if-ltz v3, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/settings/MiuiSettingsPreferenceFragment;->mPreferenceHighlighted:Z

    invoke-virtual {p0}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->getListView()Landroid/widget/ListView;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/ListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v1

    move-object v0, v1

    check-cast v0, Landroid/preference/PreferenceGroupAdapter;

    invoke-virtual {v0, v2}, Landroid/preference/PreferenceGroupAdapter;->setHighlightedDrawable(Landroid/graphics/drawable/Drawable;)V

    check-cast v1, Landroid/preference/PreferenceGroupAdapter;

    invoke-virtual {v1, v3}, Landroid/preference/PreferenceGroupAdapter;->setHighlighted(I)V

    new-instance v0, Lcom/android/settings/kX;

    invoke-direct {v0, p0, v4, v3, v2}, Lcom/android/settings/kX;-><init>(Lcom/android/settings/MiuiSettingsPreferenceFragment;Landroid/widget/ListView;ILandroid/graphics/drawable/Drawable;)V

    invoke-virtual {v4, v0}, Landroid/widget/ListView;->post(Ljava/lang/Runnable;)Z

    :cond_0
    return-void
.end method

.method private bXc()V
    .locals 2

    invoke-virtual {p0}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_0

    const-string/jumbo v1, ":android:show_fragment_title"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p0}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->getActionBar()Lmiui/app/ActionBar;

    move-result-object v1

    if-eqz v1, :cond_0

    if-lez v0, :cond_0

    invoke-virtual {v1, v0}, Landroid/app/ActionBar;->setTitle(I)V

    :cond_0
    return-void
.end method

.method private bXe()V
    .locals 3

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/android/settings/MiuiSettingsPreferenceFragment;->ceF:Landroid/view/View;

    if-nez v0, :cond_0

    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-virtual {p0}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    invoke-virtual {v0}, Landroid/preference/PreferenceScreen;->getPreferenceCount()I

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    iget-object v2, p0, Lcom/android/settings/MiuiSettingsPreferenceFragment;->ceF:Landroid/view/View;

    if-eqz v0, :cond_2

    :goto_1
    invoke-virtual {v2, v1}, Landroid/view/View;->setVisibility(I)V

    :goto_2
    return-void

    :cond_1
    move v0, v1

    goto :goto_0

    :cond_2
    const/16 v1, 0x8

    goto :goto_1

    :cond_3
    iget-object v0, p0, Lcom/android/settings/MiuiSettingsPreferenceFragment;->ceF:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_2
.end method

.method private bXf(Landroid/os/Bundle;)V
    .locals 2

    sget v0, Lmiui/R$style;->Theme_Light_Settings:I

    if-eqz p1, :cond_2

    const-string/jumbo v0, "theme_res_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    :cond_0
    :goto_0
    if-eqz v0, :cond_1

    invoke-virtual {p0, v0}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->setThemeRes(I)V

    :cond_1
    return-void

    :cond_2
    sget-boolean v1, Lmiui/os/Build;->IS_TABLET:Z

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentManager;->getBackStackEntryCount()I

    move-result v0

    const/4 v1, 0x1

    if-ge v0, v1, :cond_3

    invoke-virtual {p0}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    instance-of v0, v0, Lcom/android/settings/MiuiSettings;

    if-eqz v0, :cond_3

    const v0, 0x7f130131

    goto :goto_0

    :cond_3
    sget v0, Lmiui/R$style;->Theme_Light_Settings:I

    goto :goto_0
.end method

.method static synthetic bXg(Lcom/android/settings/MiuiSettingsPreferenceFragment;)Lcom/android/settings/MiuiSettingsPreferenceFragment$SettingsDialogFragment;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/MiuiSettingsPreferenceFragment;->ceE:Lcom/android/settings/MiuiSettingsPreferenceFragment$SettingsDialogFragment;

    return-object v0
.end method

.method static synthetic bXh(Lcom/android/settings/MiuiSettingsPreferenceFragment;Lcom/android/settings/MiuiSettingsPreferenceFragment$SettingsDialogFragment;)Lcom/android/settings/MiuiSettingsPreferenceFragment$SettingsDialogFragment;
    .locals 0

    iput-object p1, p0, Lcom/android/settings/MiuiSettingsPreferenceFragment;->ceE:Lcom/android/settings/MiuiSettingsPreferenceFragment$SettingsDialogFragment;

    return-object p1
.end method


# virtual methods
.method public Lx(I)I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public QH(ILandroid/os/Bundle;)V
    .locals 0

    return-void
.end method

.method protected Vg(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 1

    invoke-super {p0, p1, p2, p3}, Lcom/android/settings/core/MiuiInstrumentedPreferenceFragment;->onInflateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method protected alW(I)V
    .locals 3

    iget-object v0, p0, Lcom/android/settings/MiuiSettingsPreferenceFragment;->ceE:Lcom/android/settings/MiuiSettingsPreferenceFragment$SettingsDialogFragment;

    if-eqz v0, :cond_0

    const-string/jumbo v0, "MiuiSettingsPreferenceFragment"

    const-string/jumbo v1, "Old dialog fragment not null!"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    new-instance v0, Lcom/android/settings/MiuiSettingsPreferenceFragment$SettingsDialogFragment;

    invoke-direct {v0, p0, p1}, Lcom/android/settings/MiuiSettingsPreferenceFragment$SettingsDialogFragment;-><init>(Lcom/android/settings/d;I)V

    iput-object v0, p0, Lcom/android/settings/MiuiSettingsPreferenceFragment;->ceE:Lcom/android/settings/MiuiSettingsPreferenceFragment$SettingsDialogFragment;

    iget-object v0, p0, Lcom/android/settings/MiuiSettingsPreferenceFragment;->ceE:Lcom/android/settings/MiuiSettingsPreferenceFragment$SettingsDialogFragment;

    invoke-virtual {p0}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->getChildFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/android/settings/MiuiSettingsPreferenceFragment$SettingsDialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    return-void
.end method

.method protected aq()I
    .locals 1

    const v0, 0x7f12080b

    return v0
.end method

.method public at(Landroid/preference/Preference;)Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method protected bWA()Landroid/content/pm/PackageManager;
    .locals 1

    invoke-virtual {p0}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    return-object v0
.end method

.method protected bWB()Landroid/content/ContentResolver;
    .locals 1

    invoke-virtual {p0}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/MiuiSettingsPreferenceFragment;->mContentResolver:Landroid/content/ContentResolver;

    :cond_0
    iget-object v0, p0, Lcom/android/settings/MiuiSettingsPreferenceFragment;->mContentResolver:Landroid/content/ContentResolver;

    return-object v0
.end method

.method public bWC(Landroid/app/Fragment;Ljava/lang/String;ILandroid/os/Bundle;I)Z
    .locals 6

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p5

    move v4, p3

    move-object v5, p4

    invoke-virtual/range {v0 .. v5}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->bWM(Landroid/app/Fragment;Ljava/lang/String;IILandroid/os/Bundle;)Z

    move-result v0

    return v0
.end method

.method public bWD(I)Landroid/view/View;
    .locals 3

    invoke-virtual {p0}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/MiuiSettingsPreferenceFragment;->ceJ:Landroid/view/ViewGroup;

    const/4 v2, 0x0

    invoke-virtual {v0, p1, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->bXb(Landroid/view/View;)V

    return-object v0
.end method

.method public bWE(Landroid/view/View;)V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/MiuiSettingsPreferenceFragment;->ceF:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/MiuiSettingsPreferenceFragment;->ceF:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    :cond_0
    iput-object p1, p0, Lcom/android/settings/MiuiSettingsPreferenceFragment;->ceF:Landroid/view/View;

    invoke-direct {p0}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->bXe()V

    return-void
.end method

.method protected bWF(I)V
    .locals 1

    invoke-virtual {p0}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/app/Activity;->setResult(I)V

    return-void
.end method

.method protected bWG(ILandroid/content/Intent;)V
    .locals 1

    invoke-virtual {p0}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Landroid/app/Activity;->setResult(ILandroid/content/Intent;)V

    return-void
.end method

.method protected bWH(Ljava/lang/String;)Landroid/preference/Preference;
    .locals 2

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/android/settings/MiuiSettingsPreferenceFragment;->ceK:Landroid/util/ArrayMap;

    if-eqz v1, :cond_0

    iget-object v0, p0, Lcom/android/settings/MiuiSettingsPreferenceFragment;->ceK:Landroid/util/ArrayMap;

    invoke-virtual {v0, p1}, Landroid/util/ArrayMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/preference/Preference;

    :cond_0
    return-object v0
.end method

.method protected bWI(Landroid/preference/PreferenceGroup;)V
    .locals 5

    new-instance v0, Landroid/util/ArrayMap;

    invoke-direct {v0}, Landroid/util/ArrayMap;-><init>()V

    iput-object v0, p0, Lcom/android/settings/MiuiSettingsPreferenceFragment;->ceK:Landroid/util/ArrayMap;

    invoke-virtual {p1}, Landroid/preference/PreferenceGroup;->getPreferenceCount()I

    move-result v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_1

    invoke-virtual {p1, v0}, Landroid/preference/PreferenceGroup;->getPreference(I)Landroid/preference/Preference;

    move-result-object v2

    invoke-virtual {v2}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_0

    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    iget-object v3, p0, Lcom/android/settings/MiuiSettingsPreferenceFragment;->ceK:Landroid/util/ArrayMap;

    invoke-virtual {v2}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4, v2}, Landroid/util/ArrayMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    :cond_1
    return-void
.end method

.method protected bWJ(Landroid/preference/PreferenceGroup;)V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/MiuiSettingsPreferenceFragment;->ceK:Landroid/util/ArrayMap;

    invoke-virtual {v0}, Landroid/util/ArrayMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/preference/Preference;

    invoke-virtual {p1, v0}, Landroid/preference/PreferenceGroup;->removePreference(Landroid/preference/Preference;)Z

    goto :goto_0

    :cond_0
    return-void
.end method

.method protected bWK(Ljava/lang/String;)V
    .locals 2

    invoke-virtual {p0, p1}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    :cond_0
    return-void
.end method

.method public final bWL()V
    .locals 0

    invoke-virtual {p0}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->finish()V

    return-void
.end method

.method public bWM(Landroid/app/Fragment;Ljava/lang/String;IILandroid/os/Bundle;)Z
    .locals 9

    const/4 v5, 0x0

    const/4 v8, 0x1

    invoke-virtual {p0}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    instance-of v1, v0, Lcom/android/settings/bL;

    if-eqz v1, :cond_0

    check-cast v0, Lcom/android/settings/bL;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p5

    move v4, p3

    move-object v6, p1

    move v7, p4

    invoke-virtual/range {v0 .. v7}, Lcom/android/settings/bL;->Uv(Landroid/app/Fragment;Ljava/lang/String;Landroid/os/Bundle;ILjava/lang/CharSequence;Landroid/app/Fragment;I)V

    return v8

    :cond_0
    instance-of v1, v0, Landroid/preference/PreferenceActivity;

    if-eqz v1, :cond_1

    move-object v1, v0

    check-cast v1, Landroid/preference/PreferenceActivity;

    move-object v2, p2

    move-object v3, p5

    move v4, p3

    move-object v6, p1

    move v7, p4

    invoke-virtual/range {v1 .. v7}, Landroid/preference/PreferenceActivity;->startPreferencePanel(Ljava/lang/String;Landroid/os/Bundle;ILjava/lang/CharSequence;Landroid/app/Fragment;I)V

    return v8

    :cond_1
    const-string/jumbo v0, "MiuiSettingsPreferenceFragment"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Parent isn\'t SettingsActivity nor PreferenceActivity, thus there\'s no way to launch the given Fragment (name: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ", requestCode: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    return v0
.end method

.method public bWN()Landroid/view/ViewGroup;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/MiuiSettingsPreferenceFragment;->ceB:Landroid/view/ViewGroup;

    return-object v0
.end method

.method protected bWO(I)V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/android/settings/MiuiSettingsPreferenceFragment;->ceE:Lcom/android/settings/MiuiSettingsPreferenceFragment$SettingsDialogFragment;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/MiuiSettingsPreferenceFragment;->ceE:Lcom/android/settings/MiuiSettingsPreferenceFragment$SettingsDialogFragment;

    invoke-virtual {v0}, Lcom/android/settings/MiuiSettingsPreferenceFragment$SettingsDialogFragment;->bXi()I

    move-result v0

    if-ne v0, p1, :cond_0

    iget-object v0, p0, Lcom/android/settings/MiuiSettingsPreferenceFragment;->ceE:Lcom/android/settings/MiuiSettingsPreferenceFragment$SettingsDialogFragment;

    invoke-virtual {v0}, Lcom/android/settings/MiuiSettingsPreferenceFragment$SettingsDialogFragment;->dismiss()V

    :cond_0
    iput-object v1, p0, Lcom/android/settings/MiuiSettingsPreferenceFragment;->ceE:Lcom/android/settings/MiuiSettingsPreferenceFragment$SettingsDialogFragment;

    return-void
.end method

.method protected bWP()V
    .locals 1

    invoke-virtual {p0}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/preference/PreferenceScreen;->removeAll()V

    :cond_0
    return-void
.end method

.method protected bWQ(Landroid/content/DialogInterface$OnDismissListener;)V
    .locals 1

    iget-object v0, p0, Lcom/android/settings/MiuiSettingsPreferenceFragment;->ceE:Lcom/android/settings/MiuiSettingsPreferenceFragment$SettingsDialogFragment;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/MiuiSettingsPreferenceFragment;->ceE:Lcom/android/settings/MiuiSettingsPreferenceFragment$SettingsDialogFragment;

    invoke-static {v0, p1}, Lcom/android/settings/MiuiSettingsPreferenceFragment$SettingsDialogFragment;->bXj(Lcom/android/settings/MiuiSettingsPreferenceFragment$SettingsDialogFragment;Landroid/content/DialogInterface$OnDismissListener;)Landroid/content/DialogInterface$OnDismissListener;

    :cond_0
    return-void
.end method

.method protected bWR()I
    .locals 1

    iget-object v0, p0, Lcom/android/settings/MiuiSettingsPreferenceFragment;->ceK:Landroid/util/ArrayMap;

    invoke-virtual {v0}, Landroid/util/ArrayMap;->size()I

    move-result v0

    return v0
.end method

.method public bWS(I)Z
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/android/settings/MiuiSettingsPreferenceFragment;->ceE:Lcom/android/settings/MiuiSettingsPreferenceFragment$SettingsDialogFragment;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/MiuiSettingsPreferenceFragment;->ceE:Lcom/android/settings/MiuiSettingsPreferenceFragment$SettingsDialogFragment;

    invoke-virtual {v0}, Lcom/android/settings/MiuiSettingsPreferenceFragment$SettingsDialogFragment;->bXi()I

    move-result v0

    if-eq v0, p1, :cond_1

    :cond_0
    return v1

    :cond_1
    iget-object v0, p0, Lcom/android/settings/MiuiSettingsPreferenceFragment;->ceE:Lcom/android/settings/MiuiSettingsPreferenceFragment$SettingsDialogFragment;

    invoke-virtual {v0}, Lcom/android/settings/MiuiSettingsPreferenceFragment$SettingsDialogFragment;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    if-nez v0, :cond_2

    return v1

    :cond_2
    invoke-virtual {v0}, Landroid/app/Dialog;->isShowing()Z

    move-result v0

    return v0
.end method

.method protected bWT()Z
    .locals 1

    invoke-virtual {p0}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/android/settings/cS;

    invoke-interface {v0}, Lcom/android/settings/cS;->bMN()Z

    move-result v0

    return v0
.end method

.method public bWY()V
    .locals 1

    invoke-virtual {p0}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->isAdded()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/android/settings/MiuiSettingsPreferenceFragment;->mPreferenceHighlighted:Z

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/MiuiSettingsPreferenceFragment;->ceL:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/MiuiSettingsPreferenceFragment;->ceL:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->bWX(Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method protected bWZ()V
    .locals 0

    invoke-virtual {p0}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->bWY()V

    invoke-direct {p0}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->bXe()V

    return-void
.end method

.method public bWy(Landroid/app/Fragment;Ljava/lang/String;ILandroid/os/Bundle;)Z
    .locals 6

    const v3, 0x7f120990

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v4, p3

    move-object v5, p4

    invoke-virtual/range {v0 .. v5}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->bWM(Landroid/app/Fragment;Ljava/lang/String;IILandroid/os/Bundle;)Z

    move-result v0

    return v0
.end method

.method protected final bWz()Landroid/content/Context;
    .locals 1

    invoke-virtual {p0}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    return-object v0
.end method

.method public bXa()V
    .locals 2

    iget-boolean v0, p0, Lcom/android/settings/MiuiSettingsPreferenceFragment;->ceI:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/android/settings/MiuiSettingsPreferenceFragment;->ceC:Landroid/widget/ListAdapter;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/MiuiSettingsPreferenceFragment;->ceC:Landroid/widget/ListAdapter;

    iget-object v1, p0, Lcom/android/settings/MiuiSettingsPreferenceFragment;->ceD:Landroid/database/DataSetObserver;

    invoke-interface {v0, v1}, Landroid/widget/ListAdapter;->unregisterDataSetObserver(Landroid/database/DataSetObserver;)V

    :cond_0
    invoke-virtual {p0}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    invoke-virtual {v0}, Landroid/preference/PreferenceScreen;->getRootAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/MiuiSettingsPreferenceFragment;->ceC:Landroid/widget/ListAdapter;

    iget-object v0, p0, Lcom/android/settings/MiuiSettingsPreferenceFragment;->ceC:Landroid/widget/ListAdapter;

    iget-object v1, p0, Lcom/android/settings/MiuiSettingsPreferenceFragment;->ceD:Landroid/database/DataSetObserver;

    invoke-interface {v0, v1}, Landroid/widget/ListAdapter;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/settings/MiuiSettingsPreferenceFragment;->ceI:Z

    invoke-virtual {p0}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->bWZ()V

    :cond_1
    return-void
.end method

.method public bXb(Landroid/view/View;)V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/MiuiSettingsPreferenceFragment;->ceJ:Landroid/view/ViewGroup;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/MiuiSettingsPreferenceFragment;->ceJ:Landroid/view/ViewGroup;

    invoke-virtual {v0, p1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    iget-object v0, p0, Lcom/android/settings/MiuiSettingsPreferenceFragment;->ceJ:Landroid/view/ViewGroup;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    :cond_0
    return-void
.end method

.method public bXd()V
    .locals 3

    const/4 v2, 0x0

    iget-boolean v0, p0, Lcom/android/settings/MiuiSettingsPreferenceFragment;->ceI:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/settings/MiuiSettingsPreferenceFragment;->ceC:Landroid/widget/ListAdapter;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/MiuiSettingsPreferenceFragment;->ceC:Landroid/widget/ListAdapter;

    iget-object v1, p0, Lcom/android/settings/MiuiSettingsPreferenceFragment;->ceD:Landroid/database/DataSetObserver;

    invoke-interface {v0, v1}, Landroid/widget/ListAdapter;->unregisterDataSetObserver(Landroid/database/DataSetObserver;)V

    iput-object v2, p0, Lcom/android/settings/MiuiSettingsPreferenceFragment;->ceC:Landroid/widget/ListAdapter;

    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/settings/MiuiSettingsPreferenceFragment;->ceI:Z

    :cond_1
    return-void
.end method

.method public cC()Ljava/lang/String;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public finish()V
    .locals 2

    invoke-virtual {p0}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    instance-of v0, v0, Lcom/android/settings/MiuiSettings;

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->isResumed()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {v0}, Landroid/app/FragmentManager;->popBackStackImmediate()Z

    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentManager;->popBackStack()V

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->isResumed()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p0}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->onBackPressed()V

    goto :goto_0

    :cond_3
    invoke-virtual {p0}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    goto :goto_0
.end method

.method protected getIntent()Landroid/content/Intent;
    .locals 2

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    if-nez v0, :cond_0

    return-object v1

    :cond_0
    invoke-virtual {p0}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public getMetricsCategory()I
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method protected getNextButton()Landroid/widget/Button;
    .locals 1

    invoke-virtual {p0}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/android/settings/cS;

    invoke-interface {v0}, Lcom/android/settings/cS;->getNextButton()Landroid/widget/Button;

    move-result-object v0

    return-object v0
.end method

.method protected getSystemService(Ljava/lang/String;)Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 1

    invoke-super {p0, p1}, Lcom/android/settings/core/MiuiInstrumentedPreferenceFragment;->onActivityCreated(Landroid/os/Bundle;)V

    iget-object v0, p0, Lcom/android/settings/MiuiSettingsPreferenceFragment;->ceG:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->setHasOptionsMenu(Z)V

    :cond_0
    return-void
.end method

.method protected onBindPreferences()V
    .locals 0

    invoke-virtual {p0}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->bXa()V

    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3

    const/4 v2, 0x0

    invoke-super {p0, p1}, Lcom/android/settings/core/MiuiInstrumentedPreferenceFragment;->onCreate(Landroid/os/Bundle;)V

    if-eqz p1, :cond_0

    const-string/jumbo v0, "android:preference_highlighted"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/settings/MiuiSettingsPreferenceFragment;->mPreferenceHighlighted:Z

    :cond_0
    invoke-virtual {p0}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->aq()I

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/MiuiSettingsPreferenceFragment;->ceG:Ljava/lang/String;

    :cond_1
    invoke-direct {p0, p1}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->bXf(Landroid/os/Bundle;)V

    invoke-virtual {p0, p1, v2}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->qR(Landroid/os/Bundle;Ljava/lang/String;)V

    return-void
.end method

.method public onCreateDialog(I)Landroid/app/Dialog;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V
    .locals 3

    iget-object v0, p0, Lcom/android/settings/MiuiSettingsPreferenceFragment;->ceG:Ljava/lang/String;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/MiuiSettingsPreferenceFragment;->ceG:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, p1, v1, v2}, Lcom/android/settingslib/B;->crI(Landroid/app/Activity;Landroid/view/Menu;Ljava/lang/String;Ljava/lang/String;)Z

    :cond_0
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 1

    invoke-virtual {p0}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V

    :cond_0
    const/4 v0, 0x1

    return v0
.end method

.method public onDetach()V
    .locals 2

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->isRemoving()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/MiuiSettingsPreferenceFragment;->ceE:Lcom/android/settings/MiuiSettingsPreferenceFragment$SettingsDialogFragment;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/MiuiSettingsPreferenceFragment;->ceE:Lcom/android/settings/MiuiSettingsPreferenceFragment$SettingsDialogFragment;

    invoke-virtual {v0}, Lcom/android/settings/MiuiSettingsPreferenceFragment$SettingsDialogFragment;->dismiss()V

    iput-object v1, p0, Lcom/android/settings/MiuiSettingsPreferenceFragment;->ceE:Lcom/android/settings/MiuiSettingsPreferenceFragment$SettingsDialogFragment;

    :cond_0
    invoke-super {p0}, Lcom/android/settings/core/MiuiInstrumentedPreferenceFragment;->onDetach()V

    return-void
.end method

.method public onInflateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3

    if-eqz p2, :cond_0

    invoke-virtual {p0}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0, p2}, Lcom/android/settings/dc;->bYu(Landroid/app/Activity;Landroid/view/View;)V

    :cond_0
    invoke-virtual {p0, p1, p2, p3}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->Vg(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v1

    if-eqz v1, :cond_1

    const v0, 0x7f0a0321

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    instance-of v2, v0, Landroid/view/ViewGroup;

    if-eqz v2, :cond_1

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/android/settings/MiuiSettingsPreferenceFragment;->ceJ:Landroid/view/ViewGroup;

    :cond_1
    return-object v1
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    invoke-super {p0, p1}, Lcom/android/settings/core/MiuiInstrumentedPreferenceFragment;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    return v0

    :pswitch_0
    invoke-virtual {p0}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->finish()V

    const/4 v0, 0x1

    return v0

    nop

    :pswitch_data_0
    .packed-switch 0x102002c
        :pswitch_0
    .end packed-switch
.end method

.method public onPause()V
    .locals 3

    invoke-super {p0}, Lcom/android/settings/core/MiuiInstrumentedPreferenceFragment;->onPause()V

    invoke-virtual {p0}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->cC()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    :try_start_0
    invoke-static {}, Lcom/android/settings/E;->blE()V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string/jumbo v0, "MiuiSettingsPreferenceFragment"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "IllegalStateException occurs in SettingsPreferenceFragment "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->cC()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public onPreferenceTreeClick(Landroid/preference/PreferenceScreen;Landroid/preference/Preference;)Z
    .locals 1

    invoke-virtual {p0, p2}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->at(Landroid/preference/Preference;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    return v0

    :cond_0
    invoke-virtual {p2}, Landroid/preference/Preference;->getFragment()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    instance-of v0, v0, Lcom/android/settings/bL;

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/android/settings/bL;

    invoke-virtual {v0, p0, p2}, Lcom/android/settings/bL;->onPreferenceStartFragment(Landroid/preference/PreferenceFragment;Landroid/preference/Preference;)Z

    move-result v0

    return v0

    :cond_1
    invoke-super {p0, p1, p2}, Lcom/android/settings/core/MiuiInstrumentedPreferenceFragment;->onPreferenceTreeClick(Landroid/preference/PreferenceScreen;Landroid/preference/Preference;)Z

    move-result v0

    return v0
.end method

.method public onResume()V
    .locals 3

    invoke-super {p0}, Lcom/android/settings/core/MiuiInstrumentedPreferenceFragment;->onResume()V

    invoke-virtual {p0}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_0

    const-string/jumbo v1, ":settings:fragment_args_key"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/MiuiSettingsPreferenceFragment;->ceL:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->bWY()V

    :cond_0
    invoke-virtual {p0}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->cC()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    :try_start_0
    invoke-virtual {p0}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {p0}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->cC()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/settings/E;->blD(Landroid/app/Activity;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_1
    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string/jumbo v0, "MiuiSettingsPreferenceFragment"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "IllegalStateException occurs in SettingsPreferenceFragment "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->cC()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Lcom/android/settings/core/MiuiInstrumentedPreferenceFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    const-string/jumbo v0, "theme_res_id"

    invoke-virtual {p0}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->getThemedContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getThemeResId()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string/jumbo v0, "android:preference_highlighted"

    iget-boolean v1, p0, Lcom/android/settings/MiuiSettingsPreferenceFragment;->mPreferenceHighlighted:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    return-void
.end method

.method public onStart()V
    .locals 0

    invoke-super {p0}, Lcom/android/settings/core/MiuiInstrumentedPreferenceFragment;->onStart()V

    invoke-direct {p0}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->bXc()V

    return-void
.end method

.method public onStop()V
    .locals 0

    invoke-super {p0}, Lcom/android/settings/core/MiuiInstrumentedPreferenceFragment;->onStop()V

    invoke-virtual {p0}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->bXd()V

    return-void
.end method

.method protected onUnbindPreferences()V
    .locals 0

    invoke-virtual {p0}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->bXd()V

    return-void
.end method

.method public qR(Landroid/os/Bundle;Ljava/lang/String;)V
    .locals 0

    return-void
.end method
