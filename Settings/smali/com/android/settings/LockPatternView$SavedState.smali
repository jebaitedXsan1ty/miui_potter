.class Lcom/android/settings/LockPatternView$SavedState;
.super Landroid/view/View$BaseSavedState;
.source "LockPatternView.java"


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field private final btJ:I

.field private final btK:Z

.field private final btL:Z

.field private final btM:Ljava/lang/String;

.field private final btN:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/android/settings/dw;

    invoke-direct {v0}, Lcom/android/settings/dw;-><init>()V

    sput-object v0, Lcom/android/settings/LockPatternView$SavedState;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0, p1}, Landroid/view/View$BaseSavedState;-><init>(Landroid/os/Parcel;)V

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/LockPatternView$SavedState;->btM:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/android/settings/LockPatternView$SavedState;->btJ:I

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->readValue(Ljava/lang/ClassLoader;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/settings/LockPatternView$SavedState;->btL:Z

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->readValue(Ljava/lang/ClassLoader;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/settings/LockPatternView$SavedState;->btK:Z

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->readValue(Ljava/lang/ClassLoader;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/settings/LockPatternView$SavedState;->btN:Z

    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/android/settings/LockPatternView$SavedState;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/settings/LockPatternView$SavedState;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method private constructor <init>(Landroid/os/Parcelable;Ljava/lang/String;IZZZ)V
    .locals 0

    invoke-direct {p0, p1}, Landroid/view/View$BaseSavedState;-><init>(Landroid/os/Parcelable;)V

    iput-object p2, p0, Lcom/android/settings/LockPatternView$SavedState;->btM:Ljava/lang/String;

    iput p3, p0, Lcom/android/settings/LockPatternView$SavedState;->btJ:I

    iput-boolean p4, p0, Lcom/android/settings/LockPatternView$SavedState;->btL:Z

    iput-boolean p5, p0, Lcom/android/settings/LockPatternView$SavedState;->btK:Z

    iput-boolean p6, p0, Lcom/android/settings/LockPatternView$SavedState;->btN:Z

    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcelable;Ljava/lang/String;IZZZLcom/android/settings/LockPatternView$SavedState;)V
    .locals 0

    invoke-direct/range {p0 .. p6}, Lcom/android/settings/LockPatternView$SavedState;-><init>(Landroid/os/Parcelable;Ljava/lang/String;IZZZ)V

    return-void
.end method


# virtual methods
.method public bgg()I
    .locals 1

    iget v0, p0, Lcom/android/settings/LockPatternView$SavedState;->btJ:I

    return v0
.end method

.method public bgh()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/LockPatternView$SavedState;->btM:Ljava/lang/String;

    return-object v0
.end method

.method public bgi()Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/settings/LockPatternView$SavedState;->btK:Z

    return v0
.end method

.method public bgj()Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/settings/LockPatternView$SavedState;->btL:Z

    return v0
.end method

.method public bgk()Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/settings/LockPatternView$SavedState;->btN:Z

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    invoke-super {p0, p1, p2}, Landroid/view/View$BaseSavedState;->writeToParcel(Landroid/os/Parcel;I)V

    iget-object v0, p0, Lcom/android/settings/LockPatternView$SavedState;->btM:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget v0, p0, Lcom/android/settings/LockPatternView$SavedState;->btJ:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-boolean v0, p0, Lcom/android/settings/LockPatternView$SavedState;->btL:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeValue(Ljava/lang/Object;)V

    iget-boolean v0, p0, Lcom/android/settings/LockPatternView$SavedState;->btK:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeValue(Ljava/lang/Object;)V

    iget-boolean v0, p0, Lcom/android/settings/LockPatternView$SavedState;->btN:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeValue(Ljava/lang/Object;)V

    return-void
.end method
