.class public Lcom/android/settings/AutoDisableScreenButtonsAppListSettings;
.super Lcom/android/settings/BaseFragment;
.source "AutoDisableScreenButtonsAppListSettings.java"


# static fields
.field public static final bxp:[I


# instance fields
.field private bxh:Lcom/android/settings/C;

.field private bxi:Lcom/android/settings/cN;

.field private bxj:Landroid/widget/CheckBox;

.field private bxk:Landroid/app/Dialog;

.field private bxl:Ljava/util/List;

.field private bxm:Ljava/util/List;

.field private bxn:Landroid/view/LayoutInflater;

.field public bxo:[Ljava/lang/String;

.field private bxq:Landroid/widget/ListView;

.field private bxr:Landroid/content/BroadcastReceiver;

.field private mContext:Landroid/content/Context;

.field private mPackageManager:Landroid/content/pm/PackageManager;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/4 v0, 0x1

    const/4 v1, 0x2

    const/4 v2, 0x3

    filled-new-array {v0, v1, v2}, [I

    move-result-object v0

    sput-object v0, Lcom/android/settings/AutoDisableScreenButtonsAppListSettings;->bxp:[I

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/android/settings/BaseFragment;-><init>()V

    new-instance v0, Lcom/android/settings/eb;

    invoke-direct {v0, p0}, Lcom/android/settings/eb;-><init>(Lcom/android/settings/AutoDisableScreenButtonsAppListSettings;)V

    iput-object v0, p0, Lcom/android/settings/AutoDisableScreenButtonsAppListSettings;->bxr:Landroid/content/BroadcastReceiver;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/settings/AutoDisableScreenButtonsAppListSettings;->bxm:Ljava/util/List;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/settings/AutoDisableScreenButtonsAppListSettings;->bxl:Ljava/util/List;

    return-void
.end method

.method private static bkJ(Landroid/content/Context;Ljava/lang/String;I)Ljava/lang/String;
    .locals 2

    const/4 v1, 0x0

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    return-object v1

    :cond_0
    const/4 v0, 0x1

    if-ne p2, v0, :cond_1

    const v0, 0x7f1201a6

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_1
    const/4 v0, 0x2

    if-ne p2, v0, :cond_2

    const v0, 0x7f1201a7

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_2
    const/4 v0, 0x3

    if-ne p2, v0, :cond_3

    const v0, 0x7f1201a9

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_3
    return-object v1
.end method

.method private bkK()V
    .locals 5

    const/4 v2, 0x0

    const-string/jumbo v0, "is_pad"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lmiui/util/FeatureParser;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/settings/AutoDisableScreenButtonsAppListSettings;->mContext:Landroid/content/Context;

    const v1, 0x7f0d0040

    invoke-static {v0, v1, v2}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    if-eqz v1, :cond_1

    iget-object v0, p0, Lcom/android/settings/AutoDisableScreenButtonsAppListSettings;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v2, "enable_auto_disable_screen_rotation"

    invoke-static {}, Lmiui/securityspace/CrossUserUtils;->getCurrentUserId()I

    move-result v3

    const/4 v4, 0x1

    invoke-static {v0, v2, v4, v3}, Landroid/provider/MiuiSettings$System;->getBooleanForUser(Landroid/content/ContentResolver;Ljava/lang/String;ZI)Z

    move-result v2

    const v0, 0x1020001

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/android/settings/AutoDisableScreenButtonsAppListSettings;->bxj:Landroid/widget/CheckBox;

    iget-object v0, p0, Lcom/android/settings/AutoDisableScreenButtonsAppListSettings;->bxj:Landroid/widget/CheckBox;

    invoke-virtual {v0, v2}, Landroid/widget/CheckBox;->setChecked(Z)V

    iget-object v0, p0, Lcom/android/settings/AutoDisableScreenButtonsAppListSettings;->bxj:Landroid/widget/CheckBox;

    new-instance v2, Lcom/android/settings/ec;

    invoke-direct {v2, p0}, Lcom/android/settings/ec;-><init>(Lcom/android/settings/AutoDisableScreenButtonsAppListSettings;)V

    invoke-virtual {v0, v2}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    new-instance v0, Lcom/android/settings/ed;

    invoke-direct {v0, p0}, Lcom/android/settings/ed;-><init>(Lcom/android/settings/AutoDisableScreenButtonsAppListSettings;)V

    invoke-virtual {v1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/android/settings/AutoDisableScreenButtonsAppListSettings;->bxq:Landroid/widget/ListView;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->addHeaderView(Landroid/view/View;)V

    :cond_1
    return-void
.end method

.method private bkL()V
    .locals 3

    const/4 v2, 0x0

    new-instance v0, Lcom/android/settings/ee;

    invoke-virtual {p0}, Lcom/android/settings/AutoDisableScreenButtonsAppListSettings;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/android/settings/ee;-><init>(Lcom/android/settings/AutoDisableScreenButtonsAppListSettings;Landroid/app/FragmentManager;)V

    invoke-virtual {v0, v2}, Lcom/android/settings/ee;->setCancelable(Z)Lmiui/os/AsyncTaskWithProgress;

    move-result-object v0

    new-array v1, v2, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lmiui/os/AsyncTaskWithProgress;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    return-void
.end method

.method static synthetic bkM(Lcom/android/settings/AutoDisableScreenButtonsAppListSettings;)Lcom/android/settings/C;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/AutoDisableScreenButtonsAppListSettings;->bxh:Lcom/android/settings/C;

    return-object v0
.end method

.method static synthetic bkN(Lcom/android/settings/AutoDisableScreenButtonsAppListSettings;)Lcom/android/settings/cN;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/AutoDisableScreenButtonsAppListSettings;->bxi:Lcom/android/settings/cN;

    return-object v0
.end method

.method static synthetic bkO(Lcom/android/settings/AutoDisableScreenButtonsAppListSettings;)Landroid/widget/CheckBox;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/AutoDisableScreenButtonsAppListSettings;->bxj:Landroid/widget/CheckBox;

    return-object v0
.end method

.method static synthetic bkP(Lcom/android/settings/AutoDisableScreenButtonsAppListSettings;)Landroid/content/Context;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/AutoDisableScreenButtonsAppListSettings;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic bkQ(Lcom/android/settings/AutoDisableScreenButtonsAppListSettings;)Landroid/app/Dialog;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/AutoDisableScreenButtonsAppListSettings;->bxk:Landroid/app/Dialog;

    return-object v0
.end method

.method static synthetic bkR(Lcom/android/settings/AutoDisableScreenButtonsAppListSettings;)Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/AutoDisableScreenButtonsAppListSettings;->bxl:Ljava/util/List;

    return-object v0
.end method

.method static synthetic bkS(Lcom/android/settings/AutoDisableScreenButtonsAppListSettings;)Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/AutoDisableScreenButtonsAppListSettings;->bxm:Ljava/util/List;

    return-object v0
.end method

.method static synthetic bkT(Lcom/android/settings/AutoDisableScreenButtonsAppListSettings;)Landroid/view/LayoutInflater;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/AutoDisableScreenButtonsAppListSettings;->bxn:Landroid/view/LayoutInflater;

    return-object v0
.end method

.method static synthetic bkU(Lcom/android/settings/AutoDisableScreenButtonsAppListSettings;)Landroid/content/pm/PackageManager;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/AutoDisableScreenButtonsAppListSettings;->mPackageManager:Landroid/content/pm/PackageManager;

    return-object v0
.end method

.method static synthetic bkV(Lcom/android/settings/AutoDisableScreenButtonsAppListSettings;Landroid/app/Dialog;)Landroid/app/Dialog;
    .locals 0

    iput-object p1, p0, Lcom/android/settings/AutoDisableScreenButtonsAppListSettings;->bxk:Landroid/app/Dialog;

    return-object p1
.end method

.method static synthetic bkW(Landroid/content/Context;Ljava/lang/String;I)Ljava/lang/String;
    .locals 1

    invoke-static {p0, p1, p2}, Lcom/android/settings/AutoDisableScreenButtonsAppListSettings;->bkJ(Landroid/content/Context;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic bkX(Lcom/android/settings/AutoDisableScreenButtonsAppListSettings;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/AutoDisableScreenButtonsAppListSettings;->bkL()V

    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 3

    invoke-super {p0, p1}, Lcom/android/settings/BaseFragment;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/android/settings/AutoDisableScreenButtonsAppListSettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/AutoDisableScreenButtonsAppListSettings;->mContext:Landroid/content/Context;

    iget-object v0, p0, Lcom/android/settings/AutoDisableScreenButtonsAppListSettings;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0xabea

    invoke-static {v0, v1}, Lcom/android/settings/ar;->brN(Landroid/content/Context;I)V

    new-instance v0, Lcom/android/settings/cN;

    iget-object v1, p0, Lcom/android/settings/AutoDisableScreenButtonsAppListSettings;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/android/settings/cN;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/settings/AutoDisableScreenButtonsAppListSettings;->bxi:Lcom/android/settings/cN;

    invoke-virtual {p0}, Lcom/android/settings/AutoDisableScreenButtonsAppListSettings;->bLS()Landroid/content/pm/PackageManager;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/AutoDisableScreenButtonsAppListSettings;->mPackageManager:Landroid/content/pm/PackageManager;

    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    const-string/jumbo v1, "android.intent.action.PACKAGE_ADDED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string/jumbo v1, "android.intent.action.PACKAGE_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string/jumbo v1, "android.intent.action.PACKAGE_FULLY_REMOVED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string/jumbo v1, "android.intent.action.PACKAGE_REMOVED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string/jumbo v1, "android.intent.action.PACKAGE_REPLACED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/android/settings/AutoDisableScreenButtonsAppListSettings;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/android/settings/AutoDisableScreenButtonsAppListSettings;->bxr:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/String;

    const v1, 0x7f1201a6

    invoke-virtual {p0, v1}, Lcom/android/settings/AutoDisableScreenButtonsAppListSettings;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    aput-object v1, v0, v2

    const v1, 0x7f1201a7

    invoke-virtual {p0, v1}, Lcom/android/settings/AutoDisableScreenButtonsAppListSettings;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    aput-object v1, v0, v2

    const v1, 0x7f1201a9

    invoke-virtual {p0, v1}, Lcom/android/settings/AutoDisableScreenButtonsAppListSettings;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x2

    aput-object v1, v0, v2

    iput-object v0, p0, Lcom/android/settings/AutoDisableScreenButtonsAppListSettings;->bxo:[Ljava/lang/String;

    return-void
.end method

.method public onDestroy()V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/android/settings/AutoDisableScreenButtonsAppListSettings;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/android/settings/AutoDisableScreenButtonsAppListSettings;->bxr:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    iget-object v0, p0, Lcom/android/settings/AutoDisableScreenButtonsAppListSettings;->bxi:Lcom/android/settings/cN;

    invoke-virtual {v0}, Lcom/android/settings/cN;->stop()V

    iget-object v0, p0, Lcom/android/settings/AutoDisableScreenButtonsAppListSettings;->bxk:Landroid/app/Dialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/AutoDisableScreenButtonsAppListSettings;->bxk:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/AutoDisableScreenButtonsAppListSettings;->bxk:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    :cond_0
    iput-object v2, p0, Lcom/android/settings/AutoDisableScreenButtonsAppListSettings;->bxk:Landroid/app/Dialog;

    invoke-super {p0}, Lcom/android/settings/BaseFragment;->onDestroy()V

    return-void
.end method

.method public onInflateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3

    iput-object p1, p0, Lcom/android/settings/AutoDisableScreenButtonsAppListSettings;->bxn:Landroid/view/LayoutInflater;

    const v0, 0x7f0d003d

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    if-eqz p2, :cond_0

    invoke-virtual {p2}, Landroid/view/ViewGroup;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    instance-of v2, v0, Landroid/preference/PreferenceFrameLayout$LayoutParams;

    if-eqz v2, :cond_0

    check-cast v0, Landroid/preference/PreferenceFrameLayout$LayoutParams;

    const/4 v2, 0x1

    iput-boolean v2, v0, Landroid/preference/PreferenceFrameLayout$LayoutParams;->removeBorders:Z

    :cond_0
    return-object v1
.end method

.method public onResume()V
    .locals 0

    invoke-super {p0}, Lcom/android/settings/BaseFragment;->onResume()V

    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1, p2}, Lcom/android/settings/BaseFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    const v0, 0x7f0a0270

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/android/settings/AutoDisableScreenButtonsAppListSettings;->bxq:Landroid/widget/ListView;

    new-instance v0, Lcom/android/settings/C;

    invoke-direct {v0, p0}, Lcom/android/settings/C;-><init>(Lcom/android/settings/AutoDisableScreenButtonsAppListSettings;)V

    iput-object v0, p0, Lcom/android/settings/AutoDisableScreenButtonsAppListSettings;->bxh:Lcom/android/settings/C;

    iget-object v0, p0, Lcom/android/settings/AutoDisableScreenButtonsAppListSettings;->bxq:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/android/settings/AutoDisableScreenButtonsAppListSettings;->bxh:Lcom/android/settings/C;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    invoke-direct {p0}, Lcom/android/settings/AutoDisableScreenButtonsAppListSettings;->bkK()V

    invoke-direct {p0}, Lcom/android/settings/AutoDisableScreenButtonsAppListSettings;->bkL()V

    return-void
.end method
