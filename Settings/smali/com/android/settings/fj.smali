.class final Lcom/android/settings/fj;
.super Ljava/lang/Object;
.source "ToggleAnimHelper.java"

# interfaces
.implements Landroid/animation/Animator$AnimatorListener;


# instance fields
.field final synthetic ckd:Lcom/android/settings/ap;

.field final synthetic cke:I

.field final synthetic ckf:Z

.field final synthetic ckg:Landroid/widget/ImageView;


# direct methods
.method constructor <init>(Lcom/android/settings/ap;IZLandroid/widget/ImageView;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/fj;->ckd:Lcom/android/settings/ap;

    iput p2, p0, Lcom/android/settings/fj;->cke:I

    iput-boolean p3, p0, Lcom/android/settings/fj;->ckf:Z

    iput-object p4, p0, Lcom/android/settings/fj;->ckg:Landroid/widget/ImageView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationCancel(Landroid/animation/Animator;)V
    .locals 0

    return-void
.end method

.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 4

    iget-object v0, p0, Lcom/android/settings/fj;->ckg:Landroid/widget/ImageView;

    new-instance v1, Lcom/android/settings/fk;

    iget v2, p0, Lcom/android/settings/fj;->cke:I

    iget-object v3, p0, Lcom/android/settings/fj;->ckg:Landroid/widget/ImageView;

    invoke-direct {v1, p0, v2, v3}, Lcom/android/settings/fk;-><init>(Lcom/android/settings/fj;ILandroid/widget/ImageView;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public onAnimationRepeat(Landroid/animation/Animator;)V
    .locals 0

    return-void
.end method

.method public onAnimationStart(Landroid/animation/Animator;)V
    .locals 3

    iget-object v0, p0, Lcom/android/settings/fj;->ckd:Lcom/android/settings/ap;

    invoke-static {v0}, Lcom/android/settings/ap;->bqp(Lcom/android/settings/ap;)Ljava/util/HashMap;

    move-result-object v0

    iget v1, p0, Lcom/android/settings/fj;->cke:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iget-boolean v2, p0, Lcom/android/settings/fj;->ckf:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method
