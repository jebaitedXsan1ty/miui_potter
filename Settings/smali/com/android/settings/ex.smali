.class final Lcom/android/settings/ex;
.super Landroid/content/BroadcastReceiver;
.source "BatteryInfo.java"


# instance fields
.field final synthetic cjh:Lcom/android/settings/BatteryInfo;


# direct methods
.method constructor <init>(Lcom/android/settings/BatteryInfo;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/ex;->cjh:Lcom/android/settings/BatteryInfo;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 6

    const/4 v5, 0x0

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "android.intent.action.BATTERY_CHANGED"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string/jumbo v0, "plugged"

    invoke-virtual {p2, v0, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    iget-object v1, p0, Lcom/android/settings/ex;->cjh:Lcom/android/settings/BatteryInfo;

    invoke-static {v1}, Lcom/android/settings/BatteryInfo;->bme(Lcom/android/settings/BatteryInfo;)Landroid/widget/TextView;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "level"

    invoke-virtual {p2, v3, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/android/settings/ex;->cjh:Lcom/android/settings/BatteryInfo;

    invoke-static {v1}, Lcom/android/settings/BatteryInfo;->bmg(Lcom/android/settings/BatteryInfo;)Landroid/widget/TextView;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "scale"

    invoke-virtual {p2, v3, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/android/settings/ex;->cjh:Lcom/android/settings/BatteryInfo;

    invoke-static {v1}, Lcom/android/settings/BatteryInfo;->bmk(Lcom/android/settings/BatteryInfo;)Landroid/widget/TextView;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "voltage"

    invoke-virtual {p2, v3, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/settings/ex;->cjh:Lcom/android/settings/BatteryInfo;

    const v4, 0x7f12023c

    invoke-virtual {v3, v4}, Lcom/android/settings/BatteryInfo;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/android/settings/ex;->cjh:Lcom/android/settings/BatteryInfo;

    invoke-static {v1}, Lcom/android/settings/BatteryInfo;->bmj(Lcom/android/settings/BatteryInfo;)Landroid/widget/TextView;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/settings/ex;->cjh:Lcom/android/settings/BatteryInfo;

    const-string/jumbo v4, "temperature"

    invoke-virtual {p2, v4, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v4

    invoke-static {v3, v4}, Lcom/android/settings/BatteryInfo;->bml(Lcom/android/settings/BatteryInfo;I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/settings/ex;->cjh:Lcom/android/settings/BatteryInfo;

    const v4, 0x7f120239

    invoke-virtual {v3, v4}, Lcom/android/settings/BatteryInfo;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/android/settings/ex;->cjh:Lcom/android/settings/BatteryInfo;

    invoke-static {v1}, Lcom/android/settings/BatteryInfo;->bmi(Lcom/android/settings/BatteryInfo;)Landroid/widget/TextView;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "technology"

    invoke-virtual {p2, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/android/settings/ex;->cjh:Lcom/android/settings/BatteryInfo;

    invoke-static {v1}, Lcom/android/settings/BatteryInfo;->bmh(Lcom/android/settings/BatteryInfo;)Landroid/widget/TextView;

    move-result-object v1

    iget-object v2, p0, Lcom/android/settings/ex;->cjh:Lcom/android/settings/BatteryInfo;

    invoke-virtual {v2}, Lcom/android/settings/BatteryInfo;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-static {v2, p2}, Lcom/android/settings/aq;->cqz(Landroid/content/res/Resources;Landroid/content/Intent;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    packed-switch v0, :pswitch_data_0

    iget-object v0, p0, Lcom/android/settings/ex;->cjh:Lcom/android/settings/BatteryInfo;

    invoke-static {v0}, Lcom/android/settings/BatteryInfo;->bmf(Lcom/android/settings/BatteryInfo;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/ex;->cjh:Lcom/android/settings/BatteryInfo;

    const v2, 0x7f120227

    invoke-virtual {v1, v2}, Lcom/android/settings/BatteryInfo;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_0
    const-string/jumbo v0, "health"

    const/4 v1, 0x1

    invoke-virtual {p2, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/android/settings/ex;->cjh:Lcom/android/settings/BatteryInfo;

    const v1, 0x7f12021c

    invoke-virtual {v0, v1}, Lcom/android/settings/BatteryInfo;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_1
    iget-object v1, p0, Lcom/android/settings/ex;->cjh:Lcom/android/settings/BatteryInfo;

    invoke-static {v1}, Lcom/android/settings/BatteryInfo;->bmd(Lcom/android/settings/BatteryInfo;)Landroid/widget/TextView;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_0
    return-void

    :pswitch_0
    iget-object v0, p0, Lcom/android/settings/ex;->cjh:Lcom/android/settings/BatteryInfo;

    invoke-static {v0}, Lcom/android/settings/BatteryInfo;->bmf(Lcom/android/settings/BatteryInfo;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/ex;->cjh:Lcom/android/settings/BatteryInfo;

    const v2, 0x7f120228

    invoke-virtual {v1, v2}, Lcom/android/settings/BatteryInfo;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Lcom/android/settings/ex;->cjh:Lcom/android/settings/BatteryInfo;

    invoke-static {v0}, Lcom/android/settings/BatteryInfo;->bmf(Lcom/android/settings/BatteryInfo;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/ex;->cjh:Lcom/android/settings/BatteryInfo;

    const v2, 0x7f120224

    invoke-virtual {v1, v2}, Lcom/android/settings/BatteryInfo;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    :pswitch_2
    iget-object v0, p0, Lcom/android/settings/ex;->cjh:Lcom/android/settings/BatteryInfo;

    invoke-static {v0}, Lcom/android/settings/BatteryInfo;->bmf(Lcom/android/settings/BatteryInfo;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/ex;->cjh:Lcom/android/settings/BatteryInfo;

    const v2, 0x7f120229

    invoke-virtual {v1, v2}, Lcom/android/settings/BatteryInfo;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    :pswitch_3
    iget-object v0, p0, Lcom/android/settings/ex;->cjh:Lcom/android/settings/BatteryInfo;

    invoke-static {v0}, Lcom/android/settings/BatteryInfo;->bmf(Lcom/android/settings/BatteryInfo;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/ex;->cjh:Lcom/android/settings/BatteryInfo;

    const v2, 0x7f12022a

    invoke-virtual {v1, v2}, Lcom/android/settings/BatteryInfo;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    :pswitch_4
    iget-object v0, p0, Lcom/android/settings/ex;->cjh:Lcom/android/settings/BatteryInfo;

    invoke-static {v0}, Lcom/android/settings/BatteryInfo;->bmf(Lcom/android/settings/BatteryInfo;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/ex;->cjh:Lcom/android/settings/BatteryInfo;

    const v2, 0x7f120225

    invoke-virtual {v1, v2}, Lcom/android/settings/BatteryInfo;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_1
    const/4 v1, 0x3

    if-ne v0, v1, :cond_2

    iget-object v0, p0, Lcom/android/settings/ex;->cjh:Lcom/android/settings/BatteryInfo;

    const v1, 0x7f12021f

    invoke-virtual {v0, v1}, Lcom/android/settings/BatteryInfo;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_2
    const/4 v1, 0x4

    if-ne v0, v1, :cond_3

    iget-object v0, p0, Lcom/android/settings/ex;->cjh:Lcom/android/settings/BatteryInfo;

    const v1, 0x7f12021b

    invoke-virtual {v0, v1}, Lcom/android/settings/BatteryInfo;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_1

    :cond_3
    const/4 v1, 0x5

    if-ne v0, v1, :cond_4

    iget-object v0, p0, Lcom/android/settings/ex;->cjh:Lcom/android/settings/BatteryInfo;

    const v1, 0x7f12021e

    invoke-virtual {v0, v1}, Lcom/android/settings/BatteryInfo;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_1

    :cond_4
    const/4 v1, 0x6

    if-ne v0, v1, :cond_5

    iget-object v0, p0, Lcom/android/settings/ex;->cjh:Lcom/android/settings/BatteryInfo;

    const v1, 0x7f120221

    invoke-virtual {v0, v1}, Lcom/android/settings/BatteryInfo;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_1

    :cond_5
    const/4 v1, 0x7

    if-ne v0, v1, :cond_6

    iget-object v0, p0, Lcom/android/settings/ex;->cjh:Lcom/android/settings/BatteryInfo;

    const v1, 0x7f12021a

    invoke-virtual {v0, v1}, Lcom/android/settings/BatteryInfo;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_1

    :cond_6
    iget-object v0, p0, Lcom/android/settings/ex;->cjh:Lcom/android/settings/BatteryInfo;

    const v1, 0x7f120220

    invoke-virtual {v0, v1}, Lcom/android/settings/BatteryInfo;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_4
        :pswitch_3
    .end packed-switch
.end method
