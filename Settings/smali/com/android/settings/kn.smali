.class final Lcom/android/settings/kn;
.super Landroid/content/BroadcastReceiver;
.source "TetherService.java"


# instance fields
.field final synthetic cqS:Lcom/android/settings/TetherService;


# direct methods
.method constructor <init>(Lcom/android/settings/TetherService;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/kn;->cqS:Lcom/android/settings/TetherService;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 4

    const/4 v3, 0x0

    invoke-static {}, Lcom/android/settings/TetherService;->-get0()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string/jumbo v0, "TetherService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Got provision result "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v0, p0, Lcom/android/settings/kn;->cqS:Lcom/android/settings/TetherService;

    invoke-virtual {v0}, Lcom/android/settings/TetherService;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x1040152

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/android/settings/kn;->cqS:Lcom/android/settings/TetherService;

    invoke-static {v0}, Lcom/android/settings/TetherService;->bSk(Lcom/android/settings/TetherService;)Z

    move-result v0

    if-nez v0, :cond_1

    const-string/jumbo v0, "TetherService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Unexpected provision response "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/settings/kn;->cqS:Lcom/android/settings/TetherService;

    invoke-static {v0}, Lcom/android/settings/TetherService;->bSi(Lcom/android/settings/TetherService;)Ljava/util/ArrayList;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/kn;->cqS:Lcom/android/settings/TetherService;

    invoke-static {v1}, Lcom/android/settings/TetherService;->bSj(Lcom/android/settings/TetherService;)I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iget-object v1, p0, Lcom/android/settings/kn;->cqS:Lcom/android/settings/TetherService;

    invoke-static {v1, v3}, Lcom/android/settings/TetherService;->bSm(Lcom/android/settings/TetherService;Z)Z

    const-string/jumbo v1, "EntitlementResult"

    invoke-virtual {p2, v1, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    const/4 v2, -0x1

    if-eq v1, v2, :cond_2

    packed-switch v0, :pswitch_data_0

    :cond_2
    :goto_0
    iget-object v2, p0, Lcom/android/settings/kn;->cqS:Lcom/android/settings/TetherService;

    invoke-static {v2, v0, v1}, Lcom/android/settings/TetherService;->bSq(Lcom/android/settings/TetherService;II)V

    iget-object v0, p0, Lcom/android/settings/kn;->cqS:Lcom/android/settings/TetherService;

    invoke-static {v0}, Lcom/android/settings/TetherService;->bSj(Lcom/android/settings/TetherService;)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    invoke-static {v0, v1}, Lcom/android/settings/TetherService;->bSl(Lcom/android/settings/TetherService;I)I

    move-result v0

    iget-object v1, p0, Lcom/android/settings/kn;->cqS:Lcom/android/settings/TetherService;

    invoke-static {v1}, Lcom/android/settings/TetherService;->bSi(Lcom/android/settings/TetherService;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lt v0, v1, :cond_4

    iget-object v0, p0, Lcom/android/settings/kn;->cqS:Lcom/android/settings/TetherService;

    invoke-virtual {v0}, Lcom/android/settings/TetherService;->stopSelf()V

    :cond_3
    :goto_1
    return-void

    :pswitch_0
    iget-object v2, p0, Lcom/android/settings/kn;->cqS:Lcom/android/settings/TetherService;

    invoke-static {v2}, Lcom/android/settings/TetherService;->bSp(Lcom/android/settings/TetherService;)V

    goto :goto_0

    :pswitch_1
    iget-object v2, p0, Lcom/android/settings/kn;->cqS:Lcom/android/settings/TetherService;

    invoke-static {v2}, Lcom/android/settings/TetherService;->bSn(Lcom/android/settings/TetherService;)V

    goto :goto_0

    :pswitch_2
    iget-object v2, p0, Lcom/android/settings/kn;->cqS:Lcom/android/settings/TetherService;

    invoke-static {v2}, Lcom/android/settings/TetherService;->bSo(Lcom/android/settings/TetherService;)V

    goto :goto_0

    :cond_4
    iget-object v0, p0, Lcom/android/settings/kn;->cqS:Lcom/android/settings/TetherService;

    iget-object v1, p0, Lcom/android/settings/kn;->cqS:Lcom/android/settings/TetherService;

    invoke-static {v1}, Lcom/android/settings/TetherService;->bSj(Lcom/android/settings/TetherService;)I

    move-result v1

    invoke-static {v0, v1}, Lcom/android/settings/TetherService;->bSr(Lcom/android/settings/TetherService;I)V

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method
