.class public Lcom/android/settings/support/SupportDisclaimerDialogFragment$SystemInformationSpan;
.super Landroid/text/style/URLSpan;
.source "SupportDisclaimerDialogFragment.java"


# instance fields
.field private final beM:Landroid/app/DialogFragment;

.field private beN:Lcom/android/settings/overlay/b;


# direct methods
.method public constructor <init>(Landroid/app/DialogFragment;)V
    .locals 2

    const-string/jumbo v0, ""

    invoke-direct {p0, v0}, Landroid/text/style/URLSpan;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/app/DialogFragment;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/android/settings/overlay/a;->aIk(Landroid/content/Context;)Lcom/android/settings/overlay/a;

    move-result-object v0

    invoke-virtual {p1}, Landroid/app/DialogFragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/settings/overlay/a;->aIw(Landroid/content/Context;)Lcom/android/settings/overlay/b;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/support/SupportDisclaimerDialogFragment$SystemInformationSpan;->beN:Lcom/android/settings/overlay/b;

    iput-object p1, p0, Lcom/android/settings/support/SupportDisclaimerDialogFragment$SystemInformationSpan;->beM:Landroid/app/DialogFragment;

    return-void
.end method

.method public static aSV(Landroid/text/Spannable;Landroid/app/DialogFragment;)Ljava/lang/CharSequence;
    .locals 8

    const/4 v1, 0x0

    invoke-interface {p0}, Landroid/text/Spannable;->length()I

    move-result v0

    const-class v2, Landroid/text/Annotation;

    invoke-interface {p0, v1, v0, v2}, Landroid/text/Spannable;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Landroid/text/Annotation;

    array-length v2, v0

    :goto_0
    if-ge v1, v2, :cond_1

    aget-object v3, v0, v1

    invoke-interface {p0, v3}, Landroid/text/Spannable;->getSpanStart(Ljava/lang/Object;)I

    move-result v4

    invoke-interface {p0, v3}, Landroid/text/Spannable;->getSpanEnd(Ljava/lang/Object;)I

    move-result v5

    const-string/jumbo v6, "url"

    invoke-virtual {v3}, Landroid/text/Annotation;->getValue()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    new-instance v6, Lcom/android/settings/support/SupportDisclaimerDialogFragment$SystemInformationSpan;

    invoke-direct {v6, p1}, Lcom/android/settings/support/SupportDisclaimerDialogFragment$SystemInformationSpan;-><init>(Landroid/app/DialogFragment;)V

    invoke-interface {p0, v3}, Landroid/text/Spannable;->removeSpan(Ljava/lang/Object;)V

    const/16 v3, 0x21

    invoke-interface {p0, v6, v4, v5, v3}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    return-object p0
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3

    iget-object v0, p0, Lcom/android/settings/support/SupportDisclaimerDialogFragment$SystemInformationSpan;->beM:Landroid/app/DialogFragment;

    invoke-virtual {v0}, Landroid/app/DialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/support/SupportDisclaimerDialogFragment$SystemInformationSpan;->beN:Lcom/android/settings/overlay/b;

    if-eqz v1, :cond_0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/android/settings/support/SupportDisclaimerDialogFragment$SystemInformationSpan;->beN:Lcom/android/settings/overlay/b;

    iget-object v2, p0, Lcom/android/settings/support/SupportDisclaimerDialogFragment$SystemInformationSpan;->beM:Landroid/app/DialogFragment;

    invoke-virtual {v2}, Landroid/app/DialogFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v2

    invoke-virtual {v0}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-interface {v1, v2, v0}, Lcom/android/settings/overlay/b;->aIy(Landroid/os/Bundle;Landroid/app/FragmentManager;)V

    iget-object v0, p0, Lcom/android/settings/support/SupportDisclaimerDialogFragment$SystemInformationSpan;->beM:Landroid/app/DialogFragment;

    invoke-virtual {v0}, Landroid/app/DialogFragment;->dismiss()V

    :cond_0
    return-void
.end method

.method public setSupportProvider(Lcom/android/settings/overlay/b;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/support/SupportDisclaimerDialogFragment$SystemInformationSpan;->beN:Lcom/android/settings/overlay/b;

    return-void
.end method

.method public updateDrawState(Landroid/text/TextPaint;)V
    .locals 1

    invoke-super {p0, p1}, Landroid/text/style/URLSpan;->updateDrawState(Landroid/text/TextPaint;)V

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/text/TextPaint;->setUnderlineText(Z)V

    return-void
.end method
