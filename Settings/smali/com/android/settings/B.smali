.class Lcom/android/settings/B;
.super Ljava/lang/Object;
.source "AutoDisableScreenButtonsAppListSettings.java"


# instance fields
.field private bxs:Landroid/content/pm/ApplicationInfo;

.field private bxt:Ljava/lang/String;

.field private bxu:Ljava/lang/String;

.field private bxv:I

.field final synthetic bxw:Lcom/android/settings/AutoDisableScreenButtonsAppListSettings;

.field private mFlag:I


# direct methods
.method public constructor <init>(Lcom/android/settings/AutoDisableScreenButtonsAppListSettings;Landroid/content/pm/ApplicationInfo;I)V
    .locals 3

    iput-object p1, p0, Lcom/android/settings/B;->bxw:Lcom/android/settings/AutoDisableScreenButtonsAppListSettings;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, -0x1

    iput v0, p0, Lcom/android/settings/B;->bxv:I

    iput-object p2, p0, Lcom/android/settings/B;->bxs:Landroid/content/pm/ApplicationInfo;

    invoke-static {p1}, Lcom/android/settings/AutoDisableScreenButtonsAppListSettings;->bkU(Lcom/android/settings/AutoDisableScreenButtonsAppListSettings;)Landroid/content/pm/PackageManager;

    move-result-object v0

    invoke-virtual {p2, v0}, Landroid/content/pm/ApplicationInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "\\u00A0"

    const-string/jumbo v2, " "

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/B;->bxu:Ljava/lang/String;

    const/4 v0, 0x0

    iput v0, p0, Lcom/android/settings/B;->bxv:I

    invoke-virtual {p0, p3}, Lcom/android/settings/B;->bkZ(I)V

    return-void
.end method

.method public constructor <init>(Lcom/android/settings/AutoDisableScreenButtonsAppListSettings;Ljava/lang/String;)V
    .locals 1

    iput-object p1, p0, Lcom/android/settings/B;->bxw:Lcom/android/settings/AutoDisableScreenButtonsAppListSettings;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, -0x1

    iput v0, p0, Lcom/android/settings/B;->bxv:I

    iput-object p2, p0, Lcom/android/settings/B;->bxu:Ljava/lang/String;

    const/4 v0, 0x1

    iput v0, p0, Lcom/android/settings/B;->bxv:I

    return-void
.end method

.method static synthetic bla(Lcom/android/settings/B;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/B;->bxt:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic blb(Lcom/android/settings/B;)I
    .locals 1

    iget v0, p0, Lcom/android/settings/B;->mFlag:I

    return v0
.end method

.method static synthetic blc(Lcom/android/settings/B;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/B;->bxu:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic bld(Lcom/android/settings/B;)I
    .locals 1

    iget v0, p0, Lcom/android/settings/B;->bxv:I

    return v0
.end method


# virtual methods
.method public bkY()Ljava/lang/String;
    .locals 2

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/android/settings/B;->bxs:Landroid/content/pm/ApplicationInfo;

    if-nez v1, :cond_0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/android/settings/B;->bxs:Landroid/content/pm/ApplicationInfo;

    iget-object v0, v0, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    goto :goto_0
.end method

.method public bkZ(I)V
    .locals 2

    iput p1, p0, Lcom/android/settings/B;->mFlag:I

    iget-object v0, p0, Lcom/android/settings/B;->bxw:Lcom/android/settings/AutoDisableScreenButtonsAppListSettings;

    invoke-static {v0}, Lcom/android/settings/AutoDisableScreenButtonsAppListSettings;->bkP(Lcom/android/settings/AutoDisableScreenButtonsAppListSettings;)Landroid/content/Context;

    move-result-object v0

    invoke-virtual {p0}, Lcom/android/settings/B;->bkY()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, p1}, Lcom/android/settings/AutoDisableScreenButtonsAppListSettings;->bkW(Landroid/content/Context;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/B;->bxt:Ljava/lang/String;

    return-void
.end method
