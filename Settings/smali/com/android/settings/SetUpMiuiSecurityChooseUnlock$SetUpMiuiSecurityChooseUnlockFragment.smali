.class public Lcom/android/settings/SetUpMiuiSecurityChooseUnlock$SetUpMiuiSecurityChooseUnlockFragment;
.super Lcom/android/settings/KeyguardSettingsPreferenceFragment;
.source "SetUpMiuiSecurityChooseUnlock.java"


# instance fields
.field private bMQ:Landroid/widget/TextView;

.field private bMR:Lcom/android/settings/cx;

.field private bMS:Landroid/app/admin/DevicePolicyManager;

.field private bMT:Z

.field private bMU:Landroid/widget/TextView;

.field private bMV:Landroid/widget/TextView;

.field private bMW:Landroid/widget/TextView;

.field private bMX:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/android/settings/KeyguardSettingsPreferenceFragment;-><init>()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/settings/SetUpMiuiSecurityChooseUnlock$SetUpMiuiSecurityChooseUnlockFragment;->bMT:Z

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/settings/SetUpMiuiSecurityChooseUnlock$SetUpMiuiSecurityChooseUnlockFragment;->bMX:Ljava/lang/String;

    return-void
.end method

.method private bEq(Ljava/lang/String;)V
    .locals 2

    const/16 v1, 0x12e

    const-string/jumbo v0, "setup_unlock_set_pattern"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lcom/android/settings/SetUpMiuiSecurityChooseUnlock$SetUpMiuiSecurityChooseUnlockFragment;->getTargetFragment()Landroid/app/Fragment;

    move-result-object v0

    if-nez v0, :cond_1

    const/high16 v0, 0x10000

    invoke-static {v0, p0}, Lcom/android/settings/SetUpMiuiSecurityChooseUnlock;->bDY(ILcom/android/settings/KeyguardSettingsPreferenceFragment;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-static {p0, v1}, Lcom/android/settings/SetUpMiuiSecurityChooseUnlock;->bEc(Lcom/android/settings/KeyguardSettingsPreferenceFragment;I)V

    goto :goto_0

    :cond_2
    const-string/jumbo v0, "setup_unlock_set_pin"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-virtual {p0}, Lcom/android/settings/SetUpMiuiSecurityChooseUnlock$SetUpMiuiSecurityChooseUnlockFragment;->getTargetFragment()Landroid/app/Fragment;

    move-result-object v0

    if-nez v0, :cond_3

    const/high16 v0, 0x20000

    invoke-static {v0, p0}, Lcom/android/settings/SetUpMiuiSecurityChooseUnlock;->bDY(ILcom/android/settings/KeyguardSettingsPreferenceFragment;)V

    goto :goto_0

    :cond_3
    invoke-static {p0, v1}, Lcom/android/settings/SetUpMiuiSecurityChooseUnlock;->bEa(Lcom/android/settings/KeyguardSettingsPreferenceFragment;I)V

    goto :goto_0

    :cond_4
    const-string/jumbo v0, "setup_unlock_set_password"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/android/settings/SetUpMiuiSecurityChooseUnlock$SetUpMiuiSecurityChooseUnlockFragment;->getTargetFragment()Landroid/app/Fragment;

    move-result-object v0

    if-nez v0, :cond_5

    const/high16 v0, 0x40000

    invoke-static {v0, p0}, Lcom/android/settings/SetUpMiuiSecurityChooseUnlock;->bDY(ILcom/android/settings/KeyguardSettingsPreferenceFragment;)V

    goto :goto_0

    :cond_5
    invoke-static {p0, v1}, Lcom/android/settings/SetUpMiuiSecurityChooseUnlock;->bDZ(Lcom/android/settings/KeyguardSettingsPreferenceFragment;I)V

    goto :goto_0
.end method

.method private bEr()V
    .locals 2

    const/4 v1, 0x0

    invoke-static {}, Lcom/android/settings/SetUpMiuiSecurityChooseUnlock;->bEi()Lcom/android/settings/SetUpRadioButtonPreference;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/android/settings/SetUpRadioButtonPreference;->bTy(Z)V

    invoke-static {}, Lcom/android/settings/SetUpMiuiSecurityChooseUnlock;->bEh()Lcom/android/settings/SetUpRadioButtonPreference;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/android/settings/SetUpRadioButtonPreference;->bTy(Z)V

    invoke-static {}, Lcom/android/settings/SetUpMiuiSecurityChooseUnlock;->bEg()Lcom/android/settings/SetUpRadioButtonPreference;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/android/settings/SetUpRadioButtonPreference;->bTy(Z)V

    invoke-static {}, Lcom/android/settings/SetUpMiuiSecurityChooseUnlock;->bEi()Lcom/android/settings/SetUpRadioButtonPreference;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/android/settings/SetUpRadioButtonPreference;->setChecked(Z)V

    invoke-static {}, Lcom/android/settings/SetUpMiuiSecurityChooseUnlock;->bEh()Lcom/android/settings/SetUpRadioButtonPreference;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/android/settings/SetUpRadioButtonPreference;->setChecked(Z)V

    invoke-static {}, Lcom/android/settings/SetUpMiuiSecurityChooseUnlock;->bEg()Lcom/android/settings/SetUpRadioButtonPreference;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/android/settings/SetUpRadioButtonPreference;->setChecked(Z)V

    return-void
.end method

.method private bEs(Ljava/lang/String;)V
    .locals 7

    const/4 v3, 0x0

    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/android/settings/SetUpMiuiSecurityChooseUnlock$SetUpMiuiSecurityChooseUnlockFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v3}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    const v1, 0x1010355

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setIconAttribute(I)Landroid/app/AlertDialog$Builder;

    const v1, 0x7f1212d4

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    const v1, 0x7f1212d3

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    new-instance v1, Lcom/android/settings/hO;

    invoke-direct {v1, p0, p1}, Lcom/android/settings/hO;-><init>(Lcom/android/settings/SetUpMiuiSecurityChooseUnlock$SetUpMiuiSecurityChooseUnlockFragment;Ljava/lang/String;)V

    const v2, 0x7f1212d1

    invoke-virtual {v0, v2, v1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    new-instance v1, Lcom/android/settings/hP;

    invoke-direct {v1, p0}, Lcom/android/settings/hP;-><init>(Lcom/android/settings/SetUpMiuiSecurityChooseUnlock$SetUpMiuiSecurityChooseUnlockFragment;)V

    const v2, 0x7f1212d0

    invoke-virtual {v0, v2, v1}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v6

    invoke-virtual {v6, v3}, Landroid/widget/Button;->setClickable(Z)V

    new-instance v0, Lcom/android/settings/hQ;

    const-wide/16 v2, 0x1388

    const-wide/16 v4, 0x3e8

    move-object v1, p0

    invoke-direct/range {v0 .. v6}, Lcom/android/settings/hQ;-><init>(Lcom/android/settings/SetUpMiuiSecurityChooseUnlock$SetUpMiuiSecurityChooseUnlockFragment;JJLandroid/widget/Button;)V

    invoke-virtual {v0}, Lcom/android/settings/hQ;->start()Landroid/os/CountDownTimer;

    move-result-object v0

    invoke-static {v0}, Lcom/android/settings/SetUpMiuiSecurityChooseUnlock;->bEk(Landroid/os/CountDownTimer;)Landroid/os/CountDownTimer;

    return-void
.end method

.method static synthetic bEt(Lcom/android/settings/SetUpMiuiSecurityChooseUnlock$SetUpMiuiSecurityChooseUnlockFragment;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/settings/SetUpMiuiSecurityChooseUnlock$SetUpMiuiSecurityChooseUnlockFragment;->bEq(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic bEu(Lcom/android/settings/SetUpMiuiSecurityChooseUnlock$SetUpMiuiSecurityChooseUnlockFragment;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/SetUpMiuiSecurityChooseUnlock$SetUpMiuiSecurityChooseUnlockFragment;->bEr()V

    return-void
.end method


# virtual methods
.method public QH(ILandroid/os/Bundle;)V
    .locals 5

    const/4 v1, -0x1

    const/4 v0, 0x0

    if-eqz p2, :cond_2

    const-string/jumbo v2, "miui_security_fragment_result"

    invoke-virtual {p2, v2, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v2

    if-nez v2, :cond_1

    const/4 v2, 0x1

    :goto_0
    const/16 v3, 0x12e

    if-ne p1, v3, :cond_0

    invoke-virtual {p0}, Lcom/android/settings/SetUpMiuiSecurityChooseUnlock$SetUpMiuiSecurityChooseUnlockFragment;->finish()V

    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    const-string/jumbo v4, "miui_security_fragment_result"

    if-eqz v2, :cond_3

    :goto_1
    invoke-virtual {v3, v4, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    invoke-virtual {p0}, Lcom/android/settings/SetUpMiuiSecurityChooseUnlock$SetUpMiuiSecurityChooseUnlockFragment;->getTargetFragment()Landroid/app/Fragment;

    move-result-object v0

    invoke-virtual {p0}, Lcom/android/settings/SetUpMiuiSecurityChooseUnlock$SetUpMiuiSecurityChooseUnlockFragment;->getTargetRequestCode()I

    move-result v1

    invoke-static {v0, v1, v3}, Lcom/android/settings/aN;->bwR(Landroid/app/Fragment;ILandroid/os/Bundle;)V

    :cond_0
    return-void

    :cond_1
    move v2, v0

    goto :goto_0

    :cond_2
    move v2, v0

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_1
.end method

.method public cC()Ljava/lang/String;
    .locals 1

    const-class v0, Lcom/android/settings/SetUpMiuiSecurityChooseUnlock$SetUpMiuiSecurityChooseUnlockFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 2

    invoke-super {p0, p1, p2, p3}, Lcom/android/settings/KeyguardSettingsPreferenceFragment;->onActivityResult(IILandroid/content/Intent;)V

    const/16 v0, 0x12e

    if-ne p1, v0, :cond_0

    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    invoke-virtual {p0}, Lcom/android/settings/SetUpMiuiSecurityChooseUnlock$SetUpMiuiSecurityChooseUnlockFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {p0}, Lcom/android/settings/SetUpMiuiSecurityChooseUnlock$SetUpMiuiSecurityChooseUnlockFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    instance-of v1, v1, Lcom/android/settings/SetUpMiuiSecurityChooseUnlock$InternalActivity;

    if-eqz v1, :cond_1

    :goto_0
    invoke-virtual {v0, p2, p3}, Landroid/app/Activity;->setResult(ILandroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/android/settings/SetUpMiuiSecurityChooseUnlock$SetUpMiuiSecurityChooseUnlockFragment;->finish()V

    :cond_0
    return-void

    :cond_1
    const/4 p3, 0x0

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 5

    const/4 v4, 0x0

    const/16 v3, -0x2710

    invoke-super {p0, p1}, Lcom/android/settings/KeyguardSettingsPreferenceFragment;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/android/settings/SetUpMiuiSecurityChooseUnlock$SetUpMiuiSecurityChooseUnlockFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    invoke-virtual {p0}, Lcom/android/settings/SetUpMiuiSecurityChooseUnlock$SetUpMiuiSecurityChooseUnlockFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    if-eqz v0, :cond_1

    const-string/jumbo v2, "add_keyguard_password_then_add_fingerprint"

    invoke-virtual {v0, v2, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    invoke-static {v0}, Lcom/android/settings/SetUpMiuiSecurityChooseUnlock;->bEj(Z)Z

    :goto_0
    const-string/jumbo v0, "device_policy"

    invoke-virtual {p0, v0}, Lcom/android/settings/SetUpMiuiSecurityChooseUnlock$SetUpMiuiSecurityChooseUnlockFragment;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/admin/DevicePolicyManager;

    iput-object v0, p0, Lcom/android/settings/SetUpMiuiSecurityChooseUnlock$SetUpMiuiSecurityChooseUnlockFragment;->bMS:Landroid/app/admin/DevicePolicyManager;

    new-instance v0, Lcom/android/settings/cx;

    invoke-virtual {p0}, Lcom/android/settings/SetUpMiuiSecurityChooseUnlock$SetUpMiuiSecurityChooseUnlockFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-direct {v0, v2}, Lcom/android/settings/cx;-><init>(Landroid/app/Activity;)V

    iput-object v0, p0, Lcom/android/settings/SetUpMiuiSecurityChooseUnlock$SetUpMiuiSecurityChooseUnlockFragment;->bMR:Lcom/android/settings/cx;

    iget-object v0, p0, Lcom/android/settings/SetUpMiuiSecurityChooseUnlock$SetUpMiuiSecurityChooseUnlockFragment;->bMR:Lcom/android/settings/cx;

    invoke-virtual {v0}, Lcom/android/settings/cx;->bSR()Lcom/android/internal/widget/LockPatternUtils;

    move-result-object v0

    invoke-virtual {p0}, Lcom/android/settings/SetUpMiuiSecurityChooseUnlock$SetUpMiuiSecurityChooseUnlockFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-static {v1, v0, v2}, Lcom/android/settings/aN;->bwS(Landroid/os/Bundle;Lcom/android/internal/widget/LockPatternUtils;Landroid/app/Activity;)I

    move-result v0

    invoke-static {v0}, Lcom/android/settings/SetUpMiuiSecurityChooseUnlock;->bEo(I)I

    invoke-virtual {p0}, Lcom/android/settings/SetUpMiuiSecurityChooseUnlock$SetUpMiuiSecurityChooseUnlockFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "setup_userid_to_set_password"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    if-eq v0, v3, :cond_0

    invoke-static {v0}, Lcom/android/settings/SetUpMiuiSecurityChooseUnlock;->bEo(I)I

    :cond_0
    const v0, 0x7f1500dc

    invoke-virtual {p0, v0}, Lcom/android/settings/SetUpMiuiSecurityChooseUnlock$SetUpMiuiSecurityChooseUnlockFragment;->addPreferencesFromResource(I)V

    const-string/jumbo v0, "setup_unlock_set_pattern"

    invoke-virtual {p0, v0}, Lcom/android/settings/SetUpMiuiSecurityChooseUnlock$SetUpMiuiSecurityChooseUnlockFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Lcom/android/settings/SetUpRadioButtonPreference;

    invoke-static {v0}, Lcom/android/settings/SetUpMiuiSecurityChooseUnlock;->bEn(Lcom/android/settings/SetUpRadioButtonPreference;)Lcom/android/settings/SetUpRadioButtonPreference;

    const-string/jumbo v0, "setup_unlock_set_pin"

    invoke-virtual {p0, v0}, Lcom/android/settings/SetUpMiuiSecurityChooseUnlock$SetUpMiuiSecurityChooseUnlockFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Lcom/android/settings/SetUpRadioButtonPreference;

    invoke-static {v0}, Lcom/android/settings/SetUpMiuiSecurityChooseUnlock;->bEm(Lcom/android/settings/SetUpRadioButtonPreference;)Lcom/android/settings/SetUpRadioButtonPreference;

    const-string/jumbo v0, "setup_unlock_set_password"

    invoke-virtual {p0, v0}, Lcom/android/settings/SetUpMiuiSecurityChooseUnlock$SetUpMiuiSecurityChooseUnlockFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Lcom/android/settings/SetUpRadioButtonPreference;

    invoke-static {v0}, Lcom/android/settings/SetUpMiuiSecurityChooseUnlock;->bEl(Lcom/android/settings/SetUpRadioButtonPreference;)Lcom/android/settings/SetUpRadioButtonPreference;

    return-void

    :cond_1
    invoke-static {v4}, Lcom/android/settings/SetUpMiuiSecurityChooseUnlock;->bEj(Z)Z

    goto :goto_0
.end method

.method public onDestroy()V
    .locals 3

    iget-object v0, p0, Lcom/android/settings/SetUpMiuiSecurityChooseUnlock$SetUpMiuiSecurityChooseUnlockFragment;->bMX:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/android/settings/SetUpMiuiSecurityChooseUnlock$SetUpMiuiSecurityChooseUnlockFragment;->bWB()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/SetUpMiuiSecurityChooseUnlock$SetUpMiuiSecurityChooseUnlockFragment;->bMX:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Landroid/provider/MiuiSettings$Secure;->putBoolean(Landroid/content/ContentResolver;Ljava/lang/String;Z)V

    :cond_0
    iget-boolean v0, p0, Lcom/android/settings/SetUpMiuiSecurityChooseUnlock$SetUpMiuiSecurityChooseUnlockFragment;->bMT:Z

    if-nez v0, :cond_1

    new-instance v0, Landroid/security/MiuiLockPatternUtils;

    invoke-virtual {p0}, Lcom/android/settings/SetUpMiuiSecurityChooseUnlock$SetUpMiuiSecurityChooseUnlockFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/security/MiuiLockPatternUtils;-><init>(Landroid/content/Context;)V

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/security/MiuiLockPatternUtils;->setKeyguardPasswordQuality(I)V

    :cond_1
    invoke-super {p0}, Lcom/android/settings/KeyguardSettingsPreferenceFragment;->onDestroy()V

    return-void
.end method

.method public onInflateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3

    invoke-virtual {p0}, Lcom/android/settings/SetUpMiuiSecurityChooseUnlock$SetUpMiuiSecurityChooseUnlockFragment;->getActionBar()Lmiui/app/ActionBar;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ActionBar;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Landroid/app/ActionBar;->hide()V

    :cond_0
    const v0, 0x7f0d01c7

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const v0, 0x7f0a00aa

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/settings/SetUpMiuiSecurityChooseUnlock$SetUpMiuiSecurityChooseUnlockFragment;->bMW:Landroid/widget/TextView;

    const v0, 0x7f0a00a5

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/settings/SetUpMiuiSecurityChooseUnlock$SetUpMiuiSecurityChooseUnlockFragment;->bMQ:Landroid/widget/TextView;

    const v0, 0x7f0a03e7

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/settings/SetUpMiuiSecurityChooseUnlock$SetUpMiuiSecurityChooseUnlockFragment;->bMV:Landroid/widget/TextView;

    const v0, 0x7f0a03e4

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/settings/SetUpMiuiSecurityChooseUnlock$SetUpMiuiSecurityChooseUnlockFragment;->bMU:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/android/settings/SetUpMiuiSecurityChooseUnlock$SetUpMiuiSecurityChooseUnlockFragment;->bMW:Landroid/widget/TextView;

    const/4 v2, 0x4

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    if-eqz p2, :cond_1

    invoke-virtual {p2}, Landroid/view/ViewGroup;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    instance-of v2, v0, Landroid/preference/PreferenceFrameLayout$LayoutParams;

    if-eqz v2, :cond_1

    check-cast v0, Landroid/preference/PreferenceFrameLayout$LayoutParams;

    const/4 v2, 0x1

    iput-boolean v2, v0, Landroid/preference/PreferenceFrameLayout$LayoutParams;->removeBorders:Z

    :cond_1
    iget-object v0, p0, Lcom/android/settings/SetUpMiuiSecurityChooseUnlock$SetUpMiuiSecurityChooseUnlockFragment;->bMQ:Landroid/widget/TextView;

    new-instance v2, Lcom/android/settings/hN;

    invoke-direct {v2, p0}, Lcom/android/settings/hN;-><init>(Lcom/android/settings/SetUpMiuiSecurityChooseUnlock$SetUpMiuiSecurityChooseUnlockFragment;)V

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-object v1
.end method

.method public onPause()V
    .locals 0

    invoke-super {p0}, Lcom/android/settings/KeyguardSettingsPreferenceFragment;->onPause()V

    return-void
.end method

.method public onPreferenceTreeClick(Landroid/preference/PreferenceScreen;Landroid/preference/Preference;)Z
    .locals 4

    const v3, 0x7f0802ce

    const/4 v0, 0x1

    invoke-virtual {p2}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "setup_unlock_set_pattern"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-static {}, Lcom/android/settings/SetUpMiuiSecurityChooseUnlock;->bEi()Lcom/android/settings/SetUpRadioButtonPreference;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/android/settings/SetUpRadioButtonPreference;->bTy(Z)V

    invoke-static {}, Lcom/android/settings/SetUpMiuiSecurityChooseUnlock;->bEi()Lcom/android/settings/SetUpRadioButtonPreference;

    move-result-object v2

    invoke-virtual {v2, v3}, Lcom/android/settings/SetUpRadioButtonPreference;->bTz(I)V

    invoke-direct {p0, v1}, Lcom/android/settings/SetUpMiuiSecurityChooseUnlock$SetUpMiuiSecurityChooseUnlockFragment;->bEs(Ljava/lang/String;)V

    :goto_0
    return v0

    :cond_0
    const-string/jumbo v2, "setup_unlock_set_pin"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-static {}, Lcom/android/settings/SetUpMiuiSecurityChooseUnlock;->bEh()Lcom/android/settings/SetUpRadioButtonPreference;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/android/settings/SetUpRadioButtonPreference;->bTy(Z)V

    invoke-static {}, Lcom/android/settings/SetUpMiuiSecurityChooseUnlock;->bEh()Lcom/android/settings/SetUpRadioButtonPreference;

    move-result-object v2

    invoke-virtual {v2, v3}, Lcom/android/settings/SetUpRadioButtonPreference;->bTz(I)V

    invoke-direct {p0, v1}, Lcom/android/settings/SetUpMiuiSecurityChooseUnlock$SetUpMiuiSecurityChooseUnlockFragment;->bEs(Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    const-string/jumbo v2, "setup_unlock_set_password"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-static {}, Lcom/android/settings/SetUpMiuiSecurityChooseUnlock;->bEg()Lcom/android/settings/SetUpRadioButtonPreference;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/android/settings/SetUpRadioButtonPreference;->bTy(Z)V

    invoke-static {}, Lcom/android/settings/SetUpMiuiSecurityChooseUnlock;->bEg()Lcom/android/settings/SetUpRadioButtonPreference;

    move-result-object v2

    invoke-virtual {v2, v3}, Lcom/android/settings/SetUpRadioButtonPreference;->bTz(I)V

    invoke-direct {p0, v1}, Lcom/android/settings/SetUpMiuiSecurityChooseUnlock$SetUpMiuiSecurityChooseUnlockFragment;->bEs(Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onResume()V
    .locals 3

    const/4 v2, 0x0

    invoke-super {p0}, Lcom/android/settings/KeyguardSettingsPreferenceFragment;->onResume()V

    iget-object v0, p0, Lcom/android/settings/SetUpMiuiSecurityChooseUnlock$SetUpMiuiSecurityChooseUnlockFragment;->bMR:Lcom/android/settings/cx;

    invoke-virtual {v0}, Lcom/android/settings/cx;->bSR()Lcom/android/internal/widget/LockPatternUtils;

    move-result-object v0

    invoke-static {v0, v2}, Lcom/android/settings/bn;->bFG(Lcom/android/internal/widget/LockPatternUtils;I)I

    move-result v0

    iget-object v1, p0, Lcom/android/settings/SetUpMiuiSecurityChooseUnlock$SetUpMiuiSecurityChooseUnlockFragment;->bMS:Landroid/app/admin/DevicePolicyManager;

    invoke-static {v0, v1, p0}, Lcom/android/settings/SetUpMiuiSecurityChooseUnlock;->bEp(ILandroid/app/admin/DevicePolicyManager;Lcom/android/settings/KeyguardSettingsPreferenceFragment;)I

    invoke-virtual {p0}, Lcom/android/settings/SetUpMiuiSecurityChooseUnlock$SetUpMiuiSecurityChooseUnlockFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "setup_hide_disabled_prefs"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    invoke-static {}, Lcom/android/settings/SetUpMiuiSecurityChooseUnlock;->-get0()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/SetUpMiuiSecurityChooseUnlock$SetUpMiuiSecurityChooseUnlockFragment;->bMV:Landroid/widget/TextView;

    const v1, 0x7f120b57

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    iget-object v0, p0, Lcom/android/settings/SetUpMiuiSecurityChooseUnlock$SetUpMiuiSecurityChooseUnlockFragment;->bMU:Landroid/widget/TextView;

    const v1, 0x7f121043

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    :cond_0
    invoke-direct {p0}, Lcom/android/settings/SetUpMiuiSecurityChooseUnlock$SetUpMiuiSecurityChooseUnlockFragment;->bEr()V

    return-void
.end method
