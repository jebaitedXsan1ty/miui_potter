.class final Lcom/android/settings/ho;
.super Landroid/os/Handler;
.source "MiuiLocaleSettings.java"


# instance fields
.field final synthetic cmU:Lcom/android/settings/MiuiLocaleSettings;


# direct methods
.method constructor <init>(Lcom/android/settings/MiuiLocaleSettings;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/ho;->cmU:Lcom/android/settings/MiuiLocaleSettings;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/android/settings/ho;->cmU:Lcom/android/settings/MiuiLocaleSettings;

    invoke-virtual {v0}, Lcom/android/settings/MiuiLocaleSettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    if-nez v0, :cond_0

    const-string/jumbo v0, "MiuiLocaleSettings"

    const-string/jumbo v1, "[mHandler.handleMessage]getActivity is null"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :cond_0
    iget v1, p1, Landroid/os/Message;->what:I

    packed-switch v1, :pswitch_data_0

    :cond_1
    :goto_0
    return-void

    :pswitch_0
    iget-object v1, p0, Lcom/android/settings/ho;->cmU:Lcom/android/settings/MiuiLocaleSettings;

    invoke-static {v1}, Lcom/android/settings/MiuiLocaleSettings;->bBf(Lcom/android/settings/MiuiLocaleSettings;)Landroid/app/ProgressDialog;

    move-result-object v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/android/settings/ho;->cmU:Lcom/android/settings/MiuiLocaleSettings;

    invoke-static {v1}, Lcom/android/settings/MiuiLocaleSettings;->bBf(Lcom/android/settings/MiuiLocaleSettings;)Landroid/app/ProgressDialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/ProgressDialog;->cancel()V

    iget-object v1, p0, Lcom/android/settings/ho;->cmU:Lcom/android/settings/MiuiLocaleSettings;

    invoke-static {v1, v2}, Lcom/android/settings/MiuiLocaleSettings;->bBh(Lcom/android/settings/MiuiLocaleSettings;Landroid/app/ProgressDialog;)Landroid/app/ProgressDialog;

    :cond_2
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method
