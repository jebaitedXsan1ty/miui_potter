.class public Lcom/android/settings/applications/AppStateNotificationBridge;
.super Lcom/android/settings/applications/AppStateBaseBridge;
.source "AppStateNotificationBridge.java"


# static fields
.field public static final yQ:Lcom/android/settingslib/b/i;


# instance fields
.field private final mContext:Landroid/content/Context;

.field private final yR:Lcom/android/settings/notification/NotificationBackend;

.field private final yS:Landroid/content/pm/PackageManager;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/android/settings/applications/AppStateNotificationBridge$1;

    invoke-direct {v0}, Lcom/android/settings/applications/AppStateNotificationBridge$1;-><init>()V

    sput-object v0, Lcom/android/settings/applications/AppStateNotificationBridge;->yQ:Lcom/android/settingslib/b/i;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/android/settingslib/b/a;Lcom/android/settings/applications/AppStateBaseBridge$Callback;Lcom/android/settings/notification/NotificationBackend;)V
    .locals 1

    invoke-direct {p0, p2, p3}, Lcom/android/settings/applications/AppStateBaseBridge;-><init>(Lcom/android/settingslib/b/a;Lcom/android/settings/applications/AppStateBaseBridge$Callback;)V

    iput-object p1, p0, Lcom/android/settings/applications/AppStateNotificationBridge;->mContext:Landroid/content/Context;

    iget-object v0, p0, Lcom/android/settings/applications/AppStateNotificationBridge;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/applications/AppStateNotificationBridge;->yS:Landroid/content/pm/PackageManager;

    iput-object p4, p0, Lcom/android/settings/applications/AppStateNotificationBridge;->yR:Lcom/android/settings/notification/NotificationBackend;

    return-void
.end method


# virtual methods
.method protected iO()V
    .locals 8

    iget-object v0, p0, Lcom/android/settings/applications/AppStateNotificationBridge;->Ap:Lcom/android/settingslib/b/b;

    invoke-virtual {v0}, Lcom/android/settingslib/b/b;->cfs()Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settingslib/b/h;

    iget-object v4, p0, Lcom/android/settings/applications/AppStateNotificationBridge;->yR:Lcom/android/settings/notification/NotificationBackend;

    iget-object v5, p0, Lcom/android/settings/applications/AppStateNotificationBridge;->mContext:Landroid/content/Context;

    iget-object v6, p0, Lcom/android/settings/applications/AppStateNotificationBridge;->yS:Landroid/content/pm/PackageManager;

    iget-object v7, v0, Lcom/android/settingslib/b/h;->info:Landroid/content/pm/ApplicationInfo;

    invoke-virtual {v4, v5, v6, v7}, Lcom/android/settings/notification/NotificationBackend;->bz(Landroid/content/Context;Landroid/content/pm/PackageManager;Landroid/content/pm/ApplicationInfo;)Lcom/android/settings/notification/NotificationBackend$AppRow;

    move-result-object v4

    iput-object v4, v0, Lcom/android/settingslib/b/h;->cBd:Ljava/lang/Object;

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    return-void
.end method

.method protected iP(Lcom/android/settingslib/b/h;Ljava/lang/String;I)V
    .locals 4

    iget-object v0, p0, Lcom/android/settings/applications/AppStateNotificationBridge;->yR:Lcom/android/settings/notification/NotificationBackend;

    iget-object v1, p0, Lcom/android/settings/applications/AppStateNotificationBridge;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/android/settings/applications/AppStateNotificationBridge;->yS:Landroid/content/pm/PackageManager;

    iget-object v3, p1, Lcom/android/settingslib/b/h;->info:Landroid/content/pm/ApplicationInfo;

    invoke-virtual {v0, v1, v2, v3}, Lcom/android/settings/notification/NotificationBackend;->bz(Landroid/content/Context;Landroid/content/pm/PackageManager;Landroid/content/pm/ApplicationInfo;)Lcom/android/settings/notification/NotificationBackend$AppRow;

    move-result-object v0

    iput-object v0, p1, Lcom/android/settingslib/b/h;->cBd:Ljava/lang/Object;

    return-void
.end method
