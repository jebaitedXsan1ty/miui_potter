.class public Lcom/android/settings/applications/AppOpsCategory$AppListLoader;
.super Landroid/content/AsyncTaskLoader;
.source "AppOpsCategory.java"


# instance fields
.field sa:Ljava/util/List;

.field final sb:Lcom/android/settings/applications/AppOpsCategory$InterestingConfigChanges;

.field sc:Lcom/android/settings/applications/AppOpsCategory$PackageIntentReceiver;

.field final sd:Lcom/android/settings/applications/AppOpsState;

.field final se:Lcom/android/settings/applications/AppOpsState$OpsTemplate;

.field final sf:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/android/settings/applications/AppOpsState;Lcom/android/settings/applications/AppOpsState$OpsTemplate;Z)V
    .locals 1

    invoke-direct {p0, p1}, Landroid/content/AsyncTaskLoader;-><init>(Landroid/content/Context;)V

    new-instance v0, Lcom/android/settings/applications/AppOpsCategory$InterestingConfigChanges;

    invoke-direct {v0}, Lcom/android/settings/applications/AppOpsCategory$InterestingConfigChanges;-><init>()V

    iput-object v0, p0, Lcom/android/settings/applications/AppOpsCategory$AppListLoader;->sb:Lcom/android/settings/applications/AppOpsCategory$InterestingConfigChanges;

    iput-object p2, p0, Lcom/android/settings/applications/AppOpsCategory$AppListLoader;->sd:Lcom/android/settings/applications/AppOpsState;

    iput-object p3, p0, Lcom/android/settings/applications/AppOpsCategory$AppListLoader;->se:Lcom/android/settings/applications/AppOpsState$OpsTemplate;

    iput-boolean p4, p0, Lcom/android/settings/applications/AppOpsCategory$AppListLoader;->sf:Z

    return-void
.end method


# virtual methods
.method public bridge synthetic deliverResult(Ljava/lang/Object;)V
    .locals 0

    check-cast p1, Ljava/util/List;

    invoke-virtual {p0, p1}, Lcom/android/settings/applications/AppOpsCategory$AppListLoader;->qU(Ljava/util/List;)V

    return-void
.end method

.method public bridge synthetic loadInBackground()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/android/settings/applications/AppOpsCategory$AppListLoader;->loadInBackground()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public loadInBackground()Ljava/util/List;
    .locals 5

    iget-object v1, p0, Lcom/android/settings/applications/AppOpsCategory$AppListLoader;->sd:Lcom/android/settings/applications/AppOpsState;

    iget-object v2, p0, Lcom/android/settings/applications/AppOpsCategory$AppListLoader;->se:Lcom/android/settings/applications/AppOpsState$OpsTemplate;

    iget-boolean v0, p0, Lcom/android/settings/applications/AppOpsCategory$AppListLoader;->sf:Z

    if-eqz v0, :cond_0

    sget-object v0, Lcom/android/settings/applications/AppOpsState;->ED:Ljava/util/Comparator;

    :goto_0
    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v3, v4, v0}, Lcom/android/settings/applications/AppOpsState;->yT(Lcom/android/settings/applications/AppOpsState$OpsTemplate;ILjava/lang/String;Ljava/util/Comparator;)Ljava/util/List;

    move-result-object v0

    return-object v0

    :cond_0
    sget-object v0, Lcom/android/settings/applications/AppOpsState;->EE:Ljava/util/Comparator;

    goto :goto_0
.end method

.method public bridge synthetic onCanceled(Ljava/lang/Object;)V
    .locals 0

    check-cast p1, Ljava/util/List;

    invoke-virtual {p0, p1}, Lcom/android/settings/applications/AppOpsCategory$AppListLoader;->qV(Ljava/util/List;)V

    return-void
.end method

.method protected onReset()V
    .locals 3

    const/4 v2, 0x0

    invoke-super {p0}, Landroid/content/AsyncTaskLoader;->onReset()V

    invoke-virtual {p0}, Lcom/android/settings/applications/AppOpsCategory$AppListLoader;->onStopLoading()V

    iget-object v0, p0, Lcom/android/settings/applications/AppOpsCategory$AppListLoader;->sa:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/applications/AppOpsCategory$AppListLoader;->sa:Ljava/util/List;

    invoke-virtual {p0, v0}, Lcom/android/settings/applications/AppOpsCategory$AppListLoader;->qW(Ljava/util/List;)V

    iput-object v2, p0, Lcom/android/settings/applications/AppOpsCategory$AppListLoader;->sa:Ljava/util/List;

    :cond_0
    iget-object v0, p0, Lcom/android/settings/applications/AppOpsCategory$AppListLoader;->sc:Lcom/android/settings/applications/AppOpsCategory$PackageIntentReceiver;

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/android/settings/applications/AppOpsCategory$AppListLoader;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/applications/AppOpsCategory$AppListLoader;->sc:Lcom/android/settings/applications/AppOpsCategory$PackageIntentReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    iput-object v2, p0, Lcom/android/settings/applications/AppOpsCategory$AppListLoader;->sc:Lcom/android/settings/applications/AppOpsCategory$PackageIntentReceiver;

    :cond_1
    return-void
.end method

.method protected onStartLoading()V
    .locals 2

    invoke-virtual {p0}, Lcom/android/settings/applications/AppOpsCategory$AppListLoader;->onContentChanged()V

    iget-object v0, p0, Lcom/android/settings/applications/AppOpsCategory$AppListLoader;->sa:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/applications/AppOpsCategory$AppListLoader;->sa:Ljava/util/List;

    invoke-virtual {p0, v0}, Lcom/android/settings/applications/AppOpsCategory$AppListLoader;->qU(Ljava/util/List;)V

    :cond_0
    iget-object v0, p0, Lcom/android/settings/applications/AppOpsCategory$AppListLoader;->sc:Lcom/android/settings/applications/AppOpsCategory$PackageIntentReceiver;

    if-nez v0, :cond_1

    new-instance v0, Lcom/android/settings/applications/AppOpsCategory$PackageIntentReceiver;

    invoke-direct {v0, p0}, Lcom/android/settings/applications/AppOpsCategory$PackageIntentReceiver;-><init>(Lcom/android/settings/applications/AppOpsCategory$AppListLoader;)V

    iput-object v0, p0, Lcom/android/settings/applications/AppOpsCategory$AppListLoader;->sc:Lcom/android/settings/applications/AppOpsCategory$PackageIntentReceiver;

    :cond_1
    iget-object v0, p0, Lcom/android/settings/applications/AppOpsCategory$AppListLoader;->sb:Lcom/android/settings/applications/AppOpsCategory$InterestingConfigChanges;

    invoke-virtual {p0}, Lcom/android/settings/applications/AppOpsCategory$AppListLoader;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/settings/applications/AppOpsCategory$InterestingConfigChanges;->qT(Landroid/content/res/Resources;)Z

    move-result v0

    invoke-virtual {p0}, Lcom/android/settings/applications/AppOpsCategory$AppListLoader;->takeContentChanged()Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/android/settings/applications/AppOpsCategory$AppListLoader;->sa:Ljava/util/List;

    if-nez v1, :cond_4

    :cond_2
    :goto_0
    invoke-virtual {p0}, Lcom/android/settings/applications/AppOpsCategory$AppListLoader;->forceLoad()V

    :cond_3
    return-void

    :cond_4
    if-eqz v0, :cond_3

    goto :goto_0
.end method

.method protected onStopLoading()V
    .locals 0

    invoke-virtual {p0}, Lcom/android/settings/applications/AppOpsCategory$AppListLoader;->cancelLoad()Z

    return-void
.end method

.method public qU(Ljava/util/List;)V
    .locals 1

    invoke-virtual {p0}, Lcom/android/settings/applications/AppOpsCategory$AppListLoader;->isReset()Z

    move-result v0

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    invoke-virtual {p0, p1}, Lcom/android/settings/applications/AppOpsCategory$AppListLoader;->qW(Ljava/util/List;)V

    :cond_0
    iput-object p1, p0, Lcom/android/settings/applications/AppOpsCategory$AppListLoader;->sa:Ljava/util/List;

    invoke-virtual {p0}, Lcom/android/settings/applications/AppOpsCategory$AppListLoader;->isStarted()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-super {p0, p1}, Landroid/content/AsyncTaskLoader;->deliverResult(Ljava/lang/Object;)V

    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0, p1}, Lcom/android/settings/applications/AppOpsCategory$AppListLoader;->qW(Ljava/util/List;)V

    :cond_2
    return-void
.end method

.method public qV(Ljava/util/List;)V
    .locals 0

    invoke-super {p0, p1}, Landroid/content/AsyncTaskLoader;->onCanceled(Ljava/lang/Object;)V

    invoke-virtual {p0, p1}, Lcom/android/settings/applications/AppOpsCategory$AppListLoader;->qW(Ljava/util/List;)V

    return-void
.end method

.method protected qW(Ljava/util/List;)V
    .locals 0

    return-void
.end method
