.class Lcom/android/settings/applications/RunningApplicationsFragment$ServiceListAdapter;
.super Landroid/widget/BaseAdapter;
.source "RunningApplicationsFragment.java"


# instance fields
.field final EA:Lcom/android/settings/applications/RunningState;

.field final synthetic EB:Lcom/android/settings/applications/RunningApplicationsFragment;

.field final Ey:Landroid/view/LayoutInflater;

.field Ez:Ljava/util/ArrayList;


# direct methods
.method constructor <init>(Lcom/android/settings/applications/RunningApplicationsFragment;Lcom/android/settings/applications/RunningState;)V
    .locals 2

    iput-object p1, p0, Lcom/android/settings/applications/RunningApplicationsFragment$ServiceListAdapter;->EB:Lcom/android/settings/applications/RunningApplicationsFragment;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    iput-object p2, p0, Lcom/android/settings/applications/RunningApplicationsFragment$ServiceListAdapter;->EA:Lcom/android/settings/applications/RunningState;

    invoke-static {p1}, Lcom/android/settings/applications/RunningApplicationsFragment;->yO(Lcom/android/settings/applications/RunningApplicationsFragment;)Landroid/content/Context;

    move-result-object v0

    const-string/jumbo v1, "layout_inflater"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Lcom/android/settings/applications/RunningApplicationsFragment$ServiceListAdapter;->Ey:Landroid/view/LayoutInflater;

    iget-object v0, p0, Lcom/android/settings/applications/RunningApplicationsFragment$ServiceListAdapter;->EA:Lcom/android/settings/applications/RunningState;

    invoke-static {p1}, Lcom/android/settings/applications/RunningApplicationsFragment;->yP(Lcom/android/settings/applications/RunningApplicationsFragment;)Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/android/settings/applications/RunningState;->sG(Z)V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/settings/applications/RunningApplicationsFragment$ServiceListAdapter;->Ez:Ljava/util/ArrayList;

    return-void
.end method


# virtual methods
.method public areAllItemsEnabled()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public getCount()I
    .locals 1

    iget-object v0, p0, Lcom/android/settings/applications/RunningApplicationsFragment$ServiceListAdapter;->Ez:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/applications/RunningApplicationsFragment$ServiceListAdapter;->Ez:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2

    iget-object v0, p0, Lcom/android/settings/applications/RunningApplicationsFragment$ServiceListAdapter;->Ez:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settings/applications/RunningState$MergedItem;

    invoke-virtual {v0}, Lcom/android/settings/applications/RunningState$MergedItem;->hashCode()I

    move-result v0

    int-to-long v0, v0

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 0

    if-nez p2, :cond_0

    invoke-virtual {p0, p3}, Lcom/android/settings/applications/RunningApplicationsFragment$ServiceListAdapter;->yR(Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    :cond_0
    invoke-virtual {p0, p2, p1}, Lcom/android/settings/applications/RunningApplicationsFragment$ServiceListAdapter;->yQ(Landroid/view/View;I)V

    return-object p2
.end method

.method public hasStableIds()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public isEmpty()Z
    .locals 2

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/android/settings/applications/RunningApplicationsFragment$ServiceListAdapter;->EA:Lcom/android/settings/applications/RunningState;

    invoke-virtual {v1}, Lcom/android/settings/applications/RunningState;->sH()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/settings/applications/RunningApplicationsFragment$ServiceListAdapter;->Ez:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method public isEnabled(I)Z
    .locals 1

    iget-object v0, p0, Lcom/android/settings/applications/RunningApplicationsFragment$ServiceListAdapter;->Ez:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settings/applications/RunningState$MergedItem;

    iget-boolean v0, v0, Lcom/android/settings/applications/RunningState$MergedItem;->wh:Z

    xor-int/lit8 v0, v0, 0x1

    return v0
.end method

.method public yQ(Landroid/view/View;I)V
    .locals 5

    iget-object v0, p0, Lcom/android/settings/applications/RunningApplicationsFragment$ServiceListAdapter;->EA:Lcom/android/settings/applications/RunningState;

    iget-object v2, v0, Lcom/android/settings/applications/RunningState;->mLock:Ljava/lang/Object;

    monitor-enter v2

    :try_start_0
    iget-object v0, p0, Lcom/android/settings/applications/RunningApplicationsFragment$ServiceListAdapter;->Ez:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-lt p2, v0, :cond_0

    monitor-exit v2

    return-void

    :cond_0
    :try_start_1
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settings/applications/RunningProcessesView$ViewHolder;

    iget-object v1, p0, Lcom/android/settings/applications/RunningApplicationsFragment$ServiceListAdapter;->Ez:Ljava/util/ArrayList;

    invoke-virtual {v1, p2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/settings/applications/RunningState$MergedItem;

    iget-object v3, p0, Lcom/android/settings/applications/RunningApplicationsFragment$ServiceListAdapter;->EA:Lcom/android/settings/applications/RunningState;

    iget-object v4, p0, Lcom/android/settings/applications/RunningApplicationsFragment$ServiceListAdapter;->EB:Lcom/android/settings/applications/RunningApplicationsFragment;

    invoke-static {v4}, Lcom/android/settings/applications/RunningApplicationsFragment;->yN(Lcom/android/settings/applications/RunningApplicationsFragment;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0, v3, v1, v4}, Lcom/android/settings/applications/RunningProcessesView$ViewHolder;->su(Lcom/android/settings/applications/RunningState;Lcom/android/settings/applications/RunningState$BaseItem;Ljava/lang/StringBuilder;)Lcom/android/settings/applications/RunningProcessesView$ActiveItem;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/applications/RunningApplicationsFragment$ServiceListAdapter;->EB:Lcom/android/settings/applications/RunningApplicationsFragment;

    iget-object v1, v1, Lcom/android/settings/applications/RunningApplicationsFragment;->Eg:Ljava/util/HashMap;

    invoke-virtual {v1, p1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit v2

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0
.end method

.method public yR(Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lcom/android/settings/applications/RunningApplicationsFragment$ServiceListAdapter;->Ey:Landroid/view/LayoutInflater;

    const v1, 0x7f0d0199

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/android/settings/applications/RunningProcessesView$ViewHolder;

    invoke-direct {v1, v0}, Lcom/android/settings/applications/RunningProcessesView$ViewHolder;-><init>(Landroid/view/View;)V

    return-object v0
.end method

.method yS()V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/applications/RunningApplicationsFragment$ServiceListAdapter;->EB:Lcom/android/settings/applications/RunningApplicationsFragment;

    invoke-static {v0}, Lcom/android/settings/applications/RunningApplicationsFragment;->yP(Lcom/android/settings/applications/RunningApplicationsFragment;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/settings/applications/RunningApplicationsFragment$ServiceListAdapter;->EA:Lcom/android/settings/applications/RunningState;

    invoke-virtual {v0}, Lcom/android/settings/applications/RunningState;->sB()Ljava/util/ArrayList;

    move-result-object v0

    :goto_0
    iget-object v1, p0, Lcom/android/settings/applications/RunningApplicationsFragment$ServiceListAdapter;->Ez:Ljava/util/ArrayList;

    if-eq v1, v0, :cond_0

    iput-object v0, p0, Lcom/android/settings/applications/RunningApplicationsFragment$ServiceListAdapter;->Ez:Ljava/util/ArrayList;

    :cond_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/settings/applications/RunningApplicationsFragment$ServiceListAdapter;->EA:Lcom/android/settings/applications/RunningState;

    invoke-virtual {v0}, Lcom/android/settings/applications/RunningState;->sC()Ljava/util/ArrayList;

    move-result-object v0

    goto :goto_0
.end method
