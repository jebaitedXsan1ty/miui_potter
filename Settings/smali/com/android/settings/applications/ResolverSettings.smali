.class public Lcom/android/settings/applications/ResolverSettings;
.super Lcom/android/settings/BaseFragment;
.source "ResolverSettings.java"


# instance fields
.field private zN:Lcom/android/settings/applications/ResolverSettings$ResolverListAdapter;

.field private zO:Ljava/util/List;

.field private zP:I

.field private zQ:Lmiui/widget/DynamicListView;

.field protected zR:Z

.field private zS:Ljava/util/Map;

.field private zT:Ljava/util/List;

.field private zU:Lmiui/widget/DynamicListView$RearrangeListener;

.field private zV:Landroid/util/SparseArray;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/android/settings/BaseFragment;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/settings/applications/ResolverSettings;->zR:Z

    new-instance v0, Lcom/android/settings/applications/ResolverSettings$1;

    invoke-direct {v0, p0}, Lcom/android/settings/applications/ResolverSettings$1;-><init>(Lcom/android/settings/applications/ResolverSettings;)V

    iput-object v0, p0, Lcom/android/settings/applications/ResolverSettings;->zU:Lmiui/widget/DynamicListView$RearrangeListener;

    return-void
.end method

.method private vA(Landroid/content/pm/ResolveInfo;)Landroid/content/ComponentName;
    .locals 3

    new-instance v0, Landroid/content/ComponentName;

    iget-object v1, p1, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v1, v1, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    iget-object v2, p1, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v2, v2, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method private vC()V
    .locals 5

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/android/settings/applications/ResolverSettings;->zS:Ljava/util/Map;

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/android/settings/applications/ResolverSettings;->zO:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    iget-object v2, p0, Lcom/android/settings/applications/ResolverSettings;->zS:Ljava/util/Map;

    iget-object v0, p0, Lcom/android/settings/applications/ResolverSettings;->zO:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/ComponentName;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v2, v0, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/android/settings/applications/ResolverSettings;->zV:Landroid/util/SparseArray;

    iget-object v0, p0, Lcom/android/settings/applications/ResolverSettings;->zT:Ljava/util/List;

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/ResolveInfo;

    invoke-direct {p0, v0}, Lcom/android/settings/applications/ResolverSettings;->vA(Landroid/content/pm/ResolveInfo;)Landroid/content/ComponentName;

    move-result-object v1

    iget-object v3, p0, Lcom/android/settings/applications/ResolverSettings;->zS:Ljava/util/Map;

    invoke-interface {v3, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/android/settings/applications/ResolverSettings;->zV:Landroid/util/SparseArray;

    iget-object v4, p0, Lcom/android/settings/applications/ResolverSettings;->zS:Ljava/util/Map;

    invoke-interface {v4, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v3, v1, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    goto :goto_1

    :cond_1
    iget-object v3, p0, Lcom/android/settings/applications/ResolverSettings;->zO:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    iget-object v4, p0, Lcom/android/settings/applications/ResolverSettings;->zV:Landroid/util/SparseArray;

    invoke-virtual {v4, v3, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    iget-object v0, p0, Lcom/android/settings/applications/ResolverSettings;->zS:Ljava/util/Map;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v0, v1, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/android/settings/applications/ResolverSettings;->zO:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/settings/applications/ResolverSettings;->zR:Z

    goto :goto_1

    :cond_2
    return-void
.end method

.method static synthetic vD(Lcom/android/settings/applications/ResolverSettings;)Lcom/android/settings/applications/ResolverSettings$ResolverListAdapter;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/applications/ResolverSettings;->zN:Lcom/android/settings/applications/ResolverSettings$ResolverListAdapter;

    return-object v0
.end method

.method static synthetic vE(Lcom/android/settings/applications/ResolverSettings;)Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/applications/ResolverSettings;->zO:Ljava/util/List;

    return-object v0
.end method

.method static synthetic vF(Lcom/android/settings/applications/ResolverSettings;)I
    .locals 1

    iget v0, p0, Lcom/android/settings/applications/ResolverSettings;->zP:I

    return v0
.end method

.method static synthetic vG(Lcom/android/settings/applications/ResolverSettings;)Lmiui/widget/DynamicListView;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/applications/ResolverSettings;->zQ:Lmiui/widget/DynamicListView;

    return-object v0
.end method

.method static synthetic vH(Lcom/android/settings/applications/ResolverSettings;)Ljava/util/Map;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/applications/ResolverSettings;->zS:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic vI(Lcom/android/settings/applications/ResolverSettings;)Landroid/util/SparseArray;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/applications/ResolverSettings;->zV:Landroid/util/SparseArray;

    return-object v0
.end method

.method static synthetic vJ(Lcom/android/settings/applications/ResolverSettings;Landroid/content/pm/ResolveInfo;)Landroid/content/ComponentName;
    .locals 1

    invoke-direct {p0, p1}, Lcom/android/settings/applications/ResolverSettings;->vA(Landroid/content/pm/ResolveInfo;)Landroid/content/ComponentName;

    move-result-object v0

    return-object v0
.end method

.method static synthetic vK(Lcom/android/settings/applications/ResolverSettings;)Landroid/content/pm/PackageManager;
    .locals 1

    invoke-virtual {p0}, Lcom/android/settings/applications/ResolverSettings;->bLS()Landroid/content/pm/PackageManager;

    move-result-object v0

    return-object v0
.end method

.method static synthetic vL(Lcom/android/settings/applications/ResolverSettings;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/applications/ResolverSettings;->vC()V

    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 3

    const/4 v2, 0x0

    invoke-super {p0, p1}, Lcom/android/settings/BaseFragment;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/android/settings/applications/ResolverSettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "rlist"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/applications/ResolverSettings;->zT:Ljava/util/List;

    iget-object v0, p0, Lcom/android/settings/applications/ResolverSettings;->zT:Ljava/util/List;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/settings/applications/ResolverSettings;->zT:Ljava/util/List;

    :cond_0
    invoke-virtual {p0}, Lcom/android/settings/applications/ResolverSettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-static {v0}, Landroid/provider/MiuiSettings$System;->getActivityResolveOrder(Landroid/content/ContentResolver;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/applications/ResolverSettings;->zO:Ljava/util/List;

    invoke-direct {p0}, Lcom/android/settings/applications/ResolverSettings;->vC()V

    new-instance v0, Lcom/android/settings/applications/ResolverSettings$ResolverListAdapter;

    invoke-direct {v0, p0, v2}, Lcom/android/settings/applications/ResolverSettings$ResolverListAdapter;-><init>(Lcom/android/settings/applications/ResolverSettings;Lcom/android/settings/applications/ResolverSettings$ResolverListAdapter;)V

    iput-object v0, p0, Lcom/android/settings/applications/ResolverSettings;->zN:Lcom/android/settings/applications/ResolverSettings$ResolverListAdapter;

    iget-object v0, p0, Lcom/android/settings/applications/ResolverSettings;->zN:Lcom/android/settings/applications/ResolverSettings$ResolverListAdapter;

    iget-object v1, p0, Lcom/android/settings/applications/ResolverSettings;->zV:Landroid/util/SparseArray;

    invoke-virtual {v0, v1}, Lcom/android/settings/applications/ResolverSettings$ResolverListAdapter;->vM(Landroid/util/SparseArray;)V

    invoke-virtual {p0}, Lcom/android/settings/applications/ResolverSettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const-string/jumbo v1, "activity"

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    invoke-virtual {v0}, Landroid/app/ActivityManager;->getLauncherLargeIconSize()I

    move-result v0

    iput v0, p0, Lcom/android/settings/applications/ResolverSettings;->zP:I

    return-void
.end method

.method public onDestroy()V
    .locals 2

    invoke-super {p0}, Lcom/android/settings/BaseFragment;->onDestroy()V

    iget-boolean v0, p0, Lcom/android/settings/applications/ResolverSettings;->zR:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/android/settings/applications/ResolverSettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/applications/ResolverSettings;->zO:Ljava/util/List;

    invoke-static {v0, v1}, Landroid/provider/MiuiSettings$System;->putActivityResolveOrder(Landroid/content/ContentResolver;Ljava/util/List;)V

    :cond_0
    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1, p2}, Lcom/android/settings/BaseFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    iget-object v0, p0, Lcom/android/settings/applications/ResolverSettings;->zQ:Lmiui/widget/DynamicListView;

    iget-object v1, p0, Lcom/android/settings/applications/ResolverSettings;->zN:Lcom/android/settings/applications/ResolverSettings$ResolverListAdapter;

    invoke-virtual {v0, v1}, Lmiui/widget/DynamicListView;->setAdapter(Landroid/widget/ListAdapter;)V

    iget-object v0, p0, Lcom/android/settings/applications/ResolverSettings;->zQ:Lmiui/widget/DynamicListView;

    iget-object v1, p0, Lcom/android/settings/applications/ResolverSettings;->zU:Lmiui/widget/DynamicListView$RearrangeListener;

    invoke-virtual {v0, v1}, Lmiui/widget/DynamicListView;->setRearrangeListener(Lmiui/widget/DynamicListView$RearrangeListener;)V

    return-void
.end method

.method public vB(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2

    const v0, 0x7f0d018d

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const v0, 0x102000a

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lmiui/widget/DynamicListView;

    iput-object v0, p0, Lcom/android/settings/applications/ResolverSettings;->zQ:Lmiui/widget/DynamicListView;

    return-object v1
.end method
