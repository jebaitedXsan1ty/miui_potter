.class Lcom/android/settings/applications/IconLoader$IconItem;
.super Ljava/lang/Object;
.source "IconLoader.java"


# instance fields
.field AQ:Lcom/android/settingslib/b/h;

.field AR:Landroid/widget/ImageView;


# direct methods
.method public constructor <init>(Landroid/widget/ImageView;Lcom/android/settingslib/b/h;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/android/settings/applications/IconLoader$IconItem;->AR:Landroid/widget/ImageView;

    iput-object p2, p0, Lcom/android/settings/applications/IconLoader$IconItem;->AQ:Lcom/android/settingslib/b/h;

    return-void
.end method


# virtual methods
.method public wi()V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/applications/IconLoader$IconItem;->AR:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iget-object v1, p0, Lcom/android/settings/applications/IconLoader$IconItem;->AQ:Lcom/android/settingslib/b/h;

    iget-object v1, v1, Lcom/android/settingslib/b/h;->info:Landroid/content/pm/ApplicationInfo;

    iget-object v1, v1, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/applications/IconLoader$IconItem;->AR:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/android/settings/applications/IconLoader$IconItem;->AQ:Lcom/android/settingslib/b/h;

    iget-object v1, v1, Lcom/android/settingslib/b/h;->icon:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    :cond_0
    return-void
.end method
