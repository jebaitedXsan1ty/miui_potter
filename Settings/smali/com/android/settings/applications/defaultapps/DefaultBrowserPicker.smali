.class public Lcom/android/settings/applications/defaultapps/DefaultBrowserPicker;
.super Lcom/android/settings/applications/defaultapps/DefaultAppPickerFragment;
.source "DefaultBrowserPicker.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/applications/defaultapps/DefaultAppPickerFragment;-><init>()V

    return-void
.end method


# virtual methods
.method public getMetricsCategory()I
    .locals 1

    const/16 v0, 0x311

    return v0
.end method

.method protected mB()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/android/settings/applications/defaultapps/DefaultBrowserPicker;->oq:Lcom/android/settings/applications/PackageManagerWrapper;

    iget v1, p0, Lcom/android/settings/applications/defaultapps/DefaultBrowserPicker;->mUserId:I

    invoke-interface {v0, v1}, Lcom/android/settings/applications/PackageManagerWrapper;->uS(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected mG(Ljava/lang/String;)Z
    .locals 2

    iget-object v0, p0, Lcom/android/settings/applications/defaultapps/DefaultBrowserPicker;->oq:Lcom/android/settings/applications/PackageManagerWrapper;

    iget v1, p0, Lcom/android/settings/applications/defaultapps/DefaultBrowserPicker;->mUserId:I

    invoke-interface {v0, p1, v1}, Lcom/android/settings/applications/PackageManagerWrapper;->vc(Ljava/lang/String;I)Z

    move-result v0

    return v0
.end method

.method protected my()Ljava/util/List;
    .locals 10

    const/4 v0, 0x0

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iget-object v1, p0, Lcom/android/settings/applications/defaultapps/DefaultBrowserPicker;->oq:Lcom/android/settings/applications/PackageManagerWrapper;

    sget-object v3, Lcom/android/settings/applications/defaultapps/l;->oC:Landroid/content/Intent;

    iget v4, p0, Lcom/android/settings/applications/defaultapps/DefaultBrowserPicker;->mUserId:I

    const/high16 v5, 0x20000

    invoke-interface {v1, v3, v5, v4}, Lcom/android/settings/applications/PackageManagerWrapper;->uZ(Landroid/content/Intent;II)Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v4

    move v1, v0

    :goto_0
    if-ge v1, v4, :cond_2

    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/ResolveInfo;

    iget-object v5, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    if-eqz v5, :cond_0

    iget-boolean v5, v0, Landroid/content/pm/ResolveInfo;->handleAllWebDataURI:Z

    xor-int/lit8 v5, v5, 0x1

    if-eqz v5, :cond_1

    :cond_0
    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    :try_start_0
    new-instance v5, Lcom/android/settings/applications/defaultapps/a;

    iget-object v6, p0, Lcom/android/settings/applications/defaultapps/DefaultBrowserPicker;->oq:Lcom/android/settings/applications/PackageManagerWrapper;

    iget-object v7, p0, Lcom/android/settings/applications/defaultapps/DefaultBrowserPicker;->oq:Lcom/android/settings/applications/PackageManagerWrapper;

    iget-object v0, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v0, v0, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    iget v8, p0, Lcom/android/settings/applications/defaultapps/DefaultBrowserPicker;->mUserId:I

    const/4 v9, 0x0

    invoke-interface {v7, v0, v9, v8}, Lcom/android/settings/applications/PackageManagerWrapper;->uR(Ljava/lang/String;II)Landroid/content/pm/ApplicationInfo;

    move-result-object v0

    invoke-direct {v5, v6, v0}, Lcom/android/settings/applications/defaultapps/a;-><init>(Lcom/android/settings/applications/PackageManagerWrapper;Landroid/content/pm/PackageItemInfo;)V

    invoke-interface {v2, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v0

    goto :goto_1

    :cond_2
    return-object v2
.end method
