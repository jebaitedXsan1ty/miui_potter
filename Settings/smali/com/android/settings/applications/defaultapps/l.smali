.class public Lcom/android/settings/applications/defaultapps/l;
.super Lcom/android/settings/applications/defaultapps/c;
.source "DefaultBrowserPreferenceController.java"


# static fields
.field static final oC:Landroid/content/Intent;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string/jumbo v1, "android.intent.action.VIEW"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "android.intent.category.BROWSABLE"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "http:"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v0

    sput-object v0, Lcom/android/settings/applications/defaultapps/l;->oC:Landroid/content/Intent;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/settings/applications/defaultapps/c;-><init>(Landroid/content/Context;)V

    return-void
.end method

.method public static nR(Ljava/lang/String;Landroid/content/Context;)Z
    .locals 3

    const/4 v0, 0x0

    new-instance v1, Landroid/content/Intent;

    sget-object v2, Lcom/android/settings/applications/defaultapps/l;->oC:Landroid/content/Intent;

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Intent;)V

    invoke-virtual {v1, p0}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    invoke-virtual {v2, v1, v0}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method private nT()Ljava/util/List;
    .locals 4

    iget-object v0, p0, Lcom/android/settings/applications/defaultapps/l;->mPackageManager:Lcom/android/settings/applications/PackageManagerWrapper;

    sget-object v1, Lcom/android/settings/applications/defaultapps/l;->oC:Landroid/content/Intent;

    iget v2, p0, Lcom/android/settings/applications/defaultapps/l;->mUserId:I

    const/high16 v3, 0x20000

    invoke-interface {v0, v1, v3, v2}, Lcom/android/settings/applications/PackageManagerWrapper;->uZ(Landroid/content/Intent;II)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method private nU()Ljava/lang/String;
    .locals 4

    const/4 v3, 0x0

    invoke-direct {p0}, Lcom/android/settings/applications/defaultapps/l;->nT()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/ResolveInfo;

    iget-object v1, p0, Lcom/android/settings/applications/defaultapps/l;->mPackageManager:Lcom/android/settings/applications/PackageManagerWrapper;

    invoke-interface {v1}, Lcom/android/settings/applications/PackageManagerWrapper;->uW()Landroid/content/pm/PackageManager;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/pm/ResolveInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    return-object v3
.end method


# virtual methods
.method public cz(Landroid/preference/Preference;)V
    .locals 2

    invoke-super {p0, p1}, Lcom/android/settings/applications/defaultapps/c;->cz(Landroid/preference/Preference;)V

    invoke-virtual {p0}, Lcom/android/settings/applications/defaultapps/l;->ni()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p1, v0}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    :cond_0
    return-void
.end method

.method public l()Ljava/lang/String;
    .locals 1

    const-string/jumbo v0, "default_browser"

    return-object v0
.end method

.method protected mr()Lcom/android/settings/applications/defaultapps/a;
    .locals 5

    :try_start_0
    new-instance v0, Lcom/android/settings/applications/defaultapps/a;

    iget-object v1, p0, Lcom/android/settings/applications/defaultapps/l;->mPackageManager:Lcom/android/settings/applications/PackageManagerWrapper;

    iget-object v2, p0, Lcom/android/settings/applications/defaultapps/l;->mPackageManager:Lcom/android/settings/applications/PackageManagerWrapper;

    invoke-interface {v2}, Lcom/android/settings/applications/PackageManagerWrapper;->uW()Landroid/content/pm/PackageManager;

    move-result-object v2

    iget-object v3, p0, Lcom/android/settings/applications/defaultapps/l;->mPackageManager:Lcom/android/settings/applications/PackageManagerWrapper;

    iget v4, p0, Lcom/android/settings/applications/defaultapps/l;->mUserId:I

    invoke-interface {v3, v4}, Lcom/android/settings/applications/PackageManagerWrapper;->uS(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/android/settings/applications/defaultapps/a;-><init>(Lcom/android/settings/applications/PackageManagerWrapper;Landroid/content/pm/PackageItemInfo;)V
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    move-exception v0

    const/4 v0, 0x0

    return-object v0
.end method

.method public nS(Ljava/lang/String;I)Z
    .locals 5

    const/4 v0, 0x1

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/android/settings/applications/defaultapps/l;->mPackageManager:Lcom/android/settings/applications/PackageManagerWrapper;

    invoke-interface {v2, p2}, Lcom/android/settings/applications/PackageManagerWrapper;->uS(I)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0

    :cond_0
    iget-object v2, p0, Lcom/android/settings/applications/defaultapps/l;->mPackageManager:Lcom/android/settings/applications/PackageManagerWrapper;

    sget-object v3, Lcom/android/settings/applications/defaultapps/l;->oC:Landroid/content/Intent;

    const/high16 v4, 0x20000

    invoke-interface {v2, v3, v4, p2}, Lcom/android/settings/applications/PackageManagerWrapper;->uZ(Landroid/content/Intent;II)Ljava/util/List;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ne v2, v0, :cond_1

    :goto_0
    return v0

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public ni()Ljava/lang/CharSequence;
    .locals 2

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/android/settings/applications/defaultapps/l;->p()Z

    move-result v1

    if-nez v1, :cond_0

    return-object v0

    :cond_0
    invoke-virtual {p0}, Lcom/android/settings/applications/defaultapps/l;->mr()Lcom/android/settings/applications/defaultapps/a;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/android/settings/applications/defaultapps/a;->mS()Ljava/lang/CharSequence;

    move-result-object v0

    :cond_1
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    return-object v0

    :cond_2
    invoke-direct {p0}, Lcom/android/settings/applications/defaultapps/l;->nU()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public p()Z
    .locals 1

    invoke-direct {p0}, Lcom/android/settings/applications/defaultapps/l;->nT()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
