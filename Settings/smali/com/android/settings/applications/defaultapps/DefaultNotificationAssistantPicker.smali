.class public Lcom/android/settings/applications/defaultapps/DefaultNotificationAssistantPicker;
.super Lcom/android/settings/applications/defaultapps/DefaultAppPickerFragment;
.source "DefaultNotificationAssistantPicker.java"


# instance fields
.field private final os:Lcom/android/settings/utils/b;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/android/settings/applications/defaultapps/DefaultAppPickerFragment;-><init>()V

    invoke-direct {p0}, Lcom/android/settings/applications/defaultapps/DefaultNotificationAssistantPicker;->nx()Lcom/android/settings/utils/b;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/applications/defaultapps/DefaultNotificationAssistantPicker;->os:Lcom/android/settings/utils/b;

    return-void
.end method

.method private nx()Lcom/android/settings/utils/b;
    .locals 2

    new-instance v0, Lcom/android/settings/utils/b;

    invoke-direct {v0}, Lcom/android/settings/utils/b;-><init>()V

    const-string/jumbo v1, "DefaultNotiAssist"

    iput-object v1, v0, Lcom/android/settings/utils/b;->aMd:Ljava/lang/String;

    const-string/jumbo v1, "enabled_notification_assistant"

    iput-object v1, v0, Lcom/android/settings/utils/b;->aMe:Ljava/lang/String;

    const-string/jumbo v1, "android.service.notification.NotificationAssistantService"

    iput-object v1, v0, Lcom/android/settings/utils/b;->intentAction:Ljava/lang/String;

    const-string/jumbo v1, "android.permission.BIND_NOTIFICATION_ASSISTANT_SERVICE"

    iput-object v1, v0, Lcom/android/settings/utils/b;->aMf:Ljava/lang/String;

    const-string/jumbo v1, "notification assistant"

    iput-object v1, v0, Lcom/android/settings/utils/b;->aMg:Ljava/lang/String;

    const v1, 0x7f120bc7

    iput v1, v0, Lcom/android/settings/utils/b;->aMh:I

    const v1, 0x7f120bc6

    iput v1, v0, Lcom/android/settings/utils/b;->aMi:I

    const v1, 0x7f120b9a

    iput v1, v0, Lcom/android/settings/utils/b;->aMj:I

    return-object v0
.end method


# virtual methods
.method public getMetricsCategory()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method protected mB()Ljava/lang/String;
    .locals 2

    invoke-virtual {p0}, Lcom/android/settings/applications/defaultapps/DefaultNotificationAssistantPicker;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/applications/defaultapps/DefaultNotificationAssistantPicker;->os:Lcom/android/settings/utils/b;

    iget-object v1, v1, Lcom/android/settings/utils/b;->aMe:Ljava/lang/String;

    invoke-static {v0, v1}, Landroid/provider/Settings$Secure;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected mG(Ljava/lang/String;)Z
    .locals 2

    invoke-virtual {p0}, Lcom/android/settings/applications/defaultapps/DefaultNotificationAssistantPicker;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/applications/defaultapps/DefaultNotificationAssistantPicker;->os:Lcom/android/settings/utils/b;

    iget-object v1, v1, Lcom/android/settings/utils/b;->aMe:Ljava/lang/String;

    invoke-static {v0, v1, p1}, Landroid/provider/Settings$Secure;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    const/4 v0, 0x1

    return v0
.end method

.method protected mH()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method protected my()Ljava/util/List;
    .locals 10

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    invoke-static {}, Landroid/app/ActivityManager;->getCurrentUser()I

    move-result v0

    iget-object v1, p0, Lcom/android/settings/applications/defaultapps/DefaultNotificationAssistantPicker;->oq:Lcom/android/settings/applications/PackageManagerWrapper;

    new-instance v3, Landroid/content/Intent;

    iget-object v4, p0, Lcom/android/settings/applications/defaultapps/DefaultNotificationAssistantPicker;->os:Lcom/android/settings/utils/b;

    iget-object v4, v4, Lcom/android/settings/utils/b;->intentAction:Ljava/lang/String;

    invoke-direct {v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const/16 v4, 0x84

    invoke-interface {v1, v3, v4, v0}, Lcom/android/settings/applications/PackageManagerWrapper;->va(Landroid/content/Intent;II)Ljava/util/List;

    move-result-object v3

    const/4 v0, 0x0

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v4

    move v1, v0

    :goto_0
    if-ge v1, v4, :cond_1

    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/ResolveInfo;

    iget-object v0, v0, Landroid/content/pm/ResolveInfo;->serviceInfo:Landroid/content/pm/ServiceInfo;

    iget-object v5, p0, Lcom/android/settings/applications/defaultapps/DefaultNotificationAssistantPicker;->os:Lcom/android/settings/utils/b;

    iget-object v5, v5, Lcom/android/settings/utils/b;->aMf:Ljava/lang/String;

    iget-object v6, v0, Landroid/content/pm/ServiceInfo;->permission:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_0

    iget-object v5, p0, Lcom/android/settings/applications/defaultapps/DefaultNotificationAssistantPicker;->os:Lcom/android/settings/utils/b;

    iget-object v5, v5, Lcom/android/settings/utils/b;->aMd:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "Skipping "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/android/settings/applications/defaultapps/DefaultNotificationAssistantPicker;->os:Lcom/android/settings/utils/b;

    iget-object v7, v7, Lcom/android/settings/utils/b;->aMg:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, " service "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, v0, Landroid/content/pm/ServiceInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, "/"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v0, v0, Landroid/content/pm/ServiceInfo;->name:Ljava/lang/String;

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v6, ": it does not require the permission "

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v6, p0, Lcom/android/settings/applications/defaultapps/DefaultNotificationAssistantPicker;->os:Lcom/android/settings/utils/b;

    iget-object v6, v6, Lcom/android/settings/utils/b;->aMf:Ljava/lang/String;

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v5, v0}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    new-instance v5, Lcom/android/settings/applications/defaultapps/a;

    iget-object v6, p0, Lcom/android/settings/applications/defaultapps/DefaultNotificationAssistantPicker;->oq:Lcom/android/settings/applications/PackageManagerWrapper;

    iget v7, p0, Lcom/android/settings/applications/defaultapps/DefaultNotificationAssistantPicker;->mUserId:I

    new-instance v8, Landroid/content/ComponentName;

    iget-object v9, v0, Landroid/content/pm/ServiceInfo;->packageName:Ljava/lang/String;

    iget-object v0, v0, Landroid/content/pm/ServiceInfo;->name:Ljava/lang/String;

    invoke-direct {v8, v9, v0}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {v5, v6, v7, v8}, Lcom/android/settings/applications/defaultapps/a;-><init>(Lcom/android/settings/applications/PackageManagerWrapper;ILandroid/content/ComponentName;)V

    invoke-interface {v2, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_1
    return-object v2
.end method
