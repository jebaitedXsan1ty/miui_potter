.class public abstract Lcom/android/settings/applications/defaultapps/DefaultAppPickerFragment;
.super Lcom/android/settings/widget/MiuiRadioButtonPickerFragment;
.source "DefaultAppPickerFragment.java"


# instance fields
.field protected oq:Lcom/android/settings/applications/PackageManagerWrapper;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/widget/MiuiRadioButtonPickerFragment;-><init>()V

    return-void
.end method

.method static synthetic nu(Lcom/android/settings/applications/defaultapps/DefaultAppPickerFragment;Ljava/lang/String;)V
    .locals 0

    invoke-virtual {p0, p1}, Lcom/android/settings/applications/defaultapps/DefaultAppPickerFragment;->aAC(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public bindPreferenceExtra(Lcom/android/settings/widget/MiuiRadioButtonPreference;Ljava/lang/String;Lcom/android/settings/widget/r;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    instance-of v0, p3, Lcom/android/settings/applications/defaultapps/a;

    if-nez v0, :cond_0

    return-void

    :cond_0
    invoke-static {p5, p2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    const v0, 0x7f121231

    invoke-virtual {p1, v0}, Lcom/android/settings/widget/MiuiRadioButtonPreference;->setSummary(I)V

    :cond_1
    :goto_0
    return-void

    :cond_2
    move-object v0, p3

    check-cast v0, Lcom/android/settings/applications/defaultapps/a;

    iget-object v0, v0, Lcom/android/settings/applications/defaultapps/a;->summary:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    check-cast p3, Lcom/android/settings/applications/defaultapps/a;

    iget-object v0, p3, Lcom/android/settings/applications/defaultapps/a;->summary:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/android/settings/widget/MiuiRadioButtonPreference;->setSummary(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method protected mz(Lcom/android/settings/widget/r;)Ljava/lang/CharSequence;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public ns(Lcom/android/settings/widget/MiuiRadioButtonPreference;)V
    .locals 4

    invoke-virtual {p1}, Lcom/android/settings/widget/MiuiRadioButtonPreference;->getKey()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/settings/applications/defaultapps/DefaultAppPickerFragment;->aAB(Ljava/lang/String;)Lcom/android/settings/widget/r;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/android/settings/applications/defaultapps/DefaultAppPickerFragment;->mz(Lcom/android/settings/widget/r;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {p0}, Lcom/android/settings/applications/defaultapps/DefaultAppPickerFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-super {p0, p1}, Lcom/android/settings/widget/MiuiRadioButtonPickerFragment;->ns(Lcom/android/settings/widget/MiuiRadioButtonPreference;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    if-eqz v2, :cond_0

    invoke-virtual {p0, v0, v1}, Lcom/android/settings/applications/defaultapps/DefaultAppPickerFragment;->nt(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/android/settings/applications/defaultapps/DefaultAppPickerFragment$ConfirmationDialogFragment;

    move-result-object v0

    invoke-virtual {v2}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const-string/jumbo v2, "DefaultAppConfirm"

    invoke-virtual {v0, v1, v2}, Landroid/app/DialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected nt(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/android/settings/applications/defaultapps/DefaultAppPickerFragment$ConfirmationDialogFragment;
    .locals 1

    new-instance v0, Lcom/android/settings/applications/defaultapps/DefaultAppPickerFragment$ConfirmationDialogFragment;

    invoke-direct {v0}, Lcom/android/settings/applications/defaultapps/DefaultAppPickerFragment$ConfirmationDialogFragment;-><init>()V

    invoke-virtual {v0, p0, p1, p2}, Lcom/android/settings/applications/defaultapps/DefaultAppPickerFragment$ConfirmationDialogFragment;->nv(Lcom/android/settings/applications/defaultapps/DefaultAppPickerFragment;Ljava/lang/String;Ljava/lang/CharSequence;)V

    return-object v0
.end method

.method public onAttach(Landroid/content/Context;)V
    .locals 2

    invoke-super {p0, p1}, Lcom/android/settings/widget/MiuiRadioButtonPickerFragment;->onAttach(Landroid/content/Context;)V

    new-instance v0, Lcom/android/settings/applications/PackageManagerWrapperImpl;

    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/android/settings/applications/PackageManagerWrapperImpl;-><init>(Landroid/content/pm/PackageManager;)V

    iput-object v0, p0, Lcom/android/settings/applications/defaultapps/DefaultAppPickerFragment;->oq:Lcom/android/settings/applications/PackageManagerWrapper;

    return-void
.end method
