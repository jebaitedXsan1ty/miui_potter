.class Lcom/android/settings/applications/defaultapps/k;
.super Ljava/lang/Object;
.source "DefaultPhonePicker.java"


# instance fields
.field private final oB:Landroid/telecom/TelecomManager;


# direct methods
.method public constructor <init>(Landroid/telecom/TelecomManager;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/android/settings/applications/defaultapps/k;->oB:Landroid/telecom/TelecomManager;

    return-void
.end method


# virtual methods
.method public nO(Landroid/content/Context;I)Ljava/lang/String;
    .locals 1

    invoke-static {p1, p2}, Landroid/telecom/DefaultDialerManager;->getDefaultDialerApplication(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public nP()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/applications/defaultapps/k;->oB:Landroid/telecom/TelecomManager;

    invoke-virtual {v0}, Landroid/telecom/TelecomManager;->getSystemDialerPackage()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public nQ(Landroid/content/Context;Ljava/lang/String;I)Z
    .locals 1

    invoke-static {p1, p2, p3}, Landroid/telecom/DefaultDialerManager;->setDefaultDialerApplication(Landroid/content/Context;Ljava/lang/String;I)Z

    move-result v0

    return v0
.end method
