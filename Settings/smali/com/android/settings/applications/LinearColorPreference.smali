.class public Lcom/android/settings/applications/LinearColorPreference;
.super Landroid/support/v7/preference/Preference;
.source "LinearColorPreference.java"


# instance fields
.field Cf:I

.field Cg:I

.field Ch:F

.field Ci:Lcom/android/settings/applications/LinearColorBar$OnRegionTappedListener;

.field Cj:I

.field Ck:F

.field Cl:I

.field Cm:F


# virtual methods
.method public al(Landroid/support/v7/preference/l;)V
    .locals 4

    invoke-super {p0, p1}, Landroid/support/v7/preference/Preference;->al(Landroid/support/v7/preference/l;)V

    const v0, 0x7f0a0265

    invoke-virtual {p1, v0}, Landroid/support/v7/preference/l;->dma(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/settings/applications/LinearColorBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/android/settings/applications/LinearColorBar;->setShowIndicator(Z)V

    iget v1, p0, Lcom/android/settings/applications/LinearColorPreference;->Cj:I

    iget v2, p0, Lcom/android/settings/applications/LinearColorPreference;->Cl:I

    iget v3, p0, Lcom/android/settings/applications/LinearColorPreference;->Cg:I

    invoke-virtual {v0, v1, v2, v3}, Lcom/android/settings/applications/LinearColorBar;->setColors(III)V

    iget v1, p0, Lcom/android/settings/applications/LinearColorPreference;->Ck:F

    iget v2, p0, Lcom/android/settings/applications/LinearColorPreference;->Cm:F

    iget v3, p0, Lcom/android/settings/applications/LinearColorPreference;->Ch:F

    invoke-virtual {v0, v1, v2, v3}, Lcom/android/settings/applications/LinearColorBar;->setRatios(FFF)V

    iget v1, p0, Lcom/android/settings/applications/LinearColorPreference;->Cf:I

    invoke-virtual {v0, v1}, Lcom/android/settings/applications/LinearColorBar;->setColoredRegions(I)V

    iget-object v1, p0, Lcom/android/settings/applications/LinearColorPreference;->Ci:Lcom/android/settings/applications/LinearColorBar$OnRegionTappedListener;

    invoke-virtual {v0, v1}, Lcom/android/settings/applications/LinearColorBar;->setOnRegionTappedListener(Lcom/android/settings/applications/LinearColorBar$OnRegionTappedListener;)V

    return-void
.end method
