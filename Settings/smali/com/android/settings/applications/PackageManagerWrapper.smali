.class public interface abstract Lcom/android/settings/applications/PackageManagerWrapper;
.super Ljava/lang/Object;
.source "PackageManagerWrapper.java"


# virtual methods
.method public abstract uQ(Ljava/lang/String;Landroid/content/pm/IPackageDeleteObserver;II)V
.end method

.method public abstract uR(Ljava/lang/String;II)Landroid/content/pm/ApplicationInfo;
.end method

.method public abstract uS(I)Ljava/lang/String;
.end method

.method public abstract uT(Ljava/util/List;)Landroid/content/ComponentName;
.end method

.method public abstract uU(Ljava/lang/String;Landroid/os/UserHandle;)I
.end method

.method public abstract uV(II)Ljava/util/List;
.end method

.method public abstract uW()Landroid/content/pm/PackageManager;
.end method

.method public abstract uX()Landroid/os/storage/VolumeInfo;
.end method

.method public abstract uY(Ljava/lang/String;)Z
.end method

.method public abstract uZ(Landroid/content/Intent;II)Ljava/util/List;
.end method

.method public abstract va(Landroid/content/Intent;II)Ljava/util/List;
.end method

.method public abstract vb(Landroid/content/IntentFilter;I[Landroid/content/ComponentName;Landroid/content/ComponentName;)V
.end method

.method public abstract vc(Ljava/lang/String;I)Z
.end method
