.class public Lcom/android/settings/applications/LinearColorBar;
.super Landroid/widget/LinearLayout;
.source "LinearColorBar.java"


# instance fields
.field final sE:Landroid/graphics/Paint;

.field final sF:Landroid/graphics/Path;

.field private sG:I

.field final sH:Landroid/graphics/Paint;

.field final sI:Landroid/graphics/Path;

.field private sJ:F

.field sK:I

.field sL:I

.field sM:I

.field sN:I

.field sO:I

.field private sP:I

.field sQ:I

.field private sR:I

.field private sS:Lcom/android/settings/applications/LinearColorBar$OnRegionTappedListener;

.field final sT:Landroid/graphics/Paint;

.field final sU:Landroid/graphics/Rect;

.field private sV:F

.field private sW:I

.field private sX:Z

.field private sY:Z

.field private sZ:F


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3

    const/4 v1, 0x1

    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const v0, -0x312825

    iput v0, p0, Lcom/android/settings/applications/LinearColorBar;->sW:I

    iput-boolean v1, p0, Lcom/android/settings/applications/LinearColorBar;->sX:Z

    const/4 v0, 0x7

    iput v0, p0, Lcom/android/settings/applications/LinearColorBar;->sG:I

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/android/settings/applications/LinearColorBar;->sU:Landroid/graphics/Rect;

    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/android/settings/applications/LinearColorBar;->sT:Landroid/graphics/Paint;

    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/android/settings/applications/LinearColorBar;->sF:Landroid/graphics/Path;

    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/android/settings/applications/LinearColorBar;->sI:Landroid/graphics/Path;

    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/android/settings/applications/LinearColorBar;->sE:Landroid/graphics/Paint;

    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/android/settings/applications/LinearColorBar;->sH:Landroid/graphics/Paint;

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/android/settings/applications/LinearColorBar;->setWillNotDraw(Z)V

    iget-object v0, p0, Lcom/android/settings/applications/LinearColorBar;->sT:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    iget-object v0, p0, Lcom/android/settings/applications/LinearColorBar;->sE:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    iget-object v0, p0, Lcom/android/settings/applications/LinearColorBar;->sE:Landroid/graphics/Paint;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    iget-object v0, p0, Lcom/android/settings/applications/LinearColorBar;->sH:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    invoke-virtual {p0}, Lcom/android/settings/applications/LinearColorBar;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->densityDpi:I

    const/16 v2, 0xf0

    if-lt v0, v2, :cond_0

    const/4 v0, 0x2

    :goto_0
    iput v0, p0, Lcom/android/settings/applications/LinearColorBar;->sQ:I

    iget-object v0, p0, Lcom/android/settings/applications/LinearColorBar;->sH:Landroid/graphics/Paint;

    iget v2, p0, Lcom/android/settings/applications/LinearColorBar;->sQ:I

    int-to-float v2, v2

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    iget-object v0, p0, Lcom/android/settings/applications/LinearColorBar;->sH:Landroid/graphics/Paint;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    invoke-static {p1}, Lcom/android/settings/aq;->cqI(Landroid/content/Context;)I

    move-result v0

    iput v0, p0, Lcom/android/settings/applications/LinearColorBar;->sR:I

    iput v0, p0, Lcom/android/settings/applications/LinearColorBar;->sP:I

    return-void

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method private rj(II)I
    .locals 1

    invoke-virtual {p0}, Lcom/android/settings/applications/LinearColorBar;->isPressed()Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/android/settings/applications/LinearColorBar;->sN:I

    and-int/2addr v0, p2

    if-eqz v0, :cond_0

    const/4 v0, -0x1

    return v0

    :cond_0
    iget v0, p0, Lcom/android/settings/applications/LinearColorBar;->sG:I

    and-int/2addr v0, p2

    if-nez v0, :cond_1

    const v0, -0xaaaaab

    return v0

    :cond_1
    return p1
.end method

.method private rk()V
    .locals 10

    const v5, 0xffffff

    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/android/settings/applications/LinearColorBar;->getPaddingTop()I

    move-result v2

    invoke-virtual {p0}, Lcom/android/settings/applications/LinearColorBar;->getPaddingBottom()I

    move-result v3

    sub-int/2addr v2, v3

    if-gez v2, :cond_2

    move v8, v0

    :goto_0
    iget-object v0, p0, Lcom/android/settings/applications/LinearColorBar;->sU:Landroid/graphics/Rect;

    iput v8, v0, Landroid/graphics/Rect;->top:I

    iget-object v0, p0, Lcom/android/settings/applications/LinearColorBar;->sU:Landroid/graphics/Rect;

    invoke-virtual {p0}, Lcom/android/settings/applications/LinearColorBar;->getHeight()I

    move-result v2

    iput v2, v0, Landroid/graphics/Rect;->bottom:I

    iget-boolean v0, p0, Lcom/android/settings/applications/LinearColorBar;->sX:Z

    if-nez v0, :cond_0

    return-void

    :cond_0
    iget-boolean v0, p0, Lcom/android/settings/applications/LinearColorBar;->sY:Z

    if-eqz v0, :cond_1

    iget-object v9, p0, Lcom/android/settings/applications/LinearColorBar;->sE:Landroid/graphics/Paint;

    new-instance v0, Landroid/graphics/LinearGradient;

    add-int/lit8 v2, v8, -0x2

    int-to-float v4, v2

    iget v2, p0, Lcom/android/settings/applications/LinearColorBar;->sW:I

    and-int/2addr v5, v2

    iget v6, p0, Lcom/android/settings/applications/LinearColorBar;->sW:I

    sget-object v7, Landroid/graphics/Shader$TileMode;->CLAMP:Landroid/graphics/Shader$TileMode;

    move v2, v1

    move v3, v1

    invoke-direct/range {v0 .. v7}, Landroid/graphics/LinearGradient;-><init>(FFFFIILandroid/graphics/Shader$TileMode;)V

    invoke-virtual {v9, v0}, Landroid/graphics/Paint;->setShader(Landroid/graphics/Shader;)Landroid/graphics/Shader;

    :goto_1
    iget-object v9, p0, Lcom/android/settings/applications/LinearColorBar;->sH:Landroid/graphics/Paint;

    new-instance v0, Landroid/graphics/LinearGradient;

    div-int/lit8 v2, v8, 0x2

    int-to-float v4, v2

    sget-object v7, Landroid/graphics/Shader$TileMode;->CLAMP:Landroid/graphics/Shader$TileMode;

    const v5, 0xa0a0a0

    const v6, -0x5f5f60

    move v2, v1

    move v3, v1

    invoke-direct/range {v0 .. v7}, Landroid/graphics/LinearGradient;-><init>(FFFFIILandroid/graphics/Shader$TileMode;)V

    invoke-virtual {v9, v0}, Landroid/graphics/Paint;->setShader(Landroid/graphics/Shader;)Landroid/graphics/Shader;

    return-void

    :cond_1
    iget-object v9, p0, Lcom/android/settings/applications/LinearColorBar;->sE:Landroid/graphics/Paint;

    new-instance v0, Landroid/graphics/LinearGradient;

    add-int/lit8 v2, v8, -0x2

    int-to-float v4, v2

    iget v2, p0, Lcom/android/settings/applications/LinearColorBar;->sR:I

    and-int/2addr v5, v2

    iget v6, p0, Lcom/android/settings/applications/LinearColorBar;->sR:I

    sget-object v7, Landroid/graphics/Shader$TileMode;->CLAMP:Landroid/graphics/Shader$TileMode;

    move v2, v1

    move v3, v1

    invoke-direct/range {v0 .. v7}, Landroid/graphics/LinearGradient;-><init>(FFFFIILandroid/graphics/Shader$TileMode;)V

    invoke-virtual {v9, v0}, Landroid/graphics/Paint;->setShader(Landroid/graphics/Shader;)Landroid/graphics/Shader;

    goto :goto_1

    :cond_2
    move v8, v2

    goto :goto_0
.end method


# virtual methods
.method protected dispatchSetPressed(Z)V
    .locals 0

    invoke-virtual {p0}, Lcom/android/settings/applications/LinearColorBar;->invalidate()V

    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 16

    invoke-super/range {p0 .. p1}, Landroid/widget/LinearLayout;->onDraw(Landroid/graphics/Canvas;)V

    invoke-virtual/range {p0 .. p0}, Lcom/android/settings/applications/LinearColorBar;->getWidth()I

    move-result v12

    const/4 v13, 0x0

    int-to-float v1, v12

    move-object/from16 v0, p0

    iget v2, v0, Lcom/android/settings/applications/LinearColorBar;->sV:F

    mul-float/2addr v1, v2

    float-to-int v1, v1

    add-int/lit8 v9, v1, 0x0

    int-to-float v1, v12

    move-object/from16 v0, p0

    iget v2, v0, Lcom/android/settings/applications/LinearColorBar;->sZ:F

    mul-float/2addr v1, v2

    float-to-int v1, v1

    add-int v8, v9, v1

    int-to-float v1, v12

    move-object/from16 v0, p0

    iget v2, v0, Lcom/android/settings/applications/LinearColorBar;->sJ:F

    mul-float/2addr v1, v2

    float-to-int v1, v1

    add-int v2, v8, v1

    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/android/settings/applications/LinearColorBar;->sY:Z

    if-eqz v1, :cond_8

    invoke-virtual/range {p0 .. p0}, Lcom/android/settings/applications/LinearColorBar;->isLayoutRtl()Z

    move-result v1

    if-eqz v1, :cond_6

    sub-int v1, v12, v2

    :goto_0
    invoke-virtual/range {p0 .. p0}, Lcom/android/settings/applications/LinearColorBar;->isLayoutRtl()Z

    move-result v3

    if-eqz v3, :cond_7

    sub-int v2, v12, v8

    move v10, v2

    move v11, v1

    :goto_1
    move-object/from16 v0, p0

    iget v1, v0, Lcom/android/settings/applications/LinearColorBar;->sK:I

    if-ne v1, v11, :cond_0

    move-object/from16 v0, p0

    iget v1, v0, Lcom/android/settings/applications/LinearColorBar;->sL:I

    if-eq v1, v10, :cond_2

    :cond_0
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/settings/applications/LinearColorBar;->sF:Landroid/graphics/Path;

    invoke-virtual {v1}, Landroid/graphics/Path;->reset()V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/settings/applications/LinearColorBar;->sI:Landroid/graphics/Path;

    invoke-virtual {v1}, Landroid/graphics/Path;->reset()V

    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/android/settings/applications/LinearColorBar;->sX:Z

    if-eqz v1, :cond_1

    if-ge v11, v10, :cond_1

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/settings/applications/LinearColorBar;->sU:Landroid/graphics/Rect;

    iget v14, v1, Landroid/graphics/Rect;->top:I

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/settings/applications/LinearColorBar;->sF:Landroid/graphics/Path;

    int-to-float v2, v11

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/settings/applications/LinearColorBar;->sU:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->top:I

    int-to-float v3, v3

    invoke-virtual {v1, v2, v3}, Landroid/graphics/Path;->moveTo(FF)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/settings/applications/LinearColorBar;->sF:Landroid/graphics/Path;

    int-to-float v2, v11

    const/4 v3, 0x0

    const/high16 v4, -0x40000000    # -2.0f

    int-to-float v5, v14

    const/high16 v6, -0x40000000    # -2.0f

    const/4 v7, 0x0

    invoke-virtual/range {v1 .. v7}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/settings/applications/LinearColorBar;->sF:Landroid/graphics/Path;

    add-int/lit8 v2, v12, 0x2

    add-int/lit8 v2, v2, -0x1

    int-to-float v2, v2

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/settings/applications/LinearColorBar;->sF:Landroid/graphics/Path;

    add-int/lit8 v2, v12, 0x2

    add-int/lit8 v2, v2, -0x1

    int-to-float v2, v2

    int-to-float v3, v14

    int-to-float v4, v10

    const/4 v5, 0x0

    int-to-float v6, v10

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/settings/applications/LinearColorBar;->sU:Landroid/graphics/Rect;

    iget v7, v7, Landroid/graphics/Rect;->top:I

    int-to-float v7, v7

    invoke-virtual/range {v1 .. v7}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/settings/applications/LinearColorBar;->sF:Landroid/graphics/Path;

    invoke-virtual {v1}, Landroid/graphics/Path;->close()V

    move-object/from16 v0, p0

    iget v1, v0, Lcom/android/settings/applications/LinearColorBar;->sQ:I

    int-to-float v1, v1

    const/high16 v2, 0x3f000000    # 0.5f

    add-float v15, v1, v2

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/settings/applications/LinearColorBar;->sI:Landroid/graphics/Path;

    const/high16 v2, -0x40000000    # -2.0f

    add-float/2addr v2, v15

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/graphics/Path;->moveTo(FF)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/settings/applications/LinearColorBar;->sI:Landroid/graphics/Path;

    const/high16 v2, -0x40000000    # -2.0f

    add-float/2addr v2, v15

    int-to-float v3, v14

    int-to-float v4, v11

    add-float/2addr v4, v15

    const/4 v5, 0x0

    int-to-float v6, v11

    add-float/2addr v6, v15

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/settings/applications/LinearColorBar;->sU:Landroid/graphics/Rect;

    iget v7, v7, Landroid/graphics/Rect;->top:I

    int-to-float v7, v7

    invoke-virtual/range {v1 .. v7}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/settings/applications/LinearColorBar;->sI:Landroid/graphics/Path;

    add-int/lit8 v2, v12, 0x2

    add-int/lit8 v2, v2, -0x1

    int-to-float v2, v2

    sub-float/2addr v2, v15

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/graphics/Path;->moveTo(FF)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/settings/applications/LinearColorBar;->sI:Landroid/graphics/Path;

    add-int/lit8 v2, v12, 0x2

    add-int/lit8 v2, v2, -0x1

    int-to-float v2, v2

    sub-float/2addr v2, v15

    int-to-float v3, v14

    int-to-float v4, v10

    sub-float/2addr v4, v15

    const/4 v5, 0x0

    int-to-float v6, v10

    sub-float/2addr v6, v15

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/settings/applications/LinearColorBar;->sU:Landroid/graphics/Rect;

    iget v7, v7, Landroid/graphics/Rect;->top:I

    int-to-float v7, v7

    invoke-virtual/range {v1 .. v7}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    :cond_1
    move-object/from16 v0, p0

    iput v11, v0, Lcom/android/settings/applications/LinearColorBar;->sK:I

    move-object/from16 v0, p0

    iput v10, v0, Lcom/android/settings/applications/LinearColorBar;->sL:I

    :cond_2
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/settings/applications/LinearColorBar;->sI:Landroid/graphics/Path;

    invoke-virtual {v1}, Landroid/graphics/Path;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_3

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/settings/applications/LinearColorBar;->sI:Landroid/graphics/Path;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/settings/applications/LinearColorBar;->sH:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/settings/applications/LinearColorBar;->sF:Landroid/graphics/Path;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/settings/applications/LinearColorBar;->sE:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    :cond_3
    if-lez v9, :cond_11

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/settings/applications/LinearColorBar;->sU:Landroid/graphics/Rect;

    invoke-virtual/range {p0 .. p0}, Lcom/android/settings/applications/LinearColorBar;->isLayoutRtl()Z

    move-result v1

    if-eqz v1, :cond_b

    sub-int v1, v12, v9

    add-int/lit8 v1, v1, 0x0

    :goto_2
    iput v1, v2, Landroid/graphics/Rect;->left:I

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/settings/applications/LinearColorBar;->sU:Landroid/graphics/Rect;

    invoke-virtual/range {p0 .. p0}, Lcom/android/settings/applications/LinearColorBar;->isLayoutRtl()Z

    move-result v1

    if-eqz v1, :cond_c

    move v1, v12

    :goto_3
    iput v1, v2, Landroid/graphics/Rect;->right:I

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/settings/applications/LinearColorBar;->sT:Landroid/graphics/Paint;

    move-object/from16 v0, p0

    iget v2, v0, Lcom/android/settings/applications/LinearColorBar;->sP:I

    const/4 v3, 0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v2, v3}, Lcom/android/settings/applications/LinearColorBar;->rj(II)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/settings/applications/LinearColorBar;->sU:Landroid/graphics/Rect;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/settings/applications/LinearColorBar;->sT:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    add-int/lit8 v1, v9, 0x0

    sub-int/2addr v12, v1

    move v1, v9

    :goto_4
    move-object/from16 v0, p0

    iput v9, v0, Lcom/android/settings/applications/LinearColorBar;->sM:I

    move-object/from16 v0, p0

    iput v8, v0, Lcom/android/settings/applications/LinearColorBar;->sO:I

    if-ge v1, v8, :cond_10

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/settings/applications/LinearColorBar;->sU:Landroid/graphics/Rect;

    invoke-virtual/range {p0 .. p0}, Lcom/android/settings/applications/LinearColorBar;->isLayoutRtl()Z

    move-result v2

    if-eqz v2, :cond_d

    sub-int v2, v12, v8

    add-int/2addr v2, v1

    :goto_5
    iput v2, v3, Landroid/graphics/Rect;->left:I

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/settings/applications/LinearColorBar;->sU:Landroid/graphics/Rect;

    invoke-virtual/range {p0 .. p0}, Lcom/android/settings/applications/LinearColorBar;->isLayoutRtl()Z

    move-result v2

    if-eqz v2, :cond_e

    move v2, v12

    :goto_6
    iput v2, v3, Landroid/graphics/Rect;->right:I

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/settings/applications/LinearColorBar;->sT:Landroid/graphics/Paint;

    move-object/from16 v0, p0

    iget v3, v0, Lcom/android/settings/applications/LinearColorBar;->sR:I

    const/4 v4, 0x2

    move-object/from16 v0, p0

    invoke-direct {v0, v3, v4}, Lcom/android/settings/applications/LinearColorBar;->rj(II)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setColor(I)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/settings/applications/LinearColorBar;->sU:Landroid/graphics/Rect;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/settings/applications/LinearColorBar;->sT:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    sub-int v1, v8, v1

    sub-int v2, v12, v1

    move v1, v8

    :goto_7
    add-int v3, v1, v2

    if-ge v1, v3, :cond_5

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/settings/applications/LinearColorBar;->sU:Landroid/graphics/Rect;

    invoke-virtual/range {p0 .. p0}, Lcom/android/settings/applications/LinearColorBar;->isLayoutRtl()Z

    move-result v5

    if-eqz v5, :cond_4

    sub-int v5, v2, v3

    add-int/2addr v1, v5

    :cond_4
    iput v1, v4, Landroid/graphics/Rect;->left:I

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/settings/applications/LinearColorBar;->sU:Landroid/graphics/Rect;

    invoke-virtual/range {p0 .. p0}, Lcom/android/settings/applications/LinearColorBar;->isLayoutRtl()Z

    move-result v1

    if-eqz v1, :cond_f

    move v1, v2

    :goto_8
    iput v1, v4, Landroid/graphics/Rect;->right:I

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/settings/applications/LinearColorBar;->sT:Landroid/graphics/Paint;

    move-object/from16 v0, p0

    iget v2, v0, Lcom/android/settings/applications/LinearColorBar;->sW:I

    const/4 v3, 0x4

    move-object/from16 v0, p0

    invoke-direct {v0, v2, v3}, Lcom/android/settings/applications/LinearColorBar;->rj(II)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/settings/applications/LinearColorBar;->sU:Landroid/graphics/Rect;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/settings/applications/LinearColorBar;->sT:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    :cond_5
    return-void

    :cond_6
    move v1, v8

    goto/16 :goto_0

    :cond_7
    move v10, v2

    move v11, v1

    goto/16 :goto_1

    :cond_8
    invoke-virtual/range {p0 .. p0}, Lcom/android/settings/applications/LinearColorBar;->isLayoutRtl()Z

    move-result v1

    if-eqz v1, :cond_9

    sub-int v1, v12, v8

    :goto_9
    invoke-virtual/range {p0 .. p0}, Lcom/android/settings/applications/LinearColorBar;->isLayoutRtl()Z

    move-result v2

    if-eqz v2, :cond_a

    sub-int v2, v12, v9

    move v10, v2

    move v11, v1

    goto/16 :goto_1

    :cond_9
    move v1, v9

    goto :goto_9

    :cond_a
    move v10, v8

    move v11, v1

    goto/16 :goto_1

    :cond_b
    const/4 v1, 0x0

    goto/16 :goto_2

    :cond_c
    move v1, v9

    goto/16 :goto_3

    :cond_d
    move v2, v1

    goto/16 :goto_5

    :cond_e
    move v2, v8

    goto/16 :goto_6

    :cond_f
    move v1, v3

    goto :goto_8

    :cond_10
    move v2, v12

    goto :goto_7

    :cond_11
    move v1, v13

    goto/16 :goto_4
.end method

.method protected onSizeChanged(IIII)V
    .locals 0

    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/LinearLayout;->onSizeChanged(IIII)V

    invoke-direct {p0}, Lcom/android/settings/applications/LinearColorBar;->rk()V

    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 2

    iget-object v0, p0, Lcom/android/settings/applications/LinearColorBar;->sS:Lcom/android/settings/applications/LinearColorBar$OnRegionTappedListener;

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :cond_0
    :goto_0
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0

    :pswitch_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    float-to-int v0, v0

    iget v1, p0, Lcom/android/settings/applications/LinearColorBar;->sM:I

    if-ge v0, v1, :cond_1

    const/4 v0, 0x1

    iput v0, p0, Lcom/android/settings/applications/LinearColorBar;->sN:I

    :goto_1
    invoke-virtual {p0}, Lcom/android/settings/applications/LinearColorBar;->invalidate()V

    goto :goto_0

    :cond_1
    iget v1, p0, Lcom/android/settings/applications/LinearColorBar;->sO:I

    if-ge v0, v1, :cond_2

    const/4 v0, 0x2

    iput v0, p0, Lcom/android/settings/applications/LinearColorBar;->sN:I

    goto :goto_1

    :cond_2
    const/4 v0, 0x4

    iput v0, p0, Lcom/android/settings/applications/LinearColorBar;->sN:I

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method public performClick()Z
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/android/settings/applications/LinearColorBar;->sS:Lcom/android/settings/applications/LinearColorBar$OnRegionTappedListener;

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/android/settings/applications/LinearColorBar;->sN:I

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/applications/LinearColorBar;->sS:Lcom/android/settings/applications/LinearColorBar$OnRegionTappedListener;

    iget v1, p0, Lcom/android/settings/applications/LinearColorBar;->sN:I

    invoke-interface {v0, v1}, Lcom/android/settings/applications/LinearColorBar$OnRegionTappedListener;->rl(I)V

    iput v2, p0, Lcom/android/settings/applications/LinearColorBar;->sN:I

    :cond_0
    invoke-super {p0}, Landroid/widget/LinearLayout;->performClick()Z

    move-result v0

    return v0
.end method

.method public setColoredRegions(I)V
    .locals 0

    iput p1, p0, Lcom/android/settings/applications/LinearColorBar;->sG:I

    invoke-virtual {p0}, Lcom/android/settings/applications/LinearColorBar;->invalidate()V

    return-void
.end method

.method public setColors(III)V
    .locals 0

    iput p1, p0, Lcom/android/settings/applications/LinearColorBar;->sP:I

    iput p2, p0, Lcom/android/settings/applications/LinearColorBar;->sR:I

    iput p3, p0, Lcom/android/settings/applications/LinearColorBar;->sW:I

    invoke-direct {p0}, Lcom/android/settings/applications/LinearColorBar;->rk()V

    invoke-virtual {p0}, Lcom/android/settings/applications/LinearColorBar;->invalidate()V

    return-void
.end method

.method public setOnRegionTappedListener(Lcom/android/settings/applications/LinearColorBar$OnRegionTappedListener;)V
    .locals 1

    iget-object v0, p0, Lcom/android/settings/applications/LinearColorBar;->sS:Lcom/android/settings/applications/LinearColorBar$OnRegionTappedListener;

    if-eq p1, v0, :cond_0

    iput-object p1, p0, Lcom/android/settings/applications/LinearColorBar;->sS:Lcom/android/settings/applications/LinearColorBar$OnRegionTappedListener;

    if-eqz p1, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p0, v0}, Lcom/android/settings/applications/LinearColorBar;->setClickable(Z)V

    :cond_0
    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setRatios(FFF)V
    .locals 0

    iput p1, p0, Lcom/android/settings/applications/LinearColorBar;->sV:F

    iput p2, p0, Lcom/android/settings/applications/LinearColorBar;->sZ:F

    iput p3, p0, Lcom/android/settings/applications/LinearColorBar;->sJ:F

    invoke-virtual {p0}, Lcom/android/settings/applications/LinearColorBar;->invalidate()V

    return-void
.end method

.method public setShowIndicator(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/android/settings/applications/LinearColorBar;->sX:Z

    invoke-direct {p0}, Lcom/android/settings/applications/LinearColorBar;->rk()V

    invoke-virtual {p0}, Lcom/android/settings/applications/LinearColorBar;->invalidate()V

    return-void
.end method

.method public setShowingGreen(Z)V
    .locals 1

    iget-boolean v0, p0, Lcom/android/settings/applications/LinearColorBar;->sY:Z

    if-eq v0, p1, :cond_0

    iput-boolean p1, p0, Lcom/android/settings/applications/LinearColorBar;->sY:Z

    invoke-direct {p0}, Lcom/android/settings/applications/LinearColorBar;->rk()V

    invoke-virtual {p0}, Lcom/android/settings/applications/LinearColorBar;->invalidate()V

    :cond_0
    return-void
.end method
