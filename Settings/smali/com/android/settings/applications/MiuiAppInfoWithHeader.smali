.class public abstract Lcom/android/settings/applications/MiuiAppInfoWithHeader;
.super Lcom/android/settings/applications/MiuiAppInfoBase;
.source "MiuiAppInfoWithHeader.java"


# instance fields
.field private EC:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/applications/MiuiAppInfoBase;-><init>()V

    return-void
.end method


# virtual methods
.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Lcom/android/settings/applications/MiuiAppInfoBase;->onActivityCreated(Landroid/os/Bundle;)V

    iget-boolean v0, p0, Lcom/android/settings/applications/MiuiAppInfoWithHeader;->EC:Z

    if-eqz v0, :cond_0

    sget-object v0, Lcom/android/settings/applications/MiuiAppInfoWithHeader;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "onActivityCreated: ignoring duplicate call"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/settings/applications/MiuiAppInfoWithHeader;->EC:Z

    iget-object v0, p0, Lcom/android/settings/applications/MiuiAppInfoWithHeader;->mPackageInfo:Landroid/content/pm/PackageInfo;

    if-nez v0, :cond_1

    return-void

    :cond_1
    invoke-virtual {p0}, Lcom/android/settings/applications/MiuiAppInfoWithHeader;->getActivity()Landroid/app/Activity;

    return-void
.end method
