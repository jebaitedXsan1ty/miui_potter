.class public Lcom/android/settings/applications/FetchPackageStorageAsyncLoader;
.super Lcom/android/settings/utils/k;
.source "FetchPackageStorageAsyncLoader.java"


# instance fields
.field private final BG:Lcom/android/settingslib/b/H;

.field private final BH:Landroid/os/UserHandle;

.field private final mInfo:Landroid/content/pm/ApplicationInfo;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/android/settingslib/b/H;Landroid/content/pm/ApplicationInfo;Landroid/os/UserHandle;)V
    .locals 1

    invoke-direct {p0, p1}, Lcom/android/settings/utils/k;-><init>(Landroid/content/Context;)V

    invoke-static {p2}, Lcom/android/internal/util/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settingslib/b/H;

    iput-object v0, p0, Lcom/android/settings/applications/FetchPackageStorageAsyncLoader;->BG:Lcom/android/settingslib/b/H;

    iput-object p3, p0, Lcom/android/settings/applications/FetchPackageStorageAsyncLoader;->mInfo:Landroid/content/pm/ApplicationInfo;

    iput-object p4, p0, Lcom/android/settings/applications/FetchPackageStorageAsyncLoader;->BH:Landroid/os/UserHandle;

    return-void
.end method


# virtual methods
.method public loadInBackground()Lcom/android/settingslib/b/J;
    .locals 5

    const/4 v1, 0x0

    :try_start_0
    iget-object v0, p0, Lcom/android/settings/applications/FetchPackageStorageAsyncLoader;->BG:Lcom/android/settingslib/b/H;

    iget-object v2, p0, Lcom/android/settings/applications/FetchPackageStorageAsyncLoader;->mInfo:Landroid/content/pm/ApplicationInfo;

    iget-object v2, v2, Landroid/content/pm/ApplicationInfo;->volumeUuid:Ljava/lang/String;

    iget-object v3, p0, Lcom/android/settings/applications/FetchPackageStorageAsyncLoader;->mInfo:Landroid/content/pm/ApplicationInfo;

    iget-object v3, v3, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    iget-object v4, p0, Lcom/android/settings/applications/FetchPackageStorageAsyncLoader;->BH:Landroid/os/UserHandle;

    invoke-virtual {v0, v2, v3, v4}, Lcom/android/settingslib/b/H;->cfO(Ljava/lang/String;Ljava/lang/String;Landroid/os/UserHandle;)Lcom/android/settingslib/b/J;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    const-string/jumbo v2, "FetchPackageStorage"

    const-string/jumbo v3, "Package may have been removed during query, failing gracefully"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    move-object v0, v1

    goto :goto_0
.end method

.method public bridge synthetic loadInBackground()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/android/settings/applications/FetchPackageStorageAsyncLoader;->loadInBackground()Lcom/android/settingslib/b/J;

    move-result-object v0

    return-object v0
.end method

.method protected bridge synthetic onDiscardResult(Ljava/lang/Object;)V
    .locals 0

    check-cast p1, Lcom/android/settingslib/b/J;

    invoke-virtual {p0, p1}, Lcom/android/settings/applications/FetchPackageStorageAsyncLoader;->wC(Lcom/android/settingslib/b/J;)V

    return-void
.end method

.method protected wC(Lcom/android/settingslib/b/J;)V
    .locals 0

    return-void
.end method
