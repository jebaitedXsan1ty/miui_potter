.class public Lcom/android/settings/applications/AppDomainsPreference;
.super Lcom/android/settings/accessibility/MiuiListDialogPreference;
.source "AppDomainsPreference.java"


# instance fields
.field private oJ:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    invoke-direct {p0, p1, p2}, Lcom/android/settings/accessibility/MiuiListDialogPreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const v0, 0x7f0d002e

    invoke-virtual {p0, v0}, Lcom/android/settings/applications/AppDomainsPreference;->setDialogLayoutResource(I)V

    const v0, 0x7f0d002f

    invoke-virtual {p0, v0}, Lcom/android/settings/applications/AppDomainsPreference;->Uw(I)V

    return-void
.end method


# virtual methods
.method public getSummary()Ljava/lang/CharSequence;
    .locals 5

    const/4 v3, 0x1

    const/4 v4, 0x0

    invoke-virtual {p0}, Lcom/android/settings/applications/AppDomainsPreference;->getContext()Landroid/content/Context;

    move-result-object v1

    iget v0, p0, Lcom/android/settings/applications/AppDomainsPreference;->oJ:I

    if-nez v0, :cond_0

    const v0, 0x7f120642

    invoke-virtual {v1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    invoke-super {p0}, Lcom/android/settings/accessibility/MiuiListDialogPreference;->getSummary()Ljava/lang/CharSequence;

    move-result-object v2

    iget v0, p0, Lcom/android/settings/applications/AppDomainsPreference;->oJ:I

    if-ne v0, v3, :cond_1

    const v0, 0x7f120643

    :goto_0
    new-array v3, v3, [Ljava/lang/Object;

    aput-object v2, v3, v4

    invoke-virtual {v1, v0, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_1
    const v0, 0x7f120644

    goto :goto_0
.end method

.method protected of(Landroid/view/View;I)V
    .locals 2

    invoke-virtual {p0, p2}, Lcom/android/settings/applications/AppDomainsPreference;->Ux(I)Ljava/lang/CharSequence;

    move-result-object v1

    if-eqz v1, :cond_0

    const v0, 0x7f0a014e

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_0
    return-void
.end method

.method public og([Ljava/lang/CharSequence;)V
    .locals 1

    if-eqz p1, :cond_0

    array-length v0, p1

    :goto_0
    iput v0, p0, Lcom/android/settings/applications/AppDomainsPreference;->oJ:I

    invoke-super {p0, p1}, Lcom/android/settings/accessibility/MiuiListDialogPreference;->og([Ljava/lang/CharSequence;)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
