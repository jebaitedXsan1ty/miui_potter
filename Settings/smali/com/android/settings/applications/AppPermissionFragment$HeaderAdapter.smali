.class Lcom/android/settings/applications/AppPermissionFragment$HeaderAdapter;
.super Landroid/widget/ArrayAdapter;
.source "AppPermissionFragment.java"


# instance fields
.field private mContext:Landroid/content/Context;

.field private oQ:Landroid/util/ArrayMap;

.field private oR:Landroid/view/LayoutInflater;

.field private oS:Landroid/content/pm/PackageManager;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/util/List;Landroid/util/ArrayMap;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0, p2}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    iput-object p1, p0, Lcom/android/settings/applications/AppPermissionFragment$HeaderAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/applications/AppPermissionFragment$HeaderAdapter;->oS:Landroid/content/pm/PackageManager;

    const-string/jumbo v0, "layout_inflater"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Lcom/android/settings/applications/AppPermissionFragment$HeaderAdapter;->oR:Landroid/view/LayoutInflater;

    iput-object p3, p0, Lcom/android/settings/applications/AppPermissionFragment$HeaderAdapter;->oQ:Landroid/util/ArrayMap;

    return-void
.end method

.method private ol(Landroid/preference/PreferenceActivity$Header;)Landroid/graphics/drawable/Drawable;
    .locals 6

    iget-object v0, p0, Lcom/android/settings/applications/AppPermissionFragment$HeaderAdapter;->oQ:Landroid/util/ArrayMap;

    invoke-virtual {v0, p1}, Landroid/util/ArrayMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/ApplicationInfo;

    iget-object v1, p0, Lcom/android/settings/applications/AppPermissionFragment$HeaderAdapter;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/android/settings/applications/AppPermissionFragment$HeaderAdapter;->oS:Landroid/content/pm/PackageManager;

    const-wide/32 v4, 0x927c0

    invoke-static {v1, v0, v2, v4, v5}, Lmiui/maml/util/AppIconsHelper;->getIconDrawable(Landroid/content/Context;Landroid/content/pm/PackageItemInfo;Landroid/content/pm/PackageManager;J)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 8

    const/4 v3, 0x0

    const/4 v7, 0x0

    invoke-virtual {p0, p1}, Lcom/android/settings/applications/AppPermissionFragment$HeaderAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceActivity$Header;

    if-eqz p2, :cond_0

    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/settings/applications/AppPermissionFragment$ViewHolder;

    :goto_0
    iget-object v2, v1, Lcom/android/settings/applications/AppPermissionFragment$ViewHolder;->oU:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/android/settings/applications/AppPermissionFragment$HeaderAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/preference/PreferenceActivity$Header;->getTitle(Landroid/content/res/Resources;)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, v1, Lcom/android/settings/applications/AppPermissionFragment$ViewHolder;->oT:Landroid/widget/ImageView;

    invoke-direct {p0, v0}, Lcom/android/settings/applications/AppPermissionFragment$HeaderAdapter;->ol(Landroid/preference/PreferenceActivity$Header;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    return-object p2

    :cond_0
    iget-object v1, p0, Lcom/android/settings/applications/AppPermissionFragment$HeaderAdapter;->oR:Landroid/view/LayoutInflater;

    sget v2, Lmiui/R$layout;->preference_value:I

    invoke-virtual {v1, v2, p3, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    sget v1, Lmiui/R$drawable;->preference_item_bg:I

    invoke-virtual {p2, v1}, Landroid/view/View;->setBackgroundResource(I)V

    new-instance v2, Lcom/android/settings/applications/AppPermissionFragment$ViewHolder;

    invoke-direct {v2, v3}, Lcom/android/settings/applications/AppPermissionFragment$ViewHolder;-><init>(Lcom/android/settings/applications/AppPermissionFragment$ViewHolder;)V

    const v1, 0x1020016

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, v2, Lcom/android/settings/applications/AppPermissionFragment$ViewHolder;->oU:Landroid/widget/TextView;

    const v1, 0x1020006

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, v2, Lcom/android/settings/applications/AppPermissionFragment$ViewHolder;->oT:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/android/settings/applications/AppPermissionFragment$HeaderAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v3, 0x7f070286

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iget-object v3, v2, Lcom/android/settings/applications/AppPermissionFragment$ViewHolder;->oU:Landroid/widget/TextView;

    iget-object v4, v2, Lcom/android/settings/applications/AppPermissionFragment$ViewHolder;->oU:Landroid/widget/TextView;

    invoke-virtual {v4}, Landroid/widget/TextView;->getPaddingTop()I

    move-result v4

    iget-object v5, v2, Lcom/android/settings/applications/AppPermissionFragment$ViewHolder;->oU:Landroid/widget/TextView;

    invoke-virtual {v5}, Landroid/widget/TextView;->getPaddingEnd()I

    move-result v5

    iget-object v6, v2, Lcom/android/settings/applications/AppPermissionFragment$ViewHolder;->oU:Landroid/widget/TextView;

    invoke-virtual {v6}, Landroid/widget/TextView;->getPaddingBottom()I

    move-result v6

    invoke-virtual {v3, v1, v4, v5, v6}, Landroid/widget/TextView;->setPaddingRelative(IIII)V

    iget-object v1, p0, Lcom/android/settings/applications/AppPermissionFragment$HeaderAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v3, 0x7f070066

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iget-object v3, v2, Lcom/android/settings/applications/AppPermissionFragment$ViewHolder;->oT:Landroid/widget/ImageView;

    invoke-virtual {v3}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    iput v1, v3, Landroid/view/ViewGroup$LayoutParams;->height:I

    iget-object v3, v2, Lcom/android/settings/applications/AppPermissionFragment$ViewHolder;->oT:Landroid/widget/ImageView;

    invoke-virtual {v3}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    iput v1, v3, Landroid/view/ViewGroup$LayoutParams;->width:I

    iget-object v1, v2, Lcom/android/settings/applications/AppPermissionFragment$ViewHolder;->oT:Landroid/widget/ImageView;

    invoke-virtual {v1, v7, v7, v7, v7}, Landroid/widget/ImageView;->setPadding(IIII)V

    const v1, 0x1020010

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    const/16 v3, 0x8

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    sget-boolean v1, Lmiui/os/Build;->IS_TABLET:Z

    if-nez v1, :cond_1

    sget v1, Lmiui/R$id;->arrow_right:I

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v7}, Landroid/view/View;->setVisibility(I)V

    :cond_1
    invoke-virtual {p2, v2}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    move-object v1, v2

    goto/16 :goto_0
.end method
