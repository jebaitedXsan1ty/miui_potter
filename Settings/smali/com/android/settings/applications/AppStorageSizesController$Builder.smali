.class public Lcom/android/settings/applications/AppStorageSizesController$Builder;
.super Ljava/lang/Object;
.source "AppStorageSizesController.java"


# instance fields
.field private xd:Landroid/preference/Preference;

.field private xe:Landroid/preference/Preference;

.field private xf:I

.field private xg:Landroid/preference/Preference;

.field private xh:I

.field private xi:Landroid/preference/Preference;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/android/settings/applications/AppStorageSizesController;
    .locals 8

    new-instance v0, Lcom/android/settings/applications/AppStorageSizesController;

    iget-object v1, p0, Lcom/android/settings/applications/AppStorageSizesController$Builder;->xi:Landroid/preference/Preference;

    invoke-static {v1}, Lcom/android/internal/util/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/preference/Preference;

    iget-object v2, p0, Lcom/android/settings/applications/AppStorageSizesController$Builder;->xd:Landroid/preference/Preference;

    invoke-static {v2}, Lcom/android/internal/util/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/preference/Preference;

    iget-object v3, p0, Lcom/android/settings/applications/AppStorageSizesController$Builder;->xg:Landroid/preference/Preference;

    invoke-static {v3}, Lcom/android/internal/util/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/preference/Preference;

    iget-object v4, p0, Lcom/android/settings/applications/AppStorageSizesController$Builder;->xe:Landroid/preference/Preference;

    invoke-static {v4}, Lcom/android/internal/util/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/preference/Preference;

    iget v5, p0, Lcom/android/settings/applications/AppStorageSizesController$Builder;->xf:I

    iget v6, p0, Lcom/android/settings/applications/AppStorageSizesController$Builder;->xh:I

    const/4 v7, 0x0

    invoke-direct/range {v0 .. v7}, Lcom/android/settings/applications/AppStorageSizesController;-><init>(Landroid/preference/Preference;Landroid/preference/Preference;Landroid/preference/Preference;Landroid/preference/Preference;IILcom/android/settings/applications/AppStorageSizesController;)V

    return-object v0
.end method

.method public td(Landroid/preference/Preference;)Lcom/android/settings/applications/AppStorageSizesController$Builder;
    .locals 0

    iput-object p1, p0, Lcom/android/settings/applications/AppStorageSizesController$Builder;->xd:Landroid/preference/Preference;

    return-object p0
.end method

.method public te(Landroid/preference/Preference;)Lcom/android/settings/applications/AppStorageSizesController$Builder;
    .locals 0

    iput-object p1, p0, Lcom/android/settings/applications/AppStorageSizesController$Builder;->xe:Landroid/preference/Preference;

    return-object p0
.end method

.method public tf(I)Lcom/android/settings/applications/AppStorageSizesController$Builder;
    .locals 0

    iput p1, p0, Lcom/android/settings/applications/AppStorageSizesController$Builder;->xf:I

    return-object p0
.end method

.method public tg(Landroid/preference/Preference;)Lcom/android/settings/applications/AppStorageSizesController$Builder;
    .locals 0

    iput-object p1, p0, Lcom/android/settings/applications/AppStorageSizesController$Builder;->xg:Landroid/preference/Preference;

    return-object p0
.end method

.method public th(I)Lcom/android/settings/applications/AppStorageSizesController$Builder;
    .locals 0

    iput p1, p0, Lcom/android/settings/applications/AppStorageSizesController$Builder;->xh:I

    return-object p0
.end method

.method public ti(Landroid/preference/Preference;)Lcom/android/settings/applications/AppStorageSizesController$Builder;
    .locals 0

    iput-object p1, p0, Lcom/android/settings/applications/AppStorageSizesController$Builder;->xi:Landroid/preference/Preference;

    return-object p0
.end method
