.class public abstract Lcom/android/settings/applications/InstalledAppCounter;
.super Lcom/android/settings/applications/AppCounter;
.source "InstalledAppCounter.java"


# instance fields
.field private final Dc:I


# direct methods
.method public constructor <init>(Landroid/content/Context;ILcom/android/settings/applications/PackageManagerWrapper;)V
    .locals 0

    invoke-direct {p0, p1, p3}, Lcom/android/settings/applications/AppCounter;-><init>(Landroid/content/Context;Lcom/android/settings/applications/PackageManagerWrapper;)V

    iput p2, p0, Lcom/android/settings/applications/InstalledAppCounter;->Dc:I

    return-void
.end method

.method public static xI(ILcom/android/settings/applications/PackageManagerWrapper;Landroid/content/pm/ApplicationInfo;)Z
    .locals 6

    const/4 v5, 0x0

    const/4 v0, 0x1

    const/4 v1, 0x0

    iget v2, p2, Landroid/content/pm/ApplicationInfo;->uid:I

    invoke-static {v2}, Landroid/os/UserHandle;->getUserId(I)I

    move-result v2

    const/4 v3, -0x1

    if-eq p0, v3, :cond_0

    iget-object v3, p2, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    new-instance v4, Landroid/os/UserHandle;

    invoke-direct {v4, v2}, Landroid/os/UserHandle;-><init>(I)V

    invoke-interface {p1, v3, v4}, Lcom/android/settings/applications/PackageManagerWrapper;->uU(Ljava/lang/String;Landroid/os/UserHandle;)I

    move-result v3

    if-eq v3, p0, :cond_0

    return v1

    :cond_0
    iget v3, p2, Landroid/content/pm/ApplicationInfo;->flags:I

    and-int/lit16 v3, v3, 0x80

    if-eqz v3, :cond_1

    return v0

    :cond_1
    iget v3, p2, Landroid/content/pm/ApplicationInfo;->flags:I

    and-int/lit8 v3, v3, 0x1

    if-nez v3, :cond_2

    return v0

    :cond_2
    new-instance v3, Landroid/content/Intent;

    const-string/jumbo v4, "android.intent.action.MAIN"

    invoke-direct {v3, v4, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    const-string/jumbo v4, "android.intent.category.LAUNCHER"

    invoke-virtual {v3, v4}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v3

    iget-object v4, p2, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v3, v4}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v3

    const v4, 0xc0200

    invoke-interface {p1, v3, v4, v2}, Lcom/android/settings/applications/PackageManagerWrapper;->uZ(Landroid/content/Intent;II)Ljava/util/List;

    move-result-object v2

    if-eqz v2, :cond_3

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-eqz v2, :cond_3

    :goto_0
    return v0

    :cond_3
    move v0, v1

    goto :goto_0
.end method


# virtual methods
.method protected vP(Landroid/content/pm/ApplicationInfo;)Z
    .locals 2

    iget v0, p0, Lcom/android/settings/applications/InstalledAppCounter;->Dc:I

    iget-object v1, p0, Lcom/android/settings/applications/InstalledAppCounter;->AT:Lcom/android/settings/applications/PackageManagerWrapper;

    invoke-static {v0, v1, p1}, Lcom/android/settings/applications/InstalledAppCounter;->xI(ILcom/android/settings/applications/PackageManagerWrapper;Landroid/content/pm/ApplicationInfo;)Z

    move-result v0

    return v0
.end method
