.class Lcom/android/settings/applications/InstalledAppDetailsFragment$ClearUserDataObserver;
.super Landroid/content/pm/IPackageDataObserver$Stub;
.source "InstalledAppDetailsFragment.java"


# instance fields
.field final synthetic ra:Lcom/android/settings/applications/InstalledAppDetailsFragment;


# direct methods
.method constructor <init>(Lcom/android/settings/applications/InstalledAppDetailsFragment;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/applications/InstalledAppDetailsFragment$ClearUserDataObserver;->ra:Lcom/android/settings/applications/InstalledAppDetailsFragment;

    invoke-direct {p0}, Landroid/content/pm/IPackageDataObserver$Stub;-><init>()V

    return-void
.end method


# virtual methods
.method public onRemoveCompleted(Ljava/lang/String;Z)V
    .locals 2

    const/4 v0, 0x1

    iget-object v1, p0, Lcom/android/settings/applications/InstalledAppDetailsFragment$ClearUserDataObserver;->ra:Lcom/android/settings/applications/InstalledAppDetailsFragment;

    invoke-static {v1}, Lcom/android/settings/applications/InstalledAppDetailsFragment;->pX(Lcom/android/settings/applications/InstalledAppDetailsFragment;)Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    if-eqz p2, :cond_0

    :goto_0
    iput v0, v1, Landroid/os/Message;->arg1:I

    iget-object v0, p0, Lcom/android/settings/applications/InstalledAppDetailsFragment$ClearUserDataObserver;->ra:Lcom/android/settings/applications/InstalledAppDetailsFragment;

    invoke-static {v0}, Lcom/android/settings/applications/InstalledAppDetailsFragment;->pX(Lcom/android/settings/applications/InstalledAppDetailsFragment;)Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    return-void

    :cond_0
    const/4 v0, 0x2

    goto :goto_0
.end method
