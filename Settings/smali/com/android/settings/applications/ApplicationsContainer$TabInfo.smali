.class public Lcom/android/settings/applications/ApplicationsContainer$TabInfo;
.super Ljava/lang/Object;
.source "ApplicationsContainer.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# instance fields
.field public mRootView:Landroid/view/View;

.field private tA:Landroid/view/View;

.field public final tB:Lcom/android/settings/applications/ApplicationsContainer;

.field public to:Lcom/android/settings/applications/ApplicationsContainer$ApplicationsAdapter;

.field public final tp:Lcom/android/settingslib/b/a;

.field public final tq:Lcom/android/settings/applications/ManageAppClickListener;

.field public final tr:Ljava/lang/CharSequence;

.field public final ts:I

.field public tt:Landroid/view/LayoutInflater;

.field public final tu:Ljava/lang/CharSequence;

.field public tv:Z

.field private tw:Z

.field public final tx:Ljava/lang/CharSequence;

.field public final ty:I

.field private tz:Landroid/widget/ListView;


# direct methods
.method public constructor <init>(Lcom/android/settings/applications/ApplicationsContainer;Lcom/android/settingslib/b/a;Ljava/lang/CharSequence;ILcom/android/settings/applications/ManageAppClickListener;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/android/settings/applications/ApplicationsContainer$TabInfo;->tB:Lcom/android/settings/applications/ApplicationsContainer;

    iput-object p2, p0, Lcom/android/settings/applications/ApplicationsContainer$TabInfo;->tp:Lcom/android/settingslib/b/a;

    iput-object p3, p0, Lcom/android/settings/applications/ApplicationsContainer$TabInfo;->tx:Ljava/lang/CharSequence;

    iput p4, p0, Lcom/android/settings/applications/ApplicationsContainer$TabInfo;->ty:I

    packed-switch p4, :pswitch_data_0

    const/4 v0, 0x0

    iput v0, p0, Lcom/android/settings/applications/ApplicationsContainer$TabInfo;->ts:I

    :goto_0
    iput-object p5, p0, Lcom/android/settings/applications/ApplicationsContainer$TabInfo;->tq:Lcom/android/settings/applications/ManageAppClickListener;

    const v0, 0x7f120881

    invoke-virtual {p1, v0}, Lcom/android/settings/applications/ApplicationsContainer;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/applications/ApplicationsContainer$TabInfo;->tu:Ljava/lang/CharSequence;

    const v0, 0x7f12043b

    invoke-virtual {p1, v0}, Lcom/android/settings/applications/ApplicationsContainer;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/applications/ApplicationsContainer$TabInfo;->tr:Ljava/lang/CharSequence;

    return-void

    :pswitch_0
    const/4 v0, 0x1

    iput v0, p0, Lcom/android/settings/applications/ApplicationsContainer$TabInfo;->ts:I

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method static synthetic rF(Lcom/android/settings/applications/ApplicationsContainer$TabInfo;)Landroid/widget/ListView;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/applications/ApplicationsContainer$TabInfo;->tz:Landroid/widget/ListView;

    return-object v0
.end method

.method static synthetic rG(Lcom/android/settings/applications/ApplicationsContainer$TabInfo;)Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/applications/ApplicationsContainer$TabInfo;->tA:Landroid/view/View;

    return-object v0
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 8

    iget-object v1, p0, Lcom/android/settings/applications/ApplicationsContainer$TabInfo;->tq:Lcom/android/settings/applications/ManageAppClickListener;

    move-object v2, p0

    move-object v3, p1

    move-object v4, p2

    move v5, p3

    move-wide v6, p4

    invoke-interface/range {v1 .. v7}, Lcom/android/settings/applications/ManageAppClickListener;->rm(Lcom/android/settings/applications/ApplicationsContainer$TabInfo;Landroid/widget/AdapterView;Landroid/view/View;IJ)V

    return-void
.end method

.method public pause()V
    .locals 1

    iget-object v0, p0, Lcom/android/settings/applications/ApplicationsContainer$TabInfo;->to:Lcom/android/settings/applications/ApplicationsContainer$ApplicationsAdapter;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/applications/ApplicationsContainer$TabInfo;->to:Lcom/android/settings/applications/ApplicationsContainer$ApplicationsAdapter;

    invoke-virtual {v0}, Lcom/android/settings/applications/ApplicationsContainer$ApplicationsAdapter;->pause()V

    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/settings/applications/ApplicationsContainer$TabInfo;->tw:Z

    return-void
.end method

.method public rA(Landroid/view/LayoutInflater;)Landroid/view/View;
    .locals 4

    const/4 v1, 0x0

    const/4 v3, 0x1

    iget-object v0, p0, Lcom/android/settings/applications/ApplicationsContainer$TabInfo;->mRootView:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/applications/ApplicationsContainer$TabInfo;->mRootView:Landroid/view/View;

    return-object v0

    :cond_0
    iput-object p1, p0, Lcom/android/settings/applications/ApplicationsContainer$TabInfo;->tt:Landroid/view/LayoutInflater;

    const v0, 0x7f0d00e9

    invoke-virtual {p1, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/applications/ApplicationsContainer$TabInfo;->mRootView:Landroid/view/View;

    iget-object v0, p0, Lcom/android/settings/applications/ApplicationsContainer$TabInfo;->mRootView:Landroid/view/View;

    const v1, 0x7f0a0271

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/applications/ApplicationsContainer$TabInfo;->tA:Landroid/view/View;

    iget-object v0, p0, Lcom/android/settings/applications/ApplicationsContainer$TabInfo;->tA:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/settings/applications/ApplicationsContainer$TabInfo;->mRootView:Landroid/view/View;

    const v1, 0x1020004

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iget-object v0, p0, Lcom/android/settings/applications/ApplicationsContainer$TabInfo;->mRootView:Landroid/view/View;

    const v2, 0x102000a

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    if-eqz v1, :cond_1

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setEmptyView(Landroid/view/View;)V

    :cond_1
    invoke-virtual {v0, p0}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    invoke-virtual {v0, v3}, Landroid/widget/ListView;->setSaveEnabled(Z)V

    invoke-virtual {v0, v3}, Landroid/widget/ListView;->setItemsCanFocus(Z)V

    invoke-virtual {v0, v3}, Landroid/widget/ListView;->setTextFilterEnabled(Z)V

    iput-object v0, p0, Lcom/android/settings/applications/ApplicationsContainer$TabInfo;->tz:Landroid/widget/ListView;

    new-instance v0, Lcom/android/settings/applications/ApplicationsContainer$ApplicationsAdapter;

    iget-object v1, p0, Lcom/android/settings/applications/ApplicationsContainer$TabInfo;->tp:Lcom/android/settingslib/b/a;

    iget v2, p0, Lcom/android/settings/applications/ApplicationsContainer$TabInfo;->ts:I

    invoke-direct {v0, v1, p0, v2}, Lcom/android/settings/applications/ApplicationsContainer$ApplicationsAdapter;-><init>(Lcom/android/settingslib/b/a;Lcom/android/settings/applications/ApplicationsContainer$TabInfo;I)V

    iput-object v0, p0, Lcom/android/settings/applications/ApplicationsContainer$TabInfo;->to:Lcom/android/settings/applications/ApplicationsContainer$ApplicationsAdapter;

    iget-object v0, p0, Lcom/android/settings/applications/ApplicationsContainer$TabInfo;->tz:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/android/settings/applications/ApplicationsContainer$TabInfo;->to:Lcom/android/settings/applications/ApplicationsContainer$ApplicationsAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    iget-object v0, p0, Lcom/android/settings/applications/ApplicationsContainer$TabInfo;->tz:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/android/settings/applications/ApplicationsContainer$TabInfo;->to:Lcom/android/settings/applications/ApplicationsContainer$ApplicationsAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setRecyclerListener(Landroid/widget/AbsListView$RecyclerListener;)V

    iget-object v0, p0, Lcom/android/settings/applications/ApplicationsContainer$TabInfo;->tB:Lcom/android/settings/applications/ApplicationsContainer;

    invoke-virtual {v0}, Lcom/android/settings/applications/ApplicationsContainer;->ro()I

    move-result v0

    iget v1, p0, Lcom/android/settings/applications/ApplicationsContainer$TabInfo;->ty:I

    if-ne v0, v1, :cond_2

    iget-object v0, p0, Lcom/android/settings/applications/ApplicationsContainer$TabInfo;->tB:Lcom/android/settings/applications/ApplicationsContainer;

    invoke-virtual {v0, p0}, Lcom/android/settings/applications/ApplicationsContainer;->rv(Lcom/android/settings/applications/ApplicationsContainer$TabInfo;)V

    :cond_2
    iput-boolean v3, p0, Lcom/android/settings/applications/ApplicationsContainer$TabInfo;->tv:Z

    iget-object v0, p0, Lcom/android/settings/applications/ApplicationsContainer$TabInfo;->mRootView:Landroid/view/View;

    return-object v0
.end method

.method public rB()V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/applications/ApplicationsContainer$TabInfo;->to:Lcom/android/settings/applications/ApplicationsContainer$ApplicationsAdapter;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/applications/ApplicationsContainer$TabInfo;->to:Lcom/android/settings/applications/ApplicationsContainer$ApplicationsAdapter;

    invoke-virtual {v0}, Lcom/android/settings/applications/ApplicationsContainer$ApplicationsAdapter;->rI()V

    :cond_0
    iget-object v0, p0, Lcom/android/settings/applications/ApplicationsContainer$TabInfo;->mRootView:Landroid/view/View;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/settings/applications/ApplicationsContainer$TabInfo;->mRootView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/android/settings/applications/ApplicationsContainer$TabInfo;->mRootView:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    :cond_1
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/settings/applications/ApplicationsContainer$TabInfo;->tw:Z

    return-void
.end method

.method rC()Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/settings/applications/ApplicationsContainer$TabInfo;->tw:Z

    return v0
.end method

.method public rD(I)V
    .locals 1

    iget-object v0, p0, Lcom/android/settings/applications/ApplicationsContainer$TabInfo;->to:Lcom/android/settings/applications/ApplicationsContainer$ApplicationsAdapter;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/applications/ApplicationsContainer$TabInfo;->to:Lcom/android/settings/applications/ApplicationsContainer$ApplicationsAdapter;

    invoke-virtual {v0, p1}, Lcom/android/settings/applications/ApplicationsContainer$ApplicationsAdapter;->rM(I)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/settings/applications/ApplicationsContainer$TabInfo;->tw:Z

    :cond_0
    return-void
.end method

.method public rE(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/android/settings/applications/ApplicationsContainer$TabInfo;->tw:Z

    return-void
.end method
