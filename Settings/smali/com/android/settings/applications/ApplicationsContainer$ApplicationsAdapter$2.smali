.class final Lcom/android/settings/applications/ApplicationsContainer$ApplicationsAdapter$2;
.super Landroid/os/AsyncTask;
.source "ApplicationsContainer.java"


# instance fields
.field final synthetic Gq:Lcom/android/settings/applications/ApplicationsContainer$ApplicationsAdapter;


# direct methods
.method constructor <init>(Lcom/android/settings/applications/ApplicationsContainer$ApplicationsAdapter;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/applications/ApplicationsContainer$ApplicationsAdapter$2;->Gq:Lcom/android/settings/applications/ApplicationsContainer$ApplicationsAdapter;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/android/settings/applications/ApplicationsContainer$ApplicationsAdapter$2;->doInBackground([Ljava/lang/Void;)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/util/ArrayList;
    .locals 3

    iget-object v0, p0, Lcom/android/settings/applications/ApplicationsContainer$ApplicationsAdapter$2;->Gq:Lcom/android/settings/applications/ApplicationsContainer$ApplicationsAdapter;

    invoke-static {v0}, Lcom/android/settings/applications/ApplicationsContainer$ApplicationsAdapter;->rS(Lcom/android/settings/applications/ApplicationsContainer$ApplicationsAdapter;)Lcom/android/settingslib/b/b;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/applications/ApplicationsContainer$ApplicationsAdapter$2;->Gq:Lcom/android/settings/applications/ApplicationsContainer$ApplicationsAdapter;

    invoke-static {v1}, Lcom/android/settings/applications/ApplicationsContainer$ApplicationsAdapter;->rQ(Lcom/android/settings/applications/ApplicationsContainer$ApplicationsAdapter;)Lcom/android/settingslib/b/i;

    move-result-object v1

    iget-object v2, p0, Lcom/android/settings/applications/ApplicationsContainer$ApplicationsAdapter$2;->Gq:Lcom/android/settings/applications/ApplicationsContainer$ApplicationsAdapter;

    invoke-static {v2}, Lcom/android/settings/applications/ApplicationsContainer$ApplicationsAdapter;->rO(Lcom/android/settings/applications/ApplicationsContainer$ApplicationsAdapter;)Ljava/util/Comparator;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/android/settingslib/b/b;->cfx(Lcom/android/settingslib/b/i;Ljava/util/Comparator;)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    check-cast p1, Ljava/util/ArrayList;

    invoke-virtual {p0, p1}, Lcom/android/settings/applications/ApplicationsContainer$ApplicationsAdapter$2;->zR(Ljava/util/ArrayList;)V

    return-void
.end method

.method protected zR(Ljava/util/ArrayList;)V
    .locals 5

    const/4 v4, 0x0

    iget-object v0, p0, Lcom/android/settings/applications/ApplicationsContainer$ApplicationsAdapter$2;->Gq:Lcom/android/settings/applications/ApplicationsContainer$ApplicationsAdapter;

    invoke-static {v0, p1}, Lcom/android/settings/applications/ApplicationsContainer$ApplicationsAdapter;->rU(Lcom/android/settings/applications/ApplicationsContainer$ApplicationsAdapter;Ljava/util/ArrayList;)Ljava/util/ArrayList;

    iget-object v0, p0, Lcom/android/settings/applications/ApplicationsContainer$ApplicationsAdapter$2;->Gq:Lcom/android/settings/applications/ApplicationsContainer$ApplicationsAdapter;

    invoke-static {v0}, Lcom/android/settings/applications/ApplicationsContainer$ApplicationsAdapter;->rN(Lcom/android/settings/applications/ApplicationsContainer$ApplicationsAdapter;)Ljava/util/ArrayList;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/applications/ApplicationsContainer$ApplicationsAdapter$2;->Gq:Lcom/android/settings/applications/ApplicationsContainer$ApplicationsAdapter;

    iget-object v1, p0, Lcom/android/settings/applications/ApplicationsContainer$ApplicationsAdapter$2;->Gq:Lcom/android/settings/applications/ApplicationsContainer$ApplicationsAdapter;

    iget-object v2, p0, Lcom/android/settings/applications/ApplicationsContainer$ApplicationsAdapter$2;->Gq:Lcom/android/settings/applications/ApplicationsContainer$ApplicationsAdapter;

    iget-object v2, v2, Lcom/android/settings/applications/ApplicationsContainer$ApplicationsAdapter;->tF:Ljava/lang/CharSequence;

    iget-object v3, p0, Lcom/android/settings/applications/ApplicationsContainer$ApplicationsAdapter$2;->Gq:Lcom/android/settings/applications/ApplicationsContainer$ApplicationsAdapter;

    invoke-static {v3}, Lcom/android/settings/applications/ApplicationsContainer$ApplicationsAdapter;->rN(Lcom/android/settings/applications/ApplicationsContainer$ApplicationsAdapter;)Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/android/settings/applications/ApplicationsContainer$ApplicationsAdapter;->rH(Ljava/lang/CharSequence;Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/settings/applications/ApplicationsContainer$ApplicationsAdapter;->rV(Lcom/android/settings/applications/ApplicationsContainer$ApplicationsAdapter;Ljava/util/ArrayList;)Ljava/util/ArrayList;

    :cond_0
    iget-object v0, p0, Lcom/android/settings/applications/ApplicationsContainer$ApplicationsAdapter$2;->Gq:Lcom/android/settings/applications/ApplicationsContainer$ApplicationsAdapter;

    invoke-virtual {v0}, Lcom/android/settings/applications/ApplicationsContainer$ApplicationsAdapter;->notifyDataSetChanged()V

    iget-object v0, p0, Lcom/android/settings/applications/ApplicationsContainer$ApplicationsAdapter$2;->Gq:Lcom/android/settings/applications/ApplicationsContainer$ApplicationsAdapter;

    invoke-static {v0}, Lcom/android/settings/applications/ApplicationsContainer$ApplicationsAdapter;->rR(Lcom/android/settings/applications/ApplicationsContainer$ApplicationsAdapter;)I

    move-result v0

    if-lez v0, :cond_1

    iget-object v0, p0, Lcom/android/settings/applications/ApplicationsContainer$ApplicationsAdapter$2;->Gq:Lcom/android/settings/applications/ApplicationsContainer$ApplicationsAdapter;

    invoke-static {v0}, Lcom/android/settings/applications/ApplicationsContainer$ApplicationsAdapter;->rP(Lcom/android/settings/applications/ApplicationsContainer$ApplicationsAdapter;)Ljava/util/ArrayList;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/settings/applications/ApplicationsContainer$ApplicationsAdapter$2;->Gq:Lcom/android/settings/applications/ApplicationsContainer$ApplicationsAdapter;

    invoke-static {v0}, Lcom/android/settings/applications/ApplicationsContainer$ApplicationsAdapter;->rP(Lcom/android/settings/applications/ApplicationsContainer$ApplicationsAdapter;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    iget-object v1, p0, Lcom/android/settings/applications/ApplicationsContainer$ApplicationsAdapter$2;->Gq:Lcom/android/settings/applications/ApplicationsContainer$ApplicationsAdapter;

    invoke-static {v1}, Lcom/android/settings/applications/ApplicationsContainer$ApplicationsAdapter;->rR(Lcom/android/settings/applications/ApplicationsContainer$ApplicationsAdapter;)I

    move-result v1

    if-le v0, v1, :cond_1

    iget-object v0, p0, Lcom/android/settings/applications/ApplicationsContainer$ApplicationsAdapter$2;->Gq:Lcom/android/settings/applications/ApplicationsContainer$ApplicationsAdapter;

    invoke-static {v0}, Lcom/android/settings/applications/ApplicationsContainer$ApplicationsAdapter;->rT(Lcom/android/settings/applications/ApplicationsContainer$ApplicationsAdapter;)Lcom/android/settings/applications/ApplicationsContainer$TabInfo;

    move-result-object v0

    invoke-static {v0}, Lcom/android/settings/applications/ApplicationsContainer$TabInfo;->rF(Lcom/android/settings/applications/ApplicationsContainer$TabInfo;)Landroid/widget/ListView;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/applications/ApplicationsContainer$ApplicationsAdapter$2;->Gq:Lcom/android/settings/applications/ApplicationsContainer$ApplicationsAdapter;

    invoke-static {v1}, Lcom/android/settings/applications/ApplicationsContainer$ApplicationsAdapter;->rR(Lcom/android/settings/applications/ApplicationsContainer$ApplicationsAdapter;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setSelection(I)V

    iget-object v0, p0, Lcom/android/settings/applications/ApplicationsContainer$ApplicationsAdapter$2;->Gq:Lcom/android/settings/applications/ApplicationsContainer$ApplicationsAdapter;

    invoke-static {v0, v4}, Lcom/android/settings/applications/ApplicationsContainer$ApplicationsAdapter;->rW(Lcom/android/settings/applications/ApplicationsContainer$ApplicationsAdapter;I)I

    :cond_1
    return-void
.end method
