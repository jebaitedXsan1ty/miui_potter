.class Lcom/android/settings/applications/RunningState$MergedItem;
.super Lcom/android/settings/applications/RunningState$BaseItem;
.source "RunningState.java"


# instance fields
.field final wE:Ljava/util/ArrayList;

.field wF:Lcom/android/settings/applications/RunningState$ProcessItem;

.field final wG:Ljava/util/ArrayList;

.field wH:Lcom/android/settings/applications/RunningState$UserState;

.field final wI:Ljava/util/ArrayList;

.field private wJ:I

.field private wK:I


# direct methods
.method constructor <init>(I)V
    .locals 2

    const/4 v1, -0x1

    const/4 v0, 0x0

    invoke-direct {p0, v0, p1}, Lcom/android/settings/applications/RunningState$BaseItem;-><init>(ZI)V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/settings/applications/RunningState$MergedItem;->wG:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/settings/applications/RunningState$MergedItem;->wE:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/settings/applications/RunningState$MergedItem;->wI:Ljava/util/ArrayList;

    iput v1, p0, Lcom/android/settings/applications/RunningState$MergedItem;->wJ:I

    iput v1, p0, Lcom/android/settings/applications/RunningState$MergedItem;->wK:I

    return-void
.end method

.method private sT(Landroid/content/Context;II)V
    .locals 6

    const/4 v5, 0x1

    iget v0, p0, Lcom/android/settings/applications/RunningState$MergedItem;->wJ:I

    if-ne v0, p2, :cond_0

    iget v0, p0, Lcom/android/settings/applications/RunningState$MergedItem;->wK:I

    if-eq v0, p3, :cond_2

    :cond_0
    iput p2, p0, Lcom/android/settings/applications/RunningState$MergedItem;->wJ:I

    iput p3, p0, Lcom/android/settings/applications/RunningState$MergedItem;->wK:I

    const v0, 0x7f120e60

    if-eq p2, v5, :cond_4

    if-eq p3, v5, :cond_3

    const v0, 0x7f120e5d

    :cond_1
    :goto_0
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    const/4 v4, 0x0

    aput-object v3, v2, v4

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-virtual {v1, v0, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/applications/RunningState$MergedItem;->wg:Ljava/lang/String;

    :cond_2
    return-void

    :cond_3
    const v0, 0x7f120e5e

    goto :goto_0

    :cond_4
    if-eq p3, v5, :cond_1

    const v0, 0x7f120e5f

    goto :goto_0
.end method


# virtual methods
.method sU(Landroid/content/Context;Z)Z
    .locals 12

    const-wide/16 v10, 0x0

    const-wide/16 v4, -0x1

    const/4 v1, 0x0

    const/4 v2, 0x0

    iput-boolean p2, p0, Lcom/android/settings/applications/RunningState$MergedItem;->wa:Z

    iget-object v0, p0, Lcom/android/settings/applications/RunningState$MergedItem;->wH:Lcom/android/settings/applications/RunningState$UserState;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/android/settings/applications/RunningState$MergedItem;->wI:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settings/applications/RunningState$MergedItem;

    iget-object v0, v0, Lcom/android/settings/applications/RunningState$MergedItem;->wF:Lcom/android/settings/applications/RunningState$ProcessItem;

    iget-object v0, v0, Lcom/android/settings/applications/RunningState$ProcessItem;->wb:Landroid/content/pm/PackageItemInfo;

    iput-object v0, p0, Lcom/android/settings/applications/RunningState$MergedItem;->wb:Landroid/content/pm/PackageItemInfo;

    iget-object v0, p0, Lcom/android/settings/applications/RunningState$MergedItem;->wH:Lcom/android/settings/applications/RunningState$UserState;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/settings/applications/RunningState$MergedItem;->wH:Lcom/android/settings/applications/RunningState$UserState;

    iget-object v0, v0, Lcom/android/settings/applications/RunningState$UserState;->vZ:Ljava/lang/String;

    :goto_0
    iput-object v0, p0, Lcom/android/settings/applications/RunningState$MergedItem;->wj:Ljava/lang/String;

    iget-object v0, p0, Lcom/android/settings/applications/RunningState$MergedItem;->wj:Ljava/lang/String;

    iput-object v0, p0, Lcom/android/settings/applications/RunningState$MergedItem;->we:Ljava/lang/CharSequence;

    iput-wide v4, p0, Lcom/android/settings/applications/RunningState$MergedItem;->wf:J

    move v1, v2

    move v3, v2

    move v4, v2

    :goto_1
    iget-object v0, p0, Lcom/android/settings/applications/RunningState$MergedItem;->wI:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    iget-object v0, p0, Lcom/android/settings/applications/RunningState$MergedItem;->wI:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settings/applications/RunningState$MergedItem;

    iget v5, v0, Lcom/android/settings/applications/RunningState$MergedItem;->wJ:I

    add-int/2addr v4, v5

    iget v5, v0, Lcom/android/settings/applications/RunningState$MergedItem;->wK:I

    add-int/2addr v3, v5

    iget-wide v6, v0, Lcom/android/settings/applications/RunningState$MergedItem;->wf:J

    cmp-long v5, v6, v10

    if-ltz v5, :cond_0

    iget-wide v6, p0, Lcom/android/settings/applications/RunningState$MergedItem;->wf:J

    iget-wide v8, v0, Lcom/android/settings/applications/RunningState$MergedItem;->wf:J

    cmp-long v5, v6, v8

    if-gez v5, :cond_0

    iget-wide v6, v0, Lcom/android/settings/applications/RunningState$MergedItem;->wf:J

    iput-wide v6, p0, Lcom/android/settings/applications/RunningState$MergedItem;->wf:J

    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    :cond_1
    move-object v0, v1

    goto :goto_0

    :cond_2
    iget-boolean v0, p0, Lcom/android/settings/applications/RunningState$MergedItem;->wa:Z

    if-nez v0, :cond_3

    invoke-direct {p0, p1, v4, v3}, Lcom/android/settings/applications/RunningState$MergedItem;->sT(Landroid/content/Context;II)V

    :cond_3
    return v2

    :cond_4
    iget-object v0, p0, Lcom/android/settings/applications/RunningState$MergedItem;->wF:Lcom/android/settings/applications/RunningState$ProcessItem;

    iget-object v0, v0, Lcom/android/settings/applications/RunningState$ProcessItem;->wb:Landroid/content/pm/PackageItemInfo;

    iput-object v0, p0, Lcom/android/settings/applications/RunningState$MergedItem;->wb:Landroid/content/pm/PackageItemInfo;

    iget-object v0, p0, Lcom/android/settings/applications/RunningState$MergedItem;->wF:Lcom/android/settings/applications/RunningState$ProcessItem;

    iget-object v0, v0, Lcom/android/settings/applications/RunningState$ProcessItem;->we:Ljava/lang/CharSequence;

    iput-object v0, p0, Lcom/android/settings/applications/RunningState$MergedItem;->we:Ljava/lang/CharSequence;

    iget-object v0, p0, Lcom/android/settings/applications/RunningState$MergedItem;->wF:Lcom/android/settings/applications/RunningState$ProcessItem;

    iget-object v0, v0, Lcom/android/settings/applications/RunningState$ProcessItem;->wj:Ljava/lang/String;

    iput-object v0, p0, Lcom/android/settings/applications/RunningState$MergedItem;->wj:Ljava/lang/String;

    iget-boolean v0, p0, Lcom/android/settings/applications/RunningState$MergedItem;->wa:Z

    if-nez v0, :cond_5

    iget-object v0, p0, Lcom/android/settings/applications/RunningState$MergedItem;->wF:Lcom/android/settings/applications/RunningState$ProcessItem;

    iget v0, v0, Lcom/android/settings/applications/RunningState$ProcessItem;->wt:I

    if-lez v0, :cond_7

    const/4 v0, 0x1

    :goto_2
    iget-object v1, p0, Lcom/android/settings/applications/RunningState$MergedItem;->wG:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/2addr v0, v1

    iget-object v1, p0, Lcom/android/settings/applications/RunningState$MergedItem;->wE:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    invoke-direct {p0, p1, v0, v1}, Lcom/android/settings/applications/RunningState$MergedItem;->sT(Landroid/content/Context;II)V

    :cond_5
    iput-wide v4, p0, Lcom/android/settings/applications/RunningState$MergedItem;->wf:J

    move v1, v2

    :goto_3
    iget-object v0, p0, Lcom/android/settings/applications/RunningState$MergedItem;->wE:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    iget-object v0, p0, Lcom/android/settings/applications/RunningState$MergedItem;->wE:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settings/applications/RunningState$ServiceItem;

    iget-wide v4, v0, Lcom/android/settings/applications/RunningState$ServiceItem;->wf:J

    cmp-long v3, v4, v10

    if-ltz v3, :cond_6

    iget-wide v4, p0, Lcom/android/settings/applications/RunningState$MergedItem;->wf:J

    iget-wide v6, v0, Lcom/android/settings/applications/RunningState$ServiceItem;->wf:J

    cmp-long v3, v4, v6

    if-gez v3, :cond_6

    iget-wide v4, v0, Lcom/android/settings/applications/RunningState$ServiceItem;->wf:J

    iput-wide v4, p0, Lcom/android/settings/applications/RunningState$MergedItem;->wf:J

    :cond_6
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_3

    :cond_7
    move v0, v2

    goto :goto_2
.end method

.method sV(Landroid/content/Context;)Z
    .locals 8

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/android/settings/applications/RunningState$MergedItem;->wH:Lcom/android/settings/applications/RunningState$UserState;

    if-eqz v0, :cond_0

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/android/settings/applications/RunningState$MergedItem;->wl:J

    move v1, v2

    :goto_0
    iget-object v0, p0, Lcom/android/settings/applications/RunningState$MergedItem;->wI:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    iget-object v0, p0, Lcom/android/settings/applications/RunningState$MergedItem;->wI:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settings/applications/RunningState$MergedItem;

    invoke-virtual {v0, p1}, Lcom/android/settings/applications/RunningState$MergedItem;->sV(Landroid/content/Context;)Z

    iget-wide v4, p0, Lcom/android/settings/applications/RunningState$MergedItem;->wl:J

    iget-wide v6, v0, Lcom/android/settings/applications/RunningState$MergedItem;->wl:J

    add-long/2addr v4, v6

    iput-wide v4, p0, Lcom/android/settings/applications/RunningState$MergedItem;->wl:J

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/android/settings/applications/RunningState$MergedItem;->wF:Lcom/android/settings/applications/RunningState$ProcessItem;

    iget-wide v0, v0, Lcom/android/settings/applications/RunningState$ProcessItem;->wl:J

    iput-wide v0, p0, Lcom/android/settings/applications/RunningState$MergedItem;->wl:J

    move v1, v2

    :goto_1
    iget-object v0, p0, Lcom/android/settings/applications/RunningState$MergedItem;->wG:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    iget-wide v4, p0, Lcom/android/settings/applications/RunningState$MergedItem;->wl:J

    iget-object v0, p0, Lcom/android/settings/applications/RunningState$MergedItem;->wG:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settings/applications/RunningState$ProcessItem;

    iget-wide v6, v0, Lcom/android/settings/applications/RunningState$ProcessItem;->wl:J

    add-long/2addr v4, v6

    iput-wide v4, p0, Lcom/android/settings/applications/RunningState$MergedItem;->wl:J

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    :cond_1
    iget-wide v0, p0, Lcom/android/settings/applications/RunningState$MergedItem;->wl:J

    invoke-static {p1, v0, v1}, Landroid/text/format/Formatter;->formatShortFileSize(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/applications/RunningState$MergedItem;->wc:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    iput-object v0, p0, Lcom/android/settings/applications/RunningState$MergedItem;->wc:Ljava/lang/String;

    return v2

    :cond_2
    return v2
.end method
