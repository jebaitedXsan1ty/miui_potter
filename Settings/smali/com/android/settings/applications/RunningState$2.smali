.class final Lcom/android/settings/applications/RunningState$2;
.super Landroid/os/Handler;
.source "RunningState.java"


# instance fields
.field GE:I

.field final synthetic GF:Lcom/android/settings/applications/RunningState;


# direct methods
.method constructor <init>(Lcom/android/settings/applications/RunningState;)V
    .locals 1

    iput-object p1, p0, Lcom/android/settings/applications/RunningState$2;->GF:Lcom/android/settings/applications/RunningState;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Lcom/android/settings/applications/RunningState$2;->GE:I

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 5

    const/4 v2, 0x4

    const/4 v4, 0x0

    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    :cond_0
    :goto_0
    return-void

    :pswitch_0
    iget v0, p1, Landroid/os/Message;->arg1:I

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    :goto_1
    iput v0, p0, Lcom/android/settings/applications/RunningState$2;->GE:I

    goto :goto_0

    :cond_1
    const/4 v0, 0x1

    goto :goto_1

    :pswitch_1
    iget-object v0, p0, Lcom/android/settings/applications/RunningState$2;->GF:Lcom/android/settings/applications/RunningState;

    iget-object v1, v0, Lcom/android/settings/applications/RunningState;->mLock:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/android/settings/applications/RunningState$2;->GF:Lcom/android/settings/applications/RunningState;

    iget-boolean v0, v0, Lcom/android/settings/applications/RunningState;->vh:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_2

    monitor-exit v1

    return-void

    :cond_2
    monitor-exit v1

    invoke-virtual {p0, v2}, Lcom/android/settings/applications/RunningState$2;->removeMessages(I)V

    invoke-virtual {p0, v2}, Lcom/android/settings/applications/RunningState$2;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    const-wide/16 v2, 0x3e8

    invoke-virtual {p0, v0, v2, v3}, Lcom/android/settings/applications/RunningState$2;->sendMessageDelayed(Landroid/os/Message;J)Z

    iget-object v0, p0, Lcom/android/settings/applications/RunningState$2;->GF:Lcom/android/settings/applications/RunningState;

    iget-object v0, v0, Lcom/android/settings/applications/RunningState;->vG:Lcom/android/settings/applications/RunningState$OnRefreshUiListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/applications/RunningState$2;->GF:Lcom/android/settings/applications/RunningState;

    iget-object v0, v0, Lcom/android/settings/applications/RunningState;->vG:Lcom/android/settings/applications/RunningState$OnRefreshUiListener;

    iget v1, p0, Lcom/android/settings/applications/RunningState$2;->GE:I

    invoke-interface {v0, v1}, Lcom/android/settings/applications/RunningState$OnRefreshUiListener;->qJ(I)V

    iput v4, p0, Lcom/android/settings/applications/RunningState$2;->GE:I

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    nop

    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
