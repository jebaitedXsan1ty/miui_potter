.class Lcom/android/settings/applications/PreferredSmsSettings$DisplayNameComparator;
.super Ljava/lang/Object;
.source "PreferredSmsSettings.java"

# interfaces
.implements Ljava/util/Comparator;


# instance fields
.field private mContext:Landroid/content/Context;

.field private final sp:Ljava/text/Collator;

.field final synthetic sq:Lcom/android/settings/applications/PreferredSmsSettings;


# direct methods
.method public constructor <init>(Lcom/android/settings/applications/PreferredSmsSettings;Landroid/content/Context;)V
    .locals 2

    iput-object p1, p0, Lcom/android/settings/applications/PreferredSmsSettings$DisplayNameComparator;->sq:Lcom/android/settings/applications/PreferredSmsSettings;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {}, Ljava/text/Collator;->getInstance()Ljava/text/Collator;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/applications/PreferredSmsSettings$DisplayNameComparator;->sp:Ljava/text/Collator;

    iput-object p2, p0, Lcom/android/settings/applications/PreferredSmsSettings$DisplayNameComparator;->mContext:Landroid/content/Context;

    iget-object v0, p0, Lcom/android/settings/applications/PreferredSmsSettings$DisplayNameComparator;->sp:Ljava/text/Collator;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/text/Collator;->setStrength(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1

    check-cast p1, Lcom/android/internal/telephony/SmsApplication$SmsApplicationData;

    check-cast p2, Lcom/android/internal/telephony/SmsApplication$SmsApplicationData;

    invoke-virtual {p0, p1, p2}, Lcom/android/settings/applications/PreferredSmsSettings$DisplayNameComparator;->rc(Lcom/android/internal/telephony/SmsApplication$SmsApplicationData;Lcom/android/internal/telephony/SmsApplication$SmsApplicationData;)I

    move-result v0

    return v0
.end method

.method public final rc(Lcom/android/internal/telephony/SmsApplication$SmsApplicationData;Lcom/android/internal/telephony/SmsApplication$SmsApplicationData;)I
    .locals 3

    iget-object v0, p0, Lcom/android/settings/applications/PreferredSmsSettings$DisplayNameComparator;->sp:Ljava/text/Collator;

    iget-object v1, p0, Lcom/android/settings/applications/PreferredSmsSettings$DisplayNameComparator;->mContext:Landroid/content/Context;

    invoke-virtual {p1, v1}, Lcom/android/internal/telephony/SmsApplication$SmsApplicationData;->getApplicationName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/android/settings/applications/PreferredSmsSettings$DisplayNameComparator;->mContext:Landroid/content/Context;

    invoke-virtual {p2, v2}, Lcom/android/internal/telephony/SmsApplication$SmsApplicationData;->getApplicationName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/text/Collator;->compare(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    return v0
.end method
