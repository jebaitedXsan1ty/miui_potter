.class public Lcom/android/settings/applications/AppStatePowerBridge;
.super Lcom/android/settings/applications/AppStateBaseBridge;
.source "AppStatePowerBridge.java"


# static fields
.field public static final oK:Lcom/android/settingslib/b/i;


# instance fields
.field private final oL:Lcom/android/settings/fuelgauge/PowerWhitelistBackend;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    new-instance v0, Lcom/android/settingslib/b/k;

    sget-object v1, Lcom/android/settingslib/b/a;->cAx:Lcom/android/settingslib/b/i;

    new-instance v2, Lcom/android/settings/applications/AppStatePowerBridge$1;

    invoke-direct {v2}, Lcom/android/settings/applications/AppStatePowerBridge$1;-><init>()V

    invoke-direct {v0, v1, v2}, Lcom/android/settingslib/b/k;-><init>(Lcom/android/settingslib/b/i;Lcom/android/settingslib/b/i;)V

    sput-object v0, Lcom/android/settings/applications/AppStatePowerBridge;->oK:Lcom/android/settingslib/b/i;

    return-void
.end method

.method public constructor <init>(Lcom/android/settingslib/b/a;Lcom/android/settings/applications/AppStateBaseBridge$Callback;)V
    .locals 1

    invoke-direct {p0, p1, p2}, Lcom/android/settings/applications/AppStateBaseBridge;-><init>(Lcom/android/settingslib/b/a;Lcom/android/settings/applications/AppStateBaseBridge$Callback;)V

    invoke-static {}, Lcom/android/settings/fuelgauge/PowerWhitelistBackend;->getInstance()Lcom/android/settings/fuelgauge/PowerWhitelistBackend;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/applications/AppStatePowerBridge;->oL:Lcom/android/settings/fuelgauge/PowerWhitelistBackend;

    return-void
.end method


# virtual methods
.method protected iO()V
    .locals 6

    iget-object v0, p0, Lcom/android/settings/applications/AppStatePowerBridge;->Ap:Lcom/android/settingslib/b/b;

    invoke-virtual {v0}, Lcom/android/settingslib/b/b;->cfs()Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v4

    const/4 v0, 0x0

    move v2, v0

    :goto_0
    if-ge v2, v4, :cond_1

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settingslib/b/h;

    iget-object v1, p0, Lcom/android/settings/applications/AppStatePowerBridge;->oL:Lcom/android/settings/fuelgauge/PowerWhitelistBackend;

    iget-object v5, v0, Lcom/android/settingslib/b/h;->info:Landroid/content/pm/ApplicationInfo;

    iget-object v5, v5, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v1, v5}, Lcom/android/settings/fuelgauge/PowerWhitelistBackend;->Kh(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    sget-object v1, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    :goto_1
    iput-object v1, v0, Lcom/android/settingslib/b/h;->cBd:Ljava/lang/Object;

    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_0
    sget-object v1, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    goto :goto_1

    :cond_1
    return-void
.end method

.method protected iP(Lcom/android/settingslib/b/h;Ljava/lang/String;I)V
    .locals 1

    iget-object v0, p0, Lcom/android/settings/applications/AppStatePowerBridge;->oL:Lcom/android/settings/fuelgauge/PowerWhitelistBackend;

    invoke-virtual {v0, p2}, Lcom/android/settings/fuelgauge/PowerWhitelistBackend;->Kh(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    :goto_0
    iput-object v0, p1, Lcom/android/settingslib/b/h;->cBd:Ljava/lang/Object;

    return-void

    :cond_0
    sget-object v0, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    goto :goto_0
.end method
