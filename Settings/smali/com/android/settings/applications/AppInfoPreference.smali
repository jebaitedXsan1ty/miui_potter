.class public Lcom/android/settings/applications/AppInfoPreference;
.super Landroid/preference/Preference;
.source "AppInfoPreference.java"


# instance fields
.field private yI:Lcom/android/settings/applications/AppInfoPreference$AppInfo;

.field private yJ:Landroid/widget/TextView;

.field private yK:Landroid/widget/ImageView;

.field private yL:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    invoke-direct {p0, p1}, Landroid/preference/Preference;-><init>(Landroid/content/Context;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Landroid/preference/Preference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Landroid/preference/Preference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method private updateUi()V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/applications/AppInfoPreference;->yI:Lcom/android/settings/applications/AppInfoPreference$AppInfo;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/settings/applications/AppInfoPreference;->yK:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/applications/AppInfoPreference;->yK:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/android/settings/applications/AppInfoPreference;->yI:Lcom/android/settings/applications/AppInfoPreference$AppInfo;

    iget-object v1, v1, Lcom/android/settings/applications/AppInfoPreference$AppInfo;->mIcon:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    :cond_0
    iget-object v0, p0, Lcom/android/settings/applications/AppInfoPreference;->yL:Landroid/widget/TextView;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/settings/applications/AppInfoPreference;->yL:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/android/settings/applications/AppInfoPreference;->yI:Lcom/android/settings/applications/AppInfoPreference$AppInfo;

    iget-object v1, v1, Lcom/android/settings/applications/AppInfoPreference$AppInfo;->yO:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_1
    iget-object v0, p0, Lcom/android/settings/applications/AppInfoPreference;->yJ:Landroid/widget/TextView;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/settings/applications/AppInfoPreference;->yI:Lcom/android/settings/applications/AppInfoPreference$AppInfo;

    iget-boolean v0, v0, Lcom/android/settings/applications/AppInfoPreference$AppInfo;->yN:Z

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/android/settings/applications/AppInfoPreference;->yJ:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/android/settings/applications/AppInfoPreference;->yI:Lcom/android/settings/applications/AppInfoPreference$AppInfo;

    iget-object v1, v1, Lcom/android/settings/applications/AppInfoPreference$AppInfo;->yM:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/android/settings/applications/AppInfoPreference;->yJ:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    :cond_2
    :goto_0
    return-void

    :cond_3
    iget-object v0, p0, Lcom/android/settings/applications/AppInfoPreference;->yJ:Landroid/widget/TextView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method


# virtual methods
.method protected onBindView(Landroid/view/View;)V
    .locals 1

    invoke-super {p0, p1}, Landroid/preference/Preference;->onBindView(Landroid/view/View;)V

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    invoke-direct {p0}, Lcom/android/settings/applications/AppInfoPreference;->updateUi()V

    return-void
.end method

.method protected onCreateView(Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    invoke-virtual {p0}, Lcom/android/settings/applications/AppInfoPreference;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0d00e8

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const v0, 0x7f0a0065

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/android/settings/applications/AppInfoPreference;->yK:Landroid/widget/ImageView;

    const v0, 0x7f0a0066

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/settings/applications/AppInfoPreference;->yL:Landroid/widget/TextView;

    const v0, 0x7f0a006b

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/settings/applications/AppInfoPreference;->yJ:Landroid/widget/TextView;

    return-object v1
.end method

.method public uH(Lcom/android/settings/applications/AppInfoPreference$AppInfo;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/applications/AppInfoPreference;->yI:Lcom/android/settings/applications/AppInfoPreference$AppInfo;

    invoke-direct {p0}, Lcom/android/settings/applications/AppInfoPreference;->updateUi()V

    return-void
.end method
