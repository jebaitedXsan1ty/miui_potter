.class Lcom/android/settings/applications/PermissionInfoFragment$PermissionSet;
.super Ljava/lang/Object;
.source "PermissionInfoFragment.java"


# instance fields
.field private Fi:Ljava/util/Map;

.field private Fj:Ljava/util/Map;

.field private Fk:Ljava/util/Map;


# direct methods
.method constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/TreeMap;

    invoke-direct {v0}, Ljava/util/TreeMap;-><init>()V

    iput-object v0, p0, Lcom/android/settings/applications/PermissionInfoFragment$PermissionSet;->Fj:Ljava/util/Map;

    new-instance v0, Ljava/util/TreeMap;

    invoke-direct {v0}, Ljava/util/TreeMap;-><init>()V

    iput-object v0, p0, Lcom/android/settings/applications/PermissionInfoFragment$PermissionSet;->Fk:Ljava/util/Map;

    new-instance v0, Ljava/util/TreeMap;

    invoke-direct {v0}, Ljava/util/TreeMap;-><init>()V

    iput-object v0, p0, Lcom/android/settings/applications/PermissionInfoFragment$PermissionSet;->Fi:Ljava/util/Map;

    return-void
.end method

.method static synthetic zA(Lcom/android/settings/applications/PermissionInfoFragment$PermissionSet;)Ljava/util/Map;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/applications/PermissionInfoFragment$PermissionSet;->Fj:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic zB(Lcom/android/settings/applications/PermissionInfoFragment$PermissionSet;)Ljava/util/Map;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/applications/PermissionInfoFragment$PermissionSet;->Fk:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic zz(Lcom/android/settings/applications/PermissionInfoFragment$PermissionSet;)Ljava/util/Map;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/applications/PermissionInfoFragment$PermissionSet;->Fi:Ljava/util/Map;

    return-object v0
.end method


# virtual methods
.method public zw()I
    .locals 1

    iget-object v0, p0, Lcom/android/settings/applications/PermissionInfoFragment$PermissionSet;->Fk:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    return v0
.end method

.method public zx()I
    .locals 1

    iget-object v0, p0, Lcom/android/settings/applications/PermissionInfoFragment$PermissionSet;->Fj:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    return v0
.end method

.method public zy()I
    .locals 1

    iget-object v0, p0, Lcom/android/settings/applications/PermissionInfoFragment$PermissionSet;->Fi:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    return v0
.end method
