.class public abstract Lcom/android/settings/applications/AppStateBaseBridge;
.super Ljava/lang/Object;
.source "AppStateBaseBridge.java"

# interfaces
.implements Lcom/android/settingslib/b/f;


# instance fields
.field protected final Ap:Lcom/android/settingslib/b/b;

.field protected final Aq:Lcom/android/settingslib/b/a;

.field protected final Ar:Lcom/android/settings/applications/AppStateBaseBridge$Callback;

.field protected final As:Lcom/android/settings/applications/AppStateBaseBridge$BackgroundHandler;

.field protected final At:Lcom/android/settings/applications/AppStateBaseBridge$MainHandler;


# direct methods
.method public constructor <init>(Lcom/android/settingslib/b/a;Lcom/android/settings/applications/AppStateBaseBridge$Callback;)V
    .locals 3

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/android/settings/applications/AppStateBaseBridge;->Aq:Lcom/android/settingslib/b/a;

    iget-object v0, p0, Lcom/android/settings/applications/AppStateBaseBridge;->Aq:Lcom/android/settingslib/b/a;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/applications/AppStateBaseBridge;->Aq:Lcom/android/settingslib/b/a;

    invoke-virtual {v0, p0}, Lcom/android/settingslib/b/a;->ceZ(Lcom/android/settingslib/b/f;)Lcom/android/settingslib/b/b;

    move-result-object v0

    :goto_0
    iput-object v0, p0, Lcom/android/settings/applications/AppStateBaseBridge;->Ap:Lcom/android/settingslib/b/b;

    iput-object p2, p0, Lcom/android/settings/applications/AppStateBaseBridge;->Ar:Lcom/android/settings/applications/AppStateBaseBridge$Callback;

    new-instance v2, Lcom/android/settings/applications/AppStateBaseBridge$BackgroundHandler;

    iget-object v0, p0, Lcom/android/settings/applications/AppStateBaseBridge;->Aq:Lcom/android/settingslib/b/a;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/settings/applications/AppStateBaseBridge;->Aq:Lcom/android/settingslib/b/a;

    invoke-virtual {v0}, Lcom/android/settingslib/b/a;->cfg()Landroid/os/Looper;

    move-result-object v0

    :goto_1
    invoke-direct {v2, p0, v0}, Lcom/android/settings/applications/AppStateBaseBridge$BackgroundHandler;-><init>(Lcom/android/settings/applications/AppStateBaseBridge;Landroid/os/Looper;)V

    iput-object v2, p0, Lcom/android/settings/applications/AppStateBaseBridge;->As:Lcom/android/settings/applications/AppStateBaseBridge$BackgroundHandler;

    new-instance v0, Lcom/android/settings/applications/AppStateBaseBridge$MainHandler;

    invoke-direct {v0, p0, v1}, Lcom/android/settings/applications/AppStateBaseBridge$MainHandler;-><init>(Lcom/android/settings/applications/AppStateBaseBridge;Lcom/android/settings/applications/AppStateBaseBridge$MainHandler;)V

    iput-object v0, p0, Lcom/android/settings/applications/AppStateBaseBridge;->At:Lcom/android/settings/applications/AppStateBaseBridge$MainHandler;

    return-void

    :cond_0
    move-object v0, v1

    goto :goto_0

    :cond_1
    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v0

    goto :goto_1
.end method


# virtual methods
.method protected abstract iO()V
.end method

.method protected abstract iP(Lcom/android/settingslib/b/h;Ljava/lang/String;I)V
.end method

.method public in()V
    .locals 0

    return-void
.end method

.method public ip()V
    .locals 0

    return-void
.end method

.method public iq()V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/applications/AppStateBaseBridge;->As:Lcom/android/settings/applications/AppStateBaseBridge$BackgroundHandler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/android/settings/applications/AppStateBaseBridge$BackgroundHandler;->sendEmptyMessage(I)Z

    return-void
.end method

.method public ir()V
    .locals 0

    return-void
.end method

.method public is()V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/applications/AppStateBaseBridge;->As:Lcom/android/settings/applications/AppStateBaseBridge$BackgroundHandler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/android/settings/applications/AppStateBaseBridge$BackgroundHandler;->sendEmptyMessage(I)Z

    return-void
.end method

.method public it(Ljava/lang/String;)V
    .locals 0

    return-void
.end method

.method public iu(Ljava/util/ArrayList;)V
    .locals 0

    return-void
.end method

.method public iv(Z)V
    .locals 0

    return-void
.end method

.method public pause()V
    .locals 1

    iget-object v0, p0, Lcom/android/settings/applications/AppStateBaseBridge;->Ap:Lcom/android/settingslib/b/b;

    invoke-virtual {v0}, Lcom/android/settingslib/b/b;->pause()V

    return-void
.end method

.method public vX()V
    .locals 1

    iget-object v0, p0, Lcom/android/settings/applications/AppStateBaseBridge;->Ap:Lcom/android/settingslib/b/b;

    invoke-virtual {v0}, Lcom/android/settingslib/b/b;->cfy()V

    return-void
.end method

.method public vY()V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/applications/AppStateBaseBridge;->As:Lcom/android/settings/applications/AppStateBaseBridge$BackgroundHandler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/android/settings/applications/AppStateBaseBridge$BackgroundHandler;->sendEmptyMessage(I)Z

    iget-object v0, p0, Lcom/android/settings/applications/AppStateBaseBridge;->Ap:Lcom/android/settingslib/b/b;

    invoke-virtual {v0}, Lcom/android/settingslib/b/b;->cfv()V

    return-void
.end method

.method public vZ(Ljava/lang/String;I)V
    .locals 3

    iget-object v0, p0, Lcom/android/settings/applications/AppStateBaseBridge;->As:Lcom/android/settings/applications/AppStateBaseBridge$BackgroundHandler;

    const/4 v1, 0x2

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p2, v2, p1}, Lcom/android/settings/applications/AppStateBaseBridge$BackgroundHandler;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    return-void
.end method
