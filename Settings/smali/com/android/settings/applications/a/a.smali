.class public Lcom/android/settings/applications/a/a;
.super Ljava/lang/Object;
.source "InstantAppButtonsController.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field private final mContext:Landroid/content/Context;

.field private mPackageName:Ljava/lang/String;

.field private final oF:Landroid/app/Fragment;

.field private final oG:Lcom/android/settings/applications/PackageManagerWrapper;

.field private final oH:Lcom/android/settings/applications/a/b;

.field private final oI:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/app/Fragment;Landroid/view/View;Lcom/android/settings/applications/a/b;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/android/settings/applications/a/a;->mContext:Landroid/content/Context;

    iput-object p2, p0, Lcom/android/settings/applications/a/a;->oF:Landroid/app/Fragment;

    iput-object p3, p0, Lcom/android/settings/applications/a/a;->oI:Landroid/view/View;

    iput-object p4, p0, Lcom/android/settings/applications/a/a;->oH:Lcom/android/settings/applications/a/b;

    new-instance v0, Lcom/android/settings/applications/PackageManagerWrapperImpl;

    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/android/settings/applications/PackageManagerWrapperImpl;-><init>(Landroid/content/pm/PackageManager;)V

    iput-object v0, p0, Lcom/android/settings/applications/a/a;->oG:Lcom/android/settings/applications/PackageManagerWrapper;

    return-void
.end method


# virtual methods
.method public nY()V
    .locals 4

    iget-object v0, p0, Lcom/android/settings/applications/a/a;->oI:Landroid/view/View;

    const v1, 0x7f0a0213

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iget-object v1, p0, Lcom/android/settings/applications/a/a;->oI:Landroid/view/View;

    const v2, 0x7f0a00e1

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iget-object v2, p0, Lcom/android/settings/applications/a/a;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/android/settings/applications/a/a;->mPackageName:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/android/settings/applications/AppStoreUtil;->vS(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v2

    if-eqz v2, :cond_0

    const/4 v3, 0x1

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setEnabled(Z)V

    new-instance v3, Lcom/android/settings/applications/a/-$Lambda$VbGANMtwxywLifktBuk1wI2FT_s$1;

    invoke-direct {v3, p0, v2}, Lcom/android/settings/applications/a/-$Lambda$VbGANMtwxywLifktBuk1wI2FT_s$1;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_0
    new-instance v0, Lcom/android/settings/applications/a/-$Lambda$VbGANMtwxywLifktBuk1wI2FT_s;

    invoke-direct {v0, p0}, Lcom/android/settings/applications/a/-$Lambda$VbGANMtwxywLifktBuk1wI2FT_s;-><init>(Ljava/lang/Object;)V

    invoke-virtual {v1, v0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public nZ(I)Landroid/app/AlertDialog;
    .locals 4

    const/4 v3, 0x0

    const v2, 0x7f120416

    const/16 v0, 0x5033

    if-ne p1, v0, :cond_0

    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Lcom/android/settings/applications/a/a;->oF:Landroid/app/Fragment;

    invoke-virtual {v1}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v2, p0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f1203c7

    invoke-virtual {v0, v1, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/applications/a/a;->mContext:Landroid/content/Context;

    const v2, 0x7f120415

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    return-object v0

    :cond_0
    return-object v3
.end method

.method public oa(Ljava/lang/String;)Lcom/android/settings/applications/a/a;
    .locals 0

    iput-object p1, p0, Lcom/android/settings/applications/a/a;->mPackageName:Ljava/lang/String;

    return-object p0
.end method

.method synthetic ob(Landroid/content/Intent;Landroid/view/View;)V
    .locals 1

    iget-object v0, p0, Lcom/android/settings/applications/a/a;->oF:Landroid/app/Fragment;

    invoke-virtual {v0, p1}, Landroid/app/Fragment;->startActivity(Landroid/content/Intent;)V

    return-void
.end method

.method synthetic oc(Landroid/view/View;)V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/applications/a/a;->oH:Lcom/android/settings/applications/a/b;

    const/16 v1, 0x5033

    invoke-interface {v0, v1}, Lcom/android/settings/applications/a/b;->od(I)V

    return-void
.end method

.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 6

    const/4 v5, 0x0

    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/applications/a/a;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/settings/overlay/a;->aIk(Landroid/content/Context;)Lcom/android/settings/overlay/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/settings/overlay/a;->aIm()Lcom/android/settings/core/instrumentation/e;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/applications/a/a;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/android/settings/applications/a/a;->mPackageName:Ljava/lang/String;

    new-array v3, v5, [Landroid/util/Pair;

    const/16 v4, 0x39b

    invoke-virtual {v0, v1, v4, v2, v3}, Lcom/android/settings/core/instrumentation/e;->ajQ(Landroid/content/Context;ILjava/lang/String;[Landroid/util/Pair;)V

    iget-object v0, p0, Lcom/android/settings/applications/a/a;->oG:Lcom/android/settings/applications/PackageManagerWrapper;

    iget-object v1, p0, Lcom/android/settings/applications/a/a;->mPackageName:Ljava/lang/String;

    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v2

    const/4 v3, 0x0

    invoke-interface {v0, v1, v3, v5, v2}, Lcom/android/settings/applications/PackageManagerWrapper;->uQ(Ljava/lang/String;Landroid/content/pm/IPackageDeleteObserver;II)V

    :cond_0
    return-void
.end method

.method public show()Lcom/android/settings/applications/a/a;
    .locals 2

    invoke-virtual {p0}, Lcom/android/settings/applications/a/a;->nY()V

    iget-object v0, p0, Lcom/android/settings/applications/a/a;->oI:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    return-object p0
.end method
