.class Lcom/android/settings/applications/assist/l;
.super Lcom/android/settings/applications/assist/f;
.source "AssistContextPreferenceController.java"


# instance fields
.field private final nV:Landroid/net/Uri;

.field final synthetic nW:Lcom/android/settings/applications/assist/k;


# direct methods
.method constructor <init>(Lcom/android/settings/applications/assist/k;)V
    .locals 1

    iput-object p1, p0, Lcom/android/settings/applications/assist/l;->nW:Lcom/android/settings/applications/assist/k;

    invoke-direct {p0}, Lcom/android/settings/applications/assist/f;-><init>()V

    const-string/jumbo v0, "assist_structure_enabled"

    invoke-static {v0}, Landroid/provider/Settings$Secure;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/applications/assist/l;->nV:Landroid/net/Uri;

    return-void
.end method


# virtual methods
.method protected mM()Ljava/util/List;
    .locals 3

    const/4 v0, 0x1

    new-array v0, v0, [Landroid/net/Uri;

    iget-object v1, p0, Lcom/android/settings/applications/assist/l;->nV:Landroid/net/Uri;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public mN()V
    .locals 1

    iget-object v0, p0, Lcom/android/settings/applications/assist/l;->nW:Lcom/android/settings/applications/assist/k;

    invoke-static {v0}, Lcom/android/settings/applications/assist/k;->nb(Lcom/android/settings/applications/assist/k;)V

    return-void
.end method
