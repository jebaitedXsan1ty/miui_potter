.class public Lcom/android/settings/applications/assist/ManageAssist;
.super Lcom/android/settings/dashboard/MiuiDashboardFragment;
.source "ManageAssist.java"


# static fields
.field public static final SEARCH_INDEX_DATA_PROVIDER:Lcom/android/settings/search/Indexable$SearchIndexProvider;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/android/settings/applications/assist/q;

    invoke-direct {v0}, Lcom/android/settings/applications/assist/q;-><init>()V

    sput-object v0, Lcom/android/settings/applications/assist/ManageAssist;->SEARCH_INDEX_DATA_PROVIDER:Lcom/android/settings/search/Indexable$SearchIndexProvider;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/dashboard/MiuiDashboardFragment;-><init>()V

    return-void
.end method

.method private static mt(Landroid/content/Context;Lcom/android/settings/core/lifecycle/c;)Ljava/util/List;
    .locals 3

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    new-instance v1, Lcom/android/settings/applications/assist/a;

    invoke-direct {v1, p0}, Lcom/android/settings/applications/assist/a;-><init>(Landroid/content/Context;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v1, Lcom/android/settings/gestures/d;

    const-string/jumbo v2, "gesture_assist_application"

    invoke-direct {v1, p0, p1, v2}, Lcom/android/settings/gestures/d;-><init>(Landroid/content/Context;Lcom/android/settings/core/lifecycle/c;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v1, Lcom/android/settings/applications/assist/k;

    invoke-direct {v1, p0, p1}, Lcom/android/settings/applications/assist/k;-><init>(Landroid/content/Context;Lcom/android/settings/core/lifecycle/c;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v1, Lcom/android/settings/applications/assist/i;

    invoke-direct {v1, p0, p1}, Lcom/android/settings/applications/assist/i;-><init>(Landroid/content/Context;Lcom/android/settings/core/lifecycle/c;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v1, Lcom/android/settings/applications/assist/g;

    invoke-direct {v1, p0, p1}, Lcom/android/settings/applications/assist/g;-><init>(Landroid/content/Context;Lcom/android/settings/core/lifecycle/c;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v1, Lcom/android/settings/applications/assist/c;

    invoke-direct {v1, p0, p1}, Lcom/android/settings/applications/assist/c;-><init>(Landroid/content/Context;Lcom/android/settings/core/lifecycle/c;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object v0
.end method

.method static synthetic mu(Landroid/content/Context;Lcom/android/settings/core/lifecycle/c;)Ljava/util/List;
    .locals 1

    invoke-static {p0, p1}, Lcom/android/settings/applications/assist/ManageAssist;->mt(Landroid/content/Context;Lcom/android/settings/core/lifecycle/c;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method protected ar()Ljava/lang/String;
    .locals 1

    const-string/jumbo v0, "ManageAssist"

    return-object v0
.end method

.method protected as()I
    .locals 1

    const v0, 0x7f15007e

    return v0
.end method

.method public getMetricsCategory()I
    .locals 1

    const/16 v0, 0xc9

    return v0
.end method

.method protected getPreferenceControllers(Landroid/content/Context;)Ljava/util/List;
    .locals 1

    invoke-virtual {p0}, Lcom/android/settings/applications/assist/ManageAssist;->getLifecycle()Lcom/android/settings/core/lifecycle/c;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/android/settings/applications/assist/ManageAssist;->mt(Landroid/content/Context;Lcom/android/settings/core/lifecycle/c;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public onResume()V
    .locals 0

    invoke-super {p0}, Lcom/android/settings/dashboard/MiuiDashboardFragment;->onResume()V

    return-void
.end method
