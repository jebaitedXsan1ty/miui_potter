.class public Lcom/android/settings/applications/assist/g;
.super Lcom/android/settings/core/e;
.source "AssistFlashScreenPreferenceController.java"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceChangeListener;
.implements Lcom/android/settings/core/lifecycle/b;
.implements Lcom/android/settings/core/lifecycle/a/b;
.implements Lcom/android/settings/core/lifecycle/a/d;


# instance fields
.field private final nD:Lcom/android/internal/app/AssistUtils;

.field private nE:Landroid/preference/Preference;

.field private nF:Landroid/preference/PreferenceScreen;

.field private final nG:Lcom/android/settings/applications/assist/h;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/android/settings/core/lifecycle/c;)V
    .locals 1

    invoke-direct {p0, p1}, Lcom/android/settings/core/e;-><init>(Landroid/content/Context;)V

    new-instance v0, Lcom/android/internal/app/AssistUtils;

    invoke-direct {v0, p1}, Lcom/android/internal/app/AssistUtils;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/settings/applications/assist/g;->nD:Lcom/android/internal/app/AssistUtils;

    new-instance v0, Lcom/android/settings/applications/assist/h;

    invoke-direct {v0, p0}, Lcom/android/settings/applications/assist/h;-><init>(Lcom/android/settings/applications/assist/g;)V

    iput-object v0, p0, Lcom/android/settings/applications/assist/g;->nG:Lcom/android/settings/applications/assist/h;

    if-eqz p2, :cond_0

    invoke-virtual {p2, p0}, Lcom/android/settings/core/lifecycle/c;->ajv(Lcom/android/settings/core/lifecycle/b;)Lcom/android/settings/core/lifecycle/b;

    :cond_0
    return-void
.end method

.method private mU()Landroid/content/ComponentName;
    .locals 2

    iget-object v0, p0, Lcom/android/settings/applications/assist/g;->nD:Lcom/android/internal/app/AssistUtils;

    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/android/internal/app/AssistUtils;->getAssistComponentForUser(I)Landroid/content/ComponentName;

    move-result-object v0

    return-object v0
.end method

.method private mV()V
    .locals 3

    iget-object v0, p0, Lcom/android/settings/applications/assist/g;->nE:Landroid/preference/Preference;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/applications/assist/g;->nE:Landroid/preference/Preference;

    instance-of v0, v0, Landroid/preference/TwoStatePreference;

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_1

    :cond_0
    return-void

    :cond_1
    invoke-virtual {p0}, Lcom/android/settings/applications/assist/g;->p()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/android/settings/applications/assist/g;->nF:Landroid/preference/PreferenceScreen;

    invoke-virtual {p0}, Lcom/android/settings/applications/assist/g;->l()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/android/settings/applications/assist/g;->nF:Landroid/preference/PreferenceScreen;

    iget-object v1, p0, Lcom/android/settings/applications/assist/g;->nE:Landroid/preference/Preference;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    :cond_2
    :goto_0
    invoke-direct {p0}, Lcom/android/settings/applications/assist/g;->mU()Landroid/content/ComponentName;

    move-result-object v1

    iget-object v0, p0, Lcom/android/settings/applications/assist/g;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/settings/applications/assist/k;->mZ(Landroid/content/Context;)Z

    move-result v0

    iget-object v2, p0, Lcom/android/settings/applications/assist/g;->nE:Landroid/preference/Preference;

    if-eqz v0, :cond_4

    invoke-virtual {p0, v1}, Lcom/android/settings/applications/assist/g;->isPreInstalledAssistant(Landroid/content/ComponentName;)Z

    move-result v0

    :goto_1
    invoke-virtual {v2, v0}, Landroid/preference/Preference;->setEnabled(Z)V

    iget-object v0, p0, Lcom/android/settings/applications/assist/g;->nE:Landroid/preference/Preference;

    check-cast v0, Landroid/preference/TwoStatePreference;

    invoke-virtual {p0, v1}, Lcom/android/settings/applications/assist/g;->willShowFlash(Landroid/content/ComponentName;)Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/preference/TwoStatePreference;->setChecked(Z)V

    return-void

    :cond_3
    iget-object v0, p0, Lcom/android/settings/applications/assist/g;->nF:Landroid/preference/PreferenceScreen;

    iget-object v1, p0, Lcom/android/settings/applications/assist/g;->nE:Landroid/preference/Preference;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    goto :goto_0

    :cond_4
    const/4 v0, 0x0

    goto :goto_1
.end method

.method static synthetic mW(Lcom/android/settings/applications/assist/g;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/applications/assist/g;->mV()V

    return-void
.end method


# virtual methods
.method allowDisablingAssistDisclosure()Z
    .locals 1

    iget-object v0, p0, Lcom/android/settings/applications/assist/g;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/internal/app/AssistUtils;->allowDisablingAssistDisclosure(Landroid/content/Context;)Z

    move-result v0

    return v0
.end method

.method public cz(Landroid/preference/Preference;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/applications/assist/g;->mV()V

    return-void
.end method

.method public i(Landroid/preference/PreferenceScreen;)V
    .locals 1

    iput-object p1, p0, Lcom/android/settings/applications/assist/g;->nF:Landroid/preference/PreferenceScreen;

    invoke-virtual {p0}, Lcom/android/settings/applications/assist/g;->l()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/applications/assist/g;->nE:Landroid/preference/Preference;

    invoke-super {p0, p1}, Lcom/android/settings/core/e;->i(Landroid/preference/PreferenceScreen;)V

    return-void
.end method

.method isPreInstalledAssistant(Landroid/content/ComponentName;)Z
    .locals 1

    iget-object v0, p0, Lcom/android/settings/applications/assist/g;->mContext:Landroid/content/Context;

    invoke-static {v0, p1}, Lcom/android/internal/app/AssistUtils;->isPreinstalledAssistant(Landroid/content/Context;Landroid/content/ComponentName;)Z

    move-result v0

    return v0
.end method

.method public l()Ljava/lang/String;
    .locals 1

    const-string/jumbo v0, "flash"

    return-object v0
.end method

.method public onPause()V
    .locals 3

    iget-object v0, p0, Lcom/android/settings/applications/assist/g;->nG:Lcom/android/settings/applications/assist/h;

    iget-object v1, p0, Lcom/android/settings/applications/assist/g;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/android/settings/applications/assist/h;->mT(Landroid/content/ContentResolver;Z)V

    return-void
.end method

.method public onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 4

    const/4 v1, 0x1

    iget-object v0, p0, Lcom/android/settings/applications/assist/g;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string/jumbo v3, "assist_disclosure_enabled"

    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v2, v3, v0}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    return v1

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onResume()V
    .locals 3

    iget-object v0, p0, Lcom/android/settings/applications/assist/g;->nG:Lcom/android/settings/applications/assist/h;

    iget-object v1, p0, Lcom/android/settings/applications/assist/g;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/android/settings/applications/assist/h;->mT(Landroid/content/ContentResolver;Z)V

    invoke-direct {p0}, Lcom/android/settings/applications/assist/g;->mV()V

    return-void
.end method

.method public p()Z
    .locals 1

    invoke-direct {p0}, Lcom/android/settings/applications/assist/g;->mU()Landroid/content/ComponentName;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/android/settings/applications/assist/g;->allowDisablingAssistDisclosure()Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method willShowFlash(Landroid/content/ComponentName;)Z
    .locals 1

    iget-object v0, p0, Lcom/android/settings/applications/assist/g;->mContext:Landroid/content/Context;

    invoke-static {v0, p1}, Lcom/android/internal/app/AssistUtils;->shouldDisclose(Landroid/content/Context;Landroid/content/ComponentName;)Z

    move-result v0

    return v0
.end method
