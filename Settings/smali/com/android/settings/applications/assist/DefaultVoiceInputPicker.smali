.class public Lcom/android/settings/applications/assist/DefaultVoiceInputPicker;
.super Lcom/android/settings/applications/defaultapps/DefaultAppPickerFragment;
.source "DefaultVoiceInputPicker.java"


# instance fields
.field private nA:Lcom/android/settings/applications/assist/m;

.field private ny:Ljava/lang/String;

.field private nz:Lcom/android/internal/app/AssistUtils;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/applications/defaultapps/DefaultAppPickerFragment;-><init>()V

    return-void
.end method

.method public static mO(Lcom/android/settings/applications/assist/m;)Landroid/content/ComponentName;
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/android/settings/applications/assist/m;->nZ:Landroid/content/ComponentName;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/applications/assist/m;->nZ:Landroid/content/ComponentName;

    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/android/settings/applications/assist/m;->oa:Landroid/content/ComponentName;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/settings/applications/assist/m;->oa:Landroid/content/ComponentName;

    return-object v0

    :cond_1
    return-object v1
.end method

.method public static mP(Landroid/content/ComponentName;Landroid/content/ComponentName;)Z
    .locals 1

    if-nez p0, :cond_0

    if-nez p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    if-eqz p0, :cond_1

    invoke-virtual {p0, p1}, Landroid/content/ComponentName;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private mQ()Landroid/content/ComponentName;
    .locals 2

    iget-object v0, p0, Lcom/android/settings/applications/assist/DefaultVoiceInputPicker;->nz:Lcom/android/internal/app/AssistUtils;

    iget v1, p0, Lcom/android/settings/applications/assist/DefaultVoiceInputPicker;->mUserId:I

    invoke-virtual {v0, v1}, Lcom/android/internal/app/AssistUtils;->getAssistComponentForUser(I)Landroid/content/ComponentName;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getMetricsCategory()I
    .locals 1

    const/16 v0, 0x34c

    return v0
.end method

.method protected mB()Ljava/lang/String;
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/android/settings/applications/assist/DefaultVoiceInputPicker;->nA:Lcom/android/settings/applications/assist/m;

    invoke-static {v0}, Lcom/android/settings/applications/assist/DefaultVoiceInputPicker;->mO(Lcom/android/settings/applications/assist/m;)Landroid/content/ComponentName;

    move-result-object v0

    if-nez v0, :cond_0

    return-object v1

    :cond_0
    invoke-virtual {v0}, Landroid/content/ComponentName;->flattenToShortString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected mG(Ljava/lang/String;)Z
    .locals 6

    const/4 v5, 0x1

    iget-object v0, p0, Lcom/android/settings/applications/assist/DefaultVoiceInputPicker;->nA:Lcom/android/settings/applications/assist/m;

    iget-object v0, v0, Lcom/android/settings/applications/assist/m;->nX:Ljava/util/ArrayList;

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settings/applications/assist/o;

    iget-object v2, v0, Lcom/android/settings/applications/assist/o;->key:Ljava/lang/String;

    invoke-static {p1, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {p0}, Lcom/android/settings/applications/assist/DefaultVoiceInputPicker;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string/jumbo v2, "voice_interaction_service"

    invoke-static {v1, v2, p1}, Landroid/provider/Settings$Secure;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    invoke-virtual {p0}, Lcom/android/settings/applications/assist/DefaultVoiceInputPicker;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string/jumbo v2, "voice_recognition_service"

    new-instance v3, Landroid/content/ComponentName;

    iget-object v4, v0, Lcom/android/settings/applications/assist/o;->oh:Landroid/content/pm/ServiceInfo;

    iget-object v4, v4, Landroid/content/pm/ServiceInfo;->packageName:Ljava/lang/String;

    iget-object v0, v0, Lcom/android/settings/applications/assist/o;->oj:Landroid/service/voice/VoiceInteractionServiceInfo;

    invoke-virtual {v0}, Landroid/service/voice/VoiceInteractionServiceInfo;->getRecognitionService()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v3, v4, v0}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v3}, Landroid/content/ComponentName;->flattenToShortString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v2, v0}, Landroid/provider/Settings$Secure;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    return v5

    :cond_1
    iget-object v0, p0, Lcom/android/settings/applications/assist/DefaultVoiceInputPicker;->nA:Lcom/android/settings/applications/assist/m;

    iget-object v0, v0, Lcom/android/settings/applications/assist/m;->nY:Ljava/util/ArrayList;

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settings/applications/assist/p;

    iget-object v0, v0, Lcom/android/settings/applications/assist/p;->key:Ljava/lang/String;

    invoke-static {p1, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lcom/android/settings/applications/assist/DefaultVoiceInputPicker;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v1, "voice_interaction_service"

    const-string/jumbo v2, ""

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$Secure;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    invoke-virtual {p0}, Lcom/android/settings/applications/assist/DefaultVoiceInputPicker;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v1, "voice_recognition_service"

    invoke-static {v0, v1, p1}, Landroid/provider/Settings$Secure;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    return v5

    :cond_3
    return v5
.end method

.method protected my()Ljava/util/List;
    .locals 8

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    const/4 v0, 0x1

    iget-object v1, p0, Lcom/android/settings/applications/assist/DefaultVoiceInputPicker;->nA:Lcom/android/settings/applications/assist/m;

    iget-object v1, v1, Lcom/android/settings/applications/assist/m;->nX:Ljava/util/ArrayList;

    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v1, v0

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settings/applications/assist/o;

    iget-object v4, v0, Lcom/android/settings/applications/assist/o;->key:Ljava/lang/String;

    iget-object v5, p0, Lcom/android/settings/applications/assist/DefaultVoiceInputPicker;->ny:Ljava/lang/String;

    invoke-static {v4, v5}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v4

    or-int/2addr v1, v4

    new-instance v5, Lcom/android/settings/applications/assist/e;

    iget-object v6, p0, Lcom/android/settings/applications/assist/DefaultVoiceInputPicker;->oq:Lcom/android/settings/applications/PackageManagerWrapper;

    iget v7, p0, Lcom/android/settings/applications/assist/DefaultVoiceInputPicker;->mUserId:I

    invoke-direct {v5, v6, v7, v0, v4}, Lcom/android/settings/applications/assist/e;-><init>(Lcom/android/settings/applications/PackageManagerWrapper;ILcom/android/settings/applications/assist/n;Z)V

    invoke-interface {v2, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    xor-int/lit8 v1, v1, 0x1

    iget-object v0, p0, Lcom/android/settings/applications/assist/DefaultVoiceInputPicker;->nA:Lcom/android/settings/applications/assist/m;

    iget-object v0, v0, Lcom/android/settings/applications/assist/m;->nY:Ljava/util/ArrayList;

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settings/applications/assist/p;

    xor-int/lit8 v4, v1, 0x1

    new-instance v5, Lcom/android/settings/applications/assist/e;

    iget-object v6, p0, Lcom/android/settings/applications/assist/DefaultVoiceInputPicker;->oq:Lcom/android/settings/applications/PackageManagerWrapper;

    iget v7, p0, Lcom/android/settings/applications/assist/DefaultVoiceInputPicker;->mUserId:I

    invoke-direct {v5, v6, v7, v0, v4}, Lcom/android/settings/applications/assist/e;-><init>(Lcom/android/settings/applications/PackageManagerWrapper;ILcom/android/settings/applications/assist/n;Z)V

    invoke-interface {v2, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_1
    return-object v2
.end method

.method public onAttach(Landroid/content/Context;)V
    .locals 2

    invoke-super {p0, p1}, Lcom/android/settings/applications/defaultapps/DefaultAppPickerFragment;->onAttach(Landroid/content/Context;)V

    new-instance v0, Lcom/android/internal/app/AssistUtils;

    invoke-direct {v0, p1}, Lcom/android/internal/app/AssistUtils;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/settings/applications/assist/DefaultVoiceInputPicker;->nz:Lcom/android/internal/app/AssistUtils;

    new-instance v0, Lcom/android/settings/applications/assist/m;

    invoke-direct {v0, p1}, Lcom/android/settings/applications/assist/m;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/settings/applications/assist/DefaultVoiceInputPicker;->nA:Lcom/android/settings/applications/assist/m;

    iget-object v0, p0, Lcom/android/settings/applications/assist/DefaultVoiceInputPicker;->nA:Lcom/android/settings/applications/assist/m;

    invoke-virtual {v0}, Lcom/android/settings/applications/assist/m;->nc()V

    invoke-direct {p0}, Lcom/android/settings/applications/assist/DefaultVoiceInputPicker;->mQ()Landroid/content/ComponentName;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/applications/assist/DefaultVoiceInputPicker;->nA:Lcom/android/settings/applications/assist/m;

    invoke-static {v1}, Lcom/android/settings/applications/assist/DefaultVoiceInputPicker;->mO(Lcom/android/settings/applications/assist/m;)Landroid/content/ComponentName;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/settings/applications/assist/DefaultVoiceInputPicker;->mP(Landroid/content/ComponentName;Landroid/content/ComponentName;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Landroid/content/ComponentName;->flattenToShortString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/applications/assist/DefaultVoiceInputPicker;->ny:Ljava/lang/String;

    :cond_0
    return-void
.end method
