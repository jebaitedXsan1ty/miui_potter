.class public Lcom/android/settings/applications/AppStorageSizesController;
.super Ljava/lang/Object;
.source "AppStorageSizesController.java"


# instance fields
.field private final wP:Landroid/preference/Preference;

.field private final wQ:Landroid/preference/Preference;

.field private wR:Z

.field private final wS:I

.field private wT:Z

.field private final wU:Landroid/preference/Preference;

.field private final wV:I

.field private wW:J

.field private wX:J

.field private wY:J

.field private wZ:Lcom/android/settingslib/b/J;

.field private xa:Z

.field private xb:J

.field private final xc:Landroid/preference/Preference;


# direct methods
.method private constructor <init>(Landroid/preference/Preference;Landroid/preference/Preference;Landroid/preference/Preference;Landroid/preference/Preference;II)V
    .locals 2

    const-wide/16 v0, -0x1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-wide v0, p0, Lcom/android/settings/applications/AppStorageSizesController;->wX:J

    iput-wide v0, p0, Lcom/android/settings/applications/AppStorageSizesController;->wY:J

    iput-wide v0, p0, Lcom/android/settings/applications/AppStorageSizesController;->wW:J

    iput-wide v0, p0, Lcom/android/settings/applications/AppStorageSizesController;->xb:J

    iput-object p1, p0, Lcom/android/settings/applications/AppStorageSizesController;->xc:Landroid/preference/Preference;

    iput-object p2, p0, Lcom/android/settings/applications/AppStorageSizesController;->wP:Landroid/preference/Preference;

    iput-object p3, p0, Lcom/android/settings/applications/AppStorageSizesController;->wU:Landroid/preference/Preference;

    iput-object p4, p0, Lcom/android/settings/applications/AppStorageSizesController;->wQ:Landroid/preference/Preference;

    iput p5, p0, Lcom/android/settings/applications/AppStorageSizesController;->wS:I

    iput p6, p0, Lcom/android/settings/applications/AppStorageSizesController;->wV:I

    return-void
.end method

.method synthetic constructor <init>(Landroid/preference/Preference;Landroid/preference/Preference;Landroid/preference/Preference;Landroid/preference/Preference;IILcom/android/settings/applications/AppStorageSizesController;)V
    .locals 0

    invoke-direct/range {p0 .. p6}, Lcom/android/settings/applications/AppStorageSizesController;-><init>(Landroid/preference/Preference;Landroid/preference/Preference;Landroid/preference/Preference;Landroid/preference/Preference;II)V

    return-void
.end method

.method private sY(Landroid/content/Context;J)Ljava/lang/String;
    .locals 2

    invoke-static {p1, p2, p3}, Landroid/text/format/Formatter;->formatFileSize(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public sX()Lcom/android/settingslib/b/J;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/applications/AppStorageSizesController;->wZ:Lcom/android/settingslib/b/J;

    return-object v0
.end method

.method public sZ(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/android/settings/applications/AppStorageSizesController;->wR:Z

    return-void
.end method

.method public ta(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/android/settings/applications/AppStorageSizesController;->wT:Z

    return-void
.end method

.method public tb(Lcom/android/settingslib/b/J;)V
    .locals 1

    iput-object p1, p0, Lcom/android/settings/applications/AppStorageSizesController;->wZ:Lcom/android/settingslib/b/J;

    if-nez p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/android/settings/applications/AppStorageSizesController;->xa:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public tc(Landroid/content/Context;)V
    .locals 8

    const-wide/16 v2, 0x0

    iget-object v0, p0, Lcom/android/settings/applications/AppStorageSizesController;->wZ:Lcom/android/settingslib/b/J;

    if-nez v0, :cond_2

    iget-boolean v0, p0, Lcom/android/settings/applications/AppStorageSizesController;->xa:Z

    if-eqz v0, :cond_1

    iget v0, p0, Lcom/android/settings/applications/AppStorageSizesController;->wV:I

    :goto_0
    iget-object v1, p0, Lcom/android/settings/applications/AppStorageSizesController;->wP:Landroid/preference/Preference;

    invoke-virtual {v1, v0}, Landroid/preference/Preference;->setSummary(I)V

    iget-object v1, p0, Lcom/android/settings/applications/AppStorageSizesController;->wU:Landroid/preference/Preference;

    invoke-virtual {v1, v0}, Landroid/preference/Preference;->setSummary(I)V

    iget-object v1, p0, Lcom/android/settings/applications/AppStorageSizesController;->wQ:Landroid/preference/Preference;

    invoke-virtual {v1, v0}, Landroid/preference/Preference;->setSummary(I)V

    iget-object v1, p0, Lcom/android/settings/applications/AppStorageSizesController;->xc:Landroid/preference/Preference;

    invoke-virtual {v1, v0}, Landroid/preference/Preference;->setSummary(I)V

    :cond_0
    :goto_1
    return-void

    :cond_1
    iget v0, p0, Lcom/android/settings/applications/AppStorageSizesController;->wS:I

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/android/settings/applications/AppStorageSizesController;->wZ:Lcom/android/settingslib/b/J;

    invoke-interface {v0}, Lcom/android/settingslib/b/J;->cfS()J

    move-result-wide v4

    iget-boolean v0, p0, Lcom/android/settings/applications/AppStorageSizesController;->wT:Z

    if-eqz v0, :cond_7

    move-wide v0, v2

    :goto_2
    iget-wide v6, p0, Lcom/android/settings/applications/AppStorageSizesController;->wX:J

    cmp-long v6, v6, v4

    if-eqz v6, :cond_3

    iput-wide v4, p0, Lcom/android/settings/applications/AppStorageSizesController;->wX:J

    iget-object v6, p0, Lcom/android/settings/applications/AppStorageSizesController;->wP:Landroid/preference/Preference;

    invoke-direct {p0, p1, v4, v5}, Lcom/android/settings/applications/AppStorageSizesController;->sY(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    :cond_3
    iget-wide v6, p0, Lcom/android/settings/applications/AppStorageSizesController;->wY:J

    cmp-long v6, v6, v0

    if-eqz v6, :cond_4

    iput-wide v0, p0, Lcom/android/settings/applications/AppStorageSizesController;->wY:J

    iget-object v6, p0, Lcom/android/settings/applications/AppStorageSizesController;->wU:Landroid/preference/Preference;

    invoke-direct {p0, p1, v0, v1}, Lcom/android/settings/applications/AppStorageSizesController;->sY(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    :cond_4
    iget-boolean v6, p0, Lcom/android/settings/applications/AppStorageSizesController;->wT:Z

    if-nez v6, :cond_5

    iget-boolean v6, p0, Lcom/android/settings/applications/AppStorageSizesController;->wR:Z

    if-eqz v6, :cond_8

    :cond_5
    :goto_3
    iget-wide v6, p0, Lcom/android/settings/applications/AppStorageSizesController;->wW:J

    cmp-long v6, v6, v2

    if-eqz v6, :cond_6

    iput-wide v2, p0, Lcom/android/settings/applications/AppStorageSizesController;->wW:J

    iget-object v6, p0, Lcom/android/settings/applications/AppStorageSizesController;->wQ:Landroid/preference/Preference;

    invoke-direct {p0, p1, v2, v3}, Lcom/android/settings/applications/AppStorageSizesController;->sY(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    :cond_6
    add-long/2addr v0, v4

    add-long/2addr v0, v2

    iget-wide v2, p0, Lcom/android/settings/applications/AppStorageSizesController;->xb:J

    cmp-long v2, v2, v0

    if-eqz v2, :cond_0

    iput-wide v0, p0, Lcom/android/settings/applications/AppStorageSizesController;->xb:J

    iget-object v2, p0, Lcom/android/settings/applications/AppStorageSizesController;->xc:Landroid/preference/Preference;

    invoke-direct {p0, p1, v0, v1}, Lcom/android/settings/applications/AppStorageSizesController;->sY(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    goto :goto_1

    :cond_7
    iget-object v0, p0, Lcom/android/settings/applications/AppStorageSizesController;->wZ:Lcom/android/settingslib/b/J;

    invoke-interface {v0}, Lcom/android/settingslib/b/J;->cfQ()J

    move-result-wide v0

    iget-object v6, p0, Lcom/android/settings/applications/AppStorageSizesController;->wZ:Lcom/android/settingslib/b/J;

    invoke-interface {v6}, Lcom/android/settingslib/b/J;->cfR()J

    move-result-wide v6

    sub-long/2addr v0, v6

    goto :goto_2

    :cond_8
    iget-object v2, p0, Lcom/android/settings/applications/AppStorageSizesController;->wZ:Lcom/android/settingslib/b/J;

    invoke-interface {v2}, Lcom/android/settingslib/b/J;->cfR()J

    move-result-wide v2

    goto :goto_3
.end method
