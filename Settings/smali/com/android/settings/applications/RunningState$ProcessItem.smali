.class Lcom/android/settings/applications/RunningState$ProcessItem;
.super Lcom/android/settings/applications/RunningState$BaseItem;
.source "RunningState.java"


# instance fields
.field wA:I

.field wB:Lcom/android/settings/applications/RunningState$MergedItem;

.field wC:I

.field final wD:Ljava/util/HashMap;

.field final wq:I

.field final wr:Ljava/lang/String;

.field ws:Landroid/app/ActivityManager$RunningAppProcessInfo;

.field wt:I

.field wu:J

.field wv:Lcom/android/settings/applications/RunningState$ProcessItem;

.field final ww:Landroid/util/SparseArray;

.field wx:Z

.field wy:Z

.field wz:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;ILjava/lang/String;)V
    .locals 3

    const/4 v1, 0x1

    invoke-static {p2}, Landroid/os/UserHandle;->getUserId(I)I

    move-result v0

    invoke-direct {p0, v1, v0}, Lcom/android/settings/applications/RunningState$BaseItem;-><init>(ZI)V

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/android/settings/applications/RunningState$ProcessItem;->wD:Ljava/util/HashMap;

    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/android/settings/applications/RunningState$ProcessItem;->ww:Landroid/util/SparseArray;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p3, v1, v2

    const v2, 0x7f121024

    invoke-virtual {v0, v2, v1}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/applications/RunningState$ProcessItem;->wg:Ljava/lang/String;

    iput p2, p0, Lcom/android/settings/applications/RunningState$ProcessItem;->wq:I

    iput-object p3, p0, Lcom/android/settings/applications/RunningState$ProcessItem;->wr:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method sO(Landroid/content/pm/PackageManager;)V
    .locals 7

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/android/settings/applications/RunningState$ProcessItem;->wj:Ljava/lang/String;

    if-eqz v0, :cond_0

    return-void

    :cond_0
    :try_start_0
    iget-object v0, p0, Lcom/android/settings/applications/RunningState$ProcessItem;->wr:Ljava/lang/String;

    const/high16 v2, 0x400000

    invoke-virtual {p1, v0, v2}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v0

    iget v2, v0, Landroid/content/pm/ApplicationInfo;->uid:I

    iget v3, p0, Lcom/android/settings/applications/RunningState$ProcessItem;->wq:I

    if-ne v2, v3, :cond_1

    invoke-virtual {v0, p1}, Landroid/content/pm/ApplicationInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v2

    iput-object v2, p0, Lcom/android/settings/applications/RunningState$ProcessItem;->we:Ljava/lang/CharSequence;

    iget-object v2, p0, Lcom/android/settings/applications/RunningState$ProcessItem;->we:Ljava/lang/CharSequence;

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/android/settings/applications/RunningState$ProcessItem;->wj:Ljava/lang/String;

    iput-object v0, p0, Lcom/android/settings/applications/RunningState$ProcessItem;->wb:Landroid/content/pm/PackageItemInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception v0

    :cond_1
    iget v0, p0, Lcom/android/settings/applications/RunningState$ProcessItem;->wq:I

    invoke-virtual {p1, v0}, Landroid/content/pm/PackageManager;->getPackagesForUid(I)[Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_2

    const-string/jumbo v0, "RunningState"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "getPackagesForUid return null for uid: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/android/settings/applications/RunningState$ProcessItem;->wq:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    new-array v0, v1, [Ljava/lang/String;

    :cond_2
    array-length v2, v0

    const/4 v3, 0x1

    if-ne v2, v3, :cond_3

    const/4 v2, 0x0

    :try_start_1
    aget-object v2, v0, v2

    const/high16 v3, 0x400000

    invoke-virtual {p1, v2, v3}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v2

    invoke-virtual {v2, p1}, Landroid/content/pm/ApplicationInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v3

    iput-object v3, p0, Lcom/android/settings/applications/RunningState$ProcessItem;->we:Ljava/lang/CharSequence;

    iget-object v3, p0, Lcom/android/settings/applications/RunningState$ProcessItem;->we:Ljava/lang/CharSequence;

    invoke-interface {v3}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/android/settings/applications/RunningState$ProcessItem;->wj:Ljava/lang/String;

    iput-object v2, p0, Lcom/android/settings/applications/RunningState$ProcessItem;->wb:Landroid/content/pm/PackageItemInfo;
    :try_end_1
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_1 .. :try_end_1} :catch_1

    return-void

    :catch_1
    move-exception v2

    :cond_3
    array-length v2, v0

    :goto_0
    if-ge v1, v2, :cond_5

    aget-object v3, v0, v1

    const/4 v4, 0x0

    :try_start_2
    invoke-virtual {p1, v3, v4}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v4

    iget v5, v4, Landroid/content/pm/PackageInfo;->sharedUserLabel:I

    if-eqz v5, :cond_4

    iget v5, v4, Landroid/content/pm/PackageInfo;->sharedUserLabel:I

    iget-object v6, v4, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    invoke-virtual {p1, v3, v5, v6}, Landroid/content/pm/PackageManager;->getText(Ljava/lang/String;ILandroid/content/pm/ApplicationInfo;)Ljava/lang/CharSequence;

    move-result-object v3

    if-eqz v3, :cond_4

    iput-object v3, p0, Lcom/android/settings/applications/RunningState$ProcessItem;->we:Ljava/lang/CharSequence;

    invoke-interface {v3}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/android/settings/applications/RunningState$ProcessItem;->wj:Ljava/lang/String;

    iget-object v3, v4, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iput-object v3, p0, Lcom/android/settings/applications/RunningState$ProcessItem;->wb:Landroid/content/pm/PackageItemInfo;
    :try_end_2
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_2 .. :try_end_2} :catch_2

    return-void

    :catch_2
    move-exception v3

    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_5
    iget-object v1, p0, Lcom/android/settings/applications/RunningState$ProcessItem;->wD:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->size()I

    move-result v1

    if-lez v1, :cond_6

    iget-object v0, p0, Lcom/android/settings/applications/RunningState$ProcessItem;->wD:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settings/applications/RunningState$ServiceItem;

    iget-object v0, v0, Lcom/android/settings/applications/RunningState$ServiceItem;->wm:Landroid/content/pm/ServiceInfo;

    iget-object v0, v0, Landroid/content/pm/ServiceInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iput-object v0, p0, Lcom/android/settings/applications/RunningState$ProcessItem;->wb:Landroid/content/pm/PackageItemInfo;

    iget-object v0, p0, Lcom/android/settings/applications/RunningState$ProcessItem;->wb:Landroid/content/pm/PackageItemInfo;

    invoke-virtual {v0, p1}, Landroid/content/pm/PackageItemInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/applications/RunningState$ProcessItem;->we:Ljava/lang/CharSequence;

    iget-object v0, p0, Lcom/android/settings/applications/RunningState$ProcessItem;->we:Ljava/lang/CharSequence;

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/applications/RunningState$ProcessItem;->wj:Ljava/lang/String;

    return-void

    :cond_6
    :try_start_3
    array-length v1, v0

    if-nez v1, :cond_7

    const-string/jumbo v0, "RunningState"

    const-string/jumbo v1, "pkgs is empty"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :cond_7
    const/4 v1, 0x0

    aget-object v0, v0, v1

    const/high16 v1, 0x400000

    invoke-virtual {p1, v0, v1}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/pm/ApplicationInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v1

    iput-object v1, p0, Lcom/android/settings/applications/RunningState$ProcessItem;->we:Ljava/lang/CharSequence;

    iget-object v1, p0, Lcom/android/settings/applications/RunningState$ProcessItem;->we:Ljava/lang/CharSequence;

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/android/settings/applications/RunningState$ProcessItem;->wj:Ljava/lang/String;

    iput-object v0, p0, Lcom/android/settings/applications/RunningState$ProcessItem;->wb:Landroid/content/pm/PackageItemInfo;
    :try_end_3
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_3 .. :try_end_3} :catch_3

    return-void

    :catch_3
    move-exception v0

    return-void
.end method

.method sP(Ljava/util/ArrayList;Ljava/util/ArrayList;)V
    .locals 4

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/android/settings/applications/RunningState$ProcessItem;->ww:Landroid/util/SparseArray;

    invoke-virtual {v1}, Landroid/util/SparseArray;->size()I

    move-result v2

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_1

    iget-object v0, p0, Lcom/android/settings/applications/RunningState$ProcessItem;->ww:Landroid/util/SparseArray;

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settings/applications/RunningState$ProcessItem;

    invoke-virtual {v0, p1, p2}, Lcom/android/settings/applications/RunningState$ProcessItem;->sP(Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget v3, v0, Lcom/android/settings/applications/RunningState$ProcessItem;->wt:I

    if-lez v3, :cond_0

    invoke-virtual {p2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    return-void
.end method

.method sQ(Landroid/content/Context;Landroid/content/pm/PackageManager;I)Z
    .locals 6

    const/4 v2, 0x1

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/android/settings/applications/RunningState$ProcessItem;->ww:Landroid/util/SparseArray;

    invoke-virtual {v1}, Landroid/util/SparseArray;->size()I

    move-result v4

    move v3, v0

    move v1, v0

    :goto_0
    if-ge v3, v4, :cond_1

    iget-object v0, p0, Lcom/android/settings/applications/RunningState$ProcessItem;->ww:Landroid/util/SparseArray;

    invoke-virtual {v0, v3}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settings/applications/RunningState$ProcessItem;

    iget-object v5, v0, Lcom/android/settings/applications/RunningState$ProcessItem;->wv:Lcom/android/settings/applications/RunningState$ProcessItem;

    if-eq v5, p0, :cond_0

    iput-object p0, v0, Lcom/android/settings/applications/RunningState$ProcessItem;->wv:Lcom/android/settings/applications/RunningState$ProcessItem;

    move v1, v2

    :cond_0
    iput p3, v0, Lcom/android/settings/applications/RunningState$ProcessItem;->wi:I

    invoke-virtual {v0, p2}, Lcom/android/settings/applications/RunningState$ProcessItem;->sO(Landroid/content/pm/PackageManager;)V

    invoke-virtual {v0, p1, p2, p3}, Lcom/android/settings/applications/RunningState$ProcessItem;->sQ(Landroid/content/Context;Landroid/content/pm/PackageManager;I)Z

    move-result v0

    or-int/2addr v1, v0

    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_0

    :cond_1
    iget v0, p0, Lcom/android/settings/applications/RunningState$ProcessItem;->wA:I

    iget-object v3, p0, Lcom/android/settings/applications/RunningState$ProcessItem;->ww:Landroid/util/SparseArray;

    invoke-virtual {v3}, Landroid/util/SparseArray;->size()I

    move-result v3

    if-eq v0, v3, :cond_2

    iget-object v0, p0, Lcom/android/settings/applications/RunningState$ProcessItem;->ww:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v0

    iput v0, p0, Lcom/android/settings/applications/RunningState$ProcessItem;->wA:I

    :goto_1
    return v2

    :cond_2
    move v2, v1

    goto :goto_1
.end method

.method sR(Landroid/content/Context;Landroid/app/ActivityManager$RunningServiceInfo;)Z
    .locals 10

    const/4 v3, 0x1

    const/4 v4, 0x0

    const/4 v1, 0x0

    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v5

    iget-object v0, p0, Lcom/android/settings/applications/RunningState$ProcessItem;->wD:Ljava/util/HashMap;

    iget-object v2, p2, Landroid/app/ActivityManager$RunningServiceInfo;->service:Landroid/content/ComponentName;

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settings/applications/RunningState$ServiceItem;

    if-nez v0, :cond_8

    new-instance v2, Lcom/android/settings/applications/RunningState$ServiceItem;

    iget v0, p0, Lcom/android/settings/applications/RunningState$ProcessItem;->mUserId:I

    invoke-direct {v2, v0}, Lcom/android/settings/applications/RunningState$ServiceItem;-><init>(I)V

    iput-object p2, v2, Lcom/android/settings/applications/RunningState$ServiceItem;->wn:Landroid/app/ActivityManager$RunningServiceInfo;

    :try_start_0
    invoke-static {}, Landroid/app/ActivityThread;->getPackageManager()Landroid/content/pm/IPackageManager;

    move-result-object v0

    iget-object v6, p2, Landroid/app/ActivityManager$RunningServiceInfo;->service:Landroid/content/ComponentName;

    iget v7, p2, Landroid/app/ActivityManager$RunningServiceInfo;->uid:I

    invoke-static {v7}, Landroid/os/UserHandle;->getUserId(I)I

    move-result v7

    const/high16 v8, 0x400000

    invoke-interface {v0, v6, v8, v7}, Landroid/content/pm/IPackageManager;->getServiceInfo(Landroid/content/ComponentName;II)Landroid/content/pm/ServiceInfo;

    move-result-object v0

    iput-object v0, v2, Lcom/android/settings/applications/RunningState$ServiceItem;->wm:Landroid/content/pm/ServiceInfo;

    iget-object v0, v2, Lcom/android/settings/applications/RunningState$ServiceItem;->wm:Landroid/content/pm/ServiceInfo;

    if-nez v0, :cond_0

    const-string/jumbo v0, "RunningService"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "getServiceInfo returned null for: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p2, Landroid/app/ActivityManager$RunningServiceInfo;->service:Landroid/content/ComponentName;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v0, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    return v4

    :catch_0
    move-exception v0

    :cond_0
    iget-object v0, v2, Lcom/android/settings/applications/RunningState$ServiceItem;->wm:Landroid/content/pm/ServiceInfo;

    if-nez v0, :cond_1

    return v4

    :cond_1
    iget-object v0, v2, Lcom/android/settings/applications/RunningState$ServiceItem;->wn:Landroid/app/ActivityManager$RunningServiceInfo;

    iget-object v0, v0, Landroid/app/ActivityManager$RunningServiceInfo;->service:Landroid/content/ComponentName;

    invoke-virtual {v0}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v0

    iget-object v6, v2, Lcom/android/settings/applications/RunningState$ServiceItem;->wm:Landroid/content/pm/ServiceInfo;

    invoke-static {v5, v0, v6}, Lcom/android/settings/applications/RunningState;->sD(Landroid/content/pm/PackageManager;Ljava/lang/String;Landroid/content/pm/PackageItemInfo;)Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, v2, Lcom/android/settings/applications/RunningState$ServiceItem;->we:Ljava/lang/CharSequence;

    iget-object v0, p0, Lcom/android/settings/applications/RunningState$ProcessItem;->we:Ljava/lang/CharSequence;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/android/settings/applications/RunningState$ProcessItem;->we:Ljava/lang/CharSequence;

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    iput-object v0, p0, Lcom/android/settings/applications/RunningState$ProcessItem;->wj:Ljava/lang/String;

    iget-object v0, v2, Lcom/android/settings/applications/RunningState$ServiceItem;->wm:Landroid/content/pm/ServiceInfo;

    iget-object v0, v0, Landroid/content/pm/ServiceInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iput-object v0, v2, Lcom/android/settings/applications/RunningState$ServiceItem;->wb:Landroid/content/pm/PackageItemInfo;

    iget-object v0, p0, Lcom/android/settings/applications/RunningState$ProcessItem;->wD:Ljava/util/HashMap;

    iget-object v6, p2, Landroid/app/ActivityManager$RunningServiceInfo;->service:Landroid/content/ComponentName;

    invoke-virtual {v0, v6, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-object v0, v2

    move v2, v3

    :goto_1
    iget v6, p0, Lcom/android/settings/applications/RunningState$ProcessItem;->wi:I

    iput v6, v0, Lcom/android/settings/applications/RunningState$ServiceItem;->wi:I

    iput-object p2, v0, Lcom/android/settings/applications/RunningState$ServiceItem;->wn:Landroid/app/ActivityManager$RunningServiceInfo;

    iget-wide v6, p2, Landroid/app/ActivityManager$RunningServiceInfo;->restarting:J

    const-wide/16 v8, 0x0

    cmp-long v6, v6, v8

    if-nez v6, :cond_4

    iget-wide v6, p2, Landroid/app/ActivityManager$RunningServiceInfo;->activeSince:J

    :goto_2
    iget-wide v8, v0, Lcom/android/settings/applications/RunningState$ServiceItem;->wf:J

    cmp-long v8, v8, v6

    if-eqz v8, :cond_2

    iput-wide v6, v0, Lcom/android/settings/applications/RunningState$ServiceItem;->wf:J

    move v2, v3

    :cond_2
    iget-object v6, p2, Landroid/app/ActivityManager$RunningServiceInfo;->clientPackage:Ljava/lang/String;

    if-eqz v6, :cond_5

    iget v6, p2, Landroid/app/ActivityManager$RunningServiceInfo;->clientLabel:I

    if-eqz v6, :cond_5

    iget-boolean v6, v0, Lcom/android/settings/applications/RunningState$ServiceItem;->wp:Z

    if-eqz v6, :cond_7

    iput-boolean v4, v0, Lcom/android/settings/applications/RunningState$ServiceItem;->wp:Z

    :goto_3
    :try_start_1
    iget-object v2, p2, Landroid/app/ActivityManager$RunningServiceInfo;->clientPackage:Ljava/lang/String;

    invoke-virtual {v5, v2}, Landroid/content/pm/PackageManager;->getResourcesForApplication(Ljava/lang/String;)Landroid/content/res/Resources;

    move-result-object v2

    iget v4, p2, Landroid/app/ActivityManager$RunningServiceInfo;->clientLabel:I

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object v2, v5, v6

    const v2, 0x7f121020

    invoke-virtual {v4, v2, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Lcom/android/settings/applications/RunningState$ServiceItem;->wg:Ljava/lang/String;
    :try_end_1
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_1 .. :try_end_1} :catch_1

    :goto_4
    return v3

    :cond_3
    move-object v0, v1

    goto :goto_0

    :cond_4
    const-wide/16 v6, -0x1

    goto :goto_2

    :catch_1
    move-exception v2

    iput-object v1, v0, Lcom/android/settings/applications/RunningState$ServiceItem;->wg:Ljava/lang/String;

    goto :goto_4

    :cond_5
    iget-boolean v1, v0, Lcom/android/settings/applications/RunningState$ServiceItem;->wp:Z

    if-nez v1, :cond_6

    iput-boolean v3, v0, Lcom/android/settings/applications/RunningState$ServiceItem;->wp:Z

    :goto_5
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f121026

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/android/settings/applications/RunningState$ServiceItem;->wg:Ljava/lang/String;

    goto :goto_4

    :cond_6
    move v3, v2

    goto :goto_5

    :cond_7
    move v3, v2

    goto :goto_3

    :cond_8
    move v2, v4

    goto :goto_1
.end method

.method sS(Landroid/content/Context;JI)Z
    .locals 4

    const/4 v2, 0x0

    const-wide/16 v0, 0x400

    mul-long/2addr v0, p2

    iput-wide v0, p0, Lcom/android/settings/applications/RunningState$ProcessItem;->wl:J

    iget v0, p0, Lcom/android/settings/applications/RunningState$ProcessItem;->wi:I

    if-ne v0, p4, :cond_0

    iget-wide v0, p0, Lcom/android/settings/applications/RunningState$ProcessItem;->wl:J

    invoke-static {p1, v0, v1}, Landroid/text/format/Formatter;->formatShortFileSize(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/applications/RunningState$ProcessItem;->wc:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    iput-object v0, p0, Lcom/android/settings/applications/RunningState$ProcessItem;->wc:Ljava/lang/String;

    return v2

    :cond_0
    return v2
.end method
