.class public Lcom/android/settings/applications/ProcessStatsSummary;
.super Lcom/android/settings/applications/ProcessStatsBase;
.source "ProcessStatsSummary.java"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceClickListener;


# static fields
.field public static final SUMMARY_PROVIDER_FACTORY:Lcom/android/settings/dashboard/F;


# instance fields
.field private pN:Landroid/preference/Preference;

.field private pO:Landroid/preference/Preference;

.field private pP:Landroid/preference/Preference;

.field private pQ:Landroid/preference/Preference;

.field private pR:Lcom/android/settings/MiuiSummaryPreference;

.field private pS:Landroid/preference/Preference;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/android/settings/applications/ProcessStatsSummary$1;

    invoke-direct {v0}, Lcom/android/settings/applications/ProcessStatsSummary$1;-><init>()V

    sput-object v0, Lcom/android/settings/applications/ProcessStatsSummary;->SUMMARY_PROVIDER_FACTORY:Lcom/android/settings/dashboard/F;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/applications/ProcessStatsBase;-><init>()V

    return-void
.end method


# virtual methods
.method public getMetricsCategory()I
    .locals 1

    const/16 v0, 0xca

    return v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1

    invoke-super {p0, p1}, Lcom/android/settings/applications/ProcessStatsBase;->onCreate(Landroid/os/Bundle;)V

    const v0, 0x7f1500ad

    invoke-virtual {p0, v0}, Lcom/android/settings/applications/ProcessStatsSummary;->addPreferencesFromResource(I)V

    const-string/jumbo v0, "status_header"

    invoke-virtual {p0, v0}, Lcom/android/settings/applications/ProcessStatsSummary;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Lcom/android/settings/MiuiSummaryPreference;

    iput-object v0, p0, Lcom/android/settings/applications/ProcessStatsSummary;->pR:Lcom/android/settings/MiuiSummaryPreference;

    const-string/jumbo v0, "performance"

    invoke-virtual {p0, v0}, Lcom/android/settings/applications/ProcessStatsSummary;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/applications/ProcessStatsSummary;->pQ:Landroid/preference/Preference;

    const-string/jumbo v0, "total_memory"

    invoke-virtual {p0, v0}, Lcom/android/settings/applications/ProcessStatsSummary;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/applications/ProcessStatsSummary;->pS:Landroid/preference/Preference;

    const-string/jumbo v0, "average_used"

    invoke-virtual {p0, v0}, Lcom/android/settings/applications/ProcessStatsSummary;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/applications/ProcessStatsSummary;->pO:Landroid/preference/Preference;

    const-string/jumbo v0, "free"

    invoke-virtual {p0, v0}, Lcom/android/settings/applications/ProcessStatsSummary;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/applications/ProcessStatsSummary;->pP:Landroid/preference/Preference;

    const-string/jumbo v0, "apps_list"

    invoke-virtual {p0, v0}, Lcom/android/settings/applications/ProcessStatsSummary;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/applications/ProcessStatsSummary;->pN:Landroid/preference/Preference;

    iget-object v0, p0, Lcom/android/settings/applications/ProcessStatsSummary;->pN:Landroid/preference/Preference;

    invoke-virtual {v0, p0}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    return-void
.end method

.method public onPreferenceClick(Landroid/preference/Preference;)Z
    .locals 7

    const/4 v6, 0x1

    const/4 v4, 0x0

    iget-object v0, p0, Lcom/android/settings/applications/ProcessStatsSummary;->pN:Landroid/preference/Preference;

    if-ne p1, v0, :cond_0

    new-instance v5, Landroid/os/Bundle;

    invoke-direct {v5}, Landroid/os/Bundle;-><init>()V

    const-string/jumbo v0, "transfer_stats"

    invoke-virtual {v5, v0, v6}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string/jumbo v0, "duration_index"

    iget v1, p0, Lcom/android/settings/applications/ProcessStatsSummary;->Bz:I

    invoke-virtual {v5, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    iget-object v0, p0, Lcom/android/settings/applications/ProcessStatsSummary;->Bx:Lcom/android/settings/applications/ProcStatsData;

    invoke-virtual {v0}, Lcom/android/settings/applications/ProcStatsData;->zG()V

    const-class v0, Lcom/android/settings/applications/ProcessStatsUi;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    const v3, 0x7f120137

    move-object v0, p0

    move-object v1, p0

    invoke-virtual/range {v0 .. v5}, Lcom/android/settings/applications/ProcessStatsSummary;->bWM(Landroid/app/Fragment;Ljava/lang/String;IILandroid/os/Bundle;)Z

    return v6

    :cond_0
    return v4
.end method

.method public refreshUi()V
    .locals 14

    const/4 v13, 0x1

    const/4 v12, 0x0

    invoke-virtual {p0}, Lcom/android/settings/applications/ProcessStatsSummary;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/applications/ProcessStatsSummary;->Bx:Lcom/android/settings/applications/ProcStatsData;

    invoke-virtual {v1}, Lcom/android/settings/applications/ProcStatsData;->zD()Lcom/android/settings/applications/ProcStatsData$MemInfo;

    move-result-object v1

    iget-wide v2, v1, Lcom/android/settings/applications/ProcStatsData$MemInfo;->Fx:D

    iget-wide v4, v1, Lcom/android/settings/applications/ProcStatsData$MemInfo;->Fy:D

    iget-wide v6, v1, Lcom/android/settings/applications/ProcStatsData$MemInfo;->Fz:D

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    double-to-long v8, v2

    invoke-static {v1, v8, v9, v13}, Landroid/text/format/Formatter;->formatBytes(Landroid/content/res/Resources;JI)Landroid/text/format/Formatter$BytesResult;

    move-result-object v1

    double-to-long v8, v4

    invoke-static {v0, v8, v9}, Landroid/text/format/Formatter;->formatShortFileSize(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v8

    double-to-long v10, v6

    invoke-static {v0, v10, v11}, Landroid/text/format/Formatter;->formatShortFileSize(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p0}, Lcom/android/settings/applications/ProcessStatsSummary;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v10, 0x7f0300cd

    invoke-virtual {v0, v10}, Landroid/content/res/Resources;->getTextArray(I)[Ljava/lang/CharSequence;

    move-result-object v0

    iget-object v10, p0, Lcom/android/settings/applications/ProcessStatsSummary;->Bx:Lcom/android/settings/applications/ProcStatsData;

    invoke-virtual {v10}, Lcom/android/settings/applications/ProcStatsData;->zE()I

    move-result v10

    if-ltz v10, :cond_0

    array-length v11, v0

    add-int/lit8 v11, v11, -0x1

    if-ge v10, v11, :cond_0

    aget-object v0, v0, v10

    :goto_0
    iget-object v10, p0, Lcom/android/settings/applications/ProcessStatsSummary;->pR:Lcom/android/settings/MiuiSummaryPreference;

    iget-object v11, v1, Landroid/text/format/Formatter$BytesResult;->value:Ljava/lang/String;

    invoke-virtual {v10, v11}, Lcom/android/settings/MiuiSummaryPreference;->bff(Ljava/lang/String;)V

    iget-object v10, p0, Lcom/android/settings/applications/ProcessStatsSummary;->pR:Lcom/android/settings/MiuiSummaryPreference;

    iget-object v1, v1, Landroid/text/format/Formatter$BytesResult;->units:Ljava/lang/String;

    invoke-virtual {v10, v1}, Lcom/android/settings/MiuiSummaryPreference;->bfi(Ljava/lang/String;)V

    add-double/2addr v6, v2

    div-double v6, v2, v6

    double-to-float v1, v6

    iget-object v6, p0, Lcom/android/settings/applications/ProcessStatsSummary;->pR:Lcom/android/settings/MiuiSummaryPreference;

    const/4 v7, 0x0

    const/high16 v10, 0x3f800000    # 1.0f

    sub-float/2addr v10, v1

    invoke-virtual {v6, v1, v7, v10}, Lcom/android/settings/MiuiSummaryPreference;->setRatios(FFF)V

    iget-object v1, p0, Lcom/android/settings/applications/ProcessStatsSummary;->pQ:Landroid/preference/Preference;

    invoke-virtual {v1, v0}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/android/settings/applications/ProcessStatsSummary;->pS:Landroid/preference/Preference;

    invoke-virtual {v0, v8}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/android/settings/applications/ProcessStatsSummary;->pO:Landroid/preference/Preference;

    double-to-long v2, v2

    double-to-long v4, v4

    invoke-static {v2, v3, v4, v5}, Lcom/android/settings/aq;->cqF(JJ)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/android/settings/applications/ProcessStatsSummary;->pP:Landroid/preference/Preference;

    invoke-virtual {v0, v9}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    sget-object v0, Lcom/android/settings/applications/ProcessStatsSummary;->By:[I

    iget v1, p0, Lcom/android/settings/applications/ProcessStatsSummary;->Bz:I

    aget v0, v0, v1

    invoke-virtual {p0, v0}, Lcom/android/settings/applications/ProcessStatsSummary;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/applications/ProcessStatsSummary;->Bx:Lcom/android/settings/applications/ProcStatsData;

    invoke-virtual {v1}, Lcom/android/settings/applications/ProcStatsData;->zF()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    iget-object v2, p0, Lcom/android/settings/applications/ProcessStatsSummary;->pN:Landroid/preference/Preference;

    invoke-virtual {p0}, Lcom/android/settings/applications/ProcessStatsSummary;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v12

    aput-object v0, v4, v13

    const v0, 0x7f100029

    invoke-virtual {v3, v0, v1, v4}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    return-void

    :cond_0
    array-length v10, v0

    add-int/lit8 v10, v10, -0x1

    aget-object v0, v0, v10

    goto :goto_0
.end method
