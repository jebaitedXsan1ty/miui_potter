.class public Lcom/android/settings/applications/IPackageManagerWrapperImpl;
.super Ljava/lang/Object;
.source "IPackageManagerWrapperImpl.java"

# interfaces
.implements Lcom/android/settings/applications/IPackageManagerWrapper;


# instance fields
.field private final qe:Landroid/content/pm/IPackageManager;


# direct methods
.method public constructor <init>(Landroid/content/pm/IPackageManager;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/android/settings/applications/IPackageManagerWrapperImpl;->qe:Landroid/content/pm/IPackageManager;

    return-void
.end method


# virtual methods
.method public po(Ljava/lang/String;I)I
    .locals 1

    iget-object v0, p0, Lcom/android/settings/applications/IPackageManagerWrapperImpl;->qe:Landroid/content/pm/IPackageManager;

    invoke-interface {v0, p1, p2}, Landroid/content/pm/IPackageManager;->checkUidPermission(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public pp(Landroid/content/Intent;I)Landroid/content/pm/ResolveInfo;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/applications/IPackageManagerWrapperImpl;->qe:Landroid/content/pm/IPackageManager;

    invoke-interface {v0, p1, p2}, Landroid/content/pm/IPackageManager;->findPersistentPreferredActivity(Landroid/content/Intent;I)Landroid/content/pm/ResolveInfo;

    move-result-object v0

    return-object v0
.end method

.method public pq(Ljava/lang/String;)[Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/applications/IPackageManagerWrapperImpl;->qe:Landroid/content/pm/IPackageManager;

    invoke-interface {v0, p1}, Landroid/content/pm/IPackageManager;->getAppOpPermissionPackages(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public pr(Ljava/lang/String;II)Landroid/content/pm/PackageInfo;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/applications/IPackageManagerWrapperImpl;->qe:Landroid/content/pm/IPackageManager;

    invoke-interface {v0, p1, p2, p3}, Landroid/content/pm/IPackageManager;->getPackageInfo(Ljava/lang/String;II)Landroid/content/pm/PackageInfo;

    move-result-object v0

    return-object v0
.end method

.method public ps([Ljava/lang/String;II)Landroid/content/pm/ParceledListSlice;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/applications/IPackageManagerWrapperImpl;->qe:Landroid/content/pm/IPackageManager;

    invoke-interface {v0, p1, p2, p3}, Landroid/content/pm/IPackageManager;->getPackagesHoldingPermissions([Ljava/lang/String;II)Landroid/content/pm/ParceledListSlice;

    move-result-object v0

    return-object v0
.end method

.method public pt(Ljava/lang/String;I)Z
    .locals 1

    iget-object v0, p0, Lcom/android/settings/applications/IPackageManagerWrapperImpl;->qe:Landroid/content/pm/IPackageManager;

    invoke-interface {v0, p1, p2}, Landroid/content/pm/IPackageManager;->isPackageAvailable(Ljava/lang/String;I)Z

    move-result v0

    return v0
.end method
