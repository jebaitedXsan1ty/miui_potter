.class public Lcom/android/settings/applications/ProcStatsPackageEntry;
.super Ljava/lang/Object;
.source "ProcStatsPackageEntry.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field private static AC:Z

.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field AA:J

.field AB:J

.field AD:J

.field AE:J

.field AF:J

.field final AG:Ljava/util/ArrayList;

.field AH:J

.field public AI:Ljava/lang/String;

.field public AJ:Landroid/content/pm/ApplicationInfo;

.field private AK:J

.field Ay:D

.field Az:D

.field final mPackage:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput-boolean v0, Lcom/android/settings/applications/ProcStatsPackageEntry;->AC:Z

    new-instance v0, Lcom/android/settings/applications/ProcStatsPackageEntry$1;

    invoke-direct {v0}, Lcom/android/settings/applications/ProcStatsPackageEntry$1;-><init>()V

    sput-object v0, Lcom/android/settings/applications/ProcStatsPackageEntry;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/settings/applications/ProcStatsPackageEntry;->AG:Ljava/util/ArrayList;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/applications/ProcStatsPackageEntry;->mPackage:Ljava/lang/String;

    iget-object v0, p0, Lcom/android/settings/applications/ProcStatsPackageEntry;->AG:Ljava/util/ArrayList;

    sget-object v1, Lcom/android/settings/applications/ProcStatsEntry;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->readTypedList(Ljava/util/List;Landroid/os/Parcelable$Creator;)V

    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/android/settings/applications/ProcStatsPackageEntry;->AF:J

    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/android/settings/applications/ProcStatsPackageEntry;->AD:J

    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/android/settings/applications/ProcStatsPackageEntry;->AA:J

    invoke-virtual {p1}, Landroid/os/Parcel;->readDouble()D

    move-result-wide v0

    iput-wide v0, p0, Lcom/android/settings/applications/ProcStatsPackageEntry;->Az:D

    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/android/settings/applications/ProcStatsPackageEntry;->AH:J

    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/android/settings/applications/ProcStatsPackageEntry;->AE:J

    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/android/settings/applications/ProcStatsPackageEntry;->AB:J

    invoke-virtual {p1}, Landroid/os/Parcel;->readDouble()D

    move-result-wide v0

    iput-wide v0, p0, Lcom/android/settings/applications/ProcStatsPackageEntry;->Ay:D

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;J)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/settings/applications/ProcStatsPackageEntry;->AG:Ljava/util/ArrayList;

    iput-object p1, p0, Lcom/android/settings/applications/ProcStatsPackageEntry;->mPackage:Ljava/lang/String;

    iput-wide p2, p0, Lcom/android/settings/applications/ProcStatsPackageEntry;->AK:J

    return-void
.end method

.method public static wg(FLandroid/content/Context;)Ljava/lang/CharSequence;
    .locals 4

    const/4 v3, 0x1

    const/4 v2, 0x0

    const/high16 v1, 0x42c80000    # 100.0f

    const v0, 0x3f733333    # 0.95f

    cmpl-float v0, p0, v0

    if-lez v0, :cond_0

    new-array v0, v3, [Ljava/lang/Object;

    mul-float/2addr v1, p0

    float-to-int v1, v1

    invoke-static {v1}, Lcom/android/settings/aq;->cqH(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v2

    const v1, 0x7f1200e3

    invoke-virtual {p1, v1, v0}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    const/high16 v0, 0x3e800000    # 0.25f

    cmpl-float v0, p0, v0

    if-lez v0, :cond_1

    new-array v0, v3, [Ljava/lang/Object;

    mul-float/2addr v1, p0

    float-to-int v1, v1

    invoke-static {v1}, Lcom/android/settings/aq;->cqH(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v2

    const v1, 0x7f1210ec

    invoke-virtual {p1, v1, v0}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_1
    new-array v0, v3, [Ljava/lang/Object;

    mul-float/2addr v1, p0

    float-to-int v1, v1

    invoke-static {v1}, Lcom/android/settings/aq;->cqH(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v2

    const v1, 0x7f120df3

    invoke-virtual {p1, v1, v0}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public wd()V
    .locals 8

    const-wide/16 v2, 0x0

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/android/settings/applications/ProcStatsPackageEntry;->AA:J

    iput-wide v0, p0, Lcom/android/settings/applications/ProcStatsPackageEntry;->AD:J

    iput-wide v0, p0, Lcom/android/settings/applications/ProcStatsPackageEntry;->AF:J

    iput-wide v2, p0, Lcom/android/settings/applications/ProcStatsPackageEntry;->Az:D

    iput-wide v0, p0, Lcom/android/settings/applications/ProcStatsPackageEntry;->AB:J

    iput-wide v0, p0, Lcom/android/settings/applications/ProcStatsPackageEntry;->AE:J

    iput-wide v0, p0, Lcom/android/settings/applications/ProcStatsPackageEntry;->AH:J

    iput-wide v2, p0, Lcom/android/settings/applications/ProcStatsPackageEntry;->Ay:D

    iget-object v0, p0, Lcom/android/settings/applications/ProcStatsPackageEntry;->AG:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    iget-object v0, p0, Lcom/android/settings/applications/ProcStatsPackageEntry;->AG:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settings/applications/ProcStatsEntry;

    iget-wide v4, v0, Lcom/android/settings/applications/ProcStatsEntry;->Be:J

    iget-wide v6, p0, Lcom/android/settings/applications/ProcStatsPackageEntry;->AF:J

    invoke-static {v4, v5, v6, v7}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v4

    iput-wide v4, p0, Lcom/android/settings/applications/ProcStatsPackageEntry;->AF:J

    iget-wide v4, p0, Lcom/android/settings/applications/ProcStatsPackageEntry;->AD:J

    iget-wide v6, v0, Lcom/android/settings/applications/ProcStatsEntry;->Bf:J

    add-long/2addr v4, v6

    iput-wide v4, p0, Lcom/android/settings/applications/ProcStatsPackageEntry;->AD:J

    iget-wide v4, p0, Lcom/android/settings/applications/ProcStatsPackageEntry;->Az:D

    iget-wide v6, v0, Lcom/android/settings/applications/ProcStatsEntry;->Bg:D

    add-double/2addr v4, v6

    iput-wide v4, p0, Lcom/android/settings/applications/ProcStatsPackageEntry;->Az:D

    iget-wide v4, v0, Lcom/android/settings/applications/ProcStatsEntry;->Bh:J

    iget-wide v6, p0, Lcom/android/settings/applications/ProcStatsPackageEntry;->AH:J

    invoke-static {v4, v5, v6, v7}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v4

    iput-wide v4, p0, Lcom/android/settings/applications/ProcStatsPackageEntry;->AH:J

    iget-wide v4, p0, Lcom/android/settings/applications/ProcStatsPackageEntry;->AE:J

    iget-wide v6, v0, Lcom/android/settings/applications/ProcStatsEntry;->Bi:J

    add-long/2addr v4, v6

    iput-wide v4, p0, Lcom/android/settings/applications/ProcStatsPackageEntry;->AE:J

    iget-wide v4, p0, Lcom/android/settings/applications/ProcStatsPackageEntry;->Ay:D

    iget-wide v6, v0, Lcom/android/settings/applications/ProcStatsEntry;->Bj:D

    add-double/2addr v4, v6

    iput-wide v4, p0, Lcom/android/settings/applications/ProcStatsPackageEntry;->Ay:D

    iget-wide v4, p0, Lcom/android/settings/applications/ProcStatsPackageEntry;->AA:J

    iget-wide v6, v0, Lcom/android/settings/applications/ProcStatsEntry;->Bk:J

    add-long/2addr v4, v6

    iput-wide v4, p0, Lcom/android/settings/applications/ProcStatsPackageEntry;->AA:J

    iget-wide v4, p0, Lcom/android/settings/applications/ProcStatsPackageEntry;->AB:J

    iget-wide v6, v0, Lcom/android/settings/applications/ProcStatsEntry;->Bl:J

    add-long/2addr v4, v6

    iput-wide v4, p0, Lcom/android/settings/applications/ProcStatsPackageEntry;->AB:J

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    iget-wide v0, p0, Lcom/android/settings/applications/ProcStatsPackageEntry;->AD:J

    int-to-long v4, v2

    div-long/2addr v0, v4

    iput-wide v0, p0, Lcom/android/settings/applications/ProcStatsPackageEntry;->AD:J

    iget-wide v0, p0, Lcom/android/settings/applications/ProcStatsPackageEntry;->AE:J

    int-to-long v2, v2

    div-long/2addr v0, v2

    iput-wide v0, p0, Lcom/android/settings/applications/ProcStatsPackageEntry;->AE:J

    return-void
.end method

.method public we(Landroid/content/Context;Landroid/content/pm/PackageManager;)V
    .locals 3

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/settings/applications/ProcStatsPackageEntry;->AJ:Landroid/content/pm/ApplicationInfo;

    iget-object v0, p0, Lcom/android/settings/applications/ProcStatsPackageEntry;->mPackage:Ljava/lang/String;

    iput-object v0, p0, Lcom/android/settings/applications/ProcStatsPackageEntry;->AI:Ljava/lang/String;

    :try_start_0
    const-string/jumbo v0, "os"

    iget-object v1, p0, Lcom/android/settings/applications/ProcStatsPackageEntry;->mPackage:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string/jumbo v0, "android"

    const v1, 0x408200

    invoke-virtual {p2, v0, v1}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/applications/ProcStatsPackageEntry;->AJ:Landroid/content/pm/ApplicationInfo;

    const v0, 0x7f120d79

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/applications/ProcStatsPackageEntry;->AI:Ljava/lang/String;

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/settings/applications/ProcStatsPackageEntry;->mPackage:Ljava/lang/String;

    const v1, 0x408200

    invoke-virtual {p2, v0, v1}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/applications/ProcStatsPackageEntry;->AJ:Landroid/content/pm/ApplicationInfo;

    iget-object v0, p0, Lcom/android/settings/applications/ProcStatsPackageEntry;->AJ:Landroid/content/pm/ApplicationInfo;

    invoke-virtual {v0, p2}, Landroid/content/pm/ApplicationInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/applications/ProcStatsPackageEntry;->AI:Ljava/lang/String;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string/jumbo v0, "ProcStatsEntry"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "could not find package: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/settings/applications/ProcStatsPackageEntry;->mPackage:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public wf(Lcom/android/settings/applications/ProcStatsEntry;)V
    .locals 1

    iget-object v0, p0, Lcom/android/settings/applications/ProcStatsPackageEntry;->AG:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/applications/ProcStatsPackageEntry;->mPackage:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/settings/applications/ProcStatsPackageEntry;->AG:Ljava/util/ArrayList;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    iget-wide v0, p0, Lcom/android/settings/applications/ProcStatsPackageEntry;->AF:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    iget-wide v0, p0, Lcom/android/settings/applications/ProcStatsPackageEntry;->AD:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    iget-wide v0, p0, Lcom/android/settings/applications/ProcStatsPackageEntry;->AA:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    iget-wide v0, p0, Lcom/android/settings/applications/ProcStatsPackageEntry;->Az:D

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeDouble(D)V

    iget-wide v0, p0, Lcom/android/settings/applications/ProcStatsPackageEntry;->AH:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    iget-wide v0, p0, Lcom/android/settings/applications/ProcStatsPackageEntry;->AE:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    iget-wide v0, p0, Lcom/android/settings/applications/ProcStatsPackageEntry;->AB:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    iget-wide v0, p0, Lcom/android/settings/applications/ProcStatsPackageEntry;->Ay:D

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeDouble(D)V

    return-void
.end method
