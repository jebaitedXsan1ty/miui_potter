.class final Lcom/android/settings/applications/PreferredListSettings$1;
.super Ljava/lang/Object;
.source "PreferredListSettings.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field final synthetic FV:Lcom/android/settings/applications/PreferredListSettings;


# direct methods
.method constructor <init>(Lcom/android/settings/applications/PreferredListSettings;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/applications/PreferredListSettings$1;->FV:Lcom/android/settings/applications/PreferredListSettings;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 8

    const/4 v7, 0x1

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/android/settings/applications/PreferredListSettings$1;->FV:Lcom/android/settings/applications/PreferredListSettings;

    invoke-virtual {v0}, Lcom/android/settings/applications/PreferredListSettings;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v3

    move v1, v2

    :goto_0
    invoke-virtual {v3}, Landroid/preference/PreferenceScreen;->getPreferenceCount()I

    move-result v0

    if-ge v1, v0, :cond_6

    invoke-virtual {v3, v1}, Landroid/preference/PreferenceScreen;->getPreference(I)Landroid/preference/Preference;

    move-result-object v0

    invoke-virtual {v0}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_3

    iget-object v5, p0, Lcom/android/settings/applications/PreferredListSettings$1;->FV:Lcom/android/settings/applications/PreferredListSettings;

    invoke-static {v5}, Lcom/android/settings/applications/PreferredListSettings;->oW(Lcom/android/settings/applications/PreferredListSettings;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_1

    :cond_0
    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    iget-object v5, p0, Lcom/android/settings/applications/PreferredListSettings$1;->FV:Lcom/android/settings/applications/PreferredListSettings;

    invoke-static {v5}, Lcom/android/settings/applications/PreferredListSettings;->oX(Lcom/android/settings/applications/PreferredListSettings;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_3

    new-instance v0, Landroid/content/IntentFilter;

    const-string/jumbo v4, "android.intent.action.ASSIST"

    invoke-direct {v0, v4}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Lcom/android/settings/applications/DefaultAppsHelper;->pk(Landroid/content/IntentFilter;)Landroid/content/Intent;

    move-result-object v0

    iget-object v4, p0, Lcom/android/settings/applications/PreferredListSettings$1;->FV:Lcom/android/settings/applications/PreferredListSettings;

    invoke-static {v4}, Lcom/android/settings/applications/PreferredListSettings;->oY(Lcom/android/settings/applications/PreferredListSettings;)Landroid/content/pm/PackageManager;

    move-result-object v4

    invoke-virtual {v4, v0, v2}, Landroid/content/pm/PackageManager;->resolveActivity(Landroid/content/Intent;I)Landroid/content/pm/ResolveInfo;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v4, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    if-eqz v4, :cond_2

    iget-object v4, p0, Lcom/android/settings/applications/PreferredListSettings$1;->FV:Lcom/android/settings/applications/PreferredListSettings;

    invoke-static {v4}, Lcom/android/settings/applications/PreferredListSettings;->oY(Lcom/android/settings/applications/PreferredListSettings;)Landroid/content/pm/PackageManager;

    move-result-object v4

    iget-object v0, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v0, v0, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v4, v0}, Landroid/content/pm/PackageManager;->clearPackagePreferredActivities(Ljava/lang/String;)V

    :cond_2
    new-instance v0, Landroid/content/IntentFilter;

    const-string/jumbo v4, "android.intent.action.VOICE_COMMAND"

    invoke-direct {v0, v4}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Lcom/android/settings/applications/DefaultAppsHelper;->pk(Landroid/content/IntentFilter;)Landroid/content/Intent;

    move-result-object v0

    iget-object v4, p0, Lcom/android/settings/applications/PreferredListSettings$1;->FV:Lcom/android/settings/applications/PreferredListSettings;

    invoke-static {v4}, Lcom/android/settings/applications/PreferredListSettings;->oY(Lcom/android/settings/applications/PreferredListSettings;)Landroid/content/pm/PackageManager;

    move-result-object v4

    invoke-virtual {v4, v0, v2}, Landroid/content/pm/PackageManager;->resolveActivity(Landroid/content/Intent;I)Landroid/content/pm/ResolveInfo;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v4, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/android/settings/applications/PreferredListSettings$1;->FV:Lcom/android/settings/applications/PreferredListSettings;

    invoke-static {v4}, Lcom/android/settings/applications/PreferredListSettings;->oY(Lcom/android/settings/applications/PreferredListSettings;)Landroid/content/pm/PackageManager;

    move-result-object v4

    iget-object v0, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v0, v0, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v4, v0}, Landroid/content/pm/PackageManager;->clearPackagePreferredActivities(Ljava/lang/String;)V

    goto :goto_1

    :cond_3
    const/4 v4, 0x3

    if-ne v4, v1, :cond_4

    iget-object v0, p0, Lcom/android/settings/applications/PreferredListSettings$1;->FV:Lcom/android/settings/applications/PreferredListSettings;

    invoke-static {v0}, Lcom/android/settings/applications/PreferredListSettings;->oY(Lcom/android/settings/applications/PreferredListSettings;)Landroid/content/pm/PackageManager;

    move-result-object v0

    const-string/jumbo v4, "com.android.browser"

    iget-object v5, p0, Lcom/android/settings/applications/PreferredListSettings$1;->FV:Lcom/android/settings/applications/PreferredListSettings;

    invoke-virtual {v5}, Lcom/android/settings/applications/PreferredListSettings;->getUserId()I

    move-result v5

    invoke-virtual {v0, v4, v5}, Landroid/content/pm/PackageManager;->setDefaultBrowserPackageNameAsUser(Ljava/lang/String;I)Z

    goto :goto_1

    :cond_4
    invoke-virtual {v0}, Landroid/preference/Preference;->getIntent()Landroid/content/Intent;

    move-result-object v4

    const-string/jumbo v5, "preferred_app_package_name"

    invoke-virtual {v4, v5}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0}, Landroid/preference/Preference;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v5, "preferred_app_intent_filter"

    invoke-virtual {v0, v5}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/content/IntentFilter;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_0

    const-string/jumbo v5, "android.intent.action.SENDTO"

    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->getAction(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_5

    const-string/jumbo v5, "smsto"

    invoke-virtual {v0, v5}, Landroid/content/IntentFilter;->hasDataScheme(Ljava/lang/String;)Z

    move-result v0

    :goto_2
    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/applications/PreferredListSettings$1;->FV:Lcom/android/settings/applications/PreferredListSettings;

    invoke-static {v0}, Lcom/android/settings/applications/PreferredListSettings;->oY(Lcom/android/settings/applications/PreferredListSettings;)Landroid/content/pm/PackageManager;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/content/pm/PackageManager;->clearPackagePreferredActivities(Ljava/lang/String;)V

    goto/16 :goto_1

    :cond_5
    move v0, v2

    goto :goto_2

    :cond_6
    iget-object v0, p0, Lcom/android/settings/applications/PreferredListSettings$1;->FV:Lcom/android/settings/applications/PreferredListSettings;

    invoke-static {v0}, Landroid/telecom/TelecomManager;->from(Landroid/content/Context;)Landroid/telecom/TelecomManager;

    move-result-object v0

    const-string/jumbo v1, "com.android.contacts"

    invoke-virtual {v0, v1}, Landroid/telecom/TelecomManager;->setDefaultDialer(Ljava/lang/String;)Z

    const-string/jumbo v0, "com.android.mms"

    iget-object v1, p0, Lcom/android/settings/applications/PreferredListSettings$1;->FV:Lcom/android/settings/applications/PreferredListSettings;

    invoke-static {v0, v1}, Lcom/android/internal/telephony/SmsApplication;->setDefaultApplication(Ljava/lang/String;Landroid/content/Context;)V

    iget-object v0, p0, Lcom/android/settings/applications/PreferredListSettings$1;->FV:Lcom/android/settings/applications/PreferredListSettings;

    invoke-virtual {v0}, Lcom/android/settings/applications/PreferredListSettings;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v1, "assist_structure_enabled"

    invoke-static {v0, v1, v7}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    iget-object v0, p0, Lcom/android/settings/applications/PreferredListSettings$1;->FV:Lcom/android/settings/applications/PreferredListSettings;

    invoke-virtual {v0}, Lcom/android/settings/applications/PreferredListSettings;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v1, "assist_screenshot_enabled"

    invoke-static {v0, v1, v7}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    iget-object v0, p0, Lcom/android/settings/applications/PreferredListSettings$1;->FV:Lcom/android/settings/applications/PreferredListSettings;

    invoke-static {v0}, Lcom/android/settings/applications/PreferredListSettings;->oZ(Lcom/android/settings/applications/PreferredListSettings;)V

    return-void
.end method
