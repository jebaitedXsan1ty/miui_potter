.class public Lcom/android/settings/applications/ExternalSourcesDetails;
.super Lcom/android/settings/applications/MiuiAppInfoWithHeader;
.source "ExternalSourcesDetails.java"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceChangeListener;


# instance fields
.field private mUserManager:Landroid/os/UserManager;

.field private ph:Lcom/android/settings/applications/AppStateInstallAppsBridge;

.field private pi:Landroid/app/AppOpsManager;

.field private pj:Lcom/android/settings/applications/AppStateInstallAppsBridge$InstallAppsState;

.field private pk:Lcom/android/settingslib/MiuiRestrictedSwitchPreference;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/applications/MiuiAppInfoWithHeader;-><init>()V

    return-void
.end method

.method static oH(Landroid/content/Context;Lcom/android/settingslib/b/h;)Ljava/lang/CharSequence;
    .locals 4

    const/4 v3, 0x0

    invoke-static {p0}, Landroid/os/UserManager;->get(Landroid/content/Context;)Landroid/os/UserManager;

    move-result-object v0

    const-string/jumbo v1, "no_install_unknown_sources"

    iget-object v2, p1, Lcom/android/settingslib/b/h;->info:Landroid/content/pm/ApplicationInfo;

    iget v2, v2, Landroid/content/pm/ApplicationInfo;->uid:I

    invoke-static {v2}, Landroid/os/UserHandle;->getUserHandleForUid(I)Landroid/os/UserHandle;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/UserManager;->getUserRestrictionSource(Ljava/lang/String;Landroid/os/UserHandle;)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :pswitch_0
    iget-object v0, p1, Lcom/android/settingslib/b/h;->cBd:Ljava/lang/Object;

    instance-of v0, v0, Lcom/android/settings/applications/AppStateInstallAppsBridge$InstallAppsState;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/android/settingslib/b/h;->cBd:Ljava/lang/Object;

    check-cast v0, Lcom/android/settings/applications/AppStateInstallAppsBridge$InstallAppsState;

    :goto_0
    invoke-virtual {v0}, Lcom/android/settings/applications/AppStateInstallAppsBridge$InstallAppsState;->wr()Z

    move-result v0

    if-eqz v0, :cond_1

    const v0, 0x7f1206f0

    :goto_1
    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0

    :pswitch_1
    const v0, 0x7f1205d5

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0

    :pswitch_2
    const v0, 0x7f1205d2

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    new-instance v0, Lcom/android/settings/applications/AppStateInstallAppsBridge;

    invoke-direct {v0, p0, v3, v3}, Lcom/android/settings/applications/AppStateInstallAppsBridge;-><init>(Landroid/content/Context;Lcom/android/settingslib/b/a;Lcom/android/settings/applications/AppStateBaseBridge$Callback;)V

    iget-object v1, p1, Lcom/android/settingslib/b/h;->info:Landroid/content/pm/ApplicationInfo;

    iget-object v1, v1, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    iget-object v2, p1, Lcom/android/settingslib/b/h;->info:Landroid/content/pm/ApplicationInfo;

    iget v2, v2, Landroid/content/pm/ApplicationInfo;->uid:I

    invoke-virtual {v0, v1, v2}, Lcom/android/settings/applications/AppStateInstallAppsBridge;->wn(Ljava/lang/String;I)Lcom/android/settings/applications/AppStateInstallAppsBridge$InstallAppsState;

    move-result-object v0

    goto :goto_0

    :cond_1
    const v0, 0x7f1206f1

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private oI(Z)V
    .locals 5

    iget-object v1, p0, Lcom/android/settings/applications/ExternalSourcesDetails;->pi:Landroid/app/AppOpsManager;

    iget-object v0, p0, Lcom/android/settings/applications/ExternalSourcesDetails;->mPackageInfo:Landroid/content/pm/PackageInfo;

    iget-object v0, v0, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget v2, v0, Landroid/content/pm/ApplicationInfo;->uid:I

    iget-object v3, p0, Lcom/android/settings/applications/ExternalSourcesDetails;->mPackageName:Ljava/lang/String;

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    const/16 v4, 0x42

    invoke-virtual {v1, v4, v2, v3, v0}, Landroid/app/AppOpsManager;->setMode(IILjava/lang/String;I)V

    return-void

    :cond_0
    const/4 v0, 0x2

    goto :goto_0
.end method


# virtual methods
.method public getMetricsCategory()I
    .locals 1

    const/16 v0, 0x328

    return v0
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 5

    const/4 v1, 0x0

    const/4 v2, -0x1

    invoke-super {p0, p1, p2, p3}, Lcom/android/settings/applications/MiuiAppInfoWithHeader;->onActivityResult(IILandroid/content/Intent;)V

    const/16 v0, 0x65

    if-ne p1, v0, :cond_2

    if-ne p2, v2, :cond_3

    const/4 v0, 0x1

    :goto_0
    const-class v3, Lcom/android/settings/Settings$ManageAppExternalSourcesActivity;

    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0}, Lcom/android/settings/applications/ExternalSourcesDetails;->getIntent()Landroid/content/Intent;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    if-eqz v0, :cond_0

    move v1, v2

    :cond_0
    invoke-virtual {p0, v1}, Lcom/android/settings/applications/ExternalSourcesDetails;->bWF(I)V

    :cond_1
    invoke-direct {p0, v0}, Lcom/android/settings/applications/ExternalSourcesDetails;->oI(Z)V

    invoke-virtual {p0}, Lcom/android/settings/applications/ExternalSourcesDetails;->refreshUi()Z

    :cond_2
    return-void

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 4

    invoke-super {p0, p1}, Lcom/android/settings/applications/MiuiAppInfoWithHeader;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/android/settings/applications/ExternalSourcesDetails;->getActivity()Landroid/app/Activity;

    move-result-object v1

    new-instance v0, Lcom/android/settings/applications/AppStateInstallAppsBridge;

    iget-object v2, p0, Lcom/android/settings/applications/ExternalSourcesDetails;->mState:Lcom/android/settingslib/b/a;

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3}, Lcom/android/settings/applications/AppStateInstallAppsBridge;-><init>(Landroid/content/Context;Lcom/android/settingslib/b/a;Lcom/android/settings/applications/AppStateBaseBridge$Callback;)V

    iput-object v0, p0, Lcom/android/settings/applications/ExternalSourcesDetails;->ph:Lcom/android/settings/applications/AppStateInstallAppsBridge;

    const-string/jumbo v0, "appops"

    invoke-virtual {v1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AppOpsManager;

    iput-object v0, p0, Lcom/android/settings/applications/ExternalSourcesDetails;->pi:Landroid/app/AppOpsManager;

    invoke-static {v1}, Landroid/os/UserManager;->get(Landroid/content/Context;)Landroid/os/UserManager;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/applications/ExternalSourcesDetails;->mUserManager:Landroid/os/UserManager;

    const v0, 0x7f150058

    invoke-virtual {p0, v0}, Lcom/android/settings/applications/ExternalSourcesDetails;->addPreferencesFromResource(I)V

    const-string/jumbo v0, "external_sources_settings_switch"

    invoke-virtual {p0, v0}, Lcom/android/settings/applications/ExternalSourcesDetails;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Lcom/android/settingslib/MiuiRestrictedSwitchPreference;

    iput-object v0, p0, Lcom/android/settings/applications/ExternalSourcesDetails;->pk:Lcom/android/settingslib/MiuiRestrictedSwitchPreference;

    iget-object v0, p0, Lcom/android/settings/applications/ExternalSourcesDetails;->pk:Lcom/android/settingslib/MiuiRestrictedSwitchPreference;

    invoke-virtual {v0, p0}, Lcom/android/settingslib/MiuiRestrictedSwitchPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    return-void
.end method

.method public onDestroy()V
    .locals 1

    iget-object v0, p0, Lcom/android/settings/applications/ExternalSourcesDetails;->ph:Lcom/android/settings/applications/AppStateInstallAppsBridge;

    invoke-virtual {v0}, Lcom/android/settings/applications/AppStateInstallAppsBridge;->vX()V

    invoke-super {p0}, Lcom/android/settings/applications/MiuiAppInfoWithHeader;->onDestroy()V

    return-void
.end method

.method public onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 5

    const/4 v4, 0x1

    const/4 v0, 0x0

    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    iget-object v2, p0, Lcom/android/settings/applications/ExternalSourcesDetails;->pk:Lcom/android/settingslib/MiuiRestrictedSwitchPreference;

    if-ne p1, v2, :cond_4

    iget-object v2, p0, Lcom/android/settings/applications/ExternalSourcesDetails;->pj:Lcom/android/settings/applications/AppStateInstallAppsBridge$InstallAppsState;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/android/settings/applications/ExternalSourcesDetails;->pj:Lcom/android/settings/applications/AppStateInstallAppsBridge$InstallAppsState;

    invoke-virtual {v2}, Lcom/android/settings/applications/AppStateInstallAppsBridge$InstallAppsState;->wr()Z

    move-result v2

    if-eq v1, v2, :cond_3

    if-eqz v1, :cond_0

    sget-boolean v2, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z

    xor-int/lit8 v2, v2, 0x1

    if-eqz v2, :cond_0

    sget-boolean v2, Lmiui/os/Build;->IS_TABLET:Z

    xor-int/lit8 v2, v2, 0x1

    if-eqz v2, :cond_0

    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v2

    if-nez v2, :cond_0

    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v1, "com.miui.securitycenter.action.UNKNOWN_SOURCE_VERIFY"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string/jumbo v1, "com.miui.securitycenter"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    const/16 v1, 0x65

    invoke-virtual {p0, v0, v1}, Lcom/android/settings/applications/ExternalSourcesDetails;->startActivityForResult(Landroid/content/Intent;I)V

    return v4

    :cond_0
    const-class v2, Lcom/android/settings/Settings$ManageAppExternalSourcesActivity;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Lcom/android/settings/applications/ExternalSourcesDetails;->getIntent()Landroid/content/Intent;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    if-eqz v1, :cond_1

    const/4 v0, -0x1

    :cond_1
    invoke-virtual {p0, v0}, Lcom/android/settings/applications/ExternalSourcesDetails;->bWF(I)V

    :cond_2
    invoke-direct {p0, v1}, Lcom/android/settings/applications/ExternalSourcesDetails;->oI(Z)V

    invoke-virtual {p0}, Lcom/android/settings/applications/ExternalSourcesDetails;->refreshUi()Z

    :cond_3
    return v4

    :cond_4
    return v0
.end method

.method protected op(II)Landroid/app/AlertDialog;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method protected refreshUi()Z
    .locals 5

    const/4 v4, 0x0

    const/4 v3, 0x1

    iget-object v0, p0, Lcom/android/settings/applications/ExternalSourcesDetails;->mUserManager:Landroid/os/UserManager;

    const-string/jumbo v1, "no_install_unknown_sources"

    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v2

    invoke-static {v2}, Landroid/os/UserHandle;->of(I)Landroid/os/UserHandle;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/UserManager;->hasBaseUserRestriction(Ljava/lang/String;Landroid/os/UserHandle;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/applications/ExternalSourcesDetails;->pk:Lcom/android/settingslib/MiuiRestrictedSwitchPreference;

    invoke-virtual {v0, v4}, Lcom/android/settingslib/MiuiRestrictedSwitchPreference;->setChecked(Z)V

    iget-object v0, p0, Lcom/android/settings/applications/ExternalSourcesDetails;->pk:Lcom/android/settingslib/MiuiRestrictedSwitchPreference;

    const v1, 0x7f1205d2

    invoke-virtual {v0, v1}, Lcom/android/settingslib/MiuiRestrictedSwitchPreference;->setSummary(I)V

    iget-object v0, p0, Lcom/android/settings/applications/ExternalSourcesDetails;->pk:Lcom/android/settingslib/MiuiRestrictedSwitchPreference;

    invoke-virtual {v0, v4}, Lcom/android/settingslib/MiuiRestrictedSwitchPreference;->setEnabled(Z)V

    return v3

    :cond_0
    iget-object v0, p0, Lcom/android/settings/applications/ExternalSourcesDetails;->pk:Lcom/android/settingslib/MiuiRestrictedSwitchPreference;

    const-string/jumbo v1, "no_install_unknown_sources"

    invoke-virtual {v0, v1}, Lcom/android/settingslib/MiuiRestrictedSwitchPreference;->cqt(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/settings/applications/ExternalSourcesDetails;->pk:Lcom/android/settingslib/MiuiRestrictedSwitchPreference;

    invoke-virtual {v0}, Lcom/android/settingslib/MiuiRestrictedSwitchPreference;->cqw()Z

    move-result v0

    if-eqz v0, :cond_1

    return v3

    :cond_1
    iget-object v0, p0, Lcom/android/settings/applications/ExternalSourcesDetails;->ph:Lcom/android/settings/applications/AppStateInstallAppsBridge;

    iget-object v1, p0, Lcom/android/settings/applications/ExternalSourcesDetails;->mPackageName:Ljava/lang/String;

    iget-object v2, p0, Lcom/android/settings/applications/ExternalSourcesDetails;->mPackageInfo:Landroid/content/pm/PackageInfo;

    iget-object v2, v2, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget v2, v2, Landroid/content/pm/ApplicationInfo;->uid:I

    invoke-virtual {v0, v1, v2}, Lcom/android/settings/applications/AppStateInstallAppsBridge;->wn(Ljava/lang/String;I)Lcom/android/settings/applications/AppStateInstallAppsBridge$InstallAppsState;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/applications/ExternalSourcesDetails;->pj:Lcom/android/settings/applications/AppStateInstallAppsBridge$InstallAppsState;

    iget-object v0, p0, Lcom/android/settings/applications/ExternalSourcesDetails;->pj:Lcom/android/settings/applications/AppStateInstallAppsBridge$InstallAppsState;

    invoke-virtual {v0}, Lcom/android/settings/applications/AppStateInstallAppsBridge$InstallAppsState;->ws()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/android/settings/applications/ExternalSourcesDetails;->pk:Lcom/android/settingslib/MiuiRestrictedSwitchPreference;

    invoke-virtual {v0, v4}, Lcom/android/settingslib/MiuiRestrictedSwitchPreference;->setEnabled(Z)V

    return v3

    :cond_2
    iget-object v0, p0, Lcom/android/settings/applications/ExternalSourcesDetails;->pk:Lcom/android/settingslib/MiuiRestrictedSwitchPreference;

    iget-object v1, p0, Lcom/android/settings/applications/ExternalSourcesDetails;->pj:Lcom/android/settings/applications/AppStateInstallAppsBridge$InstallAppsState;

    invoke-virtual {v1}, Lcom/android/settings/applications/AppStateInstallAppsBridge$InstallAppsState;->wr()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/android/settingslib/MiuiRestrictedSwitchPreference;->setChecked(Z)V

    return v3
.end method
