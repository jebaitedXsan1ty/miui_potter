.class Lcom/android/settings/o;
.super Landroid/os/AsyncTask;
.source "TrustedCredentialsSettings.java"


# instance fields
.field private buY:Landroid/view/View;

.field private buZ:Landroid/widget/ProgressBar;

.field final synthetic bva:Lcom/android/settings/n;

.field private mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>(Lcom/android/settings/n;)V
    .locals 4

    iput-object p1, p0, Lcom/android/settings/o;->bva:Lcom/android/settings/n;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    iget-object v0, p1, Lcom/android/settings/n;->buX:Lcom/android/settings/TrustedCredentialsSettings;

    invoke-virtual {v0}, Lcom/android/settings/TrustedCredentialsSettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/o;->mContext:Landroid/content/Context;

    iget-object v0, p1, Lcom/android/settings/n;->buX:Lcom/android/settings/TrustedCredentialsSettings;

    invoke-static {v0}, Lcom/android/settings/TrustedCredentialsSettings;->bgW(Lcom/android/settings/TrustedCredentialsSettings;)Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0, p0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    iget-object v0, p1, Lcom/android/settings/n;->buX:Lcom/android/settings/TrustedCredentialsSettings;

    invoke-static {v0}, Lcom/android/settings/TrustedCredentialsSettings;->bhb(Lcom/android/settings/TrustedCredentialsSettings;)Landroid/os/UserManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/UserManager;->getUserProfiles()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/UserHandle;

    invoke-static {p1}, Lcom/android/settings/n;->bhR(Lcom/android/settings/n;)Landroid/util/SparseArray;

    move-result-object v2

    invoke-virtual {v0}, Landroid/os/UserHandle;->getIdentifier()I

    move-result v0

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {v2, v0, v3}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method private bhT()Z
    .locals 3

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/android/settings/o;->bva:Lcom/android/settings/n;

    iget-object v1, v1, Lcom/android/settings/n;->buX:Lcom/android/settings/TrustedCredentialsSettings;

    invoke-static {v1}, Lcom/android/settings/TrustedCredentialsSettings;->bhe(Lcom/android/settings/TrustedCredentialsSettings;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/settings/o;->bva:Lcom/android/settings/n;

    invoke-static {v1}, Lcom/android/settings/n;->bhS(Lcom/android/settings/n;)Lcom/android/settings/TrustedCredentialsSettings$Tab;

    move-result-object v1

    sget-object v2, Lcom/android/settings/TrustedCredentialsSettings$Tab;->buy:Lcom/android/settings/TrustedCredentialsSettings$Tab;

    if-ne v1, v2, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method private bhW(Landroid/os/UserHandle;)Z
    .locals 2

    iget-object v0, p0, Lcom/android/settings/o;->bva:Lcom/android/settings/n;

    iget-object v0, v0, Lcom/android/settings/n;->buX:Lcom/android/settings/TrustedCredentialsSettings;

    invoke-static {v0}, Lcom/android/settings/TrustedCredentialsSettings;->bhb(Lcom/android/settings/TrustedCredentialsSettings;)Landroid/os/UserManager;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/os/UserManager;->isQuietModeEnabled(Landroid/os/UserHandle;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/o;->bva:Lcom/android/settings/n;

    iget-object v0, v0, Lcom/android/settings/n;->buX:Lcom/android/settings/TrustedCredentialsSettings;

    invoke-static {v0}, Lcom/android/settings/TrustedCredentialsSettings;->bhb(Lcom/android/settings/TrustedCredentialsSettings;)Landroid/os/UserManager;

    move-result-object v0

    invoke-virtual {p1}, Landroid/os/UserHandle;->getIdentifier()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/os/UserManager;->isUserUnlocked(I)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private bhX()V
    .locals 6

    invoke-direct {p0}, Lcom/android/settings/o;->bhT()Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/settings/o;->bva:Lcom/android/settings/n;

    invoke-static {v0}, Lcom/android/settings/n;->bhR(Lcom/android/settings/n;)Landroid/util/SparseArray;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/o;->bva:Lcom/android/settings/n;

    iget-object v1, v1, Lcom/android/settings/n;->buX:Lcom/android/settings/TrustedCredentialsSettings;

    invoke-static {v1}, Lcom/android/settings/TrustedCredentialsSettings;->bha(Lcom/android/settings/TrustedCredentialsSettings;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    if-nez v0, :cond_1

    return-void

    :cond_1
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iget-object v1, p0, Lcom/android/settings/o;->mContext:Landroid/content/Context;

    const-class v3, Landroid/app/admin/DevicePolicyManager;

    invoke-virtual {v1, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/admin/DevicePolicyManager;

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_2
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settings/p;

    if-eqz v0, :cond_2

    invoke-static {v0}, Lcom/android/settings/p;->bie(Lcom/android/settings/p;)Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/android/settings/o;->bva:Lcom/android/settings/n;

    iget-object v5, v5, Lcom/android/settings/n;->buX:Lcom/android/settings/TrustedCredentialsSettings;

    invoke-static {v5}, Lcom/android/settings/TrustedCredentialsSettings;->bha(Lcom/android/settings/TrustedCredentialsSettings;)I

    move-result v5

    invoke-virtual {v1, v4, v5}, Landroid/app/admin/DevicePolicyManager;->isCaCertApproved(Ljava/lang/String;I)Z

    move-result v4

    xor-int/lit8 v4, v4, 0x1

    if-eqz v4, :cond_2

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_3
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_4

    const-string/jumbo v0, "TrustedCredentialsSettings"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "no cert is pending approval for user "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/settings/o;->bva:Lcom/android/settings/n;

    iget-object v2, v2, Lcom/android/settings/n;->buX:Lcom/android/settings/TrustedCredentialsSettings;

    invoke-static {v2}, Lcom/android/settings/TrustedCredentialsSettings;->bha(Lcom/android/settings/TrustedCredentialsSettings;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :cond_4
    iget-object v0, p0, Lcom/android/settings/o;->bva:Lcom/android/settings/n;

    iget-object v0, v0, Lcom/android/settings/n;->buX:Lcom/android/settings/TrustedCredentialsSettings;

    invoke-static {v0, v2}, Lcom/android/settings/TrustedCredentialsSettings;->bhh(Lcom/android/settings/TrustedCredentialsSettings;Ljava/util/List;)V

    return-void
.end method


# virtual methods
.method protected bhU(Landroid/util/SparseArray;)V
    .locals 6

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/android/settings/o;->bva:Lcom/android/settings/n;

    invoke-static {v0}, Lcom/android/settings/n;->bhR(Lcom/android/settings/n;)Landroid/util/SparseArray;

    move-result-object v0

    invoke-virtual {v0}, Landroid/util/SparseArray;->clear()V

    invoke-virtual {p1}, Landroid/util/SparseArray;->size()I

    move-result v3

    move v1, v2

    :goto_0
    if-ge v1, v3, :cond_0

    iget-object v0, p0, Lcom/android/settings/o;->bva:Lcom/android/settings/n;

    invoke-static {v0}, Lcom/android/settings/n;->bhR(Lcom/android/settings/n;)Landroid/util/SparseArray;

    move-result-object v4

    invoke-virtual {p1, v1}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v5

    invoke-virtual {p1, v1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    invoke-virtual {v4, v5, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/android/settings/o;->bva:Lcom/android/settings/n;

    invoke-static {v0}, Lcom/android/settings/n;->bhQ(Lcom/android/settings/n;)Lcom/android/settings/k;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/settings/k;->notifyDataSetChanged()V

    iget-object v0, p0, Lcom/android/settings/o;->buZ:Landroid/widget/ProgressBar;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/settings/o;->buY:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/settings/o;->buZ:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v2}, Landroid/widget/ProgressBar;->setProgress(I)V

    iget-object v0, p0, Lcom/android/settings/o;->bva:Lcom/android/settings/n;

    iget-object v0, v0, Lcom/android/settings/n;->buX:Lcom/android/settings/TrustedCredentialsSettings;

    invoke-static {v0}, Lcom/android/settings/TrustedCredentialsSettings;->bgW(Lcom/android/settings/TrustedCredentialsSettings;)Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0, p0}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    invoke-direct {p0}, Lcom/android/settings/o;->bhX()V

    return-void
.end method

.method protected varargs bhV([Ljava/lang/Integer;)V
    .locals 3

    const/4 v0, 0x0

    aget-object v0, p1, v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/4 v1, 0x1

    aget-object v1, p1, v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    iget-object v2, p0, Lcom/android/settings/o;->buZ:Landroid/widget/ProgressBar;

    invoke-virtual {v2}, Landroid/widget/ProgressBar;->getMax()I

    move-result v2

    if-eq v1, v2, :cond_0

    iget-object v2, p0, Lcom/android/settings/o;->buZ:Landroid/widget/ProgressBar;

    invoke-virtual {v2, v1}, Landroid/widget/ProgressBar;->setMax(I)V

    :cond_0
    iget-object v1, p0, Lcom/android/settings/o;->buZ:Landroid/widget/ProgressBar;

    invoke-virtual {v1, v0}, Landroid/widget/ProgressBar;->setProgress(I)V

    return-void
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Landroid/util/SparseArray;
    .locals 22

    new-instance v15, Landroid/util/SparseArray;

    invoke-direct {v15}, Landroid/util/SparseArray;-><init>()V

    :try_start_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/settings/o;->bva:Lcom/android/settings/n;

    iget-object v2, v2, Lcom/android/settings/n;->buX:Lcom/android/settings/TrustedCredentialsSettings;

    invoke-static {v2}, Lcom/android/settings/TrustedCredentialsSettings;->bgY(Lcom/android/settings/TrustedCredentialsSettings;)Landroid/util/SparseArray;

    move-result-object v16

    monitor-enter v16
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_1

    :try_start_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/settings/o;->bva:Lcom/android/settings/n;

    iget-object v2, v2, Lcom/android/settings/n;->buX:Lcom/android/settings/TrustedCredentialsSettings;

    invoke-static {v2}, Lcom/android/settings/TrustedCredentialsSettings;->bhb(Lcom/android/settings/TrustedCredentialsSettings;)Landroid/os/UserManager;

    move-result-object v2

    invoke-virtual {v2}, Landroid/os/UserManager;->getUserProfiles()Ljava/util/List;

    move-result-object v17

    invoke-interface/range {v17 .. v17}, Ljava/util/List;->size()I

    move-result v18

    new-instance v19, Landroid/util/SparseArray;

    move-object/from16 v0, v19

    move/from16 v1, v18

    invoke-direct {v0, v1}, Landroid/util/SparseArray;-><init>(I)V

    const/4 v11, 0x0

    const/4 v4, 0x0

    const/4 v2, 0x0

    move v3, v2

    :goto_0
    move/from16 v0, v18

    if-ge v3, v0, :cond_2

    move-object/from16 v0, v17

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/os/UserHandle;

    invoke-virtual {v2}, Landroid/os/UserHandle;->getIdentifier()I

    move-result v5

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/android/settings/o;->bhW(Landroid/os/UserHandle;)Z

    move-result v6

    if-eqz v6, :cond_0

    move v2, v11

    :goto_1
    add-int/lit8 v3, v3, 0x1

    move v11, v2

    goto :goto_0

    :cond_0
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/settings/o;->mContext:Landroid/content/Context;

    invoke-static {v6, v2}, Landroid/security/KeyChain;->bindAsUser(Landroid/content/Context;Landroid/os/UserHandle;)Landroid/security/KeyChain$KeyChainConnection;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/settings/o;->bva:Lcom/android/settings/n;

    iget-object v6, v6, Lcom/android/settings/n;->buX:Lcom/android/settings/TrustedCredentialsSettings;

    invoke-static {v6}, Lcom/android/settings/TrustedCredentialsSettings;->bgY(Lcom/android/settings/TrustedCredentialsSettings;)Landroid/util/SparseArray;

    move-result-object v6

    invoke-virtual {v6, v5, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    invoke-virtual {v2}, Landroid/security/KeyChain$KeyChainConnection;->getService()Landroid/security/IKeyChainService;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/settings/o;->bva:Lcom/android/settings/n;

    invoke-static {v6}, Lcom/android/settings/n;->bhS(Lcom/android/settings/n;)Lcom/android/settings/TrustedCredentialsSettings$Tab;

    move-result-object v6

    invoke-static {v6, v2}, Lcom/android/settings/TrustedCredentialsSettings$Tab;->bhr(Lcom/android/settings/TrustedCredentialsSettings$Tab;Landroid/security/IKeyChainService;)Ljava/util/List;

    move-result-object v6

    invoke-virtual/range {p0 .. p0}, Lcom/android/settings/o;->isCancelled()Z

    move-result v2

    if-eqz v2, :cond_1

    new-instance v2, Landroid/util/SparseArray;

    invoke-direct {v2}, Landroid/util/SparseArray;-><init>()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    monitor-exit v16
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_1

    return-object v2

    :cond_1
    :try_start_3
    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v2

    add-int/2addr v2, v11

    move-object/from16 v0, v19

    invoke-virtual {v0, v5, v6}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v2

    :try_start_4
    monitor-exit v16

    throw v2
    :try_end_4
    .catch Landroid/os/RemoteException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/lang/InterruptedException; {:try_start_4 .. :try_end_4} :catch_1

    :catch_0
    move-exception v2

    const-string/jumbo v3, "TrustedCredentialsSettings"

    const-string/jumbo v4, "Remote exception while loading aliases."

    invoke-static {v3, v4, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    new-instance v2, Landroid/util/SparseArray;

    invoke-direct {v2}, Landroid/util/SparseArray;-><init>()V

    return-object v2

    :cond_2
    const/4 v2, 0x0

    move v14, v2

    :goto_2
    move/from16 v0, v18

    if-ge v14, v0, :cond_7

    :try_start_5
    move-object/from16 v0, v17

    invoke-interface {v0, v14}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/os/UserHandle;

    invoke-virtual {v2}, Landroid/os/UserHandle;->getIdentifier()I

    move-result v8

    move-object/from16 v0, v19

    invoke-virtual {v0, v8}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v3

    move-object v0, v3

    check-cast v0, Ljava/util/List;

    move-object v10, v0

    invoke-virtual/range {p0 .. p0}, Lcom/android/settings/o;->isCancelled()Z

    move-result v3

    if-eqz v3, :cond_3

    new-instance v2, Landroid/util/SparseArray;

    invoke-direct {v2}, Landroid/util/SparseArray;-><init>()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    :try_start_6
    monitor-exit v16
    :try_end_6
    .catch Landroid/os/RemoteException; {:try_start_6 .. :try_end_6} :catch_0
    .catch Ljava/lang/InterruptedException; {:try_start_6 .. :try_end_6} :catch_1

    return-object v2

    :cond_3
    :try_start_7
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/settings/o;->bva:Lcom/android/settings/n;

    iget-object v3, v3, Lcom/android/settings/n;->buX:Lcom/android/settings/TrustedCredentialsSettings;

    invoke-static {v3}, Lcom/android/settings/TrustedCredentialsSettings;->bgY(Lcom/android/settings/TrustedCredentialsSettings;)Landroid/util/SparseArray;

    move-result-object v3

    invoke-virtual {v3, v8}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/security/KeyChain$KeyChainConnection;

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/android/settings/o;->bhW(Landroid/os/UserHandle;)Z

    move-result v2

    if-nez v2, :cond_4

    if-nez v10, :cond_5

    :cond_4
    new-instance v2, Ljava/util/ArrayList;

    const/4 v3, 0x0

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(I)V

    invoke-virtual {v15, v8, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    move v2, v4

    :goto_3
    add-int/lit8 v3, v14, 0x1

    move v14, v3

    move v4, v2

    goto :goto_2

    :cond_5
    if-eqz v3, :cond_4

    invoke-virtual {v3}, Landroid/security/KeyChain$KeyChainConnection;->getService()Landroid/security/IKeyChainService;

    move-result-object v3

    new-instance v20, Ljava/util/ArrayList;

    move-object/from16 v0, v20

    invoke-direct {v0, v11}, Ljava/util/ArrayList;-><init>(I)V

    invoke-interface {v10}, Ljava/util/List;->size()I

    move-result v21

    const/4 v2, 0x0

    move v13, v2

    move v12, v4

    :goto_4
    move/from16 v0, v21

    if-ge v13, v0, :cond_6

    invoke-interface {v10, v13}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    const/4 v2, 0x1

    invoke-interface {v3, v6, v2}, Landroid/security/IKeyChainService;->getEncodedCaCertificate(Ljava/lang/String;Z)[B

    move-result-object v2

    invoke-static {v2}, Landroid/security/KeyChain;->toCertificate([B)Ljava/security/cert/X509Certificate;

    move-result-object v7

    new-instance v2, Lcom/android/settings/p;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/settings/o;->bva:Lcom/android/settings/n;

    invoke-static {v4}, Lcom/android/settings/n;->bhQ(Lcom/android/settings/n;)Lcom/android/settings/k;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/settings/o;->bva:Lcom/android/settings/n;

    invoke-static {v5}, Lcom/android/settings/n;->bhS(Lcom/android/settings/n;)Lcom/android/settings/TrustedCredentialsSettings$Tab;

    move-result-object v5

    const/4 v9, 0x0

    invoke-direct/range {v2 .. v9}, Lcom/android/settings/p;-><init>(Landroid/security/IKeyChainService;Lcom/android/settings/k;Lcom/android/settings/TrustedCredentialsSettings$Tab;Ljava/lang/String;Ljava/security/cert/X509Certificate;ILcom/android/settings/p;)V

    move-object/from16 v0, v20

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Integer;

    add-int/lit8 v12, v12, 0x1

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    const/4 v5, 0x0

    aput-object v4, v2, v5

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    const/4 v5, 0x1

    aput-object v4, v2, v5

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/android/settings/o;->publishProgress([Ljava/lang/Object;)V

    add-int/lit8 v2, v13, 0x1

    move v13, v2

    goto :goto_4

    :cond_6
    invoke-static/range {v20 .. v20}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    move-object/from16 v0, v20

    invoke-virtual {v15, v8, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    move v2, v12

    goto :goto_3

    :cond_7
    :try_start_8
    monitor-exit v16
    :try_end_8
    .catch Landroid/os/RemoteException; {:try_start_8 .. :try_end_8} :catch_0
    .catch Ljava/lang/InterruptedException; {:try_start_8 .. :try_end_8} :catch_1

    return-object v15

    :catch_1
    move-exception v2

    const-string/jumbo v3, "TrustedCredentialsSettings"

    const-string/jumbo v4, "InterruptedException while loading aliases."

    invoke-static {v3, v4, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    new-instance v2, Landroid/util/SparseArray;

    invoke-direct {v2}, Landroid/util/SparseArray;-><init>()V

    return-object v2
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/android/settings/o;->doInBackground([Ljava/lang/Void;)Landroid/util/SparseArray;

    move-result-object v0

    return-object v0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    check-cast p1, Landroid/util/SparseArray;

    invoke-virtual {p0, p1}, Lcom/android/settings/o;->bhU(Landroid/util/SparseArray;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/o;->bva:Lcom/android/settings/n;

    iget-object v0, v0, Lcom/android/settings/n;->buX:Lcom/android/settings/TrustedCredentialsSettings;

    invoke-static {v0}, Lcom/android/settings/TrustedCredentialsSettings;->bgZ(Lcom/android/settings/TrustedCredentialsSettings;)Landroid/widget/TabHost;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/TabHost;->getTabContentView()Landroid/widget/FrameLayout;

    move-result-object v1

    iget-object v0, p0, Lcom/android/settings/o;->bva:Lcom/android/settings/n;

    invoke-static {v0}, Lcom/android/settings/n;->bhS(Lcom/android/settings/n;)Lcom/android/settings/TrustedCredentialsSettings$Tab;

    move-result-object v0

    invoke-static {v0}, Lcom/android/settings/TrustedCredentialsSettings$Tab;->bhm(Lcom/android/settings/TrustedCredentialsSettings$Tab;)I

    move-result v0

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/android/settings/o;->buZ:Landroid/widget/ProgressBar;

    iget-object v0, p0, Lcom/android/settings/o;->bva:Lcom/android/settings/n;

    invoke-static {v0}, Lcom/android/settings/n;->bhS(Lcom/android/settings/n;)Lcom/android/settings/TrustedCredentialsSettings$Tab;

    move-result-object v0

    invoke-static {v0}, Lcom/android/settings/TrustedCredentialsSettings$Tab;->bhk(Lcom/android/settings/TrustedCredentialsSettings$Tab;)I

    move-result v0

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/o;->buY:Landroid/view/View;

    iget-object v0, p0, Lcom/android/settings/o;->buZ:Landroid/widget/ProgressBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/settings/o;->buY:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method protected bridge synthetic onProgressUpdate([Ljava/lang/Object;)V
    .locals 0

    check-cast p1, [Ljava/lang/Integer;

    invoke-virtual {p0, p1}, Lcom/android/settings/o;->bhV([Ljava/lang/Integer;)V

    return-void
.end method
