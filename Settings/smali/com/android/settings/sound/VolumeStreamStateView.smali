.class public Lcom/android/settings/sound/VolumeStreamStateView;
.super Landroid/widget/ImageView;
.source "VolumeStreamStateView.java"


# static fields
.field private static final Mh:[I

.field private static final Mk:[I

.field private static final Ml:[I


# instance fields
.field private final Mi:I

.field private final Mj:I

.field private Mm:I


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-array v0, v3, [I

    const v1, 0x7f040141

    aput v1, v0, v2

    sput-object v0, Lcom/android/settings/sound/VolumeStreamStateView;->Mk:[I

    new-array v0, v3, [I

    const v1, 0x7f040143

    aput v1, v0, v2

    sput-object v0, Lcom/android/settings/sound/VolumeStreamStateView;->Ml:[I

    new-array v0, v3, [I

    const v1, 0x7f04013d

    aput v1, v0, v2

    sput-object v0, Lcom/android/settings/sound/VolumeStreamStateView;->Mh:[I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    invoke-direct {p0, p1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    const/4 v0, -0x1

    iput v0, p0, Lcom/android/settings/sound/VolumeStreamStateView;->Mm:I

    const/16 v0, 0xc

    iput v0, p0, Lcom/android/settings/sound/VolumeStreamStateView;->Mj:I

    const/16 v0, 0x380

    iput v0, p0, Lcom/android/settings/sound/VolumeStreamStateView;->Mi:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    invoke-direct {p0, p1, p2}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/4 v0, -0x1

    iput v0, p0, Lcom/android/settings/sound/VolumeStreamStateView;->Mm:I

    const/16 v0, 0xc

    iput v0, p0, Lcom/android/settings/sound/VolumeStreamStateView;->Mj:I

    const/16 v0, 0x380

    iput v0, p0, Lcom/android/settings/sound/VolumeStreamStateView;->Mi:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    const/4 v0, -0x1

    iput v0, p0, Lcom/android/settings/sound/VolumeStreamStateView;->Mm:I

    const/16 v0, 0xc

    iput v0, p0, Lcom/android/settings/sound/VolumeStreamStateView;->Mj:I

    const/16 v0, 0x380

    iput v0, p0, Lcom/android/settings/sound/VolumeStreamStateView;->Mi:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V
    .locals 1

    invoke-direct {p0, p1, p2, p3, p4}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V

    const/4 v0, -0x1

    iput v0, p0, Lcom/android/settings/sound/VolumeStreamStateView;->Mm:I

    const/16 v0, 0xc

    iput v0, p0, Lcom/android/settings/sound/VolumeStreamStateView;->Mj:I

    const/16 v0, 0x380

    iput v0, p0, Lcom/android/settings/sound/VolumeStreamStateView;->Mi:I

    return-void
.end method


# virtual methods
.method public onCreateDrawableState(I)[I
    .locals 4

    add-int/lit8 v0, p1, 0x3

    invoke-super {p0, v0}, Landroid/widget/ImageView;->onCreateDrawableState(I)[I

    move-result-object v1

    iget v0, p0, Lcom/android/settings/sound/VolumeStreamStateView;->Mm:I

    const/4 v2, -0x1

    if-eq v0, v2, :cond_1

    invoke-virtual {p0}, Lcom/android/settings/sound/VolumeStreamStateView;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string/jumbo v2, "audio"

    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    iget v2, p0, Lcom/android/settings/sound/VolumeStreamStateView;->Mm:I

    invoke-virtual {v0, v2}, Landroid/media/AudioManager;->getDevicesForStream(I)I

    move-result v2

    iget v3, p0, Lcom/android/settings/sound/VolumeStreamStateView;->Mm:I

    invoke-virtual {v0, v3}, Landroid/media/AudioManager;->getStreamVolume(I)I

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/android/settings/sound/VolumeStreamStateView;->Mk:[I

    invoke-static {v1, v0}, Lcom/android/settings/sound/VolumeStreamStateView;->mergeDrawableStates([I[I)[I

    :cond_0
    and-int/lit8 v0, v2, 0xc

    if-eqz v0, :cond_2

    sget-object v0, Lcom/android/settings/sound/VolumeStreamStateView;->Ml:[I

    invoke-static {v1, v0}, Lcom/android/settings/sound/VolumeStreamStateView;->mergeDrawableStates([I[I)[I

    :cond_1
    :goto_0
    return-object v1

    :cond_2
    and-int/lit16 v0, v2, 0x380

    if-eqz v0, :cond_1

    sget-object v0, Lcom/android/settings/sound/VolumeStreamStateView;->Mh:[I

    invoke-static {v1, v0}, Lcom/android/settings/sound/VolumeStreamStateView;->mergeDrawableStates([I[I)[I

    goto :goto_0
.end method

.method public setStream(I)V
    .locals 0

    iput p1, p0, Lcom/android/settings/sound/VolumeStreamStateView;->Mm:I

    return-void
.end method
