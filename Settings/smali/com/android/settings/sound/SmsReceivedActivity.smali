.class public Lcom/android/settings/sound/SmsReceivedActivity;
.super Lcom/android/settings/sound/a;
.source "SmsReceivedActivity.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/sound/a;-><init>()V

    return-void
.end method


# virtual methods
.method protected Ge()I
    .locals 1

    const v0, 0x7f1210eb

    return v0
.end method

.method protected Gf()Landroid/content/Intent;
    .locals 3

    const/4 v2, 0x2

    invoke-super {p0}, Lcom/android/settings/sound/a;->Gf()Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "android.intent.extra.ringtone.TYPE"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string/jumbo v1, "android.intent.extra.ringtone.DEFAULT_URI"

    invoke-static {v2}, Landroid/media/RingtoneManager;->getDefaultUri(I)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    return-object v0
.end method

.method protected getRingtoneType()I
    .locals 1

    const/16 v0, 0x10

    return v0
.end method
