.class final Lcom/android/settings/sound/j;
.super Landroid/content/BroadcastReceiver;
.source "RingerVolumeFragment.java"


# instance fields
.field final synthetic Ni:Lcom/android/settings/sound/RingerVolumeFragment;


# direct methods
.method constructor <init>(Lcom/android/settings/sound/RingerVolumeFragment;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/sound/j;->Ni:Lcom/android/settings/sound/RingerVolumeFragment;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 5

    const/4 v4, 0x0

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "android.media.VOLUME_CHANGED_ACTION"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string/jumbo v0, "android.media.EXTRA_VOLUME_STREAM_TYPE"

    const/4 v1, -0x1

    invoke-virtual {p2, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    if-ltz v0, :cond_0

    iget-object v1, p0, Lcom/android/settings/sound/j;->Ni:Lcom/android/settings/sound/RingerVolumeFragment;

    invoke-static {v1}, Lcom/android/settings/sound/RingerVolumeFragment;->GN(Lcom/android/settings/sound/RingerVolumeFragment;)Landroid/os/Handler;

    move-result-object v1

    iget-object v2, p0, Lcom/android/settings/sound/j;->Ni:Lcom/android/settings/sound/RingerVolumeFragment;

    invoke-static {v2}, Lcom/android/settings/sound/RingerVolumeFragment;->GN(Lcom/android/settings/sound/RingerVolumeFragment;)Landroid/os/Handler;

    move-result-object v2

    const-string/jumbo v3, "android.media.EXTRA_VOLUME_STREAM_VALUE"

    invoke-virtual {p2, v3, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v3

    const/16 v4, 0x66

    invoke-virtual {v2, v4, v0, v3}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    :cond_0
    return-void
.end method
