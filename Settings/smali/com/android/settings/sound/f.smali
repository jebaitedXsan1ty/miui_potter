.class final Lcom/android/settings/sound/f;
.super Landroid/database/ContentObserver;
.source "RingerVolumeFragment.java"


# instance fields
.field final synthetic Ne:Lcom/android/settings/sound/d;


# direct methods
.method constructor <init>(Lcom/android/settings/sound/d;Landroid/os/Handler;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/sound/f;->Ne:Lcom/android/settings/sound/d;

    invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    return-void
.end method


# virtual methods
.method public onChange(Z)V
    .locals 3

    invoke-super {p0, p1}, Landroid/database/ContentObserver;->onChange(Z)V

    iget-object v0, p0, Lcom/android/settings/sound/f;->Ne:Lcom/android/settings/sound/d;

    invoke-static {v0}, Lcom/android/settings/sound/d;->Hc(Lcom/android/settings/sound/d;)Lmiui/widget/SeekBar;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/sound/f;->Ne:Lcom/android/settings/sound/d;

    invoke-static {v0}, Lcom/android/settings/sound/d;->Hb(Lcom/android/settings/sound/d;)Landroid/media/AudioManager;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/sound/f;->Ne:Lcom/android/settings/sound/d;

    invoke-static {v0}, Lcom/android/settings/sound/d;->Hb(Lcom/android/settings/sound/d;)Landroid/media/AudioManager;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/sound/f;->Ne:Lcom/android/settings/sound/d;

    invoke-static {v1}, Lcom/android/settings/sound/d;->Hd(Lcom/android/settings/sound/d;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->isStreamMute(I)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/settings/sound/f;->Ne:Lcom/android/settings/sound/d;

    invoke-static {v0}, Lcom/android/settings/sound/d;->Hb(Lcom/android/settings/sound/d;)Landroid/media/AudioManager;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/sound/f;->Ne:Lcom/android/settings/sound/d;

    invoke-static {v1}, Lcom/android/settings/sound/d;->Hd(Lcom/android/settings/sound/d;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->getLastAudibleStreamVolume(I)I

    move-result v0

    :goto_0
    iget-object v1, p0, Lcom/android/settings/sound/f;->Ne:Lcom/android/settings/sound/d;

    iget-object v2, p0, Lcom/android/settings/sound/f;->Ne:Lcom/android/settings/sound/d;

    invoke-static {v2}, Lcom/android/settings/sound/d;->Hc(Lcom/android/settings/sound/d;)Lmiui/widget/SeekBar;

    move-result-object v2

    invoke-virtual {v2}, Lmiui/widget/SeekBar;->getProgress()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/android/settings/sound/d;->GT(I)I

    move-result v1

    if-eq v0, v1, :cond_0

    iget-object v1, p0, Lcom/android/settings/sound/f;->Ne:Lcom/android/settings/sound/d;

    invoke-static {v1}, Lcom/android/settings/sound/d;->Hc(Lcom/android/settings/sound/d;)Lmiui/widget/SeekBar;

    move-result-object v1

    iget-object v2, p0, Lcom/android/settings/sound/f;->Ne:Lcom/android/settings/sound/d;

    invoke-virtual {v2, v0}, Lcom/android/settings/sound/d;->GS(I)I

    move-result v2

    invoke-virtual {v1, v2}, Lmiui/widget/SeekBar;->setProgress(I)V

    iget-object v1, p0, Lcom/android/settings/sound/f;->Ne:Lcom/android/settings/sound/d;

    invoke-static {v1, v0}, Lcom/android/settings/sound/d;->He(Lcom/android/settings/sound/d;I)I

    :cond_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/settings/sound/f;->Ne:Lcom/android/settings/sound/d;

    invoke-static {v0}, Lcom/android/settings/sound/d;->Hb(Lcom/android/settings/sound/d;)Landroid/media/AudioManager;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/sound/f;->Ne:Lcom/android/settings/sound/d;

    invoke-static {v1}, Lcom/android/settings/sound/d;->Hd(Lcom/android/settings/sound/d;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->getStreamVolume(I)I

    move-result v0

    goto :goto_0
.end method
