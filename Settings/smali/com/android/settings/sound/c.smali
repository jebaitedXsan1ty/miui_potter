.class final Lcom/android/settings/sound/c;
.super Landroid/content/BroadcastReceiver;
.source "SeekBarVolumizer.java"


# instance fields
.field private Mx:Z

.field final synthetic My:Lcom/android/settings/sound/b;


# direct methods
.method private constructor <init>(Lcom/android/settings/sound/b;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/sound/c;->My:Lcom/android/settings/sound/b;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/settings/sound/b;Lcom/android/settings/sound/c;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/settings/sound/c;-><init>(Lcom/android/settings/sound/b;)V

    return-void
.end method


# virtual methods
.method public GB(Z)V
    .locals 2

    iget-boolean v0, p0, Lcom/android/settings/sound/c;->Mx:Z

    if-ne v0, p1, :cond_0

    return-void

    :cond_0
    iput-boolean p1, p0, Lcom/android/settings/sound/c;->Mx:Z

    if-eqz p1, :cond_1

    new-instance v0, Landroid/content/IntentFilter;

    const-string/jumbo v1, "android.media.VOLUME_CHANGED_ACTION"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    const-string/jumbo v1, "android.media.RINGER_MODE_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string/jumbo v1, "android.intent.action.HEADSET_PLUG"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/android/settings/sound/c;->My:Lcom/android/settings/sound/b;

    invoke-static {v1}, Lcom/android/settings/sound/b;->Gw(Lcom/android/settings/sound/b;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, p0, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/settings/sound/c;->My:Lcom/android/settings/sound/b;

    invoke-static {v0}, Lcom/android/settings/sound/b;->Gw(Lcom/android/settings/sound/b;)Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    goto :goto_0
.end method

.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 3

    const/4 v2, -0x1

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "android.media.VOLUME_CHANGED_ACTION"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const-string/jumbo v0, "android.media.EXTRA_VOLUME_STREAM_TYPE"

    invoke-virtual {p2, v0, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    const-string/jumbo v1, "android.media.EXTRA_VOLUME_STREAM_VALUE"

    invoke-virtual {p2, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    iget-object v2, p0, Lcom/android/settings/sound/c;->My:Lcom/android/settings/sound/b;

    invoke-static {v2}, Lcom/android/settings/sound/b;->Gz(Lcom/android/settings/sound/b;)I

    move-result v2

    if-ne v0, v2, :cond_0

    iget-object v0, p0, Lcom/android/settings/sound/c;->My:Lcom/android/settings/sound/b;

    invoke-static {v0}, Lcom/android/settings/sound/b;->Gy(Lcom/android/settings/sound/b;)Landroid/widget/SeekBar;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/sound/c;->My:Lcom/android/settings/sound/b;

    iget-object v2, p0, Lcom/android/settings/sound/c;->My:Lcom/android/settings/sound/b;

    invoke-static {v2}, Lcom/android/settings/sound/b;->Gy(Lcom/android/settings/sound/b;)Landroid/widget/SeekBar;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/SeekBar;->getProgress()I

    move-result v2

    invoke-virtual {v0, v2}, Lcom/android/settings/sound/b;->Gs(I)I

    move-result v0

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/android/settings/sound/c;->My:Lcom/android/settings/sound/b;

    invoke-static {v0}, Lcom/android/settings/sound/b;->Gy(Lcom/android/settings/sound/b;)Landroid/widget/SeekBar;

    move-result-object v0

    iget-object v2, p0, Lcom/android/settings/sound/c;->My:Lcom/android/settings/sound/b;

    invoke-virtual {v2, v1}, Lcom/android/settings/sound/b;->Gu(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setProgress(I)V

    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/android/settings/sound/c;->My:Lcom/android/settings/sound/b;

    invoke-static {v0}, Lcom/android/settings/sound/b;->Gx(Lcom/android/settings/sound/b;)Lcom/android/settings/sound/VolumeSeekBarPreference;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/settings/sound/VolumeSeekBarPreference;->GE()V

    return-void

    :cond_1
    const-string/jumbo v1, "android.media.RINGER_MODE_CHANGED"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v0, p0, Lcom/android/settings/sound/c;->My:Lcom/android/settings/sound/b;

    invoke-static {v0}, Lcom/android/settings/sound/b;->Gz(Lcom/android/settings/sound/b;)I

    move-result v0

    iget-object v1, p0, Lcom/android/settings/sound/c;->My:Lcom/android/settings/sound/b;

    invoke-static {v1}, Lcom/android/settings/sound/b;->Gv(Lcom/android/settings/sound/b;)Landroid/media/AudioManager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/media/AudioManager;->getUiSoundsStreamType()I

    move-result v1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/android/settings/sound/c;->My:Lcom/android/settings/sound/b;

    invoke-static {v0}, Lcom/android/settings/sound/b;->Gw(Lcom/android/settings/sound/b;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lmiui/util/AudioManagerHelper;->isSilentEnabled(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/sound/c;->My:Lcom/android/settings/sound/b;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/android/settings/sound/b;->GA(Lcom/android/settings/sound/b;Z)V

    goto :goto_0

    :cond_2
    const-string/jumbo v1, "android.intent.action.HEADSET_PLUG"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/sound/c;->My:Lcom/android/settings/sound/b;

    invoke-static {v0}, Lcom/android/settings/sound/b;->Gz(Lcom/android/settings/sound/b;)I

    move-result v0

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/android/settings/sound/c;->My:Lcom/android/settings/sound/b;

    invoke-static {v0}, Lcom/android/settings/sound/b;->Gv(Lcom/android/settings/sound/b;)Landroid/media/AudioManager;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/sound/c;->My:Lcom/android/settings/sound/b;

    invoke-static {v1}, Lcom/android/settings/sound/b;->Gz(Lcom/android/settings/sound/b;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->getStreamVolume(I)I

    move-result v0

    iget-object v1, p0, Lcom/android/settings/sound/c;->My:Lcom/android/settings/sound/b;

    invoke-static {v1}, Lcom/android/settings/sound/b;->Gy(Lcom/android/settings/sound/b;)Landroid/widget/SeekBar;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/settings/sound/c;->My:Lcom/android/settings/sound/b;

    iget-object v2, p0, Lcom/android/settings/sound/c;->My:Lcom/android/settings/sound/b;

    invoke-static {v2}, Lcom/android/settings/sound/b;->Gy(Lcom/android/settings/sound/b;)Landroid/widget/SeekBar;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/SeekBar;->getProgress()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/android/settings/sound/b;->Gs(I)I

    move-result v1

    if-eq v0, v1, :cond_0

    iget-object v1, p0, Lcom/android/settings/sound/c;->My:Lcom/android/settings/sound/b;

    invoke-static {v1}, Lcom/android/settings/sound/b;->Gy(Lcom/android/settings/sound/b;)Landroid/widget/SeekBar;

    move-result-object v1

    iget-object v2, p0, Lcom/android/settings/sound/c;->My:Lcom/android/settings/sound/b;

    invoke-virtual {v2, v0}, Lcom/android/settings/sound/b;->Gu(I)I

    move-result v0

    invoke-virtual {v1, v0}, Landroid/widget/SeekBar;->setProgress(I)V

    goto/16 :goto_0
.end method
