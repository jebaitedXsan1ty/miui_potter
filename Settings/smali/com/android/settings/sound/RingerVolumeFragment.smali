.class public Lcom/android/settings/sound/RingerVolumeFragment;
.super Lcom/android/settings/BaseFragment;
.source "RingerVolumeFragment.java"

# interfaces
.implements Landroid/view/View$OnKeyListener;


# static fields
.field static final ME:[I

.field private static final MF:[I

.field private static final MG:[I

.field private static final MH:[I


# instance fields
.field private final MD:[I

.field private MI:Landroid/media/AudioManager;

.field private MJ:[Landroid/widget/ImageView;

.field private MK:Landroid/os/Handler;

.field private ML:Landroid/content/BroadcastReceiver;

.field private MM:[Lcom/android/settings/sound/d;

.field private MN:[Lmiui/widget/SeekBar;

.field private MO:Landroid/content/BroadcastReceiver;


# direct methods
.method static synthetic -get0()[I
    .locals 1

    sget-object v0, Lcom/android/settings/sound/RingerVolumeFragment;->MG:[I

    return-object v0
.end method

.method static constructor <clinit>()V
    .locals 2

    const/4 v1, 0x6

    new-array v0, v1, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/android/settings/sound/RingerVolumeFragment;->ME:[I

    new-array v0, v1, [I

    fill-array-data v0, :array_1

    sput-object v0, Lcom/android/settings/sound/RingerVolumeFragment;->MG:[I

    new-array v0, v1, [I

    fill-array-data v0, :array_2

    sput-object v0, Lcom/android/settings/sound/RingerVolumeFragment;->MF:[I

    new-array v0, v1, [I

    fill-array-data v0, :array_3

    sput-object v0, Lcom/android/settings/sound/RingerVolumeFragment;->MH:[I

    return-void

    :array_0
    .array-data 4
        0x7f0a0395
        0x7f0a02e1
        0x7f0a004c
        0x7f0a04f9
        0x7f0a028b
        0x7f0a0093
    .end array-data

    :array_1
    .array-data 4
        0x2
        0x5
        0x4
        0x0
        0x3
        0x6
    .end array-data

    :array_2
    .array-data 4
        0x7f080193
        0x7f080190
        0x7f080188
        0x7f080191
        0x7f08018d
        0x7f08018a
    .end array-data

    :array_3
    .array-data 4
        0x7f080192
        0x7f08018f
        0x7f080187
        0x7f080191
        0x7f08018d
        0x7f080189
    .end array-data
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/android/settings/BaseFragment;-><init>()V

    const/4 v0, 0x6

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    iput-object v0, p0, Lcom/android/settings/sound/RingerVolumeFragment;->MD:[I

    sget-object v0, Lcom/android/settings/sound/RingerVolumeFragment;->MF:[I

    array-length v0, v0

    new-array v0, v0, [Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/android/settings/sound/RingerVolumeFragment;->MJ:[Landroid/widget/ImageView;

    sget-object v0, Lcom/android/settings/sound/RingerVolumeFragment;->ME:[I

    array-length v0, v0

    new-array v0, v0, [Lmiui/widget/SeekBar;

    iput-object v0, p0, Lcom/android/settings/sound/RingerVolumeFragment;->MN:[Lmiui/widget/SeekBar;

    new-instance v0, Lcom/android/settings/sound/h;

    invoke-direct {v0, p0}, Lcom/android/settings/sound/h;-><init>(Lcom/android/settings/sound/RingerVolumeFragment;)V

    iput-object v0, p0, Lcom/android/settings/sound/RingerVolumeFragment;->MK:Landroid/os/Handler;

    return-void

    nop

    :array_0
    .array-data 4
        0x7f12142f
        0x7f12142b
        0x7f121424
        0x7f121432
        0x7f121429
        0x7f121426
    .end array-data
.end method

.method private GG()V
    .locals 3

    const/4 v2, 0x0

    const/4 v0, 0x0

    :goto_0
    sget-object v1, Lcom/android/settings/sound/RingerVolumeFragment;->ME:[I

    array-length v1, v1

    if-ge v0, v1, :cond_1

    iget-object v1, p0, Lcom/android/settings/sound/RingerVolumeFragment;->MM:[Lcom/android/settings/sound/d;

    aget-object v1, v1, v0

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/settings/sound/RingerVolumeFragment;->MM:[Lcom/android/settings/sound/d;

    aget-object v1, v1, v0

    invoke-virtual {v1}, Lcom/android/settings/sound/d;->stop()V

    iget-object v1, p0, Lcom/android/settings/sound/RingerVolumeFragment;->MM:[Lcom/android/settings/sound/d;

    aput-object v2, v1, v0

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/android/settings/sound/RingerVolumeFragment;->ML:Landroid/content/BroadcastReceiver;

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lcom/android/settings/sound/RingerVolumeFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/sound/RingerVolumeFragment;->ML:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/app/Activity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    iput-object v2, p0, Lcom/android/settings/sound/RingerVolumeFragment;->ML:Landroid/content/BroadcastReceiver;

    :cond_2
    iget-object v0, p0, Lcom/android/settings/sound/RingerVolumeFragment;->MO:Landroid/content/BroadcastReceiver;

    if-eqz v0, :cond_3

    invoke-virtual {p0}, Lcom/android/settings/sound/RingerVolumeFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/sound/RingerVolumeFragment;->MO:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/app/Activity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    iput-object v2, p0, Lcom/android/settings/sound/RingerVolumeFragment;->MO:Landroid/content/BroadcastReceiver;

    :cond_3
    iget-object v0, p0, Lcom/android/settings/sound/RingerVolumeFragment;->MK:Landroid/os/Handler;

    const/16 v1, 0x65

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    iget-object v0, p0, Lcom/android/settings/sound/RingerVolumeFragment;->MK:Landroid/os/Handler;

    const/16 v1, 0x66

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    return-void
.end method

.method private GI()Landroid/net/Uri;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "android.resource://"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/android/settings/sound/RingerVolumeFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const v1, 0x7f11005c

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method private GL()V
    .locals 2

    const/4 v0, 0x0

    :goto_0
    sget-object v1, Lcom/android/settings/sound/RingerVolumeFragment;->MG:[I

    array-length v1, v1

    if-ge v0, v1, :cond_0

    invoke-direct {p0, v0}, Lcom/android/settings/sound/RingerVolumeFragment;->GM(I)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method private GM(I)V
    .locals 4

    sget-object v0, Lcom/android/settings/sound/RingerVolumeFragment;->MG:[I

    aget v1, v0, p1

    iget-object v0, p0, Lcom/android/settings/sound/RingerVolumeFragment;->MI:Landroid/media/AudioManager;

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->isStreamMute(I)Z

    move-result v0

    iget-object v2, p0, Lcom/android/settings/sound/RingerVolumeFragment;->MJ:[Landroid/widget/ImageView;

    aget-object v2, v2, p1

    if-eqz v2, :cond_0

    const/4 v2, 0x2

    if-ne v1, v2, :cond_2

    if-eqz v0, :cond_2

    iget-object v2, p0, Lcom/android/settings/sound/RingerVolumeFragment;->MI:Landroid/media/AudioManager;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/media/AudioManager;->shouldVibrate(I)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v0, p0, Lcom/android/settings/sound/RingerVolumeFragment;->MJ:[Landroid/widget/ImageView;

    aget-object v0, v0, p1

    const v2, 0x7f080194

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/android/settings/sound/RingerVolumeFragment;->MN:[Lmiui/widget/SeekBar;

    aget-object v0, v0, p1

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/settings/sound/RingerVolumeFragment;->MI:Landroid/media/AudioManager;

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->getStreamVolume(I)I

    move-result v0

    iget-object v1, p0, Lcom/android/settings/sound/RingerVolumeFragment;->MM:[Lcom/android/settings/sound/d;

    aget-object v1, v1, p1

    iget-object v2, p0, Lcom/android/settings/sound/RingerVolumeFragment;->MN:[Lmiui/widget/SeekBar;

    aget-object v2, v2, p1

    invoke-virtual {v2}, Lmiui/widget/SeekBar;->getProgress()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/android/settings/sound/d;->GT(I)I

    move-result v1

    if-eq v0, v1, :cond_1

    iget-object v1, p0, Lcom/android/settings/sound/RingerVolumeFragment;->MN:[Lmiui/widget/SeekBar;

    aget-object v1, v1, p1

    iget-object v2, p0, Lcom/android/settings/sound/RingerVolumeFragment;->MM:[Lcom/android/settings/sound/d;

    aget-object v2, v2, p1

    invoke-virtual {v2, v0}, Lcom/android/settings/sound/d;->GS(I)I

    move-result v0

    invoke-virtual {v1, v0}, Lmiui/widget/SeekBar;->setProgress(I)V

    :cond_1
    return-void

    :cond_2
    iget-object v2, p0, Lcom/android/settings/sound/RingerVolumeFragment;->MJ:[Landroid/widget/ImageView;

    aget-object v2, v2, p1

    if-eqz v0, :cond_3

    sget-object v0, Lcom/android/settings/sound/RingerVolumeFragment;->MF:[I

    aget v0, v0, p1

    :goto_1
    invoke-virtual {v2, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0

    :cond_3
    sget-object v0, Lcom/android/settings/sound/RingerVolumeFragment;->MH:[I

    aget v0, v0, p1

    goto :goto_1
.end method

.method static synthetic GN(Lcom/android/settings/sound/RingerVolumeFragment;)Landroid/os/Handler;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/sound/RingerVolumeFragment;->MK:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic GO(Lcom/android/settings/sound/RingerVolumeFragment;)Landroid/net/Uri;
    .locals 1

    invoke-direct {p0}, Lcom/android/settings/sound/RingerVolumeFragment;->GI()Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method static synthetic GP(Lcom/android/settings/sound/RingerVolumeFragment;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/sound/RingerVolumeFragment;->GL()V

    return-void
.end method

.method static synthetic GQ(Lcom/android/settings/sound/RingerVolumeFragment;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/settings/sound/RingerVolumeFragment;->GM(I)V

    return-void
.end method


# virtual methods
.method GH()V
    .locals 2

    const/4 v0, 0x0

    :goto_0
    sget-object v1, Lcom/android/settings/sound/RingerVolumeFragment;->ME:[I

    array-length v1, v1

    if-ge v0, v1, :cond_1

    iget-object v1, p0, Lcom/android/settings/sound/RingerVolumeFragment;->MM:[Lcom/android/settings/sound/d;

    aget-object v1, v1, v0

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/settings/sound/RingerVolumeFragment;->MM:[Lcom/android/settings/sound/d;

    aget-object v1, v1, v0

    invoke-virtual {v1}, Lcom/android/settings/sound/d;->Ha()V

    iget-object v1, p0, Lcom/android/settings/sound/RingerVolumeFragment;->MM:[Lcom/android/settings/sound/d;

    aget-object v1, v1, v0

    invoke-virtual {v1}, Lcom/android/settings/sound/d;->GX()V

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    invoke-direct {p0}, Lcom/android/settings/sound/RingerVolumeFragment;->GL()V

    return-void
.end method

.method protected GJ(Lcom/android/settings/sound/d;)V
    .locals 4

    iget-object v1, p0, Lcom/android/settings/sound/RingerVolumeFragment;->MM:[Lcom/android/settings/sound/d;

    const/4 v0, 0x0

    array-length v2, v1

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    if-eqz v3, :cond_0

    if-eq v3, p1, :cond_0

    invoke-virtual {v3}, Lcom/android/settings/sound/d;->Ha()V

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method GK()V
    .locals 3

    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/android/settings/sound/RingerVolumeFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v1, 0x7f12142d

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const/high16 v1, 0x1040000

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    new-instance v1, Lcom/android/settings/sound/k;

    invoke-direct {v1, p0}, Lcom/android/settings/sound/k;-><init>(Lcom/android/settings/sound/RingerVolumeFragment;)V

    const v2, 0x104000a

    invoke-virtual {v0, v2, v1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f12142e

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 3

    const/4 v2, 0x1

    const/4 v1, 0x0

    const v0, 0x7f12142d

    invoke-interface {p1, v1, v2, v1, v0}, Landroid/view/Menu;->add(IIII)Landroid/view/MenuItem;

    move-result-object v0

    const v1, 0x7f08006c

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    const/4 v1, 0x5

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setShowAsAction(I)V

    return v2
.end method

.method public onDestroy()V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/sound/RingerVolumeFragment;->GG()V

    invoke-super {p0}, Lcom/android/settings/BaseFragment;->onDestroy()V

    return-void
.end method

.method public onKey(Landroid/view/View;ILandroid/view/KeyEvent;)Z
    .locals 1

    sparse-switch p2, :sswitch_data_0

    const/4 v0, 0x0

    return v0

    :sswitch_0
    const/4 v0, 0x1

    return v0

    nop

    :sswitch_data_0
    .sparse-switch
        0x18 -> :sswitch_0
        0x19 -> :sswitch_0
        0xa4 -> :sswitch_0
    .end sparse-switch
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    invoke-super {p0, p1}, Lcom/android/settings/BaseFragment;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    return v0

    :pswitch_0
    invoke-virtual {p0}, Lcom/android/settings/sound/RingerVolumeFragment;->GK()V

    const/4 v0, 0x1

    return v0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public onPause()V
    .locals 4

    iget-object v1, p0, Lcom/android/settings/sound/RingerVolumeFragment;->MM:[Lcom/android/settings/sound/d;

    const/4 v0, 0x0

    array-length v2, v1

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    invoke-virtual {v3}, Lcom/android/settings/sound/d;->Ha()V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    invoke-super {p0}, Lcom/android/settings/BaseFragment;->onPause()V

    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 10

    const/16 v9, 0x8

    invoke-super {p0, p1, p2}, Lcom/android/settings/BaseFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    sget-object v0, Lcom/android/settings/sound/RingerVolumeFragment;->ME:[I

    array-length v0, v0

    new-array v0, v0, [Lcom/android/settings/sound/d;

    iput-object v0, p0, Lcom/android/settings/sound/RingerVolumeFragment;->MM:[Lcom/android/settings/sound/d;

    invoke-virtual {p0}, Lcom/android/settings/sound/RingerVolumeFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const-string/jumbo v1, "audio"

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    iput-object v0, p0, Lcom/android/settings/sound/RingerVolumeFragment;->MI:Landroid/media/AudioManager;

    const/4 v0, 0x0

    move v6, v0

    :goto_0
    sget-object v0, Lcom/android/settings/sound/RingerVolumeFragment;->ME:[I

    array-length v0, v0

    if-ge v6, v0, :cond_1

    sget-object v0, Lcom/android/settings/sound/RingerVolumeFragment;->ME:[I

    aget v0, v0, v6

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    sget v0, Lmiui/R$drawable;->preference_item_bg:I

    invoke-virtual {v7, v0}, Landroid/view/View;->setBackgroundResource(I)V

    const v0, 0x7f0a04fc

    invoke-virtual {v7, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lmiui/widget/SeekBar;

    iget-object v0, p0, Lcom/android/settings/sound/RingerVolumeFragment;->MN:[Lmiui/widget/SeekBar;

    aput-object v3, v0, v6

    sget-object v0, Lcom/android/settings/sound/RingerVolumeFragment;->MG:[I

    aget v0, v0, v6

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    iget-object v8, p0, Lcom/android/settings/sound/RingerVolumeFragment;->MM:[Lcom/android/settings/sound/d;

    new-instance v0, Lcom/android/settings/sound/d;

    invoke-virtual {p0}, Lcom/android/settings/sound/RingerVolumeFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    sget-object v1, Lcom/android/settings/sound/RingerVolumeFragment;->MG:[I

    aget v4, v1, v6

    invoke-direct {p0}, Lcom/android/settings/sound/RingerVolumeFragment;->GI()Landroid/net/Uri;

    move-result-object v5

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/android/settings/sound/d;-><init>(Lcom/android/settings/sound/RingerVolumeFragment;Landroid/content/Context;Lmiui/widget/SeekBar;ILandroid/net/Uri;)V

    aput-object v0, v8, v6

    :goto_1
    const v0, 0x7f0a02b4

    invoke-virtual {v7, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/android/settings/sound/RingerVolumeFragment;->MJ:[Landroid/widget/ImageView;

    aput-object v0, v1, v6

    const v0, 0x7f0a0132

    invoke-virtual {v7, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v1, p0, Lcom/android/settings/sound/RingerVolumeFragment;->MD:[I

    aget v1, v1, v6

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    add-int/lit8 v0, v6, 0x1

    move v6, v0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/android/settings/sound/RingerVolumeFragment;->MM:[Lcom/android/settings/sound/d;

    new-instance v1, Lcom/android/settings/sound/d;

    invoke-virtual {p0}, Lcom/android/settings/sound/RingerVolumeFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    sget-object v4, Lcom/android/settings/sound/RingerVolumeFragment;->MG:[I

    aget v4, v4, v6

    invoke-direct {v1, p0, v2, v3, v4}, Lcom/android/settings/sound/d;-><init>(Lcom/android/settings/sound/RingerVolumeFragment;Landroid/content/Context;Lmiui/widget/SeekBar;I)V

    aput-object v1, v0, v6

    goto :goto_1

    :cond_1
    invoke-direct {p0}, Lcom/android/settings/sound/RingerVolumeFragment;->GL()V

    iget-object v0, p0, Lcom/android/settings/sound/RingerVolumeFragment;->ML:Landroid/content/BroadcastReceiver;

    if-nez v0, :cond_2

    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    const-string/jumbo v1, "android.media.RINGER_MODE_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    new-instance v1, Lcom/android/settings/sound/i;

    invoke-direct {v1, p0}, Lcom/android/settings/sound/i;-><init>(Lcom/android/settings/sound/RingerVolumeFragment;)V

    iput-object v1, p0, Lcom/android/settings/sound/RingerVolumeFragment;->ML:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0}, Lcom/android/settings/sound/RingerVolumeFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    iget-object v2, p0, Lcom/android/settings/sound/RingerVolumeFragment;->ML:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/app/Activity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    :cond_2
    iget-object v0, p0, Lcom/android/settings/sound/RingerVolumeFragment;->MO:Landroid/content/BroadcastReceiver;

    if-nez v0, :cond_3

    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    const-string/jumbo v1, "android.media.VOLUME_CHANGED_ACTION"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    new-instance v1, Lcom/android/settings/sound/j;

    invoke-direct {v1, p0}, Lcom/android/settings/sound/j;-><init>(Lcom/android/settings/sound/RingerVolumeFragment;)V

    iput-object v1, p0, Lcom/android/settings/sound/RingerVolumeFragment;->MO:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0}, Lcom/android/settings/sound/RingerVolumeFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    iget-object v2, p0, Lcom/android/settings/sound/RingerVolumeFragment;->MO:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/app/Activity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    :cond_3
    invoke-virtual {p0}, Lcom/android/settings/sound/RingerVolumeFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/android/settings/aq;->bqw(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_4

    const v0, 0x7f0a0395

    const v1, 0x7f0a04f9

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v9}, Landroid/view/View;->setVisibility(I)V

    const v1, 0x7f0a0093

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v9}, Landroid/view/View;->setVisibility(I)V

    const v1, 0x7f0a004c

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v9}, Landroid/view/View;->setVisibility(I)V

    :goto_2
    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v9}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {p1, p0}, Landroid/view/View;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Landroid/view/View;->setFocusableInTouchMode(Z)V

    invoke-virtual {p1}, Landroid/view/View;->requestFocus()Z

    return-void

    :cond_4
    const v0, 0x7f0a02e1

    goto :goto_2
.end method

.method public vB(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2

    const v0, 0x7f0d0131

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method
