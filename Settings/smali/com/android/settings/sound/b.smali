.class public Lcom/android/settings/sound/b;
.super Ljava/lang/Object;
.source "SeekBarVolumizer.java"

# interfaces
.implements Landroid/widget/SeekBar$OnSeekBarChangeListener;
.implements Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;
.implements Landroid/os/Handler$Callback;


# instance fields
.field private Mn:Landroid/media/AudioManager;

.field private Mo:D

.field private Mp:Landroid/os/Handler;

.field private Mq:I

.field private Mr:Lcom/android/settings/sound/VolumeSeekBarPreference;

.field private Ms:Lcom/android/settings/sound/c;

.field private Mt:Landroid/media/Ringtone;

.field private Mu:Landroid/widget/SeekBar;

.field private Mv:Landroid/content/SharedPreferences;

.field private Mw:I

.field private mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>(Lcom/android/settings/sound/VolumeSeekBarPreference;)V
    .locals 4

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, -0x1

    iput v0, p0, Lcom/android/settings/sound/b;->Mq:I

    iput-object p1, p0, Lcom/android/settings/sound/b;->Mr:Lcom/android/settings/sound/VolumeSeekBarPreference;

    invoke-virtual {p1}, Lcom/android/settings/sound/VolumeSeekBarPreference;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/sound/b;->mContext:Landroid/content/Context;

    invoke-virtual {p1}, Lcom/android/settings/sound/VolumeSeekBarPreference;->GF()I

    move-result v0

    iput v0, p0, Lcom/android/settings/sound/b;->Mw:I

    iget-object v0, p0, Lcom/android/settings/sound/b;->mContext:Landroid/content/Context;

    const-string/jumbo v1, "audio"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    iput-object v0, p0, Lcom/android/settings/sound/b;->Mn:Landroid/media/AudioManager;

    iget-object v0, p0, Lcom/android/settings/sound/b;->Mn:Landroid/media/AudioManager;

    iget v1, p0, Lcom/android/settings/sound/b;->Mw:I

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->getStreamMaxVolume(I)I

    move-result v0

    int-to-double v0, v0

    const-wide/high16 v2, 0x4059000000000000L    # 100.0

    div-double v0, v2, v0

    iput-wide v0, p0, Lcom/android/settings/sound/b;->Mo:D

    new-instance v0, Lcom/android/settings/sound/c;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/android/settings/sound/c;-><init>(Lcom/android/settings/sound/b;Lcom/android/settings/sound/c;)V

    iput-object v0, p0, Lcom/android/settings/sound/b;->Ms:Lcom/android/settings/sound/c;

    iget-object v0, p0, Lcom/android/settings/sound/b;->mContext:Landroid/content/Context;

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/sound/b;->Mv:Landroid/content/SharedPreferences;

    return-void
.end method

.method static synthetic GA(Lcom/android/settings/sound/b;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/settings/sound/b;->Gq(Z)V

    return-void
.end method

.method private Gh(FFI)V
    .locals 8

    const/4 v7, 0x0

    const/high16 v6, 0x3f800000    # 1.0f

    sub-float v3, p2, p1

    cmpl-float v0, v3, v7

    if-nez v0, :cond_0

    return-void

    :cond_0
    const/4 v0, 0x1

    move v1, v0

    move v0, p1

    :goto_0
    add-int/lit8 v2, p3, 0x1

    if-ge v1, v2, :cond_1

    cmpl-float v2, v3, v7

    if-lez v2, :cond_4

    cmpl-float v2, v0, p2

    if-ltz v2, :cond_3

    :cond_1
    cmpl-float v0, v0, p2

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/settings/sound/b;->Mt:Landroid/media/Ringtone;

    invoke-virtual {v0, p2}, Landroid/media/Ringtone;->setVolume(F)V

    const-wide/16 v0, 0xa

    :try_start_0
    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_1

    :cond_2
    :goto_1
    return-void

    :cond_3
    int-to-float v0, v1

    int-to-float v2, p3

    div-float/2addr v0, v2

    mul-float/2addr v0, v0

    :goto_2
    mul-float/2addr v0, v3

    add-float v2, p1, v0

    iget-object v0, p0, Lcom/android/settings/sound/b;->Mt:Landroid/media/Ringtone;

    invoke-virtual {v0, v2}, Landroid/media/Ringtone;->setVolume(F)V

    const-wide/16 v4, 0xa

    :try_start_1
    invoke-static {v4, v5}, Ljava/lang/Thread;->sleep(J)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0

    :goto_3
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    move v0, v2

    goto :goto_0

    :cond_4
    cmpg-float v2, v0, p2

    if-lez v2, :cond_1

    int-to-float v0, v1

    int-to-float v2, p3

    div-float/2addr v0, v2

    sub-float v0, v6, v0

    mul-float/2addr v0, v0

    sub-float v0, v6, v0

    goto :goto_2

    :catch_0
    move-exception v0

    goto :goto_3

    :catch_1
    move-exception v0

    goto :goto_1
.end method

.method private Gi()Landroid/net/Uri;
    .locals 2

    iget v0, p0, Lcom/android/settings/sound/b;->Mw:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    sget-object v0, Landroid/provider/Settings$System;->DEFAULT_RINGTONE_URI:Landroid/net/Uri;

    return-object v0

    :cond_0
    iget v0, p0, Lcom/android/settings/sound/b;->Mw:I

    const/4 v1, 0x4

    if-ne v0, v1, :cond_1

    sget-object v0, Landroid/provider/Settings$System;->DEFAULT_ALARM_ALERT_URI:Landroid/net/Uri;

    return-object v0

    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "android.resource://"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/sound/b;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const v1, 0x7f11005c

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method private Gj()V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/sound/b;->Mt:Landroid/media/Ringtone;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/sound/b;->Mt:Landroid/media/Ringtone;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/media/Ringtone;->setLooping(Z)V

    :cond_0
    return-void
.end method

.method private Gk()V
    .locals 4

    const/16 v0, 0x400

    iget v1, p0, Lcom/android/settings/sound/b;->Mw:I

    iget-object v2, p0, Lcom/android/settings/sound/b;->Mn:Landroid/media/AudioManager;

    invoke-virtual {v2}, Landroid/media/AudioManager;->getUiSoundsStreamType()I

    move-result v2

    if-eq v1, v2, :cond_0

    iget v1, p0, Lcom/android/settings/sound/b;->Mw:I

    const/4 v2, 0x3

    if-ne v1, v2, :cond_1

    iget v1, p0, Lcom/android/settings/sound/b;->Mq:I

    if-nez v1, :cond_1

    :cond_0
    const v0, 0x100400

    :cond_1
    iget-object v1, p0, Lcom/android/settings/sound/b;->Mn:Landroid/media/AudioManager;

    iget v2, p0, Lcom/android/settings/sound/b;->Mw:I

    iget v3, p0, Lcom/android/settings/sound/b;->Mq:I

    invoke-virtual {v1, v2, v3, v0}, Landroid/media/AudioManager;->setStreamVolume(III)V

    return-void
.end method

.method private Gl(Z)V
    .locals 5

    const/4 v4, 0x1

    const/4 v3, 0x0

    const-string/jumbo v0, "SeekBarVolumizer"

    const-string/jumbo v1, "onStartSample"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/android/settings/sound/b;->Mv:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string/jumbo v1, "volume_sample_stream"

    iget v2, p0, Lcom/android/settings/sound/b;->Mw:I

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    :cond_0
    iget-object v0, p0, Lcom/android/settings/sound/b;->Mt:Landroid/media/Ringtone;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/settings/sound/b;->Mt:Landroid/media/Ringtone;

    invoke-virtual {v0}, Landroid/media/Ringtone;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_1

    const-string/jumbo v0, "SeekBarVolumizer"

    const-string/jumbo v1, "onStartSample isPlaying"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/settings/sound/b;->Mt:Landroid/media/Ringtone;

    invoke-virtual {v0, v4}, Landroid/media/Ringtone;->setLooping(Z)V

    :goto_0
    return-void

    :cond_1
    const-string/jumbo v0, "SeekBarVolumizer"

    const-string/jumbo v1, "onStartSample restart ringtone"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/settings/sound/b;->Mt:Landroid/media/Ringtone;

    invoke-virtual {v0, v4}, Landroid/media/Ringtone;->setLooping(Z)V

    iget-object v0, p0, Lcom/android/settings/sound/b;->Mt:Landroid/media/Ringtone;

    const-string/jumbo v1, "startLocalPlayer"

    new-array v2, v3, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/miui/whetstone/ReflectionUtils;->callMethod(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_2
    iget v0, p0, Lcom/android/settings/sound/b;->Mw:I

    invoke-static {v0, v3}, Landroid/media/AudioSystem;->isStreamActive(II)Z

    move-result v0

    if-eqz v0, :cond_4

    const-string/jumbo v0, "SeekBarVolumizer"

    const-string/jumbo v1, "onStartSample isStreamActive"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    if-eqz p1, :cond_3

    const-wide/16 v0, 0xc8

    invoke-direct {p0, v0, v1, v3}, Lcom/android/settings/sound/b;->Gp(JZ)V

    :cond_3
    return-void

    :cond_4
    iget-object v0, p0, Lcom/android/settings/sound/b;->mContext:Landroid/content/Context;

    invoke-direct {p0}, Lcom/android/settings/sound/b;->Gi()Landroid/net/Uri;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/media/RingtoneManager;->getRingtone(Landroid/content/Context;Landroid/net/Uri;)Landroid/media/Ringtone;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/sound/b;->Mt:Landroid/media/Ringtone;

    iget-object v0, p0, Lcom/android/settings/sound/b;->Mt:Landroid/media/Ringtone;

    iget v1, p0, Lcom/android/settings/sound/b;->Mw:I

    invoke-virtual {v0, v1}, Landroid/media/Ringtone;->setStreamType(I)V

    iget-object v0, p0, Lcom/android/settings/sound/b;->Mt:Landroid/media/Ringtone;

    invoke-virtual {v0, v4}, Landroid/media/Ringtone;->setLooping(Z)V

    iget-object v0, p0, Lcom/android/settings/sound/b;->Mt:Landroid/media/Ringtone;

    const-string/jumbo v1, "startLocalPlayer"

    new-array v2, v3, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/miui/whetstone/ReflectionUtils;->callMethod(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method private Gm(Z)V
    .locals 4

    const/4 v3, 0x0

    const-string/jumbo v0, "SeekBarVolumizer"

    const-string/jumbo v1, "onStopSample"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/settings/sound/b;->Mt:Landroid/media/Ringtone;

    if-eqz v0, :cond_1

    if-eqz p1, :cond_0

    const/high16 v0, 0x3f800000    # 1.0f

    const/4 v1, 0x0

    const/16 v2, 0x64

    invoke-direct {p0, v0, v1, v2}, Lcom/android/settings/sound/b;->Gh(FFI)V

    :cond_0
    iget-object v0, p0, Lcom/android/settings/sound/b;->Mt:Landroid/media/Ringtone;

    invoke-virtual {v0}, Landroid/media/Ringtone;->stop()V

    iput-object v3, p0, Lcom/android/settings/sound/b;->Mt:Landroid/media/Ringtone;

    :cond_1
    return-void
.end method

.method private Gn()V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/android/settings/sound/b;->Mp:Landroid/os/Handler;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/sound/b;->Mp:Landroid/os/Handler;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeMessages(I)V

    iget-object v0, p0, Lcom/android/settings/sound/b;->Mp:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/settings/sound/b;->Mp:Landroid/os/Handler;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    :cond_0
    return-void
.end method

.method private Go(Z)V
    .locals 2

    const-wide/16 v0, 0x0

    invoke-direct {p0, v0, v1, p1}, Lcom/android/settings/sound/b;->Gp(JZ)V

    return-void
.end method

.method private Gp(JZ)V
    .locals 5

    const/4 v3, 0x1

    iget-object v0, p0, Lcom/android/settings/sound/b;->Mp:Landroid/os/Handler;

    if-eqz v0, :cond_1

    if-eqz p3, :cond_0

    iget-object v0, p0, Lcom/android/settings/sound/b;->Mp:Landroid/os/Handler;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    :cond_0
    iget-object v0, p0, Lcom/android/settings/sound/b;->Mp:Landroid/os/Handler;

    invoke-virtual {v0, v3}, Landroid/os/Handler;->removeMessages(I)V

    iget-object v0, p0, Lcom/android/settings/sound/b;->Mp:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/settings/sound/b;->Mp:Landroid/os/Handler;

    invoke-static {p3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v1, v3, v2}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1, p1, p2}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    :cond_1
    return-void
.end method

.method private Gq(Z)V
    .locals 2

    const-wide/16 v0, 0x0

    invoke-direct {p0, v0, v1, p1}, Lcom/android/settings/sound/b;->Gr(JZ)V

    return-void
.end method

.method private Gr(JZ)V
    .locals 5

    const/4 v3, 0x2

    iget-object v0, p0, Lcom/android/settings/sound/b;->Mp:Landroid/os/Handler;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/sound/b;->Mp:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/settings/sound/b;->Mp:Landroid/os/Handler;

    const/4 v2, 0x3

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    iget-object v0, p0, Lcom/android/settings/sound/b;->Mp:Landroid/os/Handler;

    invoke-virtual {v0, v3}, Landroid/os/Handler;->removeMessages(I)V

    iget-object v0, p0, Lcom/android/settings/sound/b;->Mp:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/settings/sound/b;->Mp:Landroid/os/Handler;

    invoke-static {p3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v1, v3, v2}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1, p1, p2}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    :cond_0
    return-void
.end method

.method static synthetic Gv(Lcom/android/settings/sound/b;)Landroid/media/AudioManager;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/sound/b;->Mn:Landroid/media/AudioManager;

    return-object v0
.end method

.method static synthetic Gw(Lcom/android/settings/sound/b;)Landroid/content/Context;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/sound/b;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic Gx(Lcom/android/settings/sound/b;)Lcom/android/settings/sound/VolumeSeekBarPreference;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/sound/b;->Mr:Lcom/android/settings/sound/VolumeSeekBarPreference;

    return-object v0
.end method

.method static synthetic Gy(Lcom/android/settings/sound/b;)Landroid/widget/SeekBar;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/sound/b;->Mu:Landroid/widget/SeekBar;

    return-object v0
.end method

.method static synthetic Gz(Lcom/android/settings/sound/b;)I
    .locals 1

    iget v0, p0, Lcom/android/settings/sound/b;->Mw:I

    return v0
.end method


# virtual methods
.method public Gg()V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/sound/b;->Mp:Landroid/os/Handler;

    if-nez v0, :cond_0

    new-instance v0, Landroid/os/HandlerThread;

    const-string/jumbo v1, "SeekBarVolumizerHandler"

    invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    new-instance v1, Landroid/os/Handler;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-direct {v1, v0, p0}, Landroid/os/Handler;-><init>(Landroid/os/Looper;Landroid/os/Handler$Callback;)V

    iput-object v1, p0, Lcom/android/settings/sound/b;->Mp:Landroid/os/Handler;

    :cond_0
    iget-object v0, p0, Lcom/android/settings/sound/b;->Ms:Lcom/android/settings/sound/c;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/android/settings/sound/c;->GB(Z)V

    iget-object v0, p0, Lcom/android/settings/sound/b;->Mv:Landroid/content/SharedPreferences;

    invoke-interface {v0, p0}, Landroid/content/SharedPreferences;->registerOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    return-void
.end method

.method public Gs(I)I
    .locals 8

    const-wide/high16 v6, 0x4000000000000000L    # 2.0

    int-to-double v0, p1

    iget-wide v2, p0, Lcom/android/settings/sound/b;->Mo:D

    div-double/2addr v2, v6

    add-double/2addr v0, v2

    iget-wide v2, p0, Lcom/android/settings/sound/b;->Mo:D

    div-double/2addr v0, v2

    double-to-int v0, v0

    if-lez p1, :cond_1

    int-to-double v2, p1

    iget-wide v4, p0, Lcom/android/settings/sound/b;->Mo:D

    div-double/2addr v4, v6

    cmpg-double v1, v2, v4

    if-gez v1, :cond_1

    add-int/lit8 v0, v0, 0x1

    :cond_0
    :goto_0
    return v0

    :cond_1
    const/16 v1, 0x64

    if-ge p1, v1, :cond_0

    int-to-double v2, p1

    iget-wide v4, p0, Lcom/android/settings/sound/b;->Mo:D

    div-double/2addr v4, v6

    const-wide/high16 v6, 0x4059000000000000L    # 100.0

    sub-double v4, v6, v4

    cmpl-double v1, v2, v4

    if-lez v1, :cond_0

    add-int/lit8 v0, v0, -0x1

    goto :goto_0
.end method

.method public Gt(Landroid/widget/SeekBar;)V
    .locals 2

    iput-object p1, p0, Lcom/android/settings/sound/b;->Mu:Landroid/widget/SeekBar;

    iget-object v0, p0, Lcom/android/settings/sound/b;->Mn:Landroid/media/AudioManager;

    iget v1, p0, Lcom/android/settings/sound/b;->Mw:I

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->getLastAudibleStreamVolume(I)I

    move-result v0

    iget-object v1, p0, Lcom/android/settings/sound/b;->Mr:Lcom/android/settings/sound/VolumeSeekBarPreference;

    invoke-virtual {v1}, Lcom/android/settings/sound/VolumeSeekBarPreference;->bzj()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/android/settings/sound/b;->Gs(I)I

    move-result v1

    if-eq v1, v0, :cond_0

    iget-object v1, p0, Lcom/android/settings/sound/b;->Mu:Landroid/widget/SeekBar;

    invoke-virtual {p0, v0}, Lcom/android/settings/sound/b;->Gu(I)I

    move-result v0

    invoke-virtual {v1, v0}, Landroid/widget/SeekBar;->setProgress(I)V

    :cond_0
    iget-object v0, p0, Lcom/android/settings/sound/b;->Mu:Landroid/widget/SeekBar;

    invoke-virtual {v0, p0}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    return-void
.end method

.method public Gu(I)I
    .locals 4

    int-to-double v0, p1

    iget-wide v2, p0, Lcom/android/settings/sound/b;->Mo:D

    mul-double/2addr v0, v2

    const-wide/high16 v2, 0x3fe0000000000000L    # 0.5

    add-double/2addr v0, v2

    double-to-int v0, v0

    return v0
.end method

.method public handleMessage(Landroid/os/Message;)Z
    .locals 1

    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    :goto_0
    const/4 v0, 0x0

    return v0

    :pswitch_0
    invoke-direct {p0}, Lcom/android/settings/sound/b;->Gk()V

    goto :goto_0

    :pswitch_1
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/android/settings/sound/b;->Gl(Z)V

    goto :goto_0

    :pswitch_2
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/android/settings/sound/b;->Gm(Z)V

    goto :goto_0

    :pswitch_3
    invoke-direct {p0}, Lcom/android/settings/sound/b;->Gj()V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public onProgressChanged(Landroid/widget/SeekBar;IZ)V
    .locals 2

    if-eqz p3, :cond_1

    invoke-virtual {p0, p2}, Lcom/android/settings/sound/b;->Gs(I)I

    move-result v0

    iget v1, p0, Lcom/android/settings/sound/b;->Mq:I

    if-eq v1, v0, :cond_0

    iput v0, p0, Lcom/android/settings/sound/b;->Mq:I

    invoke-direct {p0}, Lcom/android/settings/sound/b;->Gn()V

    :cond_0
    iget-object v0, p0, Lcom/android/settings/sound/b;->Mr:Lcom/android/settings/sound/VolumeSeekBarPreference;

    invoke-virtual {v0, p1, p2, p3}, Lcom/android/settings/sound/VolumeSeekBarPreference;->onProgressChanged(Landroid/widget/SeekBar;IZ)V

    :cond_1
    return-void
.end method

.method public onSharedPreferenceChanged(Landroid/content/SharedPreferences;Ljava/lang/String;)V
    .locals 3

    const-string/jumbo v0, "volume_sample_stream"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/sound/b;->Mv:Landroid/content/SharedPreferences;

    const-string/jumbo v1, "volume_sample_stream"

    iget v2, p0, Lcom/android/settings/sound/b;->Mw:I

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    iget v1, p0, Lcom/android/settings/sound/b;->Mw:I

    if-eq v0, v1, :cond_0

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/android/settings/sound/b;->Gq(Z)V

    :cond_0
    return-void
.end method

.method public onStartTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/android/settings/sound/b;->Go(Z)V

    return-void
.end method

.method public onStopTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 3

    iget-object v0, p0, Lcom/android/settings/sound/b;->Mn:Landroid/media/AudioManager;

    iget v1, p0, Lcom/android/settings/sound/b;->Mw:I

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->isStreamMute(I)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/android/settings/sound/b;->Gq(Z)V

    :goto_0
    return-void

    :cond_0
    const-wide/16 v0, 0x7d0

    const/4 v2, 0x1

    invoke-direct {p0, v0, v1, v2}, Lcom/android/settings/sound/b;->Gr(JZ)V

    goto :goto_0
.end method

.method public pause()V
    .locals 2

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/android/settings/sound/b;->Gq(Z)V

    iget-object v0, p0, Lcom/android/settings/sound/b;->Mv:Landroid/content/SharedPreferences;

    invoke-interface {v0, p0}, Landroid/content/SharedPreferences;->unregisterOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    iget-object v0, p0, Lcom/android/settings/sound/b;->Ms:Lcom/android/settings/sound/c;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/android/settings/sound/c;->GB(Z)V

    return-void
.end method

.method public stop()V
    .locals 2

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/android/settings/sound/b;->pause()V

    iget-object v0, p0, Lcom/android/settings/sound/b;->Mp:Landroid/os/Handler;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/sound/b;->Mp:Landroid/os/Handler;

    invoke-virtual {v0}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Looper;->quitSafely()V

    iput-object v1, p0, Lcom/android/settings/sound/b;->Mp:Landroid/os/Handler;

    :cond_0
    return-void
.end method
