.class public Lcom/android/settings/ImportanceListPreference;
.super Lcom/android/settings/MiuiListPreference;
.source "ImportanceListPreference.java"


# instance fields
.field private bBh:[Ljava/lang/String;

.field private bBi:Lcom/android/settings/aj;

.field private bBj:I

.field private bBk:[Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/android/settings/ImportanceListPreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    invoke-direct {p0, p1, p2}, Lcom/android/settings/MiuiListPreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    new-instance v0, Lcom/android/settings/aj;

    invoke-direct {v0, p0, p1}, Lcom/android/settings/aj;-><init>(Lcom/android/settings/ImportanceListPreference;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/settings/ImportanceListPreference;->bBi:Lcom/android/settings/aj;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f03004c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/android/settings/ImportanceListPreference;->bBh:[Ljava/lang/String;

    const v1, 0x7f03004d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/ImportanceListPreference;->bBk:[Ljava/lang/String;

    return-void
.end method

.method static synthetic boW(Lcom/android/settings/ImportanceListPreference;)[Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/ImportanceListPreference;->bBh:[Ljava/lang/String;

    return-object v0
.end method

.method static synthetic boX(Lcom/android/settings/ImportanceListPreference;)[Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/ImportanceListPreference;->bBk:[Ljava/lang/String;

    return-object v0
.end method

.method static synthetic boY(Lcom/android/settings/ImportanceListPreference;I)I
    .locals 0

    iput p1, p0, Lcom/android/settings/ImportanceListPreference;->bBj:I

    return p1
.end method

.method static synthetic boZ(Lcom/android/settings/ImportanceListPreference;Ljava/lang/Object;)Z
    .locals 1

    invoke-virtual {p0, p1}, Lcom/android/settings/ImportanceListPreference;->callChangeListener(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method


# virtual methods
.method protected onPrepareDialogBuilder(Landroid/app/AlertDialog$Builder;)V
    .locals 4

    const/4 v3, 0x0

    invoke-virtual {p1, v3}, Landroid/app/AlertDialog$Builder;->setCustomTitle(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    iget-object v0, p0, Lcom/android/settings/ImportanceListPreference;->bBi:Lcom/android/settings/aj;

    iget v1, p0, Lcom/android/settings/ImportanceListPreference;->bBj:I

    new-instance v2, Lcom/android/settings/eS;

    invoke-direct {v2, p0}, Lcom/android/settings/eS;-><init>(Lcom/android/settings/ImportanceListPreference;)V

    invoke-virtual {p1, v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setSingleChoiceItems(Landroid/widget/ListAdapter;ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {p1, v3, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    return-void
.end method

.method public setValueIndex(I)V
    .locals 0

    invoke-super {p0, p1}, Lcom/android/settings/MiuiListPreference;->setValueIndex(I)V

    iput p1, p0, Lcom/android/settings/ImportanceListPreference;->bBj:I

    return-void
.end method
