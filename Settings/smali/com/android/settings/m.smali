.class Lcom/android/settings/m;
.super Landroid/widget/BaseAdapter;
.source "TrustedCredentialsSettings.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/widget/AdapterView$OnItemClickListener;


# instance fields
.field private final buG:[I

.field private final buH:[I

.field private final buI:Landroid/widget/LinearLayout$LayoutParams;

.field private final buJ:Landroid/widget/LinearLayout$LayoutParams;

.field private final buK:Landroid/widget/LinearLayout$LayoutParams;

.field private buL:Landroid/widget/LinearLayout;

.field private final buM:I

.field private buN:Landroid/view/ViewGroup;

.field private buO:Landroid/widget/ImageView;

.field private buP:Z

.field private buQ:Landroid/widget/ListView;

.field private final buR:Landroid/database/DataSetObserver;

.field private final buS:Lcom/android/settings/k;

.field final synthetic buT:Lcom/android/settings/TrustedCredentialsSettings;


# direct methods
.method private constructor <init>(Lcom/android/settings/TrustedCredentialsSettings;Lcom/android/settings/k;I)V
    .locals 6

    const/4 v5, 0x1

    const/4 v4, 0x0

    const/4 v3, -0x1

    iput-object p1, p0, Lcom/android/settings/m;->buT:Lcom/android/settings/TrustedCredentialsSettings;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    new-array v0, v5, [I

    const v1, 0x10100a8

    aput v1, v0, v4

    iput-object v0, p0, Lcom/android/settings/m;->buH:[I

    new-array v0, v4, [I

    iput-object v0, p0, Lcom/android/settings/m;->buG:[I

    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v1, -0x2

    const/4 v2, 0x0

    invoke-direct {v0, v3, v1, v2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(IIF)V

    iput-object v0, p0, Lcom/android/settings/m;->buI:Landroid/widget/LinearLayout$LayoutParams;

    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v0, v3, v4}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    iput-object v0, p0, Lcom/android/settings/m;->buJ:Landroid/widget/LinearLayout$LayoutParams;

    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-direct {v0, v3, v3, v1}, Landroid/widget/LinearLayout$LayoutParams;-><init>(IIF)V

    iput-object v0, p0, Lcom/android/settings/m;->buK:Landroid/widget/LinearLayout$LayoutParams;

    new-instance v0, Lcom/android/settings/dy;

    invoke-direct {v0, p0}, Lcom/android/settings/dy;-><init>(Lcom/android/settings/m;)V

    iput-object v0, p0, Lcom/android/settings/m;->buR:Landroid/database/DataSetObserver;

    iput-boolean v5, p0, Lcom/android/settings/m;->buP:Z

    iput-object p2, p0, Lcom/android/settings/m;->buS:Lcom/android/settings/k;

    iput p3, p0, Lcom/android/settings/m;->buM:I

    iget-object v0, p0, Lcom/android/settings/m;->buS:Lcom/android/settings/k;

    iget-object v1, p0, Lcom/android/settings/m;->buR:Landroid/database/DataSetObserver;

    invoke-virtual {v0, v1}, Lcom/android/settings/k;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/settings/TrustedCredentialsSettings;Lcom/android/settings/k;ILcom/android/settings/m;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/android/settings/m;-><init>(Lcom/android/settings/TrustedCredentialsSettings;Lcom/android/settings/k;I)V

    return-void
.end method

.method private bhG()Z
    .locals 2

    iget-object v0, p0, Lcom/android/settings/m;->buS:Lcom/android/settings/k;

    iget v1, p0, Lcom/android/settings/m;->buM:I

    invoke-virtual {v0, v1}, Lcom/android/settings/k;->bht(I)Z

    move-result v0

    return v0
.end method

.method private bhH()Landroid/graphics/drawable/Drawable;
    .locals 5

    const/4 v4, 0x0

    iget-object v0, p0, Lcom/android/settings/m;->buT:Lcom/android/settings/TrustedCredentialsSettings;

    invoke-virtual {v0}, Lcom/android/settings/TrustedCredentialsSettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    sget-object v1, Lcom/android/internal/R$styleable;->ExpandableListView:[I

    const/4 v2, 0x0

    const v3, 0x101006f

    invoke-virtual {v0, v2, v1, v3, v4}, Landroid/app/Activity;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    return-object v1
.end method

.method private bhI()V
    .locals 3

    iget-object v1, p0, Lcom/android/settings/m;->buO:Landroid/widget/ImageView;

    iget-boolean v0, p0, Lcom/android/settings/m;->buP:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/m;->buH:[I

    :goto_0
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Landroid/widget/ImageView;->setImageState([IZ)V

    iget-object v1, p0, Lcom/android/settings/m;->buQ:Landroid/widget/ListView;

    iget-boolean v0, p0, Lcom/android/settings/m;->buP:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/settings/m;->buK:Landroid/widget/LinearLayout$LayoutParams;

    :goto_1
    invoke-virtual {v1, v0}, Landroid/widget/ListView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v1, p0, Lcom/android/settings/m;->buL:Landroid/widget/LinearLayout;

    iget-boolean v0, p0, Lcom/android/settings/m;->buP:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/settings/m;->buK:Landroid/widget/LinearLayout$LayoutParams;

    :goto_2
    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/settings/m;->buG:[I

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/android/settings/m;->buJ:Landroid/widget/LinearLayout$LayoutParams;

    goto :goto_1

    :cond_2
    iget-object v0, p0, Lcom/android/settings/m;->buI:Landroid/widget/LinearLayout$LayoutParams;

    goto :goto_2
.end method

.method static synthetic bhN(Lcom/android/settings/m;)V
    .locals 0

    invoke-super {p0}, Landroid/widget/BaseAdapter;->notifyDataSetChanged()V

    return-void
.end method

.method static synthetic bhO(Lcom/android/settings/m;)V
    .locals 0

    invoke-super {p0}, Landroid/widget/BaseAdapter;->notifyDataSetInvalidated()V

    return-void
.end method


# virtual methods
.method public bhJ(Landroid/widget/LinearLayout;)V
    .locals 5

    const/4 v4, 0x1

    iput-object p1, p0, Lcom/android/settings/m;->buL:Landroid/widget/LinearLayout;

    iget-object v0, p0, Lcom/android/settings/m;->buL:Landroid/widget/LinearLayout;

    const v1, 0x7f0a00d4

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/android/settings/m;->buQ:Landroid/widget/ListView;

    iget-object v0, p0, Lcom/android/settings/m;->buQ:Landroid/widget/ListView;

    invoke-virtual {v0, p0}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    iget-object v0, p0, Lcom/android/settings/m;->buQ:Landroid/widget/ListView;

    invoke-virtual {v0, p0}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    iget-object v0, p0, Lcom/android/settings/m;->buQ:Landroid/widget/ListView;

    invoke-virtual {v0, v4}, Landroid/widget/ListView;->setItemsCanFocus(Z)V

    iget-object v0, p0, Lcom/android/settings/m;->buL:Landroid/widget/LinearLayout;

    const v1, 0x7f0a01dc

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/android/settings/m;->buN:Landroid/view/ViewGroup;

    iget-object v0, p0, Lcom/android/settings/m;->buN:Landroid/view/ViewGroup;

    invoke-virtual {v0, p0}, Landroid/view/ViewGroup;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/android/settings/m;->buN:Landroid/view/ViewGroup;

    const v1, 0x7f0a01cd

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/android/settings/m;->buO:Landroid/widget/ImageView;

    iget-object v0, p0, Lcom/android/settings/m;->buO:Landroid/widget/ImageView;

    invoke-direct {p0}, Lcom/android/settings/m;->bhH()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    iget-object v0, p0, Lcom/android/settings/m;->buN:Landroid/view/ViewGroup;

    const v1, 0x7f0a01d9

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iget-object v1, p0, Lcom/android/settings/m;->buS:Lcom/android/settings/k;

    iget v2, p0, Lcom/android/settings/m;->buM:I

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v4, v3, v0}, Lcom/android/settings/k;->getGroupView(IZLandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    return-void
.end method

.method public bhK(Z)V
    .locals 3

    const/4 v0, 0x0

    if-eqz p1, :cond_0

    iget-object v1, p0, Lcom/android/settings/m;->buS:Lcom/android/settings/k;

    iget v2, p0, Lcom/android/settings/m;->buM:I

    invoke-virtual {v1, v2, v0}, Lcom/android/settings/k;->bhu(IZ)Z

    move-result v0

    :cond_0
    iput-boolean v0, p0, Lcom/android/settings/m;->buP:Z

    invoke-direct {p0}, Lcom/android/settings/m;->bhI()V

    return-void
.end method

.method public bhL(Z)V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/m;->buN:Landroid/view/ViewGroup;

    const v1, 0x7f0a01da

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    return-void

    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public bhM(Z)V
    .locals 2

    iget-object v1, p0, Lcom/android/settings/m;->buN:Landroid/view/ViewGroup;

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->setVisibility(I)V

    return-void

    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public getCount()I
    .locals 2

    iget-object v0, p0, Lcom/android/settings/m;->buS:Lcom/android/settings/k;

    iget v1, p0, Lcom/android/settings/m;->buM:I

    invoke-virtual {v0, v1}, Lcom/android/settings/k;->getChildrenCount(I)I

    move-result v0

    return v0
.end method

.method public getItem(I)Lcom/android/settings/p;
    .locals 2

    iget-object v0, p0, Lcom/android/settings/m;->buS:Lcom/android/settings/k;

    iget v1, p0, Lcom/android/settings/m;->buM:I

    invoke-virtual {v0, v1, p1}, Lcom/android/settings/k;->getChild(II)Lcom/android/settings/p;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getItem(I)Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0, p1}, Lcom/android/settings/m;->getItem(I)Lcom/android/settings/p;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2

    iget-object v0, p0, Lcom/android/settings/m;->buS:Lcom/android/settings/k;

    iget v1, p0, Lcom/android/settings/m;->buM:I

    invoke-virtual {v0, v1, p1}, Lcom/android/settings/k;->getChildId(II)J

    move-result-wide v0

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 6

    iget-object v0, p0, Lcom/android/settings/m;->buS:Lcom/android/settings/k;

    iget v1, p0, Lcom/android/settings/m;->buM:I

    const/4 v3, 0x0

    move v2, p1

    move-object v4, p2

    move-object v5, p3

    invoke-virtual/range {v0 .. v5}, Lcom/android/settings/k;->getChildView(IIZLandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public notifyDataSetChanged()V
    .locals 1

    iget-object v0, p0, Lcom/android/settings/m;->buS:Lcom/android/settings/k;

    invoke-virtual {v0}, Lcom/android/settings/k;->notifyDataSetChanged()V

    return-void
.end method

.method public notifyDataSetInvalidated()V
    .locals 1

    iget-object v0, p0, Lcom/android/settings/m;->buS:Lcom/android/settings/k;

    invoke-virtual {v0}, Lcom/android/settings/k;->notifyDataSetInvalidated()V

    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 1

    invoke-direct {p0}, Lcom/android/settings/m;->bhG()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/android/settings/m;->buP:Z

    xor-int/lit8 v0, v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/android/settings/m;->buP:Z

    invoke-direct {p0}, Lcom/android/settings/m;->bhI()V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/m;->buT:Lcom/android/settings/TrustedCredentialsSettings;

    invoke-virtual {p0, p3}, Lcom/android/settings/m;->getItem(I)Lcom/android/settings/p;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/settings/TrustedCredentialsSettings;->bhg(Lcom/android/settings/TrustedCredentialsSettings;Lcom/android/settings/p;)V

    return-void
.end method
