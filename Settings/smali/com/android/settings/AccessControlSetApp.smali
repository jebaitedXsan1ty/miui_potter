.class public Lcom/android/settings/AccessControlSetApp;
.super Lmiui/preference/PreferenceActivity;
.source "AccessControlSetApp.java"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceChangeListener;


# static fields
.field public static final bKp:Ljava/util/HashSet;


# instance fields
.field private bKq:Landroid/security/ChooseLockSettingsHelper;

.field private final bKr:Ljava/util/Comparator;

.field private bKs:Landroid/os/Handler;

.field private bKt:Z

.field private bKu:Landroid/content/pm/PackageManager;

.field private bKv:Lmiui/security/SecurityManager;

.field private bKw:Landroid/os/HandlerThread;

.field private bKx:Landroid/os/Handler;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    sput-object v0, Lcom/android/settings/AccessControlSetApp;->bKp:Ljava/util/HashSet;

    sget-object v0, Lcom/android/settings/AccessControlSetApp;->bKp:Ljava/util/HashSet;

    const-string/jumbo v1, "com.android.soundrecorder"

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/android/settings/AccessControlSetApp;->bKp:Ljava/util/HashSet;

    const-string/jumbo v1, "com.android.contacts"

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/android/settings/AccessControlSetApp;->bKp:Ljava/util/HashSet;

    const-string/jumbo v1, "com.android.browser"

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/android/settings/AccessControlSetApp;->bKp:Ljava/util/HashSet;

    const-string/jumbo v1, "com.android.stk"

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/android/settings/AccessControlSetApp;->bKp:Ljava/util/HashSet;

    const-string/jumbo v1, "com.android.mms"

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    const-string/jumbo v0, "is_pad"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lmiui/util/FeatureParser;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/android/settings/AccessControlSetApp;->bKp:Ljava/util/HashSet;

    const-string/jumbo v1, "com.android.thememanager"

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    :cond_0
    sget-object v0, Lcom/android/settings/AccessControlSetApp;->bKp:Ljava/util/HashSet;

    const-string/jumbo v1, "com.android.deskclock"

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/android/settings/AccessControlSetApp;->bKp:Ljava/util/HashSet;

    const-string/jumbo v1, "com.android.gallery3d"

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/android/settings/AccessControlSetApp;->bKp:Ljava/util/HashSet;

    const-string/jumbo v1, "pl.zdunex25.updater"

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/android/settings/AccessControlSetApp;->bKp:Ljava/util/HashSet;

    const-string/jumbo v1, "com.mi.android.globalFileexplorer"

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/android/settings/AccessControlSetApp;->bKp:Ljava/util/HashSet;

    const-string/jumbo v1, "com.android.fileexplorer"

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/android/settings/AccessControlSetApp;->bKp:Ljava/util/HashSet;

    const-string/jumbo v1, "com.android.calendar"

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/android/settings/AccessControlSetApp;->bKp:Ljava/util/HashSet;

    const-string/jumbo v1, "com.android.vending"

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/android/settings/AccessControlSetApp;->bKp:Ljava/util/HashSet;

    const-string/jumbo v1, "com.android.apps.tag"

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/android/settings/AccessControlSetApp;->bKp:Ljava/util/HashSet;

    const-string/jumbo v1, "com.android.email"

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/android/settings/AccessControlSetApp;->bKp:Ljava/util/HashSet;

    const-string/jumbo v1, "com.miui.networkassistant"

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/android/settings/AccessControlSetApp;->bKp:Ljava/util/HashSet;

    const-string/jumbo v1, "com.android.providers.downloads.ui"

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/android/settings/AccessControlSetApp;->bKp:Ljava/util/HashSet;

    const-string/jumbo v1, "com.google.android.talk"

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/android/settings/AccessControlSetApp;->bKp:Ljava/util/HashSet;

    const-string/jumbo v1, "com.google.android.gm"

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/android/settings/AccessControlSetApp;->bKp:Ljava/util/HashSet;

    const-string/jumbo v1, "com.android.camera"

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/android/settings/AccessControlSetApp;->bKp:Ljava/util/HashSet;

    const-string/jumbo v1, "com.miui.camera"

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/android/settings/AccessControlSetApp;->bKp:Ljava/util/HashSet;

    const-string/jumbo v1, "com.miui.gallery"

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/android/settings/AccessControlSetApp;->bKp:Ljava/util/HashSet;

    const-string/jumbo v1, "com.miui.player"

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/android/settings/AccessControlSetApp;->bKp:Ljava/util/HashSet;

    const-string/jumbo v1, "com.miui.backup"

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/android/settings/AccessControlSetApp;->bKp:Ljava/util/HashSet;

    const-string/jumbo v1, "com.miui.notes"

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/android/settings/AccessControlSetApp;->bKp:Ljava/util/HashSet;

    const-string/jumbo v1, "com.xiaomi.market"

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/android/settings/AccessControlSetApp;->bKp:Ljava/util/HashSet;

    const-string/jumbo v1, "com.miui.antispam"

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/android/settings/AccessControlSetApp;->bKp:Ljava/util/HashSet;

    const-string/jumbo v1, "com.miui.video"

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/android/settings/AccessControlSetApp;->bKp:Ljava/util/HashSet;

    const-string/jumbo v1, "net.cactii.flash2"

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/android/settings/AccessControlSetApp;->bKp:Ljava/util/HashSet;

    const-string/jumbo v1, "com.xiaomi.gamecenter"

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/android/settings/AccessControlSetApp;->bKp:Ljava/util/HashSet;

    const-string/jumbo v1, "com.google.android.music"

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/android/settings/AccessControlSetApp;->bKp:Ljava/util/HashSet;

    const-string/jumbo v1, "com.google.android.youtube"

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/android/settings/AccessControlSetApp;->bKp:Ljava/util/HashSet;

    const-string/jumbo v1, "com.google.android.apps.plus"

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/android/settings/AccessControlSetApp;->bKp:Ljava/util/HashSet;

    const-string/jumbo v1, "com.facebook.orca"

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/android/settings/AccessControlSetApp;->bKp:Ljava/util/HashSet;

    const-string/jumbo v1, "com.android.chrome"

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/android/settings/AccessControlSetApp;->bKp:Ljava/util/HashSet;

    const-string/jumbo v1, "com.xiaomi.account"

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/android/settings/AccessControlSetApp;->bKp:Ljava/util/HashSet;

    const-string/jumbo v1, "com.xiaomi.payment"

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/android/settings/AccessControlSetApp;->bKp:Ljava/util/HashSet;

    const-string/jumbo v1, "com.mipay.wallet"

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/android/settings/AccessControlSetApp;->bKp:Ljava/util/HashSet;

    const-string/jumbo v1, "com.xiaomi.jr"

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/android/settings/AccessControlSetApp;->bKp:Ljava/util/HashSet;

    const-string/jumbo v1, "com.htc.album"

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lmiui/preference/PreferenceActivity;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/settings/AccessControlSetApp;->bKt:Z

    new-instance v0, Lcom/android/settings/hl;

    invoke-direct {v0, p0}, Lcom/android/settings/hl;-><init>(Lcom/android/settings/AccessControlSetApp;)V

    iput-object v0, p0, Lcom/android/settings/AccessControlSetApp;->bKr:Ljava/util/Comparator;

    return-void
.end method

.method private bAX(Ljava/util/ArrayList;Z)V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/AccessControlSetApp;->bKx:Landroid/os/Handler;

    new-instance v1, Lcom/android/settings/hn;

    invoke-direct {v1, p0, p1, p2}, Lcom/android/settings/hn;-><init>(Lcom/android/settings/AccessControlSetApp;Ljava/util/ArrayList;Z)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method private bAY()V
    .locals 8

    const/4 v7, 0x1

    const/4 v6, 0x0

    const v0, 0x7f150004

    invoke-virtual {p0, v0}, Lcom/android/settings/AccessControlSetApp;->addPreferencesFromResource(I)V

    iget-object v0, p0, Lcom/android/settings/AccessControlSetApp;->bKu:Landroid/content/pm/PackageManager;

    invoke-virtual {v0, v6}, Landroid/content/pm/PackageManager;->getInstalledApplications(I)Ljava/util/List;

    move-result-object v0

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/ApplicationInfo;

    if-eqz v0, :cond_0

    iget v4, v0, Landroid/content/pm/ApplicationInfo;->flags:I

    and-int/lit8 v4, v4, 0x1

    if-ne v4, v7, :cond_1

    sget-object v4, Lcom/android/settings/AccessControlSetApp;->bKp:Ljava/util/HashSet;

    iget-object v5, v0, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v4

    xor-int/lit8 v4, v4, 0x1

    if-nez v4, :cond_0

    :cond_1
    iget-object v4, p0, Lcom/android/settings/AccessControlSetApp;->bKv:Lmiui/security/SecurityManager;

    iget-object v5, v0, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v4, v5}, Lmiui/security/SecurityManager;->getApplicationAccessControlEnabled(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_2
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_3
    invoke-direct {p0, v1, v7}, Lcom/android/settings/AccessControlSetApp;->bAX(Ljava/util/ArrayList;Z)V

    invoke-direct {p0, v2, v6}, Lcom/android/settings/AccessControlSetApp;->bAX(Ljava/util/ArrayList;Z)V

    return-void
.end method

.method static synthetic bAZ(Lcom/android/settings/AccessControlSetApp;)Ljava/util/Comparator;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/AccessControlSetApp;->bKr:Ljava/util/Comparator;

    return-object v0
.end method

.method static synthetic bBa(Lcom/android/settings/AccessControlSetApp;)Landroid/content/pm/PackageManager;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/AccessControlSetApp;->bKu:Landroid/content/pm/PackageManager;

    return-object v0
.end method

.method static synthetic bBb(Lcom/android/settings/AccessControlSetApp;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/AccessControlSetApp;->bAY()V

    return-void
.end method


# virtual methods
.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 1

    const/16 v0, 0x64

    if-ne p1, v0, :cond_0

    const/4 v0, -0x1

    if-ne p2, v0, :cond_1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/settings/AccessControlSetApp;->bKt:Z

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0}, Lcom/android/settings/AccessControlSetApp;->finish()V

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4

    invoke-super {p0, p1}, Lmiui/preference/PreferenceActivity;->onCreate(Landroid/os/Bundle;)V

    const-string/jumbo v0, "security"

    invoke-virtual {p0, v0}, Lcom/android/settings/AccessControlSetApp;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmiui/security/SecurityManager;

    iput-object v0, p0, Lcom/android/settings/AccessControlSetApp;->bKv:Lmiui/security/SecurityManager;

    if-eqz p1, :cond_0

    const-string/jumbo v0, "password_confirmed"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/settings/AccessControlSetApp;->bKt:Z

    :cond_0
    new-instance v0, Landroid/security/ChooseLockSettingsHelper;

    invoke-direct {v0, p0}, Landroid/security/ChooseLockSettingsHelper;-><init>(Landroid/app/Activity;)V

    iput-object v0, p0, Lcom/android/settings/AccessControlSetApp;->bKq:Landroid/security/ChooseLockSettingsHelper;

    iget-object v0, p0, Lcom/android/settings/AccessControlSetApp;->bKq:Landroid/security/ChooseLockSettingsHelper;

    invoke-virtual {v0}, Landroid/security/ChooseLockSettingsHelper;->isACLockEnabled()Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/settings/AccessControlSetApp;->bKt:Z

    :cond_1
    invoke-virtual {p0}, Lcom/android/settings/AccessControlSetApp;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/AccessControlSetApp;->bKu:Landroid/content/pm/PackageManager;

    new-instance v0, Landroid/os/HandlerThread;

    const-string/jumbo v1, "AccessControlSetApp.Loader"

    const/4 v2, -0x2

    invoke-direct {v0, v1, v2}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;I)V

    iput-object v0, p0, Lcom/android/settings/AccessControlSetApp;->bKw:Landroid/os/HandlerThread;

    iget-object v0, p0, Lcom/android/settings/AccessControlSetApp;->bKw:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    new-instance v0, Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/settings/AccessControlSetApp;->bKw:Landroid/os/HandlerThread;

    invoke-virtual {v1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/android/settings/AccessControlSetApp;->bKx:Landroid/os/Handler;

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/android/settings/AccessControlSetApp;->bKs:Landroid/os/Handler;

    iget-boolean v0, p0, Lcom/android/settings/AccessControlSetApp;->bKt:Z

    if-eqz v0, :cond_2

    invoke-direct {p0}, Lcom/android/settings/AccessControlSetApp;->bAY()V

    :goto_0
    return-void

    :cond_2
    iget-object v0, p0, Lcom/android/settings/AccessControlSetApp;->bKs:Landroid/os/Handler;

    new-instance v1, Lcom/android/settings/hm;

    invoke-direct {v1, p0}, Lcom/android/settings/hm;-><init>(Lcom/android/settings/AccessControlSetApp;)V

    const-wide/16 v2, 0x1f4

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0
.end method

.method public onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 3

    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/AccessControlSetApp;->bKv:Lmiui/security/SecurityManager;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-virtual {v1, v0, v2}, Lmiui/security/SecurityManager;->setApplicationAccessControlEnabled(Ljava/lang/String;Z)V

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/android/settings/AccessControlSetApp;->bKv:Lmiui/security/SecurityManager;

    invoke-virtual {v1, v0}, Lmiui/security/SecurityManager;->removeAccessControlPass(Ljava/lang/String;)V

    :cond_0
    const/4 v0, 0x1

    return v0
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Lmiui/preference/PreferenceActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    const-string/jumbo v0, "password_confirmed"

    iget-boolean v1, p0, Lcom/android/settings/AccessControlSetApp;->bKt:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    return-void
.end method

.method protected onStart()V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/AccessControlSetApp;->bKq:Landroid/security/ChooseLockSettingsHelper;

    invoke-virtual {v0}, Landroid/security/ChooseLockSettingsHelper;->isACLockEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/android/settings/AccessControlSetApp;->bKt:Z

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/android/settings/ConfirmAccessControl;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/16 v1, 0x64

    invoke-virtual {p0, v0, v1}, Lcom/android/settings/AccessControlSetApp;->startActivityForResult(Landroid/content/Intent;I)V

    :goto_0
    invoke-super {p0}, Lmiui/preference/PreferenceActivity;->onStart()V

    return-void

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/settings/AccessControlSetApp;->bKt:Z

    goto :goto_0
.end method

.method protected onStop()V
    .locals 1

    invoke-super {p0}, Lmiui/preference/PreferenceActivity;->onStop()V

    iget-boolean v0, p0, Lcom/android/settings/AccessControlSetApp;->bKt:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/settings/AccessControlSetApp;->bKt:Z

    :cond_0
    return-void
.end method
