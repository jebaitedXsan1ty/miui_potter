.class Lcom/android/settings/accounts/MiuiAccountSettings$AccountPreference;
.super Landroid/preference/Preference;
.source "MiuiAccountSettings.java"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceClickListener;


# instance fields
.field private final aAf:Ljava/lang/String;

.field private final aAg:Landroid/os/Bundle;

.field private final aAh:I

.field private final aAi:Ljava/lang/String;

.field final synthetic aAj:Lcom/android/settings/accounts/MiuiAccountSettings;

.field private final mTitle:Ljava/lang/CharSequence;


# direct methods
.method public constructor <init>(Lcom/android/settings/accounts/MiuiAccountSettings;Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/String;ILjava/lang/String;Landroid/os/Bundle;Landroid/graphics/drawable/Drawable;)V
    .locals 1

    iput-object p1, p0, Lcom/android/settings/accounts/MiuiAccountSettings$AccountPreference;->aAj:Lcom/android/settings/accounts/MiuiAccountSettings;

    invoke-direct {p0, p2}, Landroid/preference/Preference;-><init>(Landroid/content/Context;)V

    iput-object p3, p0, Lcom/android/settings/accounts/MiuiAccountSettings$AccountPreference;->mTitle:Ljava/lang/CharSequence;

    iput-object p4, p0, Lcom/android/settings/accounts/MiuiAccountSettings$AccountPreference;->aAi:Ljava/lang/String;

    iput p5, p0, Lcom/android/settings/accounts/MiuiAccountSettings$AccountPreference;->aAh:I

    iput-object p6, p0, Lcom/android/settings/accounts/MiuiAccountSettings$AccountPreference;->aAf:Ljava/lang/String;

    iput-object p7, p0, Lcom/android/settings/accounts/MiuiAccountSettings$AccountPreference;->aAg:Landroid/os/Bundle;

    const v0, 0x7f0d001e

    invoke-virtual {p0, v0}, Lcom/android/settings/accounts/MiuiAccountSettings$AccountPreference;->setWidgetLayoutResource(I)V

    invoke-virtual {p0, p3}, Lcom/android/settings/accounts/MiuiAccountSettings$AccountPreference;->setTitle(Ljava/lang/CharSequence;)V

    invoke-virtual {p0, p8}, Lcom/android/settings/accounts/MiuiAccountSettings$AccountPreference;->setIcon(Landroid/graphics/drawable/Drawable;)V

    invoke-virtual {p0, p0}, Lcom/android/settings/accounts/MiuiAccountSettings$AccountPreference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    return-void
.end method

.method static synthetic aoE(Lcom/android/settings/accounts/MiuiAccountSettings$AccountPreference;)Ljava/lang/CharSequence;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/accounts/MiuiAccountSettings$AccountPreference;->mTitle:Ljava/lang/CharSequence;

    return-object v0
.end method


# virtual methods
.method public onPreferenceClick(Landroid/preference/Preference;)Z
    .locals 9

    const/4 v3, 0x0

    const/4 v4, 0x0

    iget-object v0, p0, Lcom/android/settings/accounts/MiuiAccountSettings$AccountPreference;->aAf:Ljava/lang/String;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/android/settings/accounts/MiuiAccountSettings$AccountPreference;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/accounts/MiuiAccountSettings$AccountPreference;->aAf:Ljava/lang/String;

    iget-object v2, p0, Lcom/android/settings/accounts/MiuiAccountSettings$AccountPreference;->aAg:Landroid/os/Bundle;

    iget-object v5, p0, Lcom/android/settings/accounts/MiuiAccountSettings$AccountPreference;->aAi:Ljava/lang/String;

    iget-object v6, p0, Lcom/android/settings/accounts/MiuiAccountSettings$AccountPreference;->mTitle:Ljava/lang/CharSequence;

    invoke-interface {v6}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v7

    const/4 v6, -0x1

    const/16 v8, 0xb

    invoke-static/range {v0 .. v8}, Lcom/android/settings/aq;->bqO(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;Landroid/app/Fragment;ILjava/lang/String;ILjava/lang/CharSequence;I)V

    const/4 v0, 0x1

    return v0

    :cond_0
    return v4
.end method
