.class Lcom/android/settings/accounts/MiuiAccountSettings$MasterSyncStateClickListener;
.super Ljava/lang/Object;
.source "MiuiAccountSettings.java"

# interfaces
.implements Lcom/android/settings/widget/M;


# instance fields
.field private final aAm:Landroid/os/UserHandle;

.field final synthetic aAn:Lcom/android/settings/accounts/MiuiAccountSettings;


# direct methods
.method public constructor <init>(Lcom/android/settings/accounts/MiuiAccountSettings;Landroid/os/UserHandle;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/accounts/MiuiAccountSettings$MasterSyncStateClickListener;->aAn:Lcom/android/settings/accounts/MiuiAccountSettings;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/android/settings/accounts/MiuiAccountSettings$MasterSyncStateClickListener;->aAm:Landroid/os/UserHandle;

    return-void
.end method

.method static synthetic aoF(Lcom/android/settings/accounts/MiuiAccountSettings$MasterSyncStateClickListener;)Landroid/os/UserHandle;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/accounts/MiuiAccountSettings$MasterSyncStateClickListener;->aAm:Landroid/os/UserHandle;

    return-object v0
.end method


# virtual methods
.method public VR(Lcom/android/settings/widget/TogglePreference;Z)Z
    .locals 3

    invoke-static {}, Landroid/app/ActivityManager;->isUserAMonkey()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string/jumbo v0, "AccountSettings"

    const-string/jumbo v1, "ignoring monkey\'s attempt to flip sync state"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    const/4 v0, 0x1

    return v0

    :cond_0
    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Lcom/android/settings/accounts/MiuiAccountSettings$MasterSyncStateClickListener;->aAn:Lcom/android/settings/accounts/MiuiAccountSettings;

    invoke-virtual {v1}, Lcom/android/settings/accounts/MiuiAccountSettings;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    if-eqz p2, :cond_1

    const v1, 0x7f1204e6

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    const v1, 0x7f1204e5

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    :goto_1
    new-instance v1, Lcom/android/settings/accounts/MiuiAccountSettings$MasterSyncStateClickListener$1;

    invoke-direct {v1, p0, p1, p2}, Lcom/android/settings/accounts/MiuiAccountSettings$MasterSyncStateClickListener$1;-><init>(Lcom/android/settings/accounts/MiuiAccountSettings$MasterSyncStateClickListener;Lcom/android/settings/widget/TogglePreference;Z)V

    const v2, 0x104000a

    invoke-virtual {v0, v2, v1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    const/high16 v1, 0x1040000

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Lcom/android/settings/accounts/MiuiAccountSettings$MasterSyncStateClickListener;->aAn:Lcom/android/settings/accounts/MiuiAccountSettings;

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/android/settings/accounts/MiuiAccountSettings;->aow(Lcom/android/settings/accounts/MiuiAccountSettings;Landroid/app/AlertDialog;)Landroid/app/AlertDialog;

    goto :goto_0

    :cond_1
    const v1, 0x7f1204e4

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    const v1, 0x7f1204e3

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    goto :goto_1
.end method
