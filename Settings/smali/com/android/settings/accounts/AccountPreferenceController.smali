.class public Lcom/android/settings/accounts/AccountPreferenceController;
.super Lcom/android/settings/core/e;
.source "AccountPreferenceController.java"

# interfaces
.implements Lcom/android/settingslib/c/b;
.implements Landroid/preference/Preference$OnPreferenceClickListener;
.implements Lcom/android/settings/core/lifecycle/b;
.implements Lcom/android/settings/core/lifecycle/a/d;
.implements Lcom/android/settings/core/lifecycle/a/b;


# instance fields
.field private aya:I

.field private ayb:[Ljava/lang/String;

.field private ayc:I

.field private ayd:Lcom/android/settings/accounts/AccountRestrictionHelper;

.field private aye:Lcom/android/settings/accounts/AccountPreferenceController$ManagedProfileBroadcastReceiver;

.field private ayf:Lcom/android/settings/MiuiSettingsPreferenceFragment;

.field private ayg:Landroid/preference/Preference;

.field private ayh:Landroid/util/SparseArray;

.field private ayi:Landroid/os/UserManager;

.field private mMetricsFeatureProvider:Lcom/android/settings/core/instrumentation/e;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/android/settings/MiuiSettingsPreferenceFragment;[Ljava/lang/String;)V
    .locals 1

    new-instance v0, Lcom/android/settings/accounts/AccountRestrictionHelper;

    invoke-direct {v0, p1}, Lcom/android/settings/accounts/AccountRestrictionHelper;-><init>(Landroid/content/Context;)V

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/android/settings/accounts/AccountPreferenceController;-><init>(Landroid/content/Context;Lcom/android/settings/MiuiSettingsPreferenceFragment;[Ljava/lang/String;Lcom/android/settings/accounts/AccountRestrictionHelper;)V

    return-void
.end method

.method constructor <init>(Landroid/content/Context;Lcom/android/settings/MiuiSettingsPreferenceFragment;[Ljava/lang/String;Lcom/android/settings/accounts/AccountRestrictionHelper;)V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0, p1}, Lcom/android/settings/core/e;-><init>(Landroid/content/Context;)V

    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/android/settings/accounts/AccountPreferenceController;->ayh:Landroid/util/SparseArray;

    new-instance v0, Lcom/android/settings/accounts/AccountPreferenceController$ManagedProfileBroadcastReceiver;

    invoke-direct {v0, p0, v1}, Lcom/android/settings/accounts/AccountPreferenceController$ManagedProfileBroadcastReceiver;-><init>(Lcom/android/settings/accounts/AccountPreferenceController;Lcom/android/settings/accounts/AccountPreferenceController$ManagedProfileBroadcastReceiver;)V

    iput-object v0, p0, Lcom/android/settings/accounts/AccountPreferenceController;->aye:Lcom/android/settings/accounts/AccountPreferenceController$ManagedProfileBroadcastReceiver;

    const/4 v0, 0x0

    iput v0, p0, Lcom/android/settings/accounts/AccountPreferenceController;->ayc:I

    const/4 v0, 0x1

    iput v0, p0, Lcom/android/settings/accounts/AccountPreferenceController;->aya:I

    const-string/jumbo v0, "user"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/UserManager;

    iput-object v0, p0, Lcom/android/settings/accounts/AccountPreferenceController;->ayi:Landroid/os/UserManager;

    iput-object p3, p0, Lcom/android/settings/accounts/AccountPreferenceController;->ayb:[Ljava/lang/String;

    iput-object p2, p0, Lcom/android/settings/accounts/AccountPreferenceController;->ayf:Lcom/android/settings/MiuiSettingsPreferenceFragment;

    iget-object v0, p0, Lcom/android/settings/accounts/AccountPreferenceController;->ayb:[Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/accounts/AccountPreferenceController;->ayb:[Ljava/lang/String;

    array-length v0, v0

    iput v0, p0, Lcom/android/settings/accounts/AccountPreferenceController;->ayc:I

    :cond_0
    iget-object v0, p0, Lcom/android/settings/accounts/AccountPreferenceController;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/settings/overlay/a;->aIk(Landroid/content/Context;)Lcom/android/settings/overlay/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/settings/overlay/a;->aIm()Lcom/android/settings/core/instrumentation/e;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/accounts/AccountPreferenceController;->mMetricsFeatureProvider:Lcom/android/settings/core/instrumentation/e;

    iput-object p4, p0, Lcom/android/settings/accounts/AccountPreferenceController;->ayd:Lcom/android/settings/accounts/AccountRestrictionHelper;

    return-void
.end method

.method static synthetic amA(Lcom/android/settings/accounts/AccountPreferenceController;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/accounts/AccountPreferenceController;->updateUi()V

    return-void
.end method

.method private amm(Lcom/android/settingslib/c/a;Ljava/lang/String;)Z
    .locals 5

    const/4 v4, 0x1

    const/4 v1, 0x0

    iget v0, p0, Lcom/android/settings/accounts/AccountPreferenceController;->ayc:I

    if-nez v0, :cond_0

    return v4

    :cond_0
    invoke-virtual {p1, p2}, Lcom/android/settingslib/c/a;->cgf(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v2

    if-nez v2, :cond_1

    const-string/jumbo v0, "AccountPrefController"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "No sync authorities for account type: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return v1

    :cond_1
    move v0, v1

    :goto_0
    iget v3, p0, Lcom/android/settings/accounts/AccountPreferenceController;->ayc:I

    if-ge v0, v3, :cond_3

    iget-object v3, p0, Lcom/android/settings/accounts/AccountPreferenceController;->ayb:[Ljava/lang/String;

    aget-object v3, v3, v0

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    return v4

    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_3
    return v1
.end method

.method private amo(Lcom/android/settingslib/c/a;Landroid/os/UserHandle;Landroid/util/ArrayMap;)Ljava/util/ArrayList;
    .locals 20

    invoke-virtual/range {p1 .. p1}, Lcom/android/settingslib/c/a;->cfZ()[Ljava/lang/String;

    move-result-object v15

    new-instance v16, Ljava/util/ArrayList;

    array-length v3, v15

    move-object/from16 v0, v16

    invoke-direct {v0, v3}, Ljava/util/ArrayList;-><init>(I)V

    const/4 v3, 0x0

    move v13, v3

    :goto_0
    array-length v3, v15

    if-ge v13, v3, :cond_5

    aget-object v17, v15, v13

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/settings/accounts/AccountPreferenceController;->mContext:Landroid/content/Context;

    move-object/from16 v0, v17

    invoke-static {v3, v0}, Lcom/android/settings/accounts/AccountRestrictionHelper;->alM(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    :cond_0
    :goto_1
    add-int/lit8 v3, v13, 0x1

    move v13, v3

    goto :goto_0

    :cond_1
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, v17

    invoke-direct {v0, v1, v2}, Lcom/android/settings/accounts/AccountPreferenceController;->amm(Lcom/android/settingslib/c/a;Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/settings/accounts/AccountPreferenceController;->mContext:Landroid/content/Context;

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    invoke-virtual {v0, v3, v1}, Lcom/android/settingslib/c/a;->cgd(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v9

    if-eqz v9, :cond_0

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/android/settingslib/c/a;->cgl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/android/settingslib/c/a;->cgk(Ljava/lang/String;)I

    move-result v8

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/settings/accounts/AccountPreferenceController;->mContext:Landroid/content/Context;

    invoke-static {v3}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v3

    move-object/from16 v0, v17

    move-object/from16 v1, p2

    invoke-virtual {v3, v0, v1}, Landroid/accounts/AccountManager;->getAccountsByTypeAsUser(Ljava/lang/String;Landroid/os/UserHandle;)[Landroid/accounts/Account;

    move-result-object v18

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/settings/accounts/AccountPreferenceController;->mContext:Landroid/content/Context;

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    invoke-virtual {v0, v3, v1}, Lcom/android/settingslib/c/a;->cgj(Landroid/content/Context;Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v12

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/settings/accounts/AccountPreferenceController;->ayf:Lcom/android/settings/MiuiSettingsPreferenceFragment;

    invoke-virtual {v3}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->getActivity()Landroid/app/Activity;

    move-result-object v4

    const/4 v3, 0x0

    move-object/from16 v0, v18

    array-length v0, v0

    move/from16 v19, v0

    move v14, v3

    :goto_2
    move/from16 v0, v19

    if-ge v14, v0, :cond_4

    aget-object v6, v18, v14

    invoke-static {v6}, Lcom/android/settings/accounts/AccountTypePreference;->amQ(Landroid/accounts/Account;)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p3

    invoke-virtual {v0, v3}, Landroid/util/ArrayMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/android/settings/accounts/AccountTypePreference;

    if-eqz v3, :cond_3

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_2
    :goto_3
    add-int/lit8 v3, v14, 0x1

    move v14, v3

    goto :goto_2

    :cond_3
    iget-object v3, v6, Landroid/accounts/Account;->type:Ljava/lang/String;

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Lcom/android/settingslib/c/a;->cgf(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/settings/accounts/AccountPreferenceController;->ayb:[Ljava/lang/String;

    invoke-static {v5, v3}, Lcom/android/settings/accounts/AccountRestrictionHelper;->alN([Ljava/lang/String;Ljava/util/ArrayList;)Z

    move-result v3

    if-eqz v3, :cond_2

    new-instance v11, Landroid/os/Bundle;

    invoke-direct {v11}, Landroid/os/Bundle;-><init>()V

    const-string/jumbo v3, "account"

    invoke-virtual {v11, v3, v6}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    const-string/jumbo v3, "user_handle"

    move-object/from16 v0, p2

    invoke-virtual {v11, v3, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    const-string/jumbo v3, "account_type"

    move-object/from16 v0, v17

    invoke-virtual {v11, v3, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string/jumbo v3, "account_label"

    invoke-interface {v9}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v11, v3, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string/jumbo v3, "account_title_res"

    invoke-virtual {v11, v3, v8}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string/jumbo v3, "android.intent.extra.USER"

    move-object/from16 v0, p2

    invoke-virtual {v11, v3, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    new-instance v3, Lcom/android/settings/accounts/AccountTypePreference;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/settings/accounts/AccountPreferenceController;->mMetricsFeatureProvider:Lcom/android/settings/core/instrumentation/e;

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/android/settings/accounts/AccountPreferenceController;->ayf:Lcom/android/settings/MiuiSettingsPreferenceFragment;

    invoke-virtual {v5, v10}, Lcom/android/settings/core/instrumentation/e;->ajT(Ljava/lang/Object;)I

    move-result v5

    const-class v10, Lcom/android/settings/accounts/AccountDetailDashboardFragment;

    invoke-virtual {v10}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v10

    invoke-direct/range {v3 .. v12}, Lcom/android/settings/accounts/AccountTypePreference;-><init>(Landroid/content/Context;ILandroid/accounts/Account;Ljava/lang/String;ILjava/lang/CharSequence;Ljava/lang/String;Landroid/os/Bundle;Landroid/graphics/drawable/Drawable;)V

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_3

    :cond_4
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/settings/accounts/AccountPreferenceController;->mContext:Landroid/content/Context;

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    invoke-virtual {v0, v3, v1}, Lcom/android/settingslib/c/a;->cgh(Landroid/content/Context;Ljava/lang/String;)V

    goto/16 :goto_1

    :cond_5
    new-instance v3, Lcom/android/settings/accounts/AccountPreferenceController$1;

    move-object/from16 v0, p0

    invoke-direct {v3, v0}, Lcom/android/settings/accounts/AccountPreferenceController$1;-><init>(Lcom/android/settings/accounts/AccountPreferenceController;)V

    move-object/from16 v0, v16

    invoke-static {v0, v3}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    return-object v16
.end method

.method private amp(Landroid/content/Context;Landroid/content/pm/UserInfo;)Ljava/lang/String;
    .locals 4

    const/4 v2, 0x0

    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    iget v1, p2, Landroid/content/pm/UserInfo;->id:I

    invoke-static {p1, v1}, Lcom/android/settings/aq;->bqB(Landroid/content/Context;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v1

    if-nez v1, :cond_0

    return-object v2

    :cond_0
    invoke-virtual {v0, v1}, Landroid/content/pm/PackageManager;->getApplicationLabel(Landroid/content/pm/ApplicationInfo;)Ljava/lang/CharSequence;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/accounts/AccountPreferenceController;->mContext:Landroid/content/Context;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object v0, v2, v3

    const v0, 0x7f120a1f

    invoke-virtual {v1, v0, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private amq()Z
    .locals 3

    const/4 v0, 0x1

    iget-object v1, p0, Lcom/android/settings/accounts/AccountPreferenceController;->ayi:Landroid/os/UserManager;

    invoke-virtual {v1}, Landroid/os/UserManager;->isLinkedUser()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/android/settings/accounts/AccountPreferenceController;->ayi:Landroid/os/UserManager;

    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/os/UserManager;->getProfiles(I)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ne v1, v0, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private amr()V
    .locals 3

    iget-object v0, p0, Lcom/android/settings/accounts/AccountPreferenceController;->ayh:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_1

    iget-object v0, p0, Lcom/android/settings/accounts/AccountPreferenceController;->ayh:Landroid/util/SparseArray;

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settings/accounts/AccountPreferenceController$ProfileData;

    iget-object v0, v0, Lcom/android/settings/accounts/AccountPreferenceController$ProfileData;->ayl:Lcom/android/settingslib/c/a;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/android/settingslib/c/a;->cgb()V

    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    return-void
.end method

.method private ams(Landroid/content/Context;)Lcom/android/settings/DimmableIconPreference;
    .locals 2

    new-instance v0, Lcom/android/settings/DimmableIconPreference;

    iget-object v1, p0, Lcom/android/settings/accounts/AccountPreferenceController;->ayf:Lcom/android/settings/MiuiSettingsPreferenceFragment;

    invoke-virtual {v1}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/android/settings/DimmableIconPreference;-><init>(Landroid/content/Context;)V

    const v1, 0x7f12009b

    invoke-virtual {v0, v1}, Lcom/android/settings/DimmableIconPreference;->setTitle(I)V

    const v1, 0x7f080202

    invoke-virtual {v0, v1}, Lcom/android/settings/DimmableIconPreference;->setIcon(I)V

    invoke-virtual {v0, p0}, Lcom/android/settings/DimmableIconPreference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    const/16 v1, 0x3e8

    invoke-virtual {v0, v1}, Lcom/android/settings/DimmableIconPreference;->setOrder(I)V

    return-object v0
.end method

.method private amt()Landroid/preference/Preference;
    .locals 2

    new-instance v0, Landroid/preference/Preference;

    iget-object v1, p0, Lcom/android/settings/accounts/AccountPreferenceController;->ayf:Lcom/android/settings/MiuiSettingsPreferenceFragment;

    invoke-virtual {v1}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/preference/Preference;-><init>(Landroid/content/Context;)V

    const v1, 0x7f120a1d

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setTitle(I)V

    const v1, 0x7f080240

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setIcon(I)V

    invoke-virtual {v0, p0}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    const/16 v1, 0x3e9

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setOrder(I)V

    return-object v0
.end method

.method private amu(Landroid/content/Context;)Lcom/android/settingslib/MiuiRestrictedPreference;
    .locals 2

    new-instance v0, Lcom/android/settingslib/MiuiRestrictedPreference;

    iget-object v1, p0, Lcom/android/settings/accounts/AccountPreferenceController;->ayf:Lcom/android/settings/MiuiSettingsPreferenceFragment;

    invoke-virtual {v1}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/android/settingslib/MiuiRestrictedPreference;-><init>(Landroid/content/Context;)V

    const v1, 0x7f120e0a

    invoke-virtual {v0, v1}, Lcom/android/settingslib/MiuiRestrictedPreference;->setTitle(I)V

    const v1, 0x7f080205

    invoke-virtual {v0, v1}, Lcom/android/settingslib/MiuiRestrictedPreference;->setIcon(I)V

    invoke-virtual {v0, p0}, Lcom/android/settingslib/MiuiRestrictedPreference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    const/16 v1, 0x3ea

    invoke-virtual {v0, v1}, Lcom/android/settingslib/MiuiRestrictedPreference;->setOrder(I)V

    return-object v0
.end method

.method private amv()V
    .locals 3

    iget-object v0, p0, Lcom/android/settings/accounts/AccountPreferenceController;->ayh:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_1

    iget-object v0, p0, Lcom/android/settings/accounts/AccountPreferenceController;->ayh:Landroid/util/SparseArray;

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settings/accounts/AccountPreferenceController$ProfileData;

    iget-object v0, v0, Lcom/android/settings/accounts/AccountPreferenceController$ProfileData;->ayl:Lcom/android/settingslib/c/a;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/android/settingslib/c/a;->cgi()V

    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    return-void
.end method

.method private amw(Lcom/android/settings/accounts/AccountPreferenceController$ProfileData;)V
    .locals 7

    const/4 v0, 0x0

    const/4 v3, 0x0

    iget-object v1, p0, Lcom/android/settings/accounts/AccountPreferenceController;->ayf:Lcom/android/settings/MiuiSettingsPreferenceFragment;

    invoke-virtual {v1}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->getPreferenceManager()Landroid/preference/PreferenceManager;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p1, Lcom/android/settings/accounts/AccountPreferenceController$ProfileData;->ayo:Landroid/preference/PreferenceGroup;

    invoke-virtual {v1}, Landroid/preference/PreferenceGroup;->getPreferenceManager()Landroid/preference/PreferenceManager;

    move-result-object v1

    if-nez v1, :cond_1

    :cond_0
    return-void

    :cond_1
    iget-object v1, p1, Lcom/android/settings/accounts/AccountPreferenceController$ProfileData;->ayq:Landroid/content/pm/UserInfo;

    invoke-virtual {v1}, Landroid/content/pm/UserInfo;->isEnabled()Z

    move-result v1

    if-eqz v1, :cond_5

    new-instance v2, Landroid/util/ArrayMap;

    iget-object v1, p1, Lcom/android/settings/accounts/AccountPreferenceController$ProfileData;->ayj:Landroid/util/ArrayMap;

    invoke-direct {v2, v1}, Landroid/util/ArrayMap;-><init>(Landroid/util/ArrayMap;)V

    iget-object v1, p1, Lcom/android/settings/accounts/AccountPreferenceController$ProfileData;->ayl:Lcom/android/settingslib/c/a;

    iget-object v3, p1, Lcom/android/settings/accounts/AccountPreferenceController$ProfileData;->ayq:Landroid/content/pm/UserInfo;

    invoke-virtual {v3}, Landroid/content/pm/UserInfo;->getUserHandle()Landroid/os/UserHandle;

    move-result-object v3

    invoke-direct {p0, v1, v3, v2}, Lcom/android/settings/accounts/AccountPreferenceController;->amo(Lcom/android/settingslib/c/a;Landroid/os/UserHandle;Landroid/util/ArrayMap;)Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v4

    move v1, v0

    :goto_0
    if-ge v1, v4, :cond_3

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settings/accounts/AccountTypePreference;

    invoke-virtual {v0, v1}, Lcom/android/settings/accounts/AccountTypePreference;->setOrder(I)V

    invoke-virtual {v0}, Lcom/android/settings/accounts/AccountTypePreference;->getKey()Ljava/lang/String;

    move-result-object v5

    iget-object v6, p1, Lcom/android/settings/accounts/AccountPreferenceController$ProfileData;->ayj:Landroid/util/ArrayMap;

    invoke-virtual {v6, v5}, Landroid/util/ArrayMap;->containsKey(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_2

    iget-object v6, p1, Lcom/android/settings/accounts/AccountPreferenceController$ProfileData;->ayo:Landroid/preference/PreferenceGroup;

    invoke-virtual {v6, v0}, Landroid/preference/PreferenceGroup;->addPreference(Landroid/preference/Preference;)Z

    iget-object v6, p1, Lcom/android/settings/accounts/AccountPreferenceController$ProfileData;->ayj:Landroid/util/ArrayMap;

    invoke-virtual {v6, v5, v0}, Landroid/util/ArrayMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_3
    iget-object v0, p1, Lcom/android/settings/accounts/AccountPreferenceController$ProfileData;->ayk:Lcom/android/settings/DimmableIconPreference;

    if-eqz v0, :cond_4

    iget-object v0, p1, Lcom/android/settings/accounts/AccountPreferenceController$ProfileData;->ayo:Landroid/preference/PreferenceGroup;

    iget-object v1, p1, Lcom/android/settings/accounts/AccountPreferenceController$ProfileData;->ayk:Lcom/android/settings/DimmableIconPreference;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceGroup;->addPreference(Landroid/preference/Preference;)Z

    :cond_4
    invoke-virtual {v2}, Landroid/util/ArrayMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iget-object v3, p1, Lcom/android/settings/accounts/AccountPreferenceController$ProfileData;->ayo:Landroid/preference/PreferenceGroup;

    iget-object v1, p1, Lcom/android/settings/accounts/AccountPreferenceController$ProfileData;->ayj:Landroid/util/ArrayMap;

    invoke-virtual {v1, v0}, Landroid/util/ArrayMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/preference/Preference;

    invoke-virtual {v3, v1}, Landroid/preference/PreferenceGroup;->removePreference(Landroid/preference/Preference;)Z

    iget-object v1, p1, Lcom/android/settings/accounts/AccountPreferenceController$ProfileData;->ayj:Landroid/util/ArrayMap;

    invoke-virtual {v1, v0}, Landroid/util/ArrayMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    :cond_5
    iget-object v1, p1, Lcom/android/settings/accounts/AccountPreferenceController$ProfileData;->ayo:Landroid/preference/PreferenceGroup;

    invoke-virtual {v1}, Landroid/preference/PreferenceGroup;->removeAll()V

    iget-object v1, p0, Lcom/android/settings/accounts/AccountPreferenceController;->ayg:Landroid/preference/Preference;

    if-nez v1, :cond_6

    new-instance v1, Landroid/preference/Preference;

    iget-object v2, p0, Lcom/android/settings/accounts/AccountPreferenceController;->ayf:Lcom/android/settings/MiuiSettingsPreferenceFragment;

    invoke-virtual {v2}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/preference/Preference;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/android/settings/accounts/AccountPreferenceController;->ayg:Landroid/preference/Preference;

    :cond_6
    iget-object v1, p0, Lcom/android/settings/accounts/AccountPreferenceController;->ayg:Landroid/preference/Preference;

    invoke-virtual {v1, v0}, Landroid/preference/Preference;->setEnabled(Z)V

    iget-object v0, p0, Lcom/android/settings/accounts/AccountPreferenceController;->ayg:Landroid/preference/Preference;

    const v1, 0x7f080114

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setIcon(I)V

    iget-object v0, p0, Lcom/android/settings/accounts/AccountPreferenceController;->ayg:Landroid/preference/Preference;

    invoke-virtual {v0, v3}, Landroid/preference/Preference;->setTitle(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/android/settings/accounts/AccountPreferenceController;->ayg:Landroid/preference/Preference;

    const v1, 0x7f120a1c

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setSummary(I)V

    iget-object v0, p1, Lcom/android/settings/accounts/AccountPreferenceController$ProfileData;->ayo:Landroid/preference/PreferenceGroup;

    iget-object v1, p0, Lcom/android/settings/accounts/AccountPreferenceController;->ayg:Landroid/preference/Preference;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceGroup;->addPreference(Landroid/preference/Preference;)Z

    :cond_7
    iget-object v0, p1, Lcom/android/settings/accounts/AccountPreferenceController$ProfileData;->ayp:Lcom/android/settingslib/MiuiRestrictedPreference;

    if-eqz v0, :cond_8

    iget-object v0, p1, Lcom/android/settings/accounts/AccountPreferenceController$ProfileData;->ayo:Landroid/preference/PreferenceGroup;

    iget-object v1, p1, Lcom/android/settings/accounts/AccountPreferenceController$ProfileData;->ayp:Lcom/android/settingslib/MiuiRestrictedPreference;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceGroup;->addPreference(Landroid/preference/Preference;)Z

    :cond_8
    iget-object v0, p1, Lcom/android/settings/accounts/AccountPreferenceController$ProfileData;->aym:Landroid/preference/Preference;

    if-eqz v0, :cond_9

    iget-object v0, p1, Lcom/android/settings/accounts/AccountPreferenceController$ProfileData;->ayo:Landroid/preference/PreferenceGroup;

    iget-object v1, p1, Lcom/android/settings/accounts/AccountPreferenceController$ProfileData;->aym:Landroid/preference/Preference;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceGroup;->addPreference(Landroid/preference/Preference;)Z

    :cond_9
    return-void
.end method

.method private amx(Landroid/content/pm/UserInfo;)V
    .locals 8

    const/4 v7, 0x1

    const/4 v6, 0x0

    iget-object v0, p0, Lcom/android/settings/accounts/AccountPreferenceController;->ayf:Lcom/android/settings/MiuiSettingsPreferenceFragment;

    invoke-virtual {v0}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->getPreferenceManager()Landroid/preference/PreferenceManager;

    move-result-object v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/settings/accounts/AccountPreferenceController;->ayh:Landroid/util/SparseArray;

    iget v1, p1, Landroid/content/pm/UserInfo;->id:I

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settings/accounts/AccountPreferenceController$ProfileData;

    if-eqz v0, :cond_2

    iput-boolean v6, v0, Lcom/android/settings/accounts/AccountPreferenceController$ProfileData;->ayn:Z

    invoke-virtual {p1}, Landroid/content/pm/UserInfo;->isEnabled()Z

    move-result v1

    if-eqz v1, :cond_1

    new-instance v1, Lcom/android/settingslib/c/a;

    iget-object v2, p0, Lcom/android/settings/accounts/AccountPreferenceController;->mContext:Landroid/content/Context;

    invoke-virtual {p1}, Landroid/content/pm/UserInfo;->getUserHandle()Landroid/os/UserHandle;

    move-result-object v3

    invoke-direct {v1, v2, v3, p0}, Lcom/android/settingslib/c/a;-><init>(Landroid/content/Context;Landroid/os/UserHandle;Lcom/android/settingslib/c/b;)V

    iput-object v1, v0, Lcom/android/settings/accounts/AccountPreferenceController$ProfileData;->ayl:Lcom/android/settingslib/c/a;

    :cond_1
    return-void

    :cond_2
    iget-object v0, p0, Lcom/android/settings/accounts/AccountPreferenceController;->mContext:Landroid/content/Context;

    new-instance v1, Lcom/android/settings/accounts/AccountPreferenceController$ProfileData;

    invoke-direct {v1}, Lcom/android/settings/accounts/AccountPreferenceController$ProfileData;-><init>()V

    iput-object p1, v1, Lcom/android/settings/accounts/AccountPreferenceController$ProfileData;->ayq:Landroid/content/pm/UserInfo;

    iget-object v2, p0, Lcom/android/settings/accounts/AccountPreferenceController;->ayd:Lcom/android/settings/accounts/AccountRestrictionHelper;

    iget-object v3, p0, Lcom/android/settings/accounts/AccountPreferenceController;->ayf:Lcom/android/settings/MiuiSettingsPreferenceFragment;

    invoke-virtual {v3}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/android/settings/accounts/AccountRestrictionHelper;->alJ(Landroid/content/Context;)Lcom/android/settings/cA;

    move-result-object v2

    iget v3, p0, Lcom/android/settings/accounts/AccountPreferenceController;->aya:I

    add-int/lit8 v4, v3, 0x1

    iput v4, p0, Lcom/android/settings/accounts/AccountPreferenceController;->aya:I

    invoke-virtual {v2, v3}, Lcom/android/settings/cA;->setOrder(I)V

    invoke-direct {p0}, Lcom/android/settings/accounts/AccountPreferenceController;->amq()Z

    move-result v3

    if-eqz v3, :cond_5

    new-array v3, v7, [Ljava/lang/Object;

    iget-object v4, p1, Landroid/content/pm/UserInfo;->name:Ljava/lang/String;

    aput-object v4, v3, v6

    const v4, 0x7f120081

    invoke-virtual {v0, v4, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/android/settings/cA;->setTitle(Ljava/lang/CharSequence;)V

    iget-object v3, p0, Lcom/android/settings/accounts/AccountPreferenceController;->mContext:Landroid/content/Context;

    const v4, 0x7f120084

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/android/settings/cA;->bTs(Ljava/lang/String;)V

    :goto_0
    iget-object v3, p0, Lcom/android/settings/accounts/AccountPreferenceController;->ayf:Lcom/android/settings/MiuiSettingsPreferenceFragment;

    invoke-virtual {v3}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v3

    if-eqz v3, :cond_3

    invoke-virtual {v3, v2}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    :cond_3
    iput-object v2, v1, Lcom/android/settings/accounts/AccountPreferenceController$ProfileData;->ayo:Landroid/preference/PreferenceGroup;

    invoke-virtual {p1}, Landroid/content/pm/UserInfo;->isEnabled()Z

    move-result v2

    if-eqz v2, :cond_4

    new-instance v2, Lcom/android/settingslib/c/a;

    invoke-virtual {p1}, Landroid/content/pm/UserInfo;->getUserHandle()Landroid/os/UserHandle;

    move-result-object v3

    invoke-direct {v2, v0, v3, p0}, Lcom/android/settingslib/c/a;-><init>(Landroid/content/Context;Landroid/os/UserHandle;Lcom/android/settingslib/c/b;)V

    iput-object v2, v1, Lcom/android/settings/accounts/AccountPreferenceController$ProfileData;->ayl:Lcom/android/settingslib/c/a;

    invoke-direct {p0, v0}, Lcom/android/settings/accounts/AccountPreferenceController;->ams(Landroid/content/Context;)Lcom/android/settings/DimmableIconPreference;

    move-result-object v0

    iput-object v0, v1, Lcom/android/settings/accounts/AccountPreferenceController$ProfileData;->ayk:Lcom/android/settings/DimmableIconPreference;

    iget-object v0, p0, Lcom/android/settings/accounts/AccountPreferenceController;->ayd:Lcom/android/settings/accounts/AccountRestrictionHelper;

    iget-object v2, v1, Lcom/android/settings/accounts/AccountPreferenceController$ProfileData;->ayk:Lcom/android/settings/DimmableIconPreference;

    const-string/jumbo v3, "no_modify_accounts"

    iget v4, p1, Landroid/content/pm/UserInfo;->id:I

    invoke-virtual {v0, v2, v3, v4}, Lcom/android/settings/accounts/AccountRestrictionHelper;->alK(Lcom/android/settingslib/MiuiRestrictedPreference;Ljava/lang/String;I)V

    :cond_4
    iget-object v0, p0, Lcom/android/settings/accounts/AccountPreferenceController;->ayh:Landroid/util/SparseArray;

    iget v2, p1, Landroid/content/pm/UserInfo;->id:I

    invoke-virtual {v0, v2, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    new-instance v0, Lcom/android/settings/search2/SearchFeatureProviderImpl;

    invoke-direct {v0}, Lcom/android/settings/search2/SearchFeatureProviderImpl;-><init>()V

    iget-object v1, p0, Lcom/android/settings/accounts/AccountPreferenceController;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/android/settings/search2/SearchFeatureProviderImpl;->getIndexingManager(Landroid/content/Context;)Lcom/android/settings/search2/DatabaseIndexingManager;

    move-result-object v0

    const-class v1, Lcom/android/settings/accounts/UserAndAccountDashboardFragment;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, v7}, Lcom/android/settings/search2/DatabaseIndexingManager;->updateFromClassNameResource(Ljava/lang/String;Z)V

    return-void

    :cond_5
    invoke-virtual {p1}, Landroid/content/pm/UserInfo;->isManagedProfile()Z

    move-result v3

    if-eqz v3, :cond_6

    const v3, 0x7f1203ec

    invoke-virtual {v2, v3}, Lcom/android/settings/cA;->setTitle(I)V

    invoke-direct {p0, v0, p1}, Lcom/android/settings/accounts/AccountPreferenceController;->amp(Landroid/content/Context;Landroid/content/pm/UserInfo;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/android/settings/cA;->setSummary(Ljava/lang/CharSequence;)V

    iget-object v4, p0, Lcom/android/settings/accounts/AccountPreferenceController;->mContext:Landroid/content/Context;

    new-array v5, v7, [Ljava/lang/Object;

    aput-object v3, v5, v6

    const v3, 0x7f120043

    invoke-virtual {v4, v3, v5}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/android/settings/cA;->bTs(Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lcom/android/settings/accounts/AccountPreferenceController;->amu(Landroid/content/Context;)Lcom/android/settingslib/MiuiRestrictedPreference;

    move-result-object v3

    iput-object v3, v1, Lcom/android/settings/accounts/AccountPreferenceController$ProfileData;->ayp:Lcom/android/settingslib/MiuiRestrictedPreference;

    iget-object v3, p0, Lcom/android/settings/accounts/AccountPreferenceController;->ayd:Lcom/android/settings/accounts/AccountRestrictionHelper;

    iget-object v4, v1, Lcom/android/settings/accounts/AccountPreferenceController$ProfileData;->ayp:Lcom/android/settingslib/MiuiRestrictedPreference;

    const-string/jumbo v5, "no_remove_managed_profile"

    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v6

    invoke-virtual {v3, v4, v5, v6}, Lcom/android/settings/accounts/AccountRestrictionHelper;->alK(Lcom/android/settingslib/MiuiRestrictedPreference;Ljava/lang/String;I)V

    invoke-direct {p0}, Lcom/android/settings/accounts/AccountPreferenceController;->amt()Landroid/preference/Preference;

    move-result-object v3

    iput-object v3, v1, Lcom/android/settings/accounts/AccountPreferenceController$ProfileData;->aym:Landroid/preference/Preference;

    goto/16 :goto_0

    :cond_6
    const v3, 0x7f1203eb

    invoke-virtual {v2, v3}, Lcom/android/settings/cA;->setTitle(I)V

    iget-object v3, p0, Lcom/android/settings/accounts/AccountPreferenceController;->mContext:Landroid/content/Context;

    const v4, 0x7f120042

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/android/settings/cA;->bTs(Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method static synthetic amy(Lcom/android/settings/accounts/AccountPreferenceController;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/accounts/AccountPreferenceController;->amr()V

    return-void
.end method

.method static synthetic amz(Lcom/android/settings/accounts/AccountPreferenceController;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/accounts/AccountPreferenceController;->amv()V

    return-void
.end method

.method private updateUi()V
    .locals 5

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/android/settings/accounts/AccountPreferenceController;->p()Z

    move-result v0

    if-nez v0, :cond_0

    const-string/jumbo v0, "AccountPrefController"

    const-string/jumbo v1, "We should not be showing settings for a managed profile"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/settings/accounts/AccountPreferenceController;->ayh:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v3

    move v2, v1

    :goto_0
    if-ge v2, v3, :cond_1

    iget-object v0, p0, Lcom/android/settings/accounts/AccountPreferenceController;->ayh:Landroid/util/SparseArray;

    invoke-virtual {v0, v2}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settings/accounts/AccountPreferenceController$ProfileData;

    const/4 v4, 0x1

    iput-boolean v4, v0, Lcom/android/settings/accounts/AccountPreferenceController$ProfileData;->ayn:Z

    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/android/settings/accounts/AccountPreferenceController;->ayi:Landroid/os/UserManager;

    invoke-virtual {v0}, Landroid/os/UserManager;->isLinkedUser()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/android/settings/accounts/AccountPreferenceController;->ayi:Landroid/os/UserManager;

    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/os/UserManager;->getUserInfo(I)Landroid/content/pm/UserInfo;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/android/settings/accounts/AccountPreferenceController;->amx(Landroid/content/pm/UserInfo;)V

    :cond_2
    invoke-virtual {p0}, Lcom/android/settings/accounts/AccountPreferenceController;->amn()V

    iget-object v0, p0, Lcom/android/settings/accounts/AccountPreferenceController;->ayh:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v2

    :goto_1
    if-ge v1, v2, :cond_4

    iget-object v0, p0, Lcom/android/settings/accounts/AccountPreferenceController;->ayh:Landroid/util/SparseArray;

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settings/accounts/AccountPreferenceController$ProfileData;

    invoke-direct {p0, v0}, Lcom/android/settings/accounts/AccountPreferenceController;->amw(Lcom/android/settings/accounts/AccountPreferenceController$ProfileData;)V

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    :cond_3
    iget-object v0, p0, Lcom/android/settings/accounts/AccountPreferenceController;->ayi:Landroid/os/UserManager;

    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/os/UserManager;->getProfiles(I)Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v4

    move v2, v1

    :goto_2
    if-ge v2, v4, :cond_2

    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/UserInfo;

    invoke-direct {p0, v0}, Lcom/android/settings/accounts/AccountPreferenceController;->amx(Landroid/content/pm/UserInfo;)V

    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_2

    :cond_4
    return-void
.end method


# virtual methods
.method public ame(Landroid/os/UserHandle;)V
    .locals 3

    iget-object v0, p0, Lcom/android/settings/accounts/AccountPreferenceController;->ayh:Landroid/util/SparseArray;

    invoke-virtual {p1}, Landroid/os/UserHandle;->getIdentifier()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settings/accounts/AccountPreferenceController$ProfileData;

    if-eqz v0, :cond_0

    invoke-direct {p0, v0}, Lcom/android/settings/accounts/AccountPreferenceController;->amw(Lcom/android/settings/accounts/AccountPreferenceController$ProfileData;)V

    :goto_0
    return-void

    :cond_0
    const-string/jumbo v0, "AccountPrefController"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Missing Settings screen for: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Landroid/os/UserHandle;->getIdentifier()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method amn()V
    .locals 4

    iget-object v0, p0, Lcom/android/settings/accounts/AccountPreferenceController;->ayf:Lcom/android/settings/MiuiSettingsPreferenceFragment;

    invoke-virtual {v0}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v2

    if-nez v2, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/settings/accounts/AccountPreferenceController;->ayh:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_0
    if-ltz v1, :cond_2

    iget-object v0, p0, Lcom/android/settings/accounts/AccountPreferenceController;->ayh:Landroid/util/SparseArray;

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settings/accounts/AccountPreferenceController$ProfileData;

    iget-boolean v3, v0, Lcom/android/settings/accounts/AccountPreferenceController$ProfileData;->ayn:Z

    if-eqz v3, :cond_1

    iget-object v0, v0, Lcom/android/settings/accounts/AccountPreferenceController$ProfileData;->ayo:Landroid/preference/PreferenceGroup;

    invoke-virtual {v2, v0}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    iget-object v0, p0, Lcom/android/settings/accounts/AccountPreferenceController;->ayh:Landroid/util/SparseArray;

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->removeAt(I)V

    :cond_1
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0

    :cond_2
    return-void
.end method

.method public i(Landroid/preference/PreferenceScreen;)V
    .locals 0

    invoke-super {p0, p1}, Lcom/android/settings/core/e;->i(Landroid/preference/PreferenceScreen;)V

    invoke-direct {p0}, Lcom/android/settings/accounts/AccountPreferenceController;->updateUi()V

    return-void
.end method

.method public l()Ljava/lang/String;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public onPause()V
    .locals 2

    invoke-direct {p0}, Lcom/android/settings/accounts/AccountPreferenceController;->amv()V

    iget-object v0, p0, Lcom/android/settings/accounts/AccountPreferenceController;->aye:Lcom/android/settings/accounts/AccountPreferenceController$ManagedProfileBroadcastReceiver;

    iget-object v1, p0, Lcom/android/settings/accounts/AccountPreferenceController;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/android/settings/accounts/AccountPreferenceController$ManagedProfileBroadcastReceiver;->unregister(Landroid/content/Context;)V

    return-void
.end method

.method public onPreferenceClick(Landroid/preference/Preference;)Z
    .locals 9

    const/4 v5, 0x0

    const/4 v8, 0x1

    const/4 v7, 0x0

    iget-object v0, p0, Lcom/android/settings/accounts/AccountPreferenceController;->ayh:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v2

    move v1, v7

    :goto_0
    if-ge v1, v2, :cond_3

    iget-object v0, p0, Lcom/android/settings/accounts/AccountPreferenceController;->ayh:Landroid/util/SparseArray;

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settings/accounts/AccountPreferenceController$ProfileData;

    iget-object v3, v0, Lcom/android/settings/accounts/AccountPreferenceController$ProfileData;->ayk:Lcom/android/settings/DimmableIconPreference;

    if-ne p1, v3, :cond_0

    new-instance v1, Landroid/content/Intent;

    const-string/jumbo v2, "android.settings.ADD_ACCOUNT_SETTINGS"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string/jumbo v2, "android.intent.extra.USER"

    iget-object v0, v0, Lcom/android/settings/accounts/AccountPreferenceController$ProfileData;->ayq:Landroid/content/pm/UserInfo;

    invoke-virtual {v0}, Landroid/content/pm/UserInfo;->getUserHandle()Landroid/os/UserHandle;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string/jumbo v0, "authorities"

    iget-object v2, p0, Lcom/android/settings/accounts/AccountPreferenceController;->ayb:[Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    iget-object v0, p0, Lcom/android/settings/accounts/AccountPreferenceController;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    return v8

    :cond_0
    iget-object v3, v0, Lcom/android/settings/accounts/AccountPreferenceController$ProfileData;->ayp:Lcom/android/settingslib/MiuiRestrictedPreference;

    if-ne p1, v3, :cond_1

    iget-object v0, v0, Lcom/android/settings/accounts/AccountPreferenceController$ProfileData;->ayq:Landroid/content/pm/UserInfo;

    iget v0, v0, Landroid/content/pm/UserInfo;->id:I

    invoke-static {v0}, Lcom/android/settings/accounts/RemoveUserFragment;->aoL(I)Lcom/android/settings/accounts/RemoveUserFragment;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/accounts/AccountPreferenceController;->ayf:Lcom/android/settings/MiuiSettingsPreferenceFragment;

    invoke-virtual {v1}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const-string/jumbo v2, "removeUser"

    invoke-virtual {v0, v1, v2}, Lcom/android/settings/accounts/RemoveUserFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    return v8

    :cond_1
    iget-object v3, v0, Lcom/android/settings/accounts/AccountPreferenceController$ProfileData;->aym:Landroid/preference/Preference;

    if-ne p1, v3, :cond_2

    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    const-string/jumbo v1, "android.intent.extra.USER"

    iget-object v0, v0, Lcom/android/settings/accounts/AccountPreferenceController$ProfileData;->ayq:Landroid/content/pm/UserInfo;

    invoke-virtual {v0}, Landroid/content/pm/UserInfo;->getUserHandle()Landroid/os/UserHandle;

    move-result-object v0

    invoke-virtual {v3, v1, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    iget-object v0, p0, Lcom/android/settings/accounts/AccountPreferenceController;->ayf:Lcom/android/settings/MiuiSettingsPreferenceFragment;

    invoke-virtual {v0}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/android/settings/bL;

    iget-object v1, p0, Lcom/android/settings/accounts/AccountPreferenceController;->ayf:Lcom/android/settings/MiuiSettingsPreferenceFragment;

    const-class v2, Lcom/android/settings/accounts/ManagedProfileSettings;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    const v4, 0x7f120a1d

    move-object v6, v5

    invoke-virtual/range {v0 .. v7}, Lcom/android/settings/bL;->Uv(Landroid/app/Fragment;Ljava/lang/String;Landroid/os/Bundle;ILjava/lang/CharSequence;Landroid/app/Fragment;I)V

    return v8

    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_3
    return v7
.end method

.method public onResume()V
    .locals 2

    invoke-direct {p0}, Lcom/android/settings/accounts/AccountPreferenceController;->updateUi()V

    iget-object v0, p0, Lcom/android/settings/accounts/AccountPreferenceController;->aye:Lcom/android/settings/accounts/AccountPreferenceController$ManagedProfileBroadcastReceiver;

    iget-object v1, p0, Lcom/android/settings/accounts/AccountPreferenceController;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/android/settings/accounts/AccountPreferenceController$ManagedProfileBroadcastReceiver;->register(Landroid/content/Context;)V

    invoke-direct {p0}, Lcom/android/settings/accounts/AccountPreferenceController;->amr()V

    return-void
.end method

.method public p()Z
    .locals 1

    iget-object v0, p0, Lcom/android/settings/accounts/AccountPreferenceController;->ayi:Landroid/os/UserManager;

    invoke-virtual {v0}, Landroid/os/UserManager;->isManagedProfile()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    return v0
.end method
