.class public Lcom/android/settings/accounts/AddUserWhenLockedPreferenceController;
.super Lcom/android/settings/core/e;
.source "AddUserWhenLockedPreferenceController.java"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceChangeListener;
.implements Lcom/android/settings/core/lifecycle/b;
.implements Lcom/android/settings/core/lifecycle/a/d;
.implements Lcom/android/settings/core/lifecycle/a/b;


# instance fields
.field private aAW:Z

.field private aAX:Lcom/android/settings/users/UserCapabilities;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    invoke-direct {p0, p1}, Lcom/android/settings/core/e;-><init>(Landroid/content/Context;)V

    invoke-static {p1}, Lcom/android/settings/users/UserCapabilities;->aRa(Landroid/content/Context;)Lcom/android/settings/users/UserCapabilities;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/accounts/AddUserWhenLockedPreferenceController;->aAX:Lcom/android/settings/users/UserCapabilities;

    return-void
.end method


# virtual methods
.method public cz(Landroid/preference/Preference;)V
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    check-cast p1, Lcom/android/settingslib/MiuiRestrictedSwitchPreference;

    iget-object v2, p0, Lcom/android/settings/accounts/AddUserWhenLockedPreferenceController;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string/jumbo v3, "add_users_when_locked"

    invoke-static {v2, v3, v1}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    if-ne v2, v0, :cond_0

    :goto_0
    invoke-virtual {p1, v0}, Lcom/android/settingslib/MiuiRestrictedSwitchPreference;->setChecked(Z)V

    iget-object v0, p0, Lcom/android/settings/accounts/AddUserWhenLockedPreferenceController;->aAX:Lcom/android/settings/users/UserCapabilities;

    invoke-virtual {v0}, Lcom/android/settings/users/UserCapabilities;->aRc()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/settings/accounts/AddUserWhenLockedPreferenceController;->aAX:Lcom/android/settings/users/UserCapabilities;

    invoke-virtual {v0}, Lcom/android/settings/users/UserCapabilities;->aRe()Lcom/android/settingslib/n;

    move-result-object v0

    :goto_1
    invoke-virtual {p1, v0}, Lcom/android/settingslib/MiuiRestrictedSwitchPreference;->setDisabledByAdmin(Lcom/android/settingslib/n;)V

    return-void

    :cond_0
    move v0, v1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public l()Ljava/lang/String;
    .locals 1

    const-string/jumbo v0, "add_users_when_locked"

    return-object v0
.end method

.method public onPause()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/settings/accounts/AddUserWhenLockedPreferenceController;->aAW:Z

    return-void
.end method

.method public onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 4

    const/4 v1, 0x1

    check-cast p2, Ljava/lang/Boolean;

    iget-object v0, p0, Lcom/android/settings/accounts/AddUserWhenLockedPreferenceController;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string/jumbo v3, "add_users_when_locked"

    if-eqz p2, :cond_0

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v2, v3, v0}, Landroid/provider/Settings$Global;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    return v1

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onResume()V
    .locals 2

    iget-boolean v0, p0, Lcom/android/settings/accounts/AddUserWhenLockedPreferenceController;->aAW:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/accounts/AddUserWhenLockedPreferenceController;->aAX:Lcom/android/settings/users/UserCapabilities;

    iget-object v1, p0, Lcom/android/settings/accounts/AddUserWhenLockedPreferenceController;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/android/settings/users/UserCapabilities;->aRb(Landroid/content/Context;)V

    :cond_0
    return-void
.end method

.method public p()Z
    .locals 1

    iget-object v0, p0, Lcom/android/settings/accounts/AddUserWhenLockedPreferenceController;->aAX:Lcom/android/settings/users/UserCapabilities;

    invoke-virtual {v0}, Lcom/android/settings/users/UserCapabilities;->aRf()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/settings/accounts/AddUserWhenLockedPreferenceController;->aAX:Lcom/android/settings/users/UserCapabilities;

    invoke-virtual {v0}, Lcom/android/settings/users/UserCapabilities;->aRc()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/accounts/AddUserWhenLockedPreferenceController;->aAX:Lcom/android/settings/users/UserCapabilities;

    invoke-virtual {v0}, Lcom/android/settings/users/UserCapabilities;->aRd()Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
