.class public Lcom/android/settings/accounts/MiuiManageAccountsSettings;
.super Lcom/android/settings/accounts/MiuiAccountPreferenceBase;
.source "MiuiManageAccountsSettings.java"

# interfaces
.implements Lcom/android/settings/d;


# static fields
.field private static final axK:Ljava/util/Comparator;


# instance fields
.field protected axL:Landroid/app/AlertDialog;

.field private axM:Landroid/app/Activity;

.field private axN:[Ljava/lang/String;

.field private axO:Lmiui/preference/ValuePreference;

.field private axP:Lcom/android/settings/MiuiSettingsPreferenceFragment$SettingsDialogFragment;

.field private axQ:Landroid/widget/TextView;

.field private axR:Landroid/preference/PreferenceCategory;

.field private axS:Landroid/preference/PreferenceCategory;

.field private axT:Lcom/android/settings/ab;

.field private axU:Landroid/preference/CheckBoxPreference;

.field private axV:Landroid/preference/CheckBoxPreference;

.field private axW:Z

.field private axX:Landroid/preference/PreferenceCategory;


# direct methods
.method static synthetic -get0()Ljava/util/Comparator;
    .locals 1

    sget-object v0, Lcom/android/settings/accounts/MiuiManageAccountsSettings;->axK:Ljava/util/Comparator;

    return-object v0
.end method

.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/android/settings/accounts/MiuiManageAccountsSettings$1;

    invoke-direct {v0}, Lcom/android/settings/accounts/MiuiManageAccountsSettings$1;-><init>()V

    sput-object v0, Lcom/android/settings/accounts/MiuiManageAccountsSettings;->axK:Ljava/util/Comparator;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/accounts/MiuiAccountPreferenceBase;-><init>()V

    return-void
.end method

.method private alO(Z)V
    .locals 1

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/android/settings/accounts/MiuiManageAccountsSettings;->axL:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/android/settings/accounts/MiuiManageAccountsSettings;->amc(Z)V

    goto :goto_0
.end method

.method public static alP(Ljava/lang/String;)Z
    .locals 6

    const/4 v1, 0x0

    invoke-static {}, Landroid/content/ContentResolver;->getSyncAdapterTypes()[Landroid/content/SyncAdapterType;

    move-result-object v2

    array-length v3, v2

    move v0, v1

    :goto_0
    if-ge v0, v3, :cond_1

    aget-object v4, v2, v0

    invoke-virtual {v4}, Landroid/content/SyncAdapterType;->isUserVisible()Z

    move-result v5

    if-eqz v5, :cond_0

    iget-object v4, v4, Landroid/content/SyncAdapterType;->accountType:Ljava/lang/String;

    invoke-virtual {p0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    const/4 v0, 0x1

    return v0

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    return v1
.end method

.method private alQ()V
    .locals 2

    new-instance v0, Lcom/android/settings/accounts/MiuiManageAccountsSettings$3;

    invoke-direct {v0, p0}, Lcom/android/settings/accounts/MiuiManageAccountsSettings$3;-><init>(Lcom/android/settings/accounts/MiuiManageAccountsSettings;)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/android/settings/accounts/MiuiManageAccountsSettings$3;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    return-void
.end method

.method private alR()V
    .locals 2

    const/4 v1, 0x0

    iget-boolean v0, p0, Lcom/android/settings/accounts/MiuiManageAccountsSettings;->axW:Z

    if-eqz v0, :cond_0

    invoke-static {v1, v1}, Landroid/content/ContentResolver;->cancelSync(Landroid/accounts/Account;Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method private alS()V
    .locals 4

    const/4 v3, 0x0

    iget-boolean v0, p0, Lcom/android/settings/accounts/MiuiManageAccountsSettings;->axW:Z

    if-nez v0, :cond_1

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    invoke-static {}, Landroid/content/ContentResolver;->getMasterSyncAutomatically()Z

    move-result v1

    if-nez v1, :cond_0

    const-string/jumbo v1, "force"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    :cond_0
    invoke-static {v3, v3, v0}, Landroid/content/ContentResolver;->requestSync(Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;)V

    :cond_1
    return-void
.end method

.method private alU(Landroid/view/MenuItem;)V
    .locals 1

    iget-object v0, p0, Lcom/android/settings/accounts/MiuiManageAccountsSettings;->axT:Lcom/android/settings/ab;

    invoke-virtual {v0}, Lcom/android/settings/ab;->bmR()V

    return-void
.end method

.method private alV(Z)V
    .locals 3

    iget-object v0, p0, Lcom/android/settings/accounts/MiuiManageAccountsSettings;->axM:Landroid/app/Activity;

    invoke-static {v0}, Lmiui/accounts/ExtraAccountManager;->getXiaomiAccount(Landroid/content/Context;)Landroid/accounts/Account;

    move-result-object v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    const-string/jumbo v1, "sms"

    invoke-static {v0, v1}, Landroid/content/ContentResolver;->getSyncAutomatically(Landroid/accounts/Account;Ljava/lang/String;)Z

    move-result v0

    new-instance v1, Landroid/content/Intent;

    const-string/jumbo v2, "com.miui.cloudservice.mms.UPLOAD_PHONE_LIST"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string/jumbo v2, "extra_upload_opt"

    if-eqz v0, :cond_1

    if-eqz p1, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string/jumbo v0, "com.miui.cloudservice"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    iget-object v0, p0, Lcom/android/settings/accounts/MiuiManageAccountsSettings;->axM:Landroid/app/Activity;

    invoke-virtual {v0, v1}, Landroid/app/Activity;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    return-void

    :cond_1
    const/4 v0, 0x2

    goto :goto_0
.end method

.method private alX(Landroid/view/MenuItem;)V
    .locals 1

    iget-object v0, p0, Lcom/android/settings/accounts/MiuiManageAccountsSettings;->axT:Lcom/android/settings/ab;

    invoke-virtual {v0}, Lcom/android/settings/ab;->bmS()V

    return-void
.end method

.method private alY()Z
    .locals 12

    const/4 v1, 0x0

    const-string/jumbo v0, "account"

    invoke-virtual {p0, v0}, Lcom/android/settings/accounts/MiuiManageAccountsSettings;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/accounts/AccountManager;

    invoke-virtual {v0}, Landroid/accounts/AccountManager;->getAccounts()[Landroid/accounts/Account;

    move-result-object v3

    invoke-static {}, Landroid/content/ContentResolver;->getSyncAdapterTypes()[Landroid/content/SyncAdapterType;

    move-result-object v4

    invoke-static {}, Landroid/content/ContentResolver;->getMasterSyncAutomatically()Z

    move-result v5

    array-length v6, v3

    move v2, v1

    :goto_0
    if-ge v2, v6, :cond_3

    aget-object v7, v3, v2

    array-length v8, v4

    move v0, v1

    :goto_1
    if-ge v0, v8, :cond_2

    aget-object v9, v4, v0

    iget-object v10, v9, Landroid/content/SyncAdapterType;->accountType:Ljava/lang/String;

    iget-object v11, v7, Landroid/accounts/Account;->type:Ljava/lang/String;

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_1

    if-eqz v5, :cond_0

    iget-object v9, v9, Landroid/content/SyncAdapterType;->authority:Ljava/lang/String;

    invoke-static {v7, v9}, Landroid/content/ContentResolver;->getSyncAutomatically(Landroid/accounts/Account;Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_1

    :cond_0
    const/4 v0, 0x1

    return v0

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_3
    return v1
.end method

.method private alZ(Ljava/lang/String;)V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/accounts/MiuiManageAccountsSettings;->axM:Landroid/app/Activity;

    if-nez v0, :cond_0

    return-void

    :cond_0
    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v1, "miui.intent.action.TRACK_EVENT"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string/jumbo v1, "eventId"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    iget-object v1, p0, Lcom/android/settings/accounts/MiuiManageAccountsSettings;->axM:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    iget-object v1, p0, Lcom/android/settings/accounts/MiuiManageAccountsSettings;->axM:Landroid/app/Activity;

    invoke-virtual {v1, v0}, Landroid/app/Activity;->sendBroadcast(Landroid/content/Intent;)V

    return-void
.end method

.method private ama()V
    .locals 1

    const-string/jumbo v0, "account_settings_opened"

    invoke-direct {p0, v0}, Lcom/android/settings/accounts/MiuiManageAccountsSettings;->alZ(Ljava/lang/String;)V

    return-void
.end method

.method private amb(Z)V
    .locals 1

    if-eqz p1, :cond_0

    const-string/jumbo v0, "account_settings_wifi_only_enabled"

    :goto_0
    invoke-direct {p0, v0}, Lcom/android/settings/accounts/MiuiManageAccountsSettings;->alZ(Ljava/lang/String;)V

    return-void

    :cond_0
    const-string/jumbo v0, "account_settings_wifi_only_disabled"

    goto :goto_0
.end method

.method private amc(Z)V
    .locals 13

    const/4 v1, 0x0

    const-string/jumbo v0, "account"

    invoke-virtual {p0, v0}, Lcom/android/settings/accounts/MiuiManageAccountsSettings;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/accounts/AccountManager;

    invoke-virtual {v0}, Landroid/accounts/AccountManager;->getAccounts()[Landroid/accounts/Account;

    move-result-object v3

    invoke-static {}, Landroid/content/ContentResolver;->getSyncAdapterTypes()[Landroid/content/SyncAdapterType;

    move-result-object v4

    new-instance v5, Landroid/os/Bundle;

    invoke-direct {v5}, Landroid/os/Bundle;-><init>()V

    invoke-static {}, Lcom/google/android/collect/Sets;->newHashSet()Ljava/util/HashSet;

    move-result-object v6

    array-length v7, v3

    move v2, v1

    :goto_0
    if-ge v2, v7, :cond_4

    aget-object v8, v3, v2

    invoke-interface {v6}, Ljava/util/Set;->clear()V

    array-length v9, v4

    move v0, v1

    :goto_1
    if-ge v0, v9, :cond_1

    aget-object v10, v4, v0

    iget-object v11, v10, Landroid/content/SyncAdapterType;->accountType:Ljava/lang/String;

    iget-object v12, v8, Landroid/accounts/Account;->type:Ljava/lang/String;

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_0

    iget-object v11, v10, Landroid/content/SyncAdapterType;->authority:Ljava/lang/String;

    invoke-static {v8, v11}, Landroid/content/ContentResolver;->getSyncAutomatically(Landroid/accounts/Account;Ljava/lang/String;)Z

    move-result v11

    if-eqz v11, :cond_0

    invoke-interface {v6, v10}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_1
    invoke-interface {v6}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :goto_2
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/SyncAdapterType;

    if-eqz p1, :cond_2

    iget-object v0, v0, Landroid/content/SyncAdapterType;->authority:Ljava/lang/String;

    invoke-static {v8, v0, v5}, Landroid/content/ContentResolver;->requestSync(Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;)V

    goto :goto_2

    :cond_2
    iget-object v0, v0, Landroid/content/SyncAdapterType;->authority:Ljava/lang/String;

    invoke-static {v8, v0}, Landroid/content/ContentResolver;->cancelSync(Landroid/accounts/Account;Ljava/lang/String;)V

    goto :goto_2

    :cond_3
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_4
    return-void
.end method

.method static synthetic amh(Lcom/android/settings/accounts/MiuiManageAccountsSettings;)Lmiui/preference/ValuePreference;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/accounts/MiuiManageAccountsSettings;->axO:Lmiui/preference/ValuePreference;

    return-object v0
.end method

.method static synthetic ami(Lcom/android/settings/accounts/MiuiManageAccountsSettings;)Landroid/preference/PreferenceCategory;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/accounts/MiuiManageAccountsSettings;->axR:Landroid/preference/PreferenceCategory;

    return-object v0
.end method

.method static synthetic amj(Lcom/android/settings/accounts/MiuiManageAccountsSettings;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/settings/accounts/MiuiManageAccountsSettings;->amc(Z)V

    return-void
.end method


# virtual methods
.method protected Vg(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2

    const v0, 0x7f0d00e5

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method protected alT()V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/accounts/MiuiManageAccountsSettings;->axM:Landroid/app/Activity;

    if-nez v0, :cond_0

    return-void

    :cond_0
    invoke-static {}, Landroid/content/ContentResolver;->getMasterSyncAutomatically()Z

    move-result v0

    iget-object v1, p0, Lcom/android/settings/accounts/MiuiManageAccountsSettings;->axU:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v1, v0}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    invoke-static {}, Landroid/content/ContentResolver;->getCurrentSyncs()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    iget-boolean v1, p0, Lcom/android/settings/accounts/MiuiManageAccountsSettings;->axW:Z

    if-eq v1, v0, :cond_1

    iput-boolean v0, p0, Lcom/android/settings/accounts/MiuiManageAccountsSettings;->axW:Z

    :cond_1
    invoke-virtual {p0}, Lcom/android/settings/accounts/MiuiManageAccountsSettings;->invalidateOptionsMenu()V

    return-void
.end method

.method public alW(I)V
    .locals 3

    iget-object v0, p0, Lcom/android/settings/accounts/MiuiManageAccountsSettings;->axP:Lcom/android/settings/MiuiSettingsPreferenceFragment$SettingsDialogFragment;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/accounts/MiuiManageAccountsSettings;->axM:Landroid/app/Activity;

    if-eqz v0, :cond_0

    const-string/jumbo v0, "MiuiManageAccountsSettings"

    const-string/jumbo v1, "Old dialog fragment not null!"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    new-instance v0, Lcom/android/settings/MiuiSettingsPreferenceFragment$SettingsDialogFragment;

    invoke-direct {v0, p0, p1}, Lcom/android/settings/MiuiSettingsPreferenceFragment$SettingsDialogFragment;-><init>(Lcom/android/settings/d;I)V

    iput-object v0, p0, Lcom/android/settings/accounts/MiuiManageAccountsSettings;->axP:Lcom/android/settings/MiuiSettingsPreferenceFragment$SettingsDialogFragment;

    iget-object v0, p0, Lcom/android/settings/accounts/MiuiManageAccountsSettings;->axP:Lcom/android/settings/MiuiSettingsPreferenceFragment$SettingsDialogFragment;

    iget-object v1, p0, Lcom/android/settings/accounts/MiuiManageAccountsSettings;->axM:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/android/settings/MiuiSettingsPreferenceFragment$SettingsDialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    return-void
.end method

.method public bridge synthetic amd()V
    .locals 0

    invoke-super {p0}, Lcom/android/settings/accounts/MiuiAccountPreferenceBase;->amd()V

    return-void
.end method

.method public bridge synthetic ame(Landroid/os/UserHandle;)V
    .locals 0

    invoke-super {p0, p1}, Lcom/android/settings/accounts/MiuiAccountPreferenceBase;->ame(Landroid/os/UserHandle;)V

    return-void
.end method

.method public bridge synthetic amf(Ljava/lang/String;)Ljava/util/ArrayList;
    .locals 1

    invoke-super {p0, p1}, Lcom/android/settings/accounts/MiuiAccountPreferenceBase;->amf(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic amg(Ljava/lang/String;Landroid/preference/PreferenceScreen;)Landroid/preference/PreferenceScreen;
    .locals 1

    invoke-super {p0, p1, p2}, Lcom/android/settings/accounts/MiuiAccountPreferenceBase;->amg(Ljava/lang/String;Landroid/preference/PreferenceScreen;)Landroid/preference/PreferenceScreen;

    move-result-object v0

    return-object v0
.end method

.method public cC()Ljava/lang/String;
    .locals 1

    const-class v0, Lcom/android/settings/accounts/MiuiManageAccountsSettings;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 5

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-super {p0, p1}, Lcom/android/settings/accounts/MiuiAccountPreferenceBase;->onActivityCreated(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/android/settings/accounts/MiuiManageAccountsSettings;->getView()Landroid/view/View;

    move-result-object v0

    const v3, 0x7f0a0474

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/settings/accounts/MiuiManageAccountsSettings;->axQ:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/android/settings/accounts/MiuiManageAccountsSettings;->axQ:Landroid/widget/TextView;

    const/16 v3, 0x8

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/settings/accounts/MiuiManageAccountsSettings;->axU:Landroid/preference/CheckBoxPreference;

    invoke-static {}, Landroid/content/ContentResolver;->getMasterSyncAutomatically()Z

    move-result v3

    invoke-virtual {v0, v3}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    iget-object v0, p0, Lcom/android/settings/accounts/MiuiManageAccountsSettings;->axV:Landroid/preference/CheckBoxPreference;

    if-eqz v0, :cond_0

    iget-object v3, p0, Lcom/android/settings/accounts/MiuiManageAccountsSettings;->axV:Landroid/preference/CheckBoxPreference;

    invoke-virtual {p0}, Lcom/android/settings/accounts/MiuiManageAccountsSettings;->bWB()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v4, "sync_on_wifi_only"

    invoke-static {v0, v4, v2}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    if-ne v0, v1, :cond_1

    move v0, v1

    :goto_0
    invoke-virtual {v3, v0}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    :cond_0
    iget-object v0, p0, Lcom/android/settings/accounts/MiuiManageAccountsSettings;->axM:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "authorities"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringArrayExtra(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/accounts/MiuiManageAccountsSettings;->axN:[Ljava/lang/String;

    return-void

    :cond_1
    move v0, v2

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 6

    const/4 v2, 0x1

    const/4 v5, 0x0

    const/4 v4, 0x0

    invoke-virtual {p0}, Lcom/android/settings/accounts/MiuiManageAccountsSettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/accounts/MiuiManageAccountsSettings;->axM:Landroid/app/Activity;

    new-instance v0, Lcom/android/settings/accounts/MiuiManageAccountsSettings$SyncDrawable;

    iget-object v1, p0, Lcom/android/settings/accounts/MiuiManageAccountsSettings;->axM:Landroid/app/Activity;

    invoke-direct {v0, v1}, Lcom/android/settings/accounts/MiuiManageAccountsSettings$SyncDrawable;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/settings/accounts/MiuiManageAccountsSettings;->axT:Lcom/android/settings/ab;

    invoke-super {p0, p1}, Lcom/android/settings/accounts/MiuiAccountPreferenceBase;->onCreate(Landroid/os/Bundle;)V

    const v0, 0x7f15007d

    invoke-virtual {p0, v0}, Lcom/android/settings/accounts/MiuiManageAccountsSettings;->addPreferencesFromResource(I)V

    invoke-virtual {p0, v2}, Lcom/android/settings/accounts/MiuiManageAccountsSettings;->setHasOptionsMenu(Z)V

    const-string/jumbo v0, "sync_enable"

    invoke-virtual {p0, v0}, Lcom/android/settings/accounts/MiuiManageAccountsSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    iput-object v0, p0, Lcom/android/settings/accounts/MiuiManageAccountsSettings;->axU:Landroid/preference/CheckBoxPreference;

    const-string/jumbo v0, "wifi_only"

    invoke-virtual {p0, v0}, Lcom/android/settings/accounts/MiuiManageAccountsSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    iput-object v0, p0, Lcom/android/settings/accounts/MiuiManageAccountsSettings;->axV:Landroid/preference/CheckBoxPreference;

    const-string/jumbo v0, "xiaomi_cloud_service"

    invoke-virtual {p0, v0}, Lcom/android/settings/accounts/MiuiManageAccountsSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Lmiui/preference/ValuePreference;

    iput-object v0, p0, Lcom/android/settings/accounts/MiuiManageAccountsSettings;->axO:Lmiui/preference/ValuePreference;

    iget-object v0, p0, Lcom/android/settings/accounts/MiuiManageAccountsSettings;->axM:Landroid/app/Activity;

    invoke-static {v0}, Lcom/android/settings/aq;->bqv(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/android/settings/accounts/MiuiManageAccountsSettings;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/accounts/MiuiManageAccountsSettings;->axV:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    iput-object v5, p0, Lcom/android/settings/accounts/MiuiManageAccountsSettings;->axV:Landroid/preference/CheckBoxPreference;

    :cond_0
    iget-object v0, p0, Lcom/android/settings/accounts/MiuiManageAccountsSettings;->axO:Lmiui/preference/ValuePreference;

    invoke-virtual {v0, v2}, Lmiui/preference/ValuePreference;->setShowRightArrow(Z)V

    const-string/jumbo v0, "account_other"

    invoke-virtual {p0, v0}, Lcom/android/settings/accounts/MiuiManageAccountsSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceCategory;

    iput-object v0, p0, Lcom/android/settings/accounts/MiuiManageAccountsSettings;->axR:Landroid/preference/PreferenceCategory;

    invoke-static {}, Landroid/app/AppGlobals;->getPackageManager()Landroid/content/pm/IPackageManager;

    move-result-object v0

    :try_start_0
    const-string/jumbo v1, "com.miui.cloudservice"

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-interface {v0, v1, v2, v3}, Landroid/content/pm/IPackageManager;->getApplicationInfo(Ljava/lang/String;II)Landroid/content/pm/ApplicationInfo;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/android/settings/accounts/MiuiManageAccountsSettings;->axO:Lmiui/preference/ValuePreference;

    invoke-virtual {p0}, Lcom/android/settings/accounts/MiuiManageAccountsSettings;->bWA()Landroid/content/pm/PackageManager;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/content/pm/ApplicationInfo;->loadIcon(Landroid/content/pm/PackageManager;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v1, v0}, Lmiui/preference/ValuePreference;->setIcon(Landroid/graphics/drawable/Drawable;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/android/settings/accounts/MiuiManageAccountsSettings;->axO:Lmiui/preference/ValuePreference;

    const v1, 0x7f0d014e

    invoke-virtual {v0, v1}, Lmiui/preference/ValuePreference;->setLayoutResource(I)V

    invoke-direct {p0}, Lcom/android/settings/accounts/MiuiManageAccountsSettings;->ama()V

    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Lcom/android/settings/accounts/MiuiManageAccountsSettings;->axM:Landroid/app/Activity;

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v1, 0x7f12120b

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    new-instance v1, Lcom/android/settings/accounts/MiuiManageAccountsSettings$2;

    invoke-direct {v1, p0}, Lcom/android/settings/accounts/MiuiManageAccountsSettings$2;-><init>(Lcom/android/settings/accounts/MiuiManageAccountsSettings;)V

    const v2, 0x7f121223

    invoke-virtual {v0, v2, v1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f1205f1

    invoke-virtual {v0, v1, v5}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/accounts/MiuiManageAccountsSettings;->axL:Landroid/app/AlertDialog;

    iget-object v0, p0, Lcom/android/settings/accounts/MiuiManageAccountsSettings;->axL:Landroid/app/AlertDialog;

    invoke-virtual {v0, v4}, Landroid/app/AlertDialog;->setCanceledOnTouchOutside(Z)V

    iget-object v0, p0, Lcom/android/settings/accounts/MiuiManageAccountsSettings;->aAu:Landroid/os/UserHandle;

    invoke-virtual {v0}, Landroid/os/UserHandle;->getIdentifier()I

    move-result v0

    const/16 v1, 0x3e7

    if-ne v0, v1, :cond_2

    const-string/jumbo v0, "account_sync"

    invoke-virtual {p0, v0}, Lcom/android/settings/accounts/MiuiManageAccountsSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceCategory;

    iput-object v0, p0, Lcom/android/settings/accounts/MiuiManageAccountsSettings;->axS:Landroid/preference/PreferenceCategory;

    const-string/jumbo v0, "account_xiaomi"

    invoke-virtual {p0, v0}, Lcom/android/settings/accounts/MiuiManageAccountsSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceCategory;

    iput-object v0, p0, Lcom/android/settings/accounts/MiuiManageAccountsSettings;->axX:Landroid/preference/PreferenceCategory;

    invoke-virtual {p0}, Lcom/android/settings/accounts/MiuiManageAccountsSettings;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/accounts/MiuiManageAccountsSettings;->axS:Landroid/preference/PreferenceCategory;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    invoke-virtual {p0}, Lcom/android/settings/accounts/MiuiManageAccountsSettings;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/accounts/MiuiManageAccountsSettings;->axX:Landroid/preference/PreferenceCategory;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    :cond_2
    return-void

    :catch_0
    move-exception v0

    const-string/jumbo v1, "MiuiManageAccountsSettings"

    const-string/jumbo v2, "RemoteException\uff1a"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V
    .locals 4

    const/4 v3, 0x1

    const/4 v2, 0x0

    const v0, 0x7f12007e

    invoke-interface {p1, v2, v3, v2, v0}, Landroid/view/Menu;->add(IIII)Landroid/view/MenuItem;

    move-result-object v0

    sget v1, Lmiui/R$drawable;->action_button_new_light:I

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v3}, Landroid/view/MenuItem;->setShowAsAction(I)V

    const/4 v0, 0x2

    const v1, 0x7f121222

    invoke-interface {p1, v2, v0, v2, v1}, Landroid/view/Menu;->add(IIII)Landroid/view/MenuItem;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/accounts/MiuiManageAccountsSettings;->axT:Lcom/android/settings/ab;

    invoke-virtual {v1}, Lcom/android/settings/ab;->bmQ()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/MenuItem;

    move-result-object v0

    const/4 v1, 0x5

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setShowAsAction(I)V

    return-void
.end method

.method public onDestroy()V
    .locals 1

    invoke-super {p0}, Lcom/android/settings/accounts/MiuiAccountPreferenceBase;->onDestroy()V

    iget-object v0, p0, Lcom/android/settings/accounts/MiuiManageAccountsSettings;->axL:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    return-void
.end method

.method public onDetach()V
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/settings/accounts/MiuiManageAccountsSettings;->axM:Landroid/app/Activity;

    invoke-super {p0}, Lcom/android/settings/accounts/MiuiAccountPreferenceBase;->onDetach()V

    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 7

    const/4 v6, 0x1

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    invoke-super {p0, p1}, Lcom/android/settings/accounts/MiuiAccountPreferenceBase;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    return v0

    :pswitch_0
    const-class v0, Lcom/android/settings/accounts/ChooseAccountFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    const/4 v3, -0x1

    const/4 v4, 0x0

    const v5, 0x7f12007e

    move-object v0, p0

    move-object v1, p0

    invoke-virtual/range {v0 .. v5}, Lcom/android/settings/accounts/MiuiManageAccountsSettings;->bWC(Landroid/app/Fragment;Ljava/lang/String;ILandroid/os/Bundle;I)Z

    return v6

    :pswitch_1
    iget-boolean v0, p0, Lcom/android/settings/accounts/MiuiManageAccountsSettings;->axW:Z

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/android/settings/accounts/MiuiManageAccountsSettings;->alR()V

    :goto_0
    return v6

    :cond_0
    invoke-direct {p0}, Lcom/android/settings/accounts/MiuiManageAccountsSettings;->alS()V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onPause()V
    .locals 0

    invoke-super {p0}, Lcom/android/settings/accounts/MiuiAccountPreferenceBase;->onPause()V

    invoke-static {}, Lcom/android/settings/E;->blE()V

    return-void
.end method

.method public onPreferenceTreeClick(Landroid/preference/PreferenceScreen;Landroid/preference/Preference;)Z
    .locals 5

    const/4 v1, 0x0

    const-string/jumbo v0, "sync_enable"

    invoke-virtual {p2}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    check-cast p2, Landroid/preference/CheckBoxPreference;

    invoke-virtual {p2}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v0

    invoke-static {v0}, Landroid/content/ContentResolver;->setMasterSyncAutomatically(Z)V

    invoke-direct {p0, v0}, Lcom/android/settings/accounts/MiuiManageAccountsSettings;->alO(Z)V

    invoke-virtual {p0}, Lcom/android/settings/accounts/MiuiManageAccountsSettings;->alT()V

    invoke-direct {p0, v0}, Lcom/android/settings/accounts/MiuiManageAccountsSettings;->alV(Z)V

    return v1

    :cond_0
    const-string/jumbo v0, "wifi_only"

    invoke-virtual {p2}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    check-cast p2, Landroid/preference/CheckBoxPreference;

    invoke-virtual {p2}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v2

    invoke-virtual {p0}, Lcom/android/settings/accounts/MiuiManageAccountsSettings;->bWB()Landroid/content/ContentResolver;

    move-result-object v3

    const-string/jumbo v4, "sync_on_wifi_only"

    if-eqz v2, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-static {v3, v4, v0}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    invoke-direct {p0, v2}, Lcom/android/settings/accounts/MiuiManageAccountsSettings;->amb(Z)V

    return v1

    :cond_1
    move v0, v1

    goto :goto_0

    :cond_2
    const-string/jumbo v0, "xiaomi_cloud_service"

    invoke-virtual {p2}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    const-string/jumbo v0, "account_list"

    const-string/jumbo v1, "account_list_to_cloud_service"

    invoke-static {v0, v1}, Lcom/android/settings/E;->blF(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v1, "com.xiaomi.action.MICLOUD_MAIN"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string/jumbo v1, "com.miui.cloudservice"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p2, v0}, Landroid/preference/Preference;->setIntent(Landroid/content/Intent;)V

    :cond_3
    invoke-super {p0, p1, p2}, Lcom/android/settings/accounts/MiuiAccountPreferenceBase;->onPreferenceTreeClick(Landroid/preference/PreferenceScreen;Landroid/preference/Preference;)Z

    move-result v0

    return v0
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)V
    .locals 3

    const v2, 0x7f121222

    const/4 v0, 0x2

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/android/settings/accounts/MiuiManageAccountsSettings;->alY()Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    iget-boolean v1, p0, Lcom/android/settings/accounts/MiuiManageAccountsSettings;->axW:Z

    if-eqz v1, :cond_1

    const v1, 0x7f12121e

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setTitle(I)Landroid/view/MenuItem;

    invoke-direct {p0, v0}, Lcom/android/settings/accounts/MiuiManageAccountsSettings;->alU(Landroid/view/MenuItem;)V

    :cond_0
    :goto_0
    invoke-super {p0, p1}, Lcom/android/settings/accounts/MiuiAccountPreferenceBase;->onPrepareOptionsMenu(Landroid/view/Menu;)V

    return-void

    :cond_1
    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setTitle(I)Landroid/view/MenuItem;

    invoke-direct {p0, v0}, Lcom/android/settings/accounts/MiuiManageAccountsSettings;->alX(Landroid/view/MenuItem;)V

    goto :goto_0

    :cond_2
    const/4 v1, 0x0

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setTitle(I)Landroid/view/MenuItem;

    goto :goto_0
.end method

.method public onResume()V
    .locals 2

    invoke-super {p0}, Lcom/android/settings/accounts/MiuiAccountPreferenceBase;->onResume()V

    invoke-direct {p0}, Lcom/android/settings/accounts/MiuiManageAccountsSettings;->alQ()V

    iget-object v0, p0, Lcom/android/settings/accounts/MiuiManageAccountsSettings;->axM:Landroid/app/Activity;

    const-string/jumbo v1, "account_list"

    invoke-static {v0, v1}, Lcom/android/settings/E;->blD(Landroid/app/Activity;Ljava/lang/String;)V

    return-void
.end method
