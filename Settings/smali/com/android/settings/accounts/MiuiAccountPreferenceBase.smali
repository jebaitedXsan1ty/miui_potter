.class abstract Lcom/android/settings/accounts/MiuiAccountPreferenceBase;
.super Lcom/android/settings/MiuiSettingsPreferenceFragment;
.source "MiuiAccountPreferenceBase.java"

# interfaces
.implements Lcom/android/settingslib/c/b;


# static fields
.field protected static final aAw:Z


# instance fields
.field private aAA:Ljava/lang/Object;

.field private aAB:Landroid/content/SyncStatusObserver;

.field private aAC:Ljava/text/DateFormat;

.field private aAD:Landroid/os/UserManager;

.field protected aAu:Landroid/os/UserHandle;

.field protected aAv:Lcom/android/settingslib/c/a;

.field protected aAx:Lcom/android/settings/accounts/AccountTypePreferenceLoader;

.field private aAy:Ljava/text/DateFormat;

.field private final aAz:Landroid/os/Handler;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const-string/jumbo v0, "AccountSettings"

    const/4 v1, 0x2

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    sput-boolean v0, Lcom/android/settings/accounts/MiuiAccountPreferenceBase;->aAw:Z

    return-void
.end method

.method constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/android/settings/MiuiSettingsPreferenceFragment;-><init>()V

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/android/settings/accounts/MiuiAccountPreferenceBase;->aAz:Landroid/os/Handler;

    new-instance v0, Lcom/android/settings/accounts/MiuiAccountPreferenceBase$1;

    invoke-direct {v0, p0}, Lcom/android/settings/accounts/MiuiAccountPreferenceBase$1;-><init>(Lcom/android/settings/accounts/MiuiAccountPreferenceBase;)V

    iput-object v0, p0, Lcom/android/settings/accounts/MiuiAccountPreferenceBase;->aAB:Landroid/content/SyncStatusObserver;

    return-void
.end method

.method static synthetic aoP(Lcom/android/settings/accounts/MiuiAccountPreferenceBase;)Landroid/os/Handler;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/accounts/MiuiAccountPreferenceBase;->aAz:Landroid/os/Handler;

    return-object v0
.end method


# virtual methods
.method protected alT()V
    .locals 0

    return-void
.end method

.method protected amG()V
    .locals 0

    return-void
.end method

.method public amd()V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/accounts/MiuiAccountPreferenceBase;->aAv:Lcom/android/settingslib/c/a;

    invoke-virtual {p0}, Lcom/android/settings/accounts/MiuiAccountPreferenceBase;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/settingslib/c/a;->cgc(Landroid/content/Context;)V

    invoke-virtual {p0}, Lcom/android/settings/accounts/MiuiAccountPreferenceBase;->amG()V

    return-void
.end method

.method public ame(Landroid/os/UserHandle;)V
    .locals 0

    return-void
.end method

.method public amf(Ljava/lang/String;)Ljava/util/ArrayList;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/accounts/MiuiAccountPreferenceBase;->aAv:Lcom/android/settingslib/c/a;

    invoke-virtual {v0, p1}, Lcom/android/settingslib/c/a;->cgf(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method public amg(Ljava/lang/String;Landroid/preference/PreferenceScreen;)Landroid/preference/PreferenceScreen;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/accounts/MiuiAccountPreferenceBase;->aAx:Lcom/android/settings/accounts/AccountTypePreferenceLoader;

    invoke-virtual {v0, p1, p2}, Lcom/android/settings/accounts/AccountTypePreferenceLoader;->ano(Ljava/lang/String;Landroid/preference/PreferenceScreen;)Landroid/preference/PreferenceScreen;

    move-result-object v0

    return-object v0
.end method

.method protected aoM(Ljava/util/Date;)Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/android/settings/accounts/MiuiAccountPreferenceBase;->aAy:Ljava/text/DateFormat;

    invoke-virtual {v1, p1}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/accounts/MiuiAccountPreferenceBase;->aAC:Ljava/text/DateFormat;

    invoke-virtual {v1, p1}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected aoN(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;
    .locals 2

    iget-object v0, p0, Lcom/android/settings/accounts/MiuiAccountPreferenceBase;->aAv:Lcom/android/settingslib/c/a;

    invoke-virtual {p0}, Lcom/android/settings/accounts/MiuiAccountPreferenceBase;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, Lcom/android/settingslib/c/a;->cgj(Landroid/content/Context;Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    return-object v0
.end method

.method protected aoO(Ljava/lang/String;)Ljava/lang/CharSequence;
    .locals 2

    iget-object v0, p0, Lcom/android/settings/accounts/MiuiAccountPreferenceBase;->aAv:Lcom/android/settingslib/c/a;

    invoke-virtual {p0}, Lcom/android/settings/accounts/MiuiAccountPreferenceBase;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, Lcom/android/settingslib/c/a;->cgd(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->onActivityCreated(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/android/settings/accounts/MiuiAccountPreferenceBase;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Landroid/text/format/DateFormat;->getDateFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v1

    iput-object v1, p0, Lcom/android/settings/accounts/MiuiAccountPreferenceBase;->aAy:Ljava/text/DateFormat;

    invoke-static {v0}, Landroid/text/format/DateFormat;->getTimeFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/accounts/MiuiAccountPreferenceBase;->aAC:Ljava/text/DateFormat;

    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 5

    invoke-super {p0, p1}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->onCreate(Landroid/os/Bundle;)V

    const-string/jumbo v0, "user"

    invoke-virtual {p0, v0}, Lcom/android/settings/accounts/MiuiAccountPreferenceBase;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/UserManager;

    iput-object v0, p0, Lcom/android/settings/accounts/MiuiAccountPreferenceBase;->aAD:Landroid/os/UserManager;

    invoke-virtual {p0}, Lcom/android/settings/accounts/MiuiAccountPreferenceBase;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getActivityToken()Landroid/os/IBinder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/settings/accounts/MiuiAccountPreferenceBase;->aAD:Landroid/os/UserManager;

    invoke-virtual {p0}, Lcom/android/settings/accounts/MiuiAccountPreferenceBase;->getArguments()Landroid/os/Bundle;

    move-result-object v3

    invoke-virtual {v0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v4

    invoke-static {v1, v2, v3, v4}, Lcom/android/settings/aq;->bqL(Landroid/os/IBinder;Landroid/os/UserManager;Landroid/os/Bundle;Landroid/os/Bundle;)Landroid/os/UserHandle;

    move-result-object v1

    iput-object v1, p0, Lcom/android/settings/accounts/MiuiAccountPreferenceBase;->aAu:Landroid/os/UserHandle;

    new-instance v1, Lcom/android/settingslib/c/a;

    iget-object v2, p0, Lcom/android/settings/accounts/MiuiAccountPreferenceBase;->aAu:Landroid/os/UserHandle;

    invoke-direct {v1, v0, v2, p0}, Lcom/android/settingslib/c/a;-><init>(Landroid/content/Context;Landroid/os/UserHandle;Lcom/android/settingslib/c/b;)V

    iput-object v1, p0, Lcom/android/settings/accounts/MiuiAccountPreferenceBase;->aAv:Lcom/android/settingslib/c/a;

    new-instance v0, Lcom/android/settings/accounts/AccountTypePreferenceLoader;

    iget-object v1, p0, Lcom/android/settings/accounts/MiuiAccountPreferenceBase;->aAv:Lcom/android/settingslib/c/a;

    iget-object v2, p0, Lcom/android/settings/accounts/MiuiAccountPreferenceBase;->aAu:Landroid/os/UserHandle;

    invoke-direct {v0, p0, v1, v2}, Lcom/android/settings/accounts/AccountTypePreferenceLoader;-><init>(Landroid/preference/PreferenceFragment;Lcom/android/settingslib/c/a;Landroid/os/UserHandle;)V

    iput-object v0, p0, Lcom/android/settings/accounts/MiuiAccountPreferenceBase;->aAx:Lcom/android/settings/accounts/AccountTypePreferenceLoader;

    return-void
.end method

.method public onPause()V
    .locals 1

    invoke-super {p0}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->onPause()V

    iget-object v0, p0, Lcom/android/settings/accounts/MiuiAccountPreferenceBase;->aAA:Ljava/lang/Object;

    invoke-static {v0}, Landroid/content/ContentResolver;->removeStatusChangeListener(Ljava/lang/Object;)V

    return-void
.end method

.method public onResume()V
    .locals 2

    invoke-super {p0}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->onResume()V

    iget-object v0, p0, Lcom/android/settings/accounts/MiuiAccountPreferenceBase;->aAB:Landroid/content/SyncStatusObserver;

    const/16 v1, 0xd

    invoke-static {v1, v0}, Landroid/content/ContentResolver;->addStatusChangeListener(ILandroid/content/SyncStatusObserver;)Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/accounts/MiuiAccountPreferenceBase;->aAA:Ljava/lang/Object;

    invoke-virtual {p0}, Lcom/android/settings/accounts/MiuiAccountPreferenceBase;->alT()V

    return-void
.end method
