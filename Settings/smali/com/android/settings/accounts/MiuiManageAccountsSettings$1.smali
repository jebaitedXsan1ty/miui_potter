.class final Lcom/android/settings/accounts/MiuiManageAccountsSettings$1;
.super Ljava/lang/Object;
.source "MiuiManageAccountsSettings.java"

# interfaces
.implements Ljava/util/Comparator;


# instance fields
.field private final aAY:Ljava/text/Collator;


# direct methods
.method constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {}, Ljava/text/Collator;->getInstance()Ljava/text/Collator;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/accounts/MiuiManageAccountsSettings$1;->aAY:Ljava/text/Collator;

    return-void
.end method


# virtual methods
.method public api(Landroid/preference/Preference;Landroid/preference/Preference;)I
    .locals 3

    if-nez p1, :cond_0

    if-nez p2, :cond_0

    const/4 v0, 0x0

    return v0

    :cond_0
    if-nez p1, :cond_1

    const/4 v0, -0x1

    return v0

    :cond_1
    if-nez p2, :cond_2

    const/4 v0, 0x1

    return v0

    :cond_2
    iget-object v0, p0, Lcom/android/settings/accounts/MiuiManageAccountsSettings$1;->aAY:Ljava/text/Collator;

    invoke-virtual {p1}, Landroid/preference/Preference;->getTitle()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2}, Landroid/preference/Preference;->getTitle()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/text/Collator;->compare(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1

    check-cast p1, Landroid/preference/Preference;

    check-cast p2, Landroid/preference/Preference;

    invoke-virtual {p0, p1, p2}, Lcom/android/settings/accounts/MiuiManageAccountsSettings$1;->api(Landroid/preference/Preference;Landroid/preference/Preference;)I

    move-result v0

    return v0
.end method
