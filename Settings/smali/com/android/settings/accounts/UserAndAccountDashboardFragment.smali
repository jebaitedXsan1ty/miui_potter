.class public Lcom/android/settings/accounts/UserAndAccountDashboardFragment;
.super Lcom/android/settings/dashboard/MiuiDashboardFragment;
.source "UserAndAccountDashboardFragment.java"


# static fields
.field public static final SEARCH_INDEX_DATA_PROVIDER:Lcom/android/settings/search/Indexable$SearchIndexProvider;

.field public static final SUMMARY_PROVIDER_FACTORY:Lcom/android/settings/dashboard/F;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/android/settings/accounts/UserAndAccountDashboardFragment$1;

    invoke-direct {v0}, Lcom/android/settings/accounts/UserAndAccountDashboardFragment$1;-><init>()V

    sput-object v0, Lcom/android/settings/accounts/UserAndAccountDashboardFragment;->SUMMARY_PROVIDER_FACTORY:Lcom/android/settings/dashboard/F;

    new-instance v0, Lcom/android/settings/accounts/UserAndAccountDashboardFragment$2;

    invoke-direct {v0}, Lcom/android/settings/accounts/UserAndAccountDashboardFragment$2;-><init>()V

    sput-object v0, Lcom/android/settings/accounts/UserAndAccountDashboardFragment;->SEARCH_INDEX_DATA_PROVIDER:Lcom/android/settings/search/Indexable$SearchIndexProvider;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/dashboard/MiuiDashboardFragment;-><init>()V

    return-void
.end method


# virtual methods
.method protected BX(Lcom/android/settingslib/drawer/Tile;)Z
    .locals 3

    const/4 v0, 0x1

    iget-object v1, p1, Lcom/android/settingslib/drawer/Tile;->czw:Landroid/os/Bundle;

    if-eqz v1, :cond_1

    const-string/jumbo v2, "com.android.settings.ia.account"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    return v0
.end method

.method protected ar()Ljava/lang/String;
    .locals 1

    const-string/jumbo v0, "UserAndAccountDashboard"

    return-object v0
.end method

.method protected as()I
    .locals 1

    const v0, 0x7f150101

    return v0
.end method

.method public getMetricsCategory()I
    .locals 1

    const/16 v0, 0x8

    return v0
.end method

.method protected getPreferenceControllers(Landroid/content/Context;)Ljava/util/List;
    .locals 3

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    new-instance v1, Lcom/android/settings/accounts/EmergencyInfoPreferenceController;

    invoke-direct {v1, p1}, Lcom/android/settings/accounts/EmergencyInfoPreferenceController;-><init>(Landroid/content/Context;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v1, Lcom/android/settings/accounts/AddUserWhenLockedPreferenceController;

    invoke-direct {v1, p1}, Lcom/android/settings/accounts/AddUserWhenLockedPreferenceController;-><init>(Landroid/content/Context;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-virtual {p0}, Lcom/android/settings/accounts/UserAndAccountDashboardFragment;->getLifecycle()Lcom/android/settings/core/lifecycle/c;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/android/settings/core/lifecycle/c;->ajv(Lcom/android/settings/core/lifecycle/b;)Lcom/android/settings/core/lifecycle/b;

    new-instance v1, Lcom/android/settings/accounts/AutoSyncDataPreferenceController;

    invoke-direct {v1, p1, p0}, Lcom/android/settings/accounts/AutoSyncDataPreferenceController;-><init>(Landroid/content/Context;Landroid/app/Fragment;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v1, Lcom/android/settings/accounts/AutoSyncPersonalDataPreferenceController;

    invoke-direct {v1, p1, p0}, Lcom/android/settings/accounts/AutoSyncPersonalDataPreferenceController;-><init>(Landroid/content/Context;Landroid/app/Fragment;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v1, Lcom/android/settings/accounts/AutoSyncWorkDataPreferenceController;

    invoke-direct {v1, p1, p0}, Lcom/android/settings/accounts/AutoSyncWorkDataPreferenceController;-><init>(Landroid/content/Context;Landroid/app/Fragment;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-virtual {p0}, Lcom/android/settings/accounts/UserAndAccountDashboardFragment;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string/jumbo v2, "authorities"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getStringArrayExtra(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/android/settings/accounts/AccountPreferenceController;

    invoke-direct {v2, p1, p0, v1}, Lcom/android/settings/accounts/AccountPreferenceController;-><init>(Landroid/content/Context;Lcom/android/settings/MiuiSettingsPreferenceFragment;[Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/android/settings/accounts/UserAndAccountDashboardFragment;->getLifecycle()Lcom/android/settings/core/lifecycle/c;

    move-result-object v1

    invoke-virtual {v1, v2}, Lcom/android/settings/core/lifecycle/c;->ajv(Lcom/android/settings/core/lifecycle/b;)Lcom/android/settings/core/lifecycle/b;

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object v0
.end method
