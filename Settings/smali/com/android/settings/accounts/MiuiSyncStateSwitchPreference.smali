.class public Lcom/android/settings/accounts/MiuiSyncStateSwitchPreference;
.super Landroid/preference/CheckBoxPreference;
.source "MiuiSyncStateSwitchPreference.java"


# instance fields
.field private ayN:Ljava/lang/String;

.field private ayO:Z

.field private ayP:Z

.field private ayQ:Z

.field private ayR:Z

.field private ayS:I

.field private mAccount:Landroid/accounts/Account;

.field private mPackageName:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/accounts/Account;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 3

    const/4 v2, 0x0

    const/4 v0, 0x0

    const v1, 0x7f13016b

    invoke-direct {p0, p1, v0, v2, v1}, Landroid/preference/CheckBoxPreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V

    iput-boolean v2, p0, Lcom/android/settings/accounts/MiuiSyncStateSwitchPreference;->ayP:Z

    iput-boolean v2, p0, Lcom/android/settings/accounts/MiuiSyncStateSwitchPreference;->ayQ:Z

    iput-boolean v2, p0, Lcom/android/settings/accounts/MiuiSyncStateSwitchPreference;->ayO:Z

    iput-boolean v2, p0, Lcom/android/settings/accounts/MiuiSyncStateSwitchPreference;->ayR:Z

    invoke-virtual {p0, p2, p3, p4, p5}, Lcom/android/settings/accounts/MiuiSyncStateSwitchPreference;->amT(Landroid/accounts/Account;Ljava/lang/String;Ljava/lang/String;I)V

    sget v0, Lmiui/R$layout;->preference:I

    invoke-virtual {p0, v0}, Lcom/android/settings/accounts/MiuiSyncStateSwitchPreference;->setLayoutResource(I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3

    const/4 v2, 0x0

    const/4 v1, 0x0

    const v0, 0x7f13016b

    invoke-direct {p0, p1, p2, v1, v0}, Landroid/preference/CheckBoxPreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V

    iput-boolean v1, p0, Lcom/android/settings/accounts/MiuiSyncStateSwitchPreference;->ayP:Z

    iput-boolean v1, p0, Lcom/android/settings/accounts/MiuiSyncStateSwitchPreference;->ayQ:Z

    iput-boolean v1, p0, Lcom/android/settings/accounts/MiuiSyncStateSwitchPreference;->ayO:Z

    iput-boolean v1, p0, Lcom/android/settings/accounts/MiuiSyncStateSwitchPreference;->ayR:Z

    iput-object v2, p0, Lcom/android/settings/accounts/MiuiSyncStateSwitchPreference;->mAccount:Landroid/accounts/Account;

    iput-object v2, p0, Lcom/android/settings/accounts/MiuiSyncStateSwitchPreference;->ayN:Ljava/lang/String;

    iput-object v2, p0, Lcom/android/settings/accounts/MiuiSyncStateSwitchPreference;->mPackageName:Ljava/lang/String;

    iput v1, p0, Lcom/android/settings/accounts/MiuiSyncStateSwitchPreference;->ayS:I

    return-void
.end method


# virtual methods
.method public amT(Landroid/accounts/Account;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/accounts/MiuiSyncStateSwitchPreference;->mAccount:Landroid/accounts/Account;

    iput-object p2, p0, Lcom/android/settings/accounts/MiuiSyncStateSwitchPreference;->ayN:Ljava/lang/String;

    iput-object p3, p0, Lcom/android/settings/accounts/MiuiSyncStateSwitchPreference;->mPackageName:Ljava/lang/String;

    iput p4, p0, Lcom/android/settings/accounts/MiuiSyncStateSwitchPreference;->ayS:I

    invoke-virtual {p0}, Lcom/android/settings/accounts/MiuiSyncStateSwitchPreference;->notifyChanged()V

    return-void
.end method

.method public amU()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/accounts/MiuiSyncStateSwitchPreference;->ayN:Ljava/lang/String;

    return-object v0
.end method

.method public amV()Landroid/accounts/Account;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/accounts/MiuiSyncStateSwitchPreference;->mAccount:Landroid/accounts/Account;

    return-object v0
.end method

.method public amW()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/accounts/MiuiSyncStateSwitchPreference;->mPackageName:Ljava/lang/String;

    return-object v0
.end method

.method public amX()Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/settings/accounts/MiuiSyncStateSwitchPreference;->ayR:Z

    return v0
.end method

.method public amY(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/android/settings/accounts/MiuiSyncStateSwitchPreference;->ayP:Z

    invoke-virtual {p0}, Lcom/android/settings/accounts/MiuiSyncStateSwitchPreference;->notifyChanged()V

    return-void
.end method

.method public amZ(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/android/settings/accounts/MiuiSyncStateSwitchPreference;->ayQ:Z

    invoke-virtual {p0}, Lcom/android/settings/accounts/MiuiSyncStateSwitchPreference;->notifyChanged()V

    return-void
.end method

.method public ana(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/android/settings/accounts/MiuiSyncStateSwitchPreference;->ayO:Z

    invoke-virtual {p0}, Lcom/android/settings/accounts/MiuiSyncStateSwitchPreference;->notifyChanged()V

    return-void
.end method

.method public anb(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/android/settings/accounts/MiuiSyncStateSwitchPreference;->ayR:Z

    invoke-virtual {p0}, Lcom/android/settings/accounts/MiuiSyncStateSwitchPreference;->notifyChanged()V

    return-void
.end method

.method public getUid()I
    .locals 1

    iget v0, p0, Lcom/android/settings/accounts/MiuiSyncStateSwitchPreference;->ayS:I

    return v0
.end method

.method public onBindView(Landroid/view/View;)V
    .locals 7

    const/4 v5, 0x1

    const/16 v3, 0x8

    const/4 v2, 0x0

    invoke-super {p0, p1}, Landroid/preference/CheckBoxPreference;->onBindView(Landroid/view/View;)V

    const v0, 0x7f0a0471

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/settingslib/widget/AnimatedImageView;

    const v1, 0x7f0a0472

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    iget-boolean v1, p0, Lcom/android/settings/accounts/MiuiSyncStateSwitchPreference;->ayP:Z

    if-nez v1, :cond_1

    iget-boolean v1, p0, Lcom/android/settings/accounts/MiuiSyncStateSwitchPreference;->ayQ:Z

    move v4, v1

    :goto_0
    if-eqz v4, :cond_2

    move v1, v2

    :goto_1
    invoke-virtual {v0, v1}, Lcom/android/settingslib/widget/AnimatedImageView;->setVisibility(I)V

    iget-boolean v1, p0, Lcom/android/settings/accounts/MiuiSyncStateSwitchPreference;->ayP:Z

    invoke-virtual {v0, v1}, Lcom/android/settingslib/widget/AnimatedImageView;->setAnimating(Z)V

    iget-boolean v0, p0, Lcom/android/settings/accounts/MiuiSyncStateSwitchPreference;->ayO:Z

    if-eqz v0, :cond_3

    xor-int/lit8 v0, v4, 0x1

    :goto_2
    if-eqz v0, :cond_4

    move v0, v2

    :goto_3
    invoke-virtual {v6, v0}, Landroid/view/View;->setVisibility(I)V

    const v0, 0x1020001

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iget-boolean v1, p0, Lcom/android/settings/accounts/MiuiSyncStateSwitchPreference;->ayR:Z

    if-eqz v1, :cond_5

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    const v0, 0x1020010

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/android/settings/accounts/MiuiSyncStateSwitchPreference;->getContext()Landroid/content/Context;

    move-result-object v1

    new-array v4, v5, [Ljava/lang/Object;

    invoke-virtual {p0}, Lcom/android/settings/accounts/MiuiSyncStateSwitchPreference;->getSummary()Ljava/lang/CharSequence;

    move-result-object v5

    aput-object v5, v4, v2

    const v2, 0x7f121224

    invoke-virtual {v1, v2, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_4
    const v0, 0x7f0a01f2

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    :cond_0
    return-void

    :cond_1
    move v4, v5

    goto :goto_0

    :cond_2
    move v1, v3

    goto :goto_1

    :cond_3
    move v0, v2

    goto :goto_2

    :cond_4
    move v0, v3

    goto :goto_3

    :cond_5
    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_4
.end method

.method protected onClick()V
    .locals 2

    iget-boolean v0, p0, Lcom/android/settings/accounts/MiuiSyncStateSwitchPreference;->ayR:Z

    if-nez v0, :cond_0

    invoke-static {}, Landroid/app/ActivityManager;->isUserAMonkey()Z

    move-result v0

    if-eqz v0, :cond_1

    const-string/jumbo v0, "SyncState"

    const-string/jumbo v1, "ignoring monkey\'s attempt to flip sync state"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-super {p0}, Landroid/preference/CheckBoxPreference;->onClick()V

    goto :goto_0
.end method
