.class final Lcom/android/settings/accounts/MiuiManageAccountsSettings$3;
.super Landroid/os/AsyncTask;
.source "MiuiManageAccountsSettings.java"


# instance fields
.field final synthetic aBa:Lcom/android/settings/accounts/MiuiManageAccountsSettings;


# direct methods
.method constructor <init>(Lcom/android/settings/accounts/MiuiManageAccountsSettings;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/accounts/MiuiManageAccountsSettings$3;->aBa:Lcom/android/settings/accounts/MiuiManageAccountsSettings;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected apj(Ljava/util/List;)V
    .locals 3

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/settings/accounts/MiuiManageAccountsSettings$3;->aBa:Lcom/android/settings/accounts/MiuiManageAccountsSettings;

    invoke-virtual {v0}, Lcom/android/settings/accounts/MiuiManageAccountsSettings;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/accounts/MiuiManageAccountsSettings$3;->aBa:Lcom/android/settings/accounts/MiuiManageAccountsSettings;

    invoke-static {v1}, Lcom/android/settings/accounts/MiuiManageAccountsSettings;->ami(Lcom/android/settings/accounts/MiuiManageAccountsSettings;)Landroid/preference/PreferenceCategory;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    :cond_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/settings/accounts/MiuiManageAccountsSettings$3;->aBa:Lcom/android/settings/accounts/MiuiManageAccountsSettings;

    invoke-virtual {v0}, Lcom/android/settings/accounts/MiuiManageAccountsSettings;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/accounts/MiuiManageAccountsSettings$3;->aBa:Lcom/android/settings/accounts/MiuiManageAccountsSettings;

    invoke-static {v1}, Lcom/android/settings/accounts/MiuiManageAccountsSettings;->ami(Lcom/android/settings/accounts/MiuiManageAccountsSettings;)Landroid/preference/PreferenceCategory;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    iget-object v0, p0, Lcom/android/settings/accounts/MiuiManageAccountsSettings$3;->aBa:Lcom/android/settings/accounts/MiuiManageAccountsSettings;

    invoke-static {v0}, Lcom/android/settings/accounts/MiuiManageAccountsSettings;->ami(Lcom/android/settings/accounts/MiuiManageAccountsSettings;)Landroid/preference/PreferenceCategory;

    move-result-object v0

    invoke-virtual {v0}, Landroid/preference/PreferenceCategory;->removeAll()V

    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/preference/Preference;

    iget-object v2, p0, Lcom/android/settings/accounts/MiuiManageAccountsSettings$3;->aBa:Lcom/android/settings/accounts/MiuiManageAccountsSettings;

    invoke-static {v2}, Lcom/android/settings/accounts/MiuiManageAccountsSettings;->ami(Lcom/android/settings/accounts/MiuiManageAccountsSettings;)Landroid/preference/PreferenceCategory;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/preference/PreferenceCategory;->addPreference(Landroid/preference/Preference;)Z

    goto :goto_0
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/android/settings/accounts/MiuiManageAccountsSettings$3;->doInBackground([Ljava/lang/Void;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/util/List;
    .locals 14

    const/4 v1, 0x0

    const/4 v2, 0x0

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    iget-object v0, p0, Lcom/android/settings/accounts/MiuiManageAccountsSettings$3;->aBa:Lcom/android/settings/accounts/MiuiManageAccountsSettings;

    invoke-virtual {v0}, Lcom/android/settings/accounts/MiuiManageAccountsSettings;->getActivity()Landroid/app/Activity;

    move-result-object v4

    if-nez v4, :cond_0

    return-object v3

    :cond_0
    iget-object v0, p0, Lcom/android/settings/accounts/MiuiManageAccountsSettings$3;->aBa:Lcom/android/settings/accounts/MiuiManageAccountsSettings;

    iget-object v0, v0, Lcom/android/settings/accounts/MiuiManageAccountsSettings;->aAv:Lcom/android/settingslib/c/a;

    invoke-virtual {v0, v2}, Lcom/android/settingslib/c/a;->onAccountsUpdated([Landroid/accounts/Account;)V

    iget-object v0, p0, Lcom/android/settings/accounts/MiuiManageAccountsSettings$3;->aBa:Lcom/android/settings/accounts/MiuiManageAccountsSettings;

    iget-object v0, v0, Lcom/android/settings/accounts/MiuiManageAccountsSettings;->aAv:Lcom/android/settingslib/c/a;

    invoke-virtual {v0}, Lcom/android/settingslib/c/a;->cfZ()[Ljava/lang/String;

    move-result-object v5

    array-length v6, v5

    move v2, v1

    :goto_0
    if-ge v2, v6, :cond_6

    aget-object v7, v5, v2

    const-string/jumbo v0, "com.xiaomi"

    invoke-virtual {v0, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    const-string/jumbo v0, "com.xiaomi.unactivated"

    invoke-virtual {v0, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    :goto_1
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/android/settings/accounts/MiuiManageAccountsSettings$3;->aBa:Lcom/android/settings/accounts/MiuiManageAccountsSettings;

    iget-object v0, v0, Lcom/android/settings/accounts/MiuiManageAccountsSettings;->aAv:Lcom/android/settingslib/c/a;

    invoke-virtual {v0, v4, v7}, Lcom/android/settingslib/c/a;->cgd(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v8

    if-eqz v8, :cond_1

    invoke-static {v7}, Lcom/android/settings/accounts/MiuiManageAccountsSettings;->alP(Ljava/lang/String;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-nez v0, :cond_1

    new-instance v9, Landroid/preference/Preference;

    invoke-direct {v9, v4}, Landroid/preference/Preference;-><init>(Landroid/content/Context;)V

    invoke-static {v4}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v0

    iget-object v10, p0, Lcom/android/settings/accounts/MiuiManageAccountsSettings$3;->aBa:Lcom/android/settings/accounts/MiuiManageAccountsSettings;

    iget-object v10, v10, Lcom/android/settings/accounts/MiuiManageAccountsSettings;->aAu:Landroid/os/UserHandle;

    invoke-virtual {v0, v7, v10}, Landroid/accounts/AccountManager;->getAccountsByTypeAsUser(Ljava/lang/String;Landroid/os/UserHandle;)[Landroid/accounts/Account;

    move-result-object v10

    array-length v0, v10

    const/4 v11, 0x1

    if-ne v0, v11, :cond_4

    iget-object v0, p0, Lcom/android/settings/accounts/MiuiManageAccountsSettings$3;->aBa:Lcom/android/settings/accounts/MiuiManageAccountsSettings;

    iget-object v0, v0, Lcom/android/settings/accounts/MiuiManageAccountsSettings;->aAv:Lcom/android/settingslib/c/a;

    invoke-virtual {v0, v7}, Lcom/android/settingslib/c/a;->cgg(Ljava/lang/String;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    :goto_2
    invoke-virtual {v9}, Landroid/preference/Preference;->getExtras()Landroid/os/Bundle;

    move-result-object v11

    const-string/jumbo v12, "account_type"

    invoke-virtual {v11, v12, v7}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v9}, Landroid/preference/Preference;->getExtras()Landroid/os/Bundle;

    move-result-object v11

    const-string/jumbo v12, "UserHandle"

    iget-object v13, p0, Lcom/android/settings/accounts/MiuiManageAccountsSettings$3;->aBa:Lcom/android/settings/accounts/MiuiManageAccountsSettings;

    iget-object v13, v13, Lcom/android/settings/accounts/MiuiManageAccountsSettings;->aAu:Landroid/os/UserHandle;

    invoke-virtual {v11, v12, v13}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    if-eqz v0, :cond_5

    const-class v0, Lcom/android/settings/accounts/AccountSyncSettings;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v9, v0}, Landroid/preference/Preference;->setFragment(Ljava/lang/String;)V

    invoke-virtual {v9}, Landroid/preference/Preference;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string/jumbo v11, "account"

    aget-object v10, v10, v1

    invoke-virtual {v0, v11, v10}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    :goto_3
    iget-object v0, p0, Lcom/android/settings/accounts/MiuiManageAccountsSettings$3;->aBa:Lcom/android/settings/accounts/MiuiManageAccountsSettings;

    iget-object v0, v0, Lcom/android/settings/accounts/MiuiManageAccountsSettings;->aAv:Lcom/android/settingslib/c/a;

    invoke-virtual {v0, v4, v7}, Lcom/android/settingslib/c/a;->cgj(Landroid/content/Context;Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-static {v0}, Lmiui/content/res/IconCustomizer;->generateIconStyleDrawable(Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/BitmapDrawable;

    move-result-object v0

    :cond_3
    invoke-virtual {v9, v8}, Landroid/preference/Preference;->setTitle(Ljava/lang/CharSequence;)V

    invoke-virtual {v9, v0}, Landroid/preference/Preference;->setIcon(Landroid/graphics/drawable/Drawable;)V

    const v0, 0x7f0d014e

    invoke-virtual {v9, v0}, Landroid/preference/Preference;->setLayoutResource(I)V

    invoke-interface {v3, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1

    :cond_4
    move v0, v1

    goto :goto_2

    :cond_5
    const-class v0, Lcom/android/settings/accounts/MiuiManageAccounts;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v9, v0}, Landroid/preference/Preference;->setFragment(Ljava/lang/String;)V

    goto :goto_3

    :cond_6
    invoke-static {}, Lcom/android/settings/accounts/MiuiManageAccountsSettings;->-get0()Ljava/util/Comparator;

    move-result-object v0

    invoke-static {v3, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    return-object v3
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    check-cast p1, Ljava/util/List;

    invoke-virtual {p0, p1}, Lcom/android/settings/accounts/MiuiManageAccountsSettings$3;->apj(Ljava/util/List;)V

    return-void
.end method
