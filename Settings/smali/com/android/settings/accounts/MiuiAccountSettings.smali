.class public Lcom/android/settings/accounts/MiuiAccountSettings;
.super Lcom/android/settings/MiuiSettingsPreferenceFragment;
.source "MiuiAccountSettings.java"

# interfaces
.implements Lcom/android/settingslib/c/b;
.implements Landroid/preference/Preference$OnPreferenceClickListener;
.implements Lcom/android/settings/search/Indexable;


# static fields
.field public static final SEARCH_INDEX_DATA_PROVIDER:Lcom/android/settings/search/Indexable$SearchIndexProvider;


# instance fields
.field private azL:Landroid/app/Activity;

.field private azM:[Ljava/lang/String;

.field private azN:I

.field private azO:Lmiui/preference/ValuePreference;

.field private azP:Landroid/app/AlertDialog;

.field private azQ:Lcom/android/settings/accounts/MiuiAccountSettings$ManagedProfileBroadcastReceiver;

.field private azR:Landroid/preference/Preference;

.field private azS:Landroid/util/SparseArray;

.field private azT:Lcom/android/settings/accounts/MiuiAccountSettings$SyncDrawable;

.field private azU:Landroid/preference/CheckBoxPreference;

.field private azV:Z

.field private azW:Landroid/os/UserManager;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/android/settings/accounts/MiuiAccountSettings$1;

    invoke-direct {v0}, Lcom/android/settings/accounts/MiuiAccountSettings$1;-><init>()V

    sput-object v0, Lcom/android/settings/accounts/MiuiAccountSettings;->SEARCH_INDEX_DATA_PROVIDER:Lcom/android/settings/search/Indexable$SearchIndexProvider;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Lcom/android/settings/MiuiSettingsPreferenceFragment;-><init>()V

    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/android/settings/accounts/MiuiAccountSettings;->azS:Landroid/util/SparseArray;

    new-instance v0, Lcom/android/settings/accounts/MiuiAccountSettings$ManagedProfileBroadcastReceiver;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/android/settings/accounts/MiuiAccountSettings$ManagedProfileBroadcastReceiver;-><init>(Lcom/android/settings/accounts/MiuiAccountSettings;Lcom/android/settings/accounts/MiuiAccountSettings$ManagedProfileBroadcastReceiver;)V

    iput-object v0, p0, Lcom/android/settings/accounts/MiuiAccountSettings;->azQ:Lcom/android/settings/accounts/MiuiAccountSettings$ManagedProfileBroadcastReceiver;

    const/4 v0, 0x0

    iput v0, p0, Lcom/android/settings/accounts/MiuiAccountSettings;->azN:I

    return-void
.end method

.method private anZ(Lcom/android/settingslib/c/a;Ljava/lang/String;)Z
    .locals 5

    const/4 v4, 0x1

    const/4 v1, 0x0

    iget v0, p0, Lcom/android/settings/accounts/MiuiAccountSettings;->azN:I

    if-nez v0, :cond_0

    return v4

    :cond_0
    invoke-virtual {p1, p2}, Lcom/android/settingslib/c/a;->cgf(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v2

    if-nez v2, :cond_1

    const-string/jumbo v0, "AccountSettings"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "No sync authorities for account type: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return v1

    :cond_1
    move v0, v1

    :goto_0
    iget v3, p0, Lcom/android/settings/accounts/MiuiAccountSettings;->azN:I

    if-ge v0, v3, :cond_3

    iget-object v3, p0, Lcom/android/settings/accounts/MiuiAccountSettings;->azM:[Ljava/lang/String;

    aget-object v3, v3, v0

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    return v4

    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_3
    return v1
.end method

.method static synthetic aoA(Lcom/android/settings/accounts/MiuiAccountSettings;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/accounts/MiuiAccountSettings;->aol()V

    return-void
.end method

.method static synthetic aoB(Lcom/android/settings/accounts/MiuiAccountSettings;ZI)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/android/settings/accounts/MiuiAccountSettings;->aom(ZI)V

    return-void
.end method

.method static synthetic aoC(Lcom/android/settings/accounts/MiuiAccountSettings;ZI)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/android/settings/accounts/MiuiAccountSettings;->aoq(ZI)V

    return-void
.end method

.method private aoa()V
    .locals 1

    invoke-virtual {p0}, Lcom/android/settings/accounts/MiuiAccountSettings;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/preference/PreferenceScreen;->removeAll()V

    :cond_0
    iget-object v0, p0, Lcom/android/settings/accounts/MiuiAccountSettings;->azS:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->clear()V

    return-void
.end method

.method private aob(Lcom/android/settingslib/c/a;Landroid/os/UserHandle;)Ljava/util/ArrayList;
    .locals 13

    invoke-virtual {p1}, Lcom/android/settingslib/c/a;->cfZ()[Ljava/lang/String;

    move-result-object v10

    new-instance v11, Ljava/util/ArrayList;

    array-length v0, v10

    invoke-direct {v11, v0}, Ljava/util/ArrayList;-><init>(I)V

    const/4 v0, 0x0

    move v9, v0

    :goto_0
    array-length v0, v10

    if-ge v9, v0, :cond_4

    aget-object v12, v10, v9

    invoke-direct {p0, p1, v12}, Lcom/android/settings/accounts/MiuiAccountSettings;->anZ(Lcom/android/settingslib/c/a;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    :goto_1
    add-int/lit8 v0, v9, 0x1

    move v9, v0

    goto :goto_0

    :cond_1
    const-string/jumbo v0, "com.xiaomi"

    invoke-virtual {v0, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string/jumbo v0, "com.xiaomi.unactivated"

    invoke-virtual {v0, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/android/settings/accounts/MiuiAccountSettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {p1, v0, v12}, Lcom/android/settingslib/c/a;->cgd(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v3

    if-eqz v3, :cond_0

    invoke-virtual {p1, v12}, Lcom/android/settingslib/c/a;->cgl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v12}, Lcom/android/settingslib/c/a;->cgk(Ljava/lang/String;)I

    move-result v5

    invoke-virtual {p0}, Lcom/android/settings/accounts/MiuiAccountSettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v0

    invoke-virtual {v0, v12, p2}, Landroid/accounts/AccountManager;->getAccountsByTypeAsUser(Ljava/lang/String;Landroid/os/UserHandle;)[Landroid/accounts/Account;

    move-result-object v1

    array-length v0, v1

    const/4 v2, 0x1

    if-ne v0, v2, :cond_2

    invoke-virtual {p1, v12}, Lcom/android/settingslib/c/a;->cgg(Ljava/lang/String;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    :goto_2
    if-eqz v0, :cond_3

    new-instance v7, Landroid/os/Bundle;

    invoke-direct {v7}, Landroid/os/Bundle;-><init>()V

    const-string/jumbo v0, "account"

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-virtual {v7, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    const-string/jumbo v0, "android.intent.extra.USER"

    invoke-virtual {v7, v0, p2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    new-instance v0, Lcom/android/settings/accounts/MiuiAccountSettings$AccountPreference;

    invoke-virtual {p0}, Lcom/android/settings/accounts/MiuiAccountSettings;->bWz()Landroid/content/Context;

    move-result-object v2

    const-class v1, Lcom/android/settings/accounts/AccountSyncSettings;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0}, Lcom/android/settings/accounts/MiuiAccountSettings;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {p1, v1, v12}, Lcom/android/settingslib/c/a;->cgj(Landroid/content/Context;Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v8

    move-object v1, p0

    invoke-direct/range {v0 .. v8}, Lcom/android/settings/accounts/MiuiAccountSettings$AccountPreference;-><init>(Lcom/android/settings/accounts/MiuiAccountSettings;Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/String;ILjava/lang/String;Landroid/os/Bundle;Landroid/graphics/drawable/Drawable;)V

    invoke-virtual {v11, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :goto_3
    invoke-virtual {p0}, Lcom/android/settings/accounts/MiuiAccountSettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {p1, v0, v12}, Lcom/android/settingslib/c/a;->cgh(Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_1

    :cond_2
    const/4 v0, 0x0

    goto :goto_2

    :cond_3
    new-instance v7, Landroid/os/Bundle;

    invoke-direct {v7}, Landroid/os/Bundle;-><init>()V

    const-string/jumbo v0, "account_type"

    invoke-virtual {v7, v0, v12}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string/jumbo v0, "account_label"

    invoke-interface {v3}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v7, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string/jumbo v0, "android.intent.extra.USER"

    invoke-virtual {v7, v0, p2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    new-instance v0, Lcom/android/settings/accounts/MiuiAccountSettings$AccountPreference;

    invoke-virtual {p0}, Lcom/android/settings/accounts/MiuiAccountSettings;->bWz()Landroid/content/Context;

    move-result-object v2

    const-class v1, Lcom/android/settings/accounts/MiuiManageAccounts;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0}, Lcom/android/settings/accounts/MiuiAccountSettings;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {p1, v1, v12}, Lcom/android/settingslib/c/a;->cgj(Landroid/content/Context;Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v8

    move-object v1, p0

    invoke-direct/range {v0 .. v8}, Lcom/android/settings/accounts/MiuiAccountSettings$AccountPreference;-><init>(Lcom/android/settings/accounts/MiuiAccountSettings;Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/String;ILjava/lang/String;Landroid/os/Bundle;Landroid/graphics/drawable/Drawable;)V

    invoke-virtual {v11, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_3

    :cond_4
    new-instance v0, Lcom/android/settings/accounts/MiuiAccountSettings$4;

    invoke-direct {v0, p0}, Lcom/android/settings/accounts/MiuiAccountSettings$4;-><init>(Lcom/android/settings/accounts/MiuiAccountSettings;)V

    invoke-static {v11, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    return-object v11
.end method

.method private aoc(Landroid/content/Context;Landroid/content/pm/UserInfo;)Ljava/lang/String;
    .locals 3

    const/4 v2, 0x0

    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    iget v1, p2, Landroid/content/pm/UserInfo;->id:I

    invoke-static {p1, v1}, Lcom/android/settings/aq;->bqB(Landroid/content/Context;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v1

    if-nez v1, :cond_0

    return-object v2

    :cond_0
    invoke-virtual {v0, v1}, Landroid/content/pm/PackageManager;->getApplicationLabel(Landroid/content/pm/ApplicationInfo;)Ljava/lang/CharSequence;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object v0, v1, v2

    const v0, 0x7f120a1f

    invoke-virtual {p0, v0, v1}, Lcom/android/settings/accounts/MiuiAccountSettings;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private aod()V
    .locals 3

    iget-object v0, p0, Lcom/android/settings/accounts/MiuiAccountSettings;->azS:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_1

    iget-object v0, p0, Lcom/android/settings/accounts/MiuiAccountSettings;->azS:Landroid/util/SparseArray;

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settings/accounts/MiuiAccountSettings$ProfileData;

    iget-object v0, v0, Lcom/android/settings/accounts/MiuiAccountSettings$ProfileData;->azY:Lcom/android/settingslib/c/a;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/android/settingslib/c/a;->cgb()V

    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    return-void
.end method

.method private aoe(Landroid/content/Context;)Lcom/android/settings/DimmableIconPreference;
    .locals 2

    new-instance v0, Lcom/android/settings/DimmableIconPreference;

    invoke-virtual {p0}, Lcom/android/settings/accounts/MiuiAccountSettings;->bWz()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/android/settings/DimmableIconPreference;-><init>(Landroid/content/Context;)V

    const v1, 0x7f12009b

    invoke-virtual {v0, v1}, Lcom/android/settings/DimmableIconPreference;->setTitle(I)V

    const v1, 0x7f080202

    invoke-virtual {v0, v1}, Lcom/android/settings/DimmableIconPreference;->setIcon(I)V

    invoke-virtual {v0, p0}, Lcom/android/settings/DimmableIconPreference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    const/16 v1, 0x3e8

    invoke-virtual {v0, v1}, Lcom/android/settings/DimmableIconPreference;->setOrder(I)V

    return-object v0
.end method

.method private aof()Landroid/preference/Preference;
    .locals 2

    new-instance v0, Landroid/preference/Preference;

    invoke-virtual {p0}, Lcom/android/settings/accounts/MiuiAccountSettings;->bWz()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/preference/Preference;-><init>(Landroid/content/Context;)V

    const v1, 0x7f120a1d

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setTitle(I)V

    const v1, 0x7f080280

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setIcon(I)V

    invoke-virtual {v0, p0}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    const/16 v1, 0x3e9

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setOrder(I)V

    return-object v0
.end method

.method private aog(Landroid/content/Context;)Landroid/preference/Preference;
    .locals 2

    new-instance v0, Landroid/preference/Preference;

    invoke-virtual {p0}, Lcom/android/settings/accounts/MiuiAccountSettings;->bWz()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/preference/Preference;-><init>(Landroid/content/Context;)V

    const v1, 0x7f120e0a

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setTitle(I)V

    const v1, 0x7f080205

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setIcon(I)V

    invoke-virtual {v0, p0}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    const/16 v1, 0x3ea

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setOrder(I)V

    return-object v0
.end method

.method private aoi(Landroid/view/MenuItem;)V
    .locals 1

    iget-object v0, p0, Lcom/android/settings/accounts/MiuiAccountSettings;->azT:Lcom/android/settings/accounts/MiuiAccountSettings$SyncDrawable;

    invoke-virtual {v0}, Lcom/android/settings/accounts/MiuiAccountSettings$SyncDrawable;->bmR()V

    return-void
.end method

.method private aoj(Z)V
    .locals 3

    iget-object v0, p0, Lcom/android/settings/accounts/MiuiAccountSettings;->azL:Landroid/app/Activity;

    invoke-static {v0}, Lmiui/accounts/ExtraAccountManager;->getXiaomiAccount(Landroid/content/Context;)Landroid/accounts/Account;

    move-result-object v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    const-string/jumbo v1, "sms"

    invoke-static {v0, v1}, Landroid/content/ContentResolver;->getSyncAutomatically(Landroid/accounts/Account;Ljava/lang/String;)Z

    move-result v0

    new-instance v1, Landroid/content/Intent;

    const-string/jumbo v2, "com.miui.cloudservice.mms.UPLOAD_PHONE_LIST"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string/jumbo v2, "extra_upload_opt"

    if-eqz v0, :cond_1

    if-eqz p1, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string/jumbo v0, "com.miui.cloudservice"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    iget-object v0, p0, Lcom/android/settings/accounts/MiuiAccountSettings;->azL:Landroid/app/Activity;

    invoke-virtual {v0, v1}, Landroid/app/Activity;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    return-void

    :cond_1
    const/4 v0, 0x2

    goto :goto_0
.end method

.method private aok(Landroid/view/MenuItem;)V
    .locals 1

    iget-object v0, p0, Lcom/android/settings/accounts/MiuiAccountSettings;->azT:Lcom/android/settings/accounts/MiuiAccountSettings$SyncDrawable;

    invoke-virtual {v0}, Lcom/android/settings/accounts/MiuiAccountSettings$SyncDrawable;->bmS()V

    return-void
.end method

.method private aol()V
    .locals 3

    iget-object v0, p0, Lcom/android/settings/accounts/MiuiAccountSettings;->azS:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_1

    iget-object v0, p0, Lcom/android/settings/accounts/MiuiAccountSettings;->azS:Landroid/util/SparseArray;

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settings/accounts/MiuiAccountSettings$ProfileData;

    iget-object v0, v0, Lcom/android/settings/accounts/MiuiAccountSettings$ProfileData;->azY:Lcom/android/settingslib/c/a;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/android/settingslib/c/a;->cgi()V

    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    return-void
.end method

.method private aom(ZI)V
    .locals 3

    if-eqz p1, :cond_0

    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/android/settings/accounts/MiuiAccountSettings;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v1, 0x7f12120b

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    new-instance v1, Lcom/android/settings/accounts/MiuiAccountSettings$3;

    invoke-direct {v1, p0, p2}, Lcom/android/settings/accounts/MiuiAccountSettings$3;-><init>(Lcom/android/settings/accounts/MiuiAccountSettings;I)V

    const v2, 0x7f121223

    invoke-virtual {v0, v2, v1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f1205f1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x0

    invoke-direct {p0, v0, p2}, Lcom/android/settings/accounts/MiuiAccountSettings;->aoq(ZI)V

    goto :goto_0
.end method

.method private aon()Z
    .locals 12

    const/4 v1, 0x0

    const-string/jumbo v0, "account"

    invoke-virtual {p0, v0}, Lcom/android/settings/accounts/MiuiAccountSettings;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/accounts/AccountManager;

    invoke-virtual {v0}, Landroid/accounts/AccountManager;->getAccounts()[Landroid/accounts/Account;

    move-result-object v3

    invoke-static {}, Landroid/content/ContentResolver;->getSyncAdapterTypes()[Landroid/content/SyncAdapterType;

    move-result-object v4

    invoke-static {}, Landroid/content/ContentResolver;->getMasterSyncAutomatically()Z

    move-result v5

    array-length v6, v3

    move v2, v1

    :goto_0
    if-ge v2, v6, :cond_3

    aget-object v7, v3, v2

    array-length v8, v4

    move v0, v1

    :goto_1
    if-ge v0, v8, :cond_2

    aget-object v9, v4, v0

    iget-object v10, v9, Landroid/content/SyncAdapterType;->accountType:Ljava/lang/String;

    iget-object v11, v7, Landroid/accounts/Account;->type:Ljava/lang/String;

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_1

    if-eqz v5, :cond_0

    iget-object v9, v9, Landroid/content/SyncAdapterType;->authority:Ljava/lang/String;

    invoke-static {v7, v9}, Landroid/content/ContentResolver;->getSyncAutomatically(Landroid/accounts/Account;Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_1

    :cond_0
    const/4 v0, 0x1

    return v0

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_3
    return v1
.end method

.method private aoo(Ljava/lang/String;)V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/accounts/MiuiAccountSettings;->azL:Landroid/app/Activity;

    if-nez v0, :cond_0

    return-void

    :cond_0
    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v1, "miui.intent.action.TRACK_EVENT"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string/jumbo v1, "eventId"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    iget-object v1, p0, Lcom/android/settings/accounts/MiuiAccountSettings;->azL:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    iget-object v1, p0, Lcom/android/settings/accounts/MiuiAccountSettings;->azL:Landroid/app/Activity;

    invoke-virtual {v1, v0}, Landroid/app/Activity;->sendBroadcast(Landroid/content/Intent;)V

    return-void
.end method

.method private aop(Z)V
    .locals 1

    if-eqz p1, :cond_0

    const-string/jumbo v0, "account_settings_wifi_only_enabled"

    :goto_0
    invoke-direct {p0, v0}, Lcom/android/settings/accounts/MiuiAccountSettings;->aoo(Ljava/lang/String;)V

    return-void

    :cond_0
    const-string/jumbo v0, "account_settings_wifi_only_disabled"

    goto :goto_0
.end method

.method private aoq(ZI)V
    .locals 13

    const/4 v1, 0x0

    const-string/jumbo v0, "account"

    invoke-virtual {p0, v0}, Lcom/android/settings/accounts/MiuiAccountSettings;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/accounts/AccountManager;

    invoke-virtual {v0}, Landroid/accounts/AccountManager;->getAccounts()[Landroid/accounts/Account;

    move-result-object v3

    invoke-static {p2}, Landroid/content/ContentResolver;->getSyncAdapterTypesAsUser(I)[Landroid/content/SyncAdapterType;

    move-result-object v4

    new-instance v5, Landroid/os/Bundle;

    invoke-direct {v5}, Landroid/os/Bundle;-><init>()V

    invoke-static {}, Lcom/google/android/collect/Sets;->newHashSet()Ljava/util/HashSet;

    move-result-object v6

    array-length v7, v3

    move v2, v1

    :goto_0
    if-ge v2, v7, :cond_4

    aget-object v8, v3, v2

    invoke-interface {v6}, Ljava/util/Set;->clear()V

    array-length v9, v4

    move v0, v1

    :goto_1
    if-ge v0, v9, :cond_1

    aget-object v10, v4, v0

    iget-object v11, v10, Landroid/content/SyncAdapterType;->accountType:Ljava/lang/String;

    iget-object v12, v8, Landroid/accounts/Account;->type:Ljava/lang/String;

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_0

    iget-object v11, v10, Landroid/content/SyncAdapterType;->authority:Ljava/lang/String;

    invoke-static {v8, v11, p2}, Landroid/content/ContentResolver;->getSyncAutomaticallyAsUser(Landroid/accounts/Account;Ljava/lang/String;I)Z

    move-result v11

    if-eqz v11, :cond_0

    invoke-interface {v6, v10}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_1
    invoke-interface {v6}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :goto_2
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/SyncAdapterType;

    if-eqz p1, :cond_2

    iget-object v0, v0, Landroid/content/SyncAdapterType;->authority:Ljava/lang/String;

    invoke-static {v8, v0, p2, v5}, Landroid/content/ContentResolver;->requestSyncAsUser(Landroid/accounts/Account;Ljava/lang/String;ILandroid/os/Bundle;)V

    goto :goto_2

    :cond_2
    iget-object v0, v0, Landroid/content/SyncAdapterType;->authority:Ljava/lang/String;

    invoke-static {v8, v0, p2}, Landroid/content/ContentResolver;->cancelSyncAsUser(Landroid/accounts/Account;Ljava/lang/String;I)V

    goto :goto_2

    :cond_3
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_4
    return-void
.end method

.method private aor(Lcom/android/settings/accounts/MiuiAccountSettings$ProfileData;)V
    .locals 5

    const/4 v0, 0x0

    const/4 v2, 0x0

    iget-object v1, p1, Lcom/android/settings/accounts/MiuiAccountSettings$ProfileData;->aAa:Landroid/preference/PreferenceGroup;

    invoke-virtual {v1}, Landroid/preference/PreferenceGroup;->removeAll()V

    iget-object v1, p1, Lcom/android/settings/accounts/MiuiAccountSettings$ProfileData;->aAc:Landroid/content/pm/UserInfo;

    invoke-virtual {v1}, Landroid/content/pm/UserInfo;->isEnabled()Z

    move-result v1

    if-eqz v1, :cond_4

    iget-object v1, p1, Lcom/android/settings/accounts/MiuiAccountSettings$ProfileData;->azY:Lcom/android/settingslib/c/a;

    iget-object v2, p1, Lcom/android/settings/accounts/MiuiAccountSettings$ProfileData;->aAc:Landroid/content/pm/UserInfo;

    invoke-virtual {v2}, Landroid/content/pm/UserInfo;->getUserHandle()Landroid/os/UserHandle;

    move-result-object v2

    invoke-direct {p0, v1, v2}, Lcom/android/settings/accounts/MiuiAccountSettings;->aob(Lcom/android/settingslib/c/a;Landroid/os/UserHandle;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v3

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    iget-object v4, p1, Lcom/android/settings/accounts/MiuiAccountSettings$ProfileData;->aAa:Landroid/preference/PreferenceGroup;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/preference/Preference;

    invoke-virtual {v4, v0}, Landroid/preference/PreferenceGroup;->addPreference(Landroid/preference/Preference;)Z

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    iget-object v0, p1, Lcom/android/settings/accounts/MiuiAccountSettings$ProfileData;->azX:Lcom/android/settings/DimmableIconPreference;

    if-eqz v0, :cond_1

    iget-object v0, p1, Lcom/android/settings/accounts/MiuiAccountSettings$ProfileData;->aAa:Landroid/preference/PreferenceGroup;

    iget-object v1, p1, Lcom/android/settings/accounts/MiuiAccountSettings$ProfileData;->azX:Lcom/android/settings/DimmableIconPreference;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceGroup;->addPreference(Landroid/preference/Preference;)Z

    :cond_1
    :goto_1
    iget-object v0, p1, Lcom/android/settings/accounts/MiuiAccountSettings$ProfileData;->aAb:Landroid/preference/Preference;

    if-eqz v0, :cond_2

    iget-object v0, p1, Lcom/android/settings/accounts/MiuiAccountSettings$ProfileData;->aAa:Landroid/preference/PreferenceGroup;

    iget-object v1, p1, Lcom/android/settings/accounts/MiuiAccountSettings$ProfileData;->aAb:Landroid/preference/Preference;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceGroup;->addPreference(Landroid/preference/Preference;)Z

    :cond_2
    iget-object v0, p1, Lcom/android/settings/accounts/MiuiAccountSettings$ProfileData;->azZ:Landroid/preference/Preference;

    if-eqz v0, :cond_3

    iget-object v0, p1, Lcom/android/settings/accounts/MiuiAccountSettings$ProfileData;->aAa:Landroid/preference/PreferenceGroup;

    iget-object v1, p1, Lcom/android/settings/accounts/MiuiAccountSettings$ProfileData;->azZ:Landroid/preference/Preference;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceGroup;->addPreference(Landroid/preference/Preference;)Z

    :cond_3
    return-void

    :cond_4
    iget-object v1, p0, Lcom/android/settings/accounts/MiuiAccountSettings;->azR:Landroid/preference/Preference;

    invoke-virtual {v1, v0}, Landroid/preference/Preference;->setEnabled(Z)V

    iget-object v0, p0, Lcom/android/settings/accounts/MiuiAccountSettings;->azR:Landroid/preference/Preference;

    const v1, 0x7f080114

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setIcon(I)V

    iget-object v0, p0, Lcom/android/settings/accounts/MiuiAccountSettings;->azR:Landroid/preference/Preference;

    invoke-virtual {v0, v2}, Landroid/preference/Preference;->setTitle(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/android/settings/accounts/MiuiAccountSettings;->azR:Landroid/preference/Preference;

    const v1, 0x7f120a1c

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setSummary(I)V

    iget-object v0, p1, Lcom/android/settings/accounts/MiuiAccountSettings$ProfileData;->aAa:Landroid/preference/PreferenceGroup;

    iget-object v1, p0, Lcom/android/settings/accounts/MiuiAccountSettings;->azR:Landroid/preference/Preference;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceGroup;->addPreference(Landroid/preference/Preference;)Z

    goto :goto_1
.end method

.method private aos(Landroid/content/pm/UserInfo;ZLandroid/preference/PreferenceScreen;)V
    .locals 6

    invoke-virtual {p0}, Lcom/android/settings/accounts/MiuiAccountSettings;->getActivity()Landroid/app/Activity;

    move-result-object v1

    new-instance v2, Lcom/android/settings/accounts/MiuiAccountSettings$ProfileData;

    const/4 v0, 0x0

    invoke-direct {v2, v0}, Lcom/android/settings/accounts/MiuiAccountSettings$ProfileData;-><init>(Lcom/android/settings/accounts/MiuiAccountSettings$ProfileData;)V

    iput-object p1, v2, Lcom/android/settings/accounts/MiuiAccountSettings$ProfileData;->aAc:Landroid/content/pm/UserInfo;

    if-eqz p2, :cond_2

    new-instance v0, Lcom/android/settings/cA;

    invoke-virtual {p0}, Lcom/android/settings/accounts/MiuiAccountSettings;->bWz()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v0, v3}, Lcom/android/settings/cA;-><init>(Landroid/content/Context;)V

    iput-object v0, v2, Lcom/android/settings/accounts/MiuiAccountSettings$ProfileData;->aAa:Landroid/preference/PreferenceGroup;

    invoke-virtual {p1}, Landroid/content/pm/UserInfo;->isManagedProfile()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, v2, Lcom/android/settings/accounts/MiuiAccountSettings$ProfileData;->aAa:Landroid/preference/PreferenceGroup;

    const v3, 0x7f0d0288

    invoke-virtual {v0, v3}, Landroid/preference/PreferenceGroup;->setLayoutResource(I)V

    iget-object v0, v2, Lcom/android/settings/accounts/MiuiAccountSettings$ProfileData;->aAa:Landroid/preference/PreferenceGroup;

    const v3, 0x7f1203ec

    invoke-virtual {v0, v3}, Landroid/preference/PreferenceGroup;->setTitle(I)V

    invoke-direct {p0, v1, p1}, Lcom/android/settings/accounts/MiuiAccountSettings;->aoc(Landroid/content/Context;Landroid/content/pm/UserInfo;)Ljava/lang/String;

    move-result-object v3

    iget-object v0, v2, Lcom/android/settings/accounts/MiuiAccountSettings$ProfileData;->aAa:Landroid/preference/PreferenceGroup;

    invoke-virtual {v0, v3}, Landroid/preference/PreferenceGroup;->setSummary(Ljava/lang/CharSequence;)V

    iget-object v0, v2, Lcom/android/settings/accounts/MiuiAccountSettings$ProfileData;->aAa:Landroid/preference/PreferenceGroup;

    check-cast v0, Lcom/android/settings/cA;

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v3, v4, v5

    const v3, 0x7f120043

    invoke-virtual {p0, v3, v4}, Lcom/android/settings/accounts/MiuiAccountSettings;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/android/settings/cA;->bTs(Ljava/lang/String;)V

    invoke-direct {p0, v1}, Lcom/android/settings/accounts/MiuiAccountSettings;->aog(Landroid/content/Context;)Landroid/preference/Preference;

    move-result-object v0

    iput-object v0, v2, Lcom/android/settings/accounts/MiuiAccountSettings$ProfileData;->aAb:Landroid/preference/Preference;

    invoke-direct {p0}, Lcom/android/settings/accounts/MiuiAccountSettings;->aof()Landroid/preference/Preference;

    move-result-object v0

    iput-object v0, v2, Lcom/android/settings/accounts/MiuiAccountSettings$ProfileData;->azZ:Landroid/preference/Preference;

    :goto_0
    iget-object v0, v2, Lcom/android/settings/accounts/MiuiAccountSettings$ProfileData;->aAa:Landroid/preference/PreferenceGroup;

    invoke-virtual {p3, v0}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    :goto_1
    invoke-virtual {p1}, Landroid/content/pm/UserInfo;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Lcom/android/settingslib/c/a;

    invoke-virtual {p1}, Landroid/content/pm/UserInfo;->getUserHandle()Landroid/os/UserHandle;

    move-result-object v3

    invoke-direct {v0, v1, v3, p0}, Lcom/android/settingslib/c/a;-><init>(Landroid/content/Context;Landroid/os/UserHandle;Lcom/android/settingslib/c/b;)V

    iput-object v0, v2, Lcom/android/settings/accounts/MiuiAccountSettings$ProfileData;->azY:Lcom/android/settingslib/c/a;

    const-string/jumbo v0, "no_modify_accounts"

    iget v3, p1, Landroid/content/pm/UserInfo;->id:I

    invoke-static {v1, v0, v3}, Lcom/android/settingslib/w;->crn(Landroid/content/Context;Ljava/lang/String;I)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0, v1}, Lcom/android/settings/accounts/MiuiAccountSettings;->aoe(Landroid/content/Context;)Lcom/android/settings/DimmableIconPreference;

    move-result-object v0

    iput-object v0, v2, Lcom/android/settings/accounts/MiuiAccountSettings$ProfileData;->azX:Lcom/android/settings/DimmableIconPreference;

    iget-object v0, v2, Lcom/android/settings/accounts/MiuiAccountSettings$ProfileData;->azX:Lcom/android/settings/DimmableIconPreference;

    const-string/jumbo v1, "no_modify_accounts"

    iget v3, p1, Landroid/content/pm/UserInfo;->id:I

    invoke-virtual {v0, v1, v3}, Lcom/android/settings/DimmableIconPreference;->cqd(Ljava/lang/String;I)V

    :cond_0
    iget-object v0, p0, Lcom/android/settings/accounts/MiuiAccountSettings;->azS:Landroid/util/SparseArray;

    iget v1, p1, Landroid/content/pm/UserInfo;->id:I

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    return-void

    :cond_1
    iget-object v0, v2, Lcom/android/settings/accounts/MiuiAccountSettings$ProfileData;->aAa:Landroid/preference/PreferenceGroup;

    const v3, 0x7f1203eb

    invoke-virtual {v0, v3}, Landroid/preference/PreferenceGroup;->setTitle(I)V

    iget-object v0, v2, Lcom/android/settings/accounts/MiuiAccountSettings$ProfileData;->aAa:Landroid/preference/PreferenceGroup;

    check-cast v0, Lcom/android/settings/cA;

    const v3, 0x7f120042

    invoke-virtual {p0, v3}, Lcom/android/settings/accounts/MiuiAccountSettings;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/android/settings/cA;->bTs(Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    const-string/jumbo v0, "account_other"

    invoke-virtual {p3, v0}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceCategory;

    iput-object v0, v2, Lcom/android/settings/accounts/MiuiAccountSettings$ProfileData;->aAa:Landroid/preference/PreferenceGroup;

    goto :goto_1
.end method

.method private aot()V
    .locals 7

    const/4 v3, 0x0

    const/4 v2, 0x1

    const/4 v5, 0x0

    invoke-static {}, Landroid/os/Process;->myUserHandle()Landroid/os/UserHandle;

    move-result-object v4

    const-string/jumbo v0, "wifi_only"

    invoke-virtual {p0, v0}, Lcom/android/settings/accounts/MiuiAccountSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    iput-object v0, p0, Lcom/android/settings/accounts/MiuiAccountSettings;->azU:Landroid/preference/CheckBoxPreference;

    iget-object v0, p0, Lcom/android/settings/accounts/MiuiAccountSettings;->azL:Landroid/app/Activity;

    invoke-static {v0}, Lcom/android/settings/aq;->bqv(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/android/settings/accounts/MiuiAccountSettings;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/accounts/MiuiAccountSettings;->azU:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    iput-object v5, p0, Lcom/android/settings/accounts/MiuiAccountSettings;->azU:Landroid/preference/CheckBoxPreference;

    :cond_0
    const-string/jumbo v0, "account_sync"

    invoke-virtual {p0, v0}, Lcom/android/settings/accounts/MiuiAccountSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceCategory;

    iget-object v1, p0, Lcom/android/settings/accounts/MiuiAccountSettings;->azS:Landroid/util/SparseArray;

    invoke-virtual {v1}, Landroid/util/SparseArray;->size()I

    move-result v1

    if-ne v1, v2, :cond_5

    const-string/jumbo v1, "account_settings_menu_auto_sync"

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceCategory;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    check-cast v1, Lcom/android/settings/widget/TogglePreference;

    new-instance v5, Lcom/android/settings/accounts/MiuiAccountSettings$MasterSyncStateClickListener;

    invoke-direct {v5, p0, v4}, Lcom/android/settings/accounts/MiuiAccountSettings$MasterSyncStateClickListener;-><init>(Lcom/android/settings/accounts/MiuiAccountSettings;Landroid/os/UserHandle;)V

    invoke-virtual {v1, v5}, Lcom/android/settings/widget/TogglePreference;->aCY(Lcom/android/settings/widget/M;)V

    invoke-virtual {v4}, Landroid/os/UserHandle;->getIdentifier()I

    move-result v4

    invoke-static {v4}, Landroid/content/ContentResolver;->getMasterSyncAutomaticallyAsUser(I)Z

    move-result v4

    invoke-virtual {v1, v4}, Lcom/android/settings/widget/TogglePreference;->setCheckedInternal(Z)V

    const-string/jumbo v1, "account_settings_menu_auto_sync_personal"

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceCategory;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceCategory;->removePreference(Landroid/preference/Preference;)Z

    :cond_1
    const-string/jumbo v1, "account_settings_menu_auto_sync_work"

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceCategory;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    if-eqz v1, :cond_2

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceCategory;->removePreference(Landroid/preference/Preference;)Z

    :cond_2
    iget-object v0, p0, Lcom/android/settings/accounts/MiuiAccountSettings;->azU:Landroid/preference/CheckBoxPreference;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/android/settings/accounts/MiuiAccountSettings;->azU:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, v4}, Landroid/preference/CheckBoxPreference;->setEnabled(Z)V

    :cond_3
    :goto_0
    iget-object v0, p0, Lcom/android/settings/accounts/MiuiAccountSettings;->azU:Landroid/preference/CheckBoxPreference;

    if-eqz v0, :cond_4

    iget-object v1, p0, Lcom/android/settings/accounts/MiuiAccountSettings;->azU:Landroid/preference/CheckBoxPreference;

    invoke-virtual {p0}, Lcom/android/settings/accounts/MiuiAccountSettings;->bWB()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v4, "sync_on_wifi_only"

    invoke-static {v0, v4, v3}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    if-ne v0, v2, :cond_8

    move v0, v2

    :goto_1
    invoke-virtual {v1, v0}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    :cond_4
    return-void

    :cond_5
    iget-object v1, p0, Lcom/android/settings/accounts/MiuiAccountSettings;->azS:Landroid/util/SparseArray;

    invoke-virtual {v1}, Landroid/util/SparseArray;->size()I

    move-result v1

    if-le v1, v2, :cond_3

    iget-object v1, p0, Lcom/android/settings/accounts/MiuiAccountSettings;->azS:Landroid/util/SparseArray;

    invoke-virtual {v1, v2}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/settings/accounts/MiuiAccountSettings$ProfileData;

    iget-object v1, v1, Lcom/android/settings/accounts/MiuiAccountSettings$ProfileData;->aAc:Landroid/content/pm/UserInfo;

    invoke-virtual {v1}, Landroid/content/pm/UserInfo;->getUserHandle()Landroid/os/UserHandle;

    move-result-object v5

    const-string/jumbo v1, "account_settings_menu_auto_sync_personal"

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceCategory;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    check-cast v1, Lcom/android/settings/widget/TogglePreference;

    new-instance v6, Lcom/android/settings/accounts/MiuiAccountSettings$MasterSyncStateClickListener;

    invoke-direct {v6, p0, v4}, Lcom/android/settings/accounts/MiuiAccountSettings$MasterSyncStateClickListener;-><init>(Lcom/android/settings/accounts/MiuiAccountSettings;Landroid/os/UserHandle;)V

    invoke-virtual {v1, v6}, Lcom/android/settings/widget/TogglePreference;->aCY(Lcom/android/settings/widget/M;)V

    invoke-virtual {v4}, Landroid/os/UserHandle;->getIdentifier()I

    move-result v4

    invoke-static {v4}, Landroid/content/ContentResolver;->getMasterSyncAutomaticallyAsUser(I)Z

    move-result v4

    invoke-virtual {v1, v4}, Lcom/android/settings/widget/TogglePreference;->setCheckedInternal(Z)V

    const-string/jumbo v1, "account_settings_menu_auto_sync_work"

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceCategory;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    check-cast v1, Lcom/android/settings/widget/TogglePreference;

    new-instance v6, Lcom/android/settings/accounts/MiuiAccountSettings$MasterSyncStateClickListener;

    invoke-direct {v6, p0, v5}, Lcom/android/settings/accounts/MiuiAccountSettings$MasterSyncStateClickListener;-><init>(Lcom/android/settings/accounts/MiuiAccountSettings;Landroid/os/UserHandle;)V

    invoke-virtual {v1, v6}, Lcom/android/settings/widget/TogglePreference;->aCY(Lcom/android/settings/widget/M;)V

    invoke-virtual {v5}, Landroid/os/UserHandle;->getIdentifier()I

    move-result v5

    invoke-static {v5}, Landroid/content/ContentResolver;->getMasterSyncAutomaticallyAsUser(I)Z

    move-result v5

    invoke-virtual {v1, v5}, Lcom/android/settings/widget/TogglePreference;->setCheckedInternal(Z)V

    const-string/jumbo v1, "account_settings_menu_auto_sync"

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceCategory;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    if-eqz v1, :cond_6

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceCategory;->removePreference(Landroid/preference/Preference;)Z

    :cond_6
    const-string/jumbo v0, "account_other"

    invoke-virtual {p0, v0}, Lcom/android/settings/accounts/MiuiAccountSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    if-eqz v1, :cond_7

    move-object v0, v1

    check-cast v0, Landroid/preference/PreferenceCategory;

    invoke-virtual {v0}, Landroid/preference/PreferenceCategory;->getPreferenceCount()I

    move-result v0

    if-nez v0, :cond_7

    invoke-virtual {p0}, Lcom/android/settings/accounts/MiuiAccountSettings;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    :cond_7
    iget-object v0, p0, Lcom/android/settings/accounts/MiuiAccountSettings;->azU:Landroid/preference/CheckBoxPreference;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/android/settings/accounts/MiuiAccountSettings;->azU:Landroid/preference/CheckBoxPreference;

    or-int v1, v4, v5

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setEnabled(Z)V

    goto/16 :goto_0

    :cond_8
    move v0, v3

    goto/16 :goto_1
.end method

.method static synthetic aou(Lcom/android/settings/accounts/MiuiAccountSettings;)Lmiui/preference/ValuePreference;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/accounts/MiuiAccountSettings;->azO:Lmiui/preference/ValuePreference;

    return-object v0
.end method

.method static synthetic aov(Lcom/android/settings/accounts/MiuiAccountSettings;)Landroid/os/UserManager;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/accounts/MiuiAccountSettings;->azW:Landroid/os/UserManager;

    return-object v0
.end method

.method static synthetic aow(Lcom/android/settings/accounts/MiuiAccountSettings;Landroid/app/AlertDialog;)Landroid/app/AlertDialog;
    .locals 0

    iput-object p1, p0, Lcom/android/settings/accounts/MiuiAccountSettings;->azP:Landroid/app/AlertDialog;

    return-object p1
.end method

.method static synthetic aox(Lcom/android/settings/accounts/MiuiAccountSettings;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/accounts/MiuiAccountSettings;->aoa()V

    return-void
.end method

.method static synthetic aoy(Lcom/android/settings/accounts/MiuiAccountSettings;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/accounts/MiuiAccountSettings;->aod()V

    return-void
.end method

.method static synthetic aoz(Lcom/android/settings/accounts/MiuiAccountSettings;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/settings/accounts/MiuiAccountSettings;->aoj(Z)V

    return-void
.end method


# virtual methods
.method public ame(Landroid/os/UserHandle;)V
    .locals 3

    iget-object v0, p0, Lcom/android/settings/accounts/MiuiAccountSettings;->azS:Landroid/util/SparseArray;

    invoke-virtual {p1}, Landroid/os/UserHandle;->getIdentifier()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settings/accounts/MiuiAccountSettings$ProfileData;

    if-eqz v0, :cond_0

    invoke-direct {p0, v0}, Lcom/android/settings/accounts/MiuiAccountSettings;->aor(Lcom/android/settings/accounts/MiuiAccountSettings$ProfileData;)V

    :goto_0
    return-void

    :cond_0
    const-string/jumbo v0, "AccountSettings"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Missing Settings screen for: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Landroid/os/UserHandle;->getIdentifier()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method protected aoh()V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/accounts/MiuiAccountSettings;->azL:Landroid/app/Activity;

    if-nez v0, :cond_0

    return-void

    :cond_0
    invoke-static {}, Landroid/content/ContentResolver;->getCurrentSyncs()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    iget-boolean v1, p0, Lcom/android/settings/accounts/MiuiAccountSettings;->azV:Z

    if-eq v1, v0, :cond_1

    iput-boolean v0, p0, Lcom/android/settings/accounts/MiuiAccountSettings;->azV:Z

    :cond_1
    invoke-virtual {p0}, Lcom/android/settings/accounts/MiuiAccountSettings;->invalidateOptionsMenu()V

    return-void
.end method

.method public cC()Ljava/lang/String;
    .locals 1

    const-class v0, Lcom/android/settings/accounts/MiuiAccountSettings;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getMetricsCategory()I
    .locals 1

    const/16 v0, 0x8

    return v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->onCreate(Landroid/os/Bundle;)V

    const-string/jumbo v0, "user"

    invoke-virtual {p0, v0}, Lcom/android/settings/accounts/MiuiAccountSettings;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/UserManager;

    iput-object v0, p0, Lcom/android/settings/accounts/MiuiAccountSettings;->azW:Landroid/os/UserManager;

    new-instance v0, Landroid/preference/Preference;

    invoke-virtual {p0}, Lcom/android/settings/accounts/MiuiAccountSettings;->bWz()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/preference/Preference;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/settings/accounts/MiuiAccountSettings;->azR:Landroid/preference/Preference;

    invoke-virtual {p0}, Lcom/android/settings/accounts/MiuiAccountSettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "authorities"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringArrayExtra(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/accounts/MiuiAccountSettings;->azM:[Ljava/lang/String;

    iget-object v0, p0, Lcom/android/settings/accounts/MiuiAccountSettings;->azM:[Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/accounts/MiuiAccountSettings;->azM:[Ljava/lang/String;

    array-length v0, v0

    iput v0, p0, Lcom/android/settings/accounts/MiuiAccountSettings;->azN:I

    :cond_0
    invoke-virtual {p0}, Lcom/android/settings/accounts/MiuiAccountSettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/accounts/MiuiAccountSettings;->azL:Landroid/app/Activity;

    new-instance v0, Lcom/android/settings/accounts/MiuiAccountSettings$SyncDrawable;

    iget-object v1, p0, Lcom/android/settings/accounts/MiuiAccountSettings;->azL:Landroid/app/Activity;

    invoke-direct {v0, v1}, Lcom/android/settings/accounts/MiuiAccountSettings$SyncDrawable;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/settings/accounts/MiuiAccountSettings;->azT:Lcom/android/settings/accounts/MiuiAccountSettings$SyncDrawable;

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/android/settings/accounts/MiuiAccountSettings;->setHasOptionsMenu(Z)V

    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V
    .locals 3

    const/4 v2, 0x0

    const/4 v0, 0x1

    const v1, 0x7f121222

    invoke-interface {p1, v2, v0, v2, v1}, Landroid/view/Menu;->add(IIII)Landroid/view/MenuItem;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/accounts/MiuiAccountSettings;->azT:Lcom/android/settings/accounts/MiuiAccountSettings$SyncDrawable;

    invoke-virtual {v1}, Lcom/android/settings/accounts/MiuiAccountSettings$SyncDrawable;->bmQ()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/MenuItem;

    move-result-object v0

    const/4 v1, 0x5

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setShowAsAction(I)V

    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 4

    const/4 v3, 0x1

    const/4 v2, 0x0

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    invoke-super {p0, p1}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    return v0

    :pswitch_0
    iget-boolean v0, p0, Lcom/android/settings/accounts/MiuiAccountSettings;->azV:Z

    if-eqz v0, :cond_0

    invoke-static {v2, v2}, Landroid/content/ContentResolver;->cancelSync(Landroid/accounts/Account;Ljava/lang/String;)V

    :goto_0
    return v3

    :cond_0
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    invoke-static {}, Landroid/content/ContentResolver;->getMasterSyncAutomatically()Z

    move-result v1

    if-nez v1, :cond_1

    const-string/jumbo v1, "force"

    invoke-virtual {v0, v1, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    :cond_1
    invoke-static {v2, v2, v0}, Landroid/content/ContentResolver;->requestSync(Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public onPause()V
    .locals 2

    invoke-super {p0}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->onPause()V

    invoke-direct {p0}, Lcom/android/settings/accounts/MiuiAccountSettings;->aol()V

    iget-object v0, p0, Lcom/android/settings/accounts/MiuiAccountSettings;->azQ:Lcom/android/settings/accounts/MiuiAccountSettings$ManagedProfileBroadcastReceiver;

    invoke-virtual {p0}, Lcom/android/settings/accounts/MiuiAccountSettings;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/settings/accounts/MiuiAccountSettings$ManagedProfileBroadcastReceiver;->unregister(Landroid/content/Context;)V

    iget-object v0, p0, Lcom/android/settings/accounts/MiuiAccountSettings;->azP:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/accounts/MiuiAccountSettings;->azP:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->hide()V

    :cond_0
    return-void
.end method

.method public onPreferenceClick(Landroid/preference/Preference;)Z
    .locals 9

    const/4 v5, 0x0

    const/4 v8, 0x1

    const/4 v7, 0x0

    iget-object v0, p0, Lcom/android/settings/accounts/MiuiAccountSettings;->azS:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v2

    move v1, v7

    :goto_0
    if-ge v1, v2, :cond_3

    iget-object v0, p0, Lcom/android/settings/accounts/MiuiAccountSettings;->azS:Landroid/util/SparseArray;

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settings/accounts/MiuiAccountSettings$ProfileData;

    iget-object v3, v0, Lcom/android/settings/accounts/MiuiAccountSettings$ProfileData;->azX:Lcom/android/settings/DimmableIconPreference;

    if-ne p1, v3, :cond_0

    new-instance v1, Landroid/content/Intent;

    const-string/jumbo v2, "android.settings.ADD_ACCOUNT_SETTINGS"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string/jumbo v2, "android.intent.extra.USER"

    iget-object v0, v0, Lcom/android/settings/accounts/MiuiAccountSettings$ProfileData;->aAc:Landroid/content/pm/UserInfo;

    invoke-virtual {v0}, Landroid/content/pm/UserInfo;->getUserHandle()Landroid/os/UserHandle;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string/jumbo v0, "authorities"

    iget-object v2, p0, Lcom/android/settings/accounts/MiuiAccountSettings;->azM:[Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p0, v1}, Lcom/android/settings/accounts/MiuiAccountSettings;->startActivity(Landroid/content/Intent;)V

    return v8

    :cond_0
    iget-object v3, v0, Lcom/android/settings/accounts/MiuiAccountSettings$ProfileData;->aAb:Landroid/preference/Preference;

    if-ne p1, v3, :cond_1

    iget-object v0, v0, Lcom/android/settings/accounts/MiuiAccountSettings$ProfileData;->aAc:Landroid/content/pm/UserInfo;

    iget v0, v0, Landroid/content/pm/UserInfo;->id:I

    invoke-virtual {p0}, Lcom/android/settings/accounts/MiuiAccountSettings;->getActivity()Landroid/app/Activity;

    move-result-object v1

    new-instance v2, Lcom/android/settings/accounts/MiuiAccountSettings$2;

    invoke-direct {v2, p0, v0}, Lcom/android/settings/accounts/MiuiAccountSettings$2;-><init>(Lcom/android/settings/accounts/MiuiAccountSettings;I)V

    invoke-static {v1, v0, v2}, Lcom/android/settings/users/UserDialogs;->aSL(Landroid/content/Context;ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/Dialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    return v8

    :cond_1
    iget-object v3, v0, Lcom/android/settings/accounts/MiuiAccountSettings$ProfileData;->azZ:Landroid/preference/Preference;

    if-ne p1, v3, :cond_2

    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    const-string/jumbo v1, "android.intent.extra.USER"

    iget-object v0, v0, Lcom/android/settings/accounts/MiuiAccountSettings$ProfileData;->aAc:Landroid/content/pm/UserInfo;

    invoke-virtual {v0}, Landroid/content/pm/UserInfo;->getUserHandle()Landroid/os/UserHandle;

    move-result-object v0

    invoke-virtual {v3, v1, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    invoke-virtual {p0}, Lcom/android/settings/accounts/MiuiAccountSettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/android/settings/bL;

    const-class v1, Lcom/android/settings/accounts/ManagedProfileSettings;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    const v4, 0x7f120a1d

    move-object v1, p0

    move-object v6, v5

    invoke-virtual/range {v0 .. v7}, Lcom/android/settings/bL;->Uv(Landroid/app/Fragment;Ljava/lang/String;Landroid/os/Bundle;ILjava/lang/CharSequence;Landroid/app/Fragment;I)V

    return v8

    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_3
    return v7
.end method

.method public onPreferenceTreeClick(Landroid/preference/PreferenceScreen;Landroid/preference/Preference;)Z
    .locals 5

    const/4 v1, 0x0

    const-string/jumbo v0, "wifi_only"

    invoke-virtual {p2}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    check-cast p2, Landroid/preference/CheckBoxPreference;

    invoke-virtual {p2}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v2

    invoke-virtual {p0}, Lcom/android/settings/accounts/MiuiAccountSettings;->bWB()Landroid/content/ContentResolver;

    move-result-object v3

    const-string/jumbo v4, "sync_on_wifi_only"

    if-eqz v2, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v3, v4, v0}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    invoke-direct {p0, v2}, Lcom/android/settings/accounts/MiuiAccountSettings;->aop(Z)V

    return v1

    :cond_0
    move v0, v1

    goto :goto_0

    :cond_1
    const-string/jumbo v0, "xiaomi_cloud_service"

    invoke-virtual {p2}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string/jumbo v0, "account_list"

    const-string/jumbo v1, "account_list_to_cloud_service"

    invoke-static {v0, v1}, Lcom/android/settings/E;->blF(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v1, "com.xiaomi.action.MICLOUD_MAIN"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string/jumbo v1, "com.miui.cloudservice"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p2, v0}, Landroid/preference/Preference;->setIntent(Landroid/content/Intent;)V

    :cond_2
    invoke-super {p0, p1, p2}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->onPreferenceTreeClick(Landroid/preference/PreferenceScreen;Landroid/preference/Preference;)Z

    move-result v0

    return v0
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)V
    .locals 4

    const v3, 0x7f121222

    const/4 v2, 0x1

    invoke-interface {p1, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/android/settings/accounts/MiuiAccountSettings;->aon()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    iget-boolean v1, p0, Lcom/android/settings/accounts/MiuiAccountSettings;->azV:Z

    if-eqz v1, :cond_1

    const v1, 0x7f12121e

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setTitle(I)Landroid/view/MenuItem;

    invoke-direct {p0, v0}, Lcom/android/settings/accounts/MiuiAccountSettings;->aoi(Landroid/view/MenuItem;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-interface {v0, v3}, Landroid/view/MenuItem;->setTitle(I)Landroid/view/MenuItem;

    invoke-direct {p0, v0}, Lcom/android/settings/accounts/MiuiAccountSettings;->aok(Landroid/view/MenuItem;)V

    goto :goto_0

    :cond_2
    const/4 v1, 0x0

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    invoke-interface {v0, v3}, Landroid/view/MenuItem;->setTitle(I)Landroid/view/MenuItem;

    goto :goto_0
.end method

.method public onResume()V
    .locals 2

    invoke-super {p0}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->onResume()V

    invoke-direct {p0}, Lcom/android/settings/accounts/MiuiAccountSettings;->aoa()V

    invoke-virtual {p0}, Lcom/android/settings/accounts/MiuiAccountSettings;->updateUi()V

    iget-object v0, p0, Lcom/android/settings/accounts/MiuiAccountSettings;->azQ:Lcom/android/settings/accounts/MiuiAccountSettings$ManagedProfileBroadcastReceiver;

    invoke-virtual {p0}, Lcom/android/settings/accounts/MiuiAccountSettings;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/settings/accounts/MiuiAccountSettings$ManagedProfileBroadcastReceiver;->register(Landroid/content/Context;)V

    invoke-direct {p0}, Lcom/android/settings/accounts/MiuiAccountSettings;->aod()V

    iget-object v0, p0, Lcom/android/settings/accounts/MiuiAccountSettings;->azP:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/accounts/MiuiAccountSettings;->azP:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    :cond_0
    invoke-virtual {p0}, Lcom/android/settings/accounts/MiuiAccountSettings;->aoh()V

    return-void
.end method

.method updateUi()V
    .locals 7

    const/4 v1, 0x1

    const/4 v2, 0x0

    const v0, 0x7f15000b

    invoke-virtual {p0, v0}, Lcom/android/settings/accounts/MiuiAccountSettings;->addPreferencesFromResource(I)V

    const-string/jumbo v0, "xiaomi_cloud_service"

    invoke-virtual {p0, v0}, Lcom/android/settings/accounts/MiuiAccountSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Lmiui/preference/ValuePreference;

    iput-object v0, p0, Lcom/android/settings/accounts/MiuiAccountSettings;->azO:Lmiui/preference/ValuePreference;

    iget-object v0, p0, Lcom/android/settings/accounts/MiuiAccountSettings;->azO:Lmiui/preference/ValuePreference;

    invoke-virtual {v0, v1}, Lmiui/preference/ValuePreference;->setShowRightArrow(Z)V

    invoke-static {}, Landroid/app/AppGlobals;->getPackageManager()Landroid/content/pm/IPackageManager;

    move-result-object v0

    :try_start_0
    const-string/jumbo v3, "com.miui.cloudservice"

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-interface {v0, v3, v4, v5}, Landroid/content/pm/IPackageManager;->getApplicationInfo(Ljava/lang/String;II)Landroid/content/pm/ApplicationInfo;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v3, p0, Lcom/android/settings/accounts/MiuiAccountSettings;->azO:Lmiui/preference/ValuePreference;

    invoke-virtual {p0}, Lcom/android/settings/accounts/MiuiAccountSettings;->bWA()Landroid/content/pm/PackageManager;

    move-result-object v4

    invoke-virtual {v0, v4}, Landroid/content/pm/ApplicationInfo;->loadIcon(Landroid/content/pm/PackageManager;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v3, v0}, Lmiui/preference/ValuePreference;->setIcon(Landroid/graphics/drawable/Drawable;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/android/settings/accounts/MiuiAccountSettings;->azO:Lmiui/preference/ValuePreference;

    const v3, 0x7f0d014e

    invoke-virtual {v0, v3}, Lmiui/preference/ValuePreference;->setLayoutResource(I)V

    iget-object v0, p0, Lcom/android/settings/accounts/MiuiAccountSettings;->azW:Landroid/os/UserManager;

    invoke-virtual {v0}, Landroid/os/UserManager;->isManagedProfile()Z

    move-result v0

    if-eqz v0, :cond_1

    const-string/jumbo v0, "AccountSettings"

    const-string/jumbo v1, "We should not be showing settings for a managed profile"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lcom/android/settings/accounts/MiuiAccountSettings;->finish()V

    return-void

    :catch_0
    move-exception v0

    const-string/jumbo v3, "AccountSettings"

    const-string/jumbo v4, "RemoteException\uff1a"

    invoke-static {v3, v4, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lcom/android/settings/accounts/MiuiAccountSettings;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v4

    iget-object v0, p0, Lcom/android/settings/accounts/MiuiAccountSettings;->azW:Landroid/os/UserManager;

    invoke-virtual {v0}, Landroid/os/UserManager;->isLinkedUser()Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/android/settings/accounts/MiuiAccountSettings;->azW:Landroid/os/UserManager;

    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/os/UserManager;->getUserInfo(I)Landroid/content/pm/UserInfo;

    move-result-object v0

    invoke-direct {p0, v0, v2, v4}, Lcom/android/settings/accounts/MiuiAccountSettings;->aos(Landroid/content/pm/UserInfo;ZLandroid/preference/PreferenceScreen;)V

    :cond_2
    iget-object v0, p0, Lcom/android/settings/accounts/MiuiAccountSettings;->azS:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v1

    :goto_1
    if-ge v2, v1, :cond_6

    iget-object v0, p0, Lcom/android/settings/accounts/MiuiAccountSettings;->azS:Landroid/util/SparseArray;

    invoke-virtual {v0, v2}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settings/accounts/MiuiAccountSettings$ProfileData;

    iget-object v3, v0, Lcom/android/settings/accounts/MiuiAccountSettings$ProfileData;->aAa:Landroid/preference/PreferenceGroup;

    invoke-virtual {v3, v4}, Landroid/preference/PreferenceGroup;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_3

    iget-object v3, v0, Lcom/android/settings/accounts/MiuiAccountSettings$ProfileData;->aAa:Landroid/preference/PreferenceGroup;

    invoke-virtual {v4, v3}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    :cond_3
    invoke-direct {p0, v0}, Lcom/android/settings/accounts/MiuiAccountSettings;->aor(Lcom/android/settings/accounts/MiuiAccountSettings$ProfileData;)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_4
    iget-object v0, p0, Lcom/android/settings/accounts/MiuiAccountSettings;->azW:Landroid/os/UserManager;

    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v3

    invoke-virtual {v0, v3}, Landroid/os/UserManager;->getProfiles(I)Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v6

    if-le v6, v1, :cond_5

    :goto_2
    move v3, v2

    :goto_3
    if-ge v3, v6, :cond_2

    invoke-interface {v5, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/UserInfo;

    invoke-direct {p0, v0, v1, v4}, Lcom/android/settings/accounts/MiuiAccountSettings;->aos(Landroid/content/pm/UserInfo;ZLandroid/preference/PreferenceScreen;)V

    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_3

    :cond_5
    move v1, v2

    goto :goto_2

    :cond_6
    invoke-direct {p0}, Lcom/android/settings/accounts/MiuiAccountSettings;->aot()V

    return-void
.end method
