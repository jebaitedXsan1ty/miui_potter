.class public Lcom/android/settings/accounts/MiuiManageAccounts;
.super Lcom/android/settings/accounts/MiuiAccountPreferenceBase;
.source "MiuiManageAccounts.java"

# interfaces
.implements Lcom/android/settingslib/c/b;


# instance fields
.field private azC:[Ljava/lang/String;

.field private azD:Landroid/widget/TextView;

.field private azE:Landroid/accounts/Account;

.field private mAccountType:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/accounts/MiuiAccountPreferenceBase;-><init>()V

    return-void
.end method

.method private anO()V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/accounts/MiuiManageAccounts;->mAccountType:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/android/settings/accounts/MiuiManageAccounts;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/android/settings/accounts/MiuiManageAccounts;->amg(Ljava/lang/String;Landroid/preference/PreferenceScreen;)Landroid/preference/PreferenceScreen;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-direct {p0, v0}, Lcom/android/settings/accounts/MiuiManageAccounts;->anX(Landroid/preference/PreferenceGroup;)V

    :cond_0
    return-void
.end method

.method private anP(Landroid/content/pm/PackageManager;Landroid/content/Intent;)Z
    .locals 6

    const/4 v0, 0x1

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/android/settings/accounts/MiuiManageAccounts;->aAv:Lcom/android/settingslib/c/a;

    iget-object v3, p0, Lcom/android/settings/accounts/MiuiManageAccounts;->mAccountType:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/android/settingslib/c/a;->cge(Ljava/lang/String;)Landroid/accounts/AuthenticatorDescription;

    move-result-object v2

    iget-object v3, p0, Lcom/android/settings/accounts/MiuiManageAccounts;->aAu:Landroid/os/UserHandle;

    invoke-virtual {v3}, Landroid/os/UserHandle;->getIdentifier()I

    move-result v3

    invoke-virtual {p1, p2, v1, v3}, Landroid/content/pm/PackageManager;->resolveActivityAsUser(Landroid/content/Intent;II)Landroid/content/pm/ResolveInfo;

    move-result-object v3

    if-nez v3, :cond_0

    return v1

    :cond_0
    iget-object v3, v3, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v4, v3, Landroid/content/pm/ActivityInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    :try_start_0
    iget-boolean v5, v3, Landroid/content/pm/ActivityInfo;->exported:Z

    if-eqz v5, :cond_2

    iget-object v5, v3, Landroid/content/pm/ActivityInfo;->permission:Ljava/lang/String;

    if-nez v5, :cond_1

    return v0

    :cond_1
    iget-object v3, v3, Landroid/content/pm/ActivityInfo;->permission:Ljava/lang/String;

    iget-object v5, v2, Landroid/accounts/AuthenticatorDescription;->packageName:Ljava/lang/String;

    invoke-virtual {p1, v3, v5}, Landroid/content/pm/PackageManager;->checkPermission(Ljava/lang/String;Ljava/lang/String;)I

    move-result v3

    if-nez v3, :cond_2

    return v0

    :cond_2
    iget-object v2, v2, Landroid/accounts/AuthenticatorDescription;->packageName:Ljava/lang/String;

    const/4 v3, 0x0

    invoke-virtual {p1, v2, v3}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v2

    iget v3, v4, Landroid/content/pm/ApplicationInfo;->uid:I

    iget v2, v2, Landroid/content/pm/ApplicationInfo;->uid:I
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    if-ne v3, v2, :cond_3

    :goto_0
    return v0

    :cond_3
    move v0, v1

    goto :goto_0

    :catch_0
    move-exception v0

    const-string/jumbo v2, "AccountSettings"

    const-string/jumbo v3, "Intent considered unsafe due to exception."

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    return v1
.end method

.method private anQ(ILandroid/accounts/Account;Ljava/lang/String;)Z
    .locals 2

    const/4 v0, 0x0

    invoke-static {p2, p3, p1}, Landroid/content/ContentResolver;->getSyncAutomaticallyAsUser(Landroid/accounts/Account;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {p1}, Landroid/content/ContentResolver;->getMasterSyncAutomaticallyAsUser(I)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {p2, p3, p1}, Landroid/content/ContentResolver;->getIsSyncableAsUser(Landroid/accounts/Account;Ljava/lang/String;I)I

    move-result v1

    if-lez v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method private anR(Ljava/util/List;Landroid/accounts/Account;Ljava/lang/String;)Z
    .locals 5

    const/4 v2, 0x0

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v3

    move v1, v2

    :goto_0
    if-ge v1, v3, :cond_1

    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/SyncInfo;

    iget-object v4, v0, Landroid/content/SyncInfo;->account:Landroid/accounts/Account;

    invoke-virtual {v4, p2}, Landroid/accounts/Account;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    iget-object v0, v0, Landroid/content/SyncInfo;->authority:Ljava/lang/String;

    invoke-virtual {v0, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    return v0

    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    return v2
.end method

.method private anS(Z)V
    .locals 11

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/android/settings/accounts/MiuiManageAccounts;->aAu:Landroid/os/UserHandle;

    invoke-virtual {v0}, Landroid/os/UserHandle;->getIdentifier()I

    move-result v3

    invoke-static {v3}, Landroid/content/ContentResolver;->getSyncAdapterTypesAsUser(I)[Landroid/content/SyncAdapterType;

    move-result-object v4

    new-instance v5, Landroid/os/Bundle;

    invoke-direct {v5}, Landroid/os/Bundle;-><init>()V

    const-string/jumbo v0, "force"

    const/4 v2, 0x1

    invoke-virtual {v5, v0, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    invoke-virtual {p0}, Lcom/android/settings/accounts/MiuiManageAccounts;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    invoke-virtual {v0}, Landroid/preference/PreferenceScreen;->getPreferenceCount()I

    move-result v6

    move v2, v1

    :goto_0
    if-ge v2, v6, :cond_3

    invoke-virtual {p0}, Lcom/android/settings/accounts/MiuiManageAccounts;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/preference/PreferenceScreen;->getPreference(I)Landroid/preference/Preference;

    move-result-object v0

    instance-of v7, v0, Lcom/android/settings/accounts/AccountPreference;

    if-eqz v7, :cond_2

    check-cast v0, Lcom/android/settings/accounts/AccountPreference;

    invoke-virtual {v0}, Lcom/android/settings/accounts/AccountPreference;->ani()Landroid/accounts/Account;

    move-result-object v7

    move v0, v1

    :goto_1
    array-length v8, v4

    if-ge v0, v8, :cond_2

    aget-object v8, v4, v0

    aget-object v9, v4, v0

    iget-object v9, v9, Landroid/content/SyncAdapterType;->accountType:Ljava/lang/String;

    iget-object v10, p0, Lcom/android/settings/accounts/MiuiManageAccounts;->mAccountType:Ljava/lang/String;

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_0

    iget-object v9, v8, Landroid/content/SyncAdapterType;->authority:Ljava/lang/String;

    invoke-static {v7, v9, v3}, Landroid/content/ContentResolver;->getSyncAutomaticallyAsUser(Landroid/accounts/Account;Ljava/lang/String;I)Z

    move-result v9

    if-eqz v9, :cond_0

    if-eqz p1, :cond_1

    iget-object v8, v8, Landroid/content/SyncAdapterType;->authority:Ljava/lang/String;

    invoke-static {v7, v8, v3, v5}, Landroid/content/ContentResolver;->requestSyncAsUser(Landroid/accounts/Account;Ljava/lang/String;ILandroid/os/Bundle;)V

    :cond_0
    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_1
    iget-object v8, v8, Landroid/content/SyncAdapterType;->authority:Ljava/lang/String;

    invoke-static {v7, v8, v3}, Landroid/content/ContentResolver;->cancelSyncAsUser(Landroid/accounts/Account;Ljava/lang/String;I)V

    goto :goto_2

    :cond_2
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_3
    return-void
.end method

.method public static anT(Landroid/content/Context;Ljava/lang/String;)Z
    .locals 6

    const/4 v5, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f030084

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    array-length v0, v2

    if-nez v0, :cond_1

    :cond_0
    return v5

    :cond_1
    array-length v3, v2

    move v0, v1

    :goto_0
    if-ge v0, v3, :cond_3

    aget-object v4, v2, v0

    invoke-virtual {v4, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    return v1

    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_3
    return v5
.end method

.method private anU()V
    .locals 11

    const/4 v6, 0x1

    const/4 v5, 0x0

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/android/settings/accounts/MiuiManageAccounts;->getActivity()Landroid/app/Activity;

    move-result-object v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/android/settings/accounts/MiuiManageAccounts;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/accounts/MiuiManageAccounts;->aAu:Landroid/os/UserHandle;

    invoke-virtual {v1}, Landroid/os/UserHandle;->getIdentifier()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/accounts/AccountManager;->getAccountsAsUser(I)[Landroid/accounts/Account;

    move-result-object v8

    invoke-virtual {p0}, Lcom/android/settings/accounts/MiuiManageAccounts;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    invoke-virtual {v0}, Landroid/preference/PreferenceScreen;->removeAll()V

    iput-object v2, p0, Lcom/android/settings/accounts/MiuiManageAccounts;->azE:Landroid/accounts/Account;

    const v0, 0x7f15007c

    invoke-virtual {p0, v0}, Lcom/android/settings/accounts/MiuiManageAccounts;->addPreferencesFromResource(I)V

    array-length v9, v8

    move v7, v5

    :goto_0
    if-ge v7, v9, :cond_8

    aget-object v2, v8, v7

    invoke-virtual {p0}, Lcom/android/settings/accounts/MiuiManageAccounts;->getActivity()Landroid/app/Activity;

    move-result-object v0

    iget-object v1, v2, Landroid/accounts/Account;->type:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/android/settings/accounts/MiuiManageAccounts;->anT(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    :cond_1
    :goto_1
    add-int/lit8 v0, v7, 0x1

    move v7, v0

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/android/settings/accounts/MiuiManageAccounts;->mAccountType:Ljava/lang/String;

    if-eqz v0, :cond_3

    iget-object v0, v2, Landroid/accounts/Account;->type:Ljava/lang/String;

    iget-object v1, p0, Lcom/android/settings/accounts/MiuiManageAccounts;->mAccountType:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-nez v0, :cond_1

    :cond_3
    iget-object v0, v2, Landroid/accounts/Account;->type:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/android/settings/accounts/MiuiManageAccounts;->amf(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v4

    iget-object v0, p0, Lcom/android/settings/accounts/MiuiManageAccounts;->azC:[Ljava/lang/String;

    if-eqz v0, :cond_5

    if-eqz v4, :cond_5

    iget-object v1, p0, Lcom/android/settings/accounts/MiuiManageAccounts;->azC:[Ljava/lang/String;

    array-length v3, v1

    move v0, v5

    :goto_2
    if-ge v0, v3, :cond_a

    aget-object v10, v1, v0

    invoke-virtual {v4, v10}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_6

    move v0, v6

    :goto_3
    if-eqz v0, :cond_1

    iget-object v0, v2, Landroid/accounts/Account;->type:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/android/settings/accounts/MiuiManageAccounts;->aoN(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    const-string/jumbo v0, "com.xiaomi"

    iget-object v1, v2, Landroid/accounts/Account;->type:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    const-string/jumbo v0, "com.xiaomi.unactivated"

    iget-object v1, v2, Landroid/accounts/Account;->type:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    :cond_4
    new-instance v0, Lcom/android/settings/accounts/XiaomiAccountPreference;

    invoke-virtual {p0}, Lcom/android/settings/accounts/MiuiManageAccounts;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/android/settings/accounts/XiaomiAccountPreference;-><init>(Landroid/content/Context;Landroid/accounts/Account;Landroid/graphics/drawable/Drawable;Ljava/util/ArrayList;)V

    :goto_4
    invoke-virtual {p0}, Lcom/android/settings/accounts/MiuiManageAccounts;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    iget-object v0, p0, Lcom/android/settings/accounts/MiuiManageAccounts;->azE:Landroid/accounts/Account;

    if-nez v0, :cond_1

    iput-object v2, p0, Lcom/android/settings/accounts/MiuiManageAccounts;->azE:Landroid/accounts/Account;

    invoke-virtual {p0}, Lcom/android/settings/accounts/MiuiManageAccounts;->invalidateOptionsMenu()V

    goto :goto_1

    :cond_5
    move v0, v6

    goto :goto_3

    :cond_6
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_7
    new-instance v0, Lcom/android/settings/accounts/AccountPreference;

    invoke-virtual {p0}, Lcom/android/settings/accounts/MiuiManageAccounts;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-direct/range {v0 .. v5}, Lcom/android/settings/accounts/AccountPreference;-><init>(Landroid/content/Context;Landroid/accounts/Account;Landroid/graphics/drawable/Drawable;Ljava/util/ArrayList;Z)V

    goto :goto_4

    :cond_8
    iget-object v0, p0, Lcom/android/settings/accounts/MiuiManageAccounts;->mAccountType:Ljava/lang/String;

    if-eqz v0, :cond_9

    iget-object v0, p0, Lcom/android/settings/accounts/MiuiManageAccounts;->azE:Landroid/accounts/Account;

    if-eqz v0, :cond_9

    invoke-direct {p0}, Lcom/android/settings/accounts/MiuiManageAccounts;->anO()V

    :goto_5
    return-void

    :cond_9
    invoke-virtual {p0}, Lcom/android/settings/accounts/MiuiManageAccounts;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    goto :goto_5

    :cond_a
    move v0, v5

    goto :goto_3
.end method

.method private anV()V
    .locals 28

    invoke-virtual/range {p0 .. p0}, Lcom/android/settings/accounts/MiuiManageAccounts;->getActivity()Landroid/app/Activity;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual/range {p0 .. p0}, Lcom/android/settings/accounts/MiuiManageAccounts;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Activity;->isFinishing()Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    return-void

    :cond_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/settings/accounts/MiuiManageAccounts;->aAu:Landroid/os/UserHandle;

    invoke-virtual {v2}, Landroid/os/UserHandle;->getIdentifier()I

    move-result v12

    invoke-static {v12}, Landroid/content/ContentResolver;->getCurrentSyncsAsUser(I)Ljava/util/List;

    move-result-object v13

    const/4 v8, 0x0

    new-instance v14, Ljava/util/Date;

    invoke-direct {v14}, Ljava/util/Date;-><init>()V

    invoke-static {v12}, Landroid/content/ContentResolver;->getSyncAdapterTypesAsUser(I)[Landroid/content/SyncAdapterType;

    move-result-object v3

    new-instance v15, Ljava/util/HashSet;

    invoke-direct {v15}, Ljava/util/HashSet;-><init>()V

    const/4 v2, 0x0

    array-length v4, v3

    :goto_0
    if-ge v2, v4, :cond_3

    aget-object v5, v3, v2

    invoke-virtual {v5}, Landroid/content/SyncAdapterType;->isUserVisible()Z

    move-result v6

    if-eqz v6, :cond_2

    iget-object v5, v5, Landroid/content/SyncAdapterType;->authority:Ljava/lang/String;

    invoke-virtual {v15, v5}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_3
    const/4 v2, 0x0

    invoke-virtual/range {p0 .. p0}, Lcom/android/settings/accounts/MiuiManageAccounts;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v3

    invoke-virtual {v3}, Landroid/preference/PreferenceScreen;->getPreferenceCount()I

    move-result v16

    move v11, v2

    :goto_1
    move/from16 v0, v16

    if-ge v11, v0, :cond_11

    invoke-virtual/range {p0 .. p0}, Lcom/android/settings/accounts/MiuiManageAccounts;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v2

    invoke-virtual {v2, v11}, Landroid/preference/PreferenceScreen;->getPreference(I)Landroid/preference/Preference;

    move-result-object v2

    instance-of v3, v2, Lcom/android/settings/accounts/AccountPreference;

    if-nez v3, :cond_5

    :cond_4
    :goto_2
    add-int/lit8 v2, v11, 0x1

    move v11, v2

    goto :goto_1

    :cond_5
    check-cast v2, Lcom/android/settings/accounts/AccountPreference;

    invoke-virtual {v2}, Lcom/android/settings/accounts/AccountPreference;->ani()Landroid/accounts/Account;

    move-result-object v17

    const/4 v5, 0x0

    const-wide/16 v6, 0x0

    const/4 v4, 0x0

    invoke-virtual {v2}, Lcom/android/settings/accounts/AccountPreference;->anj()Ljava/util/ArrayList;

    move-result-object v9

    const/4 v3, 0x0

    if-eqz v9, :cond_b

    invoke-interface {v9}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v18

    move v9, v8

    move v8, v5

    move v5, v4

    move v4, v3

    :goto_3
    invoke-interface/range {v18 .. v18}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_13

    invoke-interface/range {v18 .. v18}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    move-object/from16 v0, v17

    invoke-static {v0, v3, v12}, Landroid/content/ContentResolver;->getSyncStatusAsUser(Landroid/accounts/Account;Ljava/lang/String;I)Landroid/content/SyncStatusInfo;

    move-result-object v19

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-direct {v0, v12, v1, v3}, Lcom/android/settings/accounts/MiuiManageAccounts;->anQ(ILandroid/accounts/Account;Ljava/lang/String;)Z

    move-result v20

    move-object/from16 v0, v17

    invoke-static {v0, v3}, Landroid/content/ContentResolver;->isSyncPending(Landroid/accounts/Account;Ljava/lang/String;)Z

    move-result v21

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-direct {v0, v13, v1, v3}, Lcom/android/settings/accounts/MiuiManageAccounts;->anR(Ljava/util/List;Landroid/accounts/Account;Ljava/lang/String;)Z

    move-result v22

    if-eqz v19, :cond_9

    if-eqz v20, :cond_9

    move-object/from16 v0, v19

    iget-wide v0, v0, Landroid/content/SyncStatusInfo;->lastFailureTime:J

    move-wide/from16 v24, v0

    const-wide/16 v26, 0x0

    cmp-long v10, v24, v26

    if-eqz v10, :cond_9

    const/4 v10, 0x0

    move-object/from16 v0, v19

    invoke-virtual {v0, v10}, Landroid/content/SyncStatusInfo;->getLastFailureMesgAsInt(I)I

    move-result v10

    const/16 v23, 0x1

    move/from16 v0, v23

    if-eq v10, v0, :cond_8

    const/4 v10, 0x1

    :goto_4
    if-eqz v10, :cond_6

    xor-int/lit8 v10, v22, 0x1

    if-eqz v10, :cond_6

    xor-int/lit8 v10, v21, 0x1

    if-eqz v10, :cond_6

    const/4 v5, 0x1

    const/4 v9, 0x1

    :cond_6
    or-int v4, v4, v22

    if-eqz v19, :cond_7

    move-object/from16 v0, v19

    iget-wide v0, v0, Landroid/content/SyncStatusInfo;->lastSuccessTime:J

    move-wide/from16 v22, v0

    cmp-long v10, v6, v22

    if-gez v10, :cond_7

    move-object/from16 v0, v19

    iget-wide v6, v0, Landroid/content/SyncStatusInfo;->lastSuccessTime:J

    :cond_7
    if-eqz v20, :cond_a

    invoke-virtual {v15, v3}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_a

    const/4 v3, 0x1

    :goto_5
    add-int/2addr v3, v8

    move v8, v3

    goto :goto_3

    :cond_8
    const/4 v10, 0x0

    goto :goto_4

    :cond_9
    const/4 v10, 0x0

    goto :goto_4

    :cond_a
    const/4 v3, 0x0

    goto :goto_5

    :cond_b
    const-string/jumbo v9, "AccountSettings"

    const/4 v10, 0x2

    invoke-static {v9, v10}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v9

    if-eqz v9, :cond_c

    const-string/jumbo v9, "AccountSettings"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v18, "no syncadapters found for "

    move-object/from16 v0, v18

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    move-object/from16 v0, v17

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_c
    :goto_6
    if-eqz v4, :cond_d

    const/4 v3, 0x2

    const/4 v4, 0x1

    invoke-virtual {v2, v3, v4}, Lcom/android/settings/accounts/AccountPreference;->ann(IZ)V

    goto/16 :goto_2

    :cond_d
    if-nez v5, :cond_e

    const/4 v3, 0x1

    const/4 v4, 0x1

    invoke-virtual {v2, v3, v4}, Lcom/android/settings/accounts/AccountPreference;->ann(IZ)V

    goto/16 :goto_2

    :cond_e
    if-lez v5, :cond_10

    if-eqz v3, :cond_f

    const/4 v3, 0x3

    const/4 v4, 0x1

    invoke-virtual {v2, v3, v4}, Lcom/android/settings/accounts/AccountPreference;->ann(IZ)V

    goto/16 :goto_2

    :cond_f
    const/4 v3, 0x0

    const/4 v4, 0x1

    invoke-virtual {v2, v3, v4}, Lcom/android/settings/accounts/AccountPreference;->ann(IZ)V

    const-wide/16 v4, 0x0

    cmp-long v3, v6, v4

    if-lez v3, :cond_4

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Lcom/android/settings/accounts/AccountPreference;->ann(IZ)V

    invoke-virtual {v14, v6, v7}, Ljava/util/Date;->setTime(J)V

    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Lcom/android/settings/accounts/MiuiManageAccounts;->aoM(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual/range {p0 .. p0}, Lcom/android/settings/accounts/MiuiManageAccounts;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object v3, v5, v6

    const v3, 0x7f1208eb

    invoke-virtual {v4, v3, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/android/settings/accounts/AccountPreference;->setSummary(Ljava/lang/CharSequence;)V

    goto/16 :goto_2

    :cond_10
    const/4 v3, 0x1

    const/4 v4, 0x1

    invoke-virtual {v2, v3, v4}, Lcom/android/settings/accounts/AccountPreference;->ann(IZ)V

    goto/16 :goto_2

    :cond_11
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/settings/accounts/MiuiManageAccounts;->azD:Landroid/widget/TextView;

    if-eqz v8, :cond_12

    const/4 v2, 0x0

    :goto_7
    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setVisibility(I)V

    invoke-virtual/range {p0 .. p0}, Lcom/android/settings/accounts/MiuiManageAccounts;->invalidateOptionsMenu()V

    return-void

    :cond_12
    const/16 v2, 0x8

    goto :goto_7

    :cond_13
    move v3, v4

    move v4, v5

    move v5, v8

    move v8, v9

    goto :goto_6
.end method

.method private anW(Lcom/android/settings/accounts/AccountPreference;)V
    .locals 7

    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    const-string/jumbo v0, "account"

    invoke-virtual {p1}, Lcom/android/settings/accounts/AccountPreference;->ani()Landroid/accounts/Account;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    const-string/jumbo v0, "android.intent.extra.USER"

    iget-object v1, p0, Lcom/android/settings/accounts/MiuiManageAccounts;->aAu:Landroid/os/UserHandle;

    invoke-virtual {v2, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    const-string/jumbo v0, " TC "

    const-string/jumbo v1, " startAccountSettings"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lcom/android/settings/accounts/MiuiManageAccounts;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const-class v1, Lcom/android/settings/accounts/AccountSyncSettings;

    invoke-virtual {v1}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/android/settings/accounts/AccountPreference;->ani()Landroid/accounts/Account;

    move-result-object v3

    iget-object v4, v3, Landroid/accounts/Account;->name:Ljava/lang/String;

    const v3, 0x7f120089

    const/4 v6, 0x1

    move-object v5, p0

    invoke-static/range {v0 .. v6}, Lcom/android/settings/dc;->bYv(Landroid/app/Activity;Ljava/lang/String;Landroid/os/Bundle;ILjava/lang/CharSequence;Landroid/app/Fragment;I)V

    return-void
.end method

.method private anX(Landroid/preference/PreferenceGroup;)V
    .locals 6

    invoke-virtual {p0}, Lcom/android/settings/accounts/MiuiManageAccounts;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    const/4 v0, 0x0

    move v2, v0

    :goto_0
    invoke-virtual {p1}, Landroid/preference/PreferenceGroup;->getPreferenceCount()I

    move-result v0

    if-ge v2, v0, :cond_4

    invoke-virtual {p1, v2}, Landroid/preference/PreferenceGroup;->getPreference(I)Landroid/preference/Preference;

    move-result-object v1

    instance-of v0, v1, Landroid/preference/PreferenceGroup;

    if-eqz v0, :cond_0

    move-object v0, v1

    check-cast v0, Landroid/preference/PreferenceGroup;

    invoke-direct {p0, v0}, Lcom/android/settings/accounts/MiuiManageAccounts;->anX(Landroid/preference/PreferenceGroup;)V

    :cond_0
    invoke-virtual {v1}, Landroid/preference/Preference;->getIntent()Landroid/content/Intent;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_2

    invoke-virtual {v0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v4

    const-string/jumbo v5, "android.settings.LOCATION_SOURCE_SETTINGS"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    new-instance v0, Lcom/android/settings/accounts/MiuiManageAccounts$FragmentStarter;

    const-class v4, Lcom/android/settings/location/MiuiLocationSettings;

    invoke-virtual {v4}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v4

    const v5, 0x7f12095e

    invoke-direct {v0, p0, v4, v5}, Lcom/android/settings/accounts/MiuiManageAccounts$FragmentStarter;-><init>(Lcom/android/settings/accounts/MiuiManageAccounts;Ljava/lang/String;I)V

    invoke-virtual {v1, v0}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    :cond_1
    :goto_1
    add-int/lit8 v0, v2, 0x1

    :goto_2
    move v2, v0

    goto :goto_0

    :cond_2
    iget-object v4, p0, Lcom/android/settings/accounts/MiuiManageAccounts;->aAu:Landroid/os/UserHandle;

    invoke-virtual {v4}, Landroid/os/UserHandle;->getIdentifier()I

    move-result v4

    const/high16 v5, 0x10000

    invoke-virtual {v3, v0, v5, v4}, Landroid/content/pm/PackageManager;->resolveActivityAsUser(Landroid/content/Intent;II)Landroid/content/pm/ResolveInfo;

    move-result-object v4

    if-nez v4, :cond_3

    invoke-virtual {p1, v1}, Landroid/preference/PreferenceGroup;->removePreference(Landroid/preference/Preference;)Z

    move v0, v2

    goto :goto_2

    :cond_3
    const-string/jumbo v4, "account"

    iget-object v5, p0, Lcom/android/settings/accounts/MiuiManageAccounts;->azE:Landroid/accounts/Account;

    invoke-virtual {v0, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    invoke-virtual {v0}, Landroid/content/Intent;->getFlags()I

    move-result v4

    const/high16 v5, 0x10000000

    or-int/2addr v4, v5

    invoke-virtual {v0, v4}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    new-instance v0, Lcom/android/settings/accounts/MiuiManageAccounts$1;

    invoke-direct {v0, p0, v3}, Lcom/android/settings/accounts/MiuiManageAccounts$1;-><init>(Lcom/android/settings/accounts/MiuiManageAccounts;Landroid/content/pm/PackageManager;)V

    invoke-virtual {v1, v0}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    goto :goto_1

    :cond_4
    return-void
.end method

.method static synthetic anY(Lcom/android/settings/accounts/MiuiManageAccounts;Landroid/content/pm/PackageManager;Landroid/content/Intent;)Z
    .locals 1

    invoke-direct {p0, p1, p2}, Lcom/android/settings/accounts/MiuiManageAccounts;->anP(Landroid/content/pm/PackageManager;Landroid/content/Intent;)Z

    move-result v0

    return v0
.end method


# virtual methods
.method protected alT()V
    .locals 1

    invoke-direct {p0}, Lcom/android/settings/accounts/MiuiManageAccounts;->anV()V

    invoke-virtual {p0}, Lcom/android/settings/accounts/MiuiManageAccounts;->getActivity()Landroid/app/Activity;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/app/Activity;->invalidateOptionsMenu()V

    :cond_0
    return-void
.end method

.method protected amG()V
    .locals 3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    invoke-virtual {p0}, Lcom/android/settings/accounts/MiuiManageAccounts;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    invoke-virtual {v0}, Landroid/preference/PreferenceScreen;->getPreferenceCount()I

    move-result v0

    if-ge v1, v0, :cond_1

    invoke-virtual {p0}, Lcom/android/settings/accounts/MiuiManageAccounts;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->getPreference(I)Landroid/preference/Preference;

    move-result-object v0

    instance-of v2, v0, Lcom/android/settings/accounts/AccountPreference;

    if-eqz v2, :cond_0

    check-cast v0, Lcom/android/settings/accounts/AccountPreference;

    invoke-virtual {v0}, Lcom/android/settings/accounts/AccountPreference;->ani()Landroid/accounts/Account;

    move-result-object v2

    iget-object v2, v2, Landroid/accounts/Account;->type:Ljava/lang/String;

    invoke-virtual {p0, v2}, Lcom/android/settings/accounts/MiuiManageAccounts;->aoO(Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/android/settings/accounts/AccountPreference;->setSummary(Ljava/lang/CharSequence;)V

    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    return-void
.end method

.method public bridge synthetic amd()V
    .locals 0

    invoke-super {p0}, Lcom/android/settings/accounts/MiuiAccountPreferenceBase;->amd()V

    return-void
.end method

.method public ame(Landroid/os/UserHandle;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/accounts/MiuiManageAccounts;->anU()V

    invoke-virtual {p0}, Lcom/android/settings/accounts/MiuiManageAccounts;->alT()V

    return-void
.end method

.method public bridge synthetic amf(Ljava/lang/String;)Ljava/util/ArrayList;
    .locals 1

    invoke-super {p0, p1}, Lcom/android/settings/accounts/MiuiAccountPreferenceBase;->amf(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic amg(Ljava/lang/String;Landroid/preference/PreferenceScreen;)Landroid/preference/PreferenceScreen;
    .locals 1

    invoke-super {p0, p1, p2}, Lcom/android/settings/accounts/MiuiAccountPreferenceBase;->amg(Ljava/lang/String;Landroid/preference/PreferenceScreen;)Landroid/preference/PreferenceScreen;

    move-result-object v0

    return-object v0
.end method

.method public at(Landroid/preference/Preference;)Z
    .locals 2

    const/4 v1, 0x0

    instance-of v0, p1, Lcom/android/settings/accounts/XiaomiAccountPreference;

    if-eqz v0, :cond_0

    return v1

    :cond_0
    instance-of v0, p1, Lcom/android/settings/accounts/AccountPreference;

    if-eqz v0, :cond_1

    check-cast p1, Lcom/android/settings/accounts/AccountPreference;

    invoke-direct {p0, p1}, Lcom/android/settings/accounts/MiuiManageAccounts;->anW(Lcom/android/settings/accounts/AccountPreference;)V

    const/4 v0, 0x1

    return v0

    :cond_1
    return v1
.end method

.method public cC()Ljava/lang/String;
    .locals 1

    const-class v0, Lcom/android/settings/accounts/MiuiManageAccounts;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getMetricsCategory()I
    .locals 1

    const/16 v0, 0xb

    return v0
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 3

    invoke-super {p0, p1}, Lcom/android/settings/accounts/MiuiAccountPreferenceBase;->onActivityCreated(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/android/settings/accounts/MiuiManageAccounts;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {p0}, Lcom/android/settings/accounts/MiuiManageAccounts;->getView()Landroid/view/View;

    move-result-object v0

    const v2, 0x7f0a0474

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/settings/accounts/MiuiManageAccounts;->azD:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/android/settings/accounts/MiuiManageAccounts;->azD:Landroid/widget/TextView;

    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    invoke-virtual {v1}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "authorities"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringArrayExtra(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/accounts/MiuiManageAccounts;->azC:[Ljava/lang/String;

    invoke-virtual {p0}, Lcom/android/settings/accounts/MiuiManageAccounts;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_0

    const-string/jumbo v1, "account_label"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/android/settings/accounts/MiuiManageAccounts;->getActivity()Landroid/app/Activity;

    move-result-object v1

    const-string/jumbo v2, "account_label"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/app/Activity;->setTitle(Ljava/lang/CharSequence;)V

    :cond_0
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Lcom/android/settings/accounts/MiuiAccountPreferenceBase;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/android/settings/accounts/MiuiManageAccounts;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_0

    const-string/jumbo v1, "account_type"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string/jumbo v1, "account_type"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/accounts/MiuiManageAccounts;->mAccountType:Ljava/lang/String;

    :cond_0
    const v0, 0x7f15007c

    invoke-virtual {p0, v0}, Lcom/android/settings/accounts/MiuiManageAccounts;->addPreferencesFromResource(I)V

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/android/settings/accounts/MiuiManageAccounts;->setHasOptionsMenu(Z)V

    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V
    .locals 3

    const/4 v2, 0x0

    const v0, 0x7f121222

    invoke-virtual {p0, v0}, Lcom/android/settings/accounts/MiuiManageAccounts;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    invoke-interface {p1, v2, v1, v2, v0}, Landroid/view/Menu;->add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v0

    const v1, 0x7f080207

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    const v0, 0x7f121221

    invoke-virtual {p0, v0}, Lcom/android/settings/accounts/MiuiManageAccounts;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x2

    invoke-interface {p1, v2, v1, v2, v0}, Landroid/view/Menu;->add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v0

    const v1, 0x1080038

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    invoke-super {p0, p1, p2}, Lcom/android/settings/accounts/MiuiAccountPreferenceBase;->onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V

    return-void
.end method

.method public onInflateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3

    const/4 v2, 0x0

    const v0, 0x7f0d00e4

    invoke-virtual {p1, v0, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const v0, 0x7f0a0332

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    invoke-static {p2, v1, v0, v2}, Lcom/android/settings/aq;->bqJ(Landroid/view/ViewGroup;Landroid/view/View;Landroid/view/View;Z)V

    invoke-super {p0, p1, v0, p3}, Lcom/android/settings/accounts/MiuiAccountPreferenceBase;->onInflateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    return-object v1
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2

    const/4 v1, 0x1

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    invoke-super {p0, p1}, Lcom/android/settings/accounts/MiuiAccountPreferenceBase;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    return v0

    :pswitch_0
    invoke-direct {p0, v1}, Lcom/android/settings/accounts/MiuiManageAccounts;->anS(Z)V

    return v1

    :pswitch_1
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/android/settings/accounts/MiuiManageAccounts;->anS(Z)V

    return v1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onPause()V
    .locals 1

    invoke-super {p0}, Lcom/android/settings/accounts/MiuiAccountPreferenceBase;->onPause()V

    iget-object v0, p0, Lcom/android/settings/accounts/MiuiManageAccounts;->aAv:Lcom/android/settingslib/c/a;

    invoke-virtual {v0}, Lcom/android/settingslib/c/a;->cgi()V

    return-void
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)V
    .locals 3

    invoke-super {p0, p1}, Lcom/android/settings/accounts/MiuiAccountPreferenceBase;->onPrepareOptionsMenu(Landroid/view/Menu;)V

    iget-object v0, p0, Lcom/android/settings/accounts/MiuiManageAccounts;->aAu:Landroid/os/UserHandle;

    invoke-virtual {v0}, Landroid/os/UserHandle;->getIdentifier()I

    move-result v0

    invoke-static {v0}, Landroid/content/ContentResolver;->getCurrentSyncsAsUser(I)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    const/4 v1, 0x1

    invoke-interface {p1, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    xor-int/lit8 v2, v0, 0x1

    invoke-interface {v1, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    const/4 v1, 0x2

    invoke-interface {p1, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    invoke-interface {v1, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    return-void
.end method

.method public onResume()V
    .locals 1

    invoke-super {p0}, Lcom/android/settings/accounts/MiuiAccountPreferenceBase;->onResume()V

    iget-object v0, p0, Lcom/android/settings/accounts/MiuiManageAccounts;->aAv:Lcom/android/settingslib/c/a;

    invoke-virtual {v0}, Lcom/android/settingslib/c/a;->cgb()V

    invoke-virtual {p0}, Lcom/android/settings/accounts/MiuiManageAccounts;->amd()V

    invoke-direct {p0}, Lcom/android/settings/accounts/MiuiManageAccounts;->anU()V

    invoke-direct {p0}, Lcom/android/settings/accounts/MiuiManageAccounts;->anV()V

    return-void
.end method
