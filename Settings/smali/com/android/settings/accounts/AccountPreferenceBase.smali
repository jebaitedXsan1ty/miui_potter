.class abstract Lcom/android/settings/accounts/AccountPreferenceBase;
.super Lcom/android/settings/MiuiSettingsPreferenceFragment;
.source "AccountPreferenceBase.java"

# interfaces
.implements Lcom/android/settingslib/c/b;


# static fields
.field protected static final ayD:Z


# instance fields
.field protected ayE:Lcom/android/settings/accounts/AccountTypePreferenceLoader;

.field protected ayF:Lcom/android/settingslib/c/a;

.field private ayG:Ljava/text/DateFormat;

.field private final ayH:Landroid/os/Handler;

.field private ayI:Ljava/lang/Object;

.field private ayJ:Landroid/content/SyncStatusObserver;

.field private ayK:Ljava/text/DateFormat;

.field private ayL:Landroid/os/UserManager;

.field protected ayM:Landroid/os/UserHandle;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const-string/jumbo v0, "AccountPreferenceBase"

    const/4 v1, 0x2

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    sput-boolean v0, Lcom/android/settings/accounts/AccountPreferenceBase;->ayD:Z

    return-void
.end method

.method constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/android/settings/MiuiSettingsPreferenceFragment;-><init>()V

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/android/settings/accounts/AccountPreferenceBase;->ayH:Landroid/os/Handler;

    new-instance v0, Lcom/android/settings/accounts/AccountPreferenceBase$1;

    invoke-direct {v0, p0}, Lcom/android/settings/accounts/AccountPreferenceBase$1;-><init>(Lcom/android/settings/accounts/AccountPreferenceBase;)V

    iput-object v0, p0, Lcom/android/settings/accounts/AccountPreferenceBase;->ayJ:Landroid/content/SyncStatusObserver;

    return-void
.end method

.method static synthetic amS(Lcom/android/settings/accounts/AccountPreferenceBase;)Landroid/os/Handler;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/accounts/AccountPreferenceBase;->ayH:Landroid/os/Handler;

    return-object v0
.end method


# virtual methods
.method protected amR()V
    .locals 0

    return-void
.end method

.method public ame(Landroid/os/UserHandle;)V
    .locals 0

    return-void
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->onActivityCreated(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/android/settings/accounts/AccountPreferenceBase;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Landroid/text/format/DateFormat;->getDateFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v1

    iput-object v1, p0, Lcom/android/settings/accounts/AccountPreferenceBase;->ayG:Ljava/text/DateFormat;

    invoke-static {v0}, Landroid/text/format/DateFormat;->getTimeFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/accounts/AccountPreferenceBase;->ayK:Ljava/text/DateFormat;

    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 5

    invoke-super {p0, p1}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->onCreate(Landroid/os/Bundle;)V

    const-string/jumbo v0, "user"

    invoke-virtual {p0, v0}, Lcom/android/settings/accounts/AccountPreferenceBase;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/UserManager;

    iput-object v0, p0, Lcom/android/settings/accounts/AccountPreferenceBase;->ayL:Landroid/os/UserManager;

    invoke-virtual {p0}, Lcom/android/settings/accounts/AccountPreferenceBase;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getActivityToken()Landroid/os/IBinder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/settings/accounts/AccountPreferenceBase;->ayL:Landroid/os/UserManager;

    invoke-virtual {p0}, Lcom/android/settings/accounts/AccountPreferenceBase;->getArguments()Landroid/os/Bundle;

    move-result-object v3

    invoke-virtual {v0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v4

    invoke-static {v1, v2, v3, v4}, Lcom/android/settings/aq;->bqL(Landroid/os/IBinder;Landroid/os/UserManager;Landroid/os/Bundle;Landroid/os/Bundle;)Landroid/os/UserHandle;

    move-result-object v1

    iput-object v1, p0, Lcom/android/settings/accounts/AccountPreferenceBase;->ayM:Landroid/os/UserHandle;

    new-instance v1, Lcom/android/settingslib/c/a;

    iget-object v2, p0, Lcom/android/settings/accounts/AccountPreferenceBase;->ayM:Landroid/os/UserHandle;

    invoke-direct {v1, v0, v2, p0}, Lcom/android/settingslib/c/a;-><init>(Landroid/content/Context;Landroid/os/UserHandle;Lcom/android/settingslib/c/b;)V

    iput-object v1, p0, Lcom/android/settings/accounts/AccountPreferenceBase;->ayF:Lcom/android/settingslib/c/a;

    new-instance v0, Lcom/android/settings/accounts/AccountTypePreferenceLoader;

    iget-object v1, p0, Lcom/android/settings/accounts/AccountPreferenceBase;->ayF:Lcom/android/settingslib/c/a;

    iget-object v2, p0, Lcom/android/settings/accounts/AccountPreferenceBase;->ayM:Landroid/os/UserHandle;

    invoke-direct {v0, p0, v1, v2}, Lcom/android/settings/accounts/AccountTypePreferenceLoader;-><init>(Landroid/preference/PreferenceFragment;Lcom/android/settingslib/c/a;Landroid/os/UserHandle;)V

    iput-object v0, p0, Lcom/android/settings/accounts/AccountPreferenceBase;->ayE:Lcom/android/settings/accounts/AccountTypePreferenceLoader;

    return-void
.end method

.method public onPause()V
    .locals 1

    invoke-super {p0}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->onPause()V

    iget-object v0, p0, Lcom/android/settings/accounts/AccountPreferenceBase;->ayI:Ljava/lang/Object;

    invoke-static {v0}, Landroid/content/ContentResolver;->removeStatusChangeListener(Ljava/lang/Object;)V

    return-void
.end method

.method public onResume()V
    .locals 2

    invoke-super {p0}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->onResume()V

    iget-object v0, p0, Lcom/android/settings/accounts/AccountPreferenceBase;->ayJ:Landroid/content/SyncStatusObserver;

    const/16 v1, 0xd

    invoke-static {v1, v0}, Landroid/content/ContentResolver;->addStatusChangeListener(ILandroid/content/SyncStatusObserver;)Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/accounts/AccountPreferenceBase;->ayI:Ljava/lang/Object;

    invoke-virtual {p0}, Lcom/android/settings/accounts/AccountPreferenceBase;->amR()V

    return-void
.end method
