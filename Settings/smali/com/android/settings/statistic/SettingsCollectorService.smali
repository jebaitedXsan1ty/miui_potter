.class public Lcom/android/settings/statistic/SettingsCollectorService;
.super Landroid/app/job/JobService;
.source "SettingsCollectorService.java"


# instance fields
.field private bmf:Ljava/util/List;

.field private bmg:Landroid/app/job/JobParameters;

.field private bmh:Ljava/util/HashMap;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/app/job/JobService;-><init>()V

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/android/settings/statistic/SettingsCollectorService;->bmh:Ljava/util/HashMap;

    return-void
.end method

.method private aZL()V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/statistic/SettingsCollectorService;->aZM()V

    invoke-direct {p0}, Lcom/android/settings/statistic/SettingsCollectorService;->aZT()V

    return-void
.end method

.method private aZM()V
    .locals 2

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/settings/statistic/SettingsCollectorService;->bmf:Ljava/util/List;

    const-string/jumbo v0, "Settings.System"

    iget-object v1, p0, Lcom/android/settings/statistic/SettingsCollectorService;->bmf:Ljava/util/List;

    invoke-direct {p0, v0}, Lcom/android/settings/statistic/SettingsCollectorService;->aZQ(Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    const-string/jumbo v0, "Settings.Secure"

    iget-object v1, p0, Lcom/android/settings/statistic/SettingsCollectorService;->bmf:Ljava/util/List;

    invoke-direct {p0, v0}, Lcom/android/settings/statistic/SettingsCollectorService;->aZQ(Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    const-string/jumbo v0, "Settings.Global"

    iget-object v1, p0, Lcom/android/settings/statistic/SettingsCollectorService;->bmf:Ljava/util/List;

    invoke-direct {p0, v0}, Lcom/android/settings/statistic/SettingsCollectorService;->aZQ(Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    const-string/jumbo v0, "MiuiSettings.System"

    iget-object v1, p0, Lcom/android/settings/statistic/SettingsCollectorService;->bmf:Ljava/util/List;

    invoke-direct {p0, v0}, Lcom/android/settings/statistic/SettingsCollectorService;->aZQ(Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    const-string/jumbo v0, "SystemProperties"

    iget-object v1, p0, Lcom/android/settings/statistic/SettingsCollectorService;->bmf:Ljava/util/List;

    invoke-direct {p0, v0}, Lcom/android/settings/statistic/SettingsCollectorService;->aZQ(Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    const-string/jumbo v0, "PreferredSettings"

    iget-object v1, p0, Lcom/android/settings/statistic/SettingsCollectorService;->bmf:Ljava/util/List;

    invoke-direct {p0, v0}, Lcom/android/settings/statistic/SettingsCollectorService;->aZQ(Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    const-string/jumbo v0, "Special.Keys"

    iget-object v1, p0, Lcom/android/settings/statistic/SettingsCollectorService;->bmf:Ljava/util/List;

    invoke-direct {p0, v0}, Lcom/android/settings/statistic/SettingsCollectorService;->aZQ(Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    return-void
.end method

.method private aZN(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    invoke-virtual {p0}, Lcom/android/settings/statistic/SettingsCollectorService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v1, "SettingsCollector2"

    invoke-static {v0, v1, p1, p2}, Landroid/provider/MiuiSettings$SettingsCloudData;->getCloudDataString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private aZO(Ljava/lang/String;Ljava/lang/String;)Lcom/android/settings/statistic/a;
    .locals 6

    const/4 v1, 0x0

    const/4 v3, 0x0

    invoke-virtual {p2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v4

    const-string/jumbo v0, "launcher"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, Lcom/android/settings/applications/DefaultAppsHelper;->pm(I)Landroid/content/IntentFilter;

    move-result-object v0

    invoke-static {v0}, Lcom/android/settings/applications/DefaultAppsHelper;->pk(Landroid/content/IntentFilter;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0}, Lcom/android/settings/statistic/SettingsCollectorService;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    invoke-virtual {v2, v0, v1}, Landroid/content/pm/PackageManager;->resolveActivity(Landroid/content/Intent;I)Landroid/content/pm/ResolveInfo;

    move-result-object v0

    iget-object v0, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    if-nez v0, :cond_9

    return-object v3

    :cond_0
    const-string/jumbo v0, "dialer"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const-string/jumbo v0, "message"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x2

    goto :goto_0

    :cond_2
    const-string/jumbo v0, "browser"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v0, 0x3

    goto :goto_0

    :cond_3
    const-string/jumbo v0, "camera"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    const/4 v0, 0x4

    goto :goto_0

    :cond_4
    const-string/jumbo v0, "gallery"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    const/4 v0, 0x5

    goto :goto_0

    :cond_5
    const-string/jumbo v0, "music"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    const/4 v0, 0x6

    goto :goto_0

    :cond_6
    const-string/jumbo v0, "email"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    const/4 v0, 0x7

    goto :goto_0

    :cond_7
    const-string/jumbo v0, "video"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    const/16 v0, 0x8

    goto :goto_0

    :cond_8
    return-object v3

    :cond_9
    new-instance v1, Landroid/content/ComponentName;

    iget-object v2, v0, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    iget-object v0, v0, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    invoke-direct {v1, v2, v0}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1}, Landroid/content/ComponentName;->flattenToShortString()Ljava/lang/String;

    move-result-object v5

    new-instance v0, Lcom/android/settings/statistic/a;

    const-string/jumbo v2, "PreferredSettings"

    move-object v1, p0

    move-object v3, p1

    invoke-direct/range {v0 .. v5}, Lcom/android/settings/statistic/a;-><init>(Lcom/android/settings/statistic/SettingsCollectorService;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method private aZP(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/android/settings/statistic/a;
    .locals 6

    const-string/jumbo v0, "Settings.System"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/android/settings/statistic/SettingsCollectorService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-static {v0, p3}, Landroid/provider/Settings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_6

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_6

    new-instance v0, Lcom/android/settings/statistic/a;

    const-string/jumbo v2, "Settings.System"

    move-object v1, p0

    move-object v3, p2

    move-object v4, p3

    invoke-direct/range {v0 .. v5}, Lcom/android/settings/statistic/a;-><init>(Lcom/android/settings/statistic/SettingsCollectorService;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object v0

    :cond_0
    const-string/jumbo v0, "Settings.Secure"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/android/settings/statistic/SettingsCollectorService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-static {v0, p3}, Landroid/provider/Settings$Secure;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_6

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_6

    new-instance v0, Lcom/android/settings/statistic/a;

    const-string/jumbo v2, "Settings.Secure"

    move-object v1, p0

    move-object v3, p2

    move-object v4, p3

    invoke-direct/range {v0 .. v5}, Lcom/android/settings/statistic/a;-><init>(Lcom/android/settings/statistic/SettingsCollectorService;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object v0

    :cond_1
    const-string/jumbo v0, "Settings.Global"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lcom/android/settings/statistic/SettingsCollectorService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-static {v0, p3}, Landroid/provider/Settings$Global;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_6

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_6

    new-instance v0, Lcom/android/settings/statistic/a;

    const-string/jumbo v2, "Settings.Global"

    move-object v1, p0

    move-object v3, p2

    move-object v4, p3

    invoke-direct/range {v0 .. v5}, Lcom/android/settings/statistic/a;-><init>(Lcom/android/settings/statistic/SettingsCollectorService;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object v0

    :cond_2
    const-string/jumbo v0, "MiuiSettings.System"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p0}, Lcom/android/settings/statistic/SettingsCollectorService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-static {v0, p3}, Landroid/provider/MiuiSettings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_6

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_6

    new-instance v0, Lcom/android/settings/statistic/a;

    const-string/jumbo v2, "MiuiSettings.System"

    move-object v1, p0

    move-object v3, p2

    move-object v4, p3

    invoke-direct/range {v0 .. v5}, Lcom/android/settings/statistic/a;-><init>(Lcom/android/settings/statistic/SettingsCollectorService;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object v0

    :cond_3
    const-string/jumbo v0, "SystemProperties"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-static {p3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_6

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_6

    new-instance v0, Lcom/android/settings/statistic/a;

    const-string/jumbo v2, "SystemProperties"

    move-object v1, p0

    move-object v3, p2

    move-object v4, p3

    invoke-direct/range {v0 .. v5}, Lcom/android/settings/statistic/a;-><init>(Lcom/android/settings/statistic/SettingsCollectorService;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object v0

    :cond_4
    const-string/jumbo v0, "PreferredSettings"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-direct {p0, p2, p3}, Lcom/android/settings/statistic/SettingsCollectorService;->aZO(Ljava/lang/String;Ljava/lang/String;)Lcom/android/settings/statistic/a;

    move-result-object v0

    return-object v0

    :cond_5
    const-string/jumbo v0, "Special.Keys"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-direct {p0, p2, p3}, Lcom/android/settings/statistic/SettingsCollectorService;->aZR(Ljava/lang/String;Ljava/lang/String;)Lcom/android/settings/statistic/a;

    move-result-object v0

    return-object v0

    :cond_6
    const-string/jumbo v0, "SettingsCollector"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "get null for module:\""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "\", key:\""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "\""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Lcom/android/settings/statistic/a;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/android/settings/statistic/a;-><init>(Lcom/android/settings/statistic/SettingsCollectorService;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method private aZQ(Ljava/lang/String;)Ljava/util/List;
    .locals 8

    const/4 v0, 0x0

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-direct {p0, p1, v0}, Lcom/android/settings/statistic/SettingsCollectorService;->aZN(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    :try_start_0
    new-instance v2, Lorg/json/JSONArray;

    invoke-direct {v2, v0}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v2}, Lorg/json/JSONArray;->length()I

    move-result v3

    if-ge v0, v3, :cond_0

    invoke-virtual {v2, v0}, Lorg/json/JSONArray;->optJSONObject(I)Lorg/json/JSONObject;

    move-result-object v3

    const-string/jumbo v4, "key"

    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string/jumbo v5, "category"

    invoke-virtual {v3, v5, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    const-string/jumbo v6, "def"

    const-string/jumbo v7, "empty"

    invoke-virtual {v3, v6, v7}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, p1, v5, v4, v3}, Lcom/android/settings/statistic/SettingsCollectorService;->aZP(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/android/settings/statistic/a;

    move-result-object v3

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :catch_0
    move-exception v0

    const-string/jumbo v0, "SettingsCollector"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "analyze JSON failed."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    return-object v1
.end method

.method private aZR(Ljava/lang/String;Ljava/lang/String;)Lcom/android/settings/statistic/a;
    .locals 6

    :try_start_0
    new-instance v0, Lcom/android/settings/statistic/a;

    const-string/jumbo v2, "Special.Keys"

    iget-object v1, p0, Lcom/android/settings/statistic/SettingsCollectorService;->bmh:Ljava/util/HashMap;

    invoke-virtual {v1, p2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/concurrent/Callable;

    invoke-interface {v1}, Ljava/util/concurrent/Callable;->call()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    move-object v1, p0

    move-object v3, p1

    move-object v4, p2

    invoke-direct/range {v0 .. v5}, Lcom/android/settings/statistic/a;-><init>(Lcom/android/settings/statistic/SettingsCollectorService;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    move-exception v0

    const-string/jumbo v1, "SettingsCollector"

    const-string/jumbo v2, ""

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    new-instance v0, Lcom/android/settings/statistic/a;

    const-string/jumbo v2, "Special.Keys"

    const/4 v5, 0x0

    move-object v1, p0

    move-object v3, p1

    move-object v4, p2

    invoke-direct/range {v0 .. v5}, Lcom/android/settings/statistic/a;-><init>(Lcom/android/settings/statistic/SettingsCollectorService;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method private aZS()V
    .locals 3

    iget-object v0, p0, Lcom/android/settings/statistic/SettingsCollectorService;->bmh:Ljava/util/HashMap;

    const-string/jumbo v1, "font"

    new-instance v2, Lcom/android/settings/statistic/c;

    invoke-direct {v2, p0}, Lcom/android/settings/statistic/c;-><init>(Lcom/android/settings/statistic/SettingsCollectorService;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/android/settings/statistic/SettingsCollectorService;->bmh:Ljava/util/HashMap;

    const-string/jumbo v1, "font_size"

    new-instance v2, Lcom/android/settings/statistic/d;

    invoke-direct {v2, p0}, Lcom/android/settings/statistic/d;-><init>(Lcom/android/settings/statistic/SettingsCollectorService;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/android/settings/statistic/SettingsCollectorService;->bmh:Ljava/util/HashMap;

    const-string/jumbo v1, "app_notifications"

    new-instance v2, Lcom/android/settings/statistic/e;

    invoke-direct {v2, p0}, Lcom/android/settings/statistic/e;-><init>(Lcom/android/settings/statistic/SettingsCollectorService;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/android/settings/statistic/SettingsCollectorService;->bmh:Ljava/util/HashMap;

    const-string/jumbo v1, "vip_call_settings"

    new-instance v2, Lcom/android/settings/statistic/f;

    invoke-direct {v2, p0}, Lcom/android/settings/statistic/f;-><init>(Lcom/android/settings/statistic/SettingsCollectorService;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/android/settings/statistic/SettingsCollectorService;->bmh:Ljava/util/HashMap;

    const-string/jumbo v1, "vip_list"

    new-instance v2, Lcom/android/settings/statistic/g;

    invoke-direct {v2, p0}, Lcom/android/settings/statistic/g;-><init>(Lcom/android/settings/statistic/SettingsCollectorService;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/android/settings/statistic/SettingsCollectorService;->bmh:Ljava/util/HashMap;

    const-string/jumbo v1, "ring_volume"

    new-instance v2, Lcom/android/settings/statistic/h;

    invoke-direct {v2, p0}, Lcom/android/settings/statistic/h;-><init>(Lcom/android/settings/statistic/SettingsCollectorService;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/android/settings/statistic/SettingsCollectorService;->bmh:Ljava/util/HashMap;

    const-string/jumbo v1, "alarm_volume"

    new-instance v2, Lcom/android/settings/statistic/i;

    invoke-direct {v2, p0}, Lcom/android/settings/statistic/i;-><init>(Lcom/android/settings/statistic/SettingsCollectorService;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/android/settings/statistic/SettingsCollectorService;->bmh:Ljava/util/HashMap;

    const-string/jumbo v1, "music_volume"

    new-instance v2, Lcom/android/settings/statistic/j;

    invoke-direct {v2, p0}, Lcom/android/settings/statistic/j;-><init>(Lcom/android/settings/statistic/SettingsCollectorService;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/android/settings/statistic/SettingsCollectorService;->bmh:Ljava/util/HashMap;

    const-string/jumbo v1, "owner_info"

    new-instance v2, Lcom/android/settings/statistic/k;

    invoke-direct {v2, p0}, Lcom/android/settings/statistic/k;-><init>(Lcom/android/settings/statistic/SettingsCollectorService;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/android/settings/statistic/SettingsCollectorService;->bmh:Ljava/util/HashMap;

    const-string/jumbo v1, "lite_mode"

    new-instance v2, Lcom/android/settings/statistic/l;

    invoke-direct {v2, p0}, Lcom/android/settings/statistic/l;-><init>(Lcom/android/settings/statistic/SettingsCollectorService;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method private aZT()V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/statistic/SettingsCollectorService;->bmf:Ljava/util/List;

    if-nez v0, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/settings/statistic/SettingsCollectorService;->bmf:Ljava/util/List;

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settings/statistic/a;

    invoke-virtual {v0}, Lcom/android/settings/statistic/a;->aZX()V

    goto :goto_0

    :cond_1
    return-void
.end method

.method static synthetic aZU(Lcom/android/settings/statistic/SettingsCollectorService;)Landroid/app/job/JobParameters;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/statistic/SettingsCollectorService;->bmg:Landroid/app/job/JobParameters;

    return-object v0
.end method

.method static synthetic aZV(Lcom/android/settings/statistic/SettingsCollectorService;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/statistic/SettingsCollectorService;->aZL()V

    return-void
.end method

.method static synthetic aZW(Lcom/android/settings/statistic/SettingsCollectorService;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/statistic/SettingsCollectorService;->aZS()V

    return-void
.end method


# virtual methods
.method public onStartJob(Landroid/app/job/JobParameters;)Z
    .locals 3

    const/4 v2, 0x0

    invoke-virtual {p1}, Landroid/app/job/JobParameters;->getJobId()I

    move-result v0

    const v1, 0xabe5

    if-eq v0, v1, :cond_0

    return v2

    :cond_0
    const-string/jumbo v0, "SettingsCollector"

    const-string/jumbo v1, "start service, version: 2"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    iput-object p1, p0, Lcom/android/settings/statistic/SettingsCollectorService;->bmg:Landroid/app/job/JobParameters;

    new-instance v0, Lcom/android/settings/statistic/b;

    invoke-direct {v0, p0}, Lcom/android/settings/statistic/b;-><init>(Lcom/android/settings/statistic/SettingsCollectorService;)V

    new-array v1, v2, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/android/settings/statistic/b;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    const/4 v0, 0x1

    return v0
.end method

.method public onStopJob(Landroid/app/job/JobParameters;)Z
    .locals 2

    const-string/jumbo v0, "SettingsCollector"

    const-string/jumbo v1, "force stopped"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    return v0
.end method
