.class public Lcom/android/settings/CancellablePreference;
.super Landroid/preference/Preference;
.source "CancellablePreference.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private bWN:Z

.field private bWO:Lcom/android/settings/ch;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    invoke-direct {p0, p1, p2}, Landroid/preference/Preference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const v0, 0x7f0d0055

    invoke-virtual {p0, v0}, Lcom/android/settings/CancellablePreference;->setWidgetLayoutResource(I)V

    return-void
.end method


# virtual methods
.method public bQF(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/android/settings/CancellablePreference;->bWN:Z

    invoke-virtual {p0}, Lcom/android/settings/CancellablePreference;->notifyChanged()V

    return-void
.end method

.method public bQG(Lcom/android/settings/ch;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/CancellablePreference;->bWO:Lcom/android/settings/ch;

    return-void
.end method

.method public onBindView(Landroid/view/View;)V
    .locals 2

    invoke-super {p0, p1}, Landroid/preference/Preference;->onBindView(Landroid/view/View;)V

    const v0, 0x7f0a00c6

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iget-boolean v1, p0, Lcom/android/settings/CancellablePreference;->bWN:Z

    if-eqz v1, :cond_0

    const/4 v1, 0x0

    :goto_0
    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    invoke-virtual {v0, p0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void

    :cond_0
    const/4 v1, 0x4

    goto :goto_0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 1

    iget-object v0, p0, Lcom/android/settings/CancellablePreference;->bWO:Lcom/android/settings/ch;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/CancellablePreference;->bWO:Lcom/android/settings/ch;

    invoke-interface {v0, p0}, Lcom/android/settings/ch;->zX(Lcom/android/settings/CancellablePreference;)V

    :cond_0
    return-void
.end method
