.class public Lcom/android/settings/MiuiDisplaySettings;
.super Lcom/android/settings/DisplaySettings;
.source "MiuiDisplaySettings.java"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceChangeListener;


# static fields
.field private static final TAG:Ljava/lang/String;

.field private static brk:Landroid/util/SparseArray;


# instance fields
.field private bqQ:Landroid/preference/CheckBoxPreference;

.field private final bqR:Landroid/content/res/Configuration;

.field private bqS:Lmiui/preference/ValuePreference;

.field private bqT:Lcom/android/settings/WarnedListPreference;

.field private bqU:Lcom/android/settings/bA;

.field private bqV:Landroid/preference/CheckBoxPreference;

.field private bqW:Landroid/preference/CheckBoxPreference;

.field private bqX:Lcom/android/settings/dc;

.field private bqY:Landroid/database/ContentObserver;

.field private bqZ:Landroid/preference/CheckBoxPreference;

.field private bra:Lcom/android/settings/bA;

.field private brb:Lmiui/preference/ValuePreference;

.field private brc:Landroid/database/ContentObserver;

.field private brd:Landroid/preference/CheckBoxPreference;

.field private bre:Landroid/preference/CheckBoxPreference;

.field private final brf:Lcom/android/internal/view/RotationPolicy$RotationPolicyListener;

.field private brg:Landroid/preference/Preference;

.field private brh:Lcom/android/settings/display/ResolutionListPreference;

.field private bri:Lcom/android/settings/TimeoutListPreference;

.field private brj:Landroid/preference/CheckBoxPreference;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/4 v3, 0x1

    const-class v0, Lcom/android/settings/MiuiDisplaySettings;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/settings/MiuiDisplaySettings;->TAG:Ljava/lang/String;

    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    sput-object v0, Lcom/android/settings/MiuiDisplaySettings;->brk:Landroid/util/SparseArray;

    sget-object v0, Lcom/android/settings/MiuiDisplaySettings;->brk:Landroid/util/SparseArray;

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/16 v2, 0xc

    invoke-virtual {v0, v2, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    sget-object v0, Lcom/android/settings/MiuiDisplaySettings;->brk:Landroid/util/SparseArray;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v3, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    sget-object v0, Lcom/android/settings/MiuiDisplaySettings;->brk:Landroid/util/SparseArray;

    const/4 v1, 0x2

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/16 v2, 0xd

    invoke-virtual {v0, v2, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    sget-object v0, Lcom/android/settings/MiuiDisplaySettings;->brk:Landroid/util/SparseArray;

    const/4 v1, 0x3

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/16 v2, 0xe

    invoke-virtual {v0, v2, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    sget-object v0, Lcom/android/settings/MiuiDisplaySettings;->brk:Landroid/util/SparseArray;

    const/4 v1, 0x4

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/16 v2, 0xf

    invoke-virtual {v0, v2, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    sget-object v0, Lcom/android/settings/MiuiDisplaySettings;->brk:Landroid/util/SparseArray;

    const/4 v1, 0x5

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/16 v2, 0xb

    invoke-virtual {v0, v2, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/android/settings/DisplaySettings;-><init>()V

    new-instance v0, Landroid/content/res/Configuration;

    invoke-direct {v0}, Landroid/content/res/Configuration;-><init>()V

    iput-object v0, p0, Lcom/android/settings/MiuiDisplaySettings;->bqR:Landroid/content/res/Configuration;

    new-instance v0, Lcom/android/settings/di;

    invoke-direct {v0, p0}, Lcom/android/settings/di;-><init>(Lcom/android/settings/MiuiDisplaySettings;)V

    iput-object v0, p0, Lcom/android/settings/MiuiDisplaySettings;->brf:Lcom/android/internal/view/RotationPolicy$RotationPolicyListener;

    return-void
.end method

.method static synthetic beA(Lcom/android/settings/MiuiDisplaySettings;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/MiuiDisplaySettings;->bes()V

    return-void
.end method

.method private bem(Z)V
    .locals 2

    invoke-virtual {p0}, Lcom/android/settings/MiuiDisplaySettings;->bWB()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v1, "enable_screen_on_proximity_sensor"

    invoke-static {v0, v1, p1}, Landroid/provider/MiuiSettings$Global;->putBoolean(Landroid/content/ContentResolver;Ljava/lang/String;Z)Z

    return-void
.end method

.method public static ben(Landroid/content/Context;)Z
    .locals 4

    const/4 v1, 0x0

    if-eqz p0, :cond_3

    const-string/jumbo v0, "sensor"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/SensorManager;

    const-string/jumbo v2, "jason"

    sget-object v3, Lmiui/os/Build;->DEVICE:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/16 v1, 0x19

    invoke-virtual {v0, v1}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object v0

    :goto_0
    if-eqz v0, :cond_2

    const/4 v0, 0x1

    :goto_1
    return v0

    :cond_0
    const-string/jumbo v2, "polaris"

    sget-object v3, Lmiui/os/Build;->DEVICE:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    const-string/jumbo v2, "dipper"

    sget-object v3, Lmiui/os/Build;->DEVICE:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    :cond_1
    const v1, 0x1fa263e

    invoke-virtual {v0, v1}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object v0

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_0
.end method

.method private bep()V
    .locals 2

    invoke-virtual {p0}, Lcom/android/settings/MiuiDisplaySettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/settings/MiuiDisplaySettings;->bre:Landroid/preference/CheckBoxPreference;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/settings/MiuiDisplaySettings;->bre:Landroid/preference/CheckBoxPreference;

    invoke-virtual {p0}, Lcom/android/settings/MiuiDisplaySettings;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-static {v1}, Lcom/android/internal/view/RotationPolicy;->isRotationLocked(Landroid/content/Context;)Z

    move-result v1

    xor-int/lit8 v1, v1, 0x1

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    :cond_1
    return-void
.end method

.method private beq()V
    .locals 1

    iget-object v0, p0, Lcom/android/settings/MiuiDisplaySettings;->bqU:Lcom/android/settings/bA;

    invoke-virtual {v0}, Lcom/android/settings/bA;->wv()V

    iget-object v0, p0, Lcom/android/settings/MiuiDisplaySettings;->bra:Lcom/android/settings/bA;

    invoke-virtual {v0}, Lcom/android/settings/bA;->wv()V

    iget-object v0, p0, Lcom/android/settings/MiuiDisplaySettings;->bqT:Lcom/android/settings/WarnedListPreference;

    invoke-virtual {p0, v0}, Lcom/android/settings/MiuiDisplaySettings;->beo(Landroid/preference/ListPreference;)V

    return-void
.end method

.method private ber()V
    .locals 3

    const-string/jumbo v0, "screen_monochrome_mode"

    invoke-virtual {p0, v0}, Lcom/android/settings/MiuiDisplaySettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Lmiui/preference/ValuePreference;

    invoke-virtual {p0}, Lcom/android/settings/MiuiDisplaySettings;->getActivity()Landroid/app/Activity;

    move-result-object v1

    if-eqz v0, :cond_0

    if-eqz v1, :cond_0

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Lmiui/preference/ValuePreference;->setShowRightArrow(Z)V

    invoke-static {v1}, Lcom/android/settings/display/MonochromeModeFragment;->Mu(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_1

    const v1, 0x7f120ea2

    :goto_0
    invoke-virtual {v0, v1}, Lmiui/preference/ValuePreference;->setValue(I)V

    :cond_0
    return-void

    :cond_1
    const v1, 0x7f120ea1

    goto :goto_0
.end method

.method private bes()V
    .locals 3

    const-string/jumbo v0, "screen_paper_mode"

    invoke-virtual {p0, v0}, Lcom/android/settings/MiuiDisplaySettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Lmiui/preference/ValuePreference;

    invoke-virtual {p0}, Lcom/android/settings/MiuiDisplaySettings;->getActivity()Landroid/app/Activity;

    move-result-object v1

    if-eqz v0, :cond_0

    if-eqz v1, :cond_0

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Lmiui/preference/ValuePreference;->setShowRightArrow(Z)V

    invoke-static {v1}, Lcom/android/settings/display/PaperModeFragment;->LT(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_1

    const v1, 0x7f120ea2

    :goto_0
    invoke-virtual {v0, v1}, Lmiui/preference/ValuePreference;->setValue(I)V

    :cond_0
    return-void

    :cond_1
    const v1, 0x7f120ea1

    goto :goto_0
.end method

.method static synthetic beu(Lcom/android/settings/MiuiDisplaySettings;)Lmiui/preference/ValuePreference;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/MiuiDisplaySettings;->bqS:Lmiui/preference/ValuePreference;

    return-object v0
.end method

.method static synthetic bev(Lcom/android/settings/MiuiDisplaySettings;)Lcom/android/settings/WarnedListPreference;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/MiuiDisplaySettings;->bqT:Lcom/android/settings/WarnedListPreference;

    return-object v0
.end method

.method static synthetic bew(Lcom/android/settings/MiuiDisplaySettings;)Lmiui/preference/ValuePreference;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/MiuiDisplaySettings;->brb:Lmiui/preference/ValuePreference;

    return-object v0
.end method

.method static synthetic bex(Lcom/android/settings/MiuiDisplaySettings;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/settings/MiuiDisplaySettings;->bem(Z)V

    return-void
.end method

.method static synthetic bey(Lcom/android/settings/MiuiDisplaySettings;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/MiuiDisplaySettings;->bep()V

    return-void
.end method

.method static synthetic bez(Lcom/android/settings/MiuiDisplaySettings;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/MiuiDisplaySettings;->ber()V

    return-void
.end method


# virtual methods
.method public beo(Landroid/preference/ListPreference;)V
    .locals 4

    if-nez p1, :cond_0

    return-void

    :cond_0
    :try_start_0
    iget-object v0, p0, Lcom/android/settings/MiuiDisplaySettings;->bqR:Landroid/content/res/Configuration;

    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    move-result-object v1

    invoke-interface {v1}, Landroid/app/IActivityManager;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/res/Configuration;->updateFrom(Landroid/content/res/Configuration;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    sget-object v0, Lcom/android/settings/MiuiDisplaySettings;->brk:Landroid/util/SparseArray;

    iget-object v1, p0, Lcom/android/settings/MiuiDisplaySettings;->bqR:Landroid/content/res/Configuration;

    iget v1, v1, Landroid/content/res/Configuration;->uiMode:I

    and-int/lit8 v1, v1, 0xf

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/preference/ListPreference;->setValueIndex(I)V

    invoke-virtual {p0}, Lcom/android/settings/MiuiDisplaySettings;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f03009e

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v2

    const v3, 0x7f1211e7

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    aget-object v0, v2, v0

    const/4 v2, 0x0

    aput-object v0, v3, v2

    invoke-static {v1, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/preference/ListPreference;->setSummary(Ljava/lang/CharSequence;)V

    return-void

    :catch_0
    move-exception v0

    sget-object v0, Lcom/android/settings/MiuiDisplaySettings;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "Unable to retrieve font size"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public bet(Ljava/lang/Object;)V
    .locals 5

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/android/settings/MiuiDisplaySettings;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f03009f

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v3

    move v1, v0

    move v2, v0

    :goto_0
    array-length v0, v3

    if-ge v1, v0, :cond_1

    aget-object v4, v3, v1

    move-object v0, p1

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    move v2, v1

    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    sget-object v0, Lcom/android/settings/MiuiDisplaySettings;->brk:Landroid/util/SparseArray;

    sget-object v1, Lcom/android/settings/MiuiDisplaySettings;->brk:Landroid/util/SparseArray;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/util/SparseArray;->indexOfValue(Ljava/lang/Object;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v0

    iget-object v1, p0, Lcom/android/settings/MiuiDisplaySettings;->bqR:Landroid/content/res/Configuration;

    iget v1, v1, Landroid/content/res/Configuration;->uiMode:I

    and-int/lit8 v1, v1, 0xf

    if-eq v0, v1, :cond_2

    invoke-virtual {p0}, Lcom/android/settings/MiuiDisplaySettings;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/android/settings/display/j;->LA(Landroid/content/Context;I)Z

    :cond_2
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 7

    const/4 v2, 0x1

    const/4 v3, 0x0

    const/4 v6, 0x0

    invoke-super {p0, p1}, Lcom/android/settings/DisplaySettings;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/android/settings/MiuiDisplaySettings;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    const-string/jumbo v1, "screen_effect"

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    invoke-static {}, Lcom/android/settings/dc;->getInstance()Lcom/android/settings/dc;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/MiuiDisplaySettings;->bqX:Lcom/android/settings/dc;

    const-string/jumbo v0, "touch_category"

    invoke-virtual {p0, v0}, Lcom/android/settings/MiuiDisplaySettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceCategory;

    const-string/jumbo v1, "touch_sensitive"

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceCategory;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    check-cast v1, Landroid/preference/CheckBoxPreference;

    iput-object v1, p0, Lcom/android/settings/MiuiDisplaySettings;->brj:Landroid/preference/CheckBoxPreference;

    const-string/jumbo v1, "support_touch_sensitive"

    invoke-static {v1, v3}, Lmiui/util/FeatureParser;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    if-nez v1, :cond_d

    iget-object v1, p0, Lcom/android/settings/MiuiDisplaySettings;->brj:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceCategory;->removePreference(Landroid/preference/Preference;)Z

    iput-object v6, p0, Lcom/android/settings/MiuiDisplaySettings;->brj:Landroid/preference/CheckBoxPreference;

    :goto_0
    invoke-virtual {v0}, Landroid/preference/PreferenceCategory;->getPreferenceCount()I

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p0}, Lcom/android/settings/MiuiDisplaySettings;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    :cond_0
    const-string/jumbo v0, "line_breaking"

    invoke-virtual {p0, v0}, Lcom/android/settings/MiuiDisplaySettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    iput-object v0, p0, Lcom/android/settings/MiuiDisplaySettings;->bqW:Landroid/preference/CheckBoxPreference;

    const-string/jumbo v0, "gesture_wakeup"

    invoke-virtual {p0, v0}, Lcom/android/settings/MiuiDisplaySettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    iput-object v0, p0, Lcom/android/settings/MiuiDisplaySettings;->bqV:Landroid/preference/CheckBoxPreference;

    const-string/jumbo v0, "support_gesture_wakeup"

    invoke-static {v0, v3}, Lmiui/util/FeatureParser;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_e

    invoke-virtual {p0}, Lcom/android/settings/MiuiDisplaySettings;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/MiuiDisplaySettings;->bqV:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    :goto_1
    const-string/jumbo v0, "pick_up_gesture_wakeup"

    invoke-virtual {p0, v0}, Lcom/android/settings/MiuiDisplaySettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    iput-object v0, p0, Lcom/android/settings/MiuiDisplaySettings;->brd:Landroid/preference/CheckBoxPreference;

    invoke-virtual {p0}, Lcom/android/settings/MiuiDisplaySettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/android/settings/MiuiDisplaySettings;->ben(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_10

    invoke-virtual {p0}, Lcom/android/settings/MiuiDisplaySettings;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/MiuiDisplaySettings;->brd:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    :goto_2
    const-string/jumbo v0, "aod_mode"

    invoke-virtual {p0, v0}, Lcom/android/settings/MiuiDisplaySettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    iput-object v0, p0, Lcom/android/settings/MiuiDisplaySettings;->bqQ:Landroid/preference/CheckBoxPreference;

    const-string/jumbo v0, "support_aod"

    invoke-static {v0, v3}, Lmiui/util/FeatureParser;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_11

    invoke-virtual {p0}, Lcom/android/settings/MiuiDisplaySettings;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/MiuiDisplaySettings;->bqQ:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    :goto_3
    const-string/jumbo v0, "screen_timeout"

    invoke-virtual {p0, v0}, Lcom/android/settings/MiuiDisplaySettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Lcom/android/settings/TimeoutListPreference;

    iput-object v0, p0, Lcom/android/settings/MiuiDisplaySettings;->bri:Lcom/android/settings/TimeoutListPreference;

    iget-object v0, p0, Lcom/android/settings/MiuiDisplaySettings;->bri:Lcom/android/settings/TimeoutListPreference;

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/android/settings/MiuiDisplaySettings;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/MiuiDisplaySettings;->bri:Lcom/android/settings/TimeoutListPreference;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    :cond_1
    const-string/jumbo v0, "screen_monochrome_mode"

    invoke-virtual {p0, v0}, Lcom/android/settings/MiuiDisplaySettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/MiuiDisplaySettings;->brg:Landroid/preference/Preference;

    iget-object v0, p0, Lcom/android/settings/MiuiDisplaySettings;->brg:Landroid/preference/Preference;

    if-eqz v0, :cond_13

    sget v0, Landroid/provider/MiuiSettings$ScreenEffect;->SCREEN_EFFECT_SUPPORTED:I

    and-int/lit8 v0, v0, 0x8

    if-nez v0, :cond_13

    invoke-virtual {p0}, Lcom/android/settings/MiuiDisplaySettings;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/MiuiDisplaySettings;->brg:Landroid/preference/Preference;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    iput-object v6, p0, Lcom/android/settings/MiuiDisplaySettings;->brg:Landroid/preference/Preference;

    :goto_4
    const-string/jumbo v0, "screen_resolution"

    invoke-virtual {p0, v0}, Lcom/android/settings/MiuiDisplaySettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Lcom/android/settings/display/ResolutionListPreference;

    iput-object v0, p0, Lcom/android/settings/MiuiDisplaySettings;->brh:Lcom/android/settings/display/ResolutionListPreference;

    iget-object v0, p0, Lcom/android/settings/MiuiDisplaySettings;->brh:Lcom/android/settings/display/ResolutionListPreference;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/settings/MiuiDisplaySettings;->brh:Lcom/android/settings/display/ResolutionListPreference;

    invoke-virtual {v0}, Lcom/android/settings/display/ResolutionListPreference;->OA()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lcom/android/settings/MiuiDisplaySettings;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/MiuiDisplaySettings;->brh:Lcom/android/settings/display/ResolutionListPreference;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    iput-object v6, p0, Lcom/android/settings/MiuiDisplaySettings;->brh:Lcom/android/settings/display/ResolutionListPreference;

    :cond_2
    sget-boolean v0, Lmiui/os/Build;->IS_TABLET:Z

    if-eqz v0, :cond_3

    const-string/jumbo v0, "font_settings_cat"

    invoke-virtual {p0, v0}, Lcom/android/settings/MiuiDisplaySettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceCategory;

    if-eqz v0, :cond_3

    invoke-virtual {p0}, Lcom/android/settings/MiuiDisplaySettings;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    :cond_3
    sget v0, Landroid/provider/MiuiSettings$ScreenEffect;->SCREEN_EFFECT_SUPPORTED:I

    and-int/lit8 v0, v0, 0x7

    if-nez v0, :cond_4

    const-string/jumbo v0, "screen_effect"

    invoke-virtual {p0, v0}, Lcom/android/settings/MiuiDisplaySettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    if-eqz v0, :cond_4

    invoke-virtual {p0}, Lcom/android/settings/MiuiDisplaySettings;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    :cond_4
    sget-boolean v0, Landroid/provider/MiuiSettings$ScreenEffect;->isScreenPaperModeSupported:Z

    if-nez v0, :cond_14

    const-string/jumbo v0, "screen_paper_mode"

    invoke-virtual {p0, v0}, Lcom/android/settings/MiuiDisplaySettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    if-eqz v0, :cond_5

    invoke-virtual {p0}, Lcom/android/settings/MiuiDisplaySettings;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    :cond_5
    :goto_5
    new-instance v0, Lcom/android/settings/display/c;

    invoke-virtual {p0}, Lcom/android/settings/MiuiDisplaySettings;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, v1, v6}, Lcom/android/settings/display/c;-><init>(Landroid/content/Context;Landroid/widget/TextView;)V

    iput-object v0, p0, Lcom/android/settings/MiuiDisplaySettings;->bqU:Lcom/android/settings/bA;

    const-string/jumbo v0, "font_settings"

    invoke-virtual {p0, v0}, Lcom/android/settings/MiuiDisplaySettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Lmiui/preference/ValuePreference;

    iput-object v0, p0, Lcom/android/settings/MiuiDisplaySettings;->bqS:Lmiui/preference/ValuePreference;

    iget-object v0, p0, Lcom/android/settings/MiuiDisplaySettings;->bqS:Lmiui/preference/ValuePreference;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/android/settings/MiuiDisplaySettings;->bqS:Lmiui/preference/ValuePreference;

    invoke-virtual {v0, v2}, Lmiui/preference/ValuePreference;->setShowRightArrow(Z)V

    iget-object v0, p0, Lcom/android/settings/MiuiDisplaySettings;->bqU:Lcom/android/settings/bA;

    new-instance v1, Lcom/android/settings/dl;

    invoke-direct {v1, p0}, Lcom/android/settings/dl;-><init>(Lcom/android/settings/MiuiDisplaySettings;)V

    invoke-virtual {v0, v1}, Lcom/android/settings/bA;->bKU(Lcom/android/settings/bB;)V

    :cond_6
    new-instance v0, Lcom/android/settings/display/i;

    invoke-virtual {p0}, Lcom/android/settings/MiuiDisplaySettings;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, v1, v6}, Lcom/android/settings/display/i;-><init>(Landroid/content/Context;Landroid/widget/TextView;)V

    iput-object v0, p0, Lcom/android/settings/MiuiDisplaySettings;->bra:Lcom/android/settings/bA;

    const-string/jumbo v0, "page_layout_settings"

    invoke-virtual {p0, v0}, Lcom/android/settings/MiuiDisplaySettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Lmiui/preference/ValuePreference;

    iput-object v0, p0, Lcom/android/settings/MiuiDisplaySettings;->brb:Lmiui/preference/ValuePreference;

    iget-object v0, p0, Lcom/android/settings/MiuiDisplaySettings;->brb:Lmiui/preference/ValuePreference;

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/android/settings/MiuiDisplaySettings;->brb:Lmiui/preference/ValuePreference;

    invoke-virtual {v0, v2}, Lmiui/preference/ValuePreference;->setShowRightArrow(Z)V

    iget-object v0, p0, Lcom/android/settings/MiuiDisplaySettings;->bra:Lcom/android/settings/bA;

    new-instance v1, Lcom/android/settings/dm;

    invoke-direct {v1, p0}, Lcom/android/settings/dm;-><init>(Lcom/android/settings/MiuiDisplaySettings;)V

    invoke-virtual {v0, v1}, Lcom/android/settings/bA;->bKU(Lcom/android/settings/bB;)V

    :cond_7
    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v0

    if-eqz v0, :cond_8

    const-string/jumbo v0, "font_settings_cat"

    invoke-virtual {p0, v0}, Lcom/android/settings/MiuiDisplaySettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceGroup;

    iget-object v1, p0, Lcom/android/settings/MiuiDisplaySettings;->brb:Lmiui/preference/ValuePreference;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceGroup;->removePreference(Landroid/preference/Preference;)Z

    iput-object v6, p0, Lcom/android/settings/MiuiDisplaySettings;->brb:Lmiui/preference/ValuePreference;

    iget-object v0, p0, Lcom/android/settings/MiuiDisplaySettings;->bra:Lcom/android/settings/bA;

    invoke-virtual {v0, v6}, Lcom/android/settings/bA;->bKU(Lcom/android/settings/bB;)V

    :cond_8
    const/4 v0, 0x0

    if-eqz v0, :cond_9

    const-string/jumbo v0, "font_settings_cat"

    invoke-virtual {p0, v0}, Lcom/android/settings/MiuiDisplaySettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceGroup;

    if-eqz v0, :cond_9

    iget-object v1, p0, Lcom/android/settings/MiuiDisplaySettings;->bqS:Lmiui/preference/ValuePreference;

    if-eqz v1, :cond_9

    iget-object v1, p0, Lcom/android/settings/MiuiDisplaySettings;->bqS:Lmiui/preference/ValuePreference;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceGroup;->removePreference(Landroid/preference/Preference;)Z

    iput-object v6, p0, Lcom/android/settings/MiuiDisplaySettings;->bqS:Lmiui/preference/ValuePreference;

    iget-object v0, p0, Lcom/android/settings/MiuiDisplaySettings;->bqU:Lcom/android/settings/bA;

    invoke-virtual {v0, v6}, Lcom/android/settings/bA;->bKU(Lcom/android/settings/bB;)V

    :cond_9
    const-string/jumbo v0, "font_settings_cat"

    invoke-virtual {p0, v0}, Lcom/android/settings/MiuiDisplaySettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceCategory;

    if-eqz v0, :cond_a

    invoke-virtual {v0}, Landroid/preference/PreferenceCategory;->getPreferenceCount()I

    move-result v1

    if-nez v1, :cond_a

    invoke-virtual {p0}, Lcom/android/settings/MiuiDisplaySettings;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    :cond_a
    const-string/jumbo v0, "notification_pulse"

    invoke-virtual {p0, v0}, Lcom/android/settings/MiuiDisplaySettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    iput-object v0, p0, Lcom/android/settings/MiuiDisplaySettings;->bqZ:Landroid/preference/CheckBoxPreference;

    iget-object v0, p0, Lcom/android/settings/MiuiDisplaySettings;->bqZ:Landroid/preference/CheckBoxPreference;

    if-eqz v0, :cond_15

    const-string/jumbo v0, "support_led_light"

    invoke-static {v0, v3}, Lmiui/util/FeatureParser;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_15

    invoke-virtual {p0}, Lcom/android/settings/MiuiDisplaySettings;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/MiuiDisplaySettings;->bqZ:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    iput-object v6, p0, Lcom/android/settings/MiuiDisplaySettings;->bqZ:Landroid/preference/CheckBoxPreference;

    :cond_b
    :goto_6
    const-string/jumbo v0, "font_size"

    invoke-virtual {p0, v0}, Lcom/android/settings/MiuiDisplaySettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Lcom/android/settings/WarnedListPreference;

    iput-object v0, p0, Lcom/android/settings/MiuiDisplaySettings;->bqT:Lcom/android/settings/WarnedListPreference;

    iget-object v0, p0, Lcom/android/settings/MiuiDisplaySettings;->bqT:Lcom/android/settings/WarnedListPreference;

    if-eqz v0, :cond_c

    iget-object v0, p0, Lcom/android/settings/MiuiDisplaySettings;->bqT:Lcom/android/settings/WarnedListPreference;

    invoke-virtual {v0, p0}, Lcom/android/settings/WarnedListPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    iget-object v0, p0, Lcom/android/settings/MiuiDisplaySettings;->bqT:Lcom/android/settings/WarnedListPreference;

    new-instance v1, Lcom/android/settings/dn;

    invoke-direct {v1, p0}, Lcom/android/settings/dn;-><init>(Lcom/android/settings/MiuiDisplaySettings;)V

    invoke-virtual {v0, v1}, Lcom/android/settings/WarnedListPreference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    :cond_c
    const-string/jumbo v0, "auto_rotate"

    invoke-virtual {p0, v0}, Lcom/android/settings/MiuiDisplaySettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    iput-object v0, p0, Lcom/android/settings/MiuiDisplaySettings;->bre:Landroid/preference/CheckBoxPreference;

    iget-object v0, p0, Lcom/android/settings/MiuiDisplaySettings;->bre:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, v3}, Landroid/preference/CheckBoxPreference;->setPersistent(Z)V

    return-void

    :cond_d
    iget-object v1, p0, Lcom/android/settings/MiuiDisplaySettings;->brj:Landroid/preference/CheckBoxPreference;

    iget-object v4, p0, Lcom/android/settings/MiuiDisplaySettings;->bqX:Lcom/android/settings/dc;

    invoke-virtual {p0}, Lcom/android/settings/MiuiDisplaySettings;->getActivity()Landroid/app/Activity;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/android/settings/dc;->bYq(Landroid/content/Context;)Z

    move-result v4

    invoke-virtual {v1, v4}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    goto/16 :goto_0

    :cond_e
    invoke-virtual {p0}, Lcom/android/settings/MiuiDisplaySettings;->bWB()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v1, "gesture_wakeup"

    invoke-static {v0, v1, v3, v3}, Landroid/provider/MiuiSettings$System;->getBooleanForUser(Landroid/content/ContentResolver;Ljava/lang/String;ZI)Z

    move-result v0

    if-eqz v0, :cond_f

    iget-object v0, p0, Lcom/android/settings/MiuiDisplaySettings;->bqV:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, v2}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    goto/16 :goto_1

    :cond_f
    iget-object v0, p0, Lcom/android/settings/MiuiDisplaySettings;->bqV:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, v3}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    goto/16 :goto_1

    :cond_10
    iget-object v0, p0, Lcom/android/settings/MiuiDisplaySettings;->brd:Landroid/preference/CheckBoxPreference;

    invoke-virtual {p0}, Lcom/android/settings/MiuiDisplaySettings;->bWB()Landroid/content/ContentResolver;

    move-result-object v1

    const-string/jumbo v4, "pick_up_gesture_wakeup_mode"

    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v5

    invoke-static {v1, v4, v3, v5}, Landroid/provider/MiuiSettings$System;->getBooleanForUser(Landroid/content/ContentResolver;Ljava/lang/String;ZI)Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    goto/16 :goto_2

    :cond_11
    invoke-virtual {p0}, Lcom/android/settings/MiuiDisplaySettings;->bWB()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v1, "aod_mode"

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    if-eqz v0, :cond_12

    iget-object v0, p0, Lcom/android/settings/MiuiDisplaySettings;->bqQ:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, v2}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    goto/16 :goto_3

    :cond_12
    iget-object v0, p0, Lcom/android/settings/MiuiDisplaySettings;->bqQ:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, v3}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    goto/16 :goto_3

    :cond_13
    new-instance v0, Lcom/android/settings/dj;

    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    invoke-direct {v0, p0, v1}, Lcom/android/settings/dj;-><init>(Lcom/android/settings/MiuiDisplaySettings;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/android/settings/MiuiDisplaySettings;->bqY:Landroid/database/ContentObserver;

    invoke-virtual {p0}, Lcom/android/settings/MiuiDisplaySettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v1, "screen_monochrome_mode_enabled"

    invoke-static {v1}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iget-object v4, p0, Lcom/android/settings/MiuiDisplaySettings;->bqY:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1, v3, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    goto/16 :goto_4

    :cond_14
    new-instance v0, Lcom/android/settings/dk;

    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    invoke-direct {v0, p0, v1}, Lcom/android/settings/dk;-><init>(Lcom/android/settings/MiuiDisplaySettings;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/android/settings/MiuiDisplaySettings;->brc:Landroid/database/ContentObserver;

    invoke-virtual {p0}, Lcom/android/settings/MiuiDisplaySettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v1, "screen_paper_mode_enabled"

    invoke-static {v1}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iget-object v4, p0, Lcom/android/settings/MiuiDisplaySettings;->brc:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1, v3, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    goto/16 :goto_5

    :cond_15
    iget-object v0, p0, Lcom/android/settings/MiuiDisplaySettings;->bqZ:Landroid/preference/CheckBoxPreference;

    if-eqz v0, :cond_b

    :try_start_0
    iget-object v1, p0, Lcom/android/settings/MiuiDisplaySettings;->bqZ:Landroid/preference/CheckBoxPreference;

    invoke-virtual {p0}, Lcom/android/settings/MiuiDisplaySettings;->bWB()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v4, "notification_light_pulse"

    invoke-static {v0, v4}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;)I

    move-result v0

    if-ne v0, v2, :cond_16

    move v0, v2

    :goto_7
    invoke-virtual {v1, v0}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    iget-object v0, p0, Lcom/android/settings/MiuiDisplaySettings;->bqZ:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, p0}, Landroid/preference/CheckBoxPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V
    :try_end_0
    .catch Landroid/provider/Settings$SettingNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_6

    :catch_0
    move-exception v0

    goto/16 :goto_6

    :cond_16
    move v0, v3

    goto :goto_7
.end method

.method public onCreateDialog(I)Landroid/app/Dialog;
    .locals 3

    const/16 v0, 0x64

    if-ne p1, v0, :cond_0

    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/android/settings/MiuiDisplaySettings;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v1, 0x7f121287

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f121286

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    new-instance v1, Lcom/android/settings/do;

    invoke-direct {v1, p0}, Lcom/android/settings/do;-><init>(Lcom/android/settings/MiuiDisplaySettings;)V

    const v2, 0x7f121285

    invoke-virtual {v0, v2, v1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f121284

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    return-object v0

    :cond_0
    invoke-super {p0, p1}, Lcom/android/settings/DisplaySettings;->onCreateDialog(I)Landroid/app/Dialog;

    move-result-object v0

    return-object v0
.end method

.method public onDestroy()V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/android/settings/MiuiDisplaySettings;->brc:Landroid/database/ContentObserver;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/android/settings/MiuiDisplaySettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/MiuiDisplaySettings;->brc:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    iput-object v2, p0, Lcom/android/settings/MiuiDisplaySettings;->brc:Landroid/database/ContentObserver;

    :cond_0
    iget-object v0, p0, Lcom/android/settings/MiuiDisplaySettings;->bqY:Landroid/database/ContentObserver;

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/android/settings/MiuiDisplaySettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/MiuiDisplaySettings;->bqY:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    iput-object v2, p0, Lcom/android/settings/MiuiDisplaySettings;->bqY:Landroid/database/ContentObserver;

    :cond_1
    invoke-super {p0}, Lcom/android/settings/DisplaySettings;->onDestroy()V

    return-void
.end method

.method public onPause()V
    .locals 2

    invoke-virtual {p0}, Lcom/android/settings/MiuiDisplaySettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/MiuiDisplaySettings;->brf:Lcom/android/internal/view/RotationPolicy$RotationPolicyListener;

    invoke-static {v0, v1}, Lcom/android/internal/view/RotationPolicy;->unregisterRotationPolicyListener(Landroid/content/Context;Lcom/android/internal/view/RotationPolicy$RotationPolicyListener;)V

    invoke-super {p0}, Lcom/android/settings/DisplaySettings;->onPause()V

    return-void
.end method

.method public onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 5

    const/4 v1, 0x1

    const/4 v0, 0x0

    iget-object v2, p0, Lcom/android/settings/MiuiDisplaySettings;->bqZ:Landroid/preference/CheckBoxPreference;

    if-ne p1, v2, :cond_1

    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-virtual {p0}, Lcom/android/settings/MiuiDisplaySettings;->bWB()Landroid/content/ContentResolver;

    move-result-object v3

    const-string/jumbo v4, "notification_light_pulse"

    if-eqz v2, :cond_0

    move v0, v1

    :cond_0
    invoke-static {v3, v4, v0}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    return v1

    :cond_1
    const-string/jumbo v1, "font_size"

    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {p0, p2}, Lcom/android/settings/MiuiDisplaySettings;->bet(Ljava/lang/Object;)V

    :cond_2
    return v0
.end method

.method public onPreferenceTreeClick(Landroid/preference/PreferenceScreen;Landroid/preference/Preference;)Z
    .locals 5

    const/4 v0, 0x1

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/android/settings/MiuiDisplaySettings;->brj:Landroid/preference/CheckBoxPreference;

    if-ne p2, v2, :cond_1

    iget-object v1, p0, Lcom/android/settings/MiuiDisplaySettings;->brj:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v1}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v1

    iget-object v2, p0, Lcom/android/settings/MiuiDisplaySettings;->bqX:Lcom/android/settings/dc;

    invoke-virtual {p0}, Lcom/android/settings/MiuiDisplaySettings;->getActivity()Landroid/app/Activity;

    move-result-object v3

    iget-object v4, p0, Lcom/android/settings/MiuiDisplaySettings;->brj:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v4}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v4

    invoke-virtual {v2, v3, v4}, Lcom/android/settings/dc;->bYr(Landroid/content/Context;Z)V

    if-eqz v1, :cond_0

    invoke-direct {p0, v0}, Lcom/android/settings/MiuiDisplaySettings;->bem(Z)V

    :cond_0
    :goto_0
    invoke-super {p0, p1, p2}, Lcom/android/settings/DisplaySettings;->onPreferenceTreeClick(Landroid/preference/PreferenceScreen;Landroid/preference/Preference;)Z

    move-result v0

    return v0

    :cond_1
    iget-object v2, p0, Lcom/android/settings/MiuiDisplaySettings;->bqW:Landroid/preference/CheckBoxPreference;

    if-ne p2, v2, :cond_3

    const-string/jumbo v1, "persist.sys.line_breaking"

    iget-object v0, p0, Lcom/android/settings/MiuiDisplaySettings;->bqW:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_2

    const-string/jumbo v0, "true"

    :goto_1
    invoke-static {v1, v0}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    const-string/jumbo v0, "false"

    goto :goto_1

    :cond_3
    iget-object v2, p0, Lcom/android/settings/MiuiDisplaySettings;->bqV:Landroid/preference/CheckBoxPreference;

    if-ne p2, v2, :cond_4

    invoke-virtual {p0}, Lcom/android/settings/MiuiDisplaySettings;->bWB()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v2, "gesture_wakeup"

    iget-object v3, p0, Lcom/android/settings/MiuiDisplaySettings;->bqV:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v3}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v3

    invoke-static {v0, v2, v3, v1}, Landroid/provider/MiuiSettings$System;->putBooleanForUser(Landroid/content/ContentResolver;Ljava/lang/String;ZI)Z

    goto :goto_0

    :cond_4
    iget-object v2, p0, Lcom/android/settings/MiuiDisplaySettings;->brd:Landroid/preference/CheckBoxPreference;

    if-ne p2, v2, :cond_5

    invoke-virtual {p0}, Lcom/android/settings/MiuiDisplaySettings;->bWB()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v1, "pick_up_gesture_wakeup_mode"

    iget-object v2, p0, Lcom/android/settings/MiuiDisplaySettings;->brd:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v2}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v2

    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v3

    invoke-static {v0, v1, v2, v3}, Landroid/provider/MiuiSettings$System;->putBooleanForUser(Landroid/content/ContentResolver;Ljava/lang/String;ZI)Z

    goto :goto_0

    :cond_5
    iget-object v2, p0, Lcom/android/settings/MiuiDisplaySettings;->bqQ:Landroid/preference/CheckBoxPreference;

    if-ne p2, v2, :cond_7

    invoke-virtual {p0}, Lcom/android/settings/MiuiDisplaySettings;->bWB()Landroid/content/ContentResolver;

    move-result-object v2

    const-string/jumbo v3, "aod_mode"

    iget-object v4, p0, Lcom/android/settings/MiuiDisplaySettings;->bqQ:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v4}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v4

    if-eqz v4, :cond_6

    :goto_2
    invoke-static {v2, v3, v0}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    goto :goto_0

    :cond_6
    move v0, v1

    goto :goto_2

    :cond_7
    iget-object v0, p0, Lcom/android/settings/MiuiDisplaySettings;->bqS:Lmiui/preference/ValuePreference;

    if-ne p2, v0, :cond_8

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string/jumbo v2, "android.intent.action.VIEW"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const-string/jumbo v2, "android.intent.category.DEFAULT"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    const-string/jumbo v2, "android.intent.category.BROWSABLE"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "theme://zhuti.xiaomi.com/list?S.REQUEST_RESOURCE_CODE=fonts&miback=true&miref="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lcom/android/settings/MiuiDisplaySettings;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/Activity;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    const-string/jumbo v2, ":miui:starting_window_label"

    const-string/jumbo v3, ""

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const v2, 0x10008000

    invoke-virtual {v0, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    :try_start_0
    invoke-virtual {p0, v0}, Lcom/android/settings/MiuiDisplaySettings;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_3
    invoke-virtual {p0}, Lcom/android/settings/MiuiDisplaySettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/android/settings/dc;->bYs(Landroid/app/Activity;)Landroid/util/Pair;

    move-result-object v1

    invoke-virtual {p0}, Lcom/android/settings/MiuiDisplaySettings;->getActivity()Landroid/app/Activity;

    move-result-object v2

    iget-object v0, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v3

    iget-object v0, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v2, v3, v0}, Landroid/app/Activity;->overridePendingTransition(II)V

    goto/16 :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {p0}, Lcom/android/settings/MiuiDisplaySettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const v2, 0x7f121268

    invoke-static {v0, v2, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    goto :goto_3

    :cond_8
    iget-object v0, p0, Lcom/android/settings/MiuiDisplaySettings;->bre:Landroid/preference/CheckBoxPreference;

    if-ne p2, v0, :cond_0

    invoke-virtual {p0}, Lcom/android/settings/MiuiDisplaySettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/MiuiDisplaySettings;->bre:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v1}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v1

    xor-int/lit8 v1, v1, 0x1

    invoke-static {v0, v1}, Lcom/android/internal/view/RotationPolicy;->setRotationLockForAccessibility(Landroid/content/Context;Z)V

    goto/16 :goto_0
.end method

.method public onResume()V
    .locals 2

    invoke-super {p0}, Lcom/android/settings/DisplaySettings;->onResume()V

    invoke-direct {p0}, Lcom/android/settings/MiuiDisplaySettings;->bes()V

    invoke-direct {p0}, Lcom/android/settings/MiuiDisplaySettings;->beq()V

    invoke-direct {p0}, Lcom/android/settings/MiuiDisplaySettings;->ber()V

    invoke-virtual {p0}, Lcom/android/settings/MiuiDisplaySettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/MiuiDisplaySettings;->brf:Lcom/android/internal/view/RotationPolicy$RotationPolicyListener;

    invoke-static {v0, v1}, Lcom/android/internal/view/RotationPolicy;->registerRotationPolicyListener(Landroid/content/Context;Lcom/android/internal/view/RotationPolicy$RotationPolicyListener;)V

    invoke-direct {p0}, Lcom/android/settings/MiuiDisplaySettings;->bep()V

    return-void
.end method
