.class public Lcom/android/settings/HeadsetCalibrateFragment;
.super Lcom/android/settings/BaseFragment;
.source "HeadsetCalibrateFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Ljava/lang/Runnable;


# instance fields
.field private caA:Z

.field private final caB:Landroid/content/DialogInterface$OnClickListener;

.field private caC:Z

.field private caD:Z

.field private caE:Ljava/lang/String;

.field private caF:Landroid/view/View;

.field private caG:Landroid/view/View;

.field private caH:Landroid/view/View;

.field private caI:Landroid/view/View;

.field private caJ:Landroid/widget/TextView;

.field private caK:Landroid/widget/TextView;

.field private caz:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/android/settings/BaseFragment;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/settings/HeadsetCalibrateFragment;->caE:Ljava/lang/String;

    iput-boolean v1, p0, Lcom/android/settings/HeadsetCalibrateFragment;->caD:Z

    iput-boolean v1, p0, Lcom/android/settings/HeadsetCalibrateFragment;->caA:Z

    iput-boolean v1, p0, Lcom/android/settings/HeadsetCalibrateFragment;->caz:Z

    new-instance v0, Lcom/android/settings/kt;

    invoke-direct {v0, p0}, Lcom/android/settings/kt;-><init>(Lcom/android/settings/HeadsetCalibrateFragment;)V

    iput-object v0, p0, Lcom/android/settings/HeadsetCalibrateFragment;->caB:Landroid/content/DialogInterface$OnClickListener;

    return-void
.end method

.method private bTb(Z)V
    .locals 3

    const/4 v2, 0x0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/settings/HeadsetCalibrateFragment;->caA:Z

    invoke-static {p1}, Lcom/android/settings/cd;->bQr(Z)V

    iget-object v0, p0, Lcom/android/settings/HeadsetCalibrateFragment;->caK:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/settings/HeadsetCalibrateFragment;->caJ:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/settings/HeadsetCalibrateFragment;->caF:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/settings/HeadsetCalibrateFragment;->caG:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/settings/HeadsetCalibrateFragment;->caH:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    invoke-static {}, Lcom/android/settings/cd;->bQo()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "music"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/HeadsetCalibrateFragment;->caK:Landroid/widget/TextView;

    const v1, 0x7f120b09

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    iget-object v0, p0, Lcom/android/settings/HeadsetCalibrateFragment;->caJ:Landroid/widget/TextView;

    const v1, 0x7f120b08

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/settings/HeadsetCalibrateFragment;->caK:Landroid/widget/TextView;

    const v1, 0x7f120b0c

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    iget-object v0, p0, Lcom/android/settings/HeadsetCalibrateFragment;->caJ:Landroid/widget/TextView;

    const v1, 0x7f120b0b

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0
.end method

.method private bTc()Z
    .locals 3

    iget-boolean v0, p0, Lcom/android/settings/HeadsetCalibrateFragment;->caA:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/android/settings/HeadsetCalibrateFragment;->caz:Z

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/android/settings/HeadsetCalibrateFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v1, 0x1040014

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f120b06

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/HeadsetCalibrateFragment;->caB:Landroid/content/DialogInterface$OnClickListener;

    const v2, 0x104000a

    invoke-virtual {v0, v2, v1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/HeadsetCalibrateFragment;->caB:Landroid/content/DialogInterface$OnClickListener;

    const/high16 v2, 0x1040000

    invoke-virtual {v0, v2, v1}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    const/4 v0, 0x1

    return v0

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method static synthetic bTd(Lcom/android/settings/HeadsetCalibrateFragment;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/android/settings/HeadsetCalibrateFragment;->caz:Z

    return p1
.end method


# virtual methods
.method public finish()V
    .locals 1

    iget-boolean v0, p0, Lcom/android/settings/HeadsetCalibrateFragment;->caC:Z

    if-eqz v0, :cond_0

    return-void

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/settings/HeadsetCalibrateFragment;->caC:Z

    invoke-super {p0}, Lcom/android/settings/BaseFragment;->finish()V

    return-void
.end method

.method public onBackPressed()Z
    .locals 2

    invoke-direct {p0}, Lcom/android/settings/HeadsetCalibrateFragment;->bTc()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/android/settings/HeadsetCalibrateFragment;->caC:Z

    :cond_0
    return v0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 1

    iget-object v0, p0, Lcom/android/settings/HeadsetCalibrateFragment;->caF:Landroid/view/View;

    if-ne p1, v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/settings/HeadsetCalibrateFragment;->caz:Z

    invoke-virtual {p0}, Lcom/android/settings/HeadsetCalibrateFragment;->finish()V

    :cond_0
    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/settings/HeadsetCalibrateFragment;->caA:Z

    if-nez v0, :cond_2

    const/16 v0, 0x19

    if-eq p1, v0, :cond_0

    const/16 v0, 0x18

    if-ne p1, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    return v0

    :cond_1
    const/16 v0, 0x4f

    if-eq p1, v0, :cond_0

    const/16 v0, 0x55

    if-eq p1, v0, :cond_0

    :cond_2
    invoke-super {p0, p1, p2}, Lcom/android/settings/BaseFragment;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    return v0
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 4

    const/16 v3, 0x18

    const/4 v0, 0x0

    const/4 v1, 0x1

    iget-boolean v2, p0, Lcom/android/settings/HeadsetCalibrateFragment;->caA:Z

    if-nez v2, :cond_4

    const/16 v2, 0x19

    if-eq p1, v2, :cond_0

    if-ne p1, v3, :cond_2

    :cond_0
    iget-object v2, p0, Lcom/android/settings/HeadsetCalibrateFragment;->caE:Ljava/lang/String;

    invoke-static {v2}, Lcom/android/settings/cd;->bQq(Ljava/lang/String;)V

    if-eq p1, v3, :cond_1

    move v0, v1

    :cond_1
    invoke-direct {p0, v0}, Lcom/android/settings/HeadsetCalibrateFragment;->bTb(Z)V

    return v1

    :cond_2
    const/16 v2, 0x4f

    if-eq p1, v2, :cond_3

    const/16 v2, 0x55

    if-ne p1, v2, :cond_4

    :cond_3
    invoke-virtual {p0}, Lcom/android/settings/HeadsetCalibrateFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    const v3, 0x7f120b0d

    invoke-static {v2, v3, v0}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    return v1

    :cond_4
    invoke-super {p0, p1, p2}, Lcom/android/settings/BaseFragment;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v0

    return v0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x102002c

    if-ne v0, v1, :cond_0

    invoke-direct {p0}, Lcom/android/settings/HeadsetCalibrateFragment;->bTc()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    return v0

    :cond_0
    invoke-super {p0, p1}, Lcom/android/settings/BaseFragment;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    return v0
.end method

.method public onPause()V
    .locals 1

    iget-object v0, p0, Lcom/android/settings/HeadsetCalibrateFragment;->caI:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->removeCallbacks(Ljava/lang/Runnable;)Z

    iget-boolean v0, p0, Lcom/android/settings/HeadsetCalibrateFragment;->caz:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/HeadsetCalibrateFragment;->caE:Ljava/lang/String;

    invoke-static {v0}, Lcom/android/settings/cd;->bQq(Ljava/lang/String;)V

    iget-boolean v0, p0, Lcom/android/settings/HeadsetCalibrateFragment;->caD:Z

    invoke-static {v0}, Lcom/android/settings/cd;->bQr(Z)V

    :cond_0
    invoke-super {p0}, Lcom/android/settings/BaseFragment;->onPause()V

    return-void
.end method

.method public onResume()V
    .locals 4

    invoke-super {p0}, Lcom/android/settings/BaseFragment;->onResume()V

    invoke-static {}, Lcom/android/settings/cd;->bQo()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/HeadsetCalibrateFragment;->caE:Ljava/lang/String;

    const-string/jumbo v0, "volume"

    invoke-static {v0}, Lcom/android/settings/cd;->bQq(Ljava/lang/String;)V

    invoke-static {}, Lcom/android/settings/cd;->bQp()Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/settings/HeadsetCalibrateFragment;->caD:Z

    const/4 v0, 0x0

    invoke-static {v0}, Lcom/android/settings/cd;->bQr(Z)V

    iget-object v0, p0, Lcom/android/settings/HeadsetCalibrateFragment;->caI:Landroid/view/View;

    const-wide/16 v2, 0x190

    invoke-virtual {v0, p0, v2, v3}, Landroid/view/View;->postDelayed(Ljava/lang/Runnable;J)Z

    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 3

    const/16 v2, 0x8

    const v0, 0x7f0a01df

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/settings/HeadsetCalibrateFragment;->caK:Landroid/widget/TextView;

    const v0, 0x7f0a01de

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/settings/HeadsetCalibrateFragment;->caJ:Landroid/widget/TextView;

    const v0, 0x7f0a013f

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/HeadsetCalibrateFragment;->caI:Landroid/view/View;

    const v0, 0x7f0a00c2

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/HeadsetCalibrateFragment;->caG:Landroid/view/View;

    const v0, 0x7f0a00c3

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/HeadsetCalibrateFragment;->caH:Landroid/view/View;

    const v0, 0x7f0a0072

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/HeadsetCalibrateFragment;->caF:Landroid/view/View;

    iget-object v0, p0, Lcom/android/settings/HeadsetCalibrateFragment;->caK:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/settings/HeadsetCalibrateFragment;->caJ:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/settings/HeadsetCalibrateFragment;->caF:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/settings/HeadsetCalibrateFragment;->caG:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/settings/HeadsetCalibrateFragment;->caH:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/settings/HeadsetCalibrateFragment;->caF:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-super {p0, p1, p2}, Lcom/android/settings/BaseFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    return-void
.end method

.method public run()V
    .locals 4

    const/16 v0, 0x8

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/android/settings/HeadsetCalibrateFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {p0}, Lcom/android/settings/HeadsetCalibrateFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Activity;->isResumed()Z

    move-result v2

    xor-int/lit8 v2, v2, 0x1

    if-eqz v2, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/android/settings/HeadsetCalibrateFragment;->caI:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->removeCallbacks(Ljava/lang/Runnable;)Z

    return-void

    :cond_1
    iget-boolean v2, p0, Lcom/android/settings/HeadsetCalibrateFragment;->caA:Z

    if-eqz v2, :cond_2

    iget-object v1, p0, Lcom/android/settings/HeadsetCalibrateFragment;->caI:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    :goto_0
    return-void

    :cond_2
    iget-object v2, p0, Lcom/android/settings/HeadsetCalibrateFragment;->caI:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getVisibility()I

    move-result v2

    if-nez v2, :cond_3

    :goto_1
    iget-object v1, p0, Lcom/android/settings/HeadsetCalibrateFragment;->caI:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/settings/HeadsetCalibrateFragment;->caI:Landroid/view/View;

    const-wide/16 v2, 0x190

    invoke-virtual {v0, p0, v2, v3}, Landroid/view/View;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_1
.end method

.method protected vB(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2

    const v0, 0x7f0d0107

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method
