.class Lcom/android/settings/privacy/i;
.super Landroid/os/AsyncTask;
.source "PrivacyGrantDialog.java"


# instance fields
.field private LS:Landroid/content/Context;

.field final synthetic LT:Lcom/android/settings/privacy/PrivacyGrantDialog;

.field private packageName:Ljava/lang/String;

.field private url:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/android/settings/privacy/PrivacyGrantDialog;Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    iput-object p1, p0, Lcom/android/settings/privacy/i;->LT:Lcom/android/settings/privacy/PrivacyGrantDialog;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    invoke-virtual {p2}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/privacy/i;->LS:Landroid/content/Context;

    iput-object p3, p0, Lcom/android/settings/privacy/i;->packageName:Ljava/lang/String;

    iput-object p4, p0, Lcom/android/settings/privacy/i;->url:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Boolean;
    .locals 3

    iget-object v0, p0, Lcom/android/settings/privacy/i;->LS:Landroid/content/Context;

    iget-object v1, p0, Lcom/android/settings/privacy/i;->url:Ljava/lang/String;

    iget-object v2, p0, Lcom/android/settings/privacy/i;->packageName:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/android/settings/privacy/k;->FT(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/android/settings/privacy/i;->doInBackground([Ljava/lang/Void;)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method
