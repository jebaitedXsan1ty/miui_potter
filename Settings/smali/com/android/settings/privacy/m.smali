.class final Lcom/android/settings/privacy/m;
.super Ljava/lang/Object;
.source "PrivacyRevocationSettings.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field final synthetic LW:Lcom/android/settings/privacy/PrivacyRevocationSettings;

.field final synthetic LX:Lcom/android/settings/privacy/b;

.field final synthetic val$context:Landroid/content/Context;


# direct methods
.method constructor <init>(Lcom/android/settings/privacy/PrivacyRevocationSettings;Landroid/content/Context;Lcom/android/settings/privacy/b;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/privacy/m;->LW:Lcom/android/settings/privacy/PrivacyRevocationSettings;

    iput-object p2, p0, Lcom/android/settings/privacy/m;->val$context:Landroid/content/Context;

    iput-object p3, p0, Lcom/android/settings/privacy/m;->LX:Lcom/android/settings/privacy/b;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 5

    :try_start_0
    iget-object v0, p0, Lcom/android/settings/privacy/m;->val$context:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/settings/cloud/a/f;->aKn(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/privacy/m;->val$context:Landroid/content/Context;

    const v1, 0x7f120d27

    invoke-static {v0, v1}, Lcom/android/settings/cloud/a/f;->aKo(Landroid/content/Context;I)V

    iget-object v0, p0, Lcom/android/settings/privacy/m;->LW:Lcom/android/settings/privacy/PrivacyRevocationSettings;

    invoke-static {v0}, Lcom/android/settings/privacy/PrivacyRevocationSettings;->Fw(Lcom/android/settings/privacy/PrivacyRevocationSettings;)Lcom/android/settings/privacy/f;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/settings/privacy/f;->notifyDataSetChanged()V

    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/settings/privacy/m;->val$context:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/android/settings/privacy/k;->FU(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string/jumbo v0, "https://appauth.account.xiaomi.com/pass/revokeuser"

    :goto_0
    new-instance v1, Lcom/android/settings/privacy/c;

    iget-object v2, p0, Lcom/android/settings/privacy/m;->LW:Lcom/android/settings/privacy/PrivacyRevocationSettings;

    iget-object v3, p0, Lcom/android/settings/privacy/m;->val$context:Landroid/content/Context;

    iget-object v4, p0, Lcom/android/settings/privacy/m;->LX:Lcom/android/settings/privacy/b;

    invoke-direct {v1, v2, v3, v4, v0}, Lcom/android/settings/privacy/c;-><init>(Lcom/android/settings/privacy/PrivacyRevocationSettings;Landroid/content/Context;Lcom/android/settings/privacy/b;Ljava/lang/String;)V

    sget-object v0, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Void;

    invoke-virtual {v1, v0, v2}, Lcom/android/settings/privacy/c;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    :goto_1
    return-void

    :cond_1
    const-string/jumbo v0, "https://appauth.account.xiaomi.com/pass/revokedev"
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string/jumbo v1, "PrivacyRevocationSettings"

    const-string/jumbo v2, "MiuiSettings privacy modify status:"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1
.end method
