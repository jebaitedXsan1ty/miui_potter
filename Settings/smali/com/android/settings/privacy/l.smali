.class final Lcom/android/settings/privacy/l;
.super Lcom/android/settings/privacy/j;
.source "PrivacyRevocationSettings.java"


# instance fields
.field final synthetic LV:Lcom/android/settings/privacy/PrivacyRevocationSettings;


# direct methods
.method constructor <init>(Lcom/android/settings/privacy/PrivacyRevocationSettings;Landroid/content/Context;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/privacy/l;->LV:Lcom/android/settings/privacy/PrivacyRevocationSettings;

    invoke-direct {p0, p2}, Lcom/android/settings/privacy/j;-><init>(Landroid/content/Context;)V

    return-void
.end method


# virtual methods
.method public bridge synthetic loadInBackground()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/android/settings/privacy/l;->loadInBackground()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public loadInBackground()Ljava/util/List;
    .locals 6

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    const/4 v0, 0x0

    :goto_0
    invoke-static {}, Lcom/android/settings/privacy/PrivacyRevocationSettings;->-get0()[Ljava/lang/String;

    move-result-object v2

    array-length v2, v2

    if-ge v0, v2, :cond_1

    invoke-static {}, Lcom/android/settings/privacy/PrivacyRevocationSettings;->-get0()[Ljava/lang/String;

    move-result-object v2

    aget-object v2, v2, v0

    iget-object v3, p0, Lcom/android/settings/privacy/l;->LV:Lcom/android/settings/privacy/PrivacyRevocationSettings;

    invoke-virtual {v3}, Lcom/android/settings/privacy/PrivacyRevocationSettings;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3, v2}, Lcom/android/settings/privacy/h;->FL(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    new-instance v3, Lcom/android/settings/privacy/b;

    invoke-direct {v3}, Lcom/android/settings/privacy/b;-><init>()V

    iput-object v2, v3, Lcom/android/settings/privacy/b;->packageName:Ljava/lang/String;

    iget-object v4, p0, Lcom/android/settings/privacy/l;->LV:Lcom/android/settings/privacy/PrivacyRevocationSettings;

    invoke-virtual {v4}, Lcom/android/settings/privacy/PrivacyRevocationSettings;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4, v2}, Lcom/android/settings/privacy/h;->FM(Landroid/content/Context;Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    iput-object v4, v3, Lcom/android/settings/privacy/b;->Lz:Landroid/graphics/drawable/Drawable;

    iget-object v4, p0, Lcom/android/settings/privacy/l;->LV:Lcom/android/settings/privacy/PrivacyRevocationSettings;

    invoke-virtual {v4}, Lcom/android/settings/privacy/PrivacyRevocationSettings;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4, v2}, Lcom/android/settings/privacy/h;->FN(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lcom/android/settings/privacy/b;->LB:Ljava/lang/String;

    iget-object v4, p0, Lcom/android/settings/privacy/l;->LV:Lcom/android/settings/privacy/PrivacyRevocationSettings;

    iget-object v5, p0, Lcom/android/settings/privacy/l;->LV:Lcom/android/settings/privacy/PrivacyRevocationSettings;

    invoke-virtual {v5}, Lcom/android/settings/privacy/PrivacyRevocationSettings;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    invoke-static {v4, v5, v2}, Lcom/android/settings/privacy/PrivacyRevocationSettings;->Fx(Lcom/android/settings/privacy/PrivacyRevocationSettings;Landroid/content/Context;Ljava/lang/String;)Z

    move-result v2

    iput-boolean v2, v3, Lcom/android/settings/privacy/b;->LA:Z

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    return-object v1
.end method
