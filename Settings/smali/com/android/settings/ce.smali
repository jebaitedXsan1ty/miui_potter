.class public Lcom/android/settings/ce;
.super Ljava/lang/Object;
.source "MaxAspectRatioSettings.java"


# instance fields
.field private bWA:Landroid/content/pm/ApplicationInfo;

.field private bWB:I

.field private bWC:Z

.field private bWD:I

.field final synthetic bWE:Lcom/android/settings/MaxAspectRatioSettings;

.field public bWz:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/android/settings/MaxAspectRatioSettings;Landroid/content/pm/ApplicationInfo;ZI)V
    .locals 3

    iput-object p1, p0, Lcom/android/settings/ce;->bWE:Lcom/android/settings/MaxAspectRatioSettings;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, -0x1

    iput v0, p0, Lcom/android/settings/ce;->bWD:I

    iput-object p2, p0, Lcom/android/settings/ce;->bWA:Landroid/content/pm/ApplicationInfo;

    invoke-static {p1}, Lcom/android/settings/MaxAspectRatioSettings;->bQx(Lcom/android/settings/MaxAspectRatioSettings;)Landroid/content/pm/PackageManager;

    move-result-object v0

    invoke-virtual {p2, v0}, Landroid/content/pm/ApplicationInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "\\u00A0"

    const-string/jumbo v2, " "

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/ce;->bWz:Ljava/lang/String;

    const/4 v0, 0x0

    iput v0, p0, Lcom/android/settings/ce;->bWD:I

    iput-boolean p3, p0, Lcom/android/settings/ce;->bWC:Z

    iput p4, p0, Lcom/android/settings/ce;->bWB:I

    return-void
.end method

.method public constructor <init>(Lcom/android/settings/MaxAspectRatioSettings;Ljava/lang/String;)V
    .locals 1

    iput-object p1, p0, Lcom/android/settings/ce;->bWE:Lcom/android/settings/MaxAspectRatioSettings;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, -0x1

    iput v0, p0, Lcom/android/settings/ce;->bWD:I

    iput-object p2, p0, Lcom/android/settings/ce;->bWz:Ljava/lang/String;

    const/4 v0, 0x1

    iput v0, p0, Lcom/android/settings/ce;->bWD:I

    return-void
.end method

.method static synthetic bQB(Lcom/android/settings/ce;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/settings/ce;->bWC:Z

    return v0
.end method

.method static synthetic bQC(Lcom/android/settings/ce;)I
    .locals 1

    iget v0, p0, Lcom/android/settings/ce;->bWD:I

    return v0
.end method

.method static synthetic bQD(Lcom/android/settings/ce;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/android/settings/ce;->bWC:Z

    return p1
.end method


# virtual methods
.method public bQA()Z
    .locals 2

    const/4 v0, 0x1

    iget v1, p0, Lcom/android/settings/ce;->bWB:I

    if-eq v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public bQz()Ljava/lang/String;
    .locals 2

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/android/settings/ce;->bWA:Landroid/content/pm/ApplicationInfo;

    if-nez v1, :cond_0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/android/settings/ce;->bWA:Landroid/content/pm/ApplicationInfo;

    iget-object v0, v0, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    goto :goto_0
.end method
