.class final Lcom/android/settings/fuelgauge/BatteryIndicatorStyle$1;
.super Ljava/lang/Object;
.source "BatteryIndicatorStyle.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field final synthetic SZ:Lcom/android/settings/fuelgauge/BatteryIndicatorStyle;


# direct methods
.method constructor <init>(Lcom/android/settings/fuelgauge/BatteryIndicatorStyle;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/fuelgauge/BatteryIndicatorStyle$1;->SZ:Lcom/android/settings/fuelgauge/BatteryIndicatorStyle;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 3

    iget-object v0, p0, Lcom/android/settings/fuelgauge/BatteryIndicatorStyle$1;->SZ:Lcom/android/settings/fuelgauge/BatteryIndicatorStyle;

    invoke-static {v0}, Lcom/android/settings/fuelgauge/BatteryIndicatorStyle;->JT(Lcom/android/settings/fuelgauge/BatteryIndicatorStyle;)[Ljava/lang/CharSequence;

    move-result-object v0

    array-length v0, v0

    if-ge p2, v0, :cond_0

    if-ltz p2, :cond_0

    iget-object v0, p0, Lcom/android/settings/fuelgauge/BatteryIndicatorStyle$1;->SZ:Lcom/android/settings/fuelgauge/BatteryIndicatorStyle;

    invoke-static {v0}, Lcom/android/settings/fuelgauge/BatteryIndicatorStyle;->JT(Lcom/android/settings/fuelgauge/BatteryIndicatorStyle;)[Ljava/lang/CharSequence;

    move-result-object v0

    aget-object v0, v0, p2

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    iget-object v1, p0, Lcom/android/settings/fuelgauge/BatteryIndicatorStyle$1;->SZ:Lcom/android/settings/fuelgauge/BatteryIndicatorStyle;

    invoke-static {v1}, Lcom/android/settings/fuelgauge/BatteryIndicatorStyle;->JU(Lcom/android/settings/fuelgauge/BatteryIndicatorStyle;)Landroid/content/ContentResolver;

    move-result-object v1

    const-string/jumbo v2, "battery_indicator_style"

    invoke-static {v1, v2, v0}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    :cond_0
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    iget-object v0, p0, Lcom/android/settings/fuelgauge/BatteryIndicatorStyle$1;->SZ:Lcom/android/settings/fuelgauge/BatteryIndicatorStyle;

    invoke-virtual {v0}, Lcom/android/settings/fuelgauge/BatteryIndicatorStyle;->finish()V

    return-void
.end method
