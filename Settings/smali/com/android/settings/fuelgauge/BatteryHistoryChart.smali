.class public Lcom/android/settings/fuelgauge/BatteryHistoryChart;
.super Landroid/view/View;
.source "BatteryHistoryChart.java"


# instance fields
.field final PA:Landroid/graphics/Path;

.field PB:I

.field PC:Ljava/lang/String;

.field PD:I

.field final PE:Landroid/graphics/Paint;

.field final PF:Landroid/graphics/Path;

.field final PG:Ljava/util/ArrayList;

.field final PH:Landroid/graphics/Paint;

.field final PI:Landroid/graphics/Path;

.field final PJ:Landroid/graphics/Paint;

.field PK:Ljava/lang/String;

.field PL:I

.field PM:Ljava/lang/String;

.field PN:I

.field PO:J

.field PP:J

.field PQ:Ljava/lang/String;

.field PR:I

.field final PS:Landroid/graphics/Paint;

.field final PT:Landroid/graphics/Path;

.field PU:Ljava/lang/String;

.field PV:I

.field final PW:Landroid/graphics/Paint;

.field final PX:Landroid/graphics/Path;

.field PY:Z

.field PZ:Z

.field final Pb:Landroid/graphics/Path;

.field final Pc:Landroid/graphics/Path;

.field Pd:I

.field final Pe:Landroid/graphics/Path;

.field Pf:I

.field final Pg:Landroid/graphics/Path;

.field final Ph:Landroid/graphics/Paint;

.field Pi:Landroid/content/Intent;

.field Pj:I

.field final Pk:Landroid/graphics/Paint;

.field final Pl:Landroid/graphics/Paint;

.field Pm:I

.field final Pn:Landroid/graphics/Paint;

.field Po:Landroid/graphics/Bitmap;

.field Pp:Ljava/lang/String;

.field Pq:I

.field final Pr:Landroid/graphics/Paint;

.field final Ps:Landroid/graphics/Path;

.field Pt:Landroid/graphics/Canvas;

.field Pu:Ljava/lang/String;

.field Pv:I

.field Pw:I

.field Px:Ljava/lang/String;

.field Py:I

.field final Pz:Landroid/graphics/Paint;

.field QA:Ljava/lang/String;

.field QB:I

.field QC:Ljava/lang/String;

.field QD:I

.field final QE:Landroid/graphics/Paint;

.field final QF:Landroid/graphics/Path;

.field QG:J

.field QH:Landroid/os/BatteryStats;

.field QI:J

.field QJ:I

.field QK:I

.field final QL:Landroid/text/TextPaint;

.field QM:I

.field final QN:Ljava/util/ArrayList;

.field final QO:Landroid/graphics/Paint;

.field final QP:Landroid/graphics/Path;

.field QQ:Ljava/lang/String;

.field QR:I

.field final QS:Landroid/graphics/Paint;

.field final QT:Landroid/graphics/Path;

.field Qa:Z

.field Qb:Z

.field Qc:Z

.field Qd:I

.field Qe:I

.field Qf:I

.field final Qg:Landroid/text/TextPaint;

.field Qh:J

.field Qi:J

.field Qj:J

.field Qk:Lcom/android/settingslib/p;

.field Ql:Z

.field Qm:I

.field Qn:I

.field Qo:I

.field Qp:I

.field Qq:I

.field Qr:I

.field Qs:I

.field Qt:I

.field Qu:Ljava/lang/String;

.field Qv:I

.field Qw:Ljava/lang/String;

.field Qx:I

.field Qy:I

.field final Qz:Lcom/android/settings/fuelgauge/BatteryHistoryChart$ChartData;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 13

    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    new-instance v0, Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->Ph:Landroid/graphics/Paint;

    new-instance v0, Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->Pl:Landroid/graphics/Paint;

    new-instance v0, Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->Pn:Landroid/graphics/Paint;

    new-instance v0, Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->Pk:Landroid/graphics/Paint;

    new-instance v0, Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->QO:Landroid/graphics/Paint;

    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->Pz:Landroid/graphics/Paint;

    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->QE:Landroid/graphics/Paint;

    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->PW:Landroid/graphics/Paint;

    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->PS:Landroid/graphics/Paint;

    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->Pr:Landroid/graphics/Paint;

    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->QS:Landroid/graphics/Paint;

    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->PE:Landroid/graphics/Paint;

    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->PH:Landroid/graphics/Paint;

    new-instance v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart$ChartData;

    invoke-direct {v0}, Lcom/android/settings/fuelgauge/BatteryHistoryChart$ChartData;-><init>()V

    iput-object v0, p0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->Qz:Lcom/android/settings/fuelgauge/BatteryHistoryChart$ChartData;

    new-instance v0, Landroid/text/TextPaint;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/text/TextPaint;-><init>(I)V

    iput-object v0, p0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->QL:Landroid/text/TextPaint;

    new-instance v0, Landroid/text/TextPaint;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/text/TextPaint;-><init>(I)V

    iput-object v0, p0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->Qg:Landroid/text/TextPaint;

    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->PJ:Landroid/graphics/Paint;

    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->Pe:Landroid/graphics/Path;

    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->Pc:Landroid/graphics/Path;

    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->Pg:Landroid/graphics/Path;

    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->Pb:Landroid/graphics/Path;

    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->QP:Landroid/graphics/Path;

    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->PA:Landroid/graphics/Path;

    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->QF:Landroid/graphics/Path;

    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->PX:Landroid/graphics/Path;

    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->PT:Landroid/graphics/Path;

    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->Ps:Landroid/graphics/Path;

    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->QT:Landroid/graphics/Path;

    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->PF:Landroid/graphics/Path;

    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->PI:Landroid/graphics/Path;

    const/4 v0, -0x1

    iput v0, p0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->Qn:I

    const/4 v0, -0x1

    iput v0, p0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->Qm:I

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->QN:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->PG:Ljava/util/ArrayList;

    iget-object v0, p0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x10e005d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    iput v0, p0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->Pm:I

    iget-object v0, p0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x10e002e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    iput v0, p0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->Pj:I

    const/high16 v0, 0x40000000    # 2.0f

    invoke-virtual {p0}, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    const/4 v2, 0x1

    invoke-static {v2, v0, v1}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->QM:I

    iget-object v0, p0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->Ph:Landroid/graphics/Paint;

    const v1, -0xff6978

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    iget-object v0, p0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->Ph:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    iget-object v0, p0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->Pl:Landroid/graphics/Paint;

    const/16 v1, 0x80

    const/4 v2, 0x0

    const/16 v3, 0x80

    const/4 v4, 0x0

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Paint;->setARGB(IIII)V

    iget-object v0, p0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->Pl:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    iget-object v0, p0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->Pn:Landroid/graphics/Paint;

    const/16 v1, 0x80

    const/16 v2, 0x80

    const/16 v3, 0x80

    const/4 v4, 0x0

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Paint;->setARGB(IIII)V

    iget-object v0, p0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->Pn:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    iget-object v0, p0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->Pk:Landroid/graphics/Paint;

    const/16 v1, 0xc0

    const/16 v2, 0x80

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Paint;->setARGB(IIII)V

    iget-object v0, p0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->Pk:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    iget-object v0, p0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->QO:Landroid/graphics/Paint;

    const v1, -0x312845

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    iget-object v0, p0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->QO:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    iget-object v0, p0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->Pz:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    iget-object v0, p0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->QE:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    iget-object v0, p0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->PW:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    iget-object v0, p0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->Pr:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    iget-object v0, p0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->PS:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    iget-object v0, p0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->QS:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    iget-object v0, p0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->PE:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    iget-object v0, p0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->Qz:Lcom/android/settings/fuelgauge/BatteryHistoryChart$ChartData;

    sget-object v1, Lcom/android/settings/aq;->bCn:[I

    invoke-virtual {v0, v1}, Lcom/android/settings/fuelgauge/BatteryHistoryChart$ChartData;->Jj([I)V

    iget-object v0, p0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->PJ:Landroid/graphics/Paint;

    const/16 v1, 0xff

    const/16 v2, 0xff

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Paint;->setARGB(IIII)V

    iget-object v0, p0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->PJ:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    iget-object v0, p0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->QE:Landroid/graphics/Paint;

    const v1, -0xff6978

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    iget-object v0, p0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->PW:Landroid/graphics/Paint;

    const v1, -0xff6978

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    iget-object v0, p0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->Pr:Landroid/graphics/Paint;

    const v1, -0xff6978

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    iget-object v0, p0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->PS:Landroid/graphics/Paint;

    const v1, -0xff6978

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    iget-object v0, p0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->QS:Landroid/graphics/Paint;

    const v1, -0xff6978

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    iget-object v0, p0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->PE:Landroid/graphics/Paint;

    const v1, -0xff6978

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    iget-object v0, p0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->Pz:Landroid/graphics/Paint;

    const v1, -0xff6978

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    sget-object v0, Lcom/android/settings/cw;->bYJ:[I

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {p1, p2, v0, v1, v2}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v5

    new-instance v6, Lcom/android/settings/fuelgauge/BatteryHistoryChart$TextAttrs;

    invoke-direct {v6}, Lcom/android/settings/fuelgauge/BatteryHistoryChart$TextAttrs;-><init>()V

    new-instance v7, Lcom/android/settings/fuelgauge/BatteryHistoryChart$TextAttrs;

    invoke-direct {v7}, Lcom/android/settings/fuelgauge/BatteryHistoryChart$TextAttrs;-><init>()V

    const/4 v0, 0x0

    invoke-virtual {v6, p1, v5, v0}, Lcom/android/settings/fuelgauge/BatteryHistoryChart$TextAttrs;->Jl(Landroid/content/Context;Landroid/content/res/TypedArray;I)V

    const/16 v0, 0xc

    invoke-virtual {v7, p1, v5, v0}, Lcom/android/settings/fuelgauge/BatteryHistoryChart$TextAttrs;->Jl(Landroid/content/Context;Landroid/content/res/TypedArray;I)V

    const/4 v4, 0x0

    const/4 v3, 0x0

    const/4 v2, 0x0

    const/4 v1, 0x0

    invoke-virtual {v5}, Landroid/content/res/TypedArray;->getIndexCount()I

    move-result v8

    const/4 v0, 0x0

    move v12, v0

    move v0, v1

    move v1, v2

    move v2, v3

    move v3, v4

    move v4, v12

    :goto_0
    if-ge v4, v8, :cond_0

    invoke-virtual {v5, v4}, Landroid/content/res/TypedArray;->getIndex(I)I

    move-result v9

    packed-switch v9, :pswitch_data_0

    :goto_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    :pswitch_0
    const/4 v3, 0x0

    invoke-virtual {v5, v9, v3}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v3

    goto :goto_1

    :pswitch_1
    const/4 v2, 0x0

    invoke-virtual {v5, v9, v2}, Landroid/content/res/TypedArray;->getFloat(IF)F

    move-result v2

    goto :goto_1

    :pswitch_2
    const/4 v1, 0x0

    invoke-virtual {v5, v9, v1}, Landroid/content/res/TypedArray;->getFloat(IF)F

    move-result v1

    goto :goto_1

    :pswitch_3
    const/4 v0, 0x0

    invoke-virtual {v5, v9, v0}, Landroid/content/res/TypedArray;->getFloat(IF)F

    move-result v0

    goto :goto_1

    :pswitch_4
    invoke-virtual {v5, v9}, Landroid/content/res/TypedArray;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v10

    iput-object v10, v6, Lcom/android/settings/fuelgauge/BatteryHistoryChart$TextAttrs;->Ra:Landroid/content/res/ColorStateList;

    invoke-virtual {v5, v9}, Landroid/content/res/TypedArray;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v9

    iput-object v9, v7, Lcom/android/settings/fuelgauge/BatteryHistoryChart$TextAttrs;->Ra:Landroid/content/res/ColorStateList;

    goto :goto_1

    :pswitch_5
    iget v10, v6, Lcom/android/settings/fuelgauge/BatteryHistoryChart$TextAttrs;->Rb:I

    invoke-virtual {v5, v9, v10}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v10

    iput v10, v6, Lcom/android/settings/fuelgauge/BatteryHistoryChart$TextAttrs;->Rb:I

    iget v10, v7, Lcom/android/settings/fuelgauge/BatteryHistoryChart$TextAttrs;->Rb:I

    invoke-virtual {v5, v9, v10}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v9

    iput v9, v7, Lcom/android/settings/fuelgauge/BatteryHistoryChart$TextAttrs;->Rb:I

    goto :goto_1

    :pswitch_6
    iget v10, v6, Lcom/android/settings/fuelgauge/BatteryHistoryChart$TextAttrs;->Rc:I

    invoke-virtual {v5, v9, v10}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v10

    iput v10, v6, Lcom/android/settings/fuelgauge/BatteryHistoryChart$TextAttrs;->Rc:I

    iget v10, v7, Lcom/android/settings/fuelgauge/BatteryHistoryChart$TextAttrs;->Rc:I

    invoke-virtual {v5, v9, v10}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v9

    iput v9, v7, Lcom/android/settings/fuelgauge/BatteryHistoryChart$TextAttrs;->Rc:I

    goto :goto_1

    :pswitch_7
    iget v10, v6, Lcom/android/settings/fuelgauge/BatteryHistoryChart$TextAttrs;->QZ:I

    invoke-virtual {v5, v9, v10}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v10

    iput v10, v6, Lcom/android/settings/fuelgauge/BatteryHistoryChart$TextAttrs;->QZ:I

    iget v10, v7, Lcom/android/settings/fuelgauge/BatteryHistoryChart$TextAttrs;->QZ:I

    invoke-virtual {v5, v9, v10}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v9

    iput v9, v7, Lcom/android/settings/fuelgauge/BatteryHistoryChart$TextAttrs;->QZ:I

    goto :goto_1

    :pswitch_8
    iget-object v10, p0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->Ph:Landroid/graphics/Paint;

    const/4 v11, 0x0

    invoke-virtual {v5, v9, v11}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v11

    invoke-virtual {v10, v11}, Landroid/graphics/Paint;->setColor(I)V

    iget-object v10, p0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->QE:Landroid/graphics/Paint;

    const/4 v11, 0x0

    invoke-virtual {v5, v9, v11}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v11

    invoke-virtual {v10, v11}, Landroid/graphics/Paint;->setColor(I)V

    iget-object v10, p0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->PW:Landroid/graphics/Paint;

    const/4 v11, 0x0

    invoke-virtual {v5, v9, v11}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v11

    invoke-virtual {v10, v11}, Landroid/graphics/Paint;->setColor(I)V

    iget-object v10, p0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->Pr:Landroid/graphics/Paint;

    const/4 v11, 0x0

    invoke-virtual {v5, v9, v11}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v11

    invoke-virtual {v10, v11}, Landroid/graphics/Paint;->setColor(I)V

    iget-object v10, p0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->PS:Landroid/graphics/Paint;

    const/4 v11, 0x0

    invoke-virtual {v5, v9, v11}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v11

    invoke-virtual {v10, v11}, Landroid/graphics/Paint;->setColor(I)V

    iget-object v10, p0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->QS:Landroid/graphics/Paint;

    const/4 v11, 0x0

    invoke-virtual {v5, v9, v11}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v11

    invoke-virtual {v10, v11}, Landroid/graphics/Paint;->setColor(I)V

    iget-object v10, p0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->PE:Landroid/graphics/Paint;

    const/4 v11, 0x0

    invoke-virtual {v5, v9, v11}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v11

    invoke-virtual {v10, v11}, Landroid/graphics/Paint;->setColor(I)V

    iget-object v10, p0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->Pz:Landroid/graphics/Paint;

    const/4 v11, 0x0

    invoke-virtual {v5, v9, v11}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v9

    invoke-virtual {v10, v9}, Landroid/graphics/Paint;->setColor(I)V

    goto/16 :goto_1

    :pswitch_9
    iget-object v10, p0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->QO:Landroid/graphics/Paint;

    const/4 v11, 0x0

    invoke-virtual {v5, v9, v11}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v9

    invoke-virtual {v10, v9}, Landroid/graphics/Paint;->setColor(I)V

    goto/16 :goto_1

    :pswitch_a
    const/4 v10, 0x0

    invoke-virtual {v5, v9, v10}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v9

    iput v9, p0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->PB:I

    goto/16 :goto_1

    :cond_0
    invoke-virtual {v5}, Landroid/content/res/TypedArray;->recycle()V

    iget-object v4, p0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->QL:Landroid/text/TextPaint;

    invoke-virtual {v6, p1, v4}, Lcom/android/settings/fuelgauge/BatteryHistoryChart$TextAttrs;->Jk(Landroid/content/Context;Landroid/text/TextPaint;)V

    iget-object v4, p0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->Qg:Landroid/text/TextPaint;

    invoke-virtual {v7, p1, v4}, Lcom/android/settings/fuelgauge/BatteryHistoryChart$TextAttrs;->Jk(Landroid/content/Context;Landroid/text/TextPaint;)V

    iget-object v4, p0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->PH:Landroid/graphics/Paint;

    iget-object v5, p0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->QL:Landroid/text/TextPaint;

    invoke-virtual {v4, v5}, Landroid/graphics/Paint;->set(Landroid/graphics/Paint;)V

    iget-object v4, p0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->PH:Landroid/graphics/Paint;

    sget-object v5, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v4, v5}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    iget v4, p0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->QM:I

    div-int/lit8 v4, v4, 0x2

    const/4 v5, 0x1

    if-ge v4, v5, :cond_1

    const/4 v4, 0x1

    :cond_1
    iget-object v5, p0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->PH:Landroid/graphics/Paint;

    int-to-float v4, v4

    invoke-virtual {v5, v4}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    iget-object v4, p0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->PH:Landroid/graphics/Paint;

    new-instance v5, Landroid/graphics/DashPathEffect;

    const/4 v6, 0x2

    new-array v6, v6, [F

    iget v7, p0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->QM:I

    mul-int/lit8 v7, v7, 0x2

    int-to-float v7, v7

    const/4 v8, 0x0

    aput v7, v6, v8

    iget v7, p0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->QM:I

    mul-int/lit8 v7, v7, 0x2

    int-to-float v7, v7

    const/4 v8, 0x1

    aput v7, v6, v8

    const/4 v7, 0x0

    invoke-direct {v5, v6, v7}, Landroid/graphics/DashPathEffect;-><init>([FF)V

    invoke-virtual {v4, v5}, Landroid/graphics/Paint;->setPathEffect(Landroid/graphics/PathEffect;)Landroid/graphics/PathEffect;

    if-eqz v3, :cond_2

    iget-object v4, p0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->QL:Landroid/text/TextPaint;

    invoke-virtual {v4, v0, v2, v1, v3}, Landroid/text/TextPaint;->setShadowLayer(FFFI)V

    iget-object v4, p0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->Qg:Landroid/text/TextPaint;

    invoke-virtual {v4, v0, v2, v1, v3}, Landroid/text/TextPaint;->setShadowLayer(FFFI)V

    :cond_2
    return-void

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_4
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_9
        :pswitch_8
        :pswitch_a
    .end packed-switch
.end method

.method private Jd()Z
    .locals 1

    invoke-virtual {p0}, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/text/format/DateFormat;->is24HourFormat(Landroid/content/Context;)Z

    move-result v0

    return v0
.end method

.method private Je()Z
    .locals 3

    invoke-virtual {p0}, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget-object v0, v0, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-static {v0}, Llibcore/icu/LocaleData;->get(Ljava/util/Locale;)Llibcore/icu/LocaleData;

    move-result-object v0

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Llibcore/icu/LocaleData;->getDateFormat(I)Ljava/lang/String;

    move-result-object v0

    const/16 v1, 0x4d

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(I)I

    move-result v1

    const/16 v2, 0x64

    invoke-virtual {v0, v2}, Ljava/lang/String;->indexOf(I)I

    move-result v0

    if-le v1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method IZ(Ljava/util/Calendar;IIZ)V
    .locals 10

    iget-wide v0, p0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->QG:J

    iget-wide v2, p0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->PP:J

    sub-long/2addr v2, v0

    iget-object v4, p0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->PG:Ljava/util/ArrayList;

    new-instance v5, Lcom/android/settings/fuelgauge/BatteryHistoryChart$DateLabel;

    iget-object v6, p0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->QL:Landroid/text/TextPaint;

    invoke-virtual {p1}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v8

    sub-long v0, v8, v0

    sub-int v7, p3, p2

    int-to-long v8, v7

    mul-long/2addr v0, v8

    div-long/2addr v0, v2

    long-to-int v0, v0

    add-int/2addr v0, p2

    invoke-direct {v5, v6, v0, p1, p4}, Lcom/android/settings/fuelgauge/BatteryHistoryChart$DateLabel;-><init>(Landroid/text/TextPaint;ILjava/util/Calendar;Z)V

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method Ja(Ljava/util/Calendar;IIZ)V
    .locals 10

    iget-wide v0, p0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->QG:J

    iget-wide v2, p0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->PP:J

    sub-long/2addr v2, v0

    iget-object v4, p0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->QN:Ljava/util/ArrayList;

    new-instance v5, Lcom/android/settings/fuelgauge/BatteryHistoryChart$TimeLabel;

    iget-object v6, p0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->QL:Landroid/text/TextPaint;

    invoke-virtual {p1}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v8

    sub-long v0, v8, v0

    sub-int v7, p3, p2

    int-to-long v8, v7

    mul-long/2addr v0, v8

    div-long/2addr v0, v2

    long-to-int v0, v0

    add-int/2addr v0, p2

    invoke-direct {v5, v6, v0, p1, p4}, Lcom/android/settings/fuelgauge/BatteryHistoryChart$TimeLabel;-><init>(Landroid/text/TextPaint;ILjava/util/Calendar;Z)V

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method Jb(Landroid/graphics/Canvas;II)V
    .locals 18

    invoke-virtual/range {p0 .. p0}, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->isLayoutRtl()Z

    move-result v14

    if-eqz v14, :cond_1

    move/from16 v13, p2

    :goto_0
    if-eqz v14, :cond_2

    const/4 v1, 0x0

    move v12, v1

    :goto_1
    if-eqz v14, :cond_3

    sget-object v1, Landroid/graphics/Paint$Align;->RIGHT:Landroid/graphics/Paint$Align;

    move-object v11, v1

    :goto_2
    if-eqz v14, :cond_4

    sget-object v1, Landroid/graphics/Paint$Align;->LEFT:Landroid/graphics/Paint$Align;

    move-object v8, v1

    :goto_3
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->Pe:Landroid/graphics/Path;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->Ph:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->QP:Landroid/graphics/Path;

    invoke-virtual {v1}, Landroid/graphics/Path;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->QP:Landroid/graphics/Path;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->QO:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    :cond_0
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->QN:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    const/4 v2, 0x1

    if-le v1, v2, :cond_a

    move-object/from16 v0, p0

    iget v1, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->Qo:I

    move-object/from16 v0, p0

    iget v2, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->QJ:I

    sub-int/2addr v1, v2

    move-object/from16 v0, p0

    iget v2, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->QM:I

    mul-int/lit8 v2, v2, 0x4

    add-int v15, v1, v2

    move-object/from16 v0, p0

    iget v1, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->Qo:I

    move-object/from16 v0, p0

    iget v2, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->QM:I

    add-int/2addr v1, v2

    move-object/from16 v0, p0

    iget v2, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->QM:I

    div-int/lit8 v2, v2, 0x2

    add-int v16, v1, v2

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->QL:Landroid/text/TextPaint;

    sget-object v2, Landroid/graphics/Paint$Align;->LEFT:Landroid/graphics/Paint$Align;

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    const/4 v2, 0x0

    const/4 v1, 0x0

    move v9, v1

    move v10, v2

    :goto_4
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->QN:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v9, v1, :cond_b

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->QN:Ljava/util/ArrayList;

    invoke-virtual {v1, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    move-object v7, v1

    check-cast v7, Lcom/android/settings/fuelgauge/BatteryHistoryChart$TimeLabel;

    if-nez v9, :cond_5

    iget v1, v7, Lcom/android/settings/fuelgauge/BatteryHistoryChart$TimeLabel;->Rf:I

    iget v2, v7, Lcom/android/settings/fuelgauge/BatteryHistoryChart$TimeLabel;->Re:I

    div-int/lit8 v2, v2, 0x2

    sub-int/2addr v1, v2

    if-gez v1, :cond_23

    const/4 v1, 0x0

    move v10, v1

    :goto_5
    iget-object v1, v7, Lcom/android/settings/fuelgauge/BatteryHistoryChart$TimeLabel;->Rd:Ljava/lang/String;

    int-to-float v2, v10

    int-to-float v3, v15

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->QL:Landroid/text/TextPaint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    iget v1, v7, Lcom/android/settings/fuelgauge/BatteryHistoryChart$TimeLabel;->Rf:I

    int-to-float v2, v1

    move/from16 v0, v16

    int-to-float v3, v0

    iget v1, v7, Lcom/android/settings/fuelgauge/BatteryHistoryChart$TimeLabel;->Rf:I

    int-to-float v4, v1

    move-object/from16 v0, p0

    iget v1, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->QM:I

    add-int v1, v1, v16

    int-to-float v5, v1

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->QL:Landroid/text/TextPaint;

    move-object/from16 v1, p1

    invoke-virtual/range {v1 .. v6}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    iget v1, v7, Lcom/android/settings/fuelgauge/BatteryHistoryChart$TimeLabel;->Re:I

    add-int/2addr v10, v1

    move v2, v10

    :goto_6
    add-int/lit8 v1, v9, 0x1

    move v9, v1

    move v10, v2

    goto :goto_4

    :cond_1
    const/4 v1, 0x0

    move v13, v1

    goto/16 :goto_0

    :cond_2
    move/from16 v12, p2

    goto/16 :goto_1

    :cond_3
    sget-object v1, Landroid/graphics/Paint$Align;->LEFT:Landroid/graphics/Paint$Align;

    move-object v11, v1

    goto/16 :goto_2

    :cond_4
    sget-object v1, Landroid/graphics/Paint$Align;->RIGHT:Landroid/graphics/Paint$Align;

    move-object v8, v1

    goto/16 :goto_3

    :cond_5
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->QN:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    if-ge v9, v1, :cond_8

    iget v1, v7, Lcom/android/settings/fuelgauge/BatteryHistoryChart$TimeLabel;->Rf:I

    iget v2, v7, Lcom/android/settings/fuelgauge/BatteryHistoryChart$TimeLabel;->Re:I

    div-int/lit8 v2, v2, 0x2

    sub-int v17, v1, v2

    move-object/from16 v0, p0

    iget v1, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->QJ:I

    add-int/2addr v1, v10

    move/from16 v0, v17

    if-ge v0, v1, :cond_6

    move v2, v10

    goto :goto_6

    :cond_6
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->QN:Ljava/util/ArrayList;

    add-int/lit8 v2, v9, 0x1

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/settings/fuelgauge/BatteryHistoryChart$TimeLabel;

    iget v1, v1, Lcom/android/settings/fuelgauge/BatteryHistoryChart$TimeLabel;->Re:I

    sub-int v1, p2, v1

    move-object/from16 v0, p0

    iget v2, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->QJ:I

    sub-int/2addr v1, v2

    move/from16 v0, v17

    if-le v0, v1, :cond_7

    move v2, v10

    goto :goto_6

    :cond_7
    iget-object v1, v7, Lcom/android/settings/fuelgauge/BatteryHistoryChart$TimeLabel;->Rd:Ljava/lang/String;

    move/from16 v0, v17

    int-to-float v2, v0

    int-to-float v3, v15

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->QL:Landroid/text/TextPaint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    iget v1, v7, Lcom/android/settings/fuelgauge/BatteryHistoryChart$TimeLabel;->Rf:I

    int-to-float v2, v1

    move/from16 v0, v16

    int-to-float v3, v0

    iget v1, v7, Lcom/android/settings/fuelgauge/BatteryHistoryChart$TimeLabel;->Rf:I

    int-to-float v4, v1

    move-object/from16 v0, p0

    iget v1, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->QM:I

    add-int v1, v1, v16

    int-to-float v5, v1

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->QL:Landroid/text/TextPaint;

    move-object/from16 v1, p1

    invoke-virtual/range {v1 .. v6}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    iget v1, v7, Lcom/android/settings/fuelgauge/BatteryHistoryChart$TimeLabel;->Re:I

    add-int v10, v17, v1

    move v2, v10

    goto :goto_6

    :cond_8
    iget v1, v7, Lcom/android/settings/fuelgauge/BatteryHistoryChart$TimeLabel;->Rf:I

    iget v2, v7, Lcom/android/settings/fuelgauge/BatteryHistoryChart$TimeLabel;->Re:I

    div-int/lit8 v2, v2, 0x2

    sub-int/2addr v1, v2

    iget v2, v7, Lcom/android/settings/fuelgauge/BatteryHistoryChart$TimeLabel;->Re:I

    add-int/2addr v2, v1

    move/from16 v0, p2

    if-lt v2, v0, :cond_9

    add-int/lit8 v1, p2, -0x1

    iget v2, v7, Lcom/android/settings/fuelgauge/BatteryHistoryChart$TimeLabel;->Re:I

    sub-int/2addr v1, v2

    :cond_9
    iget-object v2, v7, Lcom/android/settings/fuelgauge/BatteryHistoryChart$TimeLabel;->Rd:Ljava/lang/String;

    int-to-float v1, v1

    int-to-float v3, v15

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->QL:Landroid/text/TextPaint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v1, v3, v4}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    iget v1, v7, Lcom/android/settings/fuelgauge/BatteryHistoryChart$TimeLabel;->Rf:I

    int-to-float v2, v1

    move/from16 v0, v16

    int-to-float v3, v0

    iget v1, v7, Lcom/android/settings/fuelgauge/BatteryHistoryChart$TimeLabel;->Rf:I

    int-to-float v4, v1

    move-object/from16 v0, p0

    iget v1, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->QM:I

    add-int v1, v1, v16

    int-to-float v5, v1

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->QL:Landroid/text/TextPaint;

    move-object/from16 v1, p1

    invoke-virtual/range {v1 .. v6}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    move v2, v10

    goto/16 :goto_6

    :cond_a
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->PM:Ljava/lang/String;

    if-eqz v1, :cond_b

    move-object/from16 v0, p0

    iget v1, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->Qo:I

    move-object/from16 v0, p0

    iget v2, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->QJ:I

    sub-int/2addr v1, v2

    move-object/from16 v0, p0

    iget v2, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->QM:I

    mul-int/lit8 v2, v2, 0x4

    add-int/2addr v1, v2

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->QL:Landroid/text/TextPaint;

    sget-object v3, Landroid/graphics/Paint$Align;->LEFT:Landroid/graphics/Paint$Align;

    invoke-virtual {v2, v3}, Landroid/text/TextPaint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->PM:Ljava/lang/String;

    move-object/from16 v0, p0

    iget v3, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->Qp:I

    move-object/from16 v0, p0

    iget v4, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->Qr:I

    move-object/from16 v0, p0

    iget v5, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->Qp:I

    sub-int/2addr v4, v5

    div-int/lit8 v4, v4, 0x2

    add-int/2addr v3, v4

    move-object/from16 v0, p0

    iget v4, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->PN:I

    div-int/lit8 v4, v4, 0x2

    sub-int/2addr v3, v4

    int-to-float v3, v3

    int-to-float v1, v1

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->QL:Landroid/text/TextPaint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v1, v4}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    :cond_b
    move-object/from16 v0, p0

    iget v1, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->Qe:I

    neg-int v1, v1

    move-object/from16 v0, p0

    iget v2, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->Qf:I

    move-object/from16 v0, p0

    iget v3, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->Qe:I

    sub-int/2addr v2, v3

    div-int/lit8 v2, v2, 0x3

    add-int v3, v1, v2

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->Qg:Landroid/text/TextPaint;

    invoke-virtual {v1, v11}, Landroid/text/TextPaint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->Qk:Lcom/android/settingslib/p;

    iget-object v1, v1, Lcom/android/settingslib/p;->cPY:Ljava/lang/String;

    int-to-float v2, v13

    int-to-float v4, v3

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->Qg:Landroid/text/TextPaint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v2, v4, v5}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    move-object/from16 v0, p0

    iget v1, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->Pv:I

    div-int/lit8 v1, v1, 0x2

    if-eqz v14, :cond_c

    neg-int v1, v1

    :cond_c
    move-object/from16 v0, p0

    iget v2, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->Pv:I

    sub-int v2, p2, v2

    move-object/from16 v0, p0

    iget v4, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->PL:I

    sub-int/2addr v2, v4

    div-int/lit8 v4, v2, 0x2

    if-eqz v14, :cond_1e

    move-object/from16 v0, p0

    iget v2, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->PL:I

    :goto_7
    add-int/2addr v2, v4

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->Pu:Ljava/lang/String;

    sub-int v1, v2, v1

    int-to-float v1, v1

    int-to-float v2, v3

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->Qg:Landroid/text/TextPaint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v1, v2, v5}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->Qg:Landroid/text/TextPaint;

    invoke-virtual {v1, v8}, Landroid/text/TextPaint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->PK:Ljava/lang/String;

    int-to-float v2, v12

    int-to-float v3, v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->Qg:Landroid/text/TextPaint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->Pc:Landroid/graphics/Path;

    invoke-virtual {v1}, Landroid/graphics/Path;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_d

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->Pc:Landroid/graphics/Path;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->Pl:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    :cond_d
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->Pg:Landroid/graphics/Path;

    invoke-virtual {v1}, Landroid/graphics/Path;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_e

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->Pg:Landroid/graphics/Path;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->Pn:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    :cond_e
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->Pb:Landroid/graphics/Path;

    invoke-virtual {v1}, Landroid/graphics/Path;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_f

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->Pb:Landroid/graphics/Path;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->Pk:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    :cond_f
    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->Qb:Z

    if-eqz v1, :cond_10

    move-object/from16 v0, p0

    iget v1, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->QB:I

    sub-int v1, p3, v1

    move-object/from16 v0, p0

    iget v2, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->Qt:I

    div-int/lit8 v2, v2, 0x2

    sub-int/2addr v1, v2

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->Qz:Lcom/android/settings/fuelgauge/BatteryHistoryChart$ChartData;

    move-object/from16 v0, p0

    iget v3, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->Qt:I

    move-object/from16 v0, p1

    invoke-virtual {v2, v0, v1, v3}, Lcom/android/settings/fuelgauge/BatteryHistoryChart$ChartData;->Jg(Landroid/graphics/Canvas;II)V

    :cond_10
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->QF:Landroid/graphics/Path;

    invoke-virtual {v1}, Landroid/graphics/Path;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_11

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->QF:Landroid/graphics/Path;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->QE:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    :cond_11
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->PA:Landroid/graphics/Path;

    invoke-virtual {v1}, Landroid/graphics/Path;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_12

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->PA:Landroid/graphics/Path;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->Pz:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    :cond_12
    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->Qa:Z

    if-eqz v1, :cond_13

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->PX:Landroid/graphics/Path;

    invoke-virtual {v1}, Landroid/graphics/Path;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_13

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->PX:Landroid/graphics/Path;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->PW:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    :cond_13
    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->PZ:Z

    if-eqz v1, :cond_14

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->PT:Landroid/graphics/Path;

    invoke-virtual {v1}, Landroid/graphics/Path;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_14

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->PT:Landroid/graphics/Path;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->PS:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    :cond_14
    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->PY:Z

    if-eqz v1, :cond_15

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->Ps:Landroid/graphics/Path;

    invoke-virtual {v1}, Landroid/graphics/Path;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_15

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->Ps:Landroid/graphics/Path;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->Pr:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    :cond_15
    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->Qc:Z

    if-eqz v1, :cond_16

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->QT:Landroid/graphics/Path;

    invoke-virtual {v1}, Landroid/graphics/Path;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_16

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->QT:Landroid/graphics/Path;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->QS:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    :cond_16
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->PF:Landroid/graphics/Path;

    invoke-virtual {v1}, Landroid/graphics/Path;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_17

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->PF:Landroid/graphics/Path;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->PE:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    :cond_17
    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->Ql:Z

    if-eqz v1, :cond_1d

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->QL:Landroid/text/TextPaint;

    invoke-virtual {v1}, Landroid/text/TextPaint;->getTextAlign()Landroid/graphics/Paint$Align;

    move-result-object v1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->QL:Landroid/text/TextPaint;

    invoke-virtual {v2, v11}, Landroid/text/TextPaint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->Qb:Z

    if-eqz v2, :cond_18

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->QA:Ljava/lang/String;

    int-to-float v3, v13

    move-object/from16 v0, p0

    iget v4, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->QB:I

    sub-int v4, p3, v4

    move-object/from16 v0, p0

    iget v5, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->QK:I

    sub-int/2addr v4, v5

    int-to-float v4, v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->QL:Landroid/text/TextPaint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v4, v5}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    :cond_18
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->Qa:Z

    if-eqz v2, :cond_19

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->PU:Ljava/lang/String;

    int-to-float v3, v13

    move-object/from16 v0, p0

    iget v4, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->PV:I

    sub-int v4, p3, v4

    move-object/from16 v0, p0

    iget v5, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->QK:I

    sub-int/2addr v4, v5

    int-to-float v4, v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->QL:Landroid/text/TextPaint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v4, v5}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    :cond_19
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->PZ:Z

    if-eqz v2, :cond_1a

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->PQ:Ljava/lang/String;

    int-to-float v3, v13

    move-object/from16 v0, p0

    iget v4, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->PR:I

    sub-int v4, p3, v4

    move-object/from16 v0, p0

    iget v5, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->QK:I

    sub-int/2addr v4, v5

    int-to-float v4, v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->QL:Landroid/text/TextPaint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v4, v5}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    :cond_1a
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->PY:Z

    if-eqz v2, :cond_1b

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->Pp:Ljava/lang/String;

    int-to-float v3, v13

    move-object/from16 v0, p0

    iget v4, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->Pq:I

    sub-int v4, p3, v4

    move-object/from16 v0, p0

    iget v5, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->QK:I

    sub-int/2addr v4, v5

    int-to-float v4, v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->QL:Landroid/text/TextPaint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v4, v5}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    :cond_1b
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->Qc:Z

    if-eqz v2, :cond_1c

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->QQ:Ljava/lang/String;

    int-to-float v3, v13

    move-object/from16 v0, p0

    iget v4, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->QR:I

    sub-int v4, p3, v4

    move-object/from16 v0, p0

    iget v5, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->QK:I

    sub-int/2addr v4, v5

    int-to-float v4, v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->QL:Landroid/text/TextPaint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v4, v5}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    :cond_1c
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->PC:Ljava/lang/String;

    int-to-float v3, v13

    move-object/from16 v0, p0

    iget v4, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->PD:I

    sub-int v4, p3, v4

    move-object/from16 v0, p0

    iget v5, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->QK:I

    sub-int/2addr v4, v5

    int-to-float v4, v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->QL:Landroid/text/TextPaint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v4, v5}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->Px:Ljava/lang/String;

    int-to-float v3, v13

    move-object/from16 v0, p0

    iget v4, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->Py:I

    sub-int v4, p3, v4

    move-object/from16 v0, p0

    iget v5, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->QK:I

    sub-int/2addr v4, v5

    int-to-float v4, v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->QL:Landroid/text/TextPaint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v4, v5}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->QC:Ljava/lang/String;

    int-to-float v3, v13

    move-object/from16 v0, p0

    iget v4, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->QD:I

    sub-int v4, p3, v4

    move-object/from16 v0, p0

    iget v5, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->QK:I

    sub-int/2addr v4, v5

    int-to-float v4, v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->QL:Landroid/text/TextPaint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v4, v5}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->QL:Landroid/text/TextPaint;

    invoke-virtual {v2, v1}, Landroid/text/TextPaint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    :cond_1d
    move-object/from16 v0, p0

    iget v1, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->Qp:I

    move-object/from16 v0, p0

    iget v2, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->QM:I

    sub-int/2addr v1, v2

    int-to-float v2, v1

    move-object/from16 v0, p0

    iget v1, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->Qs:I

    int-to-float v3, v1

    move-object/from16 v0, p0

    iget v1, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->Qp:I

    move-object/from16 v0, p0

    iget v4, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->QM:I

    sub-int/2addr v1, v4

    int-to-float v4, v1

    move-object/from16 v0, p0

    iget v1, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->Qo:I

    move-object/from16 v0, p0

    iget v5, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->QM:I

    div-int/lit8 v5, v5, 0x2

    add-int/2addr v1, v5

    int-to-float v5, v1

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->QL:Landroid/text/TextPaint;

    move-object/from16 v1, p1

    invoke-virtual/range {v1 .. v6}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->Ql:Z

    if-eqz v1, :cond_1f

    const/4 v1, 0x0

    move v7, v1

    :goto_8
    const/16 v1, 0xa

    if-ge v7, v1, :cond_1f

    move-object/from16 v0, p0

    iget v1, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->Qs:I

    move-object/from16 v0, p0

    iget v2, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->QM:I

    div-int/lit8 v2, v2, 0x2

    add-int/2addr v1, v2

    move-object/from16 v0, p0

    iget v2, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->Qo:I

    move-object/from16 v0, p0

    iget v3, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->Qs:I

    sub-int/2addr v2, v3

    mul-int/2addr v2, v7

    div-int/lit8 v2, v2, 0xa

    add-int/2addr v1, v2

    move-object/from16 v0, p0

    iget v2, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->Qp:I

    move-object/from16 v0, p0

    iget v3, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->QM:I

    mul-int/lit8 v3, v3, 0x2

    sub-int/2addr v2, v3

    move-object/from16 v0, p0

    iget v3, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->QM:I

    div-int/lit8 v3, v3, 0x2

    sub-int/2addr v2, v3

    int-to-float v2, v2

    int-to-float v3, v1

    move-object/from16 v0, p0

    iget v4, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->Qp:I

    move-object/from16 v0, p0

    iget v5, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->QM:I

    sub-int/2addr v4, v5

    move-object/from16 v0, p0

    iget v5, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->QM:I

    div-int/lit8 v5, v5, 0x2

    sub-int/2addr v4, v5

    int-to-float v4, v4

    int-to-float v5, v1

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->QL:Landroid/text/TextPaint;

    move-object/from16 v1, p1

    invoke-virtual/range {v1 .. v6}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    add-int/lit8 v1, v7, 0x1

    move v7, v1

    goto :goto_8

    :cond_1e
    move-object/from16 v0, p0

    iget v2, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->Pw:I

    goto/16 :goto_7

    :cond_1f
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->Qu:Ljava/lang/String;

    const/4 v2, 0x0

    move-object/from16 v0, p0

    iget v3, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->Qs:I

    int-to-float v3, v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->QL:Landroid/text/TextPaint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->Qw:Ljava/lang/String;

    move-object/from16 v0, p0

    iget v2, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->Qv:I

    move-object/from16 v0, p0

    iget v3, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->Qx:I

    sub-int/2addr v2, v3

    int-to-float v2, v2

    move-object/from16 v0, p0

    iget v3, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->Qo:I

    move-object/from16 v0, p0

    iget v4, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->QM:I

    sub-int/2addr v3, v4

    int-to-float v3, v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->QL:Landroid/text/TextPaint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    move-object/from16 v0, p0

    iget v1, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->Qp:I

    div-int/lit8 v1, v1, 0x2

    int-to-float v2, v1

    move-object/from16 v0, p0

    iget v1, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->Qo:I

    move-object/from16 v0, p0

    iget v3, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->QM:I

    add-int/2addr v1, v3

    int-to-float v3, v1

    move/from16 v0, p2

    int-to-float v4, v0

    move-object/from16 v0, p0

    iget v1, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->Qo:I

    move-object/from16 v0, p0

    iget v5, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->QM:I

    add-int/2addr v1, v5

    int-to-float v5, v1

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->QL:Landroid/text/TextPaint;

    move-object/from16 v1, p1

    invoke-virtual/range {v1 .. v6}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->PG:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_22

    move-object/from16 v0, p0

    iget v1, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->Qs:I

    move-object/from16 v0, p0

    iget v2, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->QJ:I

    add-int v5, v1, v2

    move-object/from16 v0, p0

    iget v6, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->Qo:I

    move-object/from16 v0, p0

    iget v7, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->Qr:I

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->QL:Landroid/text/TextPaint;

    sget-object v2, Landroid/graphics/Paint$Align;->LEFT:Landroid/graphics/Paint$Align;

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->PG:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    move v4, v1

    :goto_9
    if-ltz v4, :cond_22

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->PG:Ljava/util/ArrayList;

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/settings/fuelgauge/BatteryHistoryChart$DateLabel;

    iget v2, v1, Lcom/android/settings/fuelgauge/BatteryHistoryChart$DateLabel;->Ri:I

    move-object/from16 v0, p0

    iget v3, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->QM:I

    sub-int v3, v2, v3

    iget v2, v1, Lcom/android/settings/fuelgauge/BatteryHistoryChart$DateLabel;->Ri:I

    move-object/from16 v0, p0

    iget v8, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->QM:I

    mul-int/lit8 v8, v8, 0x2

    add-int/2addr v2, v8

    iget v8, v1, Lcom/android/settings/fuelgauge/BatteryHistoryChart$DateLabel;->Rh:I

    add-int/2addr v8, v2

    if-lt v8, v7, :cond_21

    iget v2, v1, Lcom/android/settings/fuelgauge/BatteryHistoryChart$DateLabel;->Ri:I

    move-object/from16 v0, p0

    iget v3, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->QM:I

    mul-int/lit8 v3, v3, 0x2

    sub-int/2addr v2, v3

    iget v3, v1, Lcom/android/settings/fuelgauge/BatteryHistoryChart$DateLabel;->Rh:I

    sub-int/2addr v2, v3

    move-object/from16 v0, p0

    iget v3, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->QM:I

    sub-int v3, v2, v3

    if-lt v3, v7, :cond_21

    :cond_20
    :goto_a
    add-int/lit8 v1, v4, -0x1

    move v4, v1

    goto :goto_9

    :cond_21
    move-object/from16 v0, p0

    iget v8, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->Qp:I

    if-lt v3, v8, :cond_20

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->PI:Landroid/graphics/Path;

    invoke-virtual {v3}, Landroid/graphics/Path;->reset()V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->PI:Landroid/graphics/Path;

    iget v8, v1, Lcom/android/settings/fuelgauge/BatteryHistoryChart$DateLabel;->Ri:I

    int-to-float v8, v8

    int-to-float v9, v5

    invoke-virtual {v3, v8, v9}, Landroid/graphics/Path;->moveTo(FF)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->PI:Landroid/graphics/Path;

    iget v8, v1, Lcom/android/settings/fuelgauge/BatteryHistoryChart$DateLabel;->Ri:I

    int-to-float v8, v8

    int-to-float v9, v6

    invoke-virtual {v3, v8, v9}, Landroid/graphics/Path;->lineTo(FF)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->PI:Landroid/graphics/Path;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->PH:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v8}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    iget-object v1, v1, Lcom/android/settings/fuelgauge/BatteryHistoryChart$DateLabel;->Rg:Ljava/lang/String;

    int-to-float v2, v2

    move-object/from16 v0, p0

    iget v3, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->QJ:I

    sub-int v3, v5, v3

    int-to-float v3, v3

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->QL:Landroid/text/TextPaint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v2, v3, v8}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    goto :goto_a

    :cond_22
    return-void

    :cond_23
    move v10, v1

    goto/16 :goto_5
.end method

.method Jc(IIIIILandroid/graphics/Path;IZZZZZZZLandroid/graphics/Path;)V
    .locals 4

    if-eqz p6, :cond_2

    if-ltz p7, :cond_1

    if-ge p7, p1, :cond_1

    if-eqz p15, :cond_0

    int-to-float v1, p1

    int-to-float v2, p5

    move-object/from16 v0, p15

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    :cond_0
    int-to-float v1, p1

    int-to-float v2, p5

    invoke-virtual {p6, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    :cond_1
    int-to-float v1, p1

    iget v2, p0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->Qs:I

    add-int/2addr v2, p3

    int-to-float v2, v2

    invoke-virtual {p6, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    int-to-float v1, p4

    iget v2, p0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->Qs:I

    add-int/2addr v2, p3

    int-to-float v2, v2

    invoke-virtual {p6, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    invoke-virtual {p6}, Landroid/graphics/Path;->close()V

    :cond_2
    if-eqz p8, :cond_3

    iget-object v1, p0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->PA:Landroid/graphics/Path;

    int-to-float v2, p1

    iget v3, p0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->Py:I

    sub-int v3, p2, v3

    int-to-float v3, v3

    invoke-virtual {v1, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    :cond_3
    if-eqz p9, :cond_4

    iget-object v1, p0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->QF:Landroid/graphics/Path;

    int-to-float v2, p1

    iget v3, p0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->QD:I

    sub-int v3, p2, v3

    int-to-float v3, v3

    invoke-virtual {v1, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    :cond_4
    if-eqz p10, :cond_5

    iget-object v1, p0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->PX:Landroid/graphics/Path;

    int-to-float v2, p1

    iget v3, p0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->PV:I

    sub-int v3, p2, v3

    int-to-float v3, v3

    invoke-virtual {v1, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    :cond_5
    if-eqz p11, :cond_6

    iget-object v1, p0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->PT:Landroid/graphics/Path;

    int-to-float v2, p1

    iget v3, p0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->PR:I

    sub-int v3, p2, v3

    int-to-float v3, v3

    invoke-virtual {v1, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    :cond_6
    if-eqz p12, :cond_7

    iget-object v1, p0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->Ps:Landroid/graphics/Path;

    int-to-float v2, p1

    iget v3, p0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->Pq:I

    sub-int v3, p2, v3

    int-to-float v3, v3

    invoke-virtual {v1, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    :cond_7
    if-eqz p13, :cond_8

    iget-object v1, p0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->QT:Landroid/graphics/Path;

    int-to-float v2, p1

    iget v3, p0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->QR:I

    sub-int v3, p2, v3

    int-to-float v3, v3

    invoke-virtual {v1, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    :cond_8
    if-eqz p14, :cond_9

    iget-object v1, p0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->PF:Landroid/graphics/Path;

    int-to-float v2, p1

    iget v3, p0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->PD:I

    sub-int v3, p2, v3

    int-to-float v3, v3

    invoke-virtual {v1, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    :cond_9
    iget-boolean v1, p0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->Qb:Z

    if-eqz v1, :cond_a

    iget-object v1, p0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->Qz:Lcom/android/settings/fuelgauge/BatteryHistoryChart$ChartData;

    invoke-virtual {v1, p1}, Lcom/android/settings/fuelgauge/BatteryHistoryChart$ChartData;->Jh(I)V

    :cond_a
    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 2

    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    invoke-virtual {p0}, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->getWidth()I

    move-result v0

    invoke-virtual {p0}, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->getHeight()I

    move-result v1

    invoke-virtual {p0, p1, v0, v1}, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->Jb(Landroid/graphics/Canvas;II)V

    return-void
.end method

.method protected onMeasure(II)V
    .locals 3

    iget-object v0, p0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->QL:Landroid/text/TextPaint;

    iget-object v1, p0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->Qu:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->measureText(Ljava/lang/String;)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->Qv:I

    iget-object v0, p0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->QL:Landroid/text/TextPaint;

    iget-object v1, p0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->Qw:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->measureText(Ljava/lang/String;)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->Qx:I

    iget-object v0, p0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->Qg:Landroid/text/TextPaint;

    iget-object v1, p0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->PK:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->measureText(Ljava/lang/String;)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->PL:I

    iget-object v0, p0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->Qg:Landroid/text/TextPaint;

    iget-object v1, p0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->Qk:Lcom/android/settingslib/p;

    iget-object v1, v1, Lcom/android/settingslib/p;->cPY:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->measureText(Ljava/lang/String;)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->Pw:I

    iget-object v0, p0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->Qg:Landroid/text/TextPaint;

    iget-object v1, p0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->Pu:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->measureText(Ljava/lang/String;)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->Pv:I

    iget-object v0, p0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->QL:Landroid/text/TextPaint;

    invoke-virtual {v0}, Landroid/text/TextPaint;->ascent()F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->QJ:I

    iget-object v0, p0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->QL:Landroid/text/TextPaint;

    invoke-virtual {v0}, Landroid/text/TextPaint;->descent()F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->QK:I

    iget-object v0, p0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->Qg:Landroid/text/TextPaint;

    invoke-virtual {v0}, Landroid/text/TextPaint;->ascent()F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->Qe:I

    iget-object v0, p0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->Qg:Landroid/text/TextPaint;

    invoke-virtual {v0}, Landroid/text/TextPaint;->descent()F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->Qf:I

    iget v0, p0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->Qf:I

    iget v1, p0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->Qe:I

    sub-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x2

    iget v1, p0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->QJ:I

    sub-int/2addr v0, v1

    iput v0, p0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->Qd:I

    invoke-virtual {p0}, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->getSuggestedMinimumWidth()I

    move-result v0

    invoke-static {v0, p1}, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->getDefaultSize(II)I

    move-result v0

    iget v1, p0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->PB:I

    iget v2, p0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->Qd:I

    add-int/2addr v1, v2

    invoke-static {v1, p2}, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->getDefaultSize(II)I

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->setMeasuredDimension(II)V

    return-void
.end method

.method protected onSizeChanged(IIII)V
    .locals 43

    invoke-super/range {p0 .. p4}, Landroid/view/View;->onSizeChanged(IIII)V

    move-object/from16 v0, p0

    iget v4, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->Qn:I

    move/from16 v0, p1

    if-ne v4, v0, :cond_0

    move-object/from16 v0, p0

    iget v4, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->Qm:I

    move/from16 v0, p2

    if-ne v4, v0, :cond_0

    return-void

    :cond_0
    move-object/from16 v0, p0

    iget v4, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->Qn:I

    if-eqz v4, :cond_1

    move-object/from16 v0, p0

    iget v4, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->Qm:I

    if-nez v4, :cond_2

    :cond_1
    return-void

    :cond_2
    move/from16 v0, p1

    move-object/from16 v1, p0

    iput v0, v1, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->Qn:I

    move/from16 v0, p2

    move-object/from16 v1, p0

    iput v0, v1, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->Qm:I

    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->Po:Landroid/graphics/Bitmap;

    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->Pt:Landroid/graphics/Canvas;

    move-object/from16 v0, p0

    iget v4, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->QK:I

    move-object/from16 v0, p0

    iget v5, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->QJ:I

    sub-int/2addr v4, v5

    mul-int/lit8 v5, v4, 0xa

    move-object/from16 v0, p0

    iget v6, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->PB:I

    add-int/2addr v5, v6

    move/from16 v0, p2

    if-le v0, v5, :cond_12

    const/4 v5, 0x1

    move-object/from16 v0, p0

    iput-boolean v5, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->Ql:Z

    mul-int/lit8 v5, v4, 0xf

    move/from16 v0, p2

    if-le v0, v5, :cond_11

    div-int/lit8 v5, v4, 0x2

    move-object/from16 v0, p0

    iput v5, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->Qt:I

    :goto_0
    move-object/from16 v0, p0

    iget v5, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->Qt:I

    if-gtz v5, :cond_3

    const/4 v5, 0x1

    move-object/from16 v0, p0

    iput v5, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->Qt:I

    :cond_3
    move-object/from16 v0, p0

    iget v5, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->Qd:I

    move-object/from16 v0, p0

    iput v5, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->Qs:I

    move-object/from16 v0, p0

    iget v5, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->Qv:I

    move-object/from16 v0, p0

    iget v6, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->QM:I

    mul-int/lit8 v6, v6, 0x3

    add-int/2addr v5, v6

    move-object/from16 v0, p0

    iput v5, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->Qp:I

    move/from16 v0, p1

    move-object/from16 v1, p0

    iput v0, v1, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->Qr:I

    move-object/from16 v0, p0

    iget v5, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->Qr:I

    move-object/from16 v0, p0

    iget v6, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->Qp:I

    sub-int v32, v5, v6

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->QL:Landroid/text/TextPaint;

    move-object/from16 v0, p0

    iget v6, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->QM:I

    int-to-float v6, v6

    invoke-virtual {v5, v6}, Landroid/text/TextPaint;->setStrokeWidth(F)V

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->Pl:Landroid/graphics/Paint;

    move-object/from16 v0, p0

    iget v6, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->QM:I

    int-to-float v6, v6

    invoke-virtual {v5, v6}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->Pn:Landroid/graphics/Paint;

    move-object/from16 v0, p0

    iget v6, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->QM:I

    int-to-float v6, v6

    invoke-virtual {v5, v6}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->Pk:Landroid/graphics/Paint;

    move-object/from16 v0, p0

    iget v6, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->QM:I

    int-to-float v6, v6

    invoke-virtual {v5, v6}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->Pz:Landroid/graphics/Paint;

    move-object/from16 v0, p0

    iget v6, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->Qt:I

    int-to-float v6, v6

    invoke-virtual {v5, v6}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->QE:Landroid/graphics/Paint;

    move-object/from16 v0, p0

    iget v6, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->Qt:I

    int-to-float v6, v6

    invoke-virtual {v5, v6}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->PW:Landroid/graphics/Paint;

    move-object/from16 v0, p0

    iget v6, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->Qt:I

    int-to-float v6, v6

    invoke-virtual {v5, v6}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->Pr:Landroid/graphics/Paint;

    move-object/from16 v0, p0

    iget v6, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->Qt:I

    int-to-float v6, v6

    invoke-virtual {v5, v6}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->PS:Landroid/graphics/Paint;

    move-object/from16 v0, p0

    iget v6, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->Qt:I

    int-to-float v6, v6

    invoke-virtual {v5, v6}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->QS:Landroid/graphics/Paint;

    move-object/from16 v0, p0

    iget v6, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->Qt:I

    int-to-float v6, v6

    invoke-virtual {v5, v6}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->PE:Landroid/graphics/Paint;

    move-object/from16 v0, p0

    iget v6, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->Qt:I

    int-to-float v6, v6

    invoke-virtual {v5, v6}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->PJ:Landroid/graphics/Paint;

    const/high16 v6, 0x3f800000    # 1.0f

    invoke-virtual {v5, v6}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    move-object/from16 v0, p0

    iget v5, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->Qt:I

    add-int/2addr v5, v4

    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->Ql:Z

    if-eqz v4, :cond_18

    move-object/from16 v0, p0

    iget v4, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->Qt:I

    move-object/from16 v0, p0

    iput v4, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->Py:I

    move-object/from16 v0, p0

    iget v4, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->Py:I

    add-int/2addr v4, v5

    move-object/from16 v0, p0

    iput v4, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->QD:I

    move-object/from16 v0, p0

    iget v4, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->QD:I

    add-int/2addr v4, v5

    move-object/from16 v0, p0

    iput v4, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->PD:I

    move-object/from16 v0, p0

    iget v4, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->PD:I

    add-int/2addr v4, v5

    move-object/from16 v0, p0

    iput v4, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->QR:I

    move-object/from16 v0, p0

    iget v6, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->QR:I

    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->Qc:Z

    if-eqz v4, :cond_13

    move v4, v5

    :goto_1
    add-int/2addr v4, v6

    move-object/from16 v0, p0

    iput v4, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->PV:I

    move-object/from16 v0, p0

    iget v6, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->PV:I

    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->Qa:Z

    if-eqz v4, :cond_14

    move v4, v5

    :goto_2
    add-int/2addr v4, v6

    move-object/from16 v0, p0

    iput v4, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->PR:I

    move-object/from16 v0, p0

    iget v6, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->PR:I

    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->PZ:Z

    if-eqz v4, :cond_15

    move v4, v5

    :goto_3
    add-int/2addr v4, v6

    move-object/from16 v0, p0

    iput v4, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->Pq:I

    move-object/from16 v0, p0

    iget v6, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->Pq:I

    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->PY:Z

    if-eqz v4, :cond_16

    move v4, v5

    :goto_4
    add-int/2addr v4, v6

    move-object/from16 v0, p0

    iput v4, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->QB:I

    move-object/from16 v0, p0

    iget v4, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->QB:I

    move-object/from16 v0, p0

    iget-boolean v6, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->Qb:Z

    if-eqz v6, :cond_17

    :goto_5
    add-int/2addr v4, v5

    move-object/from16 v0, p0

    iget v5, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->Qt:I

    mul-int/lit8 v5, v5, 0x2

    add-int/2addr v4, v5

    move-object/from16 v0, p0

    iget v5, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->Qt:I

    div-int/lit8 v5, v5, 0x2

    add-int/2addr v4, v5

    move-object/from16 v0, p0

    iput v4, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->Qq:I

    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->Qb:Z

    if-eqz v4, :cond_4

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->Qz:Lcom/android/settings/fuelgauge/BatteryHistoryChart$ChartData;

    move/from16 v0, p1

    invoke-virtual {v4, v0}, Lcom/android/settings/fuelgauge/BatteryHistoryChart$ChartData;->Ji(I)V

    :cond_4
    :goto_6
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->Pe:Landroid/graphics/Path;

    invoke-virtual {v4}, Landroid/graphics/Path;->reset()V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->Pc:Landroid/graphics/Path;

    invoke-virtual {v4}, Landroid/graphics/Path;->reset()V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->Pg:Landroid/graphics/Path;

    invoke-virtual {v4}, Landroid/graphics/Path;->reset()V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->QP:Landroid/graphics/Path;

    invoke-virtual {v4}, Landroid/graphics/Path;->reset()V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->Pb:Landroid/graphics/Path;

    invoke-virtual {v4}, Landroid/graphics/Path;->reset()V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->QF:Landroid/graphics/Path;

    invoke-virtual {v4}, Landroid/graphics/Path;->reset()V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->PX:Landroid/graphics/Path;

    invoke-virtual {v4}, Landroid/graphics/Path;->reset()V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->PT:Landroid/graphics/Path;

    invoke-virtual {v4}, Landroid/graphics/Path;->reset()V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->Ps:Landroid/graphics/Path;

    invoke-virtual {v4}, Landroid/graphics/Path;->reset()V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->QT:Landroid/graphics/Path;

    invoke-virtual {v4}, Landroid/graphics/Path;->reset()V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->PF:Landroid/graphics/Path;

    invoke-virtual {v4}, Landroid/graphics/Path;->reset()V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->PA:Landroid/graphics/Path;

    invoke-virtual {v4}, Landroid/graphics/Path;->reset()V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->QN:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->clear()V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->PG:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->clear()V

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->QG:J

    move-wide/from16 v34, v0

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->PP:J

    cmp-long v4, v4, v34

    if-lez v4, :cond_19

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->PP:J

    sub-long v4, v4, v34

    move-wide/from16 v20, v4

    :goto_7
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->QG:J

    move-wide/from16 v26, v0

    const-wide/16 v24, 0x0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->Pf:I

    move/from16 v33, v0

    move-object/from16 v0, p0

    iget v4, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->Pd:I

    move-object/from16 v0, p0

    iget v5, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->Pf:I

    sub-int v36, v4, v5

    move-object/from16 v0, p0

    iget v4, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->Qq:I

    sub-int v4, p2, v4

    move-object/from16 v0, p0

    iget v5, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->Qs:I

    sub-int v7, v4, v5

    move-object/from16 v0, p0

    iget v4, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->Qs:I

    add-int/2addr v4, v7

    move-object/from16 v0, p0

    iput v4, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->Qo:I

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->Qp:I

    move/from16 v22, v0

    move-object/from16 v0, p0

    iget v8, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->Qp:I

    const/4 v11, -0x1

    const/4 v9, -0x1

    const/4 v6, 0x0

    const/4 v10, 0x0

    const/16 v19, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    const/16 v17, 0x0

    const/4 v5, 0x0

    const/16 v18, 0x0

    const/4 v4, 0x0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->Qy:I

    move/from16 v37, v0

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->PO:J

    move-wide/from16 v28, v0

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->QG:J

    move-wide/from16 v30, v0

    cmp-long v23, v28, v30

    if-lez v23, :cond_1a

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->QH:Landroid/os/BatteryStats;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Landroid/os/BatteryStats;->startIteratingHistoryLocked()Z

    move-result v23

    if-eqz v23, :cond_4b

    new-instance v38, Landroid/os/BatteryStats$HistoryItem;

    invoke-direct/range {v38 .. v38}, Landroid/os/BatteryStats$HistoryItem;-><init>()V

    move/from16 v23, v5

    move/from16 v42, v4

    move-wide/from16 v4, v24

    move/from16 v24, v6

    move/from16 v25, v22

    move/from16 v22, v42

    :goto_8
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->QH:Landroid/os/BatteryStats;

    move-object/from16 v0, v38

    invoke-virtual {v6, v0}, Landroid/os/BatteryStats;->getNextHistoryLocked(Landroid/os/BatteryStats$HistoryItem;)Z

    move-result v6

    if-eqz v6, :cond_3f

    move/from16 v0, v24

    move/from16 v1, v37

    if-ge v0, v1, :cond_3f

    invoke-virtual/range {v38 .. v38}, Landroid/os/BatteryStats$HistoryItem;->isDeltaData()Z

    move-result v6

    if-eqz v6, :cond_39

    move-object/from16 v0, v38

    iget-wide v0, v0, Landroid/os/BatteryStats$HistoryItem;->time:J

    move-wide/from16 v28, v0

    sub-long v4, v28, v4

    add-long v30, v26, v4

    move-object/from16 v0, v38

    iget-wide v0, v0, Landroid/os/BatteryStats$HistoryItem;->time:J

    move-wide/from16 v28, v0

    move-object/from16 v0, p0

    iget v4, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->Qp:I

    sub-long v26, v30, v34

    move/from16 v0, v32

    int-to-long v0, v0

    move-wide/from16 v40, v0

    mul-long v26, v26, v40

    div-long v26, v26, v20

    move-wide/from16 v0, v26

    long-to-int v5, v0

    add-int/2addr v4, v5

    if-gez v4, :cond_5

    const/4 v4, 0x0

    :cond_5
    move-object/from16 v0, p0

    iget v5, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->Qs:I

    add-int/2addr v5, v7

    move-object/from16 v0, v38

    iget-byte v6, v0, Landroid/os/BatteryStats$HistoryItem;->batteryLevel:B

    sub-int v6, v6, v33

    add-int/lit8 v25, v7, -0x1

    mul-int v6, v6, v25

    div-int v6, v6, v36

    sub-int/2addr v5, v6

    if-eq v11, v4, :cond_9

    if-eq v9, v5, :cond_9

    move-object/from16 v0, v38

    iget-byte v6, v0, Landroid/os/BatteryStats$HistoryItem;->batteryLevel:B

    move-object/from16 v0, p0

    iget v9, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->Pj:I

    if-gt v6, v9, :cond_24

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->Pb:Landroid/graphics/Path;

    :goto_9
    move-object/from16 v0, v19

    if-eq v6, v0, :cond_26

    if-eqz v19, :cond_6

    int-to-float v9, v4

    int-to-float v11, v5

    move-object/from16 v0, v19

    invoke-virtual {v0, v9, v11}, Landroid/graphics/Path;->lineTo(FF)V

    :cond_6
    if-eqz v6, :cond_7

    int-to-float v9, v4

    int-to-float v11, v5

    invoke-virtual {v6, v9, v11}, Landroid/graphics/Path;->moveTo(FF)V

    :cond_7
    move-object/from16 v19, v6

    :cond_8
    :goto_a
    if-nez v10, :cond_27

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->Pe:Landroid/graphics/Path;

    int-to-float v6, v4

    int-to-float v8, v5

    invoke-virtual {v10, v6, v8}, Landroid/graphics/Path;->moveTo(FF)V

    move v8, v4

    :goto_b
    move v9, v5

    move v11, v4

    :cond_9
    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->Ql:Z

    if-eqz v5, :cond_4a

    move-object/from16 v0, v38

    iget v5, v0, Landroid/os/BatteryStats$HistoryItem;->states:I

    const/high16 v6, 0x80000

    and-int/2addr v5, v6

    if-eqz v5, :cond_28

    const/4 v5, 0x1

    :goto_c
    if-eq v5, v12, :cond_a

    if-eqz v5, :cond_29

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->PA:Landroid/graphics/Path;

    int-to-float v12, v4

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->Py:I

    move/from16 v25, v0

    sub-int v25, p2, v25

    move/from16 v0, v25

    int-to-float v0, v0

    move/from16 v25, v0

    move/from16 v0, v25

    invoke-virtual {v6, v12, v0}, Landroid/graphics/Path;->moveTo(FF)V

    :goto_d
    move v12, v5

    :cond_a
    move-object/from16 v0, v38

    iget v5, v0, Landroid/os/BatteryStats$HistoryItem;->states:I

    const/high16 v6, 0x100000

    and-int/2addr v5, v6

    if-eqz v5, :cond_2a

    const/4 v5, 0x1

    :goto_e
    if-eq v5, v13, :cond_b

    if-eqz v5, :cond_2b

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->QF:Landroid/graphics/Path;

    int-to-float v13, v4

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->QD:I

    move/from16 v25, v0

    sub-int v25, p2, v25

    move/from16 v0, v25

    int-to-float v0, v0

    move/from16 v25, v0

    move/from16 v0, v25

    invoke-virtual {v6, v13, v0}, Landroid/graphics/Path;->moveTo(FF)V

    :goto_f
    move v13, v5

    :cond_b
    move-object/from16 v0, v38

    iget v5, v0, Landroid/os/BatteryStats$HistoryItem;->states:I

    const/high16 v6, 0x20000000

    and-int/2addr v5, v6

    if-eqz v5, :cond_2c

    const/4 v5, 0x1

    :goto_10
    if-eq v5, v14, :cond_c

    if-eqz v5, :cond_2d

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->PX:Landroid/graphics/Path;

    int-to-float v14, v4

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->PV:I

    move/from16 v25, v0

    sub-int v25, p2, v25

    move/from16 v0, v25

    int-to-float v0, v0

    move/from16 v25, v0

    move/from16 v0, v25

    invoke-virtual {v6, v14, v0}, Landroid/graphics/Path;->moveTo(FF)V

    :goto_11
    move v14, v5

    :cond_c
    move-object/from16 v0, v38

    iget v5, v0, Landroid/os/BatteryStats$HistoryItem;->states2:I

    const/high16 v6, 0x8000000

    and-int/2addr v5, v6

    if-eqz v5, :cond_2e

    const/4 v5, 0x1

    :goto_12
    if-eq v5, v15, :cond_d

    if-eqz v5, :cond_2f

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->PT:Landroid/graphics/Path;

    int-to-float v15, v4

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->PR:I

    move/from16 v25, v0

    sub-int v25, p2, v25

    move/from16 v0, v25

    int-to-float v0, v0

    move/from16 v25, v0

    move/from16 v0, v25

    invoke-virtual {v6, v15, v0}, Landroid/graphics/Path;->moveTo(FF)V

    :goto_13
    move v15, v5

    :cond_d
    move-object/from16 v0, v38

    iget v5, v0, Landroid/os/BatteryStats$HistoryItem;->states2:I

    const/high16 v6, 0x200000

    and-int/2addr v5, v6

    if-eqz v5, :cond_30

    const/4 v5, 0x1

    :goto_14
    move/from16 v0, v16

    if-eq v5, v0, :cond_e

    if-eqz v5, :cond_31

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->Ps:Landroid/graphics/Path;

    int-to-float v0, v4

    move/from16 v16, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->Pq:I

    move/from16 v25, v0

    sub-int v25, p2, v25

    move/from16 v0, v25

    int-to-float v0, v0

    move/from16 v25, v0

    move/from16 v0, v16

    move/from16 v1, v25

    invoke-virtual {v6, v0, v1}, Landroid/graphics/Path;->moveTo(FF)V

    :goto_15
    move/from16 v16, v5

    :cond_e
    move-object/from16 v0, v38

    iget v5, v0, Landroid/os/BatteryStats$HistoryItem;->states2:I

    and-int/lit8 v5, v5, 0xf

    shr-int/lit8 v5, v5, 0x0

    move/from16 v0, v22

    if-eq v0, v5, :cond_32

    packed-switch v5, :pswitch_data_0

    :pswitch_0
    const/4 v6, 0x1

    const/16 v23, 0x1

    move/from16 v22, v5

    move v5, v6

    :goto_16
    move-object/from16 v0, v38

    iget v6, v0, Landroid/os/BatteryStats$HistoryItem;->states:I

    const/high16 v25, 0x18010000

    and-int v6, v6, v25

    if-eqz v6, :cond_49

    const/4 v6, 0x1

    :goto_17
    move/from16 v0, v17

    if-eq v6, v0, :cond_f

    if-eqz v6, :cond_33

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->QT:Landroid/graphics/Path;

    move-object/from16 v17, v0

    int-to-float v0, v4

    move/from16 v23, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->QR:I

    move/from16 v25, v0

    sub-int v25, p2, v25

    move/from16 v0, v25

    int-to-float v0, v0

    move/from16 v25, v0

    move-object/from16 v0, v17

    move/from16 v1, v23

    move/from16 v2, v25

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->moveTo(FF)V

    :goto_18
    move/from16 v17, v6

    :cond_f
    move-object/from16 v0, v38

    iget v6, v0, Landroid/os/BatteryStats$HistoryItem;->states:I

    const/high16 v23, -0x80000000

    and-int v6, v6, v23

    if-eqz v6, :cond_34

    const/4 v6, 0x1

    :goto_19
    move/from16 v0, v18

    if-eq v6, v0, :cond_10

    if-eqz v6, :cond_35

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->PF:Landroid/graphics/Path;

    move-object/from16 v18, v0

    int-to-float v0, v4

    move/from16 v23, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->PD:I

    move/from16 v25, v0

    sub-int v25, p2, v25

    move/from16 v0, v25

    int-to-float v0, v0

    move/from16 v25, v0

    move-object/from16 v0, v18

    move/from16 v1, v23

    move/from16 v2, v25

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->moveTo(FF)V

    :goto_1a
    move/from16 v18, v6

    :cond_10
    move-object/from16 v0, p0

    iget-boolean v6, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->Ql:Z

    if-eqz v6, :cond_36

    move-object/from16 v0, p0

    iget-boolean v6, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->Qb:Z

    if-eqz v6, :cond_48

    move-object/from16 v0, v38

    iget v6, v0, Landroid/os/BatteryStats$HistoryItem;->states:I

    and-int/lit16 v6, v6, 0x1c0

    shr-int/lit8 v6, v6, 0x6

    const/16 v23, 0x3

    move/from16 v0, v23

    if-ne v6, v0, :cond_37

    const/4 v6, 0x0

    :goto_1b
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->Qz:Lcom/android/settings/fuelgauge/BatteryHistoryChart$ChartData;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    invoke-virtual {v0, v4, v6}, Lcom/android/settings/fuelgauge/BatteryHistoryChart$ChartData;->Jf(II)V

    move-wide/from16 v26, v28

    move-wide/from16 v28, v30

    move/from16 v42, v22

    move/from16 v22, v4

    move/from16 v4, v42

    :goto_1c
    add-int/lit8 v6, v24, 0x1

    move/from16 v23, v5

    move/from16 v24, v6

    move/from16 v25, v22

    move/from16 v22, v4

    move-wide/from16 v4, v26

    move-wide/from16 v26, v28

    goto/16 :goto_8

    :cond_11
    div-int/lit8 v5, v4, 0x3

    move-object/from16 v0, p0

    iput v5, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->Qt:I

    goto/16 :goto_0

    :cond_12
    const/4 v5, 0x0

    move-object/from16 v0, p0

    iput-boolean v5, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->Ql:Z

    move-object/from16 v0, p0

    iget v5, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->QM:I

    move-object/from16 v0, p0

    iput v5, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->Qt:I

    goto/16 :goto_0

    :cond_13
    const/4 v4, 0x0

    goto/16 :goto_1

    :cond_14
    const/4 v4, 0x0

    goto/16 :goto_2

    :cond_15
    const/4 v4, 0x0

    goto/16 :goto_3

    :cond_16
    const/4 v4, 0x0

    goto/16 :goto_4

    :cond_17
    const/4 v5, 0x0

    goto/16 :goto_5

    :cond_18
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput v4, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->QB:I

    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput v4, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->Py:I

    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput v4, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->PD:I

    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput v4, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->QR:I

    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput v4, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->PR:I

    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput v4, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->Pq:I

    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput v4, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->PV:I

    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput v4, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->QD:I

    move-object/from16 v0, p0

    iget v4, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->QM:I

    mul-int/lit8 v4, v4, 0x4

    add-int/2addr v4, v5

    move-object/from16 v0, p0

    iput v4, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->Qq:I

    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->Qb:Z

    if-eqz v4, :cond_4

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->Qz:Lcom/android/settings/fuelgauge/BatteryHistoryChart$ChartData;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Lcom/android/settings/fuelgauge/BatteryHistoryChart$ChartData;->Ji(I)V

    goto/16 :goto_6

    :cond_19
    const-wide/16 v4, 0x1

    move-wide/from16 v20, v4

    goto/16 :goto_7

    :cond_1a
    move-object/from16 v5, v19

    :goto_1d
    if-ltz v9, :cond_1b

    if-gez v11, :cond_42

    :cond_1b
    move-object/from16 v0, p0

    iget v11, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->Qp:I

    move-object/from16 v0, p0

    iget v4, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->Qs:I

    add-int/2addr v4, v7

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->Qk:Lcom/android/settingslib/p;

    iget v6, v6, Lcom/android/settingslib/p;->cQe:I

    sub-int v6, v6, v33

    add-int/lit8 v9, v7, -0x1

    mul-int/2addr v6, v9

    div-int v6, v6, v36

    sub-int v9, v4, v6

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->Qk:Lcom/android/settingslib/p;

    iget v4, v4, Lcom/android/settingslib/p;->cQe:I

    int-to-byte v4, v4

    move-object/from16 v0, p0

    iget v6, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->Pj:I

    if-gt v4, v6, :cond_40

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->Pb:Landroid/graphics/Path;

    :goto_1e
    if-eqz v4, :cond_46

    int-to-float v5, v11

    int-to-float v6, v9

    invoke-virtual {v4, v5, v6}, Landroid/graphics/Path;->moveTo(FF)V

    :goto_1f
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->Pe:Landroid/graphics/Path;

    int-to-float v6, v11

    int-to-float v10, v9

    invoke-virtual {v5, v6, v10}, Landroid/graphics/Path;->moveTo(FF)V

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->Pe:Landroid/graphics/Path;

    move-object/from16 v19, v4

    move/from16 v5, p1

    :goto_20
    move-object/from16 v4, p0

    move/from16 v6, p2

    invoke-virtual/range {v4 .. v19}, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->Jc(IIIIILandroid/graphics/Path;IZZZZZZZLandroid/graphics/Path;)V

    move/from16 v0, p1

    if-ge v5, v0, :cond_1c

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->QP:Landroid/graphics/Path;

    int-to-float v6, v5

    int-to-float v8, v9

    invoke-virtual {v4, v6, v8}, Landroid/graphics/Path;->moveTo(FF)V

    move-object/from16 v0, p0

    iget v4, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->Qs:I

    add-int/2addr v4, v7

    rsub-int/lit8 v6, v33, 0x64

    add-int/lit8 v8, v7, -0x1

    mul-int/2addr v6, v8

    div-int v6, v6, v36

    sub-int/2addr v4, v6

    move-object/from16 v0, p0

    iget v6, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->Qs:I

    add-int/2addr v6, v7

    rsub-int/lit8 v8, v33, 0x0

    add-int/lit8 v7, v7, -0x1

    mul-int/2addr v7, v8

    div-int v7, v7, v36

    sub-int/2addr v6, v7

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->Qk:Lcom/android/settingslib/p;

    iget-boolean v7, v7, Lcom/android/settingslib/p;->cQf:Z

    if-eqz v7, :cond_43

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->QP:Landroid/graphics/Path;

    move-object/from16 v0, p0

    iget v7, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->Qr:I

    int-to-float v7, v7

    int-to-float v8, v6

    invoke-virtual {v4, v7, v8}, Landroid/graphics/Path;->lineTo(FF)V

    :goto_21
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->QP:Landroid/graphics/Path;

    int-to-float v5, v5

    int-to-float v6, v6

    invoke-virtual {v4, v5, v6}, Landroid/graphics/Path;->lineTo(FF)V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->QP:Landroid/graphics/Path;

    invoke-virtual {v4}, Landroid/graphics/Path;->close()V

    :cond_1c
    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->QG:J

    const-wide/16 v6, 0x0

    cmp-long v4, v4, v6

    if-lez v4, :cond_23

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->PP:J

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->QG:J

    cmp-long v4, v4, v6

    if-lez v4, :cond_23

    invoke-direct/range {p0 .. p0}, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->Jd()Z

    move-result v6

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v7

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->QG:J

    invoke-virtual {v7, v4, v5}, Ljava/util/Calendar;->setTimeInMillis(J)V

    const/16 v4, 0xe

    const/4 v5, 0x0

    invoke-virtual {v7, v4, v5}, Ljava/util/Calendar;->set(II)V

    const/16 v4, 0xd

    const/4 v5, 0x0

    invoke-virtual {v7, v4, v5}, Ljava/util/Calendar;->set(II)V

    const/16 v4, 0xc

    const/4 v5, 0x0

    invoke-virtual {v7, v4, v5}, Ljava/util/Calendar;->set(II)V

    invoke-virtual {v7}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v4

    move-object/from16 v0, p0

    iget-wide v8, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->QG:J

    cmp-long v8, v4, v8

    if-gez v8, :cond_1d

    const/16 v4, 0xb

    invoke-virtual {v7, v4}, Ljava/util/Calendar;->get(I)I

    move-result v4

    add-int/lit8 v4, v4, 0x1

    const/16 v5, 0xb

    invoke-virtual {v7, v5, v4}, Ljava/util/Calendar;->set(II)V

    invoke-virtual {v7}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v4

    :cond_1d
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v8

    move-object/from16 v0, p0

    iget-wide v10, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->PP:J

    invoke-virtual {v8, v10, v11}, Ljava/util/Calendar;->setTimeInMillis(J)V

    const/16 v9, 0xe

    const/4 v10, 0x0

    invoke-virtual {v8, v9, v10}, Ljava/util/Calendar;->set(II)V

    const/16 v9, 0xd

    const/4 v10, 0x0

    invoke-virtual {v8, v9, v10}, Ljava/util/Calendar;->set(II)V

    const/16 v9, 0xc

    const/4 v10, 0x0

    invoke-virtual {v8, v9, v10}, Ljava/util/Calendar;->set(II)V

    invoke-virtual {v8}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v10

    cmp-long v9, v4, v10

    if-gez v9, :cond_1f

    move-object/from16 v0, p0

    iget v9, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->Qp:I

    move-object/from16 v0, p0

    iget v12, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->Qr:I

    move-object/from16 v0, p0

    invoke-virtual {v0, v7, v9, v12, v6}, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->Ja(Ljava/util/Calendar;IIZ)V

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v9

    move-object/from16 v0, p0

    iget-wide v12, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->QG:J

    move-object/from16 v0, p0

    iget-wide v14, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->PP:J

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->QG:J

    move-wide/from16 v16, v0

    sub-long v14, v14, v16

    const-wide/16 v16, 0x2

    div-long v14, v14, v16

    add-long/2addr v12, v14

    invoke-virtual {v9, v12, v13}, Ljava/util/Calendar;->setTimeInMillis(J)V

    const/16 v12, 0xe

    const/4 v13, 0x0

    invoke-virtual {v9, v12, v13}, Ljava/util/Calendar;->set(II)V

    const/16 v12, 0xd

    const/4 v13, 0x0

    invoke-virtual {v9, v12, v13}, Ljava/util/Calendar;->set(II)V

    const/16 v12, 0xc

    const/4 v13, 0x0

    invoke-virtual {v9, v12, v13}, Ljava/util/Calendar;->set(II)V

    invoke-virtual {v9}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v12

    cmp-long v4, v12, v4

    if-lez v4, :cond_1e

    cmp-long v4, v12, v10

    if-gez v4, :cond_1e

    move-object/from16 v0, p0

    iget v4, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->Qp:I

    move-object/from16 v0, p0

    iget v5, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->Qr:I

    move-object/from16 v0, p0

    invoke-virtual {v0, v9, v4, v5, v6}, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->Ja(Ljava/util/Calendar;IIZ)V

    :cond_1e
    move-object/from16 v0, p0

    iget v4, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->Qp:I

    move-object/from16 v0, p0

    iget v5, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->Qr:I

    move-object/from16 v0, p0

    invoke-virtual {v0, v8, v4, v5, v6}, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->Ja(Ljava/util/Calendar;IIZ)V

    :cond_1f
    const/4 v4, 0x6

    invoke-virtual {v7, v4}, Ljava/util/Calendar;->get(I)I

    move-result v4

    const/4 v5, 0x6

    invoke-virtual {v8, v5}, Ljava/util/Calendar;->get(I)I

    move-result v5

    if-ne v4, v5, :cond_20

    const/4 v4, 0x1

    invoke-virtual {v7, v4}, Ljava/util/Calendar;->get(I)I

    move-result v4

    const/4 v5, 0x1

    invoke-virtual {v8, v5}, Ljava/util/Calendar;->get(I)I

    move-result v5

    if-eq v4, v5, :cond_23

    :cond_20
    invoke-direct/range {p0 .. p0}, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->Je()Z

    move-result v6

    const/16 v4, 0xb

    const/4 v5, 0x0

    invoke-virtual {v7, v4, v5}, Ljava/util/Calendar;->set(II)V

    invoke-virtual {v7}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v4

    move-object/from16 v0, p0

    iget-wide v10, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->QG:J

    cmp-long v9, v4, v10

    if-gez v9, :cond_21

    const/4 v4, 0x6

    invoke-virtual {v7, v4}, Ljava/util/Calendar;->get(I)I

    move-result v4

    add-int/lit8 v4, v4, 0x1

    const/4 v5, 0x6

    invoke-virtual {v7, v5, v4}, Ljava/util/Calendar;->set(II)V

    invoke-virtual {v7}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v4

    :cond_21
    const/16 v9, 0xb

    const/4 v10, 0x0

    invoke-virtual {v8, v9, v10}, Ljava/util/Calendar;->set(II)V

    invoke-virtual {v8}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v10

    cmp-long v9, v4, v10

    if-gez v9, :cond_22

    move-object/from16 v0, p0

    iget v9, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->Qp:I

    move-object/from16 v0, p0

    iget v12, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->Qr:I

    move-object/from16 v0, p0

    invoke-virtual {v0, v7, v9, v12, v6}, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->IZ(Ljava/util/Calendar;IIZ)V

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v7

    sub-long v12, v10, v4

    const-wide/16 v14, 0x2

    div-long/2addr v12, v14

    add-long/2addr v12, v4

    const-wide/32 v14, 0x6ddd00

    add-long/2addr v12, v14

    invoke-virtual {v7, v12, v13}, Ljava/util/Calendar;->setTimeInMillis(J)V

    const/16 v9, 0xb

    const/4 v12, 0x0

    invoke-virtual {v7, v9, v12}, Ljava/util/Calendar;->set(II)V

    const/16 v9, 0xc

    const/4 v12, 0x0

    invoke-virtual {v7, v9, v12}, Ljava/util/Calendar;->set(II)V

    invoke-virtual {v7}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v12

    cmp-long v4, v12, v4

    if-lez v4, :cond_22

    cmp-long v4, v12, v10

    if-gez v4, :cond_22

    move-object/from16 v0, p0

    iget v4, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->Qp:I

    move-object/from16 v0, p0

    iget v5, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->Qr:I

    move-object/from16 v0, p0

    invoke-virtual {v0, v7, v4, v5, v6}, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->IZ(Ljava/util/Calendar;IIZ)V

    :cond_22
    move-object/from16 v0, p0

    iget v4, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->Qp:I

    move-object/from16 v0, p0

    iget v5, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->Qr:I

    move-object/from16 v0, p0

    invoke-virtual {v0, v8, v4, v5, v6}, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->IZ(Ljava/util/Calendar;IIZ)V

    :cond_23
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->QN:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    const/4 v5, 0x2

    if-ge v4, v5, :cond_44

    invoke-virtual/range {p0 .. p0}, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->getContext()Landroid/content/Context;

    move-result-object v4

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->PP:J

    move-object/from16 v0, p0

    iget-wide v8, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->QG:J

    sub-long/2addr v6, v8

    invoke-static {v4, v6, v7}, Landroid/text/format/Formatter;->formatShortElapsedTime(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->PM:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->QL:Landroid/text/TextPaint;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->PM:Ljava/lang/String;

    invoke-virtual {v4, v5}, Landroid/text/TextPaint;->measureText(Ljava/lang/String;)F

    move-result v4

    float-to-int v4, v4

    move-object/from16 v0, p0

    iput v4, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->PN:I

    :goto_22
    return-void

    :cond_24
    move-object/from16 v0, p0

    iget v9, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->Pm:I

    if-gt v6, v9, :cond_25

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->Pg:Landroid/graphics/Path;

    goto/16 :goto_9

    :cond_25
    const/4 v6, 0x0

    goto/16 :goto_9

    :cond_26
    if-eqz v6, :cond_8

    int-to-float v9, v4

    int-to-float v11, v5

    invoke-virtual {v6, v9, v11}, Landroid/graphics/Path;->lineTo(FF)V

    goto/16 :goto_a

    :cond_27
    int-to-float v6, v4

    int-to-float v9, v5

    invoke-virtual {v10, v6, v9}, Landroid/graphics/Path;->lineTo(FF)V

    goto/16 :goto_b

    :cond_28
    const/4 v5, 0x0

    goto/16 :goto_c

    :cond_29
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->PA:Landroid/graphics/Path;

    int-to-float v12, v4

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->Py:I

    move/from16 v25, v0

    sub-int v25, p2, v25

    move/from16 v0, v25

    int-to-float v0, v0

    move/from16 v25, v0

    move/from16 v0, v25

    invoke-virtual {v6, v12, v0}, Landroid/graphics/Path;->lineTo(FF)V

    goto/16 :goto_d

    :cond_2a
    const/4 v5, 0x0

    goto/16 :goto_e

    :cond_2b
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->QF:Landroid/graphics/Path;

    int-to-float v13, v4

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->QD:I

    move/from16 v25, v0

    sub-int v25, p2, v25

    move/from16 v0, v25

    int-to-float v0, v0

    move/from16 v25, v0

    move/from16 v0, v25

    invoke-virtual {v6, v13, v0}, Landroid/graphics/Path;->lineTo(FF)V

    goto/16 :goto_f

    :cond_2c
    const/4 v5, 0x0

    goto/16 :goto_10

    :cond_2d
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->PX:Landroid/graphics/Path;

    int-to-float v14, v4

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->PV:I

    move/from16 v25, v0

    sub-int v25, p2, v25

    move/from16 v0, v25

    int-to-float v0, v0

    move/from16 v25, v0

    move/from16 v0, v25

    invoke-virtual {v6, v14, v0}, Landroid/graphics/Path;->lineTo(FF)V

    goto/16 :goto_11

    :cond_2e
    const/4 v5, 0x0

    goto/16 :goto_12

    :cond_2f
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->PT:Landroid/graphics/Path;

    int-to-float v15, v4

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->PR:I

    move/from16 v25, v0

    sub-int v25, p2, v25

    move/from16 v0, v25

    int-to-float v0, v0

    move/from16 v25, v0

    move/from16 v0, v25

    invoke-virtual {v6, v15, v0}, Landroid/graphics/Path;->lineTo(FF)V

    goto/16 :goto_13

    :cond_30
    const/4 v5, 0x0

    goto/16 :goto_14

    :cond_31
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->Ps:Landroid/graphics/Path;

    int-to-float v0, v4

    move/from16 v16, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->Pq:I

    move/from16 v25, v0

    sub-int v25, p2, v25

    move/from16 v0, v25

    int-to-float v0, v0

    move/from16 v25, v0

    move/from16 v0, v16

    move/from16 v1, v25

    invoke-virtual {v6, v0, v1}, Landroid/graphics/Path;->lineTo(FF)V

    goto/16 :goto_15

    :pswitch_1
    const/4 v6, 0x0

    const/16 v23, 0x0

    move/from16 v22, v5

    move v5, v6

    goto/16 :goto_16

    :cond_32
    move/from16 v5, v23

    goto/16 :goto_16

    :cond_33
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->QT:Landroid/graphics/Path;

    move-object/from16 v17, v0

    int-to-float v0, v4

    move/from16 v23, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->QR:I

    move/from16 v25, v0

    sub-int v25, p2, v25

    move/from16 v0, v25

    int-to-float v0, v0

    move/from16 v25, v0

    move-object/from16 v0, v17

    move/from16 v1, v23

    move/from16 v2, v25

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    goto/16 :goto_18

    :cond_34
    const/4 v6, 0x0

    goto/16 :goto_19

    :cond_35
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->PF:Landroid/graphics/Path;

    move-object/from16 v18, v0

    int-to-float v0, v4

    move/from16 v23, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->PD:I

    move/from16 v25, v0

    sub-int v25, p2, v25

    move/from16 v0, v25

    int-to-float v0, v0

    move/from16 v25, v0

    move-object/from16 v0, v18

    move/from16 v1, v23

    move/from16 v2, v25

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    goto/16 :goto_1a

    :cond_36
    move-wide/from16 v26, v28

    move-wide/from16 v28, v30

    move/from16 v42, v22

    move/from16 v22, v4

    move/from16 v4, v42

    goto/16 :goto_1c

    :cond_37
    move-object/from16 v0, v38

    iget v6, v0, Landroid/os/BatteryStats$HistoryItem;->states:I

    const/high16 v23, 0x200000

    and-int v6, v6, v23

    if-eqz v6, :cond_38

    const/4 v6, 0x1

    goto/16 :goto_1b

    :cond_38
    move-object/from16 v0, v38

    iget v6, v0, Landroid/os/BatteryStats$HistoryItem;->states:I

    and-int/lit8 v6, v6, 0x38

    shr-int/lit8 v6, v6, 0x3

    add-int/lit8 v6, v6, 0x2

    goto/16 :goto_1b

    :cond_39
    move-object/from16 v0, v38

    iget-byte v6, v0, Landroid/os/BatteryStats$HistoryItem;->cmd:B

    const/16 v28, 0x5

    move/from16 v0, v28

    if-eq v6, v0, :cond_3a

    move-object/from16 v0, v38

    iget-byte v6, v0, Landroid/os/BatteryStats$HistoryItem;->cmd:B

    const/16 v28, 0x7

    move/from16 v0, v28

    if-ne v6, v0, :cond_3c

    :cond_3a
    move-object/from16 v0, v38

    iget-wide v4, v0, Landroid/os/BatteryStats$HistoryItem;->currentTime:J

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->QG:J

    move-wide/from16 v28, v0

    cmp-long v4, v4, v28

    if-ltz v4, :cond_3d

    move-object/from16 v0, v38

    iget-wide v4, v0, Landroid/os/BatteryStats$HistoryItem;->currentTime:J

    :goto_23
    move-object/from16 v0, v38

    iget-wide v0, v0, Landroid/os/BatteryStats$HistoryItem;->time:J

    move-wide/from16 v28, v0

    move-wide/from16 v30, v4

    :goto_24
    move-object/from16 v0, v38

    iget-byte v4, v0, Landroid/os/BatteryStats$HistoryItem;->cmd:B

    const/4 v5, 0x6

    if-eq v4, v5, :cond_3e

    move-object/from16 v0, v38

    iget-byte v4, v0, Landroid/os/BatteryStats$HistoryItem;->cmd:B

    const/4 v5, 0x5

    if-ne v4, v5, :cond_3b

    sub-long v4, v26, v30

    invoke-static {v4, v5}, Ljava/lang/Math;->abs(J)J

    move-result-wide v4

    const-wide/32 v26, 0x36ee80

    cmp-long v4, v4, v26

    if-lez v4, :cond_3e

    :cond_3b
    if-eqz v10, :cond_47

    add-int/lit8 v5, v25, 0x1

    move-object/from16 v4, p0

    move/from16 v6, p2

    invoke-virtual/range {v4 .. v19}, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->Jc(IIIIILandroid/graphics/Path;IZZZZZZZLandroid/graphics/Path;)V

    const/4 v9, -0x1

    const/4 v11, -0x1

    const/4 v10, 0x0

    const/16 v19, 0x0

    const/16 v18, 0x0

    const/16 v16, 0x0

    const/4 v15, 0x0

    const/4 v14, 0x0

    const/4 v13, 0x0

    const/4 v12, 0x0

    move/from16 v4, v22

    move/from16 v5, v23

    move-wide/from16 v26, v28

    move/from16 v22, v25

    move-wide/from16 v28, v30

    goto/16 :goto_1c

    :cond_3c
    move-wide/from16 v28, v4

    move-wide/from16 v30, v26

    goto :goto_24

    :cond_3d
    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->QG:J

    move-object/from16 v0, v38

    iget-wide v0, v0, Landroid/os/BatteryStats$HistoryItem;->time:J

    move-wide/from16 v28, v0

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->Qj:J

    move-wide/from16 v30, v0

    sub-long v28, v28, v30

    add-long v4, v4, v28

    goto :goto_23

    :cond_3e
    move/from16 v4, v22

    move/from16 v5, v23

    move-wide/from16 v26, v28

    move/from16 v22, v25

    move-wide/from16 v28, v30

    goto/16 :goto_1c

    :cond_3f
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->QH:Landroid/os/BatteryStats;

    invoke-virtual {v4}, Landroid/os/BatteryStats;->finishIteratingHistoryLocked()V

    move-object/from16 v5, v19

    goto/16 :goto_1d

    :cond_40
    move-object/from16 v0, p0

    iget v6, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->Pm:I

    if-gt v4, v6, :cond_41

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->Pg:Landroid/graphics/Path;

    goto/16 :goto_1e

    :cond_41
    const/4 v4, 0x0

    goto/16 :goto_1e

    :cond_42
    move-object/from16 v0, p0

    iget v4, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->Qp:I

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->PO:J

    move-wide/from16 v22, v0

    sub-long v22, v22, v34

    move/from16 v0, v32

    int-to-long v0, v0

    move-wide/from16 v24, v0

    mul-long v22, v22, v24

    div-long v20, v22, v20

    move-wide/from16 v0, v20

    long-to-int v6, v0

    add-int/2addr v4, v6

    if-gez v4, :cond_45

    const/4 v4, 0x0

    move-object/from16 v19, v5

    move v5, v4

    goto/16 :goto_20

    :cond_43
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->QP:Landroid/graphics/Path;

    move-object/from16 v0, p0

    iget v8, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->Qr:I

    int-to-float v8, v8

    int-to-float v4, v4

    invoke-virtual {v7, v8, v4}, Landroid/graphics/Path;->lineTo(FF)V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->QP:Landroid/graphics/Path;

    move-object/from16 v0, p0

    iget v7, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->Qr:I

    int-to-float v7, v7

    int-to-float v8, v6

    invoke-virtual {v4, v7, v8}, Landroid/graphics/Path;->lineTo(FF)V

    goto/16 :goto_21

    :cond_44
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->PM:Ljava/lang/String;

    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput v4, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->PN:I

    goto/16 :goto_22

    :cond_45
    move-object/from16 v19, v5

    move v5, v4

    goto/16 :goto_20

    :cond_46
    move-object v4, v5

    goto/16 :goto_1f

    :cond_47
    move/from16 v4, v22

    move/from16 v5, v23

    move-wide/from16 v26, v28

    move/from16 v22, v25

    move-wide/from16 v28, v30

    goto/16 :goto_1c

    :cond_48
    move-wide/from16 v26, v28

    move-wide/from16 v28, v30

    move/from16 v42, v22

    move/from16 v22, v4

    move/from16 v4, v42

    goto/16 :goto_1c

    :cond_49
    move/from16 v6, v23

    goto/16 :goto_17

    :cond_4a
    move/from16 v5, v23

    move-wide/from16 v26, v28

    move-wide/from16 v28, v30

    move/from16 v42, v22

    move/from16 v22, v4

    move/from16 v4, v42

    goto/16 :goto_1c

    :cond_4b
    move-object/from16 v5, v19

    goto/16 :goto_1d

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method setStats(Landroid/os/BatteryStats;Landroid/content/Intent;)V
    .locals 18

    move-object/from16 v0, p1

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->QH:Landroid/os/BatteryStats;

    move-object/from16 v0, p2

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->Pi:Landroid/content/Intent;

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    const-wide/16 v4, 0x3e8

    mul-long/2addr v2, v4

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->QH:Landroid/os/BatteryStats;

    const/4 v5, 0x0

    invoke-virtual {v4, v2, v3, v5}, Landroid/os/BatteryStats;->computeBatteryRealtime(JI)J

    move-result-wide v4

    move-object/from16 v0, p0

    iput-wide v4, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->QI:J

    invoke-virtual/range {p0 .. p0}, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->getContext()Landroid/content/Context;

    move-result-object v4

    const v5, 0x7f120255

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->Px:Ljava/lang/String;

    invoke-virtual/range {p0 .. p0}, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->getContext()Landroid/content/Context;

    move-result-object v4

    const v5, 0x7f12025c

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->QC:Ljava/lang/String;

    invoke-virtual/range {p0 .. p0}, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->getContext()Landroid/content/Context;

    move-result-object v4

    const v5, 0x7f120258

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->PU:Ljava/lang/String;

    invoke-virtual/range {p0 .. p0}, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->getContext()Landroid/content/Context;

    move-result-object v4

    const v5, 0x7f120254

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->Pp:Ljava/lang/String;

    invoke-virtual/range {p0 .. p0}, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->getContext()Landroid/content/Context;

    move-result-object v4

    const v5, 0x7f120257

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->PQ:Ljava/lang/String;

    invoke-virtual/range {p0 .. p0}, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->getContext()Landroid/content/Context;

    move-result-object v4

    const v5, 0x7f12025e

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->QQ:Ljava/lang/String;

    invoke-virtual/range {p0 .. p0}, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->getContext()Landroid/content/Context;

    move-result-object v4

    const v5, 0x7f12025d

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->PC:Ljava/lang/String;

    invoke-virtual/range {p0 .. p0}, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->getContext()Landroid/content/Context;

    move-result-object v4

    const v5, 0x7f12025b

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->QA:Ljava/lang/String;

    const/16 v4, 0x64

    invoke-static {v4}, Lcom/android/settings/aq;->cqH(I)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->Qu:Ljava/lang/String;

    const/4 v4, 0x0

    invoke-static {v4}, Lcom/android/settings/aq;->cqH(I)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->Qw:Ljava/lang/String;

    invoke-virtual/range {p0 .. p0}, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->getContext()Landroid/content/Context;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->Pi:Landroid/content/Intent;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->QH:Landroid/os/BatteryStats;

    invoke-static {v4, v5, v6, v2, v3}, Lcom/android/settingslib/p;->cqn(Landroid/content/Context;Landroid/content/Intent;Landroid/os/BatteryStats;J)Lcom/android/settingslib/p;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->Qk:Lcom/android/settingslib/p;

    const-string/jumbo v2, ""

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->PK:Ljava/lang/String;

    const-string/jumbo v2, ""

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->Pu:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->Qk:Lcom/android/settingslib/p;

    iget-object v2, v2, Lcom/android/settingslib/p;->cPY:Ljava/lang/String;

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->setContentDescription(Ljava/lang/CharSequence;)V

    const/4 v11, 0x0

    const/4 v10, 0x0

    const/4 v5, -0x1

    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput v2, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->Pf:I

    const/16 v2, 0x64

    move-object/from16 v0, p0

    iput v2, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->Pd:I

    const-wide/16 v2, 0x0

    move-object/from16 v0, p0

    iput-wide v2, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->QG:J

    const-wide/16 v2, 0x0

    move-object/from16 v0, p0

    iput-wide v2, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->PO:J

    const-wide/16 v2, 0x0

    move-object/from16 v0, p0

    iput-wide v2, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->PP:J

    const-wide/16 v2, 0x0

    move-object/from16 v0, p0

    iput-wide v2, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->Qj:J

    const-wide/16 v2, 0x0

    move-object/from16 v0, p0

    iput-wide v2, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->Qi:J

    const-wide/16 v8, 0x0

    const-wide/16 v6, 0x0

    const/4 v4, 0x0

    const/4 v3, 0x0

    const/4 v2, 0x1

    invoke-virtual/range {p1 .. p1}, Landroid/os/BatteryStats;->startIteratingHistoryLocked()Z

    move-result v12

    if-eqz v12, :cond_8

    new-instance v12, Landroid/os/BatteryStats$HistoryItem;

    invoke-direct {v12}, Landroid/os/BatteryStats$HistoryItem;-><init>()V

    :cond_0
    :goto_0
    move-object/from16 v0, p1

    invoke-virtual {v0, v12}, Landroid/os/BatteryStats;->getNextHistoryLocked(Landroid/os/BatteryStats$HistoryItem;)Z

    move-result v13

    if-eqz v13, :cond_8

    add-int/lit8 v11, v11, 0x1

    if-eqz v2, :cond_1

    const/4 v2, 0x0

    iget-wide v14, v12, Landroid/os/BatteryStats$HistoryItem;->time:J

    move-object/from16 v0, p0

    iput-wide v14, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->Qj:J

    :cond_1
    iget-byte v13, v12, Landroid/os/BatteryStats$HistoryItem;->cmd:B

    const/4 v14, 0x5

    if-eq v13, v14, :cond_2

    iget-byte v13, v12, Landroid/os/BatteryStats$HistoryItem;->cmd:B

    const/4 v14, 0x7

    if-ne v13, v14, :cond_5

    :cond_2
    iget-wide v6, v12, Landroid/os/BatteryStats$HistoryItem;->currentTime:J

    const-wide v14, 0x39ef8b000L

    add-long/2addr v8, v14

    cmp-long v6, v6, v8

    if-gtz v6, :cond_3

    iget-wide v6, v12, Landroid/os/BatteryStats$HistoryItem;->time:J

    move-object/from16 v0, p0

    iget-wide v8, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->Qj:J

    const-wide/32 v14, 0x493e0

    add-long/2addr v8, v14

    cmp-long v6, v6, v8

    if-gez v6, :cond_4

    :cond_3
    const-wide/16 v6, 0x0

    move-object/from16 v0, p0

    iput-wide v6, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->QG:J

    :cond_4
    iget-wide v8, v12, Landroid/os/BatteryStats$HistoryItem;->currentTime:J

    iget-wide v6, v12, Landroid/os/BatteryStats$HistoryItem;->time:J

    move-object/from16 v0, p0

    iget-wide v14, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->QG:J

    const-wide/16 v16, 0x0

    cmp-long v13, v14, v16

    if-nez v13, :cond_5

    move-object/from16 v0, p0

    iget-wide v14, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->Qj:J

    sub-long v14, v6, v14

    sub-long v14, v8, v14

    move-object/from16 v0, p0

    iput-wide v14, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->QG:J

    :cond_5
    invoke-virtual {v12}, Landroid/os/BatteryStats$HistoryItem;->isDeltaData()Z

    move-result v13

    if-eqz v13, :cond_0

    iget-byte v10, v12, Landroid/os/BatteryStats$HistoryItem;->batteryLevel:B

    if-ne v10, v5, :cond_6

    const/4 v10, 0x1

    if-ne v11, v10, :cond_7

    :cond_6
    iget-byte v5, v12, Landroid/os/BatteryStats$HistoryItem;->batteryLevel:B

    :cond_7
    iget-wide v14, v12, Landroid/os/BatteryStats$HistoryItem;->time:J

    move-object/from16 v0, p0

    iput-wide v14, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->Qh:J

    iget v10, v12, Landroid/os/BatteryStats$HistoryItem;->states:I

    or-int/2addr v4, v10

    iget v10, v12, Landroid/os/BatteryStats$HistoryItem;->states2:I

    or-int/2addr v3, v10

    move v10, v11

    goto :goto_0

    :cond_8
    move-object/from16 v0, p0

    iget-wide v12, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->Qh:J

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->Qk:Lcom/android/settingslib/p;

    iget-wide v14, v2, Lcom/android/settingslib/p;->cQd:J

    const-wide/16 v16, 0x3e8

    div-long v14, v14, v16

    add-long/2addr v12, v14

    move-object/from16 v0, p0

    iput-wide v12, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->Qi:J

    move-object/from16 v0, p0

    iget-wide v12, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->Qh:J

    add-long/2addr v8, v12

    sub-long v6, v8, v6

    move-object/from16 v0, p0

    iput-wide v6, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->PO:J

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->PO:J

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->Qk:Lcom/android/settingslib/p;

    iget-wide v8, v2, Lcom/android/settingslib/p;->cQd:J

    const-wide/16 v12, 0x3e8

    div-long/2addr v8, v12

    add-long/2addr v6, v8

    move-object/from16 v0, p0

    iput-wide v6, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->PP:J

    move-object/from16 v0, p0

    iput v10, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->Qy:I

    const/high16 v2, 0x20000000

    and-int/2addr v2, v4

    if-eqz v2, :cond_b

    const/4 v2, 0x1

    :goto_1
    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->Qa:Z

    const/high16 v2, 0x8000000

    and-int/2addr v2, v3

    if-eqz v2, :cond_c

    const/4 v2, 0x1

    :goto_2
    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->PZ:Z

    const/high16 v2, 0x200000

    and-int/2addr v2, v3

    if-eqz v2, :cond_d

    const/4 v2, 0x1

    :goto_3
    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->PY:Z

    const/high16 v2, 0x20000000

    and-int/2addr v2, v3

    if-nez v2, :cond_e

    const/high16 v2, 0x18010000

    and-int/2addr v2, v4

    if-eqz v2, :cond_f

    const/4 v2, 0x1

    :goto_4
    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->Qc:Z

    invoke-virtual/range {p0 .. p0}, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/android/settings/aq;->bqv(Landroid/content/Context;)Z

    move-result v2

    if-nez v2, :cond_9

    const/4 v2, 0x1

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->Qb:Z

    :cond_9
    move-object/from16 v0, p0

    iget-wide v2, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->Qi:J

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->Qj:J

    cmp-long v2, v2, v4

    if-gtz v2, :cond_a

    move-object/from16 v0, p0

    iget-wide v2, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->Qj:J

    const-wide/16 v4, 0x1

    add-long/2addr v2, v4

    move-object/from16 v0, p0

    iput-wide v2, v0, Lcom/android/settings/fuelgauge/BatteryHistoryChart;->Qi:J

    :cond_a
    return-void

    :cond_b
    const/4 v2, 0x0

    goto :goto_1

    :cond_c
    const/4 v2, 0x0

    goto :goto_2

    :cond_d
    const/4 v2, 0x0

    goto :goto_3

    :cond_e
    const/4 v2, 0x1

    goto :goto_4

    :cond_f
    const/4 v2, 0x0

    goto :goto_4
.end method
