.class public Lcom/android/settings/fuelgauge/BatteryHistoryDetail;
.super Lcom/android/settings/MiuiSettingsPreferenceFragment;
.source "BatteryHistoryDetail.java"


# instance fields
.field private Rk:Landroid/content/Intent;

.field private Rl:Lcom/android/settings/fuelgauge/BatteryFlagParser;

.field private Rm:Lcom/android/settings/fuelgauge/BatteryFlagParser;

.field private Rn:Lcom/android/settings/fuelgauge/BatteryFlagParser;

.field private Ro:Lcom/android/settings/fuelgauge/BatteryFlagParser;

.field private Rp:Lcom/android/settings/fuelgauge/BatteryFlagParser;

.field private Rq:Z

.field private Rr:Lcom/android/settings/fuelgauge/BatteryCellParser;

.field private Rs:Lcom/android/settings/fuelgauge/BatteryFlagParser;

.field private Rt:Landroid/os/BatteryStats;

.field private Ru:Lcom/android/settings/fuelgauge/BatteryWifiParser;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/MiuiSettingsPreferenceFragment;-><init>()V

    return-void
.end method

.method private Jn(Lcom/android/settings/fuelgauge/BatteryActiveView$BatteryActiveProvider;II)V
    .locals 2

    invoke-virtual {p0}, Lcom/android/settings/fuelgauge/BatteryHistoryDetail;->getView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-interface {p1}, Lcom/android/settings/fuelgauge/BatteryActiveView$BatteryActiveProvider;->Iz()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    const v0, 0x1020016

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, p2}, Landroid/widget/TextView;->setText(I)V

    const v0, 0x7f0a0087

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/settings/fuelgauge/BatteryActiveView;

    invoke-virtual {v0, p1}, Lcom/android/settings/fuelgauge/BatteryActiveView;->setProvider(Lcom/android/settings/fuelgauge/BatteryActiveView$BatteryActiveProvider;)V

    return-void

    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method private Jo()V
    .locals 13

    const/4 v12, 0x4

    const/4 v11, 0x3

    const/4 v10, 0x2

    const/4 v9, 0x1

    const/4 v8, 0x0

    invoke-virtual {p0}, Lcom/android/settings/fuelgauge/BatteryHistoryDetail;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/fuelgauge/BatteryHistoryDetail;->Rk:Landroid/content/Intent;

    iget-object v2, p0, Lcom/android/settings/fuelgauge/BatteryHistoryDetail;->Rt:Landroid/os/BatteryStats;

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v4

    const-wide/16 v6, 0x3e8

    mul-long/2addr v4, v6

    invoke-static {v0, v1, v2, v4, v5}, Lcom/android/settingslib/p;->cqn(Landroid/content/Context;Landroid/content/Intent;Landroid/os/BatteryStats;J)Lcom/android/settingslib/p;

    move-result-object v1

    invoke-virtual {p0}, Lcom/android/settings/fuelgauge/BatteryHistoryDetail;->getView()Landroid/view/View;

    move-result-object v2

    iget-boolean v0, p0, Lcom/android/settings/fuelgauge/BatteryHistoryDetail;->Rq:Z

    if-eqz v0, :cond_0

    const v0, 0x7f0a008d

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/settingslib/graph/UsageView;

    const/16 v3, 0x8

    new-array v3, v3, [Lcom/android/settingslib/r;

    iget-object v4, p0, Lcom/android/settings/fuelgauge/BatteryHistoryDetail;->Rm:Lcom/android/settings/fuelgauge/BatteryFlagParser;

    aput-object v4, v3, v8

    iget-object v4, p0, Lcom/android/settings/fuelgauge/BatteryHistoryDetail;->Rs:Lcom/android/settings/fuelgauge/BatteryFlagParser;

    aput-object v4, v3, v9

    iget-object v4, p0, Lcom/android/settings/fuelgauge/BatteryHistoryDetail;->Rp:Lcom/android/settings/fuelgauge/BatteryFlagParser;

    aput-object v4, v3, v10

    iget-object v4, p0, Lcom/android/settings/fuelgauge/BatteryHistoryDetail;->Ro:Lcom/android/settings/fuelgauge/BatteryFlagParser;

    aput-object v4, v3, v11

    iget-object v4, p0, Lcom/android/settings/fuelgauge/BatteryHistoryDetail;->Rl:Lcom/android/settings/fuelgauge/BatteryFlagParser;

    aput-object v4, v3, v12

    iget-object v4, p0, Lcom/android/settings/fuelgauge/BatteryHistoryDetail;->Ru:Lcom/android/settings/fuelgauge/BatteryWifiParser;

    const/4 v5, 0x5

    aput-object v4, v3, v5

    iget-object v4, p0, Lcom/android/settings/fuelgauge/BatteryHistoryDetail;->Rn:Lcom/android/settings/fuelgauge/BatteryFlagParser;

    const/4 v5, 0x6

    aput-object v4, v3, v5

    iget-object v4, p0, Lcom/android/settings/fuelgauge/BatteryHistoryDetail;->Rr:Lcom/android/settings/fuelgauge/BatteryCellParser;

    const/4 v5, 0x7

    aput-object v4, v3, v5

    invoke-virtual {v1, v0, v3}, Lcom/android/settingslib/p;->cqp(Lcom/android/settingslib/graph/UsageView;[Lcom/android/settingslib/r;)V

    :goto_0
    const v0, 0x7f0a00d8

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v3, v1, Lcom/android/settingslib/p;->cPZ:Ljava/lang/String;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v0, 0x7f0a0180

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v1, v1, Lcom/android/settingslib/p;->cQb:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/android/settings/fuelgauge/BatteryHistoryDetail;->Rm:Lcom/android/settings/fuelgauge/BatteryFlagParser;

    const v1, 0x7f120255

    const v3, 0x7f0a00d9

    invoke-direct {p0, v0, v1, v3}, Lcom/android/settings/fuelgauge/BatteryHistoryDetail;->Jn(Lcom/android/settings/fuelgauge/BatteryActiveView$BatteryActiveProvider;II)V

    iget-object v0, p0, Lcom/android/settings/fuelgauge/BatteryHistoryDetail;->Rs:Lcom/android/settings/fuelgauge/BatteryFlagParser;

    const v1, 0x7f12025c

    const v3, 0x7f0a03af

    invoke-direct {p0, v0, v1, v3}, Lcom/android/settings/fuelgauge/BatteryHistoryDetail;->Jn(Lcom/android/settings/fuelgauge/BatteryActiveView$BatteryActiveProvider;II)V

    iget-object v0, p0, Lcom/android/settings/fuelgauge/BatteryHistoryDetail;->Rp:Lcom/android/settings/fuelgauge/BatteryFlagParser;

    const v1, 0x7f120258

    const v3, 0x7f0a01ca

    invoke-direct {p0, v0, v1, v3}, Lcom/android/settings/fuelgauge/BatteryHistoryDetail;->Jn(Lcom/android/settings/fuelgauge/BatteryActiveView$BatteryActiveProvider;II)V

    iget-object v0, p0, Lcom/android/settings/fuelgauge/BatteryHistoryDetail;->Ro:Lcom/android/settings/fuelgauge/BatteryFlagParser;

    const v1, 0x7f120257

    const v3, 0x7f0a01a3

    invoke-direct {p0, v0, v1, v3}, Lcom/android/settings/fuelgauge/BatteryHistoryDetail;->Jn(Lcom/android/settings/fuelgauge/BatteryActiveView$BatteryActiveProvider;II)V

    iget-object v0, p0, Lcom/android/settings/fuelgauge/BatteryHistoryDetail;->Rl:Lcom/android/settings/fuelgauge/BatteryFlagParser;

    const v1, 0x7f120254

    const v3, 0x7f0a00c5

    invoke-direct {p0, v0, v1, v3}, Lcom/android/settings/fuelgauge/BatteryHistoryDetail;->Jn(Lcom/android/settings/fuelgauge/BatteryActiveView$BatteryActiveProvider;II)V

    iget-object v0, p0, Lcom/android/settings/fuelgauge/BatteryHistoryDetail;->Ru:Lcom/android/settings/fuelgauge/BatteryWifiParser;

    const v1, 0x7f12025e

    const v3, 0x7f0a051f

    invoke-direct {p0, v0, v1, v3}, Lcom/android/settings/fuelgauge/BatteryHistoryDetail;->Jn(Lcom/android/settings/fuelgauge/BatteryActiveView$BatteryActiveProvider;II)V

    iget-object v0, p0, Lcom/android/settings/fuelgauge/BatteryHistoryDetail;->Rn:Lcom/android/settings/fuelgauge/BatteryFlagParser;

    const v1, 0x7f12025d

    const v3, 0x7f0a010b

    invoke-direct {p0, v0, v1, v3}, Lcom/android/settings/fuelgauge/BatteryHistoryDetail;->Jn(Lcom/android/settings/fuelgauge/BatteryActiveView$BatteryActiveProvider;II)V

    iget-boolean v0, p0, Lcom/android/settings/fuelgauge/BatteryHistoryDetail;->Rq:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/settings/fuelgauge/BatteryHistoryDetail;->Rr:Lcom/android/settings/fuelgauge/BatteryCellParser;

    const v1, 0x7f12025b

    const v2, 0x7f0a00d0

    invoke-direct {p0, v0, v1, v2}, Lcom/android/settings/fuelgauge/BatteryHistoryDetail;->Jn(Lcom/android/settings/fuelgauge/BatteryActiveView$BatteryActiveProvider;II)V

    :goto_1
    return-void

    :cond_0
    const v0, 0x7f0a008d

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/settingslib/graph/UsageView;

    const/4 v3, 0x7

    new-array v3, v3, [Lcom/android/settingslib/r;

    iget-object v4, p0, Lcom/android/settings/fuelgauge/BatteryHistoryDetail;->Rm:Lcom/android/settings/fuelgauge/BatteryFlagParser;

    aput-object v4, v3, v8

    iget-object v4, p0, Lcom/android/settings/fuelgauge/BatteryHistoryDetail;->Rs:Lcom/android/settings/fuelgauge/BatteryFlagParser;

    aput-object v4, v3, v9

    iget-object v4, p0, Lcom/android/settings/fuelgauge/BatteryHistoryDetail;->Rp:Lcom/android/settings/fuelgauge/BatteryFlagParser;

    aput-object v4, v3, v10

    iget-object v4, p0, Lcom/android/settings/fuelgauge/BatteryHistoryDetail;->Ro:Lcom/android/settings/fuelgauge/BatteryFlagParser;

    aput-object v4, v3, v11

    iget-object v4, p0, Lcom/android/settings/fuelgauge/BatteryHistoryDetail;->Rl:Lcom/android/settings/fuelgauge/BatteryFlagParser;

    aput-object v4, v3, v12

    iget-object v4, p0, Lcom/android/settings/fuelgauge/BatteryHistoryDetail;->Ru:Lcom/android/settings/fuelgauge/BatteryWifiParser;

    const/4 v5, 0x5

    aput-object v4, v3, v5

    iget-object v4, p0, Lcom/android/settings/fuelgauge/BatteryHistoryDetail;->Rn:Lcom/android/settings/fuelgauge/BatteryFlagParser;

    const/4 v5, 0x6

    aput-object v4, v3, v5

    invoke-virtual {v1, v0, v3}, Lcom/android/settingslib/p;->cqp(Lcom/android/settingslib/graph/UsageView;[Lcom/android/settingslib/r;)V

    goto/16 :goto_0

    :cond_1
    const v0, 0x7f0a00d0

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1
.end method


# virtual methods
.method public getMetricsCategory()I
    .locals 1

    const/16 v0, 0x33

    return v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 5

    const/4 v4, 0x0

    const/4 v3, 0x1

    invoke-super {p0, p1}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/android/settings/fuelgauge/BatteryHistoryDetail;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string/jumbo v1, "stats"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lcom/android/settings/fuelgauge/BatteryHistoryDetail;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/android/internal/os/BatteryStatsHelper;->statsFromFile(Landroid/content/Context;Ljava/lang/String;)Landroid/os/BatteryStats;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/fuelgauge/BatteryHistoryDetail;->Rt:Landroid/os/BatteryStats;

    invoke-virtual {p0}, Lcom/android/settings/fuelgauge/BatteryHistoryDetail;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string/jumbo v1, "broadcast"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/content/Intent;

    iput-object v0, p0, Lcom/android/settings/fuelgauge/BatteryHistoryDetail;->Rk:Landroid/content/Intent;

    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    invoke-virtual {p0}, Lcom/android/settings/fuelgauge/BatteryHistoryDetail;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v1

    const v2, 0x1010435

    invoke-virtual {v1, v2, v0, v3}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    invoke-virtual {p0}, Lcom/android/settings/fuelgauge/BatteryHistoryDetail;->getContext()Landroid/content/Context;

    move-result-object v1

    iget v0, v0, Landroid/util/TypedValue;->resourceId:I

    invoke-virtual {v1, v0}, Landroid/content/Context;->getColor(I)I

    move-result v0

    new-instance v1, Lcom/android/settings/fuelgauge/BatteryFlagParser;

    const/high16 v2, 0x80000

    invoke-direct {v1, v0, v4, v2}, Lcom/android/settings/fuelgauge/BatteryFlagParser;-><init>(IZI)V

    iput-object v1, p0, Lcom/android/settings/fuelgauge/BatteryHistoryDetail;->Rm:Lcom/android/settings/fuelgauge/BatteryFlagParser;

    new-instance v1, Lcom/android/settings/fuelgauge/BatteryFlagParser;

    const/high16 v2, 0x100000

    invoke-direct {v1, v0, v4, v2}, Lcom/android/settings/fuelgauge/BatteryFlagParser;-><init>(IZI)V

    iput-object v1, p0, Lcom/android/settings/fuelgauge/BatteryHistoryDetail;->Rs:Lcom/android/settings/fuelgauge/BatteryFlagParser;

    new-instance v1, Lcom/android/settings/fuelgauge/BatteryFlagParser;

    const/high16 v2, 0x20000000

    invoke-direct {v1, v0, v4, v2}, Lcom/android/settings/fuelgauge/BatteryFlagParser;-><init>(IZI)V

    iput-object v1, p0, Lcom/android/settings/fuelgauge/BatteryHistoryDetail;->Rp:Lcom/android/settings/fuelgauge/BatteryFlagParser;

    new-instance v1, Lcom/android/settings/fuelgauge/BatteryFlagParser;

    const/high16 v2, 0x8000000

    invoke-direct {v1, v0, v3, v2}, Lcom/android/settings/fuelgauge/BatteryFlagParser;-><init>(IZI)V

    iput-object v1, p0, Lcom/android/settings/fuelgauge/BatteryHistoryDetail;->Ro:Lcom/android/settings/fuelgauge/BatteryFlagParser;

    new-instance v1, Lcom/android/settings/fuelgauge/BatteryFlagParser;

    const/high16 v2, 0x200000

    invoke-direct {v1, v0, v3, v2}, Lcom/android/settings/fuelgauge/BatteryFlagParser;-><init>(IZI)V

    iput-object v1, p0, Lcom/android/settings/fuelgauge/BatteryHistoryDetail;->Rl:Lcom/android/settings/fuelgauge/BatteryFlagParser;

    new-instance v1, Lcom/android/settings/fuelgauge/BatteryWifiParser;

    invoke-direct {v1, v0}, Lcom/android/settings/fuelgauge/BatteryWifiParser;-><init>(I)V

    iput-object v1, p0, Lcom/android/settings/fuelgauge/BatteryHistoryDetail;->Ru:Lcom/android/settings/fuelgauge/BatteryWifiParser;

    new-instance v1, Lcom/android/settings/fuelgauge/BatteryFlagParser;

    const/high16 v2, -0x80000000

    invoke-direct {v1, v0, v4, v2}, Lcom/android/settings/fuelgauge/BatteryFlagParser;-><init>(IZI)V

    iput-object v1, p0, Lcom/android/settings/fuelgauge/BatteryHistoryDetail;->Rn:Lcom/android/settings/fuelgauge/BatteryFlagParser;

    invoke-virtual {p0}, Lcom/android/settings/fuelgauge/BatteryHistoryDetail;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/android/settings/aq;->bqv(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    iput-boolean v3, p0, Lcom/android/settings/fuelgauge/BatteryHistoryDetail;->Rq:Z

    :cond_0
    iget-boolean v0, p0, Lcom/android/settings/fuelgauge/BatteryHistoryDetail;->Rq:Z

    if-eqz v0, :cond_1

    new-instance v0, Lcom/android/settings/fuelgauge/BatteryCellParser;

    invoke-direct {v0}, Lcom/android/settings/fuelgauge/BatteryCellParser;-><init>()V

    iput-object v0, p0, Lcom/android/settings/fuelgauge/BatteryHistoryDetail;->Rr:Lcom/android/settings/fuelgauge/BatteryCellParser;

    :cond_1
    invoke-virtual {p0, v3}, Lcom/android/settings/fuelgauge/BatteryHistoryDetail;->setHasOptionsMenu(Z)V

    return-void
.end method

.method public onInflateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2

    const v0, 0x7f0d0049

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 0

    invoke-super {p0, p1, p2}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    invoke-direct {p0}, Lcom/android/settings/fuelgauge/BatteryHistoryDetail;->Jo()V

    return-void
.end method
