.class Lcom/android/settings/fuelgauge/BatteryHistoryChart$ChartData;
.super Ljava/lang/Object;
.source "BatteryHistoryChart.java"


# instance fields
.field QU:[I

.field QV:I

.field QW:I

.field QX:[Landroid/graphics/Paint;

.field QY:[I


# direct methods
.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method Jf(II)V
    .locals 4

    iget v0, p0, Lcom/android/settings/fuelgauge/BatteryHistoryChart$ChartData;->QV:I

    if-eq p2, v0, :cond_0

    iget v0, p0, Lcom/android/settings/fuelgauge/BatteryHistoryChart$ChartData;->QW:I

    iget-object v1, p0, Lcom/android/settings/fuelgauge/BatteryHistoryChart$ChartData;->QY:[I

    array-length v1, v1

    if-ge v0, v1, :cond_0

    iget-object v0, p0, Lcom/android/settings/fuelgauge/BatteryHistoryChart$ChartData;->QY:[I

    iget v1, p0, Lcom/android/settings/fuelgauge/BatteryHistoryChart$ChartData;->QW:I

    const v2, 0xffff

    and-int/2addr v2, p1

    shl-int/lit8 v3, p2, 0x10

    or-int/2addr v2, v3

    aput v2, v0, v1

    iget v0, p0, Lcom/android/settings/fuelgauge/BatteryHistoryChart$ChartData;->QW:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/android/settings/fuelgauge/BatteryHistoryChart$ChartData;->QW:I

    iput p2, p0, Lcom/android/settings/fuelgauge/BatteryHistoryChart$ChartData;->QV:I

    :cond_0
    return-void
.end method

.method Jg(Landroid/graphics/Canvas;II)V
    .locals 10

    const/4 v0, 0x0

    add-int v9, p2, p3

    move v6, v0

    move v5, v0

    :goto_0
    iget v1, p0, Lcom/android/settings/fuelgauge/BatteryHistoryChart$ChartData;->QW:I

    if-ge v6, v1, :cond_1

    iget-object v1, p0, Lcom/android/settings/fuelgauge/BatteryHistoryChart$ChartData;->QY:[I

    aget v1, v1, v6

    const v2, 0xffff

    and-int v7, v1, v2

    const/high16 v2, -0x10000

    and-int/2addr v1, v2

    shr-int/lit8 v8, v1, 0x10

    if-eqz v5, :cond_0

    int-to-float v1, v0

    int-to-float v2, p2

    int-to-float v3, v7

    int-to-float v4, v9

    iget-object v0, p0, Lcom/android/settings/fuelgauge/BatteryHistoryChart$ChartData;->QX:[Landroid/graphics/Paint;

    aget-object v5, v0, v5

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    :cond_0
    add-int/lit8 v0, v6, 0x1

    move v6, v0

    move v5, v8

    move v0, v7

    goto :goto_0

    :cond_1
    return-void
.end method

.method Jh(I)V
    .locals 2

    const/4 v1, 0x0

    iget v0, p0, Lcom/android/settings/fuelgauge/BatteryHistoryChart$ChartData;->QV:I

    if-eqz v0, :cond_0

    invoke-virtual {p0, p1, v1}, Lcom/android/settings/fuelgauge/BatteryHistoryChart$ChartData;->Jf(II)V

    :cond_0
    return-void
.end method

.method Ji(I)V
    .locals 2

    const/4 v1, 0x0

    if-lez p1, :cond_0

    mul-int/lit8 v0, p1, 0x2

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/android/settings/fuelgauge/BatteryHistoryChart$ChartData;->QY:[I

    :goto_0
    iput v1, p0, Lcom/android/settings/fuelgauge/BatteryHistoryChart$ChartData;->QW:I

    iput v1, p0, Lcom/android/settings/fuelgauge/BatteryHistoryChart$ChartData;->QV:I

    return-void

    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/settings/fuelgauge/BatteryHistoryChart$ChartData;->QY:[I

    goto :goto_0
.end method

.method Jj([I)V
    .locals 3

    iput-object p1, p0, Lcom/android/settings/fuelgauge/BatteryHistoryChart$ChartData;->QU:[I

    array-length v0, p1

    new-array v0, v0, [Landroid/graphics/Paint;

    iput-object v0, p0, Lcom/android/settings/fuelgauge/BatteryHistoryChart$ChartData;->QX:[Landroid/graphics/Paint;

    const/4 v0, 0x0

    :goto_0
    array-length v1, p1

    if-ge v0, v1, :cond_0

    iget-object v1, p0, Lcom/android/settings/fuelgauge/BatteryHistoryChart$ChartData;->QX:[Landroid/graphics/Paint;

    new-instance v2, Landroid/graphics/Paint;

    invoke-direct {v2}, Landroid/graphics/Paint;-><init>()V

    aput-object v2, v1, v0

    iget-object v1, p0, Lcom/android/settings/fuelgauge/BatteryHistoryChart$ChartData;->QX:[Landroid/graphics/Paint;

    aget-object v1, v1, v0

    aget v2, p1, v0

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    iget-object v1, p0, Lcom/android/settings/fuelgauge/BatteryHistoryChart$ChartData;->QX:[Landroid/graphics/Paint;

    aget-object v1, v1, v0

    sget-object v2, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method
