.class final Lcom/android/settings/fuelgauge/BatterySaverController$BatteryStateChangeReceiver;
.super Landroid/content/BroadcastReceiver;
.source "BatterySaverController.java"


# instance fields
.field private Ok:Z

.field final synthetic Ol:Lcom/android/settings/fuelgauge/BatterySaverController;


# virtual methods
.method public Ik(Z)V
    .locals 2

    if-eqz p1, :cond_1

    iget-boolean v0, p0, Lcom/android/settings/fuelgauge/BatterySaverController$BatteryStateChangeReceiver;->Ok:Z

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_1

    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    const-string/jumbo v1, "android.intent.action.BATTERY_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string/jumbo v1, "android.os.action.POWER_SAVE_MODE_CHANGING"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/android/settings/fuelgauge/BatterySaverController$BatteryStateChangeReceiver;->Ol:Lcom/android/settings/fuelgauge/BatterySaverController;

    invoke-static {v1}, Lcom/android/settings/fuelgauge/BatterySaverController;->Ih(Lcom/android/settings/fuelgauge/BatterySaverController;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, p0, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/settings/fuelgauge/BatterySaverController$BatteryStateChangeReceiver;->Ok:Z

    :cond_0
    :goto_0
    return-void

    :cond_1
    if-nez p1, :cond_0

    iget-boolean v0, p0, Lcom/android/settings/fuelgauge/BatterySaverController$BatteryStateChangeReceiver;->Ok:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/fuelgauge/BatterySaverController$BatteryStateChangeReceiver;->Ol:Lcom/android/settings/fuelgauge/BatterySaverController;

    invoke-static {v0}, Lcom/android/settings/fuelgauge/BatterySaverController;->Ih(Lcom/android/settings/fuelgauge/BatterySaverController;)Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/settings/fuelgauge/BatterySaverController$BatteryStateChangeReceiver;->Ok:Z

    goto :goto_0
.end method

.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 3

    const/4 v0, 0x0

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "android.os.action.POWER_SAVE_MODE_CHANGING"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v0, p0, Lcom/android/settings/fuelgauge/BatterySaverController$BatteryStateChangeReceiver;->Ol:Lcom/android/settings/fuelgauge/BatterySaverController;

    invoke-static {v0}, Lcom/android/settings/fuelgauge/BatterySaverController;->Ig(Lcom/android/settings/fuelgauge/BatterySaverController;)Lcom/android/settings/widget/MasterSwitchPreference;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/fuelgauge/BatterySaverController$BatteryStateChangeReceiver;->Ol:Lcom/android/settings/fuelgauge/BatterySaverController;

    invoke-static {v1}, Lcom/android/settings/fuelgauge/BatterySaverController;->Ii(Lcom/android/settings/fuelgauge/BatterySaverController;)Landroid/os/PowerManager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/PowerManager;->isPowerSaveMode()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/android/settings/widget/MasterSwitchPreference;->setChecked(Z)V

    iget-object v0, p0, Lcom/android/settings/fuelgauge/BatterySaverController$BatteryStateChangeReceiver;->Ol:Lcom/android/settings/fuelgauge/BatterySaverController;

    invoke-static {v0}, Lcom/android/settings/fuelgauge/BatterySaverController;->Ij(Lcom/android/settings/fuelgauge/BatterySaverController;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string/jumbo v2, "android.intent.action.BATTERY_CHANGED"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string/jumbo v1, "status"

    const/4 v2, -0x1

    invoke-virtual {p2, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    const/4 v2, 0x2

    if-eq v1, v2, :cond_2

    const/4 v2, 0x5

    if-eq v1, v2, :cond_2

    const/4 v0, 0x1

    :cond_2
    iget-object v1, p0, Lcom/android/settings/fuelgauge/BatterySaverController$BatteryStateChangeReceiver;->Ol:Lcom/android/settings/fuelgauge/BatterySaverController;

    invoke-static {v1}, Lcom/android/settings/fuelgauge/BatterySaverController;->Ig(Lcom/android/settings/fuelgauge/BatterySaverController;)Lcom/android/settings/widget/MasterSwitchPreference;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/android/settings/widget/MasterSwitchPreference;->aAx(Z)V

    goto :goto_0
.end method
