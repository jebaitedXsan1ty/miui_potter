.class public Lcom/android/settings/DateTimeSettings;
.super Lcom/android/settings/dashboard/MiuiDashboardFragment;
.source "DateTimeSettings.java"

# interfaces
.implements Lcom/android/settings/datetime/f;
.implements Lcom/android/settings/datetime/m;


# static fields
.field public static final SEARCH_INDEX_DATA_PROVIDER:Lcom/android/settings/search/Indexable$SearchIndexProvider;

.field public static final SUMMARY_PROVIDER_FACTORY:Lcom/android/settings/dashboard/F;


# instance fields
.field private byX:Landroid/content/BroadcastReceiver;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/android/settings/eB;

    invoke-direct {v0}, Lcom/android/settings/eB;-><init>()V

    sput-object v0, Lcom/android/settings/DateTimeSettings;->SUMMARY_PROVIDER_FACTORY:Lcom/android/settings/dashboard/F;

    new-instance v0, Lcom/android/settings/Z;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/android/settings/Z;-><init>(Lcom/android/settings/Z;)V

    sput-object v0, Lcom/android/settings/DateTimeSettings;->SEARCH_INDEX_DATA_PROVIDER:Lcom/android/settings/search/Indexable$SearchIndexProvider;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/android/settings/dashboard/MiuiDashboardFragment;-><init>()V

    new-instance v0, Lcom/android/settings/eA;

    invoke-direct {v0, p0}, Lcom/android/settings/eA;-><init>(Lcom/android/settings/DateTimeSettings;)V

    iput-object v0, p0, Lcom/android/settings/DateTimeSettings;->byX:Landroid/content/BroadcastReceiver;

    return-void
.end method


# virtual methods
.method public Lx(I)I
    .locals 1

    packed-switch p1, :pswitch_data_0

    const/4 v0, 0x0

    return v0

    :pswitch_0
    const/16 v0, 0x25f

    return v0

    :pswitch_1
    const/16 v0, 0x260

    return v0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public apJ()V
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/android/settings/DateTimeSettings;->alW(I)V

    return-void
.end method

.method public apy()V
    .locals 1

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/android/settings/DateTimeSettings;->bWO(I)V

    invoke-virtual {p0, v0}, Lcom/android/settings/DateTimeSettings;->alW(I)V

    return-void
.end method

.method public apz(Landroid/content/Context;)V
    .locals 0

    invoke-virtual {p0}, Lcom/android/settings/DateTimeSettings;->Cc()V

    return-void
.end method

.method protected ar()Ljava/lang/String;
    .locals 1

    const-string/jumbo v0, "DateTimeSettings"

    return-object v0
.end method

.method protected as()I
    .locals 1

    const v0, 0x7f15003a

    return v0
.end method

.method public getMetricsCategory()I
    .locals 1

    const/16 v0, 0x26

    return v0
.end method

.method protected getPreferenceControllers(Landroid/content/Context;)Ljava/util/List;
    .locals 6

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {p0}, Lcom/android/settings/DateTimeSettings;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string/jumbo v3, "firstRun"

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    new-instance v3, Lcom/android/settings/datetime/d;

    invoke-direct {v3, v1, p0, v2}, Lcom/android/settings/datetime/d;-><init>(Landroid/content/Context;Lcom/android/settings/datetime/g;Z)V

    new-instance v4, Lcom/android/settings/datetime/p;

    invoke-direct {v4, v1, p0}, Lcom/android/settings/datetime/p;-><init>(Landroid/content/Context;Lcom/android/settings/datetime/g;)V

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v5, Lcom/android/settings/datetime/q;

    invoke-direct {v5, v1, p0, v2}, Lcom/android/settings/datetime/q;-><init>(Landroid/content/Context;Lcom/android/settings/datetime/g;Z)V

    invoke-interface {v0, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v2, Lcom/android/settings/datetime/j;

    invoke-direct {v2, v1, v3}, Lcom/android/settings/datetime/j;-><init>(Landroid/content/Context;Lcom/android/settings/datetime/d;)V

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v2, Lcom/android/settings/datetime/e;

    invoke-direct {v2, v1, p0, v4}, Lcom/android/settings/datetime/e;-><init>(Landroid/content/Context;Lcom/android/settings/datetime/f;Lcom/android/settings/datetime/p;)V

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v2, Lcom/android/settings/datetime/l;

    invoke-direct {v2, v1, p0, v4}, Lcom/android/settings/datetime/l;-><init>(Landroid/content/Context;Lcom/android/settings/datetime/m;Lcom/android/settings/datetime/p;)V

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object v0
.end method

.method public onAttach(Landroid/content/Context;)V
    .locals 2

    invoke-super {p0, p1}, Lcom/android/settings/dashboard/MiuiDashboardFragment;->onAttach(Landroid/content/Context;)V

    invoke-virtual {p0}, Lcom/android/settings/DateTimeSettings;->getLifecycle()Lcom/android/settings/core/lifecycle/c;

    move-result-object v0

    new-instance v1, Lcom/android/settings/datetime/k;

    invoke-direct {v1, p1, p0}, Lcom/android/settings/datetime/k;-><init>(Landroid/content/Context;Lcom/android/settings/datetime/g;)V

    invoke-virtual {v0, v1}, Lcom/android/settings/core/lifecycle/c;->ajv(Lcom/android/settings/core/lifecycle/b;)Lcom/android/settings/core/lifecycle/b;

    return-void
.end method

.method public onCreateDialog(I)Landroid/app/Dialog;
    .locals 2

    packed-switch p1, :pswitch_data_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    :pswitch_0
    const-class v0, Lcom/android/settings/datetime/l;

    invoke-virtual {p0, v0}, Lcom/android/settings/DateTimeSettings;->BY(Ljava/lang/Class;)Lcom/android/settings/core/e;

    move-result-object v0

    check-cast v0, Lcom/android/settings/datetime/l;

    invoke-virtual {p0}, Lcom/android/settings/DateTimeSettings;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/settings/datetime/l;->apH(Landroid/app/Activity;)Lmiui/app/DatePickerDialog;

    move-result-object v0

    return-object v0

    :pswitch_1
    const-class v0, Lcom/android/settings/datetime/e;

    invoke-virtual {p0, v0}, Lcom/android/settings/DateTimeSettings;->BY(Ljava/lang/Class;)Lcom/android/settings/core/e;

    move-result-object v0

    check-cast v0, Lcom/android/settings/datetime/e;

    invoke-virtual {p0}, Lcom/android/settings/DateTimeSettings;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/settings/datetime/e;->apw(Landroid/app/Activity;)Lmiui/app/TimePickerDialog;

    move-result-object v0

    return-object v0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onPause()V
    .locals 2

    invoke-super {p0}, Lcom/android/settings/dashboard/MiuiDashboardFragment;->onPause()V

    invoke-virtual {p0}, Lcom/android/settings/DateTimeSettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/DateTimeSettings;->byX:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/app/Activity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    return-void
.end method

.method public onResume()V
    .locals 4

    const/4 v3, 0x0

    invoke-super {p0}, Lcom/android/settings/dashboard/MiuiDashboardFragment;->onResume()V

    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    const-string/jumbo v1, "android.intent.action.TIME_TICK"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string/jumbo v1, "android.intent.action.TIME_SET"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string/jumbo v1, "android.intent.action.TIMEZONE_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/android/settings/DateTimeSettings;->getActivity()Landroid/app/Activity;

    move-result-object v1

    iget-object v2, p0, Lcom/android/settings/DateTimeSettings;->byX:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0, v3, v3}, Landroid/app/Activity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;Ljava/lang/String;Landroid/os/Handler;)Landroid/content/Intent;

    return-void
.end method
