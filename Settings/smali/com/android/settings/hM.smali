.class final Lcom/android/settings/hM;
.super Landroid/os/AsyncTask;
.source "MiuiSecuritySettings.java"


# instance fields
.field final synthetic cnz:Lcom/android/settings/MiuiSecuritySettings;


# direct methods
.method constructor <init>(Lcom/android/settings/MiuiSecuritySettings;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/hM;->cnz:Lcom/android/settings/MiuiSecuritySettings;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected cak(Landroid/content/Intent;)V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/hM;->cnz:Lcom/android/settings/MiuiSecuritySettings;

    invoke-virtual {v0}, Lcom/android/settings/MiuiSecuritySettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    if-eqz v0, :cond_0

    if-eqz p1, :cond_1

    iget-object v0, p0, Lcom/android/settings/hM;->cnz:Lcom/android/settings/MiuiSecuritySettings;

    invoke-static {v0}, Lcom/android/settings/MiuiSecuritySettings;->bDP(Lcom/android/settings/MiuiSecuritySettings;)Lmiui/preference/ValuePreference;

    move-result-object v0

    invoke-virtual {v0, p1}, Lmiui/preference/ValuePreference;->setIntent(Landroid/content/Intent;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/settings/hM;->cnz:Lcom/android/settings/MiuiSecuritySettings;

    invoke-static {v0}, Lcom/android/settings/MiuiSecuritySettings;->bDQ(Lcom/android/settings/MiuiSecuritySettings;)Landroid/preference/PreferenceCategory;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/hM;->cnz:Lcom/android/settings/MiuiSecuritySettings;

    invoke-static {v1}, Lcom/android/settings/MiuiSecuritySettings;->bDP(Lcom/android/settings/MiuiSecuritySettings;)Lmiui/preference/ValuePreference;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceCategory;->removePreference(Landroid/preference/Preference;)Z

    goto :goto_0
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Landroid/content/Intent;
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/android/settings/hM;->cnz:Lcom/android/settings/MiuiSecuritySettings;

    invoke-virtual {v0}, Lcom/android/settings/MiuiSecuritySettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    if-nez v0, :cond_0

    return-object v1

    :cond_0
    iget-object v0, p0, Lcom/android/settings/hM;->cnz:Lcom/android/settings/MiuiSecuritySettings;

    invoke-virtual {v0}, Lcom/android/settings/MiuiSecuritySettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/android/settings/MiuiSecuritySettings;->bDv(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/android/settings/hM;->doInBackground([Ljava/lang/Void;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    check-cast p1, Landroid/content/Intent;

    invoke-virtual {p0, p1}, Lcom/android/settings/hM;->cak(Landroid/content/Intent;)V

    return-void
.end method
