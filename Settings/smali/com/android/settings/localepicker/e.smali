.class Lcom/android/settings/localepicker/e;
.super Landroid/support/v7/widget/p;
.source "LocaleDragAndDropAdapter.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# instance fields
.field private final ND:Lcom/android/settings/localepicker/LocaleDragCell;

.field final synthetic NE:Lcom/android/settings/localepicker/d;


# direct methods
.method public constructor <init>(Lcom/android/settings/localepicker/d;Lcom/android/settings/localepicker/LocaleDragCell;)V
    .locals 1

    iput-object p1, p0, Lcom/android/settings/localepicker/e;->NE:Lcom/android/settings/localepicker/d;

    invoke-direct {p0, p2}, Landroid/support/v7/widget/p;-><init>(Landroid/view/View;)V

    iput-object p2, p0, Lcom/android/settings/localepicker/e;->ND:Lcom/android/settings/localepicker/LocaleDragCell;

    iget-object v0, p0, Lcom/android/settings/localepicker/e;->ND:Lcom/android/settings/localepicker/LocaleDragCell;

    invoke-virtual {v0}, Lcom/android/settings/localepicker/LocaleDragCell;->HO()Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/widget/ImageView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    return-void
.end method


# virtual methods
.method public HM()Lcom/android/settings/localepicker/LocaleDragCell;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/localepicker/e;->ND:Lcom/android/settings/localepicker/LocaleDragCell;

    return-object v0
.end method

.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 1

    iget-object v0, p0, Lcom/android/settings/localepicker/e;->NE:Lcom/android/settings/localepicker/d;

    invoke-static {v0}, Lcom/android/settings/localepicker/d;->HF(Lcom/android/settings/localepicker/d;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p2}, Landroid/support/v4/view/S;->dQV(Landroid/view/MotionEvent;)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :cond_0
    :goto_0
    const/4 v0, 0x0

    return v0

    :pswitch_0
    iget-object v0, p0, Lcom/android/settings/localepicker/e;->NE:Lcom/android/settings/localepicker/d;

    invoke-static {v0}, Lcom/android/settings/localepicker/d;->HG(Lcom/android/settings/localepicker/d;)Landroid/support/v7/widget/a/b;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/support/v7/widget/a/b;->dnI(Landroid/support/v7/widget/p;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method
