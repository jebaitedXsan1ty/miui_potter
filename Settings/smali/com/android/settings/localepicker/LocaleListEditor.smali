.class public Lcom/android/settings/localepicker/LocaleListEditor;
.super Lcom/android/settings/SettingsPreferenceFragment;
.source "LocaleListEditor.java"

# interfaces
.implements Lcom/android/internal/app/LocalePickerWithRegion$LocaleSelectedListener;


# instance fields
.field private Nq:Lcom/android/settings/localepicker/d;

.field private Nr:Landroid/view/View;

.field private Ns:Landroid/view/Menu;

.field private Nt:Z

.field private Nu:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/SettingsPreferenceFragment;-><init>()V

    return-void
.end method

.method private Hj(Landroid/view/View;)V
    .locals 5

    const/4 v4, 0x1

    const v0, 0x7f0a0154

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView;

    new-instance v1, Lcom/android/settings/localepicker/c;

    invoke-virtual {p0}, Lcom/android/settings/localepicker/LocaleListEditor;->getContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lcom/android/settings/localepicker/LocaleListEditor;->Nq:Lcom/android/settings/localepicker/d;

    invoke-direct {v1, v2, v3}, Lcom/android/settings/localepicker/c;-><init>(Landroid/content/Context;Lcom/android/settings/localepicker/d;)V

    invoke-virtual {v1, v4}, Lcom/android/settings/localepicker/c;->drH(Z)V

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(Landroid/support/v7/widget/a;)V

    invoke-virtual {v0, v4}, Landroid/support/v7/widget/RecyclerView;->setHasFixedSize(Z)V

    iget-object v1, p0, Lcom/android/settings/localepicker/LocaleListEditor;->Nq:Lcom/android/settings/localepicker/d;

    invoke-virtual {v1, v0}, Lcom/android/settings/localepicker/d;->Hz(Landroid/support/v7/widget/RecyclerView;)V

    iget-object v1, p0, Lcom/android/settings/localepicker/LocaleListEditor;->Nq:Lcom/android/settings/localepicker/d;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setAdapter(Landroid/support/v7/widget/b;)V

    const v0, 0x7f0a0036

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/localepicker/LocaleListEditor;->Nr:Landroid/view/View;

    iget-object v0, p0, Lcom/android/settings/localepicker/LocaleListEditor;->Nr:Landroid/view/View;

    new-instance v1, Lcom/android/settings/localepicker/k;

    invoke-direct {v1, p0}, Lcom/android/settings/localepicker/k;-><init>(Lcom/android/settings/localepicker/LocaleListEditor;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method private static Hk(Landroid/content/Context;)Ljava/util/List;
    .locals 4

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-static {}, Lcom/android/internal/app/LocalePicker;->getLocales()Landroid/os/LocaleList;

    move-result-object v2

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v2}, Landroid/os/LocaleList;->size()I

    move-result v3

    if-ge v0, v3, :cond_0

    invoke-virtual {v2, v0}, Landroid/os/LocaleList;->get(I)Ljava/util/Locale;

    move-result-object v3

    invoke-static {v3}, Lcom/android/internal/app/LocaleStore;->getLocaleInfo(Ljava/util/Locale;)Lcom/android/internal/app/LocaleStore$LocaleInfo;

    move-result-object v3

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-object v1
.end method

.method private Hl(Z)V
    .locals 2

    iput-boolean p1, p0, Lcom/android/settings/localepicker/LocaleListEditor;->Nt:Z

    iget-object v0, p0, Lcom/android/settings/localepicker/LocaleListEditor;->Nq:Lcom/android/settings/localepicker/d;

    invoke-virtual {v0, p1}, Lcom/android/settings/localepicker/d;->Hw(Z)V

    iget-object v1, p0, Lcom/android/settings/localepicker/LocaleListEditor;->Nr:Landroid/view/View;

    if-eqz p1, :cond_0

    const/4 v0, 0x4

    :goto_0
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    invoke-direct {p0}, Lcom/android/settings/localepicker/LocaleListEditor;->Hn()V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private Hm()V
    .locals 5

    const v4, 0x1040013

    const/4 v3, 0x1

    iget-object v0, p0, Lcom/android/settings/localepicker/LocaleListEditor;->Nq:Lcom/android/settings/localepicker/d;

    invoke-virtual {v0}, Lcom/android/settings/localepicker/d;->Hx()I

    move-result v0

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/android/settings/localepicker/LocaleListEditor;->Nt:Z

    xor-int/lit8 v0, v0, 0x1

    invoke-direct {p0, v0}, Lcom/android/settings/localepicker/LocaleListEditor;->Hl(Z)V

    return-void

    :cond_0
    iget-object v1, p0, Lcom/android/settings/localepicker/LocaleListEditor;->Nq:Lcom/android/settings/localepicker/d;

    invoke-virtual {v1}, Lcom/android/settings/localepicker/d;->getItemCount()I

    move-result v1

    if-ne v0, v1, :cond_1

    iput-boolean v3, p0, Lcom/android/settings/localepicker/LocaleListEditor;->Nu:Z

    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/android/settings/localepicker/LocaleListEditor;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v1, 0x7f1205f9

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f1205f8

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    new-instance v1, Lcom/android/settings/localepicker/f;

    invoke-direct {v1, p0}, Lcom/android/settings/localepicker/f;-><init>(Lcom/android/settings/localepicker/LocaleListEditor;)V

    invoke-virtual {v0, v4, v1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    new-instance v1, Lcom/android/settings/localepicker/g;

    invoke-direct {v1, p0}, Lcom/android/settings/localepicker/g;-><init>(Lcom/android/settings/localepicker/LocaleListEditor;)V

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    return-void

    :cond_1
    invoke-virtual {p0}, Lcom/android/settings/localepicker/LocaleListEditor;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f100011

    invoke-virtual {v1, v2, v0}, Landroid/content/res/Resources;->getQuantityString(II)Ljava/lang/String;

    move-result-object v0

    iput-boolean v3, p0, Lcom/android/settings/localepicker/LocaleListEditor;->Nu:Z

    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/android/settings/localepicker/LocaleListEditor;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, v0}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f1205fa

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    new-instance v1, Lcom/android/settings/localepicker/h;

    invoke-direct {v1, p0}, Lcom/android/settings/localepicker/h;-><init>(Lcom/android/settings/localepicker/LocaleListEditor;)V

    const v2, 0x1040009

    invoke-virtual {v0, v2, v1}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    new-instance v1, Lcom/android/settings/localepicker/i;

    invoke-direct {v1, p0}, Lcom/android/settings/localepicker/i;-><init>(Lcom/android/settings/localepicker/LocaleListEditor;)V

    invoke-virtual {v0, v4, v1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    new-instance v1, Lcom/android/settings/localepicker/j;

    invoke-direct {v1, p0}, Lcom/android/settings/localepicker/j;-><init>(Lcom/android/settings/localepicker/LocaleListEditor;)V

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    return-void
.end method

.method private Hn()V
    .locals 5

    const/4 v0, 0x2

    const/4 v2, 0x1

    const/4 v1, 0x0

    iget-object v3, p0, Lcom/android/settings/localepicker/LocaleListEditor;->Ns:Landroid/view/Menu;

    if-nez v3, :cond_0

    return-void

    :cond_0
    iget-object v3, p0, Lcom/android/settings/localepicker/LocaleListEditor;->Ns:Landroid/view/Menu;

    invoke-interface {v3, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v3

    if-eqz v3, :cond_2

    iget-boolean v4, p0, Lcom/android/settings/localepicker/LocaleListEditor;->Nt:Z

    if-eqz v4, :cond_3

    :goto_0
    invoke-interface {v3, v0}, Landroid/view/MenuItem;->setShowAsAction(I)V

    iget-object v0, p0, Lcom/android/settings/localepicker/LocaleListEditor;->Nq:Lcom/android/settings/localepicker/d;

    invoke-virtual {v0}, Lcom/android/settings/localepicker/d;->getItemCount()I

    move-result v0

    if-le v0, v2, :cond_1

    move v1, v2

    :cond_1
    invoke-interface {v3, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    :cond_2
    return-void

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method static synthetic Ho(Lcom/android/settings/localepicker/LocaleListEditor;)Lcom/android/settings/localepicker/d;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/localepicker/LocaleListEditor;->Nq:Lcom/android/settings/localepicker/d;

    return-object v0
.end method

.method static synthetic Hp(Lcom/android/settings/localepicker/LocaleListEditor;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/android/settings/localepicker/LocaleListEditor;->Nt:Z

    return p1
.end method

.method static synthetic Hq(Lcom/android/settings/localepicker/LocaleListEditor;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/android/settings/localepicker/LocaleListEditor;->Nu:Z

    return p1
.end method

.method static synthetic Hr(Lcom/android/settings/localepicker/LocaleListEditor;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/settings/localepicker/LocaleListEditor;->Hl(Z)V

    return-void
.end method


# virtual methods
.method public getMetricsCategory()I
    .locals 1

    const/16 v0, 0x158

    return v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3

    invoke-super {p0, p1}, Lcom/android/settings/SettingsPreferenceFragment;->onCreate(Landroid/os/Bundle;)V

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/android/settings/localepicker/LocaleListEditor;->setHasOptionsMenu(Z)V

    invoke-virtual {p0}, Lcom/android/settings/localepicker/LocaleListEditor;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/android/internal/app/LocaleStore;->fillCache(Landroid/content/Context;)V

    invoke-virtual {p0}, Lcom/android/settings/localepicker/LocaleListEditor;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/android/settings/localepicker/LocaleListEditor;->Hk(Landroid/content/Context;)Ljava/util/List;

    move-result-object v0

    new-instance v1, Lcom/android/settings/localepicker/d;

    invoke-virtual {p0}, Lcom/android/settings/localepicker/LocaleListEditor;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Lcom/android/settings/localepicker/d;-><init>(Landroid/content/Context;Ljava/util/List;)V

    iput-object v1, p0, Lcom/android/settings/localepicker/LocaleListEditor;->Nq:Lcom/android/settings/localepicker/d;

    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V
    .locals 3

    const/4 v2, 0x0

    const/4 v0, 0x2

    const v1, 0x7f120924

    invoke-interface {p1, v2, v0, v2, v1}, Landroid/view/Menu;->add(IIII)Landroid/view/MenuItem;

    move-result-object v0

    const/4 v1, 0x4

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setShowAsAction(I)V

    const v1, 0x7f0801bd

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    invoke-super {p0, p1, p2}, Lcom/android/settings/SettingsPreferenceFragment;->onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V

    iput-object p1, p0, Lcom/android/settings/localepicker/LocaleListEditor;->Ns:Landroid/view/Menu;

    invoke-direct {p0}, Lcom/android/settings/localepicker/LocaleListEditor;->Hn()V

    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 4

    invoke-super {p0, p1, p2, p3}, Lcom/android/settings/SettingsPreferenceFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v1

    move-object v0, v1

    check-cast v0, Landroid/view/ViewGroup;

    const v2, 0x7f0d00df

    invoke-virtual {p1, v2, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0}, Lcom/android/settings/localepicker/LocaleListEditor;->getActivity()Landroid/app/Activity;

    move-result-object v2

    const v3, 0x7f120ce3

    invoke-virtual {v2, v3}, Landroid/app/Activity;->setTitle(I)V

    invoke-direct {p0, v0}, Lcom/android/settings/localepicker/LocaleListEditor;->Hj(Landroid/view/View;)V

    return-object v1
.end method

.method public onLocaleSelected(Lcom/android/internal/app/LocaleStore$LocaleInfo;)V
    .locals 1

    iget-object v0, p0, Lcom/android/settings/localepicker/LocaleListEditor;->Nq:Lcom/android/settings/localepicker/d;

    invoke-virtual {v0, p1}, Lcom/android/settings/localepicker/d;->HA(Lcom/android/internal/app/LocaleStore$LocaleInfo;)V

    invoke-direct {p0}, Lcom/android/settings/localepicker/LocaleListEditor;->Hn()V

    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2

    const/4 v1, 0x1

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    :cond_0
    invoke-super {p0, p1}, Lcom/android/settings/SettingsPreferenceFragment;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    return v0

    :sswitch_0
    iget-boolean v0, p0, Lcom/android/settings/localepicker/LocaleListEditor;->Nt:Z

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lcom/android/settings/localepicker/LocaleListEditor;->Hm()V

    :goto_0
    return v1

    :cond_1
    invoke-direct {p0, v1}, Lcom/android/settings/localepicker/LocaleListEditor;->Hl(Z)V

    goto :goto_0

    :sswitch_1
    iget-boolean v0, p0, Lcom/android/settings/localepicker/LocaleListEditor;->Nt:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/android/settings/localepicker/LocaleListEditor;->Hl(Z)V

    return v1

    :sswitch_data_0
    .sparse-switch
        0x2 -> :sswitch_0
        0x102002c -> :sswitch_1
    .end sparse-switch
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Lcom/android/settings/SettingsPreferenceFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    const-string/jumbo v0, "localeRemoveMode"

    iget-boolean v1, p0, Lcom/android/settings/localepicker/LocaleListEditor;->Nt:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string/jumbo v0, "showingLocaleRemoveDialog"

    iget-boolean v1, p0, Lcom/android/settings/localepicker/LocaleListEditor;->Nu:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    iget-object v0, p0, Lcom/android/settings/localepicker/LocaleListEditor;->Nq:Lcom/android/settings/localepicker/d;

    invoke-virtual {v0, p1}, Lcom/android/settings/localepicker/d;->saveState(Landroid/os/Bundle;)V

    return-void
.end method

.method public onViewStateRestored(Landroid/os/Bundle;)V
    .locals 2

    const/4 v1, 0x0

    invoke-super {p0, p1}, Lcom/android/settings/SettingsPreferenceFragment;->onViewStateRestored(Landroid/os/Bundle;)V

    if-eqz p1, :cond_0

    const-string/jumbo v0, "localeRemoveMode"

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/settings/localepicker/LocaleListEditor;->Nt:Z

    const-string/jumbo v0, "showingLocaleRemoveDialog"

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/settings/localepicker/LocaleListEditor;->Nu:Z

    :cond_0
    iget-boolean v0, p0, Lcom/android/settings/localepicker/LocaleListEditor;->Nt:Z

    invoke-direct {p0, v0}, Lcom/android/settings/localepicker/LocaleListEditor;->Hl(Z)V

    iget-object v0, p0, Lcom/android/settings/localepicker/LocaleListEditor;->Nq:Lcom/android/settings/localepicker/d;

    invoke-virtual {v0, p1}, Lcom/android/settings/localepicker/d;->Hv(Landroid/os/Bundle;)V

    iget-boolean v0, p0, Lcom/android/settings/localepicker/LocaleListEditor;->Nu:Z

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lcom/android/settings/localepicker/LocaleListEditor;->Hm()V

    :cond_1
    return-void
.end method
