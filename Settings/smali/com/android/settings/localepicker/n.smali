.class final Lcom/android/settings/localepicker/n;
.super Ljava/lang/Object;
.source "LocaleDragAndDropAdapter.java"

# interfaces
.implements Landroid/support/v7/widget/V;


# instance fields
.field final synthetic NV:Lcom/android/settings/localepicker/d;


# direct methods
.method constructor <init>(Lcom/android/settings/localepicker/d;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/localepicker/n;->NV:Lcom/android/settings/localepicker/d;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public HX()V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/android/settings/localepicker/n;->NV:Lcom/android/settings/localepicker/d;

    invoke-static {v0}, Lcom/android/settings/localepicker/d;->HI(Lcom/android/settings/localepicker/d;)Landroid/os/LocaleList;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/localepicker/n;->NV:Lcom/android/settings/localepicker/d;

    invoke-static {v0}, Lcom/android/settings/localepicker/d;->HI(Lcom/android/settings/localepicker/d;)Landroid/os/LocaleList;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/localepicker/n;->NV:Lcom/android/settings/localepicker/d;

    invoke-static {v1}, Lcom/android/settings/localepicker/d;->HH(Lcom/android/settings/localepicker/d;)Landroid/os/LocaleList;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/LocaleList;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/settings/localepicker/n;->NV:Lcom/android/settings/localepicker/d;

    invoke-static {v0}, Lcom/android/settings/localepicker/d;->HI(Lcom/android/settings/localepicker/d;)Landroid/os/LocaleList;

    move-result-object v0

    invoke-static {v0}, Lcom/android/internal/app/LocalePicker;->updateLocales(Landroid/os/LocaleList;)V

    iget-object v0, p0, Lcom/android/settings/localepicker/n;->NV:Lcom/android/settings/localepicker/d;

    iget-object v1, p0, Lcom/android/settings/localepicker/n;->NV:Lcom/android/settings/localepicker/d;

    invoke-static {v1}, Lcom/android/settings/localepicker/d;->HI(Lcom/android/settings/localepicker/d;)Landroid/os/LocaleList;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/settings/localepicker/d;->HJ(Lcom/android/settings/localepicker/d;Landroid/os/LocaleList;)Landroid/os/LocaleList;

    new-instance v0, Lcom/android/settings/ax;

    iget-object v1, p0, Lcom/android/settings/localepicker/n;->NV:Lcom/android/settings/localepicker/d;

    invoke-static {v1}, Lcom/android/settings/localepicker/d;->HE(Lcom/android/settings/localepicker/d;)Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/android/settings/ax;-><init>(Landroid/content/Context;)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/android/settings/ax;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    iget-object v0, p0, Lcom/android/settings/localepicker/n;->NV:Lcom/android/settings/localepicker/d;

    invoke-static {v0, v2}, Lcom/android/settings/localepicker/d;->HK(Lcom/android/settings/localepicker/d;Landroid/os/LocaleList;)Landroid/os/LocaleList;

    iget-object v0, p0, Lcom/android/settings/localepicker/n;->NV:Lcom/android/settings/localepicker/d;

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    invoke-static {v1}, Ljava/text/NumberFormat;->getNumberInstance(Ljava/util/Locale;)Ljava/text/NumberFormat;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/settings/localepicker/d;->HL(Lcom/android/settings/localepicker/d;Ljava/text/NumberFormat;)Ljava/text/NumberFormat;

    return-void
.end method
