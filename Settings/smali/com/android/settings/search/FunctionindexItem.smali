.class public Lcom/android/settings/search/FunctionindexItem;
.super Ljava/lang/Object;
.source "FunctionindexItem.java"


# static fields
.field public static CLASS:Ljava/lang/String;

.field public static FRAGMENT:Ljava/lang/String;

.field public static GROUP_ID_END_POSITION:I

.field public static GROUP_ID_START_POSITION:I

.field public static ID:Ljava/lang/String;

.field public static INTENT_ACTION:Ljava/lang/String;

.field public static IS_CHECKBOX:Ljava/lang/String;

.field public static IS_SEARCHABLE:Ljava/lang/String;

.field public static ITEM:Ljava/lang/String;

.field public static KEYWORDS:Ljava/lang/String;

.field public static PACKAGE:Ljava/lang/String;

.field public static PATH:Ljava/lang/String;

.field public static RESOURCE_NAME:Ljava/lang/String;

.field public static SECOND_SPACE:Ljava/lang/String;

.field public static SUMMARY:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string/jumbo v0, "item"

    sput-object v0, Lcom/android/settings/search/FunctionindexItem;->ITEM:Ljava/lang/String;

    const-string/jumbo v0, "id"

    sput-object v0, Lcom/android/settings/search/FunctionindexItem;->ID:Ljava/lang/String;

    const-string/jumbo v0, "resource_name"

    sput-object v0, Lcom/android/settings/search/FunctionindexItem;->RESOURCE_NAME:Ljava/lang/String;

    const-string/jumbo v0, "keywords"

    sput-object v0, Lcom/android/settings/search/FunctionindexItem;->KEYWORDS:Ljava/lang/String;

    const-string/jumbo v0, "path"

    sput-object v0, Lcom/android/settings/search/FunctionindexItem;->PATH:Ljava/lang/String;

    const-string/jumbo v0, "summary"

    sput-object v0, Lcom/android/settings/search/FunctionindexItem;->SUMMARY:Ljava/lang/String;

    const-string/jumbo v0, "class"

    sput-object v0, Lcom/android/settings/search/FunctionindexItem;->CLASS:Ljava/lang/String;

    const-string/jumbo v0, "package"

    sput-object v0, Lcom/android/settings/search/FunctionindexItem;->PACKAGE:Ljava/lang/String;

    const-string/jumbo v0, "intent_action"

    sput-object v0, Lcom/android/settings/search/FunctionindexItem;->INTENT_ACTION:Ljava/lang/String;

    const-string/jumbo v0, "fragment"

    sput-object v0, Lcom/android/settings/search/FunctionindexItem;->FRAGMENT:Ljava/lang/String;

    const-string/jumbo v0, "second_space"

    sput-object v0, Lcom/android/settings/search/FunctionindexItem;->SECOND_SPACE:Ljava/lang/String;

    const-string/jumbo v0, "is_searchable"

    sput-object v0, Lcom/android/settings/search/FunctionindexItem;->IS_SEARCHABLE:Ljava/lang/String;

    const-string/jumbo v0, "is_checkbox"

    sput-object v0, Lcom/android/settings/search/FunctionindexItem;->IS_CHECKBOX:Ljava/lang/String;

    const/4 v0, 0x0

    sput v0, Lcom/android/settings/search/FunctionindexItem;->GROUP_ID_START_POSITION:I

    const/4 v0, 0x2

    sput v0, Lcom/android/settings/search/FunctionindexItem;->GROUP_ID_END_POSITION:I

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
