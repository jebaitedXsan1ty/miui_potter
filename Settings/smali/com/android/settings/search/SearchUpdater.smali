.class public Lcom/android/settings/search/SearchUpdater;
.super Ljava/lang/Object;
.source "SearchUpdater.java"


# static fields
.field public static final ABOUT_DEVICE:I = 0x1

.field public static final ALL:I = -0x1

.field public static final BACKUP:I = 0x80

.field public static final DISPLAY:I = 0x4

.field public static final GOOGLE:I = 0x10000

.field public static final KEY:I = 0x20

.field public static final LOCK_SCREEN:I = 0x10

.field public static final OTHER:I = -0x80000000

.field public static final PHONE:I = 0x40

.field public static final SIM:I = 0x40000000

.field public static final SOUND:I = 0x8

.field private static final TAG:Ljava/lang/String; = "SearchUpdater"

.field public static final WIRELESS:I = 0x2

.field private static volatile sInstance:Lcom/android/settings/search/SearchUpdater;


# instance fields
.field private volatile mHandler:Lcom/android/settings/search/SearchUpdater$UpdateHandler;

.field private mOps:Ljava/util/ArrayList;

.field private final mTaskManager:Lcom/android/settings/search/SearchUpdater$TaskManager;


# direct methods
.method static synthetic -get0(Lcom/android/settings/search/SearchUpdater;)Ljava/util/ArrayList;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/search/SearchUpdater;->mOps:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic -get1(Lcom/android/settings/search/SearchUpdater;)Lcom/android/settings/search/SearchUpdater$TaskManager;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/search/SearchUpdater;->mTaskManager:Lcom/android/settings/search/SearchUpdater$TaskManager;

    return-object v0
.end method

.method static synthetic -wrap0(Lcom/android/settings/search/SearchUpdater;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/settings/search/SearchUpdater;->update(I)V

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/android/settings/search/SearchUpdater$TaskManager;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/android/settings/search/SearchUpdater$TaskManager;-><init>(Lcom/android/settings/search/SearchUpdater;Lcom/android/settings/search/SearchUpdater$TaskManager;)V

    iput-object v0, p0, Lcom/android/settings/search/SearchUpdater;->mTaskManager:Lcom/android/settings/search/SearchUpdater$TaskManager;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/settings/search/SearchUpdater;->mOps:Ljava/util/ArrayList;

    return-void
.end method

.method public static getInstance()Lcom/android/settings/search/SearchUpdater;
    .locals 2

    sget-object v0, Lcom/android/settings/search/SearchUpdater;->sInstance:Lcom/android/settings/search/SearchUpdater;

    if-nez v0, :cond_1

    const-class v1, Lcom/android/settings/search/SearchUpdater;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/android/settings/search/SearchUpdater;->sInstance:Lcom/android/settings/search/SearchUpdater;

    if-nez v0, :cond_0

    new-instance v0, Lcom/android/settings/search/SearchUpdater;

    invoke-direct {v0}, Lcom/android/settings/search/SearchUpdater;-><init>()V

    sput-object v0, Lcom/android/settings/search/SearchUpdater;->sInstance:Lcom/android/settings/search/SearchUpdater;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit v1

    :cond_1
    sget-object v0, Lcom/android/settings/search/SearchUpdater;->sInstance:Lcom/android/settings/search/SearchUpdater;

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private update(I)V
    .locals 3

    invoke-static {}, Landroid/app/AppGlobals;->getInitialApplication()Landroid/app/Application;

    move-result-object v0

    and-int/lit8 v1, p1, 0x1

    if-eqz v1, :cond_1

    :try_start_0
    iget-object v1, p0, Lcom/android/settings/search/SearchUpdater;->mOps:Ljava/util/ArrayList;

    invoke-static {v0, v1}, Lcom/android/settings/search/AboutDeviceUpdateHelper;->update(Landroid/content/Context;Ljava/util/ArrayList;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    and-int/lit8 v1, p1, 0x2

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/android/settings/search/SearchUpdater;->mOps:Ljava/util/ArrayList;

    invoke-static {v0, v1}, Lcom/android/settings/search/WirelessUpdateHelper;->update(Landroid/content/Context;Ljava/util/ArrayList;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string/jumbo v0, "SearchUpdater"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "error occurs when updating, current: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_2
    and-int/lit8 v1, p1, 0x4

    if-eqz v1, :cond_3

    :try_start_1
    iget-object v1, p0, Lcom/android/settings/search/SearchUpdater;->mOps:Ljava/util/ArrayList;

    invoke-static {v0, v1}, Lcom/android/settings/search/DisplayUpdateHelper;->update(Landroid/content/Context;Ljava/util/ArrayList;)V

    goto :goto_0

    :cond_3
    and-int/lit8 v1, p1, 0x8

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/android/settings/search/SearchUpdater;->mOps:Ljava/util/ArrayList;

    invoke-static {v0, v1}, Lcom/android/settings/search/SoundUpdateHelper;->update(Landroid/content/Context;Ljava/util/ArrayList;)V

    goto :goto_0

    :cond_4
    and-int/lit8 v1, p1, 0x10

    if-eqz v1, :cond_5

    iget-object v1, p0, Lcom/android/settings/search/SearchUpdater;->mOps:Ljava/util/ArrayList;

    invoke-static {v0, v1}, Lcom/android/settings/search/SecurityUpdateHelper;->update(Landroid/content/Context;Ljava/util/ArrayList;)V

    goto :goto_0

    :cond_5
    and-int/lit8 v1, p1, 0x20

    if-eqz v1, :cond_6

    iget-object v1, p0, Lcom/android/settings/search/SearchUpdater;->mOps:Ljava/util/ArrayList;

    invoke-static {v0, v1}, Lcom/android/settings/search/KeySettingsUpdateHelper;->update(Landroid/content/Context;Ljava/util/ArrayList;)V

    goto :goto_0

    :cond_6
    and-int/lit8 v1, p1, 0x40

    if-eqz v1, :cond_7

    iget-object v1, p0, Lcom/android/settings/search/SearchUpdater;->mOps:Ljava/util/ArrayList;

    invoke-static {v0, v1}, Lcom/android/settings/search/PhoneSettingsUpdateHelper;->update(Landroid/content/Context;Ljava/util/ArrayList;)V

    goto :goto_0

    :cond_7
    and-int/lit16 v1, p1, 0x80

    if-eqz v1, :cond_8

    iget-object v1, p0, Lcom/android/settings/search/SearchUpdater;->mOps:Ljava/util/ArrayList;

    invoke-static {v0, v1}, Lcom/android/settings/search/BackupSettingsUpdateHelper;->update(Landroid/content/Context;Ljava/util/ArrayList;)V

    goto :goto_0

    :cond_8
    const/high16 v1, 0x10000

    and-int/2addr v1, p1

    if-eqz v1, :cond_9

    iget-object v1, p0, Lcom/android/settings/search/SearchUpdater;->mOps:Ljava/util/ArrayList;

    invoke-static {v0, v1}, Lcom/android/settings/search/GoogleSettingsUpdateHelper;->update(Landroid/content/Context;Ljava/util/ArrayList;)V

    goto :goto_0

    :cond_9
    const/high16 v1, 0x40000000    # 2.0f

    and-int/2addr v1, p1

    if-eqz v1, :cond_a

    iget-object v1, p0, Lcom/android/settings/search/SearchUpdater;->mOps:Ljava/util/ArrayList;

    invoke-static {v0, v1}, Lcom/android/settings/search/SimSettingsUpdateHelper;->update(Landroid/content/Context;Ljava/util/ArrayList;)V

    goto :goto_0

    :cond_a
    const/high16 v1, -0x80000000

    and-int/2addr v1, p1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/settings/search/SearchUpdater;->mOps:Ljava/util/ArrayList;

    invoke-static {v0, v1}, Lcom/android/settings/search/OtherSettingsUpdateHelper;->update(Landroid/content/Context;Ljava/util/ArrayList;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0
.end method


# virtual methods
.method public handleUpdate(I)V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/search/SearchUpdater;->mHandler:Lcom/android/settings/search/SearchUpdater$UpdateHandler;

    if-nez v0, :cond_0

    new-instance v0, Landroid/os/HandlerThread;

    const-string/jumbo v1, "SearchUpdater"

    invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    new-instance v1, Lcom/android/settings/search/SearchUpdater$UpdateHandler;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-direct {v1, p0, v0}, Lcom/android/settings/search/SearchUpdater$UpdateHandler;-><init>(Lcom/android/settings/search/SearchUpdater;Landroid/os/Looper;)V

    iput-object v1, p0, Lcom/android/settings/search/SearchUpdater;->mHandler:Lcom/android/settings/search/SearchUpdater$UpdateHandler;

    :cond_0
    iget-object v0, p0, Lcom/android/settings/search/SearchUpdater;->mTaskManager:Lcom/android/settings/search/SearchUpdater$TaskManager;

    invoke-virtual {v0, p1}, Lcom/android/settings/search/SearchUpdater$TaskManager;->add(I)V

    iget-object v0, p0, Lcom/android/settings/search/SearchUpdater;->mHandler:Lcom/android/settings/search/SearchUpdater$UpdateHandler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/android/settings/search/SearchUpdater$UpdateHandler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    return-void
.end method
