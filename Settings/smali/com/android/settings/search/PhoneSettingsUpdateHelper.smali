.class public Lcom/android/settings/search/PhoneSettingsUpdateHelper;
.super Lcom/android/settings/search/BaseSearchUpdateHelper;
.source "PhoneSettingsUpdateHelper.java"


# static fields
.field private static final AUTOIP_RESOURCE:Ljava/lang/String; = "autoip"

.field private static final AUTO_RETRY_RESOURCE:Ljava/lang/String; = "auto_retry_mode_title"

.field private static final CALLER_ID_RESOURCE:Ljava/lang/String; = "caller_id_title"

.field private static final DIAL_PAD_TOUCH_RESOURCE:Ljava/lang/String; = "preference_dial_pad_touch_tone_title"

.field private static final DTMF_TONE_RESOURCE:Ljava/lang/String; = "dtmf_tones_title"

.field private static final DUAL_4G_RESOURCE:Ljava/lang/String; = "dual_4g_switch_title"

.field private static final PHONE_PACKAGE:Ljava/lang/String; = "com.android.phone"

.field private static final SIP_RESOURCE:Ljava/lang/String; = "sip_settings"

.field private static final T9_INDEX_RESOURCE:Ljava/lang/String; = "t9_indexing_method_title"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/search/BaseSearchUpdateHelper;-><init>()V

    return-void
.end method

.method private static getBoolean(Landroid/content/Context;Ljava/lang/String;)Z
    .locals 4

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const-string/jumbo v2, "bool"

    const-string/jumbo v3, "com.android.phone"

    invoke-virtual {v1, p1, v2, v3}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    return v0
.end method

.method static update(Landroid/content/Context;Ljava/util/ArrayList;)V
    .locals 9

    const/4 v1, 0x1

    const/4 v2, 0x0

    const-string/jumbo v0, "is_pad"

    invoke-static {v0, v2}, Lmiui/util/FeatureParser;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    :cond_0
    :try_start_0
    const-string/jumbo v0, "com.android.phone"

    const/4 v3, 0x0

    invoke-virtual {p0, v0, v3}, Landroid/content/Context;->createPackageContext(Ljava/lang/String;I)Landroid/content/Context;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    sget-boolean v0, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z

    if-eqz v0, :cond_1

    const-string/jumbo v0, "autoip"

    invoke-static {v3, p1, v0}, Lcom/android/settings/search/PhoneSettingsUpdateHelper;->hideByResource(Landroid/content/Context;Ljava/util/ArrayList;Ljava/lang/String;)V

    :cond_1
    invoke-static {}, Lmiui/os/Build;->getRegion()Ljava/lang/String;

    move-result-object v0

    sget-boolean v4, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z

    if-eqz v4, :cond_2

    const-string/jumbo v4, "HK"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_9

    const-string/jumbo v4, "TW"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_9

    const-string/jumbo v4, "SG"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_9

    const-string/jumbo v4, "ID"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    :goto_0
    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_2

    const-string/jumbo v0, "t9_indexing_method_title"

    invoke-static {v3, p1, v0}, Lcom/android/settings/search/PhoneSettingsUpdateHelper;->hideByResource(Landroid/content/Context;Ljava/util/ArrayList;Ljava/lang/String;)V

    :cond_2
    const-string/jumbo v0, "auto_retry_enabled"

    invoke-static {v3, v0}, Lcom/android/settings/search/PhoneSettingsUpdateHelper;->getBoolean(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    const-string/jumbo v0, "auto_retry_mode_title"

    invoke-static {v3, p1, v0}, Lcom/android/settings/search/PhoneSettingsUpdateHelper;->hideByResource(Landroid/content/Context;Ljava/util/ArrayList;Ljava/lang/String;)V

    :cond_3
    const-string/jumbo v0, "dtmf_type_enabled"

    invoke-static {v3, v0}, Lcom/android/settings/search/PhoneSettingsUpdateHelper;->getBoolean(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_4

    const-string/jumbo v0, "dtmf_tones_title"

    invoke-static {v3, p1, v0}, Lcom/android/settings/search/PhoneSettingsUpdateHelper;->hideByResource(Landroid/content/Context;Ljava/util/ArrayList;Ljava/lang/String;)V

    :cond_4
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    const-string/jumbo v4, "android.software.sip"

    invoke-virtual {v0, v4}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    const-string/jumbo v0, "ZA"

    invoke-static {v0}, Lmiui/os/Build;->checkRegion(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    :cond_5
    const-string/jumbo v0, "sip_settings"

    invoke-static {v3, p1, v0}, Lcom/android/settings/search/PhoneSettingsUpdateHelper;->hideByResource(Landroid/content/Context;Ljava/util/ArrayList;Ljava/lang/String;)V

    :cond_6
    invoke-static {}, Lcom/android/settingslib/H;->csn()Z

    move-result v0

    if-eqz v0, :cond_7

    const-string/jumbo v0, "preference_dial_pad_touch_tone_title"

    invoke-static {v3, p1, v0}, Lcom/android/settings/search/PhoneSettingsUpdateHelper;->hideByResource(Landroid/content/Context;Ljava/util/ArrayList;Ljava/lang/String;)V

    :cond_7
    :try_start_1
    invoke-static {}, Lcom/android/internal/telephony/PhoneFactory;->getPhones()[Lcom/android/internal/telephony/Phone;

    move-result-object v4

    array-length v5, v4
    :try_end_1
    .catch Ljava/lang/IllegalStateException; {:try_start_1 .. :try_end_1} :catch_1

    move v0, v2

    :goto_1
    if-ge v2, v5, :cond_a

    :try_start_2
    aget-object v6, v4, v2

    invoke-virtual {v6}, Lcom/android/internal/telephony/Phone;->getPhoneType()I

    move-result v7

    const/4 v8, 0x2

    if-ne v7, v8, :cond_8

    invoke-virtual {v6}, Lcom/android/internal/telephony/Phone;->getIccCard()Lcom/android/internal/telephony/IccCard;

    move-result-object v6

    invoke-interface {v6}, Lcom/android/internal/telephony/IccCard;->hasIccCard()Z
    :try_end_2
    .catch Ljava/lang/IllegalStateException; {:try_start_2 .. :try_end_2} :catch_2

    move-result v6

    if-eqz v6, :cond_8

    move v0, v1

    :cond_8
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :catch_0
    move-exception v0

    return-void

    :cond_9
    move v0, v1

    goto :goto_0

    :catch_1
    move-exception v0

    move v0, v2

    :cond_a
    :goto_2
    if-eqz v0, :cond_b

    invoke-static {}, Lmiui/telephony/TelephonyManager;->getDefault()Lmiui/telephony/TelephonyManager;

    move-result-object v0

    invoke-virtual {v0}, Lmiui/telephony/TelephonyManager;->getIccCardCount()I

    move-result v0

    if-gt v0, v1, :cond_b

    const-string/jumbo v0, "caller_id_title"

    invoke-static {v3, p1, v0}, Lcom/android/settings/search/PhoneSettingsUpdateHelper;->hideByResource(Landroid/content/Context;Ljava/util/ArrayList;Ljava/lang/String;)V

    :cond_b
    invoke-static {}, Lmiui/telephony/TelephonyManager;->getDefault()Lmiui/telephony/TelephonyManager;

    move-result-object v0

    invoke-virtual {v0}, Lmiui/telephony/TelephonyManager;->isDualVolteSupported()Z

    move-result v0

    if-nez v0, :cond_c

    const-string/jumbo v0, "dual_4g_switch_title"

    invoke-static {p0, p1, v0}, Lcom/android/settings/search/PhoneSettingsUpdateHelper;->hideByResource(Landroid/content/Context;Ljava/util/ArrayList;Ljava/lang/String;)V

    :cond_c
    return-void

    :catch_2
    move-exception v2

    goto :goto_2
.end method
