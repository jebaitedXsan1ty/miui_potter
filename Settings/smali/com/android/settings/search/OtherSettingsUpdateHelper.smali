.class Lcom/android/settings/search/OtherSettingsUpdateHelper;
.super Lcom/android/settings/search/BaseSearchUpdateHelper;
.source "OtherSettingsUpdateHelper.java"


# static fields
.field private static final APP_UPDATER_RESOURCE:Ljava/lang/String; = "system_apps_updater"

.field private static final AVOID_UI_RESOURCE:Ljava/lang/String; = "avoid_ui"

.field private static final BATTERY_INDICATOR_STYLE_RESOURCE:Ljava/lang/String; = "battery_indicator_style"

.field private static final CALL_BREATHING_LIGHT_COLOR_RESOURCE:Ljava/lang/String; = "call_breathing_light_color"

.field private static final CALL_BREATHING_LIGHT_FREQ_RESOURCE:Ljava/lang/String; = "call_breathing_light_freq"

.field private static final FAKECELL_SETTINGS_RESOURCE:Ljava/lang/String; = "manage_fakecell_settings"

.field private static final HOME_XIAOAI_RESOURCE:Ljava/lang/String; = "voice_assist"

.field private static final INFINITY_DISPLAY_RESOURCE:Ljava/lang/String; = "infinity_display_title"

.field private static final LOCKSCREEN_MAGAZINE_RESOURCE:Ljava/lang/String; = "lockscreen_magazine"

.field private static final MIUI_LAB_RESOURCE:Ljava/lang/String; = "miui_lab_settings"

.field private static final MMS_BREATHING_LIGHT_COLOR_RESOURCE:Ljava/lang/String; = "mms_breathing_light_color"

.field private static final MMS_BREATHING_LIGHT_FREQ_RESOURCE:Ljava/lang/String; = "mms_breathing_light_freq"

.field private static final POWER_MODE_RESOURCE:Ljava/lang/String; = "power_mode"

.field private static final PRINT_RESOURCE:Ljava/lang/String; = "print_settings"

.field private static final SECOND_SPACE_RESOURCE:Ljava/lang/String; = "second_space"

.field private static final SHOW_NOTIFICATION_ICON_RESOURCE:Ljava/lang/String; = "status_bar_settings_show_notification_icon"

.field private static final SPELLCHECKERS_RESOURCE:Ljava/lang/String; = "spellcheckers_settings_title"

.field private static final STATUS_BAR_RESOURCE:Ljava/lang/String; = "status_bar_settings"

.field private static final TAPLUS_RESOURCE:Ljava/lang/String; = "miui_lab_taplus_title"

.field private static final THEME_RESOURCE:Ljava/lang/String; = "theme_settings_title"

.field private static final USER_DICT_RESOURCE:Ljava/lang/String; = "user_dict_settings_title"

.field private static final VIBRATE_INPUT_DEVICES_RESOURCE:Ljava/lang/String; = "vibrate_input_devices"

.field private static final WALLPAPER_RESOURCE:Ljava/lang/String; = "wallpaper_settings_title"

.field private static final XSPACE_RESOURCE:Ljava/lang/String; = "xspace"


# direct methods
.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/search/BaseSearchUpdateHelper;-><init>()V

    return-void
.end method

.method static update(Landroid/content/Context;Ljava/util/ArrayList;)V
    .locals 11

    const/4 v10, 0x2

    const/4 v9, 0x5

    const/4 v3, 0x1

    const/4 v8, 0x0

    const/4 v1, 0x0

    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v5

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x13

    if-le v0, v2, :cond_0

    invoke-static {}, Lcom/android/settingslib/H;->csn()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string/jumbo v0, "second_space"

    invoke-static {p0, p1, v0}, Lcom/android/settings/search/OtherSettingsUpdateHelper;->hideTreeByRootResource(Landroid/content/Context;Ljava/util/ArrayList;Ljava/lang/String;)V

    :cond_0
    invoke-static {}, Lcom/android/settingslib/H;->csn()Z

    move-result v0

    if-eqz v0, :cond_1

    const-string/jumbo v0, "wallpaper_settings_title"

    invoke-static {p0, p1, v0}, Lcom/android/settings/search/OtherSettingsUpdateHelper;->hideTreeByRootResource(Landroid/content/Context;Ljava/util/ArrayList;Ljava/lang/String;)V

    const-string/jumbo v0, "xspace"

    invoke-static {p0, p1, v0}, Lcom/android/settings/search/OtherSettingsUpdateHelper;->hideByResource(Landroid/content/Context;Ljava/util/ArrayList;Ljava/lang/String;)V

    :cond_1
    invoke-static {p0}, Lmiui/theme/ThemeManagerHelper;->needDisableTheme(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_2

    invoke-static {}, Lcom/android/settingslib/H;->csn()Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_2
    const-string/jumbo v0, "theme_settings_title"

    invoke-static {p0, p1, v0}, Lcom/android/settings/search/OtherSettingsUpdateHelper;->hideTreeByRootResource(Landroid/content/Context;Ljava/util/ArrayList;Ljava/lang/String;)V

    :cond_3
    const-string/jumbo v0, "support_power_mode"

    invoke-static {v0, v1}, Lmiui/util/FeatureParser;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_4

    const-string/jumbo v0, "power_mode"

    invoke-static {p0, p1, v0}, Lcom/android/settings/search/OtherSettingsUpdateHelper;->hideByResource(Landroid/content/Context;Ljava/util/ArrayList;Ljava/lang/String;)V

    :cond_4
    invoke-static {p0}, Lcom/android/settingslib/H;->csk(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_5

    const-string/jumbo v0, "status_bar_settings"

    invoke-static {p0, p1, v0}, Lcom/android/settings/search/OtherSettingsUpdateHelper;->hideTreeByRootResource(Landroid/content/Context;Ljava/util/ArrayList;Ljava/lang/String;)V

    :cond_5
    const-string/jumbo v0, "1"

    const-string/jumbo v2, "ro.miui.notch"

    const-string/jumbo v4, "0"

    invoke-static {v2, v4}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    const-string/jumbo v0, "battery_indicator_style"

    invoke-static {p0, p1, v0}, Lcom/android/settings/search/OtherSettingsUpdateHelper;->hideByResource(Landroid/content/Context;Ljava/util/ArrayList;Ljava/lang/String;)V

    const-string/jumbo v0, "status_bar_settings_show_notification_icon"

    invoke-static {p0, p1, v0}, Lcom/android/settings/search/OtherSettingsUpdateHelper;->hideByResource(Landroid/content/Context;Ljava/util/ArrayList;Ljava/lang/String;)V

    :cond_6
    const-string/jumbo v0, "sensor"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/SensorManager;

    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object v0

    if-eqz v0, :cond_21

    const-string/jumbo v2, "Elliptic Proximity"

    invoke-virtual {v0}, Landroid/hardware/Sensor;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_16

    const-string/jumbo v2, "Elliptic Labs"

    invoke-virtual {v0}, Landroid/hardware/Sensor;->getVendor()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    :goto_0
    const-string/jumbo v2, "com.miui.sensor.avoid"

    invoke-virtual {v5, v2}, Landroid/content/pm/PackageManager;->isPackageAvailable(Ljava/lang/String;)Z

    move-result v2

    xor-int/lit8 v2, v2, 0x1

    or-int/2addr v0, v2

    if-eqz v0, :cond_7

    const-string/jumbo v0, "avoid_ui"

    invoke-static {p0, p1, v0}, Lcom/android/settings/search/OtherSettingsUpdateHelper;->hideByResource(Landroid/content/Context;Ljava/util/ArrayList;Ljava/lang/String;)V

    :cond_7
    invoke-static {p0}, Lcom/android/settings/aq;->bqw(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_8

    const-string/jumbo v0, "call_breathing_light_color"

    invoke-static {p0, p1, v0}, Lcom/android/settings/search/OtherSettingsUpdateHelper;->hideByResource(Landroid/content/Context;Ljava/util/ArrayList;Ljava/lang/String;)V

    const-string/jumbo v0, "call_breathing_light_freq"

    invoke-static {p0, p1, v0}, Lcom/android/settings/search/OtherSettingsUpdateHelper;->hideByResource(Landroid/content/Context;Ljava/util/ArrayList;Ljava/lang/String;)V

    const-string/jumbo v0, "mms_breathing_light_color"

    invoke-static {p0, p1, v0}, Lcom/android/settings/search/OtherSettingsUpdateHelper;->hideByResource(Landroid/content/Context;Ljava/util/ArrayList;Ljava/lang/String;)V

    const-string/jumbo v0, "mms_breathing_light_freq"

    invoke-static {p0, p1, v0}, Lcom/android/settings/search/OtherSettingsUpdateHelper;->hideByResource(Landroid/content/Context;Ljava/util/ArrayList;Ljava/lang/String;)V

    :cond_8
    sget-boolean v0, Lmiui/os/Build;->IS_TABLET:Z

    if-nez v0, :cond_9

    sget-boolean v0, Lmiui/os/Build;->IS_GLOBAL_BUILD:Z

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_a

    :cond_9
    const-string/jumbo v0, "system_apps_updater"

    invoke-static {p0, p1, v0}, Lcom/android/settings/search/OtherSettingsUpdateHelper;->hideByResource(Landroid/content/Context;Ljava/util/ArrayList;Ljava/lang/String;)V

    :cond_a
    const-string/jumbo v0, "textservices"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/textservice/TextServicesManager;

    invoke-virtual {v0}, Landroid/view/textservice/TextServicesManager;->getEnabledSpellCheckers()[Landroid/view/textservice/SpellCheckerInfo;

    move-result-object v2

    invoke-virtual {v0}, Landroid/view/textservice/TextServicesManager;->isSpellCheckerEnabled()Z

    move-result v0

    if-eqz v0, :cond_b

    if-nez v2, :cond_17

    :cond_b
    :goto_1
    const-string/jumbo v0, "spellcheckers_settings_title"

    invoke-static {p0, p1, v0}, Lcom/android/settings/search/OtherSettingsUpdateHelper;->hideByResource(Landroid/content/Context;Ljava/util/ArrayList;Ljava/lang/String;)V

    :cond_c
    const-string/jumbo v0, "input_method"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    invoke-virtual {v0}, Landroid/view/inputmethod/InputMethodManager;->getEnabledInputMethodList()Ljava/util/List;

    move-result-object v6

    if-nez v6, :cond_18

    move v2, v1

    :goto_2
    move v4, v1

    :goto_3
    if-ge v4, v2, :cond_20

    invoke-interface {v6, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodInfo;

    invoke-virtual {v0, v5}, Landroid/view/inputmethod/InputMethodInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v0

    if-eqz v0, :cond_19

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v7, "AOSP"

    invoke-virtual {v0, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_19

    move v0, v3

    :goto_4
    if-nez v0, :cond_d

    const-string/jumbo v0, "user_dict_settings_title"

    invoke-static {p0, p1, v0}, Lcom/android/settings/search/OtherSettingsUpdateHelper;->hideByResource(Landroid/content/Context;Ljava/util/ArrayList;Ljava/lang/String;)V

    :cond_d
    invoke-static {}, Landroid/view/InputDevice;->getDeviceIds()[I

    move-result-object v2

    move v0, v1

    :goto_5
    array-length v4, v2

    if-ge v0, v4, :cond_1f

    aget v4, v2, v0

    invoke-static {v4}, Landroid/view/InputDevice;->getDevice(I)Landroid/view/InputDevice;

    move-result-object v4

    if-eqz v4, :cond_1a

    invoke-virtual {v4}, Landroid/view/InputDevice;->isVirtual()Z

    move-result v6

    xor-int/lit8 v6, v6, 0x1

    if-eqz v6, :cond_1a

    invoke-virtual {v4}, Landroid/view/InputDevice;->getVibrator()Landroid/os/Vibrator;

    move-result-object v4

    invoke-virtual {v4}, Landroid/os/Vibrator;->hasVibrator()Z

    move-result v4

    if-eqz v4, :cond_1a

    move v0, v3

    :goto_6
    if-nez v0, :cond_e

    const-string/jumbo v0, "vibrate_input_devices"

    invoke-static {p0, p1, v0}, Lcom/android/settings/search/OtherSettingsUpdateHelper;->hideByResource(Landroid/content/Context;Ljava/util/ArrayList;Ljava/lang/String;)V

    :cond_e
    invoke-static {}, Lcom/android/settings/FakeCellSettings;->bRF()Z

    move-result v0

    if-nez v0, :cond_f

    const-string/jumbo v0, "manage_fakecell_settings"

    invoke-static {p0, p1, v0}, Lcom/android/settings/search/OtherSettingsUpdateHelper;->hideByResource(Landroid/content/Context;Ljava/util/ArrayList;Ljava/lang/String;)V

    :cond_f
    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v2, "android.printservice.PrintService"

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const/16 v2, 0x84

    invoke-virtual {v5, v0, v2}, Landroid/content/pm/PackageManager;->queryIntentServices(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_10

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_11

    :cond_10
    const-string/jumbo v0, "print_settings"

    invoke-static {p0, p1, v0}, Lcom/android/settings/search/OtherSettingsUpdateHelper;->hideByResource(Landroid/content/Context;Ljava/util/ArrayList;Ljava/lang/String;)V

    :cond_11
    sget-boolean v0, Lmiui/os/Build;->IS_GLOBAL_BUILD:Z

    if-eqz v0, :cond_1b

    const-string/jumbo v0, "miui_lab_settings"

    invoke-static {p0, p1, v0}, Lcom/android/settings/search/OtherSettingsUpdateHelper;->hideByResource(Landroid/content/Context;Ljava/util/ArrayList;Ljava/lang/String;)V

    :cond_12
    :goto_7
    const-string/jumbo v0, "window"

    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Landroid/view/IWindowManager$Stub;->asInterface(Landroid/os/IBinder;)Landroid/view/IWindowManager;

    move-result-object v0

    :try_start_0
    invoke-interface {v0}, Landroid/view/IWindowManager;->hasNavigationBar()Z

    move-result v0

    if-nez v0, :cond_13

    const-string/jumbo v0, "infinity_display_title"

    invoke-static {p0, p1, v0}, Lcom/android/settings/search/OtherSettingsUpdateHelper;->hideTreeByRootResource(Landroid/content/Context;Ljava/util/ArrayList;Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_13
    :goto_8
    const-string/jumbo v0, "support_main_xiaoai"

    invoke-static {v0, v1}, Lmiui/util/FeatureParser;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_14

    invoke-static {p0}, Lcom/android/settings/dc;->bYw(Landroid/content/Context;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_1d

    :cond_14
    const-string/jumbo v0, "voice_assist"

    invoke-static {p0, p1, v0}, Lcom/android/settings/search/OtherSettingsUpdateHelper;->hideByResource(Landroid/content/Context;Ljava/util/ArrayList;Ljava/lang/String;)V

    :cond_15
    const-string/jumbo v0, "lockscreen_magazine"

    invoke-static {p0, v0}, Lcom/android/settings/search/OtherSettingsUpdateHelper;->getIdWithResource(Landroid/content/Context;Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_9
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1e

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-array v4, v9, [Ljava/lang/String;

    const-string/jumbo v5, "intent_action"

    aput-object v5, v4, v1

    const-string/jumbo v5, "intent_data"

    aput-object v5, v4, v3

    const-string/jumbo v5, "dest_package"

    aput-object v5, v4, v10

    const-string/jumbo v5, "dest_class"

    const/4 v6, 0x3

    aput-object v5, v4, v6

    const-string/jumbo v5, "fragment"

    const/4 v6, 0x4

    aput-object v5, v4, v6

    new-array v5, v9, [Ljava/lang/String;

    const-string/jumbo v6, ""

    aput-object v6, v5, v1

    const-string/jumbo v6, ""

    aput-object v6, v5, v3

    const-string/jumbo v6, "com.mfashiongallery.emag"

    aput-object v6, v5, v10

    const-string/jumbo v6, "com.mfashiongallery.emag.ssetting.JumpSettingActivity"

    const/4 v7, 0x3

    aput-object v6, v5, v7

    const-string/jumbo v6, ""

    const/4 v7, 0x4

    aput-object v6, v5, v7

    invoke-static {p1, v0, v9, v4, v5}, Lcom/android/settings/search/BaseSearchUpdateHelper;->updateSearchItem(Ljava/util/ArrayList;Ljava/lang/String;I[Ljava/lang/String;[Ljava/lang/String;)V

    goto :goto_9

    :cond_16
    move v0, v1

    goto/16 :goto_0

    :cond_17
    array-length v0, v2

    if-nez v0, :cond_c

    goto/16 :goto_1

    :cond_18
    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v0

    move v2, v0

    goto/16 :goto_2

    :cond_19
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto/16 :goto_3

    :cond_1a
    add-int/lit8 v0, v0, 0x1

    goto/16 :goto_5

    :cond_1b
    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v2, "com.miui.contentextension.action.TAPLUS_SETTINGS"

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v0, v1}, Landroid/content/pm/PackageManager;->resolveActivity(Landroid/content/Intent;I)Landroid/content/pm/ResolveInfo;

    move-result-object v0

    if-eqz v0, :cond_1c

    iget-object v0, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    if-nez v0, :cond_12

    :cond_1c
    const-string/jumbo v0, "miui_lab_taplus_title"

    invoke-static {p0, p1, v0}, Lcom/android/settings/search/OtherSettingsUpdateHelper;->hideByResource(Landroid/content/Context;Ljava/util/ArrayList;Ljava/lang/String;)V

    goto/16 :goto_7

    :cond_1d
    const-string/jumbo v0, "content://com.xiaomi.providers.appindex/functions/com.miui.voiceassist"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    invoke-virtual {v2, v0, v8, v8}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    invoke-static {}, Lcom/android/settings/dc;->bYy()Landroid/content/Intent;

    move-result-object v2

    if-eqz v2, :cond_15

    const-string/jumbo v0, "voice_assist"

    invoke-static {p0, v0}, Lcom/android/settings/search/OtherSettingsUpdateHelper;->getIdWithResource(Landroid/content/Context;Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_a
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_15

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-array v5, v9, [Ljava/lang/String;

    const-string/jumbo v6, "intent_action"

    aput-object v6, v5, v1

    const-string/jumbo v6, "intent_data"

    aput-object v6, v5, v3

    const-string/jumbo v6, "dest_package"

    aput-object v6, v5, v10

    const-string/jumbo v6, "dest_class"

    const/4 v7, 0x3

    aput-object v6, v5, v7

    const-string/jumbo v6, "fragment"

    const/4 v7, 0x4

    aput-object v6, v5, v7

    new-array v6, v9, [Ljava/lang/String;

    const-string/jumbo v7, ""

    aput-object v7, v6, v1

    const-string/jumbo v7, ""

    aput-object v7, v6, v3

    invoke-virtual {v2}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v7

    invoke-virtual {v7}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v10

    invoke-virtual {v2}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v7

    invoke-virtual {v7}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x3

    aput-object v7, v6, v8

    const-string/jumbo v7, ""

    const/4 v8, 0x4

    aput-object v7, v6, v8

    invoke-static {p1, v0, v9, v5, v6}, Lcom/android/settings/search/BaseSearchUpdateHelper;->updateSearchItem(Ljava/util/ArrayList;Ljava/lang/String;I[Ljava/lang/String;[Ljava/lang/String;)V

    goto :goto_a

    :cond_1e
    return-void

    :catch_0
    move-exception v0

    goto/16 :goto_8

    :cond_1f
    move v0, v1

    goto/16 :goto_6

    :cond_20
    move v0, v1

    goto/16 :goto_4

    :cond_21
    move v0, v1

    goto/16 :goto_0
.end method
