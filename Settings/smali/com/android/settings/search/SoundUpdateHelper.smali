.class Lcom/android/settings/search/SoundUpdateHelper;
.super Lcom/android/settings/search/BaseSearchUpdateHelper;
.source "SoundUpdateHelper.java"


# static fields
.field private static final DOCK_SETTINGS_RESOURCE:Ljava/lang/String; = "bluetooth_dock_settings_a2dp"

.field private static final DOCK_SOUNDS_ENABLE_RESOURCE:Ljava/lang/String; = "dock_sounds_enable_title"

.field private static final DOLBY_RESOURCE:Ljava/lang/String; = "music_title_dolby_control"

.field private static final HEADSET_CALIBRATE_RESOURCE:Ljava/lang/String; = "music_headset_calibrate"

.field private static final MI_EFFECT_RESOURCE:Ljava/lang/String; = "music_mi_effect_title"

.field private static final MUSIC_EQUALIZER_RESOURCE:Ljava/lang/String; = "music_equalizer"

.field private static final MUSIC_HD_RESOURCE:Ljava/lang/String; = "music_hd_title"

.field private static final MUSIC_HIFI_RESOURCE:Ljava/lang/String; = "music_hifi_title"

.field private static final NEW_SILENT_RESOURCE:Ljava/lang/String; = "silent_settings"

.field private static final OLD_SILENT_RESOURCE:Ljava/lang/String; = "silent_mode_title"

.field private static final SOUND_RESOURCE:Ljava/lang/String; = "sound_settings"

.field private static final SOUND_VIBRATE_RESOURCE:Ljava/lang/String; = "sound_vibrate_settings"

.field private static final ZEN_MODE_RESOURCE:Ljava/lang/String; = "do_not_disturb_mode"


# direct methods
.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/search/BaseSearchUpdateHelper;-><init>()V

    return-void
.end method

.method static headsetPlug(Landroid/content/Context;Ljava/util/ArrayList;Z)V
    .locals 2

    const-string/jumbo v0, "music_headset_calibrate"

    xor-int/lit8 v1, p2, 0x1

    invoke-static {p0, p1, v0, v1}, Lcom/android/settings/search/SoundUpdateHelper;->disableByResource(Landroid/content/Context;Ljava/util/ArrayList;Ljava/lang/String;Z)V

    invoke-static {p0, p1}, Lcom/android/settings/search/SoundUpdateHelper;->updateEqualizer(Landroid/content/Context;Ljava/util/ArrayList;)V

    return-void
.end method

.method static update(Landroid/content/Context;Ljava/util/ArrayList;)V
    .locals 5

    const-string/jumbo v0, "vibrator"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Vibrator;

    invoke-virtual {v0}, Landroid/os/Vibrator;->hasVibrator()Z

    move-result v0

    if-nez v0, :cond_1

    const-string/jumbo v0, "sound_vibrate_settings"

    invoke-static {p0, v0}, Lcom/android/settings/search/SoundUpdateHelper;->getIdWithResource(Landroid/content/Context;Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const-string/jumbo v2, "name"

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f1210f6

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {p0, p1, v0, v2, v3}, Lcom/android/settings/search/SoundUpdateHelper;->updateItemData(Landroid/content/Context;Ljava/util/ArrayList;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_0
    const-string/jumbo v0, "sound_vibrate_settings"

    const-string/jumbo v1, "sound_settings"

    invoke-static {p0, p1, v0, v1}, Lcom/android/settings/search/SoundUpdateHelper;->updatePath(Landroid/content/Context;Ljava/util/ArrayList;Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f050024

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    if-eqz v0, :cond_6

    const-string/jumbo v0, "support_headset"

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lmiui/util/FeatureParser;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_6

    const-string/jumbo v0, "ro.audio.hifi"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "true"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_5

    sget-object v0, Lmiui/os/Build;->DEVICE:Ljava/lang/String;

    const-string/jumbo v1, "scorpio"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    sget-object v0, Lmiui/os/Build;->DEVICE:Ljava/lang/String;

    const-string/jumbo v1, "lithium"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_2
    const-string/jumbo v0, "music_hifi_title"

    const-string/jumbo v1, "music_hd_title"

    invoke-static {p0, p1, v0, v1}, Lcom/android/settings/search/SoundUpdateHelper;->updatePath(Landroid/content/Context;Ljava/util/ArrayList;Ljava/lang/String;Ljava/lang/String;)V

    :cond_3
    :goto_1
    const-string/jumbo v0, "audio"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    invoke-virtual {v0}, Landroid/media/AudioManager;->isWiredHeadsetOn()Z

    move-result v0

    invoke-static {p0, p1, v0}, Lcom/android/settings/search/SoundUpdateHelper;->headsetPlug(Landroid/content/Context;Ljava/util/ArrayList;Z)V

    :goto_2
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f05001a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    if-nez v0, :cond_4

    const-string/jumbo v0, "bluetooth_dock_settings_a2dp"

    invoke-static {p0, p1, v0}, Lcom/android/settings/search/SoundUpdateHelper;->hideByResource(Landroid/content/Context;Ljava/util/ArrayList;Ljava/lang/String;)V

    const-string/jumbo v0, "dock_sounds_enable_title"

    invoke-static {p0, p1, v0}, Lcom/android/settings/search/SoundUpdateHelper;->hideByResource(Landroid/content/Context;Ljava/util/ArrayList;Ljava/lang/String;)V

    :cond_4
    sget-boolean v0, Landroid/provider/MiuiSettings$SilenceMode;->isSupported:Z

    if-eqz v0, :cond_7

    const-string/jumbo v0, "silent_mode_title"

    invoke-static {p0, p1, v0}, Lcom/android/settings/search/SoundUpdateHelper;->hideByResource(Landroid/content/Context;Ljava/util/ArrayList;Ljava/lang/String;)V

    const-string/jumbo v0, "do_not_disturb_mode"

    invoke-static {p0, p1, v0}, Lcom/android/settings/search/SoundUpdateHelper;->hideTreeByRootResource(Landroid/content/Context;Ljava/util/ArrayList;Ljava/lang/String;)V

    :goto_3
    return-void

    :cond_5
    const-string/jumbo v0, "music_hifi_title"

    invoke-static {p0, p1, v0}, Lcom/android/settings/search/SoundUpdateHelper;->hideByResource(Landroid/content/Context;Ljava/util/ArrayList;Ljava/lang/String;)V

    goto :goto_1

    :cond_6
    const-string/jumbo v0, "music_title_dolby_control"

    invoke-static {p0, p1, v0}, Lcom/android/settings/search/SoundUpdateHelper;->hideTreeByRootResource(Landroid/content/Context;Ljava/util/ArrayList;Ljava/lang/String;)V

    const-string/jumbo v0, "music_equalizer"

    invoke-static {p0, p1, v0}, Lcom/android/settings/search/SoundUpdateHelper;->hideTreeByRootResource(Landroid/content/Context;Ljava/util/ArrayList;Ljava/lang/String;)V

    const-string/jumbo v0, "music_hifi_title"

    invoke-static {p0, p1, v0}, Lcom/android/settings/search/SoundUpdateHelper;->hideTreeByRootResource(Landroid/content/Context;Ljava/util/ArrayList;Ljava/lang/String;)V

    const-string/jumbo v0, "music_mi_effect_title"

    invoke-static {p0, p1, v0}, Lcom/android/settings/search/SoundUpdateHelper;->hideTreeByRootResource(Landroid/content/Context;Ljava/util/ArrayList;Ljava/lang/String;)V

    goto :goto_2

    :cond_7
    const-string/jumbo v0, "silent_settings"

    invoke-static {p0, p1, v0}, Lcom/android/settings/search/SoundUpdateHelper;->hideTreeByRootResource(Landroid/content/Context;Ljava/util/ArrayList;Ljava/lang/String;)V

    goto :goto_3
.end method

.method private static updateEqualizer(Landroid/content/Context;Ljava/util/ArrayList;)V
    .locals 5

    const/4 v2, 0x0

    const-string/jumbo v0, "audio"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    const/4 v1, 0x1

    invoke-virtual {v0}, Landroid/media/AudioManager;->isWiredHeadsetOn()Z

    move-result v3

    if-nez v3, :cond_0

    move v0, v1

    :goto_0
    const-string/jumbo v1, "music_equalizer"

    invoke-static {p0, p1, v1, v0}, Lcom/android/settings/search/SoundUpdateHelper;->disableByResource(Landroid/content/Context;Ljava/util/ArrayList;Ljava/lang/String;Z)V

    return-void

    :cond_0
    const-string/jumbo v3, "hifi_mode"

    invoke-virtual {v0, v3}, Landroid/media/AudioManager;->getParameters(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_1

    const-string/jumbo v4, "false"

    invoke-virtual {v3, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    xor-int/lit8 v3, v3, 0x1

    if-eqz v3, :cond_1

    move v0, v1

    goto :goto_0

    :cond_1
    const-string/jumbo v3, "dirac"

    invoke-virtual {v0, v3}, Landroid/media/AudioManager;->getParameters(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    const-string/jumbo v3, "="

    invoke-virtual {v0, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    xor-int/lit8 v3, v3, 0x1

    if-nez v3, :cond_2

    const-string/jumbo v3, "="

    invoke-virtual {v0, v3}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-nez v0, :cond_3

    :cond_2
    move v0, v1

    goto :goto_0

    :cond_3
    move v0, v2

    goto :goto_0
.end method
