.class public interface abstract Lcom/android/settings/search/IndexDatabaseHelper$Tables;
.super Ljava/lang/Object;
.source "IndexDatabaseHelper.java"


# static fields
.field public static final TABLE_META_INDEX:Ljava/lang/String; = "meta_index"

.field public static final TABLE_PREFS_INDEX:Ljava/lang/String; = "prefs_index"

.field public static final TABLE_SAVED_QUERIES:Ljava/lang/String; = "saved_queries"

.field public static final TABLE_SITE_MAP:Ljava/lang/String; = "site_map"
