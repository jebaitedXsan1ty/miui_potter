.class final Lcom/android/settings/search/SearchUpdater$TaskManager;
.super Ljava/lang/Object;
.source "SearchUpdater.java"


# instance fields
.field private mFlag:I

.field final synthetic this$0:Lcom/android/settings/search/SearchUpdater;


# direct methods
.method private constructor <init>(Lcom/android/settings/search/SearchUpdater;)V
    .locals 1

    iput-object p1, p0, Lcom/android/settings/search/SearchUpdater$TaskManager;->this$0:Lcom/android/settings/search/SearchUpdater;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Lcom/android/settings/search/SearchUpdater$TaskManager;->mFlag:I

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/settings/search/SearchUpdater;Lcom/android/settings/search/SearchUpdater$TaskManager;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/settings/search/SearchUpdater$TaskManager;-><init>(Lcom/android/settings/search/SearchUpdater;)V

    return-void
.end method


# virtual methods
.method public add(I)V
    .locals 1

    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/android/settings/search/SearchUpdater$TaskManager;->mFlag:I

    or-int/2addr v0, p1

    iput v0, p0, Lcom/android/settings/search/SearchUpdater$TaskManager;->mFlag:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public get()I
    .locals 3

    const/4 v2, 0x0

    monitor-enter p0

    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_1

    :try_start_0
    iget v1, p0, Lcom/android/settings/search/SearchUpdater$TaskManager;->mFlag:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    and-int/2addr v1, v0

    if-eqz v1, :cond_0

    monitor-exit p0

    return v0

    :cond_0
    shl-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    monitor-exit p0

    return v2

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public isEmpty()Z
    .locals 2

    const/4 v0, 0x0

    monitor-enter p0

    :try_start_0
    iget v1, p0, Lcom/android/settings/search/SearchUpdater$TaskManager;->mFlag:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public remove(I)V
    .locals 1

    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/android/settings/search/SearchUpdater$TaskManager;->mFlag:I

    and-int/2addr v0, p1

    if-ne v0, p1, :cond_0

    iget v0, p0, Lcom/android/settings/search/SearchUpdater$TaskManager;->mFlag:I

    xor-int/2addr v0, p1

    iput v0, p0, Lcom/android/settings/search/SearchUpdater$TaskManager;->mFlag:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
