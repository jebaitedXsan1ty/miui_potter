.class public Lcom/android/settings/search/tree/XiaoAiSettingsTree;
.super Lcom/miui/internal/search/SettingsTree;
.source "XiaoAiSettingsTree.java"


# direct methods
.method protected constructor <init>(Landroid/content/Context;Lorg/json/JSONObject;Lcom/miui/internal/search/SettingsTree;Z)V
    .locals 0

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/miui/internal/search/SettingsTree;-><init>(Landroid/content/Context;Lorg/json/JSONObject;Lcom/miui/internal/search/SettingsTree;Z)V

    return-void
.end method


# virtual methods
.method public getIntent()Landroid/content/Intent;
    .locals 1

    invoke-static {}, Lcom/android/settings/dc;->bYy()Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method protected getStatus()I
    .locals 2

    const/4 v1, 0x0

    const-string/jumbo v0, "support_main_xiaoai"

    invoke-static {v0, v1}, Lmiui/util/FeatureParser;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/search/tree/XiaoAiSettingsTree;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/settings/dc;->bYw(Landroid/content/Context;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_1

    :cond_0
    return v1

    :cond_1
    invoke-super {p0}, Lcom/miui/internal/search/SettingsTree;->getStatus()I

    move-result v0

    return v0
.end method
