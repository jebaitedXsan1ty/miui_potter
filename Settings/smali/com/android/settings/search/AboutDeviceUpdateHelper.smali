.class Lcom/android/settings/search/AboutDeviceUpdateHelper;
.super Lcom/android/settings/search/BaseSearchUpdateHelper;
.source "AboutDeviceUpdateHelper.java"


# static fields
.field private static final ABOUT_PHONE_RESOURCE:Ljava/lang/String; = "about_settings"

.field private static final ACTION_COPYRIGHT:Ljava/lang/String; = "android.settings.COPYRIGHT"

.field private static final ACTION_LICENSE:Ljava/lang/String; = "android.settings.LICENSE"

.field private static final ACTION_TERMS:Ljava/lang/String; = "android.settings.TERMS"

.field private static final APPROVE_RESOURCE:Ljava/lang/String; = "approve_title"

.field private static final BASEBAND_VERSION_RESOURCE:Ljava/lang/String; = "baseband_version"

.field private static final COPYRIGHT_RESOURCE:Ljava/lang/String; = "copyright_title"

.field private static final HARDWARE_VERSION_RESOURCE:Ljava/lang/String; = "hardware_version"

.field private static final INSTRUCTION_RESOURCE:Ljava/lang/String; = "instruction_title"

.field private static final LICENSE_RESOURCE:Ljava/lang/String; = "license_title"

.field private static final MY_DEVICE_RESOURCE:Ljava/lang/String; = "my_device"

.field private static final PRE_INSTALLED_APPLICATION_RESOURCE:Ljava/lang/String; = "pre_installed_application"

.field private static final SAFETY_LEGAL_RESOURCE:Ljava/lang/String; = "settings_safetylegal_title"

.field private static final STATUS_BT_ADDRESS_RESOURCE:Ljava/lang/String; = "status_bt_address"

.field private static final STATUS_SERIALNO_RESOURCE:Ljava/lang/String; = "status_serialno"

.field private static final STATUS_SERIAL_NUMBER_RESOURCE:Ljava/lang/String; = "status_serial_number"

.field private static final STATUS_WIMAX_MAC_ADDRESS_RESOURCE:Ljava/lang/String; = "status_wimax_mac_address"

.field private static final TERMS_RESOURCE:Ljava/lang/String; = "terms_title"

.field private static final TRANSLATION_CONTRIBUTORS_RESOURCE:Ljava/lang/String; = "translation_contributors_title"

.field private static final TYPE_APPROVAL_RESOURCE:Ljava/lang/String; = "wifi_type_approval_title"


# direct methods
.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/search/BaseSearchUpdateHelper;-><init>()V

    return-void
.end method

.method private static hideByResourceIfNoActivity(Landroid/content/Context;Ljava/util/ArrayList;Ljava/lang/String;Landroid/content/Intent;)V
    .locals 4

    const/4 v0, 0x0

    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    invoke-virtual {v1, p3, v0}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v3

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/ResolveInfo;

    iget-object v0, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v0, v0, Landroid/content/pm/ActivityInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget v0, v0, Landroid/content/pm/ApplicationInfo;->flags:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    return-void

    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    invoke-static {p0, p1, p2}, Lcom/android/settings/search/AboutDeviceUpdateHelper;->hideByResource(Landroid/content/Context;Ljava/util/ArrayList;Ljava/lang/String;)V

    return-void
.end method

.method static update(Landroid/content/Context;Ljava/util/ArrayList;)V
    .locals 4

    const-string/jumbo v0, "connectivity"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    invoke-static {}, Lcom/android/settings/device/h;->aDV()Z

    move-result v1

    if-eqz v1, :cond_d

    const-string/jumbo v1, "about_settings"

    invoke-static {p0, p1, v1}, Lcom/android/settings/search/AboutDeviceUpdateHelper;->hideTreeByRootResource(Landroid/content/Context;Ljava/util/ArrayList;Ljava/lang/String;)V

    :goto_0
    invoke-static {p0}, Lcom/android/settings/aq;->bqv(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string/jumbo v1, "baseband_version"

    invoke-static {p0, p1, v1}, Lcom/android/settings/search/AboutDeviceUpdateHelper;->hideByResource(Landroid/content/Context;Ljava/util/ArrayList;Ljava/lang/String;)V

    :cond_0
    const-string/jumbo v1, "ro.miui.cust_hardware"

    const-string/jumbo v2, ""

    invoke-static {v1, v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    const-string/jumbo v1, "hardware_version"

    invoke-static {p0, p1, v1}, Lcom/android/settings/search/AboutDeviceUpdateHelper;->hideByResource(Landroid/content/Context;Ljava/util/ArrayList;Ljava/lang/String;)V

    :cond_1
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f121622

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    const-string/jumbo v1, "wifi_type_approval_title"

    invoke-static {p0, p1, v1}, Lcom/android/settings/search/AboutDeviceUpdateHelper;->hideByResource(Landroid/content/Context;Ljava/util/ArrayList;Ljava/lang/String;)V

    :cond_2
    const-string/jumbo v1, "ro.url.safetylegal"

    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_3

    const-string/jumbo v1, "settings_safetylegal_title"

    invoke-static {p0, p1, v1}, Lcom/android/settings/search/AboutDeviceUpdateHelper;->hideByResource(Landroid/content/Context;Ljava/util/ArrayList;Ljava/lang/String;)V

    :cond_3
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f05001d

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v1

    if-nez v1, :cond_4

    const-string/jumbo v1, "translation_contributors_title"

    invoke-static {p0, p1, v1}, Lcom/android/settings/search/AboutDeviceUpdateHelper;->hideByResource(Landroid/content/Context;Ljava/util/ArrayList;Ljava/lang/String;)V

    :cond_4
    invoke-static {}, Lcom/android/settings/device/h;->aDX()Z

    move-result v1

    if-nez v1, :cond_5

    const-string/jumbo v1, "instruction_title"

    invoke-static {p0, p1, v1}, Lcom/android/settings/search/AboutDeviceUpdateHelper;->hideByResource(Landroid/content/Context;Ljava/util/ArrayList;Ljava/lang/String;)V

    :cond_5
    invoke-static {}, Lcom/android/settings/device/h;->aDY()Z

    move-result v1

    if-nez v1, :cond_6

    const-string/jumbo v1, "approve_title"

    invoke-static {p0, p1, v1}, Lcom/android/settings/search/AboutDeviceUpdateHelper;->hideByResource(Landroid/content/Context;Ljava/util/ArrayList;Ljava/lang/String;)V

    :cond_6
    const-string/jumbo v1, "copyright_title"

    new-instance v2, Landroid/content/Intent;

    const-string/jumbo v3, "android.settings.COPYRIGHT"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-static {p0, p1, v1, v2}, Lcom/android/settings/search/AboutDeviceUpdateHelper;->hideByResourceIfNoActivity(Landroid/content/Context;Ljava/util/ArrayList;Ljava/lang/String;Landroid/content/Intent;)V

    const-string/jumbo v1, "license_title"

    new-instance v2, Landroid/content/Intent;

    const-string/jumbo v3, "android.settings.LICENSE"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-static {p0, p1, v1, v2}, Lcom/android/settings/search/AboutDeviceUpdateHelper;->hideByResourceIfNoActivity(Landroid/content/Context;Ljava/util/ArrayList;Ljava/lang/String;Landroid/content/Intent;)V

    const-string/jumbo v1, "terms_title"

    new-instance v2, Landroid/content/Intent;

    const-string/jumbo v3, "android.settings.TERMS"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-static {p0, p1, v1, v2}, Lcom/android/settings/search/AboutDeviceUpdateHelper;->hideByResourceIfNoActivity(Landroid/content/Context;Ljava/util/ArrayList;Ljava/lang/String;Landroid/content/Intent;)V

    invoke-static {}, Landroid/bluetooth/BluetoothAdapter;->getDefaultAdapter()Landroid/bluetooth/BluetoothAdapter;

    move-result-object v1

    if-nez v1, :cond_7

    const-string/jumbo v1, "status_bt_address"

    invoke-static {p0, p1, v1}, Lcom/android/settings/search/AboutDeviceUpdateHelper;->hideByResource(Landroid/content/Context;Ljava/util/ArrayList;Ljava/lang/String;)V

    :cond_7
    const/4 v1, 0x6

    invoke-virtual {v0, v1}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    move-result-object v0

    if-nez v0, :cond_8

    const-string/jumbo v0, "status_wimax_mac_address"

    invoke-static {p0, p1, v0}, Lcom/android/settings/search/AboutDeviceUpdateHelper;->hideByResource(Landroid/content/Context;Ljava/util/ArrayList;Ljava/lang/String;)V

    :cond_8
    sget-object v0, Lmiui/os/Build;->SERIAL:Ljava/lang/String;

    if-eqz v0, :cond_9

    sget-object v0, Lmiui/os/Build;->SERIAL:Ljava/lang/String;

    const-string/jumbo v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_a

    :cond_9
    const-string/jumbo v0, "status_serial_number"

    invoke-static {p0, p1, v0}, Lcom/android/settings/search/AboutDeviceUpdateHelper;->hideByResource(Landroid/content/Context;Ljava/util/ArrayList;Ljava/lang/String;)V

    :cond_a
    const-string/jumbo v0, "permanent.hw.custom.serialno"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_b

    const-string/jumbo v0, "status_serialno"

    invoke-static {p0, p1, v0}, Lcom/android/settings/search/AboutDeviceUpdateHelper;->hideByResource(Landroid/content/Context;Ljava/util/ArrayList;Ljava/lang/String;)V

    :cond_b
    invoke-static {}, Lcom/android/settings/device/h;->aDR()Z

    move-result v0

    if-nez v0, :cond_c

    const-string/jumbo v0, "pre_installed_application"

    invoke-static {p0, p1, v0}, Lcom/android/settings/search/AboutDeviceUpdateHelper;->hideByResource(Landroid/content/Context;Ljava/util/ArrayList;Ljava/lang/String;)V

    :cond_c
    return-void

    :cond_d
    const-string/jumbo v1, "my_device"

    invoke-static {p0, p1, v1}, Lcom/android/settings/search/AboutDeviceUpdateHelper;->hideTreeByRootResource(Landroid/content/Context;Ljava/util/ArrayList;Ljava/lang/String;)V

    goto/16 :goto_0
.end method
