.class public Lcom/android/settings/search/SearchResult;
.super Ljava/lang/Object;
.source "SearchResult.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "SearchResult"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static checkBundleEqual(Landroid/os/Bundle;Landroid/os/Bundle;)Z
    .locals 6

    const/4 v5, 0x0

    invoke-virtual {p0}, Landroid/os/Bundle;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    return v5

    :cond_1
    invoke-virtual {p0, v0}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    instance-of v0, v1, Landroid/os/Bundle;

    if-eqz v0, :cond_2

    instance-of v0, v3, Landroid/os/Bundle;

    if-eqz v0, :cond_2

    move-object v0, v1

    check-cast v0, Landroid/os/Bundle;

    move-object v2, v3

    check-cast v2, Landroid/os/Bundle;

    invoke-static {v0, v2}, Lcom/android/settings/search/SearchResult;->checkBundleEqual(Landroid/os/Bundle;Landroid/os/Bundle;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_2

    return v5

    :cond_2
    invoke-static {v1, v3}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    return v5

    :cond_3
    const/4 v0, 0x1

    return v0
.end method

.method private static checkIntentEqual(Landroid/content/Intent;Landroid/content/Intent;)Z
    .locals 2

    if-nez p0, :cond_0

    if-nez p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    if-eqz p0, :cond_1

    if-eqz p1, :cond_1

    invoke-virtual {p0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v0

    invoke-virtual {p1}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v1

    invoke-static {v0, v1}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/settings/search/SearchResult;->checkBundleEqual(Landroid/os/Bundle;Landroid/os/Bundle;)Z

    move-result v0

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public getSearchResultList(Landroid/content/Context;Ljava/lang/String;)Ljava/util/List;
    .locals 9

    const/4 v6, 0x0

    const/4 v3, 0x0

    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-static {p2}, Lcom/android/settings/search/provider/SettingsProvider;->getSearchUri(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    sget-object v2, Lcom/miui/internal/search/Function;->FUNCTIONS:[Ljava/lang/String;

    move-object v4, v3

    move-object v5, v3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    new-instance v7, Ljava/util/LinkedList;

    invoke-direct {v7}, Ljava/util/LinkedList;-><init>()V

    if-nez v0, :cond_0

    return-object v7

    :cond_0
    :try_start_0
    const-class v1, Landroid/database/CursorWrapper;

    const-string/jumbo v2, "mCursor"

    invoke-virtual {v1, v2}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Ljava/lang/reflect/Field;->setAccessible(Z)V

    invoke-virtual {v1, v0}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/miui/internal/search/RankedResult;

    move v5, v6

    move-object v4, v3

    :goto_0
    invoke-virtual {v0}, Lcom/miui/internal/search/RankedResult;->size()I

    move-result v1

    if-ge v5, v1, :cond_2

    invoke-virtual {v0, v5}, Lcom/miui/internal/search/RankedResult;->get(I)[Ljava/lang/Object;

    move-result-object v6

    const-string/jumbo v1, "resource"

    invoke-virtual {v0, v1}, Lcom/miui/internal/search/RankedResult;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    aget-object v1, v6, v1

    check-cast v1, Ljava/lang/String;

    const-string/jumbo v2, "intent"

    invoke-virtual {v0, v2}, Lcom/miui/internal/search/RankedResult;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    aget-object v2, v6, v2

    check-cast v2, Landroid/content/Intent;

    invoke-static {v4, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v8, :cond_1

    invoke-static {v2, v3}, Lcom/android/settings/search/SearchResult;->checkIntentEqual(Landroid/content/Intent;Landroid/content/Intent;)Z

    move-result v8

    if-eqz v8, :cond_1

    move-object v2, v3

    move-object v1, v4

    :goto_1
    add-int/lit8 v3, v5, 0x1

    move v5, v3

    move-object v4, v1

    move-object v3, v2

    goto :goto_0

    :cond_1
    new-instance v4, Lcom/android/settings/search/SearchResultItem;

    const/4 v3, 0x0

    invoke-direct {v4, v3}, Lcom/android/settings/search/SearchResultItem;-><init>(I)V

    const-string/jumbo v3, "package"

    invoke-virtual {v0, v3}, Lcom/miui/internal/search/RankedResult;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    aget-object v3, v6, v3

    check-cast v3, Ljava/lang/String;

    iput-object v3, v4, Lcom/android/settings/search/SearchResultItem;->pkg:Ljava/lang/String;

    iput-object v1, v4, Lcom/android/settings/search/SearchResultItem;->resource:Ljava/lang/String;

    const-string/jumbo v3, "title"

    invoke-virtual {v0, v3}, Lcom/miui/internal/search/RankedResult;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    aget-object v3, v6, v3

    check-cast v3, Ljava/lang/String;

    iput-object v3, v4, Lcom/android/settings/search/SearchResultItem;->title:Ljava/lang/String;

    const-string/jumbo v3, "category"

    invoke-virtual {v0, v3}, Lcom/miui/internal/search/RankedResult;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    aget-object v3, v6, v3

    check-cast v3, Ljava/lang/String;

    iput-object v3, v4, Lcom/android/settings/search/SearchResultItem;->category:Ljava/lang/String;

    const-string/jumbo v3, "path"

    invoke-virtual {v0, v3}, Lcom/miui/internal/search/RankedResult;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    aget-object v3, v6, v3

    check-cast v3, Ljava/lang/String;

    iput-object v3, v4, Lcom/android/settings/search/SearchResultItem;->path:Ljava/lang/String;

    const-string/jumbo v3, "keywords"

    invoke-virtual {v0, v3}, Lcom/miui/internal/search/RankedResult;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    aget-object v3, v6, v3

    check-cast v3, Ljava/lang/String;

    iput-object v3, v4, Lcom/android/settings/search/SearchResultItem;->keywords:Ljava/lang/String;

    const-string/jumbo v3, "summary"

    invoke-virtual {v0, v3}, Lcom/miui/internal/search/RankedResult;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    aget-object v3, v6, v3

    check-cast v3, Ljava/lang/String;

    iput-object v3, v4, Lcom/android/settings/search/SearchResultItem;->summary:Ljava/lang/String;

    const-string/jumbo v3, "icon"

    invoke-virtual {v0, v3}, Lcom/miui/internal/search/RankedResult;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    aget-object v3, v6, v3

    check-cast v3, Ljava/lang/String;

    iput-object v3, v4, Lcom/android/settings/search/SearchResultItem;->icon:Ljava/lang/String;

    const-string/jumbo v3, "is_checkbox"

    invoke-virtual {v0, v3}, Lcom/miui/internal/search/RankedResult;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    aget-object v3, v6, v3

    check-cast v3, Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v3

    iput-boolean v3, v4, Lcom/android/settings/search/SearchResultItem;->checkbox:Z

    iput-object v2, v4, Lcom/android/settings/search/SearchResultItem;->intent:Landroid/content/Intent;

    const-string/jumbo v3, "status"

    invoke-virtual {v0, v3}, Lcom/miui/internal/search/RankedResult;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    aget-object v3, v6, v3

    check-cast v3, Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    iput v3, v4, Lcom/android/settings/search/SearchResultItem;->status:I

    invoke-virtual {v7, v4}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1

    :catch_0
    move-exception v0

    :goto_2
    return-object v7

    :cond_2
    invoke-virtual {v7}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_3

    sget-object v0, Lcom/android/settings/search/SearchResultItem;->EMPTY:Lcom/android/settings/search/SearchResultItem;

    invoke-virtual {v7, v0}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/NoSuchFieldException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1

    :cond_3
    return-object v7

    :catch_1
    move-exception v0

    goto :goto_2
.end method
