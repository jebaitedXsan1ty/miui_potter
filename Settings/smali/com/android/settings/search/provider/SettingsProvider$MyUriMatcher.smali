.class Lcom/android/settings/search/provider/SettingsProvider$MyUriMatcher;
.super Landroid/content/UriMatcher;
.source "SettingsProvider.java"


# static fields
.field private static final FUNCTION:I


# direct methods
.method constructor <init>()V
    .locals 3

    const/4 v2, 0x0

    const/4 v0, -0x1

    invoke-direct {p0, v0}, Landroid/content/UriMatcher;-><init>(I)V

    const-string/jumbo v0, "com.miui.settings"

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1, v2}, Lcom/android/settings/search/provider/SettingsProvider$MyUriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    const-string/jumbo v0, "com.miui.settings"

    const-string/jumbo v1, "*"

    invoke-virtual {p0, v0, v1, v2}, Lcom/android/settings/search/provider/SettingsProvider$MyUriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    return-void
.end method


# virtual methods
.method public match(Landroid/net/Uri;)I
    .locals 3

    invoke-super {p0, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Unknown URL: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    return v0
.end method
