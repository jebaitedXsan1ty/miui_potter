.class public Lcom/android/settings/search/SearchResultItem;
.super Ljava/lang/Object;
.source "SearchResultItem.java"


# static fields
.field public static final EMPTY:Lcom/android/settings/search/SearchResultItem;

.field public static final SEARCH_EMPTY:I = 0x1

.field public static final SEARCH_ITEM_NORMAL:I


# instance fields
.field public category:Ljava/lang/String;

.field public checkbox:Z

.field public icon:Ljava/lang/String;

.field public intent:Landroid/content/Intent;

.field public keywords:Ljava/lang/String;

.field public path:Ljava/lang/String;

.field public pkg:Ljava/lang/String;

.field public resource:Ljava/lang/String;

.field public status:I

.field public summary:Ljava/lang/String;

.field public title:Ljava/lang/String;

.field public final type:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/android/settings/search/SearchResultItem;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/android/settings/search/SearchResultItem;-><init>(I)V

    sput-object v0, Lcom/android/settings/search/SearchResultItem;->EMPTY:Lcom/android/settings/search/SearchResultItem;

    return-void
.end method

.method public constructor <init>(I)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/android/settings/search/SearchResultItem;->type:I

    return-void
.end method
