.class public Lcom/android/settings/aM;
.super Landroid/support/v7/preference/y;
.source "SettingsPreferenceFragment.java"


# instance fields
.field private bGC:I

.field initialHighlightedPosition:I


# direct methods
.method public constructor <init>(Landroid/support/v7/preference/PreferenceGroup;)V
    .locals 1

    const/4 v0, -0x1

    invoke-direct {p0, p1}, Landroid/support/v7/preference/y;-><init>(Landroid/support/v7/preference/PreferenceGroup;)V

    iput v0, p0, Lcom/android/settings/aM;->initialHighlightedPosition:I

    iput v0, p0, Lcom/android/settings/aM;->bGC:I

    return-void
.end method


# virtual methods
.method public bwK(I)V
    .locals 0

    iput p1, p0, Lcom/android/settings/aM;->bGC:I

    iput p1, p0, Lcom/android/settings/aM;->initialHighlightedPosition:I

    invoke-virtual {p0}, Lcom/android/settings/aM;->notifyDataSetChanged()V

    return-void
.end method

.method public bwL(Landroid/support/v7/preference/l;I)V
    .locals 2

    invoke-super {p0, p1, p2}, Landroid/support/v7/preference/y;->bwL(Landroid/support/v7/preference/l;I)V

    iget v0, p0, Lcom/android/settings/aM;->bGC:I

    if-ne p2, v0, :cond_0

    iget-object v0, p1, Landroid/support/v7/preference/l;->itemView:Landroid/view/View;

    new-instance v1, Lcom/android/settings/-$Lambda$pSE5ptIY0EUdsHkLFEZXOl6eDZI;

    invoke-direct {v1, p0, v0}, Lcom/android/settings/-$Lambda$pSE5ptIY0EUdsHkLFEZXOl6eDZI;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->post(Ljava/lang/Runnable;)Z

    :cond_0
    return-void
.end method

.method synthetic bwM(Landroid/view/View;)V
    .locals 3

    invoke-virtual {p1}, Landroid/view/View;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    invoke-virtual {p1}, Landroid/view/View;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    int-to-float v0, v0

    int-to-float v1, v1

    invoke-virtual {v2, v0, v1}, Landroid/graphics/drawable/Drawable;->setHotspot(FF)V

    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Landroid/view/View;->setPressed(Z)V

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/view/View;->setPressed(Z)V

    const/4 v0, -0x1

    iput v0, p0, Lcom/android/settings/aM;->bGC:I

    return-void
.end method
