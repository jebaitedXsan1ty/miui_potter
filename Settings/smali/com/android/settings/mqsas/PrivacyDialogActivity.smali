.class public Lcom/android/settings/mqsas/PrivacyDialogActivity;
.super Landroid/app/Activity;
.source "PrivacyDialogActivity.java"


# instance fields
.field private bqm:Lmiui/app/AlertDialog;

.field private bqn:Ljava/lang/String;

.field private bqo:I


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method

.method static synthetic bdL(Lcom/android/settings/mqsas/PrivacyDialogActivity;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/mqsas/PrivacyDialogActivity;->bqn:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic bdM(Lcom/android/settings/mqsas/PrivacyDialogActivity;)I
    .locals 1

    iget v0, p0, Lcom/android/settings/mqsas/PrivacyDialogActivity;->bqo:I

    return v0
.end method


# virtual methods
.method public bdK()V
    .locals 3

    new-instance v0, Lmiui/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Lmiui/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v1, 0x7f120ac5

    invoke-virtual {v0, v1}, Lmiui/app/AlertDialog$Builder;->setTitle(I)Lmiui/app/AlertDialog$Builder;

    const v1, 0x7f120ac4

    invoke-virtual {v0, v1}, Lmiui/app/AlertDialog$Builder;->setMessage(I)Lmiui/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/android/settings/mqsas/PrivacyDialogActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f120ac1

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lmiui/app/AlertDialog$Builder;->setCheckBox(ZLjava/lang/CharSequence;)Lmiui/app/AlertDialog$Builder;

    new-instance v1, Lcom/android/settings/mqsas/a;

    invoke-direct {v1, p0}, Lcom/android/settings/mqsas/a;-><init>(Lcom/android/settings/mqsas/PrivacyDialogActivity;)V

    invoke-virtual {v0, v1}, Lmiui/app/AlertDialog$Builder;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)Lmiui/app/AlertDialog$Builder;

    new-instance v1, Lcom/android/settings/mqsas/b;

    invoke-direct {v1, p0}, Lcom/android/settings/mqsas/b;-><init>(Lcom/android/settings/mqsas/PrivacyDialogActivity;)V

    invoke-virtual {v0, v1}, Lmiui/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Lmiui/app/AlertDialog$Builder;

    new-instance v1, Lcom/android/settings/mqsas/c;

    invoke-direct {v1, p0}, Lcom/android/settings/mqsas/c;-><init>(Lcom/android/settings/mqsas/PrivacyDialogActivity;)V

    const v2, 0x7f120ac2

    invoke-virtual {v0, v2, v1}, Lmiui/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Lmiui/app/AlertDialog$Builder;

    new-instance v1, Lcom/android/settings/mqsas/d;

    invoke-direct {v1, p0}, Lcom/android/settings/mqsas/d;-><init>(Lcom/android/settings/mqsas/PrivacyDialogActivity;)V

    const v2, 0x7f120ac3

    invoke-virtual {v0, v2, v1}, Lmiui/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Lmiui/app/AlertDialog$Builder;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lmiui/app/AlertDialog$Builder;->setCancelable(Z)Lmiui/app/AlertDialog$Builder;

    invoke-virtual {v0}, Lmiui/app/AlertDialog$Builder;->create()Lmiui/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/mqsas/PrivacyDialogActivity;->bqm:Lmiui/app/AlertDialog;

    iget-object v0, p0, Lcom/android/settings/mqsas/PrivacyDialogActivity;->bqm:Lmiui/app/AlertDialog;

    invoke-virtual {v0}, Lmiui/app/AlertDialog;->show()V

    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    const v0, 0x7f0d016b

    invoke-virtual {p0, v0}, Lcom/android/settings/mqsas/PrivacyDialogActivity;->setContentView(I)V

    invoke-virtual {p0}, Lcom/android/settings/mqsas/PrivacyDialogActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "type"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lcom/android/settings/mqsas/PrivacyDialogActivity;->bqo:I

    const-string/jumbo v1, "dgt"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/mqsas/PrivacyDialogActivity;->bqn:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/android/settings/mqsas/PrivacyDialogActivity;->bdK()V

    return-void
.end method

.method protected onDestroy()V
    .locals 1

    iget-object v0, p0, Lcom/android/settings/mqsas/PrivacyDialogActivity;->bqm:Lmiui/app/AlertDialog;

    invoke-virtual {v0}, Lmiui/app/AlertDialog;->dismiss()V

    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    return-void
.end method
