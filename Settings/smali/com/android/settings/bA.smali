.class public abstract Lcom/android/settings/bA;
.super Ljava/lang/Object;
.source "BaseSettingsController.java"


# instance fields
.field protected bRZ:Landroid/widget/TextView;

.field protected bSa:Lcom/android/settings/bB;

.field protected mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/widget/TextView;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/android/settings/bA;->mContext:Landroid/content/Context;

    iput-object p2, p0, Lcom/android/settings/bA;->bRZ:Landroid/widget/TextView;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/settings/bA;->bSa:Lcom/android/settings/bB;

    return-void
.end method


# virtual methods
.method public bKU(Lcom/android/settings/bB;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/bA;->bSa:Lcom/android/settings/bB;

    return-void
.end method

.method public bKV(Landroid/widget/TextView;)V
    .locals 2

    const/4 v1, 0x0

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/android/settings/bA;->bRZ:Landroid/widget/TextView;

    if-eq v0, p1, :cond_0

    invoke-virtual {p1}, Landroid/widget/TextView;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settings/bA;

    if-eqz v0, :cond_0

    invoke-virtual {v0, v1}, Lcom/android/settings/bA;->bKV(Landroid/widget/TextView;)V

    :cond_0
    iput-object p1, p0, Lcom/android/settings/bA;->bRZ:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/android/settings/bA;->wv()V

    return-void
.end method

.method public abstract pause()V
.end method

.method public abstract wt()V
.end method

.method protected abstract wv()V
.end method
