.class Lcom/android/settings/aQ;
.super Landroid/os/Handler;
.source "SettingsFragment.java"


# instance fields
.field final synthetic bHf:Lcom/android/settings/SettingsFragment;


# direct methods
.method constructor <init>(Lcom/android/settings/SettingsFragment;Landroid/os/Looper;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/aQ;->bHf:Lcom/android/settings/SettingsFragment;

    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 5

    const/4 v4, 0x0

    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    :cond_0
    :goto_0
    return-void

    :pswitch_0
    iget-object v0, p0, Lcom/android/settings/aQ;->bHf:Lcom/android/settings/SettingsFragment;

    invoke-virtual {v0}, Lcom/android/settings/SettingsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/aQ;->bHf:Lcom/android/settings/SettingsFragment;

    invoke-static {v0}, Lcom/android/settings/SettingsFragment;->bxy(Lcom/android/settings/SettingsFragment;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/aQ;->bHf:Lcom/android/settings/SettingsFragment;

    invoke-virtual {v0}, Lcom/android/settings/SettingsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v1, ""

    invoke-static {v1}, Lcom/android/settings/search/provider/SettingsProvider;->getSearchUri(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    const-string/jumbo v2, "load"

    const-string/jumbo v3, ""

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/content/ContentResolver;->call(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;

    iget-object v0, p0, Lcom/android/settings/aQ;->bHf:Lcom/android/settings/SettingsFragment;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/android/settings/SettingsFragment;->bxN(Lcom/android/settings/SettingsFragment;Z)Z

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Lcom/android/settings/aQ;->bHf:Lcom/android/settings/SettingsFragment;

    invoke-static {v0}, Lcom/android/settings/SettingsFragment;->bxx(Lcom/android/settings/SettingsFragment;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/settings/aQ;->bHf:Lcom/android/settings/SettingsFragment;

    invoke-static {v0}, Lcom/android/settings/SettingsFragment;->bxG(Lcom/android/settings/SettingsFragment;)Lcom/android/settings/search/SearchResult;

    move-result-object v0

    if-nez v0, :cond_2

    :cond_1
    return-void

    :cond_2
    iget-object v0, p0, Lcom/android/settings/aQ;->bHf:Lcom/android/settings/SettingsFragment;

    invoke-static {v0}, Lcom/android/settings/SettingsFragment;->bxA(Lcom/android/settings/SettingsFragment;)Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lcom/android/settings/gl;

    invoke-direct {v1, p0}, Lcom/android/settings/gl;-><init>(Lcom/android/settings/aQ;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    iget-object v1, p0, Lcom/android/settings/aQ;->bHf:Lcom/android/settings/SettingsFragment;

    invoke-static {v1}, Lcom/android/settings/SettingsFragment;->bxG(Lcom/android/settings/SettingsFragment;)Lcom/android/settings/search/SearchResult;

    move-result-object v1

    iget-object v2, p0, Lcom/android/settings/aQ;->bHf:Lcom/android/settings/SettingsFragment;

    invoke-virtual {v2}, Lcom/android/settings/SettingsFragment;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Lcom/android/settings/search/SearchResult;->getSearchResultList(Landroid/content/Context;Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/aQ;->bHf:Lcom/android/settings/SettingsFragment;

    invoke-static {v1}, Lcom/android/settings/SettingsFragment;->bxA(Lcom/android/settings/SettingsFragment;)Landroid/os/Handler;

    move-result-object v1

    new-instance v2, Lcom/android/settings/gm;

    invoke-direct {v2, p0, v0}, Lcom/android/settings/gm;-><init>(Lcom/android/settings/aQ;Ljava/util/List;)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
