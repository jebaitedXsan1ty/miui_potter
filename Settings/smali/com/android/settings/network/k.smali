.class public Lcom/android/settings/network/k;
.super Lcom/android/settings/core/c;
.source "TetherPreferenceController.java"

# interfaces
.implements Lcom/android/settings/core/lifecycle/b;
.implements Lcom/android/settings/core/lifecycle/a/b;
.implements Lcom/android/settings/core/lifecycle/a/d;
.implements Lcom/android/settings/core/lifecycle/a/e;


# instance fields
.field private final aYA:Ljava/util/concurrent/atomic/AtomicReference;

.field private final aYB:Landroid/bluetooth/BluetoothProfile$ServiceListener;

.field private final aYC:Landroid/net/ConnectivityManager;

.field private aYD:Landroid/support/v7/preference/Preference;

.field private aYE:Lcom/android/settings/network/TetherPreferenceController$TetherBroadcastReceiver;

.field private final aYx:Z

.field private aYy:Lcom/android/settings/network/l;

.field private final aYz:Landroid/bluetooth/BluetoothAdapter;


# direct methods
.method constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0, v1}, Lcom/android/settings/core/c;-><init>(Landroid/content/Context;)V

    new-instance v0, Lcom/android/settings/network/s;

    invoke-direct {v0, p0}, Lcom/android/settings/network/s;-><init>(Lcom/android/settings/network/k;)V

    iput-object v0, p0, Lcom/android/settings/network/k;->aYB:Landroid/bluetooth/BluetoothProfile$ServiceListener;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/settings/network/k;->aYx:Z

    new-instance v0, Ljava/util/concurrent/atomic/AtomicReference;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicReference;-><init>()V

    iput-object v0, p0, Lcom/android/settings/network/k;->aYA:Ljava/util/concurrent/atomic/AtomicReference;

    iput-object v1, p0, Lcom/android/settings/network/k;->aYC:Landroid/net/ConnectivityManager;

    invoke-static {}, Landroid/bluetooth/BluetoothAdapter;->getDefaultAdapter()Landroid/bluetooth/BluetoothAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/network/k;->aYz:Landroid/bluetooth/BluetoothAdapter;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/android/settings/core/lifecycle/a;)V
    .locals 3

    invoke-direct {p0, p1}, Lcom/android/settings/core/c;-><init>(Landroid/content/Context;)V

    new-instance v0, Lcom/android/settings/network/s;

    invoke-direct {v0, p0}, Lcom/android/settings/network/s;-><init>(Lcom/android/settings/network/k;)V

    iput-object v0, p0, Lcom/android/settings/network/k;->aYB:Landroid/bluetooth/BluetoothProfile$ServiceListener;

    new-instance v0, Ljava/util/concurrent/atomic/AtomicReference;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicReference;-><init>()V

    iput-object v0, p0, Lcom/android/settings/network/k;->aYA:Ljava/util/concurrent/atomic/AtomicReference;

    const-string/jumbo v0, "no_config_tethering"

    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v1

    invoke-static {p1, v0, v1}, Lcom/android/settingslib/w;->crb(Landroid/content/Context;Ljava/lang/String;I)Lcom/android/settingslib/n;

    move-result-object v0

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/android/settings/network/k;->aYx:Z

    const-string/jumbo v0, "connectivity"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    iput-object v0, p0, Lcom/android/settings/network/k;->aYC:Landroid/net/ConnectivityManager;

    invoke-static {}, Landroid/bluetooth/BluetoothAdapter;->getDefaultAdapter()Landroid/bluetooth/BluetoothAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/network/k;->aYz:Landroid/bluetooth/BluetoothAdapter;

    if-eqz p2, :cond_0

    invoke-virtual {p2, p0}, Lcom/android/settings/core/lifecycle/a;->ajt(Lcom/android/settings/core/lifecycle/b;)Lcom/android/settings/core/lifecycle/b;

    :cond_0
    iget-object v0, p0, Lcom/android/settings/network/k;->aYz:Landroid/bluetooth/BluetoothAdapter;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/settings/network/k;->aYz:Landroid/bluetooth/BluetoothAdapter;

    iget-object v1, p0, Lcom/android/settings/network/k;->aYB:Landroid/bluetooth/BluetoothProfile$ServiceListener;

    const/4 v2, 0x5

    invoke-virtual {v0, p1, v1, v2}, Landroid/bluetooth/BluetoothAdapter;->getProfileProxy(Landroid/content/Context;Landroid/bluetooth/BluetoothProfile$ServiceListener;I)Z

    :cond_1
    return-void

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private aMH()V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/network/k;->aYD:Landroid/support/v7/preference/Preference;

    if-nez v0, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/settings/network/k;->aYD:Landroid/support/v7/preference/Preference;

    const v1, 0x7f12120f

    invoke-virtual {v0, v1}, Landroid/support/v7/preference/Preference;->dks(I)V

    return-void
.end method

.method static synthetic aMI(Lcom/android/settings/network/k;)Ljava/util/concurrent/atomic/AtomicReference;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/network/k;->aYA:Ljava/util/concurrent/atomic/AtomicReference;

    return-object v0
.end method

.method static synthetic aMJ(Lcom/android/settings/network/k;)Landroid/content/Context;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/network/k;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic aMK(Lcom/android/settings/network/k;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/network/k;->aMH()V

    return-void
.end method


# virtual methods
.method public a(Landroid/support/v7/preference/PreferenceScreen;)V
    .locals 2

    invoke-super {p0, p1}, Lcom/android/settings/core/c;->a(Landroid/support/v7/preference/PreferenceScreen;)V

    const-string/jumbo v0, "tether_settings"

    invoke-virtual {p1, v0}, Landroid/support/v7/preference/PreferenceScreen;->dlg(Ljava/lang/CharSequence;)Landroid/support/v7/preference/Preference;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/network/k;->aYD:Landroid/support/v7/preference/Preference;

    iget-object v0, p0, Lcom/android/settings/network/k;->aYD:Landroid/support/v7/preference/Preference;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/android/settings/network/k;->aYx:Z

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/network/k;->aYD:Landroid/support/v7/preference/Preference;

    iget-object v1, p0, Lcom/android/settings/network/k;->aYC:Landroid/net/ConnectivityManager;

    invoke-static {v1}, Lcom/android/settingslib/v;->cqD(Landroid/net/ConnectivityManager;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/support/v7/preference/Preference;->setTitle(I)V

    iget-object v0, p0, Lcom/android/settings/network/k;->aYD:Landroid/support/v7/preference/Preference;

    iget-object v1, p0, Lcom/android/settings/network/k;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/android/settings/TetherSettings;->bBm(Landroid/content/Context;)Z

    move-result v1

    xor-int/lit8 v1, v1, 0x1

    invoke-virtual {v0, v1}, Landroid/support/v7/preference/Preference;->setEnabled(Z)V

    :cond_0
    return-void
.end method

.method public b()Ljava/lang/String;
    .locals 1

    const-string/jumbo v0, "tether_settings"

    return-object v0
.end method

.method public c()Z
    .locals 3

    iget-object v0, p0, Lcom/android/settings/network/k;->aYC:Landroid/net/ConnectivityManager;

    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->isTetheringSupported()Z

    move-result v0

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/android/settings/network/k;->aYx:Z

    xor-int/lit8 v0, v0, 0x1

    if-nez v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/android/settings/network/k;->mContext:Landroid/content/Context;

    const-string/jumbo v1, "no_config_tethering"

    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v2

    invoke-static {v0, v1, v2}, Lcom/android/settingslib/w;->crn(Landroid/content/Context;Ljava/lang/String;I)Z

    move-result v0

    :goto_0
    xor-int/lit8 v0, v0, 0x1

    return v0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public onDestroy()V
    .locals 3

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/android/settings/network/k;->aYA:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicReference;->getAndSet(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/bluetooth/BluetoothProfile;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/android/settings/network/k;->aYz:Landroid/bluetooth/BluetoothAdapter;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/settings/network/k;->aYz:Landroid/bluetooth/BluetoothAdapter;

    const/4 v2, 0x5

    invoke-virtual {v1, v2, v0}, Landroid/bluetooth/BluetoothAdapter;->closeProfileProxy(ILandroid/bluetooth/BluetoothProfile;)V

    :cond_0
    return-void
.end method

.method public onPause()V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/network/k;->aYy:Lcom/android/settings/network/l;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/network/k;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/network/k;->aYy:Lcom/android/settings/network/l;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    :cond_0
    iget-object v0, p0, Lcom/android/settings/network/k;->aYE:Lcom/android/settings/network/TetherPreferenceController$TetherBroadcastReceiver;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/settings/network/k;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/android/settings/network/k;->aYE:Lcom/android/settings/network/TetherPreferenceController$TetherBroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    :cond_1
    return-void
.end method

.method public onResume()V
    .locals 4

    iget-object v0, p0, Lcom/android/settings/network/k;->aYy:Lcom/android/settings/network/l;

    if-nez v0, :cond_0

    new-instance v0, Lcom/android/settings/network/l;

    invoke-direct {v0, p0}, Lcom/android/settings/network/l;-><init>(Lcom/android/settings/network/k;)V

    iput-object v0, p0, Lcom/android/settings/network/k;->aYy:Lcom/android/settings/network/l;

    :cond_0
    iget-object v0, p0, Lcom/android/settings/network/k;->aYE:Lcom/android/settings/network/TetherPreferenceController$TetherBroadcastReceiver;

    if-nez v0, :cond_1

    new-instance v0, Lcom/android/settings/network/TetherPreferenceController$TetherBroadcastReceiver;

    invoke-direct {v0, p0}, Lcom/android/settings/network/TetherPreferenceController$TetherBroadcastReceiver;-><init>(Lcom/android/settings/network/k;)V

    iput-object v0, p0, Lcom/android/settings/network/k;->aYE:Lcom/android/settings/network/TetherPreferenceController$TetherBroadcastReceiver;

    :cond_1
    iget-object v0, p0, Lcom/android/settings/network/k;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/android/settings/network/k;->aYE:Lcom/android/settings/network/TetherPreferenceController$TetherBroadcastReceiver;

    new-instance v2, Landroid/content/IntentFilter;

    const-string/jumbo v3, "android.net.conn.TETHER_STATE_CHANGED"

    invoke-direct {v2, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    iget-object v0, p0, Lcom/android/settings/network/k;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/network/k;->aYy:Lcom/android/settings/network/l;

    iget-object v1, v1, Lcom/android/settings/network/l;->aYF:Landroid/net/Uri;

    iget-object v2, p0, Lcom/android/settings/network/k;->aYy:Lcom/android/settings/network/l;

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v3, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    return-void
.end method

.method updateSummary()V
    .locals 13

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/android/settings/network/k;->aYD:Landroid/support/v7/preference/Preference;

    if-nez v0, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/settings/network/k;->aYC:Landroid/net/ConnectivityManager;

    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getTetheredIfaces()[Ljava/lang/String;

    move-result-object v5

    iget-object v0, p0, Lcom/android/settings/network/k;->aYC:Landroid/net/ConnectivityManager;

    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getTetherableWifiRegexs()[Ljava/lang/String;

    move-result-object v6

    iget-object v0, p0, Lcom/android/settings/network/k;->aYC:Landroid/net/ConnectivityManager;

    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getTetherableBluetoothRegexs()[Ljava/lang/String;

    move-result-object v7

    if-eqz v5, :cond_c

    if-eqz v6, :cond_3

    array-length v8, v5

    move v4, v2

    move v0, v2

    :goto_0
    if-ge v4, v8, :cond_4

    aget-object v9, v5, v4

    array-length v10, v6

    move v3, v2

    :goto_1
    if-ge v3, v10, :cond_1

    aget-object v11, v6, v3

    invoke-virtual {v9, v11}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v11

    if-eqz v11, :cond_2

    move v0, v1

    :cond_1
    add-int/lit8 v3, v4, 0x1

    move v4, v3

    goto :goto_0

    :cond_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :cond_3
    move v0, v2

    :cond_4
    array-length v3, v5

    if-le v3, v1, :cond_6

    move v12, v1

    move v1, v0

    move v0, v12

    :goto_2
    if-nez v0, :cond_8

    if-eqz v7, :cond_8

    array-length v3, v7

    if-lez v3, :cond_8

    iget-object v3, p0, Lcom/android/settings/network/k;->aYz:Landroid/bluetooth/BluetoothAdapter;

    if-eqz v3, :cond_8

    iget-object v3, p0, Lcom/android/settings/network/k;->aYz:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v3}, Landroid/bluetooth/BluetoothAdapter;->getState()I

    move-result v3

    const/16 v4, 0xc

    if-ne v3, v4, :cond_8

    iget-object v0, p0, Lcom/android/settings/network/k;->aYA:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/bluetooth/BluetoothPan;

    if-eqz v0, :cond_5

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothPan;->isTetheringOn()Z

    move-result v2

    :cond_5
    :goto_3
    if-nez v1, :cond_9

    xor-int/lit8 v0, v2, 0x1

    if-eqz v0, :cond_9

    iget-object v0, p0, Lcom/android/settings/network/k;->aYD:Landroid/support/v7/preference/Preference;

    const v1, 0x7f12120f

    invoke-virtual {v0, v1}, Landroid/support/v7/preference/Preference;->dks(I)V

    :goto_4
    return-void

    :cond_6
    array-length v3, v5

    if-ne v3, v1, :cond_7

    xor-int/lit8 v1, v0, 0x1

    move v12, v1

    move v1, v0

    move v0, v12

    goto :goto_2

    :cond_7
    move v1, v0

    move v0, v2

    goto :goto_2

    :cond_8
    move v2, v0

    goto :goto_3

    :cond_9
    if-eqz v1, :cond_a

    if-eqz v2, :cond_a

    iget-object v0, p0, Lcom/android/settings/network/k;->aYD:Landroid/support/v7/preference/Preference;

    const v1, 0x7f12125d

    invoke-virtual {v0, v1}, Landroid/support/v7/preference/Preference;->dks(I)V

    goto :goto_4

    :cond_a
    if-eqz v1, :cond_b

    iget-object v0, p0, Lcom/android/settings/network/k;->aYD:Landroid/support/v7/preference/Preference;

    const v1, 0x7f12125c

    invoke-virtual {v0, v1}, Landroid/support/v7/preference/Preference;->dks(I)V

    goto :goto_4

    :cond_b
    iget-object v0, p0, Lcom/android/settings/network/k;->aYD:Landroid/support/v7/preference/Preference;

    const v1, 0x7f12125b

    invoke-virtual {v0, v1}, Landroid/support/v7/preference/Preference;->dks(I)V

    goto :goto_4

    :cond_c
    move v0, v2

    move v1, v2

    goto :goto_2
.end method
