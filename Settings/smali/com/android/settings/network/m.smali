.class public Lcom/android/settings/network/m;
.super Lcom/android/settings/core/e;
.source "AirplaneModePreferenceController.java"

# interfaces
.implements Lcom/android/settings/core/lifecycle/b;
.implements Lcom/android/settings/core/lifecycle/a/b;
.implements Lcom/android/settings/core/lifecycle/a/d;


# instance fields
.field private aYI:Lcom/android/settings/an;

.field private aYJ:Landroid/preference/CheckBoxPreference;

.field private final aYK:Landroid/app/Fragment;

.field private final mMetricsFeatureProvider:Lcom/android/settings/core/instrumentation/e;

.field private final mPackageManager:Landroid/content/pm/PackageManager;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/app/Fragment;)V
    .locals 1

    invoke-direct {p0, p1}, Lcom/android/settings/core/e;-><init>(Landroid/content/Context;)V

    iput-object p2, p0, Lcom/android/settings/network/m;->aYK:Landroid/app/Fragment;

    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/network/m;->mPackageManager:Landroid/content/pm/PackageManager;

    invoke-static {p1}, Lcom/android/settings/overlay/a;->aIk(Landroid/content/Context;)Lcom/android/settings/overlay/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/settings/overlay/a;->aIm()Lcom/android/settings/core/instrumentation/e;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/network/m;->mMetricsFeatureProvider:Lcom/android/settings/core/instrumentation/e;

    return-void
.end method


# virtual methods
.method public fm(Landroid/preference/Preference;)Z
    .locals 5

    const/4 v4, 0x0

    const/4 v3, 0x1

    const-string/jumbo v0, "toggle_airplane"

    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string/jumbo v0, "ril.cdma.inecmmode"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/settings/network/m;->aYK:Landroid/app/Fragment;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/network/m;->aYK:Landroid/app/Fragment;

    new-instance v1, Landroid/content/Intent;

    const-string/jumbo v2, "com.android.internal.intent.action.ACTION_SHOW_NOTICE_ECM_BLOCK_OTHERS"

    invoke-direct {v1, v2, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    invoke-virtual {v0, v1, v3}, Landroid/app/Fragment;->startActivityForResult(Landroid/content/Intent;I)V

    :cond_0
    return v3

    :cond_1
    const/4 v0, 0x0

    return v0
.end method

.method public i(Landroid/preference/PreferenceScreen;)V
    .locals 4

    invoke-virtual {p0}, Lcom/android/settings/network/m;->p()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/android/settings/network/m;->l()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    iput-object v0, p0, Lcom/android/settings/network/m;->aYJ:Landroid/preference/CheckBoxPreference;

    iget-object v0, p0, Lcom/android/settings/network/m;->aYJ:Landroid/preference/CheckBoxPreference;

    if-eqz v0, :cond_0

    new-instance v0, Lcom/android/settings/an;

    iget-object v1, p0, Lcom/android/settings/network/m;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/android/settings/network/m;->aYJ:Landroid/preference/CheckBoxPreference;

    iget-object v3, p0, Lcom/android/settings/network/m;->mMetricsFeatureProvider:Lcom/android/settings/core/instrumentation/e;

    invoke-direct {v0, v1, v2, v3}, Lcom/android/settings/an;-><init>(Landroid/content/Context;Landroid/preference/CheckBoxPreference;Lcom/android/settings/core/instrumentation/e;)V

    iput-object v0, p0, Lcom/android/settings/network/m;->aYI:Lcom/android/settings/an;

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0}, Lcom/android/settings/network/m;->l()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lcom/android/settings/network/m;->cdK(Landroid/preference/PreferenceScreen;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public l()Ljava/lang/String;
    .locals 1

    const-string/jumbo v0, "toggle_airplane"

    return-object v0
.end method

.method public onPause()V
    .locals 1

    iget-object v0, p0, Lcom/android/settings/network/m;->aYI:Lcom/android/settings/an;

    invoke-virtual {v0}, Lcom/android/settings/an;->pause()V

    return-void
.end method

.method public onResume()V
    .locals 1

    iget-object v0, p0, Lcom/android/settings/network/m;->aYI:Lcom/android/settings/an;

    invoke-virtual {v0}, Lcom/android/settings/an;->bqj()V

    return-void
.end method

.method public p()Z
    .locals 2

    iget-object v0, p0, Lcom/android/settings/network/m;->mPackageManager:Landroid/content/pm/PackageManager;

    const-string/jumbo v1, "android.hardware.type.television"

    invoke-virtual {v0, v1}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    return v0
.end method
