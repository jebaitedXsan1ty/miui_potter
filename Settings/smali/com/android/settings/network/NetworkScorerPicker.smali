.class public Lcom/android/settings/network/NetworkScorerPicker;
.super Lcom/android/settings/core/InstrumentedPreferenceFragment;
.source "NetworkScorerPicker.java"

# interfaces
.implements Lcom/android/settings/widget/L;


# instance fields
.field private aYb:Lcom/android/settings/network/a;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/core/InstrumentedPreferenceFragment;-><init>()V

    return-void
.end method

.method private aMm()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/network/NetworkScorerPicker;->aYb:Lcom/android/settings/network/a;

    invoke-virtual {v0}, Lcom/android/settings/network/a;->aMp()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private aMn(Ljava/lang/String;)Z
    .locals 1

    invoke-direct {p0}, Lcom/android/settings/network/NetworkScorerPicker;->aMm()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/network/NetworkScorerPicker;->aYb:Lcom/android/settings/network/a;

    invoke-virtual {v0, p1}, Lcom/android/settings/network/a;->aMq(Ljava/lang/String;)Z

    move-result v0

    return v0

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method private updateCheckedState(Ljava/lang/String;)V
    .locals 5

    invoke-virtual {p0}, Lcom/android/settings/network/NetworkScorerPicker;->djr()Landroid/support/v7/preference/PreferenceScreen;

    move-result-object v3

    invoke-virtual {v3}, Landroid/support/v7/preference/PreferenceScreen;->dln()I

    move-result v4

    const/4 v0, 0x0

    move v2, v0

    :goto_0
    if-ge v2, v4, :cond_1

    invoke-virtual {v3, v2}, Landroid/support/v7/preference/PreferenceScreen;->dlf(I)Landroid/support/v7/preference/Preference;

    move-result-object v1

    instance-of v0, v1, Lcom/android/settings/widget/RadioButtonPreference;

    if-eqz v0, :cond_0

    move-object v0, v1

    check-cast v0, Lcom/android/settings/widget/RadioButtonPreference;

    invoke-virtual {v1}, Landroid/support/v7/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, p1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/android/settings/widget/RadioButtonPreference;->setChecked(Z)V

    :cond_0
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_1
    return-void
.end method


# virtual methods
.method public EW(Landroid/os/Bundle;Ljava/lang/String;)V
    .locals 1

    invoke-super {p0, p1, p2}, Lcom/android/settings/core/InstrumentedPreferenceFragment;->EW(Landroid/os/Bundle;Ljava/lang/String;)V

    const v0, 0x7f150094

    invoke-virtual {p0, v0}, Lcom/android/settings/network/NetworkScorerPicker;->bwf(I)V

    invoke-virtual {p0}, Lcom/android/settings/network/NetworkScorerPicker;->updateCandidates()V

    return-void
.end method

.method createNetworkScorerManagerWrapper(Landroid/content/Context;)Lcom/android/settings/network/a;
    .locals 2

    new-instance v1, Lcom/android/settings/network/a;

    const-class v0, Landroid/net/NetworkScoreManager;

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/NetworkScoreManager;

    invoke-direct {v1, v0}, Lcom/android/settings/network/a;-><init>(Landroid/net/NetworkScoreManager;)V

    return-object v1
.end method

.method public getMetricsCategory()I
    .locals 1

    const/16 v0, 0x35d

    return v0
.end method

.method public gl(Lcom/android/settings/widget/RadioButtonPreference;)V
    .locals 2

    invoke-virtual {p1}, Lcom/android/settings/widget/RadioButtonPreference;->getKey()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/android/settings/network/NetworkScorerPicker;->aMn(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-direct {p0, v0}, Lcom/android/settings/network/NetworkScorerPicker;->updateCheckedState(Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public onAttach(Landroid/content/Context;)V
    .locals 1

    invoke-super {p0, p1}, Lcom/android/settings/core/InstrumentedPreferenceFragment;->onAttach(Landroid/content/Context;)V

    invoke-virtual {p0, p1}, Lcom/android/settings/network/NetworkScorerPicker;->createNetworkScorerManagerWrapper(Landroid/content/Context;)Lcom/android/settings/network/a;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/network/NetworkScorerPicker;->aYb:Lcom/android/settings/network/a;

    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2

    invoke-super {p0, p1, p2, p3}, Lcom/android/settings/core/InstrumentedPreferenceFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lcom/android/settings/network/NetworkScorerPicker;->setHasOptionsMenu(Z)V

    return-object v0
.end method

.method public updateCandidates()V
    .locals 8

    invoke-virtual {p0}, Lcom/android/settings/network/NetworkScorerPicker;->djr()Landroid/support/v7/preference/PreferenceScreen;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v7/preference/PreferenceScreen;->removeAll()V

    iget-object v0, p0, Lcom/android/settings/network/NetworkScorerPicker;->aYb:Lcom/android/settings/network/a;

    invoke-virtual {v0}, Lcom/android/settings/network/a;->aMo()Ljava/util/List;

    move-result-object v3

    invoke-direct {p0}, Lcom/android/settings/network/NetworkScorerPicker;->aMm()Ljava/lang/String;

    move-result-object v4

    new-instance v0, Lcom/android/settings/widget/RadioButtonPreference;

    invoke-virtual {p0}, Lcom/android/settings/network/NetworkScorerPicker;->aki()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/android/settings/widget/RadioButtonPreference;-><init>(Landroid/content/Context;)V

    const v1, 0x7f120b45

    invoke-virtual {v0, v1}, Lcom/android/settings/widget/RadioButtonPreference;->setTitle(I)V

    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/android/settings/widget/RadioButtonPreference;->setChecked(Z)V

    :goto_0
    invoke-virtual {v2, v0}, Landroid/support/v7/preference/PreferenceScreen;->im(Landroid/support/v7/preference/Preference;)Z

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v5

    const/4 v0, 0x0

    move v1, v0

    :goto_1
    if-ge v1, v5, :cond_1

    new-instance v6, Lcom/android/settings/widget/RadioButtonPreference;

    invoke-virtual {p0}, Lcom/android/settings/network/NetworkScorerPicker;->aki()Landroid/content/Context;

    move-result-object v0

    invoke-direct {v6, v0}, Lcom/android/settings/widget/RadioButtonPreference;-><init>(Landroid/content/Context;)V

    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/NetworkScorerAppData;

    invoke-virtual {v0}, Landroid/net/NetworkScorerAppData;->getRecommendationServicePackageName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0}, Landroid/net/NetworkScorerAppData;->getRecommendationServiceLabel()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v6, v0}, Lcom/android/settings/widget/RadioButtonPreference;->setTitle(Ljava/lang/CharSequence;)V

    invoke-virtual {v6, v7}, Lcom/android/settings/widget/RadioButtonPreference;->setKey(Ljava/lang/String;)V

    invoke-static {v4, v7}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    invoke-virtual {v6, v0}, Lcom/android/settings/widget/RadioButtonPreference;->setChecked(Z)V

    invoke-virtual {v6, p0}, Lcom/android/settings/widget/RadioButtonPreference;->aCX(Lcom/android/settings/widget/L;)V

    invoke-virtual {v2, v6}, Landroid/support/v7/preference/PreferenceScreen;->im(Landroid/support/v7/preference/Preference;)Z

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    :cond_0
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/android/settings/widget/RadioButtonPreference;->setKey(Ljava/lang/String;)V

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/android/settings/widget/RadioButtonPreference;->setChecked(Z)V

    invoke-virtual {v0, p0}, Lcom/android/settings/widget/RadioButtonPreference;->aCX(Lcom/android/settings/widget/L;)V

    goto :goto_0

    :cond_1
    return-void
.end method
