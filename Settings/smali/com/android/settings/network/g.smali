.class public Lcom/android/settings/network/g;
.super Ljava/lang/Object;
.source "NetworkResetActionMenuController.java"


# instance fields
.field private final aYq:Lcom/android/settings/network/b;

.field private final mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/android/settings/network/g;->mContext:Landroid/content/Context;

    new-instance v0, Lcom/android/settings/network/b;

    invoke-direct {v0, p1}, Lcom/android/settings/network/b;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/settings/network/g;->aYq:Lcom/android/settings/network/b;

    return-void
.end method


# virtual methods
.method public aMB(Landroid/view/Menu;)V
    .locals 3

    const/4 v2, 0x0

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/android/settings/network/g;->aMC()Z

    move-result v1

    if-eqz v1, :cond_0

    if-eqz p1, :cond_0

    const/16 v0, 0xc9

    const v1, 0x7f120e1f

    invoke-interface {p1, v2, v0, v2, v1}, Landroid/view/Menu;->add(IIII)Landroid/view/MenuItem;

    move-result-object v0

    :cond_0
    if-eqz v0, :cond_1

    new-instance v1, Lcom/android/settings/network/-$Lambda$HyyBNGjWfkYIaCSFAk3l1ixpFO8;

    invoke-direct {v1, p0}, Lcom/android/settings/network/-$Lambda$HyyBNGjWfkYIaCSFAk3l1ixpFO8;-><init>(Ljava/lang/Object;)V

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    :cond_1
    return-void
.end method

.method aMC()Z
    .locals 1

    iget-object v0, p0, Lcom/android/settings/network/g;->aYq:Lcom/android/settings/network/b;

    invoke-virtual {v0}, Lcom/android/settings/network/b;->aMs()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    return v0
.end method

.method synthetic aMD(Landroid/view/MenuItem;)Z
    .locals 8

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/android/settings/network/g;->mContext:Landroid/content/Context;

    const-class v1, Lcom/android/settings/ResetNetwork;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    const/4 v4, 0x0

    const v5, 0x7f120e1f

    const/16 v7, 0x2ea

    move-object v3, v2

    move-object v6, v2

    invoke-static/range {v0 .. v7}, Lcom/android/settings/aq;->bra(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;Landroid/app/Fragment;IILjava/lang/CharSequence;I)V

    const/4 v0, 0x1

    return v0
.end method
