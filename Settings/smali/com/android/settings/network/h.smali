.class public Lcom/android/settings/network/h;
.super Lcom/android/settings/core/c;
.source "MobileNetworkPreferenceController.java"

# interfaces
.implements Lcom/android/settings/core/lifecycle/b;
.implements Lcom/android/settings/core/lifecycle/a/b;
.implements Lcom/android/settings/core/lifecycle/a/d;


# instance fields
.field private final aYr:Z

.field private final aYs:Landroid/telephony/SubscriptionManager$OnSubscriptionsChangedListener;

.field private aYt:Landroid/support/v7/preference/Preference;

.field private aYu:Landroid/telephony/SubscriptionManager;

.field private final aYv:Landroid/telephony/TelephonyManager;

.field mPhoneStateListener:Landroid/telephony/PhoneStateListener;

.field private final mUserManager:Landroid/os/UserManager;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    invoke-direct {p0, p1}, Lcom/android/settings/core/c;-><init>(Landroid/content/Context;)V

    new-instance v0, Lcom/android/settings/network/q;

    invoke-direct {v0, p0}, Lcom/android/settings/network/q;-><init>(Lcom/android/settings/network/h;)V

    iput-object v0, p0, Lcom/android/settings/network/h;->aYs:Landroid/telephony/SubscriptionManager$OnSubscriptionsChangedListener;

    const-string/jumbo v0, "user"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/UserManager;

    iput-object v0, p0, Lcom/android/settings/network/h;->mUserManager:Landroid/os/UserManager;

    const-string/jumbo v0, "phone"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    iput-object v0, p0, Lcom/android/settings/network/h;->aYv:Landroid/telephony/TelephonyManager;

    iget-object v0, p0, Lcom/android/settings/network/h;->mUserManager:Landroid/os/UserManager;

    invoke-virtual {v0}, Landroid/os/UserManager;->isAdminUser()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    iput-boolean v0, p0, Lcom/android/settings/network/h;->aYr:Z

    invoke-static {p1}, Landroid/telephony/SubscriptionManager;->from(Landroid/content/Context;)Landroid/telephony/SubscriptionManager;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/network/h;->aYu:Landroid/telephony/SubscriptionManager;

    return-void
.end method

.method private aME(I)Z
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/android/settings/network/h;->aYv:Landroid/telephony/TelephonyManager;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/network/h;->aYv:Landroid/telephony/TelephonyManager;

    invoke-virtual {v0, p1}, Landroid/telephony/TelephonyManager;->getServiceStateForSubscriber(I)Landroid/telephony/ServiceState;

    move-result-object v0

    invoke-virtual {v0}, Landroid/telephony/ServiceState;->getState()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    return v0

    :cond_0
    return v1
.end method

.method private aMF()V
    .locals 5

    iget-object v0, p0, Lcom/android/settings/network/h;->aYt:Landroid/support/v7/preference/Preference;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/settings/network/h;->aYu:Landroid/telephony/SubscriptionManager;

    invoke-virtual {v0}, Landroid/telephony/SubscriptionManager;->getActiveSubscriptionInfoList()Ljava/util/List;

    move-result-object v1

    if-eqz v1, :cond_3

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_3

    const/4 v0, 0x0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v1, v0

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/SubscriptionInfo;

    invoke-virtual {v0}, Landroid/telephony/SubscriptionInfo;->getSubscriptionId()I

    move-result v4

    invoke-direct {p0, v4}, Lcom/android/settings/network/h;->aME(I)Z

    move-result v4

    if-eqz v4, :cond_4

    if-eqz v1, :cond_0

    const-string/jumbo v1, ", "

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_0
    iget-object v1, p0, Lcom/android/settings/network/h;->aYv:Landroid/telephony/TelephonyManager;

    invoke-virtual {v0}, Landroid/telephony/SubscriptionInfo;->getSubscriptionId()I

    move-result v0

    invoke-virtual {v1, v0}, Landroid/telephony/TelephonyManager;->getNetworkOperatorName(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/4 v0, 0x1

    :goto_1
    move v1, v0

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/android/settings/network/h;->aYt:Landroid/support/v7/preference/Preference;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v7/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    :cond_2
    :goto_2
    return-void

    :cond_3
    iget-object v0, p0, Lcom/android/settings/network/h;->aYt:Landroid/support/v7/preference/Preference;

    iget-object v1, p0, Lcom/android/settings/network/h;->aYv:Landroid/telephony/TelephonyManager;

    invoke-virtual {v1}, Landroid/telephony/TelephonyManager;->getNetworkOperatorName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v7/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    goto :goto_2

    :cond_4
    move v0, v1

    goto :goto_1
.end method

.method static synthetic aMG(Lcom/android/settings/network/h;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/network/h;->aMF()V

    return-void
.end method


# virtual methods
.method public a(Landroid/support/v7/preference/PreferenceScreen;)V
    .locals 1

    invoke-super {p0, p1}, Lcom/android/settings/core/c;->a(Landroid/support/v7/preference/PreferenceScreen;)V

    invoke-virtual {p0}, Lcom/android/settings/network/h;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/android/settings/network/h;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/support/v7/preference/PreferenceScreen;->dlg(Ljava/lang/CharSequence;)Landroid/support/v7/preference/Preference;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/network/h;->aYt:Landroid/support/v7/preference/Preference;

    :cond_0
    return-void
.end method

.method public b()Ljava/lang/String;
    .locals 1

    const-string/jumbo v0, "mobile_network_settings"

    return-object v0
.end method

.method public c()Z
    .locals 3

    iget-boolean v0, p0, Lcom/android/settings/network/h;->aYr:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/network/h;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/settings/aq;->bqv(Landroid/content/Context;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/network/h;->mContext:Landroid/content/Context;

    const-string/jumbo v1, "no_config_mobile_networks"

    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v2

    invoke-static {v0, v1, v2}, Lcom/android/settingslib/w;->crn(Landroid/content/Context;Ljava/lang/String;I)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public dI(Landroid/support/v7/preference/Preference;)Z
    .locals 4

    const-string/jumbo v0, "mobile_network_settings"

    invoke-virtual {p1}, Landroid/support/v7/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/network/h;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/settings/aq;->brC(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v1, "android.intent.action.MAIN"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    new-instance v1, Landroid/content/ComponentName;

    const-string/jumbo v2, "com.qualcomm.qti.networksetting"

    const-string/jumbo v3, "com.qualcomm.qti.networksetting.MobileNetworkSettings"

    invoke-direct {v1, v2, v3}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    iget-object v1, p0, Lcom/android/settings/network/h;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    const/4 v0, 0x1

    return v0

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public onPause()V
    .locals 3

    iget-object v0, p0, Lcom/android/settings/network/h;->mPhoneStateListener:Landroid/telephony/PhoneStateListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/network/h;->aYv:Landroid/telephony/TelephonyManager;

    iget-object v1, p0, Lcom/android/settings/network/h;->mPhoneStateListener:Landroid/telephony/PhoneStateListener;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/telephony/TelephonyManager;->listen(Landroid/telephony/PhoneStateListener;I)V

    :cond_0
    iget-object v0, p0, Lcom/android/settings/network/h;->aYu:Landroid/telephony/SubscriptionManager;

    iget-object v1, p0, Lcom/android/settings/network/h;->aYs:Landroid/telephony/SubscriptionManager$OnSubscriptionsChangedListener;

    invoke-virtual {v0, v1}, Landroid/telephony/SubscriptionManager;->removeOnSubscriptionsChangedListener(Landroid/telephony/SubscriptionManager$OnSubscriptionsChangedListener;)V

    return-void
.end method

.method public onResume()V
    .locals 3

    iget-object v0, p0, Lcom/android/settings/network/h;->aYu:Landroid/telephony/SubscriptionManager;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/network/h;->aYu:Landroid/telephony/SubscriptionManager;

    iget-object v1, p0, Lcom/android/settings/network/h;->aYs:Landroid/telephony/SubscriptionManager$OnSubscriptionsChangedListener;

    invoke-virtual {v0, v1}, Landroid/telephony/SubscriptionManager;->addOnSubscriptionsChangedListener(Landroid/telephony/SubscriptionManager$OnSubscriptionsChangedListener;)V

    :cond_0
    invoke-virtual {p0}, Lcom/android/settings/network/h;->c()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/settings/network/h;->mPhoneStateListener:Landroid/telephony/PhoneStateListener;

    if-nez v0, :cond_1

    new-instance v0, Lcom/android/settings/network/r;

    invoke-direct {v0, p0}, Lcom/android/settings/network/r;-><init>(Lcom/android/settings/network/h;)V

    iput-object v0, p0, Lcom/android/settings/network/h;->mPhoneStateListener:Landroid/telephony/PhoneStateListener;

    :cond_1
    iget-object v0, p0, Lcom/android/settings/network/h;->aYv:Landroid/telephony/TelephonyManager;

    iget-object v1, p0, Lcom/android/settings/network/h;->mPhoneStateListener:Landroid/telephony/PhoneStateListener;

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/telephony/TelephonyManager;->listen(Landroid/telephony/PhoneStateListener;I)V

    :cond_2
    return-void
.end method
