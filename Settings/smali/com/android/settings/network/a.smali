.class public Lcom/android/settings/network/a;
.super Ljava/lang/Object;
.source "NetworkScoreManagerWrapper.java"


# instance fields
.field private final aYc:Landroid/net/NetworkScoreManager;


# direct methods
.method public constructor <init>(Landroid/net/NetworkScoreManager;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/android/settings/network/a;->aYc:Landroid/net/NetworkScoreManager;

    return-void
.end method


# virtual methods
.method public aMo()Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/network/a;->aYc:Landroid/net/NetworkScoreManager;

    invoke-virtual {v0}, Landroid/net/NetworkScoreManager;->getAllValidScorers()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public aMp()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/network/a;->aYc:Landroid/net/NetworkScoreManager;

    invoke-virtual {v0}, Landroid/net/NetworkScoreManager;->getActiveScorerPackage()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public aMq(Ljava/lang/String;)Z
    .locals 1

    iget-object v0, p0, Lcom/android/settings/network/a;->aYc:Landroid/net/NetworkScoreManager;

    invoke-virtual {v0, p1}, Landroid/net/NetworkScoreManager;->setActiveScorer(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public aMr()Landroid/net/NetworkScorerAppData;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/network/a;->aYc:Landroid/net/NetworkScoreManager;

    invoke-virtual {v0}, Landroid/net/NetworkScoreManager;->getActiveScorer()Landroid/net/NetworkScorerAppData;

    move-result-object v0

    return-object v0
.end method
