.class public Lcom/android/settings/network/NetworkDashboardFragment;
.super Lcom/android/settings/dashboard/DashboardFragment;
.source "NetworkDashboardFragment.java"

# interfaces
.implements Lcom/android/settings/network/f;


# static fields
.field public static final SEARCH_INDEX_DATA_PROVIDER:Lcom/android/settings/search/Indexable$SearchIndexProvider;


# instance fields
.field private aYe:Lcom/android/settings/network/g;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/android/settings/network/o;

    invoke-direct {v0}, Lcom/android/settings/network/o;-><init>()V

    sput-object v0, Lcom/android/settings/network/NetworkDashboardFragment;->SEARCH_INDEX_DATA_PROVIDER:Lcom/android/settings/search/Indexable$SearchIndexProvider;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/dashboard/DashboardFragment;-><init>()V

    return-void
.end method

.method static synthetic aMv(Lcom/android/settings/network/e;Landroid/content/DialogInterface;I)V
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/android/settings/network/e;->aMz(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method protected EU()Ljava/lang/String;
    .locals 1

    const-string/jumbo v0, "NetworkDashboardFrag"

    return-object v0
.end method

.method protected EV()I
    .locals 1

    const v0, 0x7f150093

    return v0
.end method

.method public Lx(I)I
    .locals 1

    const/4 v0, 0x1

    if-ne v0, p1, :cond_0

    const/16 v0, 0x261

    return v0

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public aMu()V
    .locals 1

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/android/settings/network/NetworkDashboardFragment;->bwg(I)V

    return-void
.end method

.method public getMetricsCategory()I
    .locals 1

    const/16 v0, 0x2ea

    return v0
.end method

.method protected getPreferenceControllers(Landroid/content/Context;)Ljava/util/List;
    .locals 6

    new-instance v0, Lcom/android/settings/network/m;

    invoke-direct {v0, p1, p0}, Lcom/android/settings/network/m;-><init>(Landroid/content/Context;Landroid/app/Fragment;)V

    new-instance v1, Lcom/android/settings/network/e;

    invoke-direct {v1, p1, p0}, Lcom/android/settings/network/e;-><init>(Landroid/content/Context;Lcom/android/settings/network/f;)V

    new-instance v2, Lcom/android/settings/wifi/Q;

    iget-object v3, p0, Lcom/android/settings/network/NetworkDashboardFragment;->mMetricsFeatureProvider:Lcom/android/settings/core/instrumentation/e;

    invoke-direct {v2, p1, v3}, Lcom/android/settings/wifi/Q;-><init>(Landroid/content/Context;Lcom/android/settings/core/instrumentation/e;)V

    new-instance v3, Lcom/android/settings/network/h;

    invoke-direct {v3, p1}, Lcom/android/settings/network/h;-><init>(Landroid/content/Context;)V

    new-instance v4, Lcom/android/settings/network/d;

    invoke-direct {v4, p1}, Lcom/android/settings/network/d;-><init>(Landroid/content/Context;)V

    invoke-virtual {p0}, Lcom/android/settings/network/NetworkDashboardFragment;->getLifecycle()Lcom/android/settings/core/lifecycle/a;

    move-result-object v5

    invoke-virtual {v5, v0}, Lcom/android/settings/core/lifecycle/a;->ajt(Lcom/android/settings/core/lifecycle/b;)Lcom/android/settings/core/lifecycle/b;

    invoke-virtual {v5, v1}, Lcom/android/settings/core/lifecycle/a;->ajt(Lcom/android/settings/core/lifecycle/b;)Lcom/android/settings/core/lifecycle/b;

    invoke-virtual {v5, v2}, Lcom/android/settings/core/lifecycle/a;->ajt(Lcom/android/settings/core/lifecycle/b;)Lcom/android/settings/core/lifecycle/b;

    invoke-virtual {v5, v3}, Lcom/android/settings/core/lifecycle/a;->ajt(Lcom/android/settings/core/lifecycle/b;)Lcom/android/settings/core/lifecycle/b;

    invoke-virtual {v5, v4}, Lcom/android/settings/core/lifecycle/a;->ajt(Lcom/android/settings/core/lifecycle/b;)Lcom/android/settings/core/lifecycle/b;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v3, Lcom/android/settings/network/k;

    invoke-direct {v3, p1, v5}, Lcom/android/settings/network/k;-><init>(Landroid/content/Context;Lcom/android/settings/core/lifecycle/a;)V

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v3, Lcom/android/settings/network/j;

    invoke-direct {v3, p1}, Lcom/android/settings/network/j;-><init>(Landroid/content/Context;)V

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object v0
.end method

.method public onAttach(Landroid/content/Context;)V
    .locals 1

    invoke-super {p0, p1}, Lcom/android/settings/dashboard/DashboardFragment;->onAttach(Landroid/content/Context;)V

    new-instance v0, Lcom/android/settings/network/g;

    invoke-direct {v0, p1}, Lcom/android/settings/network/g;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/settings/network/NetworkDashboardFragment;->aYe:Lcom/android/settings/network/g;

    return-void
.end method

.method public onCreateDialog(I)Landroid/app/Dialog;
    .locals 3

    const-string/jumbo v0, "NetworkDashboardFrag"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "onCreateDialog: dialogId="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    packed-switch p1, :pswitch_data_0

    invoke-super {p0, p1}, Lcom/android/settings/dashboard/DashboardFragment;->onCreateDialog(I)Landroid/app/Dialog;

    move-result-object v0

    return-object v0

    :pswitch_0
    const-class v0, Lcom/android/settings/network/e;

    invoke-virtual {p0, v0}, Lcom/android/settings/network/NetworkDashboardFragment;->EQ(Ljava/lang/Class;)Lcom/android/settings/core/c;

    move-result-object v0

    check-cast v0, Lcom/android/settings/network/e;

    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/android/settings/network/NetworkDashboardFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0}, Lcom/android/settings/network/e;->aMy()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    new-instance v2, Lcom/android/settings/network/-$Lambda$si_AikDR3SISy7DAgtH3CYhtYkg;

    invoke-direct {v2, v0}, Lcom/android/settings/network/-$Lambda$si_AikDR3SISy7DAgtH3CYhtYkg;-><init>(Ljava/lang/Object;)V

    const v0, 0x104000a

    invoke-virtual {v1, v0, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    return-object v0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V
    .locals 1

    invoke-super {p0, p1, p2}, Lcom/android/settings/dashboard/DashboardFragment;->onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V

    iget-object v0, p0, Lcom/android/settings/network/NetworkDashboardFragment;->aYe:Lcom/android/settings/network/g;

    invoke-virtual {v0, p1}, Lcom/android/settings/network/g;->aMB(Landroid/view/Menu;)V

    return-void
.end method
