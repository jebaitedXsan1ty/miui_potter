.class public Lcom/android/settings/SetUpNewFingerprintInternalActivity$SetUpNewFingerprintFragment;
.super Lcom/android/settings/BaseEditFragment;
.source "SetUpNewFingerprintInternalActivity.java"


# instance fields
.field private bPC:Landroid/app/Activity;

.field private bPD:Z

.field private bPE:Landroid/view/View;

.field private bPF:Landroid/view/View;

.field private bPG:Ljava/lang/String;

.field private bPH:I

.field private bPI:Lcom/android/settings/bx;

.field private bPJ:Lcom/android/settings/bM;

.field private bPK:Z

.field private bPL:Landroid/os/Handler;

.field private bPM:I

.field private bPN:Landroid/widget/ImageView;

.field private bPO:Landroid/widget/TextView;

.field private bPP:Landroid/widget/TextView;

.field private bPQ:Z

.field private bPR:Z

.field private bPS:Z

.field private bPT:Landroid/view/View;

.field private bPU:Landroid/net/Uri;

.field private bPV:Landroid/app/AlertDialog;

.field private bPW:Z

.field private bPX:Z

.field private bPY:Landroid/widget/VideoView;

.field private bPZ:I

.field private bQa:Landroid/os/Vibrator;

.field private bQb:I

.field private mProgress:I


# direct methods
.method public constructor <init>()V
    .locals 3

    const/4 v2, 0x0

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/android/settings/BaseEditFragment;-><init>()V

    iput-object v2, p0, Lcom/android/settings/SetUpNewFingerprintInternalActivity$SetUpNewFingerprintFragment;->bPJ:Lcom/android/settings/bM;

    const/4 v0, 0x1

    iput v0, p0, Lcom/android/settings/SetUpNewFingerprintInternalActivity$SetUpNewFingerprintFragment;->bPM:I

    iput v1, p0, Lcom/android/settings/SetUpNewFingerprintInternalActivity$SetUpNewFingerprintFragment;->mProgress:I

    iput v1, p0, Lcom/android/settings/SetUpNewFingerprintInternalActivity$SetUpNewFingerprintFragment;->bPZ:I

    iput-boolean v1, p0, Lcom/android/settings/SetUpNewFingerprintInternalActivity$SetUpNewFingerprintFragment;->bPS:Z

    iput-object v2, p0, Lcom/android/settings/SetUpNewFingerprintInternalActivity$SetUpNewFingerprintFragment;->bQa:Landroid/os/Vibrator;

    iput-object v2, p0, Lcom/android/settings/SetUpNewFingerprintInternalActivity$SetUpNewFingerprintFragment;->bPV:Landroid/app/AlertDialog;

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/android/settings/SetUpNewFingerprintInternalActivity$SetUpNewFingerprintFragment;->bPL:Landroid/os/Handler;

    iput-boolean v1, p0, Lcom/android/settings/SetUpNewFingerprintInternalActivity$SetUpNewFingerprintFragment;->bPX:Z

    iput v1, p0, Lcom/android/settings/SetUpNewFingerprintInternalActivity$SetUpNewFingerprintFragment;->bPH:I

    iput-object v2, p0, Lcom/android/settings/SetUpNewFingerprintInternalActivity$SetUpNewFingerprintFragment;->bPU:Landroid/net/Uri;

    iput-boolean v1, p0, Lcom/android/settings/SetUpNewFingerprintInternalActivity$SetUpNewFingerprintFragment;->bPW:Z

    iput-boolean v1, p0, Lcom/android/settings/SetUpNewFingerprintInternalActivity$SetUpNewFingerprintFragment;->bPR:Z

    iput-object v2, p0, Lcom/android/settings/SetUpNewFingerprintInternalActivity$SetUpNewFingerprintFragment;->bPC:Landroid/app/Activity;

    new-instance v0, Lcom/android/settings/iz;

    invoke-direct {v0, p0}, Lcom/android/settings/iz;-><init>(Lcom/android/settings/SetUpNewFingerprintInternalActivity$SetUpNewFingerprintFragment;)V

    iput-object v0, p0, Lcom/android/settings/SetUpNewFingerprintInternalActivity$SetUpNewFingerprintFragment;->bPI:Lcom/android/settings/bx;

    return-void
.end method

.method private bHP(I)Landroid/app/AlertDialog;
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/android/settings/SetUpNewFingerprintInternalActivity$SetUpNewFingerprintFragment;->bPC:Landroid/app/Activity;

    if-nez v0, :cond_0

    return-object v2

    :cond_0
    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Lcom/android/settings/SetUpNewFingerprintInternalActivity$SetUpNewFingerprintFragment;->bPC:Landroid/app/Activity;

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x1010355

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setIconAttribute(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f120854

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    return-object v0
.end method

.method private bHQ(Landroid/app/AlertDialog;)V
    .locals 1

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Landroid/app/AlertDialog;->dismiss()V

    :cond_0
    return-void
.end method

.method private bHR(Ljava/lang/String;)Landroid/net/Uri;
    .locals 4

    iget-boolean v0, p0, Lcom/android/settings/SetUpNewFingerprintInternalActivity$SetUpNewFingerprintFragment;->bPQ:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "_dark"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    :cond_0
    iget-object v0, p0, Lcom/android/settings/SetUpNewFingerprintInternalActivity$SetUpNewFingerprintFragment;->bPC:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getPackageName()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/SetUpNewFingerprintInternalActivity$SetUpNewFingerprintFragment;->bPC:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const-string/jumbo v2, "raw"

    invoke-virtual {v1, p1, v2, v0}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_1

    const/4 v0, 0x0

    return-object v0

    :cond_1
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "android.resource://"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v2, "/"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method private bHS(Landroid/net/Uri;Landroid/widget/VideoView;)V
    .locals 1

    const/4 v0, 0x0

    if-eqz p1, :cond_0

    iput-object v0, p0, Lcom/android/settings/SetUpNewFingerprintInternalActivity$SetUpNewFingerprintFragment;->bPU:Landroid/net/Uri;

    invoke-virtual {p2, p1}, Landroid/widget/VideoView;->setVideoURI(Landroid/net/Uri;)V

    new-instance v0, Lcom/android/settings/iF;

    invoke-direct {v0, p0, p2}, Lcom/android/settings/iF;-><init>(Lcom/android/settings/SetUpNewFingerprintInternalActivity$SetUpNewFingerprintFragment;Landroid/widget/VideoView;)V

    invoke-virtual {p2, v0}, Landroid/widget/VideoView;->setOnPreparedListener(Landroid/media/MediaPlayer$OnPreparedListener;)V

    :cond_0
    return-void
.end method

.method private bHT()V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/android/settings/SetUpNewFingerprintInternalActivity$SetUpNewFingerprintFragment;->bPJ:Lcom/android/settings/bM;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/SetUpNewFingerprintInternalActivity$SetUpNewFingerprintFragment;->bPJ:Lcom/android/settings/bM;

    invoke-virtual {v0}, Lcom/android/settings/bM;->bNj()V

    iput-object v1, p0, Lcom/android/settings/SetUpNewFingerprintInternalActivity$SetUpNewFingerprintFragment;->bPJ:Lcom/android/settings/bM;

    :cond_0
    return-void
.end method

.method static synthetic bHU(Lcom/android/settings/SetUpNewFingerprintInternalActivity$SetUpNewFingerprintFragment;)Landroid/app/Activity;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/SetUpNewFingerprintInternalActivity$SetUpNewFingerprintFragment;->bPC:Landroid/app/Activity;

    return-object v0
.end method

.method static synthetic bHV(Lcom/android/settings/SetUpNewFingerprintInternalActivity$SetUpNewFingerprintFragment;)Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/SetUpNewFingerprintInternalActivity$SetUpNewFingerprintFragment;->bPE:Landroid/view/View;

    return-object v0
.end method

.method static synthetic bHW(Lcom/android/settings/SetUpNewFingerprintInternalActivity$SetUpNewFingerprintFragment;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/SetUpNewFingerprintInternalActivity$SetUpNewFingerprintFragment;->bPG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic bHX(Lcom/android/settings/SetUpNewFingerprintInternalActivity$SetUpNewFingerprintFragment;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/settings/SetUpNewFingerprintInternalActivity$SetUpNewFingerprintFragment;->bPK:Z

    return v0
.end method

.method static synthetic bHY(Lcom/android/settings/SetUpNewFingerprintInternalActivity$SetUpNewFingerprintFragment;)Landroid/os/Handler;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/SetUpNewFingerprintInternalActivity$SetUpNewFingerprintFragment;->bPL:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic bHZ(Lcom/android/settings/SetUpNewFingerprintInternalActivity$SetUpNewFingerprintFragment;)I
    .locals 1

    iget v0, p0, Lcom/android/settings/SetUpNewFingerprintInternalActivity$SetUpNewFingerprintFragment;->bPM:I

    return v0
.end method

.method static synthetic bIA(Lcom/android/settings/SetUpNewFingerprintInternalActivity$SetUpNewFingerprintFragment;Ljava/lang/String;)Landroid/net/Uri;
    .locals 1

    invoke-direct {p0, p1}, Lcom/android/settings/SetUpNewFingerprintInternalActivity$SetUpNewFingerprintFragment;->bHR(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method static synthetic bIB(Lcom/android/settings/SetUpNewFingerprintInternalActivity$SetUpNewFingerprintFragment;Landroid/app/AlertDialog;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/settings/SetUpNewFingerprintInternalActivity$SetUpNewFingerprintFragment;->bHQ(Landroid/app/AlertDialog;)V

    return-void
.end method

.method static synthetic bIC(Lcom/android/settings/SetUpNewFingerprintInternalActivity$SetUpNewFingerprintFragment;Landroid/net/Uri;Landroid/widget/VideoView;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/android/settings/SetUpNewFingerprintInternalActivity$SetUpNewFingerprintFragment;->bHS(Landroid/net/Uri;Landroid/widget/VideoView;)V

    return-void
.end method

.method static synthetic bIa(Lcom/android/settings/SetUpNewFingerprintInternalActivity$SetUpNewFingerprintFragment;)Landroid/widget/ImageView;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/SetUpNewFingerprintInternalActivity$SetUpNewFingerprintFragment;->bPN:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic bIb(Lcom/android/settings/SetUpNewFingerprintInternalActivity$SetUpNewFingerprintFragment;)Landroid/widget/TextView;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/SetUpNewFingerprintInternalActivity$SetUpNewFingerprintFragment;->bPO:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic bIc(Lcom/android/settings/SetUpNewFingerprintInternalActivity$SetUpNewFingerprintFragment;)Landroid/widget/TextView;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/SetUpNewFingerprintInternalActivity$SetUpNewFingerprintFragment;->bPP:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic bId(Lcom/android/settings/SetUpNewFingerprintInternalActivity$SetUpNewFingerprintFragment;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/settings/SetUpNewFingerprintInternalActivity$SetUpNewFingerprintFragment;->bPR:Z

    return v0
.end method

.method static synthetic bIe(Lcom/android/settings/SetUpNewFingerprintInternalActivity$SetUpNewFingerprintFragment;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/settings/SetUpNewFingerprintInternalActivity$SetUpNewFingerprintFragment;->bPS:Z

    return v0
.end method

.method static synthetic bIf(Lcom/android/settings/SetUpNewFingerprintInternalActivity$SetUpNewFingerprintFragment;)Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/SetUpNewFingerprintInternalActivity$SetUpNewFingerprintFragment;->bPT:Landroid/view/View;

    return-object v0
.end method

.method static synthetic bIg(Lcom/android/settings/SetUpNewFingerprintInternalActivity$SetUpNewFingerprintFragment;)Landroid/net/Uri;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/SetUpNewFingerprintInternalActivity$SetUpNewFingerprintFragment;->bPU:Landroid/net/Uri;

    return-object v0
.end method

.method static synthetic bIh(Lcom/android/settings/SetUpNewFingerprintInternalActivity$SetUpNewFingerprintFragment;)Landroid/app/AlertDialog;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/SetUpNewFingerprintInternalActivity$SetUpNewFingerprintFragment;->bPV:Landroid/app/AlertDialog;

    return-object v0
.end method

.method static synthetic bIi(Lcom/android/settings/SetUpNewFingerprintInternalActivity$SetUpNewFingerprintFragment;)I
    .locals 1

    iget v0, p0, Lcom/android/settings/SetUpNewFingerprintInternalActivity$SetUpNewFingerprintFragment;->mProgress:I

    return v0
.end method

.method static synthetic bIj(Lcom/android/settings/SetUpNewFingerprintInternalActivity$SetUpNewFingerprintFragment;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/settings/SetUpNewFingerprintInternalActivity$SetUpNewFingerprintFragment;->bPW:Z

    return v0
.end method

.method static synthetic bIk(Lcom/android/settings/SetUpNewFingerprintInternalActivity$SetUpNewFingerprintFragment;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/settings/SetUpNewFingerprintInternalActivity$SetUpNewFingerprintFragment;->bPX:Z

    return v0
.end method

.method static synthetic bIl(Lcom/android/settings/SetUpNewFingerprintInternalActivity$SetUpNewFingerprintFragment;)Landroid/widget/VideoView;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/SetUpNewFingerprintInternalActivity$SetUpNewFingerprintFragment;->bPY:Landroid/widget/VideoView;

    return-object v0
.end method

.method static synthetic bIm(Lcom/android/settings/SetUpNewFingerprintInternalActivity$SetUpNewFingerprintFragment;)I
    .locals 1

    iget v0, p0, Lcom/android/settings/SetUpNewFingerprintInternalActivity$SetUpNewFingerprintFragment;->bPZ:I

    return v0
.end method

.method static synthetic bIn(Lcom/android/settings/SetUpNewFingerprintInternalActivity$SetUpNewFingerprintFragment;)I
    .locals 1

    iget v0, p0, Lcom/android/settings/SetUpNewFingerprintInternalActivity$SetUpNewFingerprintFragment;->bQb:I

    return v0
.end method

.method static synthetic bIo(Lcom/android/settings/SetUpNewFingerprintInternalActivity$SetUpNewFingerprintFragment;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    iput-object p1, p0, Lcom/android/settings/SetUpNewFingerprintInternalActivity$SetUpNewFingerprintFragment;->bPG:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic bIp(Lcom/android/settings/SetUpNewFingerprintInternalActivity$SetUpNewFingerprintFragment;I)I
    .locals 0

    iput p1, p0, Lcom/android/settings/SetUpNewFingerprintInternalActivity$SetUpNewFingerprintFragment;->bPH:I

    return p1
.end method

.method static synthetic bIq(Lcom/android/settings/SetUpNewFingerprintInternalActivity$SetUpNewFingerprintFragment;I)I
    .locals 0

    iput p1, p0, Lcom/android/settings/SetUpNewFingerprintInternalActivity$SetUpNewFingerprintFragment;->bPM:I

    return p1
.end method

.method static synthetic bIr(Lcom/android/settings/SetUpNewFingerprintInternalActivity$SetUpNewFingerprintFragment;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/android/settings/SetUpNewFingerprintInternalActivity$SetUpNewFingerprintFragment;->bPR:Z

    return p1
.end method

.method static synthetic bIs(Lcom/android/settings/SetUpNewFingerprintInternalActivity$SetUpNewFingerprintFragment;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/android/settings/SetUpNewFingerprintInternalActivity$SetUpNewFingerprintFragment;->bPS:Z

    return p1
.end method

.method static synthetic bIt(Lcom/android/settings/SetUpNewFingerprintInternalActivity$SetUpNewFingerprintFragment;Landroid/net/Uri;)Landroid/net/Uri;
    .locals 0

    iput-object p1, p0, Lcom/android/settings/SetUpNewFingerprintInternalActivity$SetUpNewFingerprintFragment;->bPU:Landroid/net/Uri;

    return-object p1
.end method

.method static synthetic bIu(Lcom/android/settings/SetUpNewFingerprintInternalActivity$SetUpNewFingerprintFragment;Landroid/app/AlertDialog;)Landroid/app/AlertDialog;
    .locals 0

    iput-object p1, p0, Lcom/android/settings/SetUpNewFingerprintInternalActivity$SetUpNewFingerprintFragment;->bPV:Landroid/app/AlertDialog;

    return-object p1
.end method

.method static synthetic bIv(Lcom/android/settings/SetUpNewFingerprintInternalActivity$SetUpNewFingerprintFragment;I)I
    .locals 0

    iput p1, p0, Lcom/android/settings/SetUpNewFingerprintInternalActivity$SetUpNewFingerprintFragment;->mProgress:I

    return p1
.end method

.method static synthetic bIw(Lcom/android/settings/SetUpNewFingerprintInternalActivity$SetUpNewFingerprintFragment;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/android/settings/SetUpNewFingerprintInternalActivity$SetUpNewFingerprintFragment;->bPW:Z

    return p1
.end method

.method static synthetic bIx(Lcom/android/settings/SetUpNewFingerprintInternalActivity$SetUpNewFingerprintFragment;I)I
    .locals 0

    iput p1, p0, Lcom/android/settings/SetUpNewFingerprintInternalActivity$SetUpNewFingerprintFragment;->bPZ:I

    return p1
.end method

.method static synthetic bIy(Lcom/android/settings/SetUpNewFingerprintInternalActivity$SetUpNewFingerprintFragment;I)I
    .locals 0

    iput p1, p0, Lcom/android/settings/SetUpNewFingerprintInternalActivity$SetUpNewFingerprintFragment;->bQb:I

    return p1
.end method

.method static synthetic bIz(Lcom/android/settings/SetUpNewFingerprintInternalActivity$SetUpNewFingerprintFragment;I)Landroid/app/AlertDialog;
    .locals 1

    invoke-direct {p0, p1}, Lcom/android/settings/SetUpNewFingerprintInternalActivity$SetUpNewFingerprintFragment;->bHP(I)Landroid/app/AlertDialog;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 3

    invoke-super {p0, p1, p2, p3}, Lcom/android/settings/BaseEditFragment;->onActivityResult(IILandroid/content/Intent;)V

    const/16 v0, 0x64

    if-ne p1, v0, :cond_0

    const/4 v0, -0x1

    if-ne p2, v0, :cond_1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/settings/SetUpNewFingerprintInternalActivity$SetUpNewFingerprintFragment;->bPX:Z

    const-string/jumbo v0, "hw_auth_token"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getByteArrayExtra(Ljava/lang/String;)[B

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/SetUpNewFingerprintInternalActivity$SetUpNewFingerprintFragment;->bPJ:Lcom/android/settings/bM;

    iget-object v2, p0, Lcom/android/settings/SetUpNewFingerprintInternalActivity$SetUpNewFingerprintFragment;->bPI:Lcom/android/settings/bx;

    invoke-virtual {v1, v2, v0}, Lcom/android/settings/bM;->bNh(Lcom/android/settings/bx;[B)V

    iget-object v0, p0, Lcom/android/settings/SetUpNewFingerprintInternalActivity$SetUpNewFingerprintFragment;->bPC:Landroid/app/Activity;

    const-string/jumbo v1, "vibrator"

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Vibrator;

    iput-object v0, p0, Lcom/android/settings/SetUpNewFingerprintInternalActivity$SetUpNewFingerprintFragment;->bQa:Landroid/os/Vibrator;

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0}, Lcom/android/settings/SetUpNewFingerprintInternalActivity$SetUpNewFingerprintFragment;->finish()V

    goto :goto_0
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 1

    invoke-super {p0, p1}, Lcom/android/settings/BaseEditFragment;->onAttach(Landroid/app/Activity;)V

    iget-object v0, p0, Lcom/android/settings/SetUpNewFingerprintInternalActivity$SetUpNewFingerprintFragment;->bPC:Landroid/app/Activity;

    if-nez v0, :cond_0

    iput-object p1, p0, Lcom/android/settings/SetUpNewFingerprintInternalActivity$SetUpNewFingerprintFragment;->bPC:Landroid/app/Activity;

    :cond_0
    return-void
.end method

.method public onAttach(Landroid/content/Context;)V
    .locals 1

    invoke-super {p0, p1}, Lcom/android/settings/BaseEditFragment;->onAttach(Landroid/content/Context;)V

    instance-of v0, p1, Landroid/app/Activity;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/SetUpNewFingerprintInternalActivity$SetUpNewFingerprintFragment;->bPC:Landroid/app/Activity;

    if-nez v0, :cond_0

    check-cast p1, Landroid/app/Activity;

    iput-object p1, p0, Lcom/android/settings/SetUpNewFingerprintInternalActivity$SetUpNewFingerprintFragment;->bPC:Landroid/app/Activity;

    :cond_0
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 5

    const/4 v4, 0x1

    const/4 v2, 0x0

    invoke-super {p0, p1}, Lcom/android/settings/BaseEditFragment;->onCreate(Landroid/os/Bundle;)V

    new-instance v0, Lcom/android/settings/bM;

    iget-object v1, p0, Lcom/android/settings/SetUpNewFingerprintInternalActivity$SetUpNewFingerprintFragment;->bPC:Landroid/app/Activity;

    invoke-direct {v0, v1}, Lcom/android/settings/bM;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/settings/SetUpNewFingerprintInternalActivity$SetUpNewFingerprintFragment;->bPJ:Lcom/android/settings/bM;

    invoke-virtual {p0}, Lcom/android/settings/SetUpNewFingerprintInternalActivity$SetUpNewFingerprintFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f05001e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/settings/SetUpNewFingerprintInternalActivity$SetUpNewFingerprintFragment;->bPQ:Z

    const-string/jumbo v0, "front_fingerprint_sensor"

    invoke-static {v0, v2}, Lmiui/util/FeatureParser;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/settings/SetUpNewFingerprintInternalActivity$SetUpNewFingerprintFragment;->bPK:Z

    invoke-virtual {p0}, Lcom/android/settings/SetUpNewFingerprintInternalActivity$SetUpNewFingerprintFragment;->getIntent()Landroid/content/Intent;

    move-result-object v0

    if-eqz v0, :cond_1

    const-string/jumbo v1, "add_keyguard_password_then_add_fingerprint"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    iput-boolean v1, p0, Lcom/android/settings/SetUpNewFingerprintInternalActivity$SetUpNewFingerprintFragment;->bPD:Z

    :goto_0
    iget-boolean v1, p0, Lcom/android/settings/SetUpNewFingerprintInternalActivity$SetUpNewFingerprintFragment;->bPD:Z

    if-eqz v1, :cond_2

    iput-boolean v4, p0, Lcom/android/settings/SetUpNewFingerprintInternalActivity$SetUpNewFingerprintFragment;->bPX:Z

    const-string/jumbo v1, "hw_auth_token"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getByteArrayExtra(Ljava/lang/String;)[B

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/SetUpNewFingerprintInternalActivity$SetUpNewFingerprintFragment;->bPJ:Lcom/android/settings/bM;

    iget-object v2, p0, Lcom/android/settings/SetUpNewFingerprintInternalActivity$SetUpNewFingerprintFragment;->bPI:Lcom/android/settings/bx;

    invoke-virtual {v1, v2, v0}, Lcom/android/settings/bM;->bNh(Lcom/android/settings/bx;[B)V

    iget-object v0, p0, Lcom/android/settings/SetUpNewFingerprintInternalActivity$SetUpNewFingerprintFragment;->bPC:Landroid/app/Activity;

    const-string/jumbo v1, "vibrator"

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Vibrator;

    iput-object v0, p0, Lcom/android/settings/SetUpNewFingerprintInternalActivity$SetUpNewFingerprintFragment;->bQa:Landroid/os/Vibrator;

    :cond_0
    :goto_1
    iget-object v0, p0, Lcom/android/settings/SetUpNewFingerprintInternalActivity$SetUpNewFingerprintFragment;->bPC:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v1, 0x80

    invoke-virtual {v0, v1}, Landroid/view/Window;->addFlags(I)V

    return-void

    :cond_1
    iput-boolean v2, p0, Lcom/android/settings/SetUpNewFingerprintInternalActivity$SetUpNewFingerprintFragment;->bPD:Z

    goto :goto_0

    :cond_2
    new-instance v0, Landroid/security/MiuiLockPatternUtils;

    iget-object v1, p0, Lcom/android/settings/SetUpNewFingerprintInternalActivity$SetUpNewFingerprintFragment;->bPC:Landroid/app/Activity;

    invoke-direct {v0, v1}, Landroid/security/MiuiLockPatternUtils;-><init>(Landroid/content/Context;)V

    const-class v0, Lcom/android/settings/SetUpMiuiSecurityChooseUnlock$InternalActivity;

    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v1

    if-nez v1, :cond_0

    new-instance v1, Landroid/content/Intent;

    iget-object v2, p0, Lcom/android/settings/SetUpNewFingerprintInternalActivity$SetUpNewFingerprintFragment;->bPC:Landroid/app/Activity;

    invoke-direct {v1, v2, v0}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v0, "has_challenge"

    invoke-virtual {v1, v0, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string/jumbo v0, "challenge"

    iget-object v2, p0, Lcom/android/settings/SetUpNewFingerprintInternalActivity$SetUpNewFingerprintFragment;->bPJ:Lcom/android/settings/bM;

    invoke-virtual {v2}, Lcom/android/settings/bM;->bNi()J

    move-result-wide v2

    invoke-virtual {v1, v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    const-string/jumbo v0, "add_keyguard_password_then_add_fingerprint"

    invoke-virtual {v1, v0, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const/16 v0, 0x64

    invoke-virtual {p0, v1, v0}, Lcom/android/settings/SetUpNewFingerprintInternalActivity$SetUpNewFingerprintFragment;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_1
.end method

.method public onDetach()V
    .locals 1

    invoke-super {p0}, Lcom/android/settings/BaseEditFragment;->onDetach()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/settings/SetUpNewFingerprintInternalActivity$SetUpNewFingerprintFragment;->bPC:Landroid/app/Activity;

    return-void
.end method

.method public onPause()V
    .locals 1

    iget-boolean v0, p0, Lcom/android/settings/SetUpNewFingerprintInternalActivity$SetUpNewFingerprintFragment;->bPX:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/settings/SetUpNewFingerprintInternalActivity$SetUpNewFingerprintFragment;->bPX:Z

    invoke-direct {p0}, Lcom/android/settings/SetUpNewFingerprintInternalActivity$SetUpNewFingerprintFragment;->bHT()V

    iget-object v0, p0, Lcom/android/settings/SetUpNewFingerprintInternalActivity$SetUpNewFingerprintFragment;->bPV:Landroid/app/AlertDialog;

    invoke-direct {p0, v0}, Lcom/android/settings/SetUpNewFingerprintInternalActivity$SetUpNewFingerprintFragment;->bHQ(Landroid/app/AlertDialog;)V

    invoke-virtual {p0}, Lcom/android/settings/SetUpNewFingerprintInternalActivity$SetUpNewFingerprintFragment;->finish()V

    :cond_0
    invoke-super {p0}, Lcom/android/settings/BaseEditFragment;->onPause()V

    return-void
.end method

.method public onStart()V
    .locals 0

    invoke-super {p0}, Lcom/android/settings/BaseEditFragment;->onStart()V

    return-void
.end method

.method public vB(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 4

    const/4 v3, 0x0

    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/android/settings/SetUpNewFingerprintInternalActivity$SetUpNewFingerprintFragment;->getActionBar()Lmiui/app/ActionBar;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ActionBar;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Landroid/app/ActionBar;->hide()V

    :cond_0
    const v0, 0x7f0d01c8

    invoke-virtual {p1, v0, p2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/SetUpNewFingerprintInternalActivity$SetUpNewFingerprintFragment;->bPF:Landroid/view/View;

    iget-object v0, p0, Lcom/android/settings/SetUpNewFingerprintInternalActivity$SetUpNewFingerprintFragment;->bPF:Landroid/view/View;

    const v1, 0x7f0a03ed

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/settings/SetUpNewFingerprintInternalActivity$SetUpNewFingerprintFragment;->bPP:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/android/settings/SetUpNewFingerprintInternalActivity$SetUpNewFingerprintFragment;->bPF:Landroid/view/View;

    const v1, 0x7f0a03ec

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/settings/SetUpNewFingerprintInternalActivity$SetUpNewFingerprintFragment;->bPO:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/android/settings/SetUpNewFingerprintInternalActivity$SetUpNewFingerprintFragment;->bPF:Landroid/view/View;

    const v1, 0x7f0a03ea

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/android/settings/SetUpNewFingerprintInternalActivity$SetUpNewFingerprintFragment;->bPN:Landroid/widget/ImageView;

    iget-object v0, p0, Lcom/android/settings/SetUpNewFingerprintInternalActivity$SetUpNewFingerprintFragment;->bPF:Landroid/view/View;

    const v1, 0x7f0a03eb

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/VideoView;

    iput-object v0, p0, Lcom/android/settings/SetUpNewFingerprintInternalActivity$SetUpNewFingerprintFragment;->bPY:Landroid/widget/VideoView;

    iget-object v0, p0, Lcom/android/settings/SetUpNewFingerprintInternalActivity$SetUpNewFingerprintFragment;->bPF:Landroid/view/View;

    const v1, 0x7f0a00a5

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/SetUpNewFingerprintInternalActivity$SetUpNewFingerprintFragment;->bPE:Landroid/view/View;

    iget-object v0, p0, Lcom/android/settings/SetUpNewFingerprintInternalActivity$SetUpNewFingerprintFragment;->bPF:Landroid/view/View;

    const v1, 0x7f0a00aa

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/SetUpNewFingerprintInternalActivity$SetUpNewFingerprintFragment;->bPT:Landroid/view/View;

    iget-object v0, p0, Lcom/android/settings/SetUpNewFingerprintInternalActivity$SetUpNewFingerprintFragment;->bPT:Landroid/view/View;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v1, p0, Lcom/android/settings/SetUpNewFingerprintInternalActivity$SetUpNewFingerprintFragment;->bPN:Landroid/widget/ImageView;

    iget-boolean v0, p0, Lcom/android/settings/SetUpNewFingerprintInternalActivity$SetUpNewFingerprintFragment;->bPK:Z

    if-eqz v0, :cond_2

    const v0, 0x7f0800e0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    iget-object v1, p0, Lcom/android/settings/SetUpNewFingerprintInternalActivity$SetUpNewFingerprintFragment;->bPO:Landroid/widget/TextView;

    iget-boolean v0, p0, Lcom/android/settings/SetUpNewFingerprintInternalActivity$SetUpNewFingerprintFragment;->bPK:Z

    if-eqz v0, :cond_3

    const v0, 0x7f1200b2

    :goto_1
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(I)V

    new-instance v0, Lcom/android/settings/iB;

    invoke-direct {v0, p0}, Lcom/android/settings/iB;-><init>(Lcom/android/settings/SetUpNewFingerprintInternalActivity$SetUpNewFingerprintFragment;)V

    iget-object v1, p0, Lcom/android/settings/SetUpNewFingerprintInternalActivity$SetUpNewFingerprintFragment;->bPY:Landroid/widget/VideoView;

    invoke-virtual {v1, v2}, Landroid/widget/VideoView;->setZOrderOnTop(Z)V

    iget-object v1, p0, Lcom/android/settings/SetUpNewFingerprintInternalActivity$SetUpNewFingerprintFragment;->bPY:Landroid/widget/VideoView;

    invoke-virtual {v1, v0}, Landroid/widget/VideoView;->setOnErrorListener(Landroid/media/MediaPlayer$OnErrorListener;)V

    iget-object v0, p0, Lcom/android/settings/SetUpNewFingerprintInternalActivity$SetUpNewFingerprintFragment;->bPY:Landroid/widget/VideoView;

    new-instance v1, Lcom/android/settings/iC;

    invoke-direct {v1, p0}, Lcom/android/settings/iC;-><init>(Lcom/android/settings/SetUpNewFingerprintInternalActivity$SetUpNewFingerprintFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/VideoView;->setOnCompletionListener(Landroid/media/MediaPlayer$OnCompletionListener;)V

    const-string/jumbo v0, "core_scan_output_%02d"

    new-array v1, v2, [Ljava/lang/Object;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/android/settings/SetUpNewFingerprintInternalActivity$SetUpNewFingerprintFragment;->bHR(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/android/settings/SetUpNewFingerprintInternalActivity$SetUpNewFingerprintFragment;->bPY:Landroid/widget/VideoView;

    invoke-virtual {v1, v0}, Landroid/widget/VideoView;->setVideoURI(Landroid/net/Uri;)V

    :cond_1
    iget-object v0, p0, Lcom/android/settings/SetUpNewFingerprintInternalActivity$SetUpNewFingerprintFragment;->bPE:Landroid/view/View;

    new-instance v1, Lcom/android/settings/iD;

    invoke-direct {v1, p0}, Lcom/android/settings/iD;-><init>(Lcom/android/settings/SetUpNewFingerprintInternalActivity$SetUpNewFingerprintFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/android/settings/SetUpNewFingerprintInternalActivity$SetUpNewFingerprintFragment;->bPT:Landroid/view/View;

    new-instance v1, Lcom/android/settings/iE;

    invoke-direct {v1, p0}, Lcom/android/settings/iE;-><init>(Lcom/android/settings/SetUpNewFingerprintInternalActivity$SetUpNewFingerprintFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/android/settings/SetUpNewFingerprintInternalActivity$SetUpNewFingerprintFragment;->bPF:Landroid/view/View;

    return-object v0

    :cond_2
    const v0, 0x7f0800df

    goto :goto_0

    :cond_3
    const v0, 0x7f12009d

    goto :goto_1
.end method
