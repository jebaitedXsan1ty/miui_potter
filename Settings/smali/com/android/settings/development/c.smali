.class public Lcom/android/settings/development/c;
.super Lcom/android/settings/core/e;
.source "VerifyAppsOverUsbPreferenceController.java"


# instance fields
.field private bgj:Lcom/android/settingslib/MiuiRestrictedSwitchPreference;

.field private final bgk:Lcom/android/settings/development/VerifyAppsOverUsbPreferenceController$RestrictedLockUtilsDelegate;


# direct methods
.method constructor <init>(Landroid/content/Context;)V
    .locals 1

    invoke-direct {p0, p1}, Lcom/android/settings/core/e;-><init>(Landroid/content/Context;)V

    new-instance v0, Lcom/android/settings/development/VerifyAppsOverUsbPreferenceController$RestrictedLockUtilsDelegate;

    invoke-direct {v0, p0}, Lcom/android/settings/development/VerifyAppsOverUsbPreferenceController$RestrictedLockUtilsDelegate;-><init>(Lcom/android/settings/development/c;)V

    iput-object v0, p0, Lcom/android/settings/development/c;->bgk:Lcom/android/settings/development/VerifyAppsOverUsbPreferenceController$RestrictedLockUtilsDelegate;

    return-void
.end method

.method private aUt()Z
    .locals 5

    const/4 v4, 0x1

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/android/settings/development/c;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v1, "adb_enabled"

    invoke-static {v0, v1, v3}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    if-nez v1, :cond_0

    return v3

    :cond_0
    const-string/jumbo v1, "package_verifier_enable"

    invoke-static {v0, v1, v4}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    if-nez v0, :cond_1

    return v3

    :cond_1
    iget-object v0, p0, Lcom/android/settings/development/c;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    new-instance v1, Landroid/content/Intent;

    const-string/jumbo v2, "android.intent.action.PACKAGE_NEEDS_VERIFICATION"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string/jumbo v2, "application/vnd.android.package-archive"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {v1, v4}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    invoke-virtual {v0, v1, v3}, Landroid/content/pm/PackageManager;->queryBroadcastReceivers(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_2

    return v3

    :cond_2
    return v4
.end method


# virtual methods
.method public aUu()V
    .locals 6

    const/4 v3, 0x0

    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/android/settings/development/c;->p()Z

    move-result v2

    if-nez v2, :cond_0

    return-void

    :cond_0
    invoke-direct {p0}, Lcom/android/settings/development/c;->aUt()Z

    move-result v2

    if-nez v2, :cond_1

    iget-object v0, p0, Lcom/android/settings/development/c;->bgj:Lcom/android/settingslib/MiuiRestrictedSwitchPreference;

    invoke-virtual {v0, v1}, Lcom/android/settingslib/MiuiRestrictedSwitchPreference;->setChecked(Z)V

    iget-object v0, p0, Lcom/android/settings/development/c;->bgj:Lcom/android/settingslib/MiuiRestrictedSwitchPreference;

    invoke-virtual {v0, v3}, Lcom/android/settingslib/MiuiRestrictedSwitchPreference;->setDisabledByAdmin(Lcom/android/settingslib/n;)V

    iget-object v0, p0, Lcom/android/settings/development/c;->bgj:Lcom/android/settingslib/MiuiRestrictedSwitchPreference;

    invoke-virtual {v0, v1}, Lcom/android/settingslib/MiuiRestrictedSwitchPreference;->setEnabled(Z)V

    return-void

    :cond_1
    iget-object v2, p0, Lcom/android/settings/development/c;->bgk:Lcom/android/settings/development/VerifyAppsOverUsbPreferenceController$RestrictedLockUtilsDelegate;

    iget-object v3, p0, Lcom/android/settings/development/c;->mContext:Landroid/content/Context;

    const-string/jumbo v4, "ensure_verify_apps"

    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v5

    invoke-virtual {v2, v3, v4, v5}, Lcom/android/settings/development/VerifyAppsOverUsbPreferenceController$RestrictedLockUtilsDelegate;->aUv(Landroid/content/Context;Ljava/lang/String;I)Lcom/android/settingslib/n;

    move-result-object v2

    if-eqz v2, :cond_2

    iget-object v1, p0, Lcom/android/settings/development/c;->bgj:Lcom/android/settingslib/MiuiRestrictedSwitchPreference;

    invoke-virtual {v1, v0}, Lcom/android/settingslib/MiuiRestrictedSwitchPreference;->setChecked(Z)V

    iget-object v0, p0, Lcom/android/settings/development/c;->bgj:Lcom/android/settingslib/MiuiRestrictedSwitchPreference;

    invoke-virtual {v0, v2}, Lcom/android/settingslib/MiuiRestrictedSwitchPreference;->setDisabledByAdmin(Lcom/android/settingslib/n;)V

    return-void

    :cond_2
    iget-object v2, p0, Lcom/android/settings/development/c;->bgj:Lcom/android/settingslib/MiuiRestrictedSwitchPreference;

    invoke-virtual {v2, v0}, Lcom/android/settingslib/MiuiRestrictedSwitchPreference;->setEnabled(Z)V

    iget-object v2, p0, Lcom/android/settings/development/c;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string/jumbo v3, "verifier_verify_adb_installs"

    invoke-static {v2, v3, v0}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    if-eqz v2, :cond_3

    :goto_0
    iget-object v1, p0, Lcom/android/settings/development/c;->bgj:Lcom/android/settingslib/MiuiRestrictedSwitchPreference;

    invoke-virtual {v1, v0}, Lcom/android/settingslib/MiuiRestrictedSwitchPreference;->setChecked(Z)V

    return-void

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public fm(Landroid/preference/Preference;)Z
    .locals 5

    const/4 v1, 0x1

    const/4 v0, 0x0

    const-string/jumbo v2, "verify_apps_over_usb"

    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/android/settings/development/c;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string/jumbo v3, "verifier_verify_adb_installs"

    iget-object v4, p0, Lcom/android/settings/development/c;->bgj:Lcom/android/settingslib/MiuiRestrictedSwitchPreference;

    invoke-virtual {v4}, Lcom/android/settingslib/MiuiRestrictedSwitchPreference;->isChecked()Z

    move-result v4

    if-eqz v4, :cond_0

    move v0, v1

    :cond_0
    invoke-static {v2, v3, v0}, Landroid/provider/Settings$Global;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    return v1

    :cond_1
    return v0
.end method

.method public i(Landroid/preference/PreferenceScreen;)V
    .locals 1

    invoke-super {p0, p1}, Lcom/android/settings/core/e;->i(Landroid/preference/PreferenceScreen;)V

    invoke-virtual {p0}, Lcom/android/settings/development/c;->p()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string/jumbo v0, "verify_apps_over_usb"

    invoke-virtual {p1, v0}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Lcom/android/settingslib/MiuiRestrictedSwitchPreference;

    iput-object v0, p0, Lcom/android/settings/development/c;->bgj:Lcom/android/settingslib/MiuiRestrictedSwitchPreference;

    iget-object v0, p0, Lcom/android/settings/development/c;->bgj:Lcom/android/settingslib/MiuiRestrictedSwitchPreference;

    invoke-virtual {v0, p1}, Lcom/android/settingslib/MiuiRestrictedSwitchPreference;->setPreferenceScreen(Landroid/preference/PreferenceScreen;)V

    :cond_0
    return-void
.end method

.method public l()Ljava/lang/String;
    .locals 1

    const-string/jumbo v0, "verify_apps_over_usb"

    return-object v0
.end method

.method public p()Z
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/android/settings/development/c;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string/jumbo v3, "verifier_setting_visible"

    invoke-static {v2, v3, v0}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    if-lez v2, :cond_0

    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method
