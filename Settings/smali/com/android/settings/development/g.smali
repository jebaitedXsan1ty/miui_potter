.class public Lcom/android/settings/development/g;
.super Lcom/android/settings/core/e;
.source "TelephonyMonitorPreferenceController.java"


# static fields
.field static final BUILD_TYPE:Ljava/lang/String; = "ro.build.type"

.field static final DISABLED_STATUS:Ljava/lang/String; = "disabled"

.field static final ENABLED_STATUS:Ljava/lang/String; = "enabled"

.field static final PROPERTY_TELEPHONY_MONITOR:Ljava/lang/String; = "persist.radio.enable_tel_mon"

.field static final USER_DISABLED_STATUS:Ljava/lang/String; = "user_disabled"

.field static final USER_ENABLED_STATUS:Ljava/lang/String; = "user_enabled"


# instance fields
.field private bin:Landroid/preference/SwitchPreference;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/settings/core/e;-><init>(Landroid/content/Context;)V

    return-void
.end method

.method private aXd()Z
    .locals 2

    const-string/jumbo v0, "persist.radio.enable_tel_mon"

    const-string/jumbo v1, "disabled"

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "enabled"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string/jumbo v1, "user_enabled"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public aXb(Z)V
    .locals 1

    invoke-virtual {p0}, Lcom/android/settings/development/g;->p()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/development/g;->bin:Landroid/preference/SwitchPreference;

    invoke-virtual {v0, p1}, Landroid/preference/SwitchPreference;->setEnabled(Z)V

    :cond_0
    return-void
.end method

.method public aXc()Z
    .locals 2

    invoke-virtual {p0}, Lcom/android/settings/development/g;->p()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    return v0

    :cond_0
    invoke-direct {p0}, Lcom/android/settings/development/g;->aXd()Z

    move-result v0

    iget-object v1, p0, Lcom/android/settings/development/g;->bin:Landroid/preference/SwitchPreference;

    invoke-virtual {v1, v0}, Landroid/preference/SwitchPreference;->setChecked(Z)V

    return v0
.end method

.method public cz(Landroid/preference/Preference;)V
    .locals 0

    invoke-virtual {p0}, Lcom/android/settings/development/g;->aXc()Z

    return-void
.end method

.method public fm(Landroid/preference/Preference;)Z
    .locals 3

    const/4 v2, 0x1

    const-string/jumbo v0, "telephony_monitor_switch"

    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    check-cast p1, Landroid/preference/SwitchPreference;

    const-string/jumbo v1, "persist.radio.enable_tel_mon"

    invoke-virtual {p1}, Landroid/preference/SwitchPreference;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string/jumbo v0, "user_enabled"

    :goto_0
    invoke-static {v1, v0}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/settings/development/g;->mContext:Landroid/content/Context;

    const v1, 0x7f121243

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    return v2

    :cond_0
    const-string/jumbo v0, "user_disabled"

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    return v0
.end method

.method public i(Landroid/preference/PreferenceScreen;)V
    .locals 2

    invoke-super {p0, p1}, Lcom/android/settings/core/e;->i(Landroid/preference/PreferenceScreen;)V

    invoke-virtual {p0}, Lcom/android/settings/development/g;->p()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string/jumbo v0, "telephony_monitor_switch"

    invoke-virtual {p1, v0}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/SwitchPreference;

    iput-object v0, p0, Lcom/android/settings/development/g;->bin:Landroid/preference/SwitchPreference;

    iget-object v0, p0, Lcom/android/settings/development/g;->bin:Landroid/preference/SwitchPreference;

    invoke-direct {p0}, Lcom/android/settings/development/g;->aXd()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/preference/SwitchPreference;->setChecked(Z)V

    :cond_0
    return-void
.end method

.method public l()Ljava/lang/String;
    .locals 1

    const-string/jumbo v0, "telephony_monitor_switch"

    return-object v0
.end method

.method public p()Z
    .locals 2

    iget-object v0, p0, Lcom/android/settings/development/g;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f050014

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string/jumbo v0, "ro.build.type"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "userdebug"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string/jumbo v0, "ro.build.type"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "eng"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
