.class public Lcom/android/settings/development/DevelopmentSettings;
.super Lcom/android/settings/MiuiRestrictedSettingsFragment;
.source "DevelopmentSettings.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;
.implements Landroid/content/DialogInterface$OnDismissListener;
.implements Landroid/preference/Preference$OnPreferenceChangeListener;
.implements Lcom/android/settings/search/Indexable;


# static fields
.field public static final SEARCH_INDEX_DATA_PROVIDER:Lcom/android/settings/search/Indexable$SearchIndexProvider;

.field private static final bgm:[I


# instance fields
.field private bgA:Landroid/preference/CheckBoxPreference;

.field private bgB:Landroid/preference/ListPreference;

.field private bgC:Landroid/preference/ListPreference;

.field private bgD:Landroid/preference/ListPreference;

.field private bgE:Landroid/preference/ListPreference;

.field private bgF:Landroid/preference/ListPreference;

.field private bgG:Landroid/preference/ListPreference;

.field private bgH:Landroid/preference/CheckBoxPreference;

.field private bgI:Z

.field private bgJ:Landroid/preference/CheckBoxPreference;

.field private bgK:Landroid/preference/CheckBoxPreference;

.field private bgL:Lcom/android/settings/cx;

.field private bgM:Landroid/preference/Preference;

.field private bgN:Lcom/android/settings/development/ColorModePreference;

.field private bgO:Landroid/preference/CheckBoxPreference;

.field private bgP:Ljava/lang/String;

.field private bgQ:Landroid/preference/Preference;

.field private bgR:Landroid/preference/ListPreference;

.field private bgS:Landroid/preference/ListPreference;

.field private bgT:Landroid/preference/CheckBoxPreference;

.field private bgU:Landroid/preference/CheckBoxPreference;

.field private bgV:Landroid/preference/Preference;

.field private bgW:Landroid/preference/CheckBoxPreference;

.field private bgX:Landroid/app/Dialog;

.field private bgY:Lmiui/preference/ValuePreference;

.field private bgZ:Z

.field private bgn:Landroid/app/Dialog;

.field private bgo:Landroid/preference/CheckBoxPreference;

.field private bgp:Landroid/preference/CheckBoxPreference;

.field private bgq:Landroid/app/Dialog;

.field private final bgr:Ljava/util/ArrayList;

.field private bgs:Landroid/preference/ListPreference;

.field private bgt:Landroid/preference/ListPreference;

.field private bgu:Landroid/app/backup/IBackupManager;

.field private bgv:Landroid/bluetooth/BluetoothA2dp;

.field private final bgw:Ljava/lang/Object;

.field private bgx:Landroid/content/BroadcastReceiver;

.field private bgy:Landroid/bluetooth/BluetoothProfile$ServiceListener;

.field private bgz:Landroid/preference/CheckBoxPreference;

.field private bhA:Landroid/service/persistentdata/PersistentDataBlockManager;

.field private bhB:Landroid/preference/ListPreference;

.field private bhC:Landroid/preference/Preference;

.field private bhD:Landroid/preference/CheckBoxPreference;

.field private final bhE:Ljava/util/ArrayList;

.field private bhF:Landroid/preference/PreferenceCategory;

.field private bhG:Lcom/android/settings/development/b;

.field private bhH:Landroid/preference/CheckBoxPreference;

.field private bhI:Landroid/preference/CheckBoxPreference;

.field private bhJ:Landroid/preference/CheckBoxPreference;

.field private bhK:Landroid/preference/CheckBoxPreference;

.field private bhL:Landroid/preference/ListPreference;

.field private bhM:Landroid/preference/CheckBoxPreference;

.field private bhN:Landroid/preference/CheckBoxPreference;

.field private bhO:Landroid/preference/CheckBoxPreference;

.field private bhP:Landroid/preference/ListPreference;

.field private bhQ:Landroid/preference/CheckBoxPreference;

.field private bhR:Landroid/telephony/TelephonyManager;

.field private bhS:Lcom/android/settings/development/g;

.field private bhT:Landroid/preference/CheckBoxPreference;

.field private bhU:Landroid/preference/ListPreference;

.field private bhV:Landroid/preference/ListPreference;

.field private bhW:Landroid/preference/CheckBoxPreference;

.field private bhX:Landroid/os/UserManager;

.field private bhY:Z

.field private bhZ:Landroid/preference/ListPreference;

.field private bha:Landroid/preference/CheckBoxPreference;

.field private final bhb:Ljava/util/HashSet;

.field private bhc:Z

.field private bhd:Landroid/preference/CheckBoxPreference;

.field private bhe:Landroid/app/Dialog;

.field private bhf:Lcom/android/settingslib/MiuiRestrictedSwitchPreference;

.field private bhg:Landroid/preference/CheckBoxPreference;

.field private bhh:Landroid/preference/CheckBoxPreference;

.field private bhi:Landroid/preference/CheckBoxPreference;

.field private bhj:Landroid/preference/CheckBoxPreference;

.field private bhk:Landroid/preference/CheckBoxPreference;

.field private bhl:Landroid/preference/CheckBoxPreference;

.field private bhm:Landroid/preference/CheckBoxPreference;

.field private bhn:Landroid/preference/CheckBoxPreference;

.field private bho:Z

.field private bhp:Landroid/preference/CheckBoxPreference;

.field private bhq:Landroid/preference/CheckBoxPreference;

.field private bhr:Lcom/android/settings/development/d;

.field private bhs:Landroid/preference/ListPreference;

.field private bht:Landroid/preference/ListPreference;

.field private bhu:Landroid/preference/ListPreference;

.field private bhv:Landroid/app/Dialog;

.field private bhw:Z

.field private bhx:Landroid/preference/CheckBoxPreference;

.field private bhy:Ljava/lang/String;

.field private bhz:Landroid/preference/Preference;

.field private bia:Landroid/content/BroadcastReceiver;

.field private bib:Lcom/android/settings/development/c;

.field private bic:Landroid/preference/CheckBoxPreference;

.field private bid:Lcom/android/settings/webview/a;

.field private bie:Landroid/webkit/IWebViewUpdateService;

.field private bif:Landroid/preference/CheckBoxPreference;

.field private big:Landroid/preference/CheckBoxPreference;

.field private bih:Landroid/preference/CheckBoxPreference;

.field private bii:Landroid/net/wifi/WifiManager;

.field private bij:Landroid/preference/CheckBoxPreference;

.field private bik:Landroid/preference/ListPreference;

.field private bil:Landroid/view/IWindowManager;

.field private mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/4 v0, 0x1

    new-array v0, v0, [I

    const/16 v1, 0x3a

    const/4 v2, 0x0

    aput v1, v0, v2

    sput-object v0, Lcom/android/settings/development/DevelopmentSettings;->bgm:[I

    new-instance v0, Lcom/android/settings/development/m;

    invoke-direct {v0}, Lcom/android/settings/development/m;-><init>()V

    sput-object v0, Lcom/android/settings/development/DevelopmentSettings;->SEARCH_INDEX_DATA_PROVIDER:Lcom/android/settings/search/Indexable$SearchIndexProvider;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    const-string/jumbo v0, "no_debugging_features"

    invoke-direct {p0, v0}, Lcom/android/settings/MiuiRestrictedSettingsFragment;-><init>(Ljava/lang/String;)V

    const-string/jumbo v0, "btdebug_enabled"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lmiui/util/FeatureParser;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/settings/development/DevelopmentSettings;->bgI:Z

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/android/settings/development/DevelopmentSettings;->bgw:Ljava/lang/Object;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/settings/development/DevelopmentSettings;->bgr:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/settings/development/DevelopmentSettings;->bhE:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/android/settings/development/DevelopmentSettings;->bhb:Ljava/util/HashSet;

    new-instance v0, Lcom/android/settings/development/j;

    invoke-direct {v0, p0}, Lcom/android/settings/development/j;-><init>(Lcom/android/settings/development/DevelopmentSettings;)V

    iput-object v0, p0, Lcom/android/settings/development/DevelopmentSettings;->bia:Landroid/content/BroadcastReceiver;

    new-instance v0, Lcom/android/settings/development/k;

    invoke-direct {v0, p0}, Lcom/android/settings/development/k;-><init>(Lcom/android/settings/development/DevelopmentSettings;)V

    iput-object v0, p0, Lcom/android/settings/development/DevelopmentSettings;->bgx:Landroid/content/BroadcastReceiver;

    new-instance v0, Lcom/android/settings/development/l;

    invoke-direct {v0, p0}, Lcom/android/settings/development/l;-><init>(Lcom/android/settings/development/DevelopmentSettings;)V

    iput-object v0, p0, Lcom/android/settings/development/DevelopmentSettings;->bgy:Landroid/bluetooth/BluetoothProfile$ServiceListener;

    return-void
.end method

.method private static aUA()I
    .locals 2

    const/4 v1, 0x0

    const-string/jumbo v0, "persist.sys.strictmode.visual"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    return v1

    :cond_0
    const-string/jumbo v0, "persist.sys.strictmode.visual"

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x2

    goto :goto_0
.end method

.method private aUB()Ljava/lang/String;
    .locals 2

    const-string/jumbo v0, "ro.logd.size"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    const-string/jumbo v0, "ro.config.low_ram"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "true"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string/jumbo v0, "65536"

    :cond_1
    :goto_0
    return-object v0

    :cond_2
    const-string/jumbo v0, "262144"

    goto :goto_0
.end method

.method private aUC(Landroid/preference/Preference;)V
    .locals 1

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/preference/Preference;->setEnabled(Z)V

    iget-object v0, p0, Lcom/android/settings/development/DevelopmentSettings;->bhb:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    :cond_0
    return-void
.end method

.method private aUD()V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/android/settings/development/DevelopmentSettings;->bgn:Landroid/app/Dialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/development/DevelopmentSettings;->bgn:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    iput-object v1, p0, Lcom/android/settings/development/DevelopmentSettings;->bgn:Landroid/app/Dialog;

    :cond_0
    iget-object v0, p0, Lcom/android/settings/development/DevelopmentSettings;->bgq:Landroid/app/Dialog;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/settings/development/DevelopmentSettings;->bgq:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    iput-object v1, p0, Lcom/android/settings/development/DevelopmentSettings;->bgq:Landroid/app/Dialog;

    :cond_1
    iget-object v0, p0, Lcom/android/settings/development/DevelopmentSettings;->bhe:Landroid/app/Dialog;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/settings/development/DevelopmentSettings;->bhe:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    iput-object v1, p0, Lcom/android/settings/development/DevelopmentSettings;->bhe:Landroid/app/Dialog;

    :cond_2
    iget-object v0, p0, Lcom/android/settings/development/DevelopmentSettings;->bhv:Landroid/app/Dialog;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/android/settings/development/DevelopmentSettings;->bhv:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    iput-object v1, p0, Lcom/android/settings/development/DevelopmentSettings;->bhv:Landroid/app/Dialog;

    :cond_3
    iget-object v0, p0, Lcom/android/settings/development/DevelopmentSettings;->bgX:Landroid/app/Dialog;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/android/settings/development/DevelopmentSettings;->bgX:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    iput-object v1, p0, Lcom/android/settings/development/DevelopmentSettings;->bgX:Landroid/app/Dialog;

    :cond_4
    return-void
.end method

.method private aUE()Z
    .locals 1

    invoke-direct {p0}, Lcom/android/settings/development/DevelopmentSettings;->aUJ()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/development/DevelopmentSettings;->bhX:Landroid/os/UserManager;

    invoke-static {v0}, Lcom/android/settings/development/f;->aWY(Landroid/os/UserManager;)Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private aUF(Ljava/lang/String;)Landroid/preference/CheckBoxPreference;
    .locals 3

    invoke-virtual {p0, p1}, Lcom/android/settings/development/DevelopmentSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Cannot find preference with key = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-object v1, p0, Lcom/android/settings/development/DevelopmentSettings;->bgr:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v1, p0, Lcom/android/settings/development/DevelopmentSettings;->bhE:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-object v0
.end method

.method private aUG(Ljava/lang/String;)Landroid/preference/SwitchPreference;
    .locals 3

    invoke-virtual {p0, p1}, Lcom/android/settings/development/DevelopmentSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/SwitchPreference;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Cannot find preference with key = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-object v1, p0, Lcom/android/settings/development/DevelopmentSettings;->bgr:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-object v0
.end method

.method private aUH()I
    .locals 4

    invoke-virtual {p0}, Lcom/android/settings/development/DevelopmentSettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v1, "second_user_id"

    const/16 v2, -0x2710

    const/4 v3, 0x0

    invoke-static {v0, v1, v2, v3}, Landroid/provider/Settings$Secure;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v0

    return v0
.end method

.method private aUI()V
    .locals 5

    const/4 v4, 0x0

    invoke-virtual {p0}, Lcom/android/settings/development/DevelopmentSettings;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f030037

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lcom/android/settings/development/DevelopmentSettings;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f030038

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "persist.bluetooth.avrcpversion"

    aget-object v3, v0, v4

    invoke-static {v2, v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/android/settings/development/DevelopmentSettings;->bgG:Landroid/preference/ListPreference;

    invoke-virtual {v3, v2}, Landroid/preference/ListPreference;->findIndexOfValue(Ljava/lang/String;)I

    move-result v2

    iget-object v3, p0, Lcom/android/settings/development/DevelopmentSettings;->bgG:Landroid/preference/ListPreference;

    aget-object v0, v0, v2

    invoke-virtual {v3, v0}, Landroid/preference/ListPreference;->setValue(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/settings/development/DevelopmentSettings;->bgG:Landroid/preference/ListPreference;

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Landroid/preference/ListPreference;->setSummary(Ljava/lang/CharSequence;)V

    invoke-virtual {p0}, Lcom/android/settings/development/DevelopmentSettings;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f030036

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lcom/android/settings/development/DevelopmentSettings;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f030034

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/android/settings/development/DevelopmentSettings;->bgD:Landroid/preference/ListPreference;

    aget-object v0, v0, v4

    invoke-virtual {v2, v0}, Landroid/preference/ListPreference;->setValue(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/settings/development/DevelopmentSettings;->bgD:Landroid/preference/ListPreference;

    aget-object v1, v1, v4

    invoke-virtual {v0, v1}, Landroid/preference/ListPreference;->setSummary(Ljava/lang/CharSequence;)V

    invoke-virtual {p0}, Lcom/android/settings/development/DevelopmentSettings;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f030033

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lcom/android/settings/development/DevelopmentSettings;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f030031

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/android/settings/development/DevelopmentSettings;->bgF:Landroid/preference/ListPreference;

    aget-object v0, v0, v4

    invoke-virtual {v2, v0}, Landroid/preference/ListPreference;->setValue(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/settings/development/DevelopmentSettings;->bgF:Landroid/preference/ListPreference;

    aget-object v1, v1, v4

    invoke-virtual {v0, v1}, Landroid/preference/ListPreference;->setSummary(Ljava/lang/CharSequence;)V

    invoke-virtual {p0}, Lcom/android/settings/development/DevelopmentSettings;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f03002a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lcom/android/settings/development/DevelopmentSettings;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f030028

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/android/settings/development/DevelopmentSettings;->bgB:Landroid/preference/ListPreference;

    aget-object v0, v0, v4

    invoke-virtual {v2, v0}, Landroid/preference/ListPreference;->setValue(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/settings/development/DevelopmentSettings;->bgB:Landroid/preference/ListPreference;

    aget-object v1, v1, v4

    invoke-virtual {v0, v1}, Landroid/preference/ListPreference;->setSummary(Ljava/lang/CharSequence;)V

    invoke-virtual {p0}, Lcom/android/settings/development/DevelopmentSettings;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f03002d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lcom/android/settings/development/DevelopmentSettings;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f03002b

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/android/settings/development/DevelopmentSettings;->bgC:Landroid/preference/ListPreference;

    aget-object v0, v0, v4

    invoke-virtual {v2, v0}, Landroid/preference/ListPreference;->setValue(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/settings/development/DevelopmentSettings;->bgC:Landroid/preference/ListPreference;

    aget-object v1, v1, v4

    invoke-virtual {v0, v1}, Landroid/preference/ListPreference;->setSummary(Ljava/lang/CharSequence;)V

    invoke-virtual {p0}, Lcom/android/settings/development/DevelopmentSettings;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f030030

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lcom/android/settings/development/DevelopmentSettings;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f03002e

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x3

    iget-object v3, p0, Lcom/android/settings/development/DevelopmentSettings;->bgE:Landroid/preference/ListPreference;

    aget-object v0, v0, v2

    invoke-virtual {v3, v0}, Landroid/preference/ListPreference;->setValue(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/settings/development/DevelopmentSettings;->bgE:Landroid/preference/ListPreference;

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Landroid/preference/ListPreference;->setSummary(Ljava/lang/CharSequence;)V

    return-void
.end method

.method private aUJ()Z
    .locals 3

    const/4 v0, 0x0

    const/4 v1, -0x1

    iget-object v2, p0, Lcom/android/settings/development/DevelopmentSettings;->bhA:Landroid/service/persistentdata/PersistentDataBlockManager;

    if-eqz v2, :cond_0

    iget-object v1, p0, Lcom/android/settings/development/DevelopmentSettings;->bhA:Landroid/service/persistentdata/PersistentDataBlockManager;

    invoke-virtual {v1}, Landroid/service/persistentdata/PersistentDataBlockManager;->getFlashLockState()I

    move-result v1

    :cond_0
    if-nez v1, :cond_1

    const/4 v0, 0x1

    :cond_1
    return v0
.end method

.method private static aUK(Landroid/content/Context;Ljava/lang/String;)Z
    .locals 3

    const/4 v0, 0x0

    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, p1, v2}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0

    :catch_0
    move-exception v1

    return v0
.end method

.method private aUL()Z
    .locals 4

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/android/settings/development/DevelopmentSettings;->bhR:Landroid/telephony/TelephonyManager;

    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getPhoneCount()I

    move-result v2

    move v0, v1

    :goto_0
    if-ge v0, v2, :cond_1

    iget-object v3, p0, Lcom/android/settings/development/DevelopmentSettings;->bhR:Landroid/telephony/TelephonyManager;

    invoke-virtual {v3, v0}, Landroid/telephony/TelephonyManager;->getAllowedCarriers(I)Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-lez v3, :cond_0

    const/4 v0, 0x1

    return v0

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    return v1
.end method

.method private aUN(Landroid/preference/Preference;)V
    .locals 1

    invoke-virtual {p0}, Lcom/android/settings/development/DevelopmentSettings;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    iget-object v0, p0, Lcom/android/settings/development/DevelopmentSettings;->bgr:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/android/settings/development/DevelopmentSettings;->bhE:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    return-void
.end method

.method private aUO(Landroid/preference/Preference;)Z
    .locals 2

    const-string/jumbo v0, "user"

    sget-object v1, Landroid/os/Build;->TYPE:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0, p1}, Lcom/android/settings/development/DevelopmentSettings;->aUN(Landroid/preference/Preference;)V

    const/4 v0, 0x1

    return v0

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method private aUP()V
    .locals 6

    const/4 v5, 0x1

    const/4 v2, 0x0

    const/4 v4, 0x0

    iput-boolean v5, p0, Lcom/android/settings/development/DevelopmentSettings;->bhc:Z

    move v1, v2

    :goto_0
    iget-object v0, p0, Lcom/android/settings/development/DevelopmentSettings;->bhE:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    iget-object v0, p0, Lcom/android/settings/development/DevelopmentSettings;->bhE:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v0, v2}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    invoke-virtual {p0, v0}, Lcom/android/settings/development/DevelopmentSettings;->at(Landroid/preference/Preference;)Z

    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/android/settings/development/DevelopmentSettings;->bhf:Lcom/android/settingslib/MiuiRestrictedSwitchPreference;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/settings/development/DevelopmentSettings;->bhf:Lcom/android/settingslib/MiuiRestrictedSwitchPreference;

    invoke-virtual {v0}, Lcom/android/settingslib/MiuiRestrictedSwitchPreference;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/settings/development/DevelopmentSettings;->bhf:Lcom/android/settingslib/MiuiRestrictedSwitchPreference;

    invoke-virtual {v0, v2}, Lcom/android/settingslib/MiuiRestrictedSwitchPreference;->setChecked(Z)V

    iget-object v0, p0, Lcom/android/settings/development/DevelopmentSettings;->bhf:Lcom/android/settingslib/MiuiRestrictedSwitchPreference;

    invoke-virtual {p0, v0}, Lcom/android/settings/development/DevelopmentSettings;->at(Landroid/preference/Preference;)Z

    :cond_2
    invoke-static {}, Lcom/android/settings/development/DevelopmentSettings;->aUQ()V

    invoke-direct {p0, v4, v5}, Lcom/android/settings/development/DevelopmentSettings;->aWp(Ljava/lang/Object;Z)V

    invoke-direct {p0, v4}, Lcom/android/settings/development/DevelopmentSettings;->aWo(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/android/settings/development/DevelopmentSettings;->bik:Landroid/preference/ListPreference;

    invoke-direct {p0, v2, v0, v4}, Lcom/android/settings/development/DevelopmentSettings;->aVU(ILandroid/preference/ListPreference;Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/android/settings/development/DevelopmentSettings;->bhV:Landroid/preference/ListPreference;

    invoke-direct {p0, v5, v0, v4}, Lcom/android/settings/development/DevelopmentSettings;->aVU(ILandroid/preference/ListPreference;Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/android/settings/development/DevelopmentSettings;->bgs:Landroid/preference/ListPreference;

    const/4 v1, 0x2

    invoke-direct {p0, v1, v0, v4}, Lcom/android/settings/development/DevelopmentSettings;->aVU(ILandroid/preference/ListPreference;Ljava/lang/Object;)V

    invoke-direct {p0}, Lcom/android/settings/development/DevelopmentSettings;->aVT()Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v0, -0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/android/settings/development/DevelopmentSettings;->aWE(Ljava/lang/Object;)V

    :cond_3
    invoke-direct {p0, v4}, Lcom/android/settings/development/DevelopmentSettings;->aWu(Ljava/lang/Object;)V

    invoke-direct {p0, v4}, Lcom/android/settings/development/DevelopmentSettings;->aVV(Ljava/lang/Object;)V

    iput-boolean v2, p0, Lcom/android/settings/development/DevelopmentSettings;->bho:Z

    invoke-direct {p0}, Lcom/android/settings/development/DevelopmentSettings;->aUW()V

    iput-boolean v2, p0, Lcom/android/settings/development/DevelopmentSettings;->bhc:Z

    invoke-virtual {p0}, Lcom/android/settings/development/DevelopmentSettings;->aUM()V

    return-void
.end method

.method private static aUQ()V
    .locals 4

    :try_start_0
    invoke-static {}, Landroid/app/ActivityManager;->getService()Landroid/app/IActivityManager;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x1

    invoke-interface {v0, v1, v2, v3}, Landroid/app/IActivityManager;->setDebugApp(Ljava/lang/String;ZZ)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method private aUR()V
    .locals 3

    const-string/jumbo v0, "shortcut"

    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Landroid/content/pm/IShortcutService$Stub;->asInterface(Landroid/os/IBinder;)Landroid/content/pm/IShortcutService;

    move-result-object v0

    if-eqz v0, :cond_0

    :try_start_0
    invoke-interface {v0}, Landroid/content/pm/IShortcutService;->resetThrottling()V

    invoke-virtual {p0}, Lcom/android/settings/development/DevelopmentSettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const v1, 0x7f120e26

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string/jumbo v1, "DevelopmentSettings"

    const-string/jumbo v2, "Failed to reset rate limiting"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method private aUS(Z)V
    .locals 4

    const/4 v2, 0x0

    move v1, v2

    :goto_0
    iget-object v0, p0, Lcom/android/settings/development/DevelopmentSettings;->bgr:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    iget-object v0, p0, Lcom/android/settings/development/DevelopmentSettings;->bgr:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/preference/Preference;

    if-eqz v0, :cond_0

    if-eqz p1, :cond_1

    iget-object v3, p0, Lcom/android/settings/development/DevelopmentSettings;->bhb:Ljava/util/HashSet;

    invoke-virtual {v3, v0}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v3

    xor-int/lit8 v3, v3, 0x1

    :goto_1
    invoke-virtual {v0, v3}, Landroid/preference/Preference;->setEnabled(Z)V

    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    move v3, v2

    goto :goto_1

    :cond_2
    iget-object v0, p0, Lcom/android/settings/development/DevelopmentSettings;->bhS:Lcom/android/settings/development/g;

    invoke-virtual {v0, p1}, Lcom/android/settings/development/g;->aXb(Z)V

    iget-object v0, p0, Lcom/android/settings/development/DevelopmentSettings;->bid:Lcom/android/settings/webview/a;

    invoke-virtual {v0, p1}, Lcom/android/settings/webview/a;->bcj(Z)V

    invoke-direct {p0}, Lcom/android/settings/development/DevelopmentSettings;->aUW()V

    return-void
.end method

.method private aUT()V
    .locals 3

    iget-object v0, p0, Lcom/android/settings/development/DevelopmentSettings;->bgX:Landroid/app/Dialog;

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/android/settings/development/DevelopmentSettings;->aUD()V

    :cond_0
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/android/settings/development/DevelopmentSettings;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {p0}, Lcom/android/settings/development/DevelopmentSettings;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f120c21

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f120570

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f120c20

    invoke-virtual {v0, v1, p0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f12041a

    invoke-virtual {v0, v1, p0}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/development/DevelopmentSettings;->bgX:Landroid/app/Dialog;

    iget-object v0, p0, Lcom/android/settings/development/DevelopmentSettings;->bgX:Landroid/app/Dialog;

    invoke-virtual {v0, p0}, Landroid/app/Dialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    return-void
.end method

.method private static aUU()Z
    .locals 2

    const-string/jumbo v0, "ro.frp.pst"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    return v0
.end method

.method private aUV(Landroid/content/res/Resources;I)Z
    .locals 2

    new-instance v0, Lcom/android/settings/cx;

    invoke-virtual {p0}, Lcom/android/settings/development/DevelopmentSettings;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, v1, p0}, Lcom/android/settings/cx;-><init>(Landroid/app/Activity;Landroid/app/Fragment;)V

    const v1, 0x7f120c0a

    invoke-virtual {p1, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p2, v1}, Lcom/android/settings/cx;->bSO(ILjava/lang/CharSequence;)Z

    move-result v0

    return v0
.end method

.method private aUW()V
    .locals 6

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/android/settings/development/DevelopmentSettings;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    iput-boolean v2, p0, Lcom/android/settings/development/DevelopmentSettings;->bho:Z

    iget-object v5, p0, Lcom/android/settings/development/DevelopmentSettings;->bhd:Landroid/preference/CheckBoxPreference;

    const-string/jumbo v0, "adb_enabled"

    invoke-static {v4, v0, v2}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    if-eqz v0, :cond_4

    move v0, v1

    :goto_0
    invoke-virtual {p0, v5, v0}, Lcom/android/settings/development/DevelopmentSettings;->aVK(Landroid/preference/CheckBoxPreference;Z)V

    iget-object v0, p0, Lcom/android/settings/development/DevelopmentSettings;->bhg:Landroid/preference/CheckBoxPreference;

    if-eqz v0, :cond_0

    iget-object v5, p0, Lcom/android/settings/development/DevelopmentSettings;->bhg:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v3}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    const-string/jumbo v3, "com.android.terminal"

    invoke-virtual {v0, v3}, Landroid/content/pm/PackageManager;->getApplicationEnabledSetting(Ljava/lang/String;)I

    move-result v0

    if-ne v0, v1, :cond_5

    move v0, v1

    :goto_1
    invoke-virtual {p0, v5, v0}, Lcom/android/settings/development/DevelopmentSettings;->aVK(Landroid/preference/CheckBoxPreference;Z)V

    :cond_0
    iget-boolean v0, p0, Lcom/android/settings/development/DevelopmentSettings;->bho:Z

    iget-object v3, p0, Lcom/android/settings/development/DevelopmentSettings;->bhS:Lcom/android/settings/development/g;

    invoke-virtual {v3}, Lcom/android/settings/development/g;->aXc()Z

    move-result v3

    or-int/2addr v0, v3

    iput-boolean v0, p0, Lcom/android/settings/development/DevelopmentSettings;->bho:Z

    iget-object v3, p0, Lcom/android/settings/development/DevelopmentSettings;->bhq:Landroid/preference/CheckBoxPreference;

    const-string/jumbo v0, "stay_on_while_plugged_in"

    invoke-static {v4, v0, v2}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    if-eqz v0, :cond_6

    move v0, v1

    :goto_2
    invoke-virtual {p0, v3, v0}, Lcom/android/settings/development/DevelopmentSettings;->aVK(Landroid/preference/CheckBoxPreference;Z)V

    iget-object v0, p0, Lcom/android/settings/development/DevelopmentSettings;->bgJ:Landroid/preference/CheckBoxPreference;

    const-string/jumbo v3, "persist.bluetooth.btsnoopenable"

    invoke-static {v3, v2}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    invoke-virtual {p0, v0, v3}, Lcom/android/settings/development/DevelopmentSettings;->aVK(Landroid/preference/CheckBoxPreference;Z)V

    invoke-direct {p0}, Lcom/android/settings/development/DevelopmentSettings;->aVj()V

    iget-object v3, p0, Lcom/android/settings/development/DevelopmentSettings;->bgU:Landroid/preference/CheckBoxPreference;

    const-string/jumbo v0, "debug_view_attributes"

    invoke-static {v4, v0, v2}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    if-eqz v0, :cond_7

    move v0, v1

    :goto_3
    invoke-virtual {p0, v3, v0}, Lcom/android/settings/development/DevelopmentSettings;->aVK(Landroid/preference/CheckBoxPreference;Z)V

    iget-object v3, p0, Lcom/android/settings/development/DevelopmentSettings;->bhj:Landroid/preference/CheckBoxPreference;

    const-string/jumbo v0, "force_allow_on_external"

    invoke-static {v4, v0, v2}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    if-eqz v0, :cond_8

    move v0, v1

    :goto_4
    invoke-virtual {p0, v3, v0}, Lcom/android/settings/development/DevelopmentSettings;->aVK(Landroid/preference/CheckBoxPreference;Z)V

    iget-object v0, p0, Lcom/android/settings/development/DevelopmentSettings;->bgK:Landroid/preference/CheckBoxPreference;

    const-string/jumbo v3, "bluetooth_trace_log"

    invoke-static {v4, v3, v2}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v3

    if-eqz v3, :cond_9

    :goto_5
    invoke-virtual {p0, v0, v1}, Lcom/android/settings/development/DevelopmentSettings;->aVK(Landroid/preference/CheckBoxPreference;Z)V

    invoke-direct {p0}, Lcom/android/settings/development/DevelopmentSettings;->aVL()V

    invoke-direct {p0}, Lcom/android/settings/development/DevelopmentSettings;->aVo()V

    invoke-direct {p0}, Lcom/android/settings/development/DevelopmentSettings;->aVi()V

    invoke-direct {p0}, Lcom/android/settings/development/DevelopmentSettings;->aVu()V

    invoke-direct {p0}, Lcom/android/settings/development/DevelopmentSettings;->aVJ()V

    invoke-direct {p0}, Lcom/android/settings/development/DevelopmentSettings;->aVz()V

    invoke-direct {p0}, Lcom/android/settings/development/DevelopmentSettings;->aVH()V

    invoke-direct {p0}, Lcom/android/settings/development/DevelopmentSettings;->aVk()V

    invoke-direct {p0}, Lcom/android/settings/development/DevelopmentSettings;->aVn()V

    invoke-direct {p0}, Lcom/android/settings/development/DevelopmentSettings;->aVv()V

    invoke-direct {p0}, Lcom/android/settings/development/DevelopmentSettings;->aVM()V

    invoke-direct {p0}, Lcom/android/settings/development/DevelopmentSettings;->aVF()V

    invoke-direct {p0}, Lcom/android/settings/development/DevelopmentSettings;->aVE()V

    invoke-direct {p0}, Lcom/android/settings/development/DevelopmentSettings;->aVD()V

    invoke-direct {p0}, Lcom/android/settings/development/DevelopmentSettings;->aVf()V

    invoke-direct {p0}, Lcom/android/settings/development/DevelopmentSettings;->aVh()V

    invoke-direct {p0}, Lcom/android/settings/development/DevelopmentSettings;->aUX()V

    invoke-direct {p0}, Lcom/android/settings/development/DevelopmentSettings;->aVy()V

    invoke-direct {p0}, Lcom/android/settings/development/DevelopmentSettings;->aVp()V

    invoke-direct {p0}, Lcom/android/settings/development/DevelopmentSettings;->aUZ()V

    invoke-direct {p0}, Lcom/android/settings/development/DevelopmentSettings;->aVB()V

    invoke-direct {p0}, Lcom/android/settings/development/DevelopmentSettings;->aVC()V

    invoke-direct {p0}, Lcom/android/settings/development/DevelopmentSettings;->aVG()V

    iget-object v0, p0, Lcom/android/settings/development/DevelopmentSettings;->bib:Lcom/android/settings/development/c;

    invoke-virtual {v0}, Lcom/android/settings/development/c;->aUu()V

    invoke-direct {p0}, Lcom/android/settings/development/DevelopmentSettings;->aVd()V

    invoke-direct {p0}, Lcom/android/settings/development/DevelopmentSettings;->aVm()V

    invoke-direct {p0}, Lcom/android/settings/development/DevelopmentSettings;->aVr()V

    invoke-direct {p0}, Lcom/android/settings/development/DevelopmentSettings;->aVq()V

    invoke-direct {p0}, Lcom/android/settings/development/DevelopmentSettings;->aVs()V

    invoke-direct {p0}, Lcom/android/settings/development/DevelopmentSettings;->aVR()V

    invoke-direct {p0}, Lcom/android/settings/development/DevelopmentSettings;->aVS()V

    iget-object v0, p0, Lcom/android/settings/development/DevelopmentSettings;->bif:Landroid/preference/CheckBoxPreference;

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lcom/android/settings/development/DevelopmentSettings;->aVP()V

    :cond_1
    invoke-direct {p0}, Lcom/android/settings/development/DevelopmentSettings;->aVQ()V

    iget-object v0, p0, Lcom/android/settings/development/DevelopmentSettings;->bhx:Landroid/preference/CheckBoxPreference;

    if-eqz v0, :cond_2

    invoke-direct {p0}, Lcom/android/settings/development/DevelopmentSettings;->aVt()V

    :cond_2
    invoke-direct {p0}, Lcom/android/settings/development/DevelopmentSettings;->aVI()V

    invoke-direct {p0}, Lcom/android/settings/development/DevelopmentSettings;->aVN()V

    invoke-direct {p0}, Lcom/android/settings/development/DevelopmentSettings;->aVl()V

    iget-object v0, p0, Lcom/android/settings/development/DevelopmentSettings;->bid:Lcom/android/settings/webview/a;

    invoke-virtual {v0}, Lcom/android/settings/webview/a;->l()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/settings/development/DevelopmentSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/development/DevelopmentSettings;->bid:Lcom/android/settings/webview/a;

    invoke-virtual {v1, v0}, Lcom/android/settings/webview/a;->cz(Landroid/preference/Preference;)V

    invoke-direct {p0}, Lcom/android/settings/development/DevelopmentSettings;->aVw()V

    iget-object v0, p0, Lcom/android/settings/development/DevelopmentSettings;->bgO:Landroid/preference/CheckBoxPreference;

    if-eqz v0, :cond_3

    invoke-direct {p0}, Lcom/android/settings/development/DevelopmentSettings;->aVe()V

    :cond_3
    invoke-direct {p0}, Lcom/android/settings/development/DevelopmentSettings;->aVb()V

    invoke-direct {p0}, Lcom/android/settings/development/DevelopmentSettings;->aVc()V

    invoke-direct {p0}, Lcom/android/settings/development/DevelopmentSettings;->aVa()V

    return-void

    :cond_4
    move v0, v2

    goto/16 :goto_0

    :cond_5
    move v0, v2

    goto/16 :goto_1

    :cond_6
    move v0, v2

    goto/16 :goto_2

    :cond_7
    move v0, v2

    goto/16 :goto_3

    :cond_8
    move v0, v2

    goto/16 :goto_4

    :cond_9
    move v1, v2

    goto/16 :goto_5
.end method

.method private aUX()V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/development/DevelopmentSettings;->bik:Landroid/preference/ListPreference;

    const/4 v1, 0x0

    invoke-direct {p0, v1, v0}, Lcom/android/settings/development/DevelopmentSettings;->aUY(ILandroid/preference/ListPreference;)V

    iget-object v0, p0, Lcom/android/settings/development/DevelopmentSettings;->bhV:Landroid/preference/ListPreference;

    const/4 v1, 0x1

    invoke-direct {p0, v1, v0}, Lcom/android/settings/development/DevelopmentSettings;->aUY(ILandroid/preference/ListPreference;)V

    iget-object v0, p0, Lcom/android/settings/development/DevelopmentSettings;->bgs:Landroid/preference/ListPreference;

    const/4 v1, 0x2

    invoke-direct {p0, v1, v0}, Lcom/android/settings/development/DevelopmentSettings;->aUY(ILandroid/preference/ListPreference;)V

    return-void
.end method

.method private aUY(ILandroid/preference/ListPreference;)V
    .locals 4

    const/4 v0, 0x0

    :try_start_0
    iget-object v1, p0, Lcom/android/settings/development/DevelopmentSettings;->bil:Landroid/view/IWindowManager;

    invoke-interface {v1, p1}, Landroid/view/IWindowManager;->getAnimationScale(I)F

    move-result v1

    const/high16 v2, 0x3f800000    # 1.0f

    cmpl-float v2, v1, v2

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/android/settings/development/DevelopmentSettings;->bho:Z

    :cond_0
    invoke-virtual {p2}, Landroid/preference/ListPreference;->getEntryValues()[Ljava/lang/CharSequence;

    move-result-object v2

    :goto_0
    array-length v3, v2

    if-ge v0, v3, :cond_2

    aget-object v3, v2, v0

    invoke-interface {v3}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v3

    cmpg-float v3, v1, v3

    if-gtz v3, :cond_1

    invoke-virtual {p2, v0}, Landroid/preference/ListPreference;->setValueIndex(I)V

    invoke-virtual {p2}, Landroid/preference/ListPreference;->getEntries()[Ljava/lang/CharSequence;

    move-result-object v1

    aget-object v0, v1, v0

    invoke-virtual {p2, v0}, Landroid/preference/ListPreference;->setSummary(Ljava/lang/CharSequence;)V

    return-void

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    array-length v0, v2

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {p2, v0}, Landroid/preference/ListPreference;->setValueIndex(I)V

    invoke-virtual {p2}, Landroid/preference/ListPreference;->getEntries()[Ljava/lang/CharSequence;

    move-result-object v0

    const/4 v1, 0x0

    aget-object v0, v0, v1

    invoke-virtual {p2, v0}, Landroid/preference/ListPreference;->setSummary(Ljava/lang/CharSequence;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_1
    return-void

    :catch_0
    move-exception v0

    goto :goto_1
.end method

.method private aUZ()V
    .locals 4

    const/4 v0, 0x0

    :try_start_0
    invoke-static {}, Landroid/app/ActivityManager;->getService()Landroid/app/IActivityManager;

    move-result-object v1

    invoke-interface {v1}, Landroid/app/IActivityManager;->getProcessLimit()I

    move-result v1

    iget-object v2, p0, Lcom/android/settings/development/DevelopmentSettings;->bgt:Landroid/preference/ListPreference;

    invoke-virtual {v2}, Landroid/preference/ListPreference;->getEntryValues()[Ljava/lang/CharSequence;

    move-result-object v2

    :goto_0
    array-length v3, v2

    if-ge v0, v3, :cond_2

    aget-object v3, v2, v0

    invoke-interface {v3}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    if-lt v3, v1, :cond_1

    if-eqz v0, :cond_0

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/android/settings/development/DevelopmentSettings;->bho:Z

    :cond_0
    iget-object v1, p0, Lcom/android/settings/development/DevelopmentSettings;->bgt:Landroid/preference/ListPreference;

    invoke-virtual {v1, v0}, Landroid/preference/ListPreference;->setValueIndex(I)V

    iget-object v1, p0, Lcom/android/settings/development/DevelopmentSettings;->bgt:Landroid/preference/ListPreference;

    iget-object v2, p0, Lcom/android/settings/development/DevelopmentSettings;->bgt:Landroid/preference/ListPreference;

    invoke-virtual {v2}, Landroid/preference/ListPreference;->getEntries()[Ljava/lang/CharSequence;

    move-result-object v2

    aget-object v0, v2, v0

    invoke-virtual {v1, v0}, Landroid/preference/ListPreference;->setSummary(Ljava/lang/CharSequence;)V

    return-void

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/android/settings/development/DevelopmentSettings;->bgt:Landroid/preference/ListPreference;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/preference/ListPreference;->setValueIndex(I)V

    iget-object v0, p0, Lcom/android/settings/development/DevelopmentSettings;->bgt:Landroid/preference/ListPreference;

    iget-object v1, p0, Lcom/android/settings/development/DevelopmentSettings;->bgt:Landroid/preference/ListPreference;

    invoke-virtual {v1}, Landroid/preference/ListPreference;->getEntries()[Ljava/lang/CharSequence;

    move-result-object v1

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Landroid/preference/ListPreference;->setSummary(Ljava/lang/CharSequence;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_1
    return-void

    :catch_0
    move-exception v0

    goto :goto_1
.end method

.method private aUw(Ljava/lang/String;)Landroid/preference/ListPreference;
    .locals 2

    invoke-virtual {p0, p1}, Lcom/android/settings/development/DevelopmentSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/ListPreference;

    iget-object v1, p0, Lcom/android/settings/development/DevelopmentSettings;->bgr:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual {v0, p0}, Landroid/preference/ListPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    return-object v0
.end method

.method private aUx(Z)V
    .locals 4

    if-eqz p1, :cond_1

    :try_start_0
    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/development/DevelopmentSettings;->bil:Landroid/view/IWindowManager;

    invoke-static {}, Landroid/util/MiuiDisplayMetrics;->getFactoryDeviceDensity()I

    move-result v1

    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v2

    const/4 v3, 0x0

    invoke-interface {v0, v3, v1, v2}, Landroid/view/IWindowManager;->setForcedDisplayDensityForUser(III)V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/android/settings/development/DevelopmentSettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v1, "display_density_forced"

    invoke-static {}, Landroid/util/MiuiDisplayMetrics;->getFactoryDeviceDensity()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-static {v0, v1, v2, v3}, Landroid/provider/Settings$Secure;->putStringForUser(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;I)Z

    goto :goto_0

    :catch_0
    move-exception v0

    goto :goto_0

    :cond_1
    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lcom/android/settings/development/DevelopmentSettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v1, "display_density_forced"

    sget v2, Landroid/util/MiuiDisplayMetrics;->DENSITY_DEVICE:I

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-static {v0, v1, v2, v3}, Landroid/provider/Settings$Secure;->putStringForUser(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;I)Z

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/android/settings/development/DevelopmentSettings;->bil:Landroid/view/IWindowManager;

    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v1

    const/4 v2, 0x0

    invoke-interface {v0, v2, v1}, Landroid/view/IWindowManager;->clearForcedDisplayDensityForUser(II)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0
.end method

.method private aUz()V
    .locals 4

    new-instance v0, Lcom/android/settings/development/o;

    invoke-direct {v0, p0}, Lcom/android/settings/development/o;-><init>(Lcom/android/settings/development/DevelopmentSettings;)V

    new-instance v1, Lcom/android/settings/development/p;

    invoke-direct {v1, p0}, Lcom/android/settings/development/p;-><init>(Lcom/android/settings/development/DevelopmentSettings;)V

    new-instance v2, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/android/settings/development/DevelopmentSettings;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v3, 0x7f120463

    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    const v3, 0x7f120462

    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    const v3, 0x7f12067f

    invoke-virtual {v2, v3, v0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const/high16 v2, 0x1040000

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    return-void
.end method

.method private aVB()V
    .locals 4

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/android/settings/development/DevelopmentSettings;->bhH:Landroid/preference/CheckBoxPreference;

    invoke-virtual {p0}, Lcom/android/settings/development/DevelopmentSettings;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string/jumbo v3, "anr_show_background"

    invoke-static {v2, v3, v0}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    if-eqz v2, :cond_0

    const/4 v0, 0x1

    :cond_0
    invoke-virtual {p0, v1, v0}, Lcom/android/settings/development/DevelopmentSettings;->aVK(Landroid/preference/CheckBoxPreference;Z)V

    return-void
.end method

.method private aVC()V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/development/DevelopmentSettings;->bhI:Landroid/preference/CheckBoxPreference;

    invoke-virtual {p0}, Lcom/android/settings/development/DevelopmentSettings;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-static {v1}, Landroid/provider/MiuiSettings$Secure;->isForceCloseDialogEnabled(Landroid/content/Context;)Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    return-void
.end method

.method private aVD()V
    .locals 3

    iget-object v0, p0, Lcom/android/settings/development/DevelopmentSettings;->bhJ:Landroid/preference/CheckBoxPreference;

    const-string/jumbo v1, "debug.hwui.show_layers_updates"

    const/4 v2, 0x0

    invoke-static {v1, v2}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/android/settings/development/DevelopmentSettings;->aVK(Landroid/preference/CheckBoxPreference;Z)V

    return-void
.end method

.method private aVE()V
    .locals 3

    iget-object v0, p0, Lcom/android/settings/development/DevelopmentSettings;->bhK:Landroid/preference/CheckBoxPreference;

    const-string/jumbo v1, "debug.hwui.show_dirty_regions"

    const/4 v2, 0x0

    invoke-static {v1, v2}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/android/settings/development/DevelopmentSettings;->aVK(Landroid/preference/CheckBoxPreference;Z)V

    return-void
.end method

.method private aVF()V
    .locals 5

    const/4 v2, 0x0

    const-string/jumbo v0, "debug.hwui.show_non_rect_clip"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    const-string/jumbo v0, "hide"

    :cond_0
    iget-object v1, p0, Lcom/android/settings/development/DevelopmentSettings;->bhL:Landroid/preference/ListPreference;

    invoke-virtual {v1}, Landroid/preference/ListPreference;->getEntryValues()[Ljava/lang/CharSequence;

    move-result-object v3

    move v1, v2

    :goto_0
    array-length v4, v3

    if-ge v1, v4, :cond_2

    aget-object v4, v3, v1

    invoke-virtual {v0, v4}, Ljava/lang/String;->contentEquals(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_1

    iget-object v0, p0, Lcom/android/settings/development/DevelopmentSettings;->bhL:Landroid/preference/ListPreference;

    invoke-virtual {v0, v1}, Landroid/preference/ListPreference;->setValueIndex(I)V

    iget-object v0, p0, Lcom/android/settings/development/DevelopmentSettings;->bhL:Landroid/preference/ListPreference;

    iget-object v2, p0, Lcom/android/settings/development/DevelopmentSettings;->bhL:Landroid/preference/ListPreference;

    invoke-virtual {v2}, Landroid/preference/ListPreference;->getEntries()[Ljava/lang/CharSequence;

    move-result-object v2

    aget-object v1, v2, v1

    invoke-virtual {v0, v1}, Landroid/preference/ListPreference;->setSummary(Ljava/lang/CharSequence;)V

    return-void

    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/android/settings/development/DevelopmentSettings;->bhL:Landroid/preference/ListPreference;

    invoke-virtual {v0, v2}, Landroid/preference/ListPreference;->setValueIndex(I)V

    iget-object v0, p0, Lcom/android/settings/development/DevelopmentSettings;->bhL:Landroid/preference/ListPreference;

    iget-object v1, p0, Lcom/android/settings/development/DevelopmentSettings;->bhL:Landroid/preference/ListPreference;

    invoke-virtual {v1}, Landroid/preference/ListPreference;->getEntries()[Ljava/lang/CharSequence;

    move-result-object v1

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Landroid/preference/ListPreference;->setSummary(Ljava/lang/CharSequence;)V

    return-void
.end method

.method private aVG()V
    .locals 6

    const/4 v1, 0x1

    const/4 v2, 0x0

    sget-boolean v0, Landroid/os/Build;->IS_DEBUGGABLE:Z

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    iget-object v3, p0, Lcom/android/settings/development/DevelopmentSettings;->bhM:Landroid/preference/CheckBoxPreference;

    invoke-virtual {p0}, Lcom/android/settings/development/DevelopmentSettings;->getActivity()Landroid/app/Activity;

    move-result-object v4

    invoke-virtual {v4}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    const-string/jumbo v5, "show_notification_channel_warnings"

    invoke-static {v4, v5, v0}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    if-eqz v0, :cond_1

    :goto_1
    invoke-virtual {p0, v3, v1}, Lcom/android/settings/development/DevelopmentSettings;->aVK(Landroid/preference/CheckBoxPreference;Z)V

    return-void

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    move v1, v2

    goto :goto_1
.end method

.method private aVH()V
    .locals 4

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/android/settings/development/DevelopmentSettings;->bhO:Landroid/preference/CheckBoxPreference;

    invoke-virtual {p0}, Lcom/android/settings/development/DevelopmentSettings;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string/jumbo v3, "show_touches"

    invoke-static {v2, v3, v0}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    if-eqz v2, :cond_0

    const/4 v0, 0x1

    :cond_0
    invoke-virtual {p0, v1, v0}, Lcom/android/settings/development/DevelopmentSettings;->aVK(Landroid/preference/CheckBoxPreference;Z)V

    return-void
.end method

.method private aVI()V
    .locals 5

    const/4 v1, 0x1

    const/4 v4, -0x1

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/android/settings/development/DevelopmentSettings;->bWB()Landroid/content/ContentResolver;

    move-result-object v3

    const-string/jumbo v0, "accessibility_display_daltonizer_enabled"

    invoke-static {v3, v0, v2}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    if-eqz v0, :cond_2

    const-string/jumbo v0, "accessibility_display_daltonizer"

    invoke-static {v3, v0, v4}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v3, p0, Lcom/android/settings/development/DevelopmentSettings;->bhP:Landroid/preference/ListPreference;

    invoke-virtual {v3, v0}, Landroid/preference/ListPreference;->setValue(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/android/settings/development/DevelopmentSettings;->bhP:Landroid/preference/ListPreference;

    invoke-virtual {v3, v0}, Landroid/preference/ListPreference;->findIndexOfValue(Ljava/lang/String;)I

    move-result v0

    if-gez v0, :cond_1

    iget-object v0, p0, Lcom/android/settings/development/DevelopmentSettings;->bhP:Landroid/preference/ListPreference;

    new-array v1, v1, [Ljava/lang/Object;

    const v3, 0x7f120046

    invoke-virtual {p0, v3}, Lcom/android/settings/development/DevelopmentSettings;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const v2, 0x7f1204ce

    invoke-virtual {p0, v2, v1}, Lcom/android/settings/development/DevelopmentSettings;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/ListPreference;->setSummary(Ljava/lang/CharSequence;)V

    :goto_1
    return-void

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/android/settings/development/DevelopmentSettings;->bhP:Landroid/preference/ListPreference;

    const-string/jumbo v1, "%s"

    invoke-virtual {v0, v1}, Landroid/preference/ListPreference;->setSummary(Ljava/lang/CharSequence;)V

    goto :goto_1

    :cond_2
    iget-object v0, p0, Lcom/android/settings/development/DevelopmentSettings;->bhP:Landroid/preference/ListPreference;

    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/ListPreference;->setValue(Ljava/lang/String;)V

    goto :goto_1
.end method

.method private aVJ()V
    .locals 3

    const/4 v0, 0x1

    iget-object v1, p0, Lcom/android/settings/development/DevelopmentSettings;->bhQ:Landroid/preference/CheckBoxPreference;

    invoke-static {}, Lcom/android/settings/development/DevelopmentSettings;->aUA()I

    move-result v2

    if-ne v2, v0, :cond_0

    :goto_0
    invoke-virtual {p0, v1, v0}, Lcom/android/settings/development/DevelopmentSettings;->aVK(Landroid/preference/CheckBoxPreference;Z)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private aVL()V
    .locals 3

    iget-object v0, p0, Lcom/android/settings/development/DevelopmentSettings;->bhh:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    invoke-static {}, Landroid/os/Process;->myUid()I

    move-result v0

    invoke-static {v0}, Landroid/os/UserHandle;->getUserId(I)I

    move-result v0

    iget-object v1, p0, Lcom/android/settings/development/DevelopmentSettings;->mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

    invoke-virtual {v1, v0}, Lcom/android/internal/widget/LockPatternUtils;->isSecure(I)Z

    move-result v1

    if-nez v1, :cond_2

    sget-boolean v1, Lmiui/os/Build;->IS_ALPHA_BUILD:Z

    xor-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/android/settings/development/DevelopmentSettings;->bhT:Landroid/preference/CheckBoxPreference;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/preference/CheckBoxPreference;->setEnabled(Z)V

    iget-object v1, p0, Lcom/android/settings/development/DevelopmentSettings;->bhT:Landroid/preference/CheckBoxPreference;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/android/settings/development/DevelopmentSettings;->bhT:Landroid/preference/CheckBoxPreference;

    iget-object v2, p0, Lcom/android/settings/development/DevelopmentSettings;->bgL:Lcom/android/settings/cx;

    invoke-virtual {v2}, Lcom/android/settings/cx;->bSR()Lcom/android/internal/widget/LockPatternUtils;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/android/internal/widget/LockPatternUtils;->isLockScreenDisabled(I)Z

    move-result v0

    invoke-virtual {v1, v0}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    :cond_1
    :goto_0
    return-void

    :cond_2
    iget-object v0, p0, Lcom/android/settings/development/DevelopmentSettings;->bhT:Landroid/preference/CheckBoxPreference;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setEnabled(Z)V

    goto :goto_0
.end method

.method private aVM()V
    .locals 5

    const/4 v2, 0x0

    const-string/jumbo v0, "debug.hwui.profile"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    const-string/jumbo v0, ""

    :cond_0
    iget-object v1, p0, Lcom/android/settings/development/DevelopmentSettings;->bhU:Landroid/preference/ListPreference;

    invoke-virtual {v1}, Landroid/preference/ListPreference;->getEntryValues()[Ljava/lang/CharSequence;

    move-result-object v3

    move v1, v2

    :goto_0
    array-length v4, v3

    if-ge v1, v4, :cond_2

    aget-object v4, v3, v1

    invoke-virtual {v0, v4}, Ljava/lang/String;->contentEquals(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_1

    iget-object v0, p0, Lcom/android/settings/development/DevelopmentSettings;->bhU:Landroid/preference/ListPreference;

    invoke-virtual {v0, v1}, Landroid/preference/ListPreference;->setValueIndex(I)V

    iget-object v0, p0, Lcom/android/settings/development/DevelopmentSettings;->bhU:Landroid/preference/ListPreference;

    iget-object v2, p0, Lcom/android/settings/development/DevelopmentSettings;->bhU:Landroid/preference/ListPreference;

    invoke-virtual {v2}, Landroid/preference/ListPreference;->getEntries()[Ljava/lang/CharSequence;

    move-result-object v2

    aget-object v1, v2, v1

    invoke-virtual {v0, v1}, Landroid/preference/ListPreference;->setSummary(Ljava/lang/CharSequence;)V

    return-void

    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/android/settings/development/DevelopmentSettings;->bhU:Landroid/preference/ListPreference;

    invoke-virtual {v0, v2}, Landroid/preference/ListPreference;->setValueIndex(I)V

    iget-object v0, p0, Lcom/android/settings/development/DevelopmentSettings;->bhU:Landroid/preference/ListPreference;

    iget-object v1, p0, Lcom/android/settings/development/DevelopmentSettings;->bhU:Landroid/preference/ListPreference;

    invoke-virtual {v1}, Landroid/preference/ListPreference;->getEntries()[Ljava/lang/CharSequence;

    move-result-object v1

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Landroid/preference/ListPreference;->setSummary(Ljava/lang/CharSequence;)V

    return-void
.end method

.method private aVN()V
    .locals 4

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/android/settings/development/DevelopmentSettings;->bhW:Landroid/preference/CheckBoxPreference;

    invoke-virtual {p0}, Lcom/android/settings/development/DevelopmentSettings;->bWB()Landroid/content/ContentResolver;

    move-result-object v2

    const-string/jumbo v3, "usb_audio_automatic_routing_disabled"

    invoke-static {v2, v3, v0}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    if-eqz v2, :cond_0

    const/4 v0, 0x1

    :cond_0
    invoke-virtual {p0, v1, v0}, Lcom/android/settings/development/DevelopmentSettings;->aVK(Landroid/preference/CheckBoxPreference;Z)V

    return-void
.end method

.method private aVO()V
    .locals 6

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/android/settings/development/DevelopmentSettings;->bhZ:Landroid/preference/ListPreference;

    if-eqz v0, :cond_0

    const-string/jumbo v0, "usb"

    invoke-virtual {p0, v0}, Lcom/android/settings/development/DevelopmentSettings;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/usb/UsbManager;

    invoke-virtual {p0}, Lcom/android/settings/development/DevelopmentSettings;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v3, 0x7f03010e

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0}, Lcom/android/settings/development/DevelopmentSettings;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v4, 0x7f03010d

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v4

    move v1, v2

    :goto_0
    array-length v5, v4

    if-ge v1, v5, :cond_2

    aget-object v5, v3, v1

    invoke-virtual {v0, v5}, Landroid/hardware/usb/UsbManager;->isFunctionEnabled(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_1

    :goto_1
    iget-object v0, p0, Lcom/android/settings/development/DevelopmentSettings;->bhZ:Landroid/preference/ListPreference;

    aget-object v2, v3, v1

    invoke-virtual {v0, v2}, Landroid/preference/ListPreference;->setValue(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/settings/development/DevelopmentSettings;->bhZ:Landroid/preference/ListPreference;

    aget-object v1, v4, v1

    invoke-virtual {v0, v1}, Landroid/preference/ListPreference;->setSummary(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/android/settings/development/DevelopmentSettings;->bhZ:Landroid/preference/ListPreference;

    invoke-virtual {v0, p0}, Landroid/preference/ListPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    :cond_0
    return-void

    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_2
    move v1, v2

    goto :goto_1
.end method

.method private aVP()V
    .locals 2

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/android/settings/development/DevelopmentSettings;->bii:Landroid/net/wifi/WifiManager;

    invoke-virtual {v1}, Landroid/net/wifi/WifiManager;->getAggressiveHandover()I

    move-result v1

    if-lez v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    iget-object v1, p0, Lcom/android/settings/development/DevelopmentSettings;->bif:Landroid/preference/CheckBoxPreference;

    invoke-virtual {p0, v1, v0}, Lcom/android/settings/development/DevelopmentSettings;->aVK(Landroid/preference/CheckBoxPreference;Z)V

    return-void
.end method

.method private aVQ()V
    .locals 2

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/android/settings/development/DevelopmentSettings;->bii:Landroid/net/wifi/WifiManager;

    invoke-virtual {v1}, Landroid/net/wifi/WifiManager;->getAllowScansWithTraffic()I

    move-result v1

    if-lez v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    iget-object v1, p0, Lcom/android/settings/development/DevelopmentSettings;->big:Landroid/preference/CheckBoxPreference;

    invoke-virtual {p0, v1, v0}, Lcom/android/settings/development/DevelopmentSettings;->aVK(Landroid/preference/CheckBoxPreference;Z)V

    return-void
.end method

.method private aVR()V
    .locals 4

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/android/settings/development/DevelopmentSettings;->bih:Landroid/preference/CheckBoxPreference;

    invoke-virtual {p0}, Lcom/android/settings/development/DevelopmentSettings;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string/jumbo v3, "wifi_display_certification_on"

    invoke-static {v2, v3, v0}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    if-eqz v2, :cond_0

    const/4 v0, 0x1

    :cond_0
    invoke-virtual {p0, v1, v0}, Lcom/android/settings/development/DevelopmentSettings;->aVK(Landroid/preference/CheckBoxPreference;Z)V

    return-void
.end method

.method private aVS()V
    .locals 2

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/android/settings/development/DevelopmentSettings;->bii:Landroid/net/wifi/WifiManager;

    invoke-virtual {v1}, Landroid/net/wifi/WifiManager;->getVerboseLoggingLevel()I

    move-result v1

    if-lez v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    iget-object v1, p0, Lcom/android/settings/development/DevelopmentSettings;->bij:Landroid/preference/CheckBoxPreference;

    invoke-virtual {p0, v1, v0}, Lcom/android/settings/development/DevelopmentSettings;->aVK(Landroid/preference/CheckBoxPreference;Z)V

    return-void
.end method

.method private aVT()Z
    .locals 5

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/android/settings/development/DevelopmentSettings;->bWB()Landroid/content/ContentResolver;

    move-result-object v3

    const-string/jumbo v0, "accessibility_display_daltonizer_enabled"

    invoke-static {v3, v0, v2}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    if-eqz v0, :cond_1

    const-string/jumbo v0, "accessibility_display_daltonizer"

    const/4 v4, -0x1

    invoke-static {v3, v0, v4}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v3, p0, Lcom/android/settings/development/DevelopmentSettings;->bhP:Landroid/preference/ListPreference;

    invoke-virtual {v3, v0}, Landroid/preference/ListPreference;->findIndexOfValue(Ljava/lang/String;)I

    move-result v0

    if-ltz v0, :cond_1

    return v1

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    return v2
.end method

.method private aVU(ILandroid/preference/ListPreference;Ljava/lang/Object;)V
    .locals 2

    if-eqz p3, :cond_0

    :try_start_0
    invoke-virtual {p3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v0

    :goto_0
    iget-object v1, p0, Lcom/android/settings/development/DevelopmentSettings;->bil:Landroid/view/IWindowManager;

    invoke-interface {v1, p1, v0}, Landroid/view/IWindowManager;->setAnimationScale(IF)V

    invoke-direct {p0, p1, p2}, Lcom/android/settings/development/DevelopmentSettings;->aUY(ILandroid/preference/ListPreference;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_1
    return-void

    :cond_0
    const/high16 v0, 0x3f800000    # 1.0f

    goto :goto_0

    :catch_0
    move-exception v0

    goto :goto_1
.end method

.method private aVV(Ljava/lang/Object;)V
    .locals 2

    if-eqz p1, :cond_0

    :try_start_0
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    :goto_0
    invoke-static {}, Landroid/app/ActivityManager;->getService()Landroid/app/IActivityManager;

    move-result-object v1

    invoke-interface {v1, v0}, Landroid/app/IActivityManager;->setProcessLimit(I)V

    invoke-direct {p0}, Lcom/android/settings/development/DevelopmentSettings;->aUZ()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_1
    return-void

    :cond_0
    const/4 v0, -0x1

    goto :goto_0

    :catch_0
    move-exception v0

    goto :goto_1
.end method

.method private aVW(Ljava/lang/Object;)V
    .locals 3

    const-string/jumbo v0, "persist.bluetooth.avrcpversion"

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/settings/development/DevelopmentSettings;->bgG:Landroid/preference/ListPreference;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/ListPreference;->findIndexOfValue(Ljava/lang/String;)I

    move-result v0

    if-ltz v0, :cond_0

    invoke-virtual {p0}, Lcom/android/settings/development/DevelopmentSettings;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f030038

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/android/settings/development/DevelopmentSettings;->bgG:Landroid/preference/ListPreference;

    aget-object v0, v1, v0

    invoke-virtual {v2, v0}, Landroid/preference/ListPreference;->setSummary(Ljava/lang/CharSequence;)V

    :cond_0
    return-void
.end method

.method private aVX(Landroid/preference/Preference;Ljava/lang/Object;)V
    .locals 19

    const v3, 0xf4240

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const-wide/16 v8, 0x0

    const-wide/16 v10, 0x0

    const-wide/16 v12, 0x0

    const-wide/16 v14, 0x0

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/settings/development/DevelopmentSettings;->bgD:Landroid/preference/ListPreference;

    invoke-virtual {v2}, Landroid/preference/ListPreference;->getValue()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/settings/development/DevelopmentSettings;->bgD:Landroid/preference/ListPreference;

    move-object/from16 v16, v0

    move-object/from16 v0, p1

    move-object/from16 v1, v16

    if-ne v0, v1, :cond_0

    invoke-virtual/range {p2 .. p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/settings/development/DevelopmentSettings;->bgD:Landroid/preference/ListPreference;

    move-object/from16 v16, v0

    invoke-virtual/range {p2 .. p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-virtual/range {v16 .. v17}, Landroid/preference/ListPreference;->findIndexOfValue(Ljava/lang/String;)I

    move-result v16

    if-ltz v16, :cond_0

    invoke-virtual/range {p0 .. p0}, Lcom/android/settings/development/DevelopmentSettings;->getResources()Landroid/content/res/Resources;

    move-result-object v17

    const v18, 0x7f030034

    invoke-virtual/range {v17 .. v18}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/settings/development/DevelopmentSettings;->bgD:Landroid/preference/ListPreference;

    move-object/from16 v18, v0

    aget-object v16, v17, v16

    move-object/from16 v0, v18

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Landroid/preference/ListPreference;->setSummary(Ljava/lang/CharSequence;)V

    :cond_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/settings/development/DevelopmentSettings;->bgD:Landroid/preference/ListPreference;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-virtual {v0, v2}, Landroid/preference/ListPreference;->findIndexOfValue(Ljava/lang/String;)I

    move-result v2

    packed-switch v2, :pswitch_data_0

    :goto_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/settings/development/DevelopmentSettings;->bgF:Landroid/preference/ListPreference;

    invoke-virtual {v2}, Landroid/preference/ListPreference;->getValue()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/settings/development/DevelopmentSettings;->bgF:Landroid/preference/ListPreference;

    move-object/from16 v16, v0

    move-object/from16 v0, p1

    move-object/from16 v1, v16

    if-ne v0, v1, :cond_1

    invoke-virtual/range {p2 .. p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/settings/development/DevelopmentSettings;->bgF:Landroid/preference/ListPreference;

    move-object/from16 v16, v0

    invoke-virtual/range {p2 .. p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-virtual/range {v16 .. v17}, Landroid/preference/ListPreference;->findIndexOfValue(Ljava/lang/String;)I

    move-result v16

    if-ltz v16, :cond_1

    invoke-virtual/range {p0 .. p0}, Lcom/android/settings/development/DevelopmentSettings;->getResources()Landroid/content/res/Resources;

    move-result-object v17

    const v18, 0x7f030031

    invoke-virtual/range {v17 .. v18}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/settings/development/DevelopmentSettings;->bgF:Landroid/preference/ListPreference;

    move-object/from16 v18, v0

    aget-object v16, v17, v16

    move-object/from16 v0, v18

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Landroid/preference/ListPreference;->setSummary(Ljava/lang/CharSequence;)V

    :cond_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/settings/development/DevelopmentSettings;->bgF:Landroid/preference/ListPreference;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-virtual {v0, v2}, Landroid/preference/ListPreference;->findIndexOfValue(Ljava/lang/String;)I

    move-result v2

    packed-switch v2, :pswitch_data_1

    :goto_1
    :pswitch_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/settings/development/DevelopmentSettings;->bgB:Landroid/preference/ListPreference;

    invoke-virtual {v2}, Landroid/preference/ListPreference;->getValue()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/settings/development/DevelopmentSettings;->bgB:Landroid/preference/ListPreference;

    move-object/from16 v16, v0

    move-object/from16 v0, p1

    move-object/from16 v1, v16

    if-ne v0, v1, :cond_2

    invoke-virtual/range {p2 .. p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/settings/development/DevelopmentSettings;->bgB:Landroid/preference/ListPreference;

    move-object/from16 v16, v0

    invoke-virtual/range {p2 .. p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-virtual/range {v16 .. v17}, Landroid/preference/ListPreference;->findIndexOfValue(Ljava/lang/String;)I

    move-result v16

    if-ltz v16, :cond_2

    invoke-virtual/range {p0 .. p0}, Lcom/android/settings/development/DevelopmentSettings;->getResources()Landroid/content/res/Resources;

    move-result-object v17

    const v18, 0x7f030028

    invoke-virtual/range {v17 .. v18}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/settings/development/DevelopmentSettings;->bgB:Landroid/preference/ListPreference;

    move-object/from16 v18, v0

    aget-object v16, v17, v16

    move-object/from16 v0, v18

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Landroid/preference/ListPreference;->setSummary(Ljava/lang/CharSequence;)V

    :cond_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/settings/development/DevelopmentSettings;->bgB:Landroid/preference/ListPreference;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-virtual {v0, v2}, Landroid/preference/ListPreference;->findIndexOfValue(Ljava/lang/String;)I

    move-result v2

    packed-switch v2, :pswitch_data_2

    :goto_2
    :pswitch_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/settings/development/DevelopmentSettings;->bgC:Landroid/preference/ListPreference;

    invoke-virtual {v2}, Landroid/preference/ListPreference;->getValue()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/settings/development/DevelopmentSettings;->bgC:Landroid/preference/ListPreference;

    move-object/from16 v16, v0

    move-object/from16 v0, p1

    move-object/from16 v1, v16

    if-ne v0, v1, :cond_3

    invoke-virtual/range {p2 .. p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/settings/development/DevelopmentSettings;->bgC:Landroid/preference/ListPreference;

    move-object/from16 v16, v0

    invoke-virtual/range {p2 .. p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-virtual/range {v16 .. v17}, Landroid/preference/ListPreference;->findIndexOfValue(Ljava/lang/String;)I

    move-result v16

    if-ltz v16, :cond_3

    invoke-virtual/range {p0 .. p0}, Lcom/android/settings/development/DevelopmentSettings;->getResources()Landroid/content/res/Resources;

    move-result-object v17

    const v18, 0x7f03002b

    invoke-virtual/range {v17 .. v18}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/settings/development/DevelopmentSettings;->bgC:Landroid/preference/ListPreference;

    move-object/from16 v18, v0

    aget-object v16, v17, v16

    move-object/from16 v0, v18

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Landroid/preference/ListPreference;->setSummary(Ljava/lang/CharSequence;)V

    :cond_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/settings/development/DevelopmentSettings;->bgC:Landroid/preference/ListPreference;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-virtual {v0, v2}, Landroid/preference/ListPreference;->findIndexOfValue(Ljava/lang/String;)I

    move-result v2

    packed-switch v2, :pswitch_data_3

    :goto_3
    :pswitch_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/settings/development/DevelopmentSettings;->bgE:Landroid/preference/ListPreference;

    invoke-virtual {v2}, Landroid/preference/ListPreference;->getValue()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/settings/development/DevelopmentSettings;->bgE:Landroid/preference/ListPreference;

    move-object/from16 v16, v0

    move-object/from16 v0, p1

    move-object/from16 v1, v16

    if-ne v0, v1, :cond_4

    invoke-virtual/range {p2 .. p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/settings/development/DevelopmentSettings;->bgE:Landroid/preference/ListPreference;

    move-object/from16 v16, v0

    invoke-virtual/range {p2 .. p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-virtual/range {v16 .. v17}, Landroid/preference/ListPreference;->findIndexOfValue(Ljava/lang/String;)I

    move-result v16

    if-ltz v16, :cond_4

    invoke-virtual/range {p0 .. p0}, Lcom/android/settings/development/DevelopmentSettings;->getResources()Landroid/content/res/Resources;

    move-result-object v17

    const v18, 0x7f03002e

    invoke-virtual/range {v17 .. v18}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/settings/development/DevelopmentSettings;->bgE:Landroid/preference/ListPreference;

    move-object/from16 v18, v0

    aget-object v16, v17, v16

    move-object/from16 v0, v18

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Landroid/preference/ListPreference;->setSummary(Ljava/lang/CharSequence;)V

    :cond_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/settings/development/DevelopmentSettings;->bgE:Landroid/preference/ListPreference;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-virtual {v0, v2}, Landroid/preference/ListPreference;->findIndexOfValue(Ljava/lang/String;)I

    move-result v2

    packed-switch v2, :pswitch_data_4

    :goto_4
    new-instance v2, Landroid/bluetooth/BluetoothCodecConfig;

    invoke-direct/range {v2 .. v15}, Landroid/bluetooth/BluetoothCodecConfig;-><init>(IIIIIJJJJ)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/settings/development/DevelopmentSettings;->bgw:Ljava/lang/Object;

    monitor-enter v3

    :try_start_0
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/settings/development/DevelopmentSettings;->bgv:Landroid/bluetooth/BluetoothA2dp;

    if-eqz v4, :cond_5

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/settings/development/DevelopmentSettings;->bgv:Landroid/bluetooth/BluetoothA2dp;

    invoke-virtual {v4, v2}, Landroid/bluetooth/BluetoothA2dp;->setCodecConfigPreference(Landroid/bluetooth/BluetoothCodecConfig;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    :cond_5
    monitor-exit v3

    return-void

    :pswitch_3
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/settings/development/DevelopmentSettings;->bgD:Landroid/preference/ListPreference;

    invoke-virtual {v2}, Landroid/preference/ListPreference;->getValue()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/settings/development/DevelopmentSettings;->bgD:Landroid/preference/ListPreference;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-virtual {v0, v2}, Landroid/preference/ListPreference;->findIndexOfValue(Ljava/lang/String;)I

    move-result v2

    packed-switch v2, :pswitch_data_5

    move v2, v3

    :goto_5
    move v3, v2

    goto/16 :goto_0

    :pswitch_4
    move v2, v3

    goto :goto_5

    :pswitch_5
    const/4 v2, 0x0

    goto :goto_5

    :pswitch_6
    const/4 v2, 0x1

    goto :goto_5

    :pswitch_7
    const/4 v2, 0x2

    goto :goto_5

    :pswitch_8
    const/4 v2, 0x3

    goto :goto_5

    :pswitch_9
    const/4 v2, 0x4

    goto :goto_5

    :pswitch_a
    const/4 v3, 0x0

    const v4, 0xf4240

    goto/16 :goto_0

    :pswitch_b
    const/4 v3, 0x1

    const v4, 0xf4240

    goto/16 :goto_0

    :pswitch_c
    const/4 v3, 0x2

    const v4, 0xf4240

    goto/16 :goto_0

    :pswitch_d
    const/4 v3, 0x3

    const v4, 0xf4240

    goto/16 :goto_0

    :pswitch_e
    const/4 v3, 0x4

    const v4, 0xf4240

    goto/16 :goto_0

    :pswitch_f
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/settings/development/DevelopmentSettings;->bgw:Ljava/lang/Object;

    monitor-enter v3

    :try_start_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/settings/development/DevelopmentSettings;->bgv:Landroid/bluetooth/BluetoothA2dp;

    if-eqz v2, :cond_6

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/settings/development/DevelopmentSettings;->bgv:Landroid/bluetooth/BluetoothA2dp;

    invoke-virtual {v2}, Landroid/bluetooth/BluetoothA2dp;->enableOptionalCodecs()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_6
    monitor-exit v3

    return-void

    :catchall_0
    move-exception v2

    monitor-exit v3

    throw v2

    :pswitch_10
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/settings/development/DevelopmentSettings;->bgw:Ljava/lang/Object;

    monitor-enter v3

    :try_start_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/settings/development/DevelopmentSettings;->bgv:Landroid/bluetooth/BluetoothA2dp;

    if-eqz v2, :cond_7

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/settings/development/DevelopmentSettings;->bgv:Landroid/bluetooth/BluetoothA2dp;

    invoke-virtual {v2}, Landroid/bluetooth/BluetoothA2dp;->disableOptionalCodecs()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    :cond_7
    monitor-exit v3

    return-void

    :catchall_1
    move-exception v2

    monitor-exit v3

    throw v2

    :pswitch_11
    const/4 v5, 0x1

    goto/16 :goto_1

    :pswitch_12
    const/4 v5, 0x2

    goto/16 :goto_1

    :pswitch_13
    const/4 v5, 0x4

    goto/16 :goto_1

    :pswitch_14
    const/16 v5, 0x8

    goto/16 :goto_1

    :pswitch_15
    const/4 v6, 0x1

    goto/16 :goto_2

    :pswitch_16
    const/4 v6, 0x2

    goto/16 :goto_2

    :pswitch_17
    const/4 v6, 0x4

    goto/16 :goto_2

    :pswitch_18
    const/4 v7, 0x1

    goto/16 :goto_3

    :pswitch_19
    const/4 v7, 0x2

    goto/16 :goto_3

    :pswitch_1a
    add-int/lit16 v2, v2, 0x3e8

    int-to-long v8, v2

    goto/16 :goto_4

    :catchall_2
    move-exception v2

    monitor-exit v3

    throw v2

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_3
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_0
        :pswitch_11
        :pswitch_12
        :pswitch_13
        :pswitch_14
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x0
        :pswitch_1
        :pswitch_15
        :pswitch_16
        :pswitch_17
    .end packed-switch

    :pswitch_data_3
    .packed-switch 0x0
        :pswitch_2
        :pswitch_18
        :pswitch_19
    .end packed-switch

    :pswitch_data_4
    .packed-switch 0x0
        :pswitch_1a
        :pswitch_1a
        :pswitch_1a
        :pswitch_1a
    .end packed-switch

    :pswitch_data_5
    .packed-switch 0x0
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
    .end packed-switch
.end method

.method private aVY()V
    .locals 2

    const-string/jumbo v1, "persist.bluetooth.disableabsvol"

    iget-object v0, p0, Lcom/android/settings/development/DevelopmentSettings;->bgz:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string/jumbo v0, "true"

    :goto_0
    invoke-static {v1, v0}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    return-void

    :cond_0
    const-string/jumbo v0, "false"

    goto :goto_0
.end method

.method private aVZ()V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/development/DevelopmentSettings;->bgA:Landroid/preference/CheckBoxPreference;

    if-eqz v0, :cond_0

    const-string/jumbo v1, "persist.bluetooth.enableinbandringing"

    iget-object v0, p0, Lcom/android/settings/development/DevelopmentSettings;->bgA:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_1

    const-string/jumbo v0, "true"

    :goto_0
    invoke-static {v1, v0}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    return-void

    :cond_1
    const-string/jumbo v0, "false"

    goto :goto_0
.end method

.method private aVa()V
    .locals 10

    const v9, 0x7f120336

    const/4 v1, -0x1

    const/4 v3, 0x1

    const/4 v8, 0x0

    const/4 v0, 0x0

    iget-object v2, p0, Lcom/android/settings/development/DevelopmentSettings;->bgw:Ljava/lang/Object;

    monitor-enter v2

    :try_start_0
    iget-object v4, p0, Lcom/android/settings/development/DevelopmentSettings;->bgv:Landroid/bluetooth/BluetoothA2dp;

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/android/settings/development/DevelopmentSettings;->bgv:Landroid/bluetooth/BluetoothA2dp;

    invoke-virtual {v4}, Landroid/bluetooth/BluetoothA2dp;->getCodecStatus()Landroid/bluetooth/BluetoothCodecStatus;

    move-result-object v4

    if-eqz v4, :cond_0

    invoke-virtual {v4}, Landroid/bluetooth/BluetoothCodecStatus;->getCodecConfig()Landroid/bluetooth/BluetoothCodecConfig;

    move-result-object v0

    invoke-virtual {v4}, Landroid/bluetooth/BluetoothCodecStatus;->getCodecsLocalCapabilities()[Landroid/bluetooth/BluetoothCodecConfig;

    invoke-virtual {v4}, Landroid/bluetooth/BluetoothCodecStatus;->getCodecsSelectableCapabilities()[Landroid/bluetooth/BluetoothCodecConfig;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit v2

    if-nez v0, :cond_1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0

    :cond_1
    :try_start_1
    invoke-virtual {p0}, Lcom/android/settings/development/DevelopmentSettings;->getResources()Landroid/content/res/Resources;
    :try_end_1
    .catch Ljava/lang/IllegalStateException; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v4

    if-nez v4, :cond_2

    return-void

    :catch_0
    move-exception v0

    return-void

    :cond_2
    invoke-virtual {v0}, Landroid/bluetooth/BluetoothCodecConfig;->getCodecType()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    move v2, v1

    :goto_0
    if-ltz v2, :cond_3

    iget-object v5, p0, Lcom/android/settings/development/DevelopmentSettings;->bgD:Landroid/preference/ListPreference;

    if-eqz v5, :cond_3

    const v5, 0x7f030034

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v5

    new-array v6, v3, [Ljava/lang/Object;

    aget-object v2, v5, v2

    aput-object v2, v6, v8

    invoke-virtual {v4, v9, v6}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    iget-object v5, p0, Lcom/android/settings/development/DevelopmentSettings;->bgD:Landroid/preference/ListPreference;

    invoke-virtual {v5, v2}, Landroid/preference/ListPreference;->setSummary(Ljava/lang/CharSequence;)V

    :cond_3
    invoke-virtual {v0}, Landroid/bluetooth/BluetoothCodecConfig;->getSampleRate()I

    move-result v2

    packed-switch v2, :pswitch_data_1

    :pswitch_0
    move v2, v1

    :goto_1
    if-ltz v2, :cond_4

    iget-object v5, p0, Lcom/android/settings/development/DevelopmentSettings;->bgF:Landroid/preference/ListPreference;

    if-eqz v5, :cond_4

    const v5, 0x7f030031

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v5

    new-array v6, v3, [Ljava/lang/Object;

    aget-object v2, v5, v2

    aput-object v2, v6, v8

    invoke-virtual {v4, v9, v6}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    iget-object v5, p0, Lcom/android/settings/development/DevelopmentSettings;->bgF:Landroid/preference/ListPreference;

    invoke-virtual {v5, v2}, Landroid/preference/ListPreference;->setSummary(Ljava/lang/CharSequence;)V

    :cond_4
    invoke-virtual {v0}, Landroid/bluetooth/BluetoothCodecConfig;->getBitsPerSample()I

    move-result v2

    packed-switch v2, :pswitch_data_2

    :pswitch_1
    move v2, v1

    :goto_2
    if-ltz v2, :cond_5

    iget-object v5, p0, Lcom/android/settings/development/DevelopmentSettings;->bgB:Landroid/preference/ListPreference;

    if-eqz v5, :cond_5

    const v5, 0x7f030028

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v5

    new-array v6, v3, [Ljava/lang/Object;

    aget-object v2, v5, v2

    aput-object v2, v6, v8

    invoke-virtual {v4, v9, v6}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    iget-object v5, p0, Lcom/android/settings/development/DevelopmentSettings;->bgB:Landroid/preference/ListPreference;

    invoke-virtual {v5, v2}, Landroid/preference/ListPreference;->setSummary(Ljava/lang/CharSequence;)V

    :cond_5
    invoke-virtual {v0}, Landroid/bluetooth/BluetoothCodecConfig;->getChannelMode()I

    move-result v2

    packed-switch v2, :pswitch_data_3

    move v2, v1

    :goto_3
    if-ltz v2, :cond_6

    iget-object v5, p0, Lcom/android/settings/development/DevelopmentSettings;->bgC:Landroid/preference/ListPreference;

    if-eqz v5, :cond_6

    const v5, 0x7f03002b

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v5

    new-array v6, v3, [Ljava/lang/Object;

    aget-object v2, v5, v2

    aput-object v2, v6, v8

    invoke-virtual {v4, v9, v6}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    iget-object v5, p0, Lcom/android/settings/development/DevelopmentSettings;->bgC:Landroid/preference/ListPreference;

    invoke-virtual {v5, v2}, Landroid/preference/ListPreference;->setSummary(Ljava/lang/CharSequence;)V

    :cond_6
    invoke-virtual {v0}, Landroid/bluetooth/BluetoothCodecConfig;->getCodecSpecific1()J

    move-result-wide v6

    long-to-int v0, v6

    if-lez v0, :cond_8

    rem-int/lit8 v0, v0, 0xa

    :goto_4
    packed-switch v0, :pswitch_data_4

    move v0, v1

    :pswitch_2
    if-ltz v0, :cond_7

    iget-object v1, p0, Lcom/android/settings/development/DevelopmentSettings;->bgE:Landroid/preference/ListPreference;

    if-eqz v1, :cond_7

    const v1, 0x7f03002e

    invoke-virtual {v4, v1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v1

    new-array v2, v3, [Ljava/lang/Object;

    aget-object v0, v1, v0

    aput-object v0, v2, v8

    invoke-virtual {v4, v9, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/development/DevelopmentSettings;->bgE:Landroid/preference/ListPreference;

    invoke-virtual {v1, v0}, Landroid/preference/ListPreference;->setSummary(Ljava/lang/CharSequence;)V

    :cond_7
    return-void

    :pswitch_3
    move v2, v3

    goto/16 :goto_0

    :pswitch_4
    const/4 v2, 0x2

    goto/16 :goto_0

    :pswitch_5
    const/4 v2, 0x3

    goto/16 :goto_0

    :pswitch_6
    const/4 v2, 0x4

    goto/16 :goto_0

    :pswitch_7
    const/4 v2, 0x5

    goto/16 :goto_0

    :pswitch_8
    move v2, v3

    goto/16 :goto_1

    :pswitch_9
    const/4 v2, 0x2

    goto/16 :goto_1

    :pswitch_a
    const/4 v2, 0x3

    goto/16 :goto_1

    :pswitch_b
    const/4 v2, 0x4

    goto/16 :goto_1

    :pswitch_c
    move v2, v3

    goto/16 :goto_2

    :pswitch_d
    const/4 v2, 0x2

    goto/16 :goto_2

    :pswitch_e
    const/4 v2, 0x3

    goto/16 :goto_2

    :pswitch_f
    move v2, v3

    goto :goto_3

    :pswitch_10
    const/4 v2, 0x2

    goto :goto_3

    :cond_8
    move v0, v1

    goto :goto_4

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_8
        :pswitch_9
        :pswitch_0
        :pswitch_a
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_b
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x1
        :pswitch_c
        :pswitch_d
        :pswitch_1
        :pswitch_e
    .end packed-switch

    :pswitch_data_3
    .packed-switch 0x1
        :pswitch_f
        :pswitch_10
    .end packed-switch

    :pswitch_data_4
    .packed-switch 0x0
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method

.method private aVb()V
    .locals 3

    iget-object v0, p0, Lcom/android/settings/development/DevelopmentSettings;->bgz:Landroid/preference/CheckBoxPreference;

    const-string/jumbo v1, "persist.bluetooth.disableabsvol"

    const/4 v2, 0x0

    invoke-static {v1, v2}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/android/settings/development/DevelopmentSettings;->aVK(Landroid/preference/CheckBoxPreference;Z)V

    return-void
.end method

.method private aVc()V
    .locals 3

    iget-object v0, p0, Lcom/android/settings/development/DevelopmentSettings;->bgA:Landroid/preference/CheckBoxPreference;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/development/DevelopmentSettings;->bgA:Landroid/preference/CheckBoxPreference;

    const-string/jumbo v1, "persist.bluetooth.enableinbandringing"

    const/4 v2, 0x0

    invoke-static {v1, v2}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/android/settings/development/DevelopmentSettings;->aVK(Landroid/preference/CheckBoxPreference;Z)V

    :cond_0
    return-void
.end method

.method private aVd()V
    .locals 0

    return-void
.end method

.method private aVe()V
    .locals 3

    iget-object v0, p0, Lcom/android/settings/development/DevelopmentSettings;->bgO:Landroid/preference/CheckBoxPreference;

    const-string/jumbo v1, "persist.sys.debug.color_temp"

    const/4 v2, 0x0

    invoke-static {v1, v2}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/android/settings/development/DevelopmentSettings;->aVK(Landroid/preference/CheckBoxPreference;Z)V

    return-void
.end method

.method private aVf()V
    .locals 5

    const/4 v2, 0x0

    const-string/jumbo v0, "debug.hwui.overdraw"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    const-string/jumbo v0, ""

    :cond_0
    iget-object v1, p0, Lcom/android/settings/development/DevelopmentSettings;->bgR:Landroid/preference/ListPreference;

    invoke-virtual {v1}, Landroid/preference/ListPreference;->getEntryValues()[Ljava/lang/CharSequence;

    move-result-object v3

    move v1, v2

    :goto_0
    array-length v4, v3

    if-ge v1, v4, :cond_2

    aget-object v4, v3, v1

    invoke-virtual {v0, v4}, Ljava/lang/String;->contentEquals(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_1

    iget-object v0, p0, Lcom/android/settings/development/DevelopmentSettings;->bgR:Landroid/preference/ListPreference;

    invoke-virtual {v0, v1}, Landroid/preference/ListPreference;->setValueIndex(I)V

    iget-object v0, p0, Lcom/android/settings/development/DevelopmentSettings;->bgR:Landroid/preference/ListPreference;

    iget-object v2, p0, Lcom/android/settings/development/DevelopmentSettings;->bgR:Landroid/preference/ListPreference;

    invoke-virtual {v2}, Landroid/preference/ListPreference;->getEntries()[Ljava/lang/CharSequence;

    move-result-object v2

    aget-object v1, v2, v1

    invoke-virtual {v0, v1}, Landroid/preference/ListPreference;->setSummary(Ljava/lang/CharSequence;)V

    return-void

    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/android/settings/development/DevelopmentSettings;->bgR:Landroid/preference/ListPreference;

    invoke-virtual {v0, v2}, Landroid/preference/ListPreference;->setValueIndex(I)V

    iget-object v0, p0, Lcom/android/settings/development/DevelopmentSettings;->bgR:Landroid/preference/ListPreference;

    iget-object v1, p0, Lcom/android/settings/development/DevelopmentSettings;->bgR:Landroid/preference/ListPreference;

    invoke-virtual {v1}, Landroid/preference/ListPreference;->getEntries()[Ljava/lang/CharSequence;

    move-result-object v1

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Landroid/preference/ListPreference;->setSummary(Ljava/lang/CharSequence;)V

    return-void
.end method

.method private aVg()V
    .locals 5

    const/4 v2, 0x0

    const-string/jumbo v0, "debug.hwui.renderer"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    const-string/jumbo v0, ""

    :cond_0
    iget-object v1, p0, Lcom/android/settings/development/DevelopmentSettings;->bgS:Landroid/preference/ListPreference;

    invoke-virtual {v1}, Landroid/preference/ListPreference;->getEntryValues()[Ljava/lang/CharSequence;

    move-result-object v3

    move v1, v2

    :goto_0
    array-length v4, v3

    if-ge v1, v4, :cond_2

    aget-object v4, v3, v1

    invoke-virtual {v0, v4}, Ljava/lang/String;->contentEquals(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_1

    iget-object v0, p0, Lcom/android/settings/development/DevelopmentSettings;->bgS:Landroid/preference/ListPreference;

    invoke-virtual {v0, v1}, Landroid/preference/ListPreference;->setValueIndex(I)V

    iget-object v0, p0, Lcom/android/settings/development/DevelopmentSettings;->bgS:Landroid/preference/ListPreference;

    iget-object v2, p0, Lcom/android/settings/development/DevelopmentSettings;->bgS:Landroid/preference/ListPreference;

    invoke-virtual {v2}, Landroid/preference/ListPreference;->getEntries()[Ljava/lang/CharSequence;

    move-result-object v2

    aget-object v1, v2, v1

    invoke-virtual {v0, v1}, Landroid/preference/ListPreference;->setSummary(Ljava/lang/CharSequence;)V

    return-void

    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/android/settings/development/DevelopmentSettings;->bgS:Landroid/preference/ListPreference;

    invoke-virtual {v0, v2}, Landroid/preference/ListPreference;->setValueIndex(I)V

    iget-object v0, p0, Lcom/android/settings/development/DevelopmentSettings;->bgS:Landroid/preference/ListPreference;

    iget-object v1, p0, Lcom/android/settings/development/DevelopmentSettings;->bgS:Landroid/preference/ListPreference;

    invoke-virtual {v1}, Landroid/preference/ListPreference;->getEntries()[Ljava/lang/CharSequence;

    move-result-object v1

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Landroid/preference/ListPreference;->setSummary(Ljava/lang/CharSequence;)V

    return-void
.end method

.method private aVh()V
    .locals 3

    iget-object v0, p0, Lcom/android/settings/development/DevelopmentSettings;->bgT:Landroid/preference/CheckBoxPreference;

    const-string/jumbo v1, "debug.layout"

    const/4 v2, 0x0

    invoke-static {v1, v2}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/android/settings/development/DevelopmentSettings;->aVK(Landroid/preference/CheckBoxPreference;Z)V

    return-void
.end method

.method private aVi()V
    .locals 6

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/android/settings/development/DevelopmentSettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v3, "debug_app"

    invoke-static {v0, v3}, Landroid/provider/Settings$Global;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/development/DevelopmentSettings;->bgP:Ljava/lang/String;

    iget-object v3, p0, Lcom/android/settings/development/DevelopmentSettings;->bic:Landroid/preference/CheckBoxPreference;

    invoke-virtual {p0}, Lcom/android/settings/development/DevelopmentSettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v4, "wait_for_debugger"

    invoke-static {v0, v4, v2}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {p0, v3, v0}, Lcom/android/settings/development/DevelopmentSettings;->aVK(Landroid/preference/CheckBoxPreference;Z)V

    iget-object v0, p0, Lcom/android/settings/development/DevelopmentSettings;->bgP:Ljava/lang/String;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/settings/development/DevelopmentSettings;->bgP:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_2

    :try_start_0
    invoke-virtual {p0}, Lcom/android/settings/development/DevelopmentSettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    iget-object v3, p0, Lcom/android/settings/development/DevelopmentSettings;->bgP:Ljava/lang/String;

    const/16 v4, 0x200

    invoke-virtual {v0, v3, v4}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v0

    invoke-virtual {p0}, Lcom/android/settings/development/DevelopmentSettings;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/Activity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    invoke-virtual {v3, v0}, Landroid/content/pm/PackageManager;->getApplicationLabel(Landroid/content/pm/ApplicationInfo;)Ljava/lang/CharSequence;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    :goto_1
    iget-object v3, p0, Lcom/android/settings/development/DevelopmentSettings;->bgQ:Landroid/preference/Preference;

    invoke-virtual {p0}, Lcom/android/settings/development/DevelopmentSettings;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    new-array v5, v1, [Ljava/lang/Object;

    aput-object v0, v5, v2

    const v0, 0x7f12053f

    invoke-virtual {v4, v0, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/android/settings/development/DevelopmentSettings;->bic:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setEnabled(Z)V

    iput-boolean v1, p0, Lcom/android/settings/development/DevelopmentSettings;->bho:Z

    :goto_2
    return-void

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    :try_start_1
    iget-object v0, p0, Lcom/android/settings/development/DevelopmentSettings;->bgP:Ljava/lang/String;
    :try_end_1
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    :catch_0
    move-exception v0

    iget-object v0, p0, Lcom/android/settings/development/DevelopmentSettings;->bgP:Ljava/lang/String;

    goto :goto_1

    :cond_2
    iget-object v0, p0, Lcom/android/settings/development/DevelopmentSettings;->bgQ:Landroid/preference/Preference;

    invoke-virtual {p0}, Lcom/android/settings/development/DevelopmentSettings;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v3, 0x7f12053e

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/android/settings/development/DevelopmentSettings;->bic:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, v2}, Landroid/preference/CheckBoxPreference;->setEnabled(Z)V

    goto :goto_2
.end method

.method private aVj()V
    .locals 4

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/android/settings/development/DevelopmentSettings;->bgW:Landroid/preference/CheckBoxPreference;

    invoke-virtual {p0}, Lcom/android/settings/development/DevelopmentSettings;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string/jumbo v3, "sysui_tuner_demo_on"

    invoke-static {v2, v3, v0}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    if-eqz v2, :cond_0

    const/4 v0, 0x1

    :cond_0
    invoke-virtual {p0, v1, v0}, Lcom/android/settings/development/DevelopmentSettings;->aVK(Landroid/preference/CheckBoxPreference;Z)V

    return-void
.end method

.method private aVk()V
    .locals 7

    const/4 v0, 0x1

    const/4 v1, 0x0

    :try_start_0
    const-string/jumbo v2, "SurfaceFlinger"

    invoke-static {v2}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v3

    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v4

    const-string/jumbo v5, "android.ui.ISurfaceComposer"

    invoke-virtual {v3, v5}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    const/16 v5, 0x3f2

    const/4 v6, 0x0

    invoke-interface {v2, v5, v3, v4, v6}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    invoke-virtual {v4}, Landroid/os/Parcel;->readInt()I

    invoke-virtual {v4}, Landroid/os/Parcel;->readInt()I

    invoke-virtual {v4}, Landroid/os/Parcel;->readInt()I

    move-result v2

    iget-object v5, p0, Lcom/android/settings/development/DevelopmentSettings;->bhN:Landroid/preference/CheckBoxPreference;

    if-eqz v2, :cond_1

    move v2, v0

    :goto_0
    invoke-virtual {p0, v5, v2}, Lcom/android/settings/development/DevelopmentSettings;->aVK(Landroid/preference/CheckBoxPreference;Z)V

    invoke-virtual {v4}, Landroid/os/Parcel;->readInt()I

    invoke-virtual {v4}, Landroid/os/Parcel;->readInt()I

    move-result v2

    iget-object v5, p0, Lcom/android/settings/development/DevelopmentSettings;->bha:Landroid/preference/CheckBoxPreference;

    if-eqz v2, :cond_2

    :goto_1
    invoke-virtual {p0, v5, v0}, Lcom/android/settings/development/DevelopmentSettings;->aVK(Landroid/preference/CheckBoxPreference;Z)V

    invoke-virtual {v4}, Landroid/os/Parcel;->recycle()V

    invoke-virtual {v3}, Landroid/os/Parcel;->recycle()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_2
    return-void

    :cond_1
    move v2, v1

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_1

    :catch_0
    move-exception v0

    goto :goto_2
.end method

.method private aVl()V
    .locals 4

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/android/settings/development/DevelopmentSettings;->bhm:Landroid/preference/CheckBoxPreference;

    invoke-virtual {p0}, Lcom/android/settings/development/DevelopmentSettings;->bWB()Landroid/content/ContentResolver;

    move-result-object v2

    const-string/jumbo v3, "force_resizable_activities"

    invoke-static {v2, v3, v0}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    if-eqz v2, :cond_0

    const/4 v0, 0x1

    :cond_0
    invoke-virtual {p0, v1, v0}, Lcom/android/settings/development/DevelopmentSettings;->aVK(Landroid/preference/CheckBoxPreference;Z)V

    return-void
.end method

.method private aVm()V
    .locals 4

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/android/settings/development/DevelopmentSettings;->bhn:Landroid/preference/CheckBoxPreference;

    invoke-virtual {p0}, Lcom/android/settings/development/DevelopmentSettings;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string/jumbo v3, "debug.force_rtl"

    invoke-static {v2, v3, v0}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    if-eqz v2, :cond_0

    const/4 v0, 0x1

    :cond_0
    invoke-virtual {p0, v1, v0}, Lcom/android/settings/development/DevelopmentSettings;->aVK(Landroid/preference/CheckBoxPreference;Z)V

    return-void
.end method

.method private aVn()V
    .locals 3

    iget-object v0, p0, Lcom/android/settings/development/DevelopmentSettings;->bhk:Landroid/preference/CheckBoxPreference;

    const-string/jumbo v1, "persist.sys.ui.hw"

    const/4 v2, 0x0

    invoke-static {v1, v2}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/android/settings/development/DevelopmentSettings;->aVK(Landroid/preference/CheckBoxPreference;Z)V

    return-void
.end method

.method private aVo()V
    .locals 7

    const-string/jumbo v0, "hdcp_checking"

    invoke-virtual {p0, v0}, Lcom/android/settings/development/DevelopmentSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/ListPreference;

    if-eqz v0, :cond_0

    const-string/jumbo v1, "persist.sys.hdcp_checking"

    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0}, Lcom/android/settings/development/DevelopmentSettings;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f030083

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0}, Lcom/android/settings/development/DevelopmentSettings;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f030081

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v5

    const/4 v2, 0x1

    const/4 v1, 0x0

    :goto_0
    array-length v6, v4

    if-ge v1, v6, :cond_2

    aget-object v6, v4, v1

    invoke-virtual {v3, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    :goto_1
    aget-object v2, v4, v1

    invoke-virtual {v0, v2}, Landroid/preference/ListPreference;->setValue(Ljava/lang/String;)V

    aget-object v1, v5, v1

    invoke-virtual {v0, v1}, Landroid/preference/ListPreference;->setSummary(Ljava/lang/CharSequence;)V

    invoke-virtual {v0, p0}, Landroid/preference/ListPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    :cond_0
    return-void

    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_2
    move v1, v2

    goto :goto_1
.end method

.method private aVp()V
    .locals 4

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/android/settings/development/DevelopmentSettings;->bhp:Landroid/preference/CheckBoxPreference;

    invoke-virtual {p0}, Lcom/android/settings/development/DevelopmentSettings;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string/jumbo v3, "always_finish_activities"

    invoke-static {v2, v3, v0}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    if-eqz v2, :cond_0

    const/4 v0, 0x1

    :cond_0
    invoke-virtual {p0, v1, v0}, Lcom/android/settings/development/DevelopmentSettings;->aVK(Landroid/preference/CheckBoxPreference;Z)V

    return-void
.end method

.method private aVq()V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/development/DevelopmentSettings;->bhs:Landroid/preference/ListPreference;

    if-eqz v0, :cond_1

    const-string/jumbo v0, "persist.logd.limit"

    const-string/jumbo v1, ""

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    sget-boolean v0, Lmiui/os/Build;->IS_STABLE_VERSION:Z

    if-eqz v0, :cond_2

    const-string/jumbo v0, "Warn"

    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/android/settings/development/DevelopmentSettings;->bhs:Landroid/preference/ListPreference;

    invoke-virtual {v1, v0}, Landroid/preference/ListPreference;->setValue(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/android/settings/development/DevelopmentSettings;->bhs:Landroid/preference/ListPreference;

    invoke-virtual {v1, v0}, Landroid/preference/ListPreference;->setSummary(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/android/settings/development/DevelopmentSettings;->bhs:Landroid/preference/ListPreference;

    invoke-virtual {v0, p0}, Landroid/preference/ListPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    :cond_1
    return-void

    :cond_2
    const-string/jumbo v0, "Info"

    goto :goto_0
.end method

.method private aVr()V
    .locals 9

    const v8, 0x7f0300e3

    const/4 v1, 0x1

    const/4 v4, 0x0

    const/4 v5, 0x0

    iget-object v0, p0, Lcom/android/settings/development/DevelopmentSettings;->bht:Landroid/preference/ListPreference;

    if-eqz v0, :cond_6

    const-string/jumbo v0, "persist.log.tag"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v0, "persist.logd.size"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v2, :cond_0

    const-string/jumbo v3, "Settings"

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    const-string/jumbo v0, "32768"

    :cond_0
    iget-object v2, p0, Lcom/android/settings/development/DevelopmentSettings;->bhu:Landroid/preference/ListPreference;

    if-eqz v2, :cond_2

    const-string/jumbo v2, "logd.logpersistd.enable"

    invoke-static {v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_1

    const-string/jumbo v3, "true"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    xor-int/lit8 v2, v2, 0x1

    if-nez v2, :cond_1

    const-string/jumbo v2, "32768"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_7

    :cond_1
    invoke-direct {p0, v5, v1}, Lcom/android/settings/development/DevelopmentSettings;->aWp(Ljava/lang/Object;Z)V

    iget-object v2, p0, Lcom/android/settings/development/DevelopmentSettings;->bhu:Landroid/preference/ListPreference;

    invoke-virtual {v2, v4}, Landroid/preference/ListPreference;->setEnabled(Z)V

    :cond_2
    :goto_0
    if-eqz v0, :cond_3

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-nez v2, :cond_4

    :cond_3
    invoke-direct {p0}, Lcom/android/settings/development/DevelopmentSettings;->aUB()Ljava/lang/String;

    move-result-object v0

    :cond_4
    invoke-virtual {p0}, Lcom/android/settings/development/DevelopmentSettings;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0300e6

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0}, Lcom/android/settings/development/DevelopmentSettings;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0300e5

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v3

    const/4 v2, 0x2

    const-string/jumbo v6, "ro.config.low_ram"

    invoke-static {v6}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    const-string/jumbo v7, "true"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_a

    iget-object v2, p0, Lcom/android/settings/development/DevelopmentSettings;->bht:Landroid/preference/ListPreference;

    invoke-virtual {v2, v8}, Landroid/preference/ListPreference;->setEntries(I)V

    invoke-virtual {p0}, Lcom/android/settings/development/DevelopmentSettings;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v8}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v2

    :goto_1
    invoke-virtual {p0}, Lcom/android/settings/development/DevelopmentSettings;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v6, 0x7f0300e4

    invoke-virtual {v3, v6}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v6

    move v3, v4

    :goto_2
    array-length v4, v2

    if-ge v3, v4, :cond_9

    aget-object v4, v5, v3

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_5

    aget-object v4, v2, v3

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_8

    :cond_5
    :goto_3
    iget-object v0, p0, Lcom/android/settings/development/DevelopmentSettings;->bht:Landroid/preference/ListPreference;

    aget-object v1, v5, v3

    invoke-virtual {v0, v1}, Landroid/preference/ListPreference;->setValue(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/settings/development/DevelopmentSettings;->bht:Landroid/preference/ListPreference;

    aget-object v1, v6, v3

    invoke-virtual {v0, v1}, Landroid/preference/ListPreference;->setSummary(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/android/settings/development/DevelopmentSettings;->bht:Landroid/preference/ListPreference;

    invoke-virtual {v0, p0}, Landroid/preference/ListPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    :cond_6
    return-void

    :cond_7
    iget-object v2, p0, Lcom/android/settings/development/DevelopmentSettings;->bhG:Lcom/android/settings/development/b;

    invoke-virtual {v2}, Lcom/android/settings/development/b;->aUs()Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/android/settings/development/DevelopmentSettings;->bhu:Landroid/preference/ListPreference;

    invoke-virtual {v2, v1}, Landroid/preference/ListPreference;->setEnabled(Z)V

    goto :goto_0

    :cond_8
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    :cond_9
    move v3, v1

    goto :goto_3

    :cond_a
    move v1, v2

    move-object v2, v3

    goto :goto_1
.end method

.method private aVs()V
    .locals 7

    const/4 v3, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/android/settings/development/DevelopmentSettings;->bhu:Landroid/preference/ListPreference;

    if-nez v0, :cond_0

    return-void

    :cond_0
    const-string/jumbo v0, "logd.logpersistd"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    const-string/jumbo v0, ""

    :cond_1
    const-string/jumbo v1, "logd.logpersistd.buffer"

    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v6

    if-nez v6, :cond_3

    :cond_2
    const-string/jumbo v1, "all"

    :cond_3
    const-string/jumbo v6, "logcatd"

    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_b

    const-string/jumbo v0, "kernel"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    move v0, v3

    :goto_0
    iget-object v1, p0, Lcom/android/settings/development/DevelopmentSettings;->bhu:Landroid/preference/ListPreference;

    invoke-virtual {p0}, Lcom/android/settings/development/DevelopmentSettings;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v5, 0x7f0300ea

    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v3

    aget-object v3, v3, v0

    invoke-virtual {v1, v3}, Landroid/preference/ListPreference;->setValue(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/android/settings/development/DevelopmentSettings;->bhu:Landroid/preference/ListPreference;

    invoke-virtual {p0}, Lcom/android/settings/development/DevelopmentSettings;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v5, 0x7f0300e8

    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v3

    aget-object v3, v3, v0

    invoke-virtual {v1, v3}, Landroid/preference/ListPreference;->setSummary(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/android/settings/development/DevelopmentSettings;->bhu:Landroid/preference/ListPreference;

    invoke-virtual {v1, p0}, Landroid/preference/ListPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    if-eqz v0, :cond_8

    iput-boolean v2, p0, Lcom/android/settings/development/DevelopmentSettings;->bhw:Z

    :cond_4
    :goto_1
    return-void

    :cond_5
    const-string/jumbo v0, "all"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_6

    const-string/jumbo v0, "radio"

    invoke-virtual {v1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_6

    const-string/jumbo v0, "security"

    invoke-virtual {v1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_6

    const-string/jumbo v0, "kernel"

    invoke-virtual {v1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_a

    const-string/jumbo v0, "default"

    invoke-virtual {v1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_9

    const/4 v0, 0x4

    new-array v6, v0, [Ljava/lang/String;

    const-string/jumbo v0, "main"

    aput-object v0, v6, v2

    const-string/jumbo v0, "events"

    aput-object v0, v6, v4

    const-string/jumbo v0, "system"

    aput-object v0, v6, v5

    const-string/jumbo v0, "crash"

    aput-object v0, v6, v3

    move v0, v2

    :goto_2
    array-length v3, v6

    if-ge v0, v3, :cond_9

    aget-object v3, v6, v0

    invoke-virtual {v1, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_7

    move v0, v4

    goto/16 :goto_0

    :cond_6
    move v0, v4

    goto/16 :goto_0

    :cond_7
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_8
    iget-boolean v0, p0, Lcom/android/settings/development/DevelopmentSettings;->bhw:Z

    if-nez v0, :cond_4

    invoke-virtual {p0}, Lcom/android/settings/development/DevelopmentSettings;->aUM()V

    iput-boolean v4, p0, Lcom/android/settings/development/DevelopmentSettings;->bhw:Z

    goto :goto_1

    :cond_9
    move v0, v5

    goto/16 :goto_0

    :cond_a
    move v0, v4

    goto/16 :goto_0

    :cond_b
    move v0, v2

    goto/16 :goto_0
.end method

.method private aVt()V
    .locals 5

    const/4 v0, 0x1

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/android/settings/development/DevelopmentSettings;->bhx:Landroid/preference/CheckBoxPreference;

    invoke-virtual {p0}, Lcom/android/settings/development/DevelopmentSettings;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string/jumbo v4, "mobile_data_always_on"

    invoke-static {v3, v4, v0}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v3

    if-eqz v3, :cond_0

    :goto_0
    invoke-virtual {p0, v2, v0}, Lcom/android/settings/development/DevelopmentSettings;->aVK(Landroid/preference/CheckBoxPreference;Z)V

    return-void

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method private aVu()V
    .locals 6

    const/4 v5, 0x1

    const/4 v4, 0x0

    const-string/jumbo v0, "appops"

    invoke-virtual {p0, v0}, Lcom/android/settings/development/DevelopmentSettings;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AppOpsManager;

    sget-object v1, Lcom/android/settings/development/DevelopmentSettings;->bgm:[I

    invoke-virtual {v0, v1}, Landroid/app/AppOpsManager;->getPackagesForOps([I)Ljava/util/List;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AppOpsManager$PackageOps;

    invoke-virtual {v0}, Landroid/app/AppOpsManager$PackageOps;->getOps()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AppOpsManager$OpEntry;

    invoke-virtual {v0}, Landroid/app/AppOpsManager$OpEntry;->getMode()I

    move-result v0

    if-nez v0, :cond_0

    invoke-interface {v1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AppOpsManager$PackageOps;

    invoke-virtual {v0}, Landroid/app/AppOpsManager$PackageOps;->getPackageName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/development/DevelopmentSettings;->bhy:Ljava/lang/String;

    :cond_1
    iget-object v0, p0, Lcom/android/settings/development/DevelopmentSettings;->bhy:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/android/settings/development/DevelopmentSettings;->bhy:Ljava/lang/String;

    :try_start_0
    invoke-virtual {p0}, Lcom/android/settings/development/DevelopmentSettings;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    iget-object v2, p0, Lcom/android/settings/development/DevelopmentSettings;->bhy:Ljava/lang/String;

    const/16 v3, 0x200

    invoke-virtual {v1, v2, v3}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v1

    invoke-virtual {p0}, Lcom/android/settings/development/DevelopmentSettings;->bWA()Landroid/content/pm/PackageManager;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/content/pm/PackageManager;->getApplicationLabel(Landroid/content/pm/ApplicationInfo;)Ljava/lang/CharSequence;

    move-result-object v1

    if-eqz v1, :cond_2

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    :cond_2
    :goto_0
    iget-object v1, p0, Lcom/android/settings/development/DevelopmentSettings;->bhz:Landroid/preference/Preference;

    new-array v2, v5, [Ljava/lang/Object;

    aput-object v0, v2, v4

    const v0, 0x7f120ab2

    invoke-virtual {p0, v0, v2}, Lcom/android/settings/development/DevelopmentSettings;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    iput-boolean v5, p0, Lcom/android/settings/development/DevelopmentSettings;->bho:Z

    :goto_1
    return-void

    :cond_3
    iget-object v0, p0, Lcom/android/settings/development/DevelopmentSettings;->bhz:Landroid/preference/Preference;

    const v1, 0x7f120ab1

    invoke-virtual {p0, v1}, Lcom/android/settings/development/DevelopmentSettings;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    goto :goto_1

    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method private aVv()V
    .locals 3

    iget-object v0, p0, Lcom/android/settings/development/DevelopmentSettings;->bhl:Landroid/preference/CheckBoxPreference;

    const-string/jumbo v1, "debug.egl.force_msaa"

    const/4 v2, 0x0

    invoke-static {v1, v2}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/android/settings/development/DevelopmentSettings;->aVK(Landroid/preference/CheckBoxPreference;Z)V

    return-void
.end method

.method private aVw()V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/android/settings/development/DevelopmentSettings;->bhf:Lcom/android/settingslib/MiuiRestrictedSwitchPreference;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/settings/development/DevelopmentSettings;->bhf:Lcom/android/settingslib/MiuiRestrictedSwitchPreference;

    invoke-virtual {p0}, Lcom/android/settings/development/DevelopmentSettings;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-static {v1}, Lcom/android/settings/development/f;->aWZ(Landroid/content/Context;)Z

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/android/settings/development/DevelopmentSettings;->aVA(Landroid/preference/SwitchPreference;Z)V

    invoke-direct {p0}, Lcom/android/settings/development/DevelopmentSettings;->aVx()V

    iget-object v0, p0, Lcom/android/settings/development/DevelopmentSettings;->bhf:Lcom/android/settingslib/MiuiRestrictedSwitchPreference;

    invoke-virtual {v0, v2}, Lcom/android/settingslib/MiuiRestrictedSwitchPreference;->setDisabledByAdmin(Lcom/android/settingslib/n;)V

    iget-object v0, p0, Lcom/android/settings/development/DevelopmentSettings;->bhf:Lcom/android/settingslib/MiuiRestrictedSwitchPreference;

    invoke-direct {p0}, Lcom/android/settings/development/DevelopmentSettings;->aUE()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/android/settingslib/MiuiRestrictedSwitchPreference;->setEnabled(Z)V

    iget-object v0, p0, Lcom/android/settings/development/DevelopmentSettings;->bhf:Lcom/android/settingslib/MiuiRestrictedSwitchPreference;

    invoke-virtual {v0}, Lcom/android/settingslib/MiuiRestrictedSwitchPreference;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/development/DevelopmentSettings;->bhf:Lcom/android/settingslib/MiuiRestrictedSwitchPreference;

    const-string/jumbo v1, "no_factory_reset"

    invoke-virtual {v0, v1}, Lcom/android/settingslib/MiuiRestrictedSwitchPreference;->cqt(Ljava/lang/String;)V

    :cond_0
    iget-object v0, p0, Lcom/android/settings/development/DevelopmentSettings;->bhf:Lcom/android/settingslib/MiuiRestrictedSwitchPreference;

    invoke-virtual {v0}, Lcom/android/settingslib/MiuiRestrictedSwitchPreference;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/settings/development/DevelopmentSettings;->bhf:Lcom/android/settingslib/MiuiRestrictedSwitchPreference;

    const-string/jumbo v1, "no_oem_unlock"

    invoke-virtual {v0, v1}, Lcom/android/settingslib/MiuiRestrictedSwitchPreference;->cqt(Ljava/lang/String;)V

    :cond_1
    return-void
.end method

.method private aVx()V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/development/DevelopmentSettings;->bhf:Lcom/android/settingslib/MiuiRestrictedSwitchPreference;

    if-eqz v0, :cond_1

    const v0, 0x7f120c0f

    invoke-direct {p0}, Lcom/android/settings/development/DevelopmentSettings;->aUJ()Z

    move-result v1

    if-eqz v1, :cond_2

    const v0, 0x7f120c0b

    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/android/settings/development/DevelopmentSettings;->bhf:Lcom/android/settingslib/MiuiRestrictedSwitchPreference;

    invoke-virtual {p0, v0}, Lcom/android/settings/development/DevelopmentSettings;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/android/settingslib/MiuiRestrictedSwitchPreference;->setSummary(Ljava/lang/CharSequence;)V

    :cond_1
    return-void

    :cond_2
    invoke-direct {p0}, Lcom/android/settings/development/DevelopmentSettings;->aUL()Z

    move-result v1

    if-eqz v1, :cond_3

    const v0, 0x7f120c0e

    goto :goto_0

    :cond_3
    iget-object v1, p0, Lcom/android/settings/development/DevelopmentSettings;->bhX:Landroid/os/UserManager;

    invoke-static {v1}, Lcom/android/settings/development/f;->aWY(Landroid/os/UserManager;)Z

    move-result v1

    if-nez v1, :cond_0

    const v0, 0x7f120c0d

    goto :goto_0
.end method

.method private aVy()V
    .locals 5

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/android/settings/development/DevelopmentSettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v1, "overlay_display_devices"

    invoke-static {v0, v1}, Landroid/provider/Settings$Global;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    const-string/jumbo v0, ""

    :cond_0
    iget-object v1, p0, Lcom/android/settings/development/DevelopmentSettings;->bhB:Landroid/preference/ListPreference;

    invoke-virtual {v1}, Landroid/preference/ListPreference;->getEntryValues()[Ljava/lang/CharSequence;

    move-result-object v3

    move v1, v2

    :goto_0
    array-length v4, v3

    if-ge v1, v4, :cond_2

    aget-object v4, v3, v1

    invoke-virtual {v0, v4}, Ljava/lang/String;->contentEquals(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_1

    iget-object v0, p0, Lcom/android/settings/development/DevelopmentSettings;->bhB:Landroid/preference/ListPreference;

    invoke-virtual {v0, v1}, Landroid/preference/ListPreference;->setValueIndex(I)V

    iget-object v0, p0, Lcom/android/settings/development/DevelopmentSettings;->bhB:Landroid/preference/ListPreference;

    iget-object v2, p0, Lcom/android/settings/development/DevelopmentSettings;->bhB:Landroid/preference/ListPreference;

    invoke-virtual {v2}, Landroid/preference/ListPreference;->getEntries()[Ljava/lang/CharSequence;

    move-result-object v2

    aget-object v1, v2, v1

    invoke-virtual {v0, v1}, Landroid/preference/ListPreference;->setSummary(Ljava/lang/CharSequence;)V

    return-void

    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/android/settings/development/DevelopmentSettings;->bhB:Landroid/preference/ListPreference;

    invoke-virtual {v0, v2}, Landroid/preference/ListPreference;->setValueIndex(I)V

    iget-object v0, p0, Lcom/android/settings/development/DevelopmentSettings;->bhB:Landroid/preference/ListPreference;

    iget-object v1, p0, Lcom/android/settings/development/DevelopmentSettings;->bhB:Landroid/preference/ListPreference;

    invoke-virtual {v1}, Landroid/preference/ListPreference;->getEntries()[Ljava/lang/CharSequence;

    move-result-object v1

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Landroid/preference/ListPreference;->setSummary(Ljava/lang/CharSequence;)V

    return-void
.end method

.method private aVz()V
    .locals 4

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/android/settings/development/DevelopmentSettings;->bhD:Landroid/preference/CheckBoxPreference;

    invoke-virtual {p0}, Lcom/android/settings/development/DevelopmentSettings;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string/jumbo v3, "pointer_location"

    invoke-static {v2, v3, v0}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    if-eqz v2, :cond_0

    const/4 v0, 0x1

    :cond_0
    invoke-virtual {p0, v1, v0}, Lcom/android/settings/development/DevelopmentSettings;->aVK(Landroid/preference/CheckBoxPreference;Z)V

    return-void
.end method

.method private aWA(Ljava/lang/Object;)V
    .locals 2

    const-string/jumbo v1, "debug.hwui.show_non_rect_clip"

    if-nez p1, :cond_0

    const-string/jumbo v0, ""

    :goto_0
    invoke-static {v1, v0}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/android/settings/development/DevelopmentSettings;->aUM()V

    invoke-direct {p0}, Lcom/android/settings/development/DevelopmentSettings;->aVF()V

    return-void

    :cond_0
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private aWB()V
    .locals 3

    invoke-virtual {p0}, Lcom/android/settings/development/DevelopmentSettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string/jumbo v2, "show_notification_channel_warnings"

    iget-object v0, p0, Lcom/android/settings/development/DevelopmentSettings;->bhM:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v1, v2, v0}, Landroid/provider/Settings$Global;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private aWC()V
    .locals 3

    invoke-virtual {p0}, Lcom/android/settings/development/DevelopmentSettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string/jumbo v2, "show_touches"

    iget-object v0, p0, Lcom/android/settings/development/DevelopmentSettings;->bhO:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v1, v2, v0}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private aWD()V
    .locals 5

    const/4 v0, 0x0

    :try_start_0
    const-string/jumbo v1, "SurfaceFlinger"

    invoke-static {v1}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v2

    const-string/jumbo v3, "android.ui.ISurfaceComposer"

    invoke-virtual {v2, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/android/settings/development/DevelopmentSettings;->bhN:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v3}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 v0, 0x1

    :cond_0
    invoke-virtual {v2, v0}, Landroid/os/Parcel;->writeInt(I)V

    const/16 v0, 0x3ea

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-interface {v1, v0, v2, v3, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    invoke-direct {p0}, Lcom/android/settings/development/DevelopmentSettings;->aVk()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_1
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method private aWE(Ljava/lang/Object;)V
    .locals 4

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/android/settings/development/DevelopmentSettings;->bWB()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    if-gez v1, :cond_0

    const-string/jumbo v1, "accessibility_display_daltonizer_enabled"

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    :goto_0
    return-void

    :cond_0
    const-string/jumbo v2, "accessibility_display_daltonizer_enabled"

    const/4 v3, 0x1

    invoke-static {v0, v2, v3}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    const-string/jumbo v2, "accessibility_display_daltonizer"

    invoke-static {v0, v2, v1}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    goto :goto_0
.end method

.method private aWF()V
    .locals 2

    :try_start_0
    iget-object v1, p0, Lcom/android/settings/development/DevelopmentSettings;->bil:Landroid/view/IWindowManager;

    iget-object v0, p0, Lcom/android/settings/development/DevelopmentSettings;->bhQ:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string/jumbo v0, "1"

    :goto_0
    invoke-interface {v1, v0}, Landroid/view/IWindowManager;->setStrictModeVisualIndicatorPreference(Ljava/lang/String;)V

    :goto_1
    return-void

    :cond_0
    const-string/jumbo v0, ""
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    goto :goto_1
.end method

.method private aWG(Ljava/lang/Object;)V
    .locals 2

    const-string/jumbo v1, "debug.hwui.profile"

    if-nez p1, :cond_0

    const-string/jumbo v0, ""

    :goto_0
    invoke-static {v1, v0}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/android/settings/development/DevelopmentSettings;->aUM()V

    invoke-direct {p0}, Lcom/android/settings/development/DevelopmentSettings;->aVM()V

    return-void

    :cond_0
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private aWH()V
    .locals 3

    invoke-virtual {p0}, Lcom/android/settings/development/DevelopmentSettings;->bWB()Landroid/content/ContentResolver;

    move-result-object v1

    const-string/jumbo v2, "usb_audio_automatic_routing_disabled"

    iget-object v0, p0, Lcom/android/settings/development/DevelopmentSettings;->bhW:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v1, v2, v0}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private aWI(Ljava/lang/Object;)V
    .locals 3

    invoke-virtual {p0}, Lcom/android/settings/development/DevelopmentSettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const-string/jumbo v1, "usb"

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/usb/UsbManager;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "none"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/hardware/usb/UsbManager;->setCurrentFunction(Ljava/lang/String;Z)V

    :goto_0
    return-void

    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/hardware/usb/UsbManager;->setCurrentFunction(Ljava/lang/String;Z)V

    goto :goto_0
.end method

.method private aWJ()V
    .locals 2

    iget-object v1, p0, Lcom/android/settings/development/DevelopmentSettings;->bii:Landroid/net/wifi/WifiManager;

    iget-object v0, p0, Lcom/android/settings/development/DevelopmentSettings;->bif:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, v0}, Landroid/net/wifi/WifiManager;->enableAggressiveHandover(I)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private aWK()V
    .locals 2

    iget-object v1, p0, Lcom/android/settings/development/DevelopmentSettings;->bii:Landroid/net/wifi/WifiManager;

    iget-object v0, p0, Lcom/android/settings/development/DevelopmentSettings;->big:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, v0}, Landroid/net/wifi/WifiManager;->setAllowScansWithTraffic(I)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private aWL()V
    .locals 3

    invoke-virtual {p0}, Lcom/android/settings/development/DevelopmentSettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string/jumbo v2, "wifi_display_certification_on"

    iget-object v0, p0, Lcom/android/settings/development/DevelopmentSettings;->bih:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v1, v2, v0}, Landroid/provider/Settings$Global;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private aWM()V
    .locals 2

    iget-object v1, p0, Lcom/android/settings/development/DevelopmentSettings;->bii:Landroid/net/wifi/WifiManager;

    iget-object v0, p0, Lcom/android/settings/development/DevelopmentSettings;->bij:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, v0}, Landroid/net/wifi/WifiManager;->enableVerboseLogging(I)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic aWN(Lcom/android/settings/development/DevelopmentSettings;)Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/development/DevelopmentSettings;->bgw:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic aWO(Lcom/android/settings/development/DevelopmentSettings;)Landroid/preference/CheckBoxPreference;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/development/DevelopmentSettings;->bhi:Landroid/preference/CheckBoxPreference;

    return-object v0
.end method

.method static synthetic aWP(Lcom/android/settings/development/DevelopmentSettings;)Lcom/android/internal/widget/LockPatternUtils;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/development/DevelopmentSettings;->mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

    return-object v0
.end method

.method static synthetic aWQ(Lcom/android/settings/development/DevelopmentSettings;Landroid/bluetooth/BluetoothA2dp;)Landroid/bluetooth/BluetoothA2dp;
    .locals 0

    iput-object p1, p0, Lcom/android/settings/development/DevelopmentSettings;->bgv:Landroid/bluetooth/BluetoothA2dp;

    return-object p1
.end method

.method static synthetic aWR(Lcom/android/settings/development/DevelopmentSettings;)Landroid/content/ContentResolver;
    .locals 1

    invoke-virtual {p0}, Lcom/android/settings/development/DevelopmentSettings;->bWB()Landroid/content/ContentResolver;

    move-result-object v0

    return-object v0
.end method

.method static synthetic aWS()Z
    .locals 1

    invoke-static {}, Lcom/android/settings/development/DevelopmentSettings;->aUU()Z

    move-result v0

    return v0
.end method

.method static synthetic aWT(Lcom/android/settings/development/DevelopmentSettings;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/settings/development/DevelopmentSettings;->aUx(Z)V

    return-void
.end method

.method static synthetic aWU(Lcom/android/settings/development/DevelopmentSettings;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/development/DevelopmentSettings;->aUW()V

    return-void
.end method

.method static synthetic aWV(Lcom/android/settings/development/DevelopmentSettings;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/development/DevelopmentSettings;->aVa()V

    return-void
.end method

.method static synthetic aWW(Lcom/android/settings/development/DevelopmentSettings;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/development/DevelopmentSettings;->aVO()V

    return-void
.end method

.method private aWa()V
    .locals 2

    invoke-static {}, Landroid/bluetooth/BluetoothAdapter;->getDefaultAdapter()Landroid/bluetooth/BluetoothAdapter;

    const-string/jumbo v0, "persist.bluetooth.btsnoopenable"

    iget-object v1, p0, Lcom/android/settings/development/DevelopmentSettings;->bgJ:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v1}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method private aWb()V
    .locals 3

    invoke-virtual {p0}, Lcom/android/settings/development/DevelopmentSettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string/jumbo v2, "bluetooth_trace_log"

    iget-object v0, p0, Lcom/android/settings/development/DevelopmentSettings;->bgK:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v1, v2, v0}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private aWc()V
    .locals 3

    const-string/jumbo v1, "persist.sys.debug.color_temp"

    iget-object v0, p0, Lcom/android/settings/development/DevelopmentSettings;->bgO:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string/jumbo v0, "1"

    :goto_0
    invoke-static {v1, v0}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/android/settings/development/DevelopmentSettings;->aUM()V

    invoke-virtual {p0}, Lcom/android/settings/development/DevelopmentSettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const v1, 0x7f120433

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    return-void

    :cond_0
    const-string/jumbo v0, "0"

    goto :goto_0
.end method

.method private aWd(Ljava/lang/Object;)V
    .locals 2

    const-string/jumbo v1, "debug.hwui.overdraw"

    if-nez p1, :cond_0

    const-string/jumbo v0, ""

    :goto_0
    invoke-static {v1, v0}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/android/settings/development/DevelopmentSettings;->aUM()V

    invoke-direct {p0}, Lcom/android/settings/development/DevelopmentSettings;->aVf()V

    return-void

    :cond_0
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private aWe(Ljava/lang/Object;)V
    .locals 2

    const-string/jumbo v1, "debug.hwui.renderer"

    if-nez p1, :cond_0

    const-string/jumbo v0, ""

    :goto_0
    invoke-static {v1, v0}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/android/settings/development/DevelopmentSettings;->aUM()V

    invoke-direct {p0}, Lcom/android/settings/development/DevelopmentSettings;->aVg()V

    return-void

    :cond_0
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private aWf()V
    .locals 2

    const-string/jumbo v1, "debug.layout"

    iget-object v0, p0, Lcom/android/settings/development/DevelopmentSettings;->bgT:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string/jumbo v0, "true"

    :goto_0
    invoke-static {v1, v0}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/android/settings/development/DevelopmentSettings;->aUM()V

    return-void

    :cond_0
    const-string/jumbo v0, "false"

    goto :goto_0
.end method

.method private aWg()V
    .locals 4

    :try_start_0
    invoke-static {}, Landroid/app/ActivityManager;->getService()Landroid/app/IActivityManager;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/development/DevelopmentSettings;->bgP:Ljava/lang/String;

    iget-object v2, p0, Lcom/android/settings/development/DevelopmentSettings;->bic:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v2}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v2

    const/4 v3, 0x1

    invoke-interface {v0, v1, v2, v3}, Landroid/app/IActivityManager;->setDebugApp(Ljava/lang/String;ZZ)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method private aWh()V
    .locals 4

    new-instance v1, Landroid/content/Intent;

    const-string/jumbo v0, "com.android.systemui.demo"

    invoke-direct {v1, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string/jumbo v0, "com.android.systemui"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    iget-object v0, p0, Lcom/android/settings/development/DevelopmentSettings;->bgW:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    const-string/jumbo v2, "command"

    const-string/jumbo v3, "enter"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    :goto_0
    invoke-virtual {p0}, Lcom/android/settings/development/DevelopmentSettings;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/android/settings/development/DevelopmentSettings;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string/jumbo v2, "sysui_tuner_demo_on"

    invoke-static {v1, v2, v0}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    return-void

    :cond_0
    const/4 v0, 0x0

    const-string/jumbo v2, "command"

    const-string/jumbo v3, "exit"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_0
.end method

.method private aWi()V
    .locals 5

    const/4 v0, 0x0

    :try_start_0
    const-string/jumbo v1, "SurfaceFlinger"

    invoke-static {v1}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v2

    const-string/jumbo v3, "android.ui.ISurfaceComposer"

    invoke-virtual {v2, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/android/settings/development/DevelopmentSettings;->bha:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v3}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 v0, 0x1

    :cond_0
    invoke-virtual {v2, v0}, Landroid/os/Parcel;->writeInt(I)V

    const/16 v0, 0x3f0

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-interface {v1, v0, v2, v3, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    invoke-direct {p0}, Lcom/android/settings/development/DevelopmentSettings;->aVk()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_1
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method private aWj()V
    .locals 3

    invoke-virtual {p0}, Lcom/android/settings/development/DevelopmentSettings;->bWB()Landroid/content/ContentResolver;

    move-result-object v1

    const-string/jumbo v2, "force_resizable_activities"

    iget-object v0, p0, Lcom/android/settings/development/DevelopmentSettings;->bhm:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v1, v2, v0}, Landroid/provider/Settings$Global;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private aWk()V
    .locals 4

    iget-object v0, p0, Lcom/android/settings/development/DevelopmentSettings;->bhn:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v1

    invoke-virtual {p0}, Lcom/android/settings/development/DevelopmentSettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string/jumbo v3, "debug.force_rtl"

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v2, v3, v0}, Landroid/provider/Settings$Global;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    const-string/jumbo v2, "debug.force_rtl"

    if-eqz v1, :cond_1

    const-string/jumbo v0, "1"

    :goto_1
    invoke-static {v2, v0}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/android/settings/development/DevelopmentSettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Configuration;->getLocales()Landroid/os/LocaleList;

    move-result-object v0

    invoke-static {v0}, Lcom/android/internal/app/LocalePicker;->updateLocales(Landroid/os/LocaleList;)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    const-string/jumbo v0, "0"

    goto :goto_1
.end method

.method private aWl()V
    .locals 2

    const-string/jumbo v1, "persist.sys.ui.hw"

    iget-object v0, p0, Lcom/android/settings/development/DevelopmentSettings;->bhk:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string/jumbo v0, "true"

    :goto_0
    invoke-static {v1, v0}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/android/settings/development/DevelopmentSettings;->aUM()V

    return-void

    :cond_0
    const-string/jumbo v0, "false"

    goto :goto_0
.end method

.method private aWm()V
    .locals 2

    :try_start_0
    invoke-static {}, Landroid/app/ActivityManager;->getService()Landroid/app/IActivityManager;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/development/DevelopmentSettings;->bhp:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v1}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v1

    invoke-interface {v0, v1}, Landroid/app/IActivityManager;->setAlwaysFinish(Z)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method private aWn(Ljava/lang/Object;)V
    .locals 2

    if-nez p1, :cond_0

    return-void

    :cond_0
    const-string/jumbo v0, "persist.logd.limit"

    const-string/jumbo v1, ""

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    const-string/jumbo v0, "persist.logd.limit"

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    const-string/jumbo v0, "ctl.start"

    const-string/jumbo v1, "logd-reinit"

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/android/settings/development/DevelopmentSettings;->aUM()V

    invoke-direct {p0}, Lcom/android/settings/development/DevelopmentSettings;->aVq()V

    :cond_1
    return-void
.end method

.method private aWo(Ljava/lang/Object;)V
    .locals 5

    const/4 v0, 0x0

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "32768"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    :cond_0
    const-string/jumbo v1, "persist.log.tag"

    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_1

    const-string/jumbo v1, ""

    :cond_1
    const-string/jumbo v2, ",+Settings"

    const-string/jumbo v3, ""

    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v3, "^Settings,*"

    const-string/jumbo v4, ""

    invoke-virtual {v2, v3, v4}, Ljava/lang/String;->replaceFirst(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v3, ",+"

    const-string/jumbo v4, ","

    invoke-virtual {v2, v3, v4}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v3, ",+$"

    const-string/jumbo v4, ""

    invoke-virtual {v2, v3, v4}, Ljava/lang/String;->replaceFirst(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    if-eqz v0, :cond_5

    const-string/jumbo p1, "65536"

    const-string/jumbo v0, "persist.log.tag.snet_event_log"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_4

    :cond_2
    const-string/jumbo v0, "log.tag.snet_event_log"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_4

    :cond_3
    const-string/jumbo v0, "persist.log.tag.snet_event_log"

    const-string/jumbo v3, "I"

    invoke-static {v0, v3}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    :cond_4
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v0

    if-eqz v0, :cond_9

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, ","

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "Settings"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    :cond_5
    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_6

    const-string/jumbo v0, "persist.log.tag"

    invoke-static {v0, v2}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    :cond_6
    invoke-direct {p0}, Lcom/android/settings/development/DevelopmentSettings;->aUB()Ljava/lang/String;

    move-result-object v1

    if-eqz p1, :cond_8

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-eqz v0, :cond_8

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_1
    const-string/jumbo v2, "persist.logd.size"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    const-string/jumbo v0, ""

    :cond_7
    invoke-static {v2, v0}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    const-string/jumbo v0, "ctl.start"

    const-string/jumbo v1, "logd-reinit"

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/android/settings/development/DevelopmentSettings;->aUM()V

    invoke-direct {p0}, Lcom/android/settings/development/DevelopmentSettings;->aVr()V

    return-void

    :cond_8
    move-object v0, v1

    goto :goto_1

    :cond_9
    move-object v0, v2

    goto :goto_0
.end method

.method private aWp(Ljava/lang/Object;Z)V
    .locals 4

    const/4 v0, 0x0

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/android/settings/development/DevelopmentSettings;->bhu:Landroid/preference/ListPreference;

    if-nez v2, :cond_0

    return-void

    :cond_0
    const-string/jumbo v2, "persist.log.tag"

    invoke-static {v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_1

    const-string/jumbo v3, "Settings"

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 p2, 0x1

    move-object p1, v1

    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    :cond_2
    if-eqz p2, :cond_4

    iput-boolean v0, p0, Lcom/android/settings/development/DevelopmentSettings;->bhw:Z

    :cond_3
    return-void

    :cond_4
    iget-boolean v0, p0, Lcom/android/settings/development/DevelopmentSettings;->bhw:Z

    if-nez v0, :cond_3

    const-string/jumbo v0, "logd.logpersistd"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_3

    const-string/jumbo v1, "logcatd"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/android/settings/development/DevelopmentSettings;->bhv:Landroid/app/Dialog;

    if-eqz v0, :cond_5

    invoke-direct {p0}, Lcom/android/settings/development/DevelopmentSettings;->aUD()V

    :cond_5
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/android/settings/development/DevelopmentSettings;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {p0}, Lcom/android/settings/development/DevelopmentSettings;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f120575

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f120576

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x1040013

    invoke-virtual {v0, v1, p0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x1040009

    invoke-virtual {v0, v1, p0}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/development/DevelopmentSettings;->bhv:Landroid/app/Dialog;

    iget-object v0, p0, Lcom/android/settings/development/DevelopmentSettings;->bhv:Landroid/app/Dialog;

    invoke-virtual {v0, p0}, Landroid/app/Dialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    return-void

    :cond_6
    const-string/jumbo v1, "logd.logpersistd.buffer"

    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_7

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    xor-int/lit8 v1, v1, 0x1

    :cond_7
    invoke-virtual {p0}, Lcom/android/settings/development/DevelopmentSettings;->aUM()V

    :goto_0
    const/4 v1, 0x3

    if-ge v0, v1, :cond_8

    const-string/jumbo v1, "logd.logpersistd"

    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_9

    const-string/jumbo v2, "logcatd"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_9

    :cond_8
    invoke-direct {p0}, Lcom/android/settings/development/DevelopmentSettings;->aVs()V

    return-void

    :cond_9
    const-wide/16 v2, 0x64

    :try_start_0
    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_1
.end method

.method private aWq(Ljava/lang/Object;)V
    .locals 3

    instance-of v0, p1, Ljava/lang/Boolean;

    if-nez v0, :cond_0

    return-void

    :cond_0
    check-cast p1, Ljava/lang/Boolean;

    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/android/settings/development/DevelopmentSettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-direct {v1, v0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    const v0, 0x7f120c23

    :goto_0
    invoke-virtual {v1, v0}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_2

    const v0, 0x7f120c22

    :goto_1
    invoke-virtual {v1, v0}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_3

    const v0, 0x7f120c24

    :goto_2
    new-instance v2, Lcom/android/settings/development/n;

    invoke-direct {v2, p0, p1}, Lcom/android/settings/development/n;-><init>(Lcom/android/settings/development/DevelopmentSettings;Ljava/lang/Boolean;)V

    invoke-virtual {v1, v0, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f120c29

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    iget-object v0, p0, Lcom/android/settings/development/DevelopmentSettings;->bhi:Landroid/preference/CheckBoxPreference;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    xor-int/lit8 v1, v1, 0x1

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    return-void

    :cond_1
    const v0, 0x7f12041d

    goto :goto_0

    :cond_2
    const v0, 0x7f12041c

    goto :goto_1

    :cond_3
    const v0, 0x7f12041e

    goto :goto_2
.end method

.method private aWr()V
    .locals 3

    invoke-virtual {p0}, Lcom/android/settings/development/DevelopmentSettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string/jumbo v2, "mobile_data_always_on"

    iget-object v0, p0, Lcom/android/settings/development/DevelopmentSettings;->bhx:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v1, v2, v0}, Landroid/provider/Settings$Global;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private aWs()V
    .locals 8

    const/4 v7, 0x2

    const/4 v6, 0x0

    const-string/jumbo v0, "appops"

    invoke-virtual {p0, v0}, Lcom/android/settings/development/DevelopmentSettings;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AppOpsManager;

    sget-object v1, Lcom/android/settings/development/DevelopmentSettings;->bgm:[I

    invoke-virtual {v0, v1}, Landroid/app/AppOpsManager;->getPackagesForOps([I)Ljava/util/List;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/AppOpsManager$PackageOps;

    invoke-virtual {v1}, Landroid/app/AppOpsManager$PackageOps;->getOps()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/app/AppOpsManager$OpEntry;

    invoke-virtual {v2}, Landroid/app/AppOpsManager$OpEntry;->getMode()I

    move-result v2

    if-eq v2, v7, :cond_0

    invoke-virtual {v1}, Landroid/app/AppOpsManager$PackageOps;->getPackageName()Ljava/lang/String;

    move-result-object v1

    :try_start_0
    invoke-virtual {p0}, Lcom/android/settings/development/DevelopmentSettings;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Activity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    const/16 v4, 0x200

    invoke-virtual {v2, v1, v4}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v2

    iget v2, v2, Landroid/content/pm/ApplicationInfo;->uid:I

    const/16 v4, 0x3a

    const/4 v5, 0x2

    invoke-virtual {v0, v4, v2, v1, v5}, Landroid/app/AppOpsManager;->setMode(IILjava/lang/String;I)V
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_0

    :cond_1
    iget-object v1, p0, Lcom/android/settings/development/DevelopmentSettings;->bhy:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    :try_start_1
    invoke-virtual {p0}, Lcom/android/settings/development/DevelopmentSettings;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    iget-object v2, p0, Lcom/android/settings/development/DevelopmentSettings;->bhy:Ljava/lang/String;

    const/16 v3, 0x200

    invoke-virtual {v1, v2, v3}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v1

    iget v1, v1, Landroid/content/pm/ApplicationInfo;->uid:I

    iget-object v2, p0, Lcom/android/settings/development/DevelopmentSettings;->bhy:Ljava/lang/String;

    const/16 v3, 0x3a

    const/4 v4, 0x0

    invoke-virtual {v0, v3, v1, v2, v4}, Landroid/app/AppOpsManager;->setMode(IILjava/lang/String;I)V
    :try_end_1
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_1 .. :try_end_1} :catch_1

    :cond_2
    :goto_1
    return-void

    :catch_1
    move-exception v0

    goto :goto_1
.end method

.method private aWt()V
    .locals 2

    const-string/jumbo v1, "debug.egl.force_msaa"

    iget-object v0, p0, Lcom/android/settings/development/DevelopmentSettings;->bhl:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string/jumbo v0, "true"

    :goto_0
    invoke-static {v1, v0}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/android/settings/development/DevelopmentSettings;->aUM()V

    return-void

    :cond_0
    const-string/jumbo v0, "false"

    goto :goto_0
.end method

.method private aWu(Ljava/lang/Object;)V
    .locals 2

    invoke-virtual {p0}, Lcom/android/settings/development/DevelopmentSettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v1, "overlay_display_devices"

    check-cast p1, Ljava/lang/String;

    invoke-static {v0, v1, p1}, Landroid/provider/Settings$Global;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    invoke-direct {p0}, Lcom/android/settings/development/DevelopmentSettings;->aVy()V

    return-void
.end method

.method private aWv()V
    .locals 3

    invoke-virtual {p0}, Lcom/android/settings/development/DevelopmentSettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string/jumbo v2, "pointer_location"

    iget-object v0, p0, Lcom/android/settings/development/DevelopmentSettings;->bhD:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v1, v2, v0}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private aWw()V
    .locals 3

    invoke-virtual {p0}, Lcom/android/settings/development/DevelopmentSettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string/jumbo v2, "anr_show_background"

    iget-object v0, p0, Lcom/android/settings/development/DevelopmentSettings;->bhH:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v1, v2, v0}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private aWx()V
    .locals 3

    invoke-virtual {p0}, Lcom/android/settings/development/DevelopmentSettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    sget-object v2, Landroid/provider/MiuiSettings$Secure;->FORCE_CLOCE_DIALOG_ENABLED:Ljava/lang/String;

    iget-object v0, p0, Lcom/android/settings/development/DevelopmentSettings;->bhI:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v1, v2, v0}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private aWy()V
    .locals 2

    const-string/jumbo v1, "debug.hwui.show_layers_updates"

    iget-object v0, p0, Lcom/android/settings/development/DevelopmentSettings;->bhJ:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string/jumbo v0, "true"

    :goto_0
    invoke-static {v1, v0}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/android/settings/development/DevelopmentSettings;->aUM()V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private aWz()V
    .locals 2

    const-string/jumbo v1, "debug.hwui.show_dirty_regions"

    iget-object v0, p0, Lcom/android/settings/development/DevelopmentSettings;->bhK:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string/jumbo v0, "true"

    :goto_0
    invoke-static {v1, v0}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/android/settings/development/DevelopmentSettings;->aUM()V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method aUM()V
    .locals 2

    iget-boolean v0, p0, Lcom/android/settings/development/DevelopmentSettings;->bhc:Z

    if-nez v0, :cond_0

    new-instance v0, Lcom/android/settings/development/e;

    invoke-direct {v0}, Lcom/android/settings/development/e;-><init>()V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/android/settings/development/e;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    :cond_0
    return-void
.end method

.method public aUy(Z)V
    .locals 3

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/android/settings/development/DevelopmentSettings;->bhG:Lcom/android/settings/development/b;

    invoke-virtual {v0}, Lcom/android/settings/development/b;->aUs()Z

    move-result v0

    if-eq p1, v0, :cond_1

    if-eqz p1, :cond_2

    iput-boolean v1, p0, Lcom/android/settings/development/DevelopmentSettings;->bgZ:Z

    iget-object v0, p0, Lcom/android/settings/development/DevelopmentSettings;->bhe:Landroid/app/Dialog;

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/android/settings/development/DevelopmentSettings;->aUD()V

    :cond_0
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/android/settings/development/DevelopmentSettings;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {p0}, Lcom/android/settings/development/DevelopmentSettings;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f120577

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f120578

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x1040013

    invoke-virtual {v0, v1, p0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x1040009

    invoke-virtual {v0, v1, p0}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/development/DevelopmentSettings;->bhe:Landroid/app/Dialog;

    iget-object v0, p0, Lcom/android/settings/development/DevelopmentSettings;->bhe:Landroid/app/Dialog;

    invoke-virtual {v0, p0}, Landroid/app/Dialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    :cond_1
    :goto_0
    return-void

    :cond_2
    invoke-direct {p0}, Lcom/android/settings/development/DevelopmentSettings;->aUP()V

    iget-object v0, p0, Lcom/android/settings/development/DevelopmentSettings;->bhG:Lcom/android/settings/development/b;

    invoke-virtual {v0}, Lcom/android/settings/development/b;->aUp()V

    invoke-direct {p0, v1}, Lcom/android/settings/development/DevelopmentSettings;->aUS(Z)V

    goto :goto_0
.end method

.method aVA(Landroid/preference/SwitchPreference;Z)V
    .locals 1

    invoke-virtual {p1, p2}, Landroid/preference/SwitchPreference;->setChecked(Z)V

    iget-boolean v0, p0, Lcom/android/settings/development/DevelopmentSettings;->bho:Z

    or-int/2addr v0, p2

    iput-boolean v0, p0, Lcom/android/settings/development/DevelopmentSettings;->bho:Z

    return-void
.end method

.method aVK(Landroid/preference/CheckBoxPreference;Z)V
    .locals 1

    invoke-virtual {p1, p2}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    iget-boolean v0, p0, Lcom/android/settings/development/DevelopmentSettings;->bho:Z

    or-int/2addr v0, p2

    iput-boolean v0, p0, Lcom/android/settings/development/DevelopmentSettings;->bho:Z

    return-void
.end method

.method addDashboardCategoryPreferences()V
    .locals 0

    invoke-virtual {p0}, Lcom/android/settings/development/DevelopmentSettings;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    return-void
.end method

.method public at(Landroid/preference/Preference;)Z
    .locals 5

    const/4 v3, 0x0

    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-static {}, Lcom/android/settings/aq;->bqE()Z

    move-result v2

    if-eqz v2, :cond_0

    return v1

    :cond_0
    iget-object v2, p0, Lcom/android/settings/development/DevelopmentSettings;->bhS:Lcom/android/settings/development/g;

    invoke-virtual {v2, p1}, Lcom/android/settings/development/g;->fm(Landroid/preference/Preference;)Z

    move-result v2

    if-eqz v2, :cond_1

    return v0

    :cond_1
    iget-object v2, p0, Lcom/android/settings/development/DevelopmentSettings;->bid:Lcom/android/settings/webview/a;

    invoke-virtual {v2, p1}, Lcom/android/settings/webview/a;->fm(Landroid/preference/Preference;)Z

    move-result v2

    if-eqz v2, :cond_2

    return v0

    :cond_2
    iget-object v2, p0, Lcom/android/settings/development/DevelopmentSettings;->bib:Lcom/android/settings/development/c;

    invoke-virtual {v2, p1}, Lcom/android/settings/development/c;->fm(Landroid/preference/Preference;)Z

    move-result v2

    if-eqz v2, :cond_3

    return v0

    :cond_3
    iget-object v2, p0, Lcom/android/settings/development/DevelopmentSettings;->bhd:Landroid/preference/CheckBoxPreference;

    if-ne p1, v2, :cond_7

    iget-object v0, p0, Lcom/android/settings/development/DevelopmentSettings;->bhd:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_6

    iput-boolean v1, p0, Lcom/android/settings/development/DevelopmentSettings;->bgZ:Z

    iget-object v0, p0, Lcom/android/settings/development/DevelopmentSettings;->bgn:Landroid/app/Dialog;

    if-eqz v0, :cond_4

    invoke-direct {p0}, Lcom/android/settings/development/DevelopmentSettings;->aUD()V

    :cond_4
    iget-object v0, p0, Lcom/android/settings/development/DevelopmentSettings;->bhd:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/android/settings/development/DevelopmentSettings;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-direct {v0, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {p0}, Lcom/android/settings/development/DevelopmentSettings;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f120098

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v2, 0x7f120099

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v2, 0x1040013

    invoke-virtual {v0, v2, p0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v2, 0x1040009

    invoke-virtual {v0, v2, p0}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/development/DevelopmentSettings;->bgn:Landroid/app/Dialog;

    iget-object v0, p0, Lcom/android/settings/development/DevelopmentSettings;->bgn:Landroid/app/Dialog;

    invoke-virtual {v0, p0}, Landroid/app/Dialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    :cond_5
    :goto_0
    return v1

    :cond_6
    invoke-virtual {p0}, Lcom/android/settings/development/DevelopmentSettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v2, "adb_enabled"

    invoke-static {v0, v2, v1}, Landroid/provider/Settings$Global;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    iget-object v0, p0, Lcom/android/settings/development/DevelopmentSettings;->bib:Lcom/android/settings/development/c;

    invoke-virtual {v0}, Lcom/android/settings/development/c;->aUu()V

    invoke-direct {p0}, Lcom/android/settings/development/DevelopmentSettings;->aVd()V

    goto :goto_0

    :cond_7
    iget-object v2, p0, Lcom/android/settings/development/DevelopmentSettings;->bgM:Landroid/preference/Preference;

    if-ne p1, v2, :cond_9

    iget-object v0, p0, Lcom/android/settings/development/DevelopmentSettings;->bgq:Landroid/app/Dialog;

    if-eqz v0, :cond_8

    invoke-direct {p0}, Lcom/android/settings/development/DevelopmentSettings;->aUD()V

    :cond_8
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/android/settings/development/DevelopmentSettings;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-direct {v0, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v2, 0x7f120097

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v2, 0x104000a

    invoke-virtual {v0, v2, p0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const/high16 v2, 0x1040000

    invoke-virtual {v0, v2, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/development/DevelopmentSettings;->bgq:Landroid/app/Dialog;

    goto :goto_0

    :cond_9
    iget-object v2, p0, Lcom/android/settings/development/DevelopmentSettings;->bhg:Landroid/preference/CheckBoxPreference;

    if-ne p1, v2, :cond_b

    invoke-virtual {p0}, Lcom/android/settings/development/DevelopmentSettings;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Activity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    const-string/jumbo v3, "com.android.terminal"

    iget-object v4, p0, Lcom/android/settings/development/DevelopmentSettings;->bhg:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v4}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v4

    if-eqz v4, :cond_a

    :goto_1
    invoke-virtual {v2, v3, v0, v1}, Landroid/content/pm/PackageManager;->setApplicationEnabledSetting(Ljava/lang/String;II)V

    goto :goto_0

    :cond_a
    move v0, v1

    goto :goto_1

    :cond_b
    iget-object v2, p0, Lcom/android/settings/development/DevelopmentSettings;->bhq:Landroid/preference/CheckBoxPreference;

    if-ne p1, v2, :cond_d

    invoke-virtual {p0}, Lcom/android/settings/development/DevelopmentSettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string/jumbo v3, "stay_on_while_plugged_in"

    iget-object v0, p0, Lcom/android/settings/development/DevelopmentSettings;->bhq:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_c

    const/4 v0, 0x7

    :goto_2
    invoke-static {v2, v3, v0}, Landroid/provider/Settings$Global;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    goto :goto_0

    :cond_c
    move v0, v1

    goto :goto_2

    :cond_d
    iget-object v2, p0, Lcom/android/settings/development/DevelopmentSettings;->bgJ:Landroid/preference/CheckBoxPreference;

    if-ne p1, v2, :cond_e

    invoke-direct {p0}, Lcom/android/settings/development/DevelopmentSettings;->aWa()V

    goto/16 :goto_0

    :cond_e
    iget-object v2, p0, Lcom/android/settings/development/DevelopmentSettings;->bgK:Landroid/preference/CheckBoxPreference;

    if-ne p1, v2, :cond_f

    invoke-direct {p0}, Lcom/android/settings/development/DevelopmentSettings;->aWb()V

    goto/16 :goto_0

    :cond_f
    iget-object v2, p0, Lcom/android/settings/development/DevelopmentSettings;->bhf:Lcom/android/settingslib/MiuiRestrictedSwitchPreference;

    if-ne p1, v2, :cond_11

    iget-object v2, p0, Lcom/android/settings/development/DevelopmentSettings;->bhf:Lcom/android/settingslib/MiuiRestrictedSwitchPreference;

    invoke-virtual {v2}, Lcom/android/settingslib/MiuiRestrictedSwitchPreference;->isEnabled()Z

    move-result v2

    if-eqz v2, :cond_11

    iget-object v0, p0, Lcom/android/settings/development/DevelopmentSettings;->bhf:Lcom/android/settingslib/MiuiRestrictedSwitchPreference;

    invoke-virtual {v0}, Lcom/android/settingslib/MiuiRestrictedSwitchPreference;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_10

    invoke-virtual {p0}, Lcom/android/settings/development/DevelopmentSettings;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-direct {p0, v0, v1}, Lcom/android/settings/development/DevelopmentSettings;->aUV(Landroid/content/res/Resources;I)Z

    move-result v0

    if-nez v0, :cond_5

    invoke-direct {p0}, Lcom/android/settings/development/DevelopmentSettings;->aUz()V

    goto/16 :goto_0

    :cond_10
    invoke-virtual {p0}, Lcom/android/settings/development/DevelopmentSettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0, v1}, Lcom/android/settings/development/f;->aXa(Landroid/content/Context;Z)V

    goto/16 :goto_0

    :cond_11
    iget-object v2, p0, Lcom/android/settings/development/DevelopmentSettings;->bgW:Landroid/preference/CheckBoxPreference;

    if-ne p1, v2, :cond_13

    iget-object v0, p0, Lcom/android/settings/development/DevelopmentSettings;->bgW:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_12

    iput-boolean v1, p0, Lcom/android/settings/development/DevelopmentSettings;->bgZ:Z

    invoke-direct {p0}, Lcom/android/settings/development/DevelopmentSettings;->aUT()V

    goto/16 :goto_0

    :cond_12
    invoke-direct {p0}, Lcom/android/settings/development/DevelopmentSettings;->aWh()V

    goto/16 :goto_0

    :cond_13
    iget-object v2, p0, Lcom/android/settings/development/DevelopmentSettings;->bgY:Lmiui/preference/ValuePreference;

    if-ne p1, v2, :cond_14

    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/android/settings/development/DevelopmentSettings;->getActivity()Landroid/app/Activity;

    move-result-object v2

    const-class v3, Lcom/android/settings/bootloader/BootloaderStatusActivity;

    invoke-direct {v0, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v0}, Lcom/android/settings/development/DevelopmentSettings;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    :cond_14
    iget-object v2, p0, Lcom/android/settings/development/DevelopmentSettings;->bhz:Landroid/preference/Preference;

    if-ne p1, v2, :cond_15

    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/android/settings/development/DevelopmentSettings;->getActivity()Landroid/app/Activity;

    move-result-object v2

    const-class v3, Lcom/android/settings/development/AppPicker;

    invoke-direct {v0, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v2, "com.android.settings.extra.REQUESTIING_PERMISSION"

    const-string/jumbo v3, "android.permission.ACCESS_MOCK_LOCATION"

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const/16 v2, 0x3e9

    invoke-virtual {p0, v0, v2}, Lcom/android/settings/development/DevelopmentSettings;->startActivityForResult(Landroid/content/Intent;I)V

    goto/16 :goto_0

    :cond_15
    iget-object v2, p0, Lcom/android/settings/development/DevelopmentSettings;->bgU:Landroid/preference/CheckBoxPreference;

    if-ne p1, v2, :cond_17

    invoke-virtual {p0}, Lcom/android/settings/development/DevelopmentSettings;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string/jumbo v3, "debug_view_attributes"

    iget-object v4, p0, Lcom/android/settings/development/DevelopmentSettings;->bgU:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v4}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v4

    if-eqz v4, :cond_16

    :goto_3
    invoke-static {v2, v3, v0}, Landroid/provider/Settings$Global;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    goto/16 :goto_0

    :cond_16
    move v0, v1

    goto :goto_3

    :cond_17
    iget-object v2, p0, Lcom/android/settings/development/DevelopmentSettings;->bhj:Landroid/preference/CheckBoxPreference;

    if-ne p1, v2, :cond_19

    invoke-virtual {p0}, Lcom/android/settings/development/DevelopmentSettings;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string/jumbo v3, "force_allow_on_external"

    iget-object v4, p0, Lcom/android/settings/development/DevelopmentSettings;->bhj:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v4}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v4

    if-eqz v4, :cond_18

    :goto_4
    invoke-static {v2, v3, v0}, Landroid/provider/Settings$Global;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    goto/16 :goto_0

    :cond_18
    move v0, v1

    goto :goto_4

    :cond_19
    iget-object v2, p0, Lcom/android/settings/development/DevelopmentSettings;->bgQ:Landroid/preference/Preference;

    if-ne p1, v2, :cond_1a

    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/android/settings/development/DevelopmentSettings;->getActivity()Landroid/app/Activity;

    move-result-object v2

    const-class v3, Lcom/android/settings/MiuiAppPicker;

    invoke-direct {v0, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/16 v2, 0x3e8

    invoke-virtual {p0, v0, v2}, Lcom/android/settings/development/DevelopmentSettings;->startActivityForResult(Landroid/content/Intent;I)V

    goto/16 :goto_0

    :cond_1a
    iget-object v2, p0, Lcom/android/settings/development/DevelopmentSettings;->bic:Landroid/preference/CheckBoxPreference;

    if-ne p1, v2, :cond_1b

    invoke-direct {p0}, Lcom/android/settings/development/DevelopmentSettings;->aWg()V

    goto/16 :goto_0

    :cond_1b
    iget-object v2, p0, Lcom/android/settings/development/DevelopmentSettings;->bhQ:Landroid/preference/CheckBoxPreference;

    if-ne p1, v2, :cond_1c

    invoke-direct {p0}, Lcom/android/settings/development/DevelopmentSettings;->aWF()V

    goto/16 :goto_0

    :cond_1c
    iget-object v2, p0, Lcom/android/settings/development/DevelopmentSettings;->bhD:Landroid/preference/CheckBoxPreference;

    if-ne p1, v2, :cond_1d

    invoke-direct {p0}, Lcom/android/settings/development/DevelopmentSettings;->aWv()V

    goto/16 :goto_0

    :cond_1d
    iget-object v2, p0, Lcom/android/settings/development/DevelopmentSettings;->bhO:Landroid/preference/CheckBoxPreference;

    if-ne p1, v2, :cond_1e

    invoke-direct {p0}, Lcom/android/settings/development/DevelopmentSettings;->aWC()V

    goto/16 :goto_0

    :cond_1e
    iget-object v2, p0, Lcom/android/settings/development/DevelopmentSettings;->bhN:Landroid/preference/CheckBoxPreference;

    if-ne p1, v2, :cond_1f

    invoke-direct {p0}, Lcom/android/settings/development/DevelopmentSettings;->aWD()V

    goto/16 :goto_0

    :cond_1f
    iget-object v2, p0, Lcom/android/settings/development/DevelopmentSettings;->bha:Landroid/preference/CheckBoxPreference;

    if-ne p1, v2, :cond_20

    invoke-direct {p0}, Lcom/android/settings/development/DevelopmentSettings;->aWi()V

    goto/16 :goto_0

    :cond_20
    iget-object v2, p0, Lcom/android/settings/development/DevelopmentSettings;->bhp:Landroid/preference/CheckBoxPreference;

    if-ne p1, v2, :cond_21

    invoke-direct {p0}, Lcom/android/settings/development/DevelopmentSettings;->aWm()V

    goto/16 :goto_0

    :cond_21
    iget-object v2, p0, Lcom/android/settings/development/DevelopmentSettings;->bhH:Landroid/preference/CheckBoxPreference;

    if-ne p1, v2, :cond_22

    invoke-direct {p0}, Lcom/android/settings/development/DevelopmentSettings;->aWw()V

    goto/16 :goto_0

    :cond_22
    iget-object v2, p0, Lcom/android/settings/development/DevelopmentSettings;->bhM:Landroid/preference/CheckBoxPreference;

    if-ne p1, v2, :cond_23

    invoke-direct {p0}, Lcom/android/settings/development/DevelopmentSettings;->aWB()V

    goto/16 :goto_0

    :cond_23
    iget-object v2, p0, Lcom/android/settings/development/DevelopmentSettings;->bhk:Landroid/preference/CheckBoxPreference;

    if-ne p1, v2, :cond_24

    invoke-direct {p0}, Lcom/android/settings/development/DevelopmentSettings;->aWl()V

    goto/16 :goto_0

    :cond_24
    iget-object v2, p0, Lcom/android/settings/development/DevelopmentSettings;->bhl:Landroid/preference/CheckBoxPreference;

    if-ne p1, v2, :cond_25

    invoke-direct {p0}, Lcom/android/settings/development/DevelopmentSettings;->aWt()V

    goto/16 :goto_0

    :cond_25
    iget-object v2, p0, Lcom/android/settings/development/DevelopmentSettings;->bhK:Landroid/preference/CheckBoxPreference;

    if-ne p1, v2, :cond_26

    invoke-direct {p0}, Lcom/android/settings/development/DevelopmentSettings;->aWz()V

    goto/16 :goto_0

    :cond_26
    iget-object v2, p0, Lcom/android/settings/development/DevelopmentSettings;->bhJ:Landroid/preference/CheckBoxPreference;

    if-ne p1, v2, :cond_27

    invoke-direct {p0}, Lcom/android/settings/development/DevelopmentSettings;->aWy()V

    goto/16 :goto_0

    :cond_27
    iget-object v2, p0, Lcom/android/settings/development/DevelopmentSettings;->bgT:Landroid/preference/CheckBoxPreference;

    if-ne p1, v2, :cond_28

    invoke-direct {p0}, Lcom/android/settings/development/DevelopmentSettings;->aWf()V

    goto/16 :goto_0

    :cond_28
    iget-object v2, p0, Lcom/android/settings/development/DevelopmentSettings;->bhn:Landroid/preference/CheckBoxPreference;

    if-ne p1, v2, :cond_29

    invoke-direct {p0}, Lcom/android/settings/development/DevelopmentSettings;->aWk()V

    goto/16 :goto_0

    :cond_29
    iget-object v2, p0, Lcom/android/settings/development/DevelopmentSettings;->bih:Landroid/preference/CheckBoxPreference;

    if-ne p1, v2, :cond_2a

    invoke-direct {p0}, Lcom/android/settings/development/DevelopmentSettings;->aWL()V

    goto/16 :goto_0

    :cond_2a
    iget-object v2, p0, Lcom/android/settings/development/DevelopmentSettings;->bij:Landroid/preference/CheckBoxPreference;

    if-ne p1, v2, :cond_2b

    invoke-direct {p0}, Lcom/android/settings/development/DevelopmentSettings;->aWM()V

    goto/16 :goto_0

    :cond_2b
    iget-object v2, p0, Lcom/android/settings/development/DevelopmentSettings;->bif:Landroid/preference/CheckBoxPreference;

    if-ne p1, v2, :cond_2c

    invoke-direct {p0}, Lcom/android/settings/development/DevelopmentSettings;->aWJ()V

    goto/16 :goto_0

    :cond_2c
    iget-object v2, p0, Lcom/android/settings/development/DevelopmentSettings;->big:Landroid/preference/CheckBoxPreference;

    if-ne p1, v2, :cond_2d

    invoke-direct {p0}, Lcom/android/settings/development/DevelopmentSettings;->aWK()V

    goto/16 :goto_0

    :cond_2d
    iget-object v2, p0, Lcom/android/settings/development/DevelopmentSettings;->bhx:Landroid/preference/CheckBoxPreference;

    if-ne p1, v2, :cond_2e

    invoke-direct {p0}, Lcom/android/settings/development/DevelopmentSettings;->aWr()V

    goto/16 :goto_0

    :cond_2e
    iget-object v2, p0, Lcom/android/settings/development/DevelopmentSettings;->bgO:Landroid/preference/CheckBoxPreference;

    if-ne p1, v2, :cond_2f

    invoke-direct {p0}, Lcom/android/settings/development/DevelopmentSettings;->aWc()V

    goto/16 :goto_0

    :cond_2f
    iget-object v2, p0, Lcom/android/settings/development/DevelopmentSettings;->bhW:Landroid/preference/CheckBoxPreference;

    if-ne p1, v2, :cond_30

    invoke-direct {p0}, Lcom/android/settings/development/DevelopmentSettings;->aWH()V

    goto/16 :goto_0

    :cond_30
    iget-object v2, p0, Lcom/android/settings/development/DevelopmentSettings;->bhm:Landroid/preference/CheckBoxPreference;

    if-ne p1, v2, :cond_31

    invoke-direct {p0}, Lcom/android/settings/development/DevelopmentSettings;->aWj()V

    goto/16 :goto_0

    :cond_31
    iget-object v2, p0, Lcom/android/settings/development/DevelopmentSettings;->bgz:Landroid/preference/CheckBoxPreference;

    if-ne p1, v2, :cond_32

    invoke-direct {p0}, Lcom/android/settings/development/DevelopmentSettings;->aVY()V

    goto/16 :goto_0

    :cond_32
    iget-object v2, p0, Lcom/android/settings/development/DevelopmentSettings;->bgA:Landroid/preference/CheckBoxPreference;

    if-ne p1, v2, :cond_33

    invoke-direct {p0}, Lcom/android/settings/development/DevelopmentSettings;->aVZ()V

    goto/16 :goto_0

    :cond_33
    const-string/jumbo v2, "reset_shortcut_manager_throttling"

    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_34

    invoke-direct {p0}, Lcom/android/settings/development/DevelopmentSettings;->aUR()V

    goto/16 :goto_0

    :cond_34
    iget-object v2, p0, Lcom/android/settings/development/DevelopmentSettings;->bhT:Landroid/preference/CheckBoxPreference;

    if-ne p1, v2, :cond_36

    iget-object v2, p0, Lcom/android/settings/development/DevelopmentSettings;->bhr:Lcom/android/settings/development/d;

    if-eqz v2, :cond_35

    iget-object v2, p0, Lcom/android/settings/development/DevelopmentSettings;->bhr:Lcom/android/settings/development/d;

    invoke-virtual {v2, v1}, Lcom/android/settings/development/d;->cancel(Z)Z

    :cond_35
    new-instance v2, Lcom/android/settings/development/d;

    invoke-direct {v2, p0}, Lcom/android/settings/development/d;-><init>(Lcom/android/settings/development/DevelopmentSettings;)V

    iput-object v2, p0, Lcom/android/settings/development/DevelopmentSettings;->bhr:Lcom/android/settings/development/d;

    iget-object v2, p0, Lcom/android/settings/development/DevelopmentSettings;->bhr:Lcom/android/settings/development/d;

    new-array v0, v0, [Ljava/lang/Boolean;

    iget-object v3, p0, Lcom/android/settings/development/DevelopmentSettings;->bhT:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v3}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v3

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v0, v1

    invoke-virtual {v2, v0}, Lcom/android/settings/development/d;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto/16 :goto_0

    :cond_36
    iget-object v0, p0, Lcom/android/settings/development/DevelopmentSettings;->bhI:Landroid/preference/CheckBoxPreference;

    if-ne p1, v0, :cond_37

    invoke-direct {p0}, Lcom/android/settings/development/DevelopmentSettings;->aWx()V

    goto/16 :goto_0

    :cond_37
    iget-object v0, p0, Lcom/android/settings/development/DevelopmentSettings;->bhh:Landroid/preference/CheckBoxPreference;

    if-ne p1, v0, :cond_38

    iget-object v0, p0, Lcom/android/settings/development/DevelopmentSettings;->bhh:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/android/settings/development/DevelopmentSettings;->aUy(Z)V

    goto/16 :goto_0

    :cond_38
    iget-object v0, p0, Lcom/android/settings/development/DevelopmentSettings;->bgH:Landroid/preference/CheckBoxPreference;

    if-ne p1, v0, :cond_3a

    invoke-static {}, Lcom/android/settings/BootloaderApplyActivity;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_39

    invoke-static {v1}, Lcom/android/settings/BootloaderApplyActivity;->setEnabled(Z)V

    goto/16 :goto_0

    :cond_39
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/android/settings/development/DevelopmentSettings;->getActivity()Landroid/app/Activity;

    move-result-object v2

    const-class v3, Lcom/android/settings/BootloaderApplyActivity;

    invoke-direct {v0, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/16 v2, 0xa

    invoke-virtual {p0, v0, v2}, Lcom/android/settings/development/DevelopmentSettings;->startActivityForResult(Landroid/content/Intent;I)V

    invoke-virtual {p0}, Lcom/android/settings/development/DevelopmentSettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0, v1, v1}, Landroid/app/Activity;->overridePendingTransition(II)V

    goto/16 :goto_0

    :cond_3a
    iget-object v0, p0, Lcom/android/settings/development/DevelopmentSettings;->bgp:Landroid/preference/CheckBoxPreference;

    if-ne p1, v0, :cond_3c

    invoke-virtual {p0}, Lcom/android/settings/development/DevelopmentSettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/android/a/a;->caE(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_3b

    invoke-virtual {p0}, Lcom/android/settings/development/DevelopmentSettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0, v1}, Lcom/android/a/a;->caG(Landroid/content/Context;Z)V

    goto/16 :goto_0

    :cond_3b
    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v2, "com.miui.securitycenter.action.ADB_INSTALL_VERIFY"

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const/16 v2, 0xb

    invoke-virtual {p0, v0, v2}, Lcom/android/settings/development/DevelopmentSettings;->startActivityForResult(Landroid/content/Intent;I)V

    goto/16 :goto_0

    :cond_3c
    iget-object v0, p0, Lcom/android/settings/development/DevelopmentSettings;->bgo:Landroid/preference/CheckBoxPreference;

    if-ne p1, v0, :cond_3e

    invoke-static {}, Lcom/android/a/a;->caD()Z

    move-result v0

    if-eqz v0, :cond_3d

    invoke-static {v1}, Lcom/android/a/a;->caF(Z)V

    goto/16 :goto_0

    :cond_3d
    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v2, "com.miui.securitycenter.action.ADB_INPUT_APPLY"

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const/16 v2, 0xc

    invoke-virtual {p0, v0, v2}, Lcom/android/settings/development/DevelopmentSettings;->startActivityForResult(Landroid/content/Intent;I)V

    invoke-virtual {p0}, Lcom/android/settings/development/DevelopmentSettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0, v1, v1}, Landroid/app/Activity;->overridePendingTransition(II)V

    goto/16 :goto_0

    :cond_3e
    iget-object v0, p0, Lcom/android/settings/development/DevelopmentSettings;->bgV:Landroid/preference/Preference;

    if-ne p1, v0, :cond_5

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    new-instance v2, Landroid/content/ComponentName;

    const-string/jumbo v3, "com.miui.securitycore"

    const-string/jumbo v4, "com.miui.securityspace.ui.activity.RemoveUserActivity"

    invoke-direct {v2, v3, v4}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    const-string/jumbo v2, "remove_user_bussiness_type"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/android/settings/development/DevelopmentSettings;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0
.end method

.method public getMetricsCategory()I
    .locals 1

    const/16 v0, 0x27

    return v0
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 3

    const/4 v2, 0x0

    const/4 v1, -0x1

    const/16 v0, 0x3e8

    if-ne p1, v0, :cond_1

    if-ne p2, v1, :cond_0

    invoke-virtual {p3}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/development/DevelopmentSettings;->bgP:Ljava/lang/String;

    invoke-direct {p0}, Lcom/android/settings/development/DevelopmentSettings;->aWg()V

    invoke-direct {p0}, Lcom/android/settings/development/DevelopmentSettings;->aVi()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/16 v0, 0x3e9

    if-ne p1, v0, :cond_2

    if-ne p2, v1, :cond_0

    invoke-virtual {p3}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/development/DevelopmentSettings;->bhy:Ljava/lang/String;

    invoke-direct {p0}, Lcom/android/settings/development/DevelopmentSettings;->aWs()V

    invoke-direct {p0}, Lcom/android/settings/development/DevelopmentSettings;->aVu()V

    goto :goto_0

    :cond_2
    if-nez p1, :cond_4

    if-ne p2, v1, :cond_0

    iget-object v0, p0, Lcom/android/settings/development/DevelopmentSettings;->bhf:Lcom/android/settingslib/MiuiRestrictedSwitchPreference;

    invoke-virtual {v0}, Lcom/android/settingslib/MiuiRestrictedSwitchPreference;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-direct {p0}, Lcom/android/settings/development/DevelopmentSettings;->aUz()V

    goto :goto_0

    :cond_3
    invoke-virtual {p0}, Lcom/android/settings/development/DevelopmentSettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0, v2}, Lcom/android/settings/development/f;->aXa(Landroid/content/Context;Z)V

    goto :goto_0

    :cond_4
    const/16 v0, 0xa

    if-ne p1, v0, :cond_5

    iget-object v0, p0, Lcom/android/settings/development/DevelopmentSettings;->bgH:Landroid/preference/CheckBoxPreference;

    invoke-static {}, Lcom/android/settings/BootloaderApplyActivity;->isEnabled()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    goto :goto_0

    :cond_5
    const/16 v0, 0xb

    if-ne p1, v0, :cond_6

    iget-object v0, p0, Lcom/android/settings/development/DevelopmentSettings;->bgp:Landroid/preference/CheckBoxPreference;

    invoke-virtual {p0}, Lcom/android/settings/development/DevelopmentSettings;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-static {v1}, Lcom/android/a/a;->caE(Landroid/content/Context;)Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    goto :goto_0

    :cond_6
    const/16 v0, 0xc

    if-ne p1, v0, :cond_7

    iget-object v0, p0, Lcom/android/settings/development/DevelopmentSettings;->bgo:Landroid/preference/CheckBoxPreference;

    invoke-static {}, Lcom/android/a/a;->caD()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    goto :goto_0

    :cond_7
    invoke-super {p0, p1, p2, p3}, Lcom/android/settings/MiuiRestrictedSettingsFragment;->onActivityResult(IILandroid/content/Intent;)V

    goto :goto_0
.end method

.method public onAttach(Landroid/content/Context;)V
    .locals 2

    invoke-super {p0, p1}, Lcom/android/settings/MiuiRestrictedSettingsFragment;->onAttach(Landroid/content/Context;)V

    new-instance v0, Lcom/android/settings/development/b;

    invoke-virtual {p0}, Lcom/android/settings/development/DevelopmentSettings;->getLifecycle()Lcom/android/settings/core/lifecycle/c;

    move-result-object v1

    invoke-direct {v0, p1, v1}, Lcom/android/settings/development/b;-><init>(Landroid/content/Context;Lcom/android/settings/core/lifecycle/c;)V

    iput-object v0, p0, Lcom/android/settings/development/DevelopmentSettings;->bhG:Lcom/android/settings/development/b;

    return-void
.end method

.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 4

    const/4 v3, 0x0

    const/4 v1, -0x1

    const/4 v2, 0x1

    iget-object v0, p0, Lcom/android/settings/development/DevelopmentSettings;->bgn:Landroid/app/Dialog;

    if-ne p1, v0, :cond_2

    if-ne p2, v1, :cond_1

    iput-boolean v2, p0, Lcom/android/settings/development/DevelopmentSettings;->bgZ:Z

    invoke-virtual {p0}, Lcom/android/settings/development/DevelopmentSettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v1, "adb_enabled"

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$Global;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    iget-object v0, p0, Lcom/android/settings/development/DevelopmentSettings;->bib:Lcom/android/settings/development/c;

    invoke-virtual {v0}, Lcom/android/settings/development/c;->aUu()V

    invoke-direct {p0}, Lcom/android/settings/development/DevelopmentSettings;->aVd()V

    iget-object v0, p0, Lcom/android/settings/development/DevelopmentSettings;->bhd:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, v2}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/settings/development/DevelopmentSettings;->bhd:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, v3}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/android/settings/development/DevelopmentSettings;->bgq:Landroid/app/Dialog;

    if-ne p1, v0, :cond_3

    if-ne p2, v1, :cond_0

    :try_start_0
    const-string/jumbo v0, "usb"

    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Landroid/hardware/usb/IUsbManager$Stub;->asInterface(Landroid/os/IBinder;)Landroid/hardware/usb/IUsbManager;

    move-result-object v0

    invoke-interface {v0}, Landroid/hardware/usb/IUsbManager;->clearUsbDebuggingKeys()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string/jumbo v1, "DevelopmentSettings"

    const-string/jumbo v2, "Unable to clear adb keys"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/android/settings/development/DevelopmentSettings;->bhe:Landroid/app/Dialog;

    if-ne p1, v0, :cond_5

    if-ne p2, v1, :cond_4

    iput-boolean v2, p0, Lcom/android/settings/development/DevelopmentSettings;->bgZ:Z

    iget-object v0, p0, Lcom/android/settings/development/DevelopmentSettings;->bhG:Lcom/android/settings/development/b;

    invoke-virtual {v0}, Lcom/android/settings/development/b;->aUq()V

    invoke-direct {p0, v2}, Lcom/android/settings/development/DevelopmentSettings;->aUS(Z)V

    goto :goto_0

    :cond_4
    iget-object v0, p0, Lcom/android/settings/development/DevelopmentSettings;->bhh:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, v3}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    goto :goto_0

    :cond_5
    iget-object v0, p0, Lcom/android/settings/development/DevelopmentSettings;->bhv:Landroid/app/Dialog;

    if-ne p1, v0, :cond_6

    if-eq p2, v1, :cond_0

    invoke-direct {p0}, Lcom/android/settings/development/DevelopmentSettings;->aVs()V

    goto :goto_0

    :cond_6
    iget-object v0, p0, Lcom/android/settings/development/DevelopmentSettings;->bgX:Landroid/app/Dialog;

    if-ne p1, v0, :cond_0

    if-ne p2, v1, :cond_7

    iput-boolean v2, p0, Lcom/android/settings/development/DevelopmentSettings;->bgZ:Z

    invoke-direct {p0}, Lcom/android/settings/development/DevelopmentSettings;->aWh()V

    goto :goto_0

    :cond_7
    iget-object v0, p0, Lcom/android/settings/development/DevelopmentSettings;->bgW:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, v3}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 7

    const/4 v6, 0x1

    const/4 v5, 0x0

    const/4 v4, 0x0

    invoke-super {p0, p1}, Lcom/android/settings/MiuiRestrictedSettingsFragment;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/android/settings/development/DevelopmentSettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const-string/jumbo v1, "development"

    invoke-virtual {v0, v1, v5}, Landroid/app/Activity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string/jumbo v1, "show"

    invoke-interface {v0, v1, v6}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    const-string/jumbo v0, "window"

    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Landroid/view/IWindowManager$Stub;->asInterface(Landroid/os/IBinder;)Landroid/view/IWindowManager;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/development/DevelopmentSettings;->bil:Landroid/view/IWindowManager;

    const-string/jumbo v0, "backup"

    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Landroid/app/backup/IBackupManager$Stub;->asInterface(Landroid/os/IBinder;)Landroid/app/backup/IBackupManager;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/development/DevelopmentSettings;->bgu:Landroid/app/backup/IBackupManager;

    invoke-static {}, Landroid/webkit/WebViewFactory;->getUpdateService()Landroid/webkit/IWebViewUpdateService;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/development/DevelopmentSettings;->bie:Landroid/webkit/IWebViewUpdateService;

    invoke-virtual {p0}, Lcom/android/settings/development/DevelopmentSettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const-string/jumbo v1, "persistent_data_block"

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/service/persistentdata/PersistentDataBlockManager;

    iput-object v0, p0, Lcom/android/settings/development/DevelopmentSettings;->bhA:Landroid/service/persistentdata/PersistentDataBlockManager;

    const-string/jumbo v0, "phone"

    invoke-virtual {p0, v0}, Lcom/android/settings/development/DevelopmentSettings;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    iput-object v0, p0, Lcom/android/settings/development/DevelopmentSettings;->bhR:Landroid/telephony/TelephonyManager;

    const-string/jumbo v0, "user"

    invoke-virtual {p0, v0}, Lcom/android/settings/development/DevelopmentSettings;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/UserManager;

    iput-object v0, p0, Lcom/android/settings/development/DevelopmentSettings;->bhX:Landroid/os/UserManager;

    const-string/jumbo v0, "wifi"

    invoke-virtual {p0, v0}, Lcom/android/settings/development/DevelopmentSettings;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/WifiManager;

    iput-object v0, p0, Lcom/android/settings/development/DevelopmentSettings;->bii:Landroid/net/wifi/WifiManager;

    new-instance v0, Lcom/android/settings/development/g;

    invoke-virtual {p0}, Lcom/android/settings/development/DevelopmentSettings;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/android/settings/development/g;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/settings/development/DevelopmentSettings;->bhS:Lcom/android/settings/development/g;

    new-instance v0, Lcom/android/settings/webview/a;

    invoke-virtual {p0}, Lcom/android/settings/development/DevelopmentSettings;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/android/settings/webview/a;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/settings/development/DevelopmentSettings;->bid:Lcom/android/settings/webview/a;

    new-instance v0, Lcom/android/settings/development/c;

    invoke-virtual {p0}, Lcom/android/settings/development/DevelopmentSettings;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/android/settings/development/c;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/settings/development/DevelopmentSettings;->bib:Lcom/android/settings/development/c;

    invoke-virtual {p0, v6}, Lcom/android/settings/development/DevelopmentSettings;->bXE(Z)V

    iget-object v0, p0, Lcom/android/settings/development/DevelopmentSettings;->bhX:Landroid/os/UserManager;

    const-string/jumbo v1, "no_debugging_features"

    invoke-virtual {v0, v1}, Landroid/os/UserManager;->hasUserRestriction(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    iput-boolean v6, p0, Lcom/android/settings/development/DevelopmentSettings;->bhY:Z

    const v0, 0x7f150053

    invoke-virtual {p0, v0}, Lcom/android/settings/development/DevelopmentSettings;->addPreferencesFromResource(I)V

    return-void

    :cond_0
    const v0, 0x7f15003c

    invoke-virtual {p0, v0}, Lcom/android/settings/development/DevelopmentSettings;->addPreferencesFromResource(I)V

    const-string/jumbo v0, "debug_debugging_category"

    invoke-virtual {p0, v0}, Lcom/android/settings/development/DevelopmentSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceGroup;

    const-string/jumbo v1, "development_enable"

    invoke-virtual {p0, v1}, Lcom/android/settings/development/DevelopmentSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    check-cast v1, Landroid/preference/CheckBoxPreference;

    iput-object v1, p0, Lcom/android/settings/development/DevelopmentSettings;->bhh:Landroid/preference/CheckBoxPreference;

    const-string/jumbo v1, "enable_adb"

    invoke-direct {p0, v1}, Lcom/android/settings/development/DevelopmentSettings;->aUF(Ljava/lang/String;)Landroid/preference/CheckBoxPreference;

    move-result-object v1

    iput-object v1, p0, Lcom/android/settings/development/DevelopmentSettings;->bhd:Landroid/preference/CheckBoxPreference;

    const-string/jumbo v1, "clear_adb_keys"

    invoke-virtual {p0, v1}, Lcom/android/settings/development/DevelopmentSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    iput-object v1, p0, Lcom/android/settings/development/DevelopmentSettings;->bgM:Landroid/preference/Preference;

    const-string/jumbo v1, "ro.adb.secure"

    invoke-static {v1, v5}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    if-nez v1, :cond_1

    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/android/settings/development/DevelopmentSettings;->bgM:Landroid/preference/Preference;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceGroup;->removePreference(Landroid/preference/Preference;)Z

    :cond_1
    iget-object v1, p0, Lcom/android/settings/development/DevelopmentSettings;->bgr:Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/android/settings/development/DevelopmentSettings;->bgM:Landroid/preference/Preference;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const-string/jumbo v1, "delete_second_space"

    invoke-virtual {p0, v1}, Lcom/android/settings/development/DevelopmentSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    iput-object v1, p0, Lcom/android/settings/development/DevelopmentSettings;->bgV:Landroid/preference/Preference;

    const-string/jumbo v1, "enable_terminal"

    invoke-direct {p0, v1}, Lcom/android/settings/development/DevelopmentSettings;->aUF(Ljava/lang/String;)Landroid/preference/CheckBoxPreference;

    move-result-object v1

    iput-object v1, p0, Lcom/android/settings/development/DevelopmentSettings;->bhg:Landroid/preference/CheckBoxPreference;

    invoke-virtual {p0}, Lcom/android/settings/development/DevelopmentSettings;->getActivity()Landroid/app/Activity;

    move-result-object v1

    const-string/jumbo v2, "com.android.terminal"

    invoke-static {v1, v2}, Lcom/android/settings/development/DevelopmentSettings;->aUK(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/android/settings/development/DevelopmentSettings;->bhg:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceGroup;->removePreference(Landroid/preference/Preference;)Z

    iput-object v4, p0, Lcom/android/settings/development/DevelopmentSettings;->bhg:Landroid/preference/CheckBoxPreference;

    :cond_2
    iget-object v1, p0, Lcom/android/settings/development/DevelopmentSettings;->bhS:Lcom/android/settings/development/g;

    invoke-virtual {p0}, Lcom/android/settings/development/DevelopmentSettings;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/android/settings/development/g;->i(Landroid/preference/PreferenceScreen;)V

    iget-object v1, p0, Lcom/android/settings/development/DevelopmentSettings;->bid:Lcom/android/settings/webview/a;

    invoke-virtual {p0}, Lcom/android/settings/development/DevelopmentSettings;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/android/settings/webview/a;->i(Landroid/preference/PreferenceScreen;)V

    const-string/jumbo v1, "keep_screen_on"

    invoke-direct {p0, v1}, Lcom/android/settings/development/DevelopmentSettings;->aUF(Ljava/lang/String;)Landroid/preference/CheckBoxPreference;

    move-result-object v1

    iput-object v1, p0, Lcom/android/settings/development/DevelopmentSettings;->bhq:Landroid/preference/CheckBoxPreference;

    const-string/jumbo v1, "bt_hci_snoop_log"

    invoke-direct {p0, v1}, Lcom/android/settings/development/DevelopmentSettings;->aUF(Ljava/lang/String;)Landroid/preference/CheckBoxPreference;

    move-result-object v1

    iput-object v1, p0, Lcom/android/settings/development/DevelopmentSettings;->bgJ:Landroid/preference/CheckBoxPreference;

    const-string/jumbo v1, "oem_unlock_enable"

    invoke-direct {p0, v1}, Lcom/android/settings/development/DevelopmentSettings;->aUG(Ljava/lang/String;)Landroid/preference/SwitchPreference;

    move-result-object v1

    check-cast v1, Lcom/android/settingslib/MiuiRestrictedSwitchPreference;

    iput-object v1, p0, Lcom/android/settings/development/DevelopmentSettings;->bhf:Lcom/android/settingslib/MiuiRestrictedSwitchPreference;

    invoke-static {}, Lcom/android/settings/development/DevelopmentSettings;->aUU()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-static {}, Landroid/os/Process;->myUserHandle()Landroid/os/UserHandle;

    move-result-object v1

    sget-object v2, Landroid/os/UserHandle;->OWNER:Landroid/os/UserHandle;

    invoke-virtual {v1, v2}, Landroid/os/UserHandle;->equals(Ljava/lang/Object;)Z

    move-result v1

    xor-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_4

    :cond_3
    iget-object v1, p0, Lcom/android/settings/development/DevelopmentSettings;->bhf:Lcom/android/settingslib/MiuiRestrictedSwitchPreference;

    invoke-direct {p0, v1}, Lcom/android/settings/development/DevelopmentSettings;->aUN(Landroid/preference/Preference;)V

    iput-object v4, p0, Lcom/android/settings/development/DevelopmentSettings;->bhf:Lcom/android/settingslib/MiuiRestrictedSwitchPreference;

    :cond_4
    const-string/jumbo v1, "demo_mode"

    invoke-direct {p0, v1}, Lcom/android/settings/development/DevelopmentSettings;->aUF(Ljava/lang/String;)Landroid/preference/CheckBoxPreference;

    move-result-object v1

    iput-object v1, p0, Lcom/android/settings/development/DevelopmentSettings;->bgW:Landroid/preference/CheckBoxPreference;

    const-string/jumbo v1, "bootloader_status"

    invoke-virtual {p0, v1}, Lcom/android/settings/development/DevelopmentSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    check-cast v1, Lmiui/preference/ValuePreference;

    iput-object v1, p0, Lcom/android/settings/development/DevelopmentSettings;->bgY:Lmiui/preference/ValuePreference;

    const-string/jumbo v1, "ro.secureboot.devicelock"

    invoke-static {v1, v5}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    move-result v1

    if-ne v1, v6, :cond_f

    iget-object v1, p0, Lcom/android/settings/development/DevelopmentSettings;->bgY:Lmiui/preference/ValuePreference;

    invoke-virtual {v1, v6}, Lmiui/preference/ValuePreference;->setShowRightArrow(Z)V

    const-string/jumbo v1, "ro.secureboot.lockstate"

    invoke-static {v1, v4}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "locked"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_e

    iget-object v1, p0, Lcom/android/settings/development/DevelopmentSettings;->bgY:Lmiui/preference/ValuePreference;

    invoke-virtual {v1, v4}, Lmiui/preference/ValuePreference;->setValue(Ljava/lang/String;)V

    :goto_0
    iget-object v1, p0, Lcom/android/settings/development/DevelopmentSettings;->bgr:Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/android/settings/development/DevelopmentSettings;->bgY:Lmiui/preference/ValuePreference;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :goto_1
    const-string/jumbo v1, "debug_view_attributes"

    invoke-direct {p0, v1}, Lcom/android/settings/development/DevelopmentSettings;->aUF(Ljava/lang/String;)Landroid/preference/CheckBoxPreference;

    move-result-object v1

    iput-object v1, p0, Lcom/android/settings/development/DevelopmentSettings;->bgU:Landroid/preference/CheckBoxPreference;

    const-string/jumbo v1, "force_allow_on_external"

    invoke-direct {p0, v1}, Lcom/android/settings/development/DevelopmentSettings;->aUF(Ljava/lang/String;)Landroid/preference/CheckBoxPreference;

    move-result-object v1

    iput-object v1, p0, Lcom/android/settings/development/DevelopmentSettings;->bhj:Landroid/preference/CheckBoxPreference;

    const-string/jumbo v1, "local_backup_password"

    invoke-virtual {p0, v1}, Lcom/android/settings/development/DevelopmentSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    iput-object v1, p0, Lcom/android/settings/development/DevelopmentSettings;->bhC:Landroid/preference/Preference;

    iget-object v1, p0, Lcom/android/settings/development/DevelopmentSettings;->bgr:Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/android/settings/development/DevelopmentSettings;->bhC:Landroid/preference/Preference;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const-string/jumbo v1, "bt_trace_log"

    invoke-direct {p0, v1}, Lcom/android/settings/development/DevelopmentSettings;->aUF(Ljava/lang/String;)Landroid/preference/CheckBoxPreference;

    move-result-object v1

    iput-object v1, p0, Lcom/android/settings/development/DevelopmentSettings;->bgK:Landroid/preference/CheckBoxPreference;

    iget-boolean v1, p0, Lcom/android/settings/development/DevelopmentSettings;->bgI:Z

    if-nez v1, :cond_5

    invoke-virtual {p0}, Lcom/android/settings/development/DevelopmentSettings;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v1

    iget-object v2, p0, Lcom/android/settings/development/DevelopmentSettings;->bgJ:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v1, v2}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    invoke-virtual {p0}, Lcom/android/settings/development/DevelopmentSettings;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v1

    iget-object v2, p0, Lcom/android/settings/development/DevelopmentSettings;->bgK:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v1, v2}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    :cond_5
    iget-object v1, p0, Lcom/android/settings/development/DevelopmentSettings;->bhX:Landroid/os/UserManager;

    invoke-virtual {v1}, Landroid/os/UserManager;->isAdminUser()Z

    move-result v1

    if-nez v1, :cond_6

    iget-object v1, p0, Lcom/android/settings/development/DevelopmentSettings;->bgM:Landroid/preference/Preference;

    invoke-direct {p0, v1}, Lcom/android/settings/development/DevelopmentSettings;->aUC(Landroid/preference/Preference;)V

    iget-object v1, p0, Lcom/android/settings/development/DevelopmentSettings;->bhg:Landroid/preference/CheckBoxPreference;

    invoke-direct {p0, v1}, Lcom/android/settings/development/DevelopmentSettings;->aUC(Landroid/preference/Preference;)V

    iget-object v1, p0, Lcom/android/settings/development/DevelopmentSettings;->bhC:Landroid/preference/Preference;

    invoke-direct {p0, v1}, Lcom/android/settings/development/DevelopmentSettings;->aUC(Landroid/preference/Preference;)V

    :cond_6
    const-string/jumbo v1, "debug_app"

    invoke-virtual {p0, v1}, Lcom/android/settings/development/DevelopmentSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    iput-object v1, p0, Lcom/android/settings/development/DevelopmentSettings;->bgQ:Landroid/preference/Preference;

    iget-object v1, p0, Lcom/android/settings/development/DevelopmentSettings;->bgr:Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/android/settings/development/DevelopmentSettings;->bgQ:Landroid/preference/Preference;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const-string/jumbo v1, "wait_for_debugger"

    invoke-direct {p0, v1}, Lcom/android/settings/development/DevelopmentSettings;->aUF(Ljava/lang/String;)Landroid/preference/CheckBoxPreference;

    move-result-object v1

    iput-object v1, p0, Lcom/android/settings/development/DevelopmentSettings;->bic:Landroid/preference/CheckBoxPreference;

    const-string/jumbo v1, "mock_location_app"

    invoke-virtual {p0, v1}, Lcom/android/settings/development/DevelopmentSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    iput-object v1, p0, Lcom/android/settings/development/DevelopmentSettings;->bhz:Landroid/preference/Preference;

    iget-object v1, p0, Lcom/android/settings/development/DevelopmentSettings;->bgr:Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/android/settings/development/DevelopmentSettings;->bhz:Landroid/preference/Preference;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v1, p0, Lcom/android/settings/development/DevelopmentSettings;->bib:Lcom/android/settings/development/c;

    invoke-virtual {p0}, Lcom/android/settings/development/DevelopmentSettings;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/android/settings/development/c;->i(Landroid/preference/PreferenceScreen;)V

    const-string/jumbo v1, "strict_mode"

    invoke-direct {p0, v1}, Lcom/android/settings/development/DevelopmentSettings;->aUF(Ljava/lang/String;)Landroid/preference/CheckBoxPreference;

    move-result-object v1

    iput-object v1, p0, Lcom/android/settings/development/DevelopmentSettings;->bhQ:Landroid/preference/CheckBoxPreference;

    const-string/jumbo v1, "pointer_location"

    invoke-direct {p0, v1}, Lcom/android/settings/development/DevelopmentSettings;->aUF(Ljava/lang/String;)Landroid/preference/CheckBoxPreference;

    move-result-object v1

    iput-object v1, p0, Lcom/android/settings/development/DevelopmentSettings;->bhD:Landroid/preference/CheckBoxPreference;

    const-string/jumbo v1, "show_touches"

    invoke-direct {p0, v1}, Lcom/android/settings/development/DevelopmentSettings;->aUF(Ljava/lang/String;)Landroid/preference/CheckBoxPreference;

    move-result-object v1

    iput-object v1, p0, Lcom/android/settings/development/DevelopmentSettings;->bhO:Landroid/preference/CheckBoxPreference;

    const-string/jumbo v1, "show_screen_updates"

    invoke-direct {p0, v1}, Lcom/android/settings/development/DevelopmentSettings;->aUF(Ljava/lang/String;)Landroid/preference/CheckBoxPreference;

    move-result-object v1

    iput-object v1, p0, Lcom/android/settings/development/DevelopmentSettings;->bhN:Landroid/preference/CheckBoxPreference;

    const-string/jumbo v1, "disable_overlays"

    invoke-direct {p0, v1}, Lcom/android/settings/development/DevelopmentSettings;->aUF(Ljava/lang/String;)Landroid/preference/CheckBoxPreference;

    move-result-object v1

    iput-object v1, p0, Lcom/android/settings/development/DevelopmentSettings;->bha:Landroid/preference/CheckBoxPreference;

    const-string/jumbo v1, "force_hw_ui"

    invoke-direct {p0, v1}, Lcom/android/settings/development/DevelopmentSettings;->aUF(Ljava/lang/String;)Landroid/preference/CheckBoxPreference;

    move-result-object v1

    iput-object v1, p0, Lcom/android/settings/development/DevelopmentSettings;->bhk:Landroid/preference/CheckBoxPreference;

    const-string/jumbo v1, "force_msaa"

    invoke-direct {p0, v1}, Lcom/android/settings/development/DevelopmentSettings;->aUF(Ljava/lang/String;)Landroid/preference/CheckBoxPreference;

    move-result-object v1

    iput-object v1, p0, Lcom/android/settings/development/DevelopmentSettings;->bhl:Landroid/preference/CheckBoxPreference;

    const-string/jumbo v1, "track_frame_time"

    invoke-direct {p0, v1}, Lcom/android/settings/development/DevelopmentSettings;->aUw(Ljava/lang/String;)Landroid/preference/ListPreference;

    move-result-object v1

    iput-object v1, p0, Lcom/android/settings/development/DevelopmentSettings;->bhU:Landroid/preference/ListPreference;

    const-string/jumbo v1, "show_non_rect_clip"

    invoke-direct {p0, v1}, Lcom/android/settings/development/DevelopmentSettings;->aUw(Ljava/lang/String;)Landroid/preference/ListPreference;

    move-result-object v1

    iput-object v1, p0, Lcom/android/settings/development/DevelopmentSettings;->bhL:Landroid/preference/ListPreference;

    const-string/jumbo v1, "show_hw_screen_udpates"

    invoke-direct {p0, v1}, Lcom/android/settings/development/DevelopmentSettings;->aUF(Ljava/lang/String;)Landroid/preference/CheckBoxPreference;

    move-result-object v1

    iput-object v1, p0, Lcom/android/settings/development/DevelopmentSettings;->bhK:Landroid/preference/CheckBoxPreference;

    const-string/jumbo v1, "show_hw_layers_udpates"

    invoke-direct {p0, v1}, Lcom/android/settings/development/DevelopmentSettings;->aUF(Ljava/lang/String;)Landroid/preference/CheckBoxPreference;

    move-result-object v1

    iput-object v1, p0, Lcom/android/settings/development/DevelopmentSettings;->bhJ:Landroid/preference/CheckBoxPreference;

    const-string/jumbo v1, "debug_layout"

    invoke-direct {p0, v1}, Lcom/android/settings/development/DevelopmentSettings;->aUF(Ljava/lang/String;)Landroid/preference/CheckBoxPreference;

    move-result-object v1

    iput-object v1, p0, Lcom/android/settings/development/DevelopmentSettings;->bgT:Landroid/preference/CheckBoxPreference;

    const-string/jumbo v1, "force_rtl_layout_all_locales"

    invoke-direct {p0, v1}, Lcom/android/settings/development/DevelopmentSettings;->aUF(Ljava/lang/String;)Landroid/preference/CheckBoxPreference;

    move-result-object v1

    iput-object v1, p0, Lcom/android/settings/development/DevelopmentSettings;->bhn:Landroid/preference/CheckBoxPreference;

    const-string/jumbo v1, "debug_hw_overdraw"

    invoke-direct {p0, v1}, Lcom/android/settings/development/DevelopmentSettings;->aUw(Ljava/lang/String;)Landroid/preference/ListPreference;

    move-result-object v1

    iput-object v1, p0, Lcom/android/settings/development/DevelopmentSettings;->bgR:Landroid/preference/ListPreference;

    const-string/jumbo v1, "debug_hw_renderer"

    invoke-direct {p0, v1}, Lcom/android/settings/development/DevelopmentSettings;->aUw(Ljava/lang/String;)Landroid/preference/ListPreference;

    move-result-object v1

    iput-object v1, p0, Lcom/android/settings/development/DevelopmentSettings;->bgS:Landroid/preference/ListPreference;

    const-string/jumbo v1, "wifi_display_certification"

    invoke-direct {p0, v1}, Lcom/android/settings/development/DevelopmentSettings;->aUF(Ljava/lang/String;)Landroid/preference/CheckBoxPreference;

    move-result-object v1

    iput-object v1, p0, Lcom/android/settings/development/DevelopmentSettings;->bih:Landroid/preference/CheckBoxPreference;

    const-string/jumbo v1, "wifi_verbose_logging"

    invoke-direct {p0, v1}, Lcom/android/settings/development/DevelopmentSettings;->aUF(Ljava/lang/String;)Landroid/preference/CheckBoxPreference;

    move-result-object v1

    iput-object v1, p0, Lcom/android/settings/development/DevelopmentSettings;->bij:Landroid/preference/CheckBoxPreference;

    const-string/jumbo v1, "wifi_aggressive_handover"

    invoke-direct {p0, v1}, Lcom/android/settings/development/DevelopmentSettings;->aUF(Ljava/lang/String;)Landroid/preference/CheckBoxPreference;

    move-result-object v1

    iput-object v1, p0, Lcom/android/settings/development/DevelopmentSettings;->bif:Landroid/preference/CheckBoxPreference;

    const-string/jumbo v1, "wifi_allow_scan_with_traffic"

    invoke-direct {p0, v1}, Lcom/android/settings/development/DevelopmentSettings;->aUF(Ljava/lang/String;)Landroid/preference/CheckBoxPreference;

    move-result-object v1

    iput-object v1, p0, Lcom/android/settings/development/DevelopmentSettings;->big:Landroid/preference/CheckBoxPreference;

    const-string/jumbo v1, "mobile_data_always_on"

    invoke-direct {p0, v1}, Lcom/android/settings/development/DevelopmentSettings;->aUF(Ljava/lang/String;)Landroid/preference/CheckBoxPreference;

    move-result-object v1

    iput-object v1, p0, Lcom/android/settings/development/DevelopmentSettings;->bhx:Landroid/preference/CheckBoxPreference;

    const-string/jumbo v1, "select_logd_system_level"

    invoke-direct {p0, v1}, Lcom/android/settings/development/DevelopmentSettings;->aUw(Ljava/lang/String;)Landroid/preference/ListPreference;

    move-result-object v1

    iput-object v1, p0, Lcom/android/settings/development/DevelopmentSettings;->bhs:Landroid/preference/ListPreference;

    const-string/jumbo v1, "select_logd_size"

    invoke-direct {p0, v1}, Lcom/android/settings/development/DevelopmentSettings;->aUw(Ljava/lang/String;)Landroid/preference/ListPreference;

    move-result-object v1

    iput-object v1, p0, Lcom/android/settings/development/DevelopmentSettings;->bht:Landroid/preference/ListPreference;

    const-string/jumbo v1, "1"

    const-string/jumbo v2, "ro.debuggable"

    const-string/jumbo v3, "0"

    invoke-static {v2, v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_10

    const-string/jumbo v1, "select_logpersist"

    invoke-direct {p0, v1}, Lcom/android/settings/development/DevelopmentSettings;->aUw(Ljava/lang/String;)Landroid/preference/ListPreference;

    move-result-object v1

    iput-object v1, p0, Lcom/android/settings/development/DevelopmentSettings;->bhu:Landroid/preference/ListPreference;

    :goto_2
    const-string/jumbo v1, "select_usb_configuration"

    invoke-direct {p0, v1}, Lcom/android/settings/development/DevelopmentSettings;->aUw(Ljava/lang/String;)Landroid/preference/ListPreference;

    move-result-object v1

    iput-object v1, p0, Lcom/android/settings/development/DevelopmentSettings;->bhZ:Landroid/preference/ListPreference;

    const-string/jumbo v1, "bluetooth_disable_absolute_volume"

    invoke-direct {p0, v1}, Lcom/android/settings/development/DevelopmentSettings;->aUF(Ljava/lang/String;)Landroid/preference/CheckBoxPreference;

    move-result-object v1

    iput-object v1, p0, Lcom/android/settings/development/DevelopmentSettings;->bgz:Landroid/preference/CheckBoxPreference;

    const-string/jumbo v1, "bluetooth_enable_inband_ringing"

    invoke-direct {p0, v1}, Lcom/android/settings/development/DevelopmentSettings;->aUF(Ljava/lang/String;)Landroid/preference/CheckBoxPreference;

    move-result-object v1

    iput-object v1, p0, Lcom/android/settings/development/DevelopmentSettings;->bgA:Landroid/preference/CheckBoxPreference;

    const-string/jumbo v1, "debug_networking_category"

    invoke-virtual {p0, v1}, Lcom/android/settings/development/DevelopmentSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    check-cast v1, Landroid/preference/PreferenceCategory;

    invoke-virtual {p0}, Lcom/android/settings/development/DevelopmentSettings;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Landroid/bluetooth/BluetoothHeadset;->isInbandRingingSupported(Landroid/content/Context;)Z

    move-result v2

    if-nez v2, :cond_7

    iget-object v2, p0, Lcom/android/settings/development/DevelopmentSettings;->bgA:Landroid/preference/CheckBoxPreference;

    invoke-direct {p0, v2}, Lcom/android/settings/development/DevelopmentSettings;->aUN(Landroid/preference/Preference;)V

    iget-object v2, p0, Lcom/android/settings/development/DevelopmentSettings;->bgA:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v1, v2}, Landroid/preference/PreferenceCategory;->removePreference(Landroid/preference/Preference;)Z

    iput-object v4, p0, Lcom/android/settings/development/DevelopmentSettings;->bgA:Landroid/preference/CheckBoxPreference;

    :cond_7
    const-string/jumbo v2, "bluetooth_select_avrcp_version"

    invoke-direct {p0, v2}, Lcom/android/settings/development/DevelopmentSettings;->aUw(Ljava/lang/String;)Landroid/preference/ListPreference;

    move-result-object v2

    iput-object v2, p0, Lcom/android/settings/development/DevelopmentSettings;->bgG:Landroid/preference/ListPreference;

    const-string/jumbo v2, "bluetooth_select_a2dp_codec"

    invoke-direct {p0, v2}, Lcom/android/settings/development/DevelopmentSettings;->aUw(Ljava/lang/String;)Landroid/preference/ListPreference;

    move-result-object v2

    iput-object v2, p0, Lcom/android/settings/development/DevelopmentSettings;->bgD:Landroid/preference/ListPreference;

    const-string/jumbo v2, "bluetooth_select_a2dp_sample_rate"

    invoke-direct {p0, v2}, Lcom/android/settings/development/DevelopmentSettings;->aUw(Ljava/lang/String;)Landroid/preference/ListPreference;

    move-result-object v2

    iput-object v2, p0, Lcom/android/settings/development/DevelopmentSettings;->bgF:Landroid/preference/ListPreference;

    sget-boolean v2, Lmiui/os/Build;->IS_TABLET:Z

    if-eqz v2, :cond_8

    iget-object v2, p0, Lcom/android/settings/development/DevelopmentSettings;->bhx:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v1, v2}, Landroid/preference/PreferenceCategory;->removePreference(Landroid/preference/Preference;)Z

    iput-object v4, p0, Lcom/android/settings/development/DevelopmentSettings;->bhx:Landroid/preference/CheckBoxPreference;

    iget-object v2, p0, Lcom/android/settings/development/DevelopmentSettings;->bif:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v1, v2}, Landroid/preference/PreferenceCategory;->removePreference(Landroid/preference/Preference;)Z

    iput-object v4, p0, Lcom/android/settings/development/DevelopmentSettings;->bif:Landroid/preference/CheckBoxPreference;

    :cond_8
    const-string/jumbo v1, "bluetooth_select_a2dp_bits_per_sample"

    invoke-direct {p0, v1}, Lcom/android/settings/development/DevelopmentSettings;->aUw(Ljava/lang/String;)Landroid/preference/ListPreference;

    move-result-object v1

    iput-object v1, p0, Lcom/android/settings/development/DevelopmentSettings;->bgB:Landroid/preference/ListPreference;

    const-string/jumbo v1, "bluetooth_select_a2dp_channel_mode"

    invoke-direct {p0, v1}, Lcom/android/settings/development/DevelopmentSettings;->aUw(Ljava/lang/String;)Landroid/preference/ListPreference;

    move-result-object v1

    iput-object v1, p0, Lcom/android/settings/development/DevelopmentSettings;->bgC:Landroid/preference/ListPreference;

    const-string/jumbo v1, "bluetooth_select_a2dp_ldac_playback_quality"

    invoke-direct {p0, v1}, Lcom/android/settings/development/DevelopmentSettings;->aUw(Ljava/lang/String;)Landroid/preference/ListPreference;

    move-result-object v1

    iput-object v1, p0, Lcom/android/settings/development/DevelopmentSettings;->bgE:Landroid/preference/ListPreference;

    invoke-direct {p0}, Lcom/android/settings/development/DevelopmentSettings;->aUI()V

    const-string/jumbo v1, "window_animation_scale"

    invoke-direct {p0, v1}, Lcom/android/settings/development/DevelopmentSettings;->aUw(Ljava/lang/String;)Landroid/preference/ListPreference;

    move-result-object v1

    iput-object v1, p0, Lcom/android/settings/development/DevelopmentSettings;->bik:Landroid/preference/ListPreference;

    const-string/jumbo v1, "transition_animation_scale"

    invoke-direct {p0, v1}, Lcom/android/settings/development/DevelopmentSettings;->aUw(Ljava/lang/String;)Landroid/preference/ListPreference;

    move-result-object v1

    iput-object v1, p0, Lcom/android/settings/development/DevelopmentSettings;->bhV:Landroid/preference/ListPreference;

    const-string/jumbo v1, "animator_duration_scale"

    invoke-direct {p0, v1}, Lcom/android/settings/development/DevelopmentSettings;->aUw(Ljava/lang/String;)Landroid/preference/ListPreference;

    move-result-object v1

    iput-object v1, p0, Lcom/android/settings/development/DevelopmentSettings;->bgs:Landroid/preference/ListPreference;

    const-string/jumbo v1, "overlay_display_devices"

    invoke-direct {p0, v1}, Lcom/android/settings/development/DevelopmentSettings;->aUw(Ljava/lang/String;)Landroid/preference/ListPreference;

    move-result-object v1

    iput-object v1, p0, Lcom/android/settings/development/DevelopmentSettings;->bhB:Landroid/preference/ListPreference;

    const-string/jumbo v1, "simulate_color_space"

    invoke-direct {p0, v1}, Lcom/android/settings/development/DevelopmentSettings;->aUw(Ljava/lang/String;)Landroid/preference/ListPreference;

    move-result-object v1

    iput-object v1, p0, Lcom/android/settings/development/DevelopmentSettings;->bhP:Landroid/preference/ListPreference;

    const-string/jumbo v1, "usb_audio"

    invoke-direct {p0, v1}, Lcom/android/settings/development/DevelopmentSettings;->aUF(Ljava/lang/String;)Landroid/preference/CheckBoxPreference;

    move-result-object v1

    iput-object v1, p0, Lcom/android/settings/development/DevelopmentSettings;->bhW:Landroid/preference/CheckBoxPreference;

    const-string/jumbo v1, "force_resizable_activities"

    invoke-direct {p0, v1}, Lcom/android/settings/development/DevelopmentSettings;->aUF(Ljava/lang/String;)Landroid/preference/CheckBoxPreference;

    move-result-object v1

    iput-object v1, p0, Lcom/android/settings/development/DevelopmentSettings;->bhm:Landroid/preference/CheckBoxPreference;

    const-string/jumbo v1, "immediately_destroy_activities"

    invoke-virtual {p0, v1}, Lcom/android/settings/development/DevelopmentSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    check-cast v1, Landroid/preference/CheckBoxPreference;

    iput-object v1, p0, Lcom/android/settings/development/DevelopmentSettings;->bhp:Landroid/preference/CheckBoxPreference;

    iget-object v1, p0, Lcom/android/settings/development/DevelopmentSettings;->bgr:Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/android/settings/development/DevelopmentSettings;->bhp:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v1, p0, Lcom/android/settings/development/DevelopmentSettings;->bhE:Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/android/settings/development/DevelopmentSettings;->bhp:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const-string/jumbo v1, "app_process_limit"

    invoke-direct {p0, v1}, Lcom/android/settings/development/DevelopmentSettings;->aUw(Ljava/lang/String;)Landroid/preference/ListPreference;

    move-result-object v1

    iput-object v1, p0, Lcom/android/settings/development/DevelopmentSettings;->bgt:Landroid/preference/ListPreference;

    const-string/jumbo v1, "show_all_anrs"

    invoke-virtual {p0, v1}, Lcom/android/settings/development/DevelopmentSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    check-cast v1, Landroid/preference/CheckBoxPreference;

    iput-object v1, p0, Lcom/android/settings/development/DevelopmentSettings;->bhH:Landroid/preference/CheckBoxPreference;

    iget-object v1, p0, Lcom/android/settings/development/DevelopmentSettings;->bgr:Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/android/settings/development/DevelopmentSettings;->bhH:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v1, p0, Lcom/android/settings/development/DevelopmentSettings;->bhE:Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/android/settings/development/DevelopmentSettings;->bhH:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const-string/jumbo v1, "show_fc"

    invoke-direct {p0, v1}, Lcom/android/settings/development/DevelopmentSettings;->aUF(Ljava/lang/String;)Landroid/preference/CheckBoxPreference;

    move-result-object v1

    iput-object v1, p0, Lcom/android/settings/development/DevelopmentSettings;->bhI:Landroid/preference/CheckBoxPreference;

    const-string/jumbo v1, "bootloader"

    invoke-direct {p0, v1}, Lcom/android/settings/development/DevelopmentSettings;->aUF(Ljava/lang/String;)Landroid/preference/CheckBoxPreference;

    move-result-object v1

    iput-object v1, p0, Lcom/android/settings/development/DevelopmentSettings;->bgH:Landroid/preference/CheckBoxPreference;

    invoke-static {}, Lcom/android/settings/BootloaderApplyActivity;->bVy()Z

    move-result v1

    if-eqz v1, :cond_12

    if-eqz v0, :cond_9

    iget-object v1, p0, Lcom/android/settings/development/DevelopmentSettings;->bgH:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceGroup;->removePreference(Landroid/preference/Preference;)Z

    :cond_9
    :goto_3
    const-string/jumbo v0, "adb_install"

    invoke-direct {p0, v0}, Lcom/android/settings/development/DevelopmentSettings;->aUF(Ljava/lang/String;)Landroid/preference/CheckBoxPreference;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/development/DevelopmentSettings;->bgp:Landroid/preference/CheckBoxPreference;

    const-string/jumbo v0, "adb_input"

    invoke-direct {p0, v0}, Lcom/android/settings/development/DevelopmentSettings;->aUF(Ljava/lang/String;)Landroid/preference/CheckBoxPreference;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/development/DevelopmentSettings;->bgo:Landroid/preference/CheckBoxPreference;

    const-string/jumbo v0, "is_pad"

    invoke-static {v0, v5}, Lmiui/util/FeatureParser;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_13

    const-string/jumbo v0, "debug_debugging_category"

    invoke-virtual {p0, v0}, Lcom/android/settings/development/DevelopmentSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceGroup;

    iget-object v1, p0, Lcom/android/settings/development/DevelopmentSettings;->bgp:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceGroup;->removePreference(Landroid/preference/Preference;)Z

    iget-object v1, p0, Lcom/android/settings/development/DevelopmentSettings;->bgo:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceGroup;->removePreference(Landroid/preference/Preference;)Z

    :goto_4
    const-string/jumbo v0, "unlock_enter_system"

    invoke-direct {p0, v0}, Lcom/android/settings/development/DevelopmentSettings;->aUF(Ljava/lang/String;)Landroid/preference/CheckBoxPreference;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/development/DevelopmentSettings;->bhT:Landroid/preference/CheckBoxPreference;

    new-instance v0, Lcom/android/settings/cx;

    invoke-virtual {p0}, Lcom/android/settings/development/DevelopmentSettings;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/android/settings/cx;-><init>(Landroid/app/Activity;)V

    iput-object v0, p0, Lcom/android/settings/development/DevelopmentSettings;->bgL:Lcom/android/settings/cx;

    new-instance v0, Lcom/android/internal/widget/LockPatternUtils;

    invoke-virtual {p0}, Lcom/android/settings/development/DevelopmentSettings;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/android/internal/widget/LockPatternUtils;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/settings/development/DevelopmentSettings;->mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

    const-string/jumbo v0, "show_notification_channel_warnings"

    invoke-virtual {p0, v0}, Lcom/android/settings/development/DevelopmentSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    iput-object v0, p0, Lcom/android/settings/development/DevelopmentSettings;->bhM:Landroid/preference/CheckBoxPreference;

    iget-object v0, p0, Lcom/android/settings/development/DevelopmentSettings;->bgr:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/android/settings/development/DevelopmentSettings;->bhM:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/android/settings/development/DevelopmentSettings;->bhE:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/android/settings/development/DevelopmentSettings;->bhM:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const-string/jumbo v0, "hdcp_checking"

    invoke-virtual {p0, v0}, Lcom/android/settings/development/DevelopmentSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    if-eqz v0, :cond_a

    iget-object v1, p0, Lcom/android/settings/development/DevelopmentSettings;->bgr:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-direct {p0, v0}, Lcom/android/settings/development/DevelopmentSettings;->aUO(Landroid/preference/Preference;)Z

    :cond_a
    const-string/jumbo v0, "convert_to_file_encryption"

    invoke-virtual {p0, v0}, Lcom/android/settings/development/DevelopmentSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    :try_start_0
    const-string/jumbo v1, "mount"

    invoke-static {v1}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v1

    invoke-static {v1}, Landroid/os/storage/IStorageManager$Stub;->asInterface(Landroid/os/IBinder;)Landroid/os/storage/IStorageManager;

    move-result-object v1

    invoke-interface {v1}, Landroid/os/storage/IStorageManager;->isConvertibleToFBE()Z

    move-result v1

    if-nez v1, :cond_14

    const-string/jumbo v0, "convert_to_file_encryption"

    invoke-virtual {p0, v0}, Lcom/android/settings/development/DevelopmentSettings;->bWK(Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_b
    :goto_5
    const-string/jumbo v0, "color_mode"

    invoke-virtual {p0, v0}, Lcom/android/settings/development/DevelopmentSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Lcom/android/settings/development/ColorModePreference;

    iput-object v0, p0, Lcom/android/settings/development/DevelopmentSettings;->bgN:Lcom/android/settings/development/ColorModePreference;

    iget-object v0, p0, Lcom/android/settings/development/DevelopmentSettings;->bgN:Lcom/android/settings/development/ColorModePreference;

    invoke-virtual {v0}, Lcom/android/settings/development/ColorModePreference;->aUk()V

    iget-object v0, p0, Lcom/android/settings/development/DevelopmentSettings;->bgN:Lcom/android/settings/development/ColorModePreference;

    invoke-virtual {v0}, Lcom/android/settings/development/ColorModePreference;->aUh()I

    move-result v0

    const/4 v1, 0x2

    if-lt v0, v1, :cond_c

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x19

    if-gt v0, v1, :cond_d

    :cond_c
    const-string/jumbo v0, "color_mode"

    invoke-virtual {p0, v0}, Lcom/android/settings/development/DevelopmentSettings;->bWK(Ljava/lang/String;)V

    iput-object v4, p0, Lcom/android/settings/development/DevelopmentSettings;->bgN:Lcom/android/settings/development/ColorModePreference;

    :cond_d
    const-string/jumbo v0, "color_temperature"

    invoke-virtual {p0, v0}, Lcom/android/settings/development/DevelopmentSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    iput-object v0, p0, Lcom/android/settings/development/DevelopmentSettings;->bgO:Landroid/preference/CheckBoxPreference;

    invoke-virtual {p0}, Lcom/android/settings/development/DevelopmentSettings;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f050009

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    if-eqz v0, :cond_15

    iget-object v0, p0, Lcom/android/settings/development/DevelopmentSettings;->bgr:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/android/settings/development/DevelopmentSettings;->bgO:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/android/settings/development/DevelopmentSettings;->bhE:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/android/settings/development/DevelopmentSettings;->bgO:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :goto_6
    invoke-virtual {p0}, Lcom/android/settings/development/DevelopmentSettings;->addDashboardCategoryPreferences()V

    const-string/jumbo v0, "miui_experience_optimization"

    invoke-virtual {p0, v0}, Lcom/android/settings/development/DevelopmentSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    iput-object v0, p0, Lcom/android/settings/development/DevelopmentSettings;->bhi:Landroid/preference/CheckBoxPreference;

    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v0

    if-nez v0, :cond_16

    const-string/jumbo v0, "persist.sys.miui_optimization"

    sget-boolean v1, Lmiui/os/Build;->IS_CTS_BUILD:Z

    xor-int/lit8 v1, v1, 0x1

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iget-object v1, p0, Lcom/android/settings/development/DevelopmentSettings;->bhi:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v1, v0}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    iget-object v0, p0, Lcom/android/settings/development/DevelopmentSettings;->bhi:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, p0}, Landroid/preference/CheckBoxPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    iget-object v0, p0, Lcom/android/settings/development/DevelopmentSettings;->bgr:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/android/settings/development/DevelopmentSettings;->bhi:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :goto_7
    invoke-virtual {p0}, Lcom/android/settings/development/DevelopmentSettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v1, "ota_disable_automatic_update"

    invoke-static {v0, v1, v6}, Landroid/provider/Settings$Global;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    return-void

    :cond_e
    iget-object v1, p0, Lcom/android/settings/development/DevelopmentSettings;->bgY:Lmiui/preference/ValuePreference;

    const v2, 0x7f120381

    invoke-virtual {v1, v2}, Lmiui/preference/ValuePreference;->setValue(I)V

    goto/16 :goto_0

    :cond_f
    invoke-virtual {p0}, Lcom/android/settings/development/DevelopmentSettings;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v1

    iget-object v2, p0, Lcom/android/settings/development/DevelopmentSettings;->bgY:Lmiui/preference/ValuePreference;

    invoke-virtual {v1, v2}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    goto/16 :goto_1

    :cond_10
    const-string/jumbo v1, "select_logpersist"

    invoke-virtual {p0, v1}, Lcom/android/settings/development/DevelopmentSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    check-cast v1, Landroid/preference/ListPreference;

    iput-object v1, p0, Lcom/android/settings/development/DevelopmentSettings;->bhu:Landroid/preference/ListPreference;

    iget-object v1, p0, Lcom/android/settings/development/DevelopmentSettings;->bhu:Landroid/preference/ListPreference;

    if-eqz v1, :cond_11

    iget-object v1, p0, Lcom/android/settings/development/DevelopmentSettings;->bhu:Landroid/preference/ListPreference;

    invoke-virtual {v1, v5}, Landroid/preference/ListPreference;->setEnabled(Z)V

    if-eqz v0, :cond_11

    iget-object v1, p0, Lcom/android/settings/development/DevelopmentSettings;->bhu:Landroid/preference/ListPreference;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceGroup;->removePreference(Landroid/preference/Preference;)Z

    :cond_11
    iput-object v4, p0, Lcom/android/settings/development/DevelopmentSettings;->bhu:Landroid/preference/ListPreference;

    goto/16 :goto_2

    :cond_12
    iget-object v0, p0, Lcom/android/settings/development/DevelopmentSettings;->bgH:Landroid/preference/CheckBoxPreference;

    invoke-static {}, Lcom/android/settings/BootloaderApplyActivity;->isEnabled()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    goto/16 :goto_3

    :cond_13
    iget-object v0, p0, Lcom/android/settings/development/DevelopmentSettings;->bgp:Landroid/preference/CheckBoxPreference;

    invoke-virtual {p0}, Lcom/android/settings/development/DevelopmentSettings;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-static {v1}, Lcom/android/a/a;->caE(Landroid/content/Context;)Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    iget-object v0, p0, Lcom/android/settings/development/DevelopmentSettings;->bgo:Landroid/preference/CheckBoxPreference;

    invoke-static {}, Lcom/android/a/a;->caD()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    goto/16 :goto_4

    :cond_14
    :try_start_1
    const-string/jumbo v1, "file"

    const-string/jumbo v2, "ro.crypto.type"

    const-string/jumbo v3, "none"

    invoke-static {v2, v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_b

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setEnabled(Z)V

    invoke-virtual {p0}, Lcom/android/settings/development/DevelopmentSettings;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f12047f

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_5

    :catch_0
    move-exception v0

    const-string/jumbo v0, "convert_to_file_encryption"

    invoke-virtual {p0, v0}, Lcom/android/settings/development/DevelopmentSettings;->bWK(Ljava/lang/String;)V

    goto/16 :goto_5

    :cond_15
    const-string/jumbo v0, "color_temperature"

    invoke-virtual {p0, v0}, Lcom/android/settings/development/DevelopmentSettings;->bWK(Ljava/lang/String;)V

    iput-object v4, p0, Lcom/android/settings/development/DevelopmentSettings;->bgO:Landroid/preference/CheckBoxPreference;

    goto/16 :goto_6

    :cond_16
    const-string/jumbo v0, "miui_experience_optimization"

    invoke-virtual {p0, v0}, Lcom/android/settings/development/DevelopmentSettings;->bWK(Ljava/lang/String;)V

    iput-object v4, p0, Lcom/android/settings/development/DevelopmentSettings;->bhi:Landroid/preference/CheckBoxPreference;

    goto/16 :goto_7
.end method

.method public onDestroy()V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/development/DevelopmentSettings;->aUD()V

    invoke-super {p0}, Lcom/android/settings/MiuiRestrictedSettingsFragment;->onDestroy()V

    return-void
.end method

.method public onDestroyView()V
    .locals 4

    const/4 v3, 0x0

    invoke-super {p0}, Lcom/android/settings/MiuiRestrictedSettingsFragment;->onDestroyView()V

    iget-boolean v0, p0, Lcom/android/settings/development/DevelopmentSettings;->bhY:Z

    if-eqz v0, :cond_0

    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/android/settings/development/DevelopmentSettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/development/DevelopmentSettings;->bia:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/app/Activity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    invoke-virtual {p0}, Lcom/android/settings/development/DevelopmentSettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/development/DevelopmentSettings;->bgx:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/app/Activity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    invoke-static {}, Landroid/bluetooth/BluetoothAdapter;->getDefaultAdapter()Landroid/bluetooth/BluetoothAdapter;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/android/settings/development/DevelopmentSettings;->bgv:Landroid/bluetooth/BluetoothA2dp;

    const/4 v2, 0x2

    invoke-virtual {v0, v2, v1}, Landroid/bluetooth/BluetoothAdapter;->closeProfileProxy(ILandroid/bluetooth/BluetoothProfile;)V

    iput-object v3, p0, Lcom/android/settings/development/DevelopmentSettings;->bgv:Landroid/bluetooth/BluetoothA2dp;

    :cond_1
    return-void
.end method

.method public onDismiss(Landroid/content/DialogInterface;)V
    .locals 3

    const/4 v2, 0x0

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/android/settings/development/DevelopmentSettings;->bgn:Landroid/app/Dialog;

    if-ne p1, v0, :cond_2

    iget-boolean v0, p0, Lcom/android/settings/development/DevelopmentSettings;->bgZ:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/development/DevelopmentSettings;->bhd:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, v2}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    :cond_0
    iput-object v1, p0, Lcom/android/settings/development/DevelopmentSettings;->bgn:Landroid/app/Dialog;

    :cond_1
    :goto_0
    return-void

    :cond_2
    iget-object v0, p0, Lcom/android/settings/development/DevelopmentSettings;->bhe:Landroid/app/Dialog;

    if-ne p1, v0, :cond_4

    iget-boolean v0, p0, Lcom/android/settings/development/DevelopmentSettings;->bgZ:Z

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/android/settings/development/DevelopmentSettings;->bhh:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, v2}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    :cond_3
    iput-object v1, p0, Lcom/android/settings/development/DevelopmentSettings;->bhe:Landroid/app/Dialog;

    goto :goto_0

    :cond_4
    iget-object v0, p0, Lcom/android/settings/development/DevelopmentSettings;->bhv:Landroid/app/Dialog;

    if-ne p1, v0, :cond_5

    iput-object v1, p0, Lcom/android/settings/development/DevelopmentSettings;->bhv:Landroid/app/Dialog;

    goto :goto_0

    :cond_5
    iget-object v0, p0, Lcom/android/settings/development/DevelopmentSettings;->bgX:Landroid/app/Dialog;

    if-ne p1, v0, :cond_1

    iget-boolean v0, p0, Lcom/android/settings/development/DevelopmentSettings;->bgZ:Z

    if-nez v0, :cond_6

    iget-object v0, p0, Lcom/android/settings/development/DevelopmentSettings;->bgW:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, v2}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    :cond_6
    iput-object v1, p0, Lcom/android/settings/development/DevelopmentSettings;->bgX:Landroid/app/Dialog;

    goto :goto_0
.end method

.method public onInflateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 4

    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    const-string/jumbo v1, "android.hardware.usb.action.USB_STATE"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/android/settings/development/DevelopmentSettings;->getActivity()Landroid/app/Activity;

    move-result-object v1

    iget-object v2, p0, Lcom/android/settings/development/DevelopmentSettings;->bia:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/app/Activity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/android/settings/development/DevelopmentSettings;->aVO()V

    :cond_0
    invoke-static {}, Landroid/bluetooth/BluetoothAdapter;->getDefaultAdapter()Landroid/bluetooth/BluetoothAdapter;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/android/settings/development/DevelopmentSettings;->getActivity()Landroid/app/Activity;

    move-result-object v1

    iget-object v2, p0, Lcom/android/settings/development/DevelopmentSettings;->bgy:Landroid/bluetooth/BluetoothProfile$ServiceListener;

    const/4 v3, 0x2

    invoke-virtual {v0, v1, v2, v3}, Landroid/bluetooth/BluetoothAdapter;->getProfileProxy(Landroid/content/Context;Landroid/bluetooth/BluetoothProfile$ServiceListener;I)Z

    :cond_1
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    const-string/jumbo v1, "android.bluetooth.a2dp.profile.action.CODEC_CONFIG_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/android/settings/development/DevelopmentSettings;->getActivity()Landroid/app/Activity;

    move-result-object v1

    iget-object v2, p0, Lcom/android/settings/development/DevelopmentSettings;->bgx:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/app/Activity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    move-result-object v0

    if-nez v0, :cond_2

    invoke-direct {p0}, Lcom/android/settings/development/DevelopmentSettings;->aVa()V

    :cond_2
    invoke-super {p0, p1, p2, p3}, Lcom/android/settings/MiuiRestrictedSettingsFragment;->onInflateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public onPause()V
    .locals 1

    invoke-super {p0}, Lcom/android/settings/MiuiRestrictedSettingsFragment;->onPause()V

    iget-object v0, p0, Lcom/android/settings/development/DevelopmentSettings;->bgN:Lcom/android/settings/development/ColorModePreference;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/development/DevelopmentSettings;->bgN:Lcom/android/settings/development/ColorModePreference;

    invoke-virtual {v0}, Lcom/android/settings/development/ColorModePreference;->aUj()V

    :cond_0
    return-void
.end method

.method public onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 4

    const/4 v3, 0x0

    const/4 v2, 0x1

    invoke-static {}, Lcom/android/settings/aq;->bqE()Z

    move-result v0

    if-eqz v0, :cond_0

    return v3

    :cond_0
    const-string/jumbo v0, "hdcp_checking"

    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string/jumbo v0, "persist.sys.hdcp_checking"

    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/android/settings/development/DevelopmentSettings;->aVo()V

    invoke-virtual {p0}, Lcom/android/settings/development/DevelopmentSettings;->aUM()V

    return v2

    :cond_1
    iget-object v0, p0, Lcom/android/settings/development/DevelopmentSettings;->bgG:Landroid/preference/ListPreference;

    if-ne p1, v0, :cond_2

    invoke-direct {p0, p2}, Lcom/android/settings/development/DevelopmentSettings;->aVW(Ljava/lang/Object;)V

    return v2

    :cond_2
    iget-object v0, p0, Lcom/android/settings/development/DevelopmentSettings;->bgD:Landroid/preference/ListPreference;

    if-eq p1, v0, :cond_3

    iget-object v0, p0, Lcom/android/settings/development/DevelopmentSettings;->bgF:Landroid/preference/ListPreference;

    if-ne p1, v0, :cond_4

    :cond_3
    invoke-direct {p0, p1, p2}, Lcom/android/settings/development/DevelopmentSettings;->aVX(Landroid/preference/Preference;Ljava/lang/Object;)V

    return v2

    :cond_4
    iget-object v0, p0, Lcom/android/settings/development/DevelopmentSettings;->bgB:Landroid/preference/ListPreference;

    if-eq p1, v0, :cond_3

    iget-object v0, p0, Lcom/android/settings/development/DevelopmentSettings;->bgC:Landroid/preference/ListPreference;

    if-eq p1, v0, :cond_3

    iget-object v0, p0, Lcom/android/settings/development/DevelopmentSettings;->bgE:Landroid/preference/ListPreference;

    if-eq p1, v0, :cond_3

    iget-object v0, p0, Lcom/android/settings/development/DevelopmentSettings;->bht:Landroid/preference/ListPreference;

    if-ne p1, v0, :cond_5

    invoke-direct {p0, p2}, Lcom/android/settings/development/DevelopmentSettings;->aWo(Ljava/lang/Object;)V

    return v2

    :cond_5
    iget-object v0, p0, Lcom/android/settings/development/DevelopmentSettings;->bhu:Landroid/preference/ListPreference;

    if-ne p1, v0, :cond_6

    invoke-direct {p0, p2, v3}, Lcom/android/settings/development/DevelopmentSettings;->aWp(Ljava/lang/Object;Z)V

    return v2

    :cond_6
    iget-object v0, p0, Lcom/android/settings/development/DevelopmentSettings;->bhZ:Landroid/preference/ListPreference;

    if-ne p1, v0, :cond_7

    invoke-direct {p0, p2}, Lcom/android/settings/development/DevelopmentSettings;->aWI(Ljava/lang/Object;)V

    return v2

    :cond_7
    iget-object v0, p0, Lcom/android/settings/development/DevelopmentSettings;->bik:Landroid/preference/ListPreference;

    if-ne p1, v0, :cond_8

    iget-object v0, p0, Lcom/android/settings/development/DevelopmentSettings;->bik:Landroid/preference/ListPreference;

    invoke-direct {p0, v3, v0, p2}, Lcom/android/settings/development/DevelopmentSettings;->aVU(ILandroid/preference/ListPreference;Ljava/lang/Object;)V

    return v2

    :cond_8
    iget-object v0, p0, Lcom/android/settings/development/DevelopmentSettings;->bhV:Landroid/preference/ListPreference;

    if-ne p1, v0, :cond_9

    iget-object v0, p0, Lcom/android/settings/development/DevelopmentSettings;->bhV:Landroid/preference/ListPreference;

    invoke-direct {p0, v2, v0, p2}, Lcom/android/settings/development/DevelopmentSettings;->aVU(ILandroid/preference/ListPreference;Ljava/lang/Object;)V

    return v2

    :cond_9
    iget-object v0, p0, Lcom/android/settings/development/DevelopmentSettings;->bgs:Landroid/preference/ListPreference;

    if-ne p1, v0, :cond_a

    iget-object v0, p0, Lcom/android/settings/development/DevelopmentSettings;->bgs:Landroid/preference/ListPreference;

    const/4 v1, 0x2

    invoke-direct {p0, v1, v0, p2}, Lcom/android/settings/development/DevelopmentSettings;->aVU(ILandroid/preference/ListPreference;Ljava/lang/Object;)V

    return v2

    :cond_a
    iget-object v0, p0, Lcom/android/settings/development/DevelopmentSettings;->bhB:Landroid/preference/ListPreference;

    if-ne p1, v0, :cond_b

    invoke-direct {p0, p2}, Lcom/android/settings/development/DevelopmentSettings;->aWu(Ljava/lang/Object;)V

    return v2

    :cond_b
    iget-object v0, p0, Lcom/android/settings/development/DevelopmentSettings;->bhU:Landroid/preference/ListPreference;

    if-ne p1, v0, :cond_c

    invoke-direct {p0, p2}, Lcom/android/settings/development/DevelopmentSettings;->aWG(Ljava/lang/Object;)V

    return v2

    :cond_c
    iget-object v0, p0, Lcom/android/settings/development/DevelopmentSettings;->bgR:Landroid/preference/ListPreference;

    if-ne p1, v0, :cond_d

    invoke-direct {p0, p2}, Lcom/android/settings/development/DevelopmentSettings;->aWd(Ljava/lang/Object;)V

    return v2

    :cond_d
    iget-object v0, p0, Lcom/android/settings/development/DevelopmentSettings;->bgS:Landroid/preference/ListPreference;

    if-ne p1, v0, :cond_e

    invoke-direct {p0, p2}, Lcom/android/settings/development/DevelopmentSettings;->aWe(Ljava/lang/Object;)V

    return v2

    :cond_e
    iget-object v0, p0, Lcom/android/settings/development/DevelopmentSettings;->bhL:Landroid/preference/ListPreference;

    if-ne p1, v0, :cond_f

    invoke-direct {p0, p2}, Lcom/android/settings/development/DevelopmentSettings;->aWA(Ljava/lang/Object;)V

    return v2

    :cond_f
    iget-object v0, p0, Lcom/android/settings/development/DevelopmentSettings;->bgt:Landroid/preference/ListPreference;

    if-ne p1, v0, :cond_10

    invoke-direct {p0, p2}, Lcom/android/settings/development/DevelopmentSettings;->aVV(Ljava/lang/Object;)V

    return v2

    :cond_10
    iget-object v0, p0, Lcom/android/settings/development/DevelopmentSettings;->bhP:Landroid/preference/ListPreference;

    if-ne p1, v0, :cond_11

    invoke-direct {p0, p2}, Lcom/android/settings/development/DevelopmentSettings;->aWE(Ljava/lang/Object;)V

    return v2

    :cond_11
    iget-object v0, p0, Lcom/android/settings/development/DevelopmentSettings;->bhi:Landroid/preference/CheckBoxPreference;

    if-ne p1, v0, :cond_12

    invoke-direct {p0, p2}, Lcom/android/settings/development/DevelopmentSettings;->aWq(Ljava/lang/Object;)V

    return v3

    :cond_12
    iget-object v0, p0, Lcom/android/settings/development/DevelopmentSettings;->bhs:Landroid/preference/ListPreference;

    if-ne p1, v0, :cond_13

    invoke-direct {p0, p2}, Lcom/android/settings/development/DevelopmentSettings;->aWn(Ljava/lang/Object;)V

    return v2

    :cond_13
    return v3
.end method

.method public onResume()V
    .locals 4

    const/4 v3, 0x1

    const/4 v2, 0x0

    invoke-super {p0}, Lcom/android/settings/MiuiRestrictedSettingsFragment;->onResume()V

    iget-boolean v0, p0, Lcom/android/settings/development/DevelopmentSettings;->bhY:Z

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/android/settings/development/DevelopmentSettings;->bXC()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/android/settings/development/DevelopmentSettings;->bXD()Landroid/widget/TextView;

    move-result-object v0

    const v1, 0x7f12057e

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    :cond_0
    invoke-virtual {p0}, Lcom/android/settings/development/DevelopmentSettings;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    invoke-virtual {v0}, Landroid/preference/PreferenceScreen;->removeAll()V

    return-void

    :cond_1
    invoke-virtual {p0}, Lcom/android/settings/development/DevelopmentSettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/android/settingslib/w;->crf(Landroid/content/Context;)Lcom/android/settingslib/n;

    move-result-object v0

    if-nez v0, :cond_6

    iget-object v0, p0, Lcom/android/settings/development/DevelopmentSettings;->bhb:Ljava/util/HashSet;

    iget-object v1, p0, Lcom/android/settings/development/DevelopmentSettings;->bhq:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    :goto_0
    iget-object v0, p0, Lcom/android/settings/development/DevelopmentSettings;->bhG:Lcom/android/settings/development/b;

    invoke-virtual {v0}, Lcom/android/settings/development/b;->aUs()Z

    move-result v0

    iget-object v1, p0, Lcom/android/settings/development/DevelopmentSettings;->bhh:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v1, v0}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    invoke-direct {p0, v0}, Lcom/android/settings/development/DevelopmentSettings;->aUS(Z)V

    iget-boolean v1, p0, Lcom/android/settings/development/DevelopmentSettings;->bho:Z

    if-eqz v1, :cond_2

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/settings/development/DevelopmentSettings;->bhG:Lcom/android/settings/development/b;

    invoke-virtual {v0}, Lcom/android/settings/development/b;->aUq()V

    iget-object v0, p0, Lcom/android/settings/development/DevelopmentSettings;->bhh:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, v3}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    invoke-direct {p0, v3}, Lcom/android/settings/development/DevelopmentSettings;->aUS(Z)V

    :cond_2
    iget-object v0, p0, Lcom/android/settings/development/DevelopmentSettings;->bgN:Lcom/android/settings/development/ColorModePreference;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/android/settings/development/DevelopmentSettings;->bgN:Lcom/android/settings/development/ColorModePreference;

    invoke-virtual {v0}, Lcom/android/settings/development/ColorModePreference;->aUi()V

    iget-object v0, p0, Lcom/android/settings/development/DevelopmentSettings;->bgN:Lcom/android/settings/development/ColorModePreference;

    invoke-virtual {v0}, Lcom/android/settings/development/ColorModePreference;->aUk()V

    :cond_3
    const-string/jumbo v0, "secondspace_category"

    invoke-virtual {p0, v0}, Lcom/android/settings/development/DevelopmentSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceCategory;

    iput-object v0, p0, Lcom/android/settings/development/DevelopmentSettings;->bhF:Landroid/preference/PreferenceCategory;

    invoke-direct {p0}, Lcom/android/settings/development/DevelopmentSettings;->aUH()I

    move-result v0

    invoke-static {}, Lmiui/securityspace/ConfigUtils;->isSupportSecuritySpace()Z

    move-result v1

    if-eqz v1, :cond_4

    const/16 v1, -0x2710

    if-ne v0, v1, :cond_7

    :cond_4
    :goto_1
    iget-object v0, p0, Lcom/android/settings/development/DevelopmentSettings;->bhF:Landroid/preference/PreferenceCategory;

    if-eqz v0, :cond_5

    invoke-virtual {p0}, Lcom/android/settings/development/DevelopmentSettings;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/development/DevelopmentSettings;->bhF:Landroid/preference/PreferenceCategory;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    iput-object v2, p0, Lcom/android/settings/development/DevelopmentSettings;->bhF:Landroid/preference/PreferenceCategory;

    :cond_5
    return-void

    :cond_6
    iget-object v0, p0, Lcom/android/settings/development/DevelopmentSettings;->bhb:Ljava/util/HashSet;

    iget-object v1, p0, Lcom/android/settings/development/DevelopmentSettings;->bhq:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_7
    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v0

    if-eqz v0, :cond_5

    goto :goto_1
.end method
