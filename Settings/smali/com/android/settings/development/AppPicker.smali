.class public Lcom/android/settings/development/AppPicker;
.super Lmiui/app/ListActivity;
.source "AppPicker.java"


# static fields
.field private static final bir:Ljava/util/Comparator;


# instance fields
.field private bio:Lcom/android/settings/development/i;

.field private bip:Z

.field private biq:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/android/settings/development/q;

    invoke-direct {v0}, Lcom/android/settings/development/q;-><init>()V

    sput-object v0, Lcom/android/settings/development/AppPicker;->bir:Ljava/util/Comparator;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lmiui/app/ListActivity;-><init>()V

    return-void
.end method

.method static synthetic aXe(Lcom/android/settings/development/AppPicker;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/settings/development/AppPicker;->bip:Z

    return v0
.end method

.method static synthetic aXf(Lcom/android/settings/development/AppPicker;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/development/AppPicker;->biq:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic aXg()Ljava/util/Comparator;
    .locals 1

    sget-object v0, Lcom/android/settings/development/AppPicker;->bir:Ljava/util/Comparator;

    return-object v0
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3

    const/4 v2, 0x0

    invoke-super {p0, p1}, Lmiui/app/ListActivity;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/android/settings/development/AppPicker;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "com.android.settings.extra.REQUESTIING_PERMISSION"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/development/AppPicker;->biq:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/android/settings/development/AppPicker;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "com.android.settings.extra.DEBUGGABLE"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/settings/development/AppPicker;->bip:Z

    new-instance v0, Lcom/android/settings/development/i;

    invoke-direct {v0, p0, p0}, Lcom/android/settings/development/i;-><init>(Lcom/android/settings/development/AppPicker;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/settings/development/AppPicker;->bio:Lcom/android/settings/development/i;

    iget-object v0, p0, Lcom/android/settings/development/AppPicker;->bio:Lcom/android/settings/development/i;

    invoke-virtual {v0}, Lcom/android/settings/development/i;->getCount()I

    move-result v0

    if-gtz v0, :cond_0

    invoke-virtual {p0}, Lcom/android/settings/development/AppPicker;->finish()V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/settings/development/AppPicker;->bio:Lcom/android/settings/development/i;

    invoke-virtual {p0, v0}, Lcom/android/settings/development/AppPicker;->setListAdapter(Landroid/widget/ListAdapter;)V

    goto :goto_0
.end method

.method protected onListItemClick(Landroid/widget/ListView;Landroid/view/View;IJ)V
    .locals 3

    iget-object v0, p0, Lcom/android/settings/development/AppPicker;->bio:Lcom/android/settings/development/i;

    invoke-virtual {v0, p3}, Lcom/android/settings/development/i;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settings/development/h;

    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    iget-object v2, v0, Lcom/android/settings/development/h;->info:Landroid/content/pm/ApplicationInfo;

    if-eqz v2, :cond_0

    iget-object v0, v0, Lcom/android/settings/development/h;->info:Landroid/content/pm/ApplicationInfo;

    iget-object v0, v0, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    :cond_0
    const/4 v0, -0x1

    invoke-virtual {p0, v0, v1}, Lcom/android/settings/development/AppPicker;->setResult(ILandroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/android/settings/development/AppPicker;->finish()V

    return-void
.end method

.method protected onResume()V
    .locals 0

    invoke-super {p0}, Lmiui/app/ListActivity;->onResume()V

    return-void
.end method

.method protected onStop()V
    .locals 0

    invoke-super {p0}, Lmiui/app/ListActivity;->onStop()V

    return-void
.end method
