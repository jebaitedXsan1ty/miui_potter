.class public Lcom/android/settings/UsageStatsActivity;
.super Landroid/app/Activity;
.source "UsageStatsActivity.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemSelectedListener;


# instance fields
.field private bRn:Lcom/android/settings/bu;

.field private bRo:Landroid/view/LayoutInflater;

.field private bRp:Landroid/content/pm/PackageManager;

.field private bRq:Landroid/app/usage/UsageStatsManager;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method

.method static synthetic bJS(Lcom/android/settings/UsageStatsActivity;)Landroid/view/LayoutInflater;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/UsageStatsActivity;->bRo:Landroid/view/LayoutInflater;

    return-object v0
.end method

.method static synthetic bJT(Lcom/android/settings/UsageStatsActivity;)Landroid/content/pm/PackageManager;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/UsageStatsActivity;->bRp:Landroid/content/pm/PackageManager;

    return-object v0
.end method

.method static synthetic bJU(Lcom/android/settings/UsageStatsActivity;)Landroid/app/usage/UsageStatsManager;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/UsageStatsActivity;->bRq:Landroid/app/usage/UsageStatsManager;

    return-object v0
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    const v0, 0x7f0d0256

    invoke-virtual {p0, v0}, Lcom/android/settings/UsageStatsActivity;->setContentView(I)V

    const-string/jumbo v0, "usagestats"

    invoke-virtual {p0, v0}, Lcom/android/settings/UsageStatsActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/usage/UsageStatsManager;

    iput-object v0, p0, Lcom/android/settings/UsageStatsActivity;->bRq:Landroid/app/usage/UsageStatsManager;

    const-string/jumbo v0, "layout_inflater"

    invoke-virtual {p0, v0}, Lcom/android/settings/UsageStatsActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Lcom/android/settings/UsageStatsActivity;->bRo:Landroid/view/LayoutInflater;

    invoke-virtual {p0}, Lcom/android/settings/UsageStatsActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/UsageStatsActivity;->bRp:Landroid/content/pm/PackageManager;

    const v0, 0x7f0a04c4

    invoke-virtual {p0, v0}, Lcom/android/settings/UsageStatsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    invoke-virtual {v0, p0}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    const v0, 0x7f0a0322

    invoke-virtual {p0, v0}, Lcom/android/settings/UsageStatsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    new-instance v1, Lcom/android/settings/bu;

    invoke-direct {v1, p0}, Lcom/android/settings/bu;-><init>(Lcom/android/settings/UsageStatsActivity;)V

    iput-object v1, p0, Lcom/android/settings/UsageStatsActivity;->bRn:Lcom/android/settings/bu;

    iget-object v1, p0, Lcom/android/settings/UsageStatsActivity;->bRn:Lcom/android/settings/bu;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    return-void
.end method

.method public onItemSelected(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 1

    iget-object v0, p0, Lcom/android/settings/UsageStatsActivity;->bRn:Lcom/android/settings/bu;

    invoke-virtual {v0, p3}, Lcom/android/settings/bu;->bJZ(I)V

    return-void
.end method

.method public onNothingSelected(Landroid/widget/AdapterView;)V
    .locals 0

    return-void
.end method
