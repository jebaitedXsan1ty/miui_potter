.class Lcom/android/settings/cJ;
.super Ljava/lang/Object;
.source "SettingsApplication.java"


# static fields
.field private static cdi:Lcom/android/settings/cJ;

.field private static final synthetic cdj:[I


# instance fields
.field private mContext:Landroid/content/Context;


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/android/settings/cJ;->mContext:Landroid/content/Context;

    return-void
.end method

.method private bUT(Lcom/android/settings/ShortcutHelper$Shortcut;)Landroid/content/Intent;
    .locals 4

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const/high16 v1, 0x10200000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    invoke-static {}, Lcom/android/settings/cJ;->bUW()[I

    move-result-object v1

    invoke-virtual {p1}, Lcom/android/settings/ShortcutHelper$Shortcut;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    :goto_0
    return-object v0

    :pswitch_0
    const-string/jumbo v1, "miui.intent.action.GARBAGE_CLEANUP"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const-string/jumbo v1, "android.intent.category.DEFAULT"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    new-instance v1, Landroid/content/ComponentName;

    const-string/jumbo v2, "com.android.settings"

    const-string/jumbo v3, "com.miui.optimizecenter.MainActivity"

    invoke-direct {v1, v2, v3}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    goto :goto_0

    :pswitch_1
    const-string/jumbo v1, "android.intent.action.VIEW_DATA_USAGE_SUMMARY"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const-string/jumbo v1, "android.intent.category.DEFAULT"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    new-instance v1, Landroid/content/ComponentName;

    const-string/jumbo v2, "com.miui.networkassistant"

    const-string/jumbo v3, "com.miui.networkassistant.ui.MainActivity"

    invoke-direct {v1, v2, v3}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    goto :goto_0

    :pswitch_2
    const-string/jumbo v1, "android.intent.action.SET_FIREWALL"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const-string/jumbo v1, "android.intent.category.DEFAULT"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    new-instance v1, Landroid/content/ComponentName;

    const-string/jumbo v2, "com.miui.antispam"

    const-string/jumbo v3, "com.miui.antispam.ui.activity.MainActivity"

    invoke-direct {v1, v2, v3}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    goto :goto_0

    :pswitch_3
    const-string/jumbo v1, "com.miui.powercenter.PowerCenter"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const-string/jumbo v1, "android.intent.category.DEFAULT"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    new-instance v1, Landroid/content/ComponentName;

    const-string/jumbo v2, "com.android.settings"

    const-string/jumbo v3, "com.miui.powercenter.PowerCenter"

    invoke-direct {v1, v2, v3}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    goto :goto_0

    :pswitch_4
    const-string/jumbo v1, "miui.intent.action.VIRUS_SCAN"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const-string/jumbo v1, "android.intent.category.DEFAULT"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    new-instance v1, Landroid/content/ComponentName;

    const-string/jumbo v2, "com.android.settings"

    const-string/jumbo v3, "com.miui.viruscenter.activity.VirusScanAppActivity"

    invoke-direct {v1, v2, v3}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    goto/16 :goto_0

    :pswitch_5
    const-string/jumbo v1, "miui.intent.action.PERM_CENTER"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const-string/jumbo v1, "android.intent.category.DEFAULT"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    new-instance v1, Landroid/content/ComponentName;

    const-string/jumbo v2, "com.android.settings"

    const-string/jumbo v3, "com.miui.securitycenter.permission.PermMainActivity"

    invoke-direct {v1, v2, v3}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_5
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method private static synthetic bUW()[I
    .locals 3

    sget-object v0, Lcom/android/settings/cJ;->cdj:[I

    if-eqz v0, :cond_0

    sget-object v0, Lcom/android/settings/cJ;->cdj:[I

    return-object v0

    :cond_0
    invoke-static {}, Lcom/android/settings/ShortcutHelper$Shortcut;->values()[Lcom/android/settings/ShortcutHelper$Shortcut;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Lcom/android/settings/ShortcutHelper$Shortcut;->cdl:Lcom/android/settings/ShortcutHelper$Shortcut;

    invoke-virtual {v1}, Lcom/android/settings/ShortcutHelper$Shortcut;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_5

    :goto_0
    :try_start_1
    sget-object v1, Lcom/android/settings/ShortcutHelper$Shortcut;->cdm:Lcom/android/settings/ShortcutHelper$Shortcut;

    invoke-virtual {v1}, Lcom/android/settings/ShortcutHelper$Shortcut;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_4

    :goto_1
    :try_start_2
    sget-object v1, Lcom/android/settings/ShortcutHelper$Shortcut;->cdn:Lcom/android/settings/ShortcutHelper$Shortcut;

    invoke-virtual {v1}, Lcom/android/settings/ShortcutHelper$Shortcut;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_3

    :goto_2
    :try_start_3
    sget-object v1, Lcom/android/settings/ShortcutHelper$Shortcut;->cdo:Lcom/android/settings/ShortcutHelper$Shortcut;

    invoke-virtual {v1}, Lcom/android/settings/ShortcutHelper$Shortcut;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_2

    :goto_3
    :try_start_4
    sget-object v1, Lcom/android/settings/ShortcutHelper$Shortcut;->cdp:Lcom/android/settings/ShortcutHelper$Shortcut;

    invoke-virtual {v1}, Lcom/android/settings/ShortcutHelper$Shortcut;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_1

    :goto_4
    :try_start_5
    sget-object v1, Lcom/android/settings/ShortcutHelper$Shortcut;->cdq:Lcom/android/settings/ShortcutHelper$Shortcut;

    invoke-virtual {v1}, Lcom/android/settings/ShortcutHelper$Shortcut;->ordinal()I

    move-result v1

    const/4 v2, 0x6

    aput v2, v0, v1
    :try_end_5
    .catch Ljava/lang/NoSuchFieldError; {:try_start_5 .. :try_end_5} :catch_0

    :goto_5
    sput-object v0, Lcom/android/settings/cJ;->cdj:[I

    return-object v0

    :catch_0
    move-exception v1

    goto :goto_5

    :catch_1
    move-exception v1

    goto :goto_4

    :catch_2
    move-exception v1

    goto :goto_3

    :catch_3
    move-exception v1

    goto :goto_2

    :catch_4
    move-exception v1

    goto :goto_1

    :catch_5
    move-exception v1

    goto :goto_0
.end method

.method public static getInstance(Landroid/content/Context;)Lcom/android/settings/cJ;
    .locals 2

    sget-object v0, Lcom/android/settings/cJ;->cdi:Lcom/android/settings/cJ;

    if-nez v0, :cond_0

    new-instance v0, Lcom/android/settings/cJ;

    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/android/settings/cJ;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/android/settings/cJ;->cdi:Lcom/android/settings/cJ;

    :cond_0
    sget-object v0, Lcom/android/settings/cJ;->cdi:Lcom/android/settings/cJ;

    return-object v0
.end method


# virtual methods
.method public bUU(Lcom/android/settings/ShortcutHelper$Shortcut;Ljava/lang/String;)Landroid/content/Intent;
    .locals 6

    const/4 v2, 0x0

    const/4 v0, -0x1

    invoke-static {}, Lcom/android/settings/cJ;->bUW()[I

    move-result-object v1

    invoke-virtual {p1}, Lcom/android/settings/ShortcutHelper$Shortcut;->ordinal()I

    move-result v3

    aget v1, v1, v3

    packed-switch v1, :pswitch_data_0

    move-object v1, v2

    :goto_0
    invoke-direct {p0, p1}, Lcom/android/settings/cJ;->bUT(Lcom/android/settings/ShortcutHelper$Shortcut;)Landroid/content/Intent;

    move-result-object v3

    if-nez v3, :cond_0

    return-object v2

    :pswitch_0
    const-string/jumbo v1, "com.android.settings:string/cleaner"

    const v0, 0x7f0801f5

    goto :goto_0

    :pswitch_1
    const-string/jumbo v1, "com.android.settings:string/network_assistant"

    const v0, 0x7f0801f2

    goto :goto_0

    :pswitch_2
    const-string/jumbo v1, "com.android.settings:string/anti_spam"

    const v0, 0x7f0801f0

    goto :goto_0

    :pswitch_3
    const-string/jumbo v1, "com.android.settings:string/power_mgr"

    const v0, 0x7f0801f3

    goto :goto_0

    :pswitch_4
    const-string/jumbo v1, "com.android.settings:string/virus_scan"

    const v0, 0x7f0801f7

    goto :goto_0

    :pswitch_5
    const-string/jumbo v1, "com.android.settings:string/permission_mgr"

    const v0, 0x7f0801f1

    goto :goto_0

    :cond_0
    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2, p2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string/jumbo v4, "duplicate"

    const/4 v5, 0x0

    invoke-virtual {v2, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string/jumbo v4, "android.intent.extra.shortcut.NAME"

    invoke-virtual {v2, v4, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    iget-object v1, p0, Lcom/android/settings/cJ;->mContext:Landroid/content/Context;

    invoke-static {v1, v0}, Landroid/content/Intent$ShortcutIconResource;->fromContext(Landroid/content/Context;I)Landroid/content/Intent$ShortcutIconResource;

    move-result-object v0

    const-string/jumbo v1, "android.intent.extra.shortcut.ICON_RESOURCE"

    invoke-virtual {v2, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string/jumbo v0, "android.intent.extra.shortcut.INTENT"

    invoke-virtual {v2, v0, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    return-object v2

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_5
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public bUV(Lcom/android/settings/ShortcutHelper$Shortcut;)V
    .locals 2

    const-string/jumbo v0, "com.miui.home.launcher.action.UNINSTALL_SHORTCUT"

    invoke-virtual {p0, p1, v0}, Lcom/android/settings/cJ;->bUU(Lcom/android/settings/ShortcutHelper$Shortcut;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/cJ;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    return-void
.end method
