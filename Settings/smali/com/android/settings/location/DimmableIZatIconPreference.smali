.class public Lcom/android/settings/location/DimmableIZatIconPreference;
.super Lcom/android/settings/DimmableIconPreference;
.source "DimmableIZatIconPreference.java"


# static fields
.field private static acG:Ljava/lang/reflect/Method;

.field private static acH:Ljava/lang/reflect/Method;

.field private static acI:Ljava/lang/String;

.field private static acJ:Ldalvik/system/DexClassLoader;

.field private static acK:Ljava/lang/Class;

.field private static acL:Ljava/lang/reflect/Method;

.field private static acM:Ljava/lang/Class;


# instance fields
.field acF:Z


# direct methods
.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;ILjava/lang/CharSequence;)V
    .locals 4

    const/4 v1, 0x1

    const/4 v3, 0x0

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/android/settings/DimmableIconPreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;ILjava/lang/CharSequence;)V

    sget-object v0, Lcom/android/settings/location/DimmableIZatIconPreference;->acJ:Ldalvik/system/DexClassLoader;

    new-array v1, v1, [Ljava/lang/Class;

    sget-object v2, Lcom/android/settings/location/DimmableIZatIconPreference;->acK:Ljava/lang/Class;

    aput-object v2, v1, v3

    new-instance v2, Lcom/android/settings/location/DimmableIZatIconPreference$1;

    invoke-direct {v2, p0}, Lcom/android/settings/location/DimmableIZatIconPreference$1;-><init>(Lcom/android/settings/location/DimmableIZatIconPreference;)V

    invoke-static {v0, v1, v2}, Ljava/lang/reflect/Proxy;->newProxyInstance(Ljava/lang/ClassLoader;[Ljava/lang/Class;Ljava/lang/reflect/InvocationHandler;)Ljava/lang/Object;

    move-result-object v0

    :try_start_0
    sget-object v1, Lcom/android/settings/location/DimmableIZatIconPreference;->acH:Ljava/lang/reflect/Method;

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    const/4 v3, 0x1

    aput-object v0, v2, v3

    const/4 v0, 0x0

    invoke-virtual {v1, v0, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    sget-object v1, Lcom/android/settings/location/DimmableIZatIconPreference;->acG:Ljava/lang/reflect/Method;

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v1, v0, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/settings/location/DimmableIZatIconPreference;->acF:Z
    :try_end_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/ExceptionInInitializerError; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_0
.end method

.method static SS(Landroid/content/Context;Ljava/lang/String;)Z
    .locals 4

    const/4 v1, 0x1

    invoke-static {p0}, Lcom/android/settings/location/DimmableIZatIconPreference;->SU(Landroid/content/Context;)V

    :try_start_0
    sget-object v0, Lcom/android/settings/location/DimmableIZatIconPreference;->acL:Ljava/lang/reflect/Method;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/android/settings/location/DimmableIZatIconPreference;->acL:Ljava/lang/reflect/Method;

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p0, v2, v3

    const/4 v3, 0x1

    aput-object p1, v2, v3

    const/4 v3, 0x0

    invoke-virtual {v0, v3, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z
    :try_end_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/ExceptionInInitializerError; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    :goto_0
    return v0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    move v0, v1

    goto :goto_0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method public static ST(Landroid/content/Context;Ljava/lang/CharSequence;Lcom/android/settings/location/InjectedSetting;)Lcom/android/settings/DimmableIconPreference;
    .locals 3

    const/4 v2, 0x0

    invoke-static {p0}, Lcom/android/settings/location/DimmableIZatIconPreference;->SU(Landroid/content/Context;)V

    sget-object v0, Lcom/android/settings/location/DimmableIZatIconPreference;->acI:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/android/settings/location/DimmableIZatIconPreference;->acI:Ljava/lang/String;

    iget-object v1, p2, Lcom/android/settings/location/InjectedSetting;->packageName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Lcom/android/settings/location/DimmableIZatIconPreference;

    const v1, 0x101008f

    invoke-direct {v0, p0, v2, v1, p1}, Lcom/android/settings/location/DimmableIZatIconPreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;ILjava/lang/CharSequence;)V

    return-object v0

    :cond_0
    new-instance v0, Lcom/android/settings/DimmableIconPreference;

    invoke-direct {v0, p0, p1}, Lcom/android/settings/DimmableIconPreference;-><init>(Landroid/content/Context;Ljava/lang/CharSequence;)V

    return-object v0
.end method

.method private static SU(Landroid/content/Context;)V
    .locals 6

    const/4 v5, 0x0

    sget-object v0, Lcom/android/settings/location/DimmableIZatIconPreference;->acJ:Ldalvik/system/DexClassLoader;

    if-nez v0, :cond_1

    :try_start_0
    sget-object v0, Lcom/android/settings/location/DimmableIZatIconPreference;->acM:Ljava/lang/Class;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/android/settings/location/DimmableIZatIconPreference;->acK:Ljava/lang/Class;

    if-nez v0, :cond_1

    :cond_0
    new-instance v0, Ldalvik/system/DexClassLoader;

    const-string/jumbo v1, "/system/framework/izat.xt.srv.jar"

    invoke-virtual {p0}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v2

    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    invoke-static {}, Ljava/lang/ClassLoader;->getSystemClassLoader()Ljava/lang/ClassLoader;

    move-result-object v3

    const/4 v4, 0x0

    invoke-direct {v0, v1, v2, v4, v3}, Ldalvik/system/DexClassLoader;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/ClassLoader;)V

    sput-object v0, Lcom/android/settings/location/DimmableIZatIconPreference;->acJ:Ldalvik/system/DexClassLoader;

    const-string/jumbo v0, "com.qti.izat.XTProxy"

    sget-object v1, Lcom/android/settings/location/DimmableIZatIconPreference;->acJ:Ldalvik/system/DexClassLoader;

    const/4 v2, 0x1

    invoke-static {v0, v2, v1}, Ljava/lang/Class;->forName(Ljava/lang/String;ZLjava/lang/ClassLoader;)Ljava/lang/Class;

    move-result-object v0

    sput-object v0, Lcom/android/settings/location/DimmableIZatIconPreference;->acM:Ljava/lang/Class;

    const-string/jumbo v0, "com.qti.izat.XTProxy$Notifier"

    sget-object v1, Lcom/android/settings/location/DimmableIZatIconPreference;->acJ:Ldalvik/system/DexClassLoader;

    const/4 v2, 0x1

    invoke-static {v0, v2, v1}, Ljava/lang/Class;->forName(Ljava/lang/String;ZLjava/lang/ClassLoader;)Ljava/lang/Class;

    move-result-object v0

    sput-object v0, Lcom/android/settings/location/DimmableIZatIconPreference;->acK:Ljava/lang/Class;

    sget-object v0, Lcom/android/settings/location/DimmableIZatIconPreference;->acM:Ljava/lang/Class;

    const-string/jumbo v1, "IZAT_XT_PACKAGE"

    invoke-virtual {v0, v1}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    sput-object v0, Lcom/android/settings/location/DimmableIZatIconPreference;->acI:Ljava/lang/String;

    sget-object v0, Lcom/android/settings/location/DimmableIZatIconPreference;->acM:Ljava/lang/Class;

    const-string/jumbo v1, "getXTProxy"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Class;

    const-class v3, Landroid/content/Context;

    const/4 v4, 0x0

    aput-object v3, v2, v4

    sget-object v3, Lcom/android/settings/location/DimmableIZatIconPreference;->acK:Ljava/lang/Class;

    const/4 v4, 0x1

    aput-object v3, v2, v4

    invoke-virtual {v0, v1, v2}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    sput-object v0, Lcom/android/settings/location/DimmableIZatIconPreference;->acH:Ljava/lang/reflect/Method;

    sget-object v0, Lcom/android/settings/location/DimmableIZatIconPreference;->acM:Ljava/lang/Class;

    const-string/jumbo v1, "getUserConsent"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Class;

    invoke-virtual {v0, v1, v2}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    sput-object v0, Lcom/android/settings/location/DimmableIZatIconPreference;->acG:Ljava/lang/reflect/Method;

    sget-object v0, Lcom/android/settings/location/DimmableIZatIconPreference;->acM:Ljava/lang/Class;

    const-string/jumbo v1, "showIzat"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Class;

    const-class v3, Landroid/content/Context;

    const/4 v4, 0x0

    aput-object v3, v2, v4

    const-class v3, Ljava/lang/String;

    const/4 v4, 0x1

    aput-object v3, v2, v4

    invoke-virtual {v0, v1, v2}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    sput-object v0, Lcom/android/settings/location/DimmableIZatIconPreference;->acL:Ljava/lang/reflect/Method;
    :try_end_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NoSuchFieldException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/LinkageError; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_1
    :goto_0
    return-void

    :catch_0
    move-exception v0

    sput-object v5, Lcom/android/settings/location/DimmableIZatIconPreference;->acM:Ljava/lang/Class;

    sput-object v5, Lcom/android/settings/location/DimmableIZatIconPreference;->acK:Ljava/lang/Class;

    sput-object v5, Lcom/android/settings/location/DimmableIZatIconPreference;->acI:Ljava/lang/String;

    sput-object v5, Lcom/android/settings/location/DimmableIZatIconPreference;->acH:Ljava/lang/reflect/Method;

    sput-object v5, Lcom/android/settings/location/DimmableIZatIconPreference;->acG:Ljava/lang/reflect/Method;

    sput-object v5, Lcom/android/settings/location/DimmableIZatIconPreference;->acL:Ljava/lang/reflect/Method;

    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_0
.end method
