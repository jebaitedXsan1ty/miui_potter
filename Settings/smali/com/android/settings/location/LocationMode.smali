.class public Lcom/android/settings/location/LocationMode;
.super Lcom/android/settings/location/LocationSettingsBase;
.source "LocationMode.java"

# interfaces
.implements Lcom/android/settings/widget/D;


# instance fields
.field private acC:Lcom/android/settings/widget/MiuiRadioButtonPreference;

.field private acD:Lcom/android/settings/widget/MiuiRadioButtonPreference;

.field private acE:Lcom/android/settings/widget/MiuiRadioButtonPreference;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/location/LocationSettingsBase;-><init>()V

    return-void
.end method

.method private SQ()Landroid/preference/PreferenceScreen;
    .locals 2

    invoke-virtual {p0}, Lcom/android/settings/location/LocationMode;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/preference/PreferenceScreen;->removeAll()V

    :cond_0
    const v0, 0x7f150077

    invoke-virtual {p0, v0}, Lcom/android/settings/location/LocationMode;->addPreferencesFromResource(I)V

    invoke-virtual {p0}, Lcom/android/settings/location/LocationMode;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v1

    const-string/jumbo v0, "high_accuracy"

    invoke-virtual {v1, v0}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Lcom/android/settings/widget/MiuiRadioButtonPreference;

    iput-object v0, p0, Lcom/android/settings/location/LocationMode;->acD:Lcom/android/settings/widget/MiuiRadioButtonPreference;

    const-string/jumbo v0, "battery_saving"

    invoke-virtual {v1, v0}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Lcom/android/settings/widget/MiuiRadioButtonPreference;

    iput-object v0, p0, Lcom/android/settings/location/LocationMode;->acC:Lcom/android/settings/widget/MiuiRadioButtonPreference;

    const-string/jumbo v0, "sensors_only"

    invoke-virtual {v1, v0}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Lcom/android/settings/widget/MiuiRadioButtonPreference;

    iput-object v0, p0, Lcom/android/settings/location/LocationMode;->acE:Lcom/android/settings/widget/MiuiRadioButtonPreference;

    iget-object v0, p0, Lcom/android/settings/location/LocationMode;->acD:Lcom/android/settings/widget/MiuiRadioButtonPreference;

    invoke-virtual {v0, p0}, Lcom/android/settings/widget/MiuiRadioButtonPreference;->aCa(Lcom/android/settings/widget/D;)V

    iget-object v0, p0, Lcom/android/settings/location/LocationMode;->acC:Lcom/android/settings/widget/MiuiRadioButtonPreference;

    invoke-virtual {v0, p0}, Lcom/android/settings/widget/MiuiRadioButtonPreference;->aCa(Lcom/android/settings/widget/D;)V

    iget-object v0, p0, Lcom/android/settings/location/LocationMode;->acE:Lcom/android/settings/widget/MiuiRadioButtonPreference;

    invoke-virtual {v0, p0}, Lcom/android/settings/widget/MiuiRadioButtonPreference;->aCa(Lcom/android/settings/widget/D;)V

    invoke-virtual {p0}, Lcom/android/settings/location/LocationMode;->Sw()V

    return-object v1
.end method

.method private SR(Lcom/android/settings/widget/MiuiRadioButtonPreference;)V
    .locals 3

    const/4 v2, 0x1

    const/4 v1, 0x0

    if-nez p1, :cond_1

    iget-object v0, p0, Lcom/android/settings/location/LocationMode;->acD:Lcom/android/settings/widget/MiuiRadioButtonPreference;

    invoke-virtual {v0, v1}, Lcom/android/settings/widget/MiuiRadioButtonPreference;->setChecked(Z)V

    iget-object v0, p0, Lcom/android/settings/location/LocationMode;->acC:Lcom/android/settings/widget/MiuiRadioButtonPreference;

    invoke-virtual {v0, v1}, Lcom/android/settings/widget/MiuiRadioButtonPreference;->setChecked(Z)V

    iget-object v0, p0, Lcom/android/settings/location/LocationMode;->acE:Lcom/android/settings/widget/MiuiRadioButtonPreference;

    invoke-virtual {v0, v1}, Lcom/android/settings/widget/MiuiRadioButtonPreference;->setChecked(Z)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/settings/location/LocationMode;->acD:Lcom/android/settings/widget/MiuiRadioButtonPreference;

    if-ne p1, v0, :cond_2

    iget-object v0, p0, Lcom/android/settings/location/LocationMode;->acD:Lcom/android/settings/widget/MiuiRadioButtonPreference;

    invoke-virtual {v0, v2}, Lcom/android/settings/widget/MiuiRadioButtonPreference;->setChecked(Z)V

    iget-object v0, p0, Lcom/android/settings/location/LocationMode;->acC:Lcom/android/settings/widget/MiuiRadioButtonPreference;

    invoke-virtual {v0, v1}, Lcom/android/settings/widget/MiuiRadioButtonPreference;->setChecked(Z)V

    iget-object v0, p0, Lcom/android/settings/location/LocationMode;->acE:Lcom/android/settings/widget/MiuiRadioButtonPreference;

    invoke-virtual {v0, v1}, Lcom/android/settings/widget/MiuiRadioButtonPreference;->setChecked(Z)V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/android/settings/location/LocationMode;->acC:Lcom/android/settings/widget/MiuiRadioButtonPreference;

    if-ne p1, v0, :cond_3

    iget-object v0, p0, Lcom/android/settings/location/LocationMode;->acD:Lcom/android/settings/widget/MiuiRadioButtonPreference;

    invoke-virtual {v0, v1}, Lcom/android/settings/widget/MiuiRadioButtonPreference;->setChecked(Z)V

    iget-object v0, p0, Lcom/android/settings/location/LocationMode;->acC:Lcom/android/settings/widget/MiuiRadioButtonPreference;

    invoke-virtual {v0, v2}, Lcom/android/settings/widget/MiuiRadioButtonPreference;->setChecked(Z)V

    iget-object v0, p0, Lcom/android/settings/location/LocationMode;->acE:Lcom/android/settings/widget/MiuiRadioButtonPreference;

    invoke-virtual {v0, v1}, Lcom/android/settings/widget/MiuiRadioButtonPreference;->setChecked(Z)V

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/android/settings/location/LocationMode;->acE:Lcom/android/settings/widget/MiuiRadioButtonPreference;

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/location/LocationMode;->acD:Lcom/android/settings/widget/MiuiRadioButtonPreference;

    invoke-virtual {v0, v1}, Lcom/android/settings/widget/MiuiRadioButtonPreference;->setChecked(Z)V

    iget-object v0, p0, Lcom/android/settings/location/LocationMode;->acC:Lcom/android/settings/widget/MiuiRadioButtonPreference;

    invoke-virtual {v0, v1}, Lcom/android/settings/widget/MiuiRadioButtonPreference;->setChecked(Z)V

    iget-object v0, p0, Lcom/android/settings/location/LocationMode;->acE:Lcom/android/settings/widget/MiuiRadioButtonPreference;

    invoke-virtual {v0, v2}, Lcom/android/settings/widget/MiuiRadioButtonPreference;->setChecked(Z)V

    goto :goto_0
.end method


# virtual methods
.method public Sk(IZ)V
    .locals 2

    const/4 v0, 0x0

    packed-switch p1, :pswitch_data_0

    :goto_0
    if-eqz p1, :cond_0

    xor-int/lit8 v0, p2, 0x1

    :cond_0
    iget-object v1, p0, Lcom/android/settings/location/LocationMode;->acD:Lcom/android/settings/widget/MiuiRadioButtonPreference;

    invoke-virtual {v1, v0}, Lcom/android/settings/widget/MiuiRadioButtonPreference;->setEnabled(Z)V

    iget-object v1, p0, Lcom/android/settings/location/LocationMode;->acC:Lcom/android/settings/widget/MiuiRadioButtonPreference;

    invoke-virtual {v1, v0}, Lcom/android/settings/widget/MiuiRadioButtonPreference;->setEnabled(Z)V

    iget-object v1, p0, Lcom/android/settings/location/LocationMode;->acE:Lcom/android/settings/widget/MiuiRadioButtonPreference;

    invoke-virtual {v1, v0}, Lcom/android/settings/widget/MiuiRadioButtonPreference;->setEnabled(Z)V

    return-void

    :pswitch_0
    const/4 v1, 0x0

    invoke-direct {p0, v1}, Lcom/android/settings/location/LocationMode;->SR(Lcom/android/settings/widget/MiuiRadioButtonPreference;)V

    goto :goto_0

    :pswitch_1
    iget-object v1, p0, Lcom/android/settings/location/LocationMode;->acE:Lcom/android/settings/widget/MiuiRadioButtonPreference;

    invoke-direct {p0, v1}, Lcom/android/settings/location/LocationMode;->SR(Lcom/android/settings/widget/MiuiRadioButtonPreference;)V

    goto :goto_0

    :pswitch_2
    iget-object v1, p0, Lcom/android/settings/location/LocationMode;->acC:Lcom/android/settings/widget/MiuiRadioButtonPreference;

    invoke-direct {p0, v1}, Lcom/android/settings/location/LocationMode;->SR(Lcom/android/settings/widget/MiuiRadioButtonPreference;)V

    goto :goto_0

    :pswitch_3
    iget-object v1, p0, Lcom/android/settings/location/LocationMode;->acD:Lcom/android/settings/widget/MiuiRadioButtonPreference;

    invoke-direct {p0, v1}, Lcom/android/settings/location/LocationMode;->SR(Lcom/android/settings/widget/MiuiRadioButtonPreference;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public aq()I
    .locals 1

    const v0, 0x7f12081d

    return v0
.end method

.method public getMetricsCategory()I
    .locals 1

    const/16 v0, 0x40

    return v0
.end method

.method public ns(Lcom/android/settings/widget/MiuiRadioButtonPreference;)V
    .locals 2

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/android/settings/location/LocationMode;->acD:Lcom/android/settings/widget/MiuiRadioButtonPreference;

    if-ne p1, v1, :cond_1

    const/4 v0, 0x3

    :cond_0
    :goto_0
    invoke-virtual {p0, v0}, Lcom/android/settings/location/LocationMode;->Sx(I)V

    return-void

    :cond_1
    iget-object v1, p0, Lcom/android/settings/location/LocationMode;->acC:Lcom/android/settings/widget/MiuiRadioButtonPreference;

    if-ne p1, v1, :cond_2

    const/4 v0, 0x2

    goto :goto_0

    :cond_2
    iget-object v1, p0, Lcom/android/settings/location/LocationMode;->acE:Lcom/android/settings/widget/MiuiRadioButtonPreference;

    if-ne p1, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public onPause()V
    .locals 0

    invoke-super {p0}, Lcom/android/settings/location/LocationSettingsBase;->onPause()V

    return-void
.end method

.method public onResume()V
    .locals 0

    invoke-super {p0}, Lcom/android/settings/location/LocationSettingsBase;->onResume()V

    invoke-direct {p0}, Lcom/android/settings/location/LocationMode;->SQ()Landroid/preference/PreferenceScreen;

    return-void
.end method
