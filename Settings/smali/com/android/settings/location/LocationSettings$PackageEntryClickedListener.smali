.class Lcom/android/settings/location/LocationSettings$PackageEntryClickedListener;
.super Ljava/lang/Object;
.source "LocationSettings.java"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceClickListener;


# instance fields
.field private acs:Landroid/os/UserHandle;

.field final synthetic act:Lcom/android/settings/location/LocationSettings;

.field private mPackage:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/android/settings/location/LocationSettings;Ljava/lang/String;Landroid/os/UserHandle;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/location/LocationSettings$PackageEntryClickedListener;->act:Lcom/android/settings/location/LocationSettings;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/android/settings/location/LocationSettings$PackageEntryClickedListener;->mPackage:Ljava/lang/String;

    iput-object p3, p0, Lcom/android/settings/location/LocationSettings$PackageEntryClickedListener;->acs:Landroid/os/UserHandle;

    return-void
.end method


# virtual methods
.method public onPreferenceClick(Landroid/preference/Preference;)Z
    .locals 7

    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    const-string/jumbo v0, "package"

    iget-object v1, p0, Lcom/android/settings/location/LocationSettings$PackageEntryClickedListener;->mPackage:Ljava/lang/String;

    invoke-virtual {v3, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/settings/location/LocationSettings$PackageEntryClickedListener;->act:Lcom/android/settings/location/LocationSettings;

    invoke-virtual {v0}, Lcom/android/settings/location/LocationSettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/android/settings/bL;

    iget-object v1, p0, Lcom/android/settings/location/LocationSettings$PackageEntryClickedListener;->act:Lcom/android/settings/location/LocationSettings;

    const-class v2, Lcom/android/settings/applications/InstalledAppDetails;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    iget-object v6, p0, Lcom/android/settings/location/LocationSettings$PackageEntryClickedListener;->acs:Landroid/os/UserHandle;

    const v4, 0x7f120162

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v6}, Lcom/android/settings/bL;->bMG(Landroid/app/Fragment;Ljava/lang/String;Landroid/os/Bundle;ILjava/lang/CharSequence;Landroid/os/UserHandle;)V

    const/4 v0, 0x1

    return v0
.end method
