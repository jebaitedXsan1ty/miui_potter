.class Lcom/android/settings/location/RecentAppFragment$PackageEntryClickedListener;
.super Ljava/lang/Object;
.source "RecentAppFragment.java"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceClickListener;


# instance fields
.field final synthetic acA:Lcom/android/settings/location/RecentAppFragment;

.field private acz:Landroid/os/UserHandle;

.field private mPackage:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/android/settings/location/RecentAppFragment;Ljava/lang/String;Landroid/os/UserHandle;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/location/RecentAppFragment$PackageEntryClickedListener;->acA:Lcom/android/settings/location/RecentAppFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/android/settings/location/RecentAppFragment$PackageEntryClickedListener;->mPackage:Ljava/lang/String;

    iput-object p3, p0, Lcom/android/settings/location/RecentAppFragment$PackageEntryClickedListener;->acz:Landroid/os/UserHandle;

    return-void
.end method


# virtual methods
.method public onPreferenceClick(Landroid/preference/Preference;)Z
    .locals 7

    const/4 v4, 0x0

    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    const-string/jumbo v0, "package"

    iget-object v1, p0, Lcom/android/settings/location/RecentAppFragment$PackageEntryClickedListener;->mPackage:Ljava/lang/String;

    invoke-virtual {v2, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/settings/location/RecentAppFragment$PackageEntryClickedListener;->acA:Lcom/android/settings/location/RecentAppFragment;

    invoke-virtual {v0}, Lcom/android/settings/location/RecentAppFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const-class v1, Lcom/android/settings/applications/InstalledAppDetailsFragment;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    const v3, 0x7f120162

    const/4 v6, 0x0

    move-object v5, v4

    invoke-static/range {v0 .. v6}, Lcom/android/settings/dc;->bYv(Landroid/app/Activity;Ljava/lang/String;Landroid/os/Bundle;ILjava/lang/CharSequence;Landroid/app/Fragment;I)V

    const/4 v0, 0x1

    return v0
.end method
