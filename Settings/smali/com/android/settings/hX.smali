.class final Lcom/android/settings/hX;
.super Ljava/lang/Object;
.source "CarrierCustomEditFragment.java"

# interfaces
.implements Landroid/text/TextWatcher;


# instance fields
.field final synthetic cnQ:Lcom/android/settings/CarrierCustomEditFragment;

.field final synthetic cnR:I


# direct methods
.method constructor <init>(Lcom/android/settings/CarrierCustomEditFragment;I)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/hX;->cnQ:Lcom/android/settings/CarrierCustomEditFragment;

    iput p2, p0, Lcom/android/settings/hX;->cnR:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 4

    iget-object v0, p0, Lcom/android/settings/hX;->cnQ:Lcom/android/settings/CarrierCustomEditFragment;

    invoke-virtual {v0}, Lcom/android/settings/CarrierCustomEditFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "status_bar_custom_carrier"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/android/settings/hX;->cnR:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/android/settings/hX;->cnQ:Lcom/android/settings/CarrierCustomEditFragment;

    invoke-static {v2}, Lcom/android/settings/CarrierCustomEditFragment;->bFa(Lcom/android/settings/CarrierCustomEditFragment;)[Landroid/widget/EditText;

    move-result-object v2

    iget v3, p0, Lcom/android/settings/hX;->cnR:I

    aget-object v2, v2, v3

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-interface {v2}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$System;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    return-void
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    return-void
.end method
