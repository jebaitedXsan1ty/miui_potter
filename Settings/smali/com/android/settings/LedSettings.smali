.class public Lcom/android/settings/LedSettings;
.super Lcom/android/settings/MiuiSettingsPreferenceFragment;
.source "LedSettings.java"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceChangeListener;


# static fields
.field private static final bzd:Z

.field private static final bze:Z

.field private static final bzf:Z


# instance fields
.field private bzg:Landroid/preference/CheckBoxPreference;

.field private bzh:Landroid/preference/ListPreference;

.field private bzi:Landroid/preference/ListPreference;

.field private bzj:Landroid/preference/CheckBoxPreference;

.field private bzk:Landroid/preference/ListPreference;

.field private bzl:Landroid/preference/ListPreference;

.field private bzm:Landroid/preference/ListPreference;

.field private bzn:Landroid/preference/ListPreference;

.field private bzo:Landroid/preference/ListPreference;

.field private bzp:Landroid/preference/CheckBoxPreference;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/4 v2, 0x0

    const-string/jumbo v0, "support_led_color"

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lmiui/util/FeatureParser;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    sput-boolean v0, Lcom/android/settings/LedSettings;->bze:Z

    const-string/jumbo v0, "support_led_freq"

    invoke-static {v0, v2}, Lmiui/util/FeatureParser;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    sput-boolean v0, Lcom/android/settings/LedSettings;->bzf:Z

    const-string/jumbo v0, "front_fingerprint_sensor"

    invoke-static {v0, v2}, Lmiui/util/FeatureParser;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    sput-boolean v0, Lcom/android/settings/LedSettings;->bzd:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/MiuiSettingsPreferenceFragment;-><init>()V

    return-void
.end method

.method private bmM()V
    .locals 3

    const/4 v2, 0x0

    sget-boolean v0, Lcom/android/settings/LedSettings;->bze:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/android/settings/LedSettings;->bzh:Landroid/preference/ListPreference;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/android/settings/LedSettings;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/LedSettings;->bzh:Landroid/preference/ListPreference;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    iput-object v2, p0, Lcom/android/settings/LedSettings;->bzh:Landroid/preference/ListPreference;

    :cond_0
    iget-object v0, p0, Lcom/android/settings/LedSettings;->bzl:Landroid/preference/ListPreference;

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/android/settings/LedSettings;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/LedSettings;->bzl:Landroid/preference/ListPreference;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    iput-object v2, p0, Lcom/android/settings/LedSettings;->bzl:Landroid/preference/ListPreference;

    :cond_1
    iget-object v0, p0, Lcom/android/settings/LedSettings;->bzn:Landroid/preference/ListPreference;

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lcom/android/settings/LedSettings;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/LedSettings;->bzn:Landroid/preference/ListPreference;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    iput-object v2, p0, Lcom/android/settings/LedSettings;->bzn:Landroid/preference/ListPreference;

    :cond_2
    sget-boolean v0, Lcom/android/settings/LedSettings;->bzf:Z

    if-nez v0, :cond_5

    iget-object v0, p0, Lcom/android/settings/LedSettings;->bzi:Landroid/preference/ListPreference;

    if-eqz v0, :cond_3

    invoke-virtual {p0}, Lcom/android/settings/LedSettings;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/LedSettings;->bzi:Landroid/preference/ListPreference;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    iput-object v2, p0, Lcom/android/settings/LedSettings;->bzi:Landroid/preference/ListPreference;

    :cond_3
    iget-object v0, p0, Lcom/android/settings/LedSettings;->bzm:Landroid/preference/ListPreference;

    if-eqz v0, :cond_4

    invoke-virtual {p0}, Lcom/android/settings/LedSettings;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/LedSettings;->bzm:Landroid/preference/ListPreference;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    iput-object v2, p0, Lcom/android/settings/LedSettings;->bzm:Landroid/preference/ListPreference;

    :cond_4
    iget-object v0, p0, Lcom/android/settings/LedSettings;->bzo:Landroid/preference/ListPreference;

    if-eqz v0, :cond_5

    invoke-virtual {p0}, Lcom/android/settings/LedSettings;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/LedSettings;->bzo:Landroid/preference/ListPreference;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    iput-object v2, p0, Lcom/android/settings/LedSettings;->bzo:Landroid/preference/ListPreference;

    :cond_5
    invoke-virtual {p0}, Lcom/android/settings/LedSettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/android/settings/aq;->bqw(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_9

    iget-object v0, p0, Lcom/android/settings/LedSettings;->bzl:Landroid/preference/ListPreference;

    if-eqz v0, :cond_6

    invoke-virtual {p0}, Lcom/android/settings/LedSettings;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/LedSettings;->bzl:Landroid/preference/ListPreference;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    iput-object v2, p0, Lcom/android/settings/LedSettings;->bzl:Landroid/preference/ListPreference;

    :cond_6
    iget-object v0, p0, Lcom/android/settings/LedSettings;->bzm:Landroid/preference/ListPreference;

    if-eqz v0, :cond_7

    invoke-virtual {p0}, Lcom/android/settings/LedSettings;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/LedSettings;->bzm:Landroid/preference/ListPreference;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    iput-object v2, p0, Lcom/android/settings/LedSettings;->bzm:Landroid/preference/ListPreference;

    :cond_7
    iget-object v0, p0, Lcom/android/settings/LedSettings;->bzn:Landroid/preference/ListPreference;

    if-eqz v0, :cond_8

    invoke-virtual {p0}, Lcom/android/settings/LedSettings;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/LedSettings;->bzn:Landroid/preference/ListPreference;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    iput-object v2, p0, Lcom/android/settings/LedSettings;->bzn:Landroid/preference/ListPreference;

    :cond_8
    iget-object v0, p0, Lcom/android/settings/LedSettings;->bzo:Landroid/preference/ListPreference;

    if-eqz v0, :cond_9

    invoke-virtual {p0}, Lcom/android/settings/LedSettings;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/LedSettings;->bzo:Landroid/preference/ListPreference;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    iput-object v2, p0, Lcom/android/settings/LedSettings;->bzo:Landroid/preference/ListPreference;

    :cond_9
    return-void
.end method

.method private bmN(Landroid/preference/ListPreference;IZ)I
    .locals 5

    const/4 v0, 0x0

    const/4 v1, -0x1

    invoke-virtual {p1}, Landroid/preference/ListPreference;->getEntryValues()[Ljava/lang/CharSequence;

    move-result-object v3

    array-length v4, v3

    move v2, v0

    move v0, v1

    :goto_0
    if-ge v2, v4, :cond_0

    aget-object v1, v3, v2

    add-int/lit8 v0, v0, 0x1

    if-eqz p3, :cond_1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v1

    :goto_1
    if-ne p2, v1, :cond_2

    invoke-virtual {p1}, Landroid/preference/ListPreference;->getEntries()[Ljava/lang/CharSequence;

    move-result-object v1

    aget-object v1, v1, v0

    invoke-virtual {p1, v1}, Landroid/preference/ListPreference;->setSummary(Ljava/lang/CharSequence;)V

    :cond_0
    return v0

    :cond_1
    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    goto :goto_1

    :cond_2
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_0
.end method

.method private bmO(Landroid/preference/ListPreference;IZ)V
    .locals 2

    invoke-direct {p0, p1, p2, p3}, Lcom/android/settings/LedSettings;->bmN(Landroid/preference/ListPreference;IZ)I

    move-result v0

    const/4 v1, -0x1

    if-le v0, v1, :cond_0

    invoke-virtual {p1, v0}, Landroid/preference/ListPreference;->setValueIndex(I)V

    :cond_0
    return-void
.end method


# virtual methods
.method public cC()Ljava/lang/String;
    .locals 1

    const-class v0, Lcom/android/settings/LedSettings;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 7

    const/4 v4, 0x0

    const/4 v2, 0x0

    const/4 v1, 0x1

    invoke-super {p0, p1}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->onCreate(Landroid/os/Bundle;)V

    const v0, 0x7f150075

    invoke-virtual {p0, v0}, Lcom/android/settings/LedSettings;->addPreferencesFromResource(I)V

    invoke-virtual {p0}, Lcom/android/settings/LedSettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const v3, 0x7f120908

    invoke-virtual {v0, v3}, Landroid/app/Activity;->setTitle(I)V

    const-string/jumbo v0, "pref_button_light"

    invoke-virtual {p0, v0}, Lcom/android/settings/LedSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    iput-object v0, p0, Lcom/android/settings/LedSettings;->bzj:Landroid/preference/CheckBoxPreference;

    const-string/jumbo v0, "button_light_timeout"

    invoke-virtual {p0, v0}, Lcom/android/settings/LedSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/ListPreference;

    iput-object v0, p0, Lcom/android/settings/LedSettings;->bzk:Landroid/preference/ListPreference;

    const-string/jumbo v0, "support_button_light"

    invoke-static {v0, v2}, Lmiui/util/FeatureParser;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/android/settings/LedSettings;->bzj:Landroid/preference/CheckBoxPreference;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/android/settings/LedSettings;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    iget-object v3, p0, Lcom/android/settings/LedSettings;->bzj:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, v3}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    iput-object v4, p0, Lcom/android/settings/LedSettings;->bzj:Landroid/preference/CheckBoxPreference;

    :cond_0
    iget-object v0, p0, Lcom/android/settings/LedSettings;->bzk:Landroid/preference/ListPreference;

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/android/settings/LedSettings;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    iget-object v3, p0, Lcom/android/settings/LedSettings;->bzk:Landroid/preference/ListPreference;

    invoke-virtual {v0, v3}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    iput-object v4, p0, Lcom/android/settings/LedSettings;->bzk:Landroid/preference/ListPreference;

    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/android/settings/LedSettings;->bzk:Landroid/preference/ListPreference;

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lcom/android/settings/LedSettings;->bWB()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v3, "screen_buttons_timeout"

    const/16 v4, 0x1388

    invoke-static {v0, v3, v4}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    iget-object v3, p0, Lcom/android/settings/LedSettings;->bzk:Landroid/preference/ListPreference;

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Landroid/preference/ListPreference;->setValue(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/settings/LedSettings;->bzk:Landroid/preference/ListPreference;

    iget-object v3, p0, Lcom/android/settings/LedSettings;->bzk:Landroid/preference/ListPreference;

    invoke-virtual {v3}, Landroid/preference/ListPreference;->getEntry()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/preference/ListPreference;->setSummary(Ljava/lang/CharSequence;)V

    :cond_2
    iget-object v0, p0, Lcom/android/settings/LedSettings;->bzj:Landroid/preference/CheckBoxPreference;

    if-eqz v0, :cond_3

    sget-boolean v0, Lcom/android/settings/LedSettings;->bzd:Z

    if-eqz v0, :cond_6

    const-string/jumbo v0, ""

    :goto_1
    invoke-virtual {p0}, Lcom/android/settings/LedSettings;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    new-array v4, v1, [Ljava/lang/Object;

    aput-object v0, v4, v2

    const v0, 0x7f120cdb

    invoke-virtual {v3, v0, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iget-object v3, p0, Lcom/android/settings/LedSettings;->bzj:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v3, v0}, Landroid/preference/CheckBoxPreference;->setSummary(Ljava/lang/CharSequence;)V

    iget-object v3, p0, Lcom/android/settings/LedSettings;->bzj:Landroid/preference/CheckBoxPreference;

    invoke-virtual {p0}, Lcom/android/settings/LedSettings;->bWB()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v4, "screen_buttons_turn_on"

    invoke-static {v0, v4, v1}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    if-ne v0, v1, :cond_7

    move v0, v1

    :goto_2
    invoke-virtual {v3, v0}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    :cond_3
    const-string/jumbo v0, "notification_pulse"

    invoke-virtual {p0, v0}, Lcom/android/settings/LedSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    iput-object v0, p0, Lcom/android/settings/LedSettings;->bzp:Landroid/preference/CheckBoxPreference;

    iget-object v0, p0, Lcom/android/settings/LedSettings;->bzp:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, p0}, Landroid/preference/CheckBoxPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    const-string/jumbo v0, "breathing_light_color"

    invoke-virtual {p0, v0}, Lcom/android/settings/LedSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/ListPreference;

    iput-object v0, p0, Lcom/android/settings/LedSettings;->bzh:Landroid/preference/ListPreference;

    iget-object v0, p0, Lcom/android/settings/LedSettings;->bzh:Landroid/preference/ListPreference;

    invoke-virtual {v0, p0}, Landroid/preference/ListPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    const-string/jumbo v0, "breathing_light_freq"

    invoke-virtual {p0, v0}, Lcom/android/settings/LedSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/ListPreference;

    iput-object v0, p0, Lcom/android/settings/LedSettings;->bzi:Landroid/preference/ListPreference;

    iget-object v0, p0, Lcom/android/settings/LedSettings;->bzi:Landroid/preference/ListPreference;

    invoke-virtual {v0, p0}, Landroid/preference/ListPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    const-string/jumbo v0, "call_breathing_light_color"

    invoke-virtual {p0, v0}, Lcom/android/settings/LedSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/ListPreference;

    iput-object v0, p0, Lcom/android/settings/LedSettings;->bzl:Landroid/preference/ListPreference;

    iget-object v0, p0, Lcom/android/settings/LedSettings;->bzl:Landroid/preference/ListPreference;

    invoke-virtual {v0, p0}, Landroid/preference/ListPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    const-string/jumbo v0, "call_breathing_light_freq"

    invoke-virtual {p0, v0}, Lcom/android/settings/LedSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/ListPreference;

    iput-object v0, p0, Lcom/android/settings/LedSettings;->bzm:Landroid/preference/ListPreference;

    iget-object v0, p0, Lcom/android/settings/LedSettings;->bzm:Landroid/preference/ListPreference;

    invoke-virtual {v0, p0}, Landroid/preference/ListPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    const-string/jumbo v0, "mms_breathing_light_color"

    invoke-virtual {p0, v0}, Lcom/android/settings/LedSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/ListPreference;

    iput-object v0, p0, Lcom/android/settings/LedSettings;->bzn:Landroid/preference/ListPreference;

    iget-object v0, p0, Lcom/android/settings/LedSettings;->bzn:Landroid/preference/ListPreference;

    invoke-virtual {v0, p0}, Landroid/preference/ListPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    const-string/jumbo v0, "mms_breathing_light_freq"

    invoke-virtual {p0, v0}, Lcom/android/settings/LedSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/ListPreference;

    iput-object v0, p0, Lcom/android/settings/LedSettings;->bzo:Landroid/preference/ListPreference;

    iget-object v0, p0, Lcom/android/settings/LedSettings;->bzo:Landroid/preference/ListPreference;

    invoke-virtual {v0, p0}, Landroid/preference/ListPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    invoke-virtual {p0}, Lcom/android/settings/LedSettings;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v3, 0x11060002

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {p0}, Lcom/android/settings/LedSettings;->bWB()Landroid/content/ContentResolver;

    move-result-object v3

    const-string/jumbo v4, "breathing_light_color"

    invoke-static {v3, v4, v0}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v3

    invoke-virtual {p0}, Lcom/android/settings/LedSettings;->bWB()Landroid/content/ContentResolver;

    move-result-object v4

    const-string/jumbo v5, "call_breathing_light_color"

    invoke-static {v4, v5, v0}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v4

    invoke-virtual {p0}, Lcom/android/settings/LedSettings;->bWB()Landroid/content/ContentResolver;

    move-result-object v5

    const-string/jumbo v6, "mms_breathing_light_color"

    invoke-static {v5, v6, v0}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    iget-object v5, p0, Lcom/android/settings/LedSettings;->bzh:Landroid/preference/ListPreference;

    invoke-direct {p0, v5, v3, v1}, Lcom/android/settings/LedSettings;->bmO(Landroid/preference/ListPreference;IZ)V

    iget-object v3, p0, Lcom/android/settings/LedSettings;->bzl:Landroid/preference/ListPreference;

    invoke-direct {p0, v3, v4, v1}, Lcom/android/settings/LedSettings;->bmO(Landroid/preference/ListPreference;IZ)V

    iget-object v3, p0, Lcom/android/settings/LedSettings;->bzn:Landroid/preference/ListPreference;

    invoke-direct {p0, v3, v0, v1}, Lcom/android/settings/LedSettings;->bmO(Landroid/preference/ListPreference;IZ)V

    invoke-virtual {p0}, Lcom/android/settings/LedSettings;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v3, 0x1107000b

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    invoke-virtual {p0}, Lcom/android/settings/LedSettings;->bWB()Landroid/content/ContentResolver;

    move-result-object v3

    const-string/jumbo v4, "breathing_light_freq"

    invoke-static {v3, v4, v0}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v3

    invoke-virtual {p0}, Lcom/android/settings/LedSettings;->bWB()Landroid/content/ContentResolver;

    move-result-object v4

    const-string/jumbo v5, "call_breathing_light_freq"

    invoke-static {v4, v5, v0}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v4

    invoke-virtual {p0}, Lcom/android/settings/LedSettings;->bWB()Landroid/content/ContentResolver;

    move-result-object v5

    const-string/jumbo v6, "mms_breathing_light_freq"

    invoke-static {v5, v6, v0}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    iget-object v5, p0, Lcom/android/settings/LedSettings;->bzi:Landroid/preference/ListPreference;

    invoke-direct {p0, v5, v3, v2}, Lcom/android/settings/LedSettings;->bmO(Landroid/preference/ListPreference;IZ)V

    iget-object v3, p0, Lcom/android/settings/LedSettings;->bzm:Landroid/preference/ListPreference;

    invoke-direct {p0, v3, v4, v2}, Lcom/android/settings/LedSettings;->bmO(Landroid/preference/ListPreference;IZ)V

    iget-object v3, p0, Lcom/android/settings/LedSettings;->bzo:Landroid/preference/ListPreference;

    invoke-direct {p0, v3, v0, v2}, Lcom/android/settings/LedSettings;->bmO(Landroid/preference/ListPreference;IZ)V

    iget-object v3, p0, Lcom/android/settings/LedSettings;->bzp:Landroid/preference/CheckBoxPreference;

    invoke-virtual {p0}, Lcom/android/settings/LedSettings;->bWB()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v4, "notification_light_pulse"

    invoke-static {v0, v4, v1}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    if-ne v0, v1, :cond_8

    move v0, v1

    :goto_3
    invoke-virtual {v3, v0}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    const-string/jumbo v0, "battery_light"

    invoke-virtual {p0, v0}, Lcom/android/settings/LedSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    iput-object v0, p0, Lcom/android/settings/LedSettings;->bzg:Landroid/preference/CheckBoxPreference;

    iget-object v0, p0, Lcom/android/settings/LedSettings;->bzg:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, p0}, Landroid/preference/CheckBoxPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    iget-object v0, p0, Lcom/android/settings/LedSettings;->bzg:Landroid/preference/CheckBoxPreference;

    invoke-virtual {p0}, Lcom/android/settings/LedSettings;->bWB()Landroid/content/ContentResolver;

    move-result-object v3

    const-string/jumbo v4, "battery_light_turn_on"

    invoke-static {v3, v4, v1}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v3

    if-ne v3, v1, :cond_9

    :goto_4
    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    invoke-direct {p0}, Lcom/android/settings/LedSettings;->bmM()V

    return-void

    :cond_4
    iget-object v0, p0, Lcom/android/settings/LedSettings;->bzj:Landroid/preference/CheckBoxPreference;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/android/settings/LedSettings;->bzj:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, p0}, Landroid/preference/CheckBoxPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    :cond_5
    iget-object v0, p0, Lcom/android/settings/LedSettings;->bzk:Landroid/preference/ListPreference;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/settings/LedSettings;->bzk:Landroid/preference/ListPreference;

    invoke-virtual {v0, p0}, Landroid/preference/ListPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    goto/16 :goto_0

    :cond_6
    invoke-virtual {p0}, Lcom/android/settings/LedSettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v3, 0x7f120cdc

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_1

    :cond_7
    move v0, v2

    goto/16 :goto_2

    :cond_8
    move v0, v2

    goto :goto_3

    :cond_9
    move v1, v2

    goto :goto_4
.end method

.method public onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 6

    const/4 v2, 0x0

    const/4 v1, 0x1

    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v0

    const/4 v3, -0x1

    const-string/jumbo v4, "breathing_light_color"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    check-cast p2, Ljava/lang/String;

    invoke-static {p2}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v2

    :try_start_0
    invoke-virtual {p0}, Lcom/android/settings/LedSettings;->bWB()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v3, "breathing_light_color"

    invoke-static {v0, v3, v2}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    move v0, v2

    move v2, v1

    :goto_1
    instance-of v3, p1, Landroid/preference/ListPreference;

    if-eqz v3, :cond_0

    check-cast p1, Landroid/preference/ListPreference;

    invoke-direct {p0, p1, v0, v2}, Lcom/android/settings/LedSettings;->bmN(Landroid/preference/ListPreference;IZ)I

    :cond_0
    return v1

    :catch_0
    move-exception v0

    const-string/jumbo v3, "TrackballSettings"

    const-string/jumbo v4, "could not persist breathing light color settings"

    invoke-static {v3, v4, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    move v0, v2

    move v2, v1

    goto :goto_1

    :cond_1
    const-string/jumbo v4, "breathing_light_freq"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    check-cast p2, Ljava/lang/String;

    invoke-static {p2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    :try_start_1
    invoke-virtual {p0}, Lcom/android/settings/LedSettings;->bWB()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v4, "breathing_light_freq"

    invoke-static {v0, v4, v3}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_1

    :cond_2
    :goto_2
    move v0, v3

    goto :goto_1

    :catch_1
    move-exception v0

    const-string/jumbo v4, "TrackballSettings"

    const-string/jumbo v5, "could not persist breathing light frequency settings"

    invoke-static {v4, v5, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    move v0, v3

    goto :goto_1

    :cond_3
    const-string/jumbo v4, "call_breathing_light_color"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    check-cast p2, Ljava/lang/String;

    invoke-static {p2}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v2

    :try_start_2
    invoke-virtual {p0}, Lcom/android/settings/LedSettings;->bWB()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v3, "call_breathing_light_color"

    invoke-static {v0, v3, v2}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z
    :try_end_2
    .catch Ljava/lang/NumberFormatException; {:try_start_2 .. :try_end_2} :catch_2

    goto :goto_0

    :catch_2
    move-exception v0

    const-string/jumbo v3, "TrackballSettings"

    const-string/jumbo v4, "could not persist breathing light color settings"

    invoke-static {v3, v4, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    move v0, v2

    move v2, v1

    goto :goto_1

    :cond_4
    const-string/jumbo v4, "call_breathing_light_freq"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    check-cast p2, Ljava/lang/String;

    invoke-static {p2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    :try_start_3
    invoke-virtual {p0}, Lcom/android/settings/LedSettings;->bWB()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v4, "call_breathing_light_freq"

    invoke-static {v0, v4, v3}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z
    :try_end_3
    .catch Ljava/lang/NumberFormatException; {:try_start_3 .. :try_end_3} :catch_3

    goto :goto_2

    :catch_3
    move-exception v0

    const-string/jumbo v4, "TrackballSettings"

    const-string/jumbo v5, "could not persist breathing light frequency settings"

    invoke-static {v4, v5, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    move v0, v3

    goto/16 :goto_1

    :cond_5
    const-string/jumbo v4, "mms_breathing_light_color"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_6

    check-cast p2, Ljava/lang/String;

    invoke-static {p2}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v2

    :try_start_4
    invoke-virtual {p0}, Lcom/android/settings/LedSettings;->bWB()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v3, "mms_breathing_light_color"

    invoke-static {v0, v3, v2}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z
    :try_end_4
    .catch Ljava/lang/NumberFormatException; {:try_start_4 .. :try_end_4} :catch_4

    goto/16 :goto_0

    :catch_4
    move-exception v0

    const-string/jumbo v3, "TrackballSettings"

    const-string/jumbo v4, "could not persist breathing light color settings"

    invoke-static {v3, v4, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    move v0, v2

    move v2, v1

    goto/16 :goto_1

    :cond_6
    const-string/jumbo v4, "mms_breathing_light_freq"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_7

    check-cast p2, Ljava/lang/String;

    invoke-static {p2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    :try_start_5
    invoke-virtual {p0}, Lcom/android/settings/LedSettings;->bWB()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v4, "mms_breathing_light_freq"

    invoke-static {v0, v4, v3}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z
    :try_end_5
    .catch Ljava/lang/NumberFormatException; {:try_start_5 .. :try_end_5} :catch_5

    goto/16 :goto_2

    :catch_5
    move-exception v0

    const-string/jumbo v4, "TrackballSettings"

    const-string/jumbo v5, "could not persist breathing light frequency settings"

    invoke-static {v4, v5, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    move v0, v3

    goto/16 :goto_1

    :cond_7
    const-string/jumbo v4, "notification_pulse"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_9

    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-virtual {p0}, Lcom/android/settings/LedSettings;->bWB()Landroid/content/ContentResolver;

    move-result-object v4

    const-string/jumbo v5, "notification_light_pulse"

    if-eqz v0, :cond_8

    move v0, v1

    :goto_3
    invoke-static {v4, v5, v0}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    move v0, v3

    goto/16 :goto_1

    :cond_8
    move v0, v2

    goto :goto_3

    :cond_9
    const-string/jumbo v4, "battery_light"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_b

    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-virtual {p0}, Lcom/android/settings/LedSettings;->bWB()Landroid/content/ContentResolver;

    move-result-object v4

    const-string/jumbo v5, "battery_light_turn_on"

    if-eqz v0, :cond_a

    move v0, v1

    :goto_4
    invoke-static {v4, v5, v0}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    move v0, v3

    goto/16 :goto_1

    :cond_a
    move v0, v2

    goto :goto_4

    :cond_b
    const-string/jumbo v4, "pref_button_light"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_d

    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-virtual {p0}, Lcom/android/settings/LedSettings;->bWB()Landroid/content/ContentResolver;

    move-result-object v4

    const-string/jumbo v5, "screen_buttons_turn_on"

    if-eqz v0, :cond_c

    move v0, v1

    :goto_5
    invoke-static {v4, v5, v0}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    move v0, v3

    goto/16 :goto_1

    :cond_c
    move v0, v2

    goto :goto_5

    :cond_d
    const-string/jumbo v4, "button_light_timeout"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    check-cast p2, Ljava/lang/String;

    iget-object v0, p0, Lcom/android/settings/LedSettings;->bzk:Landroid/preference/ListPreference;

    invoke-virtual {v0, p2}, Landroid/preference/ListPreference;->setValue(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/settings/LedSettings;->bzk:Landroid/preference/ListPreference;

    iget-object v2, p0, Lcom/android/settings/LedSettings;->bzk:Landroid/preference/ListPreference;

    invoke-virtual {v2}, Landroid/preference/ListPreference;->getEntry()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/preference/ListPreference;->setSummary(Ljava/lang/CharSequence;)V

    invoke-virtual {p0}, Lcom/android/settings/LedSettings;->bWB()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v2, "screen_buttons_timeout"

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v0, v2, v3}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    return v1
.end method
