.class public Lcom/android/settings/SettingsFragment;
.super Lcom/android/settings/BasePreferenceFragment;
.source "SettingsFragment.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# static fields
.field private static bHe:Ljava/util/HashMap;


# instance fields
.field private bGK:Landroid/view/View;

.field private bGL:Ljava/util/List;

.field private bGM:Landroid/widget/TextView$OnEditorActionListener;

.field private bGN:Lcom/android/settings/i;

.field private bGO:Landroid/widget/ListView;

.field private volatile bGP:Z

.field private bGQ:Z

.field private bGR:Landroid/widget/ListView;

.field private bGS:Landroid/os/Handler;

.field private bGT:Lcom/android/settings/aS;

.field private bGU:Lmiui/view/SearchActionMode$Callback;

.field private bGV:Lcom/android/settings/aQ;

.field private bGW:Landroid/widget/EditText;

.field private bGX:Landroid/view/View;

.field private volatile bGY:Lcom/android/settings/search/SearchResult;

.field private bGZ:Ljava/util/List;

.field private bHa:Landroid/widget/ListView;

.field private bHb:Ljava/lang/String;

.field private bHc:Landroid/os/HandlerThread;

.field private bHd:Landroid/text/TextWatcher;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/android/settings/SettingsFragment;->bHe:Ljava/util/HashMap;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/android/settings/BasePreferenceFragment;-><init>()V

    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/android/settings/SettingsFragment;->bGL:Ljava/util/List;

    new-instance v0, Lcom/android/settings/go;

    invoke-direct {v0, p0}, Lcom/android/settings/go;-><init>(Lcom/android/settings/SettingsFragment;)V

    iput-object v0, p0, Lcom/android/settings/SettingsFragment;->bGU:Lmiui/view/SearchActionMode$Callback;

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/android/settings/SettingsFragment;->bGS:Landroid/os/Handler;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/settings/SettingsFragment;->bGQ:Z

    new-instance v0, Lcom/android/settings/gq;

    invoke-direct {v0, p0}, Lcom/android/settings/gq;-><init>(Lcom/android/settings/SettingsFragment;)V

    iput-object v0, p0, Lcom/android/settings/SettingsFragment;->bGM:Landroid/widget/TextView$OnEditorActionListener;

    new-instance v0, Lcom/android/settings/gr;

    invoke-direct {v0, p0}, Lcom/android/settings/gr;-><init>(Lcom/android/settings/SettingsFragment;)V

    iput-object v0, p0, Lcom/android/settings/SettingsFragment;->bHd:Landroid/text/TextWatcher;

    return-void
.end method

.method static synthetic bxA(Lcom/android/settings/SettingsFragment;)Landroid/os/Handler;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/SettingsFragment;->bGS:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic bxB(Lcom/android/settings/SettingsFragment;)Lcom/android/settings/aS;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/SettingsFragment;->bGT:Lcom/android/settings/aS;

    return-object v0
.end method

.method static synthetic bxC(Lcom/android/settings/SettingsFragment;)Lmiui/view/SearchActionMode$Callback;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/SettingsFragment;->bGU:Lmiui/view/SearchActionMode$Callback;

    return-object v0
.end method

.method static synthetic bxD(Lcom/android/settings/SettingsFragment;)Lcom/android/settings/aQ;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/SettingsFragment;->bGV:Lcom/android/settings/aQ;

    return-object v0
.end method

.method static synthetic bxE(Lcom/android/settings/SettingsFragment;)Landroid/widget/EditText;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/SettingsFragment;->bGW:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic bxF(Lcom/android/settings/SettingsFragment;)Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/SettingsFragment;->bGX:Landroid/view/View;

    return-object v0
.end method

.method static synthetic bxG(Lcom/android/settings/SettingsFragment;)Lcom/android/settings/search/SearchResult;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/SettingsFragment;->bGY:Lcom/android/settings/search/SearchResult;

    return-object v0
.end method

.method static synthetic bxH(Lcom/android/settings/SettingsFragment;)Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/SettingsFragment;->bGZ:Ljava/util/List;

    return-object v0
.end method

.method static synthetic bxI(Lcom/android/settings/SettingsFragment;)Landroid/widget/ListView;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/SettingsFragment;->bHa:Landroid/widget/ListView;

    return-object v0
.end method

.method static synthetic bxJ(Lcom/android/settings/SettingsFragment;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/SettingsFragment;->bHb:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic bxK(Lcom/android/settings/SettingsFragment;)Landroid/text/TextWatcher;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/SettingsFragment;->bHd:Landroid/text/TextWatcher;

    return-object v0
.end method

.method static synthetic bxL()Ljava/util/HashMap;
    .locals 1

    sget-object v0, Lcom/android/settings/SettingsFragment;->bHe:Ljava/util/HashMap;

    return-object v0
.end method

.method static synthetic bxM(Lcom/android/settings/SettingsFragment;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/android/settings/SettingsFragment;->bGP:Z

    return p1
.end method

.method static synthetic bxN(Lcom/android/settings/SettingsFragment;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/android/settings/SettingsFragment;->bGQ:Z

    return p1
.end method

.method static synthetic bxO(Lcom/android/settings/SettingsFragment;Landroid/widget/EditText;)Landroid/widget/EditText;
    .locals 0

    iput-object p1, p0, Lcom/android/settings/SettingsFragment;->bGW:Landroid/widget/EditText;

    return-object p1
.end method

.method static synthetic bxP(Lcom/android/settings/SettingsFragment;Ljava/util/List;)Ljava/util/List;
    .locals 0

    iput-object p1, p0, Lcom/android/settings/SettingsFragment;->bGZ:Ljava/util/List;

    return-object p1
.end method

.method static synthetic bxQ(Lcom/android/settings/SettingsFragment;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    iput-object p1, p0, Lcom/android/settings/SettingsFragment;->bHb:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic bxR(Lcom/android/settings/SettingsFragment;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/text/SpannableStringBuilder;
    .locals 1

    invoke-direct {p0, p1, p2, p3}, Lcom/android/settings/SettingsFragment;->bxq(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/text/SpannableStringBuilder;

    move-result-object v0

    return-object v0
.end method

.method static synthetic bxS()Ljava/lang/String;
    .locals 1

    invoke-static {}, Lcom/android/settings/SettingsFragment;->bxn()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic bxT(Lcom/android/settings/SettingsFragment;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/SettingsFragment;->bxk()V

    return-void
.end method

.method static synthetic bxU(Lcom/android/settings/SettingsFragment;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/SettingsFragment;->bxo()V

    return-void
.end method

.method static synthetic bxV(Lcom/android/settings/SettingsFragment;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/SettingsFragment;->bxp()V

    return-void
.end method

.method static synthetic bxW(Lcom/android/settings/SettingsFragment;Landroid/view/View;Lcom/android/settings/search/SearchResultItem;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/android/settings/SettingsFragment;->bxs(Landroid/view/View;Lcom/android/settings/search/SearchResultItem;)V

    return-void
.end method

.method static synthetic bxX(Lcom/android/settings/SettingsFragment;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/settings/SettingsFragment;->bxt(Ljava/lang/String;)V

    return-void
.end method

.method private bxk()V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/SettingsFragment;->bHc:Landroid/os/HandlerThread;

    if-nez v0, :cond_0

    new-instance v0, Landroid/os/HandlerThread;

    const-string/jumbo v1, "SettingsFragment-Search"

    invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/android/settings/SettingsFragment;->bHc:Landroid/os/HandlerThread;

    iget-object v0, p0, Lcom/android/settings/SettingsFragment;->bHc:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    new-instance v0, Lcom/android/settings/aQ;

    iget-object v1, p0, Lcom/android/settings/SettingsFragment;->bHc:Landroid/os/HandlerThread;

    invoke-virtual {v1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/android/settings/aQ;-><init>(Lcom/android/settings/SettingsFragment;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/android/settings/SettingsFragment;->bGV:Lcom/android/settings/aQ;

    :cond_0
    return-void
.end method

.method private static bxl(Ljava/lang/String;I)[I
    .locals 6

    const/4 v0, 0x0

    new-array v3, p1, [I

    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    invoke-virtual {p0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v4

    move v1, v0

    :goto_0
    array-length v2, v4

    if-ge v0, v2, :cond_1

    aget-char v2, v4, v0

    const/16 v5, 0x20

    if-ne v2, v5, :cond_0

    add-int/lit8 v2, v1, 0x1

    aput v0, v3, v1

    move v1, v2

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    return-object v3
.end method

.method private static bxn()Ljava/lang/String;
    .locals 2

    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    move-result-object v0

    const/4 v1, 0x0

    :try_start_0
    invoke-interface {v0}, Landroid/app/IActivityManager;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget-object v0, v0, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-virtual {v0}, Ljava/util/Locale;->toString()Ljava/lang/String;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    move-object v0, v1

    goto :goto_0
.end method

.method private bxo()V
    .locals 4

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/android/settings/SettingsFragment;->caL:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    iget-object v0, p0, Lcom/android/settings/SettingsFragment;->caL:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceActivity$Header;

    iget v0, v0, Landroid/preference/PreferenceActivity$Header;->titleRes:I

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/android/settings/SettingsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    iget-object v0, p0, Lcom/android/settings/SettingsFragment;->caL:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceActivity$Header;

    iget v0, v0, Landroid/preference/PreferenceActivity$Header;->titleRes:I

    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v0, p0, Lcom/android/settings/SettingsFragment;->caL:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceActivity$Header;

    iget v0, v0, Landroid/preference/PreferenceActivity$Header;->iconRes:I

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    if-eqz v0, :cond_0

    sget-object v3, Lcom/android/settings/SettingsFragment;->bHe:Ljava/util/HashMap;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v3, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    return-void
.end method

.method private bxp()V
    .locals 3

    invoke-virtual {p0}, Lcom/android/settings/SettingsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const-string/jumbo v1, "input_method"

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    iget-object v1, p0, Lcom/android/settings/SettingsFragment;->bHa:Landroid/widget/ListView;

    invoke-virtual {v1}, Landroid/widget/ListView;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    return-void
.end method

.method private bxq(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/text/SpannableStringBuilder;
    .locals 9

    const/16 v8, 0x21

    const/4 v0, 0x0

    const/high16 v7, -0x10000

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    const/4 v0, 0x0

    return-object v0

    :cond_1
    new-instance v3, Landroid/text/SpannableStringBuilder;

    invoke-direct {v3, p1}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    const-string/jumbo v1, "zh_CN"

    invoke-virtual {v1, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    const-string/jumbo v1, "zh_TW"

    invoke-virtual {v1, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    :cond_2
    const-string/jumbo v1, "\\s*|\t|\n"

    const-string/jumbo v2, ""

    invoke-virtual {p1, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    const/4 v4, -0x1

    if-eq v1, v4, :cond_6

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v4

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    sub-int v2, v4, v2

    invoke-static {p1, v2}, Lcom/android/settings/SettingsFragment;->bxl(Ljava/lang/String;I)[I

    move-result-object v4

    move v2, v1

    move v1, v0

    :goto_0
    array-length v5, v4

    if-ge v0, v5, :cond_5

    aget v5, v4, v0

    if-gt v5, v2, :cond_3

    add-int/lit8 v2, v2, 0x1

    :cond_3
    aget v5, v4, v0

    if-le v5, v2, :cond_4

    aget v5, v4, v0

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v6

    add-int/2addr v6, v2

    add-int/2addr v6, v1

    if-gt v5, v6, :cond_4

    add-int/lit8 v1, v1, 0x1

    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_5
    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v0

    add-int/2addr v0, v2

    add-int/2addr v0, v1

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v4

    if-gt v0, v4, :cond_6

    new-instance v0, Landroid/text/style/ForegroundColorSpan;

    invoke-direct {v0, v7}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v4

    add-int/2addr v4, v2

    add-int/2addr v1, v4

    invoke-virtual {v3, v0, v2, v1, v8}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    :cond_6
    return-object v3

    :cond_7
    invoke-virtual {p2}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v0

    const/16 v1, 0x10

    invoke-static {v0, v1}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;I)Ljava/util/regex/Pattern;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    :goto_1
    invoke-virtual {v0}, Ljava/util/regex/Matcher;->find()Z

    move-result v1

    if-eqz v1, :cond_6

    new-instance v1, Landroid/text/style/ForegroundColorSpan;

    invoke-direct {v1, v7}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    invoke-virtual {v0}, Ljava/util/regex/Matcher;->start()I

    move-result v2

    invoke-virtual {v0}, Ljava/util/regex/Matcher;->end()I

    move-result v4

    invoke-virtual {v3, v1, v2, v4, v8}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    goto :goto_1
.end method

.method private bxr()V
    .locals 5

    const/4 v4, 0x0

    iget-object v0, p0, Lcom/android/settings/SettingsFragment;->bGR:Landroid/widget/ListView;

    const-string/jumbo v1, "removable_hint"

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->findViewWithTag(Ljava/lang/Object;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0}, Lcom/android/settings/SettingsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-static {}, Lcom/android/settings/notify/c;->getInstance()Lcom/android/settings/notify/c;

    move-result-object v2

    invoke-virtual {v1}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/android/settings/notify/c;->aZw(Landroid/content/Context;)Lcom/android/settings/notify/d;

    move-result-object v2

    if-eqz v2, :cond_2

    if-nez v0, :cond_0

    invoke-virtual {v1}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0d0188

    invoke-virtual {v0, v1, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    const-string/jumbo v1, "removable_hint"

    invoke-virtual {v0, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    iget-object v1, p0, Lcom/android/settings/SettingsFragment;->bGR:Landroid/widget/ListView;

    invoke-virtual {v1, v0}, Landroid/widget/ListView;->addHeaderView(Landroid/view/View;)V

    :cond_0
    new-instance v1, Lcom/android/settings/gt;

    invoke-direct {v1, p0, v2}, Lcom/android/settings/gt;-><init>(Lcom/android/settings/SettingsFragment;Lcom/android/settings/notify/d;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v1, 0x7f0a00e5

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    const v3, 0x7f120044

    invoke-virtual {p0, v3}, Lcom/android/settings/SettingsFragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    new-instance v3, Lcom/android/settings/gu;

    invoke-direct {v3, p0, v2}, Lcom/android/settings/gu;-><init>(Lcom/android/settings/SettingsFragment;Lcom/android/settings/notify/d;)V

    invoke-virtual {v1, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v1, 0x1020016

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v2}, Lcom/android/settings/notify/d;->aZz()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    :cond_1
    :goto_0
    return-void

    :cond_2
    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/android/settings/SettingsFragment;->bGR:Landroid/widget/ListView;

    invoke-virtual {v1, v0}, Landroid/widget/ListView;->removeHeaderView(Landroid/view/View;)Z

    goto :goto_0
.end method

.method private bxs(Landroid/view/View;Lcom/android/settings/search/SearchResultItem;)V
    .locals 2

    iget-object v0, p2, Lcom/android/settings/search/SearchResultItem;->resource:Ljava/lang/String;

    const-string/jumbo v1, "miui_updater"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget v0, p2, Lcom/android/settings/search/SearchResultItem;->status:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_1

    invoke-virtual {p0}, Lcom/android/settings/SettingsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/android/settings/device/h;->aDZ(Landroid/app/Activity;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p2, Lcom/android/settings/search/SearchResultItem;->intent:Landroid/content/Intent;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/android/settings/SettingsFragment;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p2, Lcom/android/settings/search/SearchResultItem;->intent:Landroid/content/Intent;

    invoke-virtual {v0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method private bxt(Ljava/lang/String;)V
    .locals 2

    const/4 v1, 0x1

    iget-object v0, p0, Lcom/android/settings/SettingsFragment;->bHb:Ljava/lang/String;

    invoke-static {v0, p1}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-direct {p0}, Lcom/android/settings/SettingsFragment;->bxk()V

    iget-object v0, p0, Lcom/android/settings/SettingsFragment;->bGV:Lcom/android/settings/aQ;

    invoke-virtual {v0, v1}, Lcom/android/settings/aQ;->removeMessages(I)V

    iget-object v0, p0, Lcom/android/settings/SettingsFragment;->bGV:Lcom/android/settings/aQ;

    invoke-virtual {v0, v1, p1}, Lcom/android/settings/aQ;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    iget-object v0, p0, Lcom/android/settings/SettingsFragment;->bHb:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/SettingsFragment;->bGX:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    :cond_0
    iput-object p1, p0, Lcom/android/settings/SettingsFragment;->bHb:Ljava/lang/String;

    :cond_1
    return-void
.end method

.method static synthetic bxu(Lcom/android/settings/SettingsFragment;)Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/SettingsFragment;->bGK:Landroid/view/View;

    return-object v0
.end method

.method static synthetic bxv(Lcom/android/settings/SettingsFragment;)Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/SettingsFragment;->bGL:Ljava/util/List;

    return-object v0
.end method

.method static synthetic bxw(Lcom/android/settings/SettingsFragment;)Landroid/widget/TextView$OnEditorActionListener;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/SettingsFragment;->bGM:Landroid/widget/TextView$OnEditorActionListener;

    return-object v0
.end method

.method static synthetic bxx(Lcom/android/settings/SettingsFragment;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/settings/SettingsFragment;->bGP:Z

    return v0
.end method

.method static synthetic bxy(Lcom/android/settings/SettingsFragment;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/settings/SettingsFragment;->bGQ:Z

    return v0
.end method

.method static synthetic bxz(Lcom/android/settings/SettingsFragment;)Landroid/widget/ListView;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/SettingsFragment;->bGR:Landroid/widget/ListView;

    return-object v0
.end method


# virtual methods
.method public bxj()V
    .locals 6

    const/16 v5, 0x8

    invoke-super {p0}, Lcom/android/settings/BasePreferenceFragment;->bxj()V

    invoke-virtual {p0}, Lcom/android/settings/SettingsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/android/settings/MiuiSettings;

    new-instance v1, Lcom/android/settings/i;

    iget-object v2, p0, Lcom/android/settings/SettingsFragment;->caL:Ljava/util/List;

    invoke-virtual {v0}, Lcom/android/settings/MiuiSettings;->bgs()Lcom/android/settingslib/c/a;

    move-result-object v3

    const/4 v4, 0x0

    invoke-direct {v1, v0, v2, v3, v4}, Lcom/android/settings/i;-><init>(Landroid/app/Activity;Ljava/util/List;Lcom/android/settingslib/c/a;Z)V

    iput-object v1, p0, Lcom/android/settings/SettingsFragment;->bGN:Lcom/android/settings/i;

    iget-object v0, p0, Lcom/android/settings/SettingsFragment;->bGR:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/android/settings/SettingsFragment;->bGN:Lcom/android/settings/i;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    iget-object v0, p0, Lcom/android/settings/SettingsFragment;->bGR:Landroid/widget/ListView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setChoiceMode(I)V

    iget-object v0, p0, Lcom/android/settings/SettingsFragment;->bHa:Landroid/widget/ListView;

    invoke-virtual {v0, v5}, Landroid/widget/ListView;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/settings/SettingsFragment;->bHa:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/android/settings/SettingsFragment;->bGT:Lcom/android/settings/aS;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    iget-object v0, p0, Lcom/android/settings/SettingsFragment;->bGO:Landroid/widget/ListView;

    invoke-virtual {v0, v5}, Landroid/widget/ListView;->setVisibility(I)V

    invoke-virtual {p0}, Lcom/android/settings/SettingsFragment;->bTe()V

    return-void
.end method

.method protected bxm()I
    .locals 1

    const v0, 0x7f1500db

    return v0
.end method

.method public cC()Ljava/lang/String;
    .locals 1

    const-class v0, Lcom/android/settings/SettingsFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3

    invoke-super {p0, p1}, Lcom/android/settings/BasePreferenceFragment;->onCreate(Landroid/os/Bundle;)V

    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/android/settings/SettingsFragment;->bGZ:Ljava/util/List;

    iget-object v0, p0, Lcom/android/settings/SettingsFragment;->bGZ:Ljava/util/List;

    sget-object v1, Lcom/android/settings/search/SearchResultItem;->EMPTY:Lcom/android/settings/search/SearchResultItem;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v0, Lcom/android/settings/aS;

    invoke-virtual {p0}, Lcom/android/settings/SettingsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    iget-object v2, p0, Lcom/android/settings/SettingsFragment;->bGZ:Ljava/util/List;

    invoke-direct {v0, p0, v1, v2}, Lcom/android/settings/aS;-><init>(Lcom/android/settings/SettingsFragment;Landroid/content/Context;Ljava/util/List;)V

    iput-object v0, p0, Lcom/android/settings/SettingsFragment;->bGT:Lcom/android/settings/aS;

    new-instance v0, Lcom/android/settings/search/SearchResult;

    invoke-direct {v0}, Lcom/android/settings/search/SearchResult;-><init>()V

    iput-object v0, p0, Lcom/android/settings/SettingsFragment;->bGY:Lcom/android/settings/search/SearchResult;

    return-void
.end method

.method public onDestroy()V
    .locals 3

    const/4 v2, 0x0

    const/4 v1, 0x0

    invoke-super {p0}, Lcom/android/settings/BasePreferenceFragment;->onDestroy()V

    iget-object v0, p0, Lcom/android/settings/SettingsFragment;->bHd:Landroid/text/TextWatcher;

    invoke-interface {v0, v1, v2, v2, v2}, Landroid/text/TextWatcher;->onTextChanged(Ljava/lang/CharSequence;III)V

    iget-object v0, p0, Lcom/android/settings/SettingsFragment;->bHc:Landroid/os/HandlerThread;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/SettingsFragment;->bHc:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->quit()Z

    iput-object v1, p0, Lcom/android/settings/SettingsFragment;->bHc:Landroid/os/HandlerThread;

    :cond_0
    iput-object v1, p0, Lcom/android/settings/SettingsFragment;->bGY:Lcom/android/settings/search/SearchResult;

    return-void
.end method

.method public onInflateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    const/4 v1, 0x0

    const/4 v2, 0x1

    const v0, 0x7f0d01c0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const v0, 0x102000a

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/android/settings/SettingsFragment;->bGR:Landroid/widget/ListView;

    iget-object v0, p0, Lcom/android/settings/SettingsFragment;->bGR:Landroid/widget/ListView;

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setFocusable(Z)V

    iget-object v0, p0, Lcom/android/settings/SettingsFragment;->bGR:Landroid/widget/ListView;

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setFocusableInTouchMode(Z)V

    const v0, 0x7f0a03c1

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/android/settings/SettingsFragment;->bGO:Landroid/widget/ListView;

    iget-object v0, p0, Lcom/android/settings/SettingsFragment;->bGO:Landroid/widget/ListView;

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setFocusable(Z)V

    iget-object v0, p0, Lcom/android/settings/SettingsFragment;->bGO:Landroid/widget/ListView;

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setFocusableInTouchMode(Z)V

    const v0, 0x7f0a03c6

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/android/settings/SettingsFragment;->bHa:Landroid/widget/ListView;

    iget-object v0, p0, Lcom/android/settings/SettingsFragment;->bHa:Landroid/widget/ListView;

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setFocusable(Z)V

    iget-object v0, p0, Lcom/android/settings/SettingsFragment;->bHa:Landroid/widget/ListView;

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setFocusableInTouchMode(Z)V

    iget-object v0, p0, Lcom/android/settings/SettingsFragment;->bHa:Landroid/widget/ListView;

    invoke-virtual {v0, p0}, Landroid/widget/ListView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    const v0, 0x7f0a03c2

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/SettingsFragment;->bGX:Landroid/view/View;

    :try_start_0
    invoke-virtual {p0}, Lcom/android/settings/SettingsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const/4 v2, 0x1

    new-array v2, v2, [I

    const v3, 0x1010054

    const/4 v4, 0x0

    aput v3, v2, v4

    const/4 v3, 0x0

    invoke-virtual {v0, v3, v2}, Landroid/app/Activity;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    iget-object v2, p0, Lcom/android/settings/SettingsFragment;->bGX:Landroid/view/View;

    const/4 v3, 0x0

    invoke-virtual {v0, v3}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-object v1

    :catch_0
    move-exception v0

    iget-object v2, p0, Lcom/android/settings/SettingsFragment;->bGX:Landroid/view/View;

    const/4 v3, -0x1

    invoke-virtual {v2, v3}, Landroid/view/View;->setBackgroundColor(I)V

    const-string/jumbo v2, "SettingsFragment"

    const-string/jumbo v3, "Fail to find windowBackground in current context"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public onResume()V
    .locals 0

    invoke-super {p0}, Lcom/android/settings/BasePreferenceFragment;->onResume()V

    invoke-direct {p0}, Lcom/android/settings/SettingsFragment;->bxr()V

    return-void
.end method

.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 1

    iget-object v0, p0, Lcom/android/settings/SettingsFragment;->bHa:Landroid/widget/ListView;

    if-ne p1, v0, :cond_0

    invoke-direct {p0}, Lcom/android/settings/SettingsFragment;->bxp()V

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 3

    invoke-super {p0, p1, p2}, Lcom/android/settings/BasePreferenceFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/android/settings/SettingsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, Lmiui/R$layout;->search_stub:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/SettingsFragment;->bGK:Landroid/view/View;

    sget-boolean v0, Lmiui/os/Build;->IS_TABLET:Z

    if-nez v0, :cond_0

    sget-boolean v0, Lmiui/os/Build;->IS_CTA_BUILD:Z

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/SettingsFragment;->bGR:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/android/settings/SettingsFragment;->bGK:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->addHeaderView(Landroid/view/View;)V

    iget-object v0, p0, Lcom/android/settings/SettingsFragment;->bGK:Landroid/view/View;

    new-instance v1, Lcom/android/settings/gs;

    invoke-direct {v1, p0}, Lcom/android/settings/gs;-><init>(Lcom/android/settings/SettingsFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/android/settings/SettingsFragment;->bGK:Landroid/view/View;

    const v1, 0x1020009

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const v1, 0x7f120f41

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setHint(I)V

    :cond_0
    return-void
.end method
