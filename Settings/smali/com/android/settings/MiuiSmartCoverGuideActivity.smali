.class public Lcom/android/settings/MiuiSmartCoverGuideActivity;
.super Landroid/app/Activity;
.source "MiuiSmartCoverGuideActivity.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    const v0, 0x7f0d01d4

    invoke-virtual {p0, v0}, Lcom/android/settings/MiuiSmartCoverGuideActivity;->setContentView(I)V

    invoke-virtual {p0}, Lcom/android/settings/MiuiSmartCoverGuideActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v0

    new-instance v1, Lcom/android/settings/MiuiSmartCoverSettingsFragment;

    invoke-direct {v1}, Lcom/android/settings/MiuiSmartCoverSettingsFragment;-><init>()V

    const v2, 0x7f0a00f8

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v1, v3}, Landroid/app/FragmentTransaction;->add(ILandroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;

    invoke-virtual {v0}, Landroid/app/FragmentTransaction;->commit()I

    return-void
.end method
