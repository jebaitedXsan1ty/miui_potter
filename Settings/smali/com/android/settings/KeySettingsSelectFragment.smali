.class public Lcom/android/settings/KeySettingsSelectFragment;
.super Lcom/android/settings/MiuiSettingsPreferenceFragment;
.source "KeySettingsSelectFragment.java"


# instance fields
.field private cgK:Lmiui/app/AlertDialog;

.field private cgL:Lmiui/preference/RadioButtonPreferenceCategory;

.field private cgM:Lcom/android/settings/KeySettingsPreviewPreference;

.field private cgN:Ljava/lang/String;

.field private cgO:Lmiui/preference/RadioButtonPreference;

.field private cgP:I

.field private cgQ:Landroid/content/res/Resources;

.field private mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/android/settings/MiuiSettingsPreferenceFragment;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/settings/KeySettingsSelectFragment;->cgK:Lmiui/app/AlertDialog;

    return-void
.end method

.method private bZA(Ljava/lang/String;Landroid/content/res/Resources;)Ljava/lang/String;
    .locals 1

    const-string/jumbo v0, "launch_camera"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const v0, 0x7f1208ef

    invoke-virtual {p2, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    const-string/jumbo v0, "screen_shot"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const v0, 0x7f120eb8

    invoke-virtual {p2, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_1
    const-string/jumbo v0, "launch_voice_assistant"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    const v0, 0x7f1208f4

    invoke-virtual {p2, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_2
    const-string/jumbo v0, "launch_google_search"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    const v0, 0x7f1208f2

    invoke-virtual {p2, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_3
    const-string/jumbo v0, "go_to_sleep"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    const v0, 0x7f1207cf

    invoke-virtual {p2, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_4
    const-string/jumbo v0, "turn_on_torch"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    const v0, 0x7f1212d6

    invoke-virtual {p2, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_5
    const-string/jumbo v0, "close_app"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    const v0, 0x7f120419

    invoke-virtual {p2, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_6
    const-string/jumbo v0, "split_screen"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    const v0, 0x7f121104

    invoke-virtual {p2, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_7
    const-string/jumbo v0, "mi_pay"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    const v0, 0x7f120a8b

    invoke-virtual {p2, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_8
    const-string/jumbo v0, "show_menu"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    const v0, 0x7f12106f

    invoke-virtual {p2, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_9
    const-string/jumbo v0, "launch_recents"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_a

    const v0, 0x7f1208f3

    invoke-virtual {p2, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_a
    const-string/jumbo v0, ""

    return-object v0
.end method

.method private bZB(Ljava/util/ArrayList;)V
    .locals 3

    const/4 v1, 0x0

    iget v0, p0, Lcom/android/settings/KeySettingsSelectFragment;->cgP:I

    sparse-switch v0, :sswitch_data_0

    const-string/jumbo v0, "KeySettings"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "not found mTitleId"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/android/settings/KeySettingsSelectFragment;->cgP:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/settings/KeySettingsSelectFragment;->cgN:Ljava/lang/String;

    :goto_0
    const-string/jumbo v0, "KeySettings"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "mPreferenceKey = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/settings/KeySettingsSelectFragment;->cgN:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :sswitch_0
    const-string/jumbo v0, "launch_camera"

    iput-object v0, p0, Lcom/android/settings/KeySettingsSelectFragment;->cgN:Ljava/lang/String;

    const-string/jumbo v0, "double_click_power_key"

    invoke-virtual {p1, v1, v0}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    goto :goto_0

    :sswitch_1
    const-string/jumbo v0, "screen_shot"

    iput-object v0, p0, Lcom/android/settings/KeySettingsSelectFragment;->cgN:Ljava/lang/String;

    const-string/jumbo v0, "three_gesture_down"

    invoke-virtual {p1, v1, v0}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    goto :goto_0

    :sswitch_2
    const-string/jumbo v0, "launch_voice_assistant"

    iput-object v0, p0, Lcom/android/settings/KeySettingsSelectFragment;->cgN:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    const-string/jumbo v0, "launch_google_search"

    iput-object v0, p0, Lcom/android/settings/KeySettingsSelectFragment;->cgN:Ljava/lang/String;

    goto :goto_0

    :sswitch_4
    const-string/jumbo v0, "go_to_sleep"

    iput-object v0, p0, Lcom/android/settings/KeySettingsSelectFragment;->cgN:Ljava/lang/String;

    const-string/jumbo v0, "key_combination_power_back"

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    const-string/jumbo v0, "key_combination_power_home"

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    const-string/jumbo v0, "key_combination_power_menu"

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    goto :goto_0

    :sswitch_5
    const-string/jumbo v0, "turn_on_torch"

    iput-object v0, p0, Lcom/android/settings/KeySettingsSelectFragment;->cgN:Ljava/lang/String;

    const-string/jumbo v0, "double_click_power_key"

    invoke-virtual {p1, v1, v0}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    const-string/jumbo v0, "long_press_menu_key_when_lock"

    invoke-virtual {p1, v1, v0}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    goto :goto_0

    :sswitch_6
    const-string/jumbo v0, "close_app"

    iput-object v0, p0, Lcom/android/settings/KeySettingsSelectFragment;->cgN:Ljava/lang/String;

    goto :goto_0

    :sswitch_7
    const-string/jumbo v0, "split_screen"

    iput-object v0, p0, Lcom/android/settings/KeySettingsSelectFragment;->cgN:Ljava/lang/String;

    const-string/jumbo v0, "key_combination_power_back"

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    const-string/jumbo v0, "key_combination_power_home"

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    const-string/jumbo v0, "key_combination_power_menu"

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    goto/16 :goto_0

    :sswitch_8
    const-string/jumbo v0, "mi_pay"

    iput-object v0, p0, Lcom/android/settings/KeySettingsSelectFragment;->cgN:Ljava/lang/String;

    const-string/jumbo v0, "long_press_home_key"

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    const-string/jumbo v0, "long_press_menu_key"

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    const-string/jumbo v0, "long_press_back_key"

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    const-string/jumbo v0, "key_combination_power_back"

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    const-string/jumbo v0, "key_combination_power_home"

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    const-string/jumbo v0, "key_combination_power_menu"

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    const-string/jumbo v0, "double_click_power_key"

    invoke-virtual {p1, v1, v0}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    goto/16 :goto_0

    :sswitch_9
    const-string/jumbo v0, "show_menu"

    iput-object v0, p0, Lcom/android/settings/KeySettingsSelectFragment;->cgN:Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_a
    const-string/jumbo v0, "launch_recents"

    iput-object v0, p0, Lcom/android/settings/KeySettingsSelectFragment;->cgN:Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x7f120419 -> :sswitch_6
        0x7f1207cf -> :sswitch_4
        0x7f1208ef -> :sswitch_0
        0x7f1208f2 -> :sswitch_3
        0x7f1208f3 -> :sswitch_a
        0x7f1208f4 -> :sswitch_2
        0x7f120a8b -> :sswitch_8
        0x7f120eb8 -> :sswitch_1
        0x7f12106f -> :sswitch_9
        0x7f121104 -> :sswitch_7
        0x7f1212d6 -> :sswitch_5
    .end sparse-switch
.end method

.method private bZC(Ljava/lang/String;Lmiui/preference/RadioButtonPreference;)V
    .locals 4

    const/4 v3, -0x2

    iget-object v0, p0, Lcom/android/settings/KeySettingsSelectFragment;->cgL:Lmiui/preference/RadioButtonPreferenceCategory;

    invoke-virtual {v0, p2}, Lmiui/preference/RadioButtonPreferenceCategory;->setCheckedPreference(Landroid/preference/Preference;)V

    iget-object v0, p0, Lcom/android/settings/KeySettingsSelectFragment;->cgO:Lmiui/preference/RadioButtonPreference;

    invoke-virtual {v0}, Lmiui/preference/RadioButtonPreference;->getKey()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "key_none"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/android/settings/KeySettingsSelectFragment;->bWB()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/KeySettingsSelectFragment;->cgO:Lmiui/preference/RadioButtonPreference;

    invoke-virtual {v1}, Lmiui/preference/RadioButtonPreference;->getKey()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "none"

    invoke-static {v0, v1, v2, v3}, Landroid/provider/Settings$System;->putStringForUser(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;I)Z

    :cond_0
    const-string/jumbo v0, "key_none"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lcom/android/settings/KeySettingsSelectFragment;->bWB()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/KeySettingsSelectFragment;->cgN:Ljava/lang/String;

    invoke-static {v0, p1, v1, v3}, Landroid/provider/Settings$System;->putStringForUser(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;I)Z

    :cond_1
    iput-object p2, p0, Lcom/android/settings/KeySettingsSelectFragment;->cgO:Lmiui/preference/RadioButtonPreference;

    iget-object v0, p0, Lcom/android/settings/KeySettingsSelectFragment;->cgM:Lcom/android/settings/KeySettingsPreviewPreference;

    iget-object v1, p0, Lcom/android/settings/KeySettingsSelectFragment;->cgO:Lmiui/preference/RadioButtonPreference;

    invoke-virtual {v1}, Lmiui/preference/RadioButtonPreference;->getKey()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/settings/KeySettingsPreviewPreference;->bBP(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic bZD(Lcom/android/settings/KeySettingsSelectFragment;)Lmiui/app/AlertDialog;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/KeySettingsSelectFragment;->cgK:Lmiui/app/AlertDialog;

    return-object v0
.end method

.method static synthetic bZE(Lcom/android/settings/KeySettingsSelectFragment;Lmiui/app/AlertDialog;)Lmiui/app/AlertDialog;
    .locals 0

    iput-object p1, p0, Lcom/android/settings/KeySettingsSelectFragment;->cgK:Lmiui/app/AlertDialog;

    return-object p1
.end method

.method static synthetic bZF(Lcom/android/settings/KeySettingsSelectFragment;Ljava/lang/String;Lmiui/preference/RadioButtonPreference;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/android/settings/KeySettingsSelectFragment;->bZC(Ljava/lang/String;Lmiui/preference/RadioButtonPreference;)V

    return-void
.end method

.method private bZy(Ljava/lang/String;Ljava/lang/String;Lmiui/preference/RadioButtonPreference;)V
    .locals 8

    const/4 v6, 0x0

    const/4 v7, 0x0

    iget-object v0, p0, Lcom/android/settings/KeySettingsSelectFragment;->cgK:Lmiui/app/AlertDialog;

    if-eqz v0, :cond_0

    return-void

    :cond_0
    new-instance v0, Lcom/android/settings/ls;

    invoke-direct {v0, p0, p1, p3}, Lcom/android/settings/ls;-><init>(Lcom/android/settings/KeySettingsSelectFragment;Ljava/lang/String;Lmiui/preference/RadioButtonPreference;)V

    iget-object v1, p0, Lcom/android/settings/KeySettingsSelectFragment;->cgQ:Landroid/content/res/Resources;

    invoke-direct {p0, p1, v1}, Lcom/android/settings/KeySettingsSelectFragment;->bZz(Ljava/lang/String;Landroid/content/res/Resources;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/android/settings/KeySettingsSelectFragment;->cgQ:Landroid/content/res/Resources;

    invoke-direct {p0, p2, v2}, Lcom/android/settings/KeySettingsSelectFragment;->bZA(Ljava/lang/String;Landroid/content/res/Resources;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/android/settings/KeySettingsSelectFragment;->cgQ:Landroid/content/res/Resources;

    iget v4, p0, Lcom/android/settings/KeySettingsSelectFragment;->cgP:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Lmiui/app/AlertDialog$Builder;

    iget-object v5, p0, Lcom/android/settings/KeySettingsSelectFragment;->mContext:Landroid/content/Context;

    invoke-direct {v4, v5}, Lmiui/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v4, v6}, Lmiui/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Lmiui/app/AlertDialog$Builder;

    move-result-object v4

    iget-object v5, p0, Lcom/android/settings/KeySettingsSelectFragment;->cgQ:Landroid/content/res/Resources;

    const/4 v6, 0x3

    new-array v6, v6, [Ljava/lang/Object;

    aput-object v1, v6, v7

    const/4 v1, 0x1

    aput-object v2, v6, v1

    const/4 v1, 0x2

    aput-object v3, v6, v1

    const v1, 0x7f120890

    invoke-virtual {v5, v1, v6}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Lmiui/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Lmiui/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f120892

    invoke-virtual {v1, v2, v0}, Lmiui/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Lmiui/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f120891

    invoke-virtual {v1, v2, v0}, Lmiui/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Lmiui/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0, v7}, Lmiui/app/AlertDialog$Builder;->setCancelable(Z)Lmiui/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lmiui/app/AlertDialog$Builder;->create()Lmiui/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/KeySettingsSelectFragment;->cgK:Lmiui/app/AlertDialog;

    iget-object v0, p0, Lcom/android/settings/KeySettingsSelectFragment;->cgK:Lmiui/app/AlertDialog;

    invoke-virtual {v0}, Lmiui/app/AlertDialog;->show()V

    return-void
.end method

.method private bZz(Ljava/lang/String;Landroid/content/res/Resources;)Ljava/lang/String;
    .locals 1

    const-string/jumbo v0, "double_click_power_key"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const v0, 0x7f120648

    invoke-virtual {p2, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    const-string/jumbo v0, "long_press_menu_key"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const v0, 0x7f1209f9

    invoke-virtual {p2, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_1
    const-string/jumbo v0, "long_press_home_key"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    const v0, 0x7f1209f8

    invoke-virtual {p2, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_2
    const-string/jumbo v0, "long_press_back_key"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    const v0, 0x7f1209f6

    invoke-virtual {p2, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_3
    const-string/jumbo v0, "key_combination_power_back"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    const v0, 0x7f12088d

    invoke-virtual {p2, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_4
    const-string/jumbo v0, "key_combination_power_home"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    const v0, 0x7f12088e

    invoke-virtual {p2, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_5
    const-string/jumbo v0, "key_combination_power_menu"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    const v0, 0x7f12088f

    invoke-virtual {p2, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_6
    const-string/jumbo v0, "long_press_menu_key_when_lock"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    const v0, 0x7f1209fa

    invoke-virtual {p2, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_7
    const-string/jumbo v0, "three_gesture_down"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    const v0, 0x7f121269

    invoke-virtual {p2, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_8
    const-string/jumbo v0, ""

    return-object v0
.end method


# virtual methods
.method public cC()Ljava/lang/String;
    .locals 1

    const-class v0, Lcom/android/settings/KeySettingsSelectFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 7

    const/4 v1, 0x0

    invoke-super {p0, p1}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->onActivityCreated(Landroid/os/Bundle;)V

    const v0, 0x7f15006f

    invoke-virtual {p0, v0}, Lcom/android/settings/KeySettingsSelectFragment;->addPreferencesFromResource(I)V

    invoke-virtual {p0}, Lcom/android/settings/KeySettingsSelectFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/KeySettingsSelectFragment;->mContext:Landroid/content/Context;

    invoke-virtual {p0}, Lcom/android/settings/KeySettingsSelectFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_0

    const-string/jumbo v2, ":android:show_fragment_title"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/android/settings/KeySettingsSelectFragment;->cgP:I

    :cond_0
    iget-object v0, p0, Lcom/android/settings/KeySettingsSelectFragment;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/KeySettingsSelectFragment;->cgQ:Landroid/content/res/Resources;

    iget-object v0, p0, Lcom/android/settings/KeySettingsSelectFragment;->cgQ:Landroid/content/res/Resources;

    const v2, 0x7f030089

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    array-length v4, v2

    move v0, v1

    :goto_0
    if-ge v0, v4, :cond_1

    aget-object v5, v2, v0

    invoke-virtual {v3, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    invoke-direct {p0, v3}, Lcom/android/settings/KeySettingsSelectFragment;->bZB(Ljava/util/ArrayList;)V

    const-string/jumbo v0, "key_gesture_function_preview"

    invoke-virtual {p0, v0}, Lcom/android/settings/KeySettingsSelectFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Lcom/android/settings/KeySettingsPreviewPreference;

    iput-object v0, p0, Lcom/android/settings/KeySettingsSelectFragment;->cgM:Lcom/android/settings/KeySettingsPreviewPreference;

    const-string/jumbo v0, "key_gesture_function_optional"

    invoke-virtual {p0, v0}, Lcom/android/settings/KeySettingsSelectFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Lmiui/preference/RadioButtonPreferenceCategory;

    iput-object v0, p0, Lcom/android/settings/KeySettingsSelectFragment;->cgL:Lmiui/preference/RadioButtonPreferenceCategory;

    invoke-interface {v3}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_2
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    new-instance v3, Lmiui/preference/RadioButtonPreference;

    iget-object v4, p0, Lcom/android/settings/KeySettingsSelectFragment;->mContext:Landroid/content/Context;

    invoke-direct {v3, v4}, Lmiui/preference/RadioButtonPreference;-><init>(Landroid/content/Context;)V

    invoke-virtual {v3, v0}, Lmiui/preference/RadioButtonPreference;->setKey(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/android/settings/KeySettingsSelectFragment;->cgQ:Landroid/content/res/Resources;

    const-string/jumbo v5, "string"

    iget-object v6, p0, Lcom/android/settings/KeySettingsSelectFragment;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v0, v5, v6}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v4

    invoke-virtual {v3, v4}, Lmiui/preference/RadioButtonPreference;->setTitle(I)V

    invoke-virtual {v3, v1}, Lmiui/preference/RadioButtonPreference;->setPersistent(Z)V

    iget-object v4, p0, Lcom/android/settings/KeySettingsSelectFragment;->mContext:Landroid/content/Context;

    invoke-static {v4, v0}, Landroid/provider/MiuiSettings$Key;->getKeyAndGestureShortcutFunction(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/android/settings/KeySettingsSelectFragment;->cgL:Lmiui/preference/RadioButtonPreferenceCategory;

    invoke-virtual {v5, v3}, Lmiui/preference/RadioButtonPreferenceCategory;->addPreference(Landroid/preference/Preference;)Z

    iget-object v5, p0, Lcom/android/settings/KeySettingsSelectFragment;->cgN:Ljava/lang/String;

    invoke-static {v4, v5}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_3

    iput-object v3, p0, Lcom/android/settings/KeySettingsSelectFragment;->cgO:Lmiui/preference/RadioButtonPreference;

    :cond_3
    const-string/jumbo v4, "key_none"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/settings/KeySettingsSelectFragment;->cgO:Lmiui/preference/RadioButtonPreference;

    if-nez v0, :cond_2

    iput-object v3, p0, Lcom/android/settings/KeySettingsSelectFragment;->cgO:Lmiui/preference/RadioButtonPreference;

    goto :goto_1

    :cond_4
    iget-object v0, p0, Lcom/android/settings/KeySettingsSelectFragment;->cgL:Lmiui/preference/RadioButtonPreferenceCategory;

    iget-object v1, p0, Lcom/android/settings/KeySettingsSelectFragment;->cgO:Lmiui/preference/RadioButtonPreference;

    invoke-virtual {v0, v1}, Lmiui/preference/RadioButtonPreferenceCategory;->setCheckedPreference(Landroid/preference/Preference;)V

    iget-object v0, p0, Lcom/android/settings/KeySettingsSelectFragment;->cgM:Lcom/android/settings/KeySettingsPreviewPreference;

    iget-object v1, p0, Lcom/android/settings/KeySettingsSelectFragment;->cgN:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/android/settings/KeySettingsPreviewPreference;->bBQ(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/settings/KeySettingsSelectFragment;->cgM:Lcom/android/settings/KeySettingsPreviewPreference;

    iget-object v1, p0, Lcom/android/settings/KeySettingsSelectFragment;->cgO:Lmiui/preference/RadioButtonPreference;

    invoke-virtual {v1}, Lmiui/preference/RadioButtonPreference;->getKey()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/settings/KeySettingsPreviewPreference;->bBP(Ljava/lang/String;)V

    return-void
.end method

.method public onDestroy()V
    .locals 1

    iget-object v0, p0, Lcom/android/settings/KeySettingsSelectFragment;->cgM:Lcom/android/settings/KeySettingsPreviewPreference;

    invoke-virtual {v0}, Lcom/android/settings/KeySettingsPreviewPreference;->bBK()V

    invoke-super {p0}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->onDestroy()V

    return-void
.end method

.method public onPreferenceTreeClick(Landroid/preference/PreferenceScreen;Landroid/preference/Preference;)Z
    .locals 5

    instance-of v0, p2, Lmiui/preference/RadioButtonPreference;

    if-eqz v0, :cond_0

    move-object v0, p2

    check-cast v0, Lmiui/preference/RadioButtonPreference;

    iget-object v1, p0, Lcom/android/settings/KeySettingsSelectFragment;->cgO:Lmiui/preference/RadioButtonPreference;

    if-eq v1, v0, :cond_0

    invoke-virtual {v0}, Lmiui/preference/RadioButtonPreference;->getKey()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/android/settings/KeySettingsSelectFragment;->mContext:Landroid/content/Context;

    invoke-static {v2, v1}, Landroid/provider/MiuiSettings$Key;->getKeyAndGestureShortcutFunction(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_1

    const-string/jumbo v3, "none"

    invoke-static {v2, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    xor-int/lit8 v3, v3, 0x1

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/android/settings/KeySettingsSelectFragment;->cgL:Lmiui/preference/RadioButtonPreferenceCategory;

    iget-object v4, p0, Lcom/android/settings/KeySettingsSelectFragment;->cgO:Lmiui/preference/RadioButtonPreference;

    invoke-virtual {v3, v4}, Lmiui/preference/RadioButtonPreferenceCategory;->setCheckedPreference(Landroid/preference/Preference;)V

    invoke-direct {p0, v1, v2, v0}, Lcom/android/settings/KeySettingsSelectFragment;->bZy(Ljava/lang/String;Ljava/lang/String;Lmiui/preference/RadioButtonPreference;)V

    :cond_0
    :goto_0
    invoke-super {p0, p1, p2}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->onPreferenceTreeClick(Landroid/preference/PreferenceScreen;Landroid/preference/Preference;)Z

    move-result v0

    return v0

    :cond_1
    invoke-direct {p0, v1, v0}, Lcom/android/settings/KeySettingsSelectFragment;->bZC(Ljava/lang/String;Lmiui/preference/RadioButtonPreference;)V

    goto :goto_0
.end method
