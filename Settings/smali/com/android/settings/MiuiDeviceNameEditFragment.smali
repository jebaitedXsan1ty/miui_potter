.class public Lcom/android/settings/MiuiDeviceNameEditFragment;
.super Lcom/android/settings/BaseEditFragment;
.source "MiuiDeviceNameEditFragment.java"


# instance fields
.field private bTW:Landroid/widget/EditText;

.field private bTX:Landroid/os/Handler;

.field private bTY:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/BaseEditFragment;-><init>()V

    return-void
.end method

.method static synthetic bNt(Lcom/android/settings/MiuiDeviceNameEditFragment;)Landroid/widget/EditText;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/MiuiDeviceNameEditFragment;->bTW:Landroid/widget/EditText;

    return-object v0
.end method


# virtual methods
.method public adN()V
    .locals 3

    iget-object v0, p0, Lcom/android/settings/MiuiDeviceNameEditFragment;->bTW:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v0

    :try_start_0
    invoke-virtual {p0}, Lcom/android/settings/MiuiDeviceNameEditFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-static {v1, v0}, Landroid/provider/MiuiSettings$System;->setDeviceName(Landroid/content/Context;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    invoke-super {p0}, Lcom/android/settings/BaseEditFragment;->adN()V

    return-void

    :catch_0
    move-exception v0

    invoke-virtual {p0}, Lcom/android/settings/MiuiDeviceNameEditFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const v1, 0x7f12059e

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method

.method public bdX()Z
    .locals 2

    iget-object v0, p0, Lcom/android/settings/MiuiDeviceNameEditFragment;->bTW:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lcom/android/settings/MiuiDeviceNameEditFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-static {v1}, Landroid/provider/MiuiSettings$System;->getDeviceName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    return v0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 2

    invoke-virtual {p0}, Lcom/android/settings/MiuiDeviceNameEditFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f120592

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public onDestroyView()V
    .locals 2

    invoke-super {p0}, Lcom/android/settings/BaseEditFragment;->onDestroyView()V

    iget-object v0, p0, Lcom/android/settings/MiuiDeviceNameEditFragment;->bTX:Landroid/os/Handler;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/MiuiDeviceNameEditFragment;->bTY:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/MiuiDeviceNameEditFragment;->bTX:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/settings/MiuiDeviceNameEditFragment;->bTY:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    :cond_0
    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 5

    const v0, 0x7f0a013a

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/android/settings/MiuiDeviceNameEditFragment;->bTW:Landroid/widget/EditText;

    invoke-virtual {p0}, Lcom/android/settings/MiuiDeviceNameEditFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Landroid/provider/MiuiSettings$System;->getDeviceName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/MiuiDeviceNameEditFragment;->bTW:Landroid/widget/EditText;

    invoke-virtual {v1, v0}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/android/settings/MiuiDeviceNameEditFragment;->bTW:Landroid/widget/EditText;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    invoke-virtual {v1, v0}, Landroid/widget/EditText;->setSelection(I)V

    :cond_0
    iget-object v0, p0, Lcom/android/settings/MiuiDeviceNameEditFragment;->bTW:Landroid/widget/EditText;

    const/4 v1, 0x1

    new-array v1, v1, [Landroid/text/InputFilter;

    new-instance v2, Lcom/android/settings/bN;

    new-instance v3, Lcom/android/settings/jA;

    invoke-direct {v3, p0}, Lcom/android/settings/jA;-><init>(Lcom/android/settings/MiuiDeviceNameEditFragment;)V

    const/16 v4, 0x1f

    invoke-direct {v2, v3, v4}, Lcom/android/settings/bN;-><init>(Lcom/android/settings/bO;I)V

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setFilters([Landroid/text/InputFilter;)V

    iget-object v0, p0, Lcom/android/settings/MiuiDeviceNameEditFragment;->bTW:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->requestFocus()Z

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/android/settings/MiuiDeviceNameEditFragment;->bTX:Landroid/os/Handler;

    new-instance v0, Lcom/android/settings/jB;

    invoke-direct {v0, p0}, Lcom/android/settings/jB;-><init>(Lcom/android/settings/MiuiDeviceNameEditFragment;)V

    iput-object v0, p0, Lcom/android/settings/MiuiDeviceNameEditFragment;->bTY:Ljava/lang/Runnable;

    iget-object v0, p0, Lcom/android/settings/MiuiDeviceNameEditFragment;->bTX:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/settings/MiuiDeviceNameEditFragment;->bTY:Ljava/lang/Runnable;

    const-wide/16 v2, 0x64

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    return-void
.end method

.method public vB(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2

    const v0, 0x7f0d0099

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method
