.class public Lcom/android/settings/cf;
.super Landroid/widget/BaseAdapter;
.source "MaxAspectRatioSettings.java"


# instance fields
.field bWF:Landroid/view/View$OnClickListener;

.field private bWG:Ljava/util/List;

.field final synthetic bWH:Lcom/android/settings/MaxAspectRatioSettings;


# direct methods
.method public constructor <init>(Lcom/android/settings/MaxAspectRatioSettings;)V
    .locals 1

    iput-object p1, p0, Lcom/android/settings/cf;->bWH:Lcom/android/settings/MaxAspectRatioSettings;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    new-instance v0, Lcom/android/settings/jW;

    invoke-direct {v0, p0}, Lcom/android/settings/jW;-><init>(Lcom/android/settings/cf;)V

    iput-object v0, p0, Lcom/android/settings/cf;->bWF:Landroid/view/View$OnClickListener;

    return-void
.end method


# virtual methods
.method public bQE(Ljava/util/List;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/cf;->bWG:Ljava/util/List;

    return-void
.end method

.method public getCount()I
    .locals 1

    iget-object v0, p0, Lcom/android/settings/cf;->bWG:Ljava/util/List;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/android/settings/cf;->bWG:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    goto :goto_0
.end method

.method public getItem(I)Lcom/android/settings/ce;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/cf;->bWG:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settings/ce;

    return-object v0
.end method

.method public bridge synthetic getItem(I)Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0, p1}, Lcom/android/settings/cf;->getItem(I)Lcom/android/settings/ce;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2

    int-to-long v0, p1

    return-wide v0
.end method

.method public getItemViewType(I)I
    .locals 1

    iget-object v0, p0, Lcom/android/settings/cf;->bWG:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settings/ce;

    invoke-static {v0}, Lcom/android/settings/ce;->bQC(Lcom/android/settings/ce;)I

    move-result v0

    return v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 6

    const/4 v5, 0x0

    const/4 v4, 0x0

    iget-object v0, p0, Lcom/android/settings/cf;->bWG:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settings/ce;

    invoke-static {v0}, Lcom/android/settings/ce;->bQC(Lcom/android/settings/ce;)I

    move-result v1

    if-nez v1, :cond_2

    if-nez p2, :cond_1

    new-instance v2, Lcom/android/settings/cg;

    invoke-direct {v2, p0, v4}, Lcom/android/settings/cg;-><init>(Lcom/android/settings/cf;Lcom/android/settings/cg;)V

    iget-object v1, p0, Lcom/android/settings/cf;->bWH:Lcom/android/settings/MaxAspectRatioSettings;

    invoke-static {v1}, Lcom/android/settings/MaxAspectRatioSettings;->bQw(Lcom/android/settings/MaxAspectRatioSettings;)Landroid/view/LayoutInflater;

    move-result-object v1

    const v3, 0x7f0d00f3

    invoke-virtual {v1, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    const v1, 0x1020006

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, v2, Lcom/android/settings/cg;->bWJ:Landroid/widget/ImageView;

    const v1, 0x1020016

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, v2, Lcom/android/settings/cg;->bWK:Landroid/widget/TextView;

    const v1, 0x1020040

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lmiui/widget/SlidingButton;

    iput-object v1, v2, Lcom/android/settings/cg;->bWL:Lmiui/widget/SlidingButton;

    iget-object v1, v2, Lcom/android/settings/cg;->bWL:Lmiui/widget/SlidingButton;

    new-instance v3, Lcom/android/settings/jX;

    invoke-direct {v3, p0, p2}, Lcom/android/settings/jX;-><init>(Lcom/android/settings/cf;Landroid/view/View;)V

    invoke-virtual {v1, v3}, Lmiui/widget/SlidingButton;->setOnPerformCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    invoke-virtual {p2, v2}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    move-object v1, v2

    :goto_0
    iget-object v2, p0, Lcom/android/settings/cf;->bWH:Lcom/android/settings/MaxAspectRatioSettings;

    invoke-static {v2}, Lcom/android/settings/MaxAspectRatioSettings;->bQv(Lcom/android/settings/MaxAspectRatioSettings;)Lcom/android/settings/cN;

    move-result-object v2

    iget-object v3, v1, Lcom/android/settings/cg;->bWJ:Landroid/widget/ImageView;

    invoke-virtual {v0}, Lcom/android/settings/ce;->bQz()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcom/android/settings/cN;->bWl(Landroid/widget/ImageView;Ljava/lang/String;)Z

    iget-object v2, v1, Lcom/android/settings/cg;->bWK:Landroid/widget/TextView;

    iget-object v3, v0, Lcom/android/settings/ce;->bWz:Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v2, v1, Lcom/android/settings/cg;->bWL:Lmiui/widget/SlidingButton;

    invoke-static {v0}, Lcom/android/settings/ce;->bQB(Lcom/android/settings/ce;)Z

    move-result v3

    xor-int/lit8 v3, v3, 0x1

    invoke-virtual {v2, v3}, Lmiui/widget/SlidingButton;->setChecked(Z)V

    iget-object v2, v1, Lcom/android/settings/cg;->bWL:Lmiui/widget/SlidingButton;

    invoke-virtual {v2, v5}, Lmiui/widget/SlidingButton;->setClickable(Z)V

    const v2, 0x7f0d00f1

    invoke-virtual {p2, v2, v0}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    iget-object v2, p0, Lcom/android/settings/cf;->bWF:Landroid/view/View$OnClickListener;

    invoke-virtual {p2, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {v0}, Lcom/android/settings/ce;->bQA()Z

    move-result v2

    invoke-virtual {p2, v2}, Landroid/view/View;->setEnabled(Z)V

    iget-object v2, v1, Lcom/android/settings/cg;->bWK:Landroid/widget/TextView;

    invoke-virtual {v0}, Lcom/android/settings/ce;->bQA()Z

    move-result v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setEnabled(Z)V

    iget-object v1, v1, Lcom/android/settings/cg;->bWL:Lmiui/widget/SlidingButton;

    invoke-virtual {v0}, Lcom/android/settings/ce;->bQA()Z

    move-result v0

    invoke-virtual {v1, v0}, Lmiui/widget/SlidingButton;->setEnabled(Z)V

    :cond_0
    :goto_1
    return-object p2

    :cond_1
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/settings/cg;

    goto :goto_0

    :cond_2
    invoke-static {v0}, Lcom/android/settings/ce;->bQC(Lcom/android/settings/ce;)I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    if-nez p2, :cond_3

    new-instance v2, Lcom/android/settings/cg;

    invoke-direct {v2, p0, v4}, Lcom/android/settings/cg;-><init>(Lcom/android/settings/cf;Lcom/android/settings/cg;)V

    iget-object v1, p0, Lcom/android/settings/cf;->bWH:Lcom/android/settings/MaxAspectRatioSettings;

    invoke-static {v1}, Lcom/android/settings/MaxAspectRatioSettings;->bQw(Lcom/android/settings/MaxAspectRatioSettings;)Landroid/view/LayoutInflater;

    move-result-object v1

    const v3, 0x7f0d00f2

    invoke-virtual {v1, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    const v1, 0x7f0a01db

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, v2, Lcom/android/settings/cg;->bWI:Landroid/widget/TextView;

    invoke-virtual {p2, v5}, Landroid/view/View;->setEnabled(Z)V

    invoke-virtual {p2, v2}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    move-object v1, v2

    :goto_2
    iget-object v1, v1, Lcom/android/settings/cg;->bWI:Landroid/widget/TextView;

    iget-object v0, v0, Lcom/android/settings/ce;->bWz:Ljava/lang/String;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    :cond_3
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/settings/cg;

    goto :goto_2
.end method

.method public getViewTypeCount()I
    .locals 1

    const/4 v0, 0x2

    return v0
.end method
