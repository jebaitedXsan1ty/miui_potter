.class public Lcom/android/settings/Legal;
.super Lcom/android/settings/MiuiSettingsPreferenceFragment;
.source "Legal.java"


# instance fields
.field private bIA:Landroid/preference/Preference;

.field private bIB:Landroid/preference/Preference;

.field private bIC:Landroid/preference/Preference;

.field private bID:Landroid/preference/Preference;

.field private bIz:Landroid/preference/Preference;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/MiuiSettingsPreferenceFragment;-><init>()V

    return-void
.end method


# virtual methods
.method public cC()Ljava/lang/String;
    .locals 1

    const-class v0, Lcom/android/settings/Legal;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 4

    const/4 v3, 0x1

    invoke-super {p0, p1}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->onCreate(Landroid/os/Bundle;)V

    const v0, 0x7f15003d

    invoke-virtual {p0, v0}, Lcom/android/settings/Legal;->addPreferencesFromResource(I)V

    invoke-virtual {p0}, Lcom/android/settings/Legal;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {p0}, Lcom/android/settings/Legal;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v1

    const-string/jumbo v2, "terms"

    invoke-static {v0, v1, v2, v3}, Lcom/android/settings/aq;->bqD(Landroid/content/Context;Landroid/preference/PreferenceGroup;Ljava/lang/String;I)Z

    const-string/jumbo v2, "license"

    invoke-static {v0, v1, v2, v3}, Lcom/android/settings/aq;->bqD(Landroid/content/Context;Landroid/preference/PreferenceGroup;Ljava/lang/String;I)Z

    const-string/jumbo v2, "copyright"

    invoke-static {v0, v1, v2, v3}, Lcom/android/settings/aq;->bqD(Landroid/content/Context;Landroid/preference/PreferenceGroup;Ljava/lang/String;I)Z

    const-string/jumbo v0, "miuiCopyright"

    invoke-virtual {p0, v0}, Lcom/android/settings/Legal;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/Legal;->bIz:Landroid/preference/Preference;

    const-string/jumbo v0, "miuiUserAgreement"

    invoke-virtual {p0, v0}, Lcom/android/settings/Legal;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/Legal;->bIC:Landroid/preference/Preference;

    const-string/jumbo v0, "miuiPrivacyPolicy"

    invoke-virtual {p0, v0}, Lcom/android/settings/Legal;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/Legal;->bIA:Landroid/preference/Preference;

    const-string/jumbo v0, "miuiSar"

    invoke-virtual {p0, v0}, Lcom/android/settings/Legal;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/Legal;->bIB:Landroid/preference/Preference;

    const-string/jumbo v0, "miuiUserExperienceProgram"

    invoke-virtual {p0, v0}, Lcom/android/settings/Legal;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/Legal;->bID:Landroid/preference/Preference;

    sget-boolean v0, Lmiui/os/Build;->IS_GLOBAL_BUILD:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/Legal;->bIB:Landroid/preference/Preference;

    invoke-virtual {v1, v0}, Landroid/preference/PreferenceGroup;->removePreference(Landroid/preference/Preference;)Z

    :cond_0
    return-void
.end method

.method public onPreferenceTreeClick(Landroid/preference/PreferenceScreen;Landroid/preference/Preference;)Z
    .locals 3

    new-instance v0, Landroid/content/Intent;

    invoke-static {}, Lcom/android/settings/dc;->getInstance()Lcom/android/settings/dc;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/settings/dc;->bYz()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/android/settings/Legal;->bIz:Landroid/preference/Preference;

    if-ne p2, v1, :cond_2

    const-string/jumbo v1, "android.intent.extra.LICENSE_TYPE"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    :cond_0
    :goto_0
    invoke-static {}, Lcom/android/settings/dc;->getInstance()Lcom/android/settings/dc;

    move-result-object v1

    invoke-virtual {p0}, Lcom/android/settings/Legal;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Lcom/android/settings/dc;->bYt(Landroid/content/Context;Landroid/content/Intent;)Z

    move-result v1

    if-eqz v1, :cond_5

    const-string/jumbo v1, "android.intent.extra.LICENSE_TYPE"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_5

    invoke-virtual {p2, v0}, Landroid/preference/Preference;->setIntent(Landroid/content/Intent;)V

    :cond_1
    :goto_1
    invoke-super {p0, p1, p2}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->onPreferenceTreeClick(Landroid/preference/PreferenceScreen;Landroid/preference/Preference;)Z

    move-result v0

    return v0

    :cond_2
    iget-object v1, p0, Lcom/android/settings/Legal;->bIC:Landroid/preference/Preference;

    if-ne p2, v1, :cond_3

    const-string/jumbo v1, "android.intent.extra.LICENSE_TYPE"

    const/4 v2, 0x2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    goto :goto_0

    :cond_3
    iget-object v1, p0, Lcom/android/settings/Legal;->bIA:Landroid/preference/Preference;

    if-ne p2, v1, :cond_4

    const-string/jumbo v1, "android.intent.extra.LICENSE_TYPE"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    goto :goto_0

    :cond_4
    iget-object v1, p0, Lcom/android/settings/Legal;->bID:Landroid/preference/Preference;

    if-ne p2, v1, :cond_0

    const-string/jumbo v1, "android.intent.extra.LICENSE_TYPE"

    const/16 v2, 0x8

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    goto :goto_0

    :cond_5
    iget-object v1, p0, Lcom/android/settings/Legal;->bIB:Landroid/preference/Preference;

    if-ne p2, v1, :cond_1

    const-string/jumbo v1, "android.intent.extra.LICENSE_TYPE"

    const/4 v2, 0x7

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    invoke-virtual {p2, v0}, Landroid/preference/Preference;->setIntent(Landroid/content/Intent;)V

    goto :goto_1
.end method

.method public onStart()V
    .locals 2

    invoke-super {p0}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->onStart()V

    invoke-virtual {p0}, Lcom/android/settings/Legal;->getActionBar()Lmiui/app/ActionBar;

    move-result-object v0

    if-eqz v0, :cond_0

    const v1, 0x7f120909

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setTitle(I)V

    :cond_0
    return-void
.end method
