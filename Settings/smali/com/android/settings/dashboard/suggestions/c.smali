.class public Lcom/android/settings/dashboard/suggestions/c;
.super Ljava/lang/Object;
.source "SuggestionFeaturizer.java"


# instance fields
.field private final Io:Lcom/android/settings/dashboard/suggestions/e;


# direct methods
.method public constructor <init>(Lcom/android/settings/dashboard/suggestions/e;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/android/settings/dashboard/suggestions/c;->Io:Lcom/android/settings/dashboard/suggestions/e;

    return-void
.end method

.method private static BA(Z)D
    .locals 2

    if-eqz p0, :cond_0

    const/4 v0, 0x1

    :goto_0
    int-to-double v0, v0

    return-wide v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static BC(J)D
    .locals 6

    const-wide/high16 v0, 0x3ff0000000000000L    # 1.0

    long-to-double v2, p0

    const-wide v4, 0x407f400000000000L    # 500.0

    div-double/2addr v2, v4

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->min(DD)D

    move-result-wide v0

    return-wide v0
.end method

.method private static BD(JJ)D
    .locals 6

    const-wide/high16 v0, 0x3ff0000000000000L    # 1.0

    sub-long v2, p0, p2

    long-to-double v2, v2

    const-wide v4, 0x4212a05f20000000L    # 2.0E10

    div-double/2addr v2, v4

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->min(DD)D

    move-result-wide v0

    return-wide v0
.end method


# virtual methods
.method public BB(Ljava/util/List;)Ljava/util/Map;
    .locals 14

    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    new-instance v5, Ljava/util/HashMap;

    invoke-direct {v5}, Ljava/util/HashMap;-><init>()V

    invoke-interface {v2, v0, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v1, p0, Lcom/android/settings/dashboard/suggestions/c;->Io:Lcom/android/settings/dashboard/suggestions/e;

    const-string/jumbo v6, "shown"

    const-string/jumbo v7, "last_event_time"

    invoke-virtual {v1, v0, v6, v7}, Lcom/android/settings/dashboard/suggestions/e;->BK(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    iget-object v1, p0, Lcom/android/settings/dashboard/suggestions/c;->Io:Lcom/android/settings/dashboard/suggestions/e;

    const-string/jumbo v7, "dismissed"

    const-string/jumbo v8, "last_event_time"

    invoke-virtual {v1, v0, v7, v8}, Lcom/android/settings/dashboard/suggestions/e;->BK(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    iget-object v1, p0, Lcom/android/settings/dashboard/suggestions/c;->Io:Lcom/android/settings/dashboard/suggestions/e;

    const-string/jumbo v8, "clicked"

    const-string/jumbo v9, "last_event_time"

    invoke-virtual {v1, v0, v8, v9}, Lcom/android/settings/dashboard/suggestions/e;->BK(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    const-string/jumbo v9, "is_shown"

    invoke-virtual {v6}, Ljava/lang/Long;->longValue()J

    move-result-wide v10

    const-wide/16 v12, 0x0

    cmp-long v1, v10, v12

    if-lez v1, :cond_0

    const/4 v1, 0x1

    :goto_1
    invoke-static {v1}, Lcom/android/settings/dashboard/suggestions/c;->BA(Z)D

    move-result-wide v10

    invoke-static {v10, v11}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v1

    invoke-interface {v5, v9, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string/jumbo v9, "is_dismissed"

    invoke-virtual {v7}, Ljava/lang/Long;->longValue()J

    move-result-wide v10

    const-wide/16 v12, 0x0

    cmp-long v1, v10, v12

    if-lez v1, :cond_1

    const/4 v1, 0x1

    :goto_2
    invoke-static {v1}, Lcom/android/settings/dashboard/suggestions/c;->BA(Z)D

    move-result-wide v10

    invoke-static {v10, v11}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v1

    invoke-interface {v5, v9, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string/jumbo v9, "is_clicked"

    invoke-virtual {v8}, Ljava/lang/Long;->longValue()J

    move-result-wide v10

    const-wide/16 v12, 0x0

    cmp-long v1, v10, v12

    if-lez v1, :cond_2

    const/4 v1, 0x1

    :goto_3
    invoke-static {v1}, Lcom/android/settings/dashboard/suggestions/c;->BA(Z)D

    move-result-wide v10

    invoke-static {v10, v11}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v1

    invoke-interface {v5, v9, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string/jumbo v1, "time_from_last_shown"

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v10

    invoke-virtual {v6}, Ljava/lang/Long;->longValue()J

    move-result-wide v12

    invoke-static {v10, v11, v12, v13}, Lcom/android/settings/dashboard/suggestions/c;->BD(JJ)D

    move-result-wide v10

    invoke-static {v10, v11}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v6

    invoke-interface {v5, v1, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string/jumbo v1, "time_from_last_dismissed"

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v10

    invoke-virtual {v7}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    invoke-static {v10, v11, v6, v7}, Lcom/android/settings/dashboard/suggestions/c;->BD(JJ)D

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v6

    invoke-interface {v5, v1, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string/jumbo v1, "time_from_last_clicked"

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    invoke-virtual {v8}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    invoke-static {v6, v7, v8, v9}, Lcom/android/settings/dashboard/suggestions/c;->BD(JJ)D

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v6

    invoke-interface {v5, v1, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string/jumbo v1, "shown_count"

    iget-object v6, p0, Lcom/android/settings/dashboard/suggestions/c;->Io:Lcom/android/settings/dashboard/suggestions/e;

    const-string/jumbo v7, "shown"

    const-string/jumbo v8, "count"

    invoke-virtual {v6, v0, v7, v8}, Lcom/android/settings/dashboard/suggestions/e;->BK(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)J

    move-result-wide v6

    invoke-static {v6, v7}, Lcom/android/settings/dashboard/suggestions/c;->BC(J)D

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v6

    invoke-interface {v5, v1, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string/jumbo v1, "dismissed_count"

    iget-object v6, p0, Lcom/android/settings/dashboard/suggestions/c;->Io:Lcom/android/settings/dashboard/suggestions/e;

    const-string/jumbo v7, "dismissed"

    const-string/jumbo v8, "count"

    invoke-virtual {v6, v0, v7, v8}, Lcom/android/settings/dashboard/suggestions/e;->BK(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)J

    move-result-wide v6

    invoke-static {v6, v7}, Lcom/android/settings/dashboard/suggestions/c;->BC(J)D

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v6

    invoke-interface {v5, v1, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string/jumbo v1, "clicked_count"

    iget-object v6, p0, Lcom/android/settings/dashboard/suggestions/c;->Io:Lcom/android/settings/dashboard/suggestions/e;

    const-string/jumbo v7, "clicked"

    const-string/jumbo v8, "count"

    invoke-virtual {v6, v0, v7, v8}, Lcom/android/settings/dashboard/suggestions/e;->BK(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)J

    move-result-wide v6

    invoke-static {v6, v7}, Lcom/android/settings/dashboard/suggestions/c;->BC(J)D

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    invoke-interface {v5, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_0

    :cond_0
    const/4 v1, 0x0

    goto/16 :goto_1

    :cond_1
    const/4 v1, 0x0

    goto/16 :goto_2

    :cond_2
    const/4 v1, 0x0

    goto/16 :goto_3

    :cond_3
    return-object v2
.end method
