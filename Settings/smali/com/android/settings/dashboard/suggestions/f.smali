.class public Lcom/android/settings/dashboard/suggestions/f;
.super Landroid/support/v7/widget/a/d;
.source "SuggestionDismissController.java"


# instance fields
.field private final It:Lcom/android/settings/dashboard/suggestions/g;

.field private final Iu:Lcom/android/settings/dashboard/suggestions/a;

.field private final Iv:Lcom/android/settingslib/z;

.field private final mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/support/v7/widget/RecyclerView;Lcom/android/settingslib/z;Lcom/android/settings/dashboard/suggestions/g;)V
    .locals 2

    const/4 v0, 0x0

    const/16 v1, 0x30

    invoke-direct {p0, v0, v1}, Landroid/support/v7/widget/a/d;-><init>(II)V

    iput-object p1, p0, Lcom/android/settings/dashboard/suggestions/f;->mContext:Landroid/content/Context;

    iput-object p3, p0, Lcom/android/settings/dashboard/suggestions/f;->Iv:Lcom/android/settingslib/z;

    invoke-static {p1}, Lcom/android/settings/overlay/a;->aIk(Landroid/content/Context;)Lcom/android/settings/overlay/a;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/android/settings/overlay/a;->aIv(Landroid/content/Context;)Lcom/android/settings/dashboard/suggestions/a;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/dashboard/suggestions/f;->Iu:Lcom/android/settings/dashboard/suggestions/a;

    iput-object p4, p0, Lcom/android/settings/dashboard/suggestions/f;->It:Lcom/android/settings/dashboard/suggestions/g;

    new-instance v0, Landroid/support/v7/widget/a/b;

    invoke-direct {v0, p0}, Landroid/support/v7/widget/a/b;-><init>(Landroid/support/v7/widget/a/c;)V

    invoke-virtual {v0, p2}, Landroid/support/v7/widget/a/b;->dnm(Landroid/support/v7/widget/RecyclerView;)V

    return-void
.end method


# virtual methods
.method public Br(Landroid/support/v7/widget/RecyclerView;Landroid/support/v7/widget/p;)I
    .locals 2

    invoke-virtual {p2}, Landroid/support/v7/widget/p;->getItemViewType()I

    move-result v0

    const v1, 0x7f0d01f4

    if-ne v0, v1, :cond_0

    invoke-super {p0, p1, p2}, Landroid/support/v7/widget/a/d;->Br(Landroid/support/v7/widget/RecyclerView;Landroid/support/v7/widget/p;)I

    move-result v0

    return v0

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public Bs(Landroid/support/v7/widget/RecyclerView;Landroid/support/v7/widget/p;Landroid/support/v7/widget/p;)Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public Bt(Landroid/support/v7/widget/p;I)V
    .locals 4

    iget-object v0, p0, Lcom/android/settings/dashboard/suggestions/f;->It:Lcom/android/settings/dashboard/suggestions/g;

    if-nez v0, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/settings/dashboard/suggestions/f;->It:Lcom/android/settings/dashboard/suggestions/g;

    invoke-virtual {p1}, Landroid/support/v7/widget/p;->getAdapterPosition()I

    move-result v1

    invoke-interface {v0, v1}, Lcom/android/settings/dashboard/suggestions/g;->BP(I)Lcom/android/settingslib/drawer/Tile;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/dashboard/suggestions/f;->Iu:Lcom/android/settings/dashboard/suggestions/a;

    iget-object v2, p0, Lcom/android/settings/dashboard/suggestions/f;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/android/settings/dashboard/suggestions/f;->Iv:Lcom/android/settingslib/z;

    invoke-interface {v1, v2, v3, v0}, Lcom/android/settings/dashboard/suggestions/a;->Bu(Landroid/content/Context;Lcom/android/settingslib/z;Lcom/android/settingslib/drawer/Tile;)V

    iget-object v1, p0, Lcom/android/settings/dashboard/suggestions/f;->It:Lcom/android/settings/dashboard/suggestions/g;

    invoke-interface {v1, v0}, Lcom/android/settings/dashboard/suggestions/g;->BQ(Lcom/android/settingslib/drawer/Tile;)V

    return-void
.end method
