.class final Lcom/android/settings/dashboard/B;
.super Lcom/android/settings/dashboard/z;
.source "SupportItemAdapter.java"


# instance fields
.field private KU:Ljava/util/List;

.field private KV:Lcom/android/settings/support/SupportPhone;

.field private KW:Lcom/android/settings/support/SupportPhone;


# direct methods
.method constructor <init>(Landroid/content/Context;)V
    .locals 1

    const v0, 0x7f0d01f9

    invoke-direct {p0, p1, v0}, Lcom/android/settings/dashboard/z;-><init>(Landroid/content/Context;I)V

    return-void
.end method

.method static synthetic EN(Lcom/android/settings/dashboard/B;)Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/dashboard/B;->KU:Ljava/util/List;

    return-object v0
.end method

.method static synthetic EO(Lcom/android/settings/dashboard/B;)Lcom/android/settings/support/SupportPhone;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/dashboard/B;->KV:Lcom/android/settings/support/SupportPhone;

    return-object v0
.end method

.method static synthetic EP(Lcom/android/settings/dashboard/B;)Lcom/android/settings/support/SupportPhone;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/dashboard/B;->KW:Lcom/android/settings/support/SupportPhone;

    return-object v0
.end method


# virtual methods
.method EK(Ljava/util/List;)Lcom/android/settings/dashboard/B;
    .locals 0

    iput-object p1, p0, Lcom/android/settings/dashboard/B;->KU:Ljava/util/List;

    return-object p0
.end method

.method EL(Lcom/android/settings/support/SupportPhone;)Lcom/android/settings/dashboard/B;
    .locals 0

    iput-object p1, p0, Lcom/android/settings/dashboard/B;->KV:Lcom/android/settings/support/SupportPhone;

    return-object p0
.end method

.method EM(Lcom/android/settings/support/SupportPhone;)Lcom/android/settings/dashboard/B;
    .locals 0

    iput-object p1, p0, Lcom/android/settings/dashboard/B;->KW:Lcom/android/settings/support/SupportPhone;

    return-object p0
.end method

.method build()Lcom/android/settings/dashboard/A;
    .locals 2

    new-instance v0, Lcom/android/settings/dashboard/A;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/android/settings/dashboard/A;-><init>(Lcom/android/settings/dashboard/B;Lcom/android/settings/dashboard/A;)V

    return-object v0
.end method

.method bridge synthetic build()Lcom/android/settings/dashboard/SupportItemAdapter$EscalationData;
    .locals 1

    invoke-virtual {p0}, Lcom/android/settings/dashboard/B;->build()Lcom/android/settings/dashboard/A;

    move-result-object v0

    return-object v0
.end method

.method bridge synthetic build()Lcom/android/settings/dashboard/SupportItemAdapter$SupportData;
    .locals 1

    invoke-virtual {p0}, Lcom/android/settings/dashboard/B;->build()Lcom/android/settings/dashboard/A;

    move-result-object v0

    return-object v0
.end method
