.class final Lcom/android/settings/dashboard/L;
.super Ljava/lang/Object;
.source "SummaryLoader.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic Lt:Lcom/android/settings/dashboard/C;

.field final synthetic Lu:Landroid/content/ComponentName;

.field final synthetic Lv:Ljava/lang/CharSequence;


# direct methods
.method constructor <init>(Lcom/android/settings/dashboard/C;Landroid/content/ComponentName;Ljava/lang/CharSequence;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/dashboard/L;->Lt:Lcom/android/settings/dashboard/C;

    iput-object p2, p0, Lcom/android/settings/dashboard/L;->Lu:Landroid/content/ComponentName;

    iput-object p3, p0, Lcom/android/settings/dashboard/L;->Lv:Ljava/lang/CharSequence;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    iget-object v0, p0, Lcom/android/settings/dashboard/L;->Lt:Lcom/android/settings/dashboard/C;

    iget-object v1, p0, Lcom/android/settings/dashboard/L;->Lt:Lcom/android/settings/dashboard/C;

    invoke-static {v1}, Lcom/android/settings/dashboard/C;->Fk(Lcom/android/settings/dashboard/C;)Lcom/android/settings/dashboard/n;

    move-result-object v1

    iget-object v2, p0, Lcom/android/settings/dashboard/L;->Lt:Lcom/android/settings/dashboard/C;

    invoke-static {v2}, Lcom/android/settings/dashboard/C;->Fj(Lcom/android/settings/dashboard/C;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/android/settings/dashboard/n;->De(Ljava/lang/String;)Lcom/android/settingslib/drawer/DashboardCategory;

    move-result-object v1

    iget-object v2, p0, Lcom/android/settings/dashboard/L;->Lu:Landroid/content/ComponentName;

    invoke-static {v0, v1, v2}, Lcom/android/settings/dashboard/C;->Fl(Lcom/android/settings/dashboard/C;Lcom/android/settingslib/drawer/DashboardCategory;Landroid/content/ComponentName;)Lcom/android/settingslib/drawer/Tile;

    move-result-object v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    iget-object v1, p0, Lcom/android/settings/dashboard/L;->Lt:Lcom/android/settings/dashboard/C;

    iget-object v2, p0, Lcom/android/settings/dashboard/L;->Lv:Ljava/lang/CharSequence;

    invoke-virtual {v1, v0, v2}, Lcom/android/settings/dashboard/C;->updateSummaryIfNeeded(Lcom/android/settingslib/drawer/Tile;Ljava/lang/CharSequence;)V

    return-void
.end method
