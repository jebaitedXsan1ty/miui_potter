.class Lcom/android/settings/dashboard/y;
.super Ljava/lang/Object;
.source "SupportItemAdapter.java"


# instance fields
.field private KA:I

.field private KB:Ljava/lang/CharSequence;

.field private KC:I

.field private KD:Ljava/lang/CharSequence;

.field private final KE:I

.field private Kz:I

.field protected final mContext:Landroid/content/Context;

.field private mIntent:Landroid/content/Intent;


# direct methods
.method constructor <init>(Landroid/content/Context;I)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, -0x1

    iput v0, p0, Lcom/android/settings/dashboard/y;->KA:I

    iput-object p1, p0, Lcom/android/settings/dashboard/y;->mContext:Landroid/content/Context;

    iput p2, p0, Lcom/android/settings/dashboard/y;->KE:I

    return-void
.end method

.method static synthetic Er(Lcom/android/settings/dashboard/y;)I
    .locals 1

    iget v0, p0, Lcom/android/settings/dashboard/y;->Kz:I

    return v0
.end method

.method static synthetic Es(Lcom/android/settings/dashboard/y;)Landroid/content/Intent;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/dashboard/y;->mIntent:Landroid/content/Intent;

    return-object v0
.end method

.method static synthetic Et(Lcom/android/settings/dashboard/y;)I
    .locals 1

    iget v0, p0, Lcom/android/settings/dashboard/y;->KA:I

    return v0
.end method

.method static synthetic Eu(Lcom/android/settings/dashboard/y;)Ljava/lang/CharSequence;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/dashboard/y;->KB:Ljava/lang/CharSequence;

    return-object v0
.end method

.method static synthetic Ev(Lcom/android/settings/dashboard/y;)I
    .locals 1

    iget v0, p0, Lcom/android/settings/dashboard/y;->KC:I

    return v0
.end method

.method static synthetic Ew(Lcom/android/settings/dashboard/y;)Ljava/lang/CharSequence;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/dashboard/y;->KD:Ljava/lang/CharSequence;

    return-object v0
.end method

.method static synthetic Ex(Lcom/android/settings/dashboard/y;)I
    .locals 1

    iget v0, p0, Lcom/android/settings/dashboard/y;->KE:I

    return v0
.end method


# virtual methods
.method Em(I)Lcom/android/settings/dashboard/y;
    .locals 0

    iput p1, p0, Lcom/android/settings/dashboard/y;->KA:I

    return-object p0
.end method

.method En(I)Lcom/android/settings/dashboard/y;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/dashboard/y;->mContext:Landroid/content/Context;

    invoke-virtual {v0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/dashboard/y;->KB:Ljava/lang/CharSequence;

    return-object p0
.end method

.method Eo(Ljava/lang/CharSequence;)Lcom/android/settings/dashboard/y;
    .locals 0

    iput-object p1, p0, Lcom/android/settings/dashboard/y;->KB:Ljava/lang/CharSequence;

    return-object p0
.end method

.method Ep(I)Lcom/android/settings/dashboard/y;
    .locals 0

    iput p1, p0, Lcom/android/settings/dashboard/y;->KC:I

    return-object p0
.end method

.method Eq(I)Lcom/android/settings/dashboard/y;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/dashboard/y;->mContext:Landroid/content/Context;

    invoke-virtual {v0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/dashboard/y;->KD:Ljava/lang/CharSequence;

    return-object p0
.end method

.method build()Lcom/android/settings/dashboard/SupportItemAdapter$SupportData;
    .locals 2

    new-instance v0, Lcom/android/settings/dashboard/SupportItemAdapter$SupportData;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/android/settings/dashboard/SupportItemAdapter$SupportData;-><init>(Lcom/android/settings/dashboard/y;Lcom/android/settings/dashboard/SupportItemAdapter$SupportData;)V

    return-object v0
.end method

.method setIcon(I)Lcom/android/settings/dashboard/y;
    .locals 0

    iput p1, p0, Lcom/android/settings/dashboard/y;->Kz:I

    return-object p0
.end method

.method setIntent(Landroid/content/Intent;)Lcom/android/settings/dashboard/y;
    .locals 0

    iput-object p1, p0, Lcom/android/settings/dashboard/y;->mIntent:Landroid/content/Intent;

    return-object p0
.end method
