.class public final Lcom/android/settings/dashboard/t;
.super Landroid/support/v7/widget/b;
.source "SupportItemAdapter.java"


# instance fields
.field private Ka:[Landroid/accounts/Account;

.field private final Kb:Landroid/app/Activity;

.field private final Kc:Lcom/android/settings/dashboard/u;

.field private Kd:Z

.field private final Ke:Landroid/view/View$OnClickListener;

.field private final Kf:Lcom/android/settings/dashboard/v;

.field private final Kg:Lcom/android/settings/dashboard/w;

.field private Kh:I

.field private Ki:Ljava/lang/String;

.field private final Kj:Ljava/util/List;

.field private final Kk:Lcom/android/settings/overlay/b;

.field private final mMetricsFeatureProvider:Lcom/android/settings/core/instrumentation/e;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Landroid/os/Bundle;Lcom/android/settings/overlay/b;Lcom/android/settings/core/instrumentation/e;Landroid/view/View$OnClickListener;)V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Landroid/support/v7/widget/b;-><init>()V

    iput-object p1, p0, Lcom/android/settings/dashboard/t;->Kb:Landroid/app/Activity;

    iput-object p3, p0, Lcom/android/settings/dashboard/t;->Kk:Lcom/android/settings/overlay/b;

    iput-object p4, p0, Lcom/android/settings/dashboard/t;->mMetricsFeatureProvider:Lcom/android/settings/core/instrumentation/e;

    iput-object p5, p0, Lcom/android/settings/dashboard/t;->Ke:Landroid/view/View$OnClickListener;

    new-instance v0, Lcom/android/settings/dashboard/u;

    invoke-direct {v0, p0, v1}, Lcom/android/settings/dashboard/u;-><init>(Lcom/android/settings/dashboard/t;Lcom/android/settings/dashboard/u;)V

    iput-object v0, p0, Lcom/android/settings/dashboard/t;->Kc:Lcom/android/settings/dashboard/u;

    new-instance v0, Lcom/android/settings/dashboard/v;

    invoke-direct {v0, p0, v1}, Lcom/android/settings/dashboard/v;-><init>(Lcom/android/settings/dashboard/t;Lcom/android/settings/dashboard/v;)V

    iput-object v0, p0, Lcom/android/settings/dashboard/t;->Kf:Lcom/android/settings/dashboard/v;

    new-instance v0, Lcom/android/settings/dashboard/w;

    invoke-direct {v0, p0, v1}, Lcom/android/settings/dashboard/w;-><init>(Lcom/android/settings/dashboard/t;Lcom/android/settings/dashboard/w;)V

    iput-object v0, p0, Lcom/android/settings/dashboard/t;->Kg:Lcom/android/settings/dashboard/w;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/settings/dashboard/t;->Kj:Ljava/util/List;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/settings/dashboard/t;->Kd:Z

    if-eqz p2, :cond_0

    const-string/jumbo v0, "STATE_SELECTED_COUNTRY"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/dashboard/t;->Ki:Ljava/lang/String;

    const-string/jumbo v0, "ACCOUNT_SELECTED_INDEX"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/android/settings/dashboard/t;->Kh:I

    :goto_0
    iget-object v0, p0, Lcom/android/settings/dashboard/t;->Kk:Lcom/android/settings/overlay/b;

    iget-object v1, p0, Lcom/android/settings/dashboard/t;->Kb:Landroid/app/Activity;

    invoke-interface {v0, v1}, Lcom/android/settings/overlay/b;->aIC(Landroid/content/Context;)[Landroid/accounts/Account;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/dashboard/t;->Ka:[Landroid/accounts/Account;

    invoke-virtual {p0}, Lcom/android/settings/dashboard/t;->DL()V

    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/settings/dashboard/t;->Kk:Lcom/android/settings/overlay/b;

    const/4 v1, 0x2

    invoke-interface {v0, v1}, Lcom/android/settings/overlay/b;->aIE(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/dashboard/t;->Ki:Ljava/lang/String;

    const/4 v0, 0x0

    iput v0, p0, Lcom/android/settings/dashboard/t;->Kh:I

    goto :goto_0
.end method

.method private DO()V
    .locals 1

    iget-object v0, p0, Lcom/android/settings/dashboard/t;->Ka:[Landroid/accounts/Account;

    array-length v0, v0

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/android/settings/dashboard/t;->DS()V

    :goto_0
    return-void

    :cond_0
    iget-boolean v0, p0, Lcom/android/settings/dashboard/t;->Kd:Z

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lcom/android/settings/dashboard/t;->DR()V

    goto :goto_0

    :cond_1
    invoke-direct {p0}, Lcom/android/settings/dashboard/t;->DQ()V

    goto :goto_0
.end method

.method private DP()V
    .locals 7

    const v6, 0x7f0d01fd

    const/4 v5, 0x0

    iget-object v0, p0, Lcom/android/settings/dashboard/t;->Kj:Ljava/util/List;

    new-instance v1, Lcom/android/settings/dashboard/y;

    iget-object v2, p0, Lcom/android/settings/dashboard/t;->Kb:Landroid/app/Activity;

    const v3, 0x7f0d01fe

    invoke-direct {v1, v2, v3}, Lcom/android/settings/dashboard/y;-><init>(Landroid/content/Context;I)V

    invoke-virtual {v1}, Lcom/android/settings/dashboard/y;->build()Lcom/android/settings/dashboard/SupportItemAdapter$SupportData;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/android/settings/dashboard/t;->Kb:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/dashboard/t;->Kk:Lcom/android/settings/overlay/b;

    iget-object v2, p0, Lcom/android/settings/dashboard/t;->Kb:Landroid/app/Activity;

    invoke-interface {v1, v2}, Lcom/android/settings/overlay/b;->aIG(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1, v5}, Landroid/content/pm/PackageManager;->resolveActivity(Landroid/content/Intent;I)Landroid/content/pm/ResolveInfo;

    move-result-object v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/android/settings/dashboard/t;->Kj:Ljava/util/List;

    new-instance v3, Lcom/android/settings/dashboard/y;

    iget-object v4, p0, Lcom/android/settings/dashboard/t;->Kb:Landroid/app/Activity;

    invoke-direct {v3, v4, v6}, Lcom/android/settings/dashboard/y;-><init>(Landroid/content/Context;I)V

    const v4, 0x7f0801e4

    invoke-virtual {v3, v4}, Lcom/android/settings/dashboard/y;->setIcon(I)Lcom/android/settings/dashboard/y;

    move-result-object v3

    const v4, 0x7f1211ff

    invoke-virtual {v3, v4}, Lcom/android/settings/dashboard/y;->Ep(I)Lcom/android/settings/dashboard/y;

    move-result-object v3

    invoke-virtual {v3, v1}, Lcom/android/settings/dashboard/y;->setIntent(Landroid/content/Intent;)Lcom/android/settings/dashboard/y;

    move-result-object v1

    const/16 v3, 0x1df

    invoke-virtual {v1, v3}, Lcom/android/settings/dashboard/y;->Em(I)Lcom/android/settings/dashboard/y;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/settings/dashboard/y;->build()Lcom/android/settings/dashboard/SupportItemAdapter$SupportData;

    move-result-object v1

    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    iget-object v1, p0, Lcom/android/settings/dashboard/t;->Kk:Lcom/android/settings/overlay/b;

    iget-object v2, p0, Lcom/android/settings/dashboard/t;->Kb:Landroid/app/Activity;

    invoke-interface {v1, v2}, Lcom/android/settings/overlay/b;->aIM(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1, v5}, Landroid/content/pm/PackageManager;->resolveActivity(Landroid/content/Intent;I)Landroid/content/pm/ResolveInfo;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/settings/dashboard/t;->Kj:Ljava/util/List;

    new-instance v2, Lcom/android/settings/dashboard/y;

    iget-object v3, p0, Lcom/android/settings/dashboard/t;->Kb:Landroid/app/Activity;

    invoke-direct {v2, v3, v6}, Lcom/android/settings/dashboard/y;-><init>(Landroid/content/Context;I)V

    const v3, 0x7f0801f9

    invoke-virtual {v2, v3}, Lcom/android/settings/dashboard/y;->setIcon(I)Lcom/android/settings/dashboard/y;

    move-result-object v2

    const v3, 0x7f12120a

    invoke-virtual {v2, v3}, Lcom/android/settings/dashboard/y;->Ep(I)Lcom/android/settings/dashboard/y;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/android/settings/dashboard/y;->setIntent(Landroid/content/Intent;)Lcom/android/settings/dashboard/y;

    move-result-object v1

    const/16 v2, 0x1de

    invoke-virtual {v1, v2}, Lcom/android/settings/dashboard/y;->Em(I)Lcom/android/settings/dashboard/y;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/settings/dashboard/y;->build()Lcom/android/settings/dashboard/SupportItemAdapter$SupportData;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_1
    return-void
.end method

.method private DQ()V
    .locals 9

    const v2, 0x7f1211fc

    const/4 v5, 0x2

    const/4 v8, 0x0

    iget-object v0, p0, Lcom/android/settings/dashboard/t;->Kk:Lcom/android/settings/overlay/b;

    iget-object v1, p0, Lcom/android/settings/dashboard/t;->Ki:Ljava/lang/String;

    invoke-interface {v0, v5, v1}, Lcom/android/settings/overlay/b;->aIN(ILjava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    iget-object v0, p0, Lcom/android/settings/dashboard/t;->Kb:Landroid/app/Activity;

    const v1, 0x7f1211f5

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    iget-object v4, p0, Lcom/android/settings/dashboard/t;->Kj:Ljava/util/List;

    new-instance v1, Lcom/android/settings/dashboard/B;

    iget-object v5, p0, Lcom/android/settings/dashboard/t;->Kb:Landroid/app/Activity;

    invoke-direct {v1, v5}, Lcom/android/settings/dashboard/B;-><init>(Landroid/content/Context;)V

    iget-object v5, p0, Lcom/android/settings/dashboard/t;->Kk:Lcom/android/settings/overlay/b;

    invoke-interface {v5}, Lcom/android/settings/overlay/b;->aII()Ljava/util/List;

    move-result-object v5

    invoke-virtual {v1, v5}, Lcom/android/settings/dashboard/B;->EK(Ljava/util/List;)Lcom/android/settings/dashboard/B;

    move-result-object v1

    iget-object v5, p0, Lcom/android/settings/dashboard/t;->Kk:Lcom/android/settings/overlay/b;

    iget-object v6, p0, Lcom/android/settings/dashboard/t;->Ki:Ljava/lang/String;

    const/4 v7, 0x1

    invoke-interface {v5, v6, v7}, Lcom/android/settings/overlay/b;->aIL(Ljava/lang/String;Z)Lcom/android/settings/support/SupportPhone;

    move-result-object v5

    invoke-virtual {v1, v5}, Lcom/android/settings/dashboard/B;->EL(Lcom/android/settings/support/SupportPhone;)Lcom/android/settings/dashboard/B;

    move-result-object v1

    iget-object v5, p0, Lcom/android/settings/dashboard/t;->Kk:Lcom/android/settings/overlay/b;

    iget-object v6, p0, Lcom/android/settings/dashboard/t;->Ki:Ljava/lang/String;

    invoke-interface {v5, v6, v8}, Lcom/android/settings/overlay/b;->aIL(Ljava/lang/String;Z)Lcom/android/settings/support/SupportPhone;

    move-result-object v5

    invoke-virtual {v1, v5}, Lcom/android/settings/dashboard/B;->EM(Lcom/android/settings/support/SupportPhone;)Lcom/android/settings/dashboard/B;

    move-result-object v5

    if-eqz v3, :cond_2

    const v1, 0x7f1211f6

    :goto_1
    invoke-virtual {v5, v1}, Lcom/android/settings/dashboard/B;->Ep(I)Lcom/android/settings/dashboard/y;

    move-result-object v1

    if-eqz v3, :cond_0

    const v2, 0x7f1211f4

    :cond_0
    invoke-virtual {v1, v2}, Lcom/android/settings/dashboard/y;->Eq(I)Lcom/android/settings/dashboard/y;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/android/settings/dashboard/y;->Eo(Ljava/lang/CharSequence;)Lcom/android/settings/dashboard/y;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/settings/dashboard/y;->build()Lcom/android/settings/dashboard/SupportItemAdapter$SupportData;

    move-result-object v0

    invoke-interface {v4, v8, v0}, Ljava/util/List;->add(ILjava/lang/Object;)V

    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/settings/dashboard/t;->Kk:Lcom/android/settings/overlay/b;

    iget-object v1, p0, Lcom/android/settings/dashboard/t;->Kb:Landroid/app/Activity;

    iget-object v4, p0, Lcom/android/settings/dashboard/t;->Ki:Ljava/lang/String;

    invoke-interface {v0, v1, v5, v4, v8}, Lcom/android/settings/overlay/b;->aIH(Landroid/content/Context;ILjava/lang/String;Z)Ljava/lang/CharSequence;

    move-result-object v0

    goto :goto_0

    :cond_2
    move v1, v2

    goto :goto_1
.end method

.method private DR()V
    .locals 10

    const/4 v9, 0x0

    const v4, 0x7f1211fc

    const/4 v8, 0x3

    const/4 v7, 0x2

    iget-object v0, p0, Lcom/android/settings/dashboard/t;->Kk:Lcom/android/settings/overlay/b;

    iget-object v1, p0, Lcom/android/settings/dashboard/t;->Kb:Landroid/app/Activity;

    invoke-interface {v0, v1, v7}, Lcom/android/settings/overlay/b;->aIP(Landroid/content/Context;I)Z

    move-result v0

    iget-object v1, p0, Lcom/android/settings/dashboard/t;->Kk:Lcom/android/settings/overlay/b;

    iget-object v2, p0, Lcom/android/settings/dashboard/t;->Kb:Landroid/app/Activity;

    invoke-interface {v1, v2, v8}, Lcom/android/settings/overlay/b;->aIP(Landroid/content/Context;I)Z

    move-result v1

    new-instance v2, Lcom/android/settings/dashboard/z;

    iget-object v3, p0, Lcom/android/settings/dashboard/t;->Kb:Landroid/app/Activity;

    invoke-direct {v2, v3}, Lcom/android/settings/dashboard/z;-><init>(Landroid/content/Context;)V

    if-nez v0, :cond_2

    xor-int/lit8 v3, v1, 0x1

    if-eqz v3, :cond_2

    invoke-virtual {v2, v4}, Lcom/android/settings/dashboard/z;->Ep(I)Lcom/android/settings/dashboard/y;

    move-result-object v3

    const v4, 0x7f1211fd

    invoke-virtual {v3, v4}, Lcom/android/settings/dashboard/y;->En(I)Lcom/android/settings/dashboard/y;

    :goto_0
    if-eqz v0, :cond_0

    const v0, 0x7f1211f8

    invoke-virtual {v2, v0}, Lcom/android/settings/dashboard/z;->EC(I)Lcom/android/settings/dashboard/z;

    move-result-object v0

    iget-object v3, p0, Lcom/android/settings/dashboard/t;->Kk:Lcom/android/settings/overlay/b;

    iget-object v4, p0, Lcom/android/settings/dashboard/t;->Kb:Landroid/app/Activity;

    invoke-interface {v3, v4, v7}, Lcom/android/settings/overlay/b;->aIF(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/android/settings/dashboard/z;->EA(Ljava/lang/String;)Lcom/android/settings/dashboard/z;

    move-result-object v0

    iget-object v3, p0, Lcom/android/settings/dashboard/t;->Kk:Lcom/android/settings/overlay/b;

    invoke-interface {v3, v7}, Lcom/android/settings/overlay/b;->aIO(I)Z

    move-result v3

    invoke-virtual {v0, v3}, Lcom/android/settings/dashboard/z;->Ey(Z)Lcom/android/settings/dashboard/z;

    :cond_0
    if-eqz v1, :cond_1

    const v0, 0x7f1211f7

    invoke-virtual {v2, v0}, Lcom/android/settings/dashboard/z;->ED(I)Lcom/android/settings/dashboard/z;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/dashboard/t;->Kk:Lcom/android/settings/overlay/b;

    iget-object v3, p0, Lcom/android/settings/dashboard/t;->Kb:Landroid/app/Activity;

    invoke-interface {v1, v3, v8}, Lcom/android/settings/overlay/b;->aIF(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/settings/dashboard/z;->EB(Ljava/lang/String;)Lcom/android/settings/dashboard/z;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/dashboard/t;->Kk:Lcom/android/settings/overlay/b;

    invoke-interface {v1, v8}, Lcom/android/settings/overlay/b;->aIO(I)Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/android/settings/dashboard/z;->Ez(Z)Lcom/android/settings/dashboard/z;

    :cond_1
    iget-object v0, p0, Lcom/android/settings/dashboard/t;->Kj:Ljava/util/List;

    invoke-virtual {v2}, Lcom/android/settings/dashboard/z;->build()Lcom/android/settings/dashboard/SupportItemAdapter$EscalationData;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {v0, v2, v1}, Ljava/util/List;->add(ILjava/lang/Object;)V

    return-void

    :cond_2
    iget-object v3, p0, Lcom/android/settings/dashboard/t;->Kk:Lcom/android/settings/overlay/b;

    invoke-interface {v3, v7, v9}, Lcom/android/settings/overlay/b;->aIN(ILjava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_3

    iget-object v3, p0, Lcom/android/settings/dashboard/t;->Kk:Lcom/android/settings/overlay/b;

    invoke-interface {v3, v8, v9}, Lcom/android/settings/overlay/b;->aIN(ILjava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_4

    :cond_3
    const v3, 0x7f1211f6

    invoke-virtual {v2, v3}, Lcom/android/settings/dashboard/z;->Ep(I)Lcom/android/settings/dashboard/y;

    move-result-object v3

    const v4, 0x7f1211f4

    invoke-virtual {v3, v4}, Lcom/android/settings/dashboard/y;->Eq(I)Lcom/android/settings/dashboard/y;

    move-result-object v3

    iget-object v4, p0, Lcom/android/settings/dashboard/t;->Kb:Landroid/app/Activity;

    const v5, 0x7f1211f5

    invoke-virtual {v4, v5}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/android/settings/dashboard/y;->Eo(Ljava/lang/CharSequence;)Lcom/android/settings/dashboard/y;

    goto :goto_0

    :cond_4
    iget-object v3, p0, Lcom/android/settings/dashboard/t;->Kk:Lcom/android/settings/overlay/b;

    invoke-interface {v3, v7}, Lcom/android/settings/overlay/b;->aIO(I)Z

    move-result v3

    if-nez v3, :cond_5

    iget-object v3, p0, Lcom/android/settings/dashboard/t;->Kk:Lcom/android/settings/overlay/b;

    invoke-interface {v3, v8}, Lcom/android/settings/overlay/b;->aIO(I)Z

    move-result v3

    if-eqz v3, :cond_6

    :cond_5
    invoke-virtual {v2, v4}, Lcom/android/settings/dashboard/z;->Ep(I)Lcom/android/settings/dashboard/y;

    move-result-object v3

    const v4, 0x7f1211fb

    invoke-virtual {v3, v4}, Lcom/android/settings/dashboard/y;->En(I)Lcom/android/settings/dashboard/y;

    goto/16 :goto_0

    :cond_6
    invoke-virtual {v2, v4}, Lcom/android/settings/dashboard/z;->Ep(I)Lcom/android/settings/dashboard/y;

    move-result-object v3

    iget-object v4, p0, Lcom/android/settings/dashboard/t;->Kk:Lcom/android/settings/overlay/b;

    iget-object v5, p0, Lcom/android/settings/dashboard/t;->Kb:Landroid/app/Activity;

    const/4 v6, 0x1

    invoke-interface {v4, v5, v7, v9, v6}, Lcom/android/settings/overlay/b;->aIH(Landroid/content/Context;ILjava/lang/String;Z)Ljava/lang/CharSequence;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/android/settings/dashboard/y;->Eo(Ljava/lang/CharSequence;)Lcom/android/settings/dashboard/y;

    goto/16 :goto_0
.end method

.method private DS()V
    .locals 4

    iget-object v0, p0, Lcom/android/settings/dashboard/t;->Kj:Ljava/util/List;

    new-instance v1, Lcom/android/settings/dashboard/z;

    iget-object v2, p0, Lcom/android/settings/dashboard/t;->Kb:Landroid/app/Activity;

    const v3, 0x7f0d01fb

    invoke-direct {v1, v2, v3}, Lcom/android/settings/dashboard/z;-><init>(Landroid/content/Context;I)V

    const v2, 0x7f121204

    invoke-virtual {v1, v2}, Lcom/android/settings/dashboard/z;->EC(I)Lcom/android/settings/dashboard/z;

    move-result-object v1

    const v2, 0x7f121205

    invoke-virtual {v1, v2}, Lcom/android/settings/dashboard/z;->ED(I)Lcom/android/settings/dashboard/z;

    move-result-object v1

    const v2, 0x7f121207

    invoke-virtual {v1, v2}, Lcom/android/settings/dashboard/z;->Ep(I)Lcom/android/settings/dashboard/y;

    move-result-object v1

    const v2, 0x7f121206

    invoke-virtual {v1, v2}, Lcom/android/settings/dashboard/y;->En(I)Lcom/android/settings/dashboard/y;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/settings/dashboard/y;->build()Lcom/android/settings/dashboard/SupportItemAdapter$SupportData;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {v0, v2, v1}, Ljava/util/List;->add(ILjava/lang/Object;)V

    return-void
.end method

.method private DT(Lcom/android/settings/dashboard/x;Lcom/android/settings/dashboard/SupportItemAdapter$EscalationData;)V
    .locals 4

    const/16 v2, 0x8

    const/4 v1, 0x0

    iget-object v0, p1, Lcom/android/settings/dashboard/x;->Kt:Landroid/widget/TextView;

    iget v3, p2, Lcom/android/settings/dashboard/SupportItemAdapter$EscalationData;->Kx:I

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(I)V

    iget-object v0, p1, Lcom/android/settings/dashboard/x;->Kt:Landroid/widget/TextView;

    iget-object v3, p2, Lcom/android/settings/dashboard/SupportItemAdapter$EscalationData;->Ky:Ljava/lang/CharSequence;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    iget-object v0, p1, Lcom/android/settings/dashboard/x;->Ks:Landroid/widget/TextView;

    iget-object v3, p2, Lcom/android/settings/dashboard/SupportItemAdapter$EscalationData;->Kw:Ljava/lang/CharSequence;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget v0, p2, Lcom/android/settings/dashboard/SupportItemAdapter$EscalationData;->KJ:I

    if-nez v0, :cond_2

    iget-object v0, p1, Lcom/android/settings/dashboard/x;->Kq:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    :goto_0
    iget-object v0, p2, Lcom/android/settings/dashboard/SupportItemAdapter$EscalationData;->KK:Ljava/lang/CharSequence;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p1, Lcom/android/settings/dashboard/x;->Kr:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    :goto_1
    iget-object v0, p1, Lcom/android/settings/dashboard/x;->Ko:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/android/settings/dashboard/x;->Ko:Landroid/widget/TextView;

    iget-object v3, p2, Lcom/android/settings/dashboard/SupportItemAdapter$EscalationData;->KH:Ljava/lang/CharSequence;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v3, p1, Lcom/android/settings/dashboard/x;->Ko:Landroid/widget/TextView;

    iget-boolean v0, p0, Lcom/android/settings/dashboard/t;->Kd:Z

    if-eqz v0, :cond_6

    iget-object v0, p2, Lcom/android/settings/dashboard/SupportItemAdapter$EscalationData;->KH:Ljava/lang/CharSequence;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_6

    move v0, v1

    :goto_2
    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setVisibility(I)V

    :cond_0
    iget-object v0, p1, Lcom/android/settings/dashboard/x;->Kp:Landroid/widget/TextView;

    if-eqz v0, :cond_1

    iget-object v0, p1, Lcom/android/settings/dashboard/x;->Kp:Landroid/widget/TextView;

    iget-object v3, p2, Lcom/android/settings/dashboard/SupportItemAdapter$EscalationData;->KI:Ljava/lang/CharSequence;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p1, Lcom/android/settings/dashboard/x;->Kp:Landroid/widget/TextView;

    iget-boolean v3, p0, Lcom/android/settings/dashboard/t;->Kd:Z

    if-eqz v3, :cond_7

    iget-object v3, p2, Lcom/android/settings/dashboard/SupportItemAdapter$EscalationData;->KI:Ljava/lang/CharSequence;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    xor-int/lit8 v3, v3, 0x1

    if-eqz v3, :cond_7

    :goto_3
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    :cond_1
    invoke-virtual {p0, p1}, Lcom/android/settings/dashboard/t;->bindAccountPicker(Lcom/android/settings/dashboard/x;)V

    return-void

    :cond_2
    iget-object v0, p1, Lcom/android/settings/dashboard/x;->Kq:Landroid/widget/TextView;

    iget v3, p2, Lcom/android/settings/dashboard/SupportItemAdapter$EscalationData;->KJ:I

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(I)V

    iget-object v0, p1, Lcom/android/settings/dashboard/x;->Kq:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/android/settings/dashboard/t;->Kc:Lcom/android/settings/dashboard/u;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v3, p1, Lcom/android/settings/dashboard/x;->Kq:Landroid/widget/TextView;

    iget-boolean v0, p2, Lcom/android/settings/dashboard/SupportItemAdapter$EscalationData;->KF:Z

    if-eqz v0, :cond_3

    iget-boolean v0, p0, Lcom/android/settings/dashboard/t;->Kd:Z

    :goto_4
    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setEnabled(Z)V

    iget-object v0, p1, Lcom/android/settings/dashboard/x;->Kq:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_4

    :cond_4
    iget-object v0, p1, Lcom/android/settings/dashboard/x;->Kr:Landroid/widget/TextView;

    iget-object v3, p2, Lcom/android/settings/dashboard/SupportItemAdapter$EscalationData;->KK:Ljava/lang/CharSequence;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p1, Lcom/android/settings/dashboard/x;->Kr:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/android/settings/dashboard/t;->Kc:Lcom/android/settings/dashboard/u;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v3, p1, Lcom/android/settings/dashboard/x;->Kr:Landroid/widget/TextView;

    iget-boolean v0, p2, Lcom/android/settings/dashboard/SupportItemAdapter$EscalationData;->KG:Z

    if-eqz v0, :cond_5

    iget-boolean v0, p0, Lcom/android/settings/dashboard/t;->Kd:Z

    :goto_5
    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setEnabled(Z)V

    iget-object v0, p1, Lcom/android/settings/dashboard/x;->Kr:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_1

    :cond_5
    move v0, v1

    goto :goto_5

    :cond_6
    move v0, v2

    goto :goto_2

    :cond_7
    move v1, v2

    goto :goto_3
.end method

.method private DU(Lcom/android/settings/dashboard/x;Lcom/android/settings/dashboard/A;)V
    .locals 7

    const/4 v3, 0x0

    const/16 v6, 0x8

    iget-object v0, p1, Lcom/android/settings/dashboard/x;->Kt:Landroid/widget/TextView;

    iget v1, p2, Lcom/android/settings/dashboard/A;->Kx:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    iget-object v0, p1, Lcom/android/settings/dashboard/x;->Kt:Landroid/widget/TextView;

    iget-object v1, p2, Lcom/android/settings/dashboard/A;->Ky:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    iget-object v0, p1, Lcom/android/settings/dashboard/x;->Ks:Landroid/widget/TextView;

    iget-object v1, p2, Lcom/android/settings/dashboard/A;->Kw:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p1, Lcom/android/settings/dashboard/x;->itemView:Landroid/view/View;

    const v1, 0x7f0a0420

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    new-instance v1, Landroid/widget/ArrayAdapter;

    iget-object v2, p0, Lcom/android/settings/dashboard/t;->Kb:Landroid/app/Activity;

    iget-object v4, p2, Lcom/android/settings/dashboard/A;->KR:Ljava/util/List;

    const v5, 0x1090009

    invoke-direct {v1, v2, v5, v4}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    iget-object v1, p0, Lcom/android/settings/dashboard/t;->Kk:Lcom/android/settings/overlay/b;

    invoke-interface {v1}, Lcom/android/settings/overlay/b;->aIJ()Ljava/util/List;

    move-result-object v4

    move v2, v3

    :goto_0
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v1

    if-ge v2, v1, :cond_0

    invoke-interface {v4, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    iget-object v5, p0, Lcom/android/settings/dashboard/t;->Ki:Ljava/lang/String;

    invoke-static {v1, v5}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {v0, v2}, Landroid/widget/Spinner;->setSelection(I)V

    :cond_0
    iget-object v1, p0, Lcom/android/settings/dashboard/t;->Kf:Lcom/android/settings/dashboard/v;

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    iget-object v1, p2, Lcom/android/settings/dashboard/A;->KS:Lcom/android/settings/support/SupportPhone;

    if-eqz v1, :cond_3

    iget-object v1, p1, Lcom/android/settings/dashboard/x;->Kq:Landroid/widget/TextView;

    iget-object v2, p2, Lcom/android/settings/dashboard/A;->KS:Lcom/android/settings/support/SupportPhone;

    iget-object v2, v2, Lcom/android/settings/support/SupportPhone;->beQ:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p1, Lcom/android/settings/dashboard/x;->Kq:Landroid/widget/TextView;

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v1, p1, Lcom/android/settings/dashboard/x;->Kq:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/android/settings/dashboard/t;->Kc:Lcom/android/settings/dashboard/u;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :goto_1
    iget-object v1, p2, Lcom/android/settings/dashboard/A;->KT:Lcom/android/settings/support/SupportPhone;

    if-eqz v1, :cond_4

    iget-object v1, p1, Lcom/android/settings/dashboard/x;->Kr:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/android/settings/dashboard/t;->Kb:Landroid/app/Activity;

    const v4, 0x7f121202

    invoke-virtual {v2, v4}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p1, Lcom/android/settings/dashboard/x;->Kr:Landroid/widget/TextView;

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v1, p1, Lcom/android/settings/dashboard/x;->Kr:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/android/settings/dashboard/t;->Kc:Lcom/android/settings/dashboard/u;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :goto_2
    invoke-static {}, Landroid/app/ActivityManager;->isUserAMonkey()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p1, Lcom/android/settings/dashboard/x;->Kq:Landroid/widget/TextView;

    invoke-virtual {v1, v6}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v1, p1, Lcom/android/settings/dashboard/x;->Kr:Landroid/widget/TextView;

    invoke-virtual {v1, v6}, Landroid/widget/TextView;->setVisibility(I)V

    invoke-virtual {v0, v6}, Landroid/widget/Spinner;->setVisibility(I)V

    iget-object v0, p1, Lcom/android/settings/dashboard/x;->itemView:Landroid/view/View;

    const v1, 0x7f0a044d

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/view/View;->setVisibility(I)V

    :cond_1
    return-void

    :cond_2
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_0

    :cond_3
    iget-object v1, p1, Lcom/android/settings/dashboard/x;->Kq:Landroid/widget/TextView;

    invoke-virtual {v1, v6}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_1

    :cond_4
    iget-object v1, p1, Lcom/android/settings/dashboard/x;->Kr:Landroid/widget/TextView;

    invoke-virtual {v1, v6}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_2
.end method

.method private DV(Lcom/android/settings/dashboard/x;Lcom/android/settings/dashboard/SupportItemAdapter$EscalationData;)V
    .locals 2

    iget-object v0, p1, Lcom/android/settings/dashboard/x;->Kt:Landroid/widget/TextView;

    iget v1, p2, Lcom/android/settings/dashboard/SupportItemAdapter$EscalationData;->Kx:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    iget-object v0, p1, Lcom/android/settings/dashboard/x;->Kt:Landroid/widget/TextView;

    iget-object v1, p2, Lcom/android/settings/dashboard/SupportItemAdapter$EscalationData;->Ky:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    iget-object v0, p1, Lcom/android/settings/dashboard/x;->Ks:Landroid/widget/TextView;

    iget-object v1, p2, Lcom/android/settings/dashboard/SupportItemAdapter$EscalationData;->Kw:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p1, Lcom/android/settings/dashboard/x;->Kq:Landroid/widget/TextView;

    iget v1, p2, Lcom/android/settings/dashboard/SupportItemAdapter$EscalationData;->KJ:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    iget-object v0, p1, Lcom/android/settings/dashboard/x;->Kr:Landroid/widget/TextView;

    iget-object v1, p2, Lcom/android/settings/dashboard/SupportItemAdapter$EscalationData;->KK:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p1, Lcom/android/settings/dashboard/x;->Kq:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/android/settings/dashboard/t;->Kc:Lcom/android/settings/dashboard/u;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p1, Lcom/android/settings/dashboard/x;->Kr:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/android/settings/dashboard/t;->Kc:Lcom/android/settings/dashboard/u;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method private DW(Lcom/android/settings/dashboard/x;Lcom/android/settings/dashboard/SupportItemAdapter$SupportData;)V
    .locals 2

    iget-object v0, p1, Lcom/android/settings/dashboard/x;->iconView:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/android/settings/dashboard/x;->iconView:Landroid/widget/ImageView;

    iget v1, p2, Lcom/android/settings/dashboard/SupportItemAdapter$SupportData;->Ku:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    :cond_0
    iget-object v0, p1, Lcom/android/settings/dashboard/x;->Kt:Landroid/widget/TextView;

    if-eqz v0, :cond_1

    iget-object v0, p1, Lcom/android/settings/dashboard/x;->Kt:Landroid/widget/TextView;

    iget v1, p2, Lcom/android/settings/dashboard/SupportItemAdapter$SupportData;->Kx:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    iget-object v0, p1, Lcom/android/settings/dashboard/x;->Kt:Landroid/widget/TextView;

    iget-object v1, p2, Lcom/android/settings/dashboard/SupportItemAdapter$SupportData;->Ky:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    :cond_1
    iget-object v0, p1, Lcom/android/settings/dashboard/x;->Ks:Landroid/widget/TextView;

    if-eqz v0, :cond_2

    iget-object v0, p1, Lcom/android/settings/dashboard/x;->Ks:Landroid/widget/TextView;

    iget-object v1, p2, Lcom/android/settings/dashboard/SupportItemAdapter$SupportData;->Kw:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_2
    iget-object v0, p1, Lcom/android/settings/dashboard/x;->itemView:Landroid/view/View;

    iget-object v1, p0, Lcom/android/settings/dashboard/t;->Ke:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method private DX([Landroid/accounts/Account;)[Ljava/lang/String;
    .locals 4

    array-length v0, p1

    add-int/lit8 v0, v0, 0x1

    new-array v1, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    :goto_0
    array-length v2, p1

    if-ge v0, v2, :cond_0

    aget-object v2, p1, v0

    iget-object v2, v2, Landroid/accounts/Account;->name:Ljava/lang/String;

    aput-object v2, v1, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    array-length v0, p1

    iget-object v2, p0, Lcom/android/settings/dashboard/t;->Kb:Landroid/app/Activity;

    const v3, 0x7f1211ee

    invoke-virtual {v2, v3}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v0

    return-object v1
.end method

.method private DZ()V
    .locals 3

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/android/settings/dashboard/t;->getItemCount()I

    move-result v0

    if-lez v0, :cond_1

    invoke-virtual {p0, v2}, Lcom/android/settings/dashboard/t;->getItemViewType(I)I

    move-result v0

    const v1, 0x7f0d01fb

    if-eq v0, v1, :cond_0

    const v1, 0x7f0d01f7

    if-ne v0, v1, :cond_2

    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/android/settings/dashboard/t;->Kj:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    invoke-direct {p0}, Lcom/android/settings/dashboard/t;->DO()V

    invoke-virtual {p0, v2}, Lcom/android/settings/dashboard/t;->notifyItemChanged(I)V

    :cond_1
    return-void

    :cond_2
    const v1, 0x7f0d01f9

    if-ne v0, v1, :cond_1

    goto :goto_0
.end method

.method private Ea(I)V
    .locals 4

    iget-object v0, p0, Lcom/android/settings/dashboard/t;->Kk:Lcom/android/settings/overlay/b;

    iget-object v1, p0, Lcom/android/settings/dashboard/t;->Kb:Landroid/app/Activity;

    invoke-interface {v0, v1}, Lcom/android/settings/overlay/b;->aIR(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/dashboard/t;->Ka:[Landroid/accounts/Account;

    iget v1, p0, Lcom/android/settings/dashboard/t;->Kh:I

    aget-object v0, v0, v1

    invoke-static {v0, p1}, Lcom/android/settings/support/SupportDisclaimerDialogFragment;->aST(Landroid/accounts/Account;I)Lcom/android/settings/support/SupportDisclaimerDialogFragment;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/dashboard/t;->Kb:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const-string/jumbo v2, "SupportDisclaimerDialog"

    invoke-virtual {v0, v1, v2}, Landroid/app/DialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/settings/dashboard/t;->Kk:Lcom/android/settings/overlay/b;

    iget-object v1, p0, Lcom/android/settings/dashboard/t;->Kb:Landroid/app/Activity;

    iget-object v2, p0, Lcom/android/settings/dashboard/t;->Ka:[Landroid/accounts/Account;

    iget v3, p0, Lcom/android/settings/dashboard/t;->Kh:I

    aget-object v2, v2, v3

    invoke-interface {v0, v1, v2, p1}, Lcom/android/settings/overlay/b;->aIB(Landroid/app/Activity;Landroid/accounts/Account;I)V

    return-void
.end method

.method static synthetic Eb(Lcom/android/settings/dashboard/t;)[Landroid/accounts/Account;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/dashboard/t;->Ka:[Landroid/accounts/Account;

    return-object v0
.end method

.method static synthetic Ec(Lcom/android/settings/dashboard/t;)Landroid/app/Activity;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/dashboard/t;->Kb:Landroid/app/Activity;

    return-object v0
.end method

.method static synthetic Ed(Lcom/android/settings/dashboard/t;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/settings/dashboard/t;->Kd:Z

    return v0
.end method

.method static synthetic Ee(Lcom/android/settings/dashboard/t;)Lcom/android/settings/core/instrumentation/e;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/dashboard/t;->mMetricsFeatureProvider:Lcom/android/settings/core/instrumentation/e;

    return-object v0
.end method

.method static synthetic Ef(Lcom/android/settings/dashboard/t;)I
    .locals 1

    iget v0, p0, Lcom/android/settings/dashboard/t;->Kh:I

    return v0
.end method

.method static synthetic Eg(Lcom/android/settings/dashboard/t;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/dashboard/t;->Ki:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic Eh(Lcom/android/settings/dashboard/t;)Lcom/android/settings/overlay/b;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/dashboard/t;->Kk:Lcom/android/settings/overlay/b;

    return-object v0
.end method

.method static synthetic Ei(Lcom/android/settings/dashboard/t;I)I
    .locals 0

    iput p1, p0, Lcom/android/settings/dashboard/t;->Kh:I

    return p1
.end method

.method static synthetic Ej(Lcom/android/settings/dashboard/t;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    iput-object p1, p0, Lcom/android/settings/dashboard/t;->Ki:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic Ek(Lcom/android/settings/dashboard/t;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/dashboard/t;->DZ()V

    return-void
.end method

.method static synthetic El(Lcom/android/settings/dashboard/t;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/settings/dashboard/t;->Ea(I)V

    return-void
.end method


# virtual methods
.method public DK(Z)V
    .locals 1

    iget-boolean v0, p0, Lcom/android/settings/dashboard/t;->Kd:Z

    if-eq v0, p1, :cond_0

    iput-boolean p1, p0, Lcom/android/settings/dashboard/t;->Kd:Z

    invoke-direct {p0}, Lcom/android/settings/dashboard/t;->DZ()V

    :cond_0
    return-void
.end method

.method DL()V
    .locals 1

    iget-object v0, p0, Lcom/android/settings/dashboard/t;->Kj:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    invoke-direct {p0}, Lcom/android/settings/dashboard/t;->DO()V

    invoke-direct {p0}, Lcom/android/settings/dashboard/t;->DP()V

    invoke-virtual {p0}, Lcom/android/settings/dashboard/t;->notifyDataSetChanged()V

    return-void
.end method

.method public DM([Landroid/accounts/Account;)V
    .locals 3

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/android/settings/dashboard/t;->Ka:[Landroid/accounts/Account;

    invoke-static {v0, p1}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/dashboard/t;->Ka:[Landroid/accounts/Account;

    array-length v0, v0

    if-nez v0, :cond_1

    iput v1, p0, Lcom/android/settings/dashboard/t;->Kh:I

    :goto_0
    iput-object p1, p0, Lcom/android/settings/dashboard/t;->Ka:[Landroid/accounts/Account;

    iget-object v0, p0, Lcom/android/settings/dashboard/t;->Kk:Lcom/android/settings/overlay/b;

    invoke-interface {v0}, Lcom/android/settings/overlay/b;->aIQ()V

    invoke-direct {p0}, Lcom/android/settings/dashboard/t;->DZ()V

    :cond_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/settings/dashboard/t;->Ka:[Landroid/accounts/Account;

    iget v2, p0, Lcom/android/settings/dashboard/t;->Kh:I

    aget-object v0, v0, v2

    invoke-static {p1, v0}, Lcom/android/internal/util/ArrayUtils;->indexOf([Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v0

    const/4 v2, -0x1

    if-eq v0, v2, :cond_2

    :goto_1
    iput v0, p0, Lcom/android/settings/dashboard/t;->Kh:I

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_1
.end method

.method public DN(I)V
    .locals 6

    const/4 v5, 0x0

    if-ltz p1, :cond_1

    iget-object v0, p0, Lcom/android/settings/dashboard/t;->Kj:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge p1, v0, :cond_1

    iget-object v0, p0, Lcom/android/settings/dashboard/t;->Kj:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settings/dashboard/SupportItemAdapter$SupportData;

    iget-object v1, v0, Lcom/android/settings/dashboard/SupportItemAdapter$SupportData;->intent:Landroid/content/Intent;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/android/settings/dashboard/t;->Kb:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    iget-object v2, v0, Lcom/android/settings/dashboard/SupportItemAdapter$SupportData;->intent:Landroid/content/Intent;

    invoke-virtual {v1, v2, v5}, Landroid/content/pm/PackageManager;->resolveActivity(Landroid/content/Intent;I)Landroid/content/pm/ResolveInfo;

    move-result-object v1

    if-eqz v1, :cond_1

    iget v1, v0, Lcom/android/settings/dashboard/SupportItemAdapter$SupportData;->Kv:I

    if-ltz v1, :cond_0

    iget-object v1, p0, Lcom/android/settings/dashboard/t;->mMetricsFeatureProvider:Lcom/android/settings/core/instrumentation/e;

    iget-object v2, p0, Lcom/android/settings/dashboard/t;->Kb:Landroid/app/Activity;

    iget v3, v0, Lcom/android/settings/dashboard/SupportItemAdapter$SupportData;->Kv:I

    new-array v4, v5, [Landroid/util/Pair;

    invoke-virtual {v1, v2, v3, v4}, Lcom/android/settings/core/instrumentation/e;->ajS(Landroid/content/Context;I[Landroid/util/Pair;)V

    :cond_0
    iget-object v1, p0, Lcom/android/settings/dashboard/t;->Kb:Landroid/app/Activity;

    iget-object v0, v0, Lcom/android/settings/dashboard/SupportItemAdapter$SupportData;->intent:Landroid/content/Intent;

    invoke-virtual {v1, v0, v5}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V

    :cond_1
    return-void
.end method

.method public DY(Lcom/android/settings/dashboard/x;I)V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/dashboard/t;->Kj:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settings/dashboard/SupportItemAdapter$SupportData;

    invoke-virtual {p1}, Lcom/android/settings/dashboard/x;->getItemViewType()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    :pswitch_0
    invoke-direct {p0, p1, v0}, Lcom/android/settings/dashboard/t;->DW(Lcom/android/settings/dashboard/x;Lcom/android/settings/dashboard/SupportItemAdapter$SupportData;)V

    :goto_0
    :pswitch_1
    return-void

    :pswitch_2
    check-cast v0, Lcom/android/settings/dashboard/SupportItemAdapter$EscalationData;

    invoke-direct {p0, p1, v0}, Lcom/android/settings/dashboard/t;->DV(Lcom/android/settings/dashboard/x;Lcom/android/settings/dashboard/SupportItemAdapter$EscalationData;)V

    goto :goto_0

    :pswitch_3
    check-cast v0, Lcom/android/settings/dashboard/SupportItemAdapter$EscalationData;

    invoke-direct {p0, p1, v0}, Lcom/android/settings/dashboard/t;->DT(Lcom/android/settings/dashboard/x;Lcom/android/settings/dashboard/SupportItemAdapter$EscalationData;)V

    goto :goto_0

    :pswitch_4
    check-cast v0, Lcom/android/settings/dashboard/A;

    invoke-direct {p0, p1, v0}, Lcom/android/settings/dashboard/t;->DU(Lcom/android/settings/dashboard/x;Lcom/android/settings/dashboard/A;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x7f0d01f7
        :pswitch_3
        :pswitch_0
        :pswitch_4
        :pswitch_0
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public bindAccountPicker(Lcom/android/settings/dashboard/x;)V
    .locals 5

    iget-object v0, p1, Lcom/android/settings/dashboard/x;->itemView:Landroid/view/View;

    const v1, 0x7f0a0014

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    new-instance v1, Landroid/widget/ArrayAdapter;

    iget-object v2, p0, Lcom/android/settings/dashboard/t;->Kb:Landroid/app/Activity;

    iget-object v3, p0, Lcom/android/settings/dashboard/t;->Ka:[Landroid/accounts/Account;

    invoke-direct {p0, v3}, Lcom/android/settings/dashboard/t;->DX([Landroid/accounts/Account;)[Ljava/lang/String;

    move-result-object v3

    const v4, 0x7f0d01f5

    invoke-direct {v1, v2, v4, v3}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I[Ljava/lang/Object;)V

    const v2, 0x1090009

    invoke-virtual {v1, v2}, Landroid/widget/ArrayAdapter;->setDropDownViewResource(I)V

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    iget-object v1, p0, Lcom/android/settings/dashboard/t;->Kg:Lcom/android/settings/dashboard/w;

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    iget v1, p0, Lcom/android/settings/dashboard/t;->Kh:I

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setSelection(I)V

    return-void
.end method

.method public getItemCount()I
    .locals 1

    iget-object v0, p0, Lcom/android/settings/dashboard/t;->Kj:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getItemViewType(I)I
    .locals 1

    iget-object v0, p0, Lcom/android/settings/dashboard/t;->Kj:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settings/dashboard/SupportItemAdapter$SupportData;

    iget v0, v0, Lcom/android/settings/dashboard/SupportItemAdapter$SupportData;->type:I

    return v0
.end method

.method getSupportData()Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/dashboard/t;->Kj:Ljava/util/List;

    return-object v0
.end method

.method public bridge synthetic onBindViewHolder(Landroid/support/v7/widget/p;I)V
    .locals 0

    check-cast p1, Lcom/android/settings/dashboard/x;

    invoke-virtual {p0, p1, p2}, Lcom/android/settings/dashboard/t;->DY(Lcom/android/settings/dashboard/x;I)V

    return-void
.end method

.method public bridge synthetic onCreateViewHolder(Landroid/view/ViewGroup;I)Landroid/support/v7/widget/p;
    .locals 1

    invoke-virtual {p0, p1, p2}, Lcom/android/settings/dashboard/t;->onCreateViewHolder(Landroid/view/ViewGroup;I)Lcom/android/settings/dashboard/x;

    move-result-object v0

    return-object v0
.end method

.method public onCreateViewHolder(Landroid/view/ViewGroup;I)Lcom/android/settings/dashboard/x;
    .locals 3

    new-instance v0, Lcom/android/settings/dashboard/x;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, p2, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/android/settings/dashboard/x;-><init>(Landroid/view/View;)V

    return-object v0
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    const-string/jumbo v0, "STATE_SELECTED_COUNTRY"

    iget-object v1, p0, Lcom/android/settings/dashboard/t;->Ki:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string/jumbo v0, "ACCOUNT_SELECTED_INDEX"

    iget v1, p0, Lcom/android/settings/dashboard/t;->Kh:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    return-void
.end method
