.class public abstract Lcom/android/settings/dashboard/MiuiDashboardFragment;
.super Lcom/android/settings/MiuiSettingsPreferenceFragment;
.source "MiuiDashboardFragment.java"

# interfaces
.implements Lcom/android/settingslib/drawer/b;
.implements Lcom/android/settings/search/Indexable;
.implements Lcom/android/settings/dashboard/E;


# instance fields
.field protected IC:Lcom/android/settings/dashboard/n;

.field private final ID:Ljava/util/Set;

.field private IE:Z

.field private IF:Lcom/android/settings/dashboard/a;

.field private final IG:Ljava/util/Map;

.field protected IH:Lcom/android/settings/dashboard/s;

.field private II:Lcom/android/settings/dashboard/C;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/android/settings/MiuiSettingsPreferenceFragment;-><init>()V

    new-instance v0, Landroid/util/ArrayMap;

    invoke-direct {v0}, Landroid/util/ArrayMap;-><init>()V

    iput-object v0, p0, Lcom/android/settings/dashboard/MiuiDashboardFragment;->IG:Ljava/util/Map;

    new-instance v0, Landroid/util/ArraySet;

    invoke-direct {v0}, Landroid/util/ArraySet;-><init>()V

    iput-object v0, p0, Lcom/android/settings/dashboard/MiuiDashboardFragment;->ID:Ljava/util/Set;

    return-void
.end method

.method private BW()V
    .locals 3

    invoke-virtual {p0}, Lcom/android/settings/dashboard/MiuiDashboardFragment;->as()I

    move-result v0

    if-gtz v0, :cond_0

    return-void

    :cond_0
    invoke-virtual {p0, v0}, Lcom/android/settings/dashboard/MiuiDashboardFragment;->addPreferencesFromResource(I)V

    invoke-virtual {p0}, Lcom/android/settings/dashboard/MiuiDashboardFragment;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v1

    iget-object v0, p0, Lcom/android/settings/dashboard/MiuiDashboardFragment;->IG:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settings/core/e;

    invoke-virtual {v0, v1}, Lcom/android/settings/core/e;->i(Landroid/preference/PreferenceScreen;)V

    goto :goto_0

    :cond_1
    return-void
.end method

.method private Cb(Ljava/lang/String;)V
    .locals 2

    invoke-virtual {p0}, Lcom/android/settings/dashboard/MiuiDashboardFragment;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/android/settings/dashboard/MiuiDashboardFragment;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    invoke-virtual {v0}, Landroid/preference/PreferenceScreen;->removeAll()V

    :cond_0
    invoke-direct {p0}, Lcom/android/settings/dashboard/MiuiDashboardFragment;->BW()V

    iget-object v0, p0, Lcom/android/settings/dashboard/MiuiDashboardFragment;->IH:Lcom/android/settings/dashboard/s;

    invoke-virtual {p0}, Lcom/android/settings/dashboard/MiuiDashboardFragment;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/settings/dashboard/s;->DE(Landroid/preference/PreferenceScreen;)V

    invoke-virtual {p0, p1}, Lcom/android/settings/dashboard/MiuiDashboardFragment;->refreshDashboardTiles(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method protected BV(Lcom/android/settings/core/e;)V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/dashboard/MiuiDashboardFragment;->IG:Ljava/util/Map;

    invoke-virtual {p1}, Lcom/android/settings/core/e;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method protected BX(Lcom/android/settingslib/drawer/Tile;)Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method protected BY(Ljava/lang/Class;)Lcom/android/settings/core/e;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/dashboard/MiuiDashboardFragment;->IG:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settings/core/e;

    return-object v0
.end method

.method public BZ(Lcom/android/settingslib/drawer/Tile;)V
    .locals 5

    iget-object v0, p0, Lcom/android/settings/dashboard/MiuiDashboardFragment;->IC:Lcom/android/settings/dashboard/n;

    invoke-interface {v0, p1}, Lcom/android/settings/dashboard/n;->Db(Lcom/android/settingslib/drawer/Tile;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/dashboard/MiuiDashboardFragment;->IH:Lcom/android/settings/dashboard/s;

    invoke-virtual {p0}, Lcom/android/settings/dashboard/MiuiDashboardFragment;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Lcom/android/settings/dashboard/s;->DD(Landroid/preference/PreferenceScreen;Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    if-nez v1, :cond_0

    invoke-virtual {p0}, Lcom/android/settings/dashboard/MiuiDashboardFragment;->ar()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "Can\'t find pref by key %s, skipping update summary %s/%s"

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    iget-object v0, p1, Lcom/android/settingslib/drawer/Tile;->title:Ljava/lang/CharSequence;

    const/4 v4, 0x1

    aput-object v0, v3, v4

    iget-object v0, p1, Lcom/android/settingslib/drawer/Tile;->summary:Ljava/lang/CharSequence;

    const/4 v4, 0x2

    aput-object v0, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :cond_0
    iget-object v0, p1, Lcom/android/settingslib/drawer/Tile;->summary:Ljava/lang/CharSequence;

    invoke-virtual {v1, v0}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public Ca()V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/dashboard/MiuiDashboardFragment;->IC:Lcom/android/settings/dashboard/n;

    invoke-virtual {p0}, Lcom/android/settings/dashboard/MiuiDashboardFragment;->getCategoryKey()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/android/settings/dashboard/n;->De(Ljava/lang/String;)Lcom/android/settingslib/drawer/DashboardCategory;

    move-result-object v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/android/settings/dashboard/MiuiDashboardFragment;->ar()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/settings/dashboard/MiuiDashboardFragment;->refreshDashboardTiles(Ljava/lang/String;)V

    return-void
.end method

.method protected Cc()V
    .locals 8

    iget-object v0, p0, Lcom/android/settings/dashboard/MiuiDashboardFragment;->IG:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-virtual {p0}, Lcom/android/settings/dashboard/MiuiDashboardFragment;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v1

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settings/core/e;

    invoke-virtual {v0}, Lcom/android/settings/core/e;->p()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v0}, Lcom/android/settings/core/e;->l()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/android/settings/dashboard/MiuiDashboardFragment;->IH:Lcom/android/settings/dashboard/s;

    invoke-virtual {v4, v1, v3}, Lcom/android/settings/dashboard/s;->DD(Landroid/preference/PreferenceScreen;Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v4

    if-nez v4, :cond_1

    const-string/jumbo v4, "MiuiDashboardFragment"

    const-string/jumbo v5, "Cannot find preference with key %s in Controller %s"

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    aput-object v3, v6, v7

    invoke-virtual {v0}, Lcom/android/settings/core/e;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    const/4 v3, 0x1

    aput-object v0, v6, v3

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v4, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_1
    invoke-virtual {v0, v4}, Lcom/android/settings/core/e;->cz(Landroid/preference/Preference;)V

    goto :goto_0

    :cond_2
    return-void
.end method

.method protected abstract ar()Ljava/lang/String;
.end method

.method protected abstract as()I
.end method

.method public at(Landroid/preference/Preference;)Z
    .locals 5

    iget-object v0, p0, Lcom/android/settings/dashboard/MiuiDashboardFragment;->IG:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/dashboard/MiuiDashboardFragment;->mMetricsFeatureProvider:Lcom/android/settings/core/instrumentation/e;

    invoke-virtual {p0}, Lcom/android/settings/dashboard/MiuiDashboardFragment;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {p1}, Landroid/preference/Preference;->getIntent()Landroid/content/Intent;

    move-result-object v3

    invoke-virtual {p0}, Lcom/android/settings/dashboard/MiuiDashboardFragment;->getMetricsCategory()I

    move-result v4

    invoke-virtual {v1, v2, v3, v4}, Lcom/android/settings/core/instrumentation/e;->ajR(Landroid/content/Context;Landroid/content/Intent;I)V

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settings/core/e;

    invoke-virtual {v0, p1}, Lcom/android/settings/core/e;->fm(Landroid/preference/Preference;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    return v0

    :cond_1
    invoke-super {p0, p1}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->at(Landroid/preference/Preference;)Z

    move-result v0

    return v0
.end method

.method public getCategoryKey()Ljava/lang/String;
    .locals 2

    sget-object v0, Lcom/android/settings/dashboard/r;->JT:Ljava/util/Map;

    invoke-virtual {p0}, Lcom/android/settings/dashboard/MiuiDashboardFragment;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method protected abstract getPreferenceControllers(Landroid/content/Context;)Ljava/util/List;
.end method

.method public onAttach(Landroid/content/Context;)V
    .locals 2

    invoke-super {p0, p1}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->onAttach(Landroid/content/Context;)V

    invoke-static {p1}, Lcom/android/settings/overlay/a;->aIk(Landroid/content/Context;)Lcom/android/settings/overlay/a;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/android/settings/overlay/a;->aIn(Landroid/content/Context;)Lcom/android/settings/dashboard/n;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/dashboard/MiuiDashboardFragment;->IC:Lcom/android/settings/dashboard/n;

    iget-object v0, p0, Lcom/android/settings/dashboard/MiuiDashboardFragment;->IC:Lcom/android/settings/dashboard/n;

    invoke-virtual {p0}, Lcom/android/settings/dashboard/MiuiDashboardFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    invoke-interface {v0, p1, p0, v1}, Lcom/android/settings/dashboard/n;->Dd(Landroid/content/Context;Lcom/android/settings/dashboard/MiuiDashboardFragment;Landroid/os/Bundle;)Lcom/android/settings/dashboard/s;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/dashboard/MiuiDashboardFragment;->IH:Lcom/android/settings/dashboard/s;

    invoke-virtual {p0}, Lcom/android/settings/dashboard/MiuiDashboardFragment;->getLifecycle()Lcom/android/settings/core/lifecycle/c;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/dashboard/MiuiDashboardFragment;->IH:Lcom/android/settings/dashboard/s;

    invoke-virtual {v0, v1}, Lcom/android/settings/core/lifecycle/c;->ajv(Lcom/android/settings/core/lifecycle/b;)Lcom/android/settings/core/lifecycle/b;

    invoke-virtual {p0, p1}, Lcom/android/settings/dashboard/MiuiDashboardFragment;->getPreferenceControllers(Landroid/content/Context;)Ljava/util/List;

    move-result-object v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    :cond_0
    new-instance v1, Lcom/android/settings/dashboard/a;

    invoke-direct {v1, p1}, Lcom/android/settings/dashboard/a;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/android/settings/dashboard/MiuiDashboardFragment;->IF:Lcom/android/settings/dashboard/a;

    iget-object v1, p0, Lcom/android/settings/dashboard/MiuiDashboardFragment;->IF:Lcom/android/settings/dashboard/a;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settings/core/e;

    invoke-virtual {p0, v0}, Lcom/android/settings/dashboard/MiuiDashboardFragment;->BV(Lcom/android/settings/core/e;)V

    goto :goto_0

    :cond_1
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 0

    invoke-super {p0, p1}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->onCreate(Landroid/os/Bundle;)V

    return-void
.end method

.method public onDestroy()V
    .locals 1

    iget-object v0, p0, Lcom/android/settings/dashboard/MiuiDashboardFragment;->II:Lcom/android/settings/dashboard/C;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/dashboard/MiuiDashboardFragment;->II:Lcom/android/settings/dashboard/C;

    invoke-virtual {v0}, Lcom/android/settings/dashboard/C;->Fb()V

    :cond_0
    invoke-super {p0}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->onDestroy()V

    return-void
.end method

.method public onResume()V
    .locals 0

    invoke-super {p0}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->onResume()V

    invoke-virtual {p0}, Lcom/android/settings/dashboard/MiuiDashboardFragment;->Cc()V

    return-void
.end method

.method public onStart()V
    .locals 3

    const/4 v2, 0x1

    invoke-super {p0}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->onStart()V

    iget-object v0, p0, Lcom/android/settings/dashboard/MiuiDashboardFragment;->IC:Lcom/android/settings/dashboard/n;

    invoke-virtual {p0}, Lcom/android/settings/dashboard/MiuiDashboardFragment;->getCategoryKey()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/android/settings/dashboard/n;->De(Ljava/lang/String;)Lcom/android/settingslib/drawer/DashboardCategory;

    move-result-object v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/settings/dashboard/MiuiDashboardFragment;->II:Lcom/android/settings/dashboard/C;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/settings/dashboard/MiuiDashboardFragment;->II:Lcom/android/settings/dashboard/C;

    invoke-virtual {v0, v2}, Lcom/android/settings/dashboard/C;->Fa(Z)V

    :cond_1
    invoke-virtual {p0}, Lcom/android/settings/dashboard/MiuiDashboardFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    instance-of v1, v0, Lcom/android/settingslib/drawer/a;

    if-eqz v1, :cond_2

    iput-boolean v2, p0, Lcom/android/settings/dashboard/MiuiDashboardFragment;->IE:Z

    check-cast v0, Lcom/android/settingslib/drawer/a;

    invoke-virtual {v0, p0}, Lcom/android/settingslib/drawer/a;->cdM(Lcom/android/settingslib/drawer/b;)V

    :cond_2
    return-void
.end method

.method public onStop()V
    .locals 3

    const/4 v2, 0x0

    invoke-super {p0}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->onStop()V

    iget-object v0, p0, Lcom/android/settings/dashboard/MiuiDashboardFragment;->II:Lcom/android/settings/dashboard/C;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/dashboard/MiuiDashboardFragment;->II:Lcom/android/settings/dashboard/C;

    invoke-virtual {v0, v2}, Lcom/android/settings/dashboard/C;->Fa(Z)V

    :cond_0
    iget-boolean v0, p0, Lcom/android/settings/dashboard/MiuiDashboardFragment;->IE:Z

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lcom/android/settings/dashboard/MiuiDashboardFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    instance-of v1, v0, Lcom/android/settingslib/drawer/a;

    if-eqz v1, :cond_1

    check-cast v0, Lcom/android/settingslib/drawer/a;

    invoke-virtual {v0, p0}, Lcom/android/settingslib/drawer/a;->cdN(Lcom/android/settingslib/drawer/b;)V

    :cond_1
    iput-boolean v2, p0, Lcom/android/settings/dashboard/MiuiDashboardFragment;->IE:Z

    :cond_2
    return-void
.end method

.method public qR(Landroid/os/Bundle;Ljava/lang/String;)V
    .locals 1

    invoke-super {p0, p1, p2}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->qR(Landroid/os/Bundle;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/android/settings/dashboard/MiuiDashboardFragment;->ar()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/android/settings/dashboard/MiuiDashboardFragment;->Cb(Ljava/lang/String;)V

    return-void
.end method

.method refreshDashboardTiles(Ljava/lang/String;)V
    .locals 13

    const/4 v12, 0x1

    const/4 v5, 0x0

    invoke-virtual {p0}, Lcom/android/settings/dashboard/MiuiDashboardFragment;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v7

    iget-object v0, p0, Lcom/android/settings/dashboard/MiuiDashboardFragment;->IC:Lcom/android/settings/dashboard/n;

    invoke-virtual {p0}, Lcom/android/settings/dashboard/MiuiDashboardFragment;->getCategoryKey()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/android/settings/dashboard/n;->De(Ljava/lang/String;)Lcom/android/settingslib/drawer/DashboardCategory;

    move-result-object v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "NO dashboard tiles for "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :cond_0
    iget-object v1, v0, Lcom/android/settingslib/drawer/DashboardCategory;->czy:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "tile list is empty, skipping category "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v0, v0, Lcom/android/settingslib/drawer/DashboardCategory;->title:Ljava/lang/CharSequence;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :cond_1
    new-instance v8, Ljava/util/ArrayList;

    iget-object v0, p0, Lcom/android/settings/dashboard/MiuiDashboardFragment;->ID:Ljava/util/Set;

    invoke-direct {v8, v0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iget-object v0, p0, Lcom/android/settings/dashboard/MiuiDashboardFragment;->II:Lcom/android/settings/dashboard/C;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/settings/dashboard/MiuiDashboardFragment;->II:Lcom/android/settings/dashboard/C;

    invoke-virtual {v0}, Lcom/android/settings/dashboard/C;->Fb()V

    :cond_2
    invoke-virtual {p0}, Lcom/android/settings/dashboard/MiuiDashboardFragment;->getContext()Landroid/content/Context;

    move-result-object v0

    new-instance v2, Lcom/android/settings/dashboard/C;

    invoke-virtual {p0}, Lcom/android/settings/dashboard/MiuiDashboardFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-virtual {p0}, Lcom/android/settings/dashboard/MiuiDashboardFragment;->getCategoryKey()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Lcom/android/settings/dashboard/C;-><init>(Landroid/app/Activity;Ljava/lang/String;)V

    iput-object v2, p0, Lcom/android/settings/dashboard/MiuiDashboardFragment;->II:Lcom/android/settings/dashboard/C;

    iget-object v2, p0, Lcom/android/settings/dashboard/MiuiDashboardFragment;->II:Lcom/android/settings/dashboard/C;

    invoke-virtual {v2, p0}, Lcom/android/settings/dashboard/C;->Fc(Lcom/android/settings/dashboard/E;)V

    new-array v2, v12, [I

    const v3, 0x1010429

    aput v3, v2, v5

    invoke-virtual {v0, v2}, Landroid/content/Context;->obtainStyledAttributes([I)Landroid/content/res/TypedArray;

    move-result-object v2

    const v3, 0x106000b

    invoke-virtual {v0, v3}, Landroid/content/Context;->getColor(I)I

    move-result v3

    invoke-virtual {v2, v5, v3}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v9

    invoke-virtual {v2}, Landroid/content/res/TypedArray;->recycle()V

    invoke-virtual {v0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v10

    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v11

    :cond_3
    :goto_0
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/android/settingslib/drawer/Tile;

    iget-object v0, p0, Lcom/android/settings/dashboard/MiuiDashboardFragment;->IC:Lcom/android/settings/dashboard/n;

    invoke-interface {v0, v4}, Lcom/android/settings/dashboard/n;->Db(Lcom/android/settingslib/drawer/Tile;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_4

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "tile does not contain a key, skipping "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_4
    invoke-virtual {p0, v4}, Lcom/android/settings/dashboard/MiuiDashboardFragment;->BX(Lcom/android/settingslib/drawer/Tile;)Z

    move-result v0

    if-eqz v0, :cond_3

    if-eqz v10, :cond_5

    iget-object v0, v4, Lcom/android/settingslib/drawer/Tile;->intent:Landroid/content/Intent;

    if-eqz v0, :cond_5

    iget-object v0, v4, Lcom/android/settingslib/drawer/Tile;->intent:Landroid/content/Intent;

    invoke-virtual {v0}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v10, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_5

    iget-object v0, v4, Lcom/android/settingslib/drawer/Tile;->czu:Landroid/graphics/drawable/Icon;

    invoke-virtual {v0, v9}, Landroid/graphics/drawable/Icon;->setTint(I)Landroid/graphics/drawable/Icon;

    :cond_5
    iget-object v0, p0, Lcom/android/settings/dashboard/MiuiDashboardFragment;->ID:Ljava/util/Set;

    invoke-interface {v0, v5}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/android/settings/dashboard/MiuiDashboardFragment;->IH:Lcom/android/settings/dashboard/s;

    invoke-virtual {v0, v7, v5}, Lcom/android/settings/dashboard/s;->DD(Landroid/preference/PreferenceScreen;Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v3

    iget-object v0, p0, Lcom/android/settings/dashboard/MiuiDashboardFragment;->IC:Lcom/android/settings/dashboard/n;

    invoke-virtual {p0}, Lcom/android/settings/dashboard/MiuiDashboardFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {p0}, Lcom/android/settings/dashboard/MiuiDashboardFragment;->getMetricsCategory()I

    move-result v2

    iget-object v6, p0, Lcom/android/settings/dashboard/MiuiDashboardFragment;->IF:Lcom/android/settings/dashboard/a;

    invoke-virtual {v6}, Lcom/android/settings/dashboard/a;->getOrder()I

    move-result v6

    invoke-interface/range {v0 .. v6}, Lcom/android/settings/dashboard/n;->CZ(Landroid/app/Activity;ILandroid/preference/Preference;Lcom/android/settingslib/drawer/Tile;Ljava/lang/String;I)V

    :goto_1
    invoke-interface {v8, v5}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_6
    new-instance v3, Landroid/preference/Preference;

    invoke-virtual {p0}, Lcom/android/settings/dashboard/MiuiDashboardFragment;->bWz()Landroid/content/Context;

    move-result-object v0

    invoke-direct {v3, v0}, Landroid/preference/Preference;-><init>(Landroid/content/Context;)V

    iget-object v0, p0, Lcom/android/settings/dashboard/MiuiDashboardFragment;->IC:Lcom/android/settings/dashboard/n;

    invoke-virtual {p0}, Lcom/android/settings/dashboard/MiuiDashboardFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {p0}, Lcom/android/settings/dashboard/MiuiDashboardFragment;->getMetricsCategory()I

    move-result v2

    iget-object v6, p0, Lcom/android/settings/dashboard/MiuiDashboardFragment;->IF:Lcom/android/settings/dashboard/a;

    invoke-virtual {v6}, Lcom/android/settings/dashboard/a;->getOrder()I

    move-result v6

    invoke-interface/range {v0 .. v6}, Lcom/android/settings/dashboard/n;->CZ(Landroid/app/Activity;ILandroid/preference/Preference;Lcom/android/settingslib/drawer/Tile;Ljava/lang/String;I)V

    iget-object v0, p0, Lcom/android/settings/dashboard/MiuiDashboardFragment;->IH:Lcom/android/settings/dashboard/s;

    invoke-virtual {v0, v7, v3}, Lcom/android/settings/dashboard/s;->DF(Landroid/preference/PreferenceScreen;Landroid/preference/Preference;)V

    iget-object v0, p0, Lcom/android/settings/dashboard/MiuiDashboardFragment;->ID:Ljava/util/Set;

    invoke-interface {v0, v5}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_7
    invoke-interface {v8}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iget-object v2, p0, Lcom/android/settings/dashboard/MiuiDashboardFragment;->ID:Ljava/util/Set;

    invoke-interface {v2, v0}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    iget-object v2, p0, Lcom/android/settings/dashboard/MiuiDashboardFragment;->IH:Lcom/android/settings/dashboard/s;

    invoke-virtual {v2, v7, v0}, Lcom/android/settings/dashboard/s;->DG(Landroid/preference/PreferenceScreen;Ljava/lang/String;)V

    goto :goto_2

    :cond_8
    iget-object v0, p0, Lcom/android/settings/dashboard/MiuiDashboardFragment;->II:Lcom/android/settings/dashboard/C;

    invoke-virtual {v0, v12}, Lcom/android/settings/dashboard/C;->Fa(Z)V

    return-void
.end method
