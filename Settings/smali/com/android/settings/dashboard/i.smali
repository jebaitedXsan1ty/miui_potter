.class public Lcom/android/settings/dashboard/i;
.super Ljava/lang/Object;
.source "DashboardData.java"


# instance fields
.field public final Jx:Z

.field public final Jy:I

.field public final Jz:I


# direct methods
.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean v0, p0, Lcom/android/settings/dashboard/i;->Jx:Z

    iput v0, p0, Lcom/android/settings/dashboard/i;->Jy:I

    iput v0, p0, Lcom/android/settings/dashboard/i;->Jz:I

    return-void
.end method

.method public constructor <init>(ZII)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean p1, p0, Lcom/android/settings/dashboard/i;->Jx:Z

    iput p2, p0, Lcom/android/settings/dashboard/i;->Jy:I

    iput p3, p0, Lcom/android/settings/dashboard/i;->Jz:I

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p0, p1, :cond_0

    return v0

    :cond_0
    instance-of v2, p1, Lcom/android/settings/dashboard/i;

    if-nez v2, :cond_1

    return v1

    :cond_1
    check-cast p1, Lcom/android/settings/dashboard/i;

    iget-boolean v2, p0, Lcom/android/settings/dashboard/i;->Jx:Z

    iget-boolean v3, p1, Lcom/android/settings/dashboard/i;->Jx:Z

    if-ne v2, v3, :cond_3

    iget v2, p0, Lcom/android/settings/dashboard/i;->Jy:I

    iget v3, p1, Lcom/android/settings/dashboard/i;->Jy:I

    if-ne v2, v3, :cond_3

    iget v2, p0, Lcom/android/settings/dashboard/i;->Jz:I

    iget v3, p1, Lcom/android/settings/dashboard/i;->Jz:I

    if-ne v2, v3, :cond_2

    :goto_0
    return v0

    :cond_2
    move v0, v1

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method
