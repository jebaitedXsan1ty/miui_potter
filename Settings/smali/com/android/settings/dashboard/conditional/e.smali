.class public abstract Lcom/android/settings/dashboard/conditional/e;
.super Ljava/lang/Object;
.source "Condition.java"


# instance fields
.field protected final HQ:Lcom/android/settings/dashboard/conditional/h;

.field private HR:Z

.field private HS:Z

.field private HT:J

.field protected HU:Z

.field protected final mMetricsFeatureProvider:Lcom/android/settings/core/instrumentation/e;


# direct methods
.method constructor <init>(Lcom/android/settings/dashboard/conditional/h;)V
    .locals 1

    invoke-virtual {p1}, Lcom/android/settings/dashboard/conditional/h;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/android/settings/overlay/a;->aIk(Landroid/content/Context;)Lcom/android/settings/overlay/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/settings/overlay/a;->aIm()Lcom/android/settings/core/instrumentation/e;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/android/settings/dashboard/conditional/e;-><init>(Lcom/android/settings/dashboard/conditional/h;Lcom/android/settings/core/instrumentation/e;)V

    return-void
.end method

.method constructor <init>(Lcom/android/settings/dashboard/conditional/h;Lcom/android/settings/core/instrumentation/e;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/android/settings/dashboard/conditional/e;->HQ:Lcom/android/settings/dashboard/conditional/h;

    iput-object p2, p0, Lcom/android/settings/dashboard/conditional/e;->mMetricsFeatureProvider:Lcom/android/settings/core/instrumentation/e;

    return-void
.end method


# virtual methods
.method public abstract AA()I
.end method

.method public abstract AC(I)V
.end method

.method public abstract AD()V
.end method

.method public abstract AE()V
.end method

.method AF(Landroid/os/PersistableBundle;)V
    .locals 2

    const-string/jumbo v0, "silence"

    invoke-virtual {p1, v0}, Landroid/os/PersistableBundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/settings/dashboard/conditional/e;->HS:Z

    const-string/jumbo v0, "active"

    invoke-virtual {p1, v0}, Landroid/os/PersistableBundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/settings/dashboard/conditional/e;->HR:Z

    const-string/jumbo v0, "last_state"

    invoke-virtual {p1, v0}, Landroid/os/PersistableBundle;->getLong(Ljava/lang/String;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/android/settings/dashboard/conditional/e;->HT:J

    return-void
.end method

.method AG(Landroid/os/PersistableBundle;)Z
    .locals 4

    iget-boolean v0, p0, Lcom/android/settings/dashboard/conditional/e;->HS:Z

    if-eqz v0, :cond_0

    const-string/jumbo v0, "silence"

    iget-boolean v1, p0, Lcom/android/settings/dashboard/conditional/e;->HS:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/PersistableBundle;->putBoolean(Ljava/lang/String;Z)V

    :cond_0
    iget-boolean v0, p0, Lcom/android/settings/dashboard/conditional/e;->HR:Z

    if-eqz v0, :cond_1

    const-string/jumbo v0, "active"

    iget-boolean v1, p0, Lcom/android/settings/dashboard/conditional/e;->HR:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/PersistableBundle;->putBoolean(Ljava/lang/String;Z)V

    const-string/jumbo v0, "last_state"

    iget-wide v2, p0, Lcom/android/settings/dashboard/conditional/e;->HT:J

    invoke-virtual {p1, v0, v2, v3}, Landroid/os/PersistableBundle;->putLong(Ljava/lang/String;J)V

    :cond_1
    iget-boolean v0, p0, Lcom/android/settings/dashboard/conditional/e;->HS:Z

    if-nez v0, :cond_2

    iget-boolean v0, p0, Lcom/android/settings/dashboard/conditional/e;->HR:Z

    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method protected AH()Landroid/content/IntentFilter;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method protected AI()Landroid/content/BroadcastReceiver;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method protected AJ(Z)V
    .locals 2

    iget-boolean v0, p0, Lcom/android/settings/dashboard/conditional/e;->HR:Z

    if-ne v0, p1, :cond_0

    return-void

    :cond_0
    iput-boolean p1, p0, Lcom/android/settings/dashboard/conditional/e;->HR:Z

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/android/settings/dashboard/conditional/e;->HT:J

    iget-boolean v0, p0, Lcom/android/settings/dashboard/conditional/e;->HS:Z

    if-eqz v0, :cond_1

    xor-int/lit8 v0, p1, 0x1

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/settings/dashboard/conditional/e;->HS:Z

    iget-boolean v0, p0, Lcom/android/settings/dashboard/conditional/e;->HS:Z

    invoke-virtual {p0, v0}, Lcom/android/settings/dashboard/conditional/e;->onSilenceChanged(Z)V

    :cond_1
    invoke-virtual {p0}, Lcom/android/settings/dashboard/conditional/e;->notifyChanged()V

    return-void
.end method

.method public AK()Z
    .locals 1

    invoke-virtual {p0}, Lcom/android/settings/dashboard/conditional/e;->AM()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/android/settings/dashboard/conditional/e;->AN()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method AL()J
    .locals 2

    iget-wide v0, p0, Lcom/android/settings/dashboard/conditional/e;->HT:J

    return-wide v0
.end method

.method public AM()Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/settings/dashboard/conditional/e;->HR:Z

    return v0
.end method

.method public AN()Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/settings/dashboard/conditional/e;->HS:Z

    return v0
.end method

.method public AO()V
    .locals 4

    iget-boolean v0, p0, Lcom/android/settings/dashboard/conditional/e;->HS:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/settings/dashboard/conditional/e;->HS:Z

    iget-object v0, p0, Lcom/android/settings/dashboard/conditional/e;->HQ:Lcom/android/settings/dashboard/conditional/h;

    invoke-virtual {v0}, Lcom/android/settings/dashboard/conditional/h;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/dashboard/conditional/e;->mMetricsFeatureProvider:Lcom/android/settings/core/instrumentation/e;

    invoke-virtual {p0}, Lcom/android/settings/dashboard/conditional/e;->AA()I

    move-result v2

    const/16 v3, 0x174

    invoke-virtual {v1, v0, v3, v2}, Lcom/android/settings/core/instrumentation/e;->ajU(Landroid/content/Context;II)V

    iget-boolean v0, p0, Lcom/android/settings/dashboard/conditional/e;->HS:Z

    invoke-virtual {p0, v0}, Lcom/android/settings/dashboard/conditional/e;->onSilenceChanged(Z)V

    invoke-virtual {p0}, Lcom/android/settings/dashboard/conditional/e;->notifyChanged()V

    :cond_0
    return-void
.end method

.method public abstract Az()[Ljava/lang/CharSequence;
.end method

.method public abstract getIcon()Landroid/graphics/drawable/Icon;
.end method

.method public abstract getSummary()Ljava/lang/CharSequence;
.end method

.method public abstract getTitle()Ljava/lang/CharSequence;
.end method

.method protected notifyChanged()V
    .locals 1

    iget-object v0, p0, Lcom/android/settings/dashboard/conditional/e;->HQ:Lcom/android/settings/dashboard/conditional/h;

    invoke-virtual {v0, p0}, Lcom/android/settings/dashboard/conditional/h;->AX(Lcom/android/settings/dashboard/conditional/e;)V

    return-void
.end method

.method public onPause()V
    .locals 0

    return-void
.end method

.method public onResume()V
    .locals 0

    return-void
.end method

.method onSilenceChanged(Z)V
    .locals 3

    invoke-virtual {p0}, Lcom/android/settings/dashboard/conditional/e;->AI()Landroid/content/BroadcastReceiver;

    move-result-object v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    if-eqz p1, :cond_2

    iget-boolean v1, p0, Lcom/android/settings/dashboard/conditional/e;->HU:Z

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/android/settings/dashboard/conditional/e;->HQ:Lcom/android/settings/dashboard/conditional/h;

    invoke-virtual {v1}, Lcom/android/settings/dashboard/conditional/h;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {p0}, Lcom/android/settings/dashboard/conditional/e;->AH()Landroid/content/IntentFilter;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/settings/dashboard/conditional/e;->HU:Z

    :cond_1
    :goto_0
    return-void

    :cond_2
    iget-boolean v1, p0, Lcom/android/settings/dashboard/conditional/e;->HU:Z

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/android/settings/dashboard/conditional/e;->HQ:Lcom/android/settings/dashboard/conditional/h;

    invoke-virtual {v1}, Lcom/android/settings/dashboard/conditional/h;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/settings/dashboard/conditional/e;->HU:Z

    goto :goto_0
.end method
