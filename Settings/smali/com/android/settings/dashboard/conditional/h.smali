.class public Lcom/android/settings/dashboard/conditional/h;
.super Ljava/lang/Object;
.source "ConditionManager.java"

# interfaces
.implements Lcom/android/settings/core/lifecycle/b;
.implements Lcom/android/settings/core/lifecycle/a/b;
.implements Lcom/android/settings/core/lifecycle/a/d;


# static fields
.field private static final HY:Ljava/util/Comparator;

.field private static Ic:Lcom/android/settings/dashboard/conditional/h;


# instance fields
.field private final HZ:Ljava/util/ArrayList;

.field private final Ia:Ljava/util/ArrayList;

.field private Ib:Ljava/io/File;

.field private final mContext:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/android/settings/dashboard/conditional/s;

    invoke-direct {v0}, Lcom/android/settings/dashboard/conditional/s;-><init>()V

    sput-object v0, Lcom/android/settings/dashboard/conditional/h;->HY:Ljava/util/Comparator;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Z)V
    .locals 4

    const/4 v3, 0x0

    const/4 v2, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/settings/dashboard/conditional/h;->Ia:Ljava/util/ArrayList;

    iput-object p1, p0, Lcom/android/settings/dashboard/conditional/h;->mContext:Landroid/content/Context;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/settings/dashboard/conditional/h;->HZ:Ljava/util/ArrayList;

    if-eqz p2, :cond_0

    const-string/jumbo v0, "ConditionManager"

    const-string/jumbo v1, "conditions loading synchronously"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Lcom/android/settings/dashboard/conditional/i;

    invoke-direct {v0, p0, v3}, Lcom/android/settings/dashboard/conditional/i;-><init>(Lcom/android/settings/dashboard/conditional/h;Lcom/android/settings/dashboard/conditional/i;)V

    new-array v1, v2, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/android/settings/dashboard/conditional/i;->doInBackground([Ljava/lang/Void;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/settings/dashboard/conditional/i;->Bk(Ljava/util/ArrayList;)V

    :goto_0
    return-void

    :cond_0
    const-string/jumbo v0, "ConditionManager"

    const-string/jumbo v1, "conditions loading asychronously"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Lcom/android/settings/dashboard/conditional/i;

    invoke-direct {v0, p0, v3}, Lcom/android/settings/dashboard/conditional/i;-><init>(Lcom/android/settings/dashboard/conditional/h;Lcom/android/settings/dashboard/conditional/i;)V

    new-array v1, v2, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/android/settings/dashboard/conditional/i;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0
.end method

.method public static AQ(Landroid/content/Context;)Lcom/android/settings/dashboard/conditional/h;
    .locals 1

    const/4 v0, 0x1

    invoke-static {p0, v0}, Lcom/android/settings/dashboard/conditional/h;->AS(Landroid/content/Context;Z)Lcom/android/settings/dashboard/conditional/h;

    move-result-object v0

    return-object v0
.end method

.method public static AS(Landroid/content/Context;Z)Lcom/android/settings/dashboard/conditional/h;
    .locals 2

    sget-object v0, Lcom/android/settings/dashboard/conditional/h;->Ic:Lcom/android/settings/dashboard/conditional/h;

    if-nez v0, :cond_0

    new-instance v0, Lcom/android/settings/dashboard/conditional/h;

    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Lcom/android/settings/dashboard/conditional/h;-><init>(Landroid/content/Context;Z)V

    sput-object v0, Lcom/android/settings/dashboard/conditional/h;->Ic:Lcom/android/settings/dashboard/conditional/h;

    :cond_0
    sget-object v0, Lcom/android/settings/dashboard/conditional/h;->Ic:Lcom/android/settings/dashboard/conditional/h;

    return-object v0
.end method

.method private AY(Ljava/lang/Class;Ljava/util/ArrayList;)V
    .locals 1

    invoke-direct {p0, p1, p2}, Lcom/android/settings/dashboard/conditional/h;->Bb(Ljava/lang/Class;Ljava/util/List;)Lcom/android/settings/dashboard/conditional/e;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-direct {p0, p1}, Lcom/android/settings/dashboard/conditional/h;->Ba(Ljava/lang/Class;)Lcom/android/settings/dashboard/conditional/e;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    return-void
.end method

.method private AZ(Ljava/util/ArrayList;)V
    .locals 1

    const-class v0, Lcom/android/settings/dashboard/conditional/c;

    invoke-direct {p0, v0, p1}, Lcom/android/settings/dashboard/conditional/h;->AY(Ljava/lang/Class;Ljava/util/ArrayList;)V

    const-class v0, Lcom/android/settings/dashboard/conditional/f;

    invoke-direct {p0, v0, p1}, Lcom/android/settings/dashboard/conditional/h;->AY(Ljava/lang/Class;Ljava/util/ArrayList;)V

    const-class v0, Lcom/android/settings/dashboard/conditional/a;

    invoke-direct {p0, v0, p1}, Lcom/android/settings/dashboard/conditional/h;->AY(Ljava/lang/Class;Ljava/util/ArrayList;)V

    const-class v0, Lcom/android/settings/dashboard/conditional/k;

    invoke-direct {p0, v0, p1}, Lcom/android/settings/dashboard/conditional/h;->AY(Ljava/lang/Class;Ljava/util/ArrayList;)V

    const-class v0, Lcom/android/settings/dashboard/conditional/n;

    invoke-direct {p0, v0, p1}, Lcom/android/settings/dashboard/conditional/h;->AY(Ljava/lang/Class;Ljava/util/ArrayList;)V

    const-class v0, Lcom/android/settings/dashboard/conditional/m;

    invoke-direct {p0, v0, p1}, Lcom/android/settings/dashboard/conditional/h;->AY(Ljava/lang/Class;Ljava/util/ArrayList;)V

    const-class v0, Lcom/android/settings/dashboard/conditional/r;

    invoke-direct {p0, v0, p1}, Lcom/android/settings/dashboard/conditional/h;->AY(Ljava/lang/Class;Ljava/util/ArrayList;)V

    const-class v0, Lcom/android/settings/dashboard/conditional/l;

    invoke-direct {p0, v0, p1}, Lcom/android/settings/dashboard/conditional/h;->AY(Ljava/lang/Class;Ljava/util/ArrayList;)V

    sget-object v0, Lcom/android/settings/dashboard/conditional/h;->HY:Ljava/util/Comparator;

    invoke-static {p1, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    return-void
.end method

.method private Ba(Ljava/lang/Class;)Lcom/android/settings/dashboard/conditional/e;
    .locals 3

    const-class v0, Lcom/android/settings/dashboard/conditional/c;

    if-ne v0, p1, :cond_0

    new-instance v0, Lcom/android/settings/dashboard/conditional/c;

    invoke-direct {v0, p0}, Lcom/android/settings/dashboard/conditional/c;-><init>(Lcom/android/settings/dashboard/conditional/h;)V

    return-object v0

    :cond_0
    const-class v0, Lcom/android/settings/dashboard/conditional/f;

    if-ne v0, p1, :cond_1

    new-instance v0, Lcom/android/settings/dashboard/conditional/f;

    invoke-direct {v0, p0}, Lcom/android/settings/dashboard/conditional/f;-><init>(Lcom/android/settings/dashboard/conditional/h;)V

    return-object v0

    :cond_1
    const-class v0, Lcom/android/settings/dashboard/conditional/a;

    if-ne v0, p1, :cond_2

    new-instance v0, Lcom/android/settings/dashboard/conditional/a;

    invoke-direct {v0, p0}, Lcom/android/settings/dashboard/conditional/a;-><init>(Lcom/android/settings/dashboard/conditional/h;)V

    return-object v0

    :cond_2
    const-class v0, Lcom/android/settings/dashboard/conditional/k;

    if-ne v0, p1, :cond_3

    new-instance v0, Lcom/android/settings/dashboard/conditional/k;

    invoke-direct {v0, p0}, Lcom/android/settings/dashboard/conditional/k;-><init>(Lcom/android/settings/dashboard/conditional/h;)V

    return-object v0

    :cond_3
    const-class v0, Lcom/android/settings/dashboard/conditional/n;

    if-ne v0, p1, :cond_4

    new-instance v0, Lcom/android/settings/dashboard/conditional/n;

    invoke-direct {v0, p0}, Lcom/android/settings/dashboard/conditional/n;-><init>(Lcom/android/settings/dashboard/conditional/h;)V

    return-object v0

    :cond_4
    const-class v0, Lcom/android/settings/dashboard/conditional/m;

    if-ne v0, p1, :cond_5

    new-instance v0, Lcom/android/settings/dashboard/conditional/m;

    invoke-direct {v0, p0}, Lcom/android/settings/dashboard/conditional/m;-><init>(Lcom/android/settings/dashboard/conditional/h;)V

    return-object v0

    :cond_5
    const-class v0, Lcom/android/settings/dashboard/conditional/r;

    if-ne v0, p1, :cond_6

    new-instance v0, Lcom/android/settings/dashboard/conditional/r;

    invoke-direct {v0, p0}, Lcom/android/settings/dashboard/conditional/r;-><init>(Lcom/android/settings/dashboard/conditional/h;)V

    return-object v0

    :cond_6
    const-class v0, Lcom/android/settings/dashboard/conditional/l;

    if-ne v0, p1, :cond_7

    new-instance v0, Lcom/android/settings/dashboard/conditional/l;

    invoke-direct {v0, p0}, Lcom/android/settings/dashboard/conditional/l;-><init>(Lcom/android/settings/dashboard/conditional/h;)V

    return-object v0

    :cond_7
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Unexpected Condition "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private Bb(Ljava/lang/Class;Ljava/util/List;)Lcom/android/settings/dashboard/conditional/e;
    .locals 3

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_1

    invoke-interface {p2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settings/dashboard/conditional/e;

    invoke-virtual {v0}, Lcom/android/settings/dashboard/conditional/e;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/Class;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settings/dashboard/conditional/e;

    return-object v0

    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    return-object v0
.end method

.method private Bc(Ljava/io/File;Ljava/util/ArrayList;)V
    .locals 6

    :try_start_0
    invoke-static {}, Landroid/util/Xml;->newPullParser()Lorg/xmlpull/v1/XmlPullParser;

    move-result-object v1

    new-instance v2, Ljava/io/FileReader;

    invoke-direct {v2, p1}, Ljava/io/FileReader;-><init>(Ljava/io/File;)V

    invoke-interface {v1, v2}, Lorg/xmlpull/v1/XmlPullParser;->setInput(Ljava/io/Reader;)V

    invoke-interface {v1}, Lorg/xmlpull/v1/XmlPullParser;->getEventType()I

    move-result v0

    :goto_0
    const/4 v3, 0x1

    if-eq v0, v3, :cond_2

    const-string/jumbo v0, "c"

    invoke-interface {v1}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Lorg/xmlpull/v1/XmlPullParser;->getDepth()I

    move-result v3

    const-string/jumbo v0, ""

    const-string/jumbo v4, "cls"

    invoke-interface {v1, v0, v4}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v4, "com.android.settings.dashboard.conditional."

    invoke-virtual {v0, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_0

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "com.android.settings.dashboard.conditional."

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :cond_0
    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/android/settings/dashboard/conditional/h;->Ba(Ljava/lang/Class;)Lcom/android/settings/dashboard/conditional/e;

    move-result-object v0

    invoke-static {v1}, Landroid/os/PersistableBundle;->restoreFromXml(Lorg/xmlpull/v1/XmlPullParser;)Landroid/os/PersistableBundle;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/android/settings/dashboard/conditional/e;->AF(Landroid/os/PersistableBundle;)V

    invoke-virtual {p2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :goto_1
    invoke-interface {v1}, Lorg/xmlpull/v1/XmlPullParser;->getDepth()I

    move-result v0

    if-le v0, v3, :cond_1

    invoke-interface {v1}, Lorg/xmlpull/v1/XmlPullParser;->next()I
    :try_end_0
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v0

    const-string/jumbo v1, "ConditionManager"

    const-string/jumbo v2, "Problem reading condition_state.xml"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :goto_2
    return-void

    :cond_1
    :try_start_1
    invoke-interface {v1}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v0

    goto :goto_0

    :cond_2
    invoke-virtual {v2}, Ljava/io/FileReader;->close()V
    :try_end_1
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_2
.end method

.method private Bd()V
    .locals 8

    :try_start_0
    invoke-static {}, Landroid/util/Xml;->newSerializer()Lorg/xmlpull/v1/XmlSerializer;

    move-result-object v2

    new-instance v3, Ljava/io/FileWriter;

    iget-object v0, p0, Lcom/android/settings/dashboard/conditional/h;->Ib:Ljava/io/File;

    invoke-direct {v3, v0}, Ljava/io/FileWriter;-><init>(Ljava/io/File;)V

    invoke-interface {v2, v3}, Lorg/xmlpull/v1/XmlSerializer;->setOutput(Ljava/io/Writer;)V

    const-string/jumbo v0, "UTF-8"

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-interface {v2, v0, v1}, Lorg/xmlpull/v1/XmlSerializer;->startDocument(Ljava/lang/String;Ljava/lang/Boolean;)V

    const-string/jumbo v0, ""

    const-string/jumbo v1, "cs"

    invoke-interface {v2, v0, v1}, Lorg/xmlpull/v1/XmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    iget-object v0, p0, Lcom/android/settings/dashboard/conditional/h;->HZ:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v4

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v4, :cond_1

    new-instance v5, Landroid/os/PersistableBundle;

    invoke-direct {v5}, Landroid/os/PersistableBundle;-><init>()V

    iget-object v0, p0, Lcom/android/settings/dashboard/conditional/h;->HZ:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settings/dashboard/conditional/e;

    invoke-virtual {v0, v5}, Lcom/android/settings/dashboard/conditional/e;->AG(Landroid/os/PersistableBundle;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string/jumbo v0, ""

    const-string/jumbo v6, "c"

    invoke-interface {v2, v0, v6}, Lorg/xmlpull/v1/XmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    iget-object v0, p0, Lcom/android/settings/dashboard/conditional/h;->HZ:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settings/dashboard/conditional/e;

    invoke-virtual {v0}, Lcom/android/settings/dashboard/conditional/e;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v6, ""

    const-string/jumbo v7, "cls"

    invoke-interface {v2, v6, v7, v0}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    invoke-virtual {v5, v2}, Landroid/os/PersistableBundle;->saveToXml(Lorg/xmlpull/v1/XmlSerializer;)V

    const-string/jumbo v0, ""

    const-string/jumbo v5, "c"

    invoke-interface {v2, v0, v5}, Lorg/xmlpull/v1/XmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    const-string/jumbo v0, ""

    const-string/jumbo v1, "cs"

    invoke-interface {v2, v0, v1}, Lorg/xmlpull/v1/XmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    invoke-interface {v2}, Lorg/xmlpull/v1/XmlSerializer;->flush()V

    invoke-virtual {v3}, Ljava/io/FileWriter;->close()V
    :try_end_0
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_1
    return-void

    :catch_0
    move-exception v0

    const-string/jumbo v1, "ConditionManager"

    const-string/jumbo v2, "Problem writing condition_state.xml"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1
.end method

.method static synthetic Be(Lcom/android/settings/dashboard/conditional/h;)Ljava/util/ArrayList;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/dashboard/conditional/h;->HZ:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic Bf(Lcom/android/settings/dashboard/conditional/h;)Landroid/content/Context;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/dashboard/conditional/h;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic Bg(Lcom/android/settings/dashboard/conditional/h;)Ljava/io/File;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/dashboard/conditional/h;->Ib:Ljava/io/File;

    return-object v0
.end method

.method static synthetic Bh(Lcom/android/settings/dashboard/conditional/h;Ljava/io/File;)Ljava/io/File;
    .locals 0

    iput-object p1, p0, Lcom/android/settings/dashboard/conditional/h;->Ib:Ljava/io/File;

    return-object p1
.end method

.method static synthetic Bi(Lcom/android/settings/dashboard/conditional/h;Ljava/util/ArrayList;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/settings/dashboard/conditional/h;->AZ(Ljava/util/ArrayList;)V

    return-void
.end method

.method static synthetic Bj(Lcom/android/settings/dashboard/conditional/h;Ljava/io/File;Ljava/util/ArrayList;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/android/settings/dashboard/conditional/h;->Bc(Ljava/io/File;Ljava/util/ArrayList;)V

    return-void
.end method


# virtual methods
.method public AR(Ljava/lang/Class;)Lcom/android/settings/dashboard/conditional/e;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/dashboard/conditional/h;->HZ:Ljava/util/ArrayList;

    invoke-direct {p0, p1, v0}, Lcom/android/settings/dashboard/conditional/h;->Bb(Ljava/lang/Class;Ljava/util/List;)Lcom/android/settings/dashboard/conditional/e;

    move-result-object v0

    return-object v0
.end method

.method public AT()Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/dashboard/conditional/h;->HZ:Ljava/util/ArrayList;

    return-object v0
.end method

.method public AU(Lcom/android/settings/dashboard/conditional/j;)V
    .locals 1

    iget-object v0, p0, Lcom/android/settings/dashboard/conditional/h;->Ia:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-interface {p1}, Lcom/android/settings/dashboard/conditional/j;->Bl()V

    return-void
.end method

.method public AV()V
    .locals 3

    iget-object v0, p0, Lcom/android/settings/dashboard/conditional/h;->HZ:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    iget-object v0, p0, Lcom/android/settings/dashboard/conditional/h;->HZ:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settings/dashboard/conditional/e;

    invoke-virtual {v0}, Lcom/android/settings/dashboard/conditional/e;->AE()V

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    return-void
.end method

.method public AW(Lcom/android/settings/dashboard/conditional/j;)V
    .locals 1

    iget-object v0, p0, Lcom/android/settings/dashboard/conditional/h;->Ia:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    return-void
.end method

.method public AX(Lcom/android/settings/dashboard/conditional/e;)V
    .locals 3

    invoke-direct {p0}, Lcom/android/settings/dashboard/conditional/h;->Bd()V

    iget-object v0, p0, Lcom/android/settings/dashboard/conditional/h;->HZ:Ljava/util/ArrayList;

    sget-object v1, Lcom/android/settings/dashboard/conditional/h;->HY:Ljava/util/Comparator;

    invoke-static {v0, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    iget-object v0, p0, Lcom/android/settings/dashboard/conditional/h;->Ia:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    iget-object v0, p0, Lcom/android/settings/dashboard/conditional/h;->Ia:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settings/dashboard/conditional/j;

    invoke-interface {v0}, Lcom/android/settings/dashboard/conditional/j;->Bl()V

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    return-void
.end method

.method getContext()Landroid/content/Context;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/dashboard/conditional/h;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method public onPause()V
    .locals 3

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/android/settings/dashboard/conditional/h;->HZ:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v2

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    iget-object v0, p0, Lcom/android/settings/dashboard/conditional/h;->HZ:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settings/dashboard/conditional/e;

    invoke-virtual {v0}, Lcom/android/settings/dashboard/conditional/e;->onPause()V

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    return-void
.end method

.method public onResume()V
    .locals 3

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/android/settings/dashboard/conditional/h;->HZ:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v2

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    iget-object v0, p0, Lcom/android/settings/dashboard/conditional/h;->HZ:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settings/dashboard/conditional/e;

    invoke-virtual {v0}, Lcom/android/settings/dashboard/conditional/e;->onResume()V

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    return-void
.end method
