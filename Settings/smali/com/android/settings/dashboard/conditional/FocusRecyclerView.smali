.class public Lcom/android/settings/dashboard/conditional/FocusRecyclerView;
.super Landroid/support/v7/widget/RecyclerView;
.source "FocusRecyclerView.java"


# instance fields
.field private Ih:Lcom/android/settings/dashboard/conditional/p;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Landroid/support/v7/widget/RecyclerView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method


# virtual methods
.method public onWindowFocusChanged(Z)V
    .locals 1

    invoke-super {p0, p1}, Landroid/support/v7/widget/RecyclerView;->onWindowFocusChanged(Z)V

    iget-object v0, p0, Lcom/android/settings/dashboard/conditional/FocusRecyclerView;->Ih:Lcom/android/settings/dashboard/conditional/p;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/dashboard/conditional/FocusRecyclerView;->Ih:Lcom/android/settings/dashboard/conditional/p;

    invoke-interface {v0, p1}, Lcom/android/settings/dashboard/conditional/p;->onWindowFocusChanged(Z)V

    :cond_0
    return-void
.end method

.method public setListener(Lcom/android/settings/dashboard/conditional/p;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/dashboard/conditional/FocusRecyclerView;->Ih:Lcom/android/settings/dashboard/conditional/p;

    return-void
.end method
