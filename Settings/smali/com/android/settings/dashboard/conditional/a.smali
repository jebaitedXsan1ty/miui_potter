.class public Lcom/android/settings/dashboard/conditional/a;
.super Lcom/android/settings/dashboard/conditional/e;
.source "DndCondition.java"


# static fields
.field static final DND_FILTER:Landroid/content/IntentFilter;


# instance fields
.field private HK:Landroid/service/notification/ZenModeConfig;

.field private final HL:Lcom/android/settings/dashboard/conditional/b;

.field private HM:Z

.field private HN:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Landroid/content/IntentFilter;

    const-string/jumbo v1, "android.app.action.INTERRUPTION_FILTER_CHANGED_INTERNAL"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/android/settings/dashboard/conditional/a;->DND_FILTER:Landroid/content/IntentFilter;

    return-void
.end method

.method public constructor <init>(Lcom/android/settings/dashboard/conditional/h;)V
    .locals 3

    invoke-direct {p0, p1}, Lcom/android/settings/dashboard/conditional/e;-><init>(Lcom/android/settings/dashboard/conditional/h;)V

    new-instance v0, Lcom/android/settings/dashboard/conditional/b;

    invoke-direct {v0}, Lcom/android/settings/dashboard/conditional/b;-><init>()V

    iput-object v0, p0, Lcom/android/settings/dashboard/conditional/a;->HL:Lcom/android/settings/dashboard/conditional/b;

    iget-object v0, p0, Lcom/android/settings/dashboard/conditional/a;->HQ:Lcom/android/settings/dashboard/conditional/h;

    invoke-virtual {v0}, Lcom/android/settings/dashboard/conditional/h;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/dashboard/conditional/a;->HL:Lcom/android/settings/dashboard/conditional/b;

    sget-object v2, Lcom/android/settings/dashboard/conditional/a;->DND_FILTER:Landroid/content/IntentFilter;

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/settings/dashboard/conditional/a;->HM:Z

    return-void
.end method

.method private AB()Ljava/lang/CharSequence;
    .locals 2

    iget v0, p0, Lcom/android/settings/dashboard/conditional/a;->HN:I

    packed-switch v0, :pswitch_data_0

    const/4 v0, 0x0

    return-object v0

    :pswitch_0
    iget-object v0, p0, Lcom/android/settings/dashboard/conditional/a;->HQ:Lcom/android/settings/dashboard/conditional/h;

    invoke-virtual {v0}, Lcom/android/settings/dashboard/conditional/h;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f1216b4

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0

    :pswitch_1
    iget-object v0, p0, Lcom/android/settings/dashboard/conditional/a;->HQ:Lcom/android/settings/dashboard/conditional/h;

    invoke-virtual {v0}, Lcom/android/settings/dashboard/conditional/h;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f1216b5

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0

    :pswitch_2
    iget-object v0, p0, Lcom/android/settings/dashboard/conditional/a;->HQ:Lcom/android/settings/dashboard/conditional/h;

    invoke-virtual {v0}, Lcom/android/settings/dashboard/conditional/h;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f1216b8

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method public AA()I
    .locals 1

    const/16 v0, 0x17d

    return v0
.end method

.method public AC(I)V
    .locals 4

    const/4 v3, 0x0

    if-nez p1, :cond_0

    iget-object v0, p0, Lcom/android/settings/dashboard/conditional/a;->HQ:Lcom/android/settings/dashboard/conditional/h;

    invoke-virtual {v0}, Lcom/android/settings/dashboard/conditional/h;->getContext()Landroid/content/Context;

    move-result-object v0

    const-class v1, Landroid/app/NotificationManager;

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    const-string/jumbo v1, "DndCondition"

    const/4 v2, 0x0

    invoke-virtual {v0, v3, v2, v1}, Landroid/app/NotificationManager;->setZenMode(ILandroid/net/Uri;Ljava/lang/String;)V

    invoke-virtual {p0, v3}, Lcom/android/settings/dashboard/conditional/a;->AJ(Z)V

    return-void

    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Unexpected index "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public AD()V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/dashboard/conditional/a;->HQ:Lcom/android/settings/dashboard/conditional/h;

    invoke-virtual {v0}, Lcom/android/settings/dashboard/conditional/h;->getContext()Landroid/content/Context;

    move-result-object v0

    const-class v1, Landroid/app/StatusBarManager;

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/StatusBarManager;

    const-string/jumbo v1, "dnd"

    invoke-virtual {v0, v1}, Landroid/app/StatusBarManager;->expandSettingsPanel(Ljava/lang/String;)V

    return-void
.end method

.method public AE()V
    .locals 3

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/android/settings/dashboard/conditional/a;->HQ:Lcom/android/settings/dashboard/conditional/h;

    invoke-virtual {v0}, Lcom/android/settings/dashboard/conditional/h;->getContext()Landroid/content/Context;

    move-result-object v0

    const-class v2, Landroid/app/NotificationManager;

    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    invoke-virtual {v0}, Landroid/app/NotificationManager;->getZenMode()I

    move-result v2

    iput v2, p0, Lcom/android/settings/dashboard/conditional/a;->HN:I

    iget v2, p0, Lcom/android/settings/dashboard/conditional/a;->HN:I

    if-eqz v2, :cond_0

    const/4 v1, 0x1

    :cond_0
    if-eqz v1, :cond_1

    invoke-virtual {v0}, Landroid/app/NotificationManager;->getZenModeConfig()Landroid/service/notification/ZenModeConfig;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/dashboard/conditional/a;->HK:Landroid/service/notification/ZenModeConfig;

    :goto_0
    invoke-virtual {p0, v1}, Lcom/android/settings/dashboard/conditional/a;->AJ(Z)V

    return-void

    :cond_1
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/settings/dashboard/conditional/a;->HK:Landroid/service/notification/ZenModeConfig;

    goto :goto_0
.end method

.method AF(Landroid/os/PersistableBundle;)V
    .locals 2

    invoke-super {p0, p1}, Lcom/android/settings/dashboard/conditional/e;->AF(Landroid/os/PersistableBundle;)V

    const-string/jumbo v0, "state"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/os/PersistableBundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/android/settings/dashboard/conditional/a;->HN:I

    return-void
.end method

.method AG(Landroid/os/PersistableBundle;)Z
    .locals 2

    const-string/jumbo v0, "state"

    iget v1, p0, Lcom/android/settings/dashboard/conditional/a;->HN:I

    invoke-virtual {p1, v0, v1}, Landroid/os/PersistableBundle;->putInt(Ljava/lang/String;I)V

    invoke-super {p0, p1}, Lcom/android/settings/dashboard/conditional/e;->AG(Landroid/os/PersistableBundle;)Z

    move-result v0

    return v0
.end method

.method public Az()[Ljava/lang/CharSequence;
    .locals 3

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/CharSequence;

    iget-object v1, p0, Lcom/android/settings/dashboard/conditional/a;->HQ:Lcom/android/settings/dashboard/conditional/h;

    invoke-virtual {v1}, Lcom/android/settings/dashboard/conditional/h;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f12044a

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    aput-object v1, v0, v2

    return-object v0
.end method

.method public getIcon()Landroid/graphics/drawable/Icon;
    .locals 2

    iget-object v0, p0, Lcom/android/settings/dashboard/conditional/a;->HQ:Lcom/android/settings/dashboard/conditional/h;

    invoke-virtual {v0}, Lcom/android/settings/dashboard/conditional/h;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0802c3

    invoke-static {v0, v1}, Landroid/graphics/drawable/Icon;->createWithResource(Landroid/content/Context;I)Landroid/graphics/drawable/Icon;

    move-result-object v0

    return-object v0
.end method

.method public getSummary()Ljava/lang/CharSequence;
    .locals 4

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/android/settings/dashboard/conditional/a;->HK:Landroid/service/notification/ZenModeConfig;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/settings/dashboard/conditional/a;->HK:Landroid/service/notification/ZenModeConfig;

    iget-object v0, v0, Landroid/service/notification/ZenModeConfig;->manualRule:Landroid/service/notification/ZenModeConfig$ZenRule;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/settings/dashboard/conditional/a;->HK:Landroid/service/notification/ZenModeConfig;

    iget-object v0, v0, Landroid/service/notification/ZenModeConfig;->manualRule:Landroid/service/notification/ZenModeConfig$ZenRule;

    iget-object v0, v0, Landroid/service/notification/ZenModeConfig$ZenRule;->conditionId:Landroid/net/Uri;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/settings/dashboard/conditional/a;->HQ:Lcom/android/settings/dashboard/conditional/h;

    invoke-virtual {v0}, Lcom/android/settings/dashboard/conditional/h;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x10406eb

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_1
    return-object v0

    :cond_0
    move v0, v1

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/android/settings/dashboard/conditional/a;->HQ:Lcom/android/settings/dashboard/conditional/h;

    invoke-virtual {v0}, Lcom/android/settings/dashboard/conditional/h;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v2, p0, Lcom/android/settings/dashboard/conditional/a;->HK:Landroid/service/notification/ZenModeConfig;

    invoke-static {}, Landroid/app/ActivityManager;->getCurrentUser()I

    move-result v3

    invoke-static {v0, v2, v3, v1}, Landroid/service/notification/ZenModeConfig;->getConditionSummary(Landroid/content/Context;Landroid/service/notification/ZenModeConfig;IZ)Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method

.method public getTitle()Ljava/lang/CharSequence;
    .locals 4

    iget-object v0, p0, Lcom/android/settings/dashboard/conditional/a;->HQ:Lcom/android/settings/dashboard/conditional/h;

    invoke-virtual {v0}, Lcom/android/settings/dashboard/conditional/h;->getContext()Landroid/content/Context;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    invoke-direct {p0}, Lcom/android/settings/dashboard/conditional/a;->AB()Ljava/lang/CharSequence;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    const v2, 0x7f12044e

    invoke-virtual {v0, v2, v1}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public onPause()V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/dashboard/conditional/a;->HQ:Lcom/android/settings/dashboard/conditional/h;

    invoke-virtual {v0}, Lcom/android/settings/dashboard/conditional/h;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/dashboard/conditional/a;->HL:Lcom/android/settings/dashboard/conditional/b;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/settings/dashboard/conditional/a;->HM:Z

    return-void
.end method

.method public onResume()V
    .locals 3

    iget-boolean v0, p0, Lcom/android/settings/dashboard/conditional/a;->HM:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/dashboard/conditional/a;->HQ:Lcom/android/settings/dashboard/conditional/h;

    invoke-virtual {v0}, Lcom/android/settings/dashboard/conditional/h;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/dashboard/conditional/a;->HL:Lcom/android/settings/dashboard/conditional/b;

    sget-object v2, Lcom/android/settings/dashboard/conditional/a;->DND_FILTER:Landroid/content/IntentFilter;

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/settings/dashboard/conditional/a;->HM:Z

    :cond_0
    return-void
.end method
