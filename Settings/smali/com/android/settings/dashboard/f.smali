.class public Lcom/android/settings/dashboard/f;
.super Landroid/support/v7/c/b;
.source "DashboardData.java"


# instance fields
.field private final Js:Ljava/util/List;

.field private final Jt:Ljava/util/List;


# direct methods
.method public constructor <init>(Ljava/util/List;Ljava/util/List;)V
    .locals 0

    invoke-direct {p0}, Landroid/support/v7/c/b;-><init>()V

    iput-object p1, p0, Lcom/android/settings/dashboard/f;->Jt:Ljava/util/List;

    iput-object p2, p0, Lcom/android/settings/dashboard/f;->Js:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public areContentsTheSame(II)Z
    .locals 2

    iget-object v0, p0, Lcom/android/settings/dashboard/f;->Jt:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settings/dashboard/g;

    iget-object v1, p0, Lcom/android/settings/dashboard/f;->Js:Ljava/util/List;

    invoke-interface {v1, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/settings/dashboard/g;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public areItemsTheSame(II)Z
    .locals 2

    iget-object v0, p0, Lcom/android/settings/dashboard/f;->Jt:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settings/dashboard/g;

    iget v1, v0, Lcom/android/settings/dashboard/g;->Jw:I

    iget-object v0, p0, Lcom/android/settings/dashboard/f;->Js:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settings/dashboard/g;

    iget v0, v0, Lcom/android/settings/dashboard/g;->Jw:I

    if-ne v1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getChangePayload(II)Ljava/lang/Object;
    .locals 2

    iget-object v0, p0, Lcom/android/settings/dashboard/f;->Jt:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settings/dashboard/g;

    iget v0, v0, Lcom/android/settings/dashboard/g;->type:I

    const v1, 0x7f0d0062

    if-ne v0, v1, :cond_0

    const-string/jumbo v0, "condition"

    return-object v0

    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public getNewListSize()I
    .locals 1

    iget-object v0, p0, Lcom/android/settings/dashboard/f;->Js:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getOldListSize()I
    .locals 1

    iget-object v0, p0, Lcom/android/settings/dashboard/f;->Jt:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method
