.class public Lcom/android/settings/dashboard/ExpandPreference;
.super Landroid/preference/Preference;
.source "ExpandPreference.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    invoke-direct {p0, p1}, Landroid/preference/Preference;-><init>(Landroid/content/Context;)V

    invoke-direct {p0}, Lcom/android/settings/dashboard/ExpandPreference;->Cd()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Landroid/preference/Preference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    invoke-direct {p0}, Lcom/android/settings/dashboard/ExpandPreference;->Cd()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Landroid/preference/Preference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    invoke-direct {p0}, Lcom/android/settings/dashboard/ExpandPreference;->Cd()V

    return-void
.end method

.method private Cd()V
    .locals 1

    const v0, 0x7f0d00b1

    invoke-virtual {p0, v0}, Lcom/android/settings/dashboard/ExpandPreference;->setLayoutResource(I)V

    const v0, 0x7f080186

    invoke-virtual {p0, v0}, Lcom/android/settings/dashboard/ExpandPreference;->setIcon(I)V

    const v0, 0x7f1200c6

    invoke-virtual {p0, v0}, Lcom/android/settings/dashboard/ExpandPreference;->setTitle(I)V

    const/16 v0, 0x3e7

    invoke-virtual {p0, v0}, Lcom/android/settings/dashboard/ExpandPreference;->setOrder(I)V

    return-void
.end method


# virtual methods
.method public onBindView(Landroid/view/View;)V
    .locals 0

    invoke-super {p0, p1}, Landroid/preference/Preference;->onBindView(Landroid/view/View;)V

    return-void
.end method
