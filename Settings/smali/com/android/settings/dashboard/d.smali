.class public Lcom/android/settings/dashboard/d;
.super Ljava/lang/Object;
.source "DashboardData.java"


# instance fields
.field private final Jh:Ljava/util/List;

.field private final Ji:Ljava/util/List;

.field private final Jj:Lcom/android/settings/dashboard/conditional/e;

.field private final Jk:Ljava/util/List;

.field private final Jl:I

.field private final Jm:Ljava/util/List;

.field private mId:I


# direct methods
.method private constructor <init>(Lcom/android/settings/dashboard/e;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/android/settings/dashboard/e;->CT(Lcom/android/settings/dashboard/e;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/dashboard/d;->Jh:Ljava/util/List;

    invoke-static {p1}, Lcom/android/settings/dashboard/e;->CU(Lcom/android/settings/dashboard/e;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/dashboard/d;->Ji:Ljava/util/List;

    invoke-static {p1}, Lcom/android/settings/dashboard/e;->CX(Lcom/android/settings/dashboard/e;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/dashboard/d;->Jm:Ljava/util/List;

    invoke-static {p1}, Lcom/android/settings/dashboard/e;->CW(Lcom/android/settings/dashboard/e;)I

    move-result v0

    iput v0, p0, Lcom/android/settings/dashboard/d;->Jl:I

    invoke-static {p1}, Lcom/android/settings/dashboard/e;->CV(Lcom/android/settings/dashboard/e;)Lcom/android/settings/dashboard/conditional/e;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/dashboard/d;->Jj:Lcom/android/settings/dashboard/conditional/e;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/settings/dashboard/d;->Jk:Ljava/util/List;

    const/4 v0, 0x0

    iput v0, p0, Lcom/android/settings/dashboard/d;->mId:I

    invoke-direct {p0}, Lcom/android/settings/dashboard/d;->Cs()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/settings/dashboard/e;Lcom/android/settings/dashboard/d;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/settings/dashboard/d;-><init>(Lcom/android/settings/dashboard/e;)V

    return-void
.end method

.method private CI()V
    .locals 1

    const/4 v0, 0x0

    iput v0, p0, Lcom/android/settings/dashboard/d;->mId:I

    return-void
.end method

.method static synthetic CJ(Lcom/android/settings/dashboard/d;)Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/dashboard/d;->Jh:Ljava/util/List;

    return-object v0
.end method

.method static synthetic CK(Lcom/android/settings/dashboard/d;)Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/dashboard/d;->Ji:Ljava/util/List;

    return-object v0
.end method

.method static synthetic CL(Lcom/android/settings/dashboard/d;)Lcom/android/settings/dashboard/conditional/e;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/dashboard/d;->Jj:Lcom/android/settings/dashboard/conditional/e;

    return-object v0
.end method

.method static synthetic CM(Lcom/android/settings/dashboard/d;)I
    .locals 1

    iget v0, p0, Lcom/android/settings/dashboard/d;->Jl:I

    return v0
.end method

.method static synthetic CN(Lcom/android/settings/dashboard/d;)Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/dashboard/d;->Jm:Ljava/util/List;

    return-object v0
.end method

.method private Cs()V
    .locals 10

    const/16 v9, 0x7d0

    const/4 v4, 0x1

    const/4 v8, 0x0

    const/4 v2, 0x0

    move v1, v2

    move v3, v2

    :goto_0
    iget-object v0, p0, Lcom/android/settings/dashboard/d;->Ji:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/dashboard/d;->Ji:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/dashboard/d;->Ji:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settings/dashboard/conditional/e;

    invoke-virtual {v0}, Lcom/android/settings/dashboard/conditional/e;->AK()Z

    move-result v0

    or-int/2addr v3, v0

    iget-object v5, p0, Lcom/android/settings/dashboard/d;->Ji:Ljava/util/List;

    invoke-interface {v5, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    const v6, 0x7f0d0062

    const/16 v7, 0xbb8

    invoke-direct {p0, v5, v6, v0, v7}, Lcom/android/settings/dashboard/d;->Cu(Ljava/lang/Object;IZI)V

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    invoke-direct {p0}, Lcom/android/settings/dashboard/d;->CI()V

    iget-object v0, p0, Lcom/android/settings/dashboard/d;->Jm:Ljava/util/List;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/settings/dashboard/d;->Jm:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-eqz v0, :cond_1

    move v1, v4

    :goto_1
    if-eqz v3, :cond_2

    move v0, v1

    :goto_2
    const v3, 0x7f0d0087

    invoke-direct {p0, v8, v3, v0, v2}, Lcom/android/settings/dashboard/d;->Cu(Ljava/lang/Object;IZI)V

    invoke-direct {p0}, Lcom/android/settings/dashboard/d;->Ct()Lcom/android/settings/dashboard/i;

    move-result-object v0

    const v3, 0x7f0d01f3

    invoke-direct {p0, v0, v3, v1, v2}, Lcom/android/settings/dashboard/d;->Cu(Ljava/lang/Object;IZI)V

    invoke-direct {p0}, Lcom/android/settings/dashboard/d;->CI()V

    iget-object v0, p0, Lcom/android/settings/dashboard/d;->Jm:Ljava/util/List;

    if-eqz v0, :cond_4

    invoke-virtual {p0}, Lcom/android/settings/dashboard/d;->Cx()I

    move-result v5

    move v1, v2

    :goto_3
    iget-object v0, p0, Lcom/android/settings/dashboard/d;->Jm:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_4

    iget-object v0, p0, Lcom/android/settings/dashboard/d;->Jm:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settingslib/drawer/Tile;

    if-ge v1, v5, :cond_3

    move v3, v4

    :goto_4
    invoke-direct {p0, v0, v3}, Lcom/android/settings/dashboard/d;->Cv(Lcom/android/settingslib/drawer/Tile;Z)V

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_3

    :cond_1
    move v1, v2

    goto :goto_1

    :cond_2
    move v0, v2

    goto :goto_2

    :cond_3
    move v3, v2

    goto :goto_4

    :cond_4
    invoke-direct {p0}, Lcom/android/settings/dashboard/d;->CI()V

    move v3, v2

    :goto_5
    iget-object v0, p0, Lcom/android/settings/dashboard/d;->Jh:Ljava/util/List;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/android/settings/dashboard/d;->Jh:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v3, v0, :cond_6

    iget-object v0, p0, Lcom/android/settings/dashboard/d;->Jh:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settingslib/drawer/DashboardCategory;

    iget-object v1, v0, Lcom/android/settingslib/drawer/DashboardCategory;->title:Ljava/lang/CharSequence;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    xor-int/lit8 v1, v1, 0x1

    const v5, 0x7f0d0086

    invoke-direct {p0, v0, v5, v1, v9}, Lcom/android/settings/dashboard/d;->Cu(Ljava/lang/Object;IZI)V

    move v5, v2

    :goto_6
    iget-object v1, v0, Lcom/android/settingslib/drawer/DashboardCategory;->czy:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v5, v1, :cond_5

    iget-object v1, v0, Lcom/android/settingslib/drawer/DashboardCategory;->czy:Ljava/util/List;

    invoke-interface {v1, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/settingslib/drawer/Tile;

    const v6, 0x7f0d0088

    invoke-direct {p0, v1, v6, v4, v9}, Lcom/android/settings/dashboard/d;->Cu(Ljava/lang/Object;IZI)V

    add-int/lit8 v1, v5, 0x1

    move v5, v1

    goto :goto_6

    :cond_5
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_5

    :cond_6
    return-void
.end method

.method private Ct()Lcom/android/settings/dashboard/i;
    .locals 4

    iget-object v0, p0, Lcom/android/settings/dashboard/d;->Jm:Ljava/util/List;

    if-nez v0, :cond_0

    new-instance v0, Lcom/android/settings/dashboard/i;

    invoke-direct {v0}, Lcom/android/settings/dashboard/i;-><init>()V

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Lcom/android/settings/dashboard/d;->CH()Z

    move-result v1

    iget-object v0, p0, Lcom/android/settings/dashboard/d;->Jm:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    invoke-virtual {p0}, Lcom/android/settings/dashboard/d;->Cx()I

    move-result v0

    sub-int v3, v2, v0

    new-instance v0, Lcom/android/settings/dashboard/i;

    invoke-direct {v0, v1, v2, v3}, Lcom/android/settings/dashboard/i;-><init>(ZII)V

    goto :goto_0
.end method

.method private Cu(Ljava/lang/Object;IZI)V
    .locals 4

    if-eqz p3, :cond_0

    iget-object v1, p0, Lcom/android/settings/dashboard/d;->Jk:Ljava/util/List;

    new-instance v2, Lcom/android/settings/dashboard/g;

    iget v0, p0, Lcom/android/settings/dashboard/d;->mId:I

    add-int v3, v0, p4

    iget-object v0, p0, Lcom/android/settings/dashboard/d;->Jj:Lcom/android/settings/dashboard/conditional/e;

    if-ne p1, v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-direct {v2, p1, p2, v3, v0}, Lcom/android/settings/dashboard/g;-><init>(Ljava/lang/Object;IIZ)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    iget v0, p0, Lcom/android/settings/dashboard/d;->mId:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/android/settings/dashboard/d;->mId:I

    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private Cv(Lcom/android/settingslib/drawer/Tile;Z)V
    .locals 5

    const/4 v4, 0x0

    if-eqz p2, :cond_0

    iget-object v0, p0, Lcom/android/settings/dashboard/d;->Jk:Ljava/util/List;

    new-instance v1, Lcom/android/settings/dashboard/g;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v3, p1, Lcom/android/settingslib/drawer/Tile;->title:Ljava/lang/CharSequence;

    aput-object v3, v2, v4

    invoke-static {v2}, Ljava/util/Objects;->hash([Ljava/lang/Object;)I

    move-result v2

    const v3, 0x7f0d01f4

    invoke-direct {v1, p1, v3, v2, v4}, Lcom/android/settings/dashboard/g;-><init>(Ljava/lang/Object;IIZ)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    iget v0, p0, Lcom/android/settings/dashboard/d;->mId:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/android/settings/dashboard/d;->mId:I

    return-void
.end method


# virtual methods
.method public CA(I)Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/dashboard/d;->Jk:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settings/dashboard/g;

    iget-object v0, v0, Lcom/android/settings/dashboard/g;->Jv:Ljava/lang/Object;

    return-object v0
.end method

.method public CB(I)I
    .locals 1

    iget-object v0, p0, Lcom/android/settings/dashboard/d;->Jk:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settings/dashboard/g;

    iget v0, v0, Lcom/android/settings/dashboard/g;->Jw:I

    return v0
.end method

.method public CC()Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/dashboard/d;->Jk:Ljava/util/List;

    return-object v0
.end method

.method public CD(I)I
    .locals 1

    iget-object v0, p0, Lcom/android/settings/dashboard/d;->Jk:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settings/dashboard/g;

    iget v0, v0, Lcom/android/settings/dashboard/g;->type:I

    return v0
.end method

.method public CE(Lcom/android/settingslib/drawer/Tile;)I
    .locals 4

    iget-object v0, p0, Lcom/android/settings/dashboard/d;->Jk:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_2

    iget-object v0, p0, Lcom/android/settings/dashboard/d;->Jk:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settings/dashboard/g;

    iget-object v0, v0, Lcom/android/settings/dashboard/g;->Jv:Ljava/lang/Object;

    if-ne v0, p1, :cond_0

    return v1

    :cond_0
    instance-of v3, v0, Lcom/android/settingslib/drawer/Tile;

    if-eqz v3, :cond_1

    iget-object v3, p1, Lcom/android/settingslib/drawer/Tile;->title:Ljava/lang/CharSequence;

    check-cast v0, Lcom/android/settingslib/drawer/Tile;

    iget-object v0, v0, Lcom/android/settingslib/drawer/Tile;->title:Ljava/lang/CharSequence;

    invoke-virtual {v3, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    return v1

    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_2
    const/4 v0, -0x1

    return v0
.end method

.method public CF()I
    .locals 1

    iget v0, p0, Lcom/android/settings/dashboard/d;->Jl:I

    return v0
.end method

.method public CG()Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/dashboard/d;->Jm:Ljava/util/List;

    return-object v0
.end method

.method public CH()Z
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    iget v2, p0, Lcom/android/settings/dashboard/d;->Jl:I

    if-eq v2, v0, :cond_0

    iget v2, p0, Lcom/android/settings/dashboard/d;->Jl:I

    if-nez v2, :cond_2

    iget-object v2, p0, Lcom/android/settings/dashboard/d;->Jm:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    const/4 v3, 0x2

    if-le v2, v3, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    move v0, v1

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method public Cw()Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/dashboard/d;->Jh:Ljava/util/List;

    return-object v0
.end method

.method public Cx()I
    .locals 4

    const/4 v3, 0x2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/android/settings/dashboard/d;->Jm:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    iget v2, p0, Lcom/android/settings/dashboard/d;->Jl:I

    if-nez v2, :cond_1

    invoke-static {v3, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget v2, p0, Lcom/android/settings/dashboard/d;->Jl:I

    if-eq v2, v3, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method public Cy()Lcom/android/settings/dashboard/conditional/e;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/dashboard/d;->Jj:Lcom/android/settings/dashboard/conditional/e;

    return-object v0
.end method

.method public Cz(J)Ljava/lang/Object;
    .locals 5

    iget-object v0, p0, Lcom/android/settings/dashboard/d;->Jk:Ljava/util/List;

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settings/dashboard/g;

    iget v2, v0, Lcom/android/settings/dashboard/g;->Jw:I

    int-to-long v2, v2

    cmp-long v2, v2, p1

    if-nez v2, :cond_0

    iget-object v0, v0, Lcom/android/settings/dashboard/g;->Jv:Ljava/lang/Object;

    return-object v0

    :cond_1
    const/4 v0, 0x0

    return-object v0
.end method

.method public size()I
    .locals 1

    iget-object v0, p0, Lcom/android/settings/dashboard/d;->Jk:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method
