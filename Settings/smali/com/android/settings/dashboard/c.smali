.class public Lcom/android/settings/dashboard/c;
.super Landroid/support/v7/widget/d;
.source "DashboardDecorator.java"


# instance fields
.field private final IX:Landroid/graphics/drawable/Drawable;

.field private final mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 4

    invoke-direct {p0}, Landroid/support/v7/widget/d;-><init>()V

    iput-object p1, p0, Lcom/android/settings/dashboard/c;->mContext:Landroid/content/Context;

    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    iget-object v1, p0, Lcom/android/settings/dashboard/c;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v1

    const v2, 0x1010214

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v0, v3}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    iget-object v1, p0, Lcom/android/settings/dashboard/c;->mContext:Landroid/content/Context;

    iget v0, v0, Landroid/util/TypedValue;->resourceId:I

    invoke-virtual {v1, v0}, Landroid/content/Context;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/dashboard/c;->IX:Landroid/graphics/drawable/Drawable;

    return-void
.end method

.method private Cl(Landroid/view/View;)I
    .locals 2

    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView$LayoutParams;

    invoke-virtual {p1}, Landroid/view/View;->getTop()I

    move-result v1

    iget v0, v0, Landroid/support/v7/widget/RecyclerView$LayoutParams;->topMargin:I

    add-int/2addr v0, v1

    invoke-static {p1}, Landroid/support/v4/view/z;->dPT(Landroid/view/View;)F

    move-result v1

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method


# virtual methods
.method public Cm(Landroid/graphics/Canvas;Landroid/support/v7/widget/RecyclerView;Landroid/support/v7/widget/e;)V
    .locals 7

    invoke-virtual {p2}, Landroid/support/v7/widget/RecyclerView;->getChildCount()I

    move-result v1

    const/4 v0, 0x1

    :goto_0
    if-ge v0, v1, :cond_3

    invoke-virtual {p2, v0}, Landroid/support/v7/widget/RecyclerView;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {p2, v2}, Landroid/support/v7/widget/RecyclerView;->doJ(Landroid/view/View;)Landroid/support/v7/widget/p;

    move-result-object v3

    invoke-virtual {v3}, Landroid/support/v7/widget/p;->getItemViewType()I

    move-result v4

    const v5, 0x7f0d0086

    if-ne v4, v5, :cond_1

    add-int/lit8 v3, v0, -0x1

    invoke-virtual {p2, v3}, Landroid/support/v7/widget/RecyclerView;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {p2, v3}, Landroid/support/v7/widget/RecyclerView;->doJ(Landroid/view/View;)Landroid/support/v7/widget/p;

    move-result-object v3

    invoke-virtual {v3}, Landroid/support/v7/widget/p;->getItemViewType()I

    move-result v3

    const v4, 0x7f0d0088

    if-eq v3, v4, :cond_2

    :cond_0
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    invoke-virtual {v3}, Landroid/support/v7/widget/p;->getItemViewType()I

    move-result v3

    const v4, 0x7f0d0062

    if-ne v3, v4, :cond_0

    :cond_2
    invoke-direct {p0, v2}, Lcom/android/settings/dashboard/c;->Cl(Landroid/view/View;)I

    move-result v3

    iget-object v4, p0, Lcom/android/settings/dashboard/c;->IX:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v2}, Landroid/view/View;->getLeft()I

    move-result v5

    invoke-virtual {v2}, Landroid/view/View;->getRight()I

    move-result v2

    iget-object v6, p0, Lcom/android/settings/dashboard/c;->IX:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v6}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v6

    add-int/2addr v6, v3

    invoke-virtual {v4, v5, v3, v2, v6}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    iget-object v2, p0, Lcom/android/settings/dashboard/c;->IX:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v2, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    goto :goto_1

    :cond_3
    return-void
.end method
