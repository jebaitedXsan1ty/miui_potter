.class Lcom/android/settings/dashboard/b;
.super Landroid/os/AsyncTask;
.source "DashboardSummary.java"


# instance fields
.field final synthetic IW:Lcom/android/settings/dashboard/DashboardSummary;


# direct methods
.method private constructor <init>(Lcom/android/settings/dashboard/DashboardSummary;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/dashboard/b;->IW:Lcom/android/settings/dashboard/DashboardSummary;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/settings/dashboard/DashboardSummary;Lcom/android/settings/dashboard/b;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/settings/dashboard/b;-><init>(Lcom/android/settings/dashboard/DashboardSummary;)V

    return-void
.end method


# virtual methods
.method protected Ck(Ljava/util/List;)V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/dashboard/b;->IW:Lcom/android/settings/dashboard/DashboardSummary;

    invoke-static {v0}, Lcom/android/settings/dashboard/DashboardSummary;->Cg(Lcom/android/settings/dashboard/DashboardSummary;)Landroid/os/Handler;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/android/settings/dashboard/b;->IW:Lcom/android/settings/dashboard/DashboardSummary;

    invoke-virtual {v0, p1}, Lcom/android/settings/dashboard/DashboardSummary;->updateCategoryAndSuggestion(Ljava/util/List;)V

    return-void
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/android/settings/dashboard/b;->doInBackground([Ljava/lang/Void;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/util/List;
    .locals 6

    iget-object v0, p0, Lcom/android/settings/dashboard/b;->IW:Lcom/android/settings/dashboard/DashboardSummary;

    invoke-virtual {v0}, Lcom/android/settings/dashboard/DashboardSummary;->getContext()Landroid/content/Context;

    move-result-object v2

    iget-object v0, p0, Lcom/android/settings/dashboard/b;->IW:Lcom/android/settings/dashboard/DashboardSummary;

    invoke-static {v0}, Lcom/android/settings/dashboard/DashboardSummary;->Ch(Lcom/android/settings/dashboard/DashboardSummary;)Lcom/android/settings/dashboard/suggestions/a;

    move-result-object v0

    invoke-interface {v0, v2}, Lcom/android/settings/dashboard/suggestions/a;->Bx(Landroid/content/Context;)Z

    move-result v0

    iget-object v1, p0, Lcom/android/settings/dashboard/b;->IW:Lcom/android/settings/dashboard/DashboardSummary;

    invoke-static {v1}, Lcom/android/settings/dashboard/DashboardSummary;->Ci(Lcom/android/settings/dashboard/DashboardSummary;)Lcom/android/settingslib/z;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/android/settingslib/z;->crD(Z)Ljava/util/List;

    move-result-object v3

    if-eqz v0, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    invoke-interface {v3}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settingslib/drawer/Tile;

    iget-object v5, p0, Lcom/android/settings/dashboard/b;->IW:Lcom/android/settings/dashboard/DashboardSummary;

    invoke-static {v5}, Lcom/android/settings/dashboard/DashboardSummary;->Ch(Lcom/android/settings/dashboard/DashboardSummary;)Lcom/android/settings/dashboard/suggestions/a;

    move-result-object v5

    invoke-interface {v5, v2, v0}, Lcom/android/settings/dashboard/suggestions/a;->Bv(Landroid/content/Context;Lcom/android/settingslib/drawer/Tile;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/android/settings/dashboard/b;->IW:Lcom/android/settings/dashboard/DashboardSummary;

    invoke-static {v0}, Lcom/android/settings/dashboard/DashboardSummary;->Ch(Lcom/android/settings/dashboard/DashboardSummary;)Lcom/android/settings/dashboard/suggestions/a;

    move-result-object v0

    invoke-interface {v0, v3, v1}, Lcom/android/settings/dashboard/suggestions/a;->Bz(Ljava/util/List;Ljava/util/List;)V

    :cond_1
    const/4 v0, 0x0

    move v1, v0

    :goto_1
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settingslib/drawer/Tile;

    iget-object v4, p0, Lcom/android/settings/dashboard/b;->IW:Lcom/android/settings/dashboard/DashboardSummary;

    invoke-static {v4}, Lcom/android/settings/dashboard/DashboardSummary;->Cj(Lcom/android/settings/dashboard/DashboardSummary;)Lcom/android/settings/dashboard/suggestions/d;

    move-result-object v4

    invoke-virtual {v4, v0}, Lcom/android/settings/dashboard/suggestions/d;->BE(Lcom/android/settingslib/drawer/Tile;)Z

    move-result v4

    if-eqz v4, :cond_2

    iget-object v4, p0, Lcom/android/settings/dashboard/b;->IW:Lcom/android/settings/dashboard/DashboardSummary;

    invoke-static {v4}, Lcom/android/settings/dashboard/DashboardSummary;->Ch(Lcom/android/settings/dashboard/DashboardSummary;)Lcom/android/settings/dashboard/suggestions/a;

    move-result-object v4

    iget-object v5, p0, Lcom/android/settings/dashboard/b;->IW:Lcom/android/settings/dashboard/DashboardSummary;

    invoke-static {v5}, Lcom/android/settings/dashboard/DashboardSummary;->Ci(Lcom/android/settings/dashboard/DashboardSummary;)Lcom/android/settingslib/z;

    move-result-object v5

    invoke-interface {v4, v2, v5, v0}, Lcom/android/settings/dashboard/suggestions/a;->Bu(Landroid/content/Context;Lcom/android/settingslib/z;Lcom/android/settingslib/drawer/Tile;)V

    add-int/lit8 v0, v1, -0x1

    invoke-interface {v3, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move v1, v0

    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    :cond_3
    return-object v3
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    check-cast p1, Ljava/util/List;

    invoke-virtual {p0, p1}, Lcom/android/settings/dashboard/b;->Ck(Ljava/util/List;)V

    return-void
.end method
