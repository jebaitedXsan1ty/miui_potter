.class public Lcom/android/settings/dashboard/j;
.super Landroid/support/v7/widget/Z;
.source "DashboardItemAnimator.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/support/v7/widget/Z;-><init>()V

    return-void
.end method


# virtual methods
.method public CY(Landroid/support/v7/widget/p;Landroid/support/v7/widget/p;IIII)Z
    .locals 7

    iget-object v0, p1, Landroid/support/v7/widget/p;->itemView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    instance-of v0, v0, Lcom/android/settingslib/drawer/Tile;

    if-eqz v0, :cond_1

    if-ne p1, p2, :cond_1

    invoke-virtual {p0}, Lcom/android/settings/dashboard/j;->isRunning()Z

    move-result v0

    if-nez v0, :cond_0

    int-to-float v0, p3

    iget-object v1, p1, Landroid/support/v7/widget/p;->itemView:Landroid/view/View;

    invoke-static {v1}, Landroid/support/v4/view/z;->dPC(Landroid/view/View;)F

    move-result v1

    add-float/2addr v0, v1

    float-to-int p3, v0

    int-to-float v0, p4

    iget-object v1, p1, Landroid/support/v7/widget/p;->itemView:Landroid/view/View;

    invoke-static {v1}, Landroid/support/v4/view/z;->dPT(Landroid/view/View;)F

    move-result v1

    add-float/2addr v0, v1

    float-to-int p4, v0

    :cond_0
    if-ne p3, p5, :cond_2

    if-ne p4, p6, :cond_2

    invoke-virtual {p0, p1}, Lcom/android/settings/dashboard/j;->dxQ(Landroid/support/v7/widget/p;)V

    const/4 v0, 0x0

    return v0

    :cond_1
    move v4, p4

    move v3, p3

    :goto_0
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v5, p5

    move v6, p6

    invoke-super/range {v0 .. v6}, Landroid/support/v7/widget/Z;->CY(Landroid/support/v7/widget/p;Landroid/support/v7/widget/p;IIII)Z

    move-result v0

    return v0

    :cond_2
    move v4, p4

    move v3, p3

    goto :goto_0
.end method
