.class public Lcom/android/settings/tts/a;
.super Lmiui/preference/RadioButtonPreference;
.source "TtsEnginePreference.java"


# instance fields
.field private final bjj:Landroid/speech/tts/TextToSpeech$EngineInfo;

.field private volatile bjk:Z

.field private bjl:Landroid/widget/RadioButton;

.field private final bjm:Landroid/widget/CompoundButton$OnCheckedChangeListener;

.field private final bjn:Lcom/android/settings/tts/b;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/speech/tts/TextToSpeech$EngineInfo;Lcom/android/settings/tts/b;Landroid/app/Activity;)V
    .locals 1

    invoke-direct {p0, p1}, Lmiui/preference/RadioButtonPreference;-><init>(Landroid/content/Context;)V

    new-instance v0, Lcom/android/settings/tts/c;

    invoke-direct {v0, p0}, Lcom/android/settings/tts/c;-><init>(Lcom/android/settings/tts/a;)V

    iput-object v0, p0, Lcom/android/settings/tts/a;->bjm:Landroid/widget/CompoundButton$OnCheckedChangeListener;

    iput-object p3, p0, Lcom/android/settings/tts/a;->bjn:Lcom/android/settings/tts/b;

    iput-object p2, p0, Lcom/android/settings/tts/a;->bjj:Landroid/speech/tts/TextToSpeech$EngineInfo;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/settings/tts/a;->bjk:Z

    iget-object v0, p0, Lcom/android/settings/tts/a;->bjj:Landroid/speech/tts/TextToSpeech$EngineInfo;

    iget-object v0, v0, Landroid/speech/tts/TextToSpeech$EngineInfo;->name:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/android/settings/tts/a;->setKey(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/settings/tts/a;->bjj:Landroid/speech/tts/TextToSpeech$EngineInfo;

    iget-object v0, v0, Landroid/speech/tts/TextToSpeech$EngineInfo;->label:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/android/settings/tts/a;->setTitle(Ljava/lang/CharSequence;)V

    return-void
.end method

.method static synthetic aXA(Lcom/android/settings/tts/a;Landroid/widget/Checkable;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/settings/tts/a;->aXx(Landroid/widget/Checkable;)V

    return-void
.end method

.method static synthetic aXB(Lcom/android/settings/tts/a;Landroid/widget/CompoundButton;Z)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/android/settings/tts/a;->aXy(Landroid/widget/CompoundButton;Z)V

    return-void
.end method

.method private aXw(Landroid/content/DialogInterface$OnClickListener;Landroid/content/DialogInterface$OnClickListener;)V
    .locals 7

    const/4 v6, 0x1

    const-string/jumbo v0, "TtsEnginePreference"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Displaying data alert for :"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/settings/tts/a;->bjj:Landroid/speech/tts/TextToSpeech$EngineInfo;

    iget-object v2, v2, Landroid/speech/tts/TextToSpeech$EngineInfo;->name:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/android/settings/tts/a;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v1, 0x1040014

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/android/settings/tts/a;->getContext()Landroid/content/Context;

    move-result-object v2

    new-array v3, v6, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/android/settings/tts/a;->bjj:Landroid/speech/tts/TextToSpeech$EngineInfo;

    iget-object v4, v4, Landroid/speech/tts/TextToSpeech$EngineInfo;->label:Ljava/lang/String;

    const/4 v5, 0x0

    aput-object v4, v3, v5

    const v4, 0x7f1212a8

    invoke-virtual {v2, v4, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1, v6}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x104000a

    invoke-virtual {v1, v2, p1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const/high16 v2, 0x1040000

    invoke-virtual {v1, v2, p2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    return-void
.end method

.method private aXx(Landroid/widget/Checkable;)V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/tts/a;->bjn:Lcom/android/settings/tts/b;

    invoke-interface {v0}, Lcom/android/settings/tts/b;->aXC()Landroid/widget/Checkable;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/tts/a;->bjn:Lcom/android/settings/tts/b;

    invoke-interface {v0}, Lcom/android/settings/tts/b;->aXC()Landroid/widget/Checkable;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Landroid/widget/Checkable;->setChecked(Z)V

    :cond_0
    iget-object v0, p0, Lcom/android/settings/tts/a;->bjn:Lcom/android/settings/tts/b;

    invoke-interface {v0, p1}, Lcom/android/settings/tts/b;->aXE(Landroid/widget/Checkable;)V

    iget-object v0, p0, Lcom/android/settings/tts/a;->bjn:Lcom/android/settings/tts/b;

    invoke-virtual {p0}, Lcom/android/settings/tts/a;->getKey()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/android/settings/tts/b;->aXF(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/settings/tts/a;->bjn:Lcom/android/settings/tts/b;

    invoke-interface {v0}, Lcom/android/settings/tts/b;->aXD()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/settings/tts/a;->callChangeListener(Ljava/lang/Object;)Z

    return-void
.end method

.method private aXy(Landroid/widget/CompoundButton;Z)V
    .locals 2

    iget-boolean v0, p0, Lcom/android/settings/tts/a;->bjk:Z

    if-nez v0, :cond_0

    invoke-virtual {p1}, Landroid/widget/CompoundButton;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    return-void

    :cond_1
    if-eqz p2, :cond_2

    invoke-direct {p0}, Lcom/android/settings/tts/a;->aXz()Z

    move-result v0

    if-eqz v0, :cond_3

    new-instance v0, Lcom/android/settings/tts/e;

    invoke-direct {v0, p0, p1}, Lcom/android/settings/tts/e;-><init>(Lcom/android/settings/tts/a;Landroid/widget/CompoundButton;)V

    new-instance v1, Lcom/android/settings/tts/f;

    invoke-direct {v1, p0, p1}, Lcom/android/settings/tts/f;-><init>(Lcom/android/settings/tts/a;Landroid/widget/CompoundButton;)V

    invoke-direct {p0, v0, v1}, Lcom/android/settings/tts/a;->aXw(Landroid/content/DialogInterface$OnClickListener;Landroid/content/DialogInterface$OnClickListener;)V

    :cond_2
    :goto_0
    return-void

    :cond_3
    invoke-direct {p0, p1}, Lcom/android/settings/tts/a;->aXx(Landroid/widget/Checkable;)V

    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Landroid/widget/CompoundButton;->setChecked(Z)V

    goto :goto_0
.end method

.method private aXz()Z
    .locals 1

    iget-object v0, p0, Lcom/android/settings/tts/a;->bjj:Landroid/speech/tts/TextToSpeech$EngineInfo;

    iget-boolean v0, v0, Landroid/speech/tts/TextToSpeech$EngineInfo;->system:Z

    xor-int/lit8 v0, v0, 0x1

    return v0
.end method


# virtual methods
.method public onBindView(Landroid/view/View;)V
    .locals 3

    invoke-super {p0, p1}, Lmiui/preference/RadioButtonPreference;->onBindView(Landroid/view/View;)V

    iget-object v0, p0, Lcom/android/settings/tts/a;->bjn:Lcom/android/settings/tts/b;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "Call to getView() before a call tosetSharedState()"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    const v0, 0x1020001

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioButton;

    iget-object v1, p0, Lcom/android/settings/tts/a;->bjm:Landroid/widget/CompoundButton$OnCheckedChangeListener;

    invoke-virtual {v0, v1}, Landroid/widget/RadioButton;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    invoke-virtual {p0}, Lcom/android/settings/tts/a;->getKey()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/android/settings/tts/a;->bjn:Lcom/android/settings/tts/b;

    invoke-interface {v2}, Lcom/android/settings/tts/b;->aXD()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v2, p0, Lcom/android/settings/tts/a;->bjn:Lcom/android/settings/tts/b;

    invoke-interface {v2, v0}, Lcom/android/settings/tts/b;->aXE(Landroid/widget/Checkable;)V

    :cond_1
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/android/settings/tts/a;->bjk:Z

    invoke-virtual {v0, v1}, Landroid/widget/RadioButton;->setChecked(Z)V

    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/android/settings/tts/a;->bjk:Z

    iput-object v0, p0, Lcom/android/settings/tts/a;->bjl:Landroid/widget/RadioButton;

    new-instance v1, Lcom/android/settings/tts/d;

    invoke-direct {v1, p0, v0}, Lcom/android/settings/tts/d;-><init>(Lcom/android/settings/tts/a;Landroid/widget/RadioButton;)V

    invoke-virtual {p1, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method
