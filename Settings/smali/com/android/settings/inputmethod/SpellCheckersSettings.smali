.class public Lcom/android/settings/inputmethod/SpellCheckersSettings;
.super Lcom/android/settings/MiuiSettingsPreferenceFragment;
.source "SpellCheckersSettings.java"

# interfaces
.implements Lcom/android/settings/widget/t;
.implements Landroid/preference/Preference$OnPreferenceClickListener;
.implements Landroid/preference/Preference$OnPreferenceChangeListener;


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private axi:Landroid/view/textservice/SpellCheckerInfo;

.field private axj:Landroid/app/AlertDialog;

.field private axk:[Landroid/view/textservice/SpellCheckerInfo;

.field private axl:Landroid/preference/Preference;

.field private axm:Landroid/preference/CheckBoxPreference;

.field private axn:Landroid/view/textservice/TextServicesManager;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/android/settings/inputmethod/SpellCheckersSettings;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/settings/inputmethod/SpellCheckersSettings;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/android/settings/MiuiSettingsPreferenceFragment;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/settings/inputmethod/SpellCheckersSettings;->axj:Landroid/app/AlertDialog;

    return-void
.end method

.method private alk(Landroid/view/textservice/SpellCheckerInfo;)V
    .locals 3

    invoke-virtual {p0}, Lcom/android/settings/inputmethod/SpellCheckersSettings;->bWB()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v1, "selected_spell_checker"

    invoke-virtual {p1}, Landroid/view/textservice/SpellCheckerInfo;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$Secure;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    invoke-virtual {p0}, Lcom/android/settings/inputmethod/SpellCheckersSettings;->bWB()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v1, "selected_spell_checker_subtype"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    invoke-direct {p0}, Lcom/android/settings/inputmethod/SpellCheckersSettings;->alr()V

    return-void
.end method

.method private static all(I)I
    .locals 1

    add-int/lit8 v0, p0, -0x1

    return v0
.end method

.method private static alm(I)I
    .locals 1

    add-int/lit8 v0, p0, 0x1

    return v0
.end method

.method private aln(Landroid/view/textservice/SpellCheckerInfo;Landroid/view/textservice/SpellCheckerSubtype;)Ljava/lang/CharSequence;
    .locals 3

    if-nez p1, :cond_0

    const v0, 0x7f1210fe

    invoke-virtual {p0, v0}, Lcom/android/settings/inputmethod/SpellCheckersSettings;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    if-nez p2, :cond_1

    const v0, 0x7f121380

    invoke-virtual {p0, v0}, Lcom/android/settings/inputmethod/SpellCheckersSettings;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_1
    invoke-virtual {p0}, Lcom/android/settings/inputmethod/SpellCheckersSettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {p1}, Landroid/view/textservice/SpellCheckerInfo;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Landroid/view/textservice/SpellCheckerInfo;->getServiceInfo()Landroid/content/pm/ServiceInfo;

    move-result-object v2

    iget-object v2, v2, Landroid/content/pm/ServiceInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    invoke-virtual {p2, v0, v1, v2}, Landroid/view/textservice/SpellCheckerSubtype;->getDisplayName(Landroid/content/Context;Ljava/lang/String;Landroid/content/pm/ApplicationInfo;)Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method private alo()V
    .locals 4

    const/4 v0, 0x0

    new-instance v1, Lcom/android/settings/inputmethod/SpellCheckerPreference;

    invoke-virtual {p0}, Lcom/android/settings/inputmethod/SpellCheckersSettings;->bWz()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lcom/android/settings/inputmethod/SpellCheckersSettings;->axk:[Landroid/view/textservice/SpellCheckerInfo;

    invoke-direct {v1, v2, v3}, Lcom/android/settings/inputmethod/SpellCheckerPreference;-><init>(Landroid/content/Context;[Landroid/view/textservice/SpellCheckerInfo;)V

    const v2, 0x7f120562

    invoke-virtual {v1, v2}, Lcom/android/settings/inputmethod/SpellCheckerPreference;->setTitle(I)V

    iget-object v2, p0, Lcom/android/settings/inputmethod/SpellCheckersSettings;->axk:[Landroid/view/textservice/SpellCheckerInfo;

    if-nez v2, :cond_0

    :goto_0
    if-lez v0, :cond_1

    const-string/jumbo v0, "%s"

    invoke-virtual {v1, v0}, Lcom/android/settings/inputmethod/SpellCheckerPreference;->setSummary(Ljava/lang/CharSequence;)V

    :goto_1
    const-string/jumbo v0, "default_spellchecker"

    invoke-virtual {v1, v0}, Lcom/android/settings/inputmethod/SpellCheckerPreference;->setKey(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Lcom/android/settings/inputmethod/SpellCheckerPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    invoke-virtual {p0}, Lcom/android/settings/inputmethod/SpellCheckersSettings;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/settings/inputmethod/SpellCheckersSettings;->axk:[Landroid/view/textservice/SpellCheckerInfo;

    array-length v0, v0

    goto :goto_0

    :cond_1
    const v0, 0x7f1210fe

    invoke-virtual {v1, v0}, Lcom/android/settings/inputmethod/SpellCheckerPreference;->setSummary(I)V

    goto :goto_1
.end method

.method private alp()V
    .locals 10

    const/4 v2, 0x0

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/android/settings/inputmethod/SpellCheckersSettings;->axj:Landroid/app/AlertDialog;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/settings/inputmethod/SpellCheckersSettings;->axj:Landroid/app/AlertDialog;

    invoke-virtual {v1}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/settings/inputmethod/SpellCheckersSettings;->axj:Landroid/app/AlertDialog;

    invoke-virtual {v1}, Landroid/app/AlertDialog;->dismiss()V

    :cond_0
    iget-object v1, p0, Lcom/android/settings/inputmethod/SpellCheckersSettings;->axn:Landroid/view/textservice/TextServicesManager;

    invoke-virtual {v1}, Landroid/view/textservice/TextServicesManager;->getCurrentSpellChecker()Landroid/view/textservice/SpellCheckerInfo;

    move-result-object v3

    if-nez v3, :cond_1

    return-void

    :cond_1
    iget-object v1, p0, Lcom/android/settings/inputmethod/SpellCheckersSettings;->axn:Landroid/view/textservice/TextServicesManager;

    invoke-virtual {v1, v0}, Landroid/view/textservice/TextServicesManager;->getCurrentSpellCheckerSubtype(Z)Landroid/view/textservice/SpellCheckerSubtype;

    move-result-object v4

    new-instance v5, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/android/settings/inputmethod/SpellCheckersSettings;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v5, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v1, 0x7f120c95

    invoke-virtual {v5, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v3}, Landroid/view/textservice/SpellCheckerInfo;->getSubtypeCount()I

    move-result v6

    add-int/lit8 v1, v6, 0x1

    new-array v7, v1, [Ljava/lang/CharSequence;

    invoke-direct {p0, v3, v2}, Lcom/android/settings/inputmethod/SpellCheckersSettings;->aln(Landroid/view/textservice/SpellCheckerInfo;Landroid/view/textservice/SpellCheckerSubtype;)Ljava/lang/CharSequence;

    move-result-object v1

    aput-object v1, v7, v0

    move v2, v0

    move v1, v0

    :goto_0
    if-ge v2, v6, :cond_2

    invoke-virtual {v3, v2}, Landroid/view/textservice/SpellCheckerInfo;->getSubtypeAt(I)Landroid/view/textservice/SpellCheckerSubtype;

    move-result-object v8

    invoke-static {v2}, Lcom/android/settings/inputmethod/SpellCheckersSettings;->alm(I)I

    move-result v0

    invoke-direct {p0, v3, v8}, Lcom/android/settings/inputmethod/SpellCheckersSettings;->aln(Landroid/view/textservice/SpellCheckerInfo;Landroid/view/textservice/SpellCheckerSubtype;)Ljava/lang/CharSequence;

    move-result-object v9

    aput-object v9, v7, v0

    invoke-virtual {v8, v4}, Landroid/view/textservice/SpellCheckerSubtype;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_3

    :goto_1
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    move v1, v0

    goto :goto_0

    :cond_2
    new-instance v0, Lcom/android/settings/inputmethod/SpellCheckersSettings$1;

    invoke-direct {v0, p0, v3}, Lcom/android/settings/inputmethod/SpellCheckersSettings$1;-><init>(Lcom/android/settings/inputmethod/SpellCheckersSettings;Landroid/view/textservice/SpellCheckerInfo;)V

    invoke-virtual {v5, v7, v1, v0}, Landroid/app/AlertDialog$Builder;->setSingleChoiceItems([Ljava/lang/CharSequence;ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v5}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/inputmethod/SpellCheckersSettings;->axj:Landroid/app/AlertDialog;

    iget-object v0, p0, Lcom/android/settings/inputmethod/SpellCheckersSettings;->axj:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    return-void

    :cond_3
    move v0, v1

    goto :goto_1
.end method

.method private alq(Landroid/view/textservice/SpellCheckerInfo;)V
    .locals 5

    const/4 v4, 0x1

    iget-object v0, p0, Lcom/android/settings/inputmethod/SpellCheckersSettings;->axj:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/inputmethod/SpellCheckersSettings;->axj:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/inputmethod/SpellCheckersSettings;->axj:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    :cond_0
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/android/settings/inputmethod/SpellCheckersSettings;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v1, 0x1040014

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    new-array v1, v4, [Ljava/lang/Object;

    invoke-virtual {p0}, Lcom/android/settings/inputmethod/SpellCheckersSettings;->bWA()Landroid/content/pm/PackageManager;

    move-result-object v2

    invoke-virtual {p1, v2}, Landroid/view/textservice/SpellCheckerInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    const v2, 0x7f121101

    invoke-virtual {p0, v2, v1}, Lcom/android/settings/inputmethod/SpellCheckersSettings;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0, v4}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    new-instance v1, Lcom/android/settings/inputmethod/SpellCheckersSettings$2;

    invoke-direct {v1, p0, p1}, Lcom/android/settings/inputmethod/SpellCheckersSettings$2;-><init>(Lcom/android/settings/inputmethod/SpellCheckersSettings;Landroid/view/textservice/SpellCheckerInfo;)V

    const v2, 0x104000a

    invoke-virtual {v0, v2, v1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    new-instance v1, Lcom/android/settings/inputmethod/SpellCheckersSettings$3;

    invoke-direct {v1, p0}, Lcom/android/settings/inputmethod/SpellCheckersSettings$3;-><init>(Lcom/android/settings/inputmethod/SpellCheckersSettings;)V

    const/high16 v2, 0x1040000

    invoke-virtual {v0, v2, v1}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/inputmethod/SpellCheckersSettings;->axj:Landroid/app/AlertDialog;

    iget-object v0, p0, Lcom/android/settings/inputmethod/SpellCheckersSettings;->axj:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    return-void
.end method

.method private alr()V
    .locals 7

    const/4 v0, 0x0

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/android/settings/inputmethod/SpellCheckersSettings;->axn:Landroid/view/textservice/TextServicesManager;

    invoke-virtual {v2}, Landroid/view/textservice/TextServicesManager;->getCurrentSpellChecker()Landroid/view/textservice/SpellCheckerInfo;

    move-result-object v2

    iput-object v2, p0, Lcom/android/settings/inputmethod/SpellCheckersSettings;->axi:Landroid/view/textservice/SpellCheckerInfo;

    iget-object v2, p0, Lcom/android/settings/inputmethod/SpellCheckersSettings;->axi:Landroid/view/textservice/SpellCheckerInfo;

    if-nez v2, :cond_0

    return-void

    :cond_0
    iget-object v2, p0, Lcom/android/settings/inputmethod/SpellCheckersSettings;->axn:Landroid/view/textservice/TextServicesManager;

    invoke-virtual {v2}, Landroid/view/textservice/TextServicesManager;->isSpellCheckerEnabled()Z

    move-result v3

    iget-object v2, p0, Lcom/android/settings/inputmethod/SpellCheckersSettings;->axm:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v2, v3}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    iget-object v2, p0, Lcom/android/settings/inputmethod/SpellCheckersSettings;->axi:Landroid/view/textservice/SpellCheckerInfo;

    if-eqz v2, :cond_1

    iget-object v0, p0, Lcom/android/settings/inputmethod/SpellCheckersSettings;->axn:Landroid/view/textservice/TextServicesManager;

    invoke-virtual {v0, v1}, Landroid/view/textservice/TextServicesManager;->getCurrentSpellCheckerSubtype(Z)Landroid/view/textservice/SpellCheckerSubtype;

    move-result-object v0

    :cond_1
    iget-object v2, p0, Lcom/android/settings/inputmethod/SpellCheckersSettings;->axl:Landroid/preference/Preference;

    iget-object v4, p0, Lcom/android/settings/inputmethod/SpellCheckersSettings;->axi:Landroid/view/textservice/SpellCheckerInfo;

    invoke-direct {p0, v4, v0}, Lcom/android/settings/inputmethod/SpellCheckersSettings;->aln(Landroid/view/textservice/SpellCheckerInfo;Landroid/view/textservice/SpellCheckerSubtype;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    invoke-virtual {p0}, Lcom/android/settings/inputmethod/SpellCheckersSettings;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v4

    invoke-virtual {v4}, Landroid/preference/PreferenceScreen;->getPreferenceCount()I

    move-result v5

    move v2, v1

    :goto_0
    if-ge v2, v5, :cond_4

    invoke-virtual {v4, v2}, Landroid/preference/PreferenceScreen;->getPreference(I)Landroid/preference/Preference;

    move-result-object v0

    iget-object v6, p0, Lcom/android/settings/inputmethod/SpellCheckersSettings;->axm:Landroid/preference/CheckBoxPreference;

    if-eq v0, v6, :cond_2

    invoke-virtual {v0, v3}, Landroid/preference/Preference;->setEnabled(Z)V

    :cond_2
    instance-of v6, v0, Lcom/android/settings/inputmethod/SpellCheckerPreference;

    if-eqz v6, :cond_3

    check-cast v0, Lcom/android/settings/inputmethod/SpellCheckerPreference;

    iget-object v6, p0, Lcom/android/settings/inputmethod/SpellCheckersSettings;->axi:Landroid/view/textservice/SpellCheckerInfo;

    invoke-virtual {v0, v6}, Lcom/android/settings/inputmethod/SpellCheckerPreference;->alf(Landroid/view/textservice/SpellCheckerInfo;)V

    :cond_3
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_4
    iget-object v2, p0, Lcom/android/settings/inputmethod/SpellCheckersSettings;->axl:Landroid/preference/Preference;

    if-eqz v3, :cond_5

    iget-object v0, p0, Lcom/android/settings/inputmethod/SpellCheckersSettings;->axi:Landroid/view/textservice/SpellCheckerInfo;

    if-eqz v0, :cond_5

    const/4 v0, 0x1

    :goto_1
    invoke-virtual {v2, v0}, Landroid/preference/Preference;->setEnabled(Z)V

    return-void

    :cond_5
    move v0, v1

    goto :goto_1
.end method

.method static synthetic als(Lcom/android/settings/inputmethod/SpellCheckersSettings;)Landroid/content/ContentResolver;
    .locals 1

    invoke-virtual {p0}, Lcom/android/settings/inputmethod/SpellCheckersSettings;->bWB()Landroid/content/ContentResolver;

    move-result-object v0

    return-object v0
.end method

.method static synthetic alt(I)I
    .locals 1

    invoke-static {p0}, Lcom/android/settings/inputmethod/SpellCheckersSettings;->all(I)I

    move-result v0

    return v0
.end method

.method static synthetic alu(Lcom/android/settings/inputmethod/SpellCheckersSettings;Landroid/view/textservice/SpellCheckerInfo;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/settings/inputmethod/SpellCheckersSettings;->alk(Landroid/view/textservice/SpellCheckerInfo;)V

    return-void
.end method

.method static synthetic alv(Lcom/android/settings/inputmethod/SpellCheckersSettings;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/inputmethod/SpellCheckersSettings;->alr()V

    return-void
.end method


# virtual methods
.method public gG(Landroid/widget/Switch;Z)V
    .locals 3

    invoke-virtual {p0}, Lcom/android/settings/inputmethod/SpellCheckersSettings;->bWB()Landroid/content/ContentResolver;

    move-result-object v1

    const-string/jumbo v2, "spell_checker_enabled"

    if-eqz p2, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v1, v2, v0}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    invoke-direct {p0}, Lcom/android/settings/inputmethod/SpellCheckersSettings;->alr()V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getMetricsCategory()I
    .locals 1

    const/16 v0, 0x3b

    return v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1

    invoke-super {p0, p1}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->onCreate(Landroid/os/Bundle;)V

    const v0, 0x7f1500e6

    invoke-virtual {p0, v0}, Lcom/android/settings/inputmethod/SpellCheckersSettings;->addPreferencesFromResource(I)V

    const-string/jumbo v0, "spellchecker_language"

    invoke-virtual {p0, v0}, Lcom/android/settings/inputmethod/SpellCheckersSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/inputmethod/SpellCheckersSettings;->axl:Landroid/preference/Preference;

    iget-object v0, p0, Lcom/android/settings/inputmethod/SpellCheckersSettings;->axl:Landroid/preference/Preference;

    invoke-virtual {v0, p0}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    const-string/jumbo v0, "textservices"

    invoke-virtual {p0, v0}, Lcom/android/settings/inputmethod/SpellCheckersSettings;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/textservice/TextServicesManager;

    iput-object v0, p0, Lcom/android/settings/inputmethod/SpellCheckersSettings;->axn:Landroid/view/textservice/TextServicesManager;

    iget-object v0, p0, Lcom/android/settings/inputmethod/SpellCheckersSettings;->axn:Landroid/view/textservice/TextServicesManager;

    invoke-virtual {v0}, Landroid/view/textservice/TextServicesManager;->getCurrentSpellChecker()Landroid/view/textservice/SpellCheckerInfo;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/inputmethod/SpellCheckersSettings;->axi:Landroid/view/textservice/SpellCheckerInfo;

    iget-object v0, p0, Lcom/android/settings/inputmethod/SpellCheckersSettings;->axn:Landroid/view/textservice/TextServicesManager;

    invoke-virtual {v0}, Landroid/view/textservice/TextServicesManager;->getEnabledSpellCheckers()[Landroid/view/textservice/SpellCheckerInfo;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/inputmethod/SpellCheckersSettings;->axk:[Landroid/view/textservice/SpellCheckerInfo;

    invoke-direct {p0}, Lcom/android/settings/inputmethod/SpellCheckersSettings;->alo()V

    return-void
.end method

.method public onPause()V
    .locals 0

    invoke-super {p0}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->onPause()V

    return-void
.end method

.method public onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 5

    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v0, 0x0

    iget-object v3, p0, Lcom/android/settings/inputmethod/SpellCheckersSettings;->axm:Landroid/preference/CheckBoxPreference;

    if-ne p1, v3, :cond_1

    instance-of v3, p2, Ljava/lang/Boolean;

    if-eqz v3, :cond_1

    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p0}, Lcom/android/settings/inputmethod/SpellCheckersSettings;->bWB()Landroid/content/ContentResolver;

    move-result-object v2

    const-string/jumbo v3, "spell_checker_enabled"

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    if-eqz v4, :cond_0

    move v0, v1

    :cond_0
    invoke-static {v2, v3, v0}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    invoke-direct {p0}, Lcom/android/settings/inputmethod/SpellCheckersSettings;->alr()V

    return v1

    :cond_1
    check-cast p2, Landroid/view/textservice/SpellCheckerInfo;

    if-eqz p2, :cond_6

    invoke-virtual {p2}, Landroid/view/textservice/SpellCheckerInfo;->getServiceInfo()Landroid/content/pm/ServiceInfo;

    move-result-object v3

    :goto_0
    if-eqz v3, :cond_2

    iget-object v2, v3, Landroid/content/pm/ServiceInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    :cond_2
    if-eqz v2, :cond_5

    iget v2, v2, Landroid/content/pm/ApplicationInfo;->flags:I

    and-int/lit8 v2, v2, 0x1

    if-eqz v2, :cond_3

    move v2, v1

    :goto_1
    if-eqz v2, :cond_4

    invoke-direct {p0, p2}, Lcom/android/settings/inputmethod/SpellCheckersSettings;->alk(Landroid/view/textservice/SpellCheckerInfo;)V

    return v1

    :cond_3
    move v2, v0

    goto :goto_1

    :cond_4
    invoke-direct {p0, p2}, Lcom/android/settings/inputmethod/SpellCheckersSettings;->alq(Landroid/view/textservice/SpellCheckerInfo;)V

    return v0

    :cond_5
    move v2, v0

    goto :goto_1

    :cond_6
    move-object v3, v2

    goto :goto_0
.end method

.method public onPreferenceClick(Landroid/preference/Preference;)Z
    .locals 1

    iget-object v0, p0, Lcom/android/settings/inputmethod/SpellCheckersSettings;->axl:Landroid/preference/Preference;

    if-ne p1, v0, :cond_0

    invoke-direct {p0}, Lcom/android/settings/inputmethod/SpellCheckersSettings;->alp()V

    const/4 v0, 0x1

    return v0

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public onResume()V
    .locals 1

    invoke-super {p0}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->onResume()V

    const-string/jumbo v0, "spellchecker_switch"

    invoke-virtual {p0, v0}, Lcom/android/settings/inputmethod/SpellCheckersSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    iput-object v0, p0, Lcom/android/settings/inputmethod/SpellCheckersSettings;->axm:Landroid/preference/CheckBoxPreference;

    iget-object v0, p0, Lcom/android/settings/inputmethod/SpellCheckersSettings;->axm:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, p0}, Landroid/preference/CheckBoxPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    invoke-direct {p0}, Lcom/android/settings/inputmethod/SpellCheckersSettings;->alr()V

    return-void
.end method
