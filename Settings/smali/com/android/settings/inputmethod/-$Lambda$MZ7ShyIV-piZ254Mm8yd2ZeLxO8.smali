.class final synthetic Lcom/android/settings/inputmethod/-$Lambda$MZ7ShyIV-piZ254Mm8yd2ZeLxO8;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/util/Comparator;


# instance fields
.field private final synthetic $id:B

.field private final synthetic -$f0:Ljava/lang/Object;


# direct methods
.method private final synthetic $m$0(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1

    iget-object v0, p0, Lcom/android/settings/inputmethod/-$Lambda$MZ7ShyIV-piZ254Mm8yd2ZeLxO8;->-$f0:Ljava/lang/Object;

    check-cast v0, Ljava/text/Collator;

    check-cast p1, Lcom/android/settingslib/d/a;

    check-cast p2, Lcom/android/settingslib/d/a;

    invoke-static {v0, p1, p2}, Lcom/android/settings/inputmethod/AvailableVirtualKeyboardFragment;->akE(Ljava/text/Collator;Lcom/android/settingslib/d/a;Lcom/android/settingslib/d/a;)I

    move-result v0

    return v0
.end method

.method private final synthetic $m$1(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1

    iget-object v0, p0, Lcom/android/settings/inputmethod/-$Lambda$MZ7ShyIV-piZ254Mm8yd2ZeLxO8;->-$f0:Ljava/lang/Object;

    check-cast v0, Ljava/text/Collator;

    check-cast p1, Lcom/android/settingslib/d/a;

    check-cast p2, Lcom/android/settingslib/d/a;

    invoke-static {v0, p1, p2}, Lcom/android/settings/inputmethod/VirtualKeyboardFragment;->akI(Ljava/text/Collator;Lcom/android/settingslib/d/a;Lcom/android/settingslib/d/a;)I

    move-result v0

    return v0
.end method

.method public synthetic constructor <init>(BLjava/lang/Object;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-byte p1, p0, Lcom/android/settings/inputmethod/-$Lambda$MZ7ShyIV-piZ254Mm8yd2ZeLxO8;->$id:B

    iput-object p2, p0, Lcom/android/settings/inputmethod/-$Lambda$MZ7ShyIV-piZ254Mm8yd2ZeLxO8;->-$f0:Ljava/lang/Object;

    return-void
.end method


# virtual methods
.method public final compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1

    iget-byte v0, p0, Lcom/android/settings/inputmethod/-$Lambda$MZ7ShyIV-piZ254Mm8yd2ZeLxO8;->$id:B

    packed-switch v0, :pswitch_data_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :pswitch_0
    invoke-direct {p0, p1, p2}, Lcom/android/settings/inputmethod/-$Lambda$MZ7ShyIV-piZ254Mm8yd2ZeLxO8;->$m$0(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v0

    return v0

    :pswitch_1
    invoke-direct {p0, p1, p2}, Lcom/android/settings/inputmethod/-$Lambda$MZ7ShyIV-piZ254Mm8yd2ZeLxO8;->$m$1(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v0

    return v0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
