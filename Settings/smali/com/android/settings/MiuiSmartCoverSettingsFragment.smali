.class public Lcom/android/settings/MiuiSmartCoverSettingsFragment;
.super Lcom/android/settings/MiuiSettingsPreferenceFragment;
.source "MiuiSmartCoverSettingsFragment.java"


# instance fields
.field private ceQ:Ljava/util/List;

.field private final ceR:Landroid/preference/Preference$OnPreferenceClickListener;

.field private ceS:Landroid/preference/CheckBoxPreference;

.field private ceT:Ljava/util/List;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/android/settings/MiuiSettingsPreferenceFragment;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/settings/MiuiSmartCoverSettingsFragment;->ceQ:Ljava/util/List;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/settings/MiuiSmartCoverSettingsFragment;->ceT:Ljava/util/List;

    new-instance v0, Lcom/android/settings/kZ;

    invoke-direct {v0, p0}, Lcom/android/settings/kZ;-><init>(Lcom/android/settings/MiuiSmartCoverSettingsFragment;)V

    iput-object v0, p0, Lcom/android/settings/MiuiSmartCoverSettingsFragment;->ceR:Landroid/preference/Preference$OnPreferenceClickListener;

    return-void
.end method

.method private bXk(I)V
    .locals 6

    const v3, 0x7f120ba6

    const-string/jumbo v2, "normal"

    const v1, 0x7f0803fb

    const v0, 0x7f0803fc

    packed-switch p1, :pswitch_data_0

    :goto_0
    new-instance v4, Lcom/android/settings/cT;

    invoke-virtual {p0}, Lcom/android/settings/MiuiSmartCoverSettingsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v5

    invoke-direct {v4, p0, v5, v1, v0}, Lcom/android/settings/cT;-><init>(Lcom/android/settings/MiuiSmartCoverSettingsFragment;Landroid/content/Context;II)V

    invoke-direct {p0, v4, v3, v2}, Lcom/android/settings/MiuiSmartCoverSettingsFragment;->bXn(Lcom/android/settings/cT;ILjava/lang/String;)V

    return-void

    :pswitch_0
    const v3, 0x7f1210df

    const-string/jumbo v2, "smallwindow"

    const v1, 0x7f0803fd

    const v0, 0x7f0803fe

    goto :goto_0

    :pswitch_1
    const v3, 0x7f1208ed

    const-string/jumbo v2, "lattice"

    const v1, 0x7f0803f9

    const v0, 0x7f0803fa

    goto :goto_0

    :pswitch_2
    const v3, 0x7f1207a8

    const-string/jumbo v2, "transparent"

    const v1, 0x7f0803ff

    const v0, 0x7f080400

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private bXl(ZI)V
    .locals 2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/android/settings/MiuiSmartCoverSettingsFragment;->ceQ:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/MiuiSmartCoverSettingsFragment;->ceQ:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settings/cT;

    packed-switch p2, :pswitch_data_0

    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :pswitch_0
    invoke-virtual {v0, p1}, Lcom/android/settings/cT;->setEnabled(Z)V

    goto :goto_1

    :pswitch_1
    invoke-virtual {v0, p1}, Lcom/android/settings/cT;->setChecked(Z)V

    goto :goto_1

    :cond_0
    return-void

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private bXm()V
    .locals 10

    const/4 v9, -0x2

    const/4 v2, 0x1

    const/4 v3, 0x0

    const-string/jumbo v0, "persist.sys.smartcover_mode"

    invoke-static {v0, v2}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    move-result v0

    if-eqz v0, :cond_1

    move v1, v2

    :goto_0
    const-string/jumbo v0, "smartcover_mode_enable"

    invoke-virtual {p0, v0}, Lcom/android/settings/MiuiSmartCoverSettingsFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    iput-object v0, p0, Lcom/android/settings/MiuiSmartCoverSettingsFragment;->ceS:Landroid/preference/CheckBoxPreference;

    iget-object v0, p0, Lcom/android/settings/MiuiSmartCoverSettingsFragment;->ceS:Landroid/preference/CheckBoxPreference;

    iget-object v4, p0, Lcom/android/settings/MiuiSmartCoverSettingsFragment;->ceR:Landroid/preference/Preference$OnPreferenceClickListener;

    invoke-virtual {v0, v4}, Landroid/preference/CheckBoxPreference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    iget-object v0, p0, Lcom/android/settings/MiuiSmartCoverSettingsFragment;->ceS:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    const-string/jumbo v0, "small_win_cover_type"

    invoke-static {v0}, Lmiui/util/FeatureParser;->getIntArray(Ljava/lang/String;)[I

    move-result-object v4

    if-eqz v4, :cond_0

    array-length v0, v4

    const/4 v5, 0x2

    if-ge v0, v5, :cond_2

    :cond_0
    return-void

    :cond_1
    move v1, v3

    goto :goto_0

    :cond_2
    array-length v5, v4

    move v0, v3

    :goto_1
    if-ge v0, v5, :cond_3

    aget v6, v4, v0

    iget-object v7, p0, Lcom/android/settings/MiuiSmartCoverSettingsFragment;->ceT:Ljava/util/List;

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-interface {v7, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-direct {p0, v6}, Lcom/android/settings/MiuiSmartCoverSettingsFragment;->bXk(I)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_3
    invoke-direct {p0, v1, v3}, Lcom/android/settings/MiuiSmartCoverSettingsFragment;->bXl(ZI)V

    const-string/jumbo v0, "persist.sys.smallwin_type"

    invoke-static {v0, v9}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    move-result v0

    if-ne v0, v9, :cond_5

    iget-object v0, p0, Lcom/android/settings/MiuiSmartCoverSettingsFragment;->ceQ:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settings/cT;

    :goto_2
    invoke-virtual {v0, v2}, Lcom/android/settings/cT;->setChecked(Z)V

    const-string/jumbo v0, "support_multiple_small_win_cover"

    invoke-static {v0, v3}, Lmiui/util/FeatureParser;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-virtual {p0}, Lcom/android/settings/MiuiSmartCoverSettingsFragment;->bWB()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v1, "smart_cover_key"

    invoke-static {v0, v1, v3, v3}, Landroid/provider/MiuiSettings$System;->putBooleanForUser(Landroid/content/ContentResolver;Ljava/lang/String;ZI)Z

    :cond_4
    return-void

    :cond_5
    iget-object v1, p0, Lcom/android/settings/MiuiSmartCoverSettingsFragment;->ceQ:Ljava/util/List;

    iget-object v4, p0, Lcom/android/settings/MiuiSmartCoverSettingsFragment;->ceT:Ljava/util/List;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v4, v0}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v0

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settings/cT;

    goto :goto_2
.end method

.method private bXn(Lcom/android/settings/cT;ILjava/lang/String;)V
    .locals 1

    invoke-virtual {p1, p2}, Lcom/android/settings/cT;->setTitle(I)V

    invoke-virtual {p1, p3}, Lcom/android/settings/cT;->setKey(Ljava/lang/String;)V

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/android/settings/cT;->setPersistent(Z)V

    invoke-virtual {p0}, Lcom/android/settings/MiuiSmartCoverSettingsFragment;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    iget-object v0, p0, Lcom/android/settings/MiuiSmartCoverSettingsFragment;->ceR:Landroid/preference/Preference$OnPreferenceClickListener;

    invoke-virtual {p1, v0}, Lcom/android/settings/cT;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    iget-object v0, p0, Lcom/android/settings/MiuiSmartCoverSettingsFragment;->ceQ:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method static synthetic bXo(Lcom/android/settings/MiuiSmartCoverSettingsFragment;)Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/MiuiSmartCoverSettingsFragment;->ceQ:Ljava/util/List;

    return-object v0
.end method

.method static synthetic bXp(Lcom/android/settings/MiuiSmartCoverSettingsFragment;)Landroid/preference/CheckBoxPreference;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/MiuiSmartCoverSettingsFragment;->ceS:Landroid/preference/CheckBoxPreference;

    return-object v0
.end method

.method static synthetic bXq(Lcom/android/settings/MiuiSmartCoverSettingsFragment;)Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/MiuiSmartCoverSettingsFragment;->ceT:Ljava/util/List;

    return-object v0
.end method

.method static synthetic bXr(Lcom/android/settings/MiuiSmartCoverSettingsFragment;ZI)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/android/settings/MiuiSmartCoverSettingsFragment;->bXl(ZI)V

    return-void
.end method


# virtual methods
.method public cC()Ljava/lang/String;
    .locals 1

    const-class v0, Lcom/android/settings/MiuiSmartCoverSettingsFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 1

    invoke-super {p0, p1}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->onActivityCreated(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/android/settings/MiuiSmartCoverSettingsFragment;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/preference/PreferenceScreen;->removeAll()V

    :cond_0
    const v0, 0x7f1500e1

    invoke-virtual {p0, v0}, Lcom/android/settings/MiuiSmartCoverSettingsFragment;->addPreferencesFromResource(I)V

    invoke-direct {p0}, Lcom/android/settings/MiuiSmartCoverSettingsFragment;->bXm()V

    return-void
.end method
