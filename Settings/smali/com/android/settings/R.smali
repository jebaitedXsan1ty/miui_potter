.class public Lcom/android/settings/R;
.super Ljava/lang/Object;
.source "MiuiOptionUtils.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static bmq(Landroid/content/Context;I)I
    .locals 4

    const/4 v0, 0x0

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string/jumbo v2, "airplane_mode_on"

    invoke-static {v1, v2, v0}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    const/4 v3, -0x1

    if-eq p1, v3, :cond_1

    if-eq p1, v2, :cond_1

    const-string/jumbo v2, "airplane_mode_on"

    invoke-static {v1, v2, p1}, Landroid/provider/Settings$Global;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    new-instance v1, Landroid/content/Intent;

    const-string/jumbo v2, "android.intent.action.AIRPLANE_MODE"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const/high16 v2, 0x20000000

    invoke-virtual {v1, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    const-string/jumbo v2, "state"

    if-eqz p1, :cond_0

    const/4 v0, 0x1

    :cond_0
    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    sget-object v0, Landroid/os/UserHandle;->ALL:Landroid/os/UserHandle;

    invoke-virtual {p0, v1, v0}, Landroid/content/Context;->sendBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    return p1

    :cond_1
    return v2
.end method

.method public static bmr(I)I
    .locals 6

    const/4 v2, 0x1

    const/4 v5, -0x1

    const/4 v1, 0x0

    invoke-static {}, Landroid/bluetooth/BluetoothAdapter;->getDefaultAdapter()Landroid/bluetooth/BluetoothAdapter;

    move-result-object v3

    invoke-virtual {v3}, Landroid/bluetooth/BluetoothAdapter;->getState()I

    move-result v4

    packed-switch v4, :pswitch_data_0

    :pswitch_0
    return v5

    :pswitch_1
    move v0, v1

    :goto_0
    if-eq p0, v5, :cond_1

    if-eq p0, v0, :cond_1

    if-eqz p0, :cond_0

    invoke-virtual {v3}, Landroid/bluetooth/BluetoothAdapter;->enable()Z

    :goto_1
    return p0

    :pswitch_2
    move v0, v2

    goto :goto_0

    :cond_0
    invoke-virtual {v3}, Landroid/bluetooth/BluetoothAdapter;->disable()Z

    goto :goto_1

    :cond_1
    const/16 v3, 0xb

    if-ne v4, v3, :cond_2

    return v2

    :cond_2
    const/16 v2, 0xd

    if-ne v4, v2, :cond_3

    return v1

    :cond_3
    return v0

    nop

    :pswitch_data_0
    .packed-switch 0xa
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public static bms(Landroid/content/Context;I)I
    .locals 5

    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string/jumbo v2, "gps"

    invoke-static {v3, v2}, Landroid/provider/Settings$Secure;->isLocationProviderEnabled(Landroid/content/ContentResolver;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    move v2, v0

    :goto_0
    const/4 v4, -0x1

    if-eq p1, v4, :cond_2

    if-eq p1, v2, :cond_2

    const-string/jumbo v2, "gps"

    if-eqz p1, :cond_1

    :goto_1
    invoke-static {v3, v2, v0}, Landroid/provider/Settings$Secure;->setLocationProviderEnabled(Landroid/content/ContentResolver;Ljava/lang/String;Z)V

    return p1

    :cond_0
    move v2, v1

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_1

    :cond_2
    return v2
.end method
