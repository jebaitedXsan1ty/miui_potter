.class final Lcom/android/settings/kh;
.super Ljava/lang/Object;
.source "KeyguardRestrictedListPreference.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field final synthetic cqM:Lcom/android/settings/KeyguardRestrictedListPreference;


# direct methods
.method constructor <init>(Lcom/android/settings/KeyguardRestrictedListPreference;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/kh;->cqM:Lcom/android/settings/KeyguardRestrictedListPreference;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 2

    if-ltz p2, :cond_0

    iget-object v0, p0, Lcom/android/settings/kh;->cqM:Lcom/android/settings/KeyguardRestrictedListPreference;

    invoke-virtual {v0}, Lcom/android/settings/KeyguardRestrictedListPreference;->getEntries()[Ljava/lang/CharSequence;

    move-result-object v0

    array-length v0, v0

    if-lt p2, v0, :cond_1

    :cond_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/settings/kh;->cqM:Lcom/android/settings/KeyguardRestrictedListPreference;

    invoke-virtual {v0}, Lcom/android/settings/KeyguardRestrictedListPreference;->getEntryValues()[Ljava/lang/CharSequence;

    move-result-object v0

    aget-object v0, v0, p2

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/kh;->cqM:Lcom/android/settings/KeyguardRestrictedListPreference;

    invoke-static {v1, v0}, Lcom/android/settings/KeyguardRestrictedListPreference;->bRC(Lcom/android/settings/KeyguardRestrictedListPreference;Ljava/lang/CharSequence;)Lcom/android/settings/cp;

    move-result-object v1

    if-eqz v1, :cond_3

    iget-object v0, p0, Lcom/android/settings/kh;->cqM:Lcom/android/settings/KeyguardRestrictedListPreference;

    invoke-virtual {v0}, Lcom/android/settings/KeyguardRestrictedListPreference;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, v1, Lcom/android/settings/cp;->bXG:Lcom/android/settingslib/n;

    invoke-static {v0, v1}, Lcom/android/settingslib/w;->cqW(Landroid/content/Context;Lcom/android/settingslib/n;)V

    :cond_2
    :goto_0
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    return-void

    :cond_3
    iget-object v1, p0, Lcom/android/settings/kh;->cqM:Lcom/android/settings/KeyguardRestrictedListPreference;

    invoke-virtual {v1}, Lcom/android/settings/KeyguardRestrictedListPreference;->getValue()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/android/settings/kh;->cqM:Lcom/android/settings/KeyguardRestrictedListPreference;

    invoke-static {v1, v0}, Lcom/android/settings/KeyguardRestrictedListPreference;->bRB(Lcom/android/settings/KeyguardRestrictedListPreference;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/android/settings/kh;->cqM:Lcom/android/settings/KeyguardRestrictedListPreference;

    invoke-virtual {v1, v0}, Lcom/android/settings/KeyguardRestrictedListPreference;->setValue(Ljava/lang/String;)V

    goto :goto_0
.end method
