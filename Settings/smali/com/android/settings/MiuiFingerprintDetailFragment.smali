.class public Lcom/android/settings/MiuiFingerprintDetailFragment;
.super Lcom/android/settings/MiuiSettingsPreferenceFragment;
.source "MiuiFingerprintDetailFragment.java"


# instance fields
.field private bXm:Landroid/widget/Button;

.field private bXn:Landroid/widget/EditText;

.field private bXo:Z

.field private bXp:Ljava/lang/String;

.field private bXq:Ljava/lang/String;

.field private bXr:Landroid/view/View;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/android/settings/MiuiSettingsPreferenceFragment;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/settings/MiuiFingerprintDetailFragment;->bXo:Z

    return-void
.end method

.method private bRi()V
    .locals 3

    iget-object v0, p0, Lcom/android/settings/MiuiFingerprintDetailFragment;->bXr:Landroid/view/View;

    const v1, 0x7f0a019d

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/android/settings/MiuiFingerprintDetailFragment;->bXn:Landroid/widget/EditText;

    iget-object v0, p0, Lcom/android/settings/MiuiFingerprintDetailFragment;->bXr:Landroid/view/View;

    const v1, 0x7f0a0193

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/android/settings/MiuiFingerprintDetailFragment;->bXm:Landroid/widget/Button;

    iget-object v0, p0, Lcom/android/settings/MiuiFingerprintDetailFragment;->bXn:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/android/settings/MiuiFingerprintDetailFragment;->bXq:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/android/settings/MiuiFingerprintDetailFragment;->bXn:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/android/settings/MiuiFingerprintDetailFragment;->bXn:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-interface {v1}, Landroid/text/Editable;->length()I

    move-result v1

    iget-object v2, p0, Lcom/android/settings/MiuiFingerprintDetailFragment;->bXn:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-interface {v2}, Landroid/text/Editable;->length()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/widget/EditText;->setSelection(II)V

    iget-object v0, p0, Lcom/android/settings/MiuiFingerprintDetailFragment;->bXm:Landroid/widget/Button;

    const/high16 v1, -0x10000

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setTextColor(I)V

    new-instance v0, Lcom/android/settings/kd;

    invoke-direct {v0, p0}, Lcom/android/settings/kd;-><init>(Lcom/android/settings/MiuiFingerprintDetailFragment;)V

    new-instance v1, Lcom/android/settings/ke;

    invoke-direct {v1, p0, v0}, Lcom/android/settings/ke;-><init>(Lcom/android/settings/MiuiFingerprintDetailFragment;Lcom/android/settings/f;)V

    iget-object v0, p0, Lcom/android/settings/MiuiFingerprintDetailFragment;->bXm:Landroid/widget/Button;

    new-instance v2, Lcom/android/settings/kf;

    invoke-direct {v2, p0, v1}, Lcom/android/settings/kf;-><init>(Lcom/android/settings/MiuiFingerprintDetailFragment;Landroid/content/DialogInterface$OnClickListener;)V

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method private bRj()V
    .locals 3

    iget-object v0, p0, Lcom/android/settings/MiuiFingerprintDetailFragment;->bXn:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-boolean v1, p0, Lcom/android/settings/MiuiFingerprintDetailFragment;->bXo:Z

    xor-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/android/settings/MiuiFingerprintDetailFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    iget-object v2, p0, Lcom/android/settings/MiuiFingerprintDetailFragment;->bXp:Ljava/lang/String;

    invoke-static {v1, v2, v0}, Lcom/android/settings/bd;->bAa(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method static synthetic bRk(Lcom/android/settings/MiuiFingerprintDetailFragment;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/MiuiFingerprintDetailFragment;->bXp:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic bRl(Lcom/android/settings/MiuiFingerprintDetailFragment;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/android/settings/MiuiFingerprintDetailFragment;->bXo:Z

    return p1
.end method


# virtual methods
.method public Vg(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2

    const v0, 0x7f0d00b4

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/MiuiFingerprintDetailFragment;->bXr:Landroid/view/View;

    invoke-direct {p0}, Lcom/android/settings/MiuiFingerprintDetailFragment;->bRi()V

    iget-object v0, p0, Lcom/android/settings/MiuiFingerprintDetailFragment;->bXr:Landroid/view/View;

    return-object v0
.end method

.method public cC()Ljava/lang/String;
    .locals 1

    const-class v0, Lcom/android/settings/MiuiFingerprintDetailFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/android/settings/MiuiFingerprintDetailFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_0

    const-string/jumbo v1, "extra_fingerprint_key"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/android/settings/MiuiFingerprintDetailFragment;->bXp:Ljava/lang/String;

    const-string/jumbo v1, "extra_fingerprint_title"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/MiuiFingerprintDetailFragment;->bXq:Ljava/lang/String;

    :cond_0
    return-void
.end method

.method public onPause()V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/MiuiFingerprintDetailFragment;->bRj()V

    invoke-super {p0}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->onPause()V

    return-void
.end method
