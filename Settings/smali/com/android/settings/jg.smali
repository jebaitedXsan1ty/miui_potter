.class final Lcom/android/settings/jg;
.super Ljava/lang/Object;
.source "AccessControlFragment.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field final synthetic cpm:Lcom/android/settings/AccessControlFragment;

.field final synthetic cpn:Landroid/preference/CheckBoxPreference;


# direct methods
.method constructor <init>(Lcom/android/settings/AccessControlFragment;Landroid/preference/CheckBoxPreference;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/jg;->cpm:Lcom/android/settings/AccessControlFragment;

    iput-object p2, p0, Lcom/android/settings/jg;->cpn:Landroid/preference/CheckBoxPreference;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 2

    const/4 v1, 0x1

    iget-object v0, p0, Lcom/android/settings/jg;->cpm:Lcom/android/settings/AccessControlFragment;

    invoke-static {v0}, Lcom/android/settings/AccessControlFragment;->bLr(Lcom/android/settings/AccessControlFragment;)Landroid/security/ChooseLockSettingsHelper;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/security/ChooseLockSettingsHelper;->setPrivacyModeEnabled(Z)V

    iget-object v0, p0, Lcom/android/settings/jg;->cpn:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    return-void
.end method
