.class public Lcom/android/settings/cloud/PullCloudDataReceiver;
.super Landroid/content/BroadcastReceiver;
.source "PullCloudDataReceiver.java"


# static fields
.field private static final TAG:Ljava/lang/String;


# direct methods
.method static synthetic -get0()Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/android/settings/cloud/PullCloudDataReceiver;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/android/settings/cloud/PullCloudDataReceiver;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/settings/cloud/PullCloudDataReceiver;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 3

    const/4 v2, 0x0

    if-eqz p2, :cond_0

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    :cond_0
    return-void

    :cond_1
    const-string/jumbo v0, "com.android.settings.action.PULL_CLOUD_DATA"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    sget-object v0, Lcom/android/settings/cloud/PullCloudDataReceiver;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "receive message, pull cloud data immediately"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {p1}, Lcom/android/settings/cloud/a/f;->aKn(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_2

    sget-object v0, Lcom/android/settings/cloud/PullCloudDataReceiver;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "network connected"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    sget-boolean v0, Lcom/android/settings/cloud/CloudJobService2;->aWv:Z

    if-nez v0, :cond_2

    sget-object v0, Lcom/android/settings/cloud/PullCloudDataReceiver;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "task start"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x1

    sput-boolean v0, Lcom/android/settings/cloud/CloudJobService2;->aWv:Z

    new-instance v0, Lcom/android/settings/cloud/a;

    new-instance v1, Lcom/android/settings/cloud/j;

    invoke-direct {v1, p0}, Lcom/android/settings/cloud/j;-><init>(Lcom/android/settings/cloud/PullCloudDataReceiver;)V

    invoke-direct {v0, p1, v2, v1}, Lcom/android/settings/cloud/a;-><init>(Landroid/content/Context;Landroid/app/job/JobParameters;Lcom/android/settings/cloud/b;)V

    sget-object v1, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Void;

    invoke-virtual {v0, v1, v2}, Lcom/android/settings/cloud/a;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    :cond_2
    return-void
.end method
