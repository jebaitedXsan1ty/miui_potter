.class public Lcom/android/settings/cloud/network/b;
.super Ljava/lang/Object;
.source "Connection.java"


# instance fields
.field private aWm:Ljava/util/TreeMap;

.field final synthetic this$0:Lcom/android/settings/cloud/network/a;


# direct methods
.method public constructor <init>(Lcom/android/settings/cloud/network/a;)V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lcom/android/settings/cloud/network/b;-><init>(Lcom/android/settings/cloud/network/a;Z)V

    return-void
.end method

.method public constructor <init>(Lcom/android/settings/cloud/network/a;Z)V
    .locals 1

    iput-object p1, p0, Lcom/android/settings/cloud/network/b;->this$0:Lcom/android/settings/cloud/network/a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/TreeMap;

    invoke-direct {v0}, Ljava/util/TreeMap;-><init>()V

    iput-object v0, p0, Lcom/android/settings/cloud/network/b;->aWm:Ljava/util/TreeMap;

    if-eqz p2, :cond_0

    iput-object p0, p1, Lcom/android/settings/cloud/network/a;->aVY:Lcom/android/settings/cloud/network/b;

    :cond_0
    return-void
.end method


# virtual methods
.method public aKL(Ljava/lang/String;Ljava/lang/String;)Lcom/android/settings/cloud/network/b;
    .locals 1

    if-nez p2, :cond_0

    const-string/jumbo p2, ""

    :cond_0
    iget-object v0, p0, Lcom/android/settings/cloud/network/b;->aWm:Ljava/util/TreeMap;

    invoke-virtual {v0, p1, p2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-object p0
.end method

.method public aKM()Ljava/util/TreeMap;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/cloud/network/b;->aWm:Ljava/util/TreeMap;

    return-object v0
.end method

.method protected aKN()V
    .locals 3

    iget-object v0, p0, Lcom/android/settings/cloud/network/b;->this$0:Lcom/android/settings/cloud/network/a;

    iget-object v0, v0, Lcom/android/settings/cloud/network/a;->aVY:Lcom/android/settings/cloud/network/b;

    const-string/jumbo v1, "sdk"

    sget v2, Lcom/android/settings/cloud/a/e;->aVQ:I

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/android/settings/cloud/network/b;->aKL(Ljava/lang/String;Ljava/lang/String;)Lcom/android/settings/cloud/network/b;

    iget-object v0, p0, Lcom/android/settings/cloud/network/b;->this$0:Lcom/android/settings/cloud/network/a;

    iget-object v0, v0, Lcom/android/settings/cloud/network/a;->aVY:Lcom/android/settings/cloud/network/b;

    const-string/jumbo v1, "os"

    sget-object v2, Lcom/android/settings/cloud/a/e;->aVR:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/android/settings/cloud/network/b;->aKL(Ljava/lang/String;Ljava/lang/String;)Lcom/android/settings/cloud/network/b;

    iget-object v0, p0, Lcom/android/settings/cloud/network/b;->this$0:Lcom/android/settings/cloud/network/a;

    iget-object v0, v0, Lcom/android/settings/cloud/network/a;->aVY:Lcom/android/settings/cloud/network/b;

    const-string/jumbo v1, "la"

    sget-object v2, Lcom/android/settings/cloud/a/e;->aVO:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/android/settings/cloud/network/b;->aKL(Ljava/lang/String;Ljava/lang/String;)Lcom/android/settings/cloud/network/b;

    iget-object v0, p0, Lcom/android/settings/cloud/network/b;->this$0:Lcom/android/settings/cloud/network/a;

    iget-object v0, v0, Lcom/android/settings/cloud/network/a;->aVY:Lcom/android/settings/cloud/network/b;

    const-string/jumbo v1, "co"

    sget-object v2, Lcom/android/settings/cloud/a/e;->aVN:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/android/settings/cloud/network/b;->aKL(Ljava/lang/String;Ljava/lang/String;)Lcom/android/settings/cloud/network/b;

    iget-object v0, p0, Lcom/android/settings/cloud/network/b;->this$0:Lcom/android/settings/cloud/network/a;

    iget-object v0, v0, Lcom/android/settings/cloud/network/a;->aVY:Lcom/android/settings/cloud/network/b;

    const-string/jumbo v1, "ro"

    sget-object v2, Lcom/android/settings/cloud/a/e;->aVP:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/android/settings/cloud/network/b;->aKL(Ljava/lang/String;Ljava/lang/String;)Lcom/android/settings/cloud/network/b;

    return-void
.end method

.method public isEmpty()Z
    .locals 1

    iget-object v0, p0, Lcom/android/settings/cloud/network/b;->aWm:Ljava/util/TreeMap;

    invoke-virtual {v0}, Ljava/util/TreeMap;->isEmpty()Z

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    iget-object v0, p0, Lcom/android/settings/cloud/network/b;->aWm:Ljava/util/TreeMap;

    invoke-virtual {v0}, Ljava/util/TreeMap;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string/jumbo v0, ""

    return-object v0

    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v0, p0, Lcom/android/settings/cloud/network/b;->aWm:Ljava/util/TreeMap;

    invoke-virtual {v0}, Ljava/util/TreeMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string/jumbo v3, "="

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :try_start_0
    iget-object v3, p0, Lcom/android/settings/cloud/network/b;->aWm:Ljava/util/TreeMap;

    invoke-virtual {v3, v0}, Ljava/util/TreeMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const-string/jumbo v3, "UTF-8"

    invoke-static {v0, v3}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_1
    const-string/jumbo v0, "&"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    :cond_1
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->deleteCharAt(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :catch_0
    move-exception v0

    goto :goto_1
.end method
