.class public Lcom/android/settings/cloud/a/a;
.super Ljava/lang/Object;
.source "TelephonyUtil.java"


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static aJX()I
    .locals 5

    const/4 v4, 0x0

    const/4 v1, 0x0

    const-string/jumbo v0, "miui.telephony.SubscriptionManager"

    invoke-static {v0}, Lcom/android/settings/cloud/a/d;->aKe(Ljava/lang/String;)Lcom/android/settings/cloud/a/d;

    move-result-object v0

    const-string/jumbo v2, "getDefault"

    new-array v3, v1, [Ljava/lang/Object;

    invoke-virtual {v0, v2, v4, v3}, Lcom/android/settings/cloud/a/d;->aKf(Ljava/lang/String;[Ljava/lang/Class;[Ljava/lang/Object;)Lcom/android/settings/cloud/a/d;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/settings/cloud/a/d;->aKg()Lcom/android/settings/cloud/a/d;

    move-result-object v0

    const-string/jumbo v2, "getDefaultDataSlotId"

    new-array v3, v1, [Ljava/lang/Object;

    invoke-virtual {v0, v2, v4, v3}, Lcom/android/settings/cloud/a/d;->aKh(Ljava/lang/String;[Ljava/lang/Class;[Ljava/lang/Object;)Lcom/android/settings/cloud/a/d;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/settings/cloud/a/d;->aKi()I

    move-result v0

    if-ltz v0, :cond_0

    const/4 v2, 0x1

    if-le v0, v2, :cond_1

    :cond_0
    move v0, v1

    :cond_1
    return v0
.end method

.method private static aJY()Ljava/lang/String;
    .locals 6

    const/4 v4, 0x1

    const/4 v5, 0x0

    const-string/jumbo v0, "miui.telephony.TelephonyManager"

    invoke-static {v0}, Lcom/android/settings/cloud/a/d;->aKe(Ljava/lang/String;)Lcom/android/settings/cloud/a/d;

    move-result-object v0

    const-string/jumbo v1, "getDefault"

    new-array v2, v5, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v3, v2}, Lcom/android/settings/cloud/a/d;->aKf(Ljava/lang/String;[Ljava/lang/Class;[Ljava/lang/Object;)Lcom/android/settings/cloud/a/d;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/settings/cloud/a/d;->aKg()Lcom/android/settings/cloud/a/d;

    move-result-object v0

    const-string/jumbo v1, "getSubscriberIdForSlot"

    new-array v2, v4, [Ljava/lang/Class;

    sget-object v3, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v3, v2, v5

    new-array v3, v4, [Ljava/lang/Object;

    invoke-static {}, Lcom/android/settings/cloud/a/a;->aJX()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-virtual {v0, v1, v2, v3}, Lcom/android/settings/cloud/a/d;->aKh(Ljava/lang/String;[Ljava/lang/Class;[Ljava/lang/Object;)Lcom/android/settings/cloud/a/d;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/settings/cloud/a/d;->aKj()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static aJZ()Ljava/lang/String;
    .locals 3

    const-string/jumbo v0, ""

    invoke-static {}, Lcom/android/settings/cloud/a/a;->aJY()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    const-string/jumbo v2, "46001"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string/jumbo v2, "46009"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    :cond_0
    const-string/jumbo v0, "UNICOM"

    :cond_1
    :goto_0
    return-object v0

    :cond_2
    const-string/jumbo v2, "46003"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_3

    const-string/jumbo v2, "46011"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_4

    :cond_3
    const-string/jumbo v0, "TELECOM"

    goto :goto_0

    :cond_4
    const-string/jumbo v2, "46000"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_5

    const-string/jumbo v2, "46002"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_5

    const-string/jumbo v2, "46007"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_5
    const-string/jumbo v0, "CMCC"

    goto :goto_0
.end method
