.class public Lcom/android/settings/cloud/a;
.super Landroid/os/AsyncTask;
.source "JobTask.java"


# static fields
.field private static final aWn:Landroid/net/Uri;

.field private static final aWo:Landroid/net/Uri;


# instance fields
.field private aWp:Lcom/android/settings/cloud/b;

.field private aWq:Landroid/app/job/JobParameters;

.field private mContext:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string/jumbo v0, "content://com.android.settings.cloud.CloudSettings/cloud_all_data/update_cache"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/android/settings/cloud/a;->aWo:Landroid/net/Uri;

    const-string/jumbo v0, "content://com.android.settings.cloud.CloudSettings/cloud_all_data/notify"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/android/settings/cloud/a;->aWn:Landroid/net/Uri;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/app/job/JobParameters;Lcom/android/settings/cloud/b;)V
    .locals 1

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/cloud/a;->mContext:Landroid/content/Context;

    iput-object p2, p0, Lcom/android/settings/cloud/a;->aWq:Landroid/app/job/JobParameters;

    iput-object p3, p0, Lcom/android/settings/cloud/a;->aWp:Lcom/android/settings/cloud/b;

    return-void
.end method

.method private aKO(Lcom/android/settings/cloud/e;Z)V
    .locals 4

    sget-boolean v0, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/cloud/a;->mContext:Landroid/content/Context;

    const-string/jumbo v1, "com.miui.securitycenter"

    invoke-static {v0, v1}, Landroid/provider/MiuiSettings$Privacy;->isEnabled(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    const-string/jumbo v0, "JobTask"

    const-string/jumbo v1, "intl return not allow"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :cond_0
    const-string/jumbo v0, "JobTask"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "ReturnArrivalRateToServer canRetry : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    if-eqz p1, :cond_1

    iget-object v0, p1, Lcom/android/settings/cloud/e;->aWt:Lorg/json/JSONArray;

    invoke-virtual {v0}, Lorg/json/JSONArray;->length()I

    move-result v0

    if-nez v0, :cond_2

    :cond_1
    return-void

    :cond_2
    iget v0, p1, Lcom/android/settings/cloud/e;->version:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_1

    :try_start_0
    new-instance v0, Lcom/android/settings/cloud/network/a;

    sget-object v1, Lcom/android/settings/cloud/h;->aWA:Ljava/lang/String;

    invoke-direct {v0, v1}, Lcom/android/settings/cloud/network/a;-><init>(Ljava/lang/String;)V

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/android/settings/cloud/network/a;->aKz(Z)V

    iget-object v1, p0, Lcom/android/settings/cloud/a;->mContext:Landroid/content/Context;

    invoke-static {v0, v1}, Lcom/android/settings/cloud/g;->aLb(Lcom/android/settings/cloud/network/a;Landroid/content/Context;)Lcom/android/settings/cloud/network/b;

    move-result-object v1

    iget-object v2, p1, Lcom/android/settings/cloud/e;->aWt:Lorg/json/JSONArray;

    invoke-static {v2}, Lcom/android/settings/cloud/g;->aLe(Lorg/json/JSONArray;)Ljava/util/Map;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/android/settings/cloud/g;->aLd(Lcom/android/settings/cloud/network/b;Ljava/util/Map;)V

    sget-boolean v1, Lcom/android/settings/cloud/a/f;->aVS:Z

    if-eqz v1, :cond_3

    const-string/jumbo v1, "JobTask"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "ReturnArrivalRateToServer params : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Lcom/android/settings/cloud/network/a;->aKA()Lcom/android/settings/cloud/network/b;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_3
    invoke-virtual {v0}, Lcom/android/settings/cloud/network/a;->aKB()Lcom/android/settings/cloud/network/Connection$NetworkError;

    move-result-object v1

    sget-object v2, Lcom/android/settings/cloud/network/Connection$NetworkError;->aWc:Lcom/android/settings/cloud/network/Connection$NetworkError;

    if-ne v1, v2, :cond_5

    invoke-virtual {v0}, Lcom/android/settings/cloud/network/a;->aKC()Lorg/json/JSONObject;

    move-result-object v0

    sget-boolean v1, Lcom/android/settings/cloud/a/f;->aVS:Z

    if-eqz v1, :cond_4

    const-string/jumbo v1, "JobTask"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "ReturnArrivalRateToServer response is : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_4
    :goto_0
    return-void

    :cond_5
    const-string/jumbo v0, "JobTask"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "ReturnArrivalRateToServer conn failed : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    if-eqz p2, :cond_4

    new-instance v0, Lcom/android/settings/cloud/i;

    invoke-direct {v0, p0, p1}, Lcom/android/settings/cloud/i;-><init>(Lcom/android/settings/cloud/a;Lcom/android/settings/cloud/e;)V

    invoke-virtual {v0}, Lcom/android/settings/cloud/i;->start()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string/jumbo v1, "JobTask"

    const-string/jumbo v2, "returnArrivalRateToServer error : "

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method private aKP()V
    .locals 6

    sget-boolean v0, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/cloud/a;->mContext:Landroid/content/Context;

    const-string/jumbo v1, "com.miui.securitycenter"

    invoke-static {v0, v1}, Landroid/provider/MiuiSettings$Privacy;->isEnabled(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    const-string/jumbo v0, "JobTask"

    const-string/jumbo v1, "intl update not allow"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :cond_0
    :try_start_0
    const-string/jumbo v0, "cloud_all_data_miui_version_type"

    iget-object v1, p0, Lcom/android/settings/cloud/a;->mContext:Landroid/content/Context;

    const-string/jumbo v2, ""

    invoke-static {v0, v1, v2}, Lcom/android/settings/cloud/a/f;->aKk(Ljava/lang/String;Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Lcom/android/settings/cloud/g;->aLa()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "JobTask"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "last miui version type : ["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, "] , current miui version type : ["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, "]"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_4

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_4

    const-string/jumbo v0, "JobTask"

    const-string/jumbo v2, "need to reset the data"

    invoke-static {v0, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-string/jumbo v0, "last_update_cloud_all_data_version"

    const-wide/16 v2, 0x0

    iget-object v4, p0, Lcom/android/settings/cloud/a;->mContext:Landroid/content/Context;

    invoke-static {v0, v2, v3, v4}, Lcom/android/settings/cloud/a/f;->aKl(Ljava/lang/String;JLandroid/content/Context;)V

    iget-object v0, p0, Lcom/android/settings/cloud/a;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/settings/cloud/d;->getInstance(Landroid/content/Context;)Lcom/android/settings/cloud/d;

    move-result-object v0

    const-string/jumbo v2, "cloud_all_data"

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual {v0, v2, v3, v4}, Lcom/android/settings/cloud/d;->aKT(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    :goto_0
    new-instance v0, Lcom/android/settings/cloud/network/a;

    sget-object v2, Lcom/android/settings/cloud/h;->aWz:Ljava/lang/String;

    invoke-direct {v0, v2}, Lcom/android/settings/cloud/network/a;-><init>(Ljava/lang/String;)V

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lcom/android/settings/cloud/network/a;->aKz(Z)V

    iget-object v2, p0, Lcom/android/settings/cloud/a;->mContext:Landroid/content/Context;

    invoke-static {v0, v2}, Lcom/android/settings/cloud/g;->aLb(Lcom/android/settings/cloud/network/a;Landroid/content/Context;)Lcom/android/settings/cloud/network/b;

    move-result-object v2

    iget-object v3, p0, Lcom/android/settings/cloud/a;->mContext:Landroid/content/Context;

    invoke-static {v3}, Lcom/android/settings/cloud/g;->aLc(Landroid/content/Context;)Ljava/util/Map;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/android/settings/cloud/g;->aLd(Lcom/android/settings/cloud/network/b;Ljava/util/Map;)V

    sget-boolean v2, Lcom/android/settings/cloud/a/f;->aVS:Z

    if-eqz v2, :cond_1

    const-string/jumbo v2, "JobTask"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "params : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Lcom/android/settings/cloud/network/a;->aKA()Lcom/android/settings/cloud/network/b;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    invoke-virtual {v0}, Lcom/android/settings/cloud/network/a;->aKB()Lcom/android/settings/cloud/network/Connection$NetworkError;

    move-result-object v2

    sget-object v3, Lcom/android/settings/cloud/network/Connection$NetworkError;->aWc:Lcom/android/settings/cloud/network/Connection$NetworkError;

    if-ne v2, v3, :cond_5

    invoke-virtual {v0}, Lcom/android/settings/cloud/network/a;->aKC()Lorg/json/JSONObject;

    move-result-object v0

    sget-boolean v2, Lcom/android/settings/cloud/a/f;->aVS:Z

    if-eqz v2, :cond_2

    const-string/jumbo v2, "JobTask"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "response is :"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    if-eqz v0, :cond_3

    invoke-virtual {v0}, Lorg/json/JSONObject;->length()I

    move-result v2

    if-lez v2, :cond_3

    const-string/jumbo v2, "cloud_all_data_miui_version_type"

    iget-object v3, p0, Lcom/android/settings/cloud/a;->mContext:Landroid/content/Context;

    invoke-static {v2, v1, v3}, Lcom/android/settings/cloud/a/f;->aKm(Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)V

    iget-object v1, p0, Lcom/android/settings/cloud/a;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/android/settings/cloud/d;->getInstance(Landroid/content/Context;)Lcom/android/settings/cloud/d;

    move-result-object v1

    iget-object v2, p0, Lcom/android/settings/cloud/a;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v2, v0}, Lcom/android/settings/cloud/d;->aKU(Landroid/content/Context;Lorg/json/JSONObject;)Lcom/android/settings/cloud/e;

    move-result-object v0

    if-eqz v0, :cond_3

    iget v1, v0, Lcom/android/settings/cloud/e;->version:I

    const/4 v2, -0x1

    if-eq v1, v2, :cond_3

    const-string/jumbo v1, "last_update_cloud_all_data_time"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iget-object v4, p0, Lcom/android/settings/cloud/a;->mContext:Landroid/content/Context;

    invoke-static {v1, v2, v3, v4}, Lcom/android/settings/cloud/a/f;->aKl(Ljava/lang/String;JLandroid/content/Context;)V

    const-string/jumbo v1, "last_update_cloud_all_data_version"

    iget v2, v0, Lcom/android/settings/cloud/e;->version:I

    int-to-long v2, v2

    iget-object v4, p0, Lcom/android/settings/cloud/a;->mContext:Landroid/content/Context;

    invoke-static {v1, v2, v3, v4}, Lcom/android/settings/cloud/a/f;->aKl(Ljava/lang/String;JLandroid/content/Context;)V

    iget-object v1, p0, Lcom/android/settings/cloud/a;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    sget-object v2, Lcom/android/settings/cloud/a;->aWo:Landroid/net/Uri;

    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual {v1, v2, v3, v4, v5}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    iget-object v1, p0, Lcom/android/settings/cloud/a;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    sget-object v2, Lcom/android/settings/cloud/a;->aWn:Landroid/net/Uri;

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lcom/android/settings/cloud/a;->aKO(Lcom/android/settings/cloud/e;Z)V

    :cond_3
    :goto_1
    return-void

    :cond_4
    const-string/jumbo v0, "JobTask"

    const-string/jumbo v2, "no need to reset the data "

    invoke-static {v0, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    :catch_0
    move-exception v0

    const-string/jumbo v1, "JobTask"

    const-string/jumbo v2, "updateCloudAllData error : "

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1

    :cond_5
    :try_start_1
    const-string/jumbo v0, "JobTask"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "conn failed : "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1
.end method

.method static synthetic aKQ(Lcom/android/settings/cloud/a;Lcom/android/settings/cloud/e;Z)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/android/settings/cloud/a;->aKO(Lcom/android/settings/cloud/e;Z)V

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/android/settings/cloud/a;->doInBackground([Ljava/lang/Void;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Void;
    .locals 1

    invoke-direct {p0}, Lcom/android/settings/cloud/a;->aKP()V

    const/4 v0, 0x0

    return-object v0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    check-cast p1, Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/android/settings/cloud/a;->onPostExecute(Ljava/lang/Void;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/Void;)V
    .locals 2

    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/android/settings/cloud/a;->aWp:Lcom/android/settings/cloud/b;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/cloud/a;->aWp:Lcom/android/settings/cloud/b;

    iget-object v1, p0, Lcom/android/settings/cloud/a;->aWq:Landroid/app/job/JobParameters;

    invoke-interface {v0, v1}, Lcom/android/settings/cloud/b;->aKR(Landroid/app/job/JobParameters;)V

    :cond_0
    return-void
.end method
