.class public Lcom/android/settings/cloud/push/CloudService;
.super Landroid/app/IntentService;
.source "CloudService.java"


# static fields
.field static final TAG:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/android/settings/cloud/push/CloudService;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/settings/cloud/push/CloudService;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    sget-object v0, Lcom/android/settings/cloud/push/CloudService;->TAG:Ljava/lang/String;

    invoke-direct {p0, v0}, Landroid/app/IntentService;-><init>(Ljava/lang/String;)V

    return-void
.end method

.method private aJf(Landroid/content/Context;Ljava/util/List;)V
    .locals 1

    invoke-static {p1}, Lcom/android/settings/cloud/push/d;->getInstance(Landroid/content/Context;)Lcom/android/settings/cloud/push/d;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/android/settings/cloud/push/d;->aJl(Ljava/util/List;)V

    return-void
.end method

.method private aJg(Landroid/content/Context;Ljava/util/List;)V
    .locals 1

    invoke-static {p1}, Lcom/android/settings/cloud/push/d;->getInstance(Landroid/content/Context;)Lcom/android/settings/cloud/push/d;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/android/settings/cloud/push/d;->aJm(Ljava/util/List;)V

    return-void
.end method

.method private aJh(Landroid/content/Context;Lorg/json/JSONArray;)V
    .locals 11

    const/4 v1, 0x0

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {p2}, Lorg/json/JSONArray;->length()I

    move-result v4

    move v2, v1

    :goto_0
    if-ge v2, v4, :cond_2

    invoke-virtual {p2, v2}, Lorg/json/JSONArray;->optJSONObject(I)Lorg/json/JSONObject;

    move-result-object v0

    new-instance v5, Lcom/android/settings/cloud/push/g;

    invoke-direct {v5}, Lcom/android/settings/cloud/push/g;-><init>()V

    const-string/jumbo v6, "pkg_name"

    invoke-virtual {v0, v6}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    const-string/jumbo v7, "message"

    invoke-virtual {v0, v7}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    const-string/jumbo v8, "title"

    invoke-virtual {v0, v8}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    const-string/jumbo v9, "ticker"

    invoke-virtual {v0, v9}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    const-string/jumbo v10, "flag_precise"

    invoke-virtual {v0, v10, v1}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;Z)Z

    move-result v10

    invoke-virtual {v5, v6}, Lcom/android/settings/cloud/push/g;->aJv(Ljava/lang/String;)V

    invoke-virtual {v5, v7}, Lcom/android/settings/cloud/push/g;->aJw(Ljava/lang/String;)V

    invoke-virtual {v5, v8}, Lcom/android/settings/cloud/push/g;->aJx(Ljava/lang/String;)V

    invoke-virtual {v5, v9}, Lcom/android/settings/cloud/push/g;->aJy(Ljava/lang/String;)V

    invoke-virtual {v5, v10}, Lcom/android/settings/cloud/push/g;->aJz(Z)V

    const-string/jumbo v6, "versions"

    invoke-virtual {v0, v6}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v6

    if-eqz v6, :cond_1

    invoke-virtual {v6}, Lorg/json/JSONArray;->length()I

    move-result v0

    if-lez v0, :cond_1

    new-instance v7, Ljava/util/HashSet;

    invoke-direct {v7}, Ljava/util/HashSet;-><init>()V

    move v0, v1

    :goto_1
    invoke-virtual {v6}, Lorg/json/JSONArray;->length()I

    move-result v8

    if-ge v0, v8, :cond_0

    invoke-virtual {v6, v0}, Lorg/json/JSONArray;->optInt(I)I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-interface {v7, v8}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_0
    invoke-virtual {v5, v7}, Lcom/android/settings/cloud/push/g;->aJA(Ljava/util/Set;)V

    :cond_1
    invoke-interface {v3, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_2
    invoke-direct {p0, p1, v3}, Lcom/android/settings/cloud/push/CloudService;->aJf(Landroid/content/Context;Ljava/util/List;)V

    return-void
.end method

.method private aJi(Landroid/content/Context;Lorg/json/JSONArray;)V
    .locals 9

    const/4 v1, 0x0

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {p2}, Lorg/json/JSONArray;->length()I

    move-result v4

    move v2, v1

    :goto_0
    if-ge v2, v4, :cond_2

    invoke-virtual {p2, v2}, Lorg/json/JSONArray;->optJSONObject(I)Lorg/json/JSONObject;

    move-result-object v0

    new-instance v5, Lcom/android/settings/cloud/push/j;

    invoke-direct {v5}, Lcom/android/settings/cloud/push/j;-><init>()V

    const-string/jumbo v6, "pkg_name"

    invoke-virtual {v0, v6}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    const-string/jumbo v7, "message"

    invoke-virtual {v0, v7}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    const-string/jumbo v8, "flag_precise"

    invoke-virtual {v0, v8, v1}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;Z)Z

    move-result v8

    invoke-virtual {v5, v6}, Lcom/android/settings/cloud/push/j;->aJQ(Ljava/lang/String;)V

    invoke-virtual {v5, v7}, Lcom/android/settings/cloud/push/j;->aJR(Ljava/lang/String;)V

    invoke-virtual {v5, v8}, Lcom/android/settings/cloud/push/j;->aJS(Z)V

    const-string/jumbo v6, "versions"

    invoke-virtual {v0, v6}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v6

    if-eqz v6, :cond_1

    invoke-virtual {v6}, Lorg/json/JSONArray;->length()I

    move-result v0

    if-lez v0, :cond_1

    new-instance v7, Ljava/util/HashSet;

    invoke-direct {v7}, Ljava/util/HashSet;-><init>()V

    move v0, v1

    :goto_1
    invoke-virtual {v6}, Lorg/json/JSONArray;->length()I

    move-result v8

    if-ge v0, v8, :cond_0

    invoke-virtual {v6, v0}, Lorg/json/JSONArray;->optInt(I)I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-interface {v7, v8}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_0
    invoke-virtual {v5, v7}, Lcom/android/settings/cloud/push/j;->aJT(Ljava/util/Set;)V

    :cond_1
    invoke-interface {v3, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_2
    invoke-static {p1}, Lcom/android/settings/cloud/push/h;->aJF(Landroid/content/Context;)I

    invoke-static {p1, v3}, Lcom/android/settings/cloud/push/h;->aJG(Landroid/content/Context;Ljava/util/List;)V

    return-void
.end method

.method private aJj(Landroid/content/Context;Lorg/json/JSONArray;)V
    .locals 9

    const/4 v1, 0x0

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {p2}, Lorg/json/JSONArray;->length()I

    move-result v4

    move v2, v1

    :goto_0
    if-ge v2, v4, :cond_2

    invoke-virtual {p2, v2}, Lorg/json/JSONArray;->optJSONObject(I)Lorg/json/JSONObject;

    move-result-object v0

    new-instance v5, Lcom/android/settings/cloud/push/i;

    invoke-direct {v5}, Lcom/android/settings/cloud/push/i;-><init>()V

    const-string/jumbo v6, "pkg_name"

    invoke-virtual {v0, v6}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    const-string/jumbo v7, "message"

    invoke-virtual {v0, v7}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    const-string/jumbo v8, "flag_precise"

    invoke-virtual {v0, v8, v1}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;Z)Z

    move-result v8

    invoke-virtual {v5, v6}, Lcom/android/settings/cloud/push/i;->aJJ(Ljava/lang/String;)V

    invoke-virtual {v5, v7}, Lcom/android/settings/cloud/push/i;->aJK(Ljava/lang/String;)V

    invoke-virtual {v5, v8}, Lcom/android/settings/cloud/push/i;->aJL(Z)V

    const-string/jumbo v6, "versions"

    invoke-virtual {v0, v6}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v6

    if-eqz v6, :cond_1

    invoke-virtual {v6}, Lorg/json/JSONArray;->length()I

    move-result v0

    if-lez v0, :cond_1

    new-instance v7, Ljava/util/HashSet;

    invoke-direct {v7}, Ljava/util/HashSet;-><init>()V

    move v0, v1

    :goto_1
    invoke-virtual {v6}, Lorg/json/JSONArray;->length()I

    move-result v8

    if-ge v0, v8, :cond_0

    invoke-virtual {v6, v0}, Lorg/json/JSONArray;->optInt(I)I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-interface {v7, v8}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_0
    invoke-virtual {v5, v7}, Lcom/android/settings/cloud/push/i;->aJM(Ljava/util/Set;)V

    :cond_1
    invoke-interface {v3, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_2
    invoke-static {p1}, Lcom/android/settings/cloud/push/h;->aJH(Landroid/content/Context;)I

    invoke-static {p1, v3}, Lcom/android/settings/cloud/push/h;->aJI(Landroid/content/Context;Ljava/util/List;)V

    invoke-direct {p0, p1, v3}, Lcom/android/settings/cloud/push/CloudService;->aJg(Landroid/content/Context;Ljava/util/List;)V

    return-void
.end method

.method private aJk(Landroid/content/Context;Ljava/lang/String;)V
    .locals 2

    :try_start_0
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0, p2}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    const-string/jumbo v1, "exist_compatibility"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-direct {p0, p1, v1}, Lcom/android/settings/cloud/push/CloudService;->aJh(Landroid/content/Context;Lorg/json/JSONArray;)V

    :cond_0
    const-string/jumbo v1, "install_compatibility"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-direct {p0, p1, v1}, Lcom/android/settings/cloud/push/CloudService;->aJi(Landroid/content/Context;Lorg/json/JSONArray;)V

    :cond_1
    const-string/jumbo v1, "running_compatibility"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-direct {p0, p1, v0}, Lcom/android/settings/cloud/push/CloudService;->aJj(Landroid/content/Context;Lorg/json/JSONArray;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :cond_2
    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method


# virtual methods
.method protected onHandleIntent(Landroid/content/Intent;)V
    .locals 2

    const-string/jumbo v0, "push_content"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-direct {p0, p0, v0}, Lcom/android/settings/cloud/push/CloudService;->aJk(Landroid/content/Context;Ljava/lang/String;)V

    :cond_0
    return-void
.end method
