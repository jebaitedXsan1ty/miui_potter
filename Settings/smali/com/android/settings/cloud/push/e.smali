.class Lcom/android/settings/cloud/push/e;
.super Landroid/content/pm/IPackageDeleteObserver$Stub;
.source "CompatChecker.java"


# instance fields
.field aVp:Z

.field final synthetic aVq:Lcom/android/settings/cloud/push/d;


# direct methods
.method private constructor <init>(Lcom/android/settings/cloud/push/d;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/cloud/push/e;->aVq:Lcom/android/settings/cloud/push/d;

    invoke-direct {p0}, Landroid/content/pm/IPackageDeleteObserver$Stub;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/settings/cloud/push/d;Lcom/android/settings/cloud/push/e;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/settings/cloud/push/e;-><init>(Lcom/android/settings/cloud/push/d;)V

    return-void
.end method


# virtual methods
.method public packageDeleted(Ljava/lang/String;I)V
    .locals 9

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/android/settings/cloud/push/e;->aVq:Lcom/android/settings/cloud/push/d;

    invoke-static {v0}, Lcom/android/settings/cloud/push/d;->aJr(Lcom/android/settings/cloud/push/d;)Lcom/android/settings/cloud/push/g;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/cloud/push/e;->aVq:Lcom/android/settings/cloud/push/d;

    invoke-static {v0}, Lcom/android/settings/cloud/push/d;->aJr(Lcom/android/settings/cloud/push/d;)Lcom/android/settings/cloud/push/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/settings/cloud/push/g;->getTitle()Ljava/lang/String;

    move-result-object v1

    iget-object v0, p0, Lcom/android/settings/cloud/push/e;->aVq:Lcom/android/settings/cloud/push/d;

    invoke-static {v0}, Lcom/android/settings/cloud/push/d;->aJr(Lcom/android/settings/cloud/push/d;)Lcom/android/settings/cloud/push/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/settings/cloud/push/g;->aJB()Ljava/lang/String;

    move-result-object v2

    iget-object v0, p0, Lcom/android/settings/cloud/push/e;->aVq:Lcom/android/settings/cloud/push/d;

    invoke-static {v0}, Lcom/android/settings/cloud/push/d;->aJr(Lcom/android/settings/cloud/push/d;)Lcom/android/settings/cloud/push/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/settings/cloud/push/g;->getMessage()Ljava/lang/String;

    move-result-object v3

    if-eqz v1, :cond_0

    const-string/jumbo v0, ""

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    if-eqz v2, :cond_0

    const-string/jumbo v0, ""

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    if-eqz v3, :cond_0

    const-string/jumbo v0, ""

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/cloud/push/e;->aVq:Lcom/android/settings/cloud/push/d;

    invoke-static {v0}, Lcom/android/settings/cloud/push/d;->aJq(Lcom/android/settings/cloud/push/d;)Landroid/content/Context;

    move-result-object v0

    const-string/jumbo v4, "notification"

    invoke-virtual {v0, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    new-instance v4, Landroid/app/Notification;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    const v5, 0x7f0801f6

    invoke-direct {v4, v5, v2, v6, v7}, Landroid/app/Notification;-><init>(ILjava/lang/CharSequence;J)V

    iget v2, v4, Landroid/app/Notification;->defaults:I

    or-int/lit8 v2, v2, 0x1

    iput v2, v4, Landroid/app/Notification;->defaults:I

    iget v2, v4, Landroid/app/Notification;->defaults:I

    or-int/lit8 v2, v2, 0x2

    iput v2, v4, Landroid/app/Notification;->defaults:I

    iget v2, v4, Landroid/app/Notification;->flags:I

    or-int/lit8 v2, v2, 0x10

    iput v2, v4, Landroid/app/Notification;->flags:I

    iget-object v2, p0, Lcom/android/settings/cloud/push/e;->aVq:Lcom/android/settings/cloud/push/d;

    invoke-static {v2}, Lcom/android/settings/cloud/push/d;->aJq(Lcom/android/settings/cloud/push/d;)Landroid/content/Context;

    move-result-object v2

    iget-object v5, p0, Lcom/android/settings/cloud/push/e;->aVq:Lcom/android/settings/cloud/push/d;

    invoke-static {v5}, Lcom/android/settings/cloud/push/d;->aJq(Lcom/android/settings/cloud/push/d;)Landroid/content/Context;

    move-result-object v5

    new-instance v6, Landroid/content/Intent;

    invoke-direct {v6}, Landroid/content/Intent;-><init>()V

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-static {v5, v7, v6, v8}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v5

    invoke-virtual {v4, v2, v1, v3, v5}, Landroid/app/Notification;->setLatestEventInfo(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/app/PendingIntent;)V

    const/4 v1, 0x0

    invoke-virtual {v0, v1, v4}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/settings/cloud/push/e;->aVp:Z

    invoke-virtual {p0}, Lcom/android/settings/cloud/push/e;->notifyAll()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
