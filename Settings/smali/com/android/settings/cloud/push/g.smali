.class public Lcom/android/settings/cloud/push/g;
.super Ljava/lang/Object;
.source "ExistCompatibility.java"


# instance fields
.field private aVr:Ljava/lang/String;

.field private aVs:Z

.field private aVt:Ljava/lang/String;

.field private aVu:Ljava/util/Set;

.field private mPackageName:Ljava/lang/String;

.field private mTitle:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public aJA(Ljava/util/Set;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/cloud/push/g;->aVu:Ljava/util/Set;

    return-void
.end method

.method public aJB()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/cloud/push/g;->aVt:Ljava/lang/String;

    return-object v0
.end method

.method public aJC()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/cloud/push/g;->mPackageName:Ljava/lang/String;

    return-object v0
.end method

.method public aJD()Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/settings/cloud/push/g;->aVs:Z

    return v0
.end method

.method public aJE()Ljava/util/Set;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/cloud/push/g;->aVu:Ljava/util/Set;

    return-object v0
.end method

.method public aJv(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/cloud/push/g;->mPackageName:Ljava/lang/String;

    return-void
.end method

.method public aJw(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/cloud/push/g;->aVr:Ljava/lang/String;

    return-void
.end method

.method public aJx(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/cloud/push/g;->mTitle:Ljava/lang/String;

    return-void
.end method

.method public aJy(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/cloud/push/g;->aVt:Ljava/lang/String;

    return-void
.end method

.method public aJz(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/android/settings/cloud/push/g;->aVs:Z

    return-void
.end method

.method public getMessage()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/cloud/push/g;->aVr:Ljava/lang/String;

    return-object v0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/cloud/push/g;->mTitle:Ljava/lang/String;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "ExistCompatibility : PackageName = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/cloud/push/g;->mPackageName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, " Message = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/cloud/push/g;->aVr:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, " Title = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/cloud/push/g;->mTitle:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, " Tricker = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/cloud/push/g;->aVt:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, " Precise = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/android/settings/cloud/push/g;->aVs:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, " Versions = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/cloud/push/g;->aVu:Ljava/util/Set;

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
