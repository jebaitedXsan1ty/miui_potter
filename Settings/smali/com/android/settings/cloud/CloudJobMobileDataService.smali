.class public Lcom/android/settings/cloud/CloudJobMobileDataService;
.super Landroid/app/job/JobService;
.source "CloudJobMobileDataService.java"


# static fields
.field public static final TAG:Ljava/lang/String;

.field private static final aWD:Ljava/util/concurrent/Executor;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/android/settings/cloud/CloudJobMobileDataService;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/settings/cloud/CloudJobMobileDataService;->TAG:Ljava/lang/String;

    invoke-static {}, Ljava/util/concurrent/Executors;->newSingleThreadExecutor()Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    sput-object v0, Lcom/android/settings/cloud/CloudJobMobileDataService;->aWD:Ljava/util/concurrent/Executor;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/app/job/JobService;-><init>()V

    return-void
.end method


# virtual methods
.method public onStartJob(Landroid/app/job/JobParameters;)Z
    .locals 6

    const/4 v5, 0x1

    const/4 v4, 0x0

    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v1, "com.miui.securitycenter.action.TRACK_CLOUD_COUNT"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string/jumbo v1, "com.miui.securitycenter"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/settings/cloud/CloudJobMobileDataService;->sendBroadcast(Landroid/content/Intent;)V

    invoke-static {p0}, Lcom/android/settings/cloud/a/f;->aKn(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/android/settings/cloud/CloudJobMobileDataService;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "update device release setting"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lcom/android/settings/cloud/CloudJobMobileDataService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/android/settings/cloud/b/a;->aKs(Landroid/content/Context;)V

    :cond_0
    sget-object v0, Lcom/android/settings/cloud/CloudJobMobileDataService;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "OnStartJob id = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Landroid/app/job/JobParameters;->getJobId()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string/jumbo v0, "last_update_cloud_all_data_time"

    invoke-static {v0, p0}, Lcom/android/settings/cloud/a/f;->aKq(Ljava/lang/String;Landroid/content/Context;)J

    move-result-wide v0

    invoke-static {p0}, Lcom/android/settings/cloud/a/f;->aKn(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_1

    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-eqz v2, :cond_1

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    sub-long v0, v2, v0

    const-wide/32 v2, 0xf731400

    cmp-long v0, v0, v2

    if-lez v0, :cond_1

    sget-object v0, Lcom/android/settings/cloud/CloudJobMobileDataService;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "Need to update"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    sget-boolean v0, Lcom/android/settings/cloud/CloudJobService2;->aWv:Z

    if-nez v0, :cond_1

    sput-boolean v5, Lcom/android/settings/cloud/CloudJobService2;->aWv:Z

    new-instance v0, Lcom/android/settings/cloud/a;

    new-instance v1, Lcom/android/settings/cloud/m;

    invoke-direct {v1, p0}, Lcom/android/settings/cloud/m;-><init>(Lcom/android/settings/cloud/CloudJobMobileDataService;)V

    invoke-direct {v0, p0, p1, v1}, Lcom/android/settings/cloud/a;-><init>(Landroid/content/Context;Landroid/app/job/JobParameters;Lcom/android/settings/cloud/b;)V

    sget-object v1, Lcom/android/settings/cloud/CloudJobMobileDataService;->aWD:Ljava/util/concurrent/Executor;

    new-array v2, v4, [Ljava/lang/Void;

    invoke-virtual {v0, v1, v2}, Lcom/android/settings/cloud/a;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    return v5

    :cond_1
    return v4
.end method

.method public onStopJob(Landroid/app/job/JobParameters;)Z
    .locals 4

    const/4 v3, 0x0

    sget-object v0, Lcom/android/settings/cloud/CloudJobMobileDataService;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "OnStopJob id = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Landroid/app/job/JobParameters;->getJobId()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    sput-boolean v3, Lcom/android/settings/cloud/CloudJobService2;->aWv:Z

    return v3
.end method
