.class final Lcom/android/settings/dF;
.super Ljava/lang/Object;
.source "MiuiUserCredentialsSettings.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field final synthetic chA:I

.field final synthetic chB:Lcom/android/settings/MiuiUserCredentialsSettings$Credential;

.field final synthetic chz:Lcom/android/settings/MiuiUserCredentialsSettings$CredentialDialogFragment;


# direct methods
.method constructor <init>(Lcom/android/settings/MiuiUserCredentialsSettings$CredentialDialogFragment;ILcom/android/settings/MiuiUserCredentialsSettings$Credential;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/dF;->chz:Lcom/android/settings/MiuiUserCredentialsSettings$CredentialDialogFragment;

    iput p2, p0, Lcom/android/settings/dF;->chA:I

    iput-object p3, p0, Lcom/android/settings/dF;->chB:Lcom/android/settings/MiuiUserCredentialsSettings$Credential;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 4

    iget-object v0, p0, Lcom/android/settings/dF;->chz:Lcom/android/settings/MiuiUserCredentialsSettings$CredentialDialogFragment;

    invoke-virtual {v0}, Lcom/android/settings/MiuiUserCredentialsSettings$CredentialDialogFragment;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string/jumbo v1, "no_config_credentials"

    iget v2, p0, Lcom/android/settings/dF;->chA:I

    invoke-static {v0, v1, v2}, Lcom/android/settingslib/w;->crb(Landroid/content/Context;Ljava/lang/String;I)Lcom/android/settingslib/n;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/android/settings/dF;->chz:Lcom/android/settings/MiuiUserCredentialsSettings$CredentialDialogFragment;

    invoke-virtual {v1}, Lcom/android/settings/MiuiUserCredentialsSettings$CredentialDialogFragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/android/settingslib/w;->cqW(Landroid/content/Context;Lcom/android/settingslib/n;)V

    :goto_0
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    return-void

    :cond_0
    new-instance v0, Lcom/android/settings/t;

    iget-object v1, p0, Lcom/android/settings/dF;->chz:Lcom/android/settings/MiuiUserCredentialsSettings$CredentialDialogFragment;

    iget-object v2, p0, Lcom/android/settings/dF;->chz:Lcom/android/settings/MiuiUserCredentialsSettings$CredentialDialogFragment;

    invoke-virtual {v2}, Lcom/android/settings/MiuiUserCredentialsSettings$CredentialDialogFragment;->getContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lcom/android/settings/dF;->chz:Lcom/android/settings/MiuiUserCredentialsSettings$CredentialDialogFragment;

    invoke-virtual {v3}, Lcom/android/settings/MiuiUserCredentialsSettings$CredentialDialogFragment;->getTargetFragment()Landroid/app/Fragment;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/android/settings/t;-><init>(Lcom/android/settings/MiuiUserCredentialsSettings$CredentialDialogFragment;Landroid/content/Context;Landroid/app/Fragment;)V

    const/4 v1, 0x1

    new-array v1, v1, [Lcom/android/settings/MiuiUserCredentialsSettings$Credential;

    iget-object v2, p0, Lcom/android/settings/dF;->chB:Lcom/android/settings/MiuiUserCredentialsSettings$Credential;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-virtual {v0, v1}, Lcom/android/settings/t;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0
.end method
