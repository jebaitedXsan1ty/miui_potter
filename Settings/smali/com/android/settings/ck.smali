.class Lcom/android/settings/ck;
.super Ljava/lang/Object;
.source "DeviceAdminSettings.java"

# interfaces
.implements Ljava/lang/Comparable;


# instance fields
.field public bXd:Z

.field public bXe:Landroid/app/admin/DeviceAdminInfo;

.field public name:Ljava/lang/String;


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/settings/ck;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/ck;-><init>()V

    return-void
.end method


# virtual methods
.method public bRc(Lcom/android/settings/ck;)I
    .locals 2

    iget-boolean v0, p0, Lcom/android/settings/ck;->bXd:Z

    iget-boolean v1, p1, Lcom/android/settings/ck;->bXd:Z

    if-eq v0, v1, :cond_1

    iget-boolean v0, p0, Lcom/android/settings/ck;->bXd:Z

    if-eqz v0, :cond_0

    const/4 v0, -0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/android/settings/ck;->name:Ljava/lang/String;

    iget-object v1, p1, Lcom/android/settings/ck;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 1

    check-cast p1, Lcom/android/settings/ck;

    invoke-virtual {p0, p1}, Lcom/android/settings/ck;->bRc(Lcom/android/settings/ck;)I

    move-result v0

    return v0
.end method
