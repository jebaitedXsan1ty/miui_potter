.class Lcom/android/settings/t;
.super Landroid/os/AsyncTask;
.source "MiuiUserCredentialsSettings.java"


# instance fields
.field private bvv:Landroid/content/Context;

.field private bvw:Landroid/app/Fragment;

.field final synthetic bvx:Lcom/android/settings/MiuiUserCredentialsSettings$CredentialDialogFragment;


# direct methods
.method public constructor <init>(Lcom/android/settings/MiuiUserCredentialsSettings$CredentialDialogFragment;Landroid/content/Context;Landroid/app/Fragment;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/t;->bvx:Lcom/android/settings/MiuiUserCredentialsSettings$CredentialDialogFragment;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    iput-object p2, p0, Lcom/android/settings/t;->bvv:Landroid/content/Context;

    iput-object p3, p0, Lcom/android/settings/t;->bvw:Landroid/app/Fragment;

    return-void
.end method

.method private biz(Lcom/android/settings/MiuiUserCredentialsSettings$Credential;)V
    .locals 4

    :try_start_0
    iget-object v0, p0, Lcom/android/settings/t;->bvx:Lcom/android/settings/MiuiUserCredentialsSettings$CredentialDialogFragment;

    invoke-virtual {v0}, Lcom/android/settings/MiuiUserCredentialsSettings$CredentialDialogFragment;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/security/KeyChain;->bind(Landroid/content/Context;)Landroid/security/KeyChain$KeyChainConnection;
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    :try_start_1
    invoke-virtual {v1}, Landroid/security/KeyChain$KeyChainConnection;->getService()Landroid/security/IKeyChainService;

    move-result-object v0

    iget-object v2, p1, Lcom/android/settings/MiuiUserCredentialsSettings$Credential;->alias:Ljava/lang/String;

    invoke-interface {v0, v2}, Landroid/security/IKeyChainService;->removeKeyPair(Ljava/lang/String;)Z
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    invoke-virtual {v1}, Landroid/security/KeyChain$KeyChainConnection;->close()V

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string/jumbo v1, "CredentialDialogFragment"

    const-string/jumbo v2, "Connecting to KeyChain"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    return-void

    :catch_1
    move-exception v0

    :try_start_2
    const-string/jumbo v2, "CredentialDialogFragment"

    const-string/jumbo v3, "Removing credentials"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    invoke-virtual {v1}, Landroid/security/KeyChain$KeyChainConnection;->close()V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Landroid/security/KeyChain$KeyChainConnection;->close()V

    throw v0
.end method


# virtual methods
.method protected varargs bix([Lcom/android/settings/MiuiUserCredentialsSettings$Credential;)[Lcom/android/settings/MiuiUserCredentialsSettings$Credential;
    .locals 4

    const/4 v0, 0x0

    array-length v1, p1

    :goto_0
    if-ge v0, v1, :cond_1

    aget-object v2, p1, v0

    invoke-virtual {v2}, Lcom/android/settings/MiuiUserCredentialsSettings$Credential;->biC()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-direct {p0, v2}, Lcom/android/settings/t;->biz(Lcom/android/settings/MiuiUserCredentialsSettings$Credential;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string/jumbo v1, "Not implemented for wifi certificates. This should not be reachable."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    return-object p1
.end method

.method protected varargs biy([Lcom/android/settings/MiuiUserCredentialsSettings$Credential;)V
    .locals 4

    iget-object v0, p0, Lcom/android/settings/t;->bvw:Landroid/app/Fragment;

    instance-of v0, v0, Lcom/android/settings/UserCredentialsSettings;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/settings/t;->bvw:Landroid/app/Fragment;

    invoke-virtual {v0}, Landroid/app/Fragment;->isAdded()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/settings/t;->bvw:Landroid/app/Fragment;

    check-cast v0, Lcom/android/settings/UserCredentialsSettings;

    const/4 v1, 0x0

    array-length v2, p1

    :goto_0
    if-ge v1, v2, :cond_0

    aget-object v3, p1, v1

    iget-object v3, v3, Lcom/android/settings/MiuiUserCredentialsSettings$Credential;->alias:Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/android/settings/UserCredentialsSettings;->bXP(Ljava/lang/String;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    invoke-virtual {v0}, Lcom/android/settings/UserCredentialsSettings;->bXQ()V

    :cond_1
    return-void
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    check-cast p1, [Lcom/android/settings/MiuiUserCredentialsSettings$Credential;

    invoke-virtual {p0, p1}, Lcom/android/settings/t;->bix([Lcom/android/settings/MiuiUserCredentialsSettings$Credential;)[Lcom/android/settings/MiuiUserCredentialsSettings$Credential;

    move-result-object v0

    return-object v0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    check-cast p1, [Lcom/android/settings/MiuiUserCredentialsSettings$Credential;

    invoke-virtual {p0, p1}, Lcom/android/settings/t;->biy([Lcom/android/settings/MiuiUserCredentialsSettings$Credential;)V

    return-void
.end method
