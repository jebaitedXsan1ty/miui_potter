.class public Lcom/android/settings/bv;
.super Ljava/lang/Object;
.source "FramesSequenceAnimation.java"


# instance fields
.field private bRC:Z

.field private bRD:Lcom/android/settings/bw;

.field private bRE:Z

.field private bRF:Z

.field private bRG:Z

.field private bRH:Landroid/graphics/Bitmap;

.field private bRI:I

.field private bRJ:I

.field private bRK:[I

.field private bRL:Landroid/os/Handler;

.field private bRM:Ljava/lang/ref/SoftReference;

.field private bRN:Landroid/graphics/BitmapFactory$Options;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/widget/ImageView;II)V
    .locals 5

    const/4 v4, 0x1

    const/4 v3, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    iput-object v1, p0, Lcom/android/settings/bv;->bRL:Landroid/os/Handler;

    const/4 v1, -0x1

    iput v1, p0, Lcom/android/settings/bv;->bRI:I

    const/16 v1, 0x3e8

    div-int/2addr v1, p4

    iput v1, p0, Lcom/android/settings/bv;->bRJ:I

    invoke-direct {p0, v0, p3}, Lcom/android/settings/bv;->bKb(Landroid/content/res/Resources;I)[I

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/bv;->bRK:[I

    iget-object v0, p0, Lcom/android/settings/bv;->bRK:[I

    array-length v0, v0

    if-gtz v0, :cond_0

    const-string/jumbo v0, "FramesSequenceAnimation"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "can\'t get frames from resource, framesResId is "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    new-instance v0, Ljava/lang/ref/SoftReference;

    invoke-direct {v0, p2}, Ljava/lang/ref/SoftReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/android/settings/bv;->bRM:Ljava/lang/ref/SoftReference;

    iget-object v0, p0, Lcom/android/settings/bv;->bRM:Ljava/lang/ref/SoftReference;

    invoke-virtual {v0}, Ljava/lang/ref/SoftReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/android/settings/bv;->bRK:[I

    aget v1, v1, v3

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    iput-boolean v3, p0, Lcom/android/settings/bv;->bRE:Z

    iput-boolean v3, p0, Lcom/android/settings/bv;->bRF:Z

    iput-boolean v3, p0, Lcom/android/settings/bv;->bRG:Z

    iget-object v0, p0, Lcom/android/settings/bv;->bRM:Ljava/lang/ref/SoftReference;

    invoke-virtual {v0}, Ljava/lang/ref/SoftReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/BitmapDrawable;

    if-nez v0, :cond_1

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0802db

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/BitmapDrawable;

    :cond_1
    invoke-virtual {v0}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getConfig()Landroid/graphics/Bitmap$Config;

    move-result-object v0

    invoke-static {v1, v2, v0}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/bv;->bRH:Landroid/graphics/Bitmap;

    new-instance v0, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v0}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    iput-object v0, p0, Lcom/android/settings/bv;->bRN:Landroid/graphics/BitmapFactory$Options;

    iget-object v0, p0, Lcom/android/settings/bv;->bRN:Landroid/graphics/BitmapFactory$Options;

    iget-object v1, p0, Lcom/android/settings/bv;->bRH:Landroid/graphics/Bitmap;

    iput-object v1, v0, Landroid/graphics/BitmapFactory$Options;->inBitmap:Landroid/graphics/Bitmap;

    iget-object v0, p0, Lcom/android/settings/bv;->bRN:Landroid/graphics/BitmapFactory$Options;

    iput-boolean v4, v0, Landroid/graphics/BitmapFactory$Options;->inMutable:Z

    iget-object v0, p0, Lcom/android/settings/bv;->bRN:Landroid/graphics/BitmapFactory$Options;

    iput v4, v0, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    return-void
.end method

.method private bKb(Landroid/content/res/Resources;I)[I
    .locals 6

    const/4 v1, 0x0

    invoke-virtual {p1, p2}, Landroid/content/res/Resources;->obtainTypedArray(I)Landroid/content/res/TypedArray;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/TypedArray;->length()I

    move-result v3

    new-array v4, v3, [I

    move v0, v1

    :goto_0
    if-ge v0, v3, :cond_0

    invoke-virtual {v2, v0, v1}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v5

    aput v5, v4, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    invoke-virtual {v2}, Landroid/content/res/TypedArray;->recycle()V

    return-object v4
.end method

.method private bKc()I
    .locals 7

    const/4 v6, 0x1

    iget v0, p0, Lcom/android/settings/bv;->bRI:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/android/settings/bv;->bRI:I

    iget v0, p0, Lcom/android/settings/bv;->bRI:I

    iget-object v1, p0, Lcom/android/settings/bv;->bRK:[I

    array-length v1, v1

    if-lt v0, v1, :cond_0

    const/4 v0, 0x0

    iput v0, p0, Lcom/android/settings/bv;->bRI:I

    iput-boolean v6, p0, Lcom/android/settings/bv;->bRE:Z

    :cond_0
    iget v0, p0, Lcom/android/settings/bv;->bRI:I

    int-to-double v0, v0

    iget-object v2, p0, Lcom/android/settings/bv;->bRK:[I

    array-length v2, v2

    int-to-double v2, v2

    const-wide v4, 0x3fe4cccccccccccdL    # 0.65

    mul-double/2addr v2, v4

    cmpl-double v0, v0, v2

    if-ltz v0, :cond_1

    iput-boolean v6, p0, Lcom/android/settings/bv;->bRC:Z

    :cond_1
    iget-object v0, p0, Lcom/android/settings/bv;->bRK:[I

    iget v1, p0, Lcom/android/settings/bv;->bRI:I

    aget v0, v0, v1

    return v0
.end method

.method static synthetic bKd(Lcom/android/settings/bv;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/settings/bv;->bRC:Z

    return v0
.end method

.method static synthetic bKe(Lcom/android/settings/bv;)Lcom/android/settings/bw;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/bv;->bRD:Lcom/android/settings/bw;

    return-object v0
.end method

.method static synthetic bKf(Lcom/android/settings/bv;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/settings/bv;->bRE:Z

    return v0
.end method

.method static synthetic bKg(Lcom/android/settings/bv;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/settings/bv;->bRG:Z

    return v0
.end method

.method static synthetic bKh(Lcom/android/settings/bv;)Landroid/graphics/Bitmap;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/bv;->bRH:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method static synthetic bKi(Lcom/android/settings/bv;)I
    .locals 1

    iget v0, p0, Lcom/android/settings/bv;->bRJ:I

    return v0
.end method

.method static synthetic bKj(Lcom/android/settings/bv;)Landroid/os/Handler;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/bv;->bRL:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic bKk(Lcom/android/settings/bv;)Ljava/lang/ref/SoftReference;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/bv;->bRM:Ljava/lang/ref/SoftReference;

    return-object v0
.end method

.method static synthetic bKl(Lcom/android/settings/bv;)Landroid/graphics/BitmapFactory$Options;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/bv;->bRN:Landroid/graphics/BitmapFactory$Options;

    return-object v0
.end method

.method static synthetic bKm(Lcom/android/settings/bv;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/android/settings/bv;->bRC:Z

    return p1
.end method

.method static synthetic bKn(Lcom/android/settings/bv;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/android/settings/bv;->bRE:Z

    return p1
.end method

.method static synthetic bKo(Lcom/android/settings/bv;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/android/settings/bv;->bRF:Z

    return p1
.end method

.method static synthetic bKp(Lcom/android/settings/bv;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    .locals 0

    iput-object p1, p0, Lcom/android/settings/bv;->bRH:Landroid/graphics/Bitmap;

    return-object p1
.end method

.method static synthetic bKq(Lcom/android/settings/bv;)I
    .locals 1

    invoke-direct {p0}, Lcom/android/settings/bv;->bKc()I

    move-result v0

    return v0
.end method


# virtual methods
.method public bKa(Lcom/android/settings/bw;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/bv;->bRD:Lcom/android/settings/bw;

    return-void
.end method

.method public declared-synchronized start()V
    .locals 2

    monitor-enter p0

    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lcom/android/settings/bv;->bRG:Z

    iget-boolean v0, p0, Lcom/android/settings/bv;->bRF:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    monitor-exit p0

    return-void

    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/android/settings/bv;->bRL:Landroid/os/Handler;

    new-instance v1, Lcom/android/settings/iX;

    invoke-direct {v1, p0}, Lcom/android/settings/iX;-><init>(Lcom/android/settings/bv;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    iget-object v0, p0, Lcom/android/settings/bv;->bRD:Lcom/android/settings/bw;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/settings/bv;->bRD:Lcom/android/settings/bw;

    invoke-interface {v0}, Lcom/android/settings/bw;->bKt()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_1
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized stop()V
    .locals 1

    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    iput-boolean v0, p0, Lcom/android/settings/bv;->bRF:Z

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/settings/bv;->bRG:Z

    iget-object v0, p0, Lcom/android/settings/bv;->bRD:Lcom/android/settings/bw;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/bv;->bRD:Lcom/android/settings/bw;

    invoke-interface {v0}, Lcom/android/settings/bw;->bKu()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
