.class Lcom/android/settings/bX;
.super Ljava/lang/Object;
.source "MiuiSecurityTrustedCredentials.java"

# interfaces
.implements Ljava/lang/Comparable;


# instance fields
.field private final bVA:Lcom/android/org/conscrypt/TrustedCertificateStore;

.field private final bVB:Ljava/lang/String;

.field private final bVC:Ljava/lang/String;

.field private final bVD:Lcom/android/settings/MiuiSecurityTrustedCredentials$Tab;

.field private final bVE:Ljava/security/cert/X509Certificate;

.field private final bVw:Lcom/android/settings/bV;

.field private final bVx:Ljava/lang/String;

.field private bVy:Z

.field private final bVz:Landroid/net/http/SslCertificate;


# direct methods
.method private constructor <init>(Lcom/android/org/conscrypt/TrustedCertificateStore;Lcom/android/settings/bV;Lcom/android/settings/MiuiSecurityTrustedCredentials$Tab;Ljava/lang/String;Ljava/security/cert/X509Certificate;)V
    .locals 4

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/android/settings/bX;->bVA:Lcom/android/org/conscrypt/TrustedCertificateStore;

    iput-object p2, p0, Lcom/android/settings/bX;->bVw:Lcom/android/settings/bV;

    iput-object p3, p0, Lcom/android/settings/bX;->bVD:Lcom/android/settings/MiuiSecurityTrustedCredentials$Tab;

    iput-object p4, p0, Lcom/android/settings/bX;->bVx:Ljava/lang/String;

    iput-object p5, p0, Lcom/android/settings/bX;->bVE:Ljava/security/cert/X509Certificate;

    new-instance v0, Landroid/net/http/SslCertificate;

    invoke-direct {v0, p5}, Landroid/net/http/SslCertificate;-><init>(Ljava/security/cert/X509Certificate;)V

    iput-object v0, p0, Lcom/android/settings/bX;->bVz:Landroid/net/http/SslCertificate;

    iget-object v0, p0, Lcom/android/settings/bX;->bVz:Landroid/net/http/SslCertificate;

    invoke-virtual {v0}, Landroid/net/http/SslCertificate;->getIssuedTo()Landroid/net/http/SslCertificate$DName;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/http/SslCertificate$DName;->getCName()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/bX;->bVz:Landroid/net/http/SslCertificate;

    invoke-virtual {v1}, Landroid/net/http/SslCertificate;->getIssuedTo()Landroid/net/http/SslCertificate$DName;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/http/SslCertificate$DName;->getOName()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/android/settings/bX;->bVz:Landroid/net/http/SslCertificate;

    invoke-virtual {v2}, Landroid/net/http/SslCertificate;->getIssuedTo()Landroid/net/http/SslCertificate$DName;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/http/SslCertificate$DName;->getUName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_1

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_0

    iput-object v1, p0, Lcom/android/settings/bX;->bVB:Ljava/lang/String;

    iput-object v0, p0, Lcom/android/settings/bX;->bVC:Ljava/lang/String;

    :goto_0
    iget-object v0, p0, Lcom/android/settings/bX;->bVD:Lcom/android/settings/MiuiSecurityTrustedCredentials$Tab;

    iget-object v1, p0, Lcom/android/settings/bX;->bVA:Lcom/android/org/conscrypt/TrustedCertificateStore;

    iget-object v2, p0, Lcom/android/settings/bX;->bVx:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/android/settings/MiuiSecurityTrustedCredentials$Tab;->bPh(Lcom/android/settings/MiuiSecurityTrustedCredentials$Tab;Lcom/android/org/conscrypt/TrustedCertificateStore;Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/settings/bX;->bVy:Z

    return-void

    :cond_0
    iput-object v1, p0, Lcom/android/settings/bX;->bVB:Ljava/lang/String;

    iput-object v2, p0, Lcom/android/settings/bX;->bVC:Ljava/lang/String;

    goto :goto_0

    :cond_1
    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_2

    iput-object v0, p0, Lcom/android/settings/bX;->bVB:Ljava/lang/String;

    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/android/settings/bX;->bVC:Ljava/lang/String;

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/android/settings/bX;->bVz:Landroid/net/http/SslCertificate;

    invoke-virtual {v0}, Landroid/net/http/SslCertificate;->getIssuedTo()Landroid/net/http/SslCertificate$DName;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/http/SslCertificate$DName;->getDName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/bX;->bVB:Ljava/lang/String;

    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/android/settings/bX;->bVC:Ljava/lang/String;

    goto :goto_0
.end method

.method synthetic constructor <init>(Lcom/android/org/conscrypt/TrustedCertificateStore;Lcom/android/settings/bV;Lcom/android/settings/MiuiSecurityTrustedCredentials$Tab;Ljava/lang/String;Ljava/security/cert/X509Certificate;Lcom/android/settings/bX;)V
    .locals 0

    invoke-direct/range {p0 .. p5}, Lcom/android/settings/bX;-><init>(Lcom/android/org/conscrypt/TrustedCertificateStore;Lcom/android/settings/bV;Lcom/android/settings/MiuiSecurityTrustedCredentials$Tab;Ljava/lang/String;Ljava/security/cert/X509Certificate;)V

    return-void
.end method

.method static synthetic bPA(Lcom/android/settings/bX;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/bX;->bVC:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic bPB(Lcom/android/settings/bX;)Lcom/android/settings/MiuiSecurityTrustedCredentials$Tab;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/bX;->bVD:Lcom/android/settings/MiuiSecurityTrustedCredentials$Tab;

    return-object v0
.end method

.method static synthetic bPC(Lcom/android/settings/bX;)Ljava/security/cert/X509Certificate;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/bX;->bVE:Ljava/security/cert/X509Certificate;

    return-object v0
.end method

.method static synthetic bPD(Lcom/android/settings/bX;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/android/settings/bX;->bVy:Z

    return p1
.end method

.method static synthetic bPv(Lcom/android/settings/bX;)Lcom/android/settings/bV;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/bX;->bVw:Lcom/android/settings/bV;

    return-object v0
.end method

.method static synthetic bPw(Lcom/android/settings/bX;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/bX;->bVx:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic bPx(Lcom/android/settings/bX;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/settings/bX;->bVy:Z

    return v0
.end method

.method static synthetic bPy(Lcom/android/settings/bX;)Landroid/net/http/SslCertificate;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/bX;->bVz:Landroid/net/http/SslCertificate;

    return-object v0
.end method

.method static synthetic bPz(Lcom/android/settings/bX;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/bX;->bVB:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public bPu(Lcom/android/settings/bX;)I
    .locals 2

    iget-object v0, p0, Lcom/android/settings/bX;->bVB:Ljava/lang/String;

    iget-object v1, p1, Lcom/android/settings/bX;->bVB:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v0

    if-eqz v0, :cond_0

    return v0

    :cond_0
    iget-object v0, p0, Lcom/android/settings/bX;->bVC:Ljava/lang/String;

    iget-object v1, p1, Lcom/android/settings/bX;->bVC:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 1

    check-cast p1, Lcom/android/settings/bX;

    invoke-virtual {p0, p1}, Lcom/android/settings/bX;->bPu(Lcom/android/settings/bX;)I

    move-result v0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    instance-of v0, p1, Lcom/android/settings/bX;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    return v0

    :cond_0
    check-cast p1, Lcom/android/settings/bX;

    iget-object v0, p0, Lcom/android/settings/bX;->bVx:Ljava/lang/String;

    iget-object v1, p1, Lcom/android/settings/bX;->bVx:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public hashCode()I
    .locals 1

    iget-object v0, p0, Lcom/android/settings/bX;->bVx:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    return v0
.end method
