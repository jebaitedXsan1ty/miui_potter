.class public Lcom/android/settings/cX;
.super Ljava/lang/Object;
.source "MusicRegionUtils.java"


# static fields
.field private static final cfF:Ljava/util/Set;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    new-instance v0, Ljava/util/HashSet;

    const/16 v1, 0x1c

    new-array v1, v1, [Ljava/lang/String;

    const-string/jumbo v2, "AT"

    const/4 v3, 0x0

    aput-object v2, v1, v3

    const-string/jumbo v2, "BE"

    const/4 v3, 0x1

    aput-object v2, v1, v3

    const-string/jumbo v2, "BG"

    const/4 v3, 0x2

    aput-object v2, v1, v3

    const-string/jumbo v2, "CY"

    const/4 v3, 0x3

    aput-object v2, v1, v3

    const-string/jumbo v2, "CZ"

    const/4 v3, 0x4

    aput-object v2, v1, v3

    const-string/jumbo v2, "DE"

    const/4 v3, 0x5

    aput-object v2, v1, v3

    const-string/jumbo v2, "DK"

    const/4 v3, 0x6

    aput-object v2, v1, v3

    const-string/jumbo v2, "EE"

    const/4 v3, 0x7

    aput-object v2, v1, v3

    const-string/jumbo v2, "ES"

    const/16 v3, 0x8

    aput-object v2, v1, v3

    const-string/jumbo v2, "FI"

    const/16 v3, 0x9

    aput-object v2, v1, v3

    const-string/jumbo v2, "FR"

    const/16 v3, 0xa

    aput-object v2, v1, v3

    const-string/jumbo v2, "GB"

    const/16 v3, 0xb

    aput-object v2, v1, v3

    const-string/jumbo v2, "GR"

    const/16 v3, 0xc

    aput-object v2, v1, v3

    const-string/jumbo v2, "HR"

    const/16 v3, 0xd

    aput-object v2, v1, v3

    const-string/jumbo v2, "HU"

    const/16 v3, 0xe

    aput-object v2, v1, v3

    const-string/jumbo v2, "IE"

    const/16 v3, 0xf

    aput-object v2, v1, v3

    const-string/jumbo v2, "IT"

    const/16 v3, 0x10

    aput-object v2, v1, v3

    const-string/jumbo v2, "LT"

    const/16 v3, 0x11

    aput-object v2, v1, v3

    const-string/jumbo v2, "LU"

    const/16 v3, 0x12

    aput-object v2, v1, v3

    const-string/jumbo v2, "LV"

    const/16 v3, 0x13

    aput-object v2, v1, v3

    const-string/jumbo v2, "MT"

    const/16 v3, 0x14

    aput-object v2, v1, v3

    const-string/jumbo v2, "NL"

    const/16 v3, 0x15

    aput-object v2, v1, v3

    const-string/jumbo v2, "PL"

    const/16 v3, 0x16

    aput-object v2, v1, v3

    const-string/jumbo v2, "PT"

    const/16 v3, 0x17

    aput-object v2, v1, v3

    const-string/jumbo v2, "RO"

    const/16 v3, 0x18

    aput-object v2, v1, v3

    const-string/jumbo v2, "SE"

    const/16 v3, 0x19

    aput-object v2, v1, v3

    const-string/jumbo v2, "SI"

    const/16 v3, 0x1a

    aput-object v2, v1, v3

    const-string/jumbo v2, "SK"

    const/16 v3, 0x1b

    aput-object v2, v1, v3

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    sput-object v0, Lcom/android/settings/cX;->cfF:Ljava/util/Set;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static bYe()Z
    .locals 2

    const-string/jumbo v0, "ro.miui.region"

    const-string/jumbo v1, "unknown"

    invoke-static {v0, v1}, Lcom/android/settings/cX;->bYf(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string/jumbo v1, "unknown"

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    return v0

    :cond_1
    sget-object v1, Lcom/android/settings/cX;->cfF:Ljava/util/Set;

    invoke-interface {v1, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method private static bYf(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 5

    :try_start_0
    const-string/jumbo v0, "miui.os.SystemProperties"

    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    const-string/jumbo v1, "get"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Class;

    const-class v3, Ljava/lang/String;

    const/4 v4, 0x0

    aput-object v3, v2, v4

    invoke-virtual {v0, v1, v2}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p0, v1, v2

    const/4 v2, 0x0

    invoke-virtual {v0, v2, v1}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    return-object p1
.end method
