.class public Lcom/android/settings/NavigationBarGuideView;
.super Landroid/widget/LinearLayout;
.source "NavigationBarGuideView.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private bxQ:Lmiui/app/AlertDialog;

.field private bxR:Z

.field private bxS:Landroid/view/View;

.field private bxT:Landroid/widget/RadioButton;

.field private bxU:Landroid/widget/VideoView;

.field private bxV:Z

.field private bxW:Landroid/view/View;

.field private bxX:Landroid/widget/RadioButton;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/android/settings/NavigationBarGuideView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/android/settings/NavigationBarGuideView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method static synthetic blA(Lcom/android/settings/NavigationBarGuideView;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/android/settings/NavigationBarGuideView;->bxV:Z

    return p1
.end method

.method private bln()V
    .locals 4

    const/4 v3, 0x0

    iput-boolean v3, p0, Lcom/android/settings/NavigationBarGuideView;->bxR:Z

    new-instance v0, Lmiui/app/AlertDialog$Builder;

    iget-object v1, p0, Lcom/android/settings/NavigationBarGuideView;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lmiui/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v1, 0x7f120b2b

    invoke-virtual {v0, v1}, Lmiui/app/AlertDialog$Builder;->setTitle(I)Lmiui/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f120b2a

    invoke-virtual {v0, v1}, Lmiui/app/AlertDialog$Builder;->setMessage(I)Lmiui/app/AlertDialog$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/NavigationBarGuideView;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f120b27

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v3, v1}, Lmiui/app/AlertDialog$Builder;->setCheckBox(ZLjava/lang/CharSequence;)Lmiui/app/AlertDialog$Builder;

    move-result-object v0

    new-instance v1, Lcom/android/settings/en;

    invoke-direct {v1, p0}, Lcom/android/settings/en;-><init>(Lcom/android/settings/NavigationBarGuideView;)V

    const v2, 0x7f120b28

    invoke-virtual {v0, v2, v1}, Lmiui/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Lmiui/app/AlertDialog$Builder;

    move-result-object v0

    new-instance v1, Lcom/android/settings/eo;

    invoke-direct {v1, p0}, Lcom/android/settings/eo;-><init>(Lcom/android/settings/NavigationBarGuideView;)V

    const v2, 0x7f120b29

    invoke-virtual {v0, v2, v1}, Lmiui/app/AlertDialog$Builder;->setNeutralButton(ILandroid/content/DialogInterface$OnClickListener;)Lmiui/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lmiui/app/AlertDialog$Builder;->create()Lmiui/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/NavigationBarGuideView;->bxQ:Lmiui/app/AlertDialog;

    iget-object v0, p0, Lcom/android/settings/NavigationBarGuideView;->bxQ:Lmiui/app/AlertDialog;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lmiui/app/AlertDialog;->setCanceledOnTouchOutside(Z)V

    iget-object v0, p0, Lcom/android/settings/NavigationBarGuideView;->bxQ:Lmiui/app/AlertDialog;

    new-instance v1, Lcom/android/settings/ep;

    invoke-direct {v1, p0}, Lcom/android/settings/ep;-><init>(Lcom/android/settings/NavigationBarGuideView;)V

    invoke-virtual {v0, v1}, Lmiui/app/AlertDialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    return-void
.end method

.method private blo()Z
    .locals 2

    iget-object v0, p0, Lcom/android/settings/NavigationBarGuideView;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v1, "force_fsg_nav_bar"

    invoke-static {v0, v1}, Landroid/provider/MiuiSettings$Global;->getBoolean(Landroid/content/ContentResolver;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method private blp(Landroid/widget/VideoView;I)V
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "android.resource://"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/android/settings/NavigationBarGuideView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/VideoView;->setVideoURI(Landroid/net/Uri;)V

    new-instance v0, Lcom/android/settings/el;

    invoke-direct {v0, p0, p1}, Lcom/android/settings/el;-><init>(Lcom/android/settings/NavigationBarGuideView;Landroid/widget/VideoView;)V

    invoke-virtual {p1, v0}, Landroid/widget/VideoView;->setOnPreparedListener(Landroid/media/MediaPlayer$OnPreparedListener;)V

    return-void
.end method

.method private blq()V
    .locals 3

    const/4 v2, 0x1

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/android/settings/NavigationBarGuideView;->blo()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/NavigationBarGuideView;->bxT:Landroid/widget/RadioButton;

    invoke-virtual {v0, v2}, Landroid/widget/RadioButton;->setChecked(Z)V

    iget-object v0, p0, Lcom/android/settings/NavigationBarGuideView;->bxX:Landroid/widget/RadioButton;

    invoke-virtual {v0, v1}, Landroid/widget/RadioButton;->setChecked(Z)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/settings/NavigationBarGuideView;->bxT:Landroid/widget/RadioButton;

    invoke-virtual {v0, v1}, Landroid/widget/RadioButton;->setChecked(Z)V

    iget-object v0, p0, Lcom/android/settings/NavigationBarGuideView;->bxX:Landroid/widget/RadioButton;

    invoke-virtual {v0, v2}, Landroid/widget/RadioButton;->setChecked(Z)V

    goto :goto_0
.end method

.method private blr()V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/NavigationBarGuideView;->bxU:Landroid/widget/VideoView;

    const v1, 0x7f080351

    invoke-virtual {v0, v1}, Landroid/widget/VideoView;->setBackgroundResource(I)V

    return-void
.end method

.method static synthetic bls(Lcom/android/settings/NavigationBarGuideView;)Lmiui/app/AlertDialog;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/NavigationBarGuideView;->bxQ:Lmiui/app/AlertDialog;

    return-object v0
.end method

.method static synthetic blt(Lcom/android/settings/NavigationBarGuideView;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/settings/NavigationBarGuideView;->bxR:Z

    return v0
.end method

.method static synthetic blu(Lcom/android/settings/NavigationBarGuideView;)Landroid/content/Context;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/NavigationBarGuideView;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic blv(Lcom/android/settings/NavigationBarGuideView;)Landroid/widget/RadioButton;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/NavigationBarGuideView;->bxT:Landroid/widget/RadioButton;

    return-object v0
.end method

.method static synthetic blw(Lcom/android/settings/NavigationBarGuideView;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/settings/NavigationBarGuideView;->bxV:Z

    return v0
.end method

.method static synthetic blx(Lcom/android/settings/NavigationBarGuideView;)Landroid/widget/RadioButton;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/NavigationBarGuideView;->bxX:Landroid/widget/RadioButton;

    return-object v0
.end method

.method static synthetic bly(Lcom/android/settings/NavigationBarGuideView;Lmiui/app/AlertDialog;)Lmiui/app/AlertDialog;
    .locals 0

    iput-object p1, p0, Lcom/android/settings/NavigationBarGuideView;->bxQ:Lmiui/app/AlertDialog;

    return-object p1
.end method

.method static synthetic blz(Lcom/android/settings/NavigationBarGuideView;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/android/settings/NavigationBarGuideView;->bxR:Z

    return p1
.end method


# virtual methods
.method public onAttachedToWindow()V
    .locals 0

    invoke-super {p0}, Landroid/widget/LinearLayout;->onAttachedToWindow()V

    invoke-direct {p0}, Lcom/android/settings/NavigationBarGuideView;->blr()V

    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 4

    const/4 v3, 0x1

    const/4 v2, 0x0

    invoke-direct {p0}, Lcom/android/settings/NavigationBarGuideView;->blo()Z

    move-result v0

    iget-object v1, p0, Lcom/android/settings/NavigationBarGuideView;->bxS:Landroid/view/View;

    if-ne p1, v1, :cond_2

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/android/settings/NavigationBarGuideView;->bxV:Z

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lcom/android/settings/NavigationBarGuideView;->bln()V

    iget-object v0, p0, Lcom/android/settings/NavigationBarGuideView;->bxQ:Lmiui/app/AlertDialog;

    invoke-virtual {v0}, Lmiui/app/AlertDialog;->show()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0, v3}, Lcom/android/settings/NavigationBarGuideView;->setScreenButtonHidden(Z)V

    iget-object v0, p0, Lcom/android/settings/NavigationBarGuideView;->bxT:Landroid/widget/RadioButton;

    invoke-virtual {v0, v3}, Landroid/widget/RadioButton;->setChecked(Z)V

    iget-object v0, p0, Lcom/android/settings/NavigationBarGuideView;->bxX:Landroid/widget/RadioButton;

    invoke-virtual {v0, v2}, Landroid/widget/RadioButton;->setChecked(Z)V

    goto :goto_0

    :cond_2
    if-eqz v0, :cond_3

    invoke-virtual {p0, v2}, Lcom/android/settings/NavigationBarGuideView;->setScreenButtonHidden(Z)V

    :cond_3
    iget-object v0, p0, Lcom/android/settings/NavigationBarGuideView;->bxT:Landroid/widget/RadioButton;

    invoke-virtual {v0, v2}, Landroid/widget/RadioButton;->setChecked(Z)V

    iget-object v0, p0, Lcom/android/settings/NavigationBarGuideView;->bxX:Landroid/widget/RadioButton;

    invoke-virtual {v0, v3}, Landroid/widget/RadioButton;->setChecked(Z)V

    goto :goto_0
.end method

.method protected onFinishInflate()V
    .locals 3

    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    const v0, 0x7f0a04f4

    invoke-virtual {p0, v0}, Lcom/android/settings/NavigationBarGuideView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/VideoView;

    iput-object v0, p0, Lcom/android/settings/NavigationBarGuideView;->bxU:Landroid/widget/VideoView;

    iget-object v0, p0, Lcom/android/settings/NavigationBarGuideView;->bxU:Landroid/widget/VideoView;

    const v1, 0x7f11005d

    invoke-direct {p0, v0, v1}, Lcom/android/settings/NavigationBarGuideView;->blp(Landroid/widget/VideoView;I)V

    const v0, 0x7f0a01bf

    invoke-virtual {p0, v0}, Lcom/android/settings/NavigationBarGuideView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/NavigationBarGuideView;->bxS:Landroid/view/View;

    const v0, 0x7f0a04f6

    invoke-virtual {p0, v0}, Lcom/android/settings/NavigationBarGuideView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/NavigationBarGuideView;->bxW:Landroid/view/View;

    iget-object v0, p0, Lcom/android/settings/NavigationBarGuideView;->bxS:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/android/settings/NavigationBarGuideView;->bxW:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f0a0379

    invoke-virtual {p0, v0}, Lcom/android/settings/NavigationBarGuideView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioButton;

    iput-object v0, p0, Lcom/android/settings/NavigationBarGuideView;->bxT:Landroid/widget/RadioButton;

    const v0, 0x7f0a037b

    invoke-virtual {p0, v0}, Lcom/android/settings/NavigationBarGuideView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioButton;

    iput-object v0, p0, Lcom/android/settings/NavigationBarGuideView;->bxX:Landroid/widget/RadioButton;

    invoke-direct {p0}, Lcom/android/settings/NavigationBarGuideView;->blq()V

    iget-object v0, p0, Lcom/android/settings/NavigationBarGuideView;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string/jumbo v1, "need_show_navigation_guide"

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/settings/NavigationBarGuideView;->bxV:Z

    return-void
.end method

.method public onPause()V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/NavigationBarGuideView;->blr()V

    return-void
.end method

.method public onResume()V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/NavigationBarGuideView;->blq()V

    return-void
.end method

.method setScreenButtonHidden(Z)V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/NavigationBarGuideView;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v1, "force_fsg_nav_bar"

    invoke-static {v0, v1, p1}, Landroid/provider/MiuiSettings$Global;->putBoolean(Landroid/content/ContentResolver;Ljava/lang/String;Z)Z

    return-void
.end method
