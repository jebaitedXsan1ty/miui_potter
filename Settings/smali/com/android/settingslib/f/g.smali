.class public Lcom/android/settingslib/f/g;
.super Ljava/lang/Object;
.source "StorageManagerVolumeProvider.java"

# interfaces
.implements Lcom/android/settingslib/f/b;


# instance fields
.field private cIW:Landroid/os/storage/StorageManager;


# direct methods
.method public constructor <init>(Landroid/os/storage/StorageManager;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/android/settingslib/f/g;->cIW:Landroid/os/storage/StorageManager;

    return-void
.end method


# virtual methods
.method public coi(Landroid/os/storage/VolumeInfo;)Landroid/os/storage/VolumeInfo;
    .locals 1

    iget-object v0, p0, Lcom/android/settingslib/f/g;->cIW:Landroid/os/storage/StorageManager;

    invoke-virtual {v0, p1}, Landroid/os/storage/StorageManager;->findEmulatedForPrivate(Landroid/os/storage/VolumeInfo;)Landroid/os/storage/VolumeInfo;

    move-result-object v0

    return-object v0
.end method

.method public coj(Landroid/app/usage/StorageStatsManager;Landroid/os/storage/VolumeInfo;)J
    .locals 2

    invoke-virtual {p2}, Landroid/os/storage/VolumeInfo;->getFsUuid()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/app/usage/StorageStatsManager;->getFreeBytes(Ljava/lang/String;)J

    move-result-wide v0

    return-wide v0
.end method

.method public cok(Landroid/app/usage/StorageStatsManager;Landroid/os/storage/VolumeInfo;)J
    .locals 2

    invoke-virtual {p2}, Landroid/os/storage/VolumeInfo;->getFsUuid()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/app/usage/StorageStatsManager;->getTotalBytes(Ljava/lang/String;)J

    move-result-wide v0

    return-wide v0
.end method

.method public col()Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/android/settingslib/f/g;->cIW:Landroid/os/storage/StorageManager;

    invoke-virtual {v0}, Landroid/os/storage/StorageManager;->getVolumes()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method
