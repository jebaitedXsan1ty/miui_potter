.class public Lcom/android/settingslib/f/c;
.super Ljava/lang/Object;
.source "StorageMeasurement.java"


# instance fields
.field private final cIJ:Landroid/os/storage/VolumeInfo;

.field private final cIK:Landroid/os/storage/VolumeInfo;

.field private final cIL:Landroid/os/UserManager;

.field private cIM:Ljava/lang/ref/WeakReference;

.field private final cIN:Landroid/app/usage/StorageStatsManager;

.field private final mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/os/storage/VolumeInfo;Landroid/os/storage/VolumeInfo;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settingslib/f/c;->mContext:Landroid/content/Context;

    iget-object v0, p0, Lcom/android/settingslib/f/c;->mContext:Landroid/content/Context;

    const-class v1, Landroid/os/UserManager;

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/UserManager;

    iput-object v0, p0, Lcom/android/settingslib/f/c;->cIL:Landroid/os/UserManager;

    iget-object v0, p0, Lcom/android/settingslib/f/c;->mContext:Landroid/content/Context;

    const-class v1, Landroid/app/usage/StorageStatsManager;

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/usage/StorageStatsManager;

    iput-object v0, p0, Lcom/android/settingslib/f/c;->cIN:Landroid/app/usage/StorageStatsManager;

    iput-object p2, p0, Lcom/android/settingslib/f/c;->cIK:Landroid/os/storage/VolumeInfo;

    iput-object p3, p0, Lcom/android/settingslib/f/c;->cIJ:Landroid/os/storage/VolumeInfo;

    return-void
.end method

.method static synthetic com(Lcom/android/settingslib/f/c;)Ljava/lang/ref/WeakReference;
    .locals 1

    iget-object v0, p0, Lcom/android/settingslib/f/c;->cIM:Ljava/lang/ref/WeakReference;

    return-object v0
.end method

.method private static con(Landroid/util/SparseArray;)V
    .locals 3

    const/4 v1, 0x0

    const/16 v0, 0x3e7

    invoke-virtual {p0, v0}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    invoke-virtual {p0, v1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map;

    invoke-static {}, Landroid/app/ActivityManager;->getCurrentUser()I

    move-result v2

    if-nez v2, :cond_0

    if-nez v1, :cond_1

    :cond_0
    return-void

    :cond_1
    if-eqz v0, :cond_0

    return-void
.end method

.method private coo()Lcom/android/settingslib/f/d;
    .locals 12

    iget-object v0, p0, Lcom/android/settingslib/f/c;->cIL:Landroid/os/UserManager;

    invoke-virtual {v0}, Landroid/os/UserManager;->getUsers()Ljava/util/List;

    move-result-object v1

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    new-instance v4, Lcom/android/settingslib/f/d;

    invoke-direct {v4}, Lcom/android/settingslib/f/d;-><init>()V

    iget-object v0, p0, Lcom/android/settingslib/f/c;->cIK:Landroid/os/storage/VolumeInfo;

    if-nez v0, :cond_0

    return-object v4

    :cond_0
    iget-object v0, p0, Lcom/android/settingslib/f/c;->cIK:Landroid/os/storage/VolumeInfo;

    invoke-virtual {v0}, Landroid/os/storage/VolumeInfo;->getType()I

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/android/settingslib/f/c;->cIK:Landroid/os/storage/VolumeInfo;

    invoke-virtual {v0}, Landroid/os/storage/VolumeInfo;->getPath()Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->getTotalSpace()J

    move-result-wide v0

    iput-wide v0, v4, Lcom/android/settingslib/f/d;->cIQ:J

    iget-object v0, p0, Lcom/android/settingslib/f/c;->cIK:Landroid/os/storage/VolumeInfo;

    invoke-virtual {v0}, Landroid/os/storage/VolumeInfo;->getPath()Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->getUsableSpace()J

    move-result-wide v0

    iput-wide v0, v4, Lcom/android/settingslib/f/d;->cIO:J

    return-object v4

    :cond_1
    iget-object v0, p0, Lcom/android/settingslib/f/c;->cIK:Landroid/os/storage/VolumeInfo;

    invoke-virtual {v0}, Landroid/os/storage/VolumeInfo;->getPath()Ljava/io/File;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Ljava/io/File;->getTotalSpace()J

    move-result-wide v6

    iput-wide v6, v4, Lcom/android/settingslib/f/d;->cIQ:J

    invoke-virtual {v0}, Ljava/io/File;->getUsableSpace()J

    move-result-wide v6

    iput-wide v6, v4, Lcom/android/settingslib/f/d;->cIO:J

    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v5, "action_top_ui"

    invoke-direct {v0, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string/jumbo v5, "details.availSize"

    iget-wide v6, v4, Lcom/android/settingslib/f/d;->cIO:J

    invoke-virtual {v0, v5, v6, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    iget-object v5, p0, Lcom/android/settingslib/f/c;->mContext:Landroid/content/Context;

    invoke-static {v5}, Landroid/support/v4/content/b;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/b;

    move-result-object v5

    invoke-virtual {v5, v0}, Landroid/support/v4/content/b;->eah(Landroid/content/Intent;)Z

    :cond_2
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v6

    const-string/jumbo v0, "StorageMeasurement"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v8, "Measured total storage in "

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    sub-long v2, v6, v2

    invoke-virtual {v5, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "ms"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/settingslib/f/c;->cIJ:Landroid/os/storage/VolumeInfo;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/android/settingslib/f/c;->cIJ:Landroid/os/storage/VolumeInfo;

    invoke-virtual {v0}, Landroid/os/storage/VolumeInfo;->isMountedReadable()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/UserInfo;

    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    iget-object v5, v4, Lcom/android/settingslib/f/d;->cIT:Landroid/util/SparseArray;

    iget v8, v0, Landroid/content/pm/UserInfo;->id:I

    invoke-virtual {v5, v8, v3}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    :try_start_0
    iget-object v5, p0, Lcom/android/settingslib/f/c;->cIN:Landroid/app/usage/StorageStatsManager;

    iget-object v8, p0, Lcom/android/settingslib/f/c;->cIJ:Landroid/os/storage/VolumeInfo;

    iget-object v8, v8, Landroid/os/storage/VolumeInfo;->fsUuid:Ljava/lang/String;

    iget v9, v0, Landroid/content/pm/UserInfo;->id:I

    invoke-static {v9}, Landroid/os/UserHandle;->of(I)Landroid/os/UserHandle;

    move-result-object v9

    invoke-virtual {v5, v8, v9}, Landroid/app/usage/StorageStatsManager;->queryExternalStatsForUser(Ljava/lang/String;Landroid/os/UserHandle;)Landroid/app/usage/ExternalStorageStats;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v5

    iget-object v8, v4, Lcom/android/settingslib/f/d;->cIP:Landroid/util/SparseLongArray;

    iget v9, v0, Landroid/content/pm/UserInfo;->id:I

    invoke-virtual {v5}, Landroid/app/usage/ExternalStorageStats;->getTotalBytes()J

    move-result-wide v10

    invoke-static {v8, v9, v10, v11}, Lcom/android/settingslib/f/c;->cow(Landroid/util/SparseLongArray;IJ)V

    sget-object v8, Landroid/os/Environment;->DIRECTORY_MUSIC:Ljava/lang/String;

    invoke-virtual {v5}, Landroid/app/usage/ExternalStorageStats;->getAudioBytes()J

    move-result-wide v10

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    invoke-virtual {v3, v8, v9}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v8, Landroid/os/Environment;->DIRECTORY_MOVIES:Ljava/lang/String;

    invoke-virtual {v5}, Landroid/app/usage/ExternalStorageStats;->getVideoBytes()J

    move-result-wide v10

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    invoke-virtual {v3, v8, v9}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v8, Landroid/os/Environment;->DIRECTORY_PICTURES:Ljava/lang/String;

    invoke-virtual {v5}, Landroid/app/usage/ExternalStorageStats;->getImageBytes()J

    move-result-wide v10

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    invoke-virtual {v3, v8, v9}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {v5}, Landroid/app/usage/ExternalStorageStats;->getTotalBytes()J

    move-result-wide v8

    invoke-virtual {v5}, Landroid/app/usage/ExternalStorageStats;->getAudioBytes()J

    move-result-wide v10

    sub-long/2addr v8, v10

    invoke-virtual {v5}, Landroid/app/usage/ExternalStorageStats;->getVideoBytes()J

    move-result-wide v10

    sub-long/2addr v8, v10

    invoke-virtual {v5}, Landroid/app/usage/ExternalStorageStats;->getImageBytes()J

    move-result-wide v10

    sub-long/2addr v8, v10

    iget-object v3, v4, Lcom/android/settingslib/f/d;->cIS:Landroid/util/SparseLongArray;

    iget v0, v0, Landroid/content/pm/UserInfo;->id:I

    invoke-static {v3, v0, v8, v9}, Lcom/android/settingslib/f/c;->cow(Landroid/util/SparseLongArray;IJ)V

    goto :goto_0

    :catch_0
    move-exception v0

    const-string/jumbo v3, "StorageMeasurement"

    invoke-static {v3, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    :cond_3
    iget-object v0, v4, Lcom/android/settingslib/f/d;->cIT:Landroid/util/SparseArray;

    invoke-static {v0}, Lcom/android/settingslib/f/c;->con(Landroid/util/SparseArray;)V

    iget-object v0, v4, Lcom/android/settingslib/f/d;->cIP:Landroid/util/SparseLongArray;

    invoke-static {v0}, Lcom/android/settingslib/f/c;->cot(Landroid/util/SparseLongArray;)V

    :cond_4
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    const-string/jumbo v0, "StorageMeasurement"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v8, "Measured shared storage in "

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    sub-long v6, v2, v6

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, "ms"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v0, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/settingslib/f/c;->cIK:Landroid/os/storage/VolumeInfo;

    invoke-virtual {v0}, Landroid/os/storage/VolumeInfo;->getType()I

    move-result v0

    const/4 v5, 0x1

    if-ne v0, v5, :cond_6

    iget-object v0, p0, Lcom/android/settingslib/f/c;->cIK:Landroid/os/storage/VolumeInfo;

    invoke-virtual {v0}, Landroid/os/storage/VolumeInfo;->isMountedReadable()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/UserInfo;

    :try_start_1
    iget-object v5, p0, Lcom/android/settingslib/f/c;->cIN:Landroid/app/usage/StorageStatsManager;

    iget-object v6, p0, Lcom/android/settingslib/f/c;->cIK:Landroid/os/storage/VolumeInfo;

    iget-object v6, v6, Landroid/os/storage/VolumeInfo;->fsUuid:Ljava/lang/String;

    iget v7, v0, Landroid/content/pm/UserInfo;->id:I

    invoke-static {v7}, Landroid/os/UserHandle;->of(I)Landroid/os/UserHandle;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Landroid/app/usage/StorageStatsManager;->queryStatsForUser(Ljava/lang/String;Landroid/os/UserHandle;)Landroid/app/usage/StorageStats;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v5

    iget v6, v0, Landroid/content/pm/UserInfo;->id:I

    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v7

    if-ne v6, v7, :cond_5

    iget-object v6, v4, Lcom/android/settingslib/f/d;->cIP:Landroid/util/SparseLongArray;

    iget v7, v0, Landroid/content/pm/UserInfo;->id:I

    invoke-virtual {v5}, Landroid/app/usage/StorageStats;->getCodeBytes()J

    move-result-wide v8

    invoke-static {v6, v7, v8, v9}, Lcom/android/settingslib/f/c;->cow(Landroid/util/SparseLongArray;IJ)V

    :cond_5
    iget-object v6, v4, Lcom/android/settingslib/f/d;->cIP:Landroid/util/SparseLongArray;

    iget v7, v0, Landroid/content/pm/UserInfo;->id:I

    invoke-virtual {v5}, Landroid/app/usage/StorageStats;->getDataBytes()J

    move-result-wide v8

    invoke-static {v6, v7, v8, v9}, Lcom/android/settingslib/f/c;->cow(Landroid/util/SparseLongArray;IJ)V

    iget-object v6, v4, Lcom/android/settingslib/f/d;->cIR:Landroid/util/SparseLongArray;

    iget v7, v0, Landroid/content/pm/UserInfo;->id:I

    iget-object v8, p0, Lcom/android/settingslib/f/c;->cIK:Landroid/os/storage/VolumeInfo;

    iget-object v8, v8, Landroid/os/storage/VolumeInfo;->fsUuid:Ljava/lang/String;

    iget v0, v0, Landroid/content/pm/UserInfo;->id:I

    invoke-direct {p0, v8, v0}, Lcom/android/settingslib/f/c;->cos(Ljava/lang/String;I)J

    move-result-wide v8

    invoke-static {v6, v7, v8, v9}, Lcom/android/settingslib/f/c;->cow(Landroid/util/SparseLongArray;IJ)V

    iget-wide v6, v4, Lcom/android/settingslib/f/d;->cIU:J

    invoke-virtual {v5}, Landroid/app/usage/StorageStats;->getCacheBytes()J

    move-result-wide v8

    add-long/2addr v6, v8

    iput-wide v6, v4, Lcom/android/settingslib/f/d;->cIU:J

    goto :goto_1

    :catch_1
    move-exception v0

    const-string/jumbo v5, "StorageMeasurement"

    invoke-static {v5, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1

    :cond_6
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    const-string/jumbo v5, "StorageMeasurement"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "Measured private storage in "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    sub-long/2addr v0, v2

    invoke-virtual {v6, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "ms"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v5, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0, v4}, Lcom/android/settingslib/f/c;->cov(Lcom/android/settingslib/f/d;)V

    return-object v4
.end method

.method static synthetic coq(Lcom/android/settingslib/f/c;)Lcom/android/settingslib/f/d;
    .locals 1

    invoke-direct {p0}, Lcom/android/settingslib/f/c;->coo()Lcom/android/settingslib/f/d;

    move-result-object v0

    return-object v0
.end method

.method private cos(Ljava/lang/String;I)J
    .locals 17

    new-instance v8, Landroid/util/ArraySet;

    invoke-direct {v8}, Landroid/util/ArraySet;-><init>()V

    invoke-static {}, Landroid/app/AppGlobals;->getInitialApplication()Landroid/app/Application;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Application;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    new-instance v9, Lcom/android/settingslib/b/H;

    invoke-static {}, Landroid/app/AppGlobals;->getInitialApplication()Landroid/app/Application;

    move-result-object v3

    invoke-direct {v9, v3}, Lcom/android/settingslib/b/H;-><init>(Landroid/content/Context;)V

    const/4 v3, 0x0

    move/from16 v0, p2

    invoke-virtual {v2, v3, v0}, Landroid/content/pm/PackageManager;->getInstalledApplicationsAsUser(II)Ljava/util/List;

    move-result-object v4

    invoke-static/range {p2 .. p2}, Landroid/os/UserHandle;->of(I)Landroid/os/UserHandle;

    move-result-object v10

    const-wide/16 v2, 0x0

    if-eqz v4, :cond_2

    invoke-interface {v4}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v11

    move-wide v4, v2

    :goto_0
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/pm/ApplicationInfo;

    :try_start_0
    iget-object v3, v2, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    move-object/from16 v0, p1

    invoke-virtual {v9, v0, v3, v10}, Lcom/android/settingslib/b/H;->cfO(Ljava/lang/String;Ljava/lang/String;Landroid/os/UserHandle;)Lcom/android/settingslib/b/J;

    move-result-object v3

    invoke-interface {v3}, Lcom/android/settingslib/b/J;->cfQ()J

    move-result-wide v6

    iget v12, v2, Landroid/content/pm/ApplicationInfo;->uid:I

    move-object/from16 v0, p1

    invoke-virtual {v9, v0, v12}, Lcom/android/settingslib/b/H;->cfN(Ljava/lang/String;I)J

    move-result-wide v12

    invoke-interface {v3}, Lcom/android/settingslib/b/J;->cfR()J

    move-result-wide v14

    cmp-long v16, v12, v14

    if-gez v16, :cond_0

    sub-long/2addr v6, v14

    add-long/2addr v6, v12

    :cond_0
    iget-object v12, v2, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v8, v12}, Landroid/util/ArraySet;->contains(Ljava/lang/Object;)Z

    move-result v12

    if-nez v12, :cond_4

    iget-object v12, v2, Landroid/content/pm/ApplicationInfo;->sourceDir:Ljava/lang/String;

    const-string/jumbo v13, "/data"

    invoke-virtual {v12, v13}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v12

    if-eqz v12, :cond_1

    invoke-interface {v3}, Lcom/android/settingslib/b/J;->cfS()J

    move-result-wide v12

    add-long/2addr v6, v12

    :cond_1
    iget-object v2, v2, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v8, v2}, Landroid/util/ArraySet;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-wide v2, v6

    :goto_1
    add-long/2addr v2, v4

    :goto_2
    move-wide v4, v2

    goto :goto_0

    :catch_0
    move-exception v2

    const-string/jumbo v3, "StorageMeasurement"

    const-string/jumbo v6, "App unexpectedly not found"

    invoke-static {v3, v6, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    move-wide v2, v4

    goto :goto_2

    :cond_2
    move-wide v4, v2

    :cond_3
    return-wide v4

    :cond_4
    move-wide v2, v6

    goto :goto_1
.end method

.method private static cot(Landroid/util/SparseLongArray;)V
    .locals 3

    const/16 v0, 0x3e7

    invoke-virtual {p0, v0}, Landroid/util/SparseLongArray;->get(I)J

    move-result-wide v0

    const/4 v2, 0x0

    invoke-static {p0, v2, v0, v1}, Lcom/android/settingslib/f/c;->cow(Landroid/util/SparseLongArray;IJ)V

    return-void
.end method

.method private cov(Lcom/android/settingslib/f/d;)V
    .locals 8

    const-wide/16 v4, 0x0

    iget-wide v0, p1, Lcom/android/settingslib/f/d;->cIQ:J

    iget-wide v2, p1, Lcom/android/settingslib/f/d;->cIO:J

    sub-long/2addr v0, v2

    iget-object v2, p1, Lcom/android/settingslib/f/d;->cIR:Landroid/util/SparseLongArray;

    if-eqz v2, :cond_0

    iget-object v2, p1, Lcom/android/settingslib/f/d;->cIR:Landroid/util/SparseLongArray;

    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/util/SparseLongArray;->get(I)J

    move-result-wide v2

    sub-long/2addr v0, v2

    :cond_0
    iget-wide v2, p1, Lcom/android/settingslib/f/d;->cIU:J

    sub-long v2, v0, v2

    iget-object v0, p1, Lcom/android/settingslib/f/d;->cIT:Landroid/util/SparseArray;

    if-eqz v0, :cond_1

    iget-object v0, p1, Lcom/android/settingslib/f/d;->cIT:Landroid/util/SparseArray;

    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/HashMap;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    sub-long v0, v2, v0

    move-wide v2, v0

    goto :goto_0

    :cond_1
    iget-object v1, p1, Lcom/android/settingslib/f/d;->cIP:Landroid/util/SparseLongArray;

    const/4 v0, 0x0

    :goto_1
    invoke-virtual {v1}, Landroid/util/SparseLongArray;->size()I

    move-result v6

    if-ge v0, v6, :cond_4

    invoke-virtual {v1, v0}, Landroid/util/SparseLongArray;->keyAt(I)I

    move-result v6

    invoke-static {}, Landroid/app/ActivityManager;->getCurrentUser()I

    move-result v7

    if-eq v6, v7, :cond_2

    const/16 v7, 0x63

    if-ne v6, v7, :cond_3

    :cond_2
    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_3
    const/16 v7, 0x3e7

    if-eq v6, v7, :cond_2

    invoke-virtual {v1, v0}, Landroid/util/SparseLongArray;->valueAt(I)J

    move-result-wide v6

    sub-long/2addr v2, v6

    goto :goto_2

    :cond_4
    cmp-long v0, v2, v4

    if-gez v0, :cond_5

    move-wide v2, v4

    :cond_5
    iget-object v0, p1, Lcom/android/settingslib/f/d;->cIS:Landroid/util/SparseLongArray;

    if-eqz v0, :cond_6

    iget-object v0, p1, Lcom/android/settingslib/f/d;->cIS:Landroid/util/SparseLongArray;

    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v1

    invoke-virtual {v0, v1, v2, v3}, Landroid/util/SparseLongArray;->put(IJ)V

    :cond_6
    return-void
.end method

.method private static cow(Landroid/util/SparseLongArray;IJ)V
    .locals 2

    invoke-virtual {p0, p1}, Landroid/util/SparseLongArray;->get(I)J

    move-result-wide v0

    add-long/2addr v0, p2

    invoke-virtual {p0, p1, v0, v1}, Landroid/util/SparseLongArray;->put(IJ)V

    return-void
.end method


# virtual methods
.method public cop()V
    .locals 2

    new-instance v0, Lcom/android/settingslib/f/f;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/android/settingslib/f/f;-><init>(Lcom/android/settingslib/f/c;Lcom/android/settingslib/f/f;)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/android/settingslib/f/f;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    return-void
.end method

.method public cor()V
    .locals 0

    invoke-virtual {p0}, Lcom/android/settingslib/f/c;->cop()V

    return-void
.end method

.method public cou(Lcom/android/settingslib/f/e;)V
    .locals 1

    iget-object v0, p0, Lcom/android/settingslib/f/c;->cIM:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settingslib/f/c;->cIM:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_1

    :cond_0
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/android/settingslib/f/c;->cIM:Ljava/lang/ref/WeakReference;

    :cond_1
    return-void
.end method

.method public onDestroy()V
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/settingslib/f/c;->cIM:Ljava/lang/ref/WeakReference;

    return-void
.end method
