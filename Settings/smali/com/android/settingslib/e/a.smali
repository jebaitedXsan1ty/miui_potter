.class public Lcom/android/settingslib/e/a;
.super Landroid/graphics/drawable/Drawable;
.source "UserIconDrawable.java"

# interfaces
.implements Landroid/graphics/drawable/Drawable$Callback;


# instance fields
.field private cHV:I

.field private cHW:Landroid/graphics/Paint;

.field private cHX:Landroid/graphics/drawable/Drawable;

.field private cHY:F

.field private cHZ:Landroid/content/res/ColorStateList;

.field private cIa:Z

.field private cIb:Landroid/content/res/ColorStateList;

.field private cIc:Landroid/graphics/Bitmap;

.field private cId:F

.field private final cIe:Landroid/graphics/Paint;

.field private cIf:F

.field private cIg:Landroid/graphics/PorterDuff$Mode;

.field private cIh:F

.field private final cIi:Landroid/graphics/Paint;

.field private cIj:Landroid/graphics/drawable/Drawable;

.field private cIk:F

.field private final cIl:Landroid/graphics/Matrix;

.field private cIm:F

.field private cIn:F

.field private cIo:Landroid/graphics/Bitmap;

.field private cIp:Landroid/graphics/Paint;


# direct methods
.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/android/settingslib/e/a;-><init>(I)V

    return-void
.end method

.method public constructor <init>(I)V
    .locals 4

    const/4 v3, 0x0

    const/4 v2, 0x0

    const/4 v1, 0x1

    invoke-direct {p0}, Landroid/graphics/drawable/Drawable;-><init>()V

    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/android/settingslib/e/a;->cIi:Landroid/graphics/Paint;

    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/android/settingslib/e/a;->cIe:Landroid/graphics/Paint;

    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/android/settingslib/e/a;->cIl:Landroid/graphics/Matrix;

    const/4 v0, 0x0

    iput v0, p0, Lcom/android/settingslib/e/a;->cIf:F

    iput v2, p0, Lcom/android/settingslib/e/a;->cHV:I

    iput-boolean v1, p0, Lcom/android/settingslib/e/a;->cIa:Z

    iput-object v3, p0, Lcom/android/settingslib/e/a;->cIb:Landroid/content/res/ColorStateList;

    sget-object v0, Landroid/graphics/PorterDuff$Mode;->SRC_ATOP:Landroid/graphics/PorterDuff$Mode;

    iput-object v0, p0, Lcom/android/settingslib/e/a;->cIg:Landroid/graphics/PorterDuff$Mode;

    iput-object v3, p0, Lcom/android/settingslib/e/a;->cHZ:Landroid/content/res/ColorStateList;

    iget-object v0, p0, Lcom/android/settingslib/e/a;->cIi:Landroid/graphics/Paint;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    iget-object v0, p0, Lcom/android/settingslib/e/a;->cIi:Landroid/graphics/Paint;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setFilterBitmap(Z)V

    iget-object v0, p0, Lcom/android/settingslib/e/a;->cIe:Landroid/graphics/Paint;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setFilterBitmap(Z)V

    iget-object v0, p0, Lcom/android/settingslib/e/a;->cIe:Landroid/graphics/Paint;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    if-lez p1, :cond_0

    invoke-virtual {p0, v2, v2, p1, p1}, Lcom/android/settingslib/e/a;->setBounds(IIII)V

    invoke-virtual {p0, p1}, Lcom/android/settingslib/e/a;->cnM(I)V

    :cond_0
    invoke-virtual {p0, v3}, Lcom/android/settingslib/e/a;->cnK(Landroid/graphics/Bitmap;)Lcom/android/settingslib/e/a;

    return-void
.end method

.method private cnL()V
    .locals 9

    const v7, 0x3a83126f    # 0.001f

    const/4 v6, 0x0

    const/high16 v8, 0x3f000000    # 0.5f

    iput-boolean v6, p0, Lcom/android/settingslib/e/a;->cIa:Z

    iget-object v0, p0, Lcom/android/settingslib/e/a;->cIc:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settingslib/e/a;->cIj:Landroid/graphics/drawable/Drawable;

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/android/settingslib/e/a;->cIo:Landroid/graphics/Bitmap;

    if-nez v0, :cond_1

    :cond_0
    return-void

    :cond_1
    new-instance v0, Landroid/graphics/Canvas;

    iget-object v1, p0, Lcom/android/settingslib/e/a;->cIc:Landroid/graphics/Bitmap;

    invoke-direct {v0, v1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    sget-object v1, Landroid/graphics/PorterDuff$Mode;->CLEAR:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v0, v6, v1}, Landroid/graphics/Canvas;->drawColor(ILandroid/graphics/PorterDuff$Mode;)V

    iget-object v1, p0, Lcom/android/settingslib/e/a;->cIj:Landroid/graphics/drawable/Drawable;

    if-eqz v1, :cond_6

    iget-object v1, p0, Lcom/android/settingslib/e/a;->cIj:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1, v0}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    :cond_2
    :goto_0
    iget-object v1, p0, Lcom/android/settingslib/e/a;->cHZ:Landroid/content/res/ColorStateList;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/android/settingslib/e/a;->cIp:Landroid/graphics/Paint;

    iget-object v2, p0, Lcom/android/settingslib/e/a;->cHZ:Landroid/content/res/ColorStateList;

    invoke-virtual {p0}, Lcom/android/settingslib/e/a;->getState()[I

    move-result-object v3

    invoke-virtual {v2, v3, v6}, Landroid/content/res/ColorStateList;->getColorForState([II)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    :cond_3
    iget v1, p0, Lcom/android/settingslib/e/a;->cIm:F

    iget v2, p0, Lcom/android/settingslib/e/a;->cIn:F

    add-float/2addr v1, v2

    cmpl-float v1, v1, v7

    if-lez v1, :cond_4

    iget v1, p0, Lcom/android/settingslib/e/a;->cIh:F

    iget v2, p0, Lcom/android/settingslib/e/a;->cIf:F

    sub-float/2addr v1, v2

    iget v2, p0, Lcom/android/settingslib/e/a;->cIm:F

    mul-float/2addr v2, v8

    sub-float/2addr v1, v2

    invoke-virtual {p0}, Lcom/android/settingslib/e/a;->getBounds()Landroid/graphics/Rect;

    move-result-object v2

    invoke-virtual {v2}, Landroid/graphics/Rect;->exactCenterX()F

    move-result v2

    invoke-virtual {p0}, Lcom/android/settingslib/e/a;->getBounds()Landroid/graphics/Rect;

    move-result-object v3

    invoke-virtual {v3}, Landroid/graphics/Rect;->exactCenterY()F

    move-result v3

    iget-object v4, p0, Lcom/android/settingslib/e/a;->cIp:Landroid/graphics/Paint;

    invoke-virtual {v0, v2, v3, v1, v4}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    :cond_4
    iget-object v1, p0, Lcom/android/settingslib/e/a;->cHX:Landroid/graphics/drawable/Drawable;

    if-eqz v1, :cond_5

    iget v1, p0, Lcom/android/settingslib/e/a;->cHY:F

    cmpl-float v1, v1, v7

    if-lez v1, :cond_5

    iget v1, p0, Lcom/android/settingslib/e/a;->cHY:F

    const/high16 v2, 0x40000000    # 2.0f

    mul-float/2addr v1, v2

    iget-object v2, p0, Lcom/android/settingslib/e/a;->cIc:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    int-to-float v2, v2

    sub-float/2addr v2, v1

    iget-object v3, p0, Lcom/android/settingslib/e/a;->cIc:Landroid/graphics/Bitmap;

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    int-to-float v3, v3

    sub-float/2addr v3, v1

    iget-object v4, p0, Lcom/android/settingslib/e/a;->cHX:Landroid/graphics/drawable/Drawable;

    float-to-int v5, v3

    float-to-int v6, v2

    add-float v7, v3, v1

    float-to-int v7, v7

    add-float/2addr v1, v2

    float-to-int v1, v1

    invoke-virtual {v4, v5, v6, v7, v1}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    iget-object v1, p0, Lcom/android/settingslib/e/a;->cHX:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v1

    invoke-virtual {v1}, Landroid/graphics/Rect;->width()I

    move-result v1

    int-to-float v1, v1

    mul-float/2addr v1, v8

    iget v4, p0, Lcom/android/settingslib/e/a;->cIk:F

    add-float/2addr v1, v4

    iget v4, p0, Lcom/android/settingslib/e/a;->cHY:F

    add-float/2addr v3, v4

    iget v4, p0, Lcom/android/settingslib/e/a;->cHY:F

    add-float/2addr v2, v4

    iget-object v4, p0, Lcom/android/settingslib/e/a;->cHW:Landroid/graphics/Paint;

    invoke-virtual {v0, v3, v2, v1, v4}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    iget-object v1, p0, Lcom/android/settingslib/e/a;->cHX:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1, v0}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    :cond_5
    return-void

    :cond_6
    iget-object v1, p0, Lcom/android/settingslib/e/a;->cIo:Landroid/graphics/Bitmap;

    if-eqz v1, :cond_2

    invoke-virtual {v0}, Landroid/graphics/Canvas;->save()I

    move-result v1

    iget-object v2, p0, Lcom/android/settingslib/e/a;->cIl:Landroid/graphics/Matrix;

    invoke-virtual {v0, v2}, Landroid/graphics/Canvas;->concat(Landroid/graphics/Matrix;)V

    iget-object v2, p0, Lcom/android/settingslib/e/a;->cIo:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    int-to-float v2, v2

    mul-float/2addr v2, v8

    iget-object v3, p0, Lcom/android/settingslib/e/a;->cIo:Landroid/graphics/Bitmap;

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    int-to-float v3, v3

    mul-float/2addr v3, v8

    iget v4, p0, Lcom/android/settingslib/e/a;->cId:F

    iget-object v5, p0, Lcom/android/settingslib/e/a;->cIi:Landroid/graphics/Paint;

    invoke-virtual {v0, v2, v3, v4, v5}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    invoke-virtual {v0, v1}, Landroid/graphics/Canvas;->restoreToCount(I)V

    goto/16 :goto_0
.end method

.method public static cnN(Landroid/content/Context;)I
    .locals 2

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/android/settingslib/d;->cKv:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    return v0
.end method


# virtual methods
.method public cnJ()Lcom/android/settingslib/e/a;
    .locals 5

    const/4 v4, 0x0

    const/4 v3, 0x0

    iget v0, p0, Lcom/android/settingslib/e/a;->cHV:I

    if-gtz v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "Baking requires an explicit intrinsic size"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    new-instance v0, Landroid/graphics/Rect;

    iget v1, p0, Lcom/android/settingslib/e/a;->cHV:I

    iget v2, p0, Lcom/android/settingslib/e/a;->cHV:I

    invoke-direct {v0, v4, v4, v1, v2}, Landroid/graphics/Rect;-><init>(IIII)V

    invoke-virtual {p0, v0}, Lcom/android/settingslib/e/a;->onBoundsChange(Landroid/graphics/Rect;)V

    invoke-direct {p0}, Lcom/android/settingslib/e/a;->cnL()V

    iput-object v3, p0, Lcom/android/settingslib/e/a;->cHZ:Landroid/content/res/ColorStateList;

    iput-object v3, p0, Lcom/android/settingslib/e/a;->cIp:Landroid/graphics/Paint;

    iput-object v3, p0, Lcom/android/settingslib/e/a;->cHW:Landroid/graphics/Paint;

    iget-object v0, p0, Lcom/android/settingslib/e/a;->cIj:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/settingslib/e/a;->cIj:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v3}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    iput-object v3, p0, Lcom/android/settingslib/e/a;->cIj:Landroid/graphics/drawable/Drawable;

    :cond_1
    :goto_0
    return-object p0

    :cond_2
    iget-object v0, p0, Lcom/android/settingslib/e/a;->cIo:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/settingslib/e/a;->cIo:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    iput-object v3, p0, Lcom/android/settingslib/e/a;->cIo:Landroid/graphics/Bitmap;

    goto :goto_0
.end method

.method public cnK(Landroid/graphics/Bitmap;)Lcom/android/settingslib/e/a;
    .locals 4

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/android/settingslib/e/a;->cIj:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settingslib/e/a;->cIj:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    iput-object v1, p0, Lcom/android/settingslib/e/a;->cIj:Landroid/graphics/drawable/Drawable;

    :cond_0
    iput-object p1, p0, Lcom/android/settingslib/e/a;->cIo:Landroid/graphics/Bitmap;

    iget-object v0, p0, Lcom/android/settingslib/e/a;->cIo:Landroid/graphics/Bitmap;

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/android/settingslib/e/a;->cIi:Landroid/graphics/Paint;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setShader(Landroid/graphics/Shader;)Landroid/graphics/Shader;

    iput-object v1, p0, Lcom/android/settingslib/e/a;->cIc:Landroid/graphics/Bitmap;

    :goto_0
    invoke-virtual {p0}, Lcom/android/settingslib/e/a;->getBounds()Landroid/graphics/Rect;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/settingslib/e/a;->onBoundsChange(Landroid/graphics/Rect;)V

    return-object p0

    :cond_1
    iget-object v0, p0, Lcom/android/settingslib/e/a;->cIi:Landroid/graphics/Paint;

    new-instance v1, Landroid/graphics/BitmapShader;

    sget-object v2, Landroid/graphics/Shader$TileMode;->CLAMP:Landroid/graphics/Shader$TileMode;

    sget-object v3, Landroid/graphics/Shader$TileMode;->CLAMP:Landroid/graphics/Shader$TileMode;

    invoke-direct {v1, p1, v2, v3}, Landroid/graphics/BitmapShader;-><init>(Landroid/graphics/Bitmap;Landroid/graphics/Shader$TileMode;Landroid/graphics/Shader$TileMode;)V

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setShader(Landroid/graphics/Shader;)Landroid/graphics/Shader;

    goto :goto_0
.end method

.method public cnM(I)V
    .locals 0

    iput p1, p0, Lcom/android/settingslib/e/a;->cHV:I

    return-void
.end method

.method public cnO(Landroid/graphics/drawable/Drawable;)Lcom/android/settingslib/e/a;
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/android/settingslib/e/a;->cIj:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settingslib/e/a;->cIj:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    :cond_0
    iput-object v1, p0, Lcom/android/settingslib/e/a;->cIo:Landroid/graphics/Bitmap;

    iput-object p1, p0, Lcom/android/settingslib/e/a;->cIj:Landroid/graphics/drawable/Drawable;

    iget-object v0, p0, Lcom/android/settingslib/e/a;->cIj:Landroid/graphics/drawable/Drawable;

    if-nez v0, :cond_1

    iput-object v1, p0, Lcom/android/settingslib/e/a;->cIc:Landroid/graphics/Bitmap;

    :goto_0
    invoke-virtual {p0}, Lcom/android/settingslib/e/a;->getBounds()Landroid/graphics/Rect;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/settingslib/e/a;->onBoundsChange(Landroid/graphics/Rect;)V

    return-object p0

    :cond_1
    iget-object v0, p0, Lcom/android/settingslib/e/a;->cIj:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p0}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    goto :goto_0
.end method

.method public draw(Landroid/graphics/Canvas;)V
    .locals 5

    const/4 v4, 0x0

    const/4 v1, 0x0

    iget-boolean v0, p0, Lcom/android/settingslib/e/a;->cIa:Z

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/android/settingslib/e/a;->cnL()V

    :cond_0
    iget-object v0, p0, Lcom/android/settingslib/e/a;->cIc:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/settingslib/e/a;->cIb:Landroid/content/res/ColorStateList;

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/android/settingslib/e/a;->cIe:Landroid/graphics/Paint;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColorFilter(Landroid/graphics/ColorFilter;)Landroid/graphics/ColorFilter;

    :goto_0
    iget-object v0, p0, Lcom/android/settingslib/e/a;->cIc:Landroid/graphics/Bitmap;

    iget-object v1, p0, Lcom/android/settingslib/e/a;->cIe:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v4, v4, v1}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    :cond_1
    return-void

    :cond_2
    iget-object v0, p0, Lcom/android/settingslib/e/a;->cIb:Landroid/content/res/ColorStateList;

    invoke-virtual {p0}, Lcom/android/settingslib/e/a;->getState()[I

    move-result-object v1

    iget-object v2, p0, Lcom/android/settingslib/e/a;->cIb:Landroid/content/res/ColorStateList;

    invoke-virtual {v2}, Landroid/content/res/ColorStateList;->getDefaultColor()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/res/ColorStateList;->getColorForState([II)I

    move-result v1

    iget-object v0, p0, Lcom/android/settingslib/e/a;->cIe:Landroid/graphics/Paint;

    invoke-virtual {v0}, Landroid/graphics/Paint;->getColorFilter()Landroid/graphics/ColorFilter;

    move-result-object v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/android/settingslib/e/a;->cIe:Landroid/graphics/Paint;

    new-instance v2, Landroid/graphics/PorterDuffColorFilter;

    iget-object v3, p0, Lcom/android/settingslib/e/a;->cIg:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {v2, v1, v3}, Landroid/graphics/PorterDuffColorFilter;-><init>(ILandroid/graphics/PorterDuff$Mode;)V

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setColorFilter(Landroid/graphics/ColorFilter;)Landroid/graphics/ColorFilter;

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/android/settingslib/e/a;->cIe:Landroid/graphics/Paint;

    invoke-virtual {v0}, Landroid/graphics/Paint;->getColorFilter()Landroid/graphics/ColorFilter;

    move-result-object v0

    check-cast v0, Landroid/graphics/PorterDuffColorFilter;

    iget-object v2, p0, Lcom/android/settingslib/e/a;->cIg:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v0, v2}, Landroid/graphics/PorterDuffColorFilter;->setMode(Landroid/graphics/PorterDuff$Mode;)V

    iget-object v0, p0, Lcom/android/settingslib/e/a;->cIe:Landroid/graphics/Paint;

    invoke-virtual {v0}, Landroid/graphics/Paint;->getColorFilter()Landroid/graphics/ColorFilter;

    move-result-object v0

    check-cast v0, Landroid/graphics/PorterDuffColorFilter;

    invoke-virtual {v0, v1}, Landroid/graphics/PorterDuffColorFilter;->setColor(I)V

    goto :goto_0
.end method

.method public getConstantState()Landroid/graphics/drawable/Drawable$ConstantState;
    .locals 2

    new-instance v0, Landroid/graphics/drawable/BitmapDrawable;

    iget-object v1, p0, Lcom/android/settingslib/e/a;->cIc:Landroid/graphics/Bitmap;

    invoke-direct {v0, v1}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/graphics/Bitmap;)V

    invoke-virtual {v0}, Landroid/graphics/drawable/BitmapDrawable;->getConstantState()Landroid/graphics/drawable/Drawable$ConstantState;

    move-result-object v0

    return-object v0
.end method

.method public getIntrinsicHeight()I
    .locals 1

    invoke-virtual {p0}, Lcom/android/settingslib/e/a;->getIntrinsicWidth()I

    move-result v0

    return v0
.end method

.method public getIntrinsicWidth()I
    .locals 1

    iget v0, p0, Lcom/android/settingslib/e/a;->cHV:I

    if-gtz v0, :cond_0

    iget v0, p0, Lcom/android/settingslib/e/a;->cId:F

    float-to-int v0, v0

    mul-int/lit8 v0, v0, 0x2

    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lcom/android/settingslib/e/a;->cHV:I

    goto :goto_0
.end method

.method public getOpacity()I
    .locals 1

    const/4 v0, -0x3

    return v0
.end method

.method public invalidateDrawable(Landroid/graphics/drawable/Drawable;)V
    .locals 0

    invoke-virtual {p0}, Lcom/android/settingslib/e/a;->invalidateSelf()V

    return-void
.end method

.method public invalidateSelf()V
    .locals 1

    invoke-super {p0}, Landroid/graphics/drawable/Drawable;->invalidateSelf()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/settingslib/e/a;->cIa:Z

    return-void
.end method

.method public isStateful()Z
    .locals 1

    iget-object v0, p0, Lcom/android/settingslib/e/a;->cHZ:Landroid/content/res/ColorStateList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settingslib/e/a;->cHZ:Landroid/content/res/ColorStateList;

    invoke-virtual {v0}, Landroid/content/res/ColorStateList;->isStateful()Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onBoundsChange(Landroid/graphics/Rect;)V
    .locals 7

    const/high16 v3, 0x40000000    # 2.0f

    const/high16 v6, 0x3f000000    # 0.5f

    invoke-virtual {p1}, Landroid/graphics/Rect;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/settingslib/e/a;->cIo:Landroid/graphics/Bitmap;

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/android/settingslib/e/a;->cIj:Landroid/graphics/drawable/Drawable;

    if-nez v0, :cond_1

    :cond_0
    return-void

    :cond_1
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    move-result v0

    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    int-to-float v0, v0

    mul-float/2addr v0, v6

    mul-float v1, v0, v3

    float-to-int v1, v1

    iget-object v2, p0, Lcom/android/settingslib/e/a;->cIc:Landroid/graphics/Bitmap;

    if-eqz v2, :cond_2

    iget v2, p0, Lcom/android/settingslib/e/a;->cIh:F

    mul-float/2addr v2, v3

    float-to-int v2, v2

    if-eq v1, v2, :cond_4

    :cond_2
    iput v0, p0, Lcom/android/settingslib/e/a;->cIh:F

    iget-object v0, p0, Lcom/android/settingslib/e/a;->cIc:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/android/settingslib/e/a;->cIc:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    :cond_3
    sget-object v0, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v1, v1, v0}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settingslib/e/a;->cIc:Landroid/graphics/Bitmap;

    :cond_4
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    move-result v0

    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    int-to-float v0, v0

    mul-float/2addr v0, v6

    iput v0, p0, Lcom/android/settingslib/e/a;->cIh:F

    iget v0, p0, Lcom/android/settingslib/e/a;->cIh:F

    iget v1, p0, Lcom/android/settingslib/e/a;->cIm:F

    sub-float/2addr v0, v1

    iget v1, p0, Lcom/android/settingslib/e/a;->cIn:F

    sub-float/2addr v0, v1

    iget v1, p0, Lcom/android/settingslib/e/a;->cIf:F

    sub-float/2addr v0, v1

    new-instance v1, Landroid/graphics/RectF;

    invoke-virtual {p1}, Landroid/graphics/Rect;->exactCenterX()F

    move-result v2

    sub-float/2addr v2, v0

    invoke-virtual {p1}, Landroid/graphics/Rect;->exactCenterY()F

    move-result v3

    sub-float/2addr v3, v0

    invoke-virtual {p1}, Landroid/graphics/Rect;->exactCenterX()F

    move-result v4

    add-float/2addr v4, v0

    invoke-virtual {p1}, Landroid/graphics/Rect;->exactCenterY()F

    move-result v5

    add-float/2addr v0, v5

    invoke-direct {v1, v2, v3, v4, v0}, Landroid/graphics/RectF;-><init>(FFFF)V

    iget-object v0, p0, Lcom/android/settingslib/e/a;->cIj:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_6

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    invoke-virtual {v1, v0}, Landroid/graphics/RectF;->round(Landroid/graphics/Rect;)V

    iget-object v1, p0, Lcom/android/settingslib/e/a;->cIj:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v1

    iget-object v2, p0, Lcom/android/settingslib/e/a;->cIj:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v2

    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v1

    int-to-float v1, v1

    mul-float/2addr v1, v6

    iput v1, p0, Lcom/android/settingslib/e/a;->cId:F

    iget-object v1, p0, Lcom/android/settingslib/e/a;->cIj:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1, v0}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    :cond_5
    :goto_0
    invoke-virtual {p0}, Lcom/android/settingslib/e/a;->invalidateSelf()V

    return-void

    :cond_6
    iget-object v0, p0, Lcom/android/settingslib/e/a;->cIo:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/android/settingslib/e/a;->cIo:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    int-to-float v0, v0

    mul-float/2addr v0, v6

    iget-object v2, p0, Lcom/android/settingslib/e/a;->cIo:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    int-to-float v2, v2

    mul-float/2addr v2, v6

    invoke-static {v0, v2}, Ljava/lang/Math;->min(FF)F

    move-result v3

    iput v3, p0, Lcom/android/settingslib/e/a;->cId:F

    new-instance v3, Landroid/graphics/RectF;

    iget v4, p0, Lcom/android/settingslib/e/a;->cId:F

    sub-float v4, v0, v4

    iget v5, p0, Lcom/android/settingslib/e/a;->cId:F

    sub-float v5, v2, v5

    iget v6, p0, Lcom/android/settingslib/e/a;->cId:F

    add-float/2addr v0, v6

    iget v6, p0, Lcom/android/settingslib/e/a;->cId:F

    add-float/2addr v2, v6

    invoke-direct {v3, v4, v5, v0, v2}, Landroid/graphics/RectF;-><init>(FFFF)V

    iget-object v0, p0, Lcom/android/settingslib/e/a;->cIl:Landroid/graphics/Matrix;

    sget-object v2, Landroid/graphics/Matrix$ScaleToFit;->FILL:Landroid/graphics/Matrix$ScaleToFit;

    invoke-virtual {v0, v3, v1, v2}, Landroid/graphics/Matrix;->setRectToRect(Landroid/graphics/RectF;Landroid/graphics/RectF;Landroid/graphics/Matrix$ScaleToFit;)Z

    goto :goto_0
.end method

.method public scheduleDrawable(Landroid/graphics/drawable/Drawable;Ljava/lang/Runnable;J)V
    .locals 1

    invoke-virtual {p0, p2, p3, p4}, Lcom/android/settingslib/e/a;->scheduleSelf(Ljava/lang/Runnable;J)V

    return-void
.end method

.method public setAlpha(I)V
    .locals 1

    iget-object v0, p0, Lcom/android/settingslib/e/a;->cIe:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setAlpha(I)V

    invoke-super {p0}, Landroid/graphics/drawable/Drawable;->invalidateSelf()V

    return-void
.end method

.method public setColorFilter(Landroid/graphics/ColorFilter;)V
    .locals 0

    return-void
.end method

.method public setTintList(Landroid/content/res/ColorStateList;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settingslib/e/a;->cIb:Landroid/content/res/ColorStateList;

    invoke-super {p0}, Landroid/graphics/drawable/Drawable;->invalidateSelf()V

    return-void
.end method

.method public setTintMode(Landroid/graphics/PorterDuff$Mode;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settingslib/e/a;->cIg:Landroid/graphics/PorterDuff$Mode;

    invoke-super {p0}, Landroid/graphics/drawable/Drawable;->invalidateSelf()V

    return-void
.end method

.method public unscheduleDrawable(Landroid/graphics/drawable/Drawable;Ljava/lang/Runnable;)V
    .locals 0

    invoke-virtual {p0, p2}, Lcom/android/settingslib/e/a;->unscheduleSelf(Ljava/lang/Runnable;)V

    return-void
.end method
