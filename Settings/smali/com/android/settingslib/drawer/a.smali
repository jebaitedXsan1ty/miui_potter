.class public Lcom/android/settingslib/drawer/a;
.super Lmiui/app/Activity;
.source "SettingsDrawerActivity.java"


# static fields
.field protected static final czg:Ljava/util/concurrent/ExecutorService;

.field private static final czh:Z

.field private static czi:Landroid/util/ArraySet;


# instance fields
.field private final czd:Lcom/android/settingslib/drawer/d;

.field private cze:Landroid/widget/FrameLayout;

.field private final czf:Ljava/util/List;


# direct methods
.method static synthetic -get0()Landroid/util/ArraySet;
    .locals 1

    sget-object v0, Lcom/android/settingslib/drawer/a;->czi:Landroid/util/ArraySet;

    return-object v0
.end method

.method static constructor <clinit>()V
    .locals 2

    const-string/jumbo v0, "SettingsDrawerActivity"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    sput-boolean v0, Lcom/android/settingslib/drawer/a;->czh:Z

    new-instance v0, Landroid/util/ArraySet;

    invoke-direct {v0}, Landroid/util/ArraySet;-><init>()V

    sput-object v0, Lcom/android/settingslib/drawer/a;->czi:Landroid/util/ArraySet;

    invoke-static {}, Ljava/util/concurrent/Executors;->newCachedThreadPool()Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    sput-object v0, Lcom/android/settingslib/drawer/a;->czg:Ljava/util/concurrent/ExecutorService;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Lmiui/app/Activity;-><init>()V

    new-instance v0, Lcom/android/settingslib/drawer/d;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/android/settingslib/drawer/d;-><init>(Lcom/android/settingslib/drawer/a;Lcom/android/settingslib/drawer/d;)V

    iput-object v0, p0, Lcom/android/settingslib/drawer/a;->czd:Lcom/android/settingslib/drawer/d;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/settingslib/drawer/a;->czf:Ljava/util/List;

    return-void
.end method

.method private cdQ()V
    .locals 2

    invoke-virtual {p0}, Lcom/android/settingslib/drawer/a;->getActionBar()Lmiui/app/ActionBar;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    :cond_0
    return-void
.end method

.method static synthetic cdR(Lcom/android/settingslib/drawer/a;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/settingslib/drawer/a;->cdS()V

    return-void
.end method

.method private cdS()V
    .locals 3

    iget-object v0, p0, Lcom/android/settingslib/drawer/a;->czf:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    iget-object v0, p0, Lcom/android/settingslib/drawer/a;->czf:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settingslib/drawer/b;

    invoke-interface {v0}, Lcom/android/settingslib/drawer/b;->Ca()V

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    return-void
.end method


# virtual methods
.method public cdM(Lcom/android/settingslib/drawer/b;)V
    .locals 1

    iget-object v0, p0, Lcom/android/settingslib/drawer/a;->czf:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public cdN(Lcom/android/settingslib/drawer/b;)V
    .locals 1

    iget-object v0, p0, Lcom/android/settingslib/drawer/a;->czf:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    return-void
.end method

.method public cdO()V
    .locals 3

    new-instance v0, Lcom/android/settingslib/drawer/c;

    invoke-direct {v0, p0}, Lcom/android/settingslib/drawer/c;-><init>(Lcom/android/settingslib/drawer/a;)V

    sget-object v1, Lcom/android/settingslib/drawer/a;->czg:Ljava/util/concurrent/ExecutorService;

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Void;

    invoke-virtual {v0, v1, v2}, Lcom/android/settingslib/drawer/c;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    return-void
.end method

.method public cdP(Landroid/content/ComponentName;Z)V
    .locals 4

    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/android/settingslib/drawer/a;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    invoke-virtual {v2, p1}, Landroid/content/pm/PackageManager;->getComponentEnabledSetting(Landroid/content/ComponentName;)I

    move-result v3

    if-ne v3, v1, :cond_0

    move v0, v1

    :cond_0
    if-ne v0, p2, :cond_1

    if-nez v3, :cond_2

    :cond_1
    if-eqz p2, :cond_3

    sget-object v0, Lcom/android/settingslib/drawer/a;->czi:Landroid/util/ArraySet;

    invoke-virtual {v0, p1}, Landroid/util/ArraySet;->remove(Ljava/lang/Object;)Z

    :goto_0
    if-eqz p2, :cond_4

    move v0, v1

    :goto_1
    invoke-virtual {v2, p1, v0, v1}, Landroid/content/pm/PackageManager;->setComponentEnabledSetting(Landroid/content/ComponentName;II)V

    :cond_2
    return-void

    :cond_3
    sget-object v0, Lcom/android/settingslib/drawer/a;->czi:Landroid/util/ArraySet;

    invoke-virtual {v0, p1}, Landroid/util/ArraySet;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_4
    const/4 v0, 0x2

    goto :goto_1
.end method

.method public cdT()V
    .locals 0

    invoke-virtual {p0}, Lcom/android/settingslib/drawer/a;->finish()V

    return-void
.end method

.method public cdU()Ljava/lang/String;
    .locals 1

    const-string/jumbo v0, "com.android.settings"

    return-object v0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 5

    const/16 v4, 0x26

    const/4 v3, 0x0

    invoke-super {p0, p1}, Lmiui/app/Activity;->onCreate(Landroid/os/Bundle;)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    invoke-virtual {p0}, Lcom/android/settingslib/drawer/a;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v0

    sget-object v1, Landroid/R$styleable;->Theme:[I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources$Theme;->obtainStyledAttributes([I)Landroid/content/res/TypedArray;

    move-result-object v1

    invoke-virtual {v1, v4, v3}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/android/settingslib/drawer/a;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/high16 v2, -0x80000000

    invoke-virtual {v0, v2}, Landroid/view/Window;->addFlags(I)V

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/android/settingslib/drawer/a;->requestWindowFeature(I)Z

    :cond_0
    sget v0, Lcom/android/settingslib/h;->cLn:I

    invoke-super {p0, v0}, Lmiui/app/Activity;->setContentView(I)V

    sget v0, Lcom/android/settingslib/g;->cKU:I

    invoke-virtual {p0, v0}, Lcom/android/settingslib/drawer/a;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, Lcom/android/settingslib/drawer/a;->cze:Landroid/widget/FrameLayout;

    sget v0, Lcom/android/settingslib/g;->cKP:I

    invoke-virtual {p0, v0}, Lcom/android/settingslib/drawer/a;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Toolbar;

    invoke-virtual {v1, v4, v3}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    if-eqz v1, :cond_1

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/Toolbar;->setVisibility(I)V

    return-void

    :cond_1
    invoke-virtual {p0, v0}, Lcom/android/settingslib/drawer/a;->setActionBar(Landroid/widget/Toolbar;)V

    return-void
.end method

.method public onNavigateUp()Z
    .locals 1

    invoke-virtual {p0}, Lcom/android/settingslib/drawer/a;->finish()V

    const/4 v0, 0x1

    return v0
.end method

.method protected onPause()V
    .locals 1

    iget-object v0, p0, Lcom/android/settingslib/drawer/a;->czd:Lcom/android/settingslib/drawer/d;

    invoke-virtual {p0, v0}, Lcom/android/settingslib/drawer/a;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    invoke-super {p0}, Lmiui/app/Activity;->onPause()V

    return-void
.end method

.method protected onResume()V
    .locals 4

    const/4 v3, 0x0

    invoke-super {p0}, Lmiui/app/Activity;->onResume()V

    new-instance v0, Landroid/content/IntentFilter;

    const-string/jumbo v1, "android.intent.action.PACKAGE_ADDED"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    const-string/jumbo v1, "android.intent.action.PACKAGE_REMOVED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string/jumbo v1, "android.intent.action.PACKAGE_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string/jumbo v1, "android.intent.action.PACKAGE_REPLACED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string/jumbo v1, "package"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/android/settingslib/drawer/a;->czd:Lcom/android/settingslib/drawer/d;

    invoke-virtual {p0, v1, v0}, Lcom/android/settingslib/drawer/a;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    new-instance v0, Lcom/android/settingslib/drawer/c;

    invoke-direct {v0, p0}, Lcom/android/settingslib/drawer/c;-><init>(Lcom/android/settingslib/drawer/a;)V

    sget-object v1, Lcom/android/settingslib/drawer/a;->czg:Ljava/util/concurrent/ExecutorService;

    new-array v2, v3, [Ljava/lang/Void;

    invoke-virtual {v0, v1, v2}, Lcom/android/settingslib/drawer/c;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    invoke-virtual {p0}, Lcom/android/settingslib/drawer/a;->getIntent()Landroid/content/Intent;

    move-result-object v0

    if-eqz v0, :cond_0

    const-string/jumbo v1, "show_drawer_menu"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/android/settingslib/drawer/a;->cdQ()V

    :cond_0
    return-void
.end method

.method public setContentHeaderView(Landroid/view/View;)V
    .locals 2

    iget-object v0, p0, Lcom/android/settingslib/drawer/a;->cze:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->removeAllViews()V

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/android/settingslib/drawer/a;->cze:Landroid/widget/FrameLayout;

    invoke-virtual {v0, p1}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    iget-object v0, p0, Lcom/android/settingslib/drawer/a;->cze:Landroid/widget/FrameLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setVisibility(I)V

    :cond_0
    return-void
.end method

.method public setContentView(I)V
    .locals 2

    sget v0, Lcom/android/settingslib/g;->cKT:I

    invoke-virtual {p0, v0}, Lcom/android/settingslib/drawer/a;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViews()V

    :cond_0
    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    invoke-virtual {v1, p1, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    return-void
.end method

.method public setContentView(Landroid/view/View;)V
    .locals 1

    sget v0, Lcom/android/settingslib/g;->cKT:I

    invoke-virtual {p0, v0}, Lcom/android/settingslib/drawer/a;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    invoke-virtual {v0, p1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    return-void
.end method

.method public setContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
    .locals 1

    sget v0, Lcom/android/settingslib/g;->cKT:I

    invoke-virtual {p0, v0}, Lcom/android/settingslib/drawer/a;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    invoke-virtual {v0, p1, p2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    return-void
.end method
