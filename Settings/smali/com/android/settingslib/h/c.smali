.class Lcom/android/settingslib/h/c;
.super Ljava/lang/Object;
.source "DreamBackend.java"

# interfaces
.implements Ljava/util/Comparator;


# instance fields
.field private final cJI:Landroid/content/ComponentName;


# direct methods
.method public constructor <init>(Landroid/content/ComponentName;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/android/settingslib/h/c;->cJI:Landroid/content/ComponentName;

    return-void
.end method

.method private cpi(Lcom/android/settingslib/h/b;)Ljava/lang/String;
    .locals 3

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v0, p1, Lcom/android/settingslib/h/b;->cJH:Landroid/content/ComponentName;

    iget-object v2, p0, Lcom/android/settingslib/h/c;->cJI:Landroid/content/ComponentName;

    invoke-virtual {v0, v2}, Landroid/content/ComponentName;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x30

    :goto_0
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    iget-object v0, p1, Lcom/android/settingslib/h/b;->cJE:Ljava/lang/CharSequence;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    const/16 v0, 0x31

    goto :goto_0
.end method


# virtual methods
.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1

    check-cast p1, Lcom/android/settingslib/h/b;

    check-cast p2, Lcom/android/settingslib/h/b;

    invoke-virtual {p0, p1, p2}, Lcom/android/settingslib/h/c;->cph(Lcom/android/settingslib/h/b;Lcom/android/settingslib/h/b;)I

    move-result v0

    return v0
.end method

.method public cph(Lcom/android/settingslib/h/b;Lcom/android/settingslib/h/b;)I
    .locals 2

    invoke-direct {p0, p1}, Lcom/android/settingslib/h/c;->cpi(Lcom/android/settingslib/h/b;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p2}, Lcom/android/settingslib/h/c;->cpi(Lcom/android/settingslib/h/b;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    return v0
.end method
