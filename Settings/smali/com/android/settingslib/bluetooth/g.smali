.class public final Lcom/android/settingslib/bluetooth/g;
.super Ljava/lang/Object;
.source "BluetoothDeviceFilter.java"


# static fields
.field private static final cEj:[Lcom/android/settingslib/bluetooth/h;

.field public static final cEk:Lcom/android/settingslib/bluetooth/h;

.field public static final cEl:Lcom/android/settingslib/bluetooth/h;

.field public static final cEm:Lcom/android/settingslib/bluetooth/h;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/4 v3, 0x0

    new-instance v0, Lcom/android/settingslib/bluetooth/i;

    invoke-direct {v0, v3}, Lcom/android/settingslib/bluetooth/i;-><init>(Lcom/android/settingslib/bluetooth/i;)V

    sput-object v0, Lcom/android/settingslib/bluetooth/g;->cEk:Lcom/android/settingslib/bluetooth/h;

    new-instance v0, Lcom/android/settingslib/bluetooth/j;

    invoke-direct {v0, v3}, Lcom/android/settingslib/bluetooth/j;-><init>(Lcom/android/settingslib/bluetooth/j;)V

    sput-object v0, Lcom/android/settingslib/bluetooth/g;->cEl:Lcom/android/settingslib/bluetooth/h;

    new-instance v0, Lcom/android/settingslib/bluetooth/k;

    invoke-direct {v0, v3}, Lcom/android/settingslib/bluetooth/k;-><init>(Lcom/android/settingslib/bluetooth/k;)V

    sput-object v0, Lcom/android/settingslib/bluetooth/g;->cEm:Lcom/android/settingslib/bluetooth/h;

    const/4 v0, 0x5

    new-array v0, v0, [Lcom/android/settingslib/bluetooth/h;

    sget-object v1, Lcom/android/settingslib/bluetooth/g;->cEk:Lcom/android/settingslib/bluetooth/h;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    new-instance v1, Lcom/android/settingslib/bluetooth/m;

    invoke-direct {v1, v3}, Lcom/android/settingslib/bluetooth/m;-><init>(Lcom/android/settingslib/bluetooth/m;)V

    const/4 v2, 0x1

    aput-object v1, v0, v2

    new-instance v1, Lcom/android/settingslib/bluetooth/n;

    invoke-direct {v1, v3}, Lcom/android/settingslib/bluetooth/n;-><init>(Lcom/android/settingslib/bluetooth/n;)V

    const/4 v2, 0x2

    aput-object v1, v0, v2

    new-instance v1, Lcom/android/settingslib/bluetooth/o;

    invoke-direct {v1, v3}, Lcom/android/settingslib/bluetooth/o;-><init>(Lcom/android/settingslib/bluetooth/o;)V

    const/4 v2, 0x3

    aput-object v1, v0, v2

    new-instance v1, Lcom/android/settingslib/bluetooth/p;

    invoke-direct {v1, v3}, Lcom/android/settingslib/bluetooth/p;-><init>(Lcom/android/settingslib/bluetooth/p;)V

    const/4 v2, 0x4

    aput-object v1, v0, v2

    sput-object v0, Lcom/android/settingslib/bluetooth/g;->cEj:[Lcom/android/settingslib/bluetooth/h;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static cki(I)Lcom/android/settingslib/bluetooth/h;
    .locals 3

    if-ltz p0, :cond_0

    sget-object v0, Lcom/android/settingslib/bluetooth/g;->cEj:[Lcom/android/settingslib/bluetooth/h;

    array-length v0, v0

    if-ge p0, v0, :cond_0

    sget-object v0, Lcom/android/settingslib/bluetooth/g;->cEj:[Lcom/android/settingslib/bluetooth/h;

    aget-object v0, v0, p0

    return-object v0

    :cond_0
    const-string/jumbo v0, "BluetoothDeviceFilter"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Invalid filter type "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " for device picker"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v0, Lcom/android/settingslib/bluetooth/g;->cEk:Lcom/android/settingslib/bluetooth/h;

    return-object v0
.end method
