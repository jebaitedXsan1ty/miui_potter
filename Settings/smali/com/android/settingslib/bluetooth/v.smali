.class Lcom/android/settingslib/bluetooth/v;
.super Ljava/lang/Object;
.source "LocalBluetoothProfileManager.java"

# interfaces
.implements Lcom/android/settingslib/bluetooth/I;


# instance fields
.field final synthetic cEF:Lcom/android/settingslib/bluetooth/e;

.field final cEG:Lcom/android/settingslib/bluetooth/f;


# direct methods
.method constructor <init>(Lcom/android/settingslib/bluetooth/e;Lcom/android/settingslib/bluetooth/f;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settingslib/bluetooth/v;->cEF:Lcom/android/settingslib/bluetooth/e;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/android/settingslib/bluetooth/v;->cEG:Lcom/android/settingslib/bluetooth/f;

    return-void
.end method


# virtual methods
.method public ckW(Landroid/content/Context;Landroid/content/Intent;Landroid/bluetooth/BluetoothDevice;)V
    .locals 5

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/android/settingslib/bluetooth/v;->cEF:Lcom/android/settingslib/bluetooth/e;

    invoke-static {v0}, Lcom/android/settingslib/bluetooth/e;->cke(Lcom/android/settingslib/bluetooth/e;)Lcom/android/settingslib/bluetooth/t;

    move-result-object v0

    invoke-virtual {v0, p3}, Lcom/android/settingslib/bluetooth/t;->ckL(Landroid/bluetooth/BluetoothDevice;)Lcom/android/settingslib/bluetooth/b;

    move-result-object v0

    if-nez v0, :cond_0

    const-string/jumbo v0, "LocalBluetoothProfileManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "StateChangedHandler found new device: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/settingslib/bluetooth/v;->cEF:Lcom/android/settingslib/bluetooth/e;

    invoke-static {v0}, Lcom/android/settingslib/bluetooth/e;->cke(Lcom/android/settingslib/bluetooth/e;)Lcom/android/settingslib/bluetooth/t;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settingslib/bluetooth/v;->cEF:Lcom/android/settingslib/bluetooth/e;

    invoke-static {v1}, Lcom/android/settingslib/bluetooth/e;->cjT(Lcom/android/settingslib/bluetooth/e;)Lcom/android/settingslib/bluetooth/d;

    move-result-object v1

    iget-object v2, p0, Lcom/android/settingslib/bluetooth/v;->cEF:Lcom/android/settingslib/bluetooth/e;

    invoke-virtual {v0, v1, v2, p3}, Lcom/android/settingslib/bluetooth/t;->ckP(Lcom/android/settingslib/bluetooth/d;Lcom/android/settingslib/bluetooth/e;Landroid/bluetooth/BluetoothDevice;)Lcom/android/settingslib/bluetooth/b;

    move-result-object v0

    :cond_0
    const-string/jumbo v1, "android.bluetooth.profile.extra.STATE"

    invoke-virtual {p2, v1, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    const-string/jumbo v2, "android.bluetooth.profile.extra.PREVIOUS_STATE"

    invoke-virtual {p2, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    if-nez v1, :cond_1

    const/4 v3, 0x1

    if-ne v2, v3, :cond_1

    const-string/jumbo v2, "LocalBluetoothProfileManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "Failed to connect "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/android/settingslib/bluetooth/v;->cEG:Lcom/android/settingslib/bluetooth/f;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, " device"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    iget-object v2, p0, Lcom/android/settingslib/bluetooth/v;->cEG:Lcom/android/settingslib/bluetooth/f;

    invoke-virtual {v0, v2, v1}, Lcom/android/settingslib/bluetooth/b;->ciG(Lcom/android/settingslib/bluetooth/f;I)V

    invoke-virtual {v0}, Lcom/android/settingslib/bluetooth/b;->cjn()V

    return-void
.end method
