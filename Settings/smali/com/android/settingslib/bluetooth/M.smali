.class Lcom/android/settingslib/bluetooth/M;
.super Ljava/lang/Object;
.source "BluetoothEventManager.java"

# interfaces
.implements Lcom/android/settingslib/bluetooth/I;


# instance fields
.field private final cFX:Z

.field final synthetic cFY:Lcom/android/settingslib/bluetooth/s;


# direct methods
.method constructor <init>(Lcom/android/settingslib/bluetooth/s;Z)V
    .locals 0

    iput-object p1, p0, Lcom/android/settingslib/bluetooth/M;->cFY:Lcom/android/settingslib/bluetooth/s;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean p2, p0, Lcom/android/settingslib/bluetooth/M;->cFX:Z

    return-void
.end method


# virtual methods
.method public ckW(Landroid/content/Context;Landroid/content/Intent;Landroid/bluetooth/BluetoothDevice;)V
    .locals 4

    iget-object v0, p0, Lcom/android/settingslib/bluetooth/M;->cFY:Lcom/android/settingslib/bluetooth/s;

    invoke-static {v0}, Lcom/android/settingslib/bluetooth/s;->ckF(Lcom/android/settingslib/bluetooth/s;)Ljava/util/Collection;

    move-result-object v1

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/android/settingslib/bluetooth/M;->cFY:Lcom/android/settingslib/bluetooth/s;

    invoke-static {v0}, Lcom/android/settingslib/bluetooth/s;->ckF(Lcom/android/settingslib/bluetooth/s;)Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settingslib/bluetooth/a;

    iget-boolean v3, p0, Lcom/android/settingslib/bluetooth/M;->cFX:Z

    invoke-interface {v0, v3}, Lcom/android/settingslib/bluetooth/a;->aqv(Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    :cond_0
    monitor-exit v1

    iget-object v0, p0, Lcom/android/settingslib/bluetooth/M;->cFY:Lcom/android/settingslib/bluetooth/s;

    invoke-static {v0}, Lcom/android/settingslib/bluetooth/s;->ckv(Lcom/android/settingslib/bluetooth/s;)Lcom/android/settingslib/bluetooth/t;

    move-result-object v0

    iget-boolean v1, p0, Lcom/android/settingslib/bluetooth/M;->cFX:Z

    invoke-virtual {v0, v1}, Lcom/android/settingslib/bluetooth/t;->ckS(Z)V

    return-void
.end method
