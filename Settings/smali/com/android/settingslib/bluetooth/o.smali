.class final Lcom/android/settingslib/bluetooth/o;
.super Lcom/android/settingslib/bluetooth/l;
.source "BluetoothDeviceFilter.java"


# direct methods
.method private constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/android/settingslib/bluetooth/l;-><init>(Lcom/android/settingslib/bluetooth/l;)V

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/settingslib/bluetooth/o;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/settingslib/bluetooth/o;-><init>()V

    return-void
.end method


# virtual methods
.method ckj([Landroid/os/ParcelUuid;Landroid/bluetooth/BluetoothClass;)Z
    .locals 1

    if-eqz p1, :cond_0

    sget-object v0, Landroid/bluetooth/BluetoothUuid;->PANU:Landroid/os/ParcelUuid;

    invoke-static {p1, v0}, Landroid/bluetooth/BluetoothUuid;->isUuidPresent([Landroid/os/ParcelUuid;Landroid/os/ParcelUuid;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    return v0

    :cond_0
    if-eqz p2, :cond_1

    const/4 v0, 0x4

    invoke-virtual {p2, v0}, Landroid/bluetooth/BluetoothClass;->doesClassMatch(I)Z

    move-result v0

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
