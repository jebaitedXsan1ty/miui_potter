.class public final Lcom/android/settingslib/bluetooth/x;
.super Ljava/lang/Object;
.source "MapClientProfile.java"

# interfaces
.implements Lcom/android/settingslib/bluetooth/f;


# static fields
.field private static cEI:Z

.field static final cEM:[Landroid/os/ParcelUuid;


# instance fields
.field private cEJ:Z

.field private final cEK:Lcom/android/settingslib/bluetooth/d;

.field private final cEL:Lcom/android/settingslib/bluetooth/e;

.field private final cEN:Lcom/android/settingslib/bluetooth/t;

.field private cEO:Landroid/bluetooth/BluetoothMapClient;


# direct methods
.method static synthetic -get0()Z
    .locals 1

    sget-boolean v0, Lcom/android/settingslib/bluetooth/x;->cEI:Z

    return v0
.end method

.method static constructor <clinit>()V
    .locals 3

    const/4 v2, 0x0

    sput-boolean v2, Lcom/android/settingslib/bluetooth/x;->cEI:Z

    const/4 v0, 0x3

    new-array v0, v0, [Landroid/os/ParcelUuid;

    sget-object v1, Landroid/bluetooth/BluetoothUuid;->MAP:Landroid/os/ParcelUuid;

    aput-object v1, v0, v2

    sget-object v1, Landroid/bluetooth/BluetoothUuid;->MNS:Landroid/os/ParcelUuid;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    sget-object v1, Landroid/bluetooth/BluetoothUuid;->MAS:Landroid/os/ParcelUuid;

    const/4 v2, 0x2

    aput-object v1, v0, v2

    sput-object v0, Lcom/android/settingslib/bluetooth/x;->cEM:[Landroid/os/ParcelUuid;

    return-void
.end method

.method constructor <init>(Landroid/content/Context;Lcom/android/settingslib/bluetooth/d;Lcom/android/settingslib/bluetooth/t;Lcom/android/settingslib/bluetooth/e;)V
    .locals 3

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/android/settingslib/bluetooth/x;->cEK:Lcom/android/settingslib/bluetooth/d;

    iput-object p3, p0, Lcom/android/settingslib/bluetooth/x;->cEN:Lcom/android/settingslib/bluetooth/t;

    iput-object p4, p0, Lcom/android/settingslib/bluetooth/x;->cEL:Lcom/android/settingslib/bluetooth/e;

    iget-object v0, p0, Lcom/android/settingslib/bluetooth/x;->cEK:Lcom/android/settingslib/bluetooth/d;

    new-instance v1, Lcom/android/settingslib/bluetooth/ad;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/android/settingslib/bluetooth/ad;-><init>(Lcom/android/settingslib/bluetooth/x;Lcom/android/settingslib/bluetooth/ad;)V

    const/16 v2, 0x12

    invoke-virtual {v0, p1, v1, v2}, Lcom/android/settingslib/bluetooth/d;->cjH(Landroid/content/Context;Landroid/bluetooth/BluetoothProfile$ServiceListener;I)V

    return-void
.end method

.method static synthetic ckY(Lcom/android/settingslib/bluetooth/x;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/android/settingslib/bluetooth/x;->cEJ:Z

    return p1
.end method

.method static synthetic ckZ(Lcom/android/settingslib/bluetooth/x;Landroid/bluetooth/BluetoothMapClient;)Landroid/bluetooth/BluetoothMapClient;
    .locals 0

    iput-object p1, p0, Lcom/android/settingslib/bluetooth/x;->cEO:Landroid/bluetooth/BluetoothMapClient;

    return-object p1
.end method

.method static synthetic cla(Lcom/android/settingslib/bluetooth/x;)Landroid/bluetooth/BluetoothMapClient;
    .locals 1

    iget-object v0, p0, Lcom/android/settingslib/bluetooth/x;->cEO:Landroid/bluetooth/BluetoothMapClient;

    return-object v0
.end method

.method static synthetic clb(Lcom/android/settingslib/bluetooth/x;)Lcom/android/settingslib/bluetooth/t;
    .locals 1

    iget-object v0, p0, Lcom/android/settingslib/bluetooth/x;->cEN:Lcom/android/settingslib/bluetooth/t;

    return-object v0
.end method

.method static synthetic clc(Lcom/android/settingslib/bluetooth/x;)Lcom/android/settingslib/bluetooth/d;
    .locals 1

    iget-object v0, p0, Lcom/android/settingslib/bluetooth/x;->cEK:Lcom/android/settingslib/bluetooth/d;

    return-object v0
.end method

.method static synthetic cld(Lcom/android/settingslib/bluetooth/x;)Lcom/android/settingslib/bluetooth/e;
    .locals 1

    iget-object v0, p0, Lcom/android/settingslib/bluetooth/x;->cEL:Lcom/android/settingslib/bluetooth/e;

    return-object v0
.end method


# virtual methods
.method public asi(Landroid/bluetooth/BluetoothDevice;)Z
    .locals 2

    iget-object v0, p0, Lcom/android/settingslib/bluetooth/x;->cEO:Landroid/bluetooth/BluetoothMapClient;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    return v0

    :cond_0
    invoke-virtual {p0}, Lcom/android/settingslib/bluetooth/x;->ckX()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string/jumbo v0, "MapClientProfile"

    const-string/jumbo v1, "Ignoring Connect"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x1

    return v0

    :cond_1
    iget-object v0, p0, Lcom/android/settingslib/bluetooth/x;->cEO:Landroid/bluetooth/BluetoothMapClient;

    invoke-virtual {v0, p1}, Landroid/bluetooth/BluetoothMapClient;->connect(Landroid/bluetooth/BluetoothDevice;)Z

    move-result v0

    return v0
.end method

.method public asj(Landroid/bluetooth/BluetoothDevice;)Z
    .locals 2

    const/16 v1, 0x64

    iget-object v0, p0, Lcom/android/settingslib/bluetooth/x;->cEO:Landroid/bluetooth/BluetoothMapClient;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    return v0

    :cond_0
    iget-object v0, p0, Lcom/android/settingslib/bluetooth/x;->cEO:Landroid/bluetooth/BluetoothMapClient;

    invoke-virtual {v0, p1}, Landroid/bluetooth/BluetoothMapClient;->getPriority(Landroid/bluetooth/BluetoothDevice;)I

    move-result v0

    if-le v0, v1, :cond_1

    iget-object v0, p0, Lcom/android/settingslib/bluetooth/x;->cEO:Landroid/bluetooth/BluetoothMapClient;

    invoke-virtual {v0, p1, v1}, Landroid/bluetooth/BluetoothMapClient;->setPriority(Landroid/bluetooth/BluetoothDevice;I)Z

    :cond_1
    iget-object v0, p0, Lcom/android/settingslib/bluetooth/x;->cEO:Landroid/bluetooth/BluetoothMapClient;

    invoke-virtual {v0, p1}, Landroid/bluetooth/BluetoothMapClient;->disconnect(Landroid/bluetooth/BluetoothDevice;)Z

    move-result v0

    return v0
.end method

.method public ask(Landroid/bluetooth/BluetoothDevice;)I
    .locals 1

    iget-object v0, p0, Lcom/android/settingslib/bluetooth/x;->cEO:Landroid/bluetooth/BluetoothMapClient;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    return v0

    :cond_0
    iget-object v0, p0, Lcom/android/settingslib/bluetooth/x;->cEO:Landroid/bluetooth/BluetoothMapClient;

    invoke-virtual {v0, p1}, Landroid/bluetooth/BluetoothMapClient;->getConnectionState(Landroid/bluetooth/BluetoothDevice;)I

    move-result v0

    return v0
.end method

.method public asl(Landroid/bluetooth/BluetoothClass;)I
    .locals 1

    sget v0, Lcom/android/settingslib/e;->cKD:I

    return v0
.end method

.method public asm(Landroid/bluetooth/BluetoothDevice;)I
    .locals 1

    sget v0, Lcom/android/settingslib/i;->cMd:I

    return v0
.end method

.method public asn()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public aso(Landroid/bluetooth/BluetoothDevice;)I
    .locals 1

    invoke-virtual {p0, p1}, Lcom/android/settingslib/bluetooth/x;->ask(Landroid/bluetooth/BluetoothDevice;)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :pswitch_0
    invoke-static {v0}, Lcom/android/settingslib/bluetooth/J;->cmk(I)I

    move-result v0

    return v0

    :pswitch_1
    sget v0, Lcom/android/settingslib/i;->cLR:I

    return v0

    :pswitch_2
    sget v0, Lcom/android/settingslib/i;->cLQ:I

    return v0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public asp()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public asq()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public asr(Landroid/bluetooth/BluetoothDevice;)Z
    .locals 2

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/android/settingslib/bluetooth/x;->cEO:Landroid/bluetooth/BluetoothMapClient;

    if-nez v1, :cond_0

    return v0

    :cond_0
    iget-object v1, p0, Lcom/android/settingslib/bluetooth/x;->cEO:Landroid/bluetooth/BluetoothMapClient;

    invoke-virtual {v1, p1}, Landroid/bluetooth/BluetoothMapClient;->getPriority(Landroid/bluetooth/BluetoothDevice;)I

    move-result v1

    if-lez v1, :cond_1

    const/4 v0, 0x1

    :cond_1
    return v0
.end method

.method public ass()Z
    .locals 3

    sget-boolean v0, Lcom/android/settingslib/bluetooth/x;->cEI:Z

    if-eqz v0, :cond_0

    const-string/jumbo v0, "MapClientProfile"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "isProfileReady(): "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/android/settingslib/bluetooth/x;->cEJ:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-boolean v0, p0, Lcom/android/settingslib/bluetooth/x;->cEJ:Z

    return v0
.end method

.method public ast(Landroid/bluetooth/BluetoothDevice;Z)V
    .locals 2

    const/16 v1, 0x64

    iget-object v0, p0, Lcom/android/settingslib/bluetooth/x;->cEO:Landroid/bluetooth/BluetoothMapClient;

    if-nez v0, :cond_0

    return-void

    :cond_0
    if-eqz p2, :cond_2

    iget-object v0, p0, Lcom/android/settingslib/bluetooth/x;->cEO:Landroid/bluetooth/BluetoothMapClient;

    invoke-virtual {v0, p1}, Landroid/bluetooth/BluetoothMapClient;->getPriority(Landroid/bluetooth/BluetoothDevice;)I

    move-result v0

    if-ge v0, v1, :cond_1

    iget-object v0, p0, Lcom/android/settingslib/bluetooth/x;->cEO:Landroid/bluetooth/BluetoothMapClient;

    invoke-virtual {v0, p1, v1}, Landroid/bluetooth/BluetoothMapClient;->setPriority(Landroid/bluetooth/BluetoothDevice;I)Z

    :cond_1
    :goto_0
    return-void

    :cond_2
    iget-object v0, p0, Lcom/android/settingslib/bluetooth/x;->cEO:Landroid/bluetooth/BluetoothMapClient;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Landroid/bluetooth/BluetoothMapClient;->setPriority(Landroid/bluetooth/BluetoothDevice;I)Z

    goto :goto_0
.end method

.method public ckX()Ljava/util/List;
    .locals 4

    iget-object v0, p0, Lcom/android/settingslib/bluetooth/x;->cEO:Landroid/bluetooth/BluetoothMapClient;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/ArrayList;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/android/settingslib/bluetooth/x;->cEO:Landroid/bluetooth/BluetoothMapClient;

    const/4 v1, 0x2

    const/4 v2, 0x1

    const/4 v3, 0x3

    filled-new-array {v1, v2, v3}, [I

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/bluetooth/BluetoothMapClient;->getDevicesMatchingConnectionStates([I)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method protected finalize()V
    .locals 3

    sget-boolean v0, Lcom/android/settingslib/bluetooth/x;->cEI:Z

    if-eqz v0, :cond_0

    const-string/jumbo v0, "MapClientProfile"

    const-string/jumbo v1, "finalize()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v0, p0, Lcom/android/settingslib/bluetooth/x;->cEO:Landroid/bluetooth/BluetoothMapClient;

    if-eqz v0, :cond_1

    :try_start_0
    invoke-static {}, Landroid/bluetooth/BluetoothAdapter;->getDefaultAdapter()Landroid/bluetooth/BluetoothAdapter;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settingslib/bluetooth/x;->cEO:Landroid/bluetooth/BluetoothMapClient;

    const/16 v2, 0x12

    invoke-virtual {v0, v2, v1}, Landroid/bluetooth/BluetoothAdapter;->closeProfileProxy(ILandroid/bluetooth/BluetoothProfile;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/settingslib/bluetooth/x;->cEO:Landroid/bluetooth/BluetoothMapClient;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    :cond_1
    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string/jumbo v1, "MapClientProfile"

    const-string/jumbo v2, "Error cleaning up MAP Client proxy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    const-string/jumbo v0, "MAP Client"

    return-object v0
.end method
