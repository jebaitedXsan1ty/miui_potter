.class public Lcom/android/settingslib/bluetooth/b;
.super Ljava/lang/Object;
.source "CachedBluetoothDevice.java"

# interfaces
.implements Ljava/lang/Comparable;


# instance fields
.field private cDA:Z

.field private final cDB:Ljava/util/List;

.field private cDC:J

.field private final cDD:Lcom/android/settingslib/bluetooth/d;

.field private final cDE:Ljava/util/Collection;

.field private cDF:I

.field private cDG:Ljava/util/HashMap;

.field private cDH:S

.field private final cDI:Landroid/bluetooth/BluetoothDevice;

.field private final cDJ:Lcom/android/settingslib/bluetooth/e;

.field private cDK:Z

.field private cDL:Z

.field private cDx:Landroid/bluetooth/BluetoothClass;

.field private final cDy:Ljava/util/List;

.field private cDz:Ljava/lang/String;

.field private final mContext:Landroid/content/Context;


# direct methods
.method constructor <init>(Landroid/content/Context;Lcom/android/settingslib/bluetooth/d;Lcom/android/settingslib/bluetooth/e;Landroid/bluetooth/BluetoothDevice;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/settingslib/bluetooth/b;->cDB:Ljava/util/List;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/settingslib/bluetooth/b;->cDy:Ljava/util/List;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/settingslib/bluetooth/b;->cDE:Ljava/util/Collection;

    iput-object p1, p0, Lcom/android/settingslib/bluetooth/b;->mContext:Landroid/content/Context;

    iput-object p2, p0, Lcom/android/settingslib/bluetooth/b;->cDD:Lcom/android/settingslib/bluetooth/d;

    iput-object p3, p0, Lcom/android/settingslib/bluetooth/b;->cDJ:Lcom/android/settingslib/bluetooth/e;

    iput-object p4, p0, Lcom/android/settingslib/bluetooth/b;->cDI:Landroid/bluetooth/BluetoothDevice;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/android/settingslib/bluetooth/b;->cDG:Ljava/util/HashMap;

    invoke-direct {p0}, Lcom/android/settingslib/bluetooth/b;->ciY()V

    return-void
.end method

.method private ciL()V
    .locals 1

    iget-object v0, p0, Lcom/android/settingslib/bluetooth/b;->cDI:Landroid/bluetooth/BluetoothDevice;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothDevice;->getBluetoothClass()Landroid/bluetooth/BluetoothClass;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settingslib/bluetooth/b;->cDx:Landroid/bluetooth/BluetoothClass;

    return-void
.end method

.method private ciS()V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/android/settingslib/bluetooth/b;->mContext:Landroid/content/Context;

    const-string/jumbo v1, "bluetooth_message_reject"

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    iget v1, p0, Lcom/android/settingslib/bluetooth/b;->cDF:I

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/android/settingslib/bluetooth/b;->cDI:Landroid/bluetooth/BluetoothDevice;

    invoke-virtual {v1}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    :goto_0
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    return-void

    :cond_0
    iget-object v1, p0, Lcom/android/settingslib/bluetooth/b;->cDI:Landroid/bluetooth/BluetoothDevice;

    invoke-virtual {v1}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v1

    iget v2, p0, Lcom/android/settingslib/bluetooth/b;->cDF:I

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    goto :goto_0
.end method

.method private ciY()V
    .locals 1

    invoke-direct {p0}, Lcom/android/settingslib/bluetooth/b;->cjj()V

    invoke-direct {p0}, Lcom/android/settingslib/bluetooth/b;->ciL()V

    invoke-direct {p0}, Lcom/android/settingslib/bluetooth/b;->cjv()Z

    invoke-direct {p0}, Lcom/android/settingslib/bluetooth/b;->cjm()V

    invoke-direct {p0}, Lcom/android/settingslib/bluetooth/b;->cjo()V

    invoke-direct {p0}, Lcom/android/settingslib/bluetooth/b;->cji()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/settingslib/bluetooth/b;->cDL:Z

    invoke-direct {p0}, Lcom/android/settingslib/bluetooth/b;->cjb()V

    return-void
.end method

.method private civ(Z)V
    .locals 4

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/android/settingslib/bluetooth/b;->cDB:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    const-string/jumbo v0, "CachedBluetoothDevice"

    const-string/jumbo v1, "No profiles. Maybe we will connect later"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :cond_0
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/android/settingslib/bluetooth/b;->cDA:Z

    iget-object v1, p0, Lcom/android/settingslib/bluetooth/b;->cDB:Ljava/util/List;

    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v1, v0

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settingslib/bluetooth/f;

    if-eqz p1, :cond_1

    invoke-interface {v0}, Lcom/android/settingslib/bluetooth/f;->asq()Z

    move-result v2

    :goto_1
    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/android/settingslib/bluetooth/b;->cDI:Landroid/bluetooth/BluetoothDevice;

    invoke-interface {v0, v2}, Lcom/android/settingslib/bluetooth/f;->asr(Landroid/bluetooth/BluetoothDevice;)Z

    move-result v2

    if-eqz v2, :cond_4

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {p0, v0}, Lcom/android/settingslib/bluetooth/b;->ciI(Lcom/android/settingslib/bluetooth/f;)V

    move v0, v1

    :goto_2
    move v1, v0

    goto :goto_0

    :cond_1
    invoke-interface {v0}, Lcom/android/settingslib/bluetooth/f;->asp()Z

    move-result v2

    goto :goto_1

    :cond_2
    if-nez v1, :cond_3

    invoke-direct {p0}, Lcom/android/settingslib/bluetooth/b;->cjx()V

    :cond_3
    return-void

    :cond_4
    move v0, v1

    goto :goto_2
.end method

.method private cjb()V
    .locals 3

    iget-object v1, p0, Lcom/android/settingslib/bluetooth/b;->cDE:Ljava/util/Collection;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/android/settingslib/bluetooth/b;->cDE:Ljava/util/Collection;

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settingslib/bluetooth/c;

    invoke-interface {v0}, Lcom/android/settingslib/bluetooth/c;->arT()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    :cond_0
    monitor-exit v1

    return-void
.end method

.method private cjg(Lcom/android/settingslib/bluetooth/f;)Ljava/lang/String;
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "Address:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/settingslib/bluetooth/b;->cDI:Landroid/bluetooth/BluetoothDevice;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    if-eqz p1, :cond_0

    const-string/jumbo v1, " Profile:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_0
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private cjh()V
    .locals 2

    iget-object v0, p0, Lcom/android/settingslib/bluetooth/b;->cDI:Landroid/bluetooth/BluetoothDevice;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothDevice;->getBondState()I

    move-result v0

    const/16 v1, 0xc

    if-eq v0, v1, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/settingslib/bluetooth/b;->cDI:Landroid/bluetooth/BluetoothDevice;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothDevice;->getUuids()[Landroid/os/ParcelUuid;

    move-result-object v0

    sget-object v1, Lcom/android/settingslib/bluetooth/F;->cFE:[Landroid/os/ParcelUuid;

    invoke-static {v0, v1}, Landroid/bluetooth/BluetoothUuid;->containsAnyUuid([Landroid/os/ParcelUuid;[Landroid/os/ParcelUuid;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lcom/android/settingslib/bluetooth/b;->cjf()I

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/android/settingslib/bluetooth/b;->cDI:Landroid/bluetooth/BluetoothDevice;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothDevice;->getBluetoothClass()Landroid/bluetooth/BluetoothClass;

    move-result-object v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/android/settingslib/bluetooth/b;->cDI:Landroid/bluetooth/BluetoothDevice;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothDevice;->getBluetoothClass()Landroid/bluetooth/BluetoothClass;

    move-result-object v0

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothClass;->getDeviceClass()I

    move-result v0

    const/16 v1, 0x408

    if-eq v0, v1, :cond_1

    iget-object v0, p0, Lcom/android/settingslib/bluetooth/b;->cDI:Landroid/bluetooth/BluetoothDevice;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothDevice;->getBluetoothClass()Landroid/bluetooth/BluetoothClass;

    move-result-object v0

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothClass;->getDeviceClass()I

    move-result v0

    const/16 v1, 0x404

    if-ne v0, v1, :cond_3

    :cond_1
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/android/settingslib/bluetooth/b;->ciO(I)V

    :cond_2
    :goto_0
    return-void

    :cond_3
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/android/settingslib/bluetooth/b;->ciO(I)V

    goto :goto_0
.end method

.method private cji()V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/android/settingslib/bluetooth/b;->mContext:Landroid/content/Context;

    const-string/jumbo v1, "bluetooth_message_reject"

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settingslib/bluetooth/b;->cDI:Landroid/bluetooth/BluetoothDevice;

    invoke-virtual {v1}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/android/settingslib/bluetooth/b;->cDF:I

    return-void
.end method

.method private cjj()V
    .locals 1

    iget-object v0, p0, Lcom/android/settingslib/bluetooth/b;->cDI:Landroid/bluetooth/BluetoothDevice;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothDevice;->getAliasName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settingslib/bluetooth/b;->cDz:Ljava/lang/String;

    iget-object v0, p0, Lcom/android/settingslib/bluetooth/b;->cDz:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settingslib/bluetooth/b;->cDI:Landroid/bluetooth/BluetoothDevice;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settingslib/bluetooth/b;->cDz:Ljava/lang/String;

    :cond_0
    return-void
.end method

.method private cjk()Z
    .locals 2

    invoke-virtual {p0}, Lcom/android/settingslib/bluetooth/b;->ciP()I

    move-result v0

    const/16 v1, 0xa

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/android/settingslib/bluetooth/b;->ciK()Z

    const/4 v0, 0x0

    return v0

    :cond_0
    const/4 v0, 0x1

    return v0
.end method

.method private cjm()V
    .locals 5

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/android/settingslib/bluetooth/b;->mContext:Landroid/content/Context;

    const-string/jumbo v1, "bluetooth_phonebook_permission"

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settingslib/bluetooth/b;->cDI:Landroid/bluetooth/BluetoothDevice;

    invoke-virtual {v1}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    return-void

    :cond_0
    iget-object v1, p0, Lcom/android/settingslib/bluetooth/b;->cDI:Landroid/bluetooth/BluetoothDevice;

    invoke-virtual {v1}, Landroid/bluetooth/BluetoothDevice;->getPhonebookAccessPermission()I

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/android/settingslib/bluetooth/b;->cDI:Landroid/bluetooth/BluetoothDevice;

    invoke-virtual {v1}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v1

    if-ne v1, v3, :cond_2

    iget-object v1, p0, Lcom/android/settingslib/bluetooth/b;->cDI:Landroid/bluetooth/BluetoothDevice;

    invoke-virtual {v1, v3}, Landroid/bluetooth/BluetoothDevice;->setPhonebookAccessPermission(I)Z

    :cond_1
    :goto_0
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settingslib/bluetooth/b;->cDI:Landroid/bluetooth/BluetoothDevice;

    invoke-virtual {v1}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    return-void

    :cond_2
    if-ne v1, v4, :cond_1

    iget-object v1, p0, Lcom/android/settingslib/bluetooth/b;->cDI:Landroid/bluetooth/BluetoothDevice;

    invoke-virtual {v1, v4}, Landroid/bluetooth/BluetoothDevice;->setPhonebookAccessPermission(I)Z

    goto :goto_0
.end method

.method private cjo()V
    .locals 5

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/android/settingslib/bluetooth/b;->mContext:Landroid/content/Context;

    const-string/jumbo v1, "bluetooth_message_permission"

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settingslib/bluetooth/b;->cDI:Landroid/bluetooth/BluetoothDevice;

    invoke-virtual {v1}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    return-void

    :cond_0
    iget-object v1, p0, Lcom/android/settingslib/bluetooth/b;->cDI:Landroid/bluetooth/BluetoothDevice;

    invoke-virtual {v1}, Landroid/bluetooth/BluetoothDevice;->getMessageAccessPermission()I

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/android/settingslib/bluetooth/b;->cDI:Landroid/bluetooth/BluetoothDevice;

    invoke-virtual {v1}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v1

    if-ne v1, v3, :cond_2

    iget-object v1, p0, Lcom/android/settingslib/bluetooth/b;->cDI:Landroid/bluetooth/BluetoothDevice;

    invoke-virtual {v1, v3}, Landroid/bluetooth/BluetoothDevice;->setMessageAccessPermission(I)Z

    :cond_1
    :goto_0
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settingslib/bluetooth/b;->cDI:Landroid/bluetooth/BluetoothDevice;

    invoke-virtual {v1}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    return-void

    :cond_2
    if-ne v1, v4, :cond_1

    iget-object v1, p0, Lcom/android/settingslib/bluetooth/b;->cDI:Landroid/bluetooth/BluetoothDevice;

    invoke-virtual {v1, v4}, Landroid/bluetooth/BluetoothDevice;->setMessageAccessPermission(I)Z

    goto :goto_0
.end method

.method private cjv()Z
    .locals 7

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/android/settingslib/bluetooth/b;->cDI:Landroid/bluetooth/BluetoothDevice;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothDevice;->getUuids()[Landroid/os/ParcelUuid;

    move-result-object v1

    if-nez v1, :cond_0

    return v3

    :cond_0
    iget-object v0, p0, Lcom/android/settingslib/bluetooth/b;->cDD:Lcom/android/settingslib/bluetooth/d;

    invoke-virtual {v0}, Lcom/android/settingslib/bluetooth/d;->cjL()[Landroid/os/ParcelUuid;

    move-result-object v2

    if-nez v2, :cond_1

    return v3

    :cond_1
    invoke-direct {p0}, Lcom/android/settingslib/bluetooth/b;->cjh()V

    iget-object v0, p0, Lcom/android/settingslib/bluetooth/b;->cDJ:Lcom/android/settingslib/bluetooth/e;

    iget-object v3, p0, Lcom/android/settingslib/bluetooth/b;->cDB:Ljava/util/List;

    iget-object v4, p0, Lcom/android/settingslib/bluetooth/b;->cDy:Ljava/util/List;

    iget-boolean v5, p0, Lcom/android/settingslib/bluetooth/b;->cDK:Z

    iget-object v6, p0, Lcom/android/settingslib/bluetooth/b;->cDI:Landroid/bluetooth/BluetoothDevice;

    invoke-virtual/range {v0 .. v6}, Lcom/android/settingslib/bluetooth/e;->cka([Landroid/os/ParcelUuid;[Landroid/os/ParcelUuid;Ljava/util/Collection;Ljava/util/Collection;ZLandroid/bluetooth/BluetoothDevice;)V

    const/4 v0, 0x1

    return v0
.end method

.method private cjx()V
    .locals 4

    const/4 v3, 0x1

    invoke-direct {p0}, Lcom/android/settingslib/bluetooth/b;->cjk()Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    iput-boolean v3, p0, Lcom/android/settingslib/bluetooth/b;->cDA:Z

    iget-object v0, p0, Lcom/android/settingslib/bluetooth/b;->cDB:Ljava/util/List;

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settingslib/bluetooth/f;

    invoke-interface {v0}, Lcom/android/settingslib/bluetooth/f;->asp()Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/android/settingslib/bluetooth/b;->cDI:Landroid/bluetooth/BluetoothDevice;

    invoke-interface {v0, v2, v3}, Lcom/android/settingslib/bluetooth/f;->ast(Landroid/bluetooth/BluetoothDevice;Z)V

    invoke-virtual {p0, v0}, Lcom/android/settingslib/bluetooth/b;->ciI(Lcom/android/settingslib/bluetooth/f;)V

    goto :goto_0

    :cond_2
    return-void
.end method


# virtual methods
.method public ciA()Z
    .locals 4

    const/4 v1, 0x1

    iget-object v0, p0, Lcom/android/settingslib/bluetooth/b;->cDB:Ljava/util/List;

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settingslib/bluetooth/f;

    invoke-virtual {p0, v0}, Lcom/android/settingslib/bluetooth/b;->cje(Lcom/android/settingslib/bluetooth/f;)I

    move-result v0

    if-eq v0, v1, :cond_1

    const/4 v3, 0x3

    if-ne v0, v3, :cond_0

    :cond_1
    return v1

    :cond_2
    invoke-virtual {p0}, Lcom/android/settingslib/bluetooth/b;->ciP()I

    move-result v0

    const/16 v2, 0xb

    if-ne v0, v2, :cond_3

    move v0, v1

    :goto_0
    return v0

    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public ciB(Lcom/android/settingslib/bluetooth/b;)I
    .locals 5

    const/16 v4, 0xc

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {p1}, Lcom/android/settingslib/bluetooth/b;->ciC()Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {p0}, Lcom/android/settingslib/bluetooth/b;->ciC()Z

    move-result v3

    if-eqz v3, :cond_1

    move v3, v1

    :goto_1
    sub-int/2addr v0, v3

    if-eqz v0, :cond_2

    return v0

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    move v3, v2

    goto :goto_1

    :cond_2
    invoke-virtual {p1}, Lcom/android/settingslib/bluetooth/b;->ciP()I

    move-result v0

    if-ne v0, v4, :cond_3

    move v0, v1

    :goto_2
    invoke-virtual {p0}, Lcom/android/settingslib/bluetooth/b;->ciP()I

    move-result v3

    if-ne v3, v4, :cond_4

    move v3, v1

    :goto_3
    sub-int/2addr v0, v3

    if-eqz v0, :cond_5

    return v0

    :cond_3
    move v0, v2

    goto :goto_2

    :cond_4
    move v3, v2

    goto :goto_3

    :cond_5
    iget-boolean v0, p1, Lcom/android/settingslib/bluetooth/b;->cDL:Z

    if-eqz v0, :cond_6

    move v0, v1

    :goto_4
    iget-boolean v3, p0, Lcom/android/settingslib/bluetooth/b;->cDL:Z

    if-eqz v3, :cond_7

    :goto_5
    sub-int/2addr v0, v1

    if-eqz v0, :cond_8

    return v0

    :cond_6
    move v0, v2

    goto :goto_4

    :cond_7
    move v1, v2

    goto :goto_5

    :cond_8
    iget-short v0, p1, Lcom/android/settingslib/bluetooth/b;->cDH:S

    iget-short v1, p0, Lcom/android/settingslib/bluetooth/b;->cDH:S

    sub-int/2addr v0, v1

    if-eqz v0, :cond_9

    return v0

    :cond_9
    iget-object v0, p0, Lcom/android/settingslib/bluetooth/b;->cDz:Ljava/lang/String;

    iget-object v1, p1, Lcom/android/settingslib/bluetooth/b;->cDz:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public ciC()Z
    .locals 3

    iget-object v0, p0, Lcom/android/settingslib/bluetooth/b;->cDB:Ljava/util/List;

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settingslib/bluetooth/f;

    invoke-virtual {p0, v0}, Lcom/android/settingslib/bluetooth/b;->cje(Lcom/android/settingslib/bluetooth/f;)I

    move-result v0

    const/4 v2, 0x2

    if-ne v0, v2, :cond_0

    const/4 v0, 0x1

    return v0

    :cond_1
    const/4 v0, 0x0

    return v0
.end method

.method ciD()V
    .locals 4

    invoke-direct {p0}, Lcom/android/settingslib/bluetooth/b;->cjv()Z

    iget-object v0, p0, Lcom/android/settingslib/bluetooth/b;->cDI:Landroid/bluetooth/BluetoothDevice;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothDevice;->getUuids()[Landroid/os/ParcelUuid;

    move-result-object v2

    const-wide/16 v0, 0x1388

    sget-object v3, Landroid/bluetooth/BluetoothUuid;->Hogp:Landroid/os/ParcelUuid;

    invoke-static {v2, v3}, Landroid/bluetooth/BluetoothUuid;->isUuidPresent([Landroid/os/ParcelUuid;Landroid/os/ParcelUuid;)Z

    move-result v2

    if-eqz v2, :cond_0

    const-wide/16 v0, 0x7530

    :cond_0
    iget-object v2, p0, Lcom/android/settingslib/bluetooth/b;->cDB:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_1

    iget-wide v2, p0, Lcom/android/settingslib/bluetooth/b;->cDC:J

    add-long/2addr v0, v2

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-lez v0, :cond_1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/android/settingslib/bluetooth/b;->civ(Z)V

    :cond_1
    invoke-direct {p0}, Lcom/android/settingslib/bluetooth/b;->cjb()V

    return-void
.end method

.method public ciE()I
    .locals 3

    const/4 v2, 0x2

    const/4 v1, 0x1

    iget-object v0, p0, Lcom/android/settingslib/bluetooth/b;->cDI:Landroid/bluetooth/BluetoothDevice;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothDevice;->getSimAccessPermission()I

    move-result v0

    if-ne v0, v1, :cond_0

    return v1

    :cond_0
    if-ne v0, v2, :cond_1

    return v2

    :cond_1
    const/4 v0, 0x0

    return v0
.end method

.method ciF()V
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/android/settingslib/bluetooth/b;->ciQ(Z)V

    return-void
.end method

.method public ciG(Lcom/android/settingslib/bluetooth/f;I)V
    .locals 5

    const/4 v4, 0x1

    const/4 v3, 0x0

    const-string/jumbo v0, "CachedBluetoothDevice"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "onProfileStateChanged: profile "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " newProfileState "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/settingslib/bluetooth/b;->cDD:Lcom/android/settingslib/bluetooth/d;

    invoke-virtual {v0}, Lcom/android/settingslib/bluetooth/d;->cjJ()I

    move-result v0

    const/16 v1, 0xd

    if-ne v0, v1, :cond_0

    const-string/jumbo v0, "CachedBluetoothDevice"

    const-string/jumbo v1, " BT Turninig Off...Profile conn state change ignored..."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/settingslib/bluetooth/b;->cDG:Ljava/util/HashMap;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const/4 v0, 0x2

    if-ne p2, v0, :cond_3

    instance-of v0, p1, Lcom/android/settingslib/bluetooth/H;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/settingslib/bluetooth/b;->cDI:Landroid/bluetooth/BluetoothDevice;

    invoke-interface {p1, v0, v4}, Lcom/android/settingslib/bluetooth/f;->ast(Landroid/bluetooth/BluetoothDevice;Z)V

    :cond_1
    iget-object v0, p0, Lcom/android/settingslib/bluetooth/b;->cDB:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/android/settingslib/bluetooth/b;->cDy:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/android/settingslib/bluetooth/b;->cDB:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    instance-of v0, p1, Lcom/android/settingslib/bluetooth/y;

    if-eqz v0, :cond_2

    check-cast p1, Lcom/android/settingslib/bluetooth/y;

    iget-object v0, p0, Lcom/android/settingslib/bluetooth/b;->cDI:Landroid/bluetooth/BluetoothDevice;

    invoke-virtual {p1, v0}, Lcom/android/settingslib/bluetooth/y;->clg(Landroid/bluetooth/BluetoothDevice;)Z

    move-result v0

    if-eqz v0, :cond_2

    iput-boolean v4, p0, Lcom/android/settingslib/bluetooth/b;->cDK:Z

    :cond_2
    :goto_0
    return-void

    :cond_3
    instance-of v0, p1, Lcom/android/settingslib/bluetooth/H;

    if-eqz v0, :cond_4

    if-nez p2, :cond_4

    iget-object v0, p0, Lcom/android/settingslib/bluetooth/b;->cDI:Landroid/bluetooth/BluetoothDevice;

    invoke-interface {p1, v0, v3}, Lcom/android/settingslib/bluetooth/f;->ast(Landroid/bluetooth/BluetoothDevice;Z)V

    goto :goto_0

    :cond_4
    iget-boolean v0, p0, Lcom/android/settingslib/bluetooth/b;->cDK:Z

    if-eqz v0, :cond_2

    instance-of v0, p1, Lcom/android/settingslib/bluetooth/y;

    if-eqz v0, :cond_2

    move-object v0, p1

    check-cast v0, Lcom/android/settingslib/bluetooth/y;

    iget-object v1, p0, Lcom/android/settingslib/bluetooth/b;->cDI:Landroid/bluetooth/BluetoothDevice;

    invoke-virtual {v0, v1}, Lcom/android/settingslib/bluetooth/y;->clg(Landroid/bluetooth/BluetoothDevice;)Z

    move-result v0

    if-eqz v0, :cond_2

    if-nez p2, :cond_2

    const-string/jumbo v0, "CachedBluetoothDevice"

    const-string/jumbo v1, "Removing PanProfile from device after NAP disconnect"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/settingslib/bluetooth/b;->cDB:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/android/settingslib/bluetooth/b;->cDy:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iput-boolean v3, p0, Lcom/android/settingslib/bluetooth/b;->cDK:Z

    goto :goto_0
.end method

.method ciH(Landroid/bluetooth/BluetoothClass;)V
    .locals 1

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/android/settingslib/bluetooth/b;->cDx:Landroid/bluetooth/BluetoothClass;

    if-eq v0, p1, :cond_0

    iput-object p1, p0, Lcom/android/settingslib/bluetooth/b;->cDx:Landroid/bluetooth/BluetoothClass;

    invoke-direct {p0}, Lcom/android/settingslib/bluetooth/b;->cjb()V

    :cond_0
    return-void
.end method

.method declared-synchronized ciI(Lcom/android/settingslib/bluetooth/f;)V
    .locals 3

    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lcom/android/settingslib/bluetooth/b;->cjk()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_0

    monitor-exit p0

    return-void

    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/android/settingslib/bluetooth/b;->cDI:Landroid/bluetooth/BluetoothDevice;

    invoke-interface {p1, v0}, Lcom/android/settingslib/bluetooth/f;->asi(Landroid/bluetooth/BluetoothDevice;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string/jumbo v0, "CachedBluetoothDevice"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Command sent successfully:CONNECT "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-direct {p0, p1}, Lcom/android/settingslib/bluetooth/b;->cjg(Lcom/android/settingslib/bluetooth/f;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit p0

    return-void

    :cond_1
    :try_start_2
    const-string/jumbo v0, "CachedBluetoothDevice"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Failed to connect "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " to "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/settingslib/bluetooth/b;->cDz:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public ciJ()I
    .locals 8

    const/4 v5, 0x1

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/android/settingslib/bluetooth/b;->cjp()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v6

    move v1, v2

    move v3, v2

    move v4, v2

    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settingslib/bluetooth/f;

    invoke-virtual {p0, v0}, Lcom/android/settingslib/bluetooth/b;->cje(Lcom/android/settingslib/bluetooth/f;)I

    move-result v7

    packed-switch v7, :pswitch_data_0

    move v0, v1

    move v1, v3

    move v3, v4

    :goto_1
    move v4, v3

    move v3, v1

    move v1, v0

    goto :goto_0

    :pswitch_0
    invoke-static {v7}, Lcom/android/settingslib/bluetooth/J;->cmk(I)I

    move-result v0

    return v0

    :pswitch_1
    move v0, v1

    move v1, v3

    move v3, v5

    goto :goto_1

    :pswitch_2
    invoke-interface {v0}, Lcom/android/settingslib/bluetooth/f;->ass()Z

    move-result v7

    if-eqz v7, :cond_1

    instance-of v7, v0, Lcom/android/settingslib/bluetooth/E;

    if-nez v7, :cond_0

    instance-of v7, v0, Lcom/android/settingslib/bluetooth/B;

    if-eqz v7, :cond_2

    :cond_0
    move v3, v5

    :cond_1
    :goto_2
    move v0, v1

    move v1, v3

    move v3, v4

    goto :goto_1

    :cond_2
    instance-of v7, v0, Lcom/android/settingslib/bluetooth/C;

    if-nez v7, :cond_3

    instance-of v0, v0, Lcom/android/settingslib/bluetooth/G;

    if-eqz v0, :cond_1

    :cond_3
    move v1, v5

    goto :goto_2

    :cond_4
    if-eqz v4, :cond_8

    if-eqz v3, :cond_5

    if-eqz v1, :cond_5

    sget v0, Lcom/android/settingslib/i;->cLI:I

    return v0

    :cond_5
    if-eqz v3, :cond_6

    sget v0, Lcom/android/settingslib/i;->cLG:I

    return v0

    :cond_6
    if-eqz v1, :cond_7

    sget v0, Lcom/android/settingslib/i;->cLH:I

    return v0

    :cond_7
    sget v0, Lcom/android/settingslib/i;->cLF:I

    return v0

    :cond_8
    invoke-virtual {p0}, Lcom/android/settingslib/bluetooth/b;->ciP()I

    move-result v0

    const/16 v1, 0xb

    if-ne v0, v1, :cond_9

    sget v2, Lcom/android/settingslib/i;->cLS:I

    :cond_9
    return v2

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public ciK()Z
    .locals 1

    iget-object v0, p0, Lcom/android/settingslib/bluetooth/b;->cDD:Lcom/android/settingslib/bluetooth/d;

    invoke-virtual {v0}, Lcom/android/settingslib/bluetooth/d;->cjS()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settingslib/bluetooth/b;->cDD:Lcom/android/settingslib/bluetooth/d;

    invoke-virtual {v0}, Lcom/android/settingslib/bluetooth/d;->cjR()V

    :cond_0
    iget-object v0, p0, Lcom/android/settingslib/bluetooth/b;->cDI:Landroid/bluetooth/BluetoothDevice;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothDevice;->createBond()Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x0

    return v0

    :cond_1
    const/4 v0, 0x1

    return v0
.end method

.method public ciM(Lcom/android/settingslib/bluetooth/c;)V
    .locals 2

    iget-object v1, p0, Lcom/android/settingslib/bluetooth/b;->cDE:Ljava/util/Collection;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/android/settingslib/bluetooth/b;->cDE:Ljava/util/Collection;

    invoke-interface {v0, p1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public ciN(Lcom/android/settingslib/bluetooth/f;)V
    .locals 3

    iget-object v0, p0, Lcom/android/settingslib/bluetooth/b;->cDI:Landroid/bluetooth/BluetoothDevice;

    invoke-interface {p1, v0}, Lcom/android/settingslib/bluetooth/f;->asj(Landroid/bluetooth/BluetoothDevice;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string/jumbo v0, "CachedBluetoothDevice"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Command sent successfully:DISCONNECT "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-direct {p0, p1}, Lcom/android/settingslib/bluetooth/b;->cjg(Lcom/android/settingslib/bluetooth/f;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    return-void
.end method

.method public ciO(I)V
    .locals 3

    const/4 v1, 0x2

    const/4 v0, 0x1

    const/4 v2, 0x0

    if-ne p1, v0, :cond_0

    :goto_0
    iget-object v1, p0, Lcom/android/settingslib/bluetooth/b;->cDI:Landroid/bluetooth/BluetoothDevice;

    invoke-virtual {v1, v0}, Landroid/bluetooth/BluetoothDevice;->setPhonebookAccessPermission(I)Z

    return-void

    :cond_0
    if-ne p1, v1, :cond_1

    move v0, v1

    goto :goto_0

    :cond_1
    move v0, v2

    goto :goto_0
.end method

.method public ciP()I
    .locals 1

    iget-object v0, p0, Lcom/android/settingslib/bluetooth/b;->cDI:Landroid/bluetooth/BluetoothDevice;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothDevice;->getBondState()I

    move-result v0

    return v0
.end method

.method public ciQ(Z)V
    .locals 2

    invoke-direct {p0}, Lcom/android/settingslib/bluetooth/b;->cjk()Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/android/settingslib/bluetooth/b;->cDC:J

    invoke-direct {p0, p1}, Lcom/android/settingslib/bluetooth/b;->civ(Z)V

    return-void
.end method

.method public ciR()Ljava/util/List;
    .locals 4

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iget-object v0, p0, Lcom/android/settingslib/bluetooth/b;->cDB:Ljava/util/List;

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settingslib/bluetooth/f;

    invoke-interface {v0}, Lcom/android/settingslib/bluetooth/f;->asq()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    return-object v1
.end method

.method public ciT(Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lcom/android/settingslib/bluetooth/b;->cDz:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settingslib/bluetooth/b;->cDz:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_1

    :cond_0
    iput-object p1, p0, Lcom/android/settingslib/bluetooth/b;->cDz:Ljava/lang/String;

    iget-object v0, p0, Lcom/android/settingslib/bluetooth/b;->cDI:Landroid/bluetooth/BluetoothDevice;

    invoke-virtual {v0, p1}, Landroid/bluetooth/BluetoothDevice;->setAlias(Ljava/lang/String;)Z

    invoke-direct {p0}, Lcom/android/settingslib/bluetooth/b;->cjb()V

    :cond_1
    return-void
.end method

.method ciU(S)V
    .locals 1

    iget-short v0, p0, Lcom/android/settingslib/bluetooth/b;->cDH:S

    if-eq v0, p1, :cond_0

    iput-short p1, p0, Lcom/android/settingslib/bluetooth/b;->cDH:S

    invoke-direct {p0}, Lcom/android/settingslib/bluetooth/b;->cjb()V

    :cond_0
    return-void
.end method

.method public ciV()Landroid/bluetooth/BluetoothDevice;
    .locals 1

    iget-object v0, p0, Lcom/android/settingslib/bluetooth/b;->cDI:Landroid/bluetooth/BluetoothDevice;

    return-object v0
.end method

.method public ciW()V
    .locals 4

    const-string/jumbo v0, "CachedBluetoothDevice"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, " Clearing all connection state for dev:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/settingslib/bluetooth/b;->cDI:Landroid/bluetooth/BluetoothDevice;

    invoke-virtual {v2}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lcom/android/settingslib/bluetooth/b;->cjp()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settingslib/bluetooth/f;

    iget-object v2, p0, Lcom/android/settingslib/bluetooth/b;->cDG:Ljava/util/HashMap;

    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v0, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_0
    return-void
.end method

.method public ciX()Z
    .locals 2

    const/4 v1, 0x2

    iget v0, p0, Lcom/android/settingslib/bluetooth/b;->cDF:I

    if-ge v0, v1, :cond_0

    iget v0, p0, Lcom/android/settingslib/bluetooth/b;->cDF:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/android/settingslib/bluetooth/b;->cDF:I

    invoke-direct {p0}, Lcom/android/settingslib/bluetooth/b;->ciS()V

    :cond_0
    iget v0, p0, Lcom/android/settingslib/bluetooth/b;->cDF:I

    if-lt v0, v1, :cond_1

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public ciZ()V
    .locals 3

    iget-object v0, p0, Lcom/android/settingslib/bluetooth/b;->cDB:Ljava/util/List;

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settingslib/bluetooth/f;

    invoke-virtual {p0, v0}, Lcom/android/settingslib/bluetooth/b;->ciN(Lcom/android/settingslib/bluetooth/f;)V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/android/settingslib/bluetooth/b;->cDJ:Lcom/android/settingslib/bluetooth/e;

    invoke-virtual {v0}, Lcom/android/settingslib/bluetooth/e;->ckc()Lcom/android/settingslib/bluetooth/F;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settingslib/bluetooth/b;->cDI:Landroid/bluetooth/BluetoothDevice;

    invoke-virtual {v0, v1}, Lcom/android/settingslib/bluetooth/F;->ask(Landroid/bluetooth/BluetoothDevice;)I

    move-result v1

    const/4 v2, 0x2

    if-ne v1, v2, :cond_1

    iget-object v1, p0, Lcom/android/settingslib/bluetooth/b;->cDI:Landroid/bluetooth/BluetoothDevice;

    invoke-virtual {v0, v1}, Lcom/android/settingslib/bluetooth/F;->asj(Landroid/bluetooth/BluetoothDevice;)Z

    :cond_1
    return-void
.end method

.method ciw()V
    .locals 0

    invoke-direct {p0}, Lcom/android/settingslib/bluetooth/b;->cjj()V

    invoke-direct {p0}, Lcom/android/settingslib/bluetooth/b;->cjb()V

    return-void
.end method

.method cix(Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lcom/android/settingslib/bluetooth/b;->cDz:Ljava/lang/String;

    if-nez v0, :cond_2

    iput-object p1, p0, Lcom/android/settingslib/bluetooth/b;->cDz:Ljava/lang/String;

    iget-object v0, p0, Lcom/android/settingslib/bluetooth/b;->cDz:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settingslib/bluetooth/b;->cDz:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/android/settingslib/bluetooth/b;->cDI:Landroid/bluetooth/BluetoothDevice;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settingslib/bluetooth/b;->cDz:Ljava/lang/String;

    :cond_1
    invoke-direct {p0}, Lcom/android/settingslib/bluetooth/b;->cjb()V

    :cond_2
    return-void
.end method

.method ciy()S
    .locals 1

    iget-short v0, p0, Lcom/android/settingslib/bluetooth/b;->cDH:S

    return v0
.end method

.method public ciz(Lcom/android/settingslib/bluetooth/c;)V
    .locals 2

    iget-object v1, p0, Lcom/android/settingslib/bluetooth/b;->cDE:Ljava/util/Collection;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/android/settingslib/bluetooth/b;->cDE:Ljava/util/Collection;

    invoke-interface {v0, p1}, Ljava/util/Collection;->remove(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public cja()Landroid/bluetooth/BluetoothClass;
    .locals 1

    iget-object v0, p0, Lcom/android/settingslib/bluetooth/b;->cDx:Landroid/bluetooth/BluetoothClass;

    return-object v0
.end method

.method public cjc()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/android/settingslib/bluetooth/b;->cDz:Ljava/lang/String;

    return-object v0
.end method

.method public cjd(Lcom/android/settingslib/bluetooth/f;)V
    .locals 2

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/android/settingslib/bluetooth/b;->cDC:J

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/settingslib/bluetooth/b;->cDA:Z

    invoke-virtual {p0, p1}, Lcom/android/settingslib/bluetooth/b;->ciI(Lcom/android/settingslib/bluetooth/f;)V

    invoke-virtual {p0}, Lcom/android/settingslib/bluetooth/b;->cjn()V

    return-void
.end method

.method public cje(Lcom/android/settingslib/bluetooth/f;)I
    .locals 2

    iget-object v0, p0, Lcom/android/settingslib/bluetooth/b;->cDG:Ljava/util/HashMap;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settingslib/bluetooth/b;->cDG:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/android/settingslib/bluetooth/b;->cDI:Landroid/bluetooth/BluetoothDevice;

    invoke-interface {p1, v0}, Lcom/android/settingslib/bluetooth/f;->ask(Landroid/bluetooth/BluetoothDevice;)I

    move-result v0

    iget-object v1, p0, Lcom/android/settingslib/bluetooth/b;->cDG:Ljava/util/HashMap;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v1, p1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1
    iget-object v0, p0, Lcom/android/settingslib/bluetooth/b;->cDG:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0
.end method

.method public cjf()I
    .locals 3

    const/4 v2, 0x2

    const/4 v1, 0x1

    iget-object v0, p0, Lcom/android/settingslib/bluetooth/b;->cDI:Landroid/bluetooth/BluetoothDevice;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothDevice;->getPhonebookAccessPermission()I

    move-result v0

    if-ne v0, v1, :cond_0

    return v1

    :cond_0
    if-ne v0, v2, :cond_1

    return v2

    :cond_1
    const/4 v0, 0x0

    return v0
.end method

.method public cjl()V
    .locals 4

    const/4 v3, 0x0

    invoke-virtual {p0}, Lcom/android/settingslib/bluetooth/b;->ciP()I

    move-result v0

    const/16 v1, 0xb

    if-ne v0, v1, :cond_0

    iget-object v1, p0, Lcom/android/settingslib/bluetooth/b;->cDI:Landroid/bluetooth/BluetoothDevice;

    invoke-virtual {v1}, Landroid/bluetooth/BluetoothDevice;->cancelBondProcess()Z

    :cond_0
    const/16 v1, 0xa

    if-eq v0, v1, :cond_1

    iget-object v0, p0, Lcom/android/settingslib/bluetooth/b;->cDI:Landroid/bluetooth/BluetoothDevice;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothDevice;->removeBond()Z

    move-result v0

    if-eqz v0, :cond_1

    const-string/jumbo v0, "CachedBluetoothDevice"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Command sent successfully:REMOVE_BOND "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-direct {p0, v3}, Lcom/android/settingslib/bluetooth/b;->cjg(Lcom/android/settingslib/bluetooth/f;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    return-void
.end method

.method public cjn()V
    .locals 0

    invoke-direct {p0}, Lcom/android/settingslib/bluetooth/b;->cjb()V

    return-void
.end method

.method public cjp()Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/android/settingslib/bluetooth/b;->cDB:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public cjq(I)V
    .locals 3

    const/4 v1, 0x2

    const/4 v0, 0x1

    const/4 v2, 0x0

    if-ne p1, v0, :cond_0

    :goto_0
    iget-object v1, p0, Lcom/android/settingslib/bluetooth/b;->cDI:Landroid/bluetooth/BluetoothDevice;

    invoke-virtual {v1, v0}, Landroid/bluetooth/BluetoothDevice;->setMessageAccessPermission(I)Z

    return-void

    :cond_0
    if-ne p1, v1, :cond_1

    move v0, v1

    goto :goto_0

    :cond_1
    move v0, v2

    goto :goto_0
.end method

.method public cjr()Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/android/settingslib/bluetooth/b;->cDy:Ljava/util/List;

    return-object v0
.end method

.method cjs()V
    .locals 0

    invoke-direct {p0}, Lcom/android/settingslib/bluetooth/b;->ciL()V

    invoke-direct {p0}, Lcom/android/settingslib/bluetooth/b;->cjb()V

    return-void
.end method

.method cjt(I)V
    .locals 2

    const/4 v1, 0x0

    const/16 v0, 0xa

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Lcom/android/settingslib/bluetooth/b;->cDB:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    invoke-virtual {p0, v1}, Lcom/android/settingslib/bluetooth/b;->ciO(I)V

    invoke-virtual {p0, v1}, Lcom/android/settingslib/bluetooth/b;->cjq(I)V

    invoke-virtual {p0, v1}, Lcom/android/settingslib/bluetooth/b;->cju(I)V

    iput v1, p0, Lcom/android/settingslib/bluetooth/b;->cDF:I

    invoke-direct {p0}, Lcom/android/settingslib/bluetooth/b;->ciS()V

    :cond_0
    invoke-virtual {p0}, Lcom/android/settingslib/bluetooth/b;->cjn()V

    const/16 v0, 0xc

    if-ne p1, v0, :cond_1

    iget-object v0, p0, Lcom/android/settingslib/bluetooth/b;->cDI:Landroid/bluetooth/BluetoothDevice;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothDevice;->isBluetoothDock()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lcom/android/settingslib/bluetooth/b;->ciF()V

    :cond_1
    :goto_0
    return-void

    :cond_2
    iget-object v0, p0, Lcom/android/settingslib/bluetooth/b;->cDI:Landroid/bluetooth/BluetoothDevice;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothDevice;->isBondingInitiatedLocally()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0, v1}, Lcom/android/settingslib/bluetooth/b;->ciQ(Z)V

    goto :goto_0
.end method

.method cju(I)V
    .locals 3

    const/4 v1, 0x2

    const/4 v0, 0x1

    const/4 v2, 0x0

    if-ne p1, v0, :cond_0

    :goto_0
    iget-object v1, p0, Lcom/android/settingslib/bluetooth/b;->cDI:Landroid/bluetooth/BluetoothDevice;

    invoke-virtual {v1, v0}, Landroid/bluetooth/BluetoothDevice;->setSimAccessPermission(I)Z

    return-void

    :cond_0
    if-ne p1, v1, :cond_1

    move v0, v1

    goto :goto_0

    :cond_1
    move v0, v2

    goto :goto_0
.end method

.method public cjw()I
    .locals 3

    const/4 v2, 0x2

    const/4 v1, 0x1

    iget-object v0, p0, Lcom/android/settingslib/bluetooth/b;->cDI:Landroid/bluetooth/BluetoothDevice;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothDevice;->getMessageAccessPermission()I

    move-result v0

    if-ne v0, v1, :cond_0

    return v1

    :cond_0
    if-ne v0, v2, :cond_1

    return v2

    :cond_1
    const/4 v0, 0x0

    return v0
.end method

.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 1

    check-cast p1, Lcom/android/settingslib/bluetooth/b;

    invoke-virtual {p0, p1}, Lcom/android/settingslib/bluetooth/b;->ciB(Lcom/android/settingslib/bluetooth/b;)I

    move-result v0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eqz p1, :cond_0

    instance-of v0, p1, Lcom/android/settingslib/bluetooth/b;

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x0

    return v0

    :cond_1
    iget-object v0, p0, Lcom/android/settingslib/bluetooth/b;->cDI:Landroid/bluetooth/BluetoothDevice;

    check-cast p1, Lcom/android/settingslib/bluetooth/b;

    iget-object v1, p1, Lcom/android/settingslib/bluetooth/b;->cDI:Landroid/bluetooth/BluetoothDevice;

    invoke-virtual {v0, v1}, Landroid/bluetooth/BluetoothDevice;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public hashCode()I
    .locals 1

    iget-object v0, p0, Lcom/android/settingslib/bluetooth/b;->cDI:Landroid/bluetooth/BluetoothDevice;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    return v0
.end method

.method public setVisible(Z)V
    .locals 1

    iget-boolean v0, p0, Lcom/android/settingslib/bluetooth/b;->cDL:Z

    if-eq v0, p1, :cond_0

    iput-boolean p1, p0, Lcom/android/settingslib/bluetooth/b;->cDL:Z

    invoke-direct {p0}, Lcom/android/settingslib/bluetooth/b;->cjb()V

    :cond_0
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/android/settingslib/bluetooth/b;->cDI:Landroid/bluetooth/BluetoothDevice;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothDevice;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
