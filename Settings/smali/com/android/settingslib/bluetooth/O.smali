.class Lcom/android/settingslib/bluetooth/O;
.super Ljava/lang/Object;
.source "BluetoothEventManager.java"

# interfaces
.implements Lcom/android/settingslib/bluetooth/I;


# instance fields
.field final synthetic cGa:Lcom/android/settingslib/bluetooth/s;


# direct methods
.method private constructor <init>(Lcom/android/settingslib/bluetooth/s;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settingslib/bluetooth/O;->cGa:Lcom/android/settingslib/bluetooth/s;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/settingslib/bluetooth/s;Lcom/android/settingslib/bluetooth/O;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/settingslib/bluetooth/O;-><init>(Lcom/android/settingslib/bluetooth/s;)V

    return-void
.end method


# virtual methods
.method public ckW(Landroid/content/Context;Landroid/content/Intent;Landroid/bluetooth/BluetoothDevice;)V
    .locals 3

    iget-object v0, p0, Lcom/android/settingslib/bluetooth/O;->cGa:Lcom/android/settingslib/bluetooth/s;

    invoke-static {v0}, Lcom/android/settingslib/bluetooth/s;->ckv(Lcom/android/settingslib/bluetooth/s;)Lcom/android/settingslib/bluetooth/t;

    move-result-object v0

    invoke-virtual {v0, p3}, Lcom/android/settingslib/bluetooth/t;->ckL(Landroid/bluetooth/BluetoothDevice;)Lcom/android/settingslib/bluetooth/b;

    move-result-object v0

    const-string/jumbo v1, "android.bluetooth.adapter.extra.CONNECTION_STATE"

    const/high16 v2, -0x80000000

    invoke-virtual {p2, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    iget-object v2, p0, Lcom/android/settingslib/bluetooth/O;->cGa:Lcom/android/settingslib/bluetooth/s;

    invoke-static {v2, v0, v1}, Lcom/android/settingslib/bluetooth/s;->ckC(Lcom/android/settingslib/bluetooth/s;Lcom/android/settingslib/bluetooth/b;I)V

    return-void
.end method
