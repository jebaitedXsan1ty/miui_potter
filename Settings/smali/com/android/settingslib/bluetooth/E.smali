.class public Lcom/android/settingslib/bluetooth/E;
.super Ljava/lang/Object;
.source "A2dpProfile.java"

# interfaces
.implements Lcom/android/settingslib/bluetooth/f;


# static fields
.field static final cFu:[Landroid/os/ParcelUuid;

.field private static cFv:Z


# instance fields
.field private cFA:Landroid/bluetooth/BluetoothA2dp;

.field private final cFB:Lcom/android/settingslib/bluetooth/t;

.field private cFC:Lcom/android/settingslib/bluetooth/aj;

.field private cFw:Z

.field cFx:Lcom/android/settingslib/bluetooth/ak;

.field private final cFy:Lcom/android/settingslib/bluetooth/d;

.field private final cFz:Lcom/android/settingslib/bluetooth/e;

.field private mContext:Landroid/content/Context;


# direct methods
.method static synthetic -get0()Z
    .locals 1

    sget-boolean v0, Lcom/android/settingslib/bluetooth/E;->cFv:Z

    return v0
.end method

.method static constructor <clinit>()V
    .locals 3

    const/4 v2, 0x0

    sput-boolean v2, Lcom/android/settingslib/bluetooth/E;->cFv:Z

    const/4 v0, 0x2

    new-array v0, v0, [Landroid/os/ParcelUuid;

    sget-object v1, Landroid/bluetooth/BluetoothUuid;->AudioSink:Landroid/os/ParcelUuid;

    aput-object v1, v0, v2

    sget-object v1, Landroid/bluetooth/BluetoothUuid;->AdvAudioDist:Landroid/os/ParcelUuid;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    sput-object v0, Lcom/android/settingslib/bluetooth/E;->cFu:[Landroid/os/ParcelUuid;

    return-void
.end method

.method constructor <init>(Landroid/content/Context;Lcom/android/settingslib/bluetooth/d;Lcom/android/settingslib/bluetooth/t;Lcom/android/settingslib/bluetooth/e;)V
    .locals 3

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/android/settingslib/bluetooth/E;->mContext:Landroid/content/Context;

    iput-object p2, p0, Lcom/android/settingslib/bluetooth/E;->cFy:Lcom/android/settingslib/bluetooth/d;

    iput-object p3, p0, Lcom/android/settingslib/bluetooth/E;->cFB:Lcom/android/settingslib/bluetooth/t;

    iput-object p4, p0, Lcom/android/settingslib/bluetooth/E;->cFz:Lcom/android/settingslib/bluetooth/e;

    new-instance v0, Lcom/android/settingslib/bluetooth/ah;

    invoke-direct {v0}, Lcom/android/settingslib/bluetooth/ah;-><init>()V

    iput-object v0, p0, Lcom/android/settingslib/bluetooth/E;->cFx:Lcom/android/settingslib/bluetooth/ak;

    iget-object v0, p0, Lcom/android/settingslib/bluetooth/E;->cFy:Lcom/android/settingslib/bluetooth/d;

    new-instance v1, Lcom/android/settingslib/bluetooth/al;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/android/settingslib/bluetooth/al;-><init>(Lcom/android/settingslib/bluetooth/E;Lcom/android/settingslib/bluetooth/al;)V

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v1, v2}, Lcom/android/settingslib/bluetooth/d;->cjH(Landroid/content/Context;Landroid/bluetooth/BluetoothProfile$ServiceListener;I)V

    return-void
.end method

.method static synthetic clK(Lcom/android/settingslib/bluetooth/E;)Lcom/android/settingslib/bluetooth/e;
    .locals 1

    iget-object v0, p0, Lcom/android/settingslib/bluetooth/E;->cFz:Lcom/android/settingslib/bluetooth/e;

    return-object v0
.end method

.method static synthetic clL(Lcom/android/settingslib/bluetooth/E;Landroid/bluetooth/BluetoothA2dp;)Landroid/bluetooth/BluetoothA2dp;
    .locals 0

    iput-object p1, p0, Lcom/android/settingslib/bluetooth/E;->cFA:Landroid/bluetooth/BluetoothA2dp;

    return-object p1
.end method

.method static synthetic clM(Lcom/android/settingslib/bluetooth/E;)Lcom/android/settingslib/bluetooth/t;
    .locals 1

    iget-object v0, p0, Lcom/android/settingslib/bluetooth/E;->cFB:Lcom/android/settingslib/bluetooth/t;

    return-object v0
.end method

.method static synthetic clN(Lcom/android/settingslib/bluetooth/E;)Lcom/android/settingslib/bluetooth/d;
    .locals 1

    iget-object v0, p0, Lcom/android/settingslib/bluetooth/E;->cFy:Lcom/android/settingslib/bluetooth/d;

    return-object v0
.end method

.method static synthetic clO(Lcom/android/settingslib/bluetooth/E;Lcom/android/settingslib/bluetooth/aj;)Lcom/android/settingslib/bluetooth/aj;
    .locals 0

    iput-object p1, p0, Lcom/android/settingslib/bluetooth/E;->cFC:Lcom/android/settingslib/bluetooth/aj;

    return-object p1
.end method

.method static synthetic clR(Lcom/android/settingslib/bluetooth/E;)Landroid/bluetooth/BluetoothA2dp;
    .locals 1

    iget-object v0, p0, Lcom/android/settingslib/bluetooth/E;->cFA:Landroid/bluetooth/BluetoothA2dp;

    return-object v0
.end method

.method static synthetic clS(Lcom/android/settingslib/bluetooth/E;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/android/settingslib/bluetooth/E;->cFw:Z

    return p1
.end method


# virtual methods
.method public asi(Landroid/bluetooth/BluetoothDevice;)Z
    .locals 4

    iget-object v0, p0, Lcom/android/settingslib/bluetooth/E;->cFA:Landroid/bluetooth/BluetoothA2dp;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    return v0

    :cond_0
    invoke-virtual {p0}, Lcom/android/settingslib/bluetooth/E;->clP()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/bluetooth/BluetoothDevice;

    invoke-virtual {v0, p1}, Landroid/bluetooth/BluetoothDevice;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string/jumbo v0, "A2dpProfile"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "Connecting to device "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " : disconnect skipped"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/android/settingslib/bluetooth/E;->cFA:Landroid/bluetooth/BluetoothA2dp;

    invoke-virtual {v0, p1}, Landroid/bluetooth/BluetoothA2dp;->connect(Landroid/bluetooth/BluetoothDevice;)Z

    move-result v0

    return v0
.end method

.method public asj(Landroid/bluetooth/BluetoothDevice;)Z
    .locals 4

    const/16 v3, 0x64

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/android/settingslib/bluetooth/E;->cFA:Landroid/bluetooth/BluetoothA2dp;

    if-nez v0, :cond_0

    return v2

    :cond_0
    iget-object v0, p0, Lcom/android/settingslib/bluetooth/E;->cFA:Landroid/bluetooth/BluetoothA2dp;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothA2dp;->getConnectedDevices()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_4

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/bluetooth/BluetoothDevice;

    invoke-virtual {v0, p1}, Landroid/bluetooth/BluetoothDevice;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    sget-boolean v0, Lcom/android/settingslib/bluetooth/E;->cFv:Z

    if-eqz v0, :cond_2

    const-string/jumbo v0, "A2dpProfile"

    const-string/jumbo v1, "Downgrade priority as useris disconnecting the headset"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    iget-object v0, p0, Lcom/android/settingslib/bluetooth/E;->cFA:Landroid/bluetooth/BluetoothA2dp;

    invoke-virtual {v0, p1}, Landroid/bluetooth/BluetoothA2dp;->getPriority(Landroid/bluetooth/BluetoothDevice;)I

    move-result v0

    if-le v0, v3, :cond_3

    iget-object v0, p0, Lcom/android/settingslib/bluetooth/E;->cFA:Landroid/bluetooth/BluetoothA2dp;

    invoke-virtual {v0, p1, v3}, Landroid/bluetooth/BluetoothA2dp;->setPriority(Landroid/bluetooth/BluetoothDevice;I)Z

    :cond_3
    iget-object v0, p0, Lcom/android/settingslib/bluetooth/E;->cFA:Landroid/bluetooth/BluetoothA2dp;

    invoke-virtual {v0, p1}, Landroid/bluetooth/BluetoothA2dp;->disconnect(Landroid/bluetooth/BluetoothDevice;)Z

    move-result v0

    return v0

    :cond_4
    return v2
.end method

.method public ask(Landroid/bluetooth/BluetoothDevice;)I
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/android/settingslib/bluetooth/E;->cFA:Landroid/bluetooth/BluetoothA2dp;

    if-nez v0, :cond_0

    return v2

    :cond_0
    iget-object v0, p0, Lcom/android/settingslib/bluetooth/E;->cFA:Landroid/bluetooth/BluetoothA2dp;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothA2dp;->getConnectedDevices()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_2

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/bluetooth/BluetoothDevice;

    invoke-virtual {v0, p1}, Landroid/bluetooth/BluetoothDevice;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/settingslib/bluetooth/E;->cFA:Landroid/bluetooth/BluetoothA2dp;

    invoke-virtual {v0, p1}, Landroid/bluetooth/BluetoothA2dp;->getConnectionState(Landroid/bluetooth/BluetoothDevice;)I

    move-result v0

    return v0

    :cond_2
    return v2
.end method

.method public asl(Landroid/bluetooth/BluetoothClass;)I
    .locals 1

    sget v0, Lcom/android/settingslib/e;->cKE:I

    return v0
.end method

.method public asm(Landroid/bluetooth/BluetoothDevice;)I
    .locals 1

    sget v0, Lcom/android/settingslib/i;->cMa:I

    return v0
.end method

.method public asn()I
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public aso(Landroid/bluetooth/BluetoothDevice;)I
    .locals 1

    invoke-virtual {p0, p1}, Lcom/android/settingslib/bluetooth/E;->ask(Landroid/bluetooth/BluetoothDevice;)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :pswitch_0
    invoke-static {v0}, Lcom/android/settingslib/bluetooth/J;->cmk(I)I

    move-result v0

    return v0

    :pswitch_1
    sget v0, Lcom/android/settingslib/i;->cLE:I

    return v0

    :pswitch_2
    sget v0, Lcom/android/settingslib/i;->cLD:I

    return v0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public asp()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public asq()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public asr(Landroid/bluetooth/BluetoothDevice;)Z
    .locals 2

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/android/settingslib/bluetooth/E;->cFA:Landroid/bluetooth/BluetoothA2dp;

    if-nez v1, :cond_0

    return v0

    :cond_0
    iget-object v1, p0, Lcom/android/settingslib/bluetooth/E;->cFA:Landroid/bluetooth/BluetoothA2dp;

    invoke-virtual {v1, p1}, Landroid/bluetooth/BluetoothA2dp;->getPriority(Landroid/bluetooth/BluetoothDevice;)I

    move-result v1

    if-lez v1, :cond_1

    const/4 v0, 0x1

    :cond_1
    return v0
.end method

.method public ass()Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/settingslib/bluetooth/E;->cFw:Z

    return v0
.end method

.method public ast(Landroid/bluetooth/BluetoothDevice;Z)V
    .locals 2

    const/16 v1, 0x64

    iget-object v0, p0, Lcom/android/settingslib/bluetooth/E;->cFA:Landroid/bluetooth/BluetoothA2dp;

    if-nez v0, :cond_0

    return-void

    :cond_0
    if-eqz p2, :cond_2

    iget-object v0, p0, Lcom/android/settingslib/bluetooth/E;->cFA:Landroid/bluetooth/BluetoothA2dp;

    invoke-virtual {v0, p1}, Landroid/bluetooth/BluetoothA2dp;->getPriority(Landroid/bluetooth/BluetoothDevice;)I

    move-result v0

    if-ge v0, v1, :cond_1

    iget-object v0, p0, Lcom/android/settingslib/bluetooth/E;->cFA:Landroid/bluetooth/BluetoothA2dp;

    invoke-virtual {v0, p1, v1}, Landroid/bluetooth/BluetoothA2dp;->setPriority(Landroid/bluetooth/BluetoothDevice;I)Z

    :cond_1
    :goto_0
    return-void

    :cond_2
    iget-object v0, p0, Lcom/android/settingslib/bluetooth/E;->cFA:Landroid/bluetooth/BluetoothA2dp;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Landroid/bluetooth/BluetoothA2dp;->setPriority(Landroid/bluetooth/BluetoothDevice;I)Z

    goto :goto_0
.end method

.method public clP()Ljava/util/List;
    .locals 4

    iget-object v0, p0, Lcom/android/settingslib/bluetooth/E;->cFA:Landroid/bluetooth/BluetoothA2dp;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/ArrayList;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/android/settingslib/bluetooth/E;->cFA:Landroid/bluetooth/BluetoothA2dp;

    const/4 v1, 0x2

    const/4 v2, 0x1

    const/4 v3, 0x3

    filled-new-array {v1, v2, v3}, [I

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/bluetooth/BluetoothA2dp;->getDevicesMatchingConnectionStates([I)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method clQ()Z
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/android/settingslib/bluetooth/E;->cFA:Landroid/bluetooth/BluetoothA2dp;

    if-nez v0, :cond_0

    return v2

    :cond_0
    iget-object v0, p0, Lcom/android/settingslib/bluetooth/E;->cFA:Landroid/bluetooth/BluetoothA2dp;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothA2dp;->getConnectedDevices()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/android/settingslib/bluetooth/E;->cFA:Landroid/bluetooth/BluetoothA2dp;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/bluetooth/BluetoothDevice;

    invoke-virtual {v1, v0}, Landroid/bluetooth/BluetoothA2dp;->isA2dpPlaying(Landroid/bluetooth/BluetoothDevice;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    return v0

    :cond_1
    return v2
.end method

.method protected finalize()V
    .locals 3

    sget-boolean v0, Lcom/android/settingslib/bluetooth/E;->cFv:Z

    if-eqz v0, :cond_0

    const-string/jumbo v0, "A2dpProfile"

    const-string/jumbo v1, "finalize()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v0, p0, Lcom/android/settingslib/bluetooth/E;->cFA:Landroid/bluetooth/BluetoothA2dp;

    if-eqz v0, :cond_1

    :try_start_0
    invoke-static {}, Landroid/bluetooth/BluetoothAdapter;->getDefaultAdapter()Landroid/bluetooth/BluetoothAdapter;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settingslib/bluetooth/E;->cFA:Landroid/bluetooth/BluetoothA2dp;

    const/4 v2, 0x2

    invoke-virtual {v0, v2, v1}, Landroid/bluetooth/BluetoothAdapter;->closeProfileProxy(ILandroid/bluetooth/BluetoothProfile;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/settingslib/bluetooth/E;->cFA:Landroid/bluetooth/BluetoothA2dp;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    :cond_1
    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string/jumbo v1, "A2dpProfile"

    const-string/jumbo v2, "Error cleaning up A2DP proxy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method setWrapperFactory(Lcom/android/settingslib/bluetooth/ak;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settingslib/bluetooth/E;->cFx:Lcom/android/settingslib/bluetooth/ak;

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    const-string/jumbo v0, "A2DP"

    return-object v0
.end method
