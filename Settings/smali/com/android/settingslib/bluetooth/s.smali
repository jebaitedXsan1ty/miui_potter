.class public Lcom/android/settingslib/bluetooth/s;
.super Ljava/lang/Object;
.source "BluetoothEventManager.java"


# instance fields
.field private final cEA:Landroid/content/BroadcastReceiver;

.field private final cEB:Landroid/content/IntentFilter;

.field private final cEC:Ljava/util/Map;

.field private final cEt:Landroid/content/BroadcastReceiver;

.field private final cEu:Lcom/android/settingslib/bluetooth/d;

.field private cEv:Lcom/android/settingslib/bluetooth/e;

.field private final cEw:Ljava/util/Collection;

.field private cEx:Landroid/os/Handler;

.field private final cEy:Landroid/content/IntentFilter;

.field private final cEz:Lcom/android/settingslib/bluetooth/t;

.field private mContext:Landroid/content/Context;


# direct methods
.method constructor <init>(Lcom/android/settingslib/bluetooth/d;Lcom/android/settingslib/bluetooth/t;Landroid/content/Context;)V
    .locals 5

    const/4 v4, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/settingslib/bluetooth/s;->cEw:Ljava/util/Collection;

    new-instance v0, Lcom/android/settingslib/bluetooth/W;

    invoke-direct {v0, p0}, Lcom/android/settingslib/bluetooth/W;-><init>(Lcom/android/settingslib/bluetooth/s;)V

    iput-object v0, p0, Lcom/android/settingslib/bluetooth/s;->cEt:Landroid/content/BroadcastReceiver;

    new-instance v0, Lcom/android/settingslib/bluetooth/X;

    invoke-direct {v0, p0}, Lcom/android/settingslib/bluetooth/X;-><init>(Lcom/android/settingslib/bluetooth/s;)V

    iput-object v0, p0, Lcom/android/settingslib/bluetooth/s;->cEA:Landroid/content/BroadcastReceiver;

    iput-object p1, p0, Lcom/android/settingslib/bluetooth/s;->cEu:Lcom/android/settingslib/bluetooth/d;

    iput-object p2, p0, Lcom/android/settingslib/bluetooth/s;->cEz:Lcom/android/settingslib/bluetooth/t;

    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    iput-object v0, p0, Lcom/android/settingslib/bluetooth/s;->cEB:Landroid/content/IntentFilter;

    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    iput-object v0, p0, Lcom/android/settingslib/bluetooth/s;->cEy:Landroid/content/IntentFilter;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/android/settingslib/bluetooth/s;->cEC:Ljava/util/Map;

    iput-object p3, p0, Lcom/android/settingslib/bluetooth/s;->mContext:Landroid/content/Context;

    const-string/jumbo v0, "android.bluetooth.adapter.action.STATE_CHANGED"

    new-instance v1, Lcom/android/settingslib/bluetooth/L;

    invoke-direct {v1, p0, v4}, Lcom/android/settingslib/bluetooth/L;-><init>(Lcom/android/settingslib/bluetooth/s;Lcom/android/settingslib/bluetooth/L;)V

    invoke-direct {p0, v0, v1}, Lcom/android/settingslib/bluetooth/s;->cky(Ljava/lang/String;Lcom/android/settingslib/bluetooth/I;)V

    const-string/jumbo v0, "android.bluetooth.adapter.action.CONNECTION_STATE_CHANGED"

    new-instance v1, Lcom/android/settingslib/bluetooth/O;

    invoke-direct {v1, p0, v4}, Lcom/android/settingslib/bluetooth/O;-><init>(Lcom/android/settingslib/bluetooth/s;Lcom/android/settingslib/bluetooth/O;)V

    invoke-direct {p0, v0, v1}, Lcom/android/settingslib/bluetooth/s;->cky(Ljava/lang/String;Lcom/android/settingslib/bluetooth/I;)V

    const-string/jumbo v0, "android.bluetooth.adapter.action.DISCOVERY_STARTED"

    new-instance v1, Lcom/android/settingslib/bluetooth/M;

    const/4 v2, 0x1

    invoke-direct {v1, p0, v2}, Lcom/android/settingslib/bluetooth/M;-><init>(Lcom/android/settingslib/bluetooth/s;Z)V

    invoke-direct {p0, v0, v1}, Lcom/android/settingslib/bluetooth/s;->cky(Ljava/lang/String;Lcom/android/settingslib/bluetooth/I;)V

    const-string/jumbo v0, "android.bluetooth.adapter.action.DISCOVERY_FINISHED"

    new-instance v1, Lcom/android/settingslib/bluetooth/M;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/android/settingslib/bluetooth/M;-><init>(Lcom/android/settingslib/bluetooth/s;Z)V

    invoke-direct {p0, v0, v1}, Lcom/android/settingslib/bluetooth/s;->cky(Ljava/lang/String;Lcom/android/settingslib/bluetooth/I;)V

    const-string/jumbo v0, "android.bluetooth.device.action.FOUND"

    new-instance v1, Lcom/android/settingslib/bluetooth/N;

    invoke-direct {v1, p0, v4}, Lcom/android/settingslib/bluetooth/N;-><init>(Lcom/android/settingslib/bluetooth/s;Lcom/android/settingslib/bluetooth/N;)V

    invoke-direct {p0, v0, v1}, Lcom/android/settingslib/bluetooth/s;->cky(Ljava/lang/String;Lcom/android/settingslib/bluetooth/I;)V

    const-string/jumbo v0, "android.bluetooth.device.action.DISAPPEARED"

    new-instance v1, Lcom/android/settingslib/bluetooth/P;

    invoke-direct {v1, p0, v4}, Lcom/android/settingslib/bluetooth/P;-><init>(Lcom/android/settingslib/bluetooth/s;Lcom/android/settingslib/bluetooth/P;)V

    invoke-direct {p0, v0, v1}, Lcom/android/settingslib/bluetooth/s;->cky(Ljava/lang/String;Lcom/android/settingslib/bluetooth/I;)V

    const-string/jumbo v0, "android.bluetooth.device.action.NAME_CHANGED"

    new-instance v1, Lcom/android/settingslib/bluetooth/Q;

    invoke-direct {v1, p0, v4}, Lcom/android/settingslib/bluetooth/Q;-><init>(Lcom/android/settingslib/bluetooth/s;Lcom/android/settingslib/bluetooth/Q;)V

    invoke-direct {p0, v0, v1}, Lcom/android/settingslib/bluetooth/s;->cky(Ljava/lang/String;Lcom/android/settingslib/bluetooth/I;)V

    const-string/jumbo v0, "android.bluetooth.device.action.ALIAS_CHANGED"

    new-instance v1, Lcom/android/settingslib/bluetooth/Q;

    invoke-direct {v1, p0, v4}, Lcom/android/settingslib/bluetooth/Q;-><init>(Lcom/android/settingslib/bluetooth/s;Lcom/android/settingslib/bluetooth/Q;)V

    invoke-direct {p0, v0, v1}, Lcom/android/settingslib/bluetooth/s;->cky(Ljava/lang/String;Lcom/android/settingslib/bluetooth/I;)V

    const-string/jumbo v0, "android.bluetooth.device.action.BOND_STATE_CHANGED"

    new-instance v1, Lcom/android/settingslib/bluetooth/R;

    invoke-direct {v1, p0, v4}, Lcom/android/settingslib/bluetooth/R;-><init>(Lcom/android/settingslib/bluetooth/s;Lcom/android/settingslib/bluetooth/R;)V

    invoke-direct {p0, v0, v1}, Lcom/android/settingslib/bluetooth/s;->cky(Ljava/lang/String;Lcom/android/settingslib/bluetooth/I;)V

    const-string/jumbo v0, "android.bluetooth.device.action.PAIRING_CANCEL"

    new-instance v1, Lcom/android/settingslib/bluetooth/U;

    invoke-direct {v1, p0, v4}, Lcom/android/settingslib/bluetooth/U;-><init>(Lcom/android/settingslib/bluetooth/s;Lcom/android/settingslib/bluetooth/U;)V

    invoke-direct {p0, v0, v1}, Lcom/android/settingslib/bluetooth/s;->cky(Ljava/lang/String;Lcom/android/settingslib/bluetooth/I;)V

    const-string/jumbo v0, "android.bluetooth.device.action.CLASS_CHANGED"

    new-instance v1, Lcom/android/settingslib/bluetooth/S;

    invoke-direct {v1, p0, v4}, Lcom/android/settingslib/bluetooth/S;-><init>(Lcom/android/settingslib/bluetooth/s;Lcom/android/settingslib/bluetooth/S;)V

    invoke-direct {p0, v0, v1}, Lcom/android/settingslib/bluetooth/s;->cky(Ljava/lang/String;Lcom/android/settingslib/bluetooth/I;)V

    const-string/jumbo v0, "android.bluetooth.device.action.UUID"

    new-instance v1, Lcom/android/settingslib/bluetooth/T;

    invoke-direct {v1, p0, v4}, Lcom/android/settingslib/bluetooth/T;-><init>(Lcom/android/settingslib/bluetooth/s;Lcom/android/settingslib/bluetooth/T;)V

    invoke-direct {p0, v0, v1}, Lcom/android/settingslib/bluetooth/s;->cky(Ljava/lang/String;Lcom/android/settingslib/bluetooth/I;)V

    const-string/jumbo v0, "android.intent.action.DOCK_EVENT"

    new-instance v1, Lcom/android/settingslib/bluetooth/V;

    invoke-direct {v1, p0, v4}, Lcom/android/settingslib/bluetooth/V;-><init>(Lcom/android/settingslib/bluetooth/s;Lcom/android/settingslib/bluetooth/V;)V

    invoke-direct {p0, v0, v1}, Lcom/android/settingslib/bluetooth/s;->cky(Ljava/lang/String;Lcom/android/settingslib/bluetooth/I;)V

    iget-object v0, p0, Lcom/android/settingslib/bluetooth/s;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/android/settingslib/bluetooth/s;->cEt:Landroid/content/BroadcastReceiver;

    iget-object v2, p0, Lcom/android/settingslib/bluetooth/s;->cEB:Landroid/content/IntentFilter;

    iget-object v3, p0, Lcom/android/settingslib/bluetooth/s;->cEx:Landroid/os/Handler;

    invoke-virtual {v0, v1, v2, v4, v3}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;Ljava/lang/String;Landroid/os/Handler;)Landroid/content/Intent;

    iget-object v0, p0, Lcom/android/settingslib/bluetooth/s;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/android/settingslib/bluetooth/s;->cEA:Landroid/content/BroadcastReceiver;

    iget-object v2, p0, Lcom/android/settingslib/bluetooth/s;->cEy:Landroid/content/IntentFilter;

    iget-object v3, p0, Lcom/android/settingslib/bluetooth/s;->cEx:Landroid/os/Handler;

    invoke-virtual {v0, v1, v2, v4, v3}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;Ljava/lang/String;Landroid/os/Handler;)Landroid/content/Intent;

    return-void
.end method

.method static synthetic ckC(Lcom/android/settingslib/bluetooth/s;Lcom/android/settingslib/bluetooth/b;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/android/settingslib/bluetooth/s;->ckG(Lcom/android/settingslib/bluetooth/b;I)V

    return-void
.end method

.method static synthetic ckE(Lcom/android/settingslib/bluetooth/s;)Lcom/android/settingslib/bluetooth/d;
    .locals 1

    iget-object v0, p0, Lcom/android/settingslib/bluetooth/s;->cEu:Lcom/android/settingslib/bluetooth/d;

    return-object v0
.end method

.method static synthetic ckF(Lcom/android/settingslib/bluetooth/s;)Ljava/util/Collection;
    .locals 1

    iget-object v0, p0, Lcom/android/settingslib/bluetooth/s;->cEw:Ljava/util/Collection;

    return-object v0
.end method

.method private ckG(Lcom/android/settingslib/bluetooth/b;I)V
    .locals 3

    iget-object v1, p0, Lcom/android/settingslib/bluetooth/s;->cEw:Ljava/util/Collection;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/android/settingslib/bluetooth/s;->cEw:Ljava/util/Collection;

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settingslib/bluetooth/a;

    invoke-interface {v0, p1, p2}, Lcom/android/settingslib/bluetooth/a;->aqK(Lcom/android/settingslib/bluetooth/b;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    :cond_0
    monitor-exit v1

    return-void
.end method

.method static synthetic ckH(Lcom/android/settingslib/bluetooth/s;)Landroid/content/BroadcastReceiver;
    .locals 1

    iget-object v0, p0, Lcom/android/settingslib/bluetooth/s;->cEA:Landroid/content/BroadcastReceiver;

    return-object v0
.end method

.method static synthetic ckv(Lcom/android/settingslib/bluetooth/s;)Lcom/android/settingslib/bluetooth/t;
    .locals 1

    iget-object v0, p0, Lcom/android/settingslib/bluetooth/s;->cEz:Lcom/android/settingslib/bluetooth/t;

    return-object v0
.end method

.method static synthetic ckx(Lcom/android/settingslib/bluetooth/s;)Ljava/util/Map;
    .locals 1

    iget-object v0, p0, Lcom/android/settingslib/bluetooth/s;->cEC:Ljava/util/Map;

    return-object v0
.end method

.method private cky(Ljava/lang/String;Lcom/android/settingslib/bluetooth/I;)V
    .locals 1

    iget-object v0, p0, Lcom/android/settingslib/bluetooth/s;->cEC:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/android/settingslib/bluetooth/s;->cEB:Landroid/content/IntentFilter;

    invoke-virtual {v0, p1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic ckz(Lcom/android/settingslib/bluetooth/s;)Lcom/android/settingslib/bluetooth/e;
    .locals 1

    iget-object v0, p0, Lcom/android/settingslib/bluetooth/s;->cEv:Lcom/android/settingslib/bluetooth/e;

    return-object v0
.end method


# virtual methods
.method ckA(Lcom/android/settingslib/bluetooth/b;)V
    .locals 3

    iget-object v1, p0, Lcom/android/settingslib/bluetooth/s;->cEw:Ljava/util/Collection;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/android/settingslib/bluetooth/s;->cEw:Ljava/util/Collection;

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settingslib/bluetooth/a;

    invoke-interface {v0, p1}, Lcom/android/settingslib/bluetooth/a;->aqs(Lcom/android/settingslib/bluetooth/b;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    :cond_0
    monitor-exit v1

    return-void
.end method

.method ckB(Lcom/android/settingslib/bluetooth/e;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settingslib/bluetooth/s;->cEv:Lcom/android/settingslib/bluetooth/e;

    return-void
.end method

.method ckD()V
    .locals 5

    iget-object v0, p0, Lcom/android/settingslib/bluetooth/s;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/android/settingslib/bluetooth/s;->cEA:Landroid/content/BroadcastReceiver;

    iget-object v2, p0, Lcom/android/settingslib/bluetooth/s;->cEy:Landroid/content/IntentFilter;

    iget-object v3, p0, Lcom/android/settingslib/bluetooth/s;->cEx:Landroid/os/Handler;

    const/4 v4, 0x0

    invoke-virtual {v0, v1, v2, v4, v3}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;Ljava/lang/String;Landroid/os/Handler;)Landroid/content/Intent;

    return-void
.end method

.method cks(Ljava/lang/String;Lcom/android/settingslib/bluetooth/I;)V
    .locals 1

    iget-object v0, p0, Lcom/android/settingslib/bluetooth/s;->cEC:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/android/settingslib/bluetooth/s;->cEy:Landroid/content/IntentFilter;

    invoke-virtual {v0, p1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    return-void
.end method

.method public ckt(Lcom/android/settingslib/bluetooth/a;)V
    .locals 2

    iget-object v1, p0, Lcom/android/settingslib/bluetooth/s;->cEw:Ljava/util/Collection;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/android/settingslib/bluetooth/s;->cEw:Ljava/util/Collection;

    invoke-interface {v0, p1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method cku()Z
    .locals 5

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/android/settingslib/bluetooth/s;->cEu:Lcom/android/settingslib/bluetooth/d;

    invoke-virtual {v1}, Lcom/android/settingslib/bluetooth/d;->cjB()Ljava/util/Set;

    move-result-object v1

    if-nez v1, :cond_0

    return v0

    :cond_0
    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/bluetooth/BluetoothDevice;

    iget-object v3, p0, Lcom/android/settingslib/bluetooth/s;->cEz:Lcom/android/settingslib/bluetooth/t;

    invoke-virtual {v3, v0}, Lcom/android/settingslib/bluetooth/t;->ckL(Landroid/bluetooth/BluetoothDevice;)Lcom/android/settingslib/bluetooth/b;

    move-result-object v3

    if-nez v3, :cond_2

    iget-object v1, p0, Lcom/android/settingslib/bluetooth/s;->cEz:Lcom/android/settingslib/bluetooth/t;

    iget-object v3, p0, Lcom/android/settingslib/bluetooth/s;->cEu:Lcom/android/settingslib/bluetooth/d;

    iget-object v4, p0, Lcom/android/settingslib/bluetooth/s;->cEv:Lcom/android/settingslib/bluetooth/e;

    invoke-virtual {v1, v3, v4, v0}, Lcom/android/settingslib/bluetooth/t;->ckP(Lcom/android/settingslib/bluetooth/d;Lcom/android/settingslib/bluetooth/e;Landroid/bluetooth/BluetoothDevice;)Lcom/android/settingslib/bluetooth/b;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/settingslib/bluetooth/s;->ckA(Lcom/android/settingslib/bluetooth/b;)V

    const/4 v0, 0x1

    :goto_1
    move v1, v0

    goto :goto_0

    :cond_1
    return v1

    :cond_2
    move v0, v1

    goto :goto_1
.end method

.method public ckw(Lcom/android/settingslib/bluetooth/a;)V
    .locals 2

    iget-object v1, p0, Lcom/android/settingslib/bluetooth/s;->cEw:Ljava/util/Collection;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/android/settingslib/bluetooth/s;->cEw:Ljava/util/Collection;

    invoke-interface {v0, p1}, Ljava/util/Collection;->remove(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method
