.class Lcom/android/settingslib/bluetooth/N;
.super Ljava/lang/Object;
.source "BluetoothEventManager.java"

# interfaces
.implements Lcom/android/settingslib/bluetooth/I;


# instance fields
.field final synthetic cFZ:Lcom/android/settingslib/bluetooth/s;


# direct methods
.method private constructor <init>(Lcom/android/settingslib/bluetooth/s;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settingslib/bluetooth/N;->cFZ:Lcom/android/settingslib/bluetooth/s;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/settingslib/bluetooth/s;Lcom/android/settingslib/bluetooth/N;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/settingslib/bluetooth/N;-><init>(Lcom/android/settingslib/bluetooth/s;)V

    return-void
.end method


# virtual methods
.method public ckW(Landroid/content/Context;Landroid/content/Intent;Landroid/bluetooth/BluetoothDevice;)V
    .locals 7

    const-string/jumbo v0, "android.bluetooth.device.extra.RSSI"

    const/16 v1, -0x8000

    invoke-virtual {p2, v0, v1}, Landroid/content/Intent;->getShortExtra(Ljava/lang/String;S)S

    move-result v2

    const-string/jumbo v0, "android.bluetooth.device.extra.CLASS"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/bluetooth/BluetoothClass;

    const-string/jumbo v1, "android.bluetooth.device.extra.NAME"

    invoke-virtual {p2, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iget-object v1, p0, Lcom/android/settingslib/bluetooth/N;->cFZ:Lcom/android/settingslib/bluetooth/s;

    invoke-static {v1}, Lcom/android/settingslib/bluetooth/s;->ckv(Lcom/android/settingslib/bluetooth/s;)Lcom/android/settingslib/bluetooth/t;

    move-result-object v1

    invoke-virtual {v1, p3}, Lcom/android/settingslib/bluetooth/t;->ckL(Landroid/bluetooth/BluetoothDevice;)Lcom/android/settingslib/bluetooth/b;

    move-result-object v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/android/settingslib/bluetooth/N;->cFZ:Lcom/android/settingslib/bluetooth/s;

    invoke-static {v1}, Lcom/android/settingslib/bluetooth/s;->ckv(Lcom/android/settingslib/bluetooth/s;)Lcom/android/settingslib/bluetooth/t;

    move-result-object v1

    iget-object v4, p0, Lcom/android/settingslib/bluetooth/N;->cFZ:Lcom/android/settingslib/bluetooth/s;

    invoke-static {v4}, Lcom/android/settingslib/bluetooth/s;->ckE(Lcom/android/settingslib/bluetooth/s;)Lcom/android/settingslib/bluetooth/d;

    move-result-object v4

    iget-object v5, p0, Lcom/android/settingslib/bluetooth/N;->cFZ:Lcom/android/settingslib/bluetooth/s;

    invoke-static {v5}, Lcom/android/settingslib/bluetooth/s;->ckz(Lcom/android/settingslib/bluetooth/s;)Lcom/android/settingslib/bluetooth/e;

    move-result-object v5

    invoke-virtual {v1, v4, v5, p3}, Lcom/android/settingslib/bluetooth/t;->ckP(Lcom/android/settingslib/bluetooth/d;Lcom/android/settingslib/bluetooth/e;Landroid/bluetooth/BluetoothDevice;)Lcom/android/settingslib/bluetooth/b;

    move-result-object v1

    const-string/jumbo v4, "BluetoothEventManager"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "DeviceFoundHandler created new CachedBluetoothDevice: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v4, p0, Lcom/android/settingslib/bluetooth/N;->cFZ:Lcom/android/settingslib/bluetooth/s;

    invoke-virtual {v4, v1}, Lcom/android/settingslib/bluetooth/s;->ckA(Lcom/android/settingslib/bluetooth/b;)V

    :cond_0
    invoke-virtual {v1, v2}, Lcom/android/settingslib/bluetooth/b;->ciU(S)V

    invoke-virtual {v1, v0}, Lcom/android/settingslib/bluetooth/b;->ciH(Landroid/bluetooth/BluetoothClass;)V

    invoke-virtual {v1, v3}, Lcom/android/settingslib/bluetooth/b;->cix(Ljava/lang/String;)V

    const/4 v0, 0x1

    invoke-virtual {v1, v0}, Lcom/android/settingslib/bluetooth/b;->setVisible(Z)V

    return-void
.end method
