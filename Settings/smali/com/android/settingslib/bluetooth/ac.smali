.class final Lcom/android/settingslib/bluetooth/ac;
.super Ljava/lang/Object;
.source "PbapServerProfile.java"

# interfaces
.implements Landroid/bluetooth/BluetoothPbap$ServiceListener;


# instance fields
.field final synthetic cGo:Lcom/android/settingslib/bluetooth/F;


# direct methods
.method private constructor <init>(Lcom/android/settingslib/bluetooth/F;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settingslib/bluetooth/ac;->cGo:Lcom/android/settingslib/bluetooth/F;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/settingslib/bluetooth/F;Lcom/android/settingslib/bluetooth/ac;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/settingslib/bluetooth/ac;-><init>(Lcom/android/settingslib/bluetooth/F;)V

    return-void
.end method


# virtual methods
.method public onServiceConnected(Landroid/bluetooth/BluetoothPbap;)V
    .locals 2

    invoke-static {}, Lcom/android/settingslib/bluetooth/F;->-get0()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string/jumbo v0, "PbapServerProfile"

    const-string/jumbo v1, "Bluetooth service connected"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v0, p0, Lcom/android/settingslib/bluetooth/ac;->cGo:Lcom/android/settingslib/bluetooth/F;

    invoke-static {v0, p1}, Lcom/android/settingslib/bluetooth/F;->clT(Lcom/android/settingslib/bluetooth/F;Landroid/bluetooth/BluetoothPbap;)Landroid/bluetooth/BluetoothPbap;

    iget-object v0, p0, Lcom/android/settingslib/bluetooth/ac;->cGo:Lcom/android/settingslib/bluetooth/F;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/android/settingslib/bluetooth/F;->clU(Lcom/android/settingslib/bluetooth/F;Z)Z

    return-void
.end method

.method public onServiceDisconnected()V
    .locals 2

    invoke-static {}, Lcom/android/settingslib/bluetooth/F;->-get0()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string/jumbo v0, "PbapServerProfile"

    const-string/jumbo v1, "Bluetooth service disconnected"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v0, p0, Lcom/android/settingslib/bluetooth/ac;->cGo:Lcom/android/settingslib/bluetooth/F;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/android/settingslib/bluetooth/F;->clU(Lcom/android/settingslib/bluetooth/F;Z)Z

    return-void
.end method
