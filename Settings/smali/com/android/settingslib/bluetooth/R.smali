.class Lcom/android/settingslib/bluetooth/R;
.super Ljava/lang/Object;
.source "BluetoothEventManager.java"

# interfaces
.implements Lcom/android/settingslib/bluetooth/I;


# instance fields
.field final synthetic cGd:Lcom/android/settingslib/bluetooth/s;


# direct methods
.method private constructor <init>(Lcom/android/settingslib/bluetooth/s;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settingslib/bluetooth/R;->cGd:Lcom/android/settingslib/bluetooth/s;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/settingslib/bluetooth/s;Lcom/android/settingslib/bluetooth/R;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/settingslib/bluetooth/R;-><init>(Lcom/android/settingslib/bluetooth/s;)V

    return-void
.end method

.method private cmn(Landroid/content/Context;Ljava/lang/String;I)V
    .locals 3

    packed-switch p3, :pswitch_data_0

    :pswitch_0
    const-string/jumbo v0, "BluetoothEventManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "showUnbondMessage: Not displaying any message for reason: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :pswitch_1
    sget v0, Lcom/android/settingslib/i;->cLV:I

    :goto_0
    invoke-static {p1, p2, v0}, Lcom/android/settingslib/bluetooth/J;->cmj(Landroid/content/Context;Ljava/lang/String;I)V

    return-void

    :pswitch_2
    sget v0, Lcom/android/settingslib/i;->cLW:I

    goto :goto_0

    :pswitch_3
    sget v0, Lcom/android/settingslib/i;->cLT:I

    goto :goto_0

    :pswitch_4
    sget v0, Lcom/android/settingslib/i;->cLU:I

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
    .end packed-switch
.end method


# virtual methods
.method public ckW(Landroid/content/Context;Landroid/content/Intent;Landroid/bluetooth/BluetoothDevice;)V
    .locals 6

    const/high16 v5, -0x80000000

    if-nez p3, :cond_0

    const-string/jumbo v0, "BluetoothEventManager"

    const-string/jumbo v1, "ACTION_BOND_STATE_CHANGED with no EXTRA_DEVICE"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :cond_0
    const-string/jumbo v0, "android.bluetooth.device.extra.BOND_STATE"

    invoke-virtual {p2, v0, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    iget-object v0, p0, Lcom/android/settingslib/bluetooth/R;->cGd:Lcom/android/settingslib/bluetooth/s;

    invoke-static {v0}, Lcom/android/settingslib/bluetooth/s;->ckv(Lcom/android/settingslib/bluetooth/s;)Lcom/android/settingslib/bluetooth/t;

    move-result-object v0

    invoke-virtual {v0, p3}, Lcom/android/settingslib/bluetooth/t;->ckL(Landroid/bluetooth/BluetoothDevice;)Lcom/android/settingslib/bluetooth/b;

    move-result-object v0

    if-nez v0, :cond_4

    const-string/jumbo v1, "BluetoothEventManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "CachedBluetoothDevice for device "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, " not found, calling readPairedDevices()."

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/android/settingslib/bluetooth/R;->cGd:Lcom/android/settingslib/bluetooth/s;

    invoke-virtual {v1}, Lcom/android/settingslib/bluetooth/s;->cku()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v0, p0, Lcom/android/settingslib/bluetooth/R;->cGd:Lcom/android/settingslib/bluetooth/s;

    invoke-static {v0}, Lcom/android/settingslib/bluetooth/s;->ckv(Lcom/android/settingslib/bluetooth/s;)Lcom/android/settingslib/bluetooth/t;

    move-result-object v0

    invoke-virtual {v0, p3}, Lcom/android/settingslib/bluetooth/t;->ckL(Landroid/bluetooth/BluetoothDevice;)Lcom/android/settingslib/bluetooth/b;

    move-result-object v0

    :cond_1
    if-nez v0, :cond_4

    const-string/jumbo v0, "BluetoothEventManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "Got bonding state changed for "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v3, ", but we have no record of that device."

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/settingslib/bluetooth/R;->cGd:Lcom/android/settingslib/bluetooth/s;

    invoke-static {v0}, Lcom/android/settingslib/bluetooth/s;->ckv(Lcom/android/settingslib/bluetooth/s;)Lcom/android/settingslib/bluetooth/t;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settingslib/bluetooth/R;->cGd:Lcom/android/settingslib/bluetooth/s;

    invoke-static {v1}, Lcom/android/settingslib/bluetooth/s;->ckE(Lcom/android/settingslib/bluetooth/s;)Lcom/android/settingslib/bluetooth/d;

    move-result-object v1

    iget-object v3, p0, Lcom/android/settingslib/bluetooth/R;->cGd:Lcom/android/settingslib/bluetooth/s;

    invoke-static {v3}, Lcom/android/settingslib/bluetooth/s;->ckz(Lcom/android/settingslib/bluetooth/s;)Lcom/android/settingslib/bluetooth/e;

    move-result-object v3

    invoke-virtual {v0, v1, v3, p3}, Lcom/android/settingslib/bluetooth/t;->ckP(Lcom/android/settingslib/bluetooth/d;Lcom/android/settingslib/bluetooth/e;Landroid/bluetooth/BluetoothDevice;)Lcom/android/settingslib/bluetooth/b;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settingslib/bluetooth/R;->cGd:Lcom/android/settingslib/bluetooth/s;

    invoke-virtual {v1, v0}, Lcom/android/settingslib/bluetooth/s;->ckA(Lcom/android/settingslib/bluetooth/b;)V

    move-object v1, v0

    :goto_0
    iget-object v0, p0, Lcom/android/settingslib/bluetooth/R;->cGd:Lcom/android/settingslib/bluetooth/s;

    invoke-static {v0}, Lcom/android/settingslib/bluetooth/s;->ckF(Lcom/android/settingslib/bluetooth/s;)Ljava/util/Collection;

    move-result-object v3

    monitor-enter v3

    :try_start_0
    iget-object v0, p0, Lcom/android/settingslib/bluetooth/R;->cGd:Lcom/android/settingslib/bluetooth/s;

    invoke-static {v0}, Lcom/android/settingslib/bluetooth/s;->ckF(Lcom/android/settingslib/bluetooth/s;)Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settingslib/bluetooth/a;

    invoke-interface {v0, v1, v2}, Lcom/android/settingslib/bluetooth/a;->aqt(Lcom/android/settingslib/bluetooth/b;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v0

    monitor-exit v3

    throw v0

    :cond_2
    monitor-exit v3

    invoke-virtual {v1, v2}, Lcom/android/settingslib/bluetooth/b;->cjt(I)V

    const/16 v0, 0xa

    if-ne v2, v0, :cond_3

    const-string/jumbo v0, "android.bluetooth.device.extra.REASON"

    invoke-virtual {p2, v0, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    invoke-virtual {v1}, Lcom/android/settingslib/bluetooth/b;->cjc()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, p1, v1, v0}, Lcom/android/settingslib/bluetooth/R;->cmn(Landroid/content/Context;Ljava/lang/String;I)V

    :cond_3
    return-void

    :cond_4
    move-object v1, v0

    goto :goto_0
.end method
