.class final Lcom/android/settingslib/b/G;
.super Landroid/content/pm/IPackageStatsObserver$Stub;
.source "ApplicationsState.java"


# instance fields
.field final synthetic cBC:Lcom/android/settingslib/b/d;


# direct methods
.method constructor <init>(Lcom/android/settingslib/b/d;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settingslib/b/G;->cBC:Lcom/android/settingslib/b/d;

    invoke-direct {p0}, Landroid/content/pm/IPackageStatsObserver$Stub;-><init>()V

    return-void
.end method


# virtual methods
.method public onGetStatsCompleted(Landroid/content/pm/PackageStats;Z)V
    .locals 14

    if-nez p2, :cond_0

    return-void

    :cond_0
    const/4 v1, 0x0

    iget-object v0, p0, Lcom/android/settingslib/b/G;->cBC:Lcom/android/settingslib/b/d;

    iget-object v0, v0, Lcom/android/settingslib/b/d;->cAR:Lcom/android/settingslib/b/a;

    iget-object v2, v0, Lcom/android/settingslib/b/a;->cAo:Landroid/util/SparseArray;

    monitor-enter v2

    :try_start_0
    iget-object v0, p0, Lcom/android/settingslib/b/G;->cBC:Lcom/android/settingslib/b/d;

    iget-object v0, v0, Lcom/android/settingslib/b/d;->cAR:Lcom/android/settingslib/b/a;

    iget-object v0, v0, Lcom/android/settingslib/b/a;->cAo:Landroid/util/SparseArray;

    iget v3, p1, Landroid/content/pm/PackageStats;->userHandle:I

    invoke-virtual {v0, v3}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/HashMap;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    if-nez v0, :cond_1

    monitor-exit v2

    return-void

    :cond_1
    :try_start_1
    iget-object v3, p1, Landroid/content/pm/PackageStats;->packageName:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settingslib/b/h;

    if-eqz v0, :cond_4

    monitor-enter v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    const/4 v3, 0x0

    :try_start_2
    iput-boolean v3, v0, Lcom/android/settingslib/b/h;->cBn:Z

    const-wide/16 v4, 0x0

    iput-wide v4, v0, Lcom/android/settingslib/b/h;->cBg:J

    iget-wide v4, p1, Landroid/content/pm/PackageStats;->externalCodeSize:J

    iget-wide v6, p1, Landroid/content/pm/PackageStats;->externalObbSize:J

    add-long/2addr v4, v6

    iget-wide v6, p1, Landroid/content/pm/PackageStats;->externalDataSize:J

    iget-wide v8, p1, Landroid/content/pm/PackageStats;->externalMediaSize:J

    add-long/2addr v6, v8

    add-long v8, v4, v6

    iget-object v3, p0, Lcom/android/settingslib/b/G;->cBC:Lcom/android/settingslib/b/d;

    iget-object v3, v3, Lcom/android/settingslib/b/d;->cAR:Lcom/android/settingslib/b/a;

    invoke-static {v3, p1}, Lcom/android/settingslib/b/a;->ceY(Lcom/android/settingslib/b/a;Landroid/content/pm/PackageStats;)J

    move-result-wide v10

    add-long/2addr v8, v10

    iget-wide v10, v0, Lcom/android/settingslib/b/h;->cBc:J

    cmp-long v3, v10, v8

    if-nez v3, :cond_2

    iget-wide v10, v0, Lcom/android/settingslib/b/h;->cBa:J

    iget-wide v12, p1, Landroid/content/pm/PackageStats;->cacheSize:J

    cmp-long v3, v10, v12

    if-eqz v3, :cond_6

    :cond_2
    :goto_0
    iput-wide v8, v0, Lcom/android/settingslib/b/h;->cBc:J

    iget-wide v8, p1, Landroid/content/pm/PackageStats;->cacheSize:J

    iput-wide v8, v0, Lcom/android/settingslib/b/h;->cBa:J

    iget-wide v8, p1, Landroid/content/pm/PackageStats;->codeSize:J

    iput-wide v8, v0, Lcom/android/settingslib/b/h;->cAW:J

    iget-wide v8, p1, Landroid/content/pm/PackageStats;->dataSize:J

    iput-wide v8, v0, Lcom/android/settingslib/b/h;->cAY:J

    iput-wide v4, v0, Lcom/android/settingslib/b/h;->cAX:J

    iput-wide v6, v0, Lcom/android/settingslib/b/h;->cAV:J

    iget-wide v4, p1, Landroid/content/pm/PackageStats;->externalCacheSize:J

    iput-wide v4, v0, Lcom/android/settingslib/b/h;->cAZ:J

    iget-object v1, p0, Lcom/android/settingslib/b/G;->cBC:Lcom/android/settingslib/b/d;

    iget-object v1, v1, Lcom/android/settingslib/b/d;->cAR:Lcom/android/settingslib/b/a;

    iget-wide v4, v0, Lcom/android/settingslib/b/h;->cBc:J

    invoke-static {v1, v4, v5}, Lcom/android/settingslib/b/a;->cfe(Lcom/android/settingslib/b/a;J)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/android/settingslib/b/h;->cBh:Ljava/lang/String;

    iget-object v1, p0, Lcom/android/settingslib/b/G;->cBC:Lcom/android/settingslib/b/d;

    iget-object v1, v1, Lcom/android/settingslib/b/d;->cAR:Lcom/android/settingslib/b/a;

    invoke-static {v1, p1}, Lcom/android/settingslib/b/a;->ceY(Lcom/android/settingslib/b/a;Landroid/content/pm/PackageStats;)J

    move-result-wide v4

    iput-wide v4, v0, Lcom/android/settingslib/b/h;->cBm:J

    iget-object v1, p0, Lcom/android/settingslib/b/G;->cBC:Lcom/android/settingslib/b/d;

    iget-object v1, v1, Lcom/android/settingslib/b/d;->cAR:Lcom/android/settingslib/b/a;

    iget-wide v4, v0, Lcom/android/settingslib/b/h;->cBm:J

    invoke-static {v1, v4, v5}, Lcom/android/settingslib/b/a;->cfe(Lcom/android/settingslib/b/a;J)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/android/settingslib/b/h;->cBf:Ljava/lang/String;

    iget-object v1, p0, Lcom/android/settingslib/b/G;->cBC:Lcom/android/settingslib/b/d;

    iget-object v1, v1, Lcom/android/settingslib/b/d;->cAR:Lcom/android/settingslib/b/a;

    invoke-static {v1, p1}, Lcom/android/settingslib/b/a;->ceO(Lcom/android/settingslib/b/a;Landroid/content/pm/PackageStats;)J

    move-result-wide v4

    iput-wide v4, v0, Lcom/android/settingslib/b/h;->cBs:J

    iget-object v1, p0, Lcom/android/settingslib/b/G;->cBC:Lcom/android/settingslib/b/d;

    iget-object v1, v1, Lcom/android/settingslib/b/d;->cAR:Lcom/android/settingslib/b/a;

    iget-wide v4, v0, Lcom/android/settingslib/b/h;->cBs:J

    invoke-static {v1, v4, v5}, Lcom/android/settingslib/b/a;->cfe(Lcom/android/settingslib/b/a;J)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/android/settingslib/b/h;->cBi:Ljava/lang/String;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    const/4 v1, 0x1

    :cond_3
    :try_start_3
    monitor-exit v0

    if-eqz v1, :cond_4

    iget-object v0, p0, Lcom/android/settingslib/b/G;->cBC:Lcom/android/settingslib/b/d;

    iget-object v0, v0, Lcom/android/settingslib/b/d;->cAR:Lcom/android/settingslib/b/a;

    iget-object v0, v0, Lcom/android/settingslib/b/a;->cAs:Lcom/android/settingslib/b/c;

    iget-object v1, p1, Landroid/content/pm/PackageStats;->packageName:Ljava/lang/String;

    const/4 v3, 0x4

    invoke-virtual {v0, v3, v1}, Lcom/android/settingslib/b/c;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settingslib/b/G;->cBC:Lcom/android/settingslib/b/d;

    iget-object v1, v1, Lcom/android/settingslib/b/d;->cAR:Lcom/android/settingslib/b/a;

    iget-object v1, v1, Lcom/android/settingslib/b/a;->cAs:Lcom/android/settingslib/b/c;

    invoke-virtual {v1, v0}, Lcom/android/settingslib/b/c;->sendMessage(Landroid/os/Message;)Z

    :cond_4
    iget-object v0, p0, Lcom/android/settingslib/b/G;->cBC:Lcom/android/settingslib/b/d;

    iget-object v0, v0, Lcom/android/settingslib/b/d;->cAR:Lcom/android/settingslib/b/a;

    iget-object v0, v0, Lcom/android/settingslib/b/a;->czP:Ljava/lang/String;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/android/settingslib/b/G;->cBC:Lcom/android/settingslib/b/d;

    iget-object v0, v0, Lcom/android/settingslib/b/d;->cAR:Lcom/android/settingslib/b/a;

    iget-object v0, v0, Lcom/android/settingslib/b/a;->czP:Ljava/lang/String;

    iget-object v1, p1, Landroid/content/pm/PackageStats;->packageName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/android/settingslib/b/G;->cBC:Lcom/android/settingslib/b/d;

    iget-object v0, v0, Lcom/android/settingslib/b/d;->cAR:Lcom/android/settingslib/b/a;

    iget v0, v0, Lcom/android/settingslib/b/a;->cAC:I

    iget v1, p1, Landroid/content/pm/PackageStats;->userHandle:I

    if-ne v0, v1, :cond_5

    iget-object v0, p0, Lcom/android/settingslib/b/G;->cBC:Lcom/android/settingslib/b/d;

    iget-object v0, v0, Lcom/android/settingslib/b/d;->cAR:Lcom/android/settingslib/b/a;

    const/4 v1, 0x0

    iput-object v1, v0, Lcom/android/settingslib/b/a;->czP:Ljava/lang/String;

    iget-object v0, p0, Lcom/android/settingslib/b/G;->cBC:Lcom/android/settingslib/b/d;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/android/settingslib/b/d;->sendEmptyMessage(I)Z
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    :cond_5
    monitor-exit v2

    return-void

    :cond_6
    :try_start_4
    iget-wide v10, v0, Lcom/android/settingslib/b/h;->cAW:J

    iget-wide v12, p1, Landroid/content/pm/PackageStats;->codeSize:J

    cmp-long v3, v10, v12

    if-nez v3, :cond_2

    iget-wide v10, v0, Lcom/android/settingslib/b/h;->cAY:J

    iget-wide v12, p1, Landroid/content/pm/PackageStats;->dataSize:J

    cmp-long v3, v10, v12

    if-nez v3, :cond_2

    iget-wide v10, v0, Lcom/android/settingslib/b/h;->cAX:J

    cmp-long v3, v10, v4

    if-nez v3, :cond_2

    iget-wide v10, v0, Lcom/android/settingslib/b/h;->cAV:J

    cmp-long v3, v10, v6

    if-nez v3, :cond_2

    iget-wide v10, v0, Lcom/android/settingslib/b/h;->cAZ:J

    iget-wide v12, p1, Landroid/content/pm/PackageStats;->externalCacheSize:J
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    cmp-long v3, v10, v12

    if-eqz v3, :cond_3

    goto/16 :goto_0

    :catchall_0
    move-exception v1

    :try_start_5
    monitor-exit v0

    throw v1
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    :catchall_1
    move-exception v0

    monitor-exit v2

    throw v0
.end method
