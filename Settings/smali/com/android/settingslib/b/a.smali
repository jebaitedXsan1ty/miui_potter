.class public Lcom/android/settingslib/b/a;
.super Ljava/lang/Object;
.source "ApplicationsState.java"


# static fields
.field static final REMOVE_DIACRITICALS_PATTERN:Ljava/util/regex/Pattern;

.field public static final cAD:Lcom/android/settingslib/b/i;

.field public static final cAE:Lcom/android/settingslib/b/i;

.field public static final cAa:Ljava/util/Comparator;

.field public static final cAb:Lcom/android/settingslib/b/i;

.field public static final cAd:Ljava/util/Comparator;

.field public static final cAe:Lcom/android/settingslib/b/i;

.field public static final cAj:Lcom/android/settingslib/b/i;

.field public static final cAm:Lcom/android/settingslib/b/i;

.field public static final cAn:Lcom/android/settingslib/b/i;

.field public static final cAp:Lcom/android/settingslib/b/i;

.field public static final cAq:Lcom/android/settingslib/b/i;

.field static final cAr:Ljava/lang/Object;

.field public static final cAt:Lcom/android/settingslib/b/i;

.field public static final cAu:Ljava/util/Comparator;

.field public static final cAw:Lcom/android/settingslib/b/i;

.field public static final cAx:Lcom/android/settingslib/b/i;

.field public static final cAy:Lcom/android/settingslib/b/i;

.field static czO:Lcom/android/settingslib/b/a;

.field public static final czR:Lcom/android/settingslib/b/i;

.field public static final czS:Ljava/util/Comparator;

.field public static final czV:Lcom/android/settingslib/b/i;

.field public static final czZ:Lcom/android/settingslib/b/i;


# instance fields
.field final cAA:Lcom/android/settingslib/b/d;

.field cAB:Z

.field cAC:I

.field cAc:Ljava/util/List;

.field cAf:Z

.field cAg:Z

.field final cAh:Ljava/util/ArrayList;

.field cAi:Lcom/android/settingslib/b/e;

.field final cAk:Landroid/app/usage/StorageStatsManager;

.field final cAl:Lcom/android/settingslib/b/F;

.field final cAo:Landroid/util/SparseArray;

.field final cAs:Lcom/android/settingslib/b/c;

.field final cAv:I

.field cAz:J

.field final czJ:Ljava/util/ArrayList;

.field final czK:Ljava/util/ArrayList;

.field final czL:Landroid/util/IconDrawableFactory;

.field final czM:Landroid/content/pm/IPackageManager;

.field final czN:Landroid/os/UserManager;

.field czP:Ljava/lang/String;

.field final czQ:Landroid/content/pm/PackageManager;

.field czT:Z

.field final czU:Ljava/util/ArrayList;

.field final czW:I

.field final czX:Landroid/os/HandlerThread;

.field czY:Ljava/util/UUID;

.field final mContext:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string/jumbo v0, "\\p{InCombiningDiacriticalMarks}+"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/android/settingslib/b/a;->REMOVE_DIACRITICALS_PATTERN:Ljava/util/regex/Pattern;

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/android/settingslib/b/a;->cAr:Ljava/lang/Object;

    new-instance v0, Lcom/android/settingslib/b/l;

    invoke-direct {v0}, Lcom/android/settingslib/b/l;-><init>()V

    sput-object v0, Lcom/android/settingslib/b/a;->cAa:Ljava/util/Comparator;

    new-instance v0, Lcom/android/settingslib/b/m;

    invoke-direct {v0}, Lcom/android/settingslib/b/m;-><init>()V

    sput-object v0, Lcom/android/settingslib/b/a;->cAu:Ljava/util/Comparator;

    new-instance v0, Lcom/android/settingslib/b/n;

    invoke-direct {v0}, Lcom/android/settingslib/b/n;-><init>()V

    sput-object v0, Lcom/android/settingslib/b/a;->czS:Ljava/util/Comparator;

    new-instance v0, Lcom/android/settingslib/b/o;

    invoke-direct {v0}, Lcom/android/settingslib/b/o;-><init>()V

    sput-object v0, Lcom/android/settingslib/b/a;->cAd:Ljava/util/Comparator;

    new-instance v0, Lcom/android/settingslib/b/p;

    invoke-direct {v0}, Lcom/android/settingslib/b/p;-><init>()V

    sput-object v0, Lcom/android/settingslib/b/a;->czR:Lcom/android/settingslib/b/i;

    new-instance v0, Lcom/android/settingslib/b/q;

    invoke-direct {v0}, Lcom/android/settingslib/b/q;-><init>()V

    sput-object v0, Lcom/android/settingslib/b/a;->cAx:Lcom/android/settingslib/b/i;

    new-instance v0, Lcom/android/settingslib/b/r;

    invoke-direct {v0}, Lcom/android/settingslib/b/r;-><init>()V

    sput-object v0, Lcom/android/settingslib/b/a;->cAp:Lcom/android/settingslib/b/i;

    new-instance v0, Lcom/android/settingslib/b/s;

    invoke-direct {v0}, Lcom/android/settingslib/b/s;-><init>()V

    sput-object v0, Lcom/android/settingslib/b/a;->cAe:Lcom/android/settingslib/b/i;

    new-instance v0, Lcom/android/settingslib/b/t;

    invoke-direct {v0}, Lcom/android/settingslib/b/t;-><init>()V

    sput-object v0, Lcom/android/settingslib/b/a;->cAt:Lcom/android/settingslib/b/i;

    new-instance v0, Lcom/android/settingslib/b/u;

    invoke-direct {v0}, Lcom/android/settingslib/b/u;-><init>()V

    sput-object v0, Lcom/android/settingslib/b/a;->cAb:Lcom/android/settingslib/b/i;

    new-instance v0, Lcom/android/settingslib/b/v;

    invoke-direct {v0}, Lcom/android/settingslib/b/v;-><init>()V

    sput-object v0, Lcom/android/settingslib/b/a;->czZ:Lcom/android/settingslib/b/i;

    new-instance v0, Lcom/android/settingslib/b/w;

    invoke-direct {v0}, Lcom/android/settingslib/b/w;-><init>()V

    sput-object v0, Lcom/android/settingslib/b/a;->cAm:Lcom/android/settingslib/b/i;

    new-instance v0, Lcom/android/settingslib/b/x;

    invoke-direct {v0}, Lcom/android/settingslib/b/x;-><init>()V

    sput-object v0, Lcom/android/settingslib/b/a;->cAy:Lcom/android/settingslib/b/i;

    new-instance v0, Lcom/android/settingslib/b/y;

    invoke-direct {v0}, Lcom/android/settingslib/b/y;-><init>()V

    sput-object v0, Lcom/android/settingslib/b/a;->cAj:Lcom/android/settingslib/b/i;

    new-instance v0, Lcom/android/settingslib/b/z;

    invoke-direct {v0}, Lcom/android/settingslib/b/z;-><init>()V

    sput-object v0, Lcom/android/settingslib/b/a;->cAn:Lcom/android/settingslib/b/i;

    new-instance v0, Lcom/android/settingslib/b/A;

    invoke-direct {v0}, Lcom/android/settingslib/b/A;-><init>()V

    sput-object v0, Lcom/android/settingslib/b/a;->cAD:Lcom/android/settingslib/b/i;

    new-instance v0, Lcom/android/settingslib/b/B;

    invoke-direct {v0}, Lcom/android/settingslib/b/B;-><init>()V

    sput-object v0, Lcom/android/settingslib/b/a;->czV:Lcom/android/settingslib/b/i;

    new-instance v0, Lcom/android/settingslib/b/C;

    invoke-direct {v0}, Lcom/android/settingslib/b/C;-><init>()V

    sput-object v0, Lcom/android/settingslib/b/a;->cAw:Lcom/android/settingslib/b/i;

    new-instance v0, Lcom/android/settingslib/b/D;

    invoke-direct {v0}, Lcom/android/settingslib/b/D;-><init>()V

    sput-object v0, Lcom/android/settingslib/b/a;->cAq:Lcom/android/settingslib/b/i;

    new-instance v0, Lcom/android/settingslib/b/E;

    invoke-direct {v0}, Lcom/android/settingslib/b/E;-><init>()V

    sput-object v0, Lcom/android/settingslib/b/a;->cAE:Lcom/android/settingslib/b/i;

    return-void
.end method

.method private constructor <init>(Landroid/app/Application;)V
    .locals 6

    const-wide/16 v2, 0x1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/settingslib/b/a;->czU:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/settingslib/b/a;->cAh:Ljava/util/ArrayList;

    new-instance v0, Lcom/android/settingslib/b/F;

    invoke-direct {v0}, Lcom/android/settingslib/b/F;-><init>()V

    iput-object v0, p0, Lcom/android/settingslib/b/a;->cAl:Lcom/android/settingslib/b/F;

    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/android/settingslib/b/a;->cAo:Landroid/util/SparseArray;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/settingslib/b/a;->czK:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/settingslib/b/a;->cAc:Ljava/util/List;

    iput-wide v2, p0, Lcom/android/settingslib/b/a;->cAz:J

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/settingslib/b/a;->czJ:Ljava/util/ArrayList;

    new-instance v0, Lcom/android/settingslib/b/c;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/android/settingslib/b/c;-><init>(Lcom/android/settingslib/b/a;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/android/settingslib/b/a;->cAs:Lcom/android/settingslib/b/c;

    iput-object p1, p0, Lcom/android/settingslib/b/a;->mContext:Landroid/content/Context;

    iget-object v0, p0, Lcom/android/settingslib/b/a;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settingslib/b/a;->czQ:Landroid/content/pm/PackageManager;

    iget-object v0, p0, Lcom/android/settingslib/b/a;->mContext:Landroid/content/Context;

    invoke-static {v0}, Landroid/util/IconDrawableFactory;->newInstance(Landroid/content/Context;)Landroid/util/IconDrawableFactory;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settingslib/b/a;->czL:Landroid/util/IconDrawableFactory;

    invoke-static {}, Landroid/app/AppGlobals;->getPackageManager()Landroid/content/pm/IPackageManager;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settingslib/b/a;->czM:Landroid/content/pm/IPackageManager;

    iget-object v0, p0, Lcom/android/settingslib/b/a;->mContext:Landroid/content/Context;

    const-class v1, Landroid/os/UserManager;

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/UserManager;

    iput-object v0, p0, Lcom/android/settingslib/b/a;->czN:Landroid/os/UserManager;

    iget-object v0, p0, Lcom/android/settingslib/b/a;->mContext:Landroid/content/Context;

    const-class v1, Landroid/app/usage/StorageStatsManager;

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/usage/StorageStatsManager;

    iput-object v0, p0, Lcom/android/settingslib/b/a;->cAk:Landroid/app/usage/StorageStatsManager;

    iget-object v0, p0, Lcom/android/settingslib/b/a;->czN:Landroid/os/UserManager;

    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/os/UserManager;->getProfileIdsWithDisabled(I)[I

    move-result-object v1

    const/4 v0, 0x0

    array-length v2, v1

    :goto_0
    if-ge v0, v2, :cond_0

    aget v3, v1, v0

    iget-object v4, p0, Lcom/android/settingslib/b/a;->cAo:Landroid/util/SparseArray;

    new-instance v5, Ljava/util/HashMap;

    invoke-direct {v5}, Ljava/util/HashMap;-><init>()V

    invoke-virtual {v4, v3, v5}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    new-instance v0, Landroid/os/HandlerThread;

    const-string/jumbo v1, "ApplicationsState.Loader"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;I)V

    iput-object v0, p0, Lcom/android/settingslib/b/a;->czX:Landroid/os/HandlerThread;

    iget-object v0, p0, Lcom/android/settingslib/b/a;->czX:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    new-instance v0, Lcom/android/settingslib/b/d;

    iget-object v1, p0, Lcom/android/settingslib/b/a;->czX:Landroid/os/HandlerThread;

    invoke-virtual {v1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/android/settingslib/b/d;-><init>(Lcom/android/settingslib/b/a;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/android/settingslib/b/a;->cAA:Lcom/android/settingslib/b/d;

    const v0, 0x408280

    iput v0, p0, Lcom/android/settingslib/b/a;->czW:I

    const v0, 0x8280

    iput v0, p0, Lcom/android/settingslib/b/a;->cAv:I

    iget-object v1, p0, Lcom/android/settingslib/b/a;->cAo:Landroid/util/SparseArray;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/android/settingslib/b/a;->cAo:Landroid/util/SparseArray;

    const-wide/16 v2, 0x1

    invoke-virtual {v0, v2, v3}, Landroid/util/SparseArray;->wait(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_1
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    :catch_0
    move-exception v0

    goto :goto_1
.end method

.method static synthetic ceK(Lcom/android/settingslib/b/a;Landroid/content/pm/ApplicationInfo;)Lcom/android/settingslib/b/h;
    .locals 1

    invoke-direct {p0, p1}, Lcom/android/settingslib/b/a;->cfc(Landroid/content/pm/ApplicationInfo;)Lcom/android/settingslib/b/h;

    move-result-object v0

    return-object v0
.end method

.method static synthetic ceO(Lcom/android/settingslib/b/a;Landroid/content/pm/PackageStats;)J
    .locals 2

    invoke-direct {p0, p1}, Lcom/android/settingslib/b/a;->cfm(Landroid/content/pm/PackageStats;)J

    move-result-wide v0

    return-wide v0
.end method

.method private ceP(J)Ljava/lang/String;
    .locals 3

    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-ltz v0, :cond_0

    iget-object v0, p0, Lcom/android/settingslib/b/a;->mContext:Landroid/content/Context;

    invoke-static {v0, p1, p2}, Landroid/text/format/Formatter;->formatFileSize(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method private ceQ(I)V
    .locals 3

    iget-object v0, p0, Lcom/android/settingslib/b/a;->czN:Landroid/os/UserManager;

    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/os/UserManager;->getProfileIdsWithDisabled(I)[I

    move-result-object v0

    invoke-static {v0, p1}, Lcom/android/internal/util/ArrayUtils;->contains([II)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v1, p0, Lcom/android/settingslib/b/a;->cAo:Landroid/util/SparseArray;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/android/settingslib/b/a;->cAo:Landroid/util/SparseArray;

    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    invoke-virtual {v0, p1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    iget-boolean v0, p0, Lcom/android/settingslib/b/a;->cAB:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/android/settingslib/b/a;->ceM()V

    invoke-virtual {p0}, Lcom/android/settingslib/b/a;->ceT()V

    :cond_0
    iget-object v0, p0, Lcom/android/settingslib/b/a;->cAs:Lcom/android/settingslib/b/c;

    const/4 v2, 0x2

    invoke-virtual {v0, v2}, Lcom/android/settingslib/b/c;->hasMessages(I)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/android/settingslib/b/a;->cAs:Lcom/android/settingslib/b/c;

    const/4 v2, 0x2

    invoke-virtual {v0, v2}, Lcom/android/settingslib/b/c;->sendEmptyMessage(I)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_1
    monitor-exit v1

    :cond_2
    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static ceS(Landroid/app/Application;)Lcom/android/settingslib/b/a;
    .locals 2

    sget-object v1, Lcom/android/settingslib/b/a;->cAr:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/android/settingslib/b/a;->czO:Lcom/android/settingslib/b/a;

    if-nez v0, :cond_0

    new-instance v0, Lcom/android/settingslib/b/a;

    invoke-direct {v0, p0}, Lcom/android/settingslib/b/a;-><init>(Landroid/app/Application;)V

    sput-object v0, Lcom/android/settingslib/b/a;->czO:Lcom/android/settingslib/b/a;

    :cond_0
    sget-object v0, Lcom/android/settingslib/b/a;->czO:Lcom/android/settingslib/b/a;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static ceW(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    sget-object v0, Ljava/text/Normalizer$Form;->NFD:Ljava/text/Normalizer$Form;

    invoke-static {p0, v0}, Ljava/text/Normalizer;->normalize(Ljava/lang/CharSequence;Ljava/text/Normalizer$Form;)Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/android/settingslib/b/a;->REMOVE_DIACRITICALS_PATTERN:Ljava/util/regex/Pattern;

    invoke-virtual {v1, v0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    const-string/jumbo v1, ""

    invoke-virtual {v0, v1}, Ljava/util/regex/Matcher;->replaceAll(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic ceX(Lcom/android/settingslib/b/a;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/settingslib/b/a;->ceQ(I)V

    return-void
.end method

.method static synthetic ceY(Lcom/android/settingslib/b/a;Landroid/content/pm/PackageStats;)J
    .locals 2

    invoke-direct {p0, p1}, Lcom/android/settingslib/b/a;->cfi(Landroid/content/pm/PackageStats;)J

    move-result-wide v0

    return-wide v0
.end method

.method private cfa(I)V
    .locals 4

    iget-object v1, p0, Lcom/android/settingslib/b/a;->cAo:Landroid/util/SparseArray;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/android/settingslib/b/a;->cAo:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/HashMap;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settingslib/b/h;

    iget-object v3, p0, Lcom/android/settingslib/b/a;->czK:Ljava/util/ArrayList;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    iget-object v3, p0, Lcom/android/settingslib/b/a;->cAc:Ljava/util/List;

    iget-object v0, v0, Lcom/android/settingslib/b/h;->info:Landroid/content/pm/ApplicationInfo;

    invoke-interface {v3, v0}, Ljava/util/List;->remove(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/android/settingslib/b/a;->cAo:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->remove(I)V

    iget-object v0, p0, Lcom/android/settingslib/b/a;->cAs:Lcom/android/settingslib/b/c;

    const/4 v2, 0x2

    invoke-virtual {v0, v2}, Lcom/android/settingslib/b/c;->hasMessages(I)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/android/settingslib/b/a;->cAs:Lcom/android/settingslib/b/c;

    const/4 v2, 0x2

    invoke-virtual {v0, v2}, Lcom/android/settingslib/b/c;->sendEmptyMessage(I)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_1
    monitor-exit v1

    return-void
.end method

.method private cfb()V
    .locals 2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/android/settingslib/b/a;->cAo:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    iget-object v0, p0, Lcom/android/settingslib/b/a;->cAo:Landroid/util/SparseArray;

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/android/settingslib/b/a;->czK:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    return-void
.end method

.method private cfc(Landroid/content/pm/ApplicationInfo;)Lcom/android/settingslib/b/h;
    .locals 8

    iget v0, p1, Landroid/content/pm/ApplicationInfo;->uid:I

    invoke-static {v0}, Landroid/os/UserHandle;->getUserId(I)I

    move-result v2

    iget-object v0, p0, Lcom/android/settingslib/b/a;->cAo:Landroid/util/SparseArray;

    invoke-virtual {v0, v2}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/HashMap;

    iget-object v1, p1, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settingslib/b/h;

    if-nez v0, :cond_1

    new-instance v1, Lcom/android/settingslib/b/h;

    iget-object v0, p0, Lcom/android/settingslib/b/a;->mContext:Landroid/content/Context;

    iget-wide v4, p0, Lcom/android/settingslib/b/a;->cAz:J

    const-wide/16 v6, 0x1

    add-long/2addr v6, v4

    iput-wide v6, p0, Lcom/android/settingslib/b/a;->cAz:J

    invoke-direct {v1, v0, p1, v4, v5}, Lcom/android/settingslib/b/h;-><init>(Landroid/content/Context;Landroid/content/pm/ApplicationInfo;J)V

    iget-object v0, p0, Lcom/android/settingslib/b/a;->cAo:Landroid/util/SparseArray;

    invoke-virtual {v0, v2}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/HashMap;

    iget-object v2, p1, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v0, v2, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/android/settingslib/b/a;->czK:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move-object v0, v1

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    iget-object v1, v0, Lcom/android/settingslib/b/h;->info:Landroid/content/pm/ApplicationInfo;

    if-eq v1, p1, :cond_0

    iput-object p1, v0, Lcom/android/settingslib/b/h;->info:Landroid/content/pm/ApplicationInfo;

    goto :goto_0
.end method

.method private cfd(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;
    .locals 3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/android/settingslib/b/a;->cAc:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    iget-object v0, p0, Lcom/android/settingslib/b/a;->cAc:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/ApplicationInfo;

    iget-object v2, v0, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget v2, v0, Landroid/content/pm/ApplicationInfo;->uid:I

    invoke-static {v2}, Landroid/os/UserHandle;->getUserId(I)I

    move-result v2

    if-ne p2, v2, :cond_0

    return-object v0

    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    return-object v0
.end method

.method static synthetic cfe(Lcom/android/settingslib/b/a;J)Ljava/lang/String;
    .locals 1

    invoke-direct {p0, p1, p2}, Lcom/android/settingslib/b/a;->ceP(J)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private cfi(Landroid/content/pm/PackageStats;)J
    .locals 4

    if-eqz p1, :cond_0

    iget-wide v0, p1, Landroid/content/pm/PackageStats;->codeSize:J

    iget-wide v2, p1, Landroid/content/pm/PackageStats;->dataSize:J

    add-long/2addr v0, v2

    return-wide v0

    :cond_0
    const-wide/16 v0, -0x2

    return-wide v0
.end method

.method private cfm(Landroid/content/pm/PackageStats;)J
    .locals 4

    if-eqz p1, :cond_0

    iget-wide v0, p1, Landroid/content/pm/PackageStats;->externalCodeSize:J

    iget-wide v2, p1, Landroid/content/pm/PackageStats;->externalDataSize:J

    add-long/2addr v0, v2

    iget-wide v2, p1, Landroid/content/pm/PackageStats;->externalCacheSize:J

    add-long/2addr v0, v2

    iget-wide v2, p1, Landroid/content/pm/PackageStats;->externalMediaSize:J

    add-long/2addr v0, v2

    iget-wide v2, p1, Landroid/content/pm/PackageStats;->externalObbSize:J

    add-long/2addr v0, v2

    return-wide v0

    :cond_0
    const-wide/16 v0, -0x2

    return-wide v0
.end method

.method static synthetic cfn(Lcom/android/settingslib/b/a;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/settingslib/b/a;->cfa(I)V

    return-void
.end method

.method private cfp(Ljava/util/List;)V
    .locals 4

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/ApplicationInfo;

    iget v2, v0, Landroid/content/pm/ApplicationInfo;->uid:I

    invoke-static {v2}, Lmiui/securityspace/XSpaceUserHandle;->isUidBelongtoXSpace(I)Z

    move-result v2

    if-eqz v2, :cond_0

    sget-object v2, Lmiui/securityspace/XSpaceConstant;->REQUIRED_APPS:Ljava/util/ArrayList;

    iget-object v3, v0, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    const-string/jumbo v2, "com.xiaomi.xmsf"

    iget-object v0, v0, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    :cond_2
    return-void
.end method

.method static synthetic cfr(Lcom/android/settingslib/b/a;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/settingslib/b/a;->cfb()V

    return-void
.end method


# virtual methods
.method ceL()V
    .locals 2

    iget-boolean v0, p0, Lcom/android/settingslib/b/a;->cAB:Z

    if-nez v0, :cond_0

    return-void

    :cond_0
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/android/settingslib/b/a;->czU:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    iget-object v0, p0, Lcom/android/settingslib/b/a;->czU:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settingslib/b/b;

    iget-boolean v0, v0, Lcom/android/settingslib/b/b;->cAO:Z

    if-eqz v0, :cond_1

    return-void

    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Lcom/android/settingslib/b/a;->ceM()V

    return-void
.end method

.method ceM()V
    .locals 2

    const/4 v1, 0x0

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/settingslib/b/a;->cAB:Z

    iget-object v0, p0, Lcom/android/settingslib/b/a;->cAi:Lcom/android/settingslib/b/e;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settingslib/b/a;->cAi:Lcom/android/settingslib/b/e;

    invoke-virtual {v0}, Lcom/android/settingslib/b/e;->cfA()V

    iput-object v1, p0, Lcom/android/settingslib/b/a;->cAi:Lcom/android/settingslib/b/e;

    :cond_0
    return-void
.end method

.method public ceN(Lcom/android/settingslib/b/h;)V
    .locals 2

    iget-object v0, p1, Lcom/android/settingslib/b/h;->icon:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    return-void

    :cond_0
    monitor-enter p1

    :try_start_0
    iget-object v0, p0, Lcom/android/settingslib/b/a;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/android/settingslib/b/a;->czL:Landroid/util/IconDrawableFactory;

    invoke-virtual {p1, v0, v1}, Lcom/android/settingslib/b/h;->cfD(Landroid/content/Context;Landroid/util/IconDrawableFactory;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p1

    throw v0
.end method

.method public ceR()Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/settingslib/b/a;->cAg:Z

    return v0
.end method

.method ceT()V
    .locals 9

    const/4 v8, 0x2

    const/4 v7, 0x0

    const/4 v6, 0x1

    const/4 v2, 0x0

    iget-boolean v0, p0, Lcom/android/settingslib/b/a;->cAB:Z

    if-eqz v0, :cond_0

    return-void

    :cond_0
    iput-boolean v6, p0, Lcom/android/settingslib/b/a;->cAB:Z

    iget-object v0, p0, Lcom/android/settingslib/b/a;->cAi:Lcom/android/settingslib/b/e;

    if-nez v0, :cond_1

    new-instance v0, Lcom/android/settingslib/b/e;

    invoke-direct {v0, p0, v7}, Lcom/android/settingslib/b/e;-><init>(Lcom/android/settingslib/b/a;Lcom/android/settingslib/b/e;)V

    iput-object v0, p0, Lcom/android/settingslib/b/a;->cAi:Lcom/android/settingslib/b/e;

    iget-object v0, p0, Lcom/android/settingslib/b/a;->cAi:Lcom/android/settingslib/b/e;

    invoke-virtual {v0}, Lcom/android/settingslib/b/e;->cfB()V

    :cond_1
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/settingslib/b/a;->cAc:Ljava/util/List;

    iget-object v0, p0, Lcom/android/settingslib/b/a;->czN:Landroid/os/UserManager;

    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/os/UserManager;->getProfiles(I)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/UserInfo;

    :try_start_0
    iget-object v3, p0, Lcom/android/settingslib/b/a;->cAo:Landroid/util/SparseArray;

    iget v4, v0, Landroid/content/pm/UserInfo;->id:I

    invoke-virtual {v3, v4}, Landroid/util/SparseArray;->indexOfKey(I)I

    move-result v3

    if-gez v3, :cond_2

    iget-object v3, p0, Lcom/android/settingslib/b/a;->cAo:Landroid/util/SparseArray;

    iget v4, v0, Landroid/content/pm/UserInfo;->id:I

    new-instance v5, Ljava/util/HashMap;

    invoke-direct {v5}, Ljava/util/HashMap;-><init>()V

    invoke-virtual {v3, v4, v5}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    :cond_2
    iget-object v3, p0, Lcom/android/settingslib/b/a;->czM:Landroid/content/pm/IPackageManager;

    iget v4, p0, Lcom/android/settingslib/b/a;->cAv:I

    iget v0, v0, Landroid/content/pm/UserInfo;->id:I

    invoke-interface {v3, v4, v0}, Landroid/content/pm/IPackageManager;->getInstalledApplications(II)Landroid/content/pm/ParceledListSlice;

    move-result-object v0

    iget-object v3, p0, Lcom/android/settingslib/b/a;->cAc:Ljava/util/List;

    invoke-virtual {v0}, Landroid/content/pm/ParceledListSlice;->getList()Ljava/util/List;

    move-result-object v0

    invoke-interface {v3, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/android/settingslib/b/a;->cAl:Lcom/android/settingslib/b/F;

    iget-object v1, p0, Lcom/android/settingslib/b/a;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/settingslib/b/F;->cfL(Landroid/content/res/Resources;)Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-direct {p0}, Lcom/android/settingslib/b/a;->cfb()V

    :cond_4
    iput-boolean v2, p0, Lcom/android/settingslib/b/a;->cAg:Z

    iput-boolean v2, p0, Lcom/android/settingslib/b/a;->czT:Z

    :goto_1
    iget-object v0, p0, Lcom/android/settingslib/b/a;->cAc:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_a

    iget-object v0, p0, Lcom/android/settingslib/b/a;->cAc:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/ApplicationInfo;

    iget-boolean v1, v0, Landroid/content/pm/ApplicationInfo;->enabled:Z

    if-nez v1, :cond_8

    iget v1, v0, Landroid/content/pm/ApplicationInfo;->enabledSetting:I

    const/4 v3, 0x3

    if-eq v1, v3, :cond_7

    iget-object v0, p0, Lcom/android/settingslib/b/a;->cAc:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    add-int/lit8 v2, v2, -0x1

    :cond_5
    :goto_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_6
    move v1, v2

    :goto_3
    iget-object v0, p0, Lcom/android/settingslib/b/a;->czK:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_4

    iget-object v0, p0, Lcom/android/settingslib/b/a;->czK:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settingslib/b/h;

    iput-boolean v6, v0, Lcom/android/settingslib/b/h;->cBn:Z

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_3

    :cond_7
    iput-boolean v6, p0, Lcom/android/settingslib/b/a;->cAg:Z

    :cond_8
    iget-boolean v1, p0, Lcom/android/settingslib/b/a;->czT:Z

    if-nez v1, :cond_9

    invoke-static {v0}, Lcom/android/settingslib/b/L;->cfT(Landroid/content/pm/ApplicationInfo;)Z

    move-result v1

    if-eqz v1, :cond_9

    iput-boolean v6, p0, Lcom/android/settingslib/b/a;->czT:Z

    :cond_9
    iget v1, v0, Landroid/content/pm/ApplicationInfo;->uid:I

    invoke-static {v1}, Landroid/os/UserHandle;->getUserId(I)I

    move-result v1

    const-string/jumbo v3, "ApplicationsState"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "The current packageName is: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, v0, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, "  current info\'s userId is: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v3, p0, Lcom/android/settingslib/b/a;->cAo:Landroid/util/SparseArray;

    invoke-virtual {v3, v1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/HashMap;

    iget-object v3, v0, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/settingslib/b/h;

    if-eqz v1, :cond_5

    iput-object v0, v1, Lcom/android/settingslib/b/h;->info:Landroid/content/pm/ApplicationInfo;

    goto :goto_2

    :cond_a
    iget-object v0, p0, Lcom/android/settingslib/b/a;->cAc:Ljava/util/List;

    invoke-direct {p0, v0}, Lcom/android/settingslib/b/a;->cfp(Ljava/util/List;)V

    iget-object v0, p0, Lcom/android/settingslib/b/a;->czK:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    iget-object v1, p0, Lcom/android/settingslib/b/a;->cAc:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-le v0, v1, :cond_b

    invoke-direct {p0}, Lcom/android/settingslib/b/a;->cfb()V

    :cond_b
    iget-object v0, p0, Lcom/android/settingslib/b/a;->cAc:Ljava/util/List;

    invoke-static {v0}, Lcom/android/settingslib/H;->csm(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settingslib/b/a;->cAc:Ljava/util/List;

    iput-object v7, p0, Lcom/android/settingslib/b/a;->czP:Ljava/lang/String;

    iget-object v0, p0, Lcom/android/settingslib/b/a;->cAA:Lcom/android/settingslib/b/d;

    invoke-virtual {v0, v8}, Lcom/android/settingslib/b/d;->hasMessages(I)Z

    move-result v0

    if-nez v0, :cond_c

    iget-object v0, p0, Lcom/android/settingslib/b/a;->cAA:Lcom/android/settingslib/b/d;

    invoke-virtual {v0, v8}, Lcom/android/settingslib/b/d;->sendEmptyMessage(I)Z

    :cond_c
    return-void
.end method

.method ceU()V
    .locals 4

    iget-object v2, p0, Lcom/android/settingslib/b/a;->cAo:Landroid/util/SparseArray;

    monitor-enter v2

    :try_start_0
    iget-boolean v0, p0, Lcom/android/settingslib/b/a;->cAf:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    monitor-exit v2

    return-void

    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/android/settingslib/b/a;->czJ:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/android/settingslib/b/a;->czU:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    iget-object v0, p0, Lcom/android/settingslib/b/a;->czU:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settingslib/b/b;

    iget-boolean v3, v0, Lcom/android/settingslib/b/b;->cAO:Z

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/android/settingslib/b/a;->czJ:Ljava/util/ArrayList;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_2
    monitor-exit v2

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0
.end method

.method public ceV(Ljava/lang/String;I)Lcom/android/settingslib/b/h;
    .locals 5

    const/4 v4, 0x0

    iget-object v2, p0, Lcom/android/settingslib/b/a;->cAo:Landroid/util/SparseArray;

    monitor-enter v2

    :try_start_0
    iget-object v0, p0, Lcom/android/settingslib/b/a;->cAo:Landroid/util/SparseArray;

    invoke-virtual {v0, p2}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settingslib/b/h;

    if-nez v0, :cond_1

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    xor-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_1

    invoke-direct {p0, p1, p2}, Lcom/android/settingslib/b/a;->cfd(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    if-nez v1, :cond_0

    :try_start_1
    iget-object v1, p0, Lcom/android/settingslib/b/a;->czM:Landroid/content/pm/IPackageManager;

    const/16 v3, 0x80

    invoke-interface {v1, p1, v3, p2}, Landroid/content/pm/IPackageManager;->getApplicationInfo(Ljava/lang/String;II)Landroid/content/pm/ApplicationInfo;
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v1

    :cond_0
    if-eqz v1, :cond_1

    :try_start_2
    invoke-direct {p0, v1}, Lcom/android/settingslib/b/a;->cfc(Landroid/content/pm/ApplicationInfo;)Lcom/android/settingslib/b/h;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v0

    :cond_1
    monitor-exit v2

    return-object v0

    :catch_0
    move-exception v0

    :try_start_3
    const-string/jumbo v1, "ApplicationsState"

    const-string/jumbo v3, "getEntry couldn\'t reach PackageManager"

    invoke-static {v1, v3, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    monitor-exit v2

    return-object v4

    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0
.end method

.method public ceZ(Lcom/android/settingslib/b/f;)Lcom/android/settingslib/b/b;
    .locals 3

    new-instance v0, Lcom/android/settingslib/b/b;

    invoke-direct {v0, p0, p1}, Lcom/android/settingslib/b/b;-><init>(Lcom/android/settingslib/b/a;Lcom/android/settingslib/b/f;)V

    iget-object v1, p0, Lcom/android/settingslib/b/a;->cAo:Landroid/util/SparseArray;

    monitor-enter v1

    :try_start_0
    iget-object v2, p0, Lcom/android/settingslib/b/a;->czU:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method cff(Ljava/lang/String;I)I
    .locals 3

    iget-object v0, p0, Lcom/android/settingslib/b/a;->cAc:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_0
    if-ltz v1, :cond_1

    iget-object v0, p0, Lcom/android/settingslib/b/a;->cAc:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/ApplicationInfo;

    iget-object v2, v0, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget v0, v0, Landroid/content/pm/ApplicationInfo;->uid:I

    invoke-static {v0}, Landroid/os/UserHandle;->getUserId(I)I

    move-result v0

    if-ne v0, p2, :cond_0

    return v1

    :cond_0
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0

    :cond_1
    const/4 v0, -0x1

    return v0
.end method

.method public cfg()Landroid/os/Looper;
    .locals 1

    iget-object v0, p0, Lcom/android/settingslib/b/a;->czX:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v0

    return-object v0
.end method

.method cfh(Ljava/lang/String;I)V
    .locals 4

    :try_start_0
    iget-object v1, p0, Lcom/android/settingslib/b/a;->cAo:Landroid/util/SparseArray;

    monitor-enter v1
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :try_start_1
    iget-boolean v0, p0, Lcom/android/settingslib/b/a;->cAB:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-nez v0, :cond_0

    :try_start_2
    monitor-exit v1
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_0

    return-void

    :cond_0
    :try_start_3
    invoke-virtual {p0, p1, p2}, Lcom/android/settingslib/b/a;->cff(Ljava/lang/String;I)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result v0

    if-ltz v0, :cond_1

    :try_start_4
    monitor-exit v1
    :try_end_4
    .catch Landroid/os/RemoteException; {:try_start_4 .. :try_end_4} :catch_0

    return-void

    :cond_1
    :try_start_5
    iget-object v2, p0, Lcom/android/settingslib/b/a;->czM:Landroid/content/pm/IPackageManager;

    iget-object v0, p0, Lcom/android/settingslib/b/a;->czN:Landroid/os/UserManager;

    invoke-virtual {v0, p2}, Landroid/os/UserManager;->isUserAdmin(I)Z

    move-result v0

    if-eqz v0, :cond_2

    iget v0, p0, Lcom/android/settingslib/b/a;->czW:I

    :goto_0
    invoke-interface {v2, p1, v0, p2}, Landroid/content/pm/IPackageManager;->getApplicationInfo(Ljava/lang/String;II)Landroid/content/pm/ApplicationInfo;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    move-result-object v0

    if-nez v0, :cond_3

    :try_start_6
    monitor-exit v1
    :try_end_6
    .catch Landroid/os/RemoteException; {:try_start_6 .. :try_end_6} :catch_0

    return-void

    :cond_2
    :try_start_7
    iget v0, p0, Lcom/android/settingslib/b/a;->cAv:I

    goto :goto_0

    :cond_3
    iget-boolean v2, v0, Landroid/content/pm/ApplicationInfo;->enabled:Z

    if-nez v2, :cond_5

    iget v2, v0, Landroid/content/pm/ApplicationInfo;->enabledSetting:I
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    const/4 v3, 0x3

    if-eq v2, v3, :cond_4

    :try_start_8
    monitor-exit v1
    :try_end_8
    .catch Landroid/os/RemoteException; {:try_start_8 .. :try_end_8} :catch_0

    return-void

    :cond_4
    const/4 v2, 0x1

    :try_start_9
    iput-boolean v2, p0, Lcom/android/settingslib/b/a;->cAg:Z

    :cond_5
    invoke-static {v0}, Lcom/android/settingslib/b/L;->cfT(Landroid/content/pm/ApplicationInfo;)Z

    move-result v2

    if-eqz v2, :cond_6

    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/android/settingslib/b/a;->czT:Z

    :cond_6
    iget-object v2, p0, Lcom/android/settingslib/b/a;->cAc:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/android/settingslib/b/a;->cAA:Lcom/android/settingslib/b/d;

    const/4 v2, 0x2

    invoke-virtual {v0, v2}, Lcom/android/settingslib/b/d;->hasMessages(I)Z

    move-result v0

    if-nez v0, :cond_7

    iget-object v0, p0, Lcom/android/settingslib/b/a;->cAA:Lcom/android/settingslib/b/d;

    const/4 v2, 0x2

    invoke-virtual {v0, v2}, Lcom/android/settingslib/b/d;->sendEmptyMessage(I)Z

    :cond_7
    iget-object v0, p0, Lcom/android/settingslib/b/a;->cAs:Lcom/android/settingslib/b/c;

    const/4 v2, 0x2

    invoke-virtual {v0, v2}, Lcom/android/settingslib/b/c;->hasMessages(I)Z

    move-result v0

    if-nez v0, :cond_8

    iget-object v0, p0, Lcom/android/settingslib/b/a;->cAs:Lcom/android/settingslib/b/c;

    const/4 v2, 0x2

    invoke-virtual {v0, v2}, Lcom/android/settingslib/b/c;->sendEmptyMessage(I)Z
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    :cond_8
    :try_start_a
    monitor-exit v1

    :goto_1
    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
    :try_end_a
    .catch Landroid/os/RemoteException; {:try_start_a .. :try_end_a} :catch_0

    :catch_0
    move-exception v0

    goto :goto_1
.end method

.method public cfj()Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/settingslib/b/a;->czT:Z

    return v0
.end method

.method public cfk(Ljava/lang/String;I)V
    .locals 4

    iget-object v1, p0, Lcom/android/settingslib/b/a;->cAo:Landroid/util/SparseArray;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/android/settingslib/b/a;->cAo:Landroid/util/SparseArray;

    invoke-virtual {v0, p2}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settingslib/b/h;

    if-eqz v0, :cond_0

    iget-object v2, v0, Lcom/android/settingslib/b/h;->info:Landroid/content/pm/ApplicationInfo;

    iget v2, v2, Landroid/content/pm/ApplicationInfo;->flags:I

    const/high16 v3, 0x800000

    and-int/2addr v2, v3

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/android/settingslib/b/a;->cAA:Lcom/android/settingslib/b/d;

    new-instance v3, Lcom/android/settingslib/b/-$Lambda$T-LaieuuZeihYKWamIgOOud_eX4;

    invoke-direct {v3, p2, p0, v0, p1}, Lcom/android/settingslib/b/-$Lambda$T-LaieuuZeihYKWamIgOOud_eX4;-><init>(ILjava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-virtual {v2, v3}, Lcom/android/settingslib/b/d;->post(Ljava/lang/Runnable;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method synthetic cfl(Lcom/android/settingslib/b/h;Ljava/lang/String;I)V
    .locals 4

    :try_start_0
    iget-object v0, p0, Lcom/android/settingslib/b/a;->cAk:Landroid/app/usage/StorageStatsManager;

    iget-object v1, p1, Lcom/android/settingslib/b/h;->info:Landroid/content/pm/ApplicationInfo;

    iget-object v1, v1, Landroid/content/pm/ApplicationInfo;->storageUuid:Ljava/util/UUID;

    invoke-static {p3}, Landroid/os/UserHandle;->of(I)Landroid/os/UserHandle;

    move-result-object v2

    invoke-virtual {v0, v1, p2, v2}, Landroid/app/usage/StorageStatsManager;->queryStatsForPackage(Ljava/util/UUID;Ljava/lang/String;Landroid/os/UserHandle;)Landroid/app/usage/StorageStats;

    move-result-object v0

    new-instance v1, Landroid/content/pm/PackageStats;

    invoke-direct {v1, p2, p3}, Landroid/content/pm/PackageStats;-><init>(Ljava/lang/String;I)V

    invoke-virtual {v0}, Landroid/app/usage/StorageStats;->getCodeBytes()J

    move-result-wide v2

    iput-wide v2, v1, Landroid/content/pm/PackageStats;->codeSize:J

    invoke-virtual {v0}, Landroid/app/usage/StorageStats;->getDataBytes()J

    move-result-wide v2

    iput-wide v2, v1, Landroid/content/pm/PackageStats;->dataSize:J

    invoke-virtual {v0}, Landroid/app/usage/StorageStats;->getCacheBytes()J

    move-result-wide v2

    iput-wide v2, v1, Landroid/content/pm/PackageStats;->cacheSize:J
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :try_start_1
    iget-object v0, p0, Lcom/android/settingslib/b/a;->cAA:Lcom/android/settingslib/b/d;

    iget-object v0, v0, Lcom/android/settingslib/b/d;->cAT:Landroid/content/pm/IPackageStatsObserver$Stub;

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/pm/IPackageStatsObserver$Stub;->onGetStatsCompleted(Landroid/content/pm/PackageStats;Z)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string/jumbo v1, "ApplicationsState"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "Failed to query stats: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :try_start_2
    iget-object v0, p0, Lcom/android/settingslib/b/a;->cAA:Lcom/android/settingslib/b/d;

    iget-object v0, v0, Lcom/android/settingslib/b/d;->cAT:Landroid/content/pm/IPackageStatsObserver$Stub;

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/pm/IPackageStatsObserver$Stub;->onGetStatsCompleted(Landroid/content/pm/PackageStats;Z)V
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_0

    :catch_1
    move-exception v0

    goto :goto_0

    :catch_2
    move-exception v0

    goto :goto_0
.end method

.method public cfo(Ljava/lang/String;I)V
    .locals 4

    iget-object v2, p0, Lcom/android/settingslib/b/a;->cAo:Landroid/util/SparseArray;

    monitor-enter v2

    :try_start_0
    invoke-virtual {p0, p1, p2}, Lcom/android/settingslib/b/a;->cff(Ljava/lang/String;I)I

    move-result v3

    if-ltz v3, :cond_5

    iget-object v0, p0, Lcom/android/settingslib/b/a;->cAo:Landroid/util/SparseArray;

    invoke-virtual {v0, p2}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settingslib/b/h;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/android/settingslib/b/a;->cAo:Landroid/util/SparseArray;

    invoke-virtual {v1, p2}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/HashMap;

    invoke-virtual {v1, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v1, p0, Lcom/android/settingslib/b/a;->czK:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    :cond_0
    iget-object v0, p0, Lcom/android/settingslib/b/a;->cAc:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/ApplicationInfo;

    iget-object v1, p0, Lcom/android/settingslib/b/a;->cAc:Ljava/util/List;

    invoke-interface {v1, v3}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    iget-boolean v1, v0, Landroid/content/pm/ApplicationInfo;->enabled:Z

    if-nez v1, :cond_2

    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/android/settingslib/b/a;->cAg:Z

    iget-object v1, p0, Lcom/android/settingslib/b/a;->cAc:Ljava/util/List;

    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/pm/ApplicationInfo;

    iget-boolean v1, v1, Landroid/content/pm/ApplicationInfo;->enabled:Z

    if-nez v1, :cond_1

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/android/settingslib/b/a;->cAg:Z

    :cond_2
    invoke-static {v0}, Lcom/android/settingslib/b/L;->cfT(Landroid/content/pm/ApplicationInfo;)Z

    move-result v0

    if-eqz v0, :cond_4

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/settingslib/b/a;->czT:Z

    iget-object v0, p0, Lcom/android/settingslib/b/a;->cAc:Ljava/util/List;

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/ApplicationInfo;

    invoke-static {v0}, Lcom/android/settingslib/b/L;->cfT(Landroid/content/pm/ApplicationInfo;)Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/settingslib/b/a;->czT:Z

    :cond_4
    iget-object v0, p0, Lcom/android/settingslib/b/a;->cAs:Lcom/android/settingslib/b/c;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/android/settingslib/b/c;->hasMessages(I)Z

    move-result v0

    if-nez v0, :cond_5

    iget-object v0, p0, Lcom/android/settingslib/b/a;->cAs:Lcom/android/settingslib/b/c;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/android/settingslib/b/c;->sendEmptyMessage(I)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_5
    monitor-exit v2

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0
.end method

.method public cfq(Ljava/lang/String;I)V
    .locals 0

    invoke-virtual {p0, p1, p2}, Lcom/android/settingslib/b/a;->cfo(Ljava/lang/String;I)V

    invoke-virtual {p0, p1, p2}, Lcom/android/settingslib/b/a;->cfh(Ljava/lang/String;I)V

    return-void
.end method
