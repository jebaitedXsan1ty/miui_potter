.class final Lcom/android/settingslib/b/O;
.super Landroid/content/pm/permission/RuntimePermissionPresenter$OnResultCallback;
.source "PermissionsSummaryHelper.java"


# instance fields
.field final synthetic cBL:Lcom/android/settingslib/b/N;


# direct methods
.method constructor <init>(Lcom/android/settingslib/b/N;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settingslib/b/O;->cBL:Lcom/android/settingslib/b/N;

    invoke-direct {p0}, Landroid/content/pm/permission/RuntimePermissionPresenter$OnResultCallback;-><init>()V

    return-void
.end method


# virtual methods
.method public onGetAppPermissions(Ljava/util/List;)V
    .locals 10

    const/4 v4, 0x0

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v6

    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    move v3, v4

    move v5, v4

    move v1, v4

    move v2, v4

    :goto_0
    if-ge v3, v6, :cond_1

    invoke-interface {p1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/permission/RuntimePermissionPresentationInfo;

    add-int/lit8 v5, v5, 0x1

    invoke-virtual {v0}, Landroid/content/pm/permission/RuntimePermissionPresentationInfo;->isGranted()Z

    move-result v8

    if-eqz v8, :cond_2

    invoke-virtual {v0}, Landroid/content/pm/permission/RuntimePermissionPresentationInfo;->isStandard()Z

    move-result v8

    if-eqz v8, :cond_0

    invoke-virtual {v0}, Landroid/content/pm/permission/RuntimePermissionPresentationInfo;->getLabel()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v7, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v2, 0x1

    move v9, v1

    move v1, v0

    move v0, v9

    :goto_1
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    move v2, v1

    move v1, v0

    goto :goto_0

    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v2

    goto :goto_1

    :cond_1
    invoke-static {}, Ljava/text/Collator;->getInstance()Ljava/text/Collator;

    move-result-object v0

    invoke-virtual {v0, v4}, Ljava/text/Collator;->setStrength(I)V

    invoke-static {v7, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    iget-object v0, p0, Lcom/android/settingslib/b/O;->cBL:Lcom/android/settingslib/b/N;

    invoke-virtual {v0, v2, v5, v1, v7}, Lcom/android/settingslib/b/N;->Aa(IIILjava/util/List;)V

    return-void

    :cond_2
    move v0, v1

    move v1, v2

    goto :goto_1
.end method
