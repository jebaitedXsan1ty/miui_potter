.class public Lcom/android/settingslib/b/I;
.super Ljava/lang/Object;
.source "StorageStatsSource.java"


# instance fields
.field public cBE:J

.field public cBF:J

.field public cBG:J

.field public cBH:J

.field public cBI:J


# direct methods
.method public constructor <init>(JJJJJ)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-wide p1, p0, Lcom/android/settingslib/b/I;->cBF:J

    iput-wide p3, p0, Lcom/android/settingslib/b/I;->cBG:J

    iput-wide p5, p0, Lcom/android/settingslib/b/I;->cBH:J

    iput-wide p7, p0, Lcom/android/settingslib/b/I;->cBI:J

    iput-wide p9, p0, Lcom/android/settingslib/b/I;->cBE:J

    return-void
.end method

.method public constructor <init>(Landroid/app/usage/ExternalStorageStats;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p1}, Landroid/app/usage/ExternalStorageStats;->getTotalBytes()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/android/settingslib/b/I;->cBF:J

    invoke-virtual {p1}, Landroid/app/usage/ExternalStorageStats;->getAudioBytes()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/android/settingslib/b/I;->cBG:J

    invoke-virtual {p1}, Landroid/app/usage/ExternalStorageStats;->getVideoBytes()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/android/settingslib/b/I;->cBH:J

    invoke-virtual {p1}, Landroid/app/usage/ExternalStorageStats;->getImageBytes()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/android/settingslib/b/I;->cBI:J

    invoke-virtual {p1}, Landroid/app/usage/ExternalStorageStats;->getAppBytes()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/android/settingslib/b/I;->cBE:J

    return-void
.end method
