.class Lcom/android/settingslib/b/c;
.super Landroid/os/Handler;
.source "ApplicationsState.java"


# instance fields
.field final synthetic cAQ:Lcom/android/settingslib/b/a;


# direct methods
.method public constructor <init>(Lcom/android/settingslib/b/a;Landroid/os/Looper;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settingslib/b/c;->cAQ:Lcom/android/settingslib/b/a;

    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 4

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/android/settingslib/b/c;->cAQ:Lcom/android/settingslib/b/a;

    invoke-virtual {v0}, Lcom/android/settingslib/b/a;->ceU()V

    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    :cond_0
    :goto_0
    return-void

    :pswitch_0
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/android/settingslib/b/b;

    iget-object v1, p0, Lcom/android/settingslib/b/c;->cAQ:Lcom/android/settingslib/b/a;

    iget-object v1, v1, Lcom/android/settingslib/b/a;->czJ:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, v0, Lcom/android/settingslib/b/b;->cAN:Lcom/android/settingslib/b/f;

    iget-object v0, v0, Lcom/android/settingslib/b/b;->cAF:Ljava/util/ArrayList;

    invoke-interface {v1, v0}, Lcom/android/settingslib/b/f;->iu(Ljava/util/ArrayList;)V

    goto :goto_0

    :goto_1
    :pswitch_1
    iget-object v0, p0, Lcom/android/settingslib/b/c;->cAQ:Lcom/android/settingslib/b/a;

    iget-object v0, v0, Lcom/android/settingslib/b/a;->czJ:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    iget-object v0, p0, Lcom/android/settingslib/b/c;->cAQ:Lcom/android/settingslib/b/a;

    iget-object v0, v0, Lcom/android/settingslib/b/a;->czJ:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settingslib/b/b;

    iget-object v0, v0, Lcom/android/settingslib/b/b;->cAN:Lcom/android/settingslib/b/f;

    invoke-interface {v0}, Lcom/android/settingslib/b/f;->is()V

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    :goto_2
    :pswitch_2
    iget-object v0, p0, Lcom/android/settingslib/b/c;->cAQ:Lcom/android/settingslib/b/a;

    iget-object v0, v0, Lcom/android/settingslib/b/a;->czJ:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    iget-object v0, p0, Lcom/android/settingslib/b/c;->cAQ:Lcom/android/settingslib/b/a;

    iget-object v0, v0, Lcom/android/settingslib/b/a;->czJ:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settingslib/b/b;

    iget-object v0, v0, Lcom/android/settingslib/b/b;->cAN:Lcom/android/settingslib/b/f;

    invoke-interface {v0}, Lcom/android/settingslib/b/f;->ir()V

    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    :goto_3
    :pswitch_3
    iget-object v0, p0, Lcom/android/settingslib/b/c;->cAQ:Lcom/android/settingslib/b/a;

    iget-object v0, v0, Lcom/android/settingslib/b/a;->czJ:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    iget-object v0, p0, Lcom/android/settingslib/b/c;->cAQ:Lcom/android/settingslib/b/a;

    iget-object v0, v0, Lcom/android/settingslib/b/a;->czJ:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settingslib/b/b;

    iget-object v2, v0, Lcom/android/settingslib/b/b;->cAN:Lcom/android/settingslib/b/f;

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    invoke-interface {v2, v0}, Lcom/android/settingslib/b/f;->it(Ljava/lang/String;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    :goto_4
    :pswitch_4
    iget-object v0, p0, Lcom/android/settingslib/b/c;->cAQ:Lcom/android/settingslib/b/a;

    iget-object v0, v0, Lcom/android/settingslib/b/a;->czJ:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    iget-object v0, p0, Lcom/android/settingslib/b/c;->cAQ:Lcom/android/settingslib/b/a;

    iget-object v0, v0, Lcom/android/settingslib/b/a;->czJ:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settingslib/b/b;

    iget-object v0, v0, Lcom/android/settingslib/b/b;->cAN:Lcom/android/settingslib/b/f;

    invoke-interface {v0}, Lcom/android/settingslib/b/f;->in()V

    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    :pswitch_5
    move v2, v1

    :goto_5
    iget-object v0, p0, Lcom/android/settingslib/b/c;->cAQ:Lcom/android/settingslib/b/a;

    iget-object v0, v0, Lcom/android/settingslib/b/a;->czJ:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v2, v0, :cond_0

    iget-object v0, p0, Lcom/android/settingslib/b/c;->cAQ:Lcom/android/settingslib/b/a;

    iget-object v0, v0, Lcom/android/settingslib/b/a;->czJ:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settingslib/b/b;

    iget-object v3, v0, Lcom/android/settingslib/b/b;->cAN:Lcom/android/settingslib/b/f;

    iget v0, p1, Landroid/os/Message;->arg1:I

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_6
    invoke-interface {v3, v0}, Lcom/android/settingslib/b/f;->iv(Z)V

    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_5

    :cond_1
    move v0, v1

    goto :goto_6

    :goto_7
    :pswitch_6
    iget-object v0, p0, Lcom/android/settingslib/b/c;->cAQ:Lcom/android/settingslib/b/a;

    iget-object v0, v0, Lcom/android/settingslib/b/a;->czJ:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    iget-object v0, p0, Lcom/android/settingslib/b/c;->cAQ:Lcom/android/settingslib/b/a;

    iget-object v0, v0, Lcom/android/settingslib/b/a;->czJ:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settingslib/b/b;

    iget-object v0, v0, Lcom/android/settingslib/b/b;->cAN:Lcom/android/settingslib/b/f;

    invoke-interface {v0}, Lcom/android/settingslib/b/f;->ip()V

    add-int/lit8 v1, v1, 0x1

    goto :goto_7

    :goto_8
    :pswitch_7
    iget-object v0, p0, Lcom/android/settingslib/b/c;->cAQ:Lcom/android/settingslib/b/a;

    iget-object v0, v0, Lcom/android/settingslib/b/a;->czJ:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    iget-object v0, p0, Lcom/android/settingslib/b/c;->cAQ:Lcom/android/settingslib/b/a;

    iget-object v0, v0, Lcom/android/settingslib/b/a;->czJ:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settingslib/b/b;

    iget-object v0, v0, Lcom/android/settingslib/b/b;->cAN:Lcom/android/settingslib/b/f;

    invoke-interface {v0}, Lcom/android/settingslib/b/f;->iq()V

    add-int/lit8 v1, v1, 0x1

    goto :goto_8

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method
