.class public Lcom/android/settingslib/b/h;
.super Lcom/android/settingslib/b/g;
.source "ApplicationsState.java"


# instance fields
.field public cBb:Z

.field public cBc:J

.field public cBd:Ljava/lang/Object;

.field public final cBe:J

.field public cBf:Ljava/lang/String;

.field public cBg:J

.field public cBh:Ljava/lang/String;

.field public cBi:Ljava/lang/String;

.field public cBj:Ljava/lang/String;

.field public cBk:Z

.field public cBl:Z

.field public cBm:J

.field public cBn:Z

.field public cBo:Z

.field cBp:Ljava/lang/String;

.field public final cBq:Ljava/io/File;

.field public cBr:Ljava/lang/String;

.field public cBs:J

.field public icon:Landroid/graphics/drawable/Drawable;

.field public info:Landroid/content/pm/ApplicationInfo;


# direct methods
.method constructor <init>(Landroid/content/Context;Landroid/content/pm/ApplicationInfo;J)V
    .locals 3

    invoke-direct {p0}, Lcom/android/settingslib/b/g;-><init>()V

    new-instance v0, Ljava/io/File;

    iget-object v1, p2, Landroid/content/pm/ApplicationInfo;->sourceDir:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/android/settingslib/b/h;->cBq:Ljava/io/File;

    iput-wide p3, p0, Lcom/android/settingslib/b/h;->cBe:J

    iput-object p2, p0, Lcom/android/settingslib/b/h;->info:Landroid/content/pm/ApplicationInfo;

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/android/settingslib/b/h;->cBc:J

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/settingslib/b/h;->cBn:Z

    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/android/settingslib/b/h;->cBp:Ljava/lang/String;

    iget v0, p2, Landroid/content/pm/ApplicationInfo;->uid:I

    invoke-static {v0}, Lmiui/securityspace/XSpaceUserHandle;->isUidBelongtoXSpace(I)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/settingslib/b/h;->cBl:Z

    invoke-virtual {p0, p1}, Lcom/android/settingslib/b/h;->cfE(Landroid/content/Context;)V

    return-void
.end method

.method private cfG(Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    const/4 v0, 0x0

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    :goto_0
    if-ge v0, v1, :cond_0

    invoke-virtual {p1, v0}, Ljava/lang/String;->charAt(I)C

    move-result v2

    invoke-static {v2}, Landroid/text/TextUtils;->isGraphic(C)Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    invoke-virtual {p1, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    return-object v0
.end method


# virtual methods
.method public cfC(Landroid/content/Context;)Landroid/graphics/drawable/Drawable;
    .locals 4

    iget-object v0, p0, Lcom/android/settingslib/b/h;->info:Landroid/content/pm/ApplicationInfo;

    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    const-wide/32 v2, 0x927c0

    invoke-static {p1, v0, v1, v2, v3}, Lmiui/maml/util/AppIconsHelper;->getIconDrawable(Landroid/content/Context;Landroid/content/pm/PackageItemInfo;Landroid/content/pm/PackageManager;J)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settingslib/b/h;->info:Landroid/content/pm/ApplicationInfo;

    iget v1, v1, Landroid/content/pm/ApplicationInfo;->uid:I

    invoke-static {v1}, Lmiui/securityspace/XSpaceUserHandle;->isUidBelongtoXSpace(I)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {p1, v0}, Lmiui/securityspace/XSpaceUserHandle;->getXSpaceIcon(Landroid/content/Context;Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    return-object v0

    :cond_0
    return-object v0
.end method

.method cfD(Landroid/content/Context;Landroid/util/IconDrawableFactory;)Z
    .locals 3

    const/4 v2, 0x0

    const/4 v1, 0x1

    iget-object v0, p0, Lcom/android/settingslib/b/h;->icon:Landroid/graphics/drawable/Drawable;

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/android/settingslib/b/h;->cBq:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settingslib/b/h;->info:Landroid/content/pm/ApplicationInfo;

    invoke-virtual {p2, v0}, Landroid/util/IconDrawableFactory;->getBadgedIcon(Landroid/content/pm/ApplicationInfo;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settingslib/b/h;->icon:Landroid/graphics/drawable/Drawable;

    return v1

    :cond_0
    iput-boolean v2, p0, Lcom/android/settingslib/b/h;->cBb:Z

    const v0, 0x1080806

    invoke-virtual {p1, v0}, Landroid/content/Context;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settingslib/b/h;->icon:Landroid/graphics/drawable/Drawable;

    :cond_1
    return v2

    :cond_2
    iget-boolean v0, p0, Lcom/android/settingslib/b/h;->cBb:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/android/settingslib/b/h;->cBq:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_1

    iput-boolean v1, p0, Lcom/android/settingslib/b/h;->cBb:Z

    iget-object v0, p0, Lcom/android/settingslib/b/h;->info:Landroid/content/pm/ApplicationInfo;

    invoke-virtual {p2, v0}, Landroid/util/IconDrawableFactory;->getBadgedIcon(Landroid/content/pm/ApplicationInfo;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settingslib/b/h;->icon:Landroid/graphics/drawable/Drawable;

    return v1
.end method

.method public cfE(Landroid/content/Context;)V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/android/settingslib/b/h;->cBj:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/android/settingslib/b/h;->cBb:Z

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/android/settingslib/b/h;->cBq:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-nez v0, :cond_2

    iput-boolean v2, p0, Lcom/android/settingslib/b/h;->cBb:Z

    iget-object v0, p0, Lcom/android/settingslib/b/h;->info:Landroid/content/pm/ApplicationInfo;

    iget-object v0, v0, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    iput-object v0, p0, Lcom/android/settingslib/b/h;->cBj:Ljava/lang/String;

    :goto_0
    iget-object v0, p0, Lcom/android/settingslib/b/h;->cBj:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/settingslib/b/h;->cBj:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/android/settingslib/b/h;->cfG(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_4

    invoke-static {}, Lmiui/text/ChinesePinyinConverter;->getInstance()Lmiui/text/ChinesePinyinConverter;

    move-result-object v1

    invoke-virtual {v1, v0}, Lmiui/text/ChinesePinyinConverter;->get(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_1

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmiui/text/ChinesePinyinConverter$Token;

    iget-object v0, v0, Lmiui/text/ChinesePinyinConverter$Token;->target:Ljava/lang/String;

    iput-object v0, p0, Lcom/android/settingslib/b/h;->cBp:Ljava/lang/String;

    :cond_1
    :goto_1
    return-void

    :cond_2
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/settingslib/b/h;->cBb:Z

    iget-object v0, p0, Lcom/android/settingslib/b/h;->info:Landroid/content/pm/ApplicationInfo;

    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/pm/ApplicationInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_2
    iput-object v0, p0, Lcom/android/settingslib/b/h;->cBj:Ljava/lang/String;

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/android/settingslib/b/h;->info:Landroid/content/pm/ApplicationInfo;

    iget-object v0, v0, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    goto :goto_2

    :cond_4
    iget-object v0, p0, Lcom/android/settingslib/b/h;->cBj:Ljava/lang/String;

    iput-object v0, p0, Lcom/android/settingslib/b/h;->cBp:Ljava/lang/String;

    goto :goto_1
.end method

.method public cfF()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/android/settingslib/b/h;->cBr:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settingslib/b/h;->cBr:Ljava/lang/String;

    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/android/settingslib/b/h;->cBj:Ljava/lang/String;

    invoke-static {v0}, Lcom/android/settingslib/b/a;->ceW(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settingslib/b/h;->cBr:Ljava/lang/String;

    iget-object v0, p0, Lcom/android/settingslib/b/h;->cBr:Ljava/lang/String;

    return-object v0
.end method
