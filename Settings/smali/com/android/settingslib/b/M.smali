.class public Lcom/android/settingslib/b/M;
.super Ljava/lang/Object;
.source "PermissionsSummaryHelper.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static cfX(Landroid/content/Context;Ljava/lang/String;Lcom/android/settingslib/b/N;)V
    .locals 3

    invoke-static {p0}, Landroid/content/pm/permission/RuntimePermissionPresenter;->getInstance(Landroid/content/Context;)Landroid/content/pm/permission/RuntimePermissionPresenter;

    move-result-object v0

    new-instance v1, Lcom/android/settingslib/b/O;

    invoke-direct {v1, p2}, Lcom/android/settingslib/b/O;-><init>(Lcom/android/settingslib/b/N;)V

    const/4 v2, 0x0

    invoke-virtual {v0, p1, v1, v2}, Landroid/content/pm/permission/RuntimePermissionPresenter;->getAppPermissions(Ljava/lang/String;Landroid/content/pm/permission/RuntimePermissionPresenter$OnResultCallback;Landroid/os/Handler;)V

    return-void
.end method
