.class public Lcom/android/settingslib/d/f;
.super Lcom/android/settingslib/d/g;
.source "InputMethodSubtypePreference.java"


# instance fields
.field private final cHT:Z

.field private final cHU:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/view/inputmethod/InputMethodSubtype;Landroid/view/inputmethod/InputMethodInfo;)V
    .locals 3

    const/4 v2, 0x0

    invoke-direct {p0, p1}, Lcom/android/settingslib/d/g;-><init>(Landroid/content/Context;)V

    invoke-virtual {p0, v2}, Lcom/android/settingslib/d/f;->setPersistent(Z)V

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p3}, Landroid/view/inputmethod/InputMethodInfo;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p2}, Landroid/view/inputmethod/InputMethodSubtype;->hashCode()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/settingslib/d/f;->setKey(Ljava/lang/String;)V

    invoke-static {p2, p1, p3}, Lcom/android/settingslib/d/d;->cnp(Landroid/view/inputmethod/InputMethodSubtype;Landroid/content/Context;Landroid/view/inputmethod/InputMethodInfo;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/settingslib/d/f;->setTitle(Ljava/lang/CharSequence;)V

    invoke-virtual {p2}, Landroid/view/inputmethod/InputMethodSubtype;->getLocale()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    iput-boolean v2, p0, Lcom/android/settingslib/d/f;->cHU:Z

    iput-boolean v2, p0, Lcom/android/settingslib/d/f;->cHT:Z

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    iget-object v1, v1, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-virtual {v1}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    iput-boolean v2, p0, Lcom/android/settingslib/d/f;->cHU:Z

    iget-boolean v2, p0, Lcom/android/settingslib/d/f;->cHU:Z

    if-nez v2, :cond_1

    invoke-static {v0}, Lcom/android/internal/inputmethod/InputMethodUtils;->getLanguageFromLocaleString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    :goto_1
    iput-boolean v0, p0, Lcom/android/settingslib/d/f;->cHT:Z

    goto :goto_0

    :cond_1
    const/4 v0, 0x1

    goto :goto_1
.end method


# virtual methods
.method public cnI(Landroid/preference/Preference;Ljava/text/Collator;)I
    .locals 4

    const/4 v2, 0x1

    const/4 v1, -0x1

    if-ne p0, p1, :cond_0

    const/4 v0, 0x0

    return v0

    :cond_0
    instance-of v0, p1, Lcom/android/settingslib/d/f;

    if-eqz v0, :cond_8

    move-object v0, p1

    check-cast v0, Lcom/android/settingslib/d/f;

    iget-boolean v3, p0, Lcom/android/settingslib/d/f;->cHU:Z

    if-eqz v3, :cond_1

    iget-boolean v3, v0, Lcom/android/settingslib/d/f;->cHU:Z

    xor-int/lit8 v3, v3, 0x1

    if-eqz v3, :cond_1

    return v1

    :cond_1
    iget-boolean v3, p0, Lcom/android/settingslib/d/f;->cHU:Z

    if-nez v3, :cond_2

    iget-boolean v3, v0, Lcom/android/settingslib/d/f;->cHU:Z

    if-eqz v3, :cond_2

    return v2

    :cond_2
    iget-boolean v3, p0, Lcom/android/settingslib/d/f;->cHT:Z

    if-eqz v3, :cond_3

    iget-boolean v3, v0, Lcom/android/settingslib/d/f;->cHT:Z

    xor-int/lit8 v3, v3, 0x1

    if-eqz v3, :cond_3

    return v1

    :cond_3
    iget-boolean v3, p0, Lcom/android/settingslib/d/f;->cHT:Z

    if-nez v3, :cond_4

    iget-boolean v0, v0, Lcom/android/settingslib/d/f;->cHT:Z

    if-eqz v0, :cond_4

    return v2

    :cond_4
    invoke-virtual {p0}, Lcom/android/settingslib/d/f;->getTitle()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p1}, Landroid/preference/Preference;->getTitle()Ljava/lang/CharSequence;

    move-result-object v3

    if-nez v0, :cond_5

    if-nez v3, :cond_5

    invoke-virtual {p0}, Lcom/android/settingslib/d/f;->hashCode()I

    move-result v0

    invoke-virtual {p1}, Landroid/preference/Preference;->hashCode()I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Integer;->compare(II)I

    move-result v0

    return v0

    :cond_5
    if-eqz v0, :cond_6

    if-eqz v3, :cond_6

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v3}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v0, v1}, Ljava/text/Collator;->compare(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    return v0

    :cond_6
    if-nez v0, :cond_7

    move v0, v1

    :goto_0
    return v0

    :cond_7
    move v0, v2

    goto :goto_0

    :cond_8
    invoke-super {p0, p1}, Lcom/android/settingslib/d/g;->compareTo(Landroid/preference/Preference;)I

    move-result v0

    return v0
.end method
