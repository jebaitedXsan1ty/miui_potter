.class public Lcom/android/settingslib/d/a;
.super Lcom/android/settingslib/MiuiRestrictedSwitchPreference;
.source "InputMethodPreference.java"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceClickListener;
.implements Landroid/preference/Preference$OnPreferenceChangeListener;


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private final cHA:Z

.field private final cHB:Lcom/android/settingslib/d/c;

.field private cHC:Landroid/app/AlertDialog;

.field private final cHD:Lcom/android/settingslib/d/b;

.field private final cHy:Z

.field private final cHz:Landroid/view/inputmethod/InputMethodInfo;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/android/settingslib/d/a;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/settingslib/d/a;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/view/inputmethod/InputMethodInfo;ZZLcom/android/settingslib/d/b;)V
    .locals 4

    const/4 v3, 0x0

    const/4 v0, 0x0

    invoke-direct {p0, p1}, Lcom/android/settingslib/MiuiRestrictedSwitchPreference;-><init>(Landroid/content/Context;)V

    iput-object v3, p0, Lcom/android/settingslib/d/a;->cHC:Landroid/app/AlertDialog;

    sget v1, Lmiui/R$layout;->preference:I

    invoke-virtual {p0, v1}, Lcom/android/settingslib/d/a;->setLayoutResource(I)V

    invoke-virtual {p0, v0}, Lcom/android/settingslib/d/a;->setPersistent(Z)V

    iput-object p2, p0, Lcom/android/settingslib/d/a;->cHz:Landroid/view/inputmethod/InputMethodInfo;

    iput-boolean p4, p0, Lcom/android/settingslib/d/a;->cHy:Z

    iput-object p5, p0, Lcom/android/settingslib/d/a;->cHD:Lcom/android/settingslib/d/b;

    if-nez p3, :cond_0

    invoke-virtual {p0, v0}, Lcom/android/settingslib/d/a;->setWidgetLayoutResource(I)V

    :cond_0
    const-string/jumbo v1, ""

    invoke-virtual {p0, v1}, Lcom/android/settingslib/d/a;->setSwitchTextOn(Ljava/lang/CharSequence;)V

    const-string/jumbo v1, ""

    invoke-virtual {p0, v1}, Lcom/android/settingslib/d/a;->setSwitchTextOff(Ljava/lang/CharSequence;)V

    invoke-virtual {p2}, Landroid/view/inputmethod/InputMethodInfo;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/android/settingslib/d/a;->setKey(Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    invoke-virtual {p2, v1}, Landroid/view/inputmethod/InputMethodInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/android/settingslib/d/a;->setTitle(Ljava/lang/CharSequence;)V

    invoke-virtual {p2}, Landroid/view/inputmethod/InputMethodInfo;->getSettingsActivity()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {p0, v3}, Lcom/android/settingslib/d/a;->setIntent(Landroid/content/Intent;)V

    :goto_0
    invoke-static {p1}, Lcom/android/settingslib/d/c;->getInstance(Landroid/content/Context;)Lcom/android/settingslib/d/c;

    move-result-object v1

    iput-object v1, p0, Lcom/android/settingslib/d/a;->cHB:Lcom/android/settingslib/d/c;

    invoke-static {p2}, Lcom/android/internal/inputmethod/InputMethodUtils;->isSystemIme(Landroid/view/inputmethod/InputMethodInfo;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v0, p0, Lcom/android/settingslib/d/a;->cHB:Lcom/android/settingslib/d/c;

    invoke-virtual {v0, p2, p1}, Lcom/android/settingslib/d/c;->cnf(Landroid/view/inputmethod/InputMethodInfo;Landroid/content/Context;)Z

    move-result v0

    :cond_1
    iput-boolean v0, p0, Lcom/android/settingslib/d/a;->cHA:Z

    invoke-virtual {p0, p0}, Lcom/android/settingslib/d/a;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    invoke-virtual {p0, p0}, Lcom/android/settingslib/d/a;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    return-void

    :cond_2
    new-instance v2, Landroid/content/Intent;

    const-string/jumbo v3, "android.intent.action.MAIN"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2}, Landroid/view/inputmethod/InputMethodInfo;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3, v1}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p0, v2}, Lcom/android/settingslib/d/a;->setIntent(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method private cmO()Z
    .locals 2

    invoke-virtual {p0}, Lcom/android/settingslib/d/a;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->uiMode:I

    and-int/lit8 v0, v0, 0xf

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private cmQ()V
    .locals 4

    const/4 v3, 0x1

    iget-object v0, p0, Lcom/android/settingslib/d/a;->cHC:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settingslib/d/a;->cHC:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settingslib/d/a;->cHC:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    :cond_0
    invoke-virtual {p0}, Lcom/android/settingslib/d/a;->getContext()Landroid/content/Context;

    move-result-object v0

    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, v0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, v3}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    sget v2, Lcom/android/settingslib/i;->cMt:I

    invoke-virtual {v0, v2}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    new-instance v0, Lcom/android/settingslib/d/-$Lambda$Cox36jiNe8lH_iHey56O8zUYRKQ;

    const/4 v2, 0x0

    invoke-direct {v0, v2, p0}, Lcom/android/settingslib/d/-$Lambda$Cox36jiNe8lH_iHey56O8zUYRKQ;-><init>(BLjava/lang/Object;)V

    const v2, 0x104000a

    invoke-virtual {v1, v2, v0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    new-instance v0, Lcom/android/settingslib/d/-$Lambda$Cox36jiNe8lH_iHey56O8zUYRKQ;

    invoke-direct {v0, v3, p0}, Lcom/android/settingslib/d/-$Lambda$Cox36jiNe8lH_iHey56O8zUYRKQ;-><init>(BLjava/lang/Object;)V

    const/high16 v2, 0x1040000

    invoke-virtual {v1, v2, v0}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settingslib/d/a;->cHC:Landroid/app/AlertDialog;

    iget-object v0, p0, Lcom/android/settingslib/d/a;->cHC:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    return-void
.end method

.method private cmR()Z
    .locals 2

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/android/settingslib/d/a;->getWidgetLayoutResource()I

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method private cmT()V
    .locals 6

    const/4 v4, 0x1

    iget-object v0, p0, Lcom/android/settingslib/d/a;->cHC:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settingslib/d/a;->cHC:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settingslib/d/a;->cHC:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    :cond_0
    invoke-virtual {p0}, Lcom/android/settingslib/d/a;->getContext()Landroid/content/Context;

    move-result-object v0

    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, v0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, v4}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    const v2, 0x1040014

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    iget-object v2, p0, Lcom/android/settingslib/d/a;->cHz:Landroid/view/inputmethod/InputMethodInfo;

    invoke-virtual {v2}, Landroid/view/inputmethod/InputMethodInfo;->getServiceInfo()Landroid/content/pm/ServiceInfo;

    move-result-object v2

    iget-object v2, v2, Landroid/content/pm/ServiceInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/content/pm/ApplicationInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v2

    sget v3, Lcom/android/settingslib/i;->cMy:I

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v2, v4, v5

    invoke-virtual {v0, v3, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    new-instance v0, Lcom/android/settingslib/d/-$Lambda$Cox36jiNe8lH_iHey56O8zUYRKQ;

    const/4 v2, 0x2

    invoke-direct {v0, v2, p0}, Lcom/android/settingslib/d/-$Lambda$Cox36jiNe8lH_iHey56O8zUYRKQ;-><init>(BLjava/lang/Object;)V

    const v2, 0x104000a

    invoke-virtual {v1, v2, v0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    new-instance v0, Lcom/android/settingslib/d/-$Lambda$Cox36jiNe8lH_iHey56O8zUYRKQ;

    const/4 v2, 0x3

    invoke-direct {v0, v2, p0}, Lcom/android/settingslib/d/-$Lambda$Cox36jiNe8lH_iHey56O8zUYRKQ;-><init>(BLjava/lang/Object;)V

    const/high16 v2, 0x1040000

    invoke-virtual {v1, v2, v0}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settingslib/d/a;->cHC:Landroid/app/AlertDialog;

    iget-object v0, p0, Lcom/android/settingslib/d/a;->cHC:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    return-void
.end method

.method private cmU()Landroid/view/inputmethod/InputMethodManager;
    .locals 2

    invoke-virtual {p0}, Lcom/android/settingslib/d/a;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string/jumbo v1, "input_method"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    return-object v0
.end method

.method private cmW()Ljava/lang/String;
    .locals 3

    invoke-direct {p0}, Lcom/android/settingslib/d/a;->cmU()Landroid/view/inputmethod/InputMethodManager;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settingslib/d/a;->cHz:Landroid/view/inputmethod/InputMethodInfo;

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->getEnabledInputMethodSubtypeList(Landroid/view/inputmethod/InputMethodInfo;Z)Ljava/util/List;

    move-result-object v0

    invoke-virtual {p0}, Lcom/android/settingslib/d/a;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/android/settingslib/d/a;->cHz:Landroid/view/inputmethod/InputMethodInfo;

    invoke-static {v0, v1, v2}, Lcom/android/settingslib/d/d;->cnq(Ljava/util/List;Landroid/content/Context;Landroid/view/inputmethod/InputMethodInfo;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private setCheckedInternal(Z)V
    .locals 1

    invoke-super {p0, p1}, Lcom/android/settingslib/MiuiRestrictedSwitchPreference;->setChecked(Z)V

    iget-object v0, p0, Lcom/android/settingslib/d/a;->cHD:Lcom/android/settingslib/d/b;

    invoke-interface {v0, p0}, Lcom/android/settingslib/d/b;->akC(Lcom/android/settingslib/d/a;)V

    invoke-virtual {p0}, Lcom/android/settingslib/d/a;->notifyChanged()V

    return-void
.end method


# virtual methods
.method public cmM()Landroid/view/inputmethod/InputMethodInfo;
    .locals 1

    iget-object v0, p0, Lcom/android/settingslib/d/a;->cHz:Landroid/view/inputmethod/InputMethodInfo;

    return-object v0
.end method

.method synthetic cmN(Landroid/content/DialogInterface;I)V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/android/settingslib/d/a;->setCheckedInternal(Z)V

    return-void
.end method

.method public cmP()V
    .locals 3

    iget-object v0, p0, Lcom/android/settingslib/d/a;->cHB:Lcom/android/settingslib/d/c;

    iget-object v1, p0, Lcom/android/settingslib/d/a;->cHz:Landroid/view/inputmethod/InputMethodInfo;

    invoke-virtual {p0}, Lcom/android/settingslib/d/a;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/android/settingslib/d/c;->cnc(Landroid/view/inputmethod/InputMethodInfo;Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lcom/android/settingslib/d/a;->cmR()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/android/settingslib/d/a;->setDisabledByAdmin(Lcom/android/settingslib/n;)V

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/android/settingslib/d/a;->setEnabled(Z)V

    :goto_0
    iget-object v0, p0, Lcom/android/settingslib/d/a;->cHB:Lcom/android/settingslib/d/c;

    iget-object v1, p0, Lcom/android/settingslib/d/a;->cHz:Landroid/view/inputmethod/InputMethodInfo;

    invoke-virtual {v0, v1}, Lcom/android/settingslib/d/c;->cng(Landroid/view/inputmethod/InputMethodInfo;)Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/android/settingslib/d/a;->setChecked(Z)V

    invoke-virtual {p0}, Lcom/android/settingslib/d/a;->cqw()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/android/settingslib/d/a;->cmW()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/settingslib/d/a;->setSummary(Ljava/lang/CharSequence;)V

    :cond_0
    return-void

    :cond_1
    iget-boolean v0, p0, Lcom/android/settingslib/d/a;->cHy:Z

    if-nez v0, :cond_2

    invoke-virtual {p0}, Lcom/android/settingslib/d/a;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settingslib/d/a;->cHz:Landroid/view/inputmethod/InputMethodInfo;

    invoke-virtual {v1}, Landroid/view/inputmethod/InputMethodInfo;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v2

    invoke-static {v0, v1, v2}, Lcom/android/settingslib/w;->cqY(Landroid/content/Context;Ljava/lang/String;I)Lcom/android/settingslib/n;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/settingslib/d/a;->setDisabledByAdmin(Lcom/android/settingslib/n;)V

    goto :goto_0

    :cond_2
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/android/settingslib/d/a;->setEnabled(Z)V

    goto :goto_0
.end method

.method synthetic cmS(Landroid/content/DialogInterface;I)V
    .locals 1

    iget-object v0, p0, Lcom/android/settingslib/d/a;->cHz:Landroid/view/inputmethod/InputMethodInfo;

    invoke-virtual {v0}, Landroid/view/inputmethod/InputMethodInfo;->getServiceInfo()Landroid/content/pm/ServiceInfo;

    move-result-object v0

    iget-boolean v0, v0, Landroid/content/pm/ServiceInfo;->directBootAware:Z

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/android/settingslib/d/a;->cmO()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/android/settingslib/d/a;->setCheckedInternal(Z)V

    :goto_0
    return-void

    :cond_1
    invoke-direct {p0}, Lcom/android/settingslib/d/a;->cmQ()V

    goto :goto_0
.end method

.method synthetic cmV(Landroid/content/DialogInterface;I)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/android/settingslib/d/a;->setCheckedInternal(Z)V

    return-void
.end method

.method synthetic cmX(Landroid/content/DialogInterface;I)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/android/settingslib/d/a;->setCheckedInternal(Z)V

    return-void
.end method

.method public cmY(Lcom/android/settingslib/d/a;Ljava/text/Collator;)I
    .locals 5

    const/4 v1, 0x1

    const/4 v0, -0x1

    if-ne p0, p1, :cond_0

    const/4 v0, 0x0

    return v0

    :cond_0
    iget-boolean v2, p0, Lcom/android/settingslib/d/a;->cHA:Z

    iget-boolean v3, p1, Lcom/android/settingslib/d/a;->cHA:Z

    if-ne v2, v3, :cond_3

    invoke-virtual {p0}, Lcom/android/settingslib/d/a;->getTitle()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {p1}, Lcom/android/settingslib/d/a;->getTitle()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_1

    return v1

    :cond_1
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    return v0

    :cond_2
    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v3}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v0, v1}, Ljava/text/Collator;->compare(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    return v0

    :cond_3
    iget-boolean v2, p0, Lcom/android/settingslib/d/a;->cHA:Z

    if-eqz v2, :cond_4

    :goto_0
    return v0

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method public onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/android/settingslib/d/a;->cmR()Z

    move-result v0

    if-nez v0, :cond_0

    return v1

    :cond_0
    invoke-virtual {p0}, Lcom/android/settingslib/d/a;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-direct {p0, v1}, Lcom/android/settingslib/d/a;->setCheckedInternal(Z)V

    return v1

    :cond_1
    iget-object v0, p0, Lcom/android/settingslib/d/a;->cHz:Landroid/view/inputmethod/InputMethodInfo;

    invoke-static {v0}, Lcom/android/internal/inputmethod/InputMethodUtils;->isSystemIme(Landroid/view/inputmethod/InputMethodInfo;)Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/android/settingslib/d/a;->cHz:Landroid/view/inputmethod/InputMethodInfo;

    invoke-virtual {v0}, Landroid/view/inputmethod/InputMethodInfo;->getServiceInfo()Landroid/content/pm/ServiceInfo;

    move-result-object v0

    iget-boolean v0, v0, Landroid/content/pm/ServiceInfo;->directBootAware:Z

    if-nez v0, :cond_2

    invoke-direct {p0}, Lcom/android/settingslib/d/a;->cmO()Z

    move-result v0

    if-eqz v0, :cond_4

    :cond_2
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/android/settingslib/d/a;->setCheckedInternal(Z)V

    :cond_3
    :goto_0
    return v1

    :cond_4
    invoke-direct {p0}, Lcom/android/settingslib/d/a;->cmO()Z

    move-result v0

    if-nez v0, :cond_3

    invoke-direct {p0}, Lcom/android/settingslib/d/a;->cmQ()V

    goto :goto_0

    :cond_5
    invoke-direct {p0}, Lcom/android/settingslib/d/a;->cmT()V

    goto :goto_0
.end method

.method public onPreferenceClick(Landroid/preference/Preference;)Z
    .locals 6

    const/4 v5, 0x1

    invoke-direct {p0}, Lcom/android/settingslib/d/a;->cmR()Z

    move-result v0

    if-eqz v0, :cond_0

    return v5

    :cond_0
    invoke-virtual {p0}, Lcom/android/settingslib/d/a;->getContext()Landroid/content/Context;

    move-result-object v1

    :try_start_0
    invoke-virtual {p0}, Lcom/android/settingslib/d/a;->getIntent()Landroid/content/Intent;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_1
    :goto_0
    return v5

    :catch_0
    move-exception v0

    sget-object v2, Lcom/android/settingslib/d/a;->TAG:Ljava/lang/String;

    const-string/jumbo v3, "IME\'s Settings Activity Not Found"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    sget v0, Lcom/android/settingslib/i;->cMw:I

    new-array v2, v5, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/android/settingslib/d/a;->cHz:Landroid/view/inputmethod/InputMethodInfo;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/view/inputmethod/InputMethodInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v3

    const/4 v4, 0x0

    aput-object v3, v2, v4

    invoke-virtual {v1, v0, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method
