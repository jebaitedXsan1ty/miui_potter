.class public Lcom/android/settingslib/TwoTargetPreference;
.super Landroid/support/v7/preference/Preference;
.source "TwoTargetPreference.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    invoke-direct {p0, p1}, Landroid/support/v7/preference/Preference;-><init>(Landroid/content/Context;)V

    invoke-direct {p0}, Lcom/android/settingslib/TwoTargetPreference;->cpU()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Landroid/support/v7/preference/Preference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    invoke-direct {p0}, Lcom/android/settingslib/TwoTargetPreference;->cpU()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Landroid/support/v7/preference/Preference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    invoke-direct {p0}, Lcom/android/settingslib/TwoTargetPreference;->cpU()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V
    .locals 0

    invoke-direct {p0, p1, p2, p3, p4}, Landroid/support/v7/preference/Preference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V

    invoke-direct {p0}, Lcom/android/settingslib/TwoTargetPreference;->cpU()V

    return-void
.end method

.method private cpU()V
    .locals 1

    sget v0, Lcom/android/settingslib/h;->cLj:I

    invoke-virtual {p0, v0}, Lcom/android/settingslib/TwoTargetPreference;->setLayoutResource(I)V

    invoke-virtual {p0}, Lcom/android/settingslib/TwoTargetPreference;->ayU()I

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0, v0}, Lcom/android/settingslib/TwoTargetPreference;->dkS(I)V

    :cond_0
    return-void
.end method


# virtual methods
.method public al(Landroid/support/v7/preference/l;)V
    .locals 6

    const/16 v1, 0x8

    const/4 v2, 0x0

    invoke-super {p0, p1}, Landroid/support/v7/preference/Preference;->al(Landroid/support/v7/preference/l;)V

    sget v0, Lcom/android/settingslib/g;->cLh:I

    invoke-virtual {p1, v0}, Landroid/support/v7/preference/l;->dma(I)Landroid/view/View;

    move-result-object v3

    const v0, 0x1020018

    invoke-virtual {p1, v0}, Landroid/support/v7/preference/l;->dma(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {p0}, Lcom/android/settingslib/TwoTargetPreference;->ayV()Z

    move-result v5

    if-eqz v3, :cond_0

    if-eqz v5, :cond_2

    move v0, v1

    :goto_0
    invoke-virtual {v3, v0}, Landroid/view/View;->setVisibility(I)V

    :cond_0
    if-eqz v4, :cond_1

    if-eqz v5, :cond_3

    :goto_1
    invoke-virtual {v4, v1}, Landroid/view/View;->setVisibility(I)V

    :cond_1
    return-void

    :cond_2
    move v0, v2

    goto :goto_0

    :cond_3
    move v1, v2

    goto :goto_1
.end method

.method protected ayU()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method protected ayV()Z
    .locals 2

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/android/settingslib/TwoTargetPreference;->ayU()I

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method
