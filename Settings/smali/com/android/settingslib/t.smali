.class final Lcom/android/settingslib/t;
.super Landroid/os/AsyncTask;
.source "BatteryInfo.java"


# instance fields
.field final synthetic cQl:Z

.field final synthetic cQm:Lcom/android/settingslib/q;

.field final synthetic val$context:Landroid/content/Context;


# direct methods
.method constructor <init>(Landroid/content/Context;ZLcom/android/settingslib/q;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settingslib/t;->val$context:Landroid/content/Context;

    iput-boolean p2, p0, Lcom/android/settingslib/t;->cQl:Z

    iput-object p3, p0, Lcom/android/settingslib/t;->cQm:Lcom/android/settingslib/q;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected cqs(Landroid/os/BatteryStats;)V
    .locals 7

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    const-wide/16 v2, 0x3e8

    mul-long v4, v0, v2

    iget-object v0, p0, Lcom/android/settingslib/t;->val$context:Landroid/content/Context;

    new-instance v1, Landroid/content/IntentFilter;

    const-string/jumbo v2, "android.intent.action.BATTERY_CHANGED"

    invoke-direct {v1, v2}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    const/4 v2, 0x0

    invoke-virtual {v0, v2, v1}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    move-result-object v2

    iget-object v1, p0, Lcom/android/settingslib/t;->val$context:Landroid/content/Context;

    iget-boolean v6, p0, Lcom/android/settingslib/t;->cQl:Z

    move-object v3, p1

    invoke-static/range {v1 .. v6}, Lcom/android/settingslib/p;->cqo(Landroid/content/Context;Landroid/content/Intent;Landroid/os/BatteryStats;JZ)Lcom/android/settingslib/p;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settingslib/t;->cQm:Lcom/android/settingslib/q;

    invoke-interface {v1, v0}, Lcom/android/settingslib/q;->KE(Lcom/android/settingslib/p;)V

    return-void
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Landroid/os/BatteryStats;
    .locals 3

    new-instance v1, Lcom/android/internal/os/BatteryStatsHelper;

    iget-object v0, p0, Lcom/android/settingslib/t;->val$context:Landroid/content/Context;

    const/4 v2, 0x1

    invoke-direct {v1, v0, v2}, Lcom/android/internal/os/BatteryStatsHelper;-><init>(Landroid/content/Context;Z)V

    const/4 v0, 0x0

    check-cast v0, Landroid/os/Bundle;

    invoke-virtual {v1, v0}, Lcom/android/internal/os/BatteryStatsHelper;->create(Landroid/os/Bundle;)V

    invoke-virtual {v1}, Lcom/android/internal/os/BatteryStatsHelper;->getStats()Landroid/os/BatteryStats;

    move-result-object v0

    return-object v0
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/android/settingslib/t;->doInBackground([Ljava/lang/Void;)Landroid/os/BatteryStats;

    move-result-object v0

    return-object v0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    check-cast p1, Landroid/os/BatteryStats;

    invoke-virtual {p0, p1}, Lcom/android/settingslib/t;->cqs(Landroid/os/BatteryStats;)V

    return-void
.end method
