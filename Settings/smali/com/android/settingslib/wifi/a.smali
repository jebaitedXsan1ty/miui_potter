.class public Lcom/android/settingslib/wifi/a;
.super Ljava/lang/Object;
.source "WifiTracker.java"


# static fields
.field private static final cCb:Z

.field public static cCd:I


# instance fields
.field private final cBV:Z

.field private final cBW:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private cBX:Landroid/net/wifi/WifiInfo;

.field private final cBY:Ljava/util/Set;

.field private final cBZ:Landroid/content/IntentFilter;

.field private final cCa:Lcom/android/settingslib/wifi/c;

.field private cCc:Z

.field private cCe:Ljava/lang/Integer;

.field private final cCf:Landroid/net/NetworkRequest;

.field private final cCg:Landroid/net/wifi/WifiManager;

.field private final cCh:Lcom/android/settingslib/wifi/e;

.field private final cCi:Ljava/util/HashMap;

.field private final cCj:Ljava/util/List;

.field private cCk:Lcom/android/settingslib/wifi/b;

.field private final cCl:Lcom/android/settingslib/wifi/f;

.field private final cCm:Ljava/util/List;

.field private final cCn:Z

.field private cCo:Z

.field private final cCp:Landroid/net/ConnectivityManager;

.field private final cCq:Ljava/util/HashMap;

.field private final cCr:Z

.field private final cCs:Landroid/net/NetworkScoreManager;

.field private cCt:Landroid/net/NetworkInfo;

.field private final cCu:Landroid/net/wifi/WifiNetworkScoreCache;

.field public final mContext:Landroid/content/Context;

.field private final mLock:Ljava/lang/Object;

.field final mMainHandler:Lcom/android/settingslib/wifi/WifiTracker$MainHandler;

.field final mReceiver:Landroid/content/BroadcastReceiver;

.field mScanner:Lcom/android/settingslib/wifi/WifiTracker$Scanner;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const-string/jumbo v0, "WifiTracker"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    sput-boolean v0, Lcom/android/settingslib/wifi/a;->cCb:Z

    const/4 v0, 0x0

    sput v0, Lcom/android/settingslib/wifi/a;->cCd:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/android/settingslib/wifi/e;Landroid/os/Looper;ZZZ)V
    .locals 11

    const-class v0, Landroid/net/wifi/WifiManager;

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/net/wifi/WifiManager;

    const-class v0, Landroid/net/ConnectivityManager;

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/net/ConnectivityManager;

    const-class v0, Landroid/net/NetworkScoreManager;

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Landroid/net/NetworkScoreManager;

    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v10

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    move/from16 v5, p5

    move/from16 v6, p6

    invoke-direct/range {v0 .. v10}, Lcom/android/settingslib/wifi/a;-><init>(Landroid/content/Context;Lcom/android/settingslib/wifi/e;Landroid/os/Looper;ZZZLandroid/net/wifi/WifiManager;Landroid/net/ConnectivityManager;Landroid/net/NetworkScoreManager;Landroid/os/Looper;)V

    return-void
.end method

.method constructor <init>(Landroid/content/Context;Lcom/android/settingslib/wifi/e;Landroid/os/Looper;ZZZLandroid/net/wifi/WifiManager;Landroid/net/ConnectivityManager;Landroid/net/NetworkScoreManager;Landroid/os/Looper;)V
    .locals 3

    const/4 v2, 0x0

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, Lcom/android/settingslib/wifi/a;->cBW:Ljava/util/concurrent/atomic/AtomicBoolean;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/settingslib/wifi/a;->cCm:Ljava/util/List;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/settingslib/wifi/a;->cCj:Ljava/util/List;

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/android/settingslib/wifi/a;->mLock:Ljava/lang/Object;

    new-instance v0, Lcom/android/settingslib/wifi/f;

    invoke-direct {v0, v2}, Lcom/android/settingslib/wifi/f;-><init>(Lcom/android/settingslib/wifi/f;)V

    iput-object v0, p0, Lcom/android/settingslib/wifi/a;->cCl:Lcom/android/settingslib/wifi/f;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/android/settingslib/wifi/a;->cCi:Ljava/util/HashMap;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/android/settingslib/wifi/a;->cCq:Ljava/util/HashMap;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settingslib/wifi/a;->cCe:Ljava/lang/Integer;

    new-instance v0, Landroid/util/ArraySet;

    invoke-direct {v0}, Landroid/util/ArraySet;-><init>()V

    iput-object v0, p0, Lcom/android/settingslib/wifi/a;->cBY:Ljava/util/Set;

    iput-boolean v1, p0, Lcom/android/settingslib/wifi/a;->cCo:Z

    new-instance v0, Lcom/android/settingslib/wifi/g;

    invoke-direct {v0, p0}, Lcom/android/settingslib/wifi/g;-><init>(Lcom/android/settingslib/wifi/a;)V

    iput-object v0, p0, Lcom/android/settingslib/wifi/a;->mReceiver:Landroid/content/BroadcastReceiver;

    if-nez p4, :cond_0

    xor-int/lit8 v0, p5, 0x1

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "Must include either saved or scans"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iput-object p1, p0, Lcom/android/settingslib/wifi/a;->mContext:Landroid/content/Context;

    if-nez p10, :cond_1

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object p10

    :cond_1
    new-instance v0, Lcom/android/settingslib/wifi/WifiTracker$MainHandler;

    invoke-direct {v0, p0, p10}, Lcom/android/settingslib/wifi/WifiTracker$MainHandler;-><init>(Lcom/android/settingslib/wifi/a;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/android/settingslib/wifi/a;->mMainHandler:Lcom/android/settingslib/wifi/WifiTracker$MainHandler;

    new-instance v0, Lcom/android/settingslib/wifi/c;

    if-eqz p3, :cond_2

    :goto_0
    invoke-direct {v0, p0, p3}, Lcom/android/settingslib/wifi/c;-><init>(Lcom/android/settingslib/wifi/a;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/android/settingslib/wifi/a;->cCa:Lcom/android/settingslib/wifi/c;

    iput-object p7, p0, Lcom/android/settingslib/wifi/a;->cCg:Landroid/net/wifi/WifiManager;

    iput-boolean p4, p0, Lcom/android/settingslib/wifi/a;->cCr:Z

    iput-boolean p5, p0, Lcom/android/settingslib/wifi/a;->cBV:Z

    iput-boolean p6, p0, Lcom/android/settingslib/wifi/a;->cCn:Z

    iput-object p2, p0, Lcom/android/settingslib/wifi/a;->cCh:Lcom/android/settingslib/wifi/e;

    iput-object p8, p0, Lcom/android/settingslib/wifi/a;->cCp:Landroid/net/ConnectivityManager;

    iget-object v0, p0, Lcom/android/settingslib/wifi/a;->cCg:Landroid/net/wifi/WifiManager;

    invoke-virtual {v0}, Landroid/net/wifi/WifiManager;->getVerboseLoggingLevel()I

    move-result v0

    sput v0, Lcom/android/settingslib/wifi/a;->cCd:I

    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    iput-object v0, p0, Lcom/android/settingslib/wifi/a;->cBZ:Landroid/content/IntentFilter;

    iget-object v0, p0, Lcom/android/settingslib/wifi/a;->cBZ:Landroid/content/IntentFilter;

    const-string/jumbo v1, "android.net.wifi.WIFI_STATE_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/settingslib/wifi/a;->cBZ:Landroid/content/IntentFilter;

    const-string/jumbo v1, "android.net.wifi.SCAN_RESULTS"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/settingslib/wifi/a;->cBZ:Landroid/content/IntentFilter;

    const-string/jumbo v1, "android.net.wifi.NETWORK_IDS_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/settingslib/wifi/a;->cBZ:Landroid/content/IntentFilter;

    const-string/jumbo v1, "android.net.wifi.supplicant.STATE_CHANGE"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/settingslib/wifi/a;->cBZ:Landroid/content/IntentFilter;

    const-string/jumbo v1, "android.net.wifi.CONFIGURED_NETWORKS_CHANGE"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/settingslib/wifi/a;->cBZ:Landroid/content/IntentFilter;

    const-string/jumbo v1, "android.net.wifi.LINK_CONFIGURATION_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/settingslib/wifi/a;->cBZ:Landroid/content/IntentFilter;

    const-string/jumbo v1, "android.net.wifi.STATE_CHANGE"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/settingslib/wifi/a;->cBZ:Landroid/content/IntentFilter;

    const-string/jumbo v1, "android.net.wifi.RSSI_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    new-instance v0, Landroid/net/NetworkRequest$Builder;

    invoke-direct {v0}, Landroid/net/NetworkRequest$Builder;-><init>()V

    invoke-virtual {v0}, Landroid/net/NetworkRequest$Builder;->clearCapabilities()Landroid/net/NetworkRequest$Builder;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/net/NetworkRequest$Builder;->addTransportType(I)Landroid/net/NetworkRequest$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/NetworkRequest$Builder;->build()Landroid/net/NetworkRequest;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settingslib/wifi/a;->cCf:Landroid/net/NetworkRequest;

    iput-object p9, p0, Lcom/android/settingslib/wifi/a;->cCs:Landroid/net/NetworkScoreManager;

    new-instance v0, Landroid/net/wifi/WifiNetworkScoreCache;

    new-instance v1, Lcom/android/settingslib/wifi/h;

    iget-object v2, p0, Lcom/android/settingslib/wifi/a;->cCa:Lcom/android/settingslib/wifi/c;

    invoke-direct {v1, p0, v2}, Lcom/android/settingslib/wifi/h;-><init>(Lcom/android/settingslib/wifi/a;Landroid/os/Handler;)V

    invoke-direct {v0, p1, v1}, Landroid/net/wifi/WifiNetworkScoreCache;-><init>(Landroid/content/Context;Landroid/net/wifi/WifiNetworkScoreCache$CacheListener;)V

    iput-object v0, p0, Lcom/android/settingslib/wifi/a;->cCu:Landroid/net/wifi/WifiNetworkScoreCache;

    return-void

    :cond_2
    move-object p3, p10

    goto/16 :goto_0
.end method

.method static synthetic cgA(Lcom/android/settingslib/wifi/a;Landroid/net/wifi/WifiInfo;)Landroid/net/wifi/WifiInfo;
    .locals 0

    iput-object p1, p0, Lcom/android/settingslib/wifi/a;->cBX:Landroid/net/wifi/WifiInfo;

    return-object p1
.end method

.method private cgC(I)Landroid/net/wifi/WifiConfiguration;
    .locals 5

    const/4 v4, 0x0

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/android/settingslib/wifi/a;->cCg:Landroid/net/wifi/WifiManager;

    invoke-virtual {v0}, Landroid/net/wifi/WifiManager;->getConfiguredNetworks()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/WifiConfiguration;

    iget-object v1, p0, Lcom/android/settingslib/wifi/a;->cBX:Landroid/net/wifi/WifiInfo;

    if-eqz v1, :cond_0

    iget v1, v0, Landroid/net/wifi/WifiConfiguration;->networkId:I

    if-ne p1, v1, :cond_0

    iget-boolean v1, v0, Landroid/net/wifi/WifiConfiguration;->selfAdded:Z

    if-eqz v1, :cond_1

    iget v1, v0, Landroid/net/wifi/WifiConfiguration;->numAssociation:I

    if-nez v1, :cond_1

    const/4 v1, 0x1

    :goto_0
    xor-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    return-object v0

    :cond_1
    move v1, v2

    goto :goto_0

    :cond_2
    return-object v4
.end method

.method public static cgE(Landroid/content/Context;ZZZ)Ljava/util/List;
    .locals 7

    const/4 v2, 0x0

    new-instance v0, Lcom/android/settingslib/wifi/a;

    move-object v1, p0

    move-object v3, v2

    move v4, p1

    move v5, p2

    move v6, p3

    invoke-direct/range {v0 .. v6}, Lcom/android/settingslib/wifi/a;-><init>(Landroid/content/Context;Lcom/android/settingslib/wifi/e;Landroid/os/Looper;ZZZ)V

    invoke-virtual {v0}, Lcom/android/settingslib/wifi/a;->cgz()V

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/android/settingslib/wifi/a;->cgQ(Z)V

    invoke-virtual {v0}, Lcom/android/settingslib/wifi/a;->cgM()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method private cgF(Landroid/net/NetworkInfo;)V
    .locals 10

    const/4 v5, 0x4

    const/4 v0, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x0

    iget-object v1, p0, Lcom/android/settingslib/wifi/a;->cCg:Landroid/net/wifi/WifiManager;

    invoke-virtual {v1}, Landroid/net/wifi/WifiManager;->isWifiEnabled()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v0, p0, Lcom/android/settingslib/wifi/a;->mMainHandler:Lcom/android/settingslib/wifi/WifiTracker$MainHandler;

    invoke-virtual {v0, v5}, Lcom/android/settingslib/wifi/WifiTracker$MainHandler;->sendEmptyMessage(I)Z

    return-void

    :cond_0
    if-eqz p1, :cond_3

    invoke-virtual {p1}, Landroid/net/NetworkInfo;->getDetailedState()Landroid/net/NetworkInfo$DetailedState;

    move-result-object v1

    sget-object v4, Landroid/net/NetworkInfo$DetailedState;->OBTAINING_IPADDR:Landroid/net/NetworkInfo$DetailedState;

    if-ne v1, v4, :cond_3

    iget-object v1, p0, Lcom/android/settingslib/wifi/a;->mMainHandler:Lcom/android/settingslib/wifi/WifiTracker$MainHandler;

    invoke-virtual {v1, v5}, Lcom/android/settingslib/wifi/WifiTracker$MainHandler;->sendEmptyMessage(I)Z

    :goto_0
    if-eqz p1, :cond_1

    iput-object p1, p0, Lcom/android/settingslib/wifi/a;->cCt:Landroid/net/NetworkInfo;

    :cond_1
    iget-object v1, p0, Lcom/android/settingslib/wifi/a;->cCg:Landroid/net/wifi/WifiManager;

    invoke-virtual {v1}, Landroid/net/wifi/WifiManager;->getConnectionInfo()Landroid/net/wifi/WifiInfo;

    move-result-object v1

    iput-object v1, p0, Lcom/android/settingslib/wifi/a;->cBX:Landroid/net/wifi/WifiInfo;

    iget-object v1, p0, Lcom/android/settingslib/wifi/a;->cBX:Landroid/net/wifi/WifiInfo;

    if-eqz v1, :cond_9

    iget-object v0, p0, Lcom/android/settingslib/wifi/a;->cBX:Landroid/net/wifi/WifiInfo;

    invoke-virtual {v0}, Landroid/net/wifi/WifiInfo;->getNetworkId()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/android/settingslib/wifi/a;->cgC(I)Landroid/net/wifi/WifiConfiguration;

    move-result-object v0

    move-object v1, v0

    :goto_1
    iget-object v6, p0, Lcom/android/settingslib/wifi/a;->mLock:Ljava/lang/Object;

    monitor-enter v6

    :try_start_0
    iget-object v0, p0, Lcom/android/settingslib/wifi/a;->cCj:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v5, v0

    move v4, v3

    :goto_2
    if-ltz v5, :cond_4

    iget-object v0, p0, Lcom/android/settingslib/wifi/a;->cCj:Ljava/util/List;

    invoke-interface {v0, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settingslib/wifi/i;

    invoke-virtual {v0}, Lcom/android/settingslib/wifi/i;->chM()Z

    move-result v7

    iget-object v8, p0, Lcom/android/settingslib/wifi/a;->cBX:Landroid/net/wifi/WifiInfo;

    iget-object v9, p0, Lcom/android/settingslib/wifi/a;->cCt:Landroid/net/NetworkInfo;

    invoke-virtual {v0, v1, v8, v9}, Lcom/android/settingslib/wifi/i;->cin(Landroid/net/wifi/WifiConfiguration;Landroid/net/wifi/WifiInfo;Landroid/net/NetworkInfo;)Z

    move-result v8

    if-eqz v8, :cond_2

    invoke-virtual {v0}, Lcom/android/settingslib/wifi/i;->chM()Z

    move-result v4

    if-eq v7, v4, :cond_8

    move v3, v2

    move v4, v2

    :cond_2
    :goto_3
    iget-object v7, p0, Lcom/android/settingslib/wifi/a;->cCu:Landroid/net/wifi/WifiNetworkScoreCache;

    const/4 v8, 0x0

    invoke-virtual {v0, v7, v8}, Lcom/android/settingslib/wifi/i;->chX(Landroid/net/wifi/WifiNetworkScoreCache;Z)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_7

    move v0, v2

    move v3, v2

    :goto_4
    add-int/lit8 v4, v5, -0x1

    move v5, v4

    move v4, v3

    move v3, v0

    goto :goto_2

    :cond_3
    iget-object v1, p0, Lcom/android/settingslib/wifi/a;->mMainHandler:Lcom/android/settingslib/wifi/WifiTracker$MainHandler;

    const/4 v4, 0x3

    invoke-virtual {v1, v4}, Lcom/android/settingslib/wifi/WifiTracker$MainHandler;->sendEmptyMessage(I)Z

    goto :goto_0

    :cond_4
    if-eqz v3, :cond_5

    :try_start_1
    iget-object v0, p0, Lcom/android/settingslib/wifi/a;->cCj:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    :cond_5
    if-eqz v4, :cond_6

    iget-object v0, p0, Lcom/android/settingslib/wifi/a;->mMainHandler:Lcom/android/settingslib/wifi/WifiTracker$MainHandler;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/android/settingslib/wifi/WifiTracker$MainHandler;->sendEmptyMessage(I)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_6
    monitor-exit v6

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v6

    throw v0

    :cond_7
    move v0, v3

    move v3, v4

    goto :goto_4

    :cond_8
    move v4, v2

    goto :goto_3

    :cond_9
    move-object v1, v0

    goto :goto_1
.end method

.method static synthetic cgG(Lcom/android/settingslib/wifi/a;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/settingslib/wifi/a;->cCo:Z

    return v0
.end method

.method private cgH()V
    .locals 12

    const/4 v3, 0x1

    const/4 v4, 0x0

    const/4 v1, 0x0

    new-instance v5, Ljava/util/ArrayList;

    iget-object v0, p0, Lcom/android/settingslib/wifi/a;->cCj:Ljava/util/List;

    invoke-direct {v5, v0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {v5}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settingslib/wifi/i;

    invoke-virtual {v0}, Lcom/android/settingslib/wifi/i;->cic()V

    goto :goto_0

    :cond_0
    new-instance v7, Lcom/android/settingslib/wifi/d;

    invoke-direct {v7, v1}, Lcom/android/settingslib/wifi/d;-><init>(Lcom/android/settingslib/wifi/d;)V

    iget-object v0, p0, Lcom/android/settingslib/wifi/a;->cBX:Landroid/net/wifi/WifiInfo;

    if-eqz v0, :cond_16

    iget-object v0, p0, Lcom/android/settingslib/wifi/a;->cBX:Landroid/net/wifi/WifiInfo;

    invoke-virtual {v0}, Landroid/net/wifi/WifiInfo;->getNetworkId()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/android/settingslib/wifi/a;->cgC(I)Landroid/net/wifi/WifiConfiguration;

    move-result-object v0

    move-object v2, v0

    :goto_1
    invoke-direct {p0}, Lcom/android/settingslib/wifi/a;->cgP()Ljava/util/Collection;

    move-result-object v1

    iget-object v0, p0, Lcom/android/settingslib/wifi/a;->cCg:Landroid/net/wifi/WifiManager;

    invoke-virtual {v0}, Landroid/net/wifi/WifiManager;->getConfiguredNetworks()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_7

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :cond_1
    :goto_2
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/WifiConfiguration;

    iget-boolean v9, v0, Landroid/net/wifi/WifiConfiguration;->selfAdded:Z

    if-eqz v9, :cond_2

    iget v9, v0, Landroid/net/wifi/WifiConfiguration;->numAssociation:I

    if-eqz v9, :cond_1

    :cond_2
    invoke-virtual {p0, v0, v5}, Lcom/android/settingslib/wifi/a;->getCachedOrCreate(Landroid/net/wifi/WifiConfiguration;Ljava/util/List;)Lcom/android/settingslib/wifi/i;

    move-result-object v9

    iget-object v0, p0, Lcom/android/settingslib/wifi/a;->cBX:Landroid/net/wifi/WifiInfo;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/android/settingslib/wifi/a;->cCt:Landroid/net/NetworkInfo;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/android/settingslib/wifi/a;->cBX:Landroid/net/wifi/WifiInfo;

    iget-object v10, p0, Lcom/android/settingslib/wifi/a;->cCt:Landroid/net/NetworkInfo;

    invoke-virtual {v9, v2, v0, v10}, Lcom/android/settingslib/wifi/i;->cin(Landroid/net/wifi/WifiConfiguration;Landroid/net/wifi/WifiInfo;Landroid/net/NetworkInfo;)Z

    :cond_3
    iget-boolean v0, p0, Lcom/android/settingslib/wifi/a;->cCr:Z

    if-eqz v0, :cond_6

    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :cond_4
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_15

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/ScanResult;

    iget-object v0, v0, Landroid/net/wifi/ScanResult;->SSID:Ljava/lang/String;

    invoke-virtual {v9}, Lcom/android/settingslib/wifi/i;->cih()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v0, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    move v0, v3

    :goto_3
    if-nez v0, :cond_5

    invoke-virtual {v9}, Lcom/android/settingslib/wifi/i;->chR()V

    :cond_5
    invoke-virtual {v6, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual {v9}, Lcom/android/settingslib/wifi/i;->cih()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v7, v0, v9}, Lcom/android/settingslib/wifi/d;->put(Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_2

    :cond_6
    invoke-interface {v5, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_7
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    if-eqz v1, :cond_d

    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :cond_8
    :goto_4
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_d

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/ScanResult;

    iget-object v1, v0, Landroid/net/wifi/ScanResult;->SSID:Ljava/lang/String;

    if-eqz v1, :cond_8

    iget-object v1, v0, Landroid/net/wifi/ScanResult;->SSID:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-eqz v1, :cond_8

    iget-object v1, v0, Landroid/net/wifi/ScanResult;->capabilities:Ljava/lang/String;

    const-string/jumbo v10, "[IBSS]"

    invoke-virtual {v1, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_8

    invoke-static {v0}, Landroid/net/NetworkKey;->createFromScanResult(Landroid/net/wifi/ScanResult;)Landroid/net/NetworkKey;

    move-result-object v1

    if-eqz v1, :cond_9

    iget-object v10, p0, Lcom/android/settingslib/wifi/a;->cBY:Ljava/util/Set;

    invoke-interface {v10, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v10

    xor-int/lit8 v10, v10, 0x1

    if-eqz v10, :cond_9

    invoke-interface {v8, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_9
    iget-object v1, v0, Landroid/net/wifi/ScanResult;->SSID:Ljava/lang/String;

    invoke-virtual {v7, v1}, Lcom/android/settingslib/wifi/d;->chg(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :cond_a
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_14

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/settingslib/wifi/i;

    invoke-virtual {v1, v0}, Lcom/android/settingslib/wifi/i;->chJ(Landroid/net/wifi/ScanResult;)Z

    move-result v1

    if-eqz v1, :cond_a

    move v1, v3

    :goto_5
    if-nez v1, :cond_8

    iget-boolean v1, p0, Lcom/android/settingslib/wifi/a;->cBV:Z

    if-eqz v1, :cond_8

    invoke-virtual {p0, v0, v5}, Lcom/android/settingslib/wifi/a;->getCachedOrCreate(Landroid/net/wifi/ScanResult;Ljava/util/List;)Lcom/android/settingslib/wifi/i;

    move-result-object v1

    iget-object v10, p0, Lcom/android/settingslib/wifi/a;->cBX:Landroid/net/wifi/WifiInfo;

    if-eqz v10, :cond_b

    iget-object v10, p0, Lcom/android/settingslib/wifi/a;->cCt:Landroid/net/NetworkInfo;

    if-eqz v10, :cond_b

    iget-object v10, p0, Lcom/android/settingslib/wifi/a;->cBX:Landroid/net/wifi/WifiInfo;

    iget-object v11, p0, Lcom/android/settingslib/wifi/a;->cCt:Landroid/net/NetworkInfo;

    invoke-virtual {v1, v2, v10, v11}, Lcom/android/settingslib/wifi/i;->cin(Landroid/net/wifi/WifiConfiguration;Landroid/net/wifi/WifiInfo;Landroid/net/NetworkInfo;)Z

    :cond_b
    invoke-virtual {v0}, Landroid/net/wifi/ScanResult;->isPasspointNetwork()Z

    move-result v10

    if-eqz v10, :cond_c

    :try_start_0
    iget-object v10, p0, Lcom/android/settingslib/wifi/a;->cCg:Landroid/net/wifi/WifiManager;

    invoke-virtual {v10, v0}, Landroid/net/wifi/WifiManager;->getMatchingWifiConfig(Landroid/net/wifi/ScanResult;)Landroid/net/wifi/WifiConfiguration;

    move-result-object v0

    if-eqz v0, :cond_c

    invoke-virtual {v1, v0}, Lcom/android/settingslib/wifi/i;->chA(Landroid/net/wifi/WifiConfiguration;)V
    :try_end_0
    .catch Ljava/lang/UnsupportedOperationException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_c
    :goto_6
    invoke-virtual {v6, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual {v1}, Lcom/android/settingslib/wifi/i;->cih()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v7, v0, v1}, Lcom/android/settingslib/wifi/d;->put(Ljava/lang/Object;Ljava/lang/Object;)V

    goto/16 :goto_4

    :cond_d
    invoke-direct {p0, v8}, Lcom/android/settingslib/wifi/a;->cgv(Ljava/util/Collection;)V

    invoke-interface {v6}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_7
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_e

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settingslib/wifi/i;

    iget-object v2, p0, Lcom/android/settingslib/wifi/a;->cCu:Landroid/net/wifi/WifiNetworkScoreCache;

    invoke-virtual {v0, v2, v4}, Lcom/android/settingslib/wifi/i;->chX(Landroid/net/wifi/WifiNetworkScoreCache;Z)Z

    goto :goto_7

    :cond_e
    invoke-static {v6}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    sget-boolean v0, Lcom/android/settingslib/wifi/a;->cCb:Z

    if-eqz v0, :cond_12

    const-string/jumbo v0, "WifiTracker"

    const-string/jumbo v1, "------ Dumping SSIDs that were not seen on this scan ------"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/settingslib/wifi/a;->cCj:Ljava/util/List;

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_f
    :goto_8
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_11

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settingslib/wifi/i;

    invoke-virtual {v0}, Lcom/android/settingslib/wifi/i;->chW()Ljava/lang/CharSequence;

    move-result-object v2

    if-eqz v2, :cond_f

    invoke-virtual {v0}, Lcom/android/settingslib/wifi/i;->cih()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v6}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_10
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_13

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settingslib/wifi/i;

    invoke-virtual {v0}, Lcom/android/settingslib/wifi/i;->chW()Ljava/lang/CharSequence;

    move-result-object v7

    if-eqz v7, :cond_10

    invoke-virtual {v0}, Lcom/android/settingslib/wifi/i;->chW()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_10

    move v0, v3

    :goto_9
    if-nez v0, :cond_f

    const-string/jumbo v0, "WifiTracker"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "Did not find "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v5, " in this scan"

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_8

    :cond_11
    const-string/jumbo v0, "WifiTracker"

    const-string/jumbo v1, "---- Done dumping SSIDs that were not seen on this scan ----"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_12
    iget-object v0, p0, Lcom/android/settingslib/wifi/a;->cCj:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    iget-object v0, p0, Lcom/android/settingslib/wifi/a;->cCj:Ljava/util/List;

    invoke-interface {v0, v6}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    iget-object v0, p0, Lcom/android/settingslib/wifi/a;->mMainHandler:Lcom/android/settingslib/wifi/WifiTracker$MainHandler;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/android/settingslib/wifi/WifiTracker$MainHandler;->sendEmptyMessage(I)Z

    return-void

    :catch_0
    move-exception v0

    goto/16 :goto_6

    :cond_13
    move v0, v4

    goto :goto_9

    :cond_14
    move v1, v4

    goto/16 :goto_5

    :cond_15
    move v0, v4

    goto/16 :goto_3

    :cond_16
    move-object v2, v1

    goto/16 :goto_1
.end method

.method static synthetic cgJ(Lcom/android/settingslib/wifi/a;)Ljava/util/concurrent/atomic/AtomicBoolean;
    .locals 1

    iget-object v0, p0, Lcom/android/settingslib/wifi/a;->cBW:Ljava/util/concurrent/atomic/AtomicBoolean;

    return-object v0
.end method

.method private cgK()V
    .locals 4

    iget-object v0, p0, Lcom/android/settingslib/wifi/a;->cCs:Landroid/net/NetworkScoreManager;

    iget-object v1, p0, Lcom/android/settingslib/wifi/a;->cCu:Landroid/net/wifi/WifiNetworkScoreCache;

    const/4 v2, 0x1

    const/4 v3, 0x2

    invoke-virtual {v0, v2, v1, v3}, Landroid/net/NetworkScoreManager;->registerNetworkScoreCache(ILandroid/net/INetworkScoreCache;I)V

    return-void
.end method

.method static synthetic cgN(Lcom/android/settingslib/wifi/a;Landroid/net/NetworkInfo;)Landroid/net/NetworkInfo;
    .locals 0

    iput-object p1, p0, Lcom/android/settingslib/wifi/a;->cCt:Landroid/net/NetworkInfo;

    return-object p1
.end method

.method static synthetic cgO(Lcom/android/settingslib/wifi/a;)Lcom/android/settingslib/wifi/c;
    .locals 1

    iget-object v0, p0, Lcom/android/settingslib/wifi/a;->cCa:Lcom/android/settingslib/wifi/c;

    return-object v0
.end method

.method private cgP()Ljava/util/Collection;
    .locals 7

    iget-object v0, p0, Lcom/android/settingslib/wifi/a;->cCe:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settingslib/wifi/a;->cCe:Ljava/lang/Integer;

    iget-object v0, p0, Lcom/android/settingslib/wifi/a;->cCg:Landroid/net/wifi/WifiManager;

    invoke-virtual {v0}, Landroid/net/wifi/WifiManager;->getScanResults()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/ScanResult;

    iget-object v2, v0, Landroid/net/wifi/ScanResult;->SSID:Ljava/lang/String;

    if-eqz v2, :cond_0

    iget-object v2, v0, Landroid/net/wifi/ScanResult;->SSID:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/android/settingslib/wifi/a;->cCq:Ljava/util/HashMap;

    iget-object v3, v0, Landroid/net/wifi/ScanResult;->BSSID:Ljava/lang/String;

    invoke-virtual {v2, v3, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v2, p0, Lcom/android/settingslib/wifi/a;->cCi:Ljava/util/HashMap;

    iget-object v0, v0, Landroid/net/wifi/ScanResult;->BSSID:Ljava/lang/String;

    iget-object v3, p0, Lcom/android/settingslib/wifi/a;->cCe:Ljava/lang/Integer;

    invoke-virtual {v2, v0, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/android/settingslib/wifi/a;->cCe:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/4 v1, 0x3

    if-le v0, v1, :cond_6

    sget-boolean v0, Lcom/android/settingslib/wifi/a;->cCb:Z

    if-eqz v0, :cond_2

    const-string/jumbo v0, "WifiTracker"

    const-string/jumbo v1, "------ Dumping SSIDs that were expired on this scan ------"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    iget-object v0, p0, Lcom/android/settingslib/wifi/a;->cCe:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    add-int/lit8 v0, v0, -0x3

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    iget-object v0, p0, Lcom/android/settingslib/wifi/a;->cCi:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_3
    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v2

    if-ge v1, v2, :cond_3

    iget-object v1, p0, Lcom/android/settingslib/wifi/a;->cCq:Ljava/util/HashMap;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/net/wifi/ScanResult;

    sget-boolean v2, Lcom/android/settingslib/wifi/a;->cCb:Z

    if-eqz v2, :cond_4

    const-string/jumbo v5, "WifiTracker"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "Removing "

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v6, ":("

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v1, v1, Landroid/net/wifi/ScanResult;->SSID:Ljava/lang/String;

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v5, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_4
    iget-object v1, p0, Lcom/android/settingslib/wifi/a;->cCq:Ljava/util/HashMap;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-interface {v4}, Ljava/util/Iterator;->remove()V

    goto :goto_1

    :cond_5
    sget-boolean v0, Lcom/android/settingslib/wifi/a;->cCb:Z

    if-eqz v0, :cond_6

    const-string/jumbo v0, "WifiTracker"

    const-string/jumbo v1, "---- Done Dumping SSIDs that were expired on this scan ----"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_6
    iget-object v0, p0, Lcom/android/settingslib/wifi/a;->cCq:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    return-object v0
.end method

.method private cgQ(Z)V
    .locals 8

    const/4 v1, 0x0

    new-instance v3, Landroid/util/SparseArray;

    invoke-direct {v3}, Landroid/util/SparseArray;-><init>()V

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    iget-object v0, p0, Lcom/android/settingslib/wifi/a;->cCm:Ljava/util/List;

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settingslib/wifi/i;

    iget v5, v0, Lcom/android/settingslib/wifi/i;->mId:I

    invoke-virtual {v3, v5, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    goto :goto_0

    :cond_0
    sget-boolean v0, Lcom/android/settingslib/wifi/a;->cCb:Z

    if-eqz v0, :cond_1

    const-string/jumbo v0, "WifiTracker"

    const-string/jumbo v2, "Starting to copy AP items on the MainHandler"

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    iget-object v5, p0, Lcom/android/settingslib/wifi/a;->mLock:Ljava/lang/Object;

    monitor-enter v5

    if-eqz p1, :cond_7

    :try_start_0
    iget-object v0, p0, Lcom/android/settingslib/wifi/a;->cCl:Lcom/android/settingslib/wifi/f;

    iget-object v0, v0, Lcom/android/settingslib/wifi/f;->cCB:Landroid/util/SparseIntArray;

    invoke-virtual {v0}, Landroid/util/SparseIntArray;->clone()Landroid/util/SparseIntArray;

    move-result-object v0

    move-object v2, v0

    :goto_1
    iget-object v0, p0, Lcom/android/settingslib/wifi/a;->cCl:Lcom/android/settingslib/wifi/f;

    iget-object v0, v0, Lcom/android/settingslib/wifi/f;->cCB:Landroid/util/SparseIntArray;

    invoke-virtual {v0}, Landroid/util/SparseIntArray;->clear()V

    iget-object v0, p0, Lcom/android/settingslib/wifi/a;->cCj:Ljava/util/List;

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_2
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settingslib/wifi/i;

    iget v1, v0, Lcom/android/settingslib/wifi/i;->mId:I

    invoke-virtual {v3, v1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/settingslib/wifi/i;

    if-nez v1, :cond_2

    new-instance v1, Lcom/android/settingslib/wifi/i;

    iget-object v7, p0, Lcom/android/settingslib/wifi/a;->mContext:Landroid/content/Context;

    invoke-direct {v1, v7, v0}, Lcom/android/settingslib/wifi/i;-><init>(Landroid/content/Context;Lcom/android/settingslib/wifi/i;)V

    :goto_3
    invoke-interface {v4, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_2

    :catchall_0
    move-exception v0

    monitor-exit v5

    throw v0

    :cond_2
    :try_start_1
    invoke-virtual {v1, v0}, Lcom/android/settingslib/wifi/i;->cht(Lcom/android/settingslib/wifi/i;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_3

    :cond_3
    monitor-exit v5

    iget-object v0, p0, Lcom/android/settingslib/wifi/a;->cCm:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    iget-object v0, p0, Lcom/android/settingslib/wifi/a;->cCm:Ljava/util/List;

    invoke-interface {v0, v4}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    if-eqz v2, :cond_6

    invoke-virtual {v2}, Landroid/util/SparseIntArray;->size()I

    move-result v0

    if-lez v0, :cond_6

    invoke-interface {v4}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_4
    :goto_4
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settingslib/wifi/i;

    iget v3, v0, Lcom/android/settingslib/wifi/i;->mId:I

    invoke-virtual {v2, v3}, Landroid/util/SparseIntArray;->get(I)I

    move-result v3

    iget-object v4, v0, Lcom/android/settingslib/wifi/i;->cCM:Lcom/android/settingslib/wifi/j;

    if-eqz v3, :cond_4

    if-eqz v4, :cond_4

    and-int/lit8 v5, v3, 0x1

    if-eqz v5, :cond_5

    invoke-interface {v4, v0}, Lcom/android/settingslib/wifi/j;->aaX(Lcom/android/settingslib/wifi/i;)V

    :cond_5
    and-int/lit8 v3, v3, 0x2

    if-eqz v3, :cond_4

    invoke-interface {v4, v0}, Lcom/android/settingslib/wifi/j;->aba(Lcom/android/settingslib/wifi/i;)V

    goto :goto_4

    :cond_6
    return-void

    :cond_7
    move-object v2, v1

    goto :goto_1
.end method

.method static synthetic cgR(Lcom/android/settingslib/wifi/a;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/settingslib/wifi/a;->cgm()V

    return-void
.end method

.method static synthetic cgS(Lcom/android/settingslib/wifi/a;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/settingslib/wifi/a;->cgT()V

    return-void
.end method

.method private cgT()V
    .locals 1

    iget-object v0, p0, Lcom/android/settingslib/wifi/a;->cCq:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    iget-object v0, p0, Lcom/android/settingslib/wifi/a;->cCi:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settingslib/wifi/a;->cCe:Ljava/lang/Integer;

    return-void
.end method

.method static synthetic cgU(Lcom/android/settingslib/wifi/a;)Landroid/net/wifi/WifiManager;
    .locals 1

    iget-object v0, p0, Lcom/android/settingslib/wifi/a;->cCg:Landroid/net/wifi/WifiManager;

    return-object v0
.end method

.method private cgV()V
    .locals 2

    iget-object v0, p0, Lcom/android/settingslib/wifi/a;->mLock:Ljava/lang/Object;

    monitor-enter v0

    :try_start_0
    invoke-direct {p0}, Lcom/android/settingslib/wifi/a;->cgH()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v0

    return-void

    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method

.method static synthetic cgW(Lcom/android/settingslib/wifi/a;)Lcom/android/settingslib/wifi/e;
    .locals 1

    iget-object v0, p0, Lcom/android/settingslib/wifi/a;->cCh:Lcom/android/settingslib/wifi/e;

    return-object v0
.end method

.method static synthetic cgX(Lcom/android/settingslib/wifi/a;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/settingslib/wifi/a;->cCc:Z

    return v0
.end method

.method static synthetic cgZ(Lcom/android/settingslib/wifi/a;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/settingslib/wifi/a;->cgt(I)V

    return-void
.end method

.method private cgm()V
    .locals 6

    const/4 v0, 0x0

    iget-object v3, p0, Lcom/android/settingslib/wifi/a;->mLock:Ljava/lang/Object;

    monitor-enter v3

    move v2, v0

    move v1, v0

    :goto_0
    :try_start_0
    iget-object v0, p0, Lcom/android/settingslib/wifi/a;->cCj:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_0

    iget-object v0, p0, Lcom/android/settingslib/wifi/a;->cCj:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settingslib/wifi/i;

    iget-object v4, p0, Lcom/android/settingslib/wifi/a;->cCu:Landroid/net/wifi/WifiNetworkScoreCache;

    const/4 v5, 0x0

    invoke-virtual {v0, v4, v5}, Lcom/android/settingslib/wifi/i;->chX(Landroid/net/wifi/WifiNetworkScoreCache;Z)Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    :goto_1
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    move v1, v0

    goto :goto_0

    :cond_0
    if-eqz v1, :cond_1

    iget-object v0, p0, Lcom/android/settingslib/wifi/a;->cCj:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    iget-object v0, p0, Lcom/android/settingslib/wifi/a;->mMainHandler:Lcom/android/settingslib/wifi/WifiTracker$MainHandler;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/android/settingslib/wifi/WifiTracker$MainHandler;->sendEmptyMessage(I)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_1
    monitor-exit v3

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v3

    throw v0

    :cond_2
    move v0, v1

    goto :goto_1
.end method

.method static synthetic cgo(Lcom/android/settingslib/wifi/a;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/settingslib/wifi/a;->cgV()V

    return-void
.end method

.method private cgp()V
    .locals 3

    iget-object v0, p0, Lcom/android/settingslib/wifi/a;->cCs:Landroid/net/NetworkScoreManager;

    iget-object v1, p0, Lcom/android/settingslib/wifi/a;->cCu:Landroid/net/wifi/WifiNetworkScoreCache;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Landroid/net/NetworkScoreManager;->unregisterNetworkScoreCache(ILandroid/net/INetworkScoreCache;)V

    iget-object v0, p0, Lcom/android/settingslib/wifi/a;->cCu:Landroid/net/wifi/WifiNetworkScoreCache;

    invoke-virtual {v0}, Landroid/net/wifi/WifiNetworkScoreCache;->clearScores()V

    iget-object v1, p0, Lcom/android/settingslib/wifi/a;->mLock:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/android/settingslib/wifi/a;->cBY:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method static synthetic cgr(Lcom/android/settingslib/wifi/a;)Landroid/net/ConnectivityManager;
    .locals 1

    iget-object v0, p0, Lcom/android/settingslib/wifi/a;->cCp:Landroid/net/ConnectivityManager;

    return-object v0
.end method

.method static synthetic cgs(Lcom/android/settingslib/wifi/a;)Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lcom/android/settingslib/wifi/a;->mLock:Ljava/lang/Object;

    return-object v0
.end method

.method private cgt(I)V
    .locals 3

    iget-object v0, p0, Lcom/android/settingslib/wifi/a;->cCa:Lcom/android/settingslib/wifi/c;

    const/4 v1, 0x3

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Lcom/android/settingslib/wifi/c;->obtainMessage(III)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    return-void
.end method

.method static synthetic cgu(Lcom/android/settingslib/wifi/a;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/android/settingslib/wifi/a;->cCo:Z

    return p1
.end method

.method private cgv(Ljava/util/Collection;)V
    .locals 3

    invoke-interface {p1}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    :cond_0
    sget-boolean v0, Lcom/android/settingslib/wifi/a;->cCb:Z

    if-eqz v0, :cond_1

    const-string/jumbo v0, "WifiTracker"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Requesting scores for Network Keys: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    iget-object v1, p0, Lcom/android/settingslib/wifi/a;->cCs:Landroid/net/NetworkScoreManager;

    invoke-interface {p1}, Ljava/util/Collection;->size()I

    move-result v0

    new-array v0, v0, [Landroid/net/NetworkKey;

    invoke-interface {p1, v0}, Ljava/util/Collection;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Landroid/net/NetworkKey;

    invoke-virtual {v1, v0}, Landroid/net/NetworkScoreManager;->requestScores([Landroid/net/NetworkKey;)Z

    iget-object v1, p0, Lcom/android/settingslib/wifi/a;->mLock:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/android/settingslib/wifi/a;->cBY:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method static synthetic cgx(Lcom/android/settingslib/wifi/a;Landroid/net/NetworkInfo;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/settingslib/wifi/a;->cgF(Landroid/net/NetworkInfo;)V

    return-void
.end method

.method static synthetic cgy(Lcom/android/settingslib/wifi/a;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/settingslib/wifi/a;->cgQ(Z)V

    return-void
.end method


# virtual methods
.method public cgB()V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/android/settingslib/wifi/a;->mScanner:Lcom/android/settingslib/wifi/WifiTracker$Scanner;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settingslib/wifi/a;->mScanner:Lcom/android/settingslib/wifi/WifiTracker$Scanner;

    invoke-virtual {v0}, Lcom/android/settingslib/wifi/WifiTracker$Scanner;->pause()V

    iput-object v1, p0, Lcom/android/settingslib/wifi/a;->mScanner:Lcom/android/settingslib/wifi/WifiTracker$Scanner;

    :cond_0
    return-void
.end method

.method public cgD()Z
    .locals 1

    iget-object v0, p0, Lcom/android/settingslib/wifi/a;->cBW:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    return v0
.end method

.method public cgI()V
    .locals 2

    iget-object v0, p0, Lcom/android/settingslib/wifi/a;->mScanner:Lcom/android/settingslib/wifi/WifiTracker$Scanner;

    if-nez v0, :cond_0

    new-instance v0, Lcom/android/settingslib/wifi/WifiTracker$Scanner;

    invoke-direct {v0, p0}, Lcom/android/settingslib/wifi/WifiTracker$Scanner;-><init>(Lcom/android/settingslib/wifi/a;)V

    iput-object v0, p0, Lcom/android/settingslib/wifi/a;->mScanner:Lcom/android/settingslib/wifi/WifiTracker$Scanner;

    :cond_0
    iget-object v0, p0, Lcom/android/settingslib/wifi/a;->cCa:Lcom/android/settingslib/wifi/c;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/android/settingslib/wifi/c;->sendEmptyMessage(I)Z

    iget-object v0, p0, Lcom/android/settingslib/wifi/a;->cCg:Landroid/net/wifi/WifiManager;

    invoke-virtual {v0}, Landroid/net/wifi/WifiManager;->isWifiEnabled()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/settingslib/wifi/a;->mScanner:Lcom/android/settingslib/wifi/WifiTracker$Scanner;

    invoke-virtual {v0}, Lcom/android/settingslib/wifi/WifiTracker$Scanner;->che()V

    :cond_1
    return-void
.end method

.method public cgL()V
    .locals 4

    iget-object v1, p0, Lcom/android/settingslib/wifi/a;->mLock:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    invoke-direct {p0}, Lcom/android/settingslib/wifi/a;->cgK()V

    invoke-virtual {p0}, Lcom/android/settingslib/wifi/a;->cgI()V

    iget-boolean v0, p0, Lcom/android/settingslib/wifi/a;->cCc:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/settingslib/wifi/a;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/android/settingslib/wifi/a;->mReceiver:Landroid/content/BroadcastReceiver;

    iget-object v3, p0, Lcom/android/settingslib/wifi/a;->cBZ:Landroid/content/IntentFilter;

    invoke-virtual {v0, v2, v3}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    new-instance v0, Lcom/android/settingslib/wifi/b;

    const/4 v2, 0x0

    invoke-direct {v0, p0, v2}, Lcom/android/settingslib/wifi/b;-><init>(Lcom/android/settingslib/wifi/a;Lcom/android/settingslib/wifi/b;)V

    iput-object v0, p0, Lcom/android/settingslib/wifi/a;->cCk:Lcom/android/settingslib/wifi/b;

    iget-object v0, p0, Lcom/android/settingslib/wifi/a;->cCp:Landroid/net/ConnectivityManager;

    iget-object v2, p0, Lcom/android/settingslib/wifi/a;->cCf:Landroid/net/NetworkRequest;

    iget-object v3, p0, Lcom/android/settingslib/wifi/a;->cCk:Lcom/android/settingslib/wifi/b;

    invoke-virtual {v0, v2, v3}, Landroid/net/ConnectivityManager;->registerNetworkCallback(Landroid/net/NetworkRequest;Landroid/net/ConnectivityManager$NetworkCallback;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/settingslib/wifi/a;->cCc:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public cgM()Ljava/util/List;
    .locals 2

    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/android/settingslib/wifi/a;->cCm:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    return-object v0
.end method

.method public cgY()V
    .locals 1

    iget-object v0, p0, Lcom/android/settingslib/wifi/a;->cCg:Landroid/net/wifi/WifiManager;

    invoke-virtual {v0}, Landroid/net/wifi/WifiManager;->isWifiEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settingslib/wifi/a;->mScanner:Lcom/android/settingslib/wifi/WifiTracker$Scanner;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settingslib/wifi/a;->mScanner:Lcom/android/settingslib/wifi/WifiTracker$Scanner;

    invoke-virtual {v0}, Lcom/android/settingslib/wifi/WifiTracker$Scanner;->chf()V

    :cond_0
    return-void
.end method

.method public cgn()V
    .locals 3

    iget-object v1, p0, Lcom/android/settingslib/wifi/a;->mLock:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-boolean v0, p0, Lcom/android/settingslib/wifi/a;->cCc:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settingslib/wifi/a;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/android/settingslib/wifi/a;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v2}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    iget-object v0, p0, Lcom/android/settingslib/wifi/a;->cCp:Landroid/net/ConnectivityManager;

    iget-object v2, p0, Lcom/android/settingslib/wifi/a;->cCk:Lcom/android/settingslib/wifi/b;

    invoke-virtual {v0, v2}, Landroid/net/ConnectivityManager;->unregisterNetworkCallback(Landroid/net/ConnectivityManager$NetworkCallback;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/settingslib/wifi/a;->cCc:Z

    :cond_0
    invoke-direct {p0}, Lcom/android/settingslib/wifi/a;->cgp()V

    invoke-virtual {p0}, Lcom/android/settingslib/wifi/a;->cgB()V

    iget-object v0, p0, Lcom/android/settingslib/wifi/a;->cCa:Lcom/android/settingslib/wifi/c;

    invoke-static {v0}, Lcom/android/settingslib/wifi/c;->chd(Lcom/android/settingslib/wifi/c;)V

    iget-object v0, p0, Lcom/android/settingslib/wifi/a;->mMainHandler:Lcom/android/settingslib/wifi/WifiTracker$MainHandler;

    invoke-virtual {v0}, Lcom/android/settingslib/wifi/WifiTracker$MainHandler;->cha()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/settingslib/wifi/a;->cCo:Z

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public cgq()Landroid/net/wifi/WifiManager;
    .locals 1

    iget-object v0, p0, Lcom/android/settingslib/wifi/a;->cCg:Landroid/net/wifi/WifiManager;

    return-object v0
.end method

.method public cgw()Z
    .locals 1

    iget-object v0, p0, Lcom/android/settingslib/wifi/a;->cCg:Landroid/net/wifi/WifiManager;

    invoke-virtual {v0}, Landroid/net/wifi/WifiManager;->isWifiEnabled()Z

    move-result v0

    return v0
.end method

.method public cgz()V
    .locals 4

    iget-object v1, p0, Lcom/android/settingslib/wifi/a;->mLock:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/android/settingslib/wifi/a;->cCa:Lcom/android/settingslib/wifi/c;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lcom/android/settingslib/wifi/c;->removeMessages(I)V

    iget-object v0, p0, Lcom/android/settingslib/wifi/a;->cCg:Landroid/net/wifi/WifiManager;

    invoke-virtual {v0}, Landroid/net/wifi/WifiManager;->getConnectionInfo()Landroid/net/wifi/WifiInfo;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settingslib/wifi/a;->cBX:Landroid/net/wifi/WifiInfo;

    iget-object v0, p0, Lcom/android/settingslib/wifi/a;->cCp:Landroid/net/ConnectivityManager;

    iget-object v2, p0, Lcom/android/settingslib/wifi/a;->cCg:Landroid/net/wifi/WifiManager;

    invoke-virtual {v2}, Landroid/net/wifi/WifiManager;->getCurrentNetwork()Landroid/net/Network;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/net/ConnectivityManager;->getNetworkInfo(Landroid/net/Network;)Landroid/net/NetworkInfo;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settingslib/wifi/a;->cCt:Landroid/net/NetworkInfo;

    invoke-direct {p0}, Lcom/android/settingslib/wifi/a;->cgV()V

    sget-boolean v0, Lcom/android/settingslib/wifi/a;->cCb:Z

    if-eqz v0, :cond_0

    const-string/jumbo v0, "WifiTracker"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "force update - internal access point list:\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/settingslib/wifi/a;->cCj:Ljava/util/List;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v0, p0, Lcom/android/settingslib/wifi/a;->mMainHandler:Lcom/android/settingslib/wifi/WifiTracker$MainHandler;

    const/4 v2, 0x2

    invoke-virtual {v0, v2}, Lcom/android/settingslib/wifi/WifiTracker$MainHandler;->removeMessages(I)V

    iget-object v0, p0, Lcom/android/settingslib/wifi/a;->mMainHandler:Lcom/android/settingslib/wifi/WifiTracker$MainHandler;

    iget-object v2, p0, Lcom/android/settingslib/wifi/a;->mMainHandler:Lcom/android/settingslib/wifi/WifiTracker$MainHandler;

    const/4 v3, 0x2

    invoke-static {v2, v3}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/android/settingslib/wifi/WifiTracker$MainHandler;->handleMessage(Landroid/os/Message;)V

    sget-boolean v0, Lcom/android/settingslib/wifi/a;->cCb:Z

    if-eqz v0, :cond_1

    const-string/jumbo v0, "WifiTracker"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "force update - external access point list:\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/settingslib/wifi/a;->cCm:Ljava/util/List;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_1
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public getCachedOrCreate(Landroid/net/wifi/ScanResult;Ljava/util/List;)Lcom/android/settingslib/wifi/i;
    .locals 3

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_1

    invoke-interface {p2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settingslib/wifi/i;

    invoke-virtual {v0, p1}, Lcom/android/settingslib/wifi/i;->chy(Landroid/net/wifi/ScanResult;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p2, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settingslib/wifi/i;

    invoke-virtual {v0, p1}, Lcom/android/settingslib/wifi/i;->chJ(Landroid/net/wifi/ScanResult;)Z

    return-object v0

    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    new-instance v0, Lcom/android/settingslib/wifi/i;

    iget-object v1, p0, Lcom/android/settingslib/wifi/a;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1, p1}, Lcom/android/settingslib/wifi/i;-><init>(Landroid/content/Context;Landroid/net/wifi/ScanResult;)V

    iget-object v1, p0, Lcom/android/settingslib/wifi/a;->cCl:Lcom/android/settingslib/wifi/f;

    invoke-virtual {v0, v1}, Lcom/android/settingslib/wifi/i;->chw(Lcom/android/settingslib/wifi/j;)V

    return-object v0
.end method

.method public getCachedOrCreate(Landroid/net/wifi/WifiConfiguration;Ljava/util/List;)Lcom/android/settingslib/wifi/i;
    .locals 3

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_1

    invoke-interface {p2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settingslib/wifi/i;

    invoke-virtual {v0, p1}, Lcom/android/settingslib/wifi/i;->chD(Landroid/net/wifi/WifiConfiguration;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p2, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settingslib/wifi/i;

    invoke-virtual {v0, p1}, Lcom/android/settingslib/wifi/i;->chI(Landroid/net/wifi/WifiConfiguration;)V

    return-object v0

    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    new-instance v0, Lcom/android/settingslib/wifi/i;

    iget-object v1, p0, Lcom/android/settingslib/wifi/a;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1, p1}, Lcom/android/settingslib/wifi/i;-><init>(Landroid/content/Context;Landroid/net/wifi/WifiConfiguration;)V

    iget-object v1, p0, Lcom/android/settingslib/wifi/a;->cCl:Lcom/android/settingslib/wifi/f;

    invoke-virtual {v0, v1}, Lcom/android/settingslib/wifi/i;->chw(Lcom/android/settingslib/wifi/j;)V

    return-object v0
.end method
