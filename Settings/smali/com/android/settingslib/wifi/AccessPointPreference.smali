.class public Lcom/android/settingslib/wifi/AccessPointPreference;
.super Lmiui/preference/RadioButtonPreference;
.source "AccessPointPreference.java"


# static fields
.field private static final cDj:[I

.field public static final cDm:[I

.field private static final cDn:[I

.field private static final cDp:[I


# instance fields
.field private cDk:Z

.field private final cDl:Lcom/android/settingslib/wifi/o;

.field private cDo:Landroid/widget/TextView;

.field private final cDq:Landroid/graphics/drawable/StateListDrawable;

.field private final cDr:I

.field private cDs:I

.field private cDt:Ljava/lang/CharSequence;

.field private cDu:I

.field private final cDv:Ljava/lang/Runnable;

.field protected cDw:Lcom/android/settingslib/wifi/i;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-array v0, v3, [I

    sget v1, Lcom/android/settingslib/b;->cKm:I

    aput v1, v0, v2

    sput-object v0, Lcom/android/settingslib/wifi/AccessPointPreference;->cDp:[I

    new-array v0, v3, [I

    sget v1, Lcom/android/settingslib/b;->cKn:I

    aput v1, v0, v2

    sput-object v0, Lcom/android/settingslib/wifi/AccessPointPreference;->cDn:[I

    new-array v0, v3, [I

    sget v1, Lcom/android/settingslib/b;->cKo:I

    aput v1, v0, v2

    sput-object v0, Lcom/android/settingslib/wifi/AccessPointPreference;->cDj:[I

    const/4 v0, 0x4

    new-array v0, v0, [I

    sget v1, Lcom/android/settingslib/i;->cLq:I

    aput v1, v0, v2

    sget v1, Lcom/android/settingslib/i;->cLt:I

    aput v1, v0, v3

    sget v1, Lcom/android/settingslib/i;->cLs:I

    const/4 v2, 0x2

    aput v1, v0, v2

    sget v1, Lcom/android/settingslib/i;->cLr:I

    const/4 v2, 0x3

    aput v1, v0, v2

    sput-object v0, Lcom/android/settingslib/wifi/AccessPointPreference;->cDm:[I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3

    const/4 v2, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, p2}, Lmiui/preference/RadioButtonPreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    iput-boolean v1, p0, Lcom/android/settingslib/wifi/AccessPointPreference;->cDk:Z

    iput v1, p0, Lcom/android/settingslib/wifi/AccessPointPreference;->cDu:I

    new-instance v0, Lcom/android/settingslib/wifi/n;

    invoke-direct {v0, p0}, Lcom/android/settingslib/wifi/n;-><init>(Lcom/android/settingslib/wifi/AccessPointPreference;)V

    iput-object v0, p0, Lcom/android/settingslib/wifi/AccessPointPreference;->cDv:Ljava/lang/Runnable;

    iput-object v2, p0, Lcom/android/settingslib/wifi/AccessPointPreference;->cDq:Landroid/graphics/drawable/StateListDrawable;

    iput v1, p0, Lcom/android/settingslib/wifi/AccessPointPreference;->cDr:I

    iput-object v2, p0, Lcom/android/settingslib/wifi/AccessPointPreference;->cDl:Lcom/android/settingslib/wifi/o;

    return-void
.end method

.method private cit(Landroid/widget/ImageView;)V
    .locals 2

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/android/settingslib/wifi/AccessPointPreference;->cDq:Landroid/graphics/drawable/StateListDrawable;

    if-nez v0, :cond_1

    :cond_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/settingslib/wifi/AccessPointPreference;->cDw:Lcom/android/settingslib/wifi/i;

    invoke-virtual {v0}, Lcom/android/settingslib/wifi/i;->chP()I

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/android/settingslib/wifi/AccessPointPreference;->cDq:Landroid/graphics/drawable/StateListDrawable;

    sget-object v1, Lcom/android/settingslib/wifi/AccessPointPreference;->cDp:[I

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/StateListDrawable;->setState([I)Z

    :cond_2
    :goto_0
    iget-object v0, p0, Lcom/android/settingslib/wifi/AccessPointPreference;->cDq:Landroid/graphics/drawable/StateListDrawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/StateListDrawable;->getCurrent()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    return-void

    :cond_3
    iget-object v0, p0, Lcom/android/settingslib/wifi/AccessPointPreference;->cDw:Lcom/android/settingslib/wifi/i;

    invoke-virtual {v0}, Lcom/android/settingslib/wifi/i;->chi()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/settingslib/wifi/AccessPointPreference;->cDq:Landroid/graphics/drawable/StateListDrawable;

    sget-object v1, Lcom/android/settingslib/wifi/AccessPointPreference;->cDn:[I

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/StateListDrawable;->setState([I)Z

    goto :goto_0
.end method

.method private ciu()V
    .locals 2

    iget-object v0, p0, Lcom/android/settingslib/wifi/AccessPointPreference;->cDo:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settingslib/wifi/AccessPointPreference;->cDo:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/android/settingslib/wifi/AccessPointPreference;->cDv:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->post(Ljava/lang/Runnable;)Z

    :cond_0
    return-void
.end method


# virtual methods
.method protected notifyChanged()V
    .locals 2

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v1

    if-eq v0, v1, :cond_0

    invoke-direct {p0}, Lcom/android/settingslib/wifi/AccessPointPreference;->ciu()V

    :goto_0
    return-void

    :cond_0
    invoke-super {p0}, Lmiui/preference/RadioButtonPreference;->notifyChanged()V

    goto :goto_0
.end method

.method public onBindView(Landroid/view/View;)V
    .locals 2

    invoke-super {p0, p1}, Lmiui/preference/RadioButtonPreference;->onBindView(Landroid/view/View;)V

    iget-object v0, p0, Lcom/android/settingslib/wifi/AccessPointPreference;->cDw:Lcom/android/settingslib/wifi/i;

    if-nez v0, :cond_0

    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/android/settingslib/wifi/AccessPointPreference;->getIcon()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-eqz v0, :cond_1

    iget v1, p0, Lcom/android/settingslib/wifi/AccessPointPreference;->cDs:I

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setLevel(I)Z

    :cond_1
    const v0, 0x1020016

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/settingslib/wifi/AccessPointPreference;->cDo:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/android/settingslib/wifi/AccessPointPreference;->cDt:Ljava/lang/CharSequence;

    invoke-virtual {p1, v0}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    sget v0, Lcom/android/settingslib/g;->cKV:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    invoke-direct {p0, v0}, Lcom/android/settingslib/wifi/AccessPointPreference;->cit(Landroid/widget/ImageView;)V

    return-void
.end method
