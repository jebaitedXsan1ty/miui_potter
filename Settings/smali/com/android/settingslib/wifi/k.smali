.class public Lcom/android/settingslib/wifi/k;
.super Ljava/lang/Object;
.source "WifiStatusTracker.java"


# instance fields
.field public cCZ:I

.field public cDa:Ljava/lang/String;

.field public cDb:I

.field public cDc:Landroid/net/NetworkKey;

.field public cDd:I

.field public cDe:Z

.field private final cDf:Landroid/net/wifi/WifiManager;

.field public cDg:Z

.field public enabled:Z


# direct methods
.method public constructor <init>(Landroid/net/wifi/WifiManager;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/android/settingslib/wifi/k;->cDf:Landroid/net/wifi/WifiManager;

    return-void
.end method

.method private cip(Landroid/net/wifi/WifiInfo;)Ljava/lang/String;
    .locals 6

    const/4 v5, 0x0

    invoke-virtual {p1}, Landroid/net/wifi/WifiInfo;->getSSID()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/android/settingslib/wifi/k;->cDf:Landroid/net/wifi/WifiManager;

    invoke-virtual {v0}, Landroid/net/wifi/WifiManager;->getConfiguredNetworks()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_2

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/WifiConfiguration;

    iget v0, v0, Landroid/net/wifi/WifiConfiguration;->networkId:I

    invoke-virtual {p1}, Landroid/net/wifi/WifiInfo;->getNetworkId()I

    move-result v4

    if-ne v0, v4, :cond_1

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/WifiConfiguration;

    iget-object v0, v0, Landroid/net/wifi/WifiConfiguration;->SSID:Ljava/lang/String;

    return-object v0

    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_2
    return-object v5
.end method


# virtual methods
.method public ciq(Landroid/content/Intent;)V
    .locals 7

    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v4, 0x0

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v3, "android.net.wifi.WIFI_STATE_CHANGED"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    const-string/jumbo v0, "wifi_state"

    invoke-virtual {p1, v0, v6}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/android/settingslib/wifi/k;->cDb:I

    iget v0, p0, Lcom/android/settingslib/wifi/k;->cDb:I

    if-ne v0, v5, :cond_1

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/android/settingslib/wifi/k;->enabled:Z

    const-string/jumbo v0, "wifi_state"

    invoke-virtual {p1, v0, v6}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    if-ne v0, v5, :cond_2

    :goto_1
    iput-boolean v1, p0, Lcom/android/settingslib/wifi/k;->enabled:Z

    :cond_0
    :goto_2
    return-void

    :cond_1
    move v0, v2

    goto :goto_0

    :cond_2
    move v1, v2

    goto :goto_1

    :cond_3
    const-string/jumbo v1, "android.net.wifi.STATE_CHANGE"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_a

    const-string/jumbo v0, "networkInfo"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/NetworkInfo;

    if-eqz v0, :cond_6

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v1

    xor-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_6

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->isConnectedOrConnecting()Z

    move-result v1

    :goto_3
    iput-boolean v1, p0, Lcom/android/settingslib/wifi/k;->cDe:Z

    if-eqz v0, :cond_4

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v2

    :cond_4
    iput-boolean v2, p0, Lcom/android/settingslib/wifi/k;->cDg:Z

    const-string/jumbo v0, "wifiInfo"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    if-eqz v0, :cond_7

    const-string/jumbo v0, "wifiInfo"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/WifiInfo;

    :goto_4
    iget-boolean v1, p0, Lcom/android/settingslib/wifi/k;->cDg:Z

    if-eqz v1, :cond_9

    if-eqz v0, :cond_9

    invoke-direct {p0, v0}, Lcom/android/settingslib/wifi/k;->cip(Landroid/net/wifi/WifiInfo;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/android/settingslib/wifi/k;->cDa:Ljava/lang/String;

    invoke-virtual {v0}, Landroid/net/wifi/WifiInfo;->getBSSID()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settingslib/wifi/k;->cDa:Ljava/lang/String;

    if-eqz v1, :cond_8

    if-eqz v0, :cond_8

    iget-object v1, p0, Lcom/android/settingslib/wifi/k;->cDc:Landroid/net/NetworkKey;

    if-eqz v1, :cond_5

    iget-object v1, p0, Lcom/android/settingslib/wifi/k;->cDc:Landroid/net/NetworkKey;

    iget-object v1, v1, Landroid/net/NetworkKey;->wifiKey:Landroid/net/WifiKey;

    iget-object v1, v1, Landroid/net/WifiKey;->ssid:Ljava/lang/String;

    iget-object v2, p0, Lcom/android/settingslib/wifi/k;->cDa:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    xor-int/lit8 v1, v1, 0x1

    if-nez v1, :cond_5

    iget-object v1, p0, Lcom/android/settingslib/wifi/k;->cDc:Landroid/net/NetworkKey;

    iget-object v1, v1, Landroid/net/NetworkKey;->wifiKey:Landroid/net/WifiKey;

    iget-object v1, v1, Landroid/net/WifiKey;->bssid:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    xor-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    :cond_5
    :try_start_0
    new-instance v1, Landroid/net/NetworkKey;

    new-instance v2, Landroid/net/WifiKey;

    iget-object v3, p0, Lcom/android/settingslib/wifi/k;->cDa:Ljava/lang/String;

    invoke-direct {v2, v3, v0}, Landroid/net/WifiKey;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {v1, v2}, Landroid/net/NetworkKey;-><init>(Landroid/net/WifiKey;)V

    iput-object v1, p0, Lcom/android/settingslib/wifi/k;->cDc:Landroid/net/NetworkKey;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_2

    :catch_0
    move-exception v0

    const-string/jumbo v1, "WifiStatusTracker"

    const-string/jumbo v2, "Cannot create NetworkKey"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto/16 :goto_2

    :cond_6
    move v1, v2

    goto :goto_3

    :cond_7
    iget-object v0, p0, Lcom/android/settingslib/wifi/k;->cDf:Landroid/net/wifi/WifiManager;

    invoke-virtual {v0}, Landroid/net/wifi/WifiManager;->getConnectionInfo()Landroid/net/wifi/WifiInfo;

    move-result-object v0

    goto :goto_4

    :cond_8
    iput-object v4, p0, Lcom/android/settingslib/wifi/k;->cDc:Landroid/net/NetworkKey;

    goto/16 :goto_2

    :cond_9
    iput-object v4, p0, Lcom/android/settingslib/wifi/k;->cDa:Ljava/lang/String;

    iput-object v4, p0, Lcom/android/settingslib/wifi/k;->cDc:Landroid/net/NetworkKey;

    goto/16 :goto_2

    :cond_a
    const-string/jumbo v1, "android.net.wifi.RSSI_CHANGED"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string/jumbo v0, "newRssi"

    const/16 v1, -0xc8

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/android/settingslib/wifi/k;->cCZ:I

    iget v0, p0, Lcom/android/settingslib/wifi/k;->cCZ:I

    const/4 v1, 0x5

    invoke-static {v0, v1}, Landroid/net/wifi/WifiManager;->calculateSignalLevel(II)I

    move-result v0

    iput v0, p0, Lcom/android/settingslib/wifi/k;->cDd:I

    goto/16 :goto_2
.end method
