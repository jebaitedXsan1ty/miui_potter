.class final Lcom/android/settingslib/wifi/WifiTracker$MainHandler;
.super Landroid/os/Handler;
.source "WifiTracker.java"


# static fields
.field static final MSG_ACCESS_POINT_CHANGED:I = 0x2

.field static final MSG_CONNECTED_CHANGED:I = 0x0

.field static final MSG_WIFI_STATE_CHANGED:I = 0x1


# instance fields
.field final synthetic cCw:Lcom/android/settingslib/wifi/a;


# direct methods
.method public constructor <init>(Lcom/android/settingslib/wifi/a;Landroid/os/Looper;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settingslib/wifi/WifiTracker$MainHandler;->cCw:Lcom/android/settingslib/wifi/a;

    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    return-void
.end method


# virtual methods
.method cha()V
    .locals 1

    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/android/settingslib/wifi/WifiTracker$MainHandler;->removeMessages(I)V

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/android/settingslib/wifi/WifiTracker$MainHandler;->removeMessages(I)V

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/android/settingslib/wifi/WifiTracker$MainHandler;->removeMessages(I)V

    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Lcom/android/settingslib/wifi/WifiTracker$MainHandler;->removeMessages(I)V

    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lcom/android/settingslib/wifi/WifiTracker$MainHandler;->removeMessages(I)V

    return-void
.end method

.method public handleMessage(Landroid/os/Message;)V
    .locals 2

    iget-object v0, p0, Lcom/android/settingslib/wifi/WifiTracker$MainHandler;->cCw:Lcom/android/settingslib/wifi/a;

    invoke-static {v0}, Lcom/android/settingslib/wifi/a;->cgW(Lcom/android/settingslib/wifi/a;)Lcom/android/settingslib/wifi/e;

    move-result-object v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    :cond_1
    :goto_0
    return-void

    :pswitch_0
    iget-object v0, p0, Lcom/android/settingslib/wifi/WifiTracker$MainHandler;->cCw:Lcom/android/settingslib/wifi/a;

    invoke-static {v0}, Lcom/android/settingslib/wifi/a;->cgW(Lcom/android/settingslib/wifi/a;)Lcom/android/settingslib/wifi/e;

    move-result-object v0

    invoke-interface {v0}, Lcom/android/settingslib/wifi/e;->aaZ()V

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Lcom/android/settingslib/wifi/WifiTracker$MainHandler;->cCw:Lcom/android/settingslib/wifi/a;

    invoke-static {v0}, Lcom/android/settingslib/wifi/a;->cgW(Lcom/android/settingslib/wifi/a;)Lcom/android/settingslib/wifi/e;

    move-result-object v0

    iget v1, p1, Landroid/os/Message;->arg1:I

    invoke-interface {v0, v1}, Lcom/android/settingslib/wifi/e;->abb(I)V

    goto :goto_0

    :pswitch_2
    iget-object v0, p0, Lcom/android/settingslib/wifi/WifiTracker$MainHandler;->cCw:Lcom/android/settingslib/wifi/a;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/android/settingslib/wifi/a;->cgy(Lcom/android/settingslib/wifi/a;Z)V

    iget-object v0, p0, Lcom/android/settingslib/wifi/WifiTracker$MainHandler;->cCw:Lcom/android/settingslib/wifi/a;

    invoke-static {v0}, Lcom/android/settingslib/wifi/a;->cgW(Lcom/android/settingslib/wifi/a;)Lcom/android/settingslib/wifi/e;

    move-result-object v0

    invoke-interface {v0}, Lcom/android/settingslib/wifi/e;->aaY()V

    goto :goto_0

    :pswitch_3
    iget-object v0, p0, Lcom/android/settingslib/wifi/WifiTracker$MainHandler;->cCw:Lcom/android/settingslib/wifi/a;

    iget-object v0, v0, Lcom/android/settingslib/wifi/a;->mScanner:Lcom/android/settingslib/wifi/WifiTracker$Scanner;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/settingslib/wifi/WifiTracker$MainHandler;->cCw:Lcom/android/settingslib/wifi/a;

    iget-object v0, v0, Lcom/android/settingslib/wifi/a;->mScanner:Lcom/android/settingslib/wifi/WifiTracker$Scanner;

    invoke-virtual {v0}, Lcom/android/settingslib/wifi/WifiTracker$Scanner;->che()V

    goto :goto_0

    :pswitch_4
    iget-object v0, p0, Lcom/android/settingslib/wifi/WifiTracker$MainHandler;->cCw:Lcom/android/settingslib/wifi/a;

    iget-object v0, v0, Lcom/android/settingslib/wifi/a;->mScanner:Lcom/android/settingslib/wifi/WifiTracker$Scanner;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/settingslib/wifi/WifiTracker$MainHandler;->cCw:Lcom/android/settingslib/wifi/a;

    iget-object v0, v0, Lcom/android/settingslib/wifi/a;->mScanner:Lcom/android/settingslib/wifi/WifiTracker$Scanner;

    invoke-virtual {v0}, Lcom/android/settingslib/wifi/WifiTracker$Scanner;->pause()V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method
