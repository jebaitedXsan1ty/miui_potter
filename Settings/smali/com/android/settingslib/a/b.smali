.class public abstract Lcom/android/settingslib/a/b;
.super Ljava/lang/Object;
.source "MiuiAbstractPreferenceController.java"


# instance fields
.field protected final mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/android/settingslib/a/b;->mContext:Landroid/content/Context;

    return-void
.end method

.method private cdJ(Landroid/preference/PreferenceGroup;Ljava/lang/String;)Z
    .locals 5

    const/4 v2, 0x0

    invoke-virtual {p1}, Landroid/preference/PreferenceGroup;->getPreferenceCount()I

    move-result v3

    move v1, v2

    :goto_0
    if-ge v1, v3, :cond_2

    invoke-virtual {p1, v1}, Landroid/preference/PreferenceGroup;->getPreference(I)Landroid/preference/Preference;

    move-result-object v0

    invoke-virtual {v0}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_0

    invoke-virtual {v4, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-virtual {p1, v0}, Landroid/preference/PreferenceGroup;->removePreference(Landroid/preference/Preference;)Z

    move-result v0

    return v0

    :cond_0
    instance-of v4, v0, Landroid/preference/PreferenceGroup;

    if-eqz v4, :cond_1

    check-cast v0, Landroid/preference/PreferenceGroup;

    invoke-direct {p0, v0, p2}, Lcom/android/settingslib/a/b;->cdJ(Landroid/preference/PreferenceGroup;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    return v0

    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_2
    return v2
.end method


# virtual methods
.method protected final cdK(Landroid/preference/PreferenceScreen;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/android/settingslib/a/b;->cdJ(Landroid/preference/PreferenceGroup;Ljava/lang/String;)Z

    return-void
.end method

.method public cdL(Ljava/util/List;)V
    .locals 1

    invoke-virtual {p0}, Lcom/android/settingslib/a/b;->p()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/android/settingslib/a/b;->l()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    return-void
.end method

.method public cz(Landroid/preference/Preference;)V
    .locals 0

    return-void
.end method

.method public fm(Landroid/preference/Preference;)Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public i(Landroid/preference/PreferenceScreen;)V
    .locals 1

    invoke-virtual {p0}, Lcom/android/settingslib/a/b;->p()Z

    move-result v0

    if-eqz v0, :cond_1

    instance-of v0, p0, Landroid/preference/Preference$OnPreferenceChangeListener;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/android/settingslib/a/b;->l()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast p0, Landroid/preference/Preference$OnPreferenceChangeListener;

    invoke-virtual {v0, p0}, Landroid/preference/Preference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0}, Lcom/android/settingslib/a/b;->l()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lcom/android/settingslib/a/b;->cdK(Landroid/preference/PreferenceScreen;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public abstract l()Ljava/lang/String;
.end method

.method public abstract p()Z
.end method
