.class public Lcom/android/settingslib/p;
.super Ljava/lang/Object;
.source "BatteryInfo.java"


# instance fields
.field public cPY:Ljava/lang/String;

.field public cPZ:Ljava/lang/String;

.field private cQa:J

.field public cQb:Ljava/lang/String;

.field private cQc:Z

.field public cQd:J

.field public cQe:I

.field public cQf:Z

.field private cQg:Landroid/os/BatteryStats;

.field public cQh:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/settingslib/p;->cQf:Z

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/android/settingslib/p;->cQd:J

    return-void
.end method

.method public static cqk(Landroid/content/Context;Lcom/android/settingslib/q;Z)V
    .locals 2

    new-instance v0, Lcom/android/settingslib/t;

    invoke-direct {v0, p0, p2, p1}, Lcom/android/settingslib/t;-><init>(Landroid/content/Context;ZLcom/android/settingslib/q;)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/android/settingslib/t;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    return-void
.end method

.method private static varargs cql(Landroid/os/BatteryStats;J[Lcom/android/settingslib/r;)V
    .locals 25

    const-wide/16 v14, 0x0

    const-wide/16 v12, 0x0

    const-wide/16 v10, 0x0

    const/4 v5, -0x1

    const-wide/16 v16, 0x0

    const-wide/16 v8, 0x0

    const-wide/16 v6, 0x0

    const/4 v4, 0x0

    const/4 v3, 0x0

    const/4 v2, 0x1

    invoke-virtual/range {p0 .. p0}, Landroid/os/BatteryStats;->startIteratingHistoryLocked()Z

    move-result v18

    if-eqz v18, :cond_8

    new-instance v18, Landroid/os/BatteryStats$HistoryItem;

    invoke-direct/range {v18 .. v18}, Landroid/os/BatteryStats$HistoryItem;-><init>()V

    :cond_0
    :goto_0
    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Landroid/os/BatteryStats;->getNextHistoryLocked(Landroid/os/BatteryStats$HistoryItem;)Z

    move-result v19

    if-eqz v19, :cond_8

    add-int/lit8 v3, v3, 0x1

    if-eqz v2, :cond_1

    const/4 v2, 0x0

    move-object/from16 v0, v18

    iget-wide v12, v0, Landroid/os/BatteryStats$HistoryItem;->time:J

    :cond_1
    move-object/from16 v0, v18

    iget-byte v0, v0, Landroid/os/BatteryStats$HistoryItem;->cmd:B

    move/from16 v19, v0

    const/16 v20, 0x5

    move/from16 v0, v19

    move/from16 v1, v20

    if-eq v0, v1, :cond_2

    move-object/from16 v0, v18

    iget-byte v0, v0, Landroid/os/BatteryStats$HistoryItem;->cmd:B

    move/from16 v19, v0

    const/16 v20, 0x7

    move/from16 v0, v19

    move/from16 v1, v20

    if-ne v0, v1, :cond_5

    :cond_2
    move-object/from16 v0, v18

    iget-wide v6, v0, Landroid/os/BatteryStats$HistoryItem;->currentTime:J

    const-wide v20, 0x39ef8b000L

    add-long v8, v8, v20

    cmp-long v6, v6, v8

    if-gtz v6, :cond_3

    move-object/from16 v0, v18

    iget-wide v6, v0, Landroid/os/BatteryStats$HistoryItem;->time:J

    const-wide/32 v8, 0x493e0

    add-long/2addr v8, v12

    cmp-long v6, v6, v8

    if-gez v6, :cond_4

    :cond_3
    const-wide/16 v14, 0x0

    :cond_4
    move-object/from16 v0, v18

    iget-wide v8, v0, Landroid/os/BatteryStats$HistoryItem;->currentTime:J

    move-object/from16 v0, v18

    iget-wide v6, v0, Landroid/os/BatteryStats$HistoryItem;->time:J

    const-wide/16 v20, 0x0

    cmp-long v19, v14, v20

    if-nez v19, :cond_5

    sub-long v14, v6, v12

    sub-long v14, v8, v14

    :cond_5
    invoke-virtual/range {v18 .. v18}, Landroid/os/BatteryStats$HistoryItem;->isDeltaData()Z

    move-result v19

    if-eqz v19, :cond_0

    move-object/from16 v0, v18

    iget-byte v4, v0, Landroid/os/BatteryStats$HistoryItem;->batteryLevel:B

    if-ne v4, v5, :cond_6

    const/4 v4, 0x1

    if-ne v3, v4, :cond_7

    :cond_6
    move-object/from16 v0, v18

    iget-byte v5, v0, Landroid/os/BatteryStats$HistoryItem;->batteryLevel:B

    :cond_7
    move-object/from16 v0, v18

    iget-wide v10, v0, Landroid/os/BatteryStats$HistoryItem;->time:J

    move v4, v3

    goto :goto_0

    :cond_8
    invoke-virtual/range {p0 .. p0}, Landroid/os/BatteryStats;->finishIteratingHistoryLocked()V

    add-long v2, v8, v10

    sub-long v8, v2, v6

    const-wide/16 v2, 0x3e8

    div-long v2, p1, v2

    add-long v10, v8, v2

    const/4 v3, 0x0

    const/4 v2, 0x0

    :goto_1
    move-object/from16 v0, p3

    array-length v5, v0

    if-ge v2, v5, :cond_9

    aget-object v5, p3, v2

    invoke-interface {v5, v14, v15, v10, v11}, Lcom/android/settingslib/r;->ID(JJ)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_9
    cmp-long v2, v8, v14

    if-lez v2, :cond_11

    invoke-virtual/range {p0 .. p0}, Landroid/os/BatteryStats;->startIteratingHistoryLocked()Z

    move-result v2

    if-eqz v2, :cond_11

    new-instance v5, Landroid/os/BatteryStats$HistoryItem;

    invoke-direct {v5}, Landroid/os/BatteryStats$HistoryItem;-><init>()V

    move v2, v3

    move-wide/from16 v8, v16

    :goto_2
    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Landroid/os/BatteryStats;->getNextHistoryLocked(Landroid/os/BatteryStats$HistoryItem;)Z

    move-result v3

    if-eqz v3, :cond_11

    if-ge v2, v4, :cond_11

    invoke-virtual {v5}, Landroid/os/BatteryStats$HistoryItem;->isDeltaData()Z

    move-result v3

    if-eqz v3, :cond_b

    iget-wide v10, v5, Landroid/os/BatteryStats$HistoryItem;->time:J

    sub-long v6, v10, v6

    add-long v10, v8, v6

    iget-wide v8, v5, Landroid/os/BatteryStats$HistoryItem;->time:J

    sub-long v6, v10, v14

    const-wide/16 v16, 0x0

    cmp-long v3, v6, v16

    if-gez v3, :cond_a

    const-wide/16 v6, 0x0

    :cond_a
    const/4 v3, 0x0

    :goto_3
    move-object/from16 v0, p3

    array-length v0, v0

    move/from16 v16, v0

    move/from16 v0, v16

    if-ge v3, v0, :cond_14

    aget-object v16, p3, v3

    move-object/from16 v0, v16

    invoke-interface {v0, v6, v7, v5}, Lcom/android/settingslib/r;->IB(JLandroid/os/BatteryStats$HistoryItem;)V

    add-int/lit8 v3, v3, 0x1

    goto :goto_3

    :cond_b
    iget-byte v3, v5, Landroid/os/BatteryStats$HistoryItem;->cmd:B

    const/4 v10, 0x5

    if-eq v3, v10, :cond_c

    iget-byte v3, v5, Landroid/os/BatteryStats$HistoryItem;->cmd:B

    const/4 v10, 0x7

    if-ne v3, v10, :cond_e

    :cond_c
    iget-wide v6, v5, Landroid/os/BatteryStats$HistoryItem;->currentTime:J

    cmp-long v3, v6, v14

    if-ltz v3, :cond_f

    iget-wide v6, v5, Landroid/os/BatteryStats$HistoryItem;->currentTime:J

    :goto_4
    iget-wide v10, v5, Landroid/os/BatteryStats$HistoryItem;->time:J

    move-wide/from16 v22, v10

    move-wide v10, v6

    move-wide/from16 v6, v22

    :goto_5
    iget-byte v3, v5, Landroid/os/BatteryStats$HistoryItem;->cmd:B

    const/16 v16, 0x6

    move/from16 v0, v16

    if-eq v3, v0, :cond_10

    iget-byte v3, v5, Landroid/os/BatteryStats$HistoryItem;->cmd:B

    const/16 v16, 0x5

    move/from16 v0, v16

    if-ne v3, v0, :cond_d

    sub-long/2addr v8, v10

    invoke-static {v8, v9}, Ljava/lang/Math;->abs(J)J

    move-result-wide v8

    const-wide/32 v16, 0x36ee80

    cmp-long v3, v8, v16

    if-lez v3, :cond_10

    :cond_d
    const/4 v3, 0x0

    :goto_6
    move-object/from16 v0, p3

    array-length v8, v0

    if-ge v3, v8, :cond_13

    aget-object v8, p3, v3

    invoke-interface {v8}, Lcom/android/settingslib/r;->IA()V

    add-int/lit8 v3, v3, 0x1

    goto :goto_6

    :cond_e
    move-wide v10, v8

    goto :goto_5

    :cond_f
    iget-wide v6, v5, Landroid/os/BatteryStats$HistoryItem;->time:J

    sub-long/2addr v6, v12

    add-long/2addr v6, v14

    goto :goto_4

    :cond_10
    move-wide v8, v10

    :goto_7
    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_2

    :cond_11
    invoke-virtual/range {p0 .. p0}, Landroid/os/BatteryStats;->finishIteratingHistoryLocked()V

    const/4 v2, 0x0

    :goto_8
    move-object/from16 v0, p3

    array-length v3, v0

    if-ge v2, v3, :cond_12

    aget-object v3, p3, v2

    invoke-interface {v3}, Lcom/android/settingslib/r;->IC()V

    add-int/lit8 v2, v2, 0x1

    goto :goto_8

    :cond_12
    return-void

    :cond_13
    move-wide v8, v10

    goto :goto_7

    :cond_14
    move-wide v6, v8

    move-wide v8, v10

    goto :goto_7
.end method

.method static synthetic cqm(Lcom/android/settingslib/p;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/settingslib/p;->cQc:Z

    return v0
.end method

.method public static cqn(Landroid/content/Context;Landroid/content/Intent;Landroid/os/BatteryStats;J)Lcom/android/settingslib/p;
    .locals 7

    const/4 v6, 0x0

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-wide v4, p3

    invoke-static/range {v1 .. v6}, Lcom/android/settingslib/p;->cqo(Landroid/content/Context;Landroid/content/Intent;Landroid/os/BatteryStats;JZ)Lcom/android/settingslib/p;

    move-result-object v0

    return-object v0
.end method

.method public static cqo(Landroid/content/Context;Landroid/content/Intent;Landroid/os/BatteryStats;JZ)Lcom/android/settingslib/p;
    .locals 9

    new-instance v1, Lcom/android/settingslib/p;

    invoke-direct {v1}, Lcom/android/settingslib/p;-><init>()V

    iput-object p2, v1, Lcom/android/settingslib/p;->cQg:Landroid/os/BatteryStats;

    invoke-static {p1}, Lcom/android/settingslib/v;->cqB(Landroid/content/Intent;)I

    move-result v0

    iput v0, v1, Lcom/android/settingslib/p;->cQe:I

    iget v0, v1, Lcom/android/settingslib/p;->cQe:I

    invoke-static {v0}, Lcom/android/settingslib/v;->cqH(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lcom/android/settingslib/p;->cPZ:Ljava/lang/String;

    const-string/jumbo v0, "plugged"

    const/4 v2, 0x0

    invoke-virtual {p1, v0, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, v1, Lcom/android/settingslib/p;->cQc:Z

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-static {v2, p1}, Lcom/android/settingslib/v;->cqz(Landroid/content/res/Resources;Landroid/content/Intent;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lcom/android/settingslib/p;->cQh:Ljava/lang/String;

    iget-boolean v0, v1, Lcom/android/settingslib/p;->cQc:Z

    if-nez v0, :cond_4

    invoke-virtual {p2, p3, p4}, Landroid/os/BatteryStats;->computeBatteryTimeRemaining(J)J

    move-result-wide v4

    const-wide/16 v6, 0x0

    cmp-long v0, v4, v6

    if-lez v0, :cond_3

    iput-wide v4, v1, Lcom/android/settingslib/p;->cQd:J

    const-wide/16 v6, 0x3e8

    div-long/2addr v4, v6

    invoke-static {p0, v4, v5}, Landroid/text/format/Formatter;->formatShortElapsedTime(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v3

    if-eqz p5, :cond_1

    sget v0, Lcom/android/settingslib/i;->cMK:I

    :goto_1
    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v3, v4, v5

    invoke-virtual {v2, v0, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lcom/android/settingslib/p;->cQb:Ljava/lang/String;

    if-eqz p5, :cond_2

    sget v0, Lcom/android/settingslib/i;->cMH:I

    :goto_2
    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    iget-object v5, v1, Lcom/android/settingslib/p;->cPZ:Ljava/lang/String;

    const/4 v6, 0x0

    aput-object v5, v4, v6

    const/4 v5, 0x1

    aput-object v3, v4, v5

    invoke-virtual {v2, v0, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lcom/android/settingslib/p;->cPY:Ljava/lang/String;

    :goto_3
    return-object v1

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    sget v0, Lcom/android/settingslib/i;->cMJ:I

    goto :goto_1

    :cond_2
    sget v0, Lcom/android/settingslib/i;->cMG:I

    goto :goto_2

    :cond_3
    const/4 v0, 0x0

    iput-object v0, v1, Lcom/android/settingslib/p;->cQb:Ljava/lang/String;

    iget-object v0, v1, Lcom/android/settingslib/p;->cPZ:Ljava/lang/String;

    iput-object v0, v1, Lcom/android/settingslib/p;->cPY:Ljava/lang/String;

    goto :goto_3

    :cond_4
    invoke-virtual {p2, p3, p4}, Landroid/os/BatteryStats;->computeChargeTimeRemaining(J)J

    move-result-wide v4

    const-string/jumbo v0, "status"

    const/4 v3, 0x1

    invoke-virtual {p1, v0, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    const/4 v3, 0x0

    iput-boolean v3, v1, Lcom/android/settingslib/p;->cQf:Z

    const-wide/16 v6, 0x0

    cmp-long v3, v4, v6

    if-lez v3, :cond_6

    const/4 v3, 0x5

    if-eq v0, v3, :cond_6

    iput-wide v4, v1, Lcom/android/settingslib/p;->cQd:J

    const-wide/16 v6, 0x3e8

    div-long/2addr v4, v6

    invoke-static {p0, v4, v5}, Landroid/text/format/Formatter;->formatShortElapsedTime(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v3

    if-eqz p5, :cond_5

    sget v0, Lcom/android/settingslib/i;->cMF:I

    :goto_4
    sget v4, Lcom/android/settingslib/i;->cMI:I

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object v3, v5, v6

    invoke-virtual {v2, v4, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v1, Lcom/android/settingslib/p;->cQb:Ljava/lang/String;

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    iget-object v5, v1, Lcom/android/settingslib/p;->cPZ:Ljava/lang/String;

    const/4 v6, 0x0

    aput-object v5, v4, v6

    const/4 v5, 0x1

    aput-object v3, v4, v5

    invoke-virtual {v2, v0, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lcom/android/settingslib/p;->cPY:Ljava/lang/String;

    goto :goto_3

    :cond_5
    sget v0, Lcom/android/settingslib/i;->cME:I

    goto :goto_4

    :cond_6
    sget v0, Lcom/android/settingslib/i;->cLx:I

    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v3, 0x0

    iput-object v3, v1, Lcom/android/settingslib/p;->cQb:Ljava/lang/String;

    sget v3, Lcom/android/settingslib/i;->cMD:I

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    iget-object v5, v1, Lcom/android/settingslib/p;->cPZ:Ljava/lang/String;

    const/4 v6, 0x0

    aput-object v5, v4, v6

    const/4 v5, 0x1

    aput-object v0, v4, v5

    invoke-virtual {v2, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lcom/android/settingslib/p;->cPY:Ljava/lang/String;

    goto :goto_3
.end method

.method static synthetic cqq(Lcom/android/settingslib/p;J)J
    .locals 1

    iput-wide p1, p0, Lcom/android/settingslib/p;->cQa:J

    return-wide p1
.end method

.method public static cqr(Landroid/content/Context;Lcom/android/settingslib/q;)V
    .locals 1

    const/4 v0, 0x0

    invoke-static {p0, p1, v0}, Lcom/android/settingslib/p;->cqk(Landroid/content/Context;Lcom/android/settingslib/q;Z)V

    return-void
.end method


# virtual methods
.method public varargs cqp(Lcom/android/settingslib/graph/UsageView;[Lcom/android/settingslib/r;)V
    .locals 11

    const/4 v10, 0x1

    const/4 v1, 0x0

    new-instance v2, Lcom/android/settingslib/s;

    invoke-direct {v2, p0, p1}, Lcom/android/settingslib/s;-><init>(Lcom/android/settingslib/p;Lcom/android/settingslib/graph/UsageView;)V

    array-length v0, p2

    add-int/lit8 v0, v0, 0x1

    new-array v3, v0, [Lcom/android/settingslib/r;

    move v0, v1

    :goto_0
    array-length v4, p2

    if-ge v0, v4, :cond_0

    aget-object v4, p2, v0

    aput-object v4, v3, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    array-length v0, p2

    aput-object v2, v3, v0

    iget-object v0, p0, Lcom/android/settingslib/p;->cQg:Landroid/os/BatteryStats;

    iget-wide v4, p0, Lcom/android/settingslib/p;->cQd:J

    invoke-static {v0, v4, v5, v3}, Lcom/android/settingslib/p;->cql(Landroid/os/BatteryStats;J[Lcom/android/settingslib/r;)V

    invoke-virtual {p1}, Lcom/android/settingslib/graph/UsageView;->getContext()Landroid/content/Context;

    move-result-object v2

    sget v0, Lcom/android/settingslib/i;->cMm:I

    new-array v3, v10, [Ljava/lang/Object;

    iget-wide v4, p0, Lcom/android/settingslib/p;->cQa:J

    invoke-static {v2, v4, v5}, Landroid/text/format/Formatter;->formatShortElapsedTime(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v1

    invoke-virtual {v2, v0, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    const-string/jumbo v0, ""

    iget-wide v4, p0, Lcom/android/settingslib/p;->cQd:J

    const-wide/16 v6, 0x0

    cmp-long v4, v4, v6

    if-eqz v4, :cond_1

    sget v0, Lcom/android/settingslib/i;->cMM:I

    new-array v4, v10, [Ljava/lang/Object;

    iget-wide v6, p0, Lcom/android/settingslib/p;->cQd:J

    const-wide/16 v8, 0x3e8

    div-long/2addr v6, v8

    invoke-static {v2, v6, v7}, Landroid/text/format/Formatter;->formatShortElapsedTime(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v1

    invoke-virtual {v2, v0, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    :cond_1
    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/CharSequence;

    aput-object v3, v2, v1

    aput-object v0, v2, v10

    invoke-virtual {p1, v2}, Lcom/android/settingslib/graph/UsageView;->setBottomLabels([Ljava/lang/CharSequence;)V

    return-void
.end method
