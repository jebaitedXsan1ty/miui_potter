.class public Lcom/android/settingslib/MiuiRestrictedSwitchPreference;
.super Landroid/preference/SwitchPreference;
.source "MiuiRestrictedSwitchPreference.java"


# instance fields
.field cQn:Ljava/lang/String;

.field cQo:Landroid/preference/PreferenceScreen;

.field cQp:Lcom/android/settingslib/o;

.field cQq:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/android/settingslib/MiuiRestrictedSwitchPreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    sget v0, Lcom/android/settingslib/b;->cKl:I

    invoke-direct {p0, p1, p2, v0}, Lcom/android/settingslib/MiuiRestrictedSwitchPreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/android/settingslib/MiuiRestrictedSwitchPreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V
    .locals 6

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-direct {p0, p1, p2, p3, p4}, Landroid/preference/SwitchPreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V

    iput-boolean v1, p0, Lcom/android/settingslib/MiuiRestrictedSwitchPreference;->cQq:Z

    iput-object v2, p0, Lcom/android/settingslib/MiuiRestrictedSwitchPreference;->cQn:Ljava/lang/String;

    iput-object v2, p0, Lcom/android/settingslib/MiuiRestrictedSwitchPreference;->cQo:Landroid/preference/PreferenceScreen;

    sget v0, Lcom/android/settingslib/h;->cLm:I

    invoke-virtual {p0, v0}, Lcom/android/settingslib/MiuiRestrictedSwitchPreference;->setWidgetLayoutResource(I)V

    new-instance v0, Lcom/android/settingslib/o;

    invoke-direct {v0, p1, p0, p2}, Lcom/android/settingslib/o;-><init>(Landroid/content/Context;Landroid/preference/Preference;Landroid/util/AttributeSet;)V

    iput-object v0, p0, Lcom/android/settingslib/MiuiRestrictedSwitchPreference;->cQp:Lcom/android/settingslib/o;

    if-eqz p2, :cond_1

    sget-object v0, Lcom/android/settingslib/j;->cOH:[I

    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v3

    sget v0, Lcom/android/settingslib/j;->cOJ:I

    invoke-virtual {v3, v0}, Landroid/content/res/TypedArray;->peekValue(I)Landroid/util/TypedValue;

    move-result-object v0

    if-eqz v0, :cond_0

    iget v4, v0, Landroid/util/TypedValue;->type:I

    const/16 v5, 0x12

    if-ne v4, v5, :cond_5

    iget v0, v0, Landroid/util/TypedValue;->data:I

    if-eqz v0, :cond_4

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/android/settingslib/MiuiRestrictedSwitchPreference;->cQq:Z

    :cond_0
    sget v0, Lcom/android/settingslib/j;->cOI:I

    invoke-virtual {v3, v0}, Landroid/content/res/TypedArray;->peekValue(I)Landroid/util/TypedValue;

    move-result-object v0

    if-eqz v0, :cond_6

    iget v3, v0, Landroid/util/TypedValue;->type:I

    const/4 v4, 0x3

    if-ne v3, v4, :cond_6

    iget v3, v0, Landroid/util/TypedValue;->resourceId:I

    if-eqz v3, :cond_7

    iget v0, v0, Landroid/util/TypedValue;->resourceId:I

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_1
    if-nez v0, :cond_8

    move-object v0, v2

    :goto_2
    iput-object v0, p0, Lcom/android/settingslib/MiuiRestrictedSwitchPreference;->cQn:Ljava/lang/String;

    :cond_1
    iget-object v0, p0, Lcom/android/settingslib/MiuiRestrictedSwitchPreference;->cQn:Ljava/lang/String;

    if-nez v0, :cond_2

    sget v0, Lcom/android/settingslib/i;->cMu:I

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settingslib/MiuiRestrictedSwitchPreference;->cQn:Ljava/lang/String;

    :cond_2
    iget-boolean v0, p0, Lcom/android/settingslib/MiuiRestrictedSwitchPreference;->cQq:Z

    if-eqz v0, :cond_3

    sget v0, Lcom/android/settingslib/h;->cLl:I

    invoke-virtual {p0, v0}, Lcom/android/settingslib/MiuiRestrictedSwitchPreference;->setLayoutResource(I)V

    invoke-virtual {p0, v1}, Lcom/android/settingslib/MiuiRestrictedSwitchPreference;->cqv(Z)V

    :cond_3
    return-void

    :cond_4
    move v0, v1

    goto :goto_0

    :cond_5
    move v0, v1

    goto :goto_0

    :cond_6
    move-object v0, v2

    goto :goto_1

    :cond_7
    iget-object v0, v0, Landroid/util/TypedValue;->string:Ljava/lang/CharSequence;

    goto :goto_1

    :cond_8
    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_2
.end method


# virtual methods
.method public cqt(Ljava/lang/String;)V
    .locals 2

    iget-object v0, p0, Lcom/android/settingslib/MiuiRestrictedSwitchPreference;->cQp:Lcom/android/settingslib/o;

    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v1

    invoke-virtual {v0, p1, v1}, Lcom/android/settingslib/o;->cqg(Ljava/lang/String;I)V

    return-void
.end method

.method public cqu(Ljava/lang/String;I)V
    .locals 1

    iget-object v0, p0, Lcom/android/settingslib/MiuiRestrictedSwitchPreference;->cQp:Lcom/android/settingslib/o;

    invoke-virtual {v0, p1, p2}, Lcom/android/settingslib/o;->cqg(Ljava/lang/String;I)V

    return-void
.end method

.method public cqv(Z)V
    .locals 1

    iget-object v0, p0, Lcom/android/settingslib/MiuiRestrictedSwitchPreference;->cQp:Lcom/android/settingslib/o;

    invoke-virtual {v0, p1}, Lcom/android/settingslib/o;->cqh(Z)V

    return-void
.end method

.method public cqw()Z
    .locals 1

    iget-object v0, p0, Lcom/android/settingslib/MiuiRestrictedSwitchPreference;->cQp:Lcom/android/settingslib/o;

    invoke-virtual {v0}, Lcom/android/settingslib/o;->cqj()Z

    move-result v0

    return v0
.end method

.method protected onAttachedToHierarchy(Landroid/preference/PreferenceManager;)V
    .locals 1

    iget-object v0, p0, Lcom/android/settingslib/MiuiRestrictedSwitchPreference;->cQp:Lcom/android/settingslib/o;

    invoke-virtual {v0}, Lcom/android/settingslib/o;->cqi()V

    invoke-super {p0, p1}, Landroid/preference/SwitchPreference;->onAttachedToHierarchy(Landroid/preference/PreferenceManager;)V

    return-void
.end method

.method public onBindView(Landroid/view/View;)V
    .locals 5

    const/16 v3, 0x8

    const/4 v2, 0x0

    invoke-super {p0, p1}, Landroid/preference/SwitchPreference;->onBindView(Landroid/view/View;)V

    iget-object v0, p0, Lcom/android/settingslib/MiuiRestrictedSwitchPreference;->cQp:Lcom/android/settingslib/o;

    invoke-virtual {v0, p1}, Lcom/android/settingslib/o;->onBindView(Landroid/view/View;)V

    sget v0, Lcom/android/settingslib/g;->cLd:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    const v0, 0x1020040

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lmiui/widget/SlidingButton;

    if-eqz v4, :cond_0

    invoke-virtual {p0}, Lcom/android/settingslib/MiuiRestrictedSwitchPreference;->cqw()Z

    move-result v1

    if-eqz v1, :cond_3

    move v1, v2

    :goto_0
    invoke-virtual {v4, v1}, Landroid/view/View;->setVisibility(I)V

    :cond_0
    if-eqz v0, :cond_1

    new-instance v1, Lcom/android/settingslib/u;

    invoke-direct {v1, p0, p1, v0}, Lcom/android/settingslib/u;-><init>(Lcom/android/settingslib/MiuiRestrictedSwitchPreference;Landroid/view/View;Lmiui/widget/SlidingButton;)V

    invoke-virtual {v0, v1}, Lmiui/widget/SlidingButton;->setOnPerformCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    invoke-virtual {p0}, Lcom/android/settingslib/MiuiRestrictedSwitchPreference;->cqw()Z

    move-result v1

    if-eqz v1, :cond_4

    move v1, v3

    :goto_1
    invoke-virtual {v0, v1}, Lmiui/widget/SlidingButton;->setVisibility(I)V

    :cond_1
    iget-boolean v0, p0, Lcom/android/settingslib/MiuiRestrictedSwitchPreference;->cQq:Z

    if-eqz v0, :cond_6

    sget v0, Lcom/android/settingslib/g;->cKQ:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lcom/android/settingslib/MiuiRestrictedSwitchPreference;->cqw()Z

    move-result v1

    if-eqz v1, :cond_5

    iget-object v1, p0, Lcom/android/settingslib/MiuiRestrictedSwitchPreference;->cQn:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    :cond_2
    :goto_2
    return-void

    :cond_3
    move v1, v3

    goto :goto_0

    :cond_4
    move v1, v2

    goto :goto_1

    :cond_5
    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_2

    :cond_6
    const v0, 0x1020010

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lcom/android/settingslib/MiuiRestrictedSwitchPreference;->cqw()Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/android/settingslib/MiuiRestrictedSwitchPreference;->cQn:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_2
.end method

.method public performClick(Landroid/preference/PreferenceScreen;)V
    .locals 1

    iget-object v0, p0, Lcom/android/settingslib/MiuiRestrictedSwitchPreference;->cQp:Lcom/android/settingslib/o;

    invoke-virtual {v0}, Lcom/android/settingslib/o;->performClick()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-super {p0, p1}, Landroid/preference/SwitchPreference;->performClick(Landroid/preference/PreferenceScreen;)V

    :cond_0
    return-void
.end method

.method public setDisabledByAdmin(Lcom/android/settingslib/n;)V
    .locals 1

    iget-object v0, p0, Lcom/android/settingslib/MiuiRestrictedSwitchPreference;->cQp:Lcom/android/settingslib/o;

    invoke-virtual {v0, p1}, Lcom/android/settingslib/o;->setDisabledByAdmin(Lcom/android/settingslib/n;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/android/settingslib/MiuiRestrictedSwitchPreference;->notifyChanged()V

    :cond_0
    return-void
.end method

.method public setEnabled(Z)V
    .locals 2

    if-eqz p1, :cond_0

    invoke-virtual {p0}, Lcom/android/settingslib/MiuiRestrictedSwitchPreference;->cqw()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settingslib/MiuiRestrictedSwitchPreference;->cQp:Lcom/android/settingslib/o;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/android/settingslib/o;->setDisabledByAdmin(Lcom/android/settingslib/n;)Z

    return-void

    :cond_0
    invoke-super {p0, p1}, Landroid/preference/SwitchPreference;->setEnabled(Z)V

    return-void
.end method

.method public setPreferenceScreen(Landroid/preference/PreferenceScreen;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settingslib/MiuiRestrictedSwitchPreference;->cQo:Landroid/preference/PreferenceScreen;

    return-void
.end method
