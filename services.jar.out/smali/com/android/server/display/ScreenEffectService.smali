.class public Lcom/android/server/display/ScreenEffectService;
.super Lcom/android/server/SystemService;
.source "ScreenEffectService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/display/ScreenEffectService$LocalService;,
        Lcom/android/server/display/ScreenEffectService$ScreenEffectHandler;,
        Lcom/android/server/display/ScreenEffectService$SettingsObserver;,
        Lcom/android/server/display/ScreenEffectService$UserSwitchReceiver;
    }
.end annotation


# static fields
.field private static final MSG_UPDATE_MONOCHROME_MODE:I = 0x3

.field private static final MSG_UPDATE_NIGHT_LIGHT_COLOR:I = 0x5

.field private static final MSG_UPDATE_PAPER_MODE:I = 0x1

.field private static final MSG_UPDATE_SCREEN_OPTIMIZE:I = 0x2

.field private static final SUPPORT_MONOCHROME_MODE:Z

.field private static final TAG:Ljava/lang/String; = "ScreenEffectService"

.field private static sDisplayPowerController:Lcom/android/server/display/DisplayPowerController;

.field static sScreenEffectManager:Lcom/android/server/display/ScreenEffectService$LocalService;


# instance fields
.field private mContext:Landroid/content/Context;

.field private mDisplayFeatureManager:Lmiui/hardware/display/DisplayFeatureManager;

.field private mDisplayState:I

.field private mHandler:Landroid/os/Handler;

.field private mHandlerThread:Landroid/os/HandlerThread;

.field private mIsScreenEyeCare:Z

.field private mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

.field private mMonochromeModeEnabled:Z

.field private mMonochromeModeType:I

.field private mMonochromeWhiteList:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private mNightLightBrightness:I

.field private mNightLightColor:I

.field private mPaperModeEnabled:Z

.field private mPaperModeLevel:I

.field private mPaperModeWhiteList:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private mScreenColorLevel:I

.field private mScreenOptimizeMode:I

.field private mSettingsObserver:Lcom/android/server/display/ScreenEffectService$SettingsObserver;

.field private mTopAppPkg:Ljava/lang/String;


# direct methods
.method static synthetic -get0()Z
    .locals 1

    sget-boolean v0, Lcom/android/server/display/ScreenEffectService;->SUPPORT_MONOCHROME_MODE:Z

    return v0
.end method

.method static synthetic -get1(Lcom/android/server/display/ScreenEffectService;)Landroid/content/Context;
    .locals 1

    iget-object v0, p0, Lcom/android/server/display/ScreenEffectService;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic -get10(Lcom/android/server/display/ScreenEffectService;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/android/server/display/ScreenEffectService;->mTopAppPkg:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic -get11()Lcom/android/server/display/DisplayPowerController;
    .locals 1

    sget-object v0, Lcom/android/server/display/ScreenEffectService;->sDisplayPowerController:Lcom/android/server/display/DisplayPowerController;

    return-object v0
.end method

.method static synthetic -get2(Lcom/android/server/display/ScreenEffectService;)I
    .locals 1

    iget v0, p0, Lcom/android/server/display/ScreenEffectService;->mDisplayState:I

    return v0
.end method

.method static synthetic -get3(Lcom/android/server/display/ScreenEffectService;)Landroid/os/Handler;
    .locals 1

    iget-object v0, p0, Lcom/android/server/display/ScreenEffectService;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic -get4(Lcom/android/server/display/ScreenEffectService;)Lcom/android/internal/widget/LockPatternUtils;
    .locals 1

    iget-object v0, p0, Lcom/android/server/display/ScreenEffectService;->mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

    return-object v0
.end method

.method static synthetic -get5(Lcom/android/server/display/ScreenEffectService;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/server/display/ScreenEffectService;->mMonochromeModeEnabled:Z

    return v0
.end method

.method static synthetic -get6(Lcom/android/server/display/ScreenEffectService;)I
    .locals 1

    iget v0, p0, Lcom/android/server/display/ScreenEffectService;->mMonochromeModeType:I

    return v0
.end method

.method static synthetic -get7(Lcom/android/server/display/ScreenEffectService;)I
    .locals 1

    iget v0, p0, Lcom/android/server/display/ScreenEffectService;->mNightLightBrightness:I

    return v0
.end method

.method static synthetic -get8(Lcom/android/server/display/ScreenEffectService;)I
    .locals 1

    iget v0, p0, Lcom/android/server/display/ScreenEffectService;->mNightLightColor:I

    return v0
.end method

.method static synthetic -get9(Lcom/android/server/display/ScreenEffectService;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/server/display/ScreenEffectService;->mPaperModeEnabled:Z

    return v0
.end method

.method static synthetic -set0(Lcom/android/server/display/ScreenEffectService;I)I
    .locals 0

    iput p1, p0, Lcom/android/server/display/ScreenEffectService;->mDisplayState:I

    return p1
.end method

.method static synthetic -set1(Lcom/android/server/display/ScreenEffectService;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/android/server/display/ScreenEffectService;->mMonochromeModeEnabled:Z

    return p1
.end method

.method static synthetic -set10(Lcom/android/server/display/ScreenEffectService;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    iput-object p1, p0, Lcom/android/server/display/ScreenEffectService;->mTopAppPkg:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic -set2(Lcom/android/server/display/ScreenEffectService;I)I
    .locals 0

    iput p1, p0, Lcom/android/server/display/ScreenEffectService;->mMonochromeModeType:I

    return p1
.end method

.method static synthetic -set3(Lcom/android/server/display/ScreenEffectService;Ljava/util/HashMap;)Ljava/util/HashMap;
    .locals 0

    iput-object p1, p0, Lcom/android/server/display/ScreenEffectService;->mMonochromeWhiteList:Ljava/util/HashMap;

    return-object p1
.end method

.method static synthetic -set4(Lcom/android/server/display/ScreenEffectService;I)I
    .locals 0

    iput p1, p0, Lcom/android/server/display/ScreenEffectService;->mNightLightBrightness:I

    return p1
.end method

.method static synthetic -set5(Lcom/android/server/display/ScreenEffectService;I)I
    .locals 0

    iput p1, p0, Lcom/android/server/display/ScreenEffectService;->mNightLightColor:I

    return p1
.end method

.method static synthetic -set6(Lcom/android/server/display/ScreenEffectService;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/android/server/display/ScreenEffectService;->mPaperModeEnabled:Z

    return p1
.end method

.method static synthetic -set7(Lcom/android/server/display/ScreenEffectService;I)I
    .locals 0

    iput p1, p0, Lcom/android/server/display/ScreenEffectService;->mPaperModeLevel:I

    return p1
.end method

.method static synthetic -set8(Lcom/android/server/display/ScreenEffectService;I)I
    .locals 0

    iput p1, p0, Lcom/android/server/display/ScreenEffectService;->mScreenColorLevel:I

    return p1
.end method

.method static synthetic -set9(Lcom/android/server/display/ScreenEffectService;I)I
    .locals 0

    iput p1, p0, Lcom/android/server/display/ScreenEffectService;->mScreenOptimizeMode:I

    return p1
.end method

.method static synthetic -wrap0(Lcom/android/server/display/ScreenEffectService;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/display/ScreenEffectService;->loadSettings()V

    return-void
.end method

.method static synthetic -wrap1(Lcom/android/server/display/ScreenEffectService;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/display/ScreenEffectService;->updateMonochromeMode()V

    return-void
.end method

.method static synthetic -wrap2(Lcom/android/server/display/ScreenEffectService;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/display/ScreenEffectService;->updateNightLightColor()V

    return-void
.end method

.method static synthetic -wrap3(Lcom/android/server/display/ScreenEffectService;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/display/ScreenEffectService;->updatePaperMode(Z)V

    return-void
.end method

.method static synthetic -wrap4(Lcom/android/server/display/ScreenEffectService;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/display/ScreenEffectService;->updateScreenOptimize()V

    return-void
.end method

.method static constructor <clinit>()V
    .locals 2

    const/4 v0, 0x0

    sget v1, Landroid/provider/MiuiSettings$ScreenEffect;->SCREEN_EFFECT_SUPPORTED:I

    and-int/lit8 v1, v1, 0x8

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    sput-boolean v0, Lcom/android/server/display/ScreenEffectService;->SUPPORT_MONOCHROME_MODE:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    invoke-direct {p0, p1}, Lcom/android/server/SystemService;-><init>(Landroid/content/Context;)V

    iput-object p1, p0, Lcom/android/server/display/ScreenEffectService;->mContext:Landroid/content/Context;

    new-instance v0, Landroid/os/HandlerThread;

    const-string/jumbo v1, "ScreenEffectThread"

    invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/android/server/display/ScreenEffectService;->mHandlerThread:Landroid/os/HandlerThread;

    iget-object v0, p0, Lcom/android/server/display/ScreenEffectService;->mHandlerThread:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    new-instance v0, Lcom/android/server/display/ScreenEffectService$ScreenEffectHandler;

    iget-object v1, p0, Lcom/android/server/display/ScreenEffectService;->mHandlerThread:Landroid/os/HandlerThread;

    invoke-virtual {v1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/android/server/display/ScreenEffectService$ScreenEffectHandler;-><init>(Lcom/android/server/display/ScreenEffectService;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/android/server/display/ScreenEffectService;->mHandler:Landroid/os/Handler;

    return-void
.end method

.method private checkSettingsData()V
    .locals 8

    const/4 v3, 0x0

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, -0x2

    const/4 v4, 0x0

    iget-object v1, p0, Lcom/android/server/display/ScreenEffectService;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string/jumbo v2, "screen_paper_mode"

    invoke-static {v1, v2, v6}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    if-ne v1, v7, :cond_0

    iget-object v1, p0, Lcom/android/server/display/ScreenEffectService;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string/jumbo v2, "screen_paper_mode_enabled"

    invoke-static {v1, v2, v4, v5}, Landroid/provider/Settings$System;->putIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)Z

    iget-object v1, p0, Lcom/android/server/display/ScreenEffectService;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string/jumbo v2, "screen_paper_mode"

    invoke-static {v2}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v1, v2, v3, v3}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    :cond_0
    iget-object v1, p0, Lcom/android/server/display/ScreenEffectService;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string/jumbo v2, "screen_paper_mode_level"

    invoke-static {v1, v2, v4}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    if-nez v1, :cond_1

    const-string/jumbo v1, "persist.sys.eyecare_cache"

    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_4

    iget-object v1, p0, Lcom/android/server/display/ScreenEffectService;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string/jumbo v2, "screen_paper_mode_level"

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    invoke-static {v1, v2, v3, v5}, Landroid/provider/Settings$System;->putIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)Z

    :cond_1
    :goto_0
    iget-object v1, p0, Lcom/android/server/display/ScreenEffectService;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string/jumbo v2, "screen_color_level"

    invoke-static {v1, v2, v4}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    if-nez v1, :cond_2

    const-string/jumbo v1, "persist.sys.display_prefer"

    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/android/server/display/ScreenEffectService;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string/jumbo v2, "screen_color_level"

    invoke-static {}, Lmiui/hardware/display/DisplayFeatureManager;->getInstance()Lmiui/hardware/display/DisplayFeatureManager;

    move-result-object v3

    invoke-virtual {v3}, Lmiui/hardware/display/DisplayFeatureManager;->getColorPrefer()I

    move-result v3

    invoke-static {v1, v2, v3, v5}, Landroid/provider/Settings$System;->putIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)Z

    :cond_2
    iget-object v1, p0, Lcom/android/server/display/ScreenEffectService;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string/jumbo v2, "screen_optimize_mode"

    invoke-static {v1, v2, v4}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    if-nez v1, :cond_3

    const-string/jumbo v1, "persist.sys.display_ce"

    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_3

    invoke-static {}, Lmiui/hardware/display/DisplayFeatureManager;->getInstance()Lmiui/hardware/display/DisplayFeatureManager;

    move-result-object v1

    invoke-virtual {v1}, Lmiui/hardware/display/DisplayFeatureManager;->isAdEnable()Z

    move-result v1

    if-eqz v1, :cond_5

    iget-object v1, p0, Lcom/android/server/display/ScreenEffectService;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string/jumbo v2, "screen_optimize_mode"

    invoke-static {v1, v2, v6, v5}, Landroid/provider/Settings$System;->putIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)Z

    :cond_3
    :goto_1
    return-void

    :cond_4
    iget-object v1, p0, Lcom/android/server/display/ScreenEffectService;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string/jumbo v2, "screen_paper_mode_level"

    sget v3, Landroid/provider/MiuiSettings$ScreenEffect;->DEFAULT_PAPER_MODE_LEVEL:I

    invoke-static {v1, v2, v3, v5}, Landroid/provider/Settings$System;->putIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)Z

    goto :goto_0

    :cond_5
    invoke-static {}, Lmiui/hardware/display/DisplayFeatureManager;->getInstance()Lmiui/hardware/display/DisplayFeatureManager;

    move-result-object v1

    invoke-virtual {v1}, Lmiui/hardware/display/DisplayFeatureManager;->getScreenGamut()I

    move-result v1

    if-nez v1, :cond_6

    iget-object v1, p0, Lcom/android/server/display/ScreenEffectService;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string/jumbo v2, "screen_optimize_mode"

    invoke-static {v1, v2, v7, v5}, Landroid/provider/Settings$System;->putIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)Z

    goto :goto_1

    :cond_6
    iget-object v1, p0, Lcom/android/server/display/ScreenEffectService;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string/jumbo v2, "screen_optimize_mode"

    const/4 v3, 0x3

    invoke-static {v1, v2, v3, v5}, Landroid/provider/Settings$System;->putIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)Z

    goto :goto_1
.end method

.method private loadSettings()V
    .locals 9

    const/4 v8, 0x2

    const/4 v2, 0x1

    const/4 v7, -0x1

    const/4 v3, 0x0

    const/4 v6, -0x2

    iget-object v1, p0, Lcom/android/server/display/ScreenEffectService;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string/jumbo v4, "screen_paper_mode_enabled"

    invoke-static {v1, v4, v3, v6}, Landroid/provider/Settings$System;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v1

    if-eqz v1, :cond_2

    move v1, v2

    :goto_0
    iput-boolean v1, p0, Lcom/android/server/display/ScreenEffectService;->mPaperModeEnabled:Z

    iget-object v1, p0, Lcom/android/server/display/ScreenEffectService;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string/jumbo v4, "screen_paper_mode_level"

    sget v5, Landroid/provider/MiuiSettings$ScreenEffect;->DEFAULT_PAPER_MODE_LEVEL:I

    invoke-static {v1, v4, v5, v6}, Landroid/provider/Settings$System;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v1

    iput v1, p0, Lcom/android/server/display/ScreenEffectService;->mPaperModeLevel:I

    iget-object v1, p0, Lcom/android/server/display/ScreenEffectService;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string/jumbo v4, "screen_optimize_mode"

    sget v5, Landroid/provider/MiuiSettings$ScreenEffect;->DEFAULT_SCREEN_OPTIMIZE_MODE:I

    invoke-static {v1, v4, v5, v6}, Landroid/provider/Settings$System;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v1

    iput v1, p0, Lcom/android/server/display/ScreenEffectService;->mScreenOptimizeMode:I

    iget-object v1, p0, Lcom/android/server/display/ScreenEffectService;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string/jumbo v4, "screen_color_level"

    invoke-static {v1, v4, v8, v6}, Landroid/provider/Settings$System;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v1

    iput v1, p0, Lcom/android/server/display/ScreenEffectService;->mScreenColorLevel:I

    sget-boolean v1, Lcom/android/server/display/ScreenEffectService;->SUPPORT_MONOCHROME_MODE:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/server/display/ScreenEffectService;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string/jumbo v4, "screen_monochrome_mode_enabled"

    invoke-static {v1, v4, v3, v6}, Landroid/provider/Settings$System;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v1

    if-eqz v1, :cond_3

    :goto_1
    iput-boolean v2, p0, Lcom/android/server/display/ScreenEffectService;->mMonochromeModeEnabled:Z

    iget-object v1, p0, Lcom/android/server/display/ScreenEffectService;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string/jumbo v2, "screen_monochrome_mode"

    invoke-static {v1, v2, v8, v6}, Landroid/provider/Settings$System;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v1

    iput v1, p0, Lcom/android/server/display/ScreenEffectService;->mMonochromeModeType:I

    iget-object v1, p0, Lcom/android/server/display/ScreenEffectService;->mContext:Landroid/content/Context;

    const-string/jumbo v2, "screen_monochrome_mode_white_list"

    invoke-static {v1, v2}, Landroid/provider/MiuiSettings$ScreenEffect;->getScreenModePkgList(Landroid/content/Context;Ljava/lang/String;)Ljava/util/HashMap;

    move-result-object v1

    iput-object v1, p0, Lcom/android/server/display/ScreenEffectService;->mMonochromeWhiteList:Ljava/util/HashMap;

    :cond_0
    sget-boolean v1, Lmiui/os/DeviceFeature;->SUPPORT_NIGHT_LIGHT_ADJ:Z

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/android/server/display/ScreenEffectService;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string/jumbo v2, "night_light_level"

    invoke-static {v1, v2, v7, v6}, Landroid/provider/Settings$System;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v0

    if-eq v0, v7, :cond_4

    and-int/lit16 v1, v0, 0xff

    iget-object v2, p0, Lcom/android/server/display/ScreenEffectService;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x11070015

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v2

    iget-object v3, p0, Lcom/android/server/display/ScreenEffectService;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x11070014

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v3

    invoke-static {v1, v2, v3}, Landroid/util/MathUtils;->constrain(III)I

    move-result v1

    iput v1, p0, Lcom/android/server/display/ScreenEffectService;->mNightLightBrightness:I

    const v1, 0xff00

    and-int/2addr v1, v0

    shr-int/lit8 v1, v1, 0x8

    iget-object v2, p0, Lcom/android/server/display/ScreenEffectService;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x11070018

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v2

    iget-object v3, p0, Lcom/android/server/display/ScreenEffectService;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x11070017

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v3

    invoke-static {v1, v2, v3}, Landroid/util/MathUtils;->constrain(III)I

    move-result v1

    iput v1, p0, Lcom/android/server/display/ScreenEffectService;->mNightLightColor:I

    :cond_1
    :goto_2
    return-void

    :cond_2
    move v1, v3

    goto/16 :goto_0

    :cond_3
    move v2, v3

    goto :goto_1

    :cond_4
    iget-object v1, p0, Lcom/android/server/display/ScreenEffectService;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x11070016

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    iput v1, p0, Lcom/android/server/display/ScreenEffectService;->mNightLightBrightness:I

    iget-object v1, p0, Lcom/android/server/display/ScreenEffectService;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x11070019

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    iput v1, p0, Lcom/android/server/display/ScreenEffectService;->mNightLightColor:I

    goto :goto_2
.end method

.method static setDisplayPowerController(Lcom/android/server/display/DisplayPowerController;)V
    .locals 0

    sput-object p0, Lcom/android/server/display/ScreenEffectService;->sDisplayPowerController:Lcom/android/server/display/DisplayPowerController;

    return-void
.end method

.method private setScreenEyeCare(Z)V
    .locals 3

    iput-boolean p1, p0, Lcom/android/server/display/ScreenEffectService;->mIsScreenEyeCare:Z

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/android/server/display/ScreenEffectService;->mDisplayFeatureManager:Lmiui/hardware/display/DisplayFeatureManager;

    iget v1, p0, Lcom/android/server/display/ScreenEffectService;->mPaperModeLevel:I

    const/4 v2, 0x3

    invoke-virtual {v0, v2, v1}, Lmiui/hardware/display/DisplayFeatureManager;->setScreenEffect(II)V

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0}, Lcom/android/server/display/ScreenEffectService;->updateScreenOptimize()V

    goto :goto_0
.end method

.method public static startScreenEffectService()V
    .locals 2

    sget v1, Landroid/provider/MiuiSettings$ScreenEffect;->SCREEN_EFFECT_SUPPORTED:I

    if-eqz v1, :cond_0

    sget-object v1, Lcom/android/server/display/ScreenEffectService;->sScreenEffectManager:Lcom/android/server/display/ScreenEffectService$LocalService;

    if-nez v1, :cond_0

    const-class v1, Lcom/android/server/SystemServiceManager;

    invoke-static {v1}, Lcom/android/server/LocalServices;->getService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/server/SystemServiceManager;

    const-class v1, Lcom/android/server/display/ScreenEffectService;

    invoke-virtual {v0, v1}, Lcom/android/server/SystemServiceManager;->startService(Ljava/lang/Class;)Lcom/android/server/SystemService;

    :cond_0
    return-void
.end method

.method private updateMonochromeMode()V
    .locals 7

    const/4 v3, 0x0

    const/4 v6, 0x4

    const/4 v2, 0x1

    iget-boolean v4, p0, Lcom/android/server/display/ScreenEffectService;->mMonochromeModeEnabled:Z

    if-nez v4, :cond_0

    iget-object v2, p0, Lcom/android/server/display/ScreenEffectService;->mDisplayFeatureManager:Lmiui/hardware/display/DisplayFeatureManager;

    invoke-virtual {v2, v6, v3}, Lmiui/hardware/display/DisplayFeatureManager;->setScreenEffect(II)V

    :goto_0
    return-void

    :cond_0
    iget v4, p0, Lcom/android/server/display/ScreenEffectService;->mMonochromeModeType:I

    if-ne v4, v2, :cond_1

    iget-object v3, p0, Lcom/android/server/display/ScreenEffectService;->mDisplayFeatureManager:Lmiui/hardware/display/DisplayFeatureManager;

    invoke-virtual {v3, v6, v2}, Lmiui/hardware/display/DisplayFeatureManager;->setScreenEffect(II)V

    goto :goto_0

    :cond_1
    iget-object v4, p0, Lcom/android/server/display/ScreenEffectService;->mMonochromeWhiteList:Ljava/util/HashMap;

    iget-object v5, p0, Lcom/android/server/display/ScreenEffectService;->mTopAppPkg:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    :goto_1
    iget-object v4, p0, Lcom/android/server/display/ScreenEffectService;->mDisplayFeatureManager:Lmiui/hardware/display/DisplayFeatureManager;

    if-eqz v0, :cond_3

    :goto_2
    invoke-virtual {v4, v6, v2}, Lmiui/hardware/display/DisplayFeatureManager;->setScreenEffect(II)V

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    :cond_3
    move v2, v3

    goto :goto_2
.end method

.method private updateNightLightColor()V
    .locals 3

    iget-object v0, p0, Lcom/android/server/display/ScreenEffectService;->mDisplayFeatureManager:Lmiui/hardware/display/DisplayFeatureManager;

    iget v1, p0, Lcom/android/server/display/ScreenEffectService;->mNightLightColor:I

    const/16 v2, 0x9

    invoke-virtual {v0, v2, v1}, Lmiui/hardware/display/DisplayFeatureManager;->setScreenEffect(II)V

    return-void
.end method

.method private updatePaperMode(Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/display/ScreenEffectService;->setScreenEyeCare(Z)V

    return-void
.end method

.method static updateScreenEffect(I)V
    .locals 1

    sget-object v0, Lcom/android/server/display/ScreenEffectService;->sScreenEffectManager:Lcom/android/server/display/ScreenEffectService$LocalService;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/android/server/display/ScreenEffectService;->sScreenEffectManager:Lcom/android/server/display/ScreenEffectService$LocalService;

    invoke-virtual {v0, p0}, Lcom/android/server/display/ScreenEffectService$LocalService;->updateScreenEffect(I)V

    :cond_0
    return-void
.end method

.method private updateScreenOptimize()V
    .locals 4

    iget-boolean v2, p0, Lcom/android/server/display/ScreenEffectService;->mIsScreenEyeCare:Z

    if-eqz v2, :cond_0

    return-void

    :cond_0
    iget v1, p0, Lcom/android/server/display/ScreenEffectService;->mScreenColorLevel:I

    sget v2, Landroid/provider/MiuiSettings$ScreenEffect;->SCREEN_EFFECT_SUPPORTED:I

    and-int/lit8 v2, v2, 0x1

    if-eqz v2, :cond_1

    iget v2, p0, Lcom/android/server/display/ScreenEffectService;->mScreenOptimizeMode:I

    const/4 v3, 0x1

    if-eq v2, v3, :cond_1

    const/4 v1, 0x2

    :cond_1
    const/4 v0, 0x0

    iget v2, p0, Lcom/android/server/display/ScreenEffectService;->mScreenOptimizeMode:I

    const/4 v3, 0x2

    if-ne v2, v3, :cond_3

    const/4 v0, 0x1

    :cond_2
    :goto_0
    iget-object v2, p0, Lcom/android/server/display/ScreenEffectService;->mDisplayFeatureManager:Lmiui/hardware/display/DisplayFeatureManager;

    invoke-virtual {v2, v0, v1}, Lmiui/hardware/display/DisplayFeatureManager;->setScreenEffect(II)V

    return-void

    :cond_3
    iget v2, p0, Lcom/android/server/display/ScreenEffectService;->mScreenOptimizeMode:I

    const/4 v3, 0x3

    if-ne v2, v3, :cond_2

    const/4 v0, 0x2

    goto :goto_0
.end method


# virtual methods
.method public onBootPhase(I)V
    .locals 6

    const/4 v2, 0x1

    const/4 v5, 0x0

    const/4 v4, -0x1

    const/16 v0, 0x1f4

    if-ne p1, v0, :cond_2

    invoke-direct {p0}, Lcom/android/server/display/ScreenEffectService;->checkSettingsData()V

    invoke-direct {p0}, Lcom/android/server/display/ScreenEffectService;->loadSettings()V

    iget-object v0, p0, Lcom/android/server/display/ScreenEffectService;->mHandler:Landroid/os/Handler;

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    sget-boolean v0, Lmiui/os/DeviceFeature;->SUPPORT_NIGHT_LIGHT_ADJ:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/server/display/ScreenEffectService;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    :cond_0
    new-instance v0, Lcom/android/server/display/ScreenEffectService$SettingsObserver;

    invoke-direct {v0, p0}, Lcom/android/server/display/ScreenEffectService$SettingsObserver;-><init>(Lcom/android/server/display/ScreenEffectService;)V

    iput-object v0, p0, Lcom/android/server/display/ScreenEffectService;->mSettingsObserver:Lcom/android/server/display/ScreenEffectService$SettingsObserver;

    iget-object v0, p0, Lcom/android/server/display/ScreenEffectService;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v1, "screen_paper_mode_enabled"

    invoke-static {v1}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/display/ScreenEffectService;->mSettingsObserver:Lcom/android/server/display/ScreenEffectService$SettingsObserver;

    invoke-virtual {v0, v1, v5, v2, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    iget-object v0, p0, Lcom/android/server/display/ScreenEffectService;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v1, "screen_paper_mode_level"

    invoke-static {v1}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/display/ScreenEffectService;->mSettingsObserver:Lcom/android/server/display/ScreenEffectService$SettingsObserver;

    invoke-virtual {v0, v1, v5, v2, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    iget-object v0, p0, Lcom/android/server/display/ScreenEffectService;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v1, "screen_optimize_mode"

    invoke-static {v1}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/display/ScreenEffectService;->mSettingsObserver:Lcom/android/server/display/ScreenEffectService$SettingsObserver;

    invoke-virtual {v0, v1, v5, v2, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    iget-object v0, p0, Lcom/android/server/display/ScreenEffectService;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v1, "screen_color_level"

    invoke-static {v1}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/display/ScreenEffectService;->mSettingsObserver:Lcom/android/server/display/ScreenEffectService$SettingsObserver;

    invoke-virtual {v0, v1, v5, v2, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    iget-object v0, p0, Lcom/android/server/display/ScreenEffectService;->mContext:Landroid/content/Context;

    new-instance v1, Lcom/android/server/display/ScreenEffectService$UserSwitchReceiver;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/android/server/display/ScreenEffectService$UserSwitchReceiver;-><init>(Lcom/android/server/display/ScreenEffectService;Lcom/android/server/display/ScreenEffectService$UserSwitchReceiver;)V

    new-instance v2, Landroid/content/IntentFilter;

    const-string/jumbo v3, "android.intent.action.USER_SWITCHED"

    invoke-direct {v2, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    sget-boolean v0, Lcom/android/server/display/ScreenEffectService;->SUPPORT_MONOCHROME_MODE:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/server/display/ScreenEffectService;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v1, "screen_monochrome_mode_enabled"

    invoke-static {v1}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/display/ScreenEffectService;->mSettingsObserver:Lcom/android/server/display/ScreenEffectService$SettingsObserver;

    invoke-virtual {v0, v1, v5, v2, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    iget-object v0, p0, Lcom/android/server/display/ScreenEffectService;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v1, "screen_monochrome_mode"

    invoke-static {v1}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/display/ScreenEffectService;->mSettingsObserver:Lcom/android/server/display/ScreenEffectService$SettingsObserver;

    invoke-virtual {v0, v1, v5, v2, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    iget-object v0, p0, Lcom/android/server/display/ScreenEffectService;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v1, "screen_monochrome_mode_white_list"

    invoke-static {v1}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/display/ScreenEffectService;->mSettingsObserver:Lcom/android/server/display/ScreenEffectService$SettingsObserver;

    invoke-virtual {v0, v1, v5, v2, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    iget-object v0, p0, Lcom/android/server/display/ScreenEffectService;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    :cond_1
    sget-boolean v0, Lmiui/os/DeviceFeature;->SUPPORT_NIGHT_LIGHT_ADJ:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/server/display/ScreenEffectService;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v1, "night_light_level"

    invoke-static {v1}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/display/ScreenEffectService;->mSettingsObserver:Lcom/android/server/display/ScreenEffectService$SettingsObserver;

    invoke-virtual {v0, v1, v5, v2, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    :cond_2
    return-void
.end method

.method public onStart()V
    .locals 2

    new-instance v0, Lcom/android/internal/widget/LockPatternUtils;

    iget-object v1, p0, Lcom/android/server/display/ScreenEffectService;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/android/internal/widget/LockPatternUtils;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/server/display/ScreenEffectService;->mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

    invoke-static {}, Lmiui/hardware/display/DisplayFeatureManager;->getInstance()Lmiui/hardware/display/DisplayFeatureManager;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/display/ScreenEffectService;->mDisplayFeatureManager:Lmiui/hardware/display/DisplayFeatureManager;

    new-instance v0, Lcom/android/server/display/ScreenEffectService$LocalService;

    invoke-direct {v0, p0}, Lcom/android/server/display/ScreenEffectService$LocalService;-><init>(Lcom/android/server/display/ScreenEffectService;)V

    sput-object v0, Lcom/android/server/display/ScreenEffectService;->sScreenEffectManager:Lcom/android/server/display/ScreenEffectService$LocalService;

    return-void
.end method
