.class Lcom/android/server/display/ScreenEffectService$SettingsObserver;
.super Landroid/database/ContentObserver;
.source "ScreenEffectService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/display/ScreenEffectService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SettingsObserver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/display/ScreenEffectService;


# direct methods
.method public constructor <init>(Lcom/android/server/display/ScreenEffectService;)V
    .locals 1

    iput-object p1, p0, Lcom/android/server/display/ScreenEffectService$SettingsObserver;->this$0:Lcom/android/server/display/ScreenEffectService;

    invoke-static {p1}, Lcom/android/server/display/ScreenEffectService;->-get3(Lcom/android/server/display/ScreenEffectService;)Landroid/os/Handler;

    move-result-object v0

    invoke-direct {p0, v0}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    return-void
.end method


# virtual methods
.method public onChange(ZLandroid/net/Uri;)V
    .locals 8

    const/4 v5, 0x2

    const/4 v2, 0x1

    const/4 v3, 0x0

    const/4 v7, -0x2

    invoke-virtual {p2}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v4, "screen_paper_mode_enabled"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    iget-object v4, p0, Lcom/android/server/display/ScreenEffectService$SettingsObserver;->this$0:Lcom/android/server/display/ScreenEffectService;

    iget-object v5, p0, Lcom/android/server/display/ScreenEffectService$SettingsObserver;->this$0:Lcom/android/server/display/ScreenEffectService;

    invoke-static {v5}, Lcom/android/server/display/ScreenEffectService;->-get1(Lcom/android/server/display/ScreenEffectService;)Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    const-string/jumbo v6, "screen_paper_mode_enabled"

    invoke-static {v5, v6, v3, v7}, Landroid/provider/Settings$System;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v5

    if-eqz v5, :cond_8

    :goto_0
    invoke-static {v4, v2}, Lcom/android/server/display/ScreenEffectService;->-set6(Lcom/android/server/display/ScreenEffectService;Z)Z

    iget-object v2, p0, Lcom/android/server/display/ScreenEffectService$SettingsObserver;->this$0:Lcom/android/server/display/ScreenEffectService;

    iget-object v3, p0, Lcom/android/server/display/ScreenEffectService$SettingsObserver;->this$0:Lcom/android/server/display/ScreenEffectService;

    invoke-static {v3}, Lcom/android/server/display/ScreenEffectService;->-get9(Lcom/android/server/display/ScreenEffectService;)Z

    move-result v3

    invoke-static {v2, v3}, Lcom/android/server/display/ScreenEffectService;->-wrap3(Lcom/android/server/display/ScreenEffectService;Z)V

    :cond_0
    :goto_1
    return-void

    :cond_1
    const-string/jumbo v4, "screen_paper_mode_level"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    iget-object v3, p0, Lcom/android/server/display/ScreenEffectService$SettingsObserver;->this$0:Lcom/android/server/display/ScreenEffectService;

    iget-object v4, p0, Lcom/android/server/display/ScreenEffectService$SettingsObserver;->this$0:Lcom/android/server/display/ScreenEffectService;

    invoke-static {v4}, Lcom/android/server/display/ScreenEffectService;->-get1(Lcom/android/server/display/ScreenEffectService;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    const-string/jumbo v5, "screen_paper_mode_level"

    sget v6, Landroid/provider/MiuiSettings$ScreenEffect;->DEFAULT_PAPER_MODE_LEVEL:I

    invoke-static {v4, v5, v6, v7}, Landroid/provider/Settings$System;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v4

    invoke-static {v3, v4}, Lcom/android/server/display/ScreenEffectService;->-set7(Lcom/android/server/display/ScreenEffectService;I)I

    iget-object v3, p0, Lcom/android/server/display/ScreenEffectService$SettingsObserver;->this$0:Lcom/android/server/display/ScreenEffectService;

    invoke-static {v3}, Lcom/android/server/display/ScreenEffectService;->-get9(Lcom/android/server/display/ScreenEffectService;)Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/android/server/display/ScreenEffectService$SettingsObserver;->this$0:Lcom/android/server/display/ScreenEffectService;

    invoke-static {v3, v2}, Lcom/android/server/display/ScreenEffectService;->-wrap3(Lcom/android/server/display/ScreenEffectService;Z)V

    goto :goto_1

    :cond_2
    const-string/jumbo v4, "screen_optimize_mode"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    iget-object v2, p0, Lcom/android/server/display/ScreenEffectService$SettingsObserver;->this$0:Lcom/android/server/display/ScreenEffectService;

    iget-object v3, p0, Lcom/android/server/display/ScreenEffectService$SettingsObserver;->this$0:Lcom/android/server/display/ScreenEffectService;

    invoke-static {v3}, Lcom/android/server/display/ScreenEffectService;->-get1(Lcom/android/server/display/ScreenEffectService;)Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string/jumbo v4, "screen_optimize_mode"

    sget v5, Landroid/provider/MiuiSettings$ScreenEffect;->DEFAULT_SCREEN_OPTIMIZE_MODE:I

    invoke-static {v3, v4, v5, v7}, Landroid/provider/Settings$System;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v3

    invoke-static {v2, v3}, Lcom/android/server/display/ScreenEffectService;->-set9(Lcom/android/server/display/ScreenEffectService;I)I

    iget-object v2, p0, Lcom/android/server/display/ScreenEffectService$SettingsObserver;->this$0:Lcom/android/server/display/ScreenEffectService;

    invoke-static {v2}, Lcom/android/server/display/ScreenEffectService;->-wrap4(Lcom/android/server/display/ScreenEffectService;)V

    goto :goto_1

    :cond_3
    const-string/jumbo v4, "screen_color_level"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    iget-object v2, p0, Lcom/android/server/display/ScreenEffectService$SettingsObserver;->this$0:Lcom/android/server/display/ScreenEffectService;

    iget-object v3, p0, Lcom/android/server/display/ScreenEffectService$SettingsObserver;->this$0:Lcom/android/server/display/ScreenEffectService;

    invoke-static {v3}, Lcom/android/server/display/ScreenEffectService;->-get1(Lcom/android/server/display/ScreenEffectService;)Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string/jumbo v4, "screen_color_level"

    invoke-static {v3, v4, v5, v7}, Landroid/provider/Settings$System;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v3

    invoke-static {v2, v3}, Lcom/android/server/display/ScreenEffectService;->-set8(Lcom/android/server/display/ScreenEffectService;I)I

    iget-object v2, p0, Lcom/android/server/display/ScreenEffectService$SettingsObserver;->this$0:Lcom/android/server/display/ScreenEffectService;

    invoke-static {v2}, Lcom/android/server/display/ScreenEffectService;->-wrap4(Lcom/android/server/display/ScreenEffectService;)V

    goto :goto_1

    :cond_4
    const-string/jumbo v4, "screen_monochrome_mode_enabled"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    iget-object v4, p0, Lcom/android/server/display/ScreenEffectService$SettingsObserver;->this$0:Lcom/android/server/display/ScreenEffectService;

    iget-object v5, p0, Lcom/android/server/display/ScreenEffectService$SettingsObserver;->this$0:Lcom/android/server/display/ScreenEffectService;

    invoke-static {v5}, Lcom/android/server/display/ScreenEffectService;->-get1(Lcom/android/server/display/ScreenEffectService;)Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    const-string/jumbo v6, "screen_monochrome_mode_enabled"

    invoke-static {v5, v6, v3, v7}, Landroid/provider/Settings$System;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v5

    if-eqz v5, :cond_9

    :goto_2
    invoke-static {v4, v2}, Lcom/android/server/display/ScreenEffectService;->-set1(Lcom/android/server/display/ScreenEffectService;Z)Z

    iget-object v2, p0, Lcom/android/server/display/ScreenEffectService$SettingsObserver;->this$0:Lcom/android/server/display/ScreenEffectService;

    invoke-static {v2}, Lcom/android/server/display/ScreenEffectService;->-wrap1(Lcom/android/server/display/ScreenEffectService;)V

    goto/16 :goto_1

    :cond_5
    const-string/jumbo v2, "screen_monochrome_mode"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    iget-object v2, p0, Lcom/android/server/display/ScreenEffectService$SettingsObserver;->this$0:Lcom/android/server/display/ScreenEffectService;

    iget-object v3, p0, Lcom/android/server/display/ScreenEffectService$SettingsObserver;->this$0:Lcom/android/server/display/ScreenEffectService;

    invoke-static {v3}, Lcom/android/server/display/ScreenEffectService;->-get1(Lcom/android/server/display/ScreenEffectService;)Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string/jumbo v4, "screen_monochrome_mode"

    invoke-static {v3, v4, v5, v7}, Landroid/provider/Settings$System;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v3

    invoke-static {v2, v3}, Lcom/android/server/display/ScreenEffectService;->-set2(Lcom/android/server/display/ScreenEffectService;I)I

    iget-object v2, p0, Lcom/android/server/display/ScreenEffectService$SettingsObserver;->this$0:Lcom/android/server/display/ScreenEffectService;

    invoke-static {v2}, Lcom/android/server/display/ScreenEffectService;->-wrap1(Lcom/android/server/display/ScreenEffectService;)V

    goto/16 :goto_1

    :cond_6
    const-string/jumbo v2, "screen_monochrome_mode_white_list"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_7

    iget-object v2, p0, Lcom/android/server/display/ScreenEffectService$SettingsObserver;->this$0:Lcom/android/server/display/ScreenEffectService;

    iget-object v3, p0, Lcom/android/server/display/ScreenEffectService$SettingsObserver;->this$0:Lcom/android/server/display/ScreenEffectService;

    invoke-static {v3}, Lcom/android/server/display/ScreenEffectService;->-get1(Lcom/android/server/display/ScreenEffectService;)Landroid/content/Context;

    move-result-object v3

    const-string/jumbo v4, "screen_monochrome_mode_white_list"

    invoke-static {v3, v4}, Landroid/provider/MiuiSettings$ScreenEffect;->getScreenModePkgList(Landroid/content/Context;Ljava/lang/String;)Ljava/util/HashMap;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/android/server/display/ScreenEffectService;->-set3(Lcom/android/server/display/ScreenEffectService;Ljava/util/HashMap;)Ljava/util/HashMap;

    goto/16 :goto_1

    :cond_7
    const-string/jumbo v2, "night_light_level"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/android/server/display/ScreenEffectService$SettingsObserver;->this$0:Lcom/android/server/display/ScreenEffectService;

    invoke-static {v2}, Lcom/android/server/display/ScreenEffectService;->-get1(Lcom/android/server/display/ScreenEffectService;)Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string/jumbo v3, "night_light_level"

    const/4 v4, -0x1

    invoke-static {v2, v3, v4, v7}, Landroid/provider/Settings$System;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v1

    sget-object v2, Lcom/android/server/display/ScreenEffectService;->sScreenEffectManager:Lcom/android/server/display/ScreenEffectService$LocalService;

    if-eqz v2, :cond_0

    sget-object v2, Lcom/android/server/display/ScreenEffectService;->sScreenEffectManager:Lcom/android/server/display/ScreenEffectService$LocalService;

    invoke-virtual {v2, v1}, Lcom/android/server/display/ScreenEffectService$LocalService;->setNightLight(I)V

    goto/16 :goto_1

    :cond_8
    move v2, v3

    goto/16 :goto_0

    :cond_9
    move v2, v3

    goto :goto_2
.end method
