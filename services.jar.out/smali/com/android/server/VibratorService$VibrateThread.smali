.class Lcom/android/server/VibratorService$VibrateThread;
.super Ljava/lang/Thread;
.source "VibratorService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/VibratorService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "VibrateThread"
.end annotation


# instance fields
.field private mForceStop:Z

.field private final mUid:I

.field private final mUsageHint:I

.field private final mWaveform:Landroid/os/VibrationEffect$Waveform;

.field final synthetic this$0:Lcom/android/server/VibratorService;


# direct methods
.method constructor <init>(Lcom/android/server/VibratorService;Landroid/os/VibrationEffect$Waveform;II)V
    .locals 2

    iput-object p1, p0, Lcom/android/server/VibratorService$VibrateThread;->this$0:Lcom/android/server/VibratorService;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    iput-object p2, p0, Lcom/android/server/VibratorService$VibrateThread;->mWaveform:Landroid/os/VibrationEffect$Waveform;

    iput p3, p0, Lcom/android/server/VibratorService$VibrateThread;->mUid:I

    iput p4, p0, Lcom/android/server/VibratorService$VibrateThread;->mUsageHint:I

    invoke-static {p1}, Lcom/android/server/VibratorService;->-get4(Lcom/android/server/VibratorService;)Landroid/os/WorkSource;

    move-result-object v0

    invoke-virtual {v0, p3}, Landroid/os/WorkSource;->set(I)V

    invoke-static {p1}, Lcom/android/server/VibratorService;->-get5(Lcom/android/server/VibratorService;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    invoke-static {p1}, Lcom/android/server/VibratorService;->-get4(Lcom/android/server/VibratorService;)Landroid/os/WorkSource;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/PowerManager$WakeLock;->setWorkSource(Landroid/os/WorkSource;)V

    return-void
.end method

.method private delayLocked(J)J
    .locals 11

    const-wide/16 v8, 0x0

    move-wide v2, p1

    cmp-long v5, p1, v8

    if-lez v5, :cond_2

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v6

    add-long v0, p1, v6

    :goto_0
    :try_start_0
    invoke-virtual {p0, v2, v3}, Lcom/android/server/VibratorService$VibrateThread;->wait(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_1
    iget-boolean v5, p0, Lcom/android/server/VibratorService$VibrateThread;->mForceStop:Z

    if-eqz v5, :cond_1

    :cond_0
    sub-long v6, p1, v2

    return-wide v6

    :catch_0
    move-exception v4

    goto :goto_1

    :cond_1
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v6

    sub-long v2, v0, v6

    cmp-long v5, v2, v8

    if-lez v5, :cond_0

    goto :goto_0

    :cond_2
    return-wide v8
.end method

.method private getTotalOnDuration([J[III)J
    .locals 6

    move v0, p3

    const-wide/16 v2, 0x0

    :cond_0
    aget v4, p2, v0

    if-eqz v4, :cond_2

    add-int/lit8 v1, v0, 0x1

    aget-wide v4, p1, v0

    add-long/2addr v2, v4

    array-length v4, p1

    if-lt v1, v4, :cond_3

    if-ltz p4, :cond_1

    move v0, p4

    :goto_0
    if-ne v0, p3, :cond_0

    const-wide/16 v4, 0x3e8

    return-wide v4

    :cond_1
    move v0, v1

    :cond_2
    return-wide v2

    :cond_3
    move v0, v1

    goto :goto_0
.end method


# virtual methods
.method public cancel()V
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/android/server/VibratorService$VibrateThread;->this$0:Lcom/android/server/VibratorService;

    invoke-static {v0}, Lcom/android/server/VibratorService;->-get3(Lcom/android/server/VibratorService;)Lcom/android/server/VibratorService$VibrateThread;

    move-result-object v0

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/android/server/VibratorService$VibrateThread;->mForceStop:Z

    iget-object v0, p0, Lcom/android/server/VibratorService$VibrateThread;->this$0:Lcom/android/server/VibratorService;

    invoke-static {v0}, Lcom/android/server/VibratorService;->-get3(Lcom/android/server/VibratorService;)Lcom/android/server/VibratorService$VibrateThread;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/server/VibratorService$VibrateThread;->notify()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public playWaveform()Z
    .locals 20

    monitor-enter p0

    :try_start_0
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/server/VibratorService$VibrateThread;->mWaveform:Landroid/os/VibrationEffect$Waveform;

    invoke-virtual {v3}, Landroid/os/VibrationEffect$Waveform;->getTimings()[J

    move-result-object v15

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/server/VibratorService$VibrateThread;->mWaveform:Landroid/os/VibrationEffect$Waveform;

    invoke-virtual {v3}, Landroid/os/VibrationEffect$Waveform;->getAmplitudes()[I

    move-result-object v2

    array-length v13, v15

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/server/VibratorService$VibrateThread;->mWaveform:Landroid/os/VibrationEffect$Waveform;

    invoke-virtual {v3}, Landroid/os/VibrationEffect$Waveform;->getRepeatIndex()I

    move-result v14

    const/4 v9, 0x0

    const-wide/16 v4, 0x0

    move v12, v9

    :goto_0
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/android/server/VibratorService$VibrateThread;->mForceStop:Z

    if-nez v3, :cond_5

    if-ge v12, v13, :cond_4

    aget v6, v2, v12

    add-int/lit8 v9, v12, 0x1

    aget-wide v10, v15, v12

    const-wide/16 v18, 0x0

    cmp-long v3, v10, v18

    if-gtz v3, :cond_0

    move v12, v9

    goto :goto_0

    :cond_0
    if-eqz v6, :cond_1

    const-wide/16 v18, 0x0

    cmp-long v3, v4, v18

    if-gtz v3, :cond_3

    add-int/lit8 v3, v9, -0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v15, v2, v3, v14}, Lcom/android/server/VibratorService$VibrateThread;->getTotalOnDuration([J[III)J

    move-result-wide v4

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/server/VibratorService$VibrateThread;->this$0:Lcom/android/server/VibratorService;

    move-object/from16 v0, p0

    iget v7, v0, Lcom/android/server/VibratorService$VibrateThread;->mUid:I

    move-object/from16 v0, p0

    iget v8, v0, Lcom/android/server/VibratorService$VibrateThread;->mUsageHint:I

    invoke-static/range {v3 .. v8}, Lcom/android/server/VibratorService;->-wrap1(Lcom/android/server/VibratorService;JIII)V

    :cond_1
    :goto_1
    move-object/from16 v0, p0

    invoke-direct {v0, v10, v11}, Lcom/android/server/VibratorService$VibrateThread;->delayLocked(J)J

    move-result-wide v16

    if-eqz v6, :cond_2

    sub-long v4, v4, v16

    :cond_2
    :goto_2
    move v12, v9

    goto :goto_0

    :cond_3
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/server/VibratorService$VibrateThread;->this$0:Lcom/android/server/VibratorService;

    invoke-static {v3, v6}, Lcom/android/server/VibratorService;->-wrap2(Lcom/android/server/VibratorService;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v3

    monitor-exit p0

    throw v3

    :cond_4
    if-gez v14, :cond_6

    :cond_5
    :try_start_1
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/android/server/VibratorService$VibrateThread;->mForceStop:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    xor-int/lit8 v3, v3, 0x1

    monitor-exit p0

    return v3

    :cond_6
    move v9, v14

    goto :goto_2
.end method

.method public run()V
    .locals 3

    const/4 v1, -0x8

    invoke-static {v1}, Landroid/os/Process;->setThreadPriority(I)V

    iget-object v1, p0, Lcom/android/server/VibratorService$VibrateThread;->this$0:Lcom/android/server/VibratorService;

    invoke-static {v1}, Lcom/android/server/VibratorService;->-get5(Lcom/android/server/VibratorService;)Landroid/os/PowerManager$WakeLock;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->acquire()V

    :try_start_0
    invoke-virtual {p0}, Lcom/android/server/VibratorService$VibrateThread;->playWaveform()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/android/server/VibratorService$VibrateThread;->this$0:Lcom/android/server/VibratorService;

    invoke-virtual {v1}, Lcom/android/server/VibratorService;->onVibrationFinished()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    iget-object v1, p0, Lcom/android/server/VibratorService$VibrateThread;->this$0:Lcom/android/server/VibratorService;

    invoke-static {v1}, Lcom/android/server/VibratorService;->-get5(Lcom/android/server/VibratorService;)Landroid/os/PowerManager$WakeLock;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->release()V

    return-void

    :catchall_0
    move-exception v1

    iget-object v2, p0, Lcom/android/server/VibratorService$VibrateThread;->this$0:Lcom/android/server/VibratorService;

    invoke-static {v2}, Lcom/android/server/VibratorService;->-get5(Lcom/android/server/VibratorService;)Landroid/os/PowerManager$WakeLock;

    move-result-object v2

    invoke-virtual {v2}, Landroid/os/PowerManager$WakeLock;->release()V

    throw v1
.end method
