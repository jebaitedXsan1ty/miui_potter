.class Lcom/android/server/am/ProcessRecordInjector;
.super Ljava/lang/Object;
.source "ProcessRecordInjector.java"


# static fields
.field private static final DEVICE:Ljava/lang/String;

.field private static final PROCESS_BUFFER_SIZE:I = 0x1e

.field private static final TAG:Ljava/lang/String; = "ProcessRecordInjector"

.field private static final sCachedProcessList:Ljava/util/List;
    .annotation build Lcom/android/internal/annotations/GuardedBy;
        value = "sLock"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lmiui/mqsas/sdk/event/KillProcessEvent;",
            ">;"
        }
    .end annotation
.end field

.field private static final sDeathProcessList:Landroid/util/SparseArray;
    .annotation build Lcom/android/internal/annotations/GuardedBy;
        value = "sLock"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Lmiui/mqsas/sdk/event/KillProcessEvent;",
            ">;"
        }
    .end annotation
.end field

.field private static final sKillingProcessList:Landroid/util/SparseArray;
    .annotation build Lcom/android/internal/annotations/GuardedBy;
        value = "sLock"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Lmiui/mqsas/sdk/event/KillProcessEvent;",
            ">;"
        }
    .end annotation
.end field

.field private static final sLock:Ljava/lang/Object;

.field private static sPidsSelfLocked:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Lcom/android/server/am/ProcessRecord;",
            ">;"
        }
    .end annotation
.end field

.field private static volatile sProcessManagerInternal:Lmiui/process/ProcessManagerInternal;

.field private static sSystemBootCompleted:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const/4 v1, 0x0

    sput-object v1, Lcom/android/server/am/ProcessRecordInjector;->sPidsSelfLocked:Landroid/util/SparseArray;

    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    sput-object v0, Lcom/android/server/am/ProcessRecordInjector;->sKillingProcessList:Landroid/util/SparseArray;

    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    sput-object v0, Lcom/android/server/am/ProcessRecordInjector;->sDeathProcessList:Landroid/util/SparseArray;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/android/server/am/ProcessRecordInjector;->sCachedProcessList:Ljava/util/List;

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/android/server/am/ProcessRecordInjector;->sLock:Ljava/lang/Object;

    sput-object v1, Lcom/android/server/am/ProcessRecordInjector;->sProcessManagerInternal:Lmiui/process/ProcessManagerInternal;

    const-string/jumbo v0, "ro.product.device"

    const-string/jumbo v1, "UNKNOWN"

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/server/am/ProcessRecordInjector;->DEVICE:Ljava/lang/String;

    return-void
.end method

.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static getProcessManagerInternal()Lmiui/process/ProcessManagerInternal;
    .locals 2

    sget-object v0, Lcom/android/server/am/ProcessRecordInjector;->sProcessManagerInternal:Lmiui/process/ProcessManagerInternal;

    if-nez v0, :cond_1

    const-class v1, Lcom/android/server/am/ProcessRecordInjector;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/android/server/am/ProcessRecordInjector;->sProcessManagerInternal:Lmiui/process/ProcessManagerInternal;

    if-nez v0, :cond_0

    const-class v0, Lmiui/process/ProcessManagerInternal;

    invoke-static {v0}, Lcom/android/server/LocalServices;->getService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmiui/process/ProcessManagerInternal;

    sput-object v0, Lcom/android/server/am/ProcessRecordInjector;->sProcessManagerInternal:Lmiui/process/ProcessManagerInternal;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit v1

    :cond_1
    sget-object v0, Lcom/android/server/am/ProcessRecordInjector;->sProcessManagerInternal:Lmiui/process/ProcessManagerInternal;

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static getProcessNameByPid(I)Ljava/lang/String;
    .locals 5

    const/4 v4, 0x0

    sget-object v2, Lcom/android/server/am/ProcessRecordInjector;->sPidsSelfLocked:Landroid/util/SparseArray;

    if-nez v2, :cond_0

    const-string/jumbo v2, "activity"

    invoke-static {v2}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    check-cast v0, Lcom/android/server/am/ActivityManagerService;

    iget-object v2, v0, Lcom/android/server/am/ActivityManagerService;->mPidsSelfLocked:Landroid/util/SparseArray;

    sput-object v2, Lcom/android/server/am/ProcessRecordInjector;->sPidsSelfLocked:Landroid/util/SparseArray;

    :cond_0
    sget-object v3, Lcom/android/server/am/ProcessRecordInjector;->sPidsSelfLocked:Landroid/util/SparseArray;

    monitor-enter v3

    :try_start_0
    sget-object v2, Lcom/android/server/am/ProcessRecordInjector;->sPidsSelfLocked:Landroid/util/SparseArray;

    invoke-virtual {v2, p0}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/server/am/ProcessRecord;

    if-eqz v1, :cond_1

    iget-object v2, v1, Lcom/android/server/am/ProcessRecord;->processName:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v3

    return-object v2

    :cond_1
    monitor-exit v3

    return-object v4

    :catchall_0
    move-exception v2

    monitor-exit v3

    throw v2
.end method

.method public static isAppInterestingToUser(I)Z
    .locals 4

    sget-object v2, Lcom/android/server/am/ProcessRecordInjector;->sPidsSelfLocked:Landroid/util/SparseArray;

    if-nez v2, :cond_0

    const-string/jumbo v2, "activity"

    invoke-static {v2}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    check-cast v0, Lcom/android/server/am/ActivityManagerService;

    iget-object v2, v0, Lcom/android/server/am/ActivityManagerService;->mPidsSelfLocked:Landroid/util/SparseArray;

    sput-object v2, Lcom/android/server/am/ProcessRecordInjector;->sPidsSelfLocked:Landroid/util/SparseArray;

    :cond_0
    sget-object v3, Lcom/android/server/am/ProcessRecordInjector;->sPidsSelfLocked:Landroid/util/SparseArray;

    monitor-enter v3

    :try_start_0
    sget-object v2, Lcom/android/server/am/ProcessRecordInjector;->sPidsSelfLocked:Landroid/util/SparseArray;

    invoke-virtual {v2, p0}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/server/am/ProcessRecord;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/android/server/am/ProcessRecord;->isInterestingToUserLocked()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    monitor-exit v3

    return v2

    :cond_1
    monitor-exit v3

    const/4 v2, 0x0

    return v2

    :catchall_0
    move-exception v2

    monitor-exit v3

    throw v2
.end method

.method public static isSystemBootCompleted()Z
    .locals 2

    sget-boolean v0, Lcom/android/server/am/ProcessRecordInjector;->sSystemBootCompleted:Z

    if-nez v0, :cond_0

    const-string/jumbo v0, "1"

    const-string/jumbo v1, "sys.boot_completed"

    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    sput-boolean v0, Lcom/android/server/am/ProcessRecordInjector;->sSystemBootCompleted:Z

    :cond_0
    sget-boolean v0, Lcom/android/server/am/ProcessRecordInjector;->sSystemBootCompleted:Z

    return v0
.end method

.method public static reportBinderDied(Lcom/android/server/am/ProcessRecord;)V
    .locals 4

    if-eqz p0, :cond_0

    iget-object v0, p0, Lcom/android/server/am/ProcessRecord;->processName:Ljava/lang/String;

    iget v1, p0, Lcom/android/server/am/ProcessRecord;->pid:I

    iget v2, p0, Lcom/android/server/am/ProcessRecord;->setAdj:I

    invoke-virtual {p0}, Lcom/android/server/am/ProcessRecord;->isInterestingToUserLocked()Z

    move-result v3

    invoke-static {v0, v1, v2, v3}, Lcom/android/server/am/ProcessRecordInjector;->reportBinderDied(Ljava/lang/String;IIZ)V

    :cond_0
    return-void
.end method

.method public static reportBinderDied(Ljava/lang/String;IIZ)V
    .locals 6

    sget-object v2, Lcom/android/server/am/ProcessRecordInjector;->sLock:Ljava/lang/Object;

    monitor-enter v2

    :try_start_0
    sget-object v1, Lcom/android/server/am/ProcessRecordInjector;->sKillingProcessList:Landroid/util/SparseArray;

    invoke-virtual {v1, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmiui/mqsas/sdk/event/KillProcessEvent;

    if-nez v0, :cond_0

    new-instance v0, Lmiui/mqsas/sdk/event/KillProcessEvent;

    invoke-direct {v0}, Lmiui/mqsas/sdk/event/KillProcessEvent;-><init>()V

    const-string/jumbo v1, "lowmemorykiller"

    invoke-virtual {v0, v1}, Lmiui/mqsas/sdk/event/KillProcessEvent;->setPolicy(Ljava/lang/String;)V

    invoke-virtual {v0, p0}, Lmiui/mqsas/sdk/event/KillProcessEvent;->setKilledProc(Ljava/lang/String;)V

    invoke-virtual {v0, p2}, Lmiui/mqsas/sdk/event/KillProcessEvent;->setProcAdj(I)V

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v4

    invoke-virtual {v0, v4, v5}, Lmiui/mqsas/sdk/event/KillProcessEvent;->setKilledTime(J)V

    invoke-virtual {v0, p3}, Lmiui/mqsas/sdk/event/KillProcessEvent;->setInterestingToUser(Z)V

    :cond_0
    sget-object v1, Lcom/android/server/am/ProcessRecordInjector;->sDeathProcessList:Landroid/util/SparseArray;

    invoke-virtual {v1, p1, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v2

    invoke-static {p0}, Lcom/android/server/am/FindDeviceAliveChecker;->postCheckFindDeviceAliveDelayed(Ljava/lang/String;)V

    return-void

    :catchall_0
    move-exception v1

    monitor-exit v2

    throw v1
.end method

.method public static reportCleanUpAppRecord(Lcom/android/server/am/ProcessRecord;)V
    .locals 5

    if-eqz p0, :cond_0

    iget-object v0, p0, Lcom/android/server/am/ProcessRecord;->processName:Ljava/lang/String;

    iget v1, p0, Lcom/android/server/am/ProcessRecord;->pid:I

    iget v2, p0, Lcom/android/server/am/ProcessRecord;->setAdj:I

    iget v3, p0, Lcom/android/server/am/ProcessRecord;->setProcState:I

    invoke-virtual {p0}, Lcom/android/server/am/ProcessRecord;->isInterestingToUserLocked()Z

    move-result v4

    invoke-static {v0, v1, v2, v3, v4}, Lcom/android/server/am/ProcessRecordInjector;->reportCleanUpAppRecord(Ljava/lang/String;IIIZ)V

    :cond_0
    return-void
.end method

.method public static reportCleanUpAppRecord(Ljava/lang/String;IIIZ)V
    .locals 8

    sget-object v4, Lcom/android/server/am/ProcessRecordInjector;->sLock:Ljava/lang/Object;

    monitor-enter v4

    :try_start_0
    sget-object v3, Lcom/android/server/am/ProcessRecordInjector;->sKillingProcessList:Landroid/util/SparseArray;

    invoke-virtual {v3, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lmiui/mqsas/sdk/event/KillProcessEvent;

    sget-object v3, Lcom/android/server/am/ProcessRecordInjector;->sDeathProcessList:Landroid/util/SparseArray;

    invoke-virtual {v3, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmiui/mqsas/sdk/event/KillProcessEvent;

    if-eqz v2, :cond_2

    sget-object v3, Lcom/android/server/am/ProcessRecordInjector;->sKillingProcessList:Landroid/util/SparseArray;

    invoke-virtual {v3, p1}, Landroid/util/SparseArray;->remove(I)V

    sget-object v3, Lcom/android/server/am/ProcessRecordInjector;->sCachedProcessList:Ljava/util/List;

    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :goto_0
    if-eqz v0, :cond_0

    sget-object v3, Lcom/android/server/am/ProcessRecordInjector;->sDeathProcessList:Landroid/util/SparseArray;

    invoke-virtual {v3, p1}, Landroid/util/SparseArray;->remove(I)V

    :cond_0
    sget-object v3, Lcom/android/server/am/ProcessRecordInjector;->sCachedProcessList:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    const/16 v5, 0x1e

    if-lt v3, v5, :cond_1

    invoke-static {}, Lcom/android/server/am/ProcessRecordInjector;->isSystemBootCompleted()Z

    move-result v3

    if-eqz v3, :cond_1

    const-string/jumbo v3, "ProcessRecordInjector"

    const-string/jumbo v5, "Begin to report kill process events..."

    invoke-static {v3, v5}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    sget-object v3, Lcom/android/server/am/ProcessRecordInjector;->sCachedProcessList:Ljava/util/List;

    invoke-interface {v1, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    sget-object v3, Lcom/android/server/am/ProcessRecordInjector;->sCachedProcessList:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->clear()V

    invoke-static {v1}, Lcom/android/server/am/ProcessRecordInjector;->reportKillProcessEvents(Ljava/util/List;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_1
    monitor-exit v4

    return-void

    :cond_2
    if-nez v0, :cond_3

    :try_start_1
    new-instance v0, Lmiui/mqsas/sdk/event/KillProcessEvent;

    invoke-direct {v0}, Lmiui/mqsas/sdk/event/KillProcessEvent;-><init>()V

    const-string/jumbo v3, "exception"

    invoke-virtual {v0, v3}, Lmiui/mqsas/sdk/event/KillProcessEvent;->setPolicy(Ljava/lang/String;)V

    invoke-virtual {v0, p0}, Lmiui/mqsas/sdk/event/KillProcessEvent;->setKilledProc(Ljava/lang/String;)V

    invoke-virtual {v0, p2}, Lmiui/mqsas/sdk/event/KillProcessEvent;->setProcAdj(I)V

    invoke-virtual {v0, p3}, Lmiui/mqsas/sdk/event/KillProcessEvent;->setProcState(I)V

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v6

    invoke-virtual {v0, v6, v7}, Lmiui/mqsas/sdk/event/KillProcessEvent;->setKilledTime(J)V

    invoke-virtual {v0, p4}, Lmiui/mqsas/sdk/event/KillProcessEvent;->setInterestingToUser(Z)V

    :cond_3
    sget-object v3, Lcom/android/server/am/ProcessRecordInjector;->sCachedProcessList:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v3

    monitor-exit v4

    throw v3
.end method

.method public static reportKillProcessEvent(IILjava/lang/String;)V
    .locals 6

    const/4 v0, 0x0

    const/4 v5, 0x0

    if-ne p0, p1, :cond_0

    const-string/jumbo p2, "killself"

    :goto_0
    invoke-static {p1}, Lcom/android/server/am/ProcessRecordInjector;->getProcessNameByPid(I)Ljava/lang/String;

    move-result-object v0

    const/4 v2, -0x1

    const v3, 0x7fffffff

    move v1, p1

    move-object v4, p2

    invoke-static/range {v0 .. v5}, Lcom/android/server/am/ProcessRecordInjector;->reportKillProcessEvent(Ljava/lang/String;IIILjava/lang/String;Z)V

    return-void

    :cond_0
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-static {p0}, Lcom/android/server/am/ProcessRecordInjector;->getProcessNameByPid(I)Ljava/lang/String;

    move-result-object p2

    :cond_1
    invoke-static {p1}, Lcom/android/server/am/ProcessRecordInjector;->isAppInterestingToUser(I)Z

    move-result v5

    goto :goto_0
.end method

.method public static reportKillProcessEvent(Lcom/android/server/am/ProcessRecord;Ljava/lang/String;)V
    .locals 6

    if-eqz p0, :cond_0

    iget-object v0, p0, Lcom/android/server/am/ProcessRecord;->processName:Ljava/lang/String;

    iget v1, p0, Lcom/android/server/am/ProcessRecord;->pid:I

    iget v2, p0, Lcom/android/server/am/ProcessRecord;->setProcState:I

    iget v3, p0, Lcom/android/server/am/ProcessRecord;->setAdj:I

    invoke-virtual {p0}, Lcom/android/server/am/ProcessRecord;->isInterestingToUserLocked()Z

    move-result v5

    move-object v4, p1

    invoke-static/range {v0 .. v5}, Lcom/android/server/am/ProcessRecordInjector;->reportKillProcessEvent(Ljava/lang/String;IIILjava/lang/String;Z)V

    :cond_0
    return-void
.end method

.method public static reportKillProcessEvent(Ljava/lang/String;IIILjava/lang/String;Z)V
    .locals 6

    sget-object v2, Lcom/android/server/am/ProcessRecordInjector;->sLock:Ljava/lang/Object;

    monitor-enter v2

    :try_start_0
    new-instance v0, Lmiui/mqsas/sdk/event/KillProcessEvent;

    invoke-direct {v0}, Lmiui/mqsas/sdk/event/KillProcessEvent;-><init>()V

    invoke-virtual {v0, p4}, Lmiui/mqsas/sdk/event/KillProcessEvent;->setKilledReason(Ljava/lang/String;)V

    invoke-virtual {v0, p0}, Lmiui/mqsas/sdk/event/KillProcessEvent;->setKilledProc(Ljava/lang/String;)V

    invoke-virtual {v0, p2}, Lmiui/mqsas/sdk/event/KillProcessEvent;->setProcState(I)V

    invoke-virtual {v0, p3}, Lmiui/mqsas/sdk/event/KillProcessEvent;->setProcAdj(I)V

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v4

    invoke-virtual {v0, v4, v5}, Lmiui/mqsas/sdk/event/KillProcessEvent;->setKilledTime(J)V

    invoke-virtual {v0, p5}, Lmiui/mqsas/sdk/event/KillProcessEvent;->setInterestingToUser(Z)V

    sget-object v1, Lcom/android/server/am/ProcessRecordInjector;->sKillingProcessList:Landroid/util/SparseArray;

    invoke-virtual {v1, p1, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v2

    invoke-static {}, Lcom/android/server/am/ProcessRecordInjector;->getProcessManagerInternal()Lmiui/process/ProcessManagerInternal;

    move-result-object v1

    invoke-virtual {v1, p4, p0, p1}, Lmiui/process/ProcessManagerInternal;->recordKillProcessEventIfNeeded(Ljava/lang/String;Ljava/lang/String;I)V

    return-void

    :catchall_0
    move-exception v1

    monitor-exit v2

    throw v1
.end method

.method private static reportKillProcessEvents(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lmiui/mqsas/sdk/event/KillProcessEvent;",
            ">;)V"
        }
    .end annotation

    new-instance v0, Landroid/content/pm/ParceledListSlice;

    invoke-direct {v0, p0}, Landroid/content/pm/ParceledListSlice;-><init>(Ljava/util/List;)V

    invoke-static {}, Lcom/android/internal/os/BackgroundThread;->getHandler()Landroid/os/Handler;

    move-result-object v1

    new-instance v2, Lcom/android/server/am/ProcessRecordInjector$1;

    invoke-direct {v2, v0}, Lcom/android/server/am/ProcessRecordInjector$1;-><init>(Landroid/content/pm/ParceledListSlice;)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public static updateProcessForegroundLocked(Lcom/android/server/am/ProcessRecord;)V
    .locals 2

    invoke-static {}, Lcom/android/server/am/ProcessRecordInjector;->getProcessManagerInternal()Lmiui/process/ProcessManagerInternal;

    move-result-object v0

    iget v1, p0, Lcom/android/server/am/ProcessRecord;->pid:I

    invoke-virtual {v0, v1}, Lmiui/process/ProcessManagerInternal;->updateProcessForegroundLocked(I)V

    return-void
.end method
