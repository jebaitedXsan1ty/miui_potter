.class final Lcom/android/server/am/BroadcastQueueInjector$2;
.super Ljava/lang/Object;
.source "BroadcastQueueInjector.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/server/am/BroadcastQueueInjector;->checkAbnormalBroadcastInQueueLocked(Lcom/android/server/am/ActivityManagerService;Lcom/android/server/am/BroadcastQueue;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$ams:Lcom/android/server/am/ActivityManagerService;

.field final synthetic val$r:Lcom/android/server/am/BroadcastQueueInjector$AbnormalBroadcastRecord;


# direct methods
.method constructor <init>(Lcom/android/server/am/ActivityManagerService;Lcom/android/server/am/BroadcastQueueInjector$AbnormalBroadcastRecord;)V
    .locals 0

    iput-object p1, p0, Lcom/android/server/am/BroadcastQueueInjector$2;->val$ams:Lcom/android/server/am/ActivityManagerService;

    iput-object p2, p0, Lcom/android/server/am/BroadcastQueueInjector$2;->val$r:Lcom/android/server/am/BroadcastQueueInjector$AbnormalBroadcastRecord;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    iget-object v1, p0, Lcom/android/server/am/BroadcastQueueInjector$2;->val$ams:Lcom/android/server/am/ActivityManagerService;

    monitor-enter v1

    :try_start_0
    invoke-static {}, Lcom/android/server/am/ActivityManagerService;->boostPriorityForLockedSection()V

    const-string/jumbo v0, "BroadcastQueueInjector"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "detect abnormal ordered broadcast, force-stop:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/server/am/BroadcastQueueInjector$2;->val$r:Lcom/android/server/am/BroadcastQueueInjector$AbnormalBroadcastRecord;

    iget-object v3, v3, Lcom/android/server/am/BroadcastQueueInjector$AbnormalBroadcastRecord;->callerPackage:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " userId:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/server/am/BroadcastQueueInjector$2;->val$r:Lcom/android/server/am/BroadcastQueueInjector$AbnormalBroadcastRecord;

    iget v3, v3, Lcom/android/server/am/BroadcastQueueInjector$AbnormalBroadcastRecord;->userId:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/server/am/BroadcastQueueInjector$2;->val$ams:Lcom/android/server/am/ActivityManagerService;

    iget-object v2, p0, Lcom/android/server/am/BroadcastQueueInjector$2;->val$r:Lcom/android/server/am/BroadcastQueueInjector$AbnormalBroadcastRecord;

    iget-object v2, v2, Lcom/android/server/am/BroadcastQueueInjector$AbnormalBroadcastRecord;->callerPackage:Ljava/lang/String;

    iget-object v3, p0, Lcom/android/server/am/BroadcastQueueInjector$2;->val$r:Lcom/android/server/am/BroadcastQueueInjector$AbnormalBroadcastRecord;

    iget v3, v3, Lcom/android/server/am/BroadcastQueueInjector$AbnormalBroadcastRecord;->userId:I

    const-string/jumbo v4, "abnormal ordered broadcast"

    invoke-virtual {v0, v2, v3, v4}, Lcom/android/server/am/ActivityManagerService;->forceStopPackage(Ljava/lang/String;ILjava/lang/String;)V

    invoke-static {}, Lcom/android/server/am/BroadcastQueueInjector;->-get0()Ljava/util/concurrent/atomic/AtomicBoolean;

    move-result-object v0

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    invoke-static {}, Lcom/android/server/am/ActivityManagerService;->resetPriorityAfterLockedSection()V

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    invoke-static {}, Lcom/android/server/am/ActivityManagerService;->resetPriorityAfterLockedSection()V

    throw v0
.end method
