.class public Lcom/android/server/policy/MiuiPhoneWindowManager;
.super Lcom/android/server/policy/BaseMiuiPhoneWindowManager;
.source "MiuiPhoneWindowManager.java"


# static fields
.field private static final ACTION_NOT_PASS_TO_USER:I = 0x0

.field private static final ACTION_PASS_TO_USER:I = 0x1

.field private static final FINGERPRINT_NAV_ACTION_DEFAULT:I = -0x1

.field private static final FINGERPRINT_NAV_ACTION_HOME:I = 0x1

.field private static final FINGERPRINT_NAV_ACTION_NONE:I = 0x0

.field protected static final NAV_BAR_BOTTOM:I = 0x0

.field protected static final NAV_BAR_LEFT:I = 0x2

.field protected static final NAV_BAR_RIGHT:I = 0x1


# instance fields
.field private mAdjust:Z

.field private mDisplayHeight:I

.field private mDisplayRotation:I

.field private mDisplayWidth:I

.field private mEnableNotchConfig:Z

.field private mFpNavCenterActionChooseDialog:Lmiui/app/AlertDialog;

.field private mKeyguard:Landroid/view/WindowManagerPolicy$WindowState;

.field private mKeyguardAdded:Z

.field private mNeedAdjust:Z

.field private mPackage:Ljava/lang/String;

.field private mWindowStatusBinder:Landroid/os/Binder;


# direct methods
.method static synthetic -get0(Lcom/android/server/policy/MiuiPhoneWindowManager;)Lmiui/app/AlertDialog;
    .locals 1

    iget-object v0, p0, Lcom/android/server/policy/MiuiPhoneWindowManager;->mFpNavCenterActionChooseDialog:Lmiui/app/AlertDialog;

    return-object v0
.end method

.method static synthetic -get1(Lcom/android/server/policy/MiuiPhoneWindowManager;)Landroid/os/Binder;
    .locals 1

    iget-object v0, p0, Lcom/android/server/policy/MiuiPhoneWindowManager;->mWindowStatusBinder:Landroid/os/Binder;

    return-object v0
.end method

.method static synthetic -set0(Lcom/android/server/policy/MiuiPhoneWindowManager;Lmiui/app/AlertDialog;)Lmiui/app/AlertDialog;
    .locals 0

    iput-object p1, p0, Lcom/android/server/policy/MiuiPhoneWindowManager;->mFpNavCenterActionChooseDialog:Lmiui/app/AlertDialog;

    return-object p1
.end method

.method static synthetic -wrap0(Lcom/android/server/policy/MiuiPhoneWindowManager;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/policy/MiuiPhoneWindowManager;->bringUpActionChooseDlg()V

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;-><init>()V

    iput-object v1, p0, Lcom/android/server/policy/MiuiPhoneWindowManager;->mKeyguard:Landroid/view/WindowManagerPolicy$WindowState;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/server/policy/MiuiPhoneWindowManager;->mKeyguardAdded:Z

    new-instance v0, Landroid/os/Binder;

    invoke-direct {v0}, Landroid/os/Binder;-><init>()V

    iput-object v0, p0, Lcom/android/server/policy/MiuiPhoneWindowManager;->mWindowStatusBinder:Landroid/os/Binder;

    iput-object v1, p0, Lcom/android/server/policy/MiuiPhoneWindowManager;->mFpNavCenterActionChooseDialog:Lmiui/app/AlertDialog;

    return-void
.end method

.method private bringUpActionChooseDlg()V
    .locals 4

    iget-object v2, p0, Lcom/android/server/policy/MiuiPhoneWindowManager;->mFpNavCenterActionChooseDialog:Lmiui/app/AlertDialog;

    if-eqz v2, :cond_0

    return-void

    :cond_0
    new-instance v0, Lcom/android/server/policy/MiuiPhoneWindowManager$2;

    invoke-direct {v0, p0}, Lcom/android/server/policy/MiuiPhoneWindowManager$2;-><init>(Lcom/android/server/policy/MiuiPhoneWindowManager;)V

    new-instance v2, Lmiui/app/AlertDialog$Builder;

    iget-object v3, p0, Lcom/android/server/policy/MiuiPhoneWindowManager;->mContext:Landroid/content/Context;

    invoke-direct {v2, v3}, Lmiui/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v3, 0x110800c5

    invoke-virtual {v2, v3}, Lmiui/app/AlertDialog$Builder;->setTitle(I)Lmiui/app/AlertDialog$Builder;

    move-result-object v2

    const v3, 0x110800c6

    invoke-virtual {v2, v3}, Lmiui/app/AlertDialog$Builder;->setMessage(I)Lmiui/app/AlertDialog$Builder;

    move-result-object v2

    const v3, 0x110800c7

    invoke-virtual {v2, v3, v0}, Lmiui/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Lmiui/app/AlertDialog$Builder;

    move-result-object v2

    const v3, 0x110800c8

    invoke-virtual {v2, v3, v0}, Lmiui/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Lmiui/app/AlertDialog$Builder;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lmiui/app/AlertDialog$Builder;->setCancelable(Z)Lmiui/app/AlertDialog$Builder;

    move-result-object v2

    invoke-virtual {v2}, Lmiui/app/AlertDialog$Builder;->create()Lmiui/app/AlertDialog;

    move-result-object v2

    iput-object v2, p0, Lcom/android/server/policy/MiuiPhoneWindowManager;->mFpNavCenterActionChooseDialog:Lmiui/app/AlertDialog;

    iget-object v2, p0, Lcom/android/server/policy/MiuiPhoneWindowManager;->mFpNavCenterActionChooseDialog:Lmiui/app/AlertDialog;

    invoke-virtual {v2}, Lmiui/app/AlertDialog;->getWindow()Landroid/view/Window;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v1

    const/16 v2, 0x7d8

    iput v2, v1, Landroid/view/WindowManager$LayoutParams;->type:I

    iget-object v2, p0, Lcom/android/server/policy/MiuiPhoneWindowManager;->mFpNavCenterActionChooseDialog:Lmiui/app/AlertDialog;

    invoke-virtual {v2}, Lmiui/app/AlertDialog;->getWindow()Landroid/view/Window;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    iget-object v2, p0, Lcom/android/server/policy/MiuiPhoneWindowManager;->mFpNavCenterActionChooseDialog:Lmiui/app/AlertDialog;

    invoke-virtual {v2}, Lmiui/app/AlertDialog;->show()V

    return-void
.end method

.method private drawsSystemBarBackground(Landroid/view/WindowManagerPolicy$WindowState;)Z
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-eqz p1, :cond_0

    invoke-interface {p1}, Landroid/view/WindowManagerPolicy$WindowState;->getAttrs()Landroid/view/WindowManager$LayoutParams;

    move-result-object v2

    iget v2, v2, Landroid/view/WindowManager$LayoutParams;->flags:I

    const/high16 v3, -0x80000000

    and-int/2addr v2, v3

    if-eqz v2, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method private forcesDrawStatusBarBackground(Landroid/view/WindowManagerPolicy$WindowState;)Z
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-eqz p1, :cond_0

    invoke-interface {p1}, Landroid/view/WindowManagerPolicy$WindowState;->getAttrs()Landroid/view/WindowManager$LayoutParams;

    move-result-object v2

    iget v2, v2, Landroid/view/WindowManager$LayoutParams;->privateFlags:I

    const/high16 v3, 0x20000

    and-int/2addr v2, v3

    if-eqz v2, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method private getExtraWindowSystemUiVis(Landroid/view/WindowManagerPolicy$WindowState;)I
    .locals 3

    const/4 v0, 0x0

    if-eqz p1, :cond_0

    invoke-interface {p1}, Landroid/view/WindowManagerPolicy$WindowState;->getAttrs()Landroid/view/WindowManager$LayoutParams;

    move-result-object v1

    iget v1, v1, Landroid/view/WindowManager$LayoutParams;->extraFlags:I

    or-int/lit8 v0, v1, 0x0

    invoke-interface {p1}, Landroid/view/WindowManagerPolicy$WindowState;->getAttrs()Landroid/view/WindowManager$LayoutParams;

    move-result-object v1

    iget v1, v1, Landroid/view/WindowManager$LayoutParams;->type:I

    const/4 v2, 0x3

    if-ne v1, v2, :cond_0

    or-int/lit8 v0, v0, 0x1

    :cond_0
    invoke-static {v0}, Landroid/app/MiuiStatusBarManager;->getSystemUIVisibilityFlags(I)I

    move-result v0

    return v0
.end method

.method private hideNavBar(II)Z
    .locals 2

    const/4 v1, 0x0

    and-int/lit8 v0, p2, 0x2

    if-nez v0, :cond_0

    and-int/lit16 v0, p2, 0x1800

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    return v0

    :cond_1
    return v1
.end method

.method private hideStatusBar(II)Z
    .locals 2

    const/4 v1, 0x0

    and-int/lit16 v0, p1, 0x400

    if-nez v0, :cond_0

    and-int/lit8 v0, p2, 0x4

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    return v0

    :cond_1
    return v1
.end method

.method private injectEvent(Landroid/view/KeyEvent;II)V
    .locals 20

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v4

    new-instance v3, Landroid/view/KeyEvent;

    invoke-virtual/range {p1 .. p1}, Landroid/view/KeyEvent;->getFlags()I

    move-result v14

    invoke-virtual/range {p1 .. p1}, Landroid/view/KeyEvent;->getSource()I

    move-result v15

    const/4 v8, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v13, 0x0

    move-wide v6, v4

    move/from16 v9, p2

    move/from16 v12, p3

    invoke-direct/range {v3 .. v15}, Landroid/view/KeyEvent;-><init>(JJIIIIIIII)V

    new-instance v7, Landroid/view/KeyEvent;

    invoke-virtual/range {p1 .. p1}, Landroid/view/KeyEvent;->getFlags()I

    move-result v18

    invoke-virtual/range {p1 .. p1}, Landroid/view/KeyEvent;->getSource()I

    move-result v19

    const/4 v12, 0x1

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v17, 0x0

    move-wide v8, v4

    move-wide v10, v4

    move/from16 v13, p2

    move/from16 v16, p3

    invoke-direct/range {v7 .. v19}, Landroid/view/KeyEvent;-><init>(JJIIIIIIII)V

    invoke-static {}, Landroid/hardware/input/InputManager;->getInstance()Landroid/hardware/input/InputManager;

    move-result-object v2

    const/4 v6, 0x0

    invoke-virtual {v2, v3, v6}, Landroid/hardware/input/InputManager;->injectInputEvent(Landroid/view/InputEvent;I)Z

    invoke-static {}, Landroid/hardware/input/InputManager;->getInstance()Landroid/hardware/input/InputManager;

    move-result-object v2

    const/4 v6, 0x0

    invoke-virtual {v2, v7, v6}, Landroid/hardware/input/InputManager;->injectInputEvent(Landroid/view/InputEvent;I)Z

    const/4 v2, 0x0

    const/4 v6, 0x1

    const/4 v8, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v6, v8}, Lcom/android/server/policy/MiuiPhoneWindowManager;->performHapticFeedbackLw(Landroid/view/WindowManagerPolicy$WindowState;IZ)Z

    return-void
.end method

.method private processBackFingerprintDpcenterEvent(Landroid/view/KeyEvent;Z)V
    .locals 4

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/android/server/policy/MiuiPhoneWindowManager;->isDeviceProvisioned()Z

    move-result v0

    if-eqz v0, :cond_0

    if-eqz p2, :cond_1

    iget-object v0, p0, Lcom/android/server/policy/MiuiPhoneWindowManager;->mFocusedWindow:Landroid/view/WindowManagerPolicy$WindowState;

    if-eqz v0, :cond_0

    const-string/jumbo v0, "com.android.camera"

    iget-object v1, p0, Lcom/android/server/policy/MiuiPhoneWindowManager;->mFocusedWindow:Landroid/view/WindowManagerPolicy$WindowState;

    invoke-interface {v1}, Landroid/view/WindowManagerPolicy$WindowState;->getOwningPackage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getDeviceId()I

    move-result v0

    const/16 v1, 0x1b

    invoke-direct {p0, p1, v1, v0}, Lcom/android/server/policy/MiuiPhoneWindowManager;->injectEvent(Landroid/view/KeyEvent;II)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/server/policy/MiuiPhoneWindowManager;->mPowerManager:Landroid/os/PowerManager;

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    const-string/jumbo v1, "miui.policy:FINGERPRINT_DPAD_CENTER"

    invoke-virtual {v0, v2, v3, v1}, Landroid/os/PowerManager;->wakeUp(JLjava/lang/String;)V

    goto :goto_0
.end method

.method private processFrontFingerprintDpcenterEvent(Landroid/view/KeyEvent;)V
    .locals 8

    const/4 v7, 0x1

    const/4 v2, 0x0

    const/4 v6, -0x1

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result v1

    if-ne v1, v7, :cond_5

    iget-boolean v1, p0, Lcom/android/server/policy/MiuiPhoneWindowManager;->mDpadCenterDown:Z

    if-eqz v1, :cond_0

    iput-boolean v2, p0, Lcom/android/server/policy/MiuiPhoneWindowManager;->mDpadCenterDown:Z

    iget-boolean v1, p0, Lcom/android/server/policy/MiuiPhoneWindowManager;->mHomeDownAfterDpCenter:Z

    if-eqz v1, :cond_0

    iput-boolean v2, p0, Lcom/android/server/policy/MiuiPhoneWindowManager;->mHomeDownAfterDpCenter:Z

    const-string/jumbo v1, "BaseMiuiPhoneWindowManager"

    const-string/jumbo v2, "After dpcenter & home down, ignore tap fingerprint"

    invoke-static {v1, v2}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/android/server/policy/MiuiPhoneWindowManager;->isDeviceProvisioned()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/android/server/policy/MiuiPhoneWindowManager;->mMiuiKeyguardDelegate:Lcom/android/server/policy/MiuiKeyguardServiceDelegate;

    invoke-virtual {v1}, Lcom/android/server/policy/MiuiKeyguardServiceDelegate;->isShowingAndNotHidden()Z

    move-result v1

    xor-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_1

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getEventTime()J

    move-result-wide v2

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getDownTime()J

    move-result-wide v4

    sub-long/2addr v2, v4

    const-wide/16 v4, 0x12c

    cmp-long v1, v2, v4

    if-gez v1, :cond_1

    iget-boolean v1, p0, Lcom/android/server/policy/MiuiPhoneWindowManager;->mSingleKeyUse:Z

    if-eqz v1, :cond_2

    const/4 v1, 0x4

    invoke-direct {p0, p1, v1, v6}, Lcom/android/server/policy/MiuiPhoneWindowManager;->injectEvent(Landroid/view/KeyEvent;II)V

    :cond_1
    :goto_0
    return-void

    :cond_2
    iget-object v1, p0, Lcom/android/server/policy/MiuiPhoneWindowManager;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string/jumbo v2, "fingerprint_nav_center_action"

    const/4 v3, -0x2

    invoke-static {v1, v2, v6, v3}, Landroid/provider/Settings$System;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v0

    if-ne v6, v0, :cond_3

    iget-object v1, p0, Lcom/android/server/policy/MiuiPhoneWindowManager;->mHandler:Landroid/os/Handler;

    new-instance v2, Lcom/android/server/policy/MiuiPhoneWindowManager$1;

    invoke-direct {v2, p0}, Lcom/android/server/policy/MiuiPhoneWindowManager$1;-><init>(Lcom/android/server/policy/MiuiPhoneWindowManager;)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0

    :cond_3
    if-ne v7, v0, :cond_4

    const/4 v1, 0x3

    invoke-direct {p0, p1, v1, v6}, Lcom/android/server/policy/MiuiPhoneWindowManager;->injectEvent(Landroid/view/KeyEvent;II)V

    goto :goto_0

    :cond_4
    if-nez v0, :cond_1

    return-void

    :cond_5
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result v1

    if-nez v1, :cond_1

    iput-boolean v7, p0, Lcom/android/server/policy/MiuiPhoneWindowManager;->mDpadCenterDown:Z

    goto :goto_0
.end method

.method private processFrontFingerprintDprightEvent(Landroid/view/KeyEvent;)V
    .locals 2

    const-string/jumbo v0, "BaseMiuiPhoneWindowManager"

    const-string/jumbo v1, "processFrontFingerprintDprightEvent"

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method


# virtual methods
.method protected adjustFrame(Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/view/WindowManagerPolicy$WindowState;Landroid/view/WindowManagerPolicy$WindowState;FI)V
    .locals 14

    iget-boolean v12, p0, Lcom/android/server/policy/MiuiPhoneWindowManager;->mHasNavigationBar:Z

    if-nez v12, :cond_0

    return-void

    :cond_0
    invoke-interface/range {p9 .. p9}, Landroid/view/WindowManagerPolicy$WindowState;->getAttrs()Landroid/view/WindowManager$LayoutParams;

    move-result-object v1

    move-object/from16 v0, p9

    invoke-static {v0, v1}, Lcom/android/server/policy/PolicyControl;->getWindowFlags(Landroid/view/WindowManagerPolicy$WindowState;Landroid/view/WindowManager$LayoutParams;)I

    move-result v2

    const/4 v12, 0x0

    move-object/from16 v0, p9

    invoke-static {v0, v12}, Lcom/android/server/policy/PolicyControl;->getSystemUiVisibility(Landroid/view/WindowManagerPolicy$WindowState;Landroid/view/WindowManager$LayoutParams;)I

    move-result v11

    invoke-static/range {p11 .. p11}, Lmiui/util/CustomizeUtil;->isRestrict(F)Z

    move-result v12

    if-eqz v12, :cond_3

    invoke-interface/range {p9 .. p9}, Landroid/view/WindowManagerPolicy$WindowState;->isInMultiWindowMode()Z

    move-result v12

    xor-int/lit8 v12, v12, 0x1

    if-eqz v12, :cond_3

    if-nez p10, :cond_3

    iget v12, p0, Lcom/android/server/policy/MiuiPhoneWindowManager;->mNavigationBarPosition:I

    if-nez v12, :cond_4

    iget v12, p0, Lcom/android/server/policy/MiuiPhoneWindowManager;->mUnrestrictedScreenWidth:I

    int-to-float v12, v12

    mul-float v12, v12, p11

    const/high16 v13, 0x3f000000    # 0.5f

    add-float/2addr v12, v13

    float-to-int v5, v12

    iget v12, p0, Lcom/android/server/policy/MiuiPhoneWindowManager;->mUnrestrictedScreenHeight:I

    iget v13, p0, Lcom/android/server/policy/MiuiPhoneWindowManager;->mNavBarHeight:I

    sub-int v4, v12, v13

    sub-int v8, v4, v5

    iget v12, p1, Landroid/graphics/Rect;->top:I

    sub-int v10, v8, v12

    invoke-direct {p0, v2, v11}, Lcom/android/server/policy/MiuiPhoneWindowManager;->hideStatusBar(II)Z

    move-result v12

    if-eqz v12, :cond_1

    move/from16 v0, p12

    invoke-direct {p0, v2, v0}, Lcom/android/server/policy/MiuiPhoneWindowManager;->hideNavBar(II)Z

    move-result v12

    if-eqz v12, :cond_1

    iget v12, p0, Lcom/android/server/policy/MiuiPhoneWindowManager;->mUnrestrictedScreenHeight:I

    sub-int/2addr v12, v5

    div-int/lit8 v3, v12, 0x2

    iget v12, p0, Lcom/android/server/policy/MiuiPhoneWindowManager;->mUnrestrictedScreenHeight:I

    sub-int v4, v12, v3

    sub-int v8, v4, v5

    :cond_1
    iget v12, p1, Landroid/graphics/Rect;->bottom:I

    if-gt v12, v4, :cond_2

    iget v12, p1, Landroid/graphics/Rect;->top:I

    if-lt v12, v8, :cond_2

    return-void

    :cond_2
    iput v8, p1, Landroid/graphics/Rect;->top:I

    iput v4, p1, Landroid/graphics/Rect;->bottom:I

    iget v12, p1, Landroid/graphics/Rect;->bottom:I

    iget v13, p1, Landroid/graphics/Rect;->top:I

    sub-int/2addr v12, v13

    move-object/from16 v0, p2

    iput v12, v0, Landroid/graphics/Rect;->bottom:I

    move-object/from16 v0, p4

    iget v12, v0, Landroid/graphics/Rect;->bottom:I

    sub-int/2addr v12, v10

    move-object/from16 v0, p4

    iput v12, v0, Landroid/graphics/Rect;->bottom:I

    move-object/from16 v0, p5

    iget v12, v0, Landroid/graphics/Rect;->bottom:I

    sub-int/2addr v12, v10

    move-object/from16 v0, p5

    iput v12, v0, Landroid/graphics/Rect;->bottom:I

    :cond_3
    :goto_0
    return-void

    :cond_4
    iget v12, p0, Lcom/android/server/policy/MiuiPhoneWindowManager;->mNavigationBarPosition:I

    const/4 v13, 0x1

    if-ne v12, v13, :cond_6

    iget v12, p0, Lcom/android/server/policy/MiuiPhoneWindowManager;->mUnrestrictedScreenHeight:I

    int-to-float v12, v12

    mul-float v12, v12, p11

    const/high16 v13, 0x3f000000    # 0.5f

    add-float/2addr v12, v13

    float-to-int v9, v12

    iget v12, p0, Lcom/android/server/policy/MiuiPhoneWindowManager;->mUnrestrictedScreenWidth:I

    iget v13, p0, Lcom/android/server/policy/MiuiPhoneWindowManager;->mNavBarWidth:I

    sub-int v7, v12, v13

    sub-int v6, v7, v9

    invoke-direct {p0, v2, v11}, Lcom/android/server/policy/MiuiPhoneWindowManager;->hideStatusBar(II)Z

    move-result v12

    if-eqz v12, :cond_5

    move/from16 v0, p12

    invoke-direct {p0, v2, v0}, Lcom/android/server/policy/MiuiPhoneWindowManager;->hideNavBar(II)Z

    move-result v12

    if-eqz v12, :cond_5

    iget v12, p0, Lcom/android/server/policy/MiuiPhoneWindowManager;->mUnrestrictedScreenWidth:I

    sub-int/2addr v12, v9

    div-int/lit8 v3, v12, 0x2

    iget v12, p0, Lcom/android/server/policy/MiuiPhoneWindowManager;->mUnrestrictedScreenWidth:I

    sub-int v7, v12, v3

    sub-int v6, v7, v9

    :cond_5
    iput v6, p1, Landroid/graphics/Rect;->left:I

    iput v7, p1, Landroid/graphics/Rect;->right:I

    iget v12, p1, Landroid/graphics/Rect;->right:I

    iget v13, p1, Landroid/graphics/Rect;->left:I

    sub-int/2addr v12, v13

    move-object/from16 v0, p2

    iput v12, v0, Landroid/graphics/Rect;->right:I

    goto :goto_0

    :cond_6
    iget v12, p0, Lcom/android/server/policy/MiuiPhoneWindowManager;->mNavigationBarPosition:I

    const/4 v13, 0x2

    if-ne v12, v13, :cond_3

    iget v12, p0, Lcom/android/server/policy/MiuiPhoneWindowManager;->mUnrestrictedScreenHeight:I

    int-to-float v12, v12

    mul-float v12, v12, p11

    const/high16 v13, 0x3f000000    # 0.5f

    add-float/2addr v12, v13

    float-to-int v9, v12

    iget v6, p0, Lcom/android/server/policy/MiuiPhoneWindowManager;->mNavBarWidth:I

    add-int v7, v6, v9

    invoke-direct {p0, v2, v11}, Lcom/android/server/policy/MiuiPhoneWindowManager;->hideStatusBar(II)Z

    move-result v12

    if-eqz v12, :cond_7

    move/from16 v0, p12

    invoke-direct {p0, v2, v0}, Lcom/android/server/policy/MiuiPhoneWindowManager;->hideNavBar(II)Z

    move-result v12

    if-eqz v12, :cond_7

    iget v12, p0, Lcom/android/server/policy/MiuiPhoneWindowManager;->mUnrestrictedScreenWidth:I

    sub-int/2addr v12, v9

    div-int/lit8 v3, v12, 0x2

    iget v12, p0, Lcom/android/server/policy/MiuiPhoneWindowManager;->mUnrestrictedScreenWidth:I

    sub-int v7, v12, v3

    sub-int v6, v7, v9

    :cond_7
    iput v6, p1, Landroid/graphics/Rect;->left:I

    iput v7, p1, Landroid/graphics/Rect;->right:I

    iget v12, p1, Landroid/graphics/Rect;->right:I

    move-object/from16 v0, p2

    iput v12, v0, Landroid/graphics/Rect;->right:I

    iget v12, p1, Landroid/graphics/Rect;->left:I

    move-object/from16 v0, p2

    iput v12, v0, Landroid/graphics/Rect;->left:I

    move-object/from16 v0, p4

    iget v12, v0, Landroid/graphics/Rect;->left:I

    sub-int/2addr v12, v6

    move-object/from16 v0, p4

    iput v12, v0, Landroid/graphics/Rect;->left:I

    move-object/from16 v0, p5

    iget v12, v0, Landroid/graphics/Rect;->left:I

    sub-int/2addr v12, v6

    move-object/from16 v0, p5

    iput v12, v0, Landroid/graphics/Rect;->left:I

    move-object/from16 v0, p7

    iget v12, v0, Landroid/graphics/Rect;->left:I

    sub-int/2addr v12, v6

    move-object/from16 v0, p7

    iput v12, v0, Landroid/graphics/Rect;->left:I

    goto/16 :goto_0
.end method

.method protected adjustFrame(Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/view/WindowManagerPolicy$WindowState;Landroid/view/WindowManagerPolicy$WindowState;I)V
    .locals 21

    sget-boolean v2, Lmiui/util/CustomizeUtil;->HAS_NOTCH:Z

    if-nez v2, :cond_0

    return-void

    :cond_0
    invoke-interface/range {p9 .. p9}, Landroid/view/WindowManagerPolicy$WindowState;->getAttrs()Landroid/view/WindowManager$LayoutParams;

    move-result-object v9

    move-object/from16 v0, p9

    invoke-static {v0, v9}, Lcom/android/server/policy/PolicyControl;->getWindowFlags(Landroid/view/WindowManagerPolicy$WindowState;Landroid/view/WindowManager$LayoutParams;)I

    move-result v15

    const/4 v2, 0x0

    move-object/from16 v0, p9

    invoke-static {v0, v2}, Lcom/android/server/policy/PolicyControl;->getSystemUiVisibility(Landroid/view/WindowManagerPolicy$WindowState;Landroid/view/WindowManager$LayoutParams;)I

    move-result v19

    iget v2, v9, Landroid/view/WindowManager$LayoutParams;->extraFlags:I

    and-int/lit16 v2, v2, 0x100

    if-eqz v2, :cond_2

    const/4 v10, 0x1

    :goto_0
    move/from16 v0, p11

    and-int/lit16 v2, v0, 0x100

    if-eqz v2, :cond_3

    const/4 v14, 0x1

    :goto_1
    if-nez v10, :cond_4

    move v7, v14

    :goto_2
    if-eqz v10, :cond_6

    iget v2, v9, Landroid/view/WindowManager$LayoutParams;->extraFlags:I

    and-int/lit16 v2, v2, 0x200

    if-eqz v2, :cond_5

    const/4 v13, 0x1

    :goto_3
    if-eqz v10, :cond_9

    iget v2, v9, Landroid/view/WindowManager$LayoutParams;->extraFlags:I

    and-int/lit16 v2, v2, 0x400

    if-eqz v2, :cond_8

    const/4 v11, 0x1

    :goto_4
    const/16 v17, 0x0

    and-int/lit16 v2, v15, 0x200

    if-eqz v2, :cond_1

    iget v2, v9, Landroid/view/WindowManager$LayoutParams;->type:I

    const/16 v3, 0x7da

    if-eq v2, v3, :cond_1

    invoke-interface/range {p9 .. p9}, Landroid/view/WindowManagerPolicy$WindowState;->isInMultiWindowMode()Z

    move-result v2

    xor-int/lit8 v2, v2, 0x1

    if-eqz v2, :cond_1

    const/16 v17, 0x1

    :cond_1
    const/4 v4, 0x0

    const/4 v5, 0x0

    move/from16 v0, p11

    and-int/lit16 v2, v0, 0x80

    if-eqz v2, :cond_b

    const/16 v18, 0x1

    :goto_5
    iget v2, v9, Landroid/view/WindowManager$LayoutParams;->type:I

    const/16 v3, 0x7dd

    if-ne v2, v3, :cond_c

    return-void

    :cond_2
    const/4 v10, 0x0

    goto :goto_0

    :cond_3
    const/4 v14, 0x0

    goto :goto_1

    :cond_4
    const/4 v7, 0x1

    goto :goto_2

    :cond_5
    const/4 v13, 0x0

    goto :goto_3

    :cond_6
    move/from16 v0, p11

    and-int/lit16 v2, v0, 0x200

    if-eqz v2, :cond_7

    const/4 v13, 0x1

    goto :goto_3

    :cond_7
    const/4 v13, 0x0

    goto :goto_3

    :cond_8
    const/4 v11, 0x0

    goto :goto_4

    :cond_9
    move/from16 v0, p11

    and-int/lit16 v2, v0, 0x400

    if-eqz v2, :cond_a

    const/4 v11, 0x1

    goto :goto_4

    :cond_a
    const/4 v11, 0x0

    goto :goto_4

    :cond_b
    const/16 v18, 0x0

    goto :goto_5

    :cond_c
    move-object/from16 v0, p0

    iget v2, v0, Lcom/android/server/policy/MiuiPhoneWindowManager;->mDisplayRotation:I

    packed-switch v2, :pswitch_data_0

    :cond_d
    :goto_6
    :pswitch_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/server/policy/MiuiPhoneWindowManager;->mTopFullscreenOpaqueWindowState:Landroid/view/WindowManagerPolicy$WindowState;

    move-object/from16 v0, p9

    if-ne v0, v2, :cond_f

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/server/policy/MiuiPhoneWindowManager;->mPackage:Ljava/lang/String;

    invoke-interface/range {p9 .. p9}, Landroid/view/WindowManagerPolicy$WindowState;->getOwningPackage()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_e

    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/android/server/policy/MiuiPhoneWindowManager;->mAdjust:Z

    if-eq v2, v5, :cond_1c

    :cond_e
    :goto_7
    move-object/from16 v0, p0

    iput-boolean v5, v0, Lcom/android/server/policy/MiuiPhoneWindowManager;->mAdjust:Z

    move v8, v5

    move-object/from16 v0, p0

    iput-boolean v4, v0, Lcom/android/server/policy/MiuiPhoneWindowManager;->mNeedAdjust:Z

    move/from16 v16, v4

    invoke-interface/range {p9 .. p9}, Landroid/view/WindowManagerPolicy$WindowState;->getOwningPackage()Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, p0

    iput-object v6, v0, Lcom/android/server/policy/MiuiPhoneWindowManager;->mPackage:Ljava/lang/String;

    move-object/from16 v0, p0

    iput-boolean v7, v0, Lcom/android/server/policy/MiuiPhoneWindowManager;->mEnableNotchConfig:Z

    move v12, v7

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/server/policy/MiuiPhoneWindowManager;->mHandler:Landroid/os/Handler;

    move-object/from16 v20, v0

    new-instance v2, Lcom/android/server/policy/MiuiPhoneWindowManager$3;

    move-object/from16 v3, p0

    invoke-direct/range {v2 .. v7}, Lcom/android/server/policy/MiuiPhoneWindowManager$3;-><init>(Lcom/android/server/policy/MiuiPhoneWindowManager;ZZLjava/lang/String;Z)V

    move-object/from16 v0, v20

    invoke-virtual {v0, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :cond_f
    return-void

    :pswitch_1
    if-eqz v7, :cond_10

    xor-int/lit8 v2, v13, 0x1

    if-nez v2, :cond_11

    :cond_10
    if-nez v7, :cond_d

    move-object/from16 v0, p0

    move/from16 v1, v19

    invoke-direct {v0, v15, v1}, Lcom/android/server/policy/MiuiPhoneWindowManager;->hideStatusBar(II)Z

    move-result v2

    if-eqz v2, :cond_d

    :cond_11
    const/4 v4, 0x1

    if-eqz v18, :cond_12

    if-eqz v7, :cond_d

    :cond_12
    if-eqz v17, :cond_13

    move-object/from16 v0, p1

    iget v2, v0, Landroid/graphics/Rect;->top:I

    move-object/from16 v0, p0

    iget v3, v0, Lcom/android/server/policy/MiuiPhoneWindowManager;->mStatusBarHeight:I

    invoke-static {v2, v3}, Ljava/lang/Math;->max(II)I

    move-result v2

    move-object/from16 v0, p1

    iput v2, v0, Landroid/graphics/Rect;->top:I

    :goto_8
    const/4 v5, 0x1

    goto :goto_6

    :cond_13
    move-object/from16 v0, p2

    iget v2, v0, Landroid/graphics/Rect;->top:I

    move-object/from16 v0, p0

    iget v3, v0, Lcom/android/server/policy/MiuiPhoneWindowManager;->mStatusBarHeight:I

    invoke-static {v2, v3}, Ljava/lang/Math;->max(II)I

    move-result v2

    move-object/from16 v0, p2

    iput v2, v0, Landroid/graphics/Rect;->top:I

    goto :goto_8

    :pswitch_2
    if-eqz v7, :cond_14

    xor-int/lit8 v2, v11, 0x1

    if-nez v2, :cond_15

    :cond_14
    xor-int/lit8 v2, v7, 0x1

    if-eqz v2, :cond_d

    :cond_15
    const/4 v4, 0x1

    if-eqz v18, :cond_16

    if-eqz v7, :cond_d

    :cond_16
    if-eqz v17, :cond_17

    move-object/from16 v0, p1

    iget v2, v0, Landroid/graphics/Rect;->left:I

    move-object/from16 v0, p0

    iget v3, v0, Lcom/android/server/policy/MiuiPhoneWindowManager;->mStatusBarHeight:I

    invoke-static {v2, v3}, Ljava/lang/Math;->max(II)I

    move-result v2

    move-object/from16 v0, p1

    iput v2, v0, Landroid/graphics/Rect;->left:I

    :goto_9
    const/4 v5, 0x1

    goto/16 :goto_6

    :cond_17
    move-object/from16 v0, p2

    iget v2, v0, Landroid/graphics/Rect;->left:I

    move-object/from16 v0, p0

    iget v3, v0, Lcom/android/server/policy/MiuiPhoneWindowManager;->mStatusBarHeight:I

    invoke-static {v2, v3}, Ljava/lang/Math;->max(II)I

    move-result v2

    move-object/from16 v0, p2

    iput v2, v0, Landroid/graphics/Rect;->left:I

    goto :goto_9

    :pswitch_3
    if-eqz v7, :cond_18

    xor-int/lit8 v2, v11, 0x1

    if-nez v2, :cond_19

    :cond_18
    xor-int/lit8 v2, v7, 0x1

    if-eqz v2, :cond_d

    :cond_19
    const/4 v4, 0x1

    if-eqz v18, :cond_1a

    if-eqz v7, :cond_d

    :cond_1a
    if-eqz v17, :cond_1b

    move-object/from16 v0, p1

    iget v2, v0, Landroid/graphics/Rect;->right:I

    move-object/from16 v0, p0

    iget v3, v0, Lcom/android/server/policy/MiuiPhoneWindowManager;->mDisplayWidth:I

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/server/policy/MiuiPhoneWindowManager;->mStatusBarHeight:I

    move/from16 v20, v0

    sub-int v3, v3, v20

    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I

    move-result v2

    move-object/from16 v0, p1

    iput v2, v0, Landroid/graphics/Rect;->right:I

    :goto_a
    const/4 v5, 0x1

    goto/16 :goto_6

    :cond_1b
    move-object/from16 v0, p2

    iget v2, v0, Landroid/graphics/Rect;->right:I

    move-object/from16 v0, p0

    iget v3, v0, Lcom/android/server/policy/MiuiPhoneWindowManager;->mDisplayWidth:I

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/server/policy/MiuiPhoneWindowManager;->mStatusBarHeight:I

    move/from16 v20, v0

    sub-int v3, v3, v20

    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I

    move-result v2

    move-object/from16 v0, p2

    iput v2, v0, Landroid/graphics/Rect;->right:I

    goto :goto_a

    :cond_1c
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/android/server/policy/MiuiPhoneWindowManager;->mNeedAdjust:Z

    if-ne v2, v4, :cond_e

    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/android/server/policy/MiuiPhoneWindowManager;->mEnableNotchConfig:Z

    if-eq v2, v7, :cond_f

    goto/16 :goto_7

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method public beginLayoutLw(ZIIII)V
    .locals 2

    invoke-super/range {p0 .. p5}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->beginLayoutLw(ZIIII)V

    iput p4, p0, Lcom/android/server/policy/MiuiPhoneWindowManager;->mDisplayRotation:I

    iput p2, p0, Lcom/android/server/policy/MiuiPhoneWindowManager;->mDisplayWidth:I

    iput p3, p0, Lcom/android/server/policy/MiuiPhoneWindowManager;->mDisplayHeight:I

    iget-object v0, p0, Lcom/android/server/policy/MiuiPhoneWindowManager;->mNavigationBar:Landroid/view/WindowManagerPolicy$WindowState;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/android/server/policy/MiuiPhoneWindowManager;->forceShowNavigationBar()Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/android/server/policy/MiuiPhoneWindowManager;->mNavigationBarPosition:I

    if-nez v0, :cond_1

    sget-object v0, Lcom/android/server/policy/MiuiPhoneWindowManager;->mTmpNavigationFrame:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->top:I

    iput v0, p0, Lcom/android/server/policy/MiuiPhoneWindowManager;->mSystemBottom:I

    sget-object v0, Lcom/android/server/policy/MiuiPhoneWindowManager;->mTmpNavigationFrame:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->top:I

    iput v0, p0, Lcom/android/server/policy/MiuiPhoneWindowManager;->mDockBottom:I

    iget v0, p0, Lcom/android/server/policy/MiuiPhoneWindowManager;->mDockBottom:I

    iget v1, p0, Lcom/android/server/policy/MiuiPhoneWindowManager;->mRestrictedScreenTop:I

    sub-int/2addr v0, v1

    iput v0, p0, Lcom/android/server/policy/MiuiPhoneWindowManager;->mRestrictedScreenHeight:I

    iget v0, p0, Lcom/android/server/policy/MiuiPhoneWindowManager;->mDockBottom:I

    iget v1, p0, Lcom/android/server/policy/MiuiPhoneWindowManager;->mRestrictedOverscanScreenTop:I

    sub-int/2addr v0, v1

    iput v0, p0, Lcom/android/server/policy/MiuiPhoneWindowManager;->mRestrictedOverscanScreenHeight:I

    :goto_0
    iget v0, p0, Lcom/android/server/policy/MiuiPhoneWindowManager;->mDockTop:I

    iput v0, p0, Lcom/android/server/policy/MiuiPhoneWindowManager;->mCurTop:I

    iput v0, p0, Lcom/android/server/policy/MiuiPhoneWindowManager;->mVoiceContentTop:I

    iput v0, p0, Lcom/android/server/policy/MiuiPhoneWindowManager;->mContentTop:I

    iget v0, p0, Lcom/android/server/policy/MiuiPhoneWindowManager;->mDockBottom:I

    iput v0, p0, Lcom/android/server/policy/MiuiPhoneWindowManager;->mCurBottom:I

    iput v0, p0, Lcom/android/server/policy/MiuiPhoneWindowManager;->mVoiceContentBottom:I

    iput v0, p0, Lcom/android/server/policy/MiuiPhoneWindowManager;->mContentBottom:I

    iget v0, p0, Lcom/android/server/policy/MiuiPhoneWindowManager;->mDockLeft:I

    iput v0, p0, Lcom/android/server/policy/MiuiPhoneWindowManager;->mCurLeft:I

    iput v0, p0, Lcom/android/server/policy/MiuiPhoneWindowManager;->mVoiceContentLeft:I

    iput v0, p0, Lcom/android/server/policy/MiuiPhoneWindowManager;->mContentLeft:I

    iget v0, p0, Lcom/android/server/policy/MiuiPhoneWindowManager;->mDockRight:I

    iput v0, p0, Lcom/android/server/policy/MiuiPhoneWindowManager;->mCurRight:I

    iput v0, p0, Lcom/android/server/policy/MiuiPhoneWindowManager;->mVoiceContentRight:I

    iput v0, p0, Lcom/android/server/policy/MiuiPhoneWindowManager;->mContentRight:I

    :cond_0
    return-void

    :cond_1
    sget-object v0, Lcom/android/server/policy/MiuiPhoneWindowManager;->mTmpNavigationFrame:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->left:I

    iput v0, p0, Lcom/android/server/policy/MiuiPhoneWindowManager;->mSystemRight:I

    sget-object v0, Lcom/android/server/policy/MiuiPhoneWindowManager;->mTmpNavigationFrame:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->left:I

    iput v0, p0, Lcom/android/server/policy/MiuiPhoneWindowManager;->mDockRight:I

    iget v0, p0, Lcom/android/server/policy/MiuiPhoneWindowManager;->mDockRight:I

    iget v1, p0, Lcom/android/server/policy/MiuiPhoneWindowManager;->mRestrictedScreenLeft:I

    sub-int/2addr v0, v1

    iput v0, p0, Lcom/android/server/policy/MiuiPhoneWindowManager;->mRestrictedScreenWidth:I

    iget v0, p0, Lcom/android/server/policy/MiuiPhoneWindowManager;->mDockRight:I

    iget v1, p0, Lcom/android/server/policy/MiuiPhoneWindowManager;->mRestrictedOverscanScreenLeft:I

    sub-int/2addr v0, v1

    iput v0, p0, Lcom/android/server/policy/MiuiPhoneWindowManager;->mRestrictedOverscanScreenWidth:I

    goto :goto_0
.end method

.method protected callSuperInterceptKeyBeforeQueueing(Landroid/view/KeyEvent;IZ)I
    .locals 1

    invoke-super {p0, p1, p2}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->interceptKeyBeforeQueueing(Landroid/view/KeyEvent;I)I

    move-result v0

    return v0
.end method

.method public canBeHiddenByKeyguardLw(Landroid/view/WindowManagerPolicy$WindowState;)Z
    .locals 2

    invoke-interface {p1}, Landroid/view/WindowManagerPolicy$WindowState;->getAttrs()Landroid/view/WindowManager$LayoutParams;

    move-result-object v0

    iget v0, v0, Landroid/view/WindowManager$LayoutParams;->type:I

    const/16 v1, 0x7d4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->canBeHiddenByKeyguardLw(Landroid/view/WindowManagerPolicy$WindowState;)Z

    move-result v0

    goto :goto_0
.end method

.method protected finishActivityInternal(Landroid/os/IBinder;ILandroid/content/Intent;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, p1, p2, p3, v1}, Landroid/app/IActivityManager;->finishActivity(Landroid/os/IBinder;ILandroid/content/Intent;I)Z

    return-void
.end method

.method protected forceStopPackage(Ljava/lang/String;I)V
    .locals 3

    iget-object v1, p0, Lcom/android/server/policy/MiuiPhoneWindowManager;->mContext:Landroid/content/Context;

    const-string/jumbo v2, "activity"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    invoke-virtual {v0, p1, p2}, Landroid/app/ActivityManager;->forceStopPackageAsUser(Ljava/lang/String;I)V

    return-void
.end method

.method getExtraSystemUiVisibility(Landroid/view/WindowManagerPolicy$WindowState;F)I
    .locals 9

    const/high16 v8, 0x40000000    # 2.0f

    invoke-super {p0, p1, p2}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->getExtraSystemUiVisibility(Landroid/view/WindowManagerPolicy$WindowState;F)I

    iget-boolean v7, p0, Lcom/android/server/policy/MiuiPhoneWindowManager;->mForceShowSystemBars:Z

    if-eqz v7, :cond_1

    iget-boolean v7, p0, Lcom/android/server/policy/MiuiPhoneWindowManager;->mForceStatusBarFromKeyguard:Z

    xor-int/lit8 v2, v7, 0x1

    :goto_0
    invoke-virtual {p0}, Lcom/android/server/policy/MiuiPhoneWindowManager;->isStatusBarKeyguard()Z

    move-result v7

    if-eqz v7, :cond_2

    iget-object v4, p0, Lcom/android/server/policy/MiuiPhoneWindowManager;->mStatusBar:Landroid/view/WindowManagerPolicy$WindowState;

    :goto_1
    invoke-direct {p0, v4}, Lcom/android/server/policy/MiuiPhoneWindowManager;->getExtraWindowSystemUiVis(Landroid/view/WindowManagerPolicy$WindowState;)I

    move-result v5

    iget-object v7, p0, Lcom/android/server/policy/MiuiPhoneWindowManager;->mTopFullscreenOpaqueWindowState:Landroid/view/WindowManagerPolicy$WindowState;

    invoke-direct {p0, v7}, Lcom/android/server/policy/MiuiPhoneWindowManager;->drawsSystemBarBackground(Landroid/view/WindowManagerPolicy$WindowState;)Z

    move-result v7

    if-eqz v7, :cond_3

    and-int v7, v5, v8

    if-nez v7, :cond_3

    const/4 v3, 0x1

    :goto_2
    iget-object v7, p0, Lcom/android/server/policy/MiuiPhoneWindowManager;->mTopDockedOpaqueWindowState:Landroid/view/WindowManagerPolicy$WindowState;

    invoke-direct {p0, v7}, Lcom/android/server/policy/MiuiPhoneWindowManager;->getExtraWindowSystemUiVis(Landroid/view/WindowManagerPolicy$WindowState;)I

    move-result v1

    iget-object v7, p0, Lcom/android/server/policy/MiuiPhoneWindowManager;->mTopDockedOpaqueWindowState:Landroid/view/WindowManagerPolicy$WindowState;

    invoke-direct {p0, v7}, Lcom/android/server/policy/MiuiPhoneWindowManager;->drawsSystemBarBackground(Landroid/view/WindowManagerPolicy$WindowState;)Z

    move-result v7

    if-eqz v7, :cond_4

    and-int v7, v1, v8

    if-nez v7, :cond_4

    const/4 v0, 0x1

    :goto_3
    const/4 v6, 0x0

    if-eqz v3, :cond_5

    if-eqz v0, :cond_5

    const/16 v6, 0x8

    const v7, -0x40000001    # -1.9999999f

    and-int/lit8 v6, v7, 0x8

    :cond_0
    :goto_4
    return v6

    :cond_1
    const/4 v2, 0x0

    goto :goto_0

    :cond_2
    iget-object v4, p0, Lcom/android/server/policy/MiuiPhoneWindowManager;->mTopFullscreenOpaqueWindowState:Landroid/view/WindowManagerPolicy$WindowState;

    goto :goto_1

    :cond_3
    iget-object v7, p0, Lcom/android/server/policy/MiuiPhoneWindowManager;->mTopFullscreenOpaqueWindowState:Landroid/view/WindowManagerPolicy$WindowState;

    invoke-direct {p0, v7}, Lcom/android/server/policy/MiuiPhoneWindowManager;->forcesDrawStatusBarBackground(Landroid/view/WindowManagerPolicy$WindowState;)Z

    move-result v3

    goto :goto_2

    :cond_4
    iget-object v7, p0, Lcom/android/server/policy/MiuiPhoneWindowManager;->mTopDockedOpaqueWindowState:Landroid/view/WindowManagerPolicy$WindowState;

    invoke-direct {p0, v7}, Lcom/android/server/policy/MiuiPhoneWindowManager;->forcesDrawStatusBarBackground(Landroid/view/WindowManagerPolicy$WindowState;)Z

    move-result v0

    goto :goto_3

    :cond_5
    iget-boolean v7, p0, Lcom/android/server/policy/MiuiPhoneWindowManager;->mTranslucentDecorEnabled:Z

    if-nez v7, :cond_6

    iget-object v7, p0, Lcom/android/server/policy/MiuiPhoneWindowManager;->mStatusBar:Landroid/view/WindowManagerPolicy$WindowState;

    if-eq v4, v7, :cond_6

    :goto_5
    const/4 v6, 0x0

    goto :goto_4

    :cond_6
    if-eqz v2, :cond_0

    goto :goto_5
.end method

.method protected getKeyguardWindowState()Landroid/view/WindowManagerPolicy$WindowState;
    .locals 1

    iget-object v0, p0, Lcom/android/server/policy/MiuiPhoneWindowManager;->mKeyguard:Landroid/view/WindowManagerPolicy$WindowState;

    return-object v0
.end method

.method protected getWakePolicyFlag()I
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public getWindowLayerFromTypeLw(IZ)I
    .locals 5

    const/16 v0, 0xb

    const/16 v1, 0xa

    const/4 v4, 0x1

    const/4 v3, 0x2

    if-lt p1, v4, :cond_0

    const/16 v2, 0x63

    if-gt p1, v2, :cond_0

    return v3

    :cond_0
    packed-switch p1, :pswitch_data_0

    :pswitch_0
    const-string/jumbo v0, "WindowManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Unknown window type: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    return v3

    :pswitch_1
    return v4

    :pswitch_2
    return v3

    :pswitch_3
    return v3

    :pswitch_4
    return v3

    :pswitch_5
    const/4 v0, 0x3

    return v0

    :pswitch_6
    const/4 v0, 0x4

    return v0

    :pswitch_7
    const/4 v0, 0x5

    return v0

    :pswitch_8
    const/4 v0, 0x6

    return v0

    :pswitch_9
    const/4 v0, 0x7

    return v0

    :pswitch_a
    const/16 v0, 0x8

    return v0

    :pswitch_b
    const/16 v0, 0x9

    return v0

    :pswitch_c
    if-eqz p2, :cond_1

    :goto_0
    return v0

    :cond_1
    move v0, v1

    goto :goto_0

    :pswitch_d
    const/16 v0, 0xc

    return v0

    :pswitch_e
    const/16 v0, 0xd

    return v0

    :pswitch_f
    const/16 v0, 0xe

    return v0

    :pswitch_10
    const/16 v0, 0xf

    return v0

    :pswitch_11
    const/16 v0, 0x10

    return v0

    :pswitch_12
    const/16 v0, 0x11

    return v0

    :pswitch_13
    const/16 v0, 0x12

    return v0

    :pswitch_14
    const/16 v0, 0x13

    return v0

    :pswitch_15
    const/16 v0, 0x14

    return v0

    :pswitch_16
    const/16 v0, 0x15

    return v0

    :pswitch_17
    if-eqz p2, :cond_2

    const/16 v0, 0x16

    :cond_2
    return v0

    :pswitch_18
    const/16 v0, 0x17

    return v0

    :pswitch_19
    const/16 v0, 0x18

    return v0

    :pswitch_1a
    const/16 v0, 0x19

    return v0

    :pswitch_1b
    if-eqz p2, :cond_3

    const/16 v1, 0x1a

    :cond_3
    return v1

    :pswitch_1c
    const/16 v0, 0x1b

    return v0

    :pswitch_1d
    const/16 v0, 0x1c

    return v0

    :pswitch_1e
    const/16 v0, 0x1d

    return v0

    :pswitch_1f
    const/16 v0, 0x1e

    return v0

    :pswitch_20
    const/16 v0, 0x1f

    return v0

    :pswitch_21
    const/16 v0, 0x20

    return v0

    :pswitch_22
    const/16 v0, 0x21

    return v0

    :pswitch_data_0
    .packed-switch 0x7d0
        :pswitch_13
        :pswitch_6
        :pswitch_5
        :pswitch_c
        :pswitch_11
        :pswitch_a
        :pswitch_17
        :pswitch_b
        :pswitch_9
        :pswitch_15
        :pswitch_1b
        :pswitch_f
        :pswitch_10
        :pswitch_1
        :pswitch_14
        :pswitch_20
        :pswitch_1e
        :pswitch_12
        :pswitch_22
        :pswitch_18
        :pswitch_16
        :pswitch_21
        :pswitch_8
        :pswitch_e
        :pswitch_19
        :pswitch_0
        :pswitch_1d
        :pswitch_1c
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_7
        :pswitch_1f
        :pswitch_6
        :pswitch_3
        :pswitch_4
        :pswitch_1a
        :pswitch_2
        :pswitch_d
    .end packed-switch
.end method

.method public init(Landroid/content/Context;Landroid/view/IWindowManager;Landroid/view/WindowManagerPolicy$WindowManagerFuncs;)V
    .locals 0

    invoke-super {p0, p1, p2, p3}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->init(Landroid/content/Context;Landroid/view/IWindowManager;Landroid/view/WindowManagerPolicy$WindowManagerFuncs;)V

    invoke-virtual {p0, p1, p2, p3}, Lcom/android/server/policy/MiuiPhoneWindowManager;->initInternal(Landroid/content/Context;Landroid/view/IWindowManager;Landroid/view/WindowManagerPolicy$WindowManagerFuncs;)V

    return-void
.end method

.method protected intercept(Landroid/view/KeyEvent;IZI)I
    .locals 4

    invoke-super {p0, p1, p2, p3, p4}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->intercept(Landroid/view/KeyEvent;IZI)I

    iget-object v1, p0, Lcom/android/server/policy/MiuiPhoneWindowManager;->mContext:Landroid/content/Context;

    const-string/jumbo v2, "power"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    const/4 v1, -0x1

    if-ne p4, v1, :cond_1

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Landroid/os/PowerManager;->goToSleep(J)V

    :cond_0
    :goto_0
    const/4 v1, 0x0

    return v1

    :cond_1
    const/4 v1, 0x1

    if-ne p4, v1, :cond_0

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Landroid/os/PowerManager;->wakeUp(J)V

    goto :goto_0
.end method

.method public interceptKeyBeforeQueueing(Landroid/view/KeyEvent;I)I
    .locals 2

    const/4 v0, 0x0

    const/high16 v1, 0x20000000

    and-int/2addr v1, p2

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    invoke-virtual {p0, p1, p2, v0}, Lcom/android/server/policy/MiuiPhoneWindowManager;->interceptKeyBeforeQueueingInternal(Landroid/view/KeyEvent;IZ)I

    move-result v0

    return v0
.end method

.method protected isFingerPrintKey(Landroid/view/KeyEvent;)Z
    .locals 4

    const/4 v3, 0x0

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getDevice()Landroid/view/InputDevice;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/server/policy/MiuiPhoneWindowManager;->mFpNavEventNameList:Ljava/util/List;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/server/policy/MiuiPhoneWindowManager;->mFpNavEventNameList:Ljava/util/List;

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getDevice()Landroid/view/InputDevice;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/InputDevice;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    return v3

    :pswitch_0
    const/4 v1, 0x1

    return v1

    :cond_0
    return v3

    :pswitch_data_0
    .packed-switch 0x16
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method protected isInLockTaskMode()Z
    .locals 3

    :try_start_0
    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {v0}, Landroid/app/IActivityManager;->isInLockTaskMode()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    return v2

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    :cond_0
    const/4 v2, 0x0

    return v2
.end method

.method public isKeyguardHostWindow(Landroid/view/WindowManager$LayoutParams;)Z
    .locals 2

    iget v0, p1, Landroid/view/WindowManager$LayoutParams;->type:I

    const/16 v1, 0x7d4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected isScreenOnInternal()Z
    .locals 1

    invoke-virtual {p0}, Lcom/android/server/policy/MiuiPhoneWindowManager;->isScreenOn()Z

    move-result v0

    return v0
.end method

.method public isStatusBarKeyguard()Z
    .locals 1

    iget-object v0, p0, Lcom/android/server/policy/MiuiPhoneWindowManager;->mKeyguard:Landroid/view/WindowManagerPolicy$WindowState;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/server/policy/MiuiPhoneWindowManager;->mKeyguard:Landroid/view/WindowManagerPolicy$WindowState;

    invoke-interface {v0}, Landroid/view/WindowManagerPolicy$WindowState;->isVisibleLw()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/server/policy/MiuiPhoneWindowManager;->mKeyguard:Landroid/view/WindowManagerPolicy$WindowState;

    invoke-interface {v0}, Landroid/view/WindowManagerPolicy$WindowState;->isAnimatingLw()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected launchAssistActionInternal(Ljava/lang/String;Landroid/os/Bundle;)V
    .locals 2

    if-eqz p1, :cond_0

    const/4 v1, 0x1

    invoke-virtual {p2, p1, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    :cond_0
    invoke-virtual {p0}, Lcom/android/server/policy/MiuiPhoneWindowManager;->getStatusBarManagerInternal()Lcom/android/server/statusbar/StatusBarManagerInternal;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-interface {v0, p2}, Lcom/android/server/statusbar/StatusBarManagerInternal;->startAssist(Landroid/os/Bundle;)V

    :cond_1
    return-void
.end method

.method protected launchRecentPanelInternal()V
    .locals 1

    invoke-virtual {p0}, Lcom/android/server/policy/MiuiPhoneWindowManager;->getStatusBarManagerInternal()Lcom/android/server/statusbar/StatusBarManagerInternal;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lcom/android/server/statusbar/StatusBarManagerInternal;->toggleRecentApps()V

    :cond_0
    return-void
.end method

.method public onConfigurationChanged()V
    .locals 0

    invoke-super {p0}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->onConfigurationChanged()V

    invoke-virtual {p0}, Lcom/android/server/policy/MiuiPhoneWindowManager;->updateNavigationBarWidth()V

    return-void
.end method

.method protected onStatusBarPanelRevealed(Lcom/android/internal/statusbar/IStatusBarService;)V
    .locals 3

    const/4 v1, 0x1

    const/4 v2, 0x0

    :try_start_0
    invoke-interface {p1, v1, v2}, Lcom/android/internal/statusbar/IStatusBarService;->onPanelRevealed(ZI)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method protected preloadRecentAppsInternal()V
    .locals 1

    invoke-virtual {p0}, Lcom/android/server/policy/MiuiPhoneWindowManager;->getStatusBarManagerInternal()Lcom/android/server/statusbar/StatusBarManagerInternal;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lcom/android/server/statusbar/StatusBarManagerInternal;->preloadRecentApps()V

    :cond_0
    return-void
.end method

.method public prepareAddWindowLw(Landroid/view/WindowManagerPolicy$WindowState;Landroid/view/WindowManager$LayoutParams;)I
    .locals 3

    iget v1, p2, Landroid/view/WindowManager$LayoutParams;->type:I

    const/16 v2, 0x7d4

    if-ne v2, v1, :cond_1

    iget-object v1, p0, Lcom/android/server/policy/MiuiPhoneWindowManager;->mKeyguard:Landroid/view/WindowManagerPolicy$WindowState;

    if-eqz v1, :cond_0

    const/4 v1, -0x7

    return v1

    :cond_0
    iput-object p1, p0, Lcom/android/server/policy/MiuiPhoneWindowManager;->mKeyguard:Landroid/view/WindowManagerPolicy$WindowState;

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/android/server/policy/MiuiPhoneWindowManager;->mKeyguardAdded:Z

    :cond_1
    invoke-super {p0, p1, p2}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->prepareAddWindowLw(Landroid/view/WindowManagerPolicy$WindowState;Landroid/view/WindowManager$LayoutParams;)I

    move-result v0

    iget v1, p2, Landroid/view/WindowManager$LayoutParams;->type:I

    const/16 v2, 0x7d0

    if-ne v2, v1, :cond_2

    iget-object v1, p0, Lcom/android/server/policy/MiuiPhoneWindowManager;->mKeyguard:Landroid/view/WindowManagerPolicy$WindowState;

    if-nez v1, :cond_2

    iget-boolean v1, p0, Lcom/android/server/policy/MiuiPhoneWindowManager;->mKeyguardAdded:Z

    :cond_2
    return v0
.end method

.method protected processFingerprintNavigationEvent(Landroid/view/KeyEvent;Z)I
    .locals 3

    const/4 v2, 0x0

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    return v2

    :pswitch_0
    iget-boolean v1, p0, Lcom/android/server/policy/MiuiPhoneWindowManager;->mFrontFingerprintSensor:Z

    if-eqz v1, :cond_1

    iget-boolean v1, p0, Lcom/android/server/policy/MiuiPhoneWindowManager;->mSupportTapFingerprintSensorToHome:Z

    if-eqz v1, :cond_0

    invoke-direct {p0, p1}, Lcom/android/server/policy/MiuiPhoneWindowManager;->processFrontFingerprintDpcenterEvent(Landroid/view/KeyEvent;)V

    return v2

    :cond_0
    return v2

    :cond_1
    invoke-direct {p0, p1, p2}, Lcom/android/server/policy/MiuiPhoneWindowManager;->processBackFingerprintDpcenterEvent(Landroid/view/KeyEvent;Z)V

    return v2

    :pswitch_1
    invoke-direct {p0, p1}, Lcom/android/server/policy/MiuiPhoneWindowManager;->processFrontFingerprintDprightEvent(Landroid/view/KeyEvent;)V

    return v2

    :pswitch_data_0
    .packed-switch 0x16
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public removeWindowLw(Landroid/view/WindowManagerPolicy$WindowState;)V
    .locals 4

    const/4 v3, 0x0

    iget-object v1, p0, Lcom/android/server/policy/MiuiPhoneWindowManager;->mKeyguard:Landroid/view/WindowManagerPolicy$WindowState;

    if-ne v1, p1, :cond_1

    const-string/jumbo v1, "WindowManager"

    const-string/jumbo v2, "Removing keyguard window (Did it crash?)"

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    iput-object v3, p0, Lcom/android/server/policy/MiuiPhoneWindowManager;->mKeyguard:Landroid/view/WindowManagerPolicy$WindowState;

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v1, p0, Lcom/android/server/policy/MiuiPhoneWindowManager;->mStatusBar:Landroid/view/WindowManagerPolicy$WindowState;

    if-ne v1, p1, :cond_2

    const/4 v0, 0x1

    :goto_1
    invoke-super {p0, p1}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->removeWindowLw(Landroid/view/WindowManagerPolicy$WindowState;)V

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/android/server/policy/MiuiPhoneWindowManager;->mKeyguard:Landroid/view/WindowManagerPolicy$WindowState;

    if-nez v1, :cond_0

    iget-boolean v1, p0, Lcom/android/server/policy/MiuiPhoneWindowManager;->mKeyguardAdded:Z

    xor-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method protected screenOffBecauseOfProxSensor()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method protected setKeyguardVisibilityState(ZZ)Z
    .locals 6

    const/4 v5, 0x0

    const/4 v4, 0x1

    iget-boolean v2, p0, Lcom/android/server/policy/MiuiPhoneWindowManager;->mKeyguardOccluded:Z

    iget-object v3, p0, Lcom/android/server/policy/MiuiPhoneWindowManager;->mMiuiKeyguardDelegate:Lcom/android/server/policy/MiuiKeyguardServiceDelegate;

    iget-object v3, v3, Lcom/android/server/policy/MiuiKeyguardServiceDelegate;->mKeyguardDelegate:Lcom/android/server/policy/keyguard/KeyguardServiceDelegate;

    invoke-virtual {v3}, Lcom/android/server/policy/keyguard/KeyguardServiceDelegate;->isShowing()Z

    move-result v1

    if-ne v2, p1, :cond_1

    move v0, p2

    :goto_0
    if-nez p1, :cond_2

    if-eqz v0, :cond_2

    if-eqz v1, :cond_2

    iput-boolean v5, p0, Lcom/android/server/policy/MiuiPhoneWindowManager;->mKeyguardOccluded:Z

    iget-object v3, p0, Lcom/android/server/policy/MiuiPhoneWindowManager;->mMiuiKeyguardDelegate:Lcom/android/server/policy/MiuiKeyguardServiceDelegate;

    iget-object v3, v3, Lcom/android/server/policy/MiuiKeyguardServiceDelegate;->mKeyguardDelegate:Lcom/android/server/policy/keyguard/KeyguardServiceDelegate;

    invoke-virtual {v3, v5, v4}, Lcom/android/server/policy/keyguard/KeyguardServiceDelegate;->setOccluded(ZZ)V

    iget-object v3, p0, Lcom/android/server/policy/MiuiPhoneWindowManager;->mKeyguard:Landroid/view/WindowManagerPolicy$WindowState;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/android/server/policy/MiuiPhoneWindowManager;->mKeyguard:Landroid/view/WindowManagerPolicy$WindowState;

    invoke-interface {v3, v4}, Landroid/view/WindowManagerPolicy$WindowState;->showLw(Z)Z

    :cond_0
    return v4

    :cond_1
    const/4 v0, 0x1

    goto :goto_0

    :cond_2
    if-eqz p1, :cond_4

    if-eqz v0, :cond_4

    if-eqz v1, :cond_4

    iput-boolean v4, p0, Lcom/android/server/policy/MiuiPhoneWindowManager;->mKeyguardOccluded:Z

    iget-object v3, p0, Lcom/android/server/policy/MiuiPhoneWindowManager;->mMiuiKeyguardDelegate:Lcom/android/server/policy/MiuiKeyguardServiceDelegate;

    iget-object v3, v3, Lcom/android/server/policy/MiuiKeyguardServiceDelegate;->mKeyguardDelegate:Lcom/android/server/policy/keyguard/KeyguardServiceDelegate;

    invoke-virtual {v3, v4, v5}, Lcom/android/server/policy/keyguard/KeyguardServiceDelegate;->setOccluded(ZZ)V

    iget-object v3, p0, Lcom/android/server/policy/MiuiPhoneWindowManager;->mKeyguard:Landroid/view/WindowManagerPolicy$WindowState;

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/android/server/policy/MiuiPhoneWindowManager;->mKeyguard:Landroid/view/WindowManagerPolicy$WindowState;

    invoke-interface {v3, v4}, Landroid/view/WindowManagerPolicy$WindowState;->hideLw(Z)Z

    :cond_3
    return v4

    :cond_4
    if-eqz v0, :cond_7

    iput-boolean p1, p0, Lcom/android/server/policy/MiuiPhoneWindowManager;->mKeyguardOccluded:Z

    iget-object v3, p0, Lcom/android/server/policy/MiuiPhoneWindowManager;->mMiuiKeyguardDelegate:Lcom/android/server/policy/MiuiKeyguardServiceDelegate;

    iget-object v3, v3, Lcom/android/server/policy/MiuiKeyguardServiceDelegate;->mKeyguardDelegate:Lcom/android/server/policy/keyguard/KeyguardServiceDelegate;

    invoke-virtual {v3, p1, v5}, Lcom/android/server/policy/keyguard/KeyguardServiceDelegate;->setOccluded(ZZ)V

    iget-object v3, p0, Lcom/android/server/policy/MiuiPhoneWindowManager;->mKeyguard:Landroid/view/WindowManagerPolicy$WindowState;

    if-eqz v3, :cond_5

    if-eqz p1, :cond_6

    iget-object v3, p0, Lcom/android/server/policy/MiuiPhoneWindowManager;->mKeyguard:Landroid/view/WindowManagerPolicy$WindowState;

    invoke-interface {v3, v4}, Landroid/view/WindowManagerPolicy$WindowState;->hideLw(Z)Z

    :cond_5
    :goto_1
    return v5

    :cond_6
    iget-object v3, p0, Lcom/android/server/policy/MiuiPhoneWindowManager;->mKeyguard:Landroid/view/WindowManagerPolicy$WindowState;

    invoke-interface {v3, v4}, Landroid/view/WindowManagerPolicy$WindowState;->showLw(Z)Z

    goto :goto_1

    :cond_7
    return v5
.end method

.method public startedGoingToSleep(I)V
    .locals 0

    invoke-virtual {p0, p1}, Lcom/android/server/policy/MiuiPhoneWindowManager;->screenTurnedOffInternal(I)V

    invoke-super {p0, p1}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->startedGoingToSleep(I)V

    return-void
.end method

.method protected stopLockTaskMode()Z
    .locals 3

    :try_start_0
    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {v0}, Landroid/app/IActivityManager;->isInLockTaskMode()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Landroid/app/IActivityManager;->stopSystemLockTaskMode()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    const/4 v2, 0x1

    return v2

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    :cond_0
    const/4 v2, 0x0

    return v2
.end method

.method public systemReady()V
    .locals 3

    invoke-super {p0}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->systemReady()V

    new-instance v0, Lcom/android/server/policy/MiuiKeyguardServiceDelegate;

    iget-object v1, p0, Lcom/android/server/policy/MiuiPhoneWindowManager;->mKeyguardDelegate:Lcom/android/server/policy/keyguard/KeyguardServiceDelegate;

    iget-object v2, p0, Lcom/android/server/policy/MiuiPhoneWindowManager;->mPowerManager:Landroid/os/PowerManager;

    invoke-direct {v0, p0, v1, v2}, Lcom/android/server/policy/MiuiKeyguardServiceDelegate;-><init>(Lcom/android/server/policy/PhoneWindowManager;Lcom/android/server/policy/keyguard/KeyguardServiceDelegate;Landroid/os/PowerManager;)V

    iput-object v0, p0, Lcom/android/server/policy/MiuiPhoneWindowManager;->mMiuiKeyguardDelegate:Lcom/android/server/policy/MiuiKeyguardServiceDelegate;

    invoke-virtual {p0}, Lcom/android/server/policy/MiuiPhoneWindowManager;->systemReadyInternal()V

    return-void
.end method

.method protected toggleSplitScreenInternal()V
    .locals 1

    invoke-virtual {p0}, Lcom/android/server/policy/MiuiPhoneWindowManager;->getStatusBarManagerInternal()Lcom/android/server/statusbar/StatusBarManagerInternal;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lcom/android/server/statusbar/StatusBarManagerInternal;->toggleSplitScreen()V

    :cond_0
    return-void
.end method

.method protected updateNavigationBarWidth()V
    .locals 6

    iget-object v1, p0, Lcom/android/server/policy/MiuiPhoneWindowManager;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iget-object v2, p0, Lcom/android/server/policy/MiuiPhoneWindowManager;->mNavigationBarWidthForRotationDefault:[I

    iget v3, p0, Lcom/android/server/policy/MiuiPhoneWindowManager;->mPortraitRotation:I

    iget-boolean v1, p0, Lcom/android/server/policy/MiuiPhoneWindowManager;->mSupportFsg:Z

    if-eqz v1, :cond_0

    const/4 v1, 0x0

    :goto_0
    iget-object v4, p0, Lcom/android/server/policy/MiuiPhoneWindowManager;->mNavigationBarWidthForRotationDefault:[I

    iget v5, p0, Lcom/android/server/policy/MiuiPhoneWindowManager;->mSeascapeRotation:I

    aput v1, v4, v5

    iget-object v4, p0, Lcom/android/server/policy/MiuiPhoneWindowManager;->mNavigationBarWidthForRotationDefault:[I

    iget v5, p0, Lcom/android/server/policy/MiuiPhoneWindowManager;->mLandscapeRotation:I

    aput v1, v4, v5

    iget-object v4, p0, Lcom/android/server/policy/MiuiPhoneWindowManager;->mNavigationBarWidthForRotationDefault:[I

    iget v5, p0, Lcom/android/server/policy/MiuiPhoneWindowManager;->mUpsideDownRotation:I

    aput v1, v4, v5

    aput v1, v2, v3

    return-void

    :cond_0
    const v1, 0x10500fd

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    goto :goto_0
.end method
