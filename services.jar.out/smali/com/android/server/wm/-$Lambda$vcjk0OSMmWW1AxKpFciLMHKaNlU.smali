.class final synthetic Lcom/android/server/wm/-$Lambda$vcjk0OSMmWW1AxKpFciLMHKaNlU;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/util/function/Consumer;


# instance fields
.field private final synthetic $id:B

.field private final synthetic -$f0:Ljava/lang/Object;


# direct methods
.method private final synthetic $m$0(Ljava/lang/Object;)V
    .locals 1

    iget-object v0, p0, Lcom/android/server/wm/-$Lambda$vcjk0OSMmWW1AxKpFciLMHKaNlU;->-$f0:Ljava/lang/Object;

    check-cast v0, Landroid/util/SparseArray;

    check-cast p1, Lcom/android/server/wm/WindowState;

    invoke-static {v0, p1}, Lcom/android/server/wm/AccessibilityController$DisplayMagnifier$MagnifiedViewport;->lambda$-com_android_server_wm_AccessibilityController$DisplayMagnifier$MagnifiedViewport_30001(Landroid/util/SparseArray;Lcom/android/server/wm/WindowState;)V

    return-void
.end method

.method private final synthetic $m$1(Ljava/lang/Object;)V
    .locals 1

    iget-object v0, p0, Lcom/android/server/wm/-$Lambda$vcjk0OSMmWW1AxKpFciLMHKaNlU;->-$f0:Ljava/lang/Object;

    check-cast v0, Landroid/util/SparseArray;

    check-cast p1, Lcom/android/server/wm/WindowState;

    invoke-static {v0, p1}, Lcom/android/server/wm/AccessibilityController$WindowsForAccessibilityObserver;->lambda$-com_android_server_wm_AccessibilityController$WindowsForAccessibilityObserver_59257(Landroid/util/SparseArray;Lcom/android/server/wm/WindowState;)V

    return-void
.end method

.method private final synthetic $m$10(Ljava/lang/Object;)V
    .locals 1

    iget-object v0, p0, Lcom/android/server/wm/-$Lambda$vcjk0OSMmWW1AxKpFciLMHKaNlU;->-$f0:Ljava/lang/Object;

    check-cast v0, Lcom/android/server/wm/DisplayContent;

    check-cast p1, Lcom/android/server/wm/WindowState;

    invoke-virtual {v0, p1}, Lcom/android/server/wm/DisplayContent;->lambda$-com_android_server_wm_DisplayContent_104340(Lcom/android/server/wm/WindowState;)V

    return-void
.end method

.method private final synthetic $m$11(Ljava/lang/Object;)V
    .locals 1

    iget-object v0, p0, Lcom/android/server/wm/-$Lambda$vcjk0OSMmWW1AxKpFciLMHKaNlU;->-$f0:Ljava/lang/Object;

    check-cast v0, Ljava/io/FileDescriptor;

    check-cast p1, Lcom/android/server/wm/WindowState;

    invoke-static {v0, p1}, Lcom/android/server/wm/DisplayContent;->lambda$-com_android_server_wm_DisplayContent_114456(Ljava/io/FileDescriptor;Lcom/android/server/wm/WindowState;)V

    return-void
.end method

.method private final synthetic $m$12(Ljava/lang/Object;)V
    .locals 1

    iget-object v0, p0, Lcom/android/server/wm/-$Lambda$vcjk0OSMmWW1AxKpFciLMHKaNlU;->-$f0:Ljava/lang/Object;

    check-cast v0, Lcom/android/server/wm/DisplayContent;

    check-cast p1, Lcom/android/server/wm/WindowState;

    invoke-virtual {v0, p1}, Lcom/android/server/wm/DisplayContent;->lambda$-com_android_server_wm_DisplayContent_143297(Lcom/android/server/wm/WindowState;)V

    return-void
.end method

.method private final synthetic $m$13(Ljava/lang/Object;)V
    .locals 1

    iget-object v0, p0, Lcom/android/server/wm/-$Lambda$vcjk0OSMmWW1AxKpFciLMHKaNlU;->-$f0:Ljava/lang/Object;

    check-cast v0, Lcom/android/server/wm/DisplayContent;

    check-cast p1, Lcom/android/server/wm/WindowState;

    invoke-virtual {v0, p1}, Lcom/android/server/wm/DisplayContent;->lambda$-com_android_server_wm_DisplayContent_120874(Lcom/android/server/wm/WindowState;)V

    return-void
.end method

.method private final synthetic $m$14(Ljava/lang/Object;)V
    .locals 1

    iget-object v0, p0, Lcom/android/server/wm/-$Lambda$vcjk0OSMmWW1AxKpFciLMHKaNlU;->-$f0:Ljava/lang/Object;

    check-cast v0, Lcom/android/server/wm/GestureStubController;

    check-cast p1, Lcom/android/server/wm/WindowState;

    invoke-virtual {v0, p1}, Lcom/android/server/wm/GestureStubController;->lambda$-com_android_server_wm_GestureStubController_3147(Lcom/android/server/wm/WindowState;)V

    return-void
.end method

.method private final synthetic $m$15(Ljava/lang/Object;)V
    .locals 1

    iget-object v0, p0, Lcom/android/server/wm/-$Lambda$vcjk0OSMmWW1AxKpFciLMHKaNlU;->-$f0:Ljava/lang/Object;

    check-cast v0, Lcom/android/server/wm/GestureStubController;

    check-cast p1, Lcom/android/server/wm/WindowState;

    invoke-virtual {v0, p1}, Lcom/android/server/wm/GestureStubController;->lambda$-com_android_server_wm_GestureStubController_3945(Lcom/android/server/wm/WindowState;)V

    return-void
.end method

.method private final synthetic $m$16(Ljava/lang/Object;)V
    .locals 1

    iget-object v0, p0, Lcom/android/server/wm/-$Lambda$vcjk0OSMmWW1AxKpFciLMHKaNlU;->-$f0:Ljava/lang/Object;

    check-cast v0, Lcom/android/server/wm/RootWindowContainer;

    check-cast p1, Lcom/android/server/wm/WindowState;

    invoke-virtual {v0, p1}, Lcom/android/server/wm/RootWindowContainer;->lambda$-com_android_server_wm_RootWindowContainer_7116(Lcom/android/server/wm/WindowState;)V

    return-void
.end method

.method private final synthetic $m$17(Ljava/lang/Object;)V
    .locals 1

    iget-object v0, p0, Lcom/android/server/wm/-$Lambda$vcjk0OSMmWW1AxKpFciLMHKaNlU;->-$f0:Ljava/lang/Object;

    check-cast v0, Lcom/android/server/wm/RootWindowContainer;

    check-cast p1, Lcom/android/server/wm/WindowState;

    invoke-virtual {v0, p1}, Lcom/android/server/wm/RootWindowContainer;->lambda$-com_android_server_wm_RootWindowContainer_18126(Lcom/android/server/wm/WindowState;)V

    return-void
.end method

.method private final synthetic $m$18(Ljava/lang/Object;)V
    .locals 1

    iget-object v0, p0, Lcom/android/server/wm/-$Lambda$vcjk0OSMmWW1AxKpFciLMHKaNlU;->-$f0:Ljava/lang/Object;

    check-cast v0, Lcom/android/server/wm/TaskSnapshotController;

    check-cast p1, Lcom/android/server/wm/Task;

    invoke-virtual {v0, p1}, Lcom/android/server/wm/TaskSnapshotController;->lambda$-com_android_server_wm_TaskSnapshotController_13692(Lcom/android/server/wm/Task;)V

    return-void
.end method

.method private final synthetic $m$19(Ljava/lang/Object;)V
    .locals 1

    iget-object v0, p0, Lcom/android/server/wm/-$Lambda$vcjk0OSMmWW1AxKpFciLMHKaNlU;->-$f0:Ljava/lang/Object;

    check-cast v0, Lcom/android/server/wm/WindowLayersController;

    check-cast p1, Lcom/android/server/wm/WindowState;

    invoke-virtual {v0, p1}, Lcom/android/server/wm/WindowLayersController;->lambda$-com_android_server_wm_WindowLayersController_3664(Lcom/android/server/wm/WindowState;)V

    return-void
.end method

.method private final synthetic $m$2(Ljava/lang/Object;)V
    .locals 1

    iget-object v0, p0, Lcom/android/server/wm/-$Lambda$vcjk0OSMmWW1AxKpFciLMHKaNlU;->-$f0:Ljava/lang/Object;

    check-cast v0, Lcom/android/server/wm/WindowManagerService;

    check-cast p1, Lcom/android/server/wm/WindowState;

    invoke-static {v0, p1}, Lcom/android/server/wm/AppWindowToken;->-com_android_server_wm_AppWindowToken-mthref-0(Lcom/android/server/wm/WindowManagerService;Lcom/android/server/wm/WindowState;)V

    return-void
.end method

.method private final synthetic $m$20(Ljava/lang/Object;)V
    .locals 1

    iget-object v0, p0, Lcom/android/server/wm/-$Lambda$vcjk0OSMmWW1AxKpFciLMHKaNlU;->-$f0:Ljava/lang/Object;

    check-cast v0, Ljava/io/PrintWriter;

    check-cast p1, Lcom/android/server/wm/WindowState;

    invoke-static {v0, p1}, Lcom/android/server/wm/WindowManagerService;->lambda$-com_android_server_wm_WindowManagerService_300910(Ljava/io/PrintWriter;Lcom/android/server/wm/WindowState;)V

    return-void
.end method

.method private final synthetic $m$21(Ljava/lang/Object;)V
    .locals 1

    iget-object v0, p0, Lcom/android/server/wm/-$Lambda$vcjk0OSMmWW1AxKpFciLMHKaNlU;->-$f0:Ljava/lang/Object;

    check-cast v0, Ljava/util/ArrayList;

    check-cast p1, Lcom/android/server/wm/WindowState;

    invoke-static {v0, p1}, Lcom/android/server/wm/WindowManagerService;->lambda$-com_android_server_wm_WindowManagerService_187307(Ljava/util/ArrayList;Lcom/android/server/wm/WindowState;)V

    return-void
.end method

.method private final synthetic $m$3(Ljava/lang/Object;)V
    .locals 1

    iget-object v0, p0, Lcom/android/server/wm/-$Lambda$vcjk0OSMmWW1AxKpFciLMHKaNlU;->-$f0:Ljava/lang/Object;

    check-cast v0, Lcom/android/server/wm/DisplayContent;

    check-cast p1, Lcom/android/server/wm/WindowState;

    invoke-virtual {v0, p1}, Lcom/android/server/wm/DisplayContent;->lambda$-com_android_server_wm_DisplayContent_16532(Lcom/android/server/wm/WindowState;)V

    return-void
.end method

.method private final synthetic $m$4(Ljava/lang/Object;)V
    .locals 1

    iget-object v0, p0, Lcom/android/server/wm/-$Lambda$vcjk0OSMmWW1AxKpFciLMHKaNlU;->-$f0:Ljava/lang/Object;

    check-cast v0, Lcom/android/server/wm/DisplayContent;

    check-cast p1, Lcom/android/server/wm/WindowState;

    invoke-virtual {v0, p1}, Lcom/android/server/wm/DisplayContent;->lambda$-com_android_server_wm_DisplayContent_18845(Lcom/android/server/wm/WindowState;)V

    return-void
.end method

.method private final synthetic $m$5(Ljava/lang/Object;)V
    .locals 1

    iget-object v0, p0, Lcom/android/server/wm/-$Lambda$vcjk0OSMmWW1AxKpFciLMHKaNlU;->-$f0:Ljava/lang/Object;

    check-cast v0, Lcom/android/server/wm/DisplayContent;

    check-cast p1, Lcom/android/server/wm/WindowState;

    invoke-virtual {v0, p1}, Lcom/android/server/wm/DisplayContent;->lambda$-com_android_server_wm_DisplayContent_20794(Lcom/android/server/wm/WindowState;)V

    return-void
.end method

.method private final synthetic $m$6(Ljava/lang/Object;)V
    .locals 1

    iget-object v0, p0, Lcom/android/server/wm/-$Lambda$vcjk0OSMmWW1AxKpFciLMHKaNlU;->-$f0:Ljava/lang/Object;

    check-cast v0, Lcom/android/server/wm/DisplayContent;

    check-cast p1, Lcom/android/server/wm/WindowState;

    invoke-virtual {v0, p1}, Lcom/android/server/wm/DisplayContent;->lambda$-com_android_server_wm_DisplayContent_23451(Lcom/android/server/wm/WindowState;)V

    return-void
.end method

.method private final synthetic $m$7(Ljava/lang/Object;)V
    .locals 1

    iget-object v0, p0, Lcom/android/server/wm/-$Lambda$vcjk0OSMmWW1AxKpFciLMHKaNlU;->-$f0:Ljava/lang/Object;

    check-cast v0, Lcom/android/server/wm/DisplayContent;

    check-cast p1, Lcom/android/server/wm/WindowState;

    invoke-virtual {v0, p1}, Lcom/android/server/wm/DisplayContent;->lambda$-com_android_server_wm_DisplayContent_26994(Lcom/android/server/wm/WindowState;)V

    return-void
.end method

.method private final synthetic $m$8(Ljava/lang/Object;)V
    .locals 1

    iget-object v0, p0, Lcom/android/server/wm/-$Lambda$vcjk0OSMmWW1AxKpFciLMHKaNlU;->-$f0:Ljava/lang/Object;

    check-cast v0, Lcom/android/server/wm/DisplayContent;

    check-cast p1, Lcom/android/server/wm/WindowState;

    invoke-virtual {v0, p1}, Lcom/android/server/wm/DisplayContent;->lambda$-com_android_server_wm_DisplayContent_29018(Lcom/android/server/wm/WindowState;)V

    return-void
.end method

.method private final synthetic $m$9(Ljava/lang/Object;)V
    .locals 1

    iget-object v0, p0, Lcom/android/server/wm/-$Lambda$vcjk0OSMmWW1AxKpFciLMHKaNlU;->-$f0:Ljava/lang/Object;

    check-cast v0, Lcom/android/server/wm/DisplayContent;

    check-cast p1, Lcom/android/server/wm/WindowState;

    invoke-virtual {v0, p1}, Lcom/android/server/wm/DisplayContent;->lambda$-com_android_server_wm_DisplayContent_29223(Lcom/android/server/wm/WindowState;)V

    return-void
.end method

.method public synthetic constructor <init>(BLjava/lang/Object;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-byte p1, p0, Lcom/android/server/wm/-$Lambda$vcjk0OSMmWW1AxKpFciLMHKaNlU;->$id:B

    iput-object p2, p0, Lcom/android/server/wm/-$Lambda$vcjk0OSMmWW1AxKpFciLMHKaNlU;->-$f0:Ljava/lang/Object;

    return-void
.end method


# virtual methods
.method public final accept(Ljava/lang/Object;)V
    .locals 1

    iget-byte v0, p0, Lcom/android/server/wm/-$Lambda$vcjk0OSMmWW1AxKpFciLMHKaNlU;->$id:B

    packed-switch v0, :pswitch_data_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :pswitch_0
    invoke-direct {p0, p1}, Lcom/android/server/wm/-$Lambda$vcjk0OSMmWW1AxKpFciLMHKaNlU;->$m$0(Ljava/lang/Object;)V

    return-void

    :pswitch_1
    invoke-direct {p0, p1}, Lcom/android/server/wm/-$Lambda$vcjk0OSMmWW1AxKpFciLMHKaNlU;->$m$1(Ljava/lang/Object;)V

    return-void

    :pswitch_2
    invoke-direct {p0, p1}, Lcom/android/server/wm/-$Lambda$vcjk0OSMmWW1AxKpFciLMHKaNlU;->$m$2(Ljava/lang/Object;)V

    return-void

    :pswitch_3
    invoke-direct {p0, p1}, Lcom/android/server/wm/-$Lambda$vcjk0OSMmWW1AxKpFciLMHKaNlU;->$m$3(Ljava/lang/Object;)V

    return-void

    :pswitch_4
    invoke-direct {p0, p1}, Lcom/android/server/wm/-$Lambda$vcjk0OSMmWW1AxKpFciLMHKaNlU;->$m$4(Ljava/lang/Object;)V

    return-void

    :pswitch_5
    invoke-direct {p0, p1}, Lcom/android/server/wm/-$Lambda$vcjk0OSMmWW1AxKpFciLMHKaNlU;->$m$5(Ljava/lang/Object;)V

    return-void

    :pswitch_6
    invoke-direct {p0, p1}, Lcom/android/server/wm/-$Lambda$vcjk0OSMmWW1AxKpFciLMHKaNlU;->$m$6(Ljava/lang/Object;)V

    return-void

    :pswitch_7
    invoke-direct {p0, p1}, Lcom/android/server/wm/-$Lambda$vcjk0OSMmWW1AxKpFciLMHKaNlU;->$m$7(Ljava/lang/Object;)V

    return-void

    :pswitch_8
    invoke-direct {p0, p1}, Lcom/android/server/wm/-$Lambda$vcjk0OSMmWW1AxKpFciLMHKaNlU;->$m$8(Ljava/lang/Object;)V

    return-void

    :pswitch_9
    invoke-direct {p0, p1}, Lcom/android/server/wm/-$Lambda$vcjk0OSMmWW1AxKpFciLMHKaNlU;->$m$9(Ljava/lang/Object;)V

    return-void

    :pswitch_a
    invoke-direct {p0, p1}, Lcom/android/server/wm/-$Lambda$vcjk0OSMmWW1AxKpFciLMHKaNlU;->$m$10(Ljava/lang/Object;)V

    return-void

    :pswitch_b
    invoke-direct {p0, p1}, Lcom/android/server/wm/-$Lambda$vcjk0OSMmWW1AxKpFciLMHKaNlU;->$m$11(Ljava/lang/Object;)V

    return-void

    :pswitch_c
    invoke-direct {p0, p1}, Lcom/android/server/wm/-$Lambda$vcjk0OSMmWW1AxKpFciLMHKaNlU;->$m$12(Ljava/lang/Object;)V

    return-void

    :pswitch_d
    invoke-direct {p0, p1}, Lcom/android/server/wm/-$Lambda$vcjk0OSMmWW1AxKpFciLMHKaNlU;->$m$13(Ljava/lang/Object;)V

    return-void

    :pswitch_e
    invoke-direct {p0, p1}, Lcom/android/server/wm/-$Lambda$vcjk0OSMmWW1AxKpFciLMHKaNlU;->$m$14(Ljava/lang/Object;)V

    return-void

    :pswitch_f
    invoke-direct {p0, p1}, Lcom/android/server/wm/-$Lambda$vcjk0OSMmWW1AxKpFciLMHKaNlU;->$m$15(Ljava/lang/Object;)V

    return-void

    :pswitch_10
    invoke-direct {p0, p1}, Lcom/android/server/wm/-$Lambda$vcjk0OSMmWW1AxKpFciLMHKaNlU;->$m$16(Ljava/lang/Object;)V

    return-void

    :pswitch_11
    invoke-direct {p0, p1}, Lcom/android/server/wm/-$Lambda$vcjk0OSMmWW1AxKpFciLMHKaNlU;->$m$17(Ljava/lang/Object;)V

    return-void

    :pswitch_12
    invoke-direct {p0, p1}, Lcom/android/server/wm/-$Lambda$vcjk0OSMmWW1AxKpFciLMHKaNlU;->$m$18(Ljava/lang/Object;)V

    return-void

    :pswitch_13
    invoke-direct {p0, p1}, Lcom/android/server/wm/-$Lambda$vcjk0OSMmWW1AxKpFciLMHKaNlU;->$m$19(Ljava/lang/Object;)V

    return-void

    :pswitch_14
    invoke-direct {p0, p1}, Lcom/android/server/wm/-$Lambda$vcjk0OSMmWW1AxKpFciLMHKaNlU;->$m$20(Ljava/lang/Object;)V

    return-void

    :pswitch_15
    invoke-direct {p0, p1}, Lcom/android/server/wm/-$Lambda$vcjk0OSMmWW1AxKpFciLMHKaNlU;->$m$21(Ljava/lang/Object;)V

    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_11
        :pswitch_12
        :pswitch_13
        :pswitch_14
        :pswitch_15
    .end packed-switch
.end method
