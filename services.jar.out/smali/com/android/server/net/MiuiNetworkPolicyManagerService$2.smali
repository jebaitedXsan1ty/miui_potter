.class Lcom/android/server/net/MiuiNetworkPolicyManagerService$2;
.super Ljava/lang/Object;
.source "MiuiNetworkPolicyManagerService.java"

# interfaces
.implements Landroid/os/Handler$Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/net/MiuiNetworkPolicyManagerService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/net/MiuiNetworkPolicyManagerService;


# direct methods
.method constructor <init>(Lcom/android/server/net/MiuiNetworkPolicyManagerService;)V
    .locals 0

    iput-object p1, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService$2;->this$0:Lcom/android/server/net/MiuiNetworkPolicyManagerService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)Z
    .locals 12

    const-wide/16 v10, 0x61a8

    const/4 v8, 0x5

    const/4 v4, 0x4

    const/4 v7, 0x0

    const/4 v6, 0x1

    iget v3, p1, Landroid/os/Message;->what:I

    packed-switch v3, :pswitch_data_0

    :goto_0
    return v7

    :pswitch_0
    iget-object v3, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService$2;->this$0:Lcom/android/server/net/MiuiNetworkPolicyManagerService;

    iget v4, p1, Landroid/os/Message;->arg1:I

    iget v5, p1, Landroid/os/Message;->arg2:I

    invoke-static {v3, v4, v5}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->-wrap12(Lcom/android/server/net/MiuiNetworkPolicyManagerService;II)V

    goto :goto_0

    :pswitch_1
    iget-object v3, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService$2;->this$0:Lcom/android/server/net/MiuiNetworkPolicyManagerService;

    iget v4, p1, Landroid/os/Message;->arg1:I

    invoke-static {v3, v4}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->-wrap10(Lcom/android/server/net/MiuiNetworkPolicyManagerService;I)V

    goto :goto_0

    :pswitch_2
    iget v2, p1, Landroid/os/Message;->arg1:I

    iget v3, p1, Landroid/os/Message;->arg2:I

    if-ne v3, v6, :cond_1

    const/4 v0, 0x1

    :goto_1
    if-eqz v0, :cond_2

    iget-object v3, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService$2;->this$0:Lcom/android/server/net/MiuiNetworkPolicyManagerService;

    invoke-static {v3}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->-get2(Lcom/android/server/net/MiuiNetworkPolicyManagerService;)Z

    move-result v3

    xor-int/lit8 v3, v3, 0x1

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService$2;->this$0:Lcom/android/server/net/MiuiNetworkPolicyManagerService;

    invoke-static {v3}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->-get1(Lcom/android/server/net/MiuiNetworkPolicyManagerService;)Landroid/os/Handler;

    move-result-object v3

    invoke-virtual {v3, v8}, Landroid/os/Handler;->removeMessages(I)V

    iget-object v3, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService$2;->this$0:Lcom/android/server/net/MiuiNetworkPolicyManagerService;

    invoke-static {v3}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->-get1(Lcom/android/server/net/MiuiNetworkPolicyManagerService;)Landroid/os/Handler;

    move-result-object v3

    invoke-virtual {v3, v4, v10, v11}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    iget-object v3, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService$2;->this$0:Lcom/android/server/net/MiuiNetworkPolicyManagerService;

    invoke-static {v3, v6}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->-wrap11(Lcom/android/server/net/MiuiNetworkPolicyManagerService;Z)V

    :cond_0
    :goto_2
    return v6

    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    :cond_2
    if-nez v0, :cond_0

    iget-object v3, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService$2;->this$0:Lcom/android/server/net/MiuiNetworkPolicyManagerService;

    invoke-static {v3}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->-get1(Lcom/android/server/net/MiuiNetworkPolicyManagerService;)Landroid/os/Handler;

    move-result-object v3

    invoke-virtual {v3, v8}, Landroid/os/Handler;->removeMessages(I)V

    iget-object v3, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService$2;->this$0:Lcom/android/server/net/MiuiNetworkPolicyManagerService;

    invoke-static {v3}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->-get1(Lcom/android/server/net/MiuiNetworkPolicyManagerService;)Landroid/os/Handler;

    move-result-object v3

    invoke-virtual {v3, v4}, Landroid/os/Handler;->removeMessages(I)V

    iget-object v3, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService$2;->this$0:Lcom/android/server/net/MiuiNetworkPolicyManagerService;

    invoke-static {v3}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->-get2(Lcom/android/server/net/MiuiNetworkPolicyManagerService;)Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService$2;->this$0:Lcom/android/server/net/MiuiNetworkPolicyManagerService;

    invoke-static {v3, v7}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->-wrap11(Lcom/android/server/net/MiuiNetworkPolicyManagerService;Z)V

    goto :goto_2

    :pswitch_3
    iget-object v3, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService$2;->this$0:Lcom/android/server/net/MiuiNetworkPolicyManagerService;

    iget v4, p1, Landroid/os/Message;->arg1:I

    invoke-static {v3, v4}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->-set2(Lcom/android/server/net/MiuiNetworkPolicyManagerService;I)I

    iget-object v3, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService$2;->this$0:Lcom/android/server/net/MiuiNetworkPolicyManagerService;

    invoke-static {v3}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->-wrap2(Lcom/android/server/net/MiuiNetworkPolicyManagerService;)I

    move-result v1

    const-string/jumbo v3, "MiuiNetworkPolicy"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "networkPriorityMode = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, " mNetworkPriorityMode ="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService$2;->this$0:Lcom/android/server/net/MiuiNetworkPolicyManagerService;

    invoke-static {v5}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->-get7(Lcom/android/server/net/MiuiNetworkPolicyManagerService;)I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, " mWifiConnected="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService$2;->this$0:Lcom/android/server/net/MiuiNetworkPolicyManagerService;

    invoke-static {v5}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->-get10(Lcom/android/server/net/MiuiNetworkPolicyManagerService;)Z

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v3, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService$2;->this$0:Lcom/android/server/net/MiuiNetworkPolicyManagerService;

    invoke-static {v3}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->-get10(Lcom/android/server/net/MiuiNetworkPolicyManagerService;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService$2;->this$0:Lcom/android/server/net/MiuiNetworkPolicyManagerService;

    invoke-static {v3}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->-get7(Lcom/android/server/net/MiuiNetworkPolicyManagerService;)I

    move-result v3

    if-eq v1, v3, :cond_3

    iget-object v3, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService$2;->this$0:Lcom/android/server/net/MiuiNetworkPolicyManagerService;

    invoke-static {v3, v1}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->-wrap7(Lcom/android/server/net/MiuiNetworkPolicyManagerService;I)V

    :cond_3
    return v6

    :pswitch_4
    iget-object v3, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService$2;->this$0:Lcom/android/server/net/MiuiNetworkPolicyManagerService;

    invoke-static {v3}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->-get2(Lcom/android/server/net/MiuiNetworkPolicyManagerService;)Z

    move-result v3

    if-eqz v3, :cond_4

    iget-object v3, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService$2;->this$0:Lcom/android/server/net/MiuiNetworkPolicyManagerService;

    invoke-static {v3, v7}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->-wrap11(Lcom/android/server/net/MiuiNetworkPolicyManagerService;Z)V

    iget-object v3, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService$2;->this$0:Lcom/android/server/net/MiuiNetworkPolicyManagerService;

    invoke-static {v3}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->-get1(Lcom/android/server/net/MiuiNetworkPolicyManagerService;)Landroid/os/Handler;

    move-result-object v3

    const-wide/16 v4, 0x1388

    invoke-virtual {v3, v8, v4, v5}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    :cond_4
    return v6

    :pswitch_5
    iget-object v3, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService$2;->this$0:Lcom/android/server/net/MiuiNetworkPolicyManagerService;

    invoke-static {v3}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->-get2(Lcom/android/server/net/MiuiNetworkPolicyManagerService;)Z

    move-result v3

    if-nez v3, :cond_5

    iget-object v3, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService$2;->this$0:Lcom/android/server/net/MiuiNetworkPolicyManagerService;

    invoke-static {v3, v6}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->-wrap11(Lcom/android/server/net/MiuiNetworkPolicyManagerService;Z)V

    iget-object v3, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService$2;->this$0:Lcom/android/server/net/MiuiNetworkPolicyManagerService;

    invoke-static {v3}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->-get1(Lcom/android/server/net/MiuiNetworkPolicyManagerService;)Landroid/os/Handler;

    move-result-object v3

    invoke-virtual {v3, v4, v10, v11}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    :cond_5
    return v6

    :pswitch_6
    iget-object v3, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService$2;->this$0:Lcom/android/server/net/MiuiNetworkPolicyManagerService;

    invoke-static {v3}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->-wrap6(Lcom/android/server/net/MiuiNetworkPolicyManagerService;)V

    iget-object v3, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService$2;->this$0:Lcom/android/server/net/MiuiNetworkPolicyManagerService;

    invoke-static {v3}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->-get1(Lcom/android/server/net/MiuiNetworkPolicyManagerService;)Landroid/os/Handler;

    move-result-object v3

    const-wide/16 v4, 0xbb8

    const/4 v6, 0x6

    invoke-virtual {v3, v6, v4, v5}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_3
    .end packed-switch
.end method
