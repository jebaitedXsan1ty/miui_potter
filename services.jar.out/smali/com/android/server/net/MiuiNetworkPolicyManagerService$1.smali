.class Lcom/android/server/net/MiuiNetworkPolicyManagerService$1;
.super Landroid/content/BroadcastReceiver;
.source "MiuiNetworkPolicyManagerService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/net/MiuiNetworkPolicyManagerService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/net/MiuiNetworkPolicyManagerService;


# direct methods
.method constructor <init>(Lcom/android/server/net/MiuiNetworkPolicyManagerService;)V
    .locals 0

    iput-object p1, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService$1;->this$0:Lcom/android/server/net/MiuiNetworkPolicyManagerService;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 7

    const/4 v4, 0x0

    const-string/jumbo v3, "networkInfo"

    invoke-virtual {p2, v3}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/NetworkInfo;

    iget-object v3, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService$1;->this$0:Lcom/android/server/net/MiuiNetworkPolicyManagerService;

    invoke-static {v3}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->-get10(Lcom/android/server/net/MiuiNetworkPolicyManagerService;)Z

    move-result v2

    iget-object v5, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService$1;->this$0:Lcom/android/server/net/MiuiNetworkPolicyManagerService;

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v3

    :goto_0
    invoke-static {v5, v3}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->-set4(Lcom/android/server/net/MiuiNetworkPolicyManagerService;Z)Z

    const-string/jumbo v3, "MiuiNetworkPolicy"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "wasConnected = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, " mWifiConnected = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService$1;->this$0:Lcom/android/server/net/MiuiNetworkPolicyManagerService;

    invoke-static {v6}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->-get10(Lcom/android/server/net/MiuiNetworkPolicyManagerService;)Z

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, " mNetworkPriorityMode ="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService$1;->this$0:Lcom/android/server/net/MiuiNetworkPolicyManagerService;

    invoke-static {v6}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->-get7(Lcom/android/server/net/MiuiNetworkPolicyManagerService;)I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v3, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService$1;->this$0:Lcom/android/server/net/MiuiNetworkPolicyManagerService;

    invoke-static {v3}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->-get10(Lcom/android/server/net/MiuiNetworkPolicyManagerService;)Z

    move-result v3

    if-eq v3, v2, :cond_1

    iget-object v3, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService$1;->this$0:Lcom/android/server/net/MiuiNetworkPolicyManagerService;

    invoke-static {v3, v4}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->-wrap8(Lcom/android/server/net/MiuiNetworkPolicyManagerService;Z)V

    iget-object v3, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService$1;->this$0:Lcom/android/server/net/MiuiNetworkPolicyManagerService;

    invoke-static {v3}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->-wrap2(Lcom/android/server/net/MiuiNetworkPolicyManagerService;)I

    move-result v1

    iget-object v3, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService$1;->this$0:Lcom/android/server/net/MiuiNetworkPolicyManagerService;

    invoke-static {v3, v1}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->-wrap1(Lcom/android/server/net/MiuiNetworkPolicyManagerService;I)Z

    move-result v3

    if-nez v3, :cond_0

    iget-object v3, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService$1;->this$0:Lcom/android/server/net/MiuiNetworkPolicyManagerService;

    invoke-static {v3, v1}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->-wrap0(Lcom/android/server/net/MiuiNetworkPolicyManagerService;I)Z

    move-result v3

    if-eqz v3, :cond_1

    :cond_0
    iget-object v3, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService$1;->this$0:Lcom/android/server/net/MiuiNetworkPolicyManagerService;

    iget-object v4, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService$1;->this$0:Lcom/android/server/net/MiuiNetworkPolicyManagerService;

    invoke-static {v4}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->-get10(Lcom/android/server/net/MiuiNetworkPolicyManagerService;)Z

    move-result v4

    if-eqz v4, :cond_3

    :goto_1
    invoke-static {v3, v1}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->-wrap7(Lcom/android/server/net/MiuiNetworkPolicyManagerService;I)V

    :cond_1
    return-void

    :cond_2
    move v3, v4

    goto :goto_0

    :cond_3
    const/16 v1, 0xff

    goto :goto_1
.end method
