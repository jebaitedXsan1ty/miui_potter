.class public Lcom/android/server/pm/DefaultPermissionGrantPolicyInjector;
.super Ljava/lang/Object;
.source "DefaultPermissionGrantPolicyInjector.java"


# static fields
.field private static INCALL_UI:Ljava/lang/String; = null

.field private static final MIUI_APPS:[Ljava/lang/String;

.field private static final MIUI_SYSTEM_APPS:[Ljava/lang/String;

.field private static final RUNTIME_PERMISSIONS:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final RUNTIME_PERMSSION_PROPTERY:Ljava/lang/String; = "persist.sys.runtime_perm"

.field private static final STATE_DEF:I = -0x1

.field private static final STATE_GRANT:I = 0x0

.field private static final STATE_REVOKE:I = 0x1


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    const-string/jumbo v0, "com.android.incallui"

    sput-object v0, Lcom/android/server/pm/DefaultPermissionGrantPolicyInjector;->INCALL_UI:Ljava/lang/String;

    new-instance v0, Landroid/util/ArraySet;

    invoke-direct {v0}, Landroid/util/ArraySet;-><init>()V

    sput-object v0, Lcom/android/server/pm/DefaultPermissionGrantPolicyInjector;->RUNTIME_PERMISSIONS:Ljava/util/Set;

    sget-object v0, Lcom/android/server/pm/DefaultPermissionGrantPolicyInjector;->RUNTIME_PERMISSIONS:Ljava/util/Set;

    const-string/jumbo v1, "android.permission.READ_PHONE_STATE"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/android/server/pm/DefaultPermissionGrantPolicyInjector;->RUNTIME_PERMISSIONS:Ljava/util/Set;

    const-string/jumbo v1, "android.permission.CALL_PHONE"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/android/server/pm/DefaultPermissionGrantPolicyInjector;->RUNTIME_PERMISSIONS:Ljava/util/Set;

    const-string/jumbo v1, "android.permission.READ_CALL_LOG"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/android/server/pm/DefaultPermissionGrantPolicyInjector;->RUNTIME_PERMISSIONS:Ljava/util/Set;

    const-string/jumbo v1, "android.permission.WRITE_CALL_LOG"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/android/server/pm/DefaultPermissionGrantPolicyInjector;->RUNTIME_PERMISSIONS:Ljava/util/Set;

    const-string/jumbo v1, "com.android.voicemail.permission.ADD_VOICEMAIL"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/android/server/pm/DefaultPermissionGrantPolicyInjector;->RUNTIME_PERMISSIONS:Ljava/util/Set;

    const-string/jumbo v1, "android.permission.USE_SIP"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/android/server/pm/DefaultPermissionGrantPolicyInjector;->RUNTIME_PERMISSIONS:Ljava/util/Set;

    const-string/jumbo v1, "android.permission.PROCESS_OUTGOING_CALLS"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/android/server/pm/DefaultPermissionGrantPolicyInjector;->RUNTIME_PERMISSIONS:Ljava/util/Set;

    const-string/jumbo v1, "android.permission.READ_CONTACTS"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/android/server/pm/DefaultPermissionGrantPolicyInjector;->RUNTIME_PERMISSIONS:Ljava/util/Set;

    const-string/jumbo v1, "android.permission.WRITE_CONTACTS"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/android/server/pm/DefaultPermissionGrantPolicyInjector;->RUNTIME_PERMISSIONS:Ljava/util/Set;

    const-string/jumbo v1, "android.permission.GET_ACCOUNTS"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/android/server/pm/DefaultPermissionGrantPolicyInjector;->RUNTIME_PERMISSIONS:Ljava/util/Set;

    const-string/jumbo v1, "android.permission.ACCESS_FINE_LOCATION"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/android/server/pm/DefaultPermissionGrantPolicyInjector;->RUNTIME_PERMISSIONS:Ljava/util/Set;

    const-string/jumbo v1, "android.permission.ACCESS_COARSE_LOCATION"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/android/server/pm/DefaultPermissionGrantPolicyInjector;->RUNTIME_PERMISSIONS:Ljava/util/Set;

    const-string/jumbo v1, "android.permission.READ_CALENDAR"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/android/server/pm/DefaultPermissionGrantPolicyInjector;->RUNTIME_PERMISSIONS:Ljava/util/Set;

    const-string/jumbo v1, "android.permission.WRITE_CALENDAR"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/android/server/pm/DefaultPermissionGrantPolicyInjector;->RUNTIME_PERMISSIONS:Ljava/util/Set;

    const-string/jumbo v1, "android.permission.SEND_SMS"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/android/server/pm/DefaultPermissionGrantPolicyInjector;->RUNTIME_PERMISSIONS:Ljava/util/Set;

    const-string/jumbo v1, "android.permission.RECEIVE_SMS"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/android/server/pm/DefaultPermissionGrantPolicyInjector;->RUNTIME_PERMISSIONS:Ljava/util/Set;

    const-string/jumbo v1, "android.permission.READ_SMS"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/android/server/pm/DefaultPermissionGrantPolicyInjector;->RUNTIME_PERMISSIONS:Ljava/util/Set;

    const-string/jumbo v1, "android.permission.RECEIVE_WAP_PUSH"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/android/server/pm/DefaultPermissionGrantPolicyInjector;->RUNTIME_PERMISSIONS:Ljava/util/Set;

    const-string/jumbo v1, "android.permission.RECEIVE_MMS"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/android/server/pm/DefaultPermissionGrantPolicyInjector;->RUNTIME_PERMISSIONS:Ljava/util/Set;

    const-string/jumbo v1, "android.permission.READ_CELL_BROADCASTS"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/android/server/pm/DefaultPermissionGrantPolicyInjector;->RUNTIME_PERMISSIONS:Ljava/util/Set;

    const-string/jumbo v1, "android.permission.RECORD_AUDIO"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/android/server/pm/DefaultPermissionGrantPolicyInjector;->RUNTIME_PERMISSIONS:Ljava/util/Set;

    const-string/jumbo v1, "android.permission.CAMERA"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/android/server/pm/DefaultPermissionGrantPolicyInjector;->RUNTIME_PERMISSIONS:Ljava/util/Set;

    const-string/jumbo v1, "android.permission.BODY_SENSORS"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/android/server/pm/DefaultPermissionGrantPolicyInjector;->RUNTIME_PERMISSIONS:Ljava/util/Set;

    const-string/jumbo v1, "android.permission.READ_EXTERNAL_STORAGE"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/android/server/pm/DefaultPermissionGrantPolicyInjector;->RUNTIME_PERMISSIONS:Ljava/util/Set;

    const-string/jumbo v1, "android.permission.WRITE_EXTERNAL_STORAGE"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    const/16 v0, 0x2e

    new-array v0, v0, [Ljava/lang/String;

    const-string/jumbo v1, "com.miui.core"

    aput-object v1, v0, v3

    const-string/jumbo v1, "com.android.soundrecorder"

    aput-object v1, v0, v4

    const-string/jumbo v1, "com.android.fileexplorer"

    aput-object v1, v0, v5

    const-string/jumbo v1, "com.android.calendar"

    aput-object v1, v0, v6

    const-string/jumbo v1, "com.android.deskclock"

    aput-object v1, v0, v7

    const-string/jumbo v1, "com.android.browser"

    const/4 v2, 0x5

    aput-object v1, v0, v2

    const-string/jumbo v1, "com.android.camera"

    const/4 v2, 0x6

    aput-object v1, v0, v2

    const-string/jumbo v1, "com.android.mms"

    const/4 v2, 0x7

    aput-object v1, v0, v2

    const-string/jumbo v1, "com.xiaomi.xmsf"

    const/16 v2, 0x8

    aput-object v1, v0, v2

    const-string/jumbo v1, "com.android.quicksearchbox"

    const/16 v2, 0x9

    aput-object v1, v0, v2

    const-string/jumbo v1, "com.miui.home"

    const/16 v2, 0xa

    aput-object v1, v0, v2

    const-string/jumbo v1, "com.miui.securityadd"

    const/16 v2, 0xb

    aput-object v1, v0, v2

    const-string/jumbo v1, "com.miui.guardprovider"

    const/16 v2, 0xc

    aput-object v1, v0, v2

    const-string/jumbo v1, "com.android.providers.downloads"

    const/16 v2, 0xd

    aput-object v1, v0, v2

    const-string/jumbo v1, "com.android.providers.downloads.ui"

    const/16 v2, 0xe

    aput-object v1, v0, v2

    const-string/jumbo v1, "com.miui.cloudservice"

    const/16 v2, 0xf

    aput-object v1, v0, v2

    const-string/jumbo v1, "com.cleanmaster.sdk"

    const/16 v2, 0x10

    aput-object v1, v0, v2

    const-string/jumbo v1, "com.android.incallui"

    const/16 v2, 0x11

    aput-object v1, v0, v2

    const-string/jumbo v1, "com.trafficctr.miui"

    const/16 v2, 0x12

    aput-object v1, v0, v2

    const-string/jumbo v1, "com.opera.max.oem.xiaomi"

    const/16 v2, 0x13

    aput-object v1, v0, v2

    const-string/jumbo v1, "com.xiaomi.account"

    const/16 v2, 0x14

    aput-object v1, v0, v2

    const-string/jumbo v1, "com.android.contacts"

    const/16 v2, 0x15

    aput-object v1, v0, v2

    const-string/jumbo v1, "com.android.bluetooth"

    const/16 v2, 0x16

    aput-object v1, v0, v2

    const-string/jumbo v1, "com.miui.cloudbackup"

    const/16 v2, 0x17

    aput-object v1, v0, v2

    const-string/jumbo v1, "com.miui.voip"

    const/16 v2, 0x18

    aput-object v1, v0, v2

    const-string/jumbo v1, "com.xiaomi.finddevice"

    const/16 v2, 0x19

    aput-object v1, v0, v2

    const-string/jumbo v1, "com.xiaomi.payment"

    const/16 v2, 0x1a

    aput-object v1, v0, v2

    const-string/jumbo v1, "com.miui.virtualsim"

    const/16 v2, 0x1b

    aput-object v1, v0, v2

    const-string/jumbo v1, "com.miui.gallery"

    const/16 v2, 0x1c

    aput-object v1, v0, v2

    const-string/jumbo v1, "com.miui.compass"

    const/16 v2, 0x1d

    aput-object v1, v0, v2

    const-string/jumbo v1, "com.miui.bugreport"

    const/16 v2, 0x1e

    aput-object v1, v0, v2

    const-string/jumbo v1, "com.miui.mipub"

    const/16 v2, 0x1f

    aput-object v1, v0, v2

    const-string/jumbo v1, "com.miui.backup"

    const/16 v2, 0x20

    aput-object v1, v0, v2

    const-string/jumbo v1, "com.xiaomi.midrop"

    const/16 v2, 0x21

    aput-object v1, v0, v2

    const-string/jumbo v1, "com.miui.analytics"

    const/16 v2, 0x22

    aput-object v1, v0, v2

    const-string/jumbo v1, "com.miui.systemAdSolution"

    const/16 v2, 0x23

    aput-object v1, v0, v2

    const-string/jumbo v1, "com.miui.msa.global"

    const/16 v2, 0x24

    aput-object v1, v0, v2

    const-string/jumbo v1, "com.xiaomi.metok"

    const/16 v2, 0x25

    aput-object v1, v0, v2

    const-string/jumbo v1, "com.xiaomi.metoknlp"

    const/16 v2, 0x26

    aput-object v1, v0, v2

    const-string/jumbo v1, "com.android.htmlviewer"

    const/16 v2, 0x27

    aput-object v1, v0, v2

    const-string/jumbo v1, "com.xiaomi.simactivate.service"

    const/16 v2, 0x28

    aput-object v1, v0, v2

    const-string/jumbo v1, "com.miui.extraphoto"

    const/16 v2, 0x29

    aput-object v1, v0, v2

    const-string/jumbo v1, "com.miui.packageinstaller"

    const/16 v2, 0x2a

    aput-object v1, v0, v2

    const-string/jumbo v1, "com.google.android.packageinstaller"

    const/16 v2, 0x2b

    aput-object v1, v0, v2

    const-string/jumbo v1, "com.miui.hybrid"

    const/16 v2, 0x2c

    aput-object v1, v0, v2

    const-string/jumbo v1, "com.xiaomi.camera.parallelservice"

    const/16 v2, 0x2d

    aput-object v1, v0, v2

    sput-object v0, Lcom/android/server/pm/DefaultPermissionGrantPolicyInjector;->MIUI_SYSTEM_APPS:[Ljava/lang/String;

    const/16 v0, 0x2b

    new-array v0, v0, [Ljava/lang/String;

    const-string/jumbo v1, "com.android.thememanager"

    aput-object v1, v0, v3

    const-string/jumbo v1, "com.miui.barcodescanner"

    aput-object v1, v0, v4

    const-string/jumbo v1, "com.miui.dmregservice"

    aput-object v1, v0, v5

    const-string/jumbo v1, "com.miui.notes"

    aput-object v1, v0, v6

    const-string/jumbo v1, "com.miui.weather2"

    aput-object v1, v0, v7

    const-string/jumbo v1, "com.xiaomi.gamecenter"

    const/4 v2, 0x5

    aput-object v1, v0, v2

    const-string/jumbo v1, "com.miui.fmradio"

    const/4 v2, 0x6

    aput-object v1, v0, v2

    const-string/jumbo v1, "com.android.email"

    const/4 v2, 0x7

    aput-object v1, v0, v2

    const-string/jumbo v1, "com.miui.video"

    const/16 v2, 0x8

    aput-object v1, v0, v2

    const-string/jumbo v1, "com.miui.player"

    const/16 v2, 0x9

    aput-object v1, v0, v2

    const-string/jumbo v1, "com.xiaomi.market"

    const/16 v2, 0xa

    aput-object v1, v0, v2

    const-string/jumbo v1, "com.xiaomi.jr"

    const/16 v2, 0xb

    aput-object v1, v0, v2

    const-string/jumbo v1, "com.xiaomi.vip"

    const/16 v2, 0xc

    aput-object v1, v0, v2

    const-string/jumbo v1, "com.mi.vtalk"

    const/16 v2, 0xd

    aput-object v1, v0, v2

    const-string/jumbo v1, "com.xiaomi.gamecenter.sdk.service"

    const/16 v2, 0xe

    aput-object v1, v0, v2

    const-string/jumbo v1, "com.mipay.wallet"

    const/16 v2, 0xf

    aput-object v1, v0, v2

    const-string/jumbo v1, "com.miui.tsmclient"

    const/16 v2, 0x10

    aput-object v1, v0, v2

    const-string/jumbo v1, "org.simalliance.openmobileapi.service"

    const/16 v2, 0x11

    aput-object v1, v0, v2

    const-string/jumbo v1, "com.xiaomi.channel"

    const/16 v2, 0x12

    aput-object v1, v0, v2

    const-string/jumbo v1, "com.miui.yellowpage"

    const/16 v2, 0x13

    aput-object v1, v0, v2

    const-string/jumbo v1, "com.xiaomi.o2o"

    const/16 v2, 0x14

    aput-object v1, v0, v2

    const-string/jumbo v1, "com.miui.miuibbs"

    const/16 v2, 0x15

    aput-object v1, v0, v2

    const-string/jumbo v1, "com.xiaomi.pass"

    const/16 v2, 0x16

    aput-object v1, v0, v2

    const-string/jumbo v1, "com.miui.voiceassist"

    const/16 v2, 0x17

    aput-object v1, v0, v2

    const-string/jumbo v1, "com.xiaomi.mircs"

    const/16 v2, 0x18

    aput-object v1, v0, v2

    const-string/jumbo v1, "com.android.vending"

    const/16 v2, 0x19

    aput-object v1, v0, v2

    const-string/jumbo v1, "com.android.calculator2"

    const/16 v2, 0x1a

    aput-object v1, v0, v2

    const-string/jumbo v1, "com.xiaomi.scanner"

    const/16 v2, 0x1b

    aput-object v1, v0, v2

    const-string/jumbo v1, "com.milink.service"

    const/16 v2, 0x1c

    aput-object v1, v0, v2

    const-string/jumbo v1, "com.miui.sysbase"

    const/16 v2, 0x1d

    aput-object v1, v0, v2

    const-string/jumbo v1, "com.miui.calculator"

    const/16 v2, 0x1e

    aput-object v1, v0, v2

    const-string/jumbo v1, "com.miui.milivetalk"

    const/16 v2, 0x1f

    aput-object v1, v0, v2

    const-string/jumbo v1, "com.miui.smsextra"

    const/16 v2, 0x20

    aput-object v1, v0, v2

    const-string/jumbo v1, "com.xiaomi.oga"

    const/16 v2, 0x21

    aput-object v1, v0, v2

    const-string/jumbo v1, "com.miui.contentextension"

    const/16 v2, 0x22

    aput-object v1, v0, v2

    const-string/jumbo v1, "com.miui.personalassistant"

    const/16 v2, 0x23

    aput-object v1, v0, v2

    const-string/jumbo v1, "com.android.storagemonitor"

    const/16 v2, 0x24

    aput-object v1, v0, v2

    const-string/jumbo v1, "com.xiaomi.gamecenter.pad"

    const/16 v2, 0x25

    aput-object v1, v0, v2

    const-string/jumbo v1, "com.miui.voicetrigger"

    const/16 v2, 0x26

    aput-object v1, v0, v2

    const-string/jumbo v1, "com.xiaomi.vipaccount"

    const/16 v2, 0x27

    aput-object v1, v0, v2

    const-string/jumbo v1, "com.google.android.gms"

    const/16 v2, 0x28

    aput-object v1, v0, v2

    const-string/jumbo v1, "com.miui.greenguard"

    const/16 v2, 0x29

    aput-object v1, v0, v2

    const-string/jumbo v1, "com.mobiletools.systemhelper"

    const/16 v2, 0x2a

    aput-object v1, v0, v2

    sput-object v0, Lcom/android/server/pm/DefaultPermissionGrantPolicyInjector;->MIUI_APPS:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static doesPackageSupportRuntimePermissions(Landroid/content/pm/PackageParser$Package;)Z
    .locals 2

    iget-object v0, p0, Landroid/content/pm/PackageParser$Package;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget v0, v0, Landroid/content/pm/ApplicationInfo;->targetSdkVersion:I

    const/16 v1, 0x16

    if-le v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static grantDefaultPermissions(Lcom/android/server/pm/PackageManagerService;I)V
    .locals 1

    const/4 v0, 0x0

    if-eqz v0, :cond_0

    return-void

    :cond_0
    invoke-static {p0, p1}, Lcom/android/server/pm/DefaultPermissionGrantPolicyInjector;->realGrantDefaultPermissions(Lcom/android/server/pm/PackageManagerService;I)V

    return-void
.end method

.method private static grantIncallUiPermission(Lcom/android/server/pm/PackageManagerService;I)V
    .locals 5

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    const-string/jumbo v4, "android.permission.RECORD_AUDIO"

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const-string/jumbo v4, "android.permission.WRITE_EXTERNAL_STORAGE"

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const-string/jumbo v4, "android.permission.READ_EXTERNAL_STORAGE"

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const-string/jumbo v4, "android.permission.READ_CONTACTS"

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-interface {v2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    sget-object v4, Lcom/android/server/pm/DefaultPermissionGrantPolicyInjector;->INCALL_UI:Ljava/lang/String;

    invoke-virtual {p0, v0, v4, p1}, Lcom/android/server/pm/PackageManagerService;->checkPermission(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v3

    const/4 v4, -0x1

    if-ne v3, v4, :cond_0

    sget-object v4, Lcom/android/server/pm/DefaultPermissionGrantPolicyInjector;->INCALL_UI:Ljava/lang/String;

    invoke-virtual {p0, v4, v0, p1}, Lcom/android/server/pm/PackageManagerService;->grantRuntimePermission(Ljava/lang/String;Ljava/lang/String;I)V

    goto :goto_0

    :cond_1
    return-void
.end method

.method public static grantRuntimePermission(Ljava/lang/String;I)V
    .locals 2

    invoke-static {}, Landroid/app/AppGlobals;->getPackageManager()Landroid/content/pm/IPackageManager;

    move-result-object v0

    check-cast v0, Lcom/android/server/pm/PackageManagerService;

    sget-object v1, Lcom/android/server/pm/DefaultPermissionGrantPolicyInjector;->INCALL_UI:Ljava/lang/String;

    invoke-virtual {v1, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {v0, p1}, Lcom/android/server/pm/DefaultPermissionGrantPolicyInjector;->grantIncallUiPermission(Lcom/android/server/pm/PackageManagerService;I)V

    :cond_0
    return-void
.end method

.method private static grantRuntimePermissionsLPw(Lcom/android/server/pm/PackageManagerService;Ljava/lang/String;ZZZI)V
    .locals 17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/server/pm/PackageManagerService;->mPackages:Landroid/util/ArrayMap;

    move-object/from16 v16, v0

    monitor-enter v16

    :try_start_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/server/pm/PackageManagerService;->mPackages:Landroid/util/ArrayMap;

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Landroid/util/ArrayMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Landroid/content/pm/PackageParser$Package;

    if-eqz v13, :cond_0

    invoke-static {v13}, Lcom/android/server/pm/DefaultPermissionGrantPolicyInjector;->doesPackageSupportRuntimePermissions(Landroid/content/pm/PackageParser$Package;)Z

    move-result v2

    xor-int/lit8 v2, v2, 0x1

    if-nez v2, :cond_0

    iget-object v2, v13, Landroid/content/pm/PackageParser$Package;->requestedPermissions:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->isEmpty()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    monitor-exit v16

    return-void

    :cond_1
    :try_start_1
    iget-object v14, v13, Landroid/content/pm/PackageParser$Package;->requestedPermissions:Ljava/util/ArrayList;

    const/4 v11, 0x0

    invoke-virtual {v13}, Landroid/content/pm/PackageParser$Package;->isUpdatedSystemApp()Z

    move-result v2

    if-eqz v2, :cond_3

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/server/pm/PackageManagerService;->mSettings:Lcom/android/server/pm/Settings;

    iget-object v4, v13, Landroid/content/pm/PackageParser$Package;->packageName:Ljava/lang/String;

    invoke-virtual {v2, v4}, Lcom/android/server/pm/Settings;->getDisabledSystemPkgLPr(Ljava/lang/String;)Lcom/android/server/pm/PackageSetting;

    move-result-object v15

    if-eqz v15, :cond_3

    iget-object v2, v15, Lcom/android/server/pm/PackageSetting;->pkg:Landroid/content/pm/PackageParser$Package;

    if-eqz v2, :cond_3

    iget-object v2, v15, Lcom/android/server/pm/PackageSetting;->pkg:Landroid/content/pm/PackageParser$Package;

    iget-object v2, v2, Landroid/content/pm/PackageParser$Package;->requestedPermissions:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->isEmpty()Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v2

    if-eqz v2, :cond_2

    monitor-exit v16

    return-void

    :cond_2
    :try_start_2
    iget-object v2, v15, Lcom/android/server/pm/PackageSetting;->pkg:Landroid/content/pm/PackageParser$Package;

    iget-object v2, v2, Landroid/content/pm/PackageParser$Package;->requestedPermissions:Ljava/util/ArrayList;

    invoke-interface {v14, v2}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    new-instance v11, Landroid/util/ArraySet;

    invoke-direct {v11, v14}, Landroid/util/ArraySet;-><init>(Ljava/util/Collection;)V

    iget-object v2, v15, Lcom/android/server/pm/PackageSetting;->pkg:Landroid/content/pm/PackageParser$Package;

    iget-object v14, v2, Landroid/content/pm/PackageParser$Package;->requestedPermissions:Ljava/util/ArrayList;

    :cond_3
    invoke-interface {v14}, Ljava/util/List;->size()I

    move-result v10

    const/4 v12, 0x0

    :goto_0
    if-ge v12, v10, :cond_9

    invoke-interface {v14, v12}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    if-eqz v11, :cond_5

    invoke-interface {v11, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    xor-int/lit8 v2, v2, 0x1

    if-eqz v2, :cond_5

    :cond_4
    :goto_1
    add-int/lit8 v12, v12, 0x1

    goto :goto_0

    :cond_5
    sget-object v2, Lcom/android/server/pm/DefaultPermissionGrantPolicyInjector;->RUNTIME_PERMISSIONS:Ljava/util/Set;

    invoke-interface {v2, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, v13, Landroid/content/pm/PackageParser$Package;->packageName:Ljava/lang/String;

    move-object/from16 v0, p0

    move/from16 v1, p5

    invoke-virtual {v0, v3, v2, v1}, Lcom/android/server/pm/PackageManagerService;->getPermissionFlags(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v9

    if-eqz v9, :cond_6

    if-eqz p4, :cond_4

    :cond_6
    const/16 v8, 0x14

    and-int/lit8 v2, v9, 0x14

    if-nez v2, :cond_4

    iget-object v2, v13, Landroid/content/pm/PackageParser$Package;->packageName:Ljava/lang/String;

    move-object/from16 v0, p0

    move/from16 v1, p5

    invoke-virtual {v0, v2, v3, v1}, Lcom/android/server/pm/PackageManagerService;->grantRuntimePermission(Ljava/lang/String;Ljava/lang/String;I)V

    const/16 v5, 0x20

    if-eqz p2, :cond_8

    const/16 v5, 0x30

    :cond_7
    :goto_2
    iget-object v4, v13, Landroid/content/pm/PackageParser$Package;->packageName:Ljava/lang/String;

    move-object/from16 v2, p0

    move v6, v5

    move/from16 v7, p5

    invoke-virtual/range {v2 .. v7}, Lcom/android/server/pm/PackageManagerService;->updatePermissionFlags(Ljava/lang/String;Ljava/lang/String;III)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v2

    monitor-exit v16

    throw v2

    :cond_8
    if-eqz p3, :cond_7

    const/16 v5, 0x22

    goto :goto_2

    :cond_9
    monitor-exit v16

    return-void
.end method

.method private static realGrantDefaultPermissions(Lcom/android/server/pm/PackageManagerService;I)V
    .locals 12

    const/4 v4, 0x1

    const/4 v2, 0x0

    const/4 v11, 0x0

    :goto_0
    sget-object v0, Lcom/android/server/pm/DefaultPermissionGrantPolicyInjector;->MIUI_SYSTEM_APPS:[Ljava/lang/String;

    array-length v0, v0

    if-ge v11, v0, :cond_0

    sget-object v0, Lcom/android/server/pm/DefaultPermissionGrantPolicyInjector;->MIUI_SYSTEM_APPS:[Ljava/lang/String;

    aget-object v1, v0, v11

    move-object v0, p0

    move v3, v2

    move v5, p1

    invoke-static/range {v0 .. v5}, Lcom/android/server/pm/DefaultPermissionGrantPolicyInjector;->grantRuntimePermissionsLPw(Lcom/android/server/pm/PackageManagerService;Ljava/lang/String;ZZZI)V

    add-int/lit8 v11, v11, 0x1

    goto :goto_0

    :cond_0
    const/4 v11, 0x0

    :goto_1
    sget-object v0, Lcom/android/server/pm/DefaultPermissionGrantPolicyInjector;->MIUI_APPS:[Ljava/lang/String;

    array-length v0, v0

    if-ge v11, v0, :cond_1

    sget-object v0, Lcom/android/server/pm/DefaultPermissionGrantPolicyInjector;->MIUI_APPS:[Ljava/lang/String;

    aget-object v6, v0, v11

    move-object v5, p0

    move v7, v2

    move v8, v4

    move v9, v2

    move v10, p1

    invoke-static/range {v5 .. v10}, Lcom/android/server/pm/DefaultPermissionGrantPolicyInjector;->grantRuntimePermissionsLPw(Lcom/android/server/pm/PackageManagerService;Ljava/lang/String;ZZZI)V

    add-int/lit8 v11, v11, 0x1

    goto :goto_1

    :cond_1
    return-void
.end method

.method public static setCoreRuntimePermissionEnabled(ZII)V
    .locals 3

    const/4 v2, 0x0

    if-eqz p2, :cond_0

    return-void

    :cond_0
    invoke-static {}, Landroid/app/AppGlobals;->getPackageManager()Landroid/content/pm/IPackageManager;

    move-result-object v0

    check-cast v0, Lcom/android/server/pm/PackageManagerService;

    if-eqz p0, :cond_1

    invoke-static {v0, p2}, Lcom/android/server/pm/DefaultPermissionGrantPolicyInjector;->realGrantDefaultPermissions(Lcom/android/server/pm/PackageManagerService;I)V

    const-string/jumbo v1, "persist.sys.runtime_perm"

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_1
    const-string/jumbo v1, "persist.sys.runtime_perm"

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
