.class public Lcom/android/server/securespaces/PackageManagerQueue;
.super Ljava/lang/Object;
.source "PackageManagerQueue.java"


# static fields
.field private static final DIRECTORY:Ljava/lang/String; = ".Spaces"

.field private static final FILE_NAME:Ljava/lang/String; = "pmops"

.field public static final PMO_ADDEXCLUDEWIPE:I = 0x7

.field public static final PMO_CLEARUSERDATA:I = 0x6

.field public static final PMO_CREATEUSERCONFIG:I = 0x5

.field public static final PMO_CREATEUSERDATA:I = 0x0

.field public static final PMO_DELETECACHEDIRS:I = 0x4

.field public static final PMO_DELETECODECACHEDIRS:I = 0x3

.field public static final PMO_DESTORYAPPDATA:I = 0x9

.field public static final PMO_LINKNATIVELIBRARYDIR:I = 0x2

.field public static final PMO_REMOVE:I = 0x1

.field private static final SALT_LENGTH:I = 0x10

.field static final TAG:Ljava/lang/String; = "PackageManagerQueue"

.field private static final VERSION:I = 0x2

.field private static final pmqLock:Ljava/lang/Object;

.field private static sInstance:Lcom/android/server/securespaces/PackageManagerQueue;


# instance fields
.field private ceDataInode:J

.field private flag:I

.field private nativeLibraryDir:Ljava/lang/String;

.field private opCode:I

.field private packageName:Ljava/lang/String;

.field private seinfo:Ljava/lang/String;

.field private uid:I

.field private user:I

.field private uuid:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput-object v0, Lcom/android/server/securespaces/PackageManagerQueue;->sInstance:Lcom/android/server/securespaces/PackageManagerQueue;

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/android/server/securespaces/PackageManagerQueue;->pmqLock:Ljava/lang/Object;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    const/4 v1, -0x1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput v1, p0, Lcom/android/server/securespaces/PackageManagerQueue;->opCode:I

    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/android/server/securespaces/PackageManagerQueue;->packageName:Ljava/lang/String;

    iput v1, p0, Lcom/android/server/securespaces/PackageManagerQueue;->user:I

    iput v1, p0, Lcom/android/server/securespaces/PackageManagerQueue;->uid:I

    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/android/server/securespaces/PackageManagerQueue;->seinfo:Ljava/lang/String;

    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/android/server/securespaces/PackageManagerQueue;->nativeLibraryDir:Ljava/lang/String;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/server/securespaces/PackageManagerQueue;->uuid:Ljava/lang/String;

    iput v1, p0, Lcom/android/server/securespaces/PackageManagerQueue;->flag:I

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/android/server/securespaces/PackageManagerQueue;->ceDataInode:J

    return-void
.end method

.method public static declared-synchronized getInstance()Lcom/android/server/securespaces/PackageManagerQueue;
    .locals 2

    const-class v1, Lcom/android/server/securespaces/PackageManagerQueue;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/android/server/securespaces/PackageManagerQueue;->sInstance:Lcom/android/server/securespaces/PackageManagerQueue;

    if-nez v0, :cond_0

    new-instance v0, Lcom/android/server/securespaces/PackageManagerQueue;

    invoke-direct {v0}, Lcom/android/server/securespaces/PackageManagerQueue;-><init>()V

    sput-object v0, Lcom/android/server/securespaces/PackageManagerQueue;->sInstance:Lcom/android/server/securespaces/PackageManagerQueue;

    :cond_0
    sget-object v0, Lcom/android/server/securespaces/PackageManagerQueue;->sInstance:Lcom/android/server/securespaces/PackageManagerQueue;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public processQueue(Lcom/android/server/pm/Installer;Ljava/lang/Object;I)V
    .locals 24

    new-instance v20, Ljava/io/File;

    invoke-static/range {p3 .. p3}, Landroid/os/Environment;->getUserSystemDirectory(I)Ljava/io/File;

    move-result-object v3

    const-string/jumbo v4, ".Spaces"

    move-object/from16 v0, v20

    invoke-direct {v0, v3, v4}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    new-instance v21, Ljava/io/File;

    const-string/jumbo v3, "pmops"

    move-object/from16 v0, v21

    move-object/from16 v1, v20

    invoke-direct {v0, v1, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    const/16 v16, 0x0

    const/4 v2, 0x0

    const/4 v11, 0x0

    sget-object v23, Lcom/android/server/securespaces/PackageManagerQueue;->pmqLock:Ljava/lang/Object;

    monitor-enter v23

    :try_start_0
    invoke-virtual/range {v21 .. v21}, Ljava/io/File;->exists()Z
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_a
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_d
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_10
    .catchall {:try_start_0 .. :try_end_0} :catchall_4

    move-result v3

    if-nez v3, :cond_0

    monitor-exit v23

    return-void

    :cond_0
    :try_start_1
    new-instance v17, Ljava/io/FileInputStream;

    move-object/from16 v0, v17

    move-object/from16 v1, v21

    invoke-direct {v0, v1}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_a
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_d
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_10
    .catchall {:try_start_1 .. :try_end_1} :catchall_4

    :try_start_2
    new-instance v10, Ljava/io/BufferedInputStream;

    const/16 v3, 0x800

    move-object/from16 v0, v17

    invoke-direct {v10, v0, v3}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;I)V
    :try_end_2
    .catch Ljava/io/FileNotFoundException; {:try_start_2 .. :try_end_2} :catch_b
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_e
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_11
    .catchall {:try_start_2 .. :try_end_2} :catchall_5

    :try_start_3
    new-instance v12, Ljava/io/DataInputStream;

    invoke-direct {v12, v10}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V
    :try_end_3
    .catch Ljava/io/FileNotFoundException; {:try_start_3 .. :try_end_3} :catch_c
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_f
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_12
    .catchall {:try_start_3 .. :try_end_3} :catchall_6

    const/16 v3, 0x10

    :try_start_4
    new-array v0, v3, [B

    move-object/from16 v18, v0

    const/16 v3, 0x10

    new-array v0, v3, [B

    move-object/from16 v19, v0

    invoke-virtual {v12}, Ljava/io/DataInputStream;->readInt()I

    move-result v22

    const/4 v3, 0x2

    move/from16 v0, v22

    if-eq v0, v3, :cond_2

    const-string/jumbo v3, "PackageManagerQueue"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "Error processing PackageManager op-file for user "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move/from16 v0, p3

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, ". Unrecognized version."

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_4
    .catch Ljava/io/FileNotFoundException; {:try_start_4 .. :try_end_4} :catch_1
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    if-eqz v12, :cond_1

    :try_start_5
    invoke-virtual {v12}, Ljava/io/DataInputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    :cond_1
    :goto_0
    monitor-exit v23

    return-void

    :catch_0
    move-exception v14

    :try_start_6
    invoke-virtual {v14}, Ljava/io/IOException;->printStackTrace()V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v3

    move-object v11, v12

    move-object v2, v10

    move-object/from16 v16, v17

    :goto_1
    monitor-exit v23

    throw v3

    :cond_2
    :try_start_7
    move-object/from16 v0, v18

    invoke-virtual {v12, v0}, Ljava/io/DataInputStream;->readFully([B)V

    :goto_2
    invoke-virtual {v10}, Ljava/io/BufferedInputStream;->available()I

    move-result v3

    if-lez v3, :cond_8

    move-object/from16 v0, v19

    invoke-virtual {v12, v0}, Ljava/io/DataInputStream;->readFully([B)V

    invoke-virtual {v12}, Ljava/io/DataInputStream;->readInt()I

    move-result v3

    move-object/from16 v0, p0

    iput v3, v0, Lcom/android/server/securespaces/PackageManagerQueue;->opCode:I

    move-object/from16 v0, p0

    iget v3, v0, Lcom/android/server/securespaces/PackageManagerQueue;->opCode:I

    packed-switch v3, :pswitch_data_0

    :pswitch_0
    const-string/jumbo v3, "PackageManagerQueue"

    const-string/jumbo v4, "Unknown opcode in queueOperation."

    invoke-static {v3, v4}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_7
    .catch Ljava/io/FileNotFoundException; {:try_start_7 .. :try_end_7} :catch_1
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_2
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_4
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    goto :goto_2

    :catch_1
    move-exception v13

    move-object v11, v12

    move-object v2, v10

    move-object/from16 v16, v17

    :goto_3
    :try_start_8
    const-string/jumbo v3, "PackageManagerQueue"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "FileNotFoundException caught in processQueue(): "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v13}, Ljava/io/FileNotFoundException;->printStackTrace()V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_4

    if-eqz v11, :cond_3

    :try_start_9
    invoke-virtual {v11}, Ljava/io/DataInputStream;->close()V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_8
    .catchall {:try_start_9 .. :try_end_9} :catchall_1

    :cond_3
    :goto_4
    monitor-exit v23

    invoke-virtual/range {v21 .. v21}, Ljava/io/File;->delete()Z

    return-void

    :pswitch_1
    :try_start_a
    invoke-virtual {v12}, Ljava/io/DataInputStream;->readUTF()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/android/server/securespaces/PackageManagerQueue;->uuid:Ljava/lang/String;

    invoke-virtual {v12}, Ljava/io/DataInputStream;->readUTF()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/android/server/securespaces/PackageManagerQueue;->packageName:Ljava/lang/String;

    invoke-virtual {v12}, Ljava/io/DataInputStream;->readInt()I

    move-result v3

    move-object/from16 v0, p0

    iput v3, v0, Lcom/android/server/securespaces/PackageManagerQueue;->user:I

    invoke-virtual {v12}, Ljava/io/DataInputStream;->readInt()I

    move-result v3

    move-object/from16 v0, p0

    iput v3, v0, Lcom/android/server/securespaces/PackageManagerQueue;->uid:I

    invoke-virtual {v12}, Ljava/io/DataInputStream;->readUTF()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/android/server/securespaces/PackageManagerQueue;->seinfo:Ljava/lang/String;

    :goto_5
    if-nez p1, :cond_6

    const-string/jumbo v3, "PackageManagerQueue"

    const-string/jumbo v4, "Unable to execute queued PackageManager operation.  reference to Installer is null."

    invoke-static {v3, v4}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_a
    .catch Ljava/io/FileNotFoundException; {:try_start_a .. :try_end_a} :catch_1
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_2
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_a} :catch_4
    .catchall {:try_start_a .. :try_end_a} :catchall_2

    if-eqz v12, :cond_4

    :try_start_b
    invoke-virtual {v12}, Ljava/io/DataInputStream;->close()V
    :try_end_b
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_6
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    :cond_4
    :goto_6
    monitor-exit v23

    return-void

    :pswitch_2
    :try_start_c
    invoke-virtual {v12}, Ljava/io/DataInputStream;->readUTF()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/android/server/securespaces/PackageManagerQueue;->uuid:Ljava/lang/String;

    invoke-virtual {v12}, Ljava/io/DataInputStream;->readUTF()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/android/server/securespaces/PackageManagerQueue;->packageName:Ljava/lang/String;

    invoke-virtual {v12}, Ljava/io/DataInputStream;->readInt()I

    move-result v3

    move-object/from16 v0, p0

    iput v3, v0, Lcom/android/server/securespaces/PackageManagerQueue;->user:I
    :try_end_c
    .catch Ljava/io/FileNotFoundException; {:try_start_c .. :try_end_c} :catch_1
    .catch Ljava/io/IOException; {:try_start_c .. :try_end_c} :catch_2
    .catch Ljava/lang/Exception; {:try_start_c .. :try_end_c} :catch_4
    .catchall {:try_start_c .. :try_end_c} :catchall_2

    goto :goto_5

    :catch_2
    move-exception v14

    move-object v11, v12

    move-object v2, v10

    move-object/from16 v16, v17

    :goto_7
    :try_start_d
    const-string/jumbo v3, "PackageManagerQueue"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "IOException caught in processQueue(): "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v14}, Ljava/io/IOException;->printStackTrace()V
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_4

    if-eqz v11, :cond_3

    :try_start_e
    invoke-virtual {v11}, Ljava/io/DataInputStream;->close()V
    :try_end_e
    .catch Ljava/io/IOException; {:try_start_e .. :try_end_e} :catch_3
    .catchall {:try_start_e .. :try_end_e} :catchall_1

    goto :goto_4

    :catch_3
    move-exception v14

    :try_start_f
    invoke-virtual {v14}, Ljava/io/IOException;->printStackTrace()V
    :try_end_f
    .catchall {:try_start_f .. :try_end_f} :catchall_1

    goto/16 :goto_4

    :catchall_1
    move-exception v3

    goto/16 :goto_1

    :pswitch_3
    :try_start_10
    invoke-virtual {v12}, Ljava/io/DataInputStream;->readUTF()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/android/server/securespaces/PackageManagerQueue;->uuid:Ljava/lang/String;

    invoke-virtual {v12}, Ljava/io/DataInputStream;->readUTF()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/android/server/securespaces/PackageManagerQueue;->packageName:Ljava/lang/String;

    invoke-virtual {v12}, Ljava/io/DataInputStream;->readUTF()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/android/server/securespaces/PackageManagerQueue;->nativeLibraryDir:Ljava/lang/String;

    invoke-virtual {v12}, Ljava/io/DataInputStream;->readInt()I

    move-result v3

    move-object/from16 v0, p0

    iput v3, v0, Lcom/android/server/securespaces/PackageManagerQueue;->user:I
    :try_end_10
    .catch Ljava/io/FileNotFoundException; {:try_start_10 .. :try_end_10} :catch_1
    .catch Ljava/io/IOException; {:try_start_10 .. :try_end_10} :catch_2
    .catch Ljava/lang/Exception; {:try_start_10 .. :try_end_10} :catch_4
    .catchall {:try_start_10 .. :try_end_10} :catchall_2

    goto :goto_5

    :catch_4
    move-exception v15

    move-object v11, v12

    move-object v2, v10

    move-object/from16 v16, v17

    :goto_8
    :try_start_11
    const-string/jumbo v3, "PackageManagerQueue"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "Exception caught in processQueue(): "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v15}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_11
    .catchall {:try_start_11 .. :try_end_11} :catchall_4

    if-eqz v11, :cond_3

    :try_start_12
    invoke-virtual {v11}, Ljava/io/DataInputStream;->close()V
    :try_end_12
    .catch Ljava/io/IOException; {:try_start_12 .. :try_end_12} :catch_5
    .catchall {:try_start_12 .. :try_end_12} :catchall_1

    goto/16 :goto_4

    :catch_5
    move-exception v14

    :try_start_13
    invoke-virtual {v14}, Ljava/io/IOException;->printStackTrace()V
    :try_end_13
    .catchall {:try_start_13 .. :try_end_13} :catchall_1

    goto/16 :goto_4

    :pswitch_4
    :try_start_14
    invoke-virtual {v12}, Ljava/io/DataInputStream;->readUTF()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/android/server/securespaces/PackageManagerQueue;->uuid:Ljava/lang/String;

    invoke-virtual {v12}, Ljava/io/DataInputStream;->readUTF()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/android/server/securespaces/PackageManagerQueue;->packageName:Ljava/lang/String;

    invoke-virtual {v12}, Ljava/io/DataInputStream;->readInt()I

    move-result v3

    move-object/from16 v0, p0

    iput v3, v0, Lcom/android/server/securespaces/PackageManagerQueue;->user:I
    :try_end_14
    .catch Ljava/io/FileNotFoundException; {:try_start_14 .. :try_end_14} :catch_1
    .catch Ljava/io/IOException; {:try_start_14 .. :try_end_14} :catch_2
    .catch Ljava/lang/Exception; {:try_start_14 .. :try_end_14} :catch_4
    .catchall {:try_start_14 .. :try_end_14} :catchall_2

    goto/16 :goto_5

    :catchall_2
    move-exception v3

    move-object v11, v12

    move-object v2, v10

    move-object/from16 v16, v17

    :goto_9
    if-eqz v11, :cond_5

    :try_start_15
    invoke-virtual {v11}, Ljava/io/DataInputStream;->close()V
    :try_end_15
    .catch Ljava/io/IOException; {:try_start_15 .. :try_end_15} :catch_9
    .catchall {:try_start_15 .. :try_end_15} :catchall_1

    :cond_5
    :goto_a
    :try_start_16
    throw v3
    :try_end_16
    .catchall {:try_start_16 .. :try_end_16} :catchall_1

    :pswitch_5
    :try_start_17
    invoke-virtual {v12}, Ljava/io/DataInputStream;->readUTF()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/android/server/securespaces/PackageManagerQueue;->uuid:Ljava/lang/String;

    invoke-virtual {v12}, Ljava/io/DataInputStream;->readUTF()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/android/server/securespaces/PackageManagerQueue;->packageName:Ljava/lang/String;

    invoke-virtual {v12}, Ljava/io/DataInputStream;->readInt()I

    move-result v3

    move-object/from16 v0, p0

    iput v3, v0, Lcom/android/server/securespaces/PackageManagerQueue;->user:I

    goto/16 :goto_5

    :pswitch_6
    invoke-virtual {v12}, Ljava/io/DataInputStream;->readInt()I

    move-result v3

    move-object/from16 v0, p0

    iput v3, v0, Lcom/android/server/securespaces/PackageManagerQueue;->user:I

    goto/16 :goto_5

    :pswitch_7
    invoke-virtual {v12}, Ljava/io/DataInputStream;->readUTF()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/android/server/securespaces/PackageManagerQueue;->uuid:Ljava/lang/String;

    invoke-virtual {v12}, Ljava/io/DataInputStream;->readUTF()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/android/server/securespaces/PackageManagerQueue;->packageName:Ljava/lang/String;

    invoke-virtual {v12}, Ljava/io/DataInputStream;->readInt()I

    move-result v3

    move-object/from16 v0, p0

    iput v3, v0, Lcom/android/server/securespaces/PackageManagerQueue;->user:I

    goto/16 :goto_5

    :pswitch_8
    invoke-virtual {v12}, Ljava/io/DataInputStream;->readUTF()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/android/server/securespaces/PackageManagerQueue;->uuid:Ljava/lang/String;

    invoke-virtual {v12}, Ljava/io/DataInputStream;->readUTF()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/android/server/securespaces/PackageManagerQueue;->packageName:Ljava/lang/String;

    invoke-virtual {v12}, Ljava/io/DataInputStream;->readInt()I

    move-result v3

    move-object/from16 v0, p0

    iput v3, v0, Lcom/android/server/securespaces/PackageManagerQueue;->user:I

    invoke-virtual {v12}, Ljava/io/DataInputStream;->readInt()I

    move-result v3

    move-object/from16 v0, p0

    iput v3, v0, Lcom/android/server/securespaces/PackageManagerQueue;->flag:I

    invoke-virtual {v12}, Ljava/io/DataInputStream;->readLong()J

    move-result-wide v4

    move-object/from16 v0, p0

    iput-wide v4, v0, Lcom/android/server/securespaces/PackageManagerQueue;->ceDataInode:J
    :try_end_17
    .catch Ljava/io/FileNotFoundException; {:try_start_17 .. :try_end_17} :catch_1
    .catch Ljava/io/IOException; {:try_start_17 .. :try_end_17} :catch_2
    .catch Ljava/lang/Exception; {:try_start_17 .. :try_end_17} :catch_4
    .catchall {:try_start_17 .. :try_end_17} :catchall_2

    goto/16 :goto_5

    :catch_6
    move-exception v14

    :try_start_18
    invoke-virtual {v14}, Ljava/io/IOException;->printStackTrace()V
    :try_end_18
    .catchall {:try_start_18 .. :try_end_18} :catchall_0

    goto/16 :goto_6

    :cond_6
    :try_start_19
    move-object/from16 v0, p0

    iget v3, v0, Lcom/android/server/securespaces/PackageManagerQueue;->user:I

    move/from16 v0, p3

    if-eq v3, v0, :cond_7

    const-string/jumbo v3, "PackageManagerQueue"

    const-string/jumbo v4, "Invalid user id in PackagerManager queue."

    invoke-static {v3, v4}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_2

    :cond_7
    monitor-enter p2
    :try_end_19
    .catch Ljava/io/FileNotFoundException; {:try_start_19 .. :try_end_19} :catch_1
    .catch Ljava/io/IOException; {:try_start_19 .. :try_end_19} :catch_2
    .catch Ljava/lang/Exception; {:try_start_19 .. :try_end_19} :catch_4
    .catchall {:try_start_19 .. :try_end_19} :catchall_2

    :try_start_1a
    move-object/from16 v0, p0

    iget v3, v0, Lcom/android/server/securespaces/PackageManagerQueue;->opCode:I

    packed-switch v3, :pswitch_data_1

    const-string/jumbo v3, "PackageManagerQueue"

    const-string/jumbo v4, "Unknown opcode in queueOperation."

    invoke-static {v3, v4}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1a
    .catchall {:try_start_1a .. :try_end_1a} :catchall_3

    :try_start_1b
    monitor-exit p2
    :try_end_1b
    .catch Ljava/io/FileNotFoundException; {:try_start_1b .. :try_end_1b} :catch_1
    .catch Ljava/io/IOException; {:try_start_1b .. :try_end_1b} :catch_2
    .catch Ljava/lang/Exception; {:try_start_1b .. :try_end_1b} :catch_4
    .catchall {:try_start_1b .. :try_end_1b} :catchall_2

    goto/16 :goto_2

    :pswitch_9
    :try_start_1c
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/server/securespaces/PackageManagerQueue;->uuid:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/server/securespaces/PackageManagerQueue;->packageName:Ljava/lang/String;

    move-object/from16 v0, p0

    iget v6, v0, Lcom/android/server/securespaces/PackageManagerQueue;->user:I

    move-object/from16 v0, p0

    iget v7, v0, Lcom/android/server/securespaces/PackageManagerQueue;->flag:I

    move-object/from16 v0, p0

    iget-wide v8, v0, Lcom/android/server/securespaces/PackageManagerQueue;->ceDataInode:J

    move-object/from16 v3, p1

    invoke-virtual/range {v3 .. v9}, Lcom/android/server/pm/Installer;->destroyAppData(Ljava/lang/String;Ljava/lang/String;IIJ)V

    const-string/jumbo v3, "PackageManagerQueue"

    const-string/jumbo v4, "destroyAppData"

    invoke-static {v3, v4}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1c
    .catchall {:try_start_1c .. :try_end_1c} :catchall_3

    :try_start_1d
    monitor-exit p2

    goto/16 :goto_2

    :catchall_3
    move-exception v3

    monitor-exit p2

    throw v3
    :try_end_1d
    .catch Ljava/io/FileNotFoundException; {:try_start_1d .. :try_end_1d} :catch_1
    .catch Ljava/io/IOException; {:try_start_1d .. :try_end_1d} :catch_2
    .catch Ljava/lang/Exception; {:try_start_1d .. :try_end_1d} :catch_4
    .catchall {:try_start_1d .. :try_end_1d} :catchall_2

    :cond_8
    if-eqz v12, :cond_9

    :try_start_1e
    invoke-virtual {v12}, Ljava/io/DataInputStream;->close()V
    :try_end_1e
    .catch Ljava/io/IOException; {:try_start_1e .. :try_end_1e} :catch_7
    .catchall {:try_start_1e .. :try_end_1e} :catchall_0

    :cond_9
    :goto_b
    move-object v11, v12

    move-object v2, v10

    move-object/from16 v16, v17

    goto/16 :goto_4

    :catch_7
    move-exception v14

    :try_start_1f
    invoke-virtual {v14}, Ljava/io/IOException;->printStackTrace()V
    :try_end_1f
    .catchall {:try_start_1f .. :try_end_1f} :catchall_0

    goto :goto_b

    :catch_8
    move-exception v14

    :try_start_20
    invoke-virtual {v14}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_4

    :catch_9
    move-exception v14

    invoke-virtual {v14}, Ljava/io/IOException;->printStackTrace()V
    :try_end_20
    .catchall {:try_start_20 .. :try_end_20} :catchall_1

    goto/16 :goto_a

    :catchall_4
    move-exception v3

    goto/16 :goto_9

    :catchall_5
    move-exception v3

    move-object/from16 v16, v17

    goto/16 :goto_9

    :catchall_6
    move-exception v3

    move-object v2, v10

    move-object/from16 v16, v17

    goto/16 :goto_9

    :catch_a
    move-exception v13

    goto/16 :goto_3

    :catch_b
    move-exception v13

    move-object/from16 v16, v17

    goto/16 :goto_3

    :catch_c
    move-exception v13

    move-object v2, v10

    move-object/from16 v16, v17

    goto/16 :goto_3

    :catch_d
    move-exception v14

    goto/16 :goto_7

    :catch_e
    move-exception v14

    move-object/from16 v16, v17

    goto/16 :goto_7

    :catch_f
    move-exception v14

    move-object v2, v10

    move-object/from16 v16, v17

    goto/16 :goto_7

    :catch_10
    move-exception v15

    goto/16 :goto_8

    :catch_11
    move-exception v15

    move-object/from16 v16, v17

    goto/16 :goto_8

    :catch_12
    move-exception v15

    move-object v2, v10

    move-object/from16 v16, v17

    goto/16 :goto_8

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_0
        :pswitch_0
        :pswitch_8
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x9
        :pswitch_9
    .end packed-switch
.end method

.method public setOp(II)V
    .locals 0

    iput p1, p0, Lcom/android/server/securespaces/PackageManagerQueue;->opCode:I

    iput p2, p0, Lcom/android/server/securespaces/PackageManagerQueue;->user:I

    return-void
.end method

.method public setOp(ILjava/lang/String;I)V
    .locals 0

    iput p1, p0, Lcom/android/server/securespaces/PackageManagerQueue;->opCode:I

    iput p3, p0, Lcom/android/server/securespaces/PackageManagerQueue;->user:I

    iput-object p2, p0, Lcom/android/server/securespaces/PackageManagerQueue;->uuid:Ljava/lang/String;

    return-void
.end method

.method public setOp(ILjava/lang/String;Ljava/lang/String;I)V
    .locals 0

    iput p1, p0, Lcom/android/server/securespaces/PackageManagerQueue;->opCode:I

    iput-object p3, p0, Lcom/android/server/securespaces/PackageManagerQueue;->packageName:Ljava/lang/String;

    iput p4, p0, Lcom/android/server/securespaces/PackageManagerQueue;->user:I

    iput-object p2, p0, Lcom/android/server/securespaces/PackageManagerQueue;->uuid:Ljava/lang/String;

    return-void
.end method

.method public setOp(ILjava/lang/String;Ljava/lang/String;IIJ)V
    .locals 0

    iput p1, p0, Lcom/android/server/securespaces/PackageManagerQueue;->opCode:I

    iput-object p2, p0, Lcom/android/server/securespaces/PackageManagerQueue;->uuid:Ljava/lang/String;

    iput-object p3, p0, Lcom/android/server/securespaces/PackageManagerQueue;->packageName:Ljava/lang/String;

    iput p4, p0, Lcom/android/server/securespaces/PackageManagerQueue;->user:I

    iput p5, p0, Lcom/android/server/securespaces/PackageManagerQueue;->flag:I

    iput-wide p6, p0, Lcom/android/server/securespaces/PackageManagerQueue;->ceDataInode:J

    return-void
.end method

.method public setOp(ILjava/lang/String;Ljava/lang/String;IILjava/lang/String;)V
    .locals 0

    iput p1, p0, Lcom/android/server/securespaces/PackageManagerQueue;->opCode:I

    iput-object p3, p0, Lcom/android/server/securespaces/PackageManagerQueue;->packageName:Ljava/lang/String;

    iput p4, p0, Lcom/android/server/securespaces/PackageManagerQueue;->user:I

    iput p5, p0, Lcom/android/server/securespaces/PackageManagerQueue;->uid:I

    iput-object p6, p0, Lcom/android/server/securespaces/PackageManagerQueue;->seinfo:Ljava/lang/String;

    iput-object p2, p0, Lcom/android/server/securespaces/PackageManagerQueue;->uuid:Ljava/lang/String;

    return-void
.end method

.method public setOp(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 0

    iput p1, p0, Lcom/android/server/securespaces/PackageManagerQueue;->opCode:I

    iput-object p3, p0, Lcom/android/server/securespaces/PackageManagerQueue;->packageName:Ljava/lang/String;

    iput-object p4, p0, Lcom/android/server/securespaces/PackageManagerQueue;->nativeLibraryDir:Ljava/lang/String;

    iput p5, p0, Lcom/android/server/securespaces/PackageManagerQueue;->user:I

    iput-object p2, p0, Lcom/android/server/securespaces/PackageManagerQueue;->uuid:Ljava/lang/String;

    return-void
.end method

.method public writeOp()V
    .locals 20

    new-instance v13, Ljava/io/File;

    move-object/from16 v0, p0

    iget v15, v0, Lcom/android/server/securespaces/PackageManagerQueue;->user:I

    invoke-static {v15}, Landroid/os/Environment;->getUserSystemDirectory(I)Ljava/io/File;

    move-result-object v15

    const-string/jumbo v16, ".Spaces"

    move-object/from16 v0, v16

    invoke-direct {v13, v15, v0}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    new-instance v14, Ljava/io/File;

    const-string/jumbo v15, "pmops"

    invoke-direct {v14, v13, v15}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    const/4 v9, 0x0

    const/4 v2, 0x0

    const/4 v4, 0x0

    const/16 v15, 0x10

    new-array v11, v15, [B

    new-instance v12, Ljava/security/SecureRandom;

    invoke-direct {v12}, Ljava/security/SecureRandom;-><init>()V

    sget-object v16, Lcom/android/server/securespaces/PackageManagerQueue;->pmqLock:Ljava/lang/Object;

    monitor-enter v16

    :try_start_0
    invoke-virtual {v14}, Ljava/io/File;->exists()Z

    move-result v15

    if-nez v15, :cond_1

    const-string/jumbo v15, "PackageManagerQueue"

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v18, "PackageManger op-file does not exist for user "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/server/securespaces/PackageManagerQueue;->user:I

    move/from16 v18, v0

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v17

    const-string/jumbo v18, ".  Creating it now."

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-static {v15, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v13}, Ljava/io/File;->mkdirs()Z

    new-instance v10, Ljava/io/FileOutputStream;

    invoke-direct {v10, v14}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_4
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :try_start_1
    new-instance v3, Ljava/io/BufferedOutputStream;

    const/16 v15, 0x800

    invoke-direct {v3, v10, v15}, Ljava/io/BufferedOutputStream;-><init>(Ljava/io/OutputStream;I)V
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_8
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_b
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_e
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    :try_start_2
    new-instance v5, Ljava/io/DataOutputStream;

    invoke-direct {v5, v3}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V
    :try_end_2
    .catch Ljava/io/FileNotFoundException; {:try_start_2 .. :try_end_2} :catch_9
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_c
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_f
    .catchall {:try_start_2 .. :try_end_2} :catchall_3

    :try_start_3
    invoke-virtual {v12, v11}, Ljava/security/SecureRandom;->nextBytes([B)V

    const/4 v15, 0x2

    invoke-virtual {v5, v15}, Ljava/io/DataOutputStream;->writeInt(I)V

    invoke-virtual {v5, v11}, Ljava/io/DataOutputStream;->write([B)V
    :try_end_3
    .catch Ljava/io/FileNotFoundException; {:try_start_3 .. :try_end_3} :catch_a
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_d
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_10
    .catchall {:try_start_3 .. :try_end_3} :catchall_4

    move-object v4, v5

    move-object v2, v3

    move-object v9, v10

    :goto_0
    :try_start_4
    const-string/jumbo v15, "PackageManagerQueue"

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v18, "Appending record to PackageManager op-file for user "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/server/securespaces/PackageManagerQueue;->user:I

    move/from16 v18, v0

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-static {v15, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v12, v11}, Ljava/security/SecureRandom;->nextBytes([B)V

    invoke-virtual {v4, v11}, Ljava/io/DataOutputStream;->write([B)V

    move-object/from16 v0, p0

    iget v15, v0, Lcom/android/server/securespaces/PackageManagerQueue;->opCode:I

    invoke-virtual {v4, v15}, Ljava/io/DataOutputStream;->writeInt(I)V

    move-object/from16 v0, p0

    iget v15, v0, Lcom/android/server/securespaces/PackageManagerQueue;->opCode:I

    packed-switch v15, :pswitch_data_0

    :pswitch_0
    const-string/jumbo v15, "PackageManagerQueue"

    const-string/jumbo v17, "Unknown opcode in queueOperation."

    move-object/from16 v0, v17

    invoke-static {v15, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_4
    .catch Ljava/io/FileNotFoundException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    :goto_1
    if-eqz v4, :cond_0

    :try_start_5
    invoke-virtual {v4}, Ljava/io/DataOutputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_6
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    :cond_0
    :goto_2
    monitor-exit v16

    return-void

    :cond_1
    :try_start_6
    new-instance v10, Ljava/io/FileOutputStream;

    const/4 v15, 0x1

    invoke-direct {v10, v14, v15}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;Z)V
    :try_end_6
    .catch Ljava/io/FileNotFoundException; {:try_start_6 .. :try_end_6} :catch_0
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_2
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_4
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    :try_start_7
    new-instance v3, Ljava/io/BufferedOutputStream;

    const/16 v15, 0x800

    invoke-direct {v3, v10, v15}, Ljava/io/BufferedOutputStream;-><init>(Ljava/io/OutputStream;I)V
    :try_end_7
    .catch Ljava/io/FileNotFoundException; {:try_start_7 .. :try_end_7} :catch_8
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_b
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_e
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    :try_start_8
    new-instance v5, Ljava/io/DataOutputStream;

    invoke-direct {v5, v3}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V
    :try_end_8
    .catch Ljava/io/FileNotFoundException; {:try_start_8 .. :try_end_8} :catch_9
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_c
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_f
    .catchall {:try_start_8 .. :try_end_8} :catchall_3

    move-object v4, v5

    move-object v2, v3

    move-object v9, v10

    goto :goto_0

    :pswitch_1
    :try_start_9
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/android/server/securespaces/PackageManagerQueue;->uuid:Ljava/lang/String;

    invoke-virtual {v4, v15}, Ljava/io/DataOutputStream;->writeUTF(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/android/server/securespaces/PackageManagerQueue;->packageName:Ljava/lang/String;

    invoke-virtual {v4, v15}, Ljava/io/DataOutputStream;->writeUTF(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget v15, v0, Lcom/android/server/securespaces/PackageManagerQueue;->user:I

    invoke-virtual {v4, v15}, Ljava/io/DataOutputStream;->writeInt(I)V

    move-object/from16 v0, p0

    iget v15, v0, Lcom/android/server/securespaces/PackageManagerQueue;->uid:I

    invoke-virtual {v4, v15}, Ljava/io/DataOutputStream;->writeInt(I)V

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/android/server/securespaces/PackageManagerQueue;->seinfo:Ljava/lang/String;

    invoke-virtual {v4, v15}, Ljava/io/DataOutputStream;->writeUTF(Ljava/lang/String;)V
    :try_end_9
    .catch Ljava/io/FileNotFoundException; {:try_start_9 .. :try_end_9} :catch_0
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_2
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_4
    .catchall {:try_start_9 .. :try_end_9} :catchall_1

    goto :goto_1

    :catch_0
    move-exception v6

    :goto_3
    :try_start_a
    const-string/jumbo v15, "PackageManagerQueue"

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v18, "FileNotFoundException caught in writeOp(): "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-static {v15, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v6}, Ljava/io/FileNotFoundException;->printStackTrace()V
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_1

    if-eqz v4, :cond_0

    :try_start_b
    invoke-virtual {v4}, Ljava/io/DataOutputStream;->close()V
    :try_end_b
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_1
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    goto :goto_2

    :catch_1
    move-exception v7

    :try_start_c
    invoke-virtual {v7}, Ljava/io/IOException;->printStackTrace()V
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_0

    goto :goto_2

    :catchall_0
    move-exception v15

    monitor-exit v16

    throw v15

    :pswitch_2
    :try_start_d
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/android/server/securespaces/PackageManagerQueue;->uuid:Ljava/lang/String;

    invoke-virtual {v4, v15}, Ljava/io/DataOutputStream;->writeUTF(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/android/server/securespaces/PackageManagerQueue;->packageName:Ljava/lang/String;

    invoke-virtual {v4, v15}, Ljava/io/DataOutputStream;->writeUTF(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget v15, v0, Lcom/android/server/securespaces/PackageManagerQueue;->user:I

    invoke-virtual {v4, v15}, Ljava/io/DataOutputStream;->writeInt(I)V
    :try_end_d
    .catch Ljava/io/FileNotFoundException; {:try_start_d .. :try_end_d} :catch_0
    .catch Ljava/io/IOException; {:try_start_d .. :try_end_d} :catch_2
    .catch Ljava/lang/Exception; {:try_start_d .. :try_end_d} :catch_4
    .catchall {:try_start_d .. :try_end_d} :catchall_1

    goto/16 :goto_1

    :catch_2
    move-exception v7

    :goto_4
    :try_start_e
    const-string/jumbo v15, "PackageManagerQueue"

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v18, "IOException caught in writeOp(): "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-static {v15, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v7}, Ljava/io/IOException;->printStackTrace()V
    :try_end_e
    .catchall {:try_start_e .. :try_end_e} :catchall_1

    if-eqz v4, :cond_0

    :try_start_f
    invoke-virtual {v4}, Ljava/io/DataOutputStream;->close()V
    :try_end_f
    .catch Ljava/io/IOException; {:try_start_f .. :try_end_f} :catch_3
    .catchall {:try_start_f .. :try_end_f} :catchall_0

    goto/16 :goto_2

    :catch_3
    move-exception v7

    :try_start_10
    invoke-virtual {v7}, Ljava/io/IOException;->printStackTrace()V
    :try_end_10
    .catchall {:try_start_10 .. :try_end_10} :catchall_0

    goto/16 :goto_2

    :pswitch_3
    :try_start_11
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/android/server/securespaces/PackageManagerQueue;->uuid:Ljava/lang/String;

    invoke-virtual {v4, v15}, Ljava/io/DataOutputStream;->writeUTF(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/android/server/securespaces/PackageManagerQueue;->packageName:Ljava/lang/String;

    invoke-virtual {v4, v15}, Ljava/io/DataOutputStream;->writeUTF(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/android/server/securespaces/PackageManagerQueue;->nativeLibraryDir:Ljava/lang/String;

    invoke-virtual {v4, v15}, Ljava/io/DataOutputStream;->writeUTF(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget v15, v0, Lcom/android/server/securespaces/PackageManagerQueue;->user:I

    invoke-virtual {v4, v15}, Ljava/io/DataOutputStream;->writeInt(I)V
    :try_end_11
    .catch Ljava/io/FileNotFoundException; {:try_start_11 .. :try_end_11} :catch_0
    .catch Ljava/io/IOException; {:try_start_11 .. :try_end_11} :catch_2
    .catch Ljava/lang/Exception; {:try_start_11 .. :try_end_11} :catch_4
    .catchall {:try_start_11 .. :try_end_11} :catchall_1

    goto/16 :goto_1

    :catch_4
    move-exception v8

    :goto_5
    :try_start_12
    const-string/jumbo v15, "PackageManagerQueue"

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v18, "Exception caught in writeOp(): "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-static {v15, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v8}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_12
    .catchall {:try_start_12 .. :try_end_12} :catchall_1

    if-eqz v4, :cond_0

    :try_start_13
    invoke-virtual {v4}, Ljava/io/DataOutputStream;->close()V
    :try_end_13
    .catch Ljava/io/IOException; {:try_start_13 .. :try_end_13} :catch_5
    .catchall {:try_start_13 .. :try_end_13} :catchall_0

    goto/16 :goto_2

    :catch_5
    move-exception v7

    :try_start_14
    invoke-virtual {v7}, Ljava/io/IOException;->printStackTrace()V
    :try_end_14
    .catchall {:try_start_14 .. :try_end_14} :catchall_0

    goto/16 :goto_2

    :pswitch_4
    :try_start_15
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/android/server/securespaces/PackageManagerQueue;->uuid:Ljava/lang/String;

    invoke-virtual {v4, v15}, Ljava/io/DataOutputStream;->writeUTF(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/android/server/securespaces/PackageManagerQueue;->packageName:Ljava/lang/String;

    invoke-virtual {v4, v15}, Ljava/io/DataOutputStream;->writeUTF(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget v15, v0, Lcom/android/server/securespaces/PackageManagerQueue;->user:I

    invoke-virtual {v4, v15}, Ljava/io/DataOutputStream;->writeInt(I)V
    :try_end_15
    .catch Ljava/io/FileNotFoundException; {:try_start_15 .. :try_end_15} :catch_0
    .catch Ljava/io/IOException; {:try_start_15 .. :try_end_15} :catch_2
    .catch Ljava/lang/Exception; {:try_start_15 .. :try_end_15} :catch_4
    .catchall {:try_start_15 .. :try_end_15} :catchall_1

    goto/16 :goto_1

    :catchall_1
    move-exception v15

    :goto_6
    if-eqz v4, :cond_2

    :try_start_16
    invoke-virtual {v4}, Ljava/io/DataOutputStream;->close()V
    :try_end_16
    .catch Ljava/io/IOException; {:try_start_16 .. :try_end_16} :catch_7
    .catchall {:try_start_16 .. :try_end_16} :catchall_0

    :cond_2
    :goto_7
    :try_start_17
    throw v15
    :try_end_17
    .catchall {:try_start_17 .. :try_end_17} :catchall_0

    :pswitch_5
    :try_start_18
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/android/server/securespaces/PackageManagerQueue;->uuid:Ljava/lang/String;

    invoke-virtual {v4, v15}, Ljava/io/DataOutputStream;->writeUTF(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/android/server/securespaces/PackageManagerQueue;->packageName:Ljava/lang/String;

    invoke-virtual {v4, v15}, Ljava/io/DataOutputStream;->writeUTF(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget v15, v0, Lcom/android/server/securespaces/PackageManagerQueue;->user:I

    invoke-virtual {v4, v15}, Ljava/io/DataOutputStream;->writeInt(I)V

    goto/16 :goto_1

    :pswitch_6
    move-object/from16 v0, p0

    iget v15, v0, Lcom/android/server/securespaces/PackageManagerQueue;->user:I

    invoke-virtual {v4, v15}, Ljava/io/DataOutputStream;->writeInt(I)V

    goto/16 :goto_1

    :pswitch_7
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/android/server/securespaces/PackageManagerQueue;->uuid:Ljava/lang/String;

    invoke-virtual {v4, v15}, Ljava/io/DataOutputStream;->writeUTF(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/android/server/securespaces/PackageManagerQueue;->packageName:Ljava/lang/String;

    invoke-virtual {v4, v15}, Ljava/io/DataOutputStream;->writeUTF(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget v15, v0, Lcom/android/server/securespaces/PackageManagerQueue;->user:I

    invoke-virtual {v4, v15}, Ljava/io/DataOutputStream;->writeInt(I)V

    goto/16 :goto_1

    :pswitch_8
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/android/server/securespaces/PackageManagerQueue;->uuid:Ljava/lang/String;

    invoke-virtual {v4, v15}, Ljava/io/DataOutputStream;->writeUTF(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/android/server/securespaces/PackageManagerQueue;->packageName:Ljava/lang/String;

    invoke-virtual {v4, v15}, Ljava/io/DataOutputStream;->writeUTF(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget v15, v0, Lcom/android/server/securespaces/PackageManagerQueue;->user:I

    invoke-virtual {v4, v15}, Ljava/io/DataOutputStream;->writeInt(I)V

    move-object/from16 v0, p0

    iget v15, v0, Lcom/android/server/securespaces/PackageManagerQueue;->flag:I

    invoke-virtual {v4, v15}, Ljava/io/DataOutputStream;->writeInt(I)V

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/android/server/securespaces/PackageManagerQueue;->ceDataInode:J

    move-wide/from16 v18, v0

    move-wide/from16 v0, v18

    invoke-virtual {v4, v0, v1}, Ljava/io/DataOutputStream;->writeLong(J)V
    :try_end_18
    .catch Ljava/io/FileNotFoundException; {:try_start_18 .. :try_end_18} :catch_0
    .catch Ljava/io/IOException; {:try_start_18 .. :try_end_18} :catch_2
    .catch Ljava/lang/Exception; {:try_start_18 .. :try_end_18} :catch_4
    .catchall {:try_start_18 .. :try_end_18} :catchall_1

    goto/16 :goto_1

    :catch_6
    move-exception v7

    :try_start_19
    invoke-virtual {v7}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_2

    :catch_7
    move-exception v7

    invoke-virtual {v7}, Ljava/io/IOException;->printStackTrace()V
    :try_end_19
    .catchall {:try_start_19 .. :try_end_19} :catchall_0

    goto :goto_7

    :catchall_2
    move-exception v15

    move-object v9, v10

    goto :goto_6

    :catchall_3
    move-exception v15

    move-object v2, v3

    move-object v9, v10

    goto :goto_6

    :catchall_4
    move-exception v15

    move-object v4, v5

    move-object v2, v3

    move-object v9, v10

    goto :goto_6

    :catch_8
    move-exception v6

    move-object v9, v10

    goto/16 :goto_3

    :catch_9
    move-exception v6

    move-object v2, v3

    move-object v9, v10

    goto/16 :goto_3

    :catch_a
    move-exception v6

    move-object v4, v5

    move-object v2, v3

    move-object v9, v10

    goto/16 :goto_3

    :catch_b
    move-exception v7

    move-object v9, v10

    goto/16 :goto_4

    :catch_c
    move-exception v7

    move-object v2, v3

    move-object v9, v10

    goto/16 :goto_4

    :catch_d
    move-exception v7

    move-object v4, v5

    move-object v2, v3

    move-object v9, v10

    goto/16 :goto_4

    :catch_e
    move-exception v8

    move-object v9, v10

    goto/16 :goto_5

    :catch_f
    move-exception v8

    move-object v2, v3

    move-object v9, v10

    goto/16 :goto_5

    :catch_10
    move-exception v8

    move-object v4, v5

    move-object v2, v3

    move-object v9, v10

    goto/16 :goto_5

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_0
        :pswitch_0
        :pswitch_8
    .end packed-switch
.end method
