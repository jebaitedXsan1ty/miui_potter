.class public Lcom/android/server/ScreenOnMonitor;
.super Ljava/lang/Object;
.source "ScreenOnMonitor.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/ScreenOnMonitor$ScreenOnMonitorHandler;
    }
.end annotation


# static fields
.field private static final AVG_MAX_COUNT:J = 0x32L

.field public static final BLOCK_SCREEN_ON_BEGIN:I = 0x2

.field public static final BLOCK_SCREEN_ON_END:I = 0x3

.field public static final DATE:Ljava/util/Date;

.field private static final INTERVAL_REPORT_TIME:J = 0xdbba00L

.field private static final MSG_RECORD_TIME:I = 0x3

.field private static final MSG_REPORT:I = 0x5

.field private static final MSG_SCREEN_ON_TIMEOUT:I = 0x4

.field private static final MSG_START_MONITOR:I = 0x1

.field private static final MSG_STOP_MONITOR:I = 0x2

.field private static PROPERTY_SCREEN_ON_UPLOAD:Ljava/lang/String; = null

.field private static final REPORT_DELAY:J = 0x7d0L

.field private static final SCREEN_ON_TIMEOUT:J = 0x3e8L

.field public static final SET_DISPLAY_STATE_BEGIN:I = 0x0

.field public static final SET_DISPLAY_STATE_END:I = 0x1

.field public static final SIMPLE_DATE_FORMAT:Ljava/text/SimpleDateFormat;

.field private static final TAG:Ljava/lang/String; = "ScreenOnMonitor"

.field private static final TYPE_WAKE_SOURCE_DEFAULT:I = -0x1

.field private static final TYPE_WAKE_SOURCE_DP_CENTER:I = 0x2

.field private static final TYPE_WAKE_SOURCE_KEYGUARD_FINGER_PASS:I = 0x4

.field private static final TYPE_WAKE_SOURCE_KEYGUARD_NOTIFICATION:I = 0x3

.field private static final TYPE_WAKE_SOURCE_LID:I = 0x5

.field private static final TYPE_WAKE_SOURCE_POWER:I = 0x1

.field private static final TYPE_WAKE_SOURCE_TOTAL:I

.field private static volatile sInstance:Lcom/android/server/ScreenOnMonitor;


# instance fields
.field private mAvgCount:[I

.field private mBlockScreenOnBegin:J

.field private mBlockScreenOnEnd:J

.field private mDisplayBrightness:I

.field private mDisplayState:I

.field private mHandler:Lcom/android/server/ScreenOnMonitor$ScreenOnMonitorHandler;

.field private mLastReportTime:J

.field private mNeedRecord:[Z

.field private mSetDisplayStateBegin:J

.field private mSetDisplayStateEnd:J

.field private mStartTime:J

.field private mStopTime:J

.field private mTimeStamp:J

.field private mTimeoutSummary:Ljava/lang/String;

.field private mTotalBlockScreenOnTime:[J

.field private mTotalScreenOnTime:[J

.field private mTotalSetDisplayTime:[J

.field private mTypeNeedRecordSb:Ljava/lang/StringBuilder;

.field private mUploadVersion:Ljava/lang/String;

.field private mWakeLock:Landroid/os/PowerManager$WakeLock;

.field private mWakeSource:Ljava/lang/String;


# direct methods
.method static synthetic -wrap0(Lcom/android/server/ScreenOnMonitor;IJ)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/android/server/ScreenOnMonitor;->handleRecordTime(IJ)V

    return-void
.end method

.method static synthetic -wrap1(Lcom/android/server/ScreenOnMonitor;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/ScreenOnMonitor;->handleReport(Z)V

    return-void
.end method

.method static synthetic -wrap2(Lcom/android/server/ScreenOnMonitor;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/ScreenOnMonitor;->handleScreenOnTimeout()V

    return-void
.end method

.method static synthetic -wrap3(Lcom/android/server/ScreenOnMonitor;Lcom/android/internal/os/SomeArgs;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/ScreenOnMonitor;->handleStartMonitor(Lcom/android/internal/os/SomeArgs;)V

    return-void
.end method

.method static synthetic -wrap4(Lcom/android/server/ScreenOnMonitor;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/ScreenOnMonitor;->handleStopMonitor(Z)V

    return-void
.end method

.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Ljava/util/Date;

    invoke-direct {v0}, Ljava/util/Date;-><init>()V

    sput-object v0, Lcom/android/server/ScreenOnMonitor;->DATE:Ljava/util/Date;

    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string/jumbo v1, "yyyy-MM-dd HH:mm:ss"

    invoke-direct {v0, v1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/android/server/ScreenOnMonitor;->SIMPLE_DATE_FORMAT:Ljava/text/SimpleDateFormat;

    const-string/jumbo v0, "persist.sys.screenon"

    sput-object v0, Lcom/android/server/ScreenOnMonitor;->PROPERTY_SCREEN_ON_UPLOAD:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>()V
    .locals 11

    const/4 v6, 0x1

    const/4 v7, 0x0

    const/4 v10, 0x6

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v5, -0x1

    iput v5, p0, Lcom/android/server/ScreenOnMonitor;->mDisplayBrightness:I

    iput v7, p0, Lcom/android/server/ScreenOnMonitor;->mDisplayState:I

    const-wide/16 v8, -0x1

    iput-wide v8, p0, Lcom/android/server/ScreenOnMonitor;->mStopTime:J

    new-array v5, v10, [I

    fill-array-data v5, :array_0

    iput-object v5, p0, Lcom/android/server/ScreenOnMonitor;->mAvgCount:[I

    new-array v5, v10, [J

    fill-array-data v5, :array_1

    iput-object v5, p0, Lcom/android/server/ScreenOnMonitor;->mTotalScreenOnTime:[J

    new-array v5, v10, [J

    fill-array-data v5, :array_2

    iput-object v5, p0, Lcom/android/server/ScreenOnMonitor;->mTotalSetDisplayTime:[J

    new-array v5, v10, [J

    fill-array-data v5, :array_3

    iput-object v5, p0, Lcom/android/server/ScreenOnMonitor;->mTotalBlockScreenOnTime:[J

    new-array v5, v10, [Z

    fill-array-data v5, :array_4

    iput-object v5, p0, Lcom/android/server/ScreenOnMonitor;->mNeedRecord:[Z

    sget-boolean v5, Lmiui/os/Build;->IS_STABLE_VERSION:Z

    if-eqz v5, :cond_0

    return-void

    :cond_0
    sget-object v5, Lcom/android/server/ScreenOnMonitor;->PROPERTY_SCREEN_ON_UPLOAD:Ljava/lang/String;

    invoke-static {v5}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_4

    const-string/jumbo v5, ","

    invoke-virtual {v3, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    array-length v5, v4

    const/4 v8, 0x2

    if-ne v5, v8, :cond_4

    aget-object v1, v4, v7

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_2

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v5

    iget-object v8, p0, Lcom/android/server/ScreenOnMonitor;->mNeedRecord:[Z

    array-length v8, v8

    if-ne v5, v8, :cond_2

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iput-object v5, p0, Lcom/android/server/ScreenOnMonitor;->mTypeNeedRecordSb:Ljava/lang/StringBuilder;

    const/4 v0, 0x0

    :goto_0
    iget-object v5, p0, Lcom/android/server/ScreenOnMonitor;->mNeedRecord:[Z

    array-length v5, v5

    if-ge v0, v5, :cond_3

    iget-object v8, p0, Lcom/android/server/ScreenOnMonitor;->mNeedRecord:[Z

    invoke-virtual {v1, v0}, Ljava/lang/String;->charAt(I)C

    move-result v5

    const/16 v9, 0x30

    if-ne v5, v9, :cond_1

    move v5, v6

    :goto_1
    aput-boolean v5, v8, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    move v5, v7

    goto :goto_1

    :cond_2
    new-instance v5, Ljava/lang/StringBuilder;

    const-string/jumbo v7, "000000"

    invoke-direct {v5, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iput-object v5, p0, Lcom/android/server/ScreenOnMonitor;->mTypeNeedRecordSb:Ljava/lang/StringBuilder;

    :cond_3
    aget-object v5, v4, v6

    iput-object v5, p0, Lcom/android/server/ScreenOnMonitor;->mUploadVersion:Ljava/lang/String;

    :cond_4
    iget-object v5, p0, Lcom/android/server/ScreenOnMonitor;->mUploadVersion:Ljava/lang/String;

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_5

    const-string/jumbo v5, "0.0.0"

    iput-object v5, p0, Lcom/android/server/ScreenOnMonitor;->mUploadVersion:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string/jumbo v7, "000000"

    invoke-direct {v5, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iput-object v5, p0, Lcom/android/server/ScreenOnMonitor;->mTypeNeedRecordSb:Ljava/lang/StringBuilder;

    :cond_5
    new-instance v5, Lcom/android/server/ScreenOnMonitor$ScreenOnMonitorHandler;

    invoke-static {}, Landroid/os/AnrMonitor;->getWorkHandler()Landroid/os/Handler;

    move-result-object v7

    invoke-virtual {v7}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    move-result-object v7

    invoke-direct {v5, p0, v7}, Lcom/android/server/ScreenOnMonitor$ScreenOnMonitorHandler;-><init>(Lcom/android/server/ScreenOnMonitor;Landroid/os/Looper;)V

    iput-object v5, p0, Lcom/android/server/ScreenOnMonitor;->mHandler:Lcom/android/server/ScreenOnMonitor$ScreenOnMonitorHandler;

    invoke-static {}, Landroid/app/ActivityThread;->currentApplication()Landroid/app/Application;

    move-result-object v5

    const-string/jumbo v7, "power"

    invoke-virtual {v5, v7}, Landroid/app/Application;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/os/PowerManager;

    const-string/jumbo v5, "ScreenOnMonitor"

    invoke-virtual {v2, v6, v5}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v5

    iput-object v5, p0, Lcom/android/server/ScreenOnMonitor;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    return-void

    nop

    :array_0
    .array-data 4
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
    .end array-data

    :array_1
    .array-data 8
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
    .end array-data

    :array_2
    .array-data 8
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
    .end array-data

    :array_3
    .array-data 8
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
    .end array-data

    :array_4
    .array-data 1
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
    .end array-data
.end method

.method public static getInstance()Lcom/android/server/ScreenOnMonitor;
    .locals 2

    sget-object v0, Lcom/android/server/ScreenOnMonitor;->sInstance:Lcom/android/server/ScreenOnMonitor;

    if-nez v0, :cond_1

    const-class v1, Lcom/android/server/ScreenOnMonitor;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/android/server/ScreenOnMonitor;->sInstance:Lcom/android/server/ScreenOnMonitor;

    if-nez v0, :cond_0

    new-instance v0, Lcom/android/server/ScreenOnMonitor;

    invoke-direct {v0}, Lcom/android/server/ScreenOnMonitor;-><init>()V

    sput-object v0, Lcom/android/server/ScreenOnMonitor;->sInstance:Lcom/android/server/ScreenOnMonitor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit v1

    :cond_1
    sget-object v0, Lcom/android/server/ScreenOnMonitor;->sInstance:Lcom/android/server/ScreenOnMonitor;

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private getScreenOnDetail()Ljava/lang/String;
    .locals 12

    const-wide/16 v10, 0x0

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    iget-wide v6, p0, Lcom/android/server/ScreenOnMonitor;->mStopTime:J

    cmp-long v5, v6, v10

    if-lez v5, :cond_0

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    iget-wide v6, p0, Lcom/android/server/ScreenOnMonitor;->mStopTime:J

    iget-wide v8, p0, Lcom/android/server/ScreenOnMonitor;->mStartTime:J

    sub-long/2addr v6, v8

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, "ms"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    :goto_0
    iget-wide v6, p0, Lcom/android/server/ScreenOnMonitor;->mSetDisplayStateEnd:J

    cmp-long v5, v6, v10

    if-lez v5, :cond_1

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    iget-wide v6, p0, Lcom/android/server/ScreenOnMonitor;->mSetDisplayStateEnd:J

    iget-wide v8, p0, Lcom/android/server/ScreenOnMonitor;->mSetDisplayStateBegin:J

    sub-long/2addr v6, v8

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, "ms"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    :goto_1
    iget-wide v6, p0, Lcom/android/server/ScreenOnMonitor;->mBlockScreenOnEnd:J

    cmp-long v5, v6, v10

    if-lez v5, :cond_3

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    iget-wide v6, p0, Lcom/android/server/ScreenOnMonitor;->mBlockScreenOnEnd:J

    iget-wide v8, p0, Lcom/android/server/ScreenOnMonitor;->mBlockScreenOnBegin:J

    sub-long/2addr v6, v8

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, "ms"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_2
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "total="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, " setDisp="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, " blockScreen="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    return-object v5

    :cond_0
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    iget-wide v6, p0, Lcom/android/server/ScreenOnMonitor;->mStartTime:J

    sub-long v6, v2, v6

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, "+ms"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_0

    :cond_1
    iget-wide v6, p0, Lcom/android/server/ScreenOnMonitor;->mSetDisplayStateBegin:J

    cmp-long v5, v6, v10

    if-lez v5, :cond_2

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    iget-wide v6, p0, Lcom/android/server/ScreenOnMonitor;->mSetDisplayStateBegin:J

    sub-long v6, v2, v6

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, "+ms"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_1

    :cond_2
    move-object v1, v4

    goto/16 :goto_1

    :cond_3
    iget-wide v6, p0, Lcom/android/server/ScreenOnMonitor;->mBlockScreenOnBegin:J

    cmp-long v5, v6, v10

    if-lez v5, :cond_4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    iget-wide v6, p0, Lcom/android/server/ScreenOnMonitor;->mBlockScreenOnBegin:J

    sub-long v6, v2, v6

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, "+ms"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_2

    :cond_4
    move-object v0, v4

    goto/16 :goto_2
.end method

.method private getTimeoutSummary()Ljava/lang/String;
    .locals 4

    const-wide/16 v2, 0x0

    iget-wide v0, p0, Lcom/android/server/ScreenOnMonitor;->mBlockScreenOnEnd:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    iget-wide v0, p0, Lcom/android/server/ScreenOnMonitor;->mSetDisplayStateEnd:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    const-string/jumbo v0, "Abnormal in setting display state"

    return-object v0

    :cond_0
    iget-wide v0, p0, Lcom/android/server/ScreenOnMonitor;->mBlockScreenOnEnd:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    iget-wide v0, p0, Lcom/android/server/ScreenOnMonitor;->mSetDisplayStateEnd:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_1

    const-string/jumbo v0, "Abnormal in blocking screen on"

    return-object v0

    :cond_1
    iget-wide v0, p0, Lcom/android/server/ScreenOnMonitor;->mBlockScreenOnEnd:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_2

    iget-wide v0, p0, Lcom/android/server/ScreenOnMonitor;->mSetDisplayStateEnd:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_2

    const-string/jumbo v0, "Abnormal in setting brightness"

    return-object v0

    :cond_2
    iget-wide v0, p0, Lcom/android/server/ScreenOnMonitor;->mBlockScreenOnBegin:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_3

    const-string/jumbo v0, "Abnormal before setting screen state"

    return-object v0

    :cond_3
    iget-wide v0, p0, Lcom/android/server/ScreenOnMonitor;->mSetDisplayStateBegin:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_4

    const-string/jumbo v0, "Abnormal before setting display state"

    return-object v0

    :cond_4
    const-string/jumbo v0, "Abnormal in setting display state and blocking screen on"

    return-object v0
.end method

.method private getWakeupSrcIndex(Ljava/lang/String;)I
    .locals 1

    const-string/jumbo v0, "android.policy:POWER"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    return v0

    :cond_0
    const-string/jumbo v0, "miui.policy:FINGERPRINT_DPAD_CENTER"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    return v0

    :cond_1
    const-string/jumbo v0, "keyguard_screenon_notification"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    return v0

    :cond_2
    const-string/jumbo v0, "keyguard_screenon_finger_pass"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v0, 0x4

    return v0

    :cond_3
    const-string/jumbo v0, "android.policy:LID"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    const/4 v0, 0x5

    return v0

    :cond_4
    const/4 v0, -0x1

    return v0
.end method

.method private handleRecordTime(IJ)V
    .locals 4

    const-wide/16 v2, 0x0

    iget-wide v0, p0, Lcom/android/server/ScreenOnMonitor;->mStartTime:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    return-void

    :cond_0
    packed-switch p1, :pswitch_data_0

    :cond_1
    :goto_0
    return-void

    :pswitch_0
    iget-wide v0, p0, Lcom/android/server/ScreenOnMonitor;->mSetDisplayStateBegin:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    iput-wide p2, p0, Lcom/android/server/ScreenOnMonitor;->mSetDisplayStateBegin:J

    goto :goto_0

    :pswitch_1
    iget-wide v0, p0, Lcom/android/server/ScreenOnMonitor;->mSetDisplayStateEnd:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    iput-wide p2, p0, Lcom/android/server/ScreenOnMonitor;->mSetDisplayStateEnd:J

    goto :goto_0

    :pswitch_2
    iget-wide v0, p0, Lcom/android/server/ScreenOnMonitor;->mBlockScreenOnBegin:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    iput-wide p2, p0, Lcom/android/server/ScreenOnMonitor;->mBlockScreenOnBegin:J

    goto :goto_0

    :pswitch_3
    iget-wide v0, p0, Lcom/android/server/ScreenOnMonitor;->mBlockScreenOnEnd:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    iput-wide p2, p0, Lcom/android/server/ScreenOnMonitor;->mBlockScreenOnEnd:J

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private handleReport(Z)V
    .locals 18

    invoke-direct/range {p0 .. p0}, Lcom/android/server/ScreenOnMonitor;->getScreenOnDetail()Ljava/lang/String;

    move-result-object v8

    const-string/jumbo v9, "ScreenOnMonitor"

    invoke-static {v9, v8}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/android/server/ScreenOnMonitor;->mTimeoutSummary:Ljava/lang/String;

    if-eqz v9, :cond_2

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v4

    move-object/from16 v0, p0

    iget-wide v14, v0, Lcom/android/server/ScreenOnMonitor;->mLastReportTime:J

    const-wide/16 v16, 0x0

    cmp-long v9, v14, v16

    if-eqz v9, :cond_0

    move-object/from16 v0, p0

    iget-wide v14, v0, Lcom/android/server/ScreenOnMonitor;->mLastReportTime:J

    sub-long v14, v4, v14

    const-wide/32 v16, 0xdbba00

    cmp-long v9, v14, v16

    if-lez v9, :cond_1

    :cond_0
    move-object/from16 v0, p0

    iput-wide v4, v0, Lcom/android/server/ScreenOnMonitor;->mLastReportTime:J

    new-instance v6, Lmiui/mqsas/sdk/event/ScreenOnEvent;

    invoke-direct {v6}, Lmiui/mqsas/sdk/event/ScreenOnEvent;-><init>()V

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/android/server/ScreenOnMonitor;->mTimeoutSummary:Ljava/lang/String;

    invoke-virtual {v6, v9}, Lmiui/mqsas/sdk/event/ScreenOnEvent;->setTimeoutSummary(Ljava/lang/String;)V

    invoke-virtual {v6, v8}, Lmiui/mqsas/sdk/event/ScreenOnEvent;->setmTimeOutDetail(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/android/server/ScreenOnMonitor;->mWakeSource:Ljava/lang/String;

    invoke-virtual {v6, v9}, Lmiui/mqsas/sdk/event/ScreenOnEvent;->setWakeSource(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-wide v14, v0, Lcom/android/server/ScreenOnMonitor;->mTimeStamp:J

    move-object/from16 v0, p0

    invoke-direct {v0, v14, v15}, Lcom/android/server/ScreenOnMonitor;->toCalendarTime(J)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v6, v9}, Lmiui/mqsas/sdk/event/ScreenOnEvent;->setTimeStamp(Ljava/lang/String;)V

    const-string/jumbo v9, "lt_screen_on"

    invoke-virtual {v6, v9}, Lmiui/mqsas/sdk/event/ScreenOnEvent;->setScreenOnType(Ljava/lang/String;)V

    invoke-static {}, Lmiui/mqsas/sdk/MQSEventManagerDelegate;->getInstance()Lmiui/mqsas/sdk/MQSEventManagerDelegate;

    move-result-object v9

    invoke-virtual {v9, v6}, Lmiui/mqsas/sdk/MQSEventManagerDelegate;->reportScreenOnEvent(Lmiui/mqsas/sdk/event/ScreenOnEvent;)V

    :cond_1
    const/4 v9, 0x0

    move-object/from16 v0, p0

    iput-object v9, v0, Lcom/android/server/ScreenOnMonitor;->mTimeoutSummary:Ljava/lang/String;

    :cond_2
    if-eqz p1, :cond_7

    move-object/from16 v0, p0

    iget-wide v14, v0, Lcom/android/server/ScreenOnMonitor;->mStopTime:J

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/android/server/ScreenOnMonitor;->mStartTime:J

    move-wide/from16 v16, v0

    sub-long v14, v14, v16

    const-wide/16 v16, 0x3e8

    cmp-long v9, v14, v16

    if-gez v9, :cond_7

    sget-object v9, Landroid/os/Build$VERSION;->INCREMENTAL:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/server/ScreenOnMonitor;->mUploadVersion:Ljava/lang/String;

    invoke-virtual {v9, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    xor-int/lit8 v9, v9, 0x1

    if-eqz v9, :cond_7

    move-object/from16 v0, p0

    iget-wide v14, v0, Lcom/android/server/ScreenOnMonitor;->mStopTime:J

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/android/server/ScreenOnMonitor;->mStartTime:J

    move-wide/from16 v16, v0

    sub-long v10, v14, v16

    move-object/from16 v0, p0

    iget-wide v14, v0, Lcom/android/server/ScreenOnMonitor;->mSetDisplayStateEnd:J

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/android/server/ScreenOnMonitor;->mSetDisplayStateBegin:J

    move-wide/from16 v16, v0

    sub-long v12, v14, v16

    move-object/from16 v0, p0

    iget-wide v14, v0, Lcom/android/server/ScreenOnMonitor;->mBlockScreenOnEnd:J

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/android/server/ScreenOnMonitor;->mBlockScreenOnBegin:J

    move-wide/from16 v16, v0

    sub-long v2, v14, v16

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/android/server/ScreenOnMonitor;->mNeedRecord:[Z

    const/4 v14, 0x0

    aget-boolean v9, v9, v14

    if-eqz v9, :cond_3

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/android/server/ScreenOnMonitor;->mAvgCount:[I

    const/4 v14, 0x0

    aget v15, v9, v14

    add-int/lit8 v15, v15, 0x1

    aput v15, v9, v14

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/android/server/ScreenOnMonitor;->mTotalScreenOnTime:[J

    const/4 v14, 0x0

    aget-wide v16, v9, v14

    add-long v16, v16, v10

    aput-wide v16, v9, v14

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/android/server/ScreenOnMonitor;->mTotalSetDisplayTime:[J

    const/4 v14, 0x0

    aget-wide v16, v9, v14

    add-long v16, v16, v12

    aput-wide v16, v9, v14

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/android/server/ScreenOnMonitor;->mTotalBlockScreenOnTime:[J

    const/4 v14, 0x0

    aget-wide v16, v9, v14

    add-long v16, v16, v2

    aput-wide v16, v9, v14

    :cond_3
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/android/server/ScreenOnMonitor;->mAvgCount:[I

    const/4 v14, 0x0

    aget v9, v9, v14

    int-to-long v14, v9

    const-wide/16 v16, 0x32

    cmp-long v9, v14, v16

    if-nez v9, :cond_4

    new-instance v6, Lmiui/mqsas/sdk/event/ScreenOnEvent;

    invoke-direct {v6}, Lmiui/mqsas/sdk/event/ScreenOnEvent;-><init>()V

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/android/server/ScreenOnMonitor;->mTotalScreenOnTime:[J

    const/4 v14, 0x0

    aget-wide v14, v9, v14

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/android/server/ScreenOnMonitor;->mAvgCount:[I

    const/16 v16, 0x0

    aget v9, v9, v16

    int-to-long v0, v9

    move-wide/from16 v16, v0

    div-long v14, v14, v16

    invoke-virtual {v6, v14, v15}, Lmiui/mqsas/sdk/event/ScreenOnEvent;->setTotalTime(J)V

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/android/server/ScreenOnMonitor;->mTotalSetDisplayTime:[J

    const/4 v14, 0x0

    aget-wide v14, v9, v14

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/android/server/ScreenOnMonitor;->mAvgCount:[I

    const/16 v16, 0x0

    aget v9, v9, v16

    int-to-long v0, v9

    move-wide/from16 v16, v0

    div-long v14, v14, v16

    invoke-virtual {v6, v14, v15}, Lmiui/mqsas/sdk/event/ScreenOnEvent;->setSetDisplayTime(J)V

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/android/server/ScreenOnMonitor;->mTotalBlockScreenOnTime:[J

    const/4 v14, 0x0

    aget-wide v14, v9, v14

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/android/server/ScreenOnMonitor;->mAvgCount:[I

    const/16 v16, 0x0

    aget v9, v9, v16

    int-to-long v0, v9

    move-wide/from16 v16, v0

    div-long v14, v14, v16

    invoke-virtual {v6, v14, v15}, Lmiui/mqsas/sdk/event/ScreenOnEvent;->setBlockScreenTime(J)V

    const-string/jumbo v9, "avg_screen_on"

    invoke-virtual {v6, v9}, Lmiui/mqsas/sdk/event/ScreenOnEvent;->setScreenOnType(Ljava/lang/String;)V

    invoke-static {}, Lmiui/mqsas/sdk/MQSEventManagerDelegate;->getInstance()Lmiui/mqsas/sdk/MQSEventManagerDelegate;

    move-result-object v9

    invoke-virtual {v9, v6}, Lmiui/mqsas/sdk/MQSEventManagerDelegate;->reportScreenOnEvent(Lmiui/mqsas/sdk/event/ScreenOnEvent;)V

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/android/server/ScreenOnMonitor;->mTotalScreenOnTime:[J

    const-wide/16 v14, 0x0

    const/16 v16, 0x0

    aput-wide v14, v9, v16

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/android/server/ScreenOnMonitor;->mTotalSetDisplayTime:[J

    const-wide/16 v14, 0x0

    const/16 v16, 0x0

    aput-wide v14, v9, v16

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/android/server/ScreenOnMonitor;->mTotalBlockScreenOnTime:[J

    const-wide/16 v14, 0x0

    const/16 v16, 0x0

    aput-wide v14, v9, v16

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/android/server/ScreenOnMonitor;->mAvgCount:[I

    const/4 v14, 0x0

    const/4 v15, 0x0

    aput v14, v9, v15

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/android/server/ScreenOnMonitor;->mNeedRecord:[Z

    const/4 v14, 0x0

    const/4 v15, 0x0

    aput-boolean v14, v9, v15

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/android/server/ScreenOnMonitor;->mTypeNeedRecordSb:Ljava/lang/StringBuilder;

    const/4 v14, 0x0

    const/16 v15, 0x31

    invoke-virtual {v9, v14, v15}, Ljava/lang/StringBuilder;->setCharAt(IC)V

    sget-object v9, Lcom/android/server/ScreenOnMonitor;->PROPERTY_SCREEN_ON_UPLOAD:Ljava/lang/String;

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/android/server/ScreenOnMonitor;->mTypeNeedRecordSb:Ljava/lang/StringBuilder;

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string/jumbo v15, ","

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/android/server/ScreenOnMonitor;->mUploadVersion:Ljava/lang/String;

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v9, v14}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    :cond_4
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/android/server/ScreenOnMonitor;->mWakeSource:Ljava/lang/String;

    move-object/from16 v0, p0

    invoke-direct {v0, v9}, Lcom/android/server/ScreenOnMonitor;->getWakeupSrcIndex(Ljava/lang/String;)I

    move-result v7

    const/4 v9, -0x1

    if-ne v7, v9, :cond_5

    return-void

    :cond_5
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/android/server/ScreenOnMonitor;->mNeedRecord:[Z

    aget-boolean v9, v9, v7

    if-eqz v9, :cond_6

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/android/server/ScreenOnMonitor;->mAvgCount:[I

    aget v14, v9, v7

    add-int/lit8 v14, v14, 0x1

    aput v14, v9, v7

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/android/server/ScreenOnMonitor;->mTotalScreenOnTime:[J

    aget-wide v14, v9, v7

    add-long/2addr v14, v10

    aput-wide v14, v9, v7

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/android/server/ScreenOnMonitor;->mTotalSetDisplayTime:[J

    aget-wide v14, v9, v7

    add-long/2addr v14, v12

    aput-wide v14, v9, v7

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/android/server/ScreenOnMonitor;->mTotalBlockScreenOnTime:[J

    aget-wide v14, v9, v7

    add-long/2addr v14, v2

    aput-wide v14, v9, v7

    :cond_6
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/android/server/ScreenOnMonitor;->mAvgCount:[I

    aget v9, v9, v7

    int-to-long v14, v9

    const-wide/16 v16, 0x32

    cmp-long v9, v14, v16

    if-nez v9, :cond_7

    new-instance v6, Lmiui/mqsas/sdk/event/ScreenOnEvent;

    invoke-direct {v6}, Lmiui/mqsas/sdk/event/ScreenOnEvent;-><init>()V

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/android/server/ScreenOnMonitor;->mTotalScreenOnTime:[J

    aget-wide v14, v9, v7

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/android/server/ScreenOnMonitor;->mAvgCount:[I

    aget v9, v9, v7

    int-to-long v0, v9

    move-wide/from16 v16, v0

    div-long v14, v14, v16

    invoke-virtual {v6, v14, v15}, Lmiui/mqsas/sdk/event/ScreenOnEvent;->setTotalTime(J)V

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/android/server/ScreenOnMonitor;->mTotalSetDisplayTime:[J

    aget-wide v14, v9, v7

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/android/server/ScreenOnMonitor;->mAvgCount:[I

    aget v9, v9, v7

    int-to-long v0, v9

    move-wide/from16 v16, v0

    div-long v14, v14, v16

    invoke-virtual {v6, v14, v15}, Lmiui/mqsas/sdk/event/ScreenOnEvent;->setSetDisplayTime(J)V

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/android/server/ScreenOnMonitor;->mTotalBlockScreenOnTime:[J

    aget-wide v14, v9, v7

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/android/server/ScreenOnMonitor;->mAvgCount:[I

    aget v9, v9, v7

    int-to-long v0, v9

    move-wide/from16 v16, v0

    div-long v14, v14, v16

    invoke-virtual {v6, v14, v15}, Lmiui/mqsas/sdk/event/ScreenOnEvent;->setBlockScreenTime(J)V

    sget-object v9, Lmiui/mqsas/sdk/event/ScreenOnEvent;->TYPE_SCREEN_ON:[Ljava/lang/String;

    aget-object v9, v9, v7

    invoke-virtual {v6, v9}, Lmiui/mqsas/sdk/event/ScreenOnEvent;->setScreenOnType(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/android/server/ScreenOnMonitor;->mWakeSource:Ljava/lang/String;

    invoke-virtual {v6, v9}, Lmiui/mqsas/sdk/event/ScreenOnEvent;->setWakeSource(Ljava/lang/String;)V

    invoke-static {}, Lmiui/mqsas/sdk/MQSEventManagerDelegate;->getInstance()Lmiui/mqsas/sdk/MQSEventManagerDelegate;

    move-result-object v9

    invoke-virtual {v9, v6}, Lmiui/mqsas/sdk/MQSEventManagerDelegate;->reportScreenOnEvent(Lmiui/mqsas/sdk/event/ScreenOnEvent;)V

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/android/server/ScreenOnMonitor;->mTotalScreenOnTime:[J

    const-wide/16 v14, 0x0

    aput-wide v14, v9, v7

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/android/server/ScreenOnMonitor;->mTotalSetDisplayTime:[J

    const-wide/16 v14, 0x0

    aput-wide v14, v9, v7

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/android/server/ScreenOnMonitor;->mTotalBlockScreenOnTime:[J

    const-wide/16 v14, 0x0

    aput-wide v14, v9, v7

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/android/server/ScreenOnMonitor;->mAvgCount:[I

    const/4 v14, 0x0

    aput v14, v9, v7

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/android/server/ScreenOnMonitor;->mNeedRecord:[Z

    const/4 v14, 0x0

    aput-boolean v14, v9, v7

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/android/server/ScreenOnMonitor;->mTypeNeedRecordSb:Ljava/lang/StringBuilder;

    const/16 v14, 0x31

    invoke-virtual {v9, v7, v14}, Ljava/lang/StringBuilder;->setCharAt(IC)V

    sget-object v9, Lcom/android/server/ScreenOnMonitor;->PROPERTY_SCREEN_ON_UPLOAD:Ljava/lang/String;

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/android/server/ScreenOnMonitor;->mTypeNeedRecordSb:Ljava/lang/StringBuilder;

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string/jumbo v15, ","

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/android/server/ScreenOnMonitor;->mUploadVersion:Ljava/lang/String;

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v9, v14}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct/range {p0 .. p0}, Lcom/android/server/ScreenOnMonitor;->needRecordScreenOn()Z

    move-result v9

    if-nez v9, :cond_7

    sget-object v9, Lcom/android/server/ScreenOnMonitor;->PROPERTY_SCREEN_ON_UPLOAD:Ljava/lang/String;

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/android/server/ScreenOnMonitor;->mTypeNeedRecordSb:Ljava/lang/StringBuilder;

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string/jumbo v15, ","

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    sget-object v15, Landroid/os/Build$VERSION;->INCREMENTAL:Ljava/lang/String;

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v9, v14}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    :cond_7
    return-void
.end method

.method private handleScreenOnTimeout()V
    .locals 4

    invoke-direct {p0}, Lcom/android/server/ScreenOnMonitor;->getTimeoutSummary()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/ScreenOnMonitor;->mTimeoutSummary:Ljava/lang/String;

    const-string/jumbo v0, "ScreenOnMonitor"

    iget-object v1, p0, Lcom/android/server/ScreenOnMonitor;->mTimeoutSummary:Ljava/lang/String;

    invoke-static {v0, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/server/ScreenOnMonitor;->mHandler:Lcom/android/server/ScreenOnMonitor$ScreenOnMonitorHandler;

    const/4 v1, 0x5

    const-wide/16 v2, 0x7d0

    invoke-virtual {v0, v1, v2, v3}, Lcom/android/server/ScreenOnMonitor$ScreenOnMonitorHandler;->sendEmptyMessageDelayed(IJ)Z

    return-void
.end method

.method private handleStartMonitor(Lcom/android/internal/os/SomeArgs;)V
    .locals 4

    const-wide/16 v2, 0x0

    :try_start_0
    iget-wide v0, p0, Lcom/android/server/ScreenOnMonitor;->mStartTime:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/android/internal/os/SomeArgs;->recycle()V

    return-void

    :cond_0
    :try_start_1
    iget-object v0, p1, Lcom/android/internal/os/SomeArgs;->arg1:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/android/server/ScreenOnMonitor;->mStartTime:J

    iget-object v0, p1, Lcom/android/internal/os/SomeArgs;->arg2:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/android/server/ScreenOnMonitor;->mWakeSource:Ljava/lang/String;

    iget-object v0, p1, Lcom/android/internal/os/SomeArgs;->arg3:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/android/server/ScreenOnMonitor;->mTimeStamp:J

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/android/server/ScreenOnMonitor;->mStopTime:J

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/android/server/ScreenOnMonitor;->mBlockScreenOnBegin:J

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/android/server/ScreenOnMonitor;->mBlockScreenOnEnd:J

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/android/server/ScreenOnMonitor;->mSetDisplayStateBegin:J

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/android/server/ScreenOnMonitor;->mSetDisplayStateEnd:J

    iget-object v0, p0, Lcom/android/server/ScreenOnMonitor;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/android/server/ScreenOnMonitor;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->acquire()V

    :cond_1
    iget-object v0, p0, Lcom/android/server/ScreenOnMonitor;->mHandler:Lcom/android/server/ScreenOnMonitor$ScreenOnMonitorHandler;

    const/4 v1, 0x4

    const-wide/16 v2, 0x3e8

    invoke-virtual {v0, v1, v2, v3}, Lcom/android/server/ScreenOnMonitor$ScreenOnMonitorHandler;->sendEmptyMessageDelayed(IJ)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    invoke-virtual {p1}, Lcom/android/internal/os/SomeArgs;->recycle()V

    return-void

    :catchall_0
    move-exception v0

    invoke-virtual {p1}, Lcom/android/internal/os/SomeArgs;->recycle()V

    throw v0
.end method

.method private handleStopMonitor(Z)V
    .locals 4

    const-wide/16 v2, 0x0

    iget-object v0, p0, Lcom/android/server/ScreenOnMonitor;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/server/ScreenOnMonitor;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    :cond_0
    iget-wide v0, p0, Lcom/android/server/ScreenOnMonitor;->mStartTime:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/server/ScreenOnMonitor;->mHandler:Lcom/android/server/ScreenOnMonitor$ScreenOnMonitorHandler;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/android/server/ScreenOnMonitor$ScreenOnMonitorHandler;->removeMessages(I)V

    iget-object v0, p0, Lcom/android/server/ScreenOnMonitor;->mHandler:Lcom/android/server/ScreenOnMonitor$ScreenOnMonitorHandler;

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Lcom/android/server/ScreenOnMonitor$ScreenOnMonitorHandler;->removeMessages(I)V

    if-eqz p1, :cond_2

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/android/server/ScreenOnMonitor;->handleReport(Z)V

    :cond_2
    iput-wide v2, p0, Lcom/android/server/ScreenOnMonitor;->mStartTime:J

    return-void
.end method

.method private needRecordScreenOn()Z
    .locals 5

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/android/server/ScreenOnMonitor;->mNeedRecord:[Z

    array-length v4, v3

    move v1, v2

    :goto_0
    if-ge v1, v4, :cond_1

    aget-boolean v0, v3, v1

    if-eqz v0, :cond_0

    const/4 v1, 0x1

    return v1

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    return v2
.end method

.method private toCalendarTime(J)Ljava/lang/String;
    .locals 3

    sget-object v0, Lcom/android/server/ScreenOnMonitor;->DATE:Ljava/util/Date;

    invoke-virtual {v0, p1, p2}, Ljava/util/Date;->setTime(J)V

    sget-object v0, Lcom/android/server/ScreenOnMonitor;->SIMPLE_DATE_FORMAT:Ljava/text/SimpleDateFormat;

    sget-object v1, Lcom/android/server/ScreenOnMonitor;->DATE:Ljava/util/Date;

    invoke-virtual {v0, v1}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public recordTime(I)V
    .locals 5

    sget-boolean v0, Lmiui/os/Build;->IS_STABLE_VERSION:Z

    if-eqz v0, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/server/ScreenOnMonitor;->mHandler:Lcom/android/server/ScreenOnMonitor$ScreenOnMonitorHandler;

    iget-object v1, p0, Lcom/android/server/ScreenOnMonitor;->mHandler:Lcom/android/server/ScreenOnMonitor$ScreenOnMonitorHandler;

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    const/4 v3, 0x3

    const/4 v4, -0x1

    invoke-virtual {v1, v3, p1, v4, v2}, Lcom/android/server/ScreenOnMonitor$ScreenOnMonitorHandler;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/server/ScreenOnMonitor$ScreenOnMonitorHandler;->sendMessage(Landroid/os/Message;)Z

    return-void
.end method

.method public startMonitor(Ljava/lang/String;)V
    .locals 6

    sget-boolean v1, Lmiui/os/Build;->IS_STABLE_VERSION:Z

    if-nez v1, :cond_0

    iget v1, p0, Lcom/android/server/ScreenOnMonitor;->mDisplayState:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_1

    :cond_0
    return-void

    :cond_1
    iget-wide v2, p0, Lcom/android/server/ScreenOnMonitor;->mStartTime:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-nez v1, :cond_0

    invoke-static {}, Lcom/android/internal/os/SomeArgs;->obtain()Lcom/android/internal/os/SomeArgs;

    move-result-object v0

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    iput-object v1, v0, Lcom/android/internal/os/SomeArgs;->arg1:Ljava/lang/Object;

    iput-object p1, v0, Lcom/android/internal/os/SomeArgs;->arg2:Ljava/lang/Object;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    iput-object v1, v0, Lcom/android/internal/os/SomeArgs;->arg3:Ljava/lang/Object;

    iget-object v1, p0, Lcom/android/server/ScreenOnMonitor;->mHandler:Lcom/android/server/ScreenOnMonitor$ScreenOnMonitorHandler;

    const/4 v2, 0x1

    invoke-virtual {v1, v2, v0}, Lcom/android/server/ScreenOnMonitor$ScreenOnMonitorHandler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/Message;->sendToTarget()V

    return-void
.end method

.method public stopMonitor(II)V
    .locals 3

    const/4 v1, 0x0

    const/4 v2, 0x2

    sget-boolean v0, Lmiui/os/Build;->IS_STABLE_VERSION:Z

    if-eqz v0, :cond_0

    return-void

    :cond_0
    iget v0, p0, Lcom/android/server/ScreenOnMonitor;->mDisplayState:I

    if-ne v0, v2, :cond_2

    if-eq p2, v2, :cond_2

    iget-object v0, p0, Lcom/android/server/ScreenOnMonitor;->mHandler:Lcom/android/server/ScreenOnMonitor$ScreenOnMonitorHandler;

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Lcom/android/server/ScreenOnMonitor$ScreenOnMonitorHandler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    :cond_1
    :goto_0
    iput p1, p0, Lcom/android/server/ScreenOnMonitor;->mDisplayBrightness:I

    iput p2, p0, Lcom/android/server/ScreenOnMonitor;->mDisplayState:I

    return-void

    :cond_2
    iget v0, p0, Lcom/android/server/ScreenOnMonitor;->mDisplayBrightness:I

    if-nez v0, :cond_1

    if-eqz p1, :cond_1

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/android/server/ScreenOnMonitor;->mStopTime:J

    iget-object v0, p0, Lcom/android/server/ScreenOnMonitor;->mHandler:Lcom/android/server/ScreenOnMonitor$ScreenOnMonitorHandler;

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Lcom/android/server/ScreenOnMonitor$ScreenOnMonitorHandler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    goto :goto_0
.end method
